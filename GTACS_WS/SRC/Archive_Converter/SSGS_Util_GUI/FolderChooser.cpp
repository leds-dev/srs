
#include "stdafx.h"
#include <stdio.h>
#include <string.h>
#include <direct.h>
#include <winnetwk.h>

#include "fc_resource.h"
#include "FolderChooser.h"

#if defined(need_this_for_TVE_XXXXX_macros)
#  include "commctrl.h"
#endif

/* Visual Studio 2005 hates you if you call fopen or sprintf or getenv
 */
#pragma warning(disable : 4996)

#define THIS_DBOX_MINIMUM_WIDTH    200
#define THIS_DBOX_MINIMUM_HEIGHT   200

#define NETWORK_NEIGHBORHOOD       _T("Network Neighborhood")

/* When resizing a window, do not make the items within it "active"
 * and do not change the layering order
 */
#define MY_POS_FLAGS               (SWP_NOACTIVATE | SWP_NOZORDER)


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



CDlgFolderChooser::CDlgFolderChooser(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgFolderChooser::IDD, pParent)
{
	TRACE(_T("CDlgFolderChooser::CDlgFolderChooser(%p)\n"), pParent);

	m_hNetworkRoot = NULL;
}


void CDlgFolderChooser::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_FC_TREE, m_Tree);
}


BEGIN_MESSAGE_MAP(CDlgFolderChooser, CDialog)
	ON_WM_SIZING()
	ON_NOTIFY(TVN_SELCHANGED,    IDC_FC_TREE, OnSelchangedTree)
	ON_NOTIFY(TVN_ITEMEXPANDING, IDC_FC_TREE, OnItemexpandingTree)
END_MESSAGE_MAP()


static bool OSO_I_ParsePath(
	CString sPath,              // Full path to parse
	int nLevel,                 // Level to return 0 = the root or drive name
	CString *const psDirName    // returned directory name
)
{
	// Parse the path and extract the given directory or drive name.
	// nLevel 0 will return the drive number or "" for a network name.

	int nStart = 0;             // Location in sPath to search from
	while (nLevel)
	{
		nStart = sPath.Find(_T('\\'), nStart);
		if (nStart < 0)
		{
			return false;    // no more directories
		}
		--nLevel;
		++nStart;
	}

	// We now have the start point to find the end.
	int nEnd = sPath.Find(_T('\\'), nStart);
	*psDirName = (nEnd < 0) ? sPath.Mid(nStart) : sPath.Mid(nStart, nEnd - nStart);

	return true;
}


/* These are the offsets into the 255x16 "filetypes.bmp" file
 * which contains 15 icons (each 17x16).
 *
 * The first seven match to types of "drives" (defined in winbase.h) and
 * the rest match to types of "resource-displays" (defined in winnetwk.h)
 */
#define WIDTH_OF_FILETYPE_ICON        17
#define HEIGHT_OF_FILETYPE_ICON       16
#define FILETYPE_ICON_UNKNOWN        (-1)
#define FILETYPE_ICON_FOLDER_OPEN      0
#define FILETYPE_ICON_FOLDER_CLOSED    1
#define FILETYPE_ICON_REMOVABLE        2
#define FILETYPE_ICON_FIXED            3
#define FILETYPE_ICON_REMOTE           4
#define FILETYPE_ICON_CDROM            5
#define FILETYPE_ICON_RAMDISK          6
#define FILETYPE_ICON_NEIGHBORHOOD     7    /* a.k.a. GENERIC */
#define FILETYPE_ICON_DOMAIN           8
#define FILETYPE_ICON_SERVER           9
#define FILETYPE_ICON_SHARE           10
#define FILETYPE_ICON_FILE            11
#define FILETYPE_ICON_GROUP           12
#define FILETYPE_ICON_NETWORK         13
#define FILETYPE_ICON_ROOT            14
#define FILETYPE_ICON_SHAREADMIN      15    /* no icon for this in the .BMP */
#define FILETYPE_ICON_DIRECTORY       16    /* no icon for this in the .BMP */
#define FILETYPE_ICON_TREE            17    /* no icon for this in the .BMP */
#define FILETYPE_ICON_NDSCONTAINER    18    /* no icon for this in the .BMP */


static int OSO_I_WhichIconForThisDriveType(
	int nDriveType
)
{
	switch (nDriveType)
	{
	case DRIVE_REMOVABLE:   return FILETYPE_ICON_REMOVABLE;
	case DRIVE_FIXED:       return FILETYPE_ICON_FIXED;
	case DRIVE_REMOTE:      return FILETYPE_ICON_REMOTE;
	case DRIVE_CDROM:       return FILETYPE_ICON_CDROM;
	case DRIVE_RAMDISK:     return FILETYPE_ICON_RAMDISK;
	case DRIVE_UNKNOWN:
	case DRIVE_NO_ROOT_DIR:
	default:                return FILETYPE_ICON_UNKNOWN;
	}
}


static int OSO_I_WhichIconForThisResource(
	int nDisplayType
)
{
	/* The old code simply added a hard-coded seven to the given value.
	 */
	switch (nDisplayType)
	{
	case RESOURCEDISPLAYTYPE_GENERIC:       return FILETYPE_ICON_NEIGHBORHOOD;
	case RESOURCEDISPLAYTYPE_DOMAIN:        return FILETYPE_ICON_DOMAIN;
	case RESOURCEDISPLAYTYPE_SERVER:        return FILETYPE_ICON_SERVER;
	case RESOURCEDISPLAYTYPE_SHARE:         return FILETYPE_ICON_SHARE;
	case RESOURCEDISPLAYTYPE_FILE:          return FILETYPE_ICON_FILE;
	case RESOURCEDISPLAYTYPE_GROUP:         return FILETYPE_ICON_GROUP;
	case RESOURCEDISPLAYTYPE_NETWORK:       return FILETYPE_ICON_NETWORK;
	case RESOURCEDISPLAYTYPE_ROOT:          return FILETYPE_ICON_ROOT;
#if defined(NOT_IN_OUR_BITMAP_YET)
	case RESOURCEDISPLAYTYPE_SHAREADMIN:    return FILETYPE_ICON_SHAREADMIN;
	case RESOURCEDISPLAYTYPE_DIRECTORY:     return FILETYPE_ICON_DIRECTORY;
	case RESOURCEDISPLAYTYPE_TREE:          return FILETYPE_ICON_TREE;
	case RESOURCEDISPLAYTYPE_NDSCONTAINER:  return FILETYPE_ICON_NDSCONTAINER;
#endif
	default:                                return FILETYPE_ICON_UNKNOWN;
	}
}


BOOL CDlgFolderChooser::OnInitDialog()
{
	TRACE(_T("CDlgFolderChooser::OnInitDialog()\n"));

	this->SetWindowText((m_sTitle == "") ? "Choose a Folder" : m_sTitle);

	CDialog::OnInitDialog();

	if (m_ImageListTree.Create(IDB_FC_FILETYPES, WIDTH_OF_FILETYPE_ICON, 1, RGB(255, 255, 255)) == 0)
	{
		char cMessage[512];
		sprintf(cMessage, "Unable to create Image list tree using file %s", "res\\filetypes.bmp");
		AfxMessageBox(cMessage, MB_OK | MB_ICONEXCLAMATION);
	}
	m_Tree.SetImageList(&m_ImageListTree, TVSIL_NORMAL);

	/* List the local drives
	 */
	for (TCHAR cLetter = _T('A'); cLetter <= _T('Z'); cLetter++)
	{
		CString sDrive = cLetter;
		sDrive += _T(":");
		UINT nType = GetDriveType(sDrive + _T("\\"));
		switch (nType)
		{
		case DRIVE_FIXED:
		case DRIVE_CDROM:
		case DRIVE_REMOTE:
		case DRIVE_RAMDISK:
		case DRIVE_REMOVABLE:
			InsertItem(TVI_ROOT, NULL, sDrive, OSO_I_WhichIconForThisDriveType(nType), true);
			break;
		case DRIVE_UNKNOWN:
		case DRIVE_NO_ROOT_DIR:
			/* ignore this letter */
			break;
		}
	}

	/* List the network neighbourhood
	 */
	m_hNetworkRoot = InsertItem(TVI_ROOT, NULL, NETWORK_NEIGHBORHOOD, FILETYPE_ICON_NEIGHBORHOOD, true);

	/* If the initial value for the path was not specified via SetPath()
	 * then use the process's current default directory.
	 */
	if (m_sPath == "")
	{
		int nDrive = _getdrive();
		TCHAR szPath[_MAX_PATH];
		_getdcwd(nDrive, szPath, _MAX_PATH);
		m_sPath = szPath;
	}

	/* Expand the tree to the initial path
	 */
	int nLevel = 0;
	CString sDirName;
	HTREEITEM hItem;
	HTREEITEM hCurrent = TVI_ROOT;					//Current item in the list being expanded.
	while (OSO_I_ParsePath(m_sPath, nLevel, &sDirName))
	{
		//Network or Drive expand
		if (nLevel == 0 && sDirName.IsEmpty())
		{
			sDirName = NETWORK_NEIGHBORHOOD;
		}
		//Search for the matching tree item and expand
		for (hItem = m_Tree.GetChildItem(hCurrent); hItem != NULL; hItem = m_Tree.GetNextSiblingItem(hItem))
		{
			if (sDirName.CompareNoCase(m_Tree.GetItemText(hItem)) == 0)
			{
				hCurrent = hItem;
				m_Tree.Expand(hCurrent, TVE_EXPAND);
				break;
			}
		}
		++nLevel;
	}

	return TRUE;
}


/* this hook is necessary to properly resize the contents of the dialog box
 */
void CDlgFolderChooser::OnSizing(
	UINT nSide,
	LPRECT lpRect
)
{
	int nDeltaWidth;
	int nDeltaHeight;

	/* override any attempt to make the dialog box too skinny or too short
	 */
	if (lpRect->right - lpRect->left < THIS_DBOX_MINIMUM_WIDTH)
	{
		if (nSide == WMSZ_LEFT || nSide == WMSZ_TOPLEFT || nSide == WMSZ_BOTTOMLEFT)
		{
			lpRect->left = lpRect->right - THIS_DBOX_MINIMUM_WIDTH;
		}
		else
		{
			lpRect->right = lpRect->left + THIS_DBOX_MINIMUM_WIDTH;
		}
	}
	if (lpRect->bottom - lpRect->top < THIS_DBOX_MINIMUM_HEIGHT)
	{
		if (nSide == WMSZ_TOP || nSide == WMSZ_TOPLEFT || nSide == WMSZ_TOPRIGHT)
		{
			lpRect->top = lpRect->bottom - THIS_DBOX_MINIMUM_HEIGHT;
		}
		else
		{
			lpRect->bottom = lpRect->top + THIS_DBOX_MINIMUM_HEIGHT;
		}
	}


	{
		CRect sBefore;
		this->GetWindowRect(&sBefore);
		nDeltaWidth  = (lpRect->right  - lpRect->left) - (sBefore.right  - sBefore.left);
		nDeltaHeight = (lpRect->bottom - lpRect->top)  - (sBefore.bottom - sBefore.top);
	}

	CWnd::OnSizing(nSide, lpRect);

	/* This hook is frequently called even when there is no change,
	 * so don't bother doing the work if there's no reason to
	 */
	if (nDeltaWidth || nDeltaHeight)
	{
		HDWP hdwp = BeginDeferWindowPos(4);
		for (CWnd *pChild = GetWindow(GW_CHILD); pChild != NULL; pChild = pChild->GetWindow(GW_HWNDNEXT))
		{
			CRect r1;
			pChild->GetWindowRect(&r1);
			ScreenToClient(&r1);
			long nCode;
			nCode = pChild->SendMessage(WM_GETDLGCODE);
			if (nCode & DLGC_BUTTON)
			{
				// This is the OK or CANCEL button ---
				// move in one or two directions, but do not resize
				r1.left   += nDeltaWidth;
				r1.right  += nDeltaWidth;
				r1.top    += nDeltaHeight;
				r1.bottom += nDeltaHeight;
			}
			else if (nCode & DLGC_STATIC)
			{
				/* This is the "current path selected" shown above the tree list ---
				 * allow only the width to be resized, and do not move TopLeft or BottmLeft corners
				 */
				r1.right  += nDeltaWidth;
			}
			else
			{
				/* This is the tree list itself ---
				 * resize in one or two dimensions, but do not move TopLeft corner
				 */
				r1.right  += nDeltaWidth;
				r1.bottom += nDeltaHeight;
			}
			int nNewWidth  = r1.right  - r1.left;
			int nNewHeight = r1.bottom - r1.top;
			if (hdwp != NULL)
			{
				hdwp = DeferWindowPos(hdwp, pChild->m_hWnd, NULL, r1.left, r1.top, nNewWidth, nNewHeight, MY_POS_FLAGS);
			}
			else
			{
				pChild->SetWindowPos(NULL, r1.left, r1.top, nNewWidth, nNewHeight, MY_POS_FLAGS);
			}
		}
		if (hdwp != NULL)
		{
			EndDeferWindowPos(hdwp);
		}
	}
}



HTREEITEM CDlgFolderChooser::InsertItem(
	HTREEITEM hParent,                 // Parent to Attach to
	NETRESOURCE *const pNetResource,
	CString sText,                     // Text to Add
	int iImage,                        // a FILETYPE_ICON_XXXX value
	bool bShowImage
)
{
	TRACE(_T("CDlgFolderChooser::InsertItem(%p,%p,%s +++)\n"), hParent, pNetResource, sText);

	TVINSERTSTRUCT sInsert;

	sInsert.hParent               = hParent;
	sInsert.hInsertAfter          = TVI_LAST;
	sInsert.itemex.mask           = TVIF_IMAGE | TVIF_TEXT | TVIF_CHILDREN | TVIF_SELECTEDIMAGE | TVIF_PARAM;
	sInsert.itemex.pszText        = sText.GetBuffer(sText.GetLength());  sText.ReleaseBuffer();
	sInsert.itemex.iImage         = iImage;
	sInsert.itemex.cChildren      = 1;
	sInsert.itemex.lParam         = (LPARAM) pNetResource;
	sInsert.itemex.iSelectedImage = (bShowImage) ? iImage : 0;  /* zero means do not display an icon */

	return m_Tree.InsertItem(&sInsert);
}


bool CDlgFolderChooser::PopulateTree(
	CString sPath,       // Path to use for populating from
	HTREEITEM hParent    // Where to add the data
)
{
	// Called in request to expand an item in the tree

	TRACE(_T("CDlgFolderChooser::PopulateTree(%s)\n"), sPath);
	bool bGotChildren = false;						//True if a child is added.

	/* Populate Network neighbourhood tree (Entire network and Local-Computers)
	 */
	if (hParent == m_hNetworkRoot)
	{
		bGotChildren = EnumNetwork(hParent);
	}

	/* Network item(Search deep into the network)
	 */
	else if (m_Tree.GetItemData(hParent))
	{
		bGotChildren = EnumNetwork(hParent);
	}

	/* Search for files and populate the CTreeCtrl
	 */
	else
	{
		CFileFind finder;
		BOOL bWorking = finder.FindFile(sPath + _T("*.*"));
		while (bWorking)
		{
			bWorking = finder.FindNextFile();
			if (! finder.IsDots())
			{
				if (finder.IsDirectory())
				{
					InsertItem(hParent, NULL, finder.GetFileName(), FILETYPE_ICON_FOLDER_CLOSED, false);
					bGotChildren = true;
				}
			}
		}
	}

	/* Remove the [+] if no children
	 */
	if (! bGotChildren)
	{
		TVITEM item    = { 0 };
		item.mask      = TVIF_HANDLE | TVIF_CHILDREN;
		item.hItem     = hParent;
		item.cChildren = 0;
		m_Tree.SetItem(&item);
	}

	return bGotChildren;
}


CString CDlgFolderChooser::GetItemPath(
	HTREEITEM hItem
)
{
	TRACE(_T("CDlgFolderChooser::GetItemPath(%p)\n"), hItem);

	CString sRet;

	while (hItem != NULL)
	{
		/* If this item is a share name, use it and then stop building . . .
		 * otherwise, simply prepend this item's name to the string we're building
		 * and keep following the chain.
		 */
		NETRESOURCE *const pNetResource = (NETRESOURCE *) m_Tree.GetItemData(hItem);
		if (pNetResource != NULL)
		{
			sRet  = CString(pNetResource->lpRemoteName) + _T('\\') + sRet;
			hItem = NULL;
		}
		else
		{
			sRet  = m_Tree.GetItemText(hItem) + _T('\\') + sRet;
			hItem = m_Tree.GetParentItem(hItem);
		}
	}

	return sRet;
}




void CDlgFolderChooser::OnItemexpandingTree(
	NMHDR* pNMHDR,
	LRESULT* pResult
)
{
	TRACE(_T("CDlgFolderChooser::OnItemexpandingTree(%p)\n"), pNMHDR);

	/* A node on the tree is about to expand . . .
	 * Called when some user tries to expand the tree.
	 */

	if (pNMHDR != NULL)
	{
		CWaitCursor CursorWaiting;						//Show the wait cursor while expanding
		NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW *) pNMHDR;
		switch (pNMTreeView->action)
		{
		case TVE_EXPAND:
			/* Update the location display and refresh the children
			 */
			{
				CString sPath = GetItemPath(pNMTreeView->itemNew.hItem);
				if (! m_Tree.GetChildItem(pNMTreeView->itemNew.hItem))
				{
					PopulateTree(sPath, pNMTreeView->itemNew.hItem);
					if (m_Tree.GetSelectedItem() != pNMTreeView->itemNew.hItem)
					{
						m_Tree.SelectItem(pNMTreeView->itemNew.hItem);
					}
				}
			}
			break;
		case TVE_COLLAPSE:
		case TVE_TOGGLE:
		case TVE_EXPANDPARTIAL:
		case TVE_COLLAPSERESET:
			/* ignore */
			break;
		}
	}

	if (pResult != NULL)
	{
		*pResult = 0;
	}
}


void CDlgFolderChooser::OnSelchangedTree(
	NMHDR* pNMHDR,
	LRESULT* pResult
)
{
	/* Update the Edit dox with the new selection path
	 */

	TRACE(_T("CDlgFolderChooser::OnSelchangedTree(%p)\n"), pNMHDR);

	CString sPath = "";      // Currently selected path or empty if not valid

	if (pNMHDR != NULL)
	{
		NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW *) pNMHDR;

		/* Disable search on Workstation roots
		 */
		if (m_Tree.GetItemData(pNMTreeView->itemNew.hItem) || pNMTreeView->itemNew.hItem == m_hNetworkRoot)
		{
			GetDlgItem(IDOK)->EnableWindow(false);
		}
		else
		{
			GetDlgItem(IDOK)->EnableWindow(true);
			sPath = GetItemPath(pNMTreeView->itemNew.hItem);
		}
	}

	if (pResult != NULL)
	{
		*pResult = 0;
	}

	/* display the current selection
	 */
	ASSERT(GetDlgItem(IDC_FC_CURRENTPATH));
	SetDlgItemText(IDC_FC_CURRENTPATH, sPath);
}


static TCHAR* OSO_I_MakeObjectDynamic(
	LPTSTR szData
)
{
	//	Created a duplicate of the given string. (There must be a function for this)

	TRACE(_T("MakeObjectDynamic(%s)\n"), szData);

	int nLength = 0;
	TCHAR * szRet = NULL;

	if (szData)
	{
		nLength = _tcslen(szData) + 1;
	}
	if (nLength > 0)
	{
		szRet = new TCHAR[nLength];
		ASSERT(szRet);
		_tcscpy(szRet, szData);
	}

	return szRet;
}




bool CDlgFolderChooser::EnumNetwork(
	HTREEITEM hParent
)
{
	TRACE(_T("CDlgFolderChooser::EnumNetwork(%p)\n"), hParent);

	bool bGotChildren = false;
	DWORD dwResult;
	char *pcBadNews = NULL;
	HANDLE hEnum = NULL;
	DWORD dwScope;
	DWORD nBufferSize = 16 * 1024;   // We want this much memory for holding the list of objects we find
	DWORD cEntries;
	LPNETRESOURCE lpnrDrv = NULL;
	NETRESOURCE *pNetResource;

	/* If the item already has a network resource, use it
	 */
	pNetResource = (NETRESOURCE *) m_Tree.GetItemData(hParent);
	dwScope = pNetResource ?  RESOURCE_GLOBALNET : RESOURCE_CONTEXT;
	if ((dwResult = WNetOpenEnum(dwScope, RESOURCETYPE_ANY, 0, pNetResource, &hEnum)) != NO_ERROR)
	{
		pcBadNews = "begin";
	}
	else
	{
		/* Get items until no more remain.
		 */
		lpnrDrv = (LPNETRESOURCE) GlobalAlloc(GPTR, nBufferSize);
		while (1)
		{
			int j;
			cEntries = 0xFFFFFFFF;
			if ((dwResult = WNetEnumResource(hEnum, &cEntries, lpnrDrv, &nBufferSize)) != NO_ERROR)
			{
				if (dwResult != ERROR_NO_MORE_ITEMS)
				{
					pcBadNews = "complete";
				}
				break;
			}
			for (j = 0; j < (int) cEntries; j++)
			{
				CString sNameRemote = lpnrDrv[j].lpRemoteName;
				int nType = 9;
				if (sNameRemote.IsEmpty())
				{
					sNameRemote = lpnrDrv[j].lpComment;
					nType = 8;
				}

				/* Remove up to two leading backslashes
				 */
				if (sNameRemote.GetLength() > 0 && sNameRemote[0] == _T('\\'))
				{
					sNameRemote = sNameRemote.Mid(1);
				}
				if (sNameRemote.GetLength() > 0 && sNameRemote[0] == _T('\\'))
				{
					sNameRemote = sNameRemote.Mid(1);
				}

				/* Display either the name of the share or the appropiate icon
				 */
				if (lpnrDrv[j].dwDisplayType == RESOURCEDISPLAYTYPE_SHARE)
				{
					int nPos = sNameRemote.Find(_T('\\'));
					if (nPos >= 0)
					{
						sNameRemote = sNameRemote.Mid(nPos + 1);
					}
					InsertItem(hParent, NULL, sNameRemote, FILETYPE_ICON_FOLDER_CLOSED, false);
				}
				else
				{
					NETRESOURCE* pResource = new NETRESOURCE;
					ASSERT(pResource);
					*pResource = lpnrDrv[j];
					pResource->lpLocalName	= OSO_I_MakeObjectDynamic(pResource->lpLocalName);
					pResource->lpRemoteName = OSO_I_MakeObjectDynamic(pResource->lpRemoteName);
					pResource->lpComment	= OSO_I_MakeObjectDynamic(pResource->lpComment);
					pResource->lpProvider	= OSO_I_MakeObjectDynamic(pResource->lpProvider);
					InsertItem(hParent, pResource, sNameRemote, OSO_I_WhichIconForThisResource(pResource->dwDisplayType), true);
				}
				bGotChildren = true;
			}
		}
		GlobalFree((HGLOBAL) lpnrDrv);
		WNetCloseEnum(hEnum);
	}

	if (pcBadNews != NULL)
	{
		char cMessage[128];
		sprintf(cMessage, "Unable to %s network drive enumeration (error=%d)", pcBadNews, dwResult);
		AfxMessageBox(cMessage, MB_OK | MB_ICONEXCLAMATION);
	}

	return bGotChildren;
}


BOOL CDlgFolderChooser::DestroyWindow()
{
	TRACE(_T("CDlgFolderChooser::DestroyWindow()\n"));

	HTREEITEM hCurrItem;
	HTREEITEM hNextItem;

	/* Save the current selection before heading home
	 */
	m_sPath = GetItemPath(m_Tree.GetSelectedItem());

	hCurrItem = m_Tree.GetRootItem();
	while (hCurrItem != NULL)
	{
		if ((hNextItem = m_Tree.GetChildItem(hCurrItem)) != NULL)
		{
			/* We cannot destroy this node until we've destroyed all of its children
			 */
			hCurrItem = hNextItem;
		}
		else
		{
			/* Item has no children, so we can destroy it,
			 * after determining where to go next in the tree
			 */
			if ((hNextItem = m_Tree.GetNextSiblingItem(hCurrItem)) == NULL)
			{
				if ((hNextItem = m_Tree.GetPrevSiblingItem(hCurrItem)) == NULL)
				{
					hNextItem = m_Tree.GetParentItem(hCurrItem);
				}
			}

			/* If there was a resource for this item, the pointer was stored as the "lParam"
			 * so we need to clean that up before destroying the item itself
			 */
			NETRESOURCE *const pNetResource = (NETRESOURCE *) m_Tree.GetItemData(hCurrItem);
			if (pNetResource != NULL)
			{
				delete [] (pNetResource->lpLocalName);
				delete [] (pNetResource->lpRemoteName);
				delete [] (pNetResource->lpComment);
				delete [] (pNetResource->lpProvider);
				delete pNetResource;
			}
			m_Tree.DeleteItem(hCurrItem);
			hCurrItem = hNextItem;
		}
	}

	/* Okay, the normal destruction can ensue now
	 */
	return CDialog::DestroyWindow();
}
