#ifndef FOLDERCHOOSER_DEFINED
#define FOLDERCHOOSER_DEFINED  1

#include "fc_resource.h"

class CDlgFolderChooser : public CDialog
{
public:
	CDlgFolderChooser(CWnd* pParent = NULL);   // standard constructor

	enum { IDD = IDD_FC_GET_PATH };
	CTreeCtrl m_Tree;

	CString GetPath() const       { return m_sPath; }
	void SetPath(CString sPath)   { m_sPath  = sPath;  }
	void SetTitle(CString sTitle) { m_sTitle = sTitle; }

public:
	virtual BOOL DestroyWindow();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

protected:
	virtual BOOL OnInitDialog();
	afx_msg void OnSizing(UINT nSide, LPRECT lpRect);
	afx_msg void OnSelchangedTree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemexpandingTree(NMHDR* pNMHDR, LRESULT* pResult);
	DECLARE_MESSAGE_MAP()

private:
	CString GetItemPath(HTREEITEM hItem);
	bool PopulateTree(CString sPath, HTREEITEM hParent);
	HTREEITEM InsertItem(HTREEITEM hParent, NETRESOURCE *const pNetResource, CString sText, int iImage, bool bShowImage);
	bool EnumNetwork(HTREEITEM hParent);

private:
	CImageList m_ImageListTree;    // Item in the Tree image list
	HTREEITEM  m_hNetworkRoot;     // Network neighbourhood root
	CString    m_sPath;            // Highlighted path
	CString    m_sTitle;           // contents of the title bar
};

#endif
