
// #define _WIN32_WINNT  0x0500

#include "stdafx.h"
#include "afxtempl.h"

#include "LookupWinMacros.h"

#ifndef WM_GETOBJECT
#define WM_GETOBJECT          0x003D
#endif
#ifndef WM_MENURBUTTONUP
#define WM_MENURBUTTONUP      0x0122
#endif
#ifndef WM_MENUDRAG
#define WM_MENUDRAG           0x0123
#endif
#ifndef WM_MENUGETOBJECT
#define WM_MENUGETOBJECT      0x0124
#endif
#ifndef WM_UNINITMENUPOPUP
#define WM_UNINITMENUPOPUP    0x0125
#endif
#ifndef WM_MENUCOMMAND
#define WM_MENUCOMMAND        0x0126
#endif
#ifndef WM_IME_REQUEST
#define WM_IME_REQUEST        0x0288
#endif

char *OSO_LookupWinMacro_WM(
	UINT uMsg
)
{
	switch (uMsg)
	{
	case WM_NULL:                   return "NULL";
	case WM_CREATE:                 return "CREATE";
	case WM_DESTROY:                return "DESTROY";
	case WM_MOVE:                   return "MOVE";
	case WM_SIZE:                   return "SIZE";
	case WM_ACTIVATE:               return "ACTIVATE";
	case WM_SETFOCUS:               return "SETFOCUS";
	case WM_KILLFOCUS:              return "KILLFOCUS";
	case WM_ENABLE:                 return "ENABLE";
	case WM_SETREDRAW:              return "SETREDRAW";
	case WM_SETTEXT:                return "SETTEXT";
	case WM_GETTEXT:                return "GETTEXT";
	case WM_GETTEXTLENGTH:          return "GETTEXTLENGTH";
	case WM_PAINT:                  return "PAINT";
	case WM_CLOSE:                  return "CLOSE";
	case WM_QUERYENDSESSION:        return "QUERYENDSESSION";
	case WM_QUIT:                   return "QUIT";
	case WM_QUERYOPEN:              return "QUERYOPEN";
	case WM_ERASEBKGND:             return "ERASEBKGND";
	case WM_SYSCOLORCHANGE:         return "SYSCOLORCHANGE";
	case WM_ENDSESSION:             return "ENDSESSION";
	case WM_SHOWWINDOW:             return "SHOWWINDOW";
	case WM_WININICHANGE:           return "WININICHANGE";
	case WM_DEVMODECHANGE:          return "DEVMODECHANGE";
	case WM_ACTIVATEAPP:            return "ACTIVATEAPP";
	case WM_FONTCHANGE:             return "FONTCHANGE";
	case WM_TIMECHANGE:             return "TIMECHANGE";
	case WM_CANCELMODE:             return "CANCELMODE";
	case WM_SETCURSOR:              return "SETCURSOR";
	case WM_MOUSEACTIVATE:          return "MOUSEACTIVATE";
	case WM_CHILDACTIVATE:          return "CHILDACTIVATE";
	case WM_QUEUESYNC:              return "QUEUESYNC";
	case WM_GETMINMAXINFO:          return "GETMINMAXINFO";
	case WM_PAINTICON:              return "PAINTICON";
	case WM_ICONERASEBKGND:         return "ICONERASEBKGND";
	case WM_NEXTDLGCTL:             return "NEXTDLGCTL";
	case WM_SPOOLERSTATUS:          return "SPOOLERSTATUS";
	case WM_DRAWITEM:               return "DRAWITEM";
	case WM_MEASUREITEM:            return "MEASUREITEM";
	case WM_DELETEITEM:             return "DELETEITEM";
	case WM_VKEYTOITEM:             return "VKEYTOITEM";
	case WM_CHARTOITEM:             return "CHARTOITEM";
	case WM_SETFONT:                return "SETFONT";
	case WM_GETFONT:                return "GETFONT";
	case WM_SETHOTKEY:              return "SETHOTKEY";
	case WM_GETHOTKEY:              return "GETHOTKEY";
	case WM_QUERYDRAGICON:          return "QUERYDRAGICON";
	case WM_COMPAREITEM:            return "COMPAREITEM";
	case WM_GETOBJECT:              return "GETOBJECT";
	case WM_COMPACTING:             return "COMPACTING";
	case WM_COMMNOTIFY:             return "COMMNOTIFY";
	case WM_WINDOWPOSCHANGING:      return "WINDOWPOSCHANGING";
	case WM_WINDOWPOSCHANGED:       return "WINDOWPOSCHANGED";
	case WM_POWER:                  return "POWER";
	case WM_COPYDATA:               return "COPYDATA";
	case WM_CANCELJOURNAL:          return "CANCELJOURNAL";
	case WM_NOTIFY:                 return "NOTIFY";
	case WM_INPUTLANGCHANGEREQUEST: return "INPUTLANGCHANGEREQUEST";
	case WM_INPUTLANGCHANGE:        return "INPUTLANGCHANGE";
	case WM_TCARD:                  return "TCARD";
	case WM_HELP:                   return "HELP";
	case WM_USERCHANGED:            return "USERCHANGED";
	case WM_NOTIFYFORMAT:           return "NOTIFYFORMAT";
	case WM_CONTEXTMENU:            return "CONTEXTMENU";
	case WM_STYLECHANGING:          return "STYLECHANGING";
	case WM_STYLECHANGED:           return "STYLECHANGED";
	case WM_DISPLAYCHANGE:          return "DISPLAYCHANGE";
	case WM_GETICON:                return "GETICON";
	case WM_SETICON:                return "SETICON";
	case WM_NCCREATE:               return "NCCREATE";
	case WM_NCDESTROY:              return "NCDESTROY";
	case WM_NCCALCSIZE:             return "NCCALCSIZE";
	case WM_NCHITTEST:              return "NCHITTEST";
	case WM_NCPAINT:                return "NCPAINT";
	case WM_NCACTIVATE:             return "NCACTIVATE";
	case WM_GETDLGCODE:             return "GETDLGCODE";
	case WM_SYNCPAINT:              return "SYNCPAINT";
	case WM_NCMOUSEMOVE:            return "NCMOUSEMOVE";
	case WM_NCLBUTTONDOWN:          return "NCLBUTTONDOWN";
	case WM_NCLBUTTONUP:            return "NCLBUTTONUP";
	case WM_NCLBUTTONDBLCLK:        return "NCLBUTTONDBLCLK";
	case WM_NCRBUTTONDOWN:          return "NCRBUTTONDOWN";
	case WM_NCRBUTTONUP:            return "NCRBUTTONUP";
	case WM_NCRBUTTONDBLCLK:        return "NCRBUTTONDBLCLK";
	case WM_NCMBUTTONDOWN:          return "NCMBUTTONDOWN";
	case WM_NCMBUTTONUP:            return "NCMBUTTONUP";
	case WM_NCMBUTTONDBLCLK:        return "NCMBUTTONDBLCLK";
	case WM_KEYDOWN:                return "KEYDOWN";
	case WM_KEYUP:                  return "KEYUP";
	case WM_CHAR:                   return "CHAR";
	case WM_DEADCHAR:               return "DEADCHAR";
	case WM_SYSKEYDOWN:             return "SYSKEYDOWN";
	case WM_SYSKEYUP:               return "SYSKEYUP";
	case WM_SYSCHAR:                return "SYSCHAR";
	case WM_SYSDEADCHAR:            return "SYSDEADCHAR";
	case WM_KEYLAST:                return "KEYLAST";
	case WM_IME_STARTCOMPOSITION:   return "IME_STARTCOMPOSITION";
	case WM_IME_ENDCOMPOSITION:     return "IME_ENDCOMPOSITION";
	case WM_IME_KEYLAST:            return "IME_KEYLAST";
	case WM_INITDIALOG:             return "INITDIALOG";
	case WM_COMMAND:                return "COMMAND";
	case WM_SYSCOMMAND:             return "SYSCOMMAND";
	case WM_TIMER:                  return "TIMER";
	case WM_HSCROLL:                return "HSCROLL";
	case WM_VSCROLL:                return "VSCROLL";
	case WM_INITMENU:               return "INITMENU";
	case WM_INITMENUPOPUP:          return "INITMENUPOPUP";
	case WM_MENUSELECT:             return "MENUSELECT";
	case WM_MENUCHAR:               return "MENUCHAR";
	case WM_ENTERIDLE:              return "ENTERIDLE";
	case WM_MENURBUTTONUP:          return "MENURBUTTONUP";
	case WM_MENUDRAG:               return "MENUDRAG";
	case WM_MENUGETOBJECT:          return "MENUGETOBJECT";
	case WM_UNINITMENUPOPUP:        return "UNINITMENUPOPUP";
	case WM_MENUCOMMAND:            return "MENUCOMMAND";
	case WM_CTLCOLORMSGBOX:         return "CTLCOLORMSGBOX";
	case WM_CTLCOLOREDIT:           return "CTLCOLOREDIT";
	case WM_CTLCOLORLISTBOX:        return "CTLCOLORLISTBOX";
	case WM_CTLCOLORBTN:            return "CTLCOLORBTN";
	case WM_CTLCOLORDLG:            return "CTLCOLORDLG";
	case WM_CTLCOLORSCROLLBAR:      return "CTLCOLORSCROLLBAR";
	case WM_CTLCOLORSTATIC:         return "CTLCOLORSTATIC";
	case WM_MOUSEMOVE:              return "MOUSEMOVE";
	case WM_LBUTTONDOWN:            return "LBUTTONDOWN";
	case WM_LBUTTONUP:              return "LBUTTONUP";
	case WM_LBUTTONDBLCLK:          return "LBUTTONDBLCLK";
	case WM_RBUTTONDOWN:            return "RBUTTONDOWN";
	case WM_RBUTTONUP:              return "RBUTTONUP";
	case WM_RBUTTONDBLCLK:          return "RBUTTONDBLCLK";
	case WM_MBUTTONDOWN:            return "MBUTTONDOWN";
	case WM_MBUTTONUP:              return "MBUTTONUP";
	case WM_MBUTTONDBLCLK:          return "MBUTTONDBLCLK";
	case WM_MOUSEWHEEL:             return "MOUSEWHEEL";
	case WM_PARENTNOTIFY:           return "PARENTNOTIFY";
	case WM_ENTERMENULOOP:          return "ENTERMENULOOP";
	case WM_EXITMENULOOP:           return "EXITMENULOOP";
	case WM_NEXTMENU:               return "NEXTMENU";
	case WM_SIZING:                 return "SIZING";
	case WM_CAPTURECHANGED:         return "CAPTURECHANGED";
	case WM_MOVING:                 return "MOVING";
	case WM_POWERBROADCAST:         return "POWERBROADCAST";
	case WM_DEVICECHANGE:           return "DEVICECHANGE";
	case WM_MDICREATE:              return "MDICREATE";
	case WM_MDIDESTROY:             return "MDIDESTROY";
	case WM_MDIACTIVATE:            return "MDIACTIVATE";
	case WM_MDIRESTORE:             return "MDIRESTORE";
	case WM_MDINEXT:                return "MDINEXT";
	case WM_MDIMAXIMIZE:            return "MDIMAXIMIZE";
	case WM_MDITILE:                return "MDITILE";
	case WM_MDICASCADE:             return "MDICASCADE";
	case WM_MDIICONARRANGE:         return "MDIICONARRANGE";
	case WM_MDIGETACTIVE:           return "MDIGETACTIVE";
	case WM_MDISETMENU:             return "MDISETMENU";
	case WM_ENTERSIZEMOVE:          return "ENTERSIZEMOVE";
	case WM_EXITSIZEMOVE:           return "EXITSIZEMOVE";
	case WM_DROPFILES:              return "DROPFILES";
	case WM_MDIREFRESHMENU:         return "MDIREFRESHMENU";
	case WM_IME_SETCONTEXT:         return "IME_SETCONTEXT";
	case WM_IME_NOTIFY:             return "IME_NOTIFY";
	case WM_IME_CONTROL:            return "IME_CONTROL";
	case WM_IME_COMPOSITIONFULL:    return "IME_COMPOSITIONFULL";
	case WM_IME_SELECT:             return "IME_SELECT";
	case WM_IME_CHAR:               return "IME_CHAR";
	case WM_IME_REQUEST:            return "IME_REQUEST";
	case WM_IME_KEYDOWN:            return "IME_KEYDOWN";
	case WM_IME_KEYUP:              return "IME_KEYUP";
	case WM_MOUSEHOVER:             return "MOUSEHOVER";
	case WM_MOUSELEAVE:             return "MOUSELEAVE";
	case WM_CUT:                    return "CUT";
	case WM_COPY:                   return "COPY";
	case WM_PASTE:                  return "PASTE";
	case WM_CLEAR:                  return "CLEAR";
	case WM_UNDO:                   return "UNDO";
	case WM_RENDERFORMAT:           return "RENDERFORMAT";
	case WM_RENDERALLFORMATS:       return "RENDERALLFORMATS";
	case WM_DESTROYCLIPBOARD:       return "DESTROYCLIPBOARD";
	case WM_DRAWCLIPBOARD:          return "DRAWCLIPBOARD";
	case WM_PAINTCLIPBOARD:         return "PAINTCLIPBOARD";
	case WM_VSCROLLCLIPBOARD:       return "VSCROLLCLIPBOARD";
	case WM_SIZECLIPBOARD:          return "SIZECLIPBOARD";
	case WM_ASKCBFORMATNAME:        return "ASKCBFORMATNAME";
	case WM_CHANGECBCHAIN:          return "CHANGECBCHAIN";
	case WM_HSCROLLCLIPBOARD:       return "HSCROLLCLIPBOARD";
	case WM_QUERYNEWPALETTE:        return "QUERYNEWPALETTE";
	case WM_PALETTEISCHANGING:      return "PALETTEISCHANGING";
	case WM_PALETTECHANGED:         return "PALETTECHANGED";
	case WM_HOTKEY:                 return "HOTKEY";
	case WM_PRINT:                  return "PRINT";
	case WM_PRINTCLIENT:            return "PRINTCLIENT";
	case WM_HANDHELDFIRST:          return "HANDHELDFIRST";
	case WM_HANDHELDLAST:           return "HANDHELDLAST";
	case WM_AFXFIRST:               return "AFXFIRST";
	case WM_AFXLAST:                return "AFXLAST";
	case WM_PENWINFIRST:            return "PENWINFIRST";
	case WM_PENWINLAST:             return "PENWINLAST";
	case WM_APP:                    return "APP";
	case WM_USER:                   return "USER";
	default:                        return "?";
	}
}

char *OSO_LookupWinMacro_CDN(
	UINT nCode
)
{
	switch (nCode)
	{
	case CDN_INITDONE:       return "INITDONE";
	case CDN_SELCHANGE:      return "SELCHANGE";
	case CDN_FOLDERCHANGE:   return "FOLDERCHANGE";
	case CDN_SHAREVIOLATION: return "SHAREVIOLATION";
	case CDN_HELP:           return "HELP";
	case CDN_FILEOK:         return "FILEOK";
	case CDN_TYPECHANGE:     return "TYPECHANGE";
	case CDN_INCLUDEITEM:    return "INCLUDEITEM";
	default:                 return "?";
	}
}
