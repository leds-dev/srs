// SsgsUtilGui.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "SsgsUtilGui.h"
#include "SsgsUtilGuiDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSsgsUtilGuiApp

BEGIN_MESSAGE_MAP(CSsgsUtilGuiApp, CWinApp)
	//{{AFX_MSG_MAP(CSsgsUtilGuiApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSsgsUtilGuiApp construction

CSsgsUtilGuiApp::CSsgsUtilGuiApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CSsgsUtilGuiApp object

CSsgsUtilGuiApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CSsgsUtilGuiApp initialization

BOOL CSsgsUtilGuiApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#elif defined(VS_2005_SAYS_NO_LONGER_NEEDED)
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#else
#endif

	CSsgsUtilGuiDlg dlg;
	m_pMainWnd = &dlg;

	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
		exit(1);
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
		exit(1);
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
