// SsgsUtilGui.h : main header file for the SSGSUTILGUI application
//

#if !defined(AFX_SSGSUTILGUI_H__75B5D3C4_58BE_11D7_8547_004036106B71__INCLUDED_)
#define AFX_SSGSUTILGUI_H__75B5D3C4_58BE_11D7_8547_004036106B71__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSsgsUtilGuiApp:
// See SsgsUtilGui.cpp for the implementation of this class
//

class CSsgsUtilGuiApp : public CWinApp
{
public:
	CSsgsUtilGuiApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSsgsUtilGuiApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSsgsUtilGuiApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SSGSUTILGUI_H__75B5D3C4_58BE_11D7_8547_004036106B71__INCLUDED_)
