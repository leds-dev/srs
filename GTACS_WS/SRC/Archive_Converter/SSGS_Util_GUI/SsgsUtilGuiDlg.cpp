// SsgsUtilGuiDlg.cpp : implementation file

/* SSGS Convert-ARC-to-DM4 GUI
 *
 * This GUI runs on Windows but the actual conversion program runs on Solaris;
 * we accomplish this by creating a text file on a shared drive and assuming
 * that a cron job on the other end will find it within <n> seconds, read it,
 * process the .ARC files listed within it, and create a logfile for us to read.
 * Of course, _we_ are writing to our _own_ logfile at the same time, copying
 * the contents of the other and adding our own remarks as necessary.
 *
 * Modified:  M. Dalal    Dec 2009      NOAA
 * Changed to work with new gtacs structure (epochsw/bin)
 * Modified:  M. Dalal    Jun 2011      OSPO, NOAA
 * SSGS-1149: Moved the location of Convert GUI to D:\SSGS\SSGSUtil instead of D:\SSGSUtil.
 *
 * Modified:  F. Shaw   Aug 2013     KTTS
 * SRS contract - moved location of Convert GUI to C:\SSGS\SSGSUtil instead of D:\SSGS\SSGSUtil
 *
 */

/* XP
 */
// #define WINVER        0x0501
// #define _WIN32_WINNT  0x0501

#include "stdafx.h"
#include "afxtempl.h"
#include "dlgs.h"           /* need the macros in here for resizing the contents of the "open file" dialog box */
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <process.h>
#include <direct.h>
#include <io.h>
#include <errno.h>
#include <time.h>

#include "LookupWinMacros.h"
#include "SsgsUtilGui.h"
#include "SsgsUtilGuiDlg.h"
#include "FolderChooser.h"

#if defined(_MSC_VER) && _MSC_VER >= 1400
#  define USING_VS2005_OR_LATER  1
#else
#  define USING_VS2005_OR_LATER  0
#endif

#define TODO_SYNTAX_VERSION     "1.1"

#define DPM_TRACK_THE_CODE       0
#define DPM_DEBUG_OPENFILE_HOOK  0

/* These macros are self-explanatory.
 */
#define ONE_SECOND         1
#define ONE_MINUTE       (60 * ONE_SECOND)
#define ONE_HOUR         (60 * ONE_MINUTE)
#define ONE_DAY          (24 * ONE_HOUR)
#define ONE_WEEK          (7 * ONE_DAY)

/* These macros are flags for moving and/or stretching various items within the "Open File" dialog box.
 */
#define MOVE_DOWN         0x0001
#define MOVE_RIGHT        0x0002
#define STRETCH_DOWN      0x0010
#define STRETCH_RIGHT     0x0020
#define MOVE_BOTH         (MOVE_DOWN | MOVE_RIGHT)
#define STRETCH_BOTH      (STRETCH_DOWN | STRETCH_RIGHT)

/* Visual Studio 2005 hates you if you call fopen or sprintf or getenv
 */
#if USING_VS2005_OR_LATER
#  pragma warning(disable : 4996)
#else
#  define strcat_s(a1, a2, a3)   strcat(a1, a3)
#  define strcpy_s(a1, a2, a3)   strcpy(a1, a3)
#endif

#if DPM_TRACK_THE_CODE
static FILE *gpfTracking = NULL;
#  define PRINT_1_TRACK(a1, a2)  if (gpfTracking != NULL) fprintf(gpfTracking, a1, a2)
#else
#  define PRINT_1_TRACK(a1, a2)
#endif


/* Convention #1:  variables whose names end in "Path" always have a trailing backslash.
 * Convention #2:  variables whose names end in "Dir"   never have a trailing backslash.
 */

/* These are simple literal strings, not tied to either platform.
 */
static const CString gcDirExt              = ".d";
static const CString gcLogExt              = ".log";
static const CString gcTodoExt             = ".todo";
static const CString gcLogEntryDelim       = "**************************************************";

/* These are facts about the server which we simply hard-code . . .
 * if this project was bigger we would stuff these values into a config file
 * but it's not so we don't.
 */
static const CString gcMagicNameServer     = "gtacs0";
static const CString gcMagicNameSpacecraft = "goes";
static const CString gcMagicNameStream     = "rt";
static const CString gcRemoteUsername      = "gtacsops";
static const CString gcRemoteSubdirectory  = "epoch";
static const CString gcRemoteBinDir        = "epochsw\\bin";
static const CString gcRemoteOutDir        = gcRemoteSubdirectory + "\\" + "output";

/* These are facts about the Windows box running this program.
 */
static const CString gcLocalHomePath       = "c:\\SSGS\\"; // "SRS contract - moved location of Convert GUI to C:\SSGS\"
static const CString gcLocalUtilPath       = gcLocalHomePath + "SSGSUtil\\";
static const CString gcLocalLogPath        = gcLocalUtilPath + "log\\";
static       CString gcLocalTempPath;       /* this will be the expansion of %TEMP%\ */
static       CString gcDestPath;            /* this might be on a local disk or on the fileserver */
static       CString gcRemoteWorkDir;       // "\\sgtacs04\gtacsops\epoch\output\ARC_sgtacs04_2008090-150512_00001564.d"

/* The first three of these four are chosen by the user via the GUI,
 * while the prefix is constructed from the first and last characters of the server name.
 */
static       CString gcChosenServer;        // "sgtacs04"
static       CString gcChosenSpacecraft;    // "goes13"
static       CString gcChosenStream;        // "rt6"
static       CString gcStreamPrefix;        // "s4"

/* These are constructed at runtime, after the user has chosen server & spacecraft & stream
 */
static       CString gcRemoteShare;         // "\\sgtacs04\gtacsops\"
static       CString gcRemoteBinPath;       // "\\sgtacs04\gtacsops\epochsw\bin\"
static       CString gcRemoteOutPath;       // "\\sgtacs04\gtacsops\epoch\output\"
static       CString gcRemoteArcDir;        //                                  "sgtacs04\goes13\s4rt6"
static       CString gcRemoteArcPath;       // "\\sgtacs04\gtacsops\epoch\output\sgtacs04\goes13\s4rt6\"
static       CString gcRemoteLogFilename;

/* This is constructed each time we want to write to it
 */
static       CString gcLocalLogFile;        // "D:\SSGSUtil\log\2008123.log"

#if DPM_DEBUG_OPENFILE_HOOK
static FILE *gpfOK = NULL;
#endif
static FILE *gpfDump = NULL;

#define CONFIG_FILENAME                "CONVERT_REPOSITORY.BAT"
#define CONFIG_VARNAME                 "NET_REPOSITORY"
#define nLogScrubDelay                  ONE_WEEK


/* Well, yes, there _is_ a standard . . . but why should Microsoft follow it?
 */
#define sleep(n)     Sleep(n * 1000)


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define SYSTEM_COMMAND_MASK  0x0000FFF0


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


class OSOConvertingDlg : public CDialog
{
public:
	OSOConvertingDlg();
	void OSO_M_ShowCounts(void);
	void OSO_M_TimerWentOff(UINT_PTR nIDEvent, DWORD dwTime);

// Dialog Data
	//{{AFX_DATA(OSOConvertingDlg)
	enum { IDD = IDD_CONVERTING };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(OSOConvertingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual BOOL OnInitDialog();
	void OSO_M_StopTimer(void);
	void OSO_M_OnAbort(void);

	//{{AFX_MSG(OSOConvertingDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	int m_nArcsFound;
	int m_nErrsFound;
	int m_nDm4sFound;
	int m_nDm4sMoved;

public:
	bool m_bIgnoreTimer;
	bool m_bWaitingForCron;
	long m_nStart[2];
	long m_nFiles[2];
	long m_nSizes[2];
	long m_nTimes[2];
	POSITION m_pos;
	UINT_PTR m_pvOneSecond;
	DWORD m_nBeginTime;
	CSsgsUtilGuiDlg *m_wMain;
	long m_nCurrArcSize;
	CString m_cCurrArcName;
	CString m_cCurrLogfile;
};


OSOConvertingDlg::OSOConvertingDlg() : CDialog(OSOConvertingDlg::IDD)
{
	//{{AFX_DATA_INIT(OSOConvertingDlg)
	//}}AFX_DATA_INIT
}

void OSOConvertingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(OSOConvertingDlg)
	//}}AFX_DATA_MAP
}

void OSOConvertingDlg::OSO_M_OnAbort(void)
{
	m_bIgnoreTimer = true;
	if (AfxMessageBox("Are you sure you want to abort?", MB_YESNO) == IDYES)
	{
		EndDialog(IDCANCEL);
	}
	else
	{
		m_bIgnoreTimer = false;
	}
}

BEGIN_MESSAGE_MAP(OSOConvertingDlg, CDialog)
	//{{AFX_MSG_MAP(OSOConvertingDlg)
	ON_BN_CLICKED(IDCANCEL,           OSO_M_OnAbort)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



class OSOOverwritingDlg : public CDialog
{
public:
	OSOOverwritingDlg();

// Dialog Data
	//{{AFX_DATA(OSOOverwritingDlg)
	enum { IDD = IDD_OVERWRITING };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(OSOOverwritingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual BOOL OnInitDialog();
	afx_msg void OSO_M_OnButtonOverwriteYes();
	afx_msg void OSO_M_OnButtonOverwriteNo();
	afx_msg void OSO_M_OnButtonOverwriteAll();
	afx_msg void OSO_M_OnButtonOverwriteNone();

	//{{AFX_MSG(OSOOverwritingDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

public:
	CString cBasename;
	CString cFullWorkName;
	CString cFullDestName;
};


OSOOverwritingDlg::OSOOverwritingDlg() : CDialog(OSOOverwritingDlg::IDD)
{
	//{{AFX_DATA_INIT(OSOOverwritingDlg)
	//}}AFX_DATA_INIT
}

void OSOOverwritingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(OSOOverwritingDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(OSOOverwritingDlg, CDialog)
	//{{AFX_MSG_MAP(OSOOverwritingDlg)
	ON_BN_CLICKED(IDYES,                      OSO_M_OnButtonOverwriteYes)
	ON_BN_CLICKED(IDNO,                       OSO_M_OnButtonOverwriteNo)
	ON_BN_CLICKED(IDC_BUTTON_OVERWRITE_ALL,   OSO_M_OnButtonOverwriteAll)
	ON_BN_CLICKED(IDC_BUTTON_OVERWRITE_NONE,  OSO_M_OnButtonOverwriteNone)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// CFileViewDlg dialog used for "Show Remote Logfile"

class OSOFileViewDlg : public CDialog
{
public:
	OSOFileViewDlg(CWnd* pParent = NULL);

	CString m_cFilename;
	CString m_cDescription;

// Dialog Data
	//{{AFX_DATA(CFileViewDlg)
	enum { IDD = IDD_FILEVIEW };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(OSOFileViewDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual BOOL OnInitDialog();

	//{{AFX_MSG(OSOFileViewDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CFont m_sCourierNewFont;

};


OSOFileViewDlg::OSOFileViewDlg(CWnd* pParent /*=NULL*/) : CDialog(OSOFileViewDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(OSOFileViewDlg)
	//}}AFX_DATA_INIT
}

void OSOFileViewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileViewDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(OSOFileViewDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSsgsUtilGuiDlg dialog

CSsgsUtilGuiDlg::CSsgsUtilGuiDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSsgsUtilGuiDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSsgsUtilGuiDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDI_NOAA);
	m_bPathWasDefined = false;
}

void CSsgsUtilGuiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSsgsUtilGuiDlg)
	DDX_Control(pDX, IDC_LIST_ARCFILES, m_ArcList);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSsgsUtilGuiDlg, CDialog)
	//{{AFX_MSG_MAP(CSsgsUtilGuiDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()

	ON_BN_CLICKED(IDC_RADIO_SGT1,     OSO_M_OnRadioSgt1)
	ON_BN_CLICKED(IDC_RADIO_SGT2,     OSO_M_OnRadioSgt2)
	ON_BN_CLICKED(IDC_RADIO_SGT3,     OSO_M_OnRadioSgt3)
	ON_BN_CLICKED(IDC_RADIO_SGT4,     OSO_M_OnRadioSgt4)
	ON_BN_CLICKED(IDC_RADIO_SGT5,     OSO_M_OnRadioSgt5)

	ON_BN_CLICKED(IDC_RADIO_CGT1,     OSO_M_OnRadioCgt1)
	ON_BN_CLICKED(IDC_RADIO_CGT2,     OSO_M_OnRadioCgt2)
	ON_BN_CLICKED(IDC_RADIO_CGT3,     OSO_M_OnRadioCgt3)
//	ON_BN_CLICKED(IDC_RADIO_CGT4,     OSO_M_OnRadioCgt4)
//	ON_BN_CLICKED(IDC_RADIO_CGT5,     OSO_M_OnRadioCgt5)

	ON_BN_CLICKED(IDC_RADIO_SC13,     OSO_M_OnRadioSc13)
	ON_BN_CLICKED(IDC_RADIO_SC14,     OSO_M_OnRadioSc14)
	ON_BN_CLICKED(IDC_RADIO_SC15,     OSO_M_OnRadioSc15)
	ON_BN_CLICKED(IDC_RADIO_SC16,     OSO_M_OnRadioSc16)

	ON_BN_CLICKED(IDC_RADIO_STR1,     OSO_M_OnRadioStr1)
	ON_BN_CLICKED(IDC_RADIO_STR2,     OSO_M_OnRadioStr2)
	ON_BN_CLICKED(IDC_RADIO_STR3,     OSO_M_OnRadioStr3)
	ON_BN_CLICKED(IDC_RADIO_STR4,     OSO_M_OnRadioStr4)
	ON_BN_CLICKED(IDC_RADIO_STR5,     OSO_M_OnRadioStr5)
	ON_BN_CLICKED(IDC_RADIO_STR6,     OSO_M_OnRadioStr6)
	ON_BN_CLICKED(IDC_RADIO_STR7,     OSO_M_OnRadioStr7)
	ON_BN_CLICKED(IDC_RADIO_STR8,     OSO_M_OnRadioStr8)

	ON_BN_CLICKED(IDC_BUTTON_SELECT,  OSO_M_OnButtonSelect)
	ON_BN_CLICKED(IDC_BUTTON_BROWSE,  OSO_M_OnButtonBrowse)
	ON_BN_CLICKED(IDC_BUTTON_CONVERT, OSO_M_OnButtonConvert)
	ON_BN_CLICKED(IDC_BUTTON_SHOWRES, OSO_M_OnButtonShowResults)
	ON_BN_CLICKED(IDC_BUTTON_SHOWLOG, OSO_M_OnButtonShowLog)
	ON_BN_CLICKED(IDC_BUTTON_EXIT,    OSO_M_OnExit)
	ON_BN_CLICKED(IDOK,               OSO_M_OnOk)

	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


#ifndef SL_SEVERITY_WARNING
#define SL_SEVERITY_WARNING     0
#define SL_SEVERITY_SUCCESS     1
#define SL_SEVERITY_ERROR       2
#define SL_SEVERITY_INFO        3
#define SL_SEVERITY_FATAL       4
#endif


static void OSO_I_ShowMessage(
	int nSeverity,
	const char *pcUserMessage,
	bool bAppendErrnoString
)
{
	UINT nFlags = MB_OK | MB_APPLMODAL;
	char cMessage[2048];

	if (pcUserMessage != NULL)
	{
		if (bAppendErrnoString && errno > 0)
		{
			strcpy_s(cMessage, sizeof(cMessage), pcUserMessage);
			strcat_s(cMessage, sizeof(cMessage), " --- ");
			strcat_s(cMessage, sizeof(cMessage), strerror(errno));
			pcUserMessage = cMessage;
		}
		if (nSeverity == SL_SEVERITY_SUCCESS)  nFlags |= 0;             /* sorry but there is no MB_ICONHAPPYFACE */
		if (nSeverity == SL_SEVERITY_INFO)     nFlags |= MB_ICONINFORMATION;    /* same value as MB_ICONASTERISK */
		if (nSeverity == SL_SEVERITY_WARNING)  nFlags |= MB_ICONQUESTION;       /* no alias */
		if (nSeverity == SL_SEVERITY_ERROR)    nFlags |= MB_ICONEXCLAMATION;    /* same value as MB_ICONWARNING */
		if (nSeverity == SL_SEVERITY_FATAL)    nFlags |= MB_ICONERROR;          /* same value as MB_ICONHAND and MB_ICONSTOP */
		AfxMessageBox(pcUserMessage, nFlags);
	}
}


static void OSO_I_EnsureTrailingBackslash(
	CString *pcPath
)
{
	if (pcPath->Right(1) != '\\')
	{
		pcPath->AppendChar('\\');
	}
}


static bool OSO_I_FileExists(
	const char *pcFilespec
)
{
	struct _stat sDummy;

	return (_stat(pcFilespec, &sDummy) == 0);
}


static bool OSO_I_DoesWildcardExist(
	CString pcWildcard
)
{
	long nTempFile;
	struct _finddata_t sDummy;

	nTempFile = _findfirst(pcWildcard, &sDummy);
	_findclose(nTempFile);

	return (nTempFile != -1L);
}


static long OSO_I_HowBigIsThisFile(
	const char *pcFilespec,
	bool bKb
)
{
	struct _stat sFileInfo;

	if (_stat(pcFilespec, &sFileInfo) == 0)
	{
		if (bKb) sFileInfo.st_size /= 1024;
		return sFileInfo.st_size;
	}

	return -1;
}


static long OSO_I_HowOldIsThisFile(
	CString pcFilespec
)
{
	struct _stat sFileInfo;

	if (_stat(pcFilespec, &sFileInfo) == 0)
	{
		return (long) (time(0) - sFileInfo.st_mtime);
	}

	return -1;
}


static bool OSO_I_DeleteOneFile(
	CString cFilename,
	bool bSignalAccessProblem
)
{
	if (remove(cFilename) == 0)
	{
		return true;
	}
	if (bSignalAccessProblem || errno != EACCES)
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, "Unable to delete " + cFilename, true);
	}

	return false;
}


static bool OSO_I_DirectoryIsWriteable(
	CString cTestPath,
	bool bSignal
)
{
	char *pcReason;
	FILE *pfTest = NULL;
	CString cTestName;

	cTestName = cTestPath + "JUST_TESTING.TMP";
	if (! OSO_I_FileExists(cTestPath.Left(cTestPath.GetLength() - 1)))
	{
		pcReason = " does not exist";
	}
	else if ((pfTest = fopen(cTestName, "w")) == NULL)
	{
		pcReason = " does not seem to be writeable";
	}
	else
	{
		fclose(pfTest);
		OSO_I_DeleteOneFile(cTestName, true);
		return true;
	}

	if (bSignal)
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, "Please choose a different folder, because " + cTestPath + pcReason, true);
	}

	return false;
}


static void OSO_I_PurgeLocalLogFiles(
	void
)
{
	intptr_t hFile;
	time_t nNow;
	CString logSpec;
	struct _finddata_t sFileInfo;

	nNow    = time(0);
	logSpec = gcLocalLogPath + "*" + gcLogExt;
	hFile   = _findfirst(logSpec, &sFileInfo);
	while (hFile != -1)
	{
		if (sFileInfo.time_create < nNow - nLogScrubDelay)
		{
			OSO_I_DeleteOneFile(gcLocalLogPath + sFileInfo.name, false);
		}
		if (_findnext(hFile, &sFileInfo) < 0)
		{
			break;
		}
	}
	if (errno != ENOENT)
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, "Unable to find local logfile " + logSpec, true);
	}
	_findclose(hFile);
}


static CString OSO_I_GetPwd(
	void
)
{
	int nDrive;
	TCHAR szPath[_MAX_PATH];
	CString cCurrentPath;

	nDrive = _getdrive();
	_getdcwd(nDrive, szPath, _MAX_PATH);
	if (islower(szPath[0]))
	{
		szPath[0] = _toupper(szPath[0]);
	}
	cCurrentPath = szPath;

	return cCurrentPath;
}


static void OSO_I_ShowPwd(
	char *pcWhat
)
{
	if (gpfDump != NULL) fprintf(gpfDump, "pwd=%s %s\n", OSO_I_GetPwd(), pcWhat);
}


static bool OSO_I_SaveDestPath(
	void
)
{
	FILE *pfConfig;
	bool bSaved = false;

	if ((pfConfig = fopen(CONFIG_FILENAME, "w")) == NULL)
	{
		OSO_I_ShowMessage(SL_SEVERITY_WARNING, "Unable to create " CONFIG_FILENAME, true);
	}
	else
	{
		/* The line must look like
		 *
		 *       set NET_REPOSITORY=\\sgoesndevfs01\ABE\CnvrtRepository
		 *
		 * so remove the \DMF\goes99\ that was probably appended
		 * and of course the trailing backslash.
		 */
		int nOffset;
		CString cPathToWrite = gcDestPath;
		if ((nOffset = cPathToWrite.Find("DMF\\goes")) > 0)
		{
			cPathToWrite = cPathToWrite.Left(nOffset);
		}
		if (cPathToWrite.Right(1) == "\\")
		{
			cPathToWrite = cPathToWrite.Left(cPathToWrite.GetLength() - 1);
		}
		fprintf(pfConfig, "set %s=%s\n", CONFIG_VARNAME, cPathToWrite.GetString());
		fclose(pfConfig);
		bSaved = true;
	}

	return bSaved;
}


static bool OSO_I_LoadDestPath(
	void
)
{
	char *pcValue;
	FILE *pfConfig;
	bool bFileProblem = false;
	bool bPathWasDefined = false;

	/* Set the initial value for the destination folder,
	 * either by translating the environmental variable
	 * or reading it from a config file or just using the
	 * current default directory.
	 */
	gcDestPath = "";

	if ((pcValue = getenv(CONFIG_VARNAME)) != NULL)
	{
		gcDestPath = pcValue;
		bPathWasDefined = true;
		if (gpfDump != NULL) fprintf(gpfDump, "destpath=%s due to getenv(%s)\n", gcDestPath, CONFIG_VARNAME);
	}
	else if ((pfConfig = fopen(CONFIG_FILENAME, "r")) == NULL)
	{
		bFileProblem = true;
	}
	else
	{
		/* We are only interested in one line, which should look like
		 *
		 *       set NET_REPOSITORY=\\sgoesndevfs01\ABE\CnvrtRepository
		 *
		 * after removing the trailing CR and/or LF.
		 */
		char cReadBuffer[4096];
		while (fgets(cReadBuffer, sizeof(cReadBuffer), pfConfig) != NULL)
		{
			char *pcEquals;
			if (gpfDump != NULL) fprintf(gpfDump, "read line %s!\n", cReadBuffer);
			if ((pcEquals = strchr(cReadBuffer, '\n')) != NULL)
			{
				*pcEquals = '\0';
			}
			if ((pcEquals = strchr(cReadBuffer, '\r')) != NULL)
			{
				*pcEquals = '\0';
			}
			if ((pcEquals = strchr(cReadBuffer, '=')) != NULL)
			{
				*pcEquals = '\0';
				if (strcmp(cReadBuffer, "set " CONFIG_VARNAME) == 0)
				{
					gcDestPath = pcEquals + 1;
					gcDestPath.Trim();
					if (gpfDump != NULL) fprintf(gpfDump, "destpath=%s due to file %s in %s\n", gcDestPath, CONFIG_FILENAME, OSO_I_GetPwd());
					bPathWasDefined = true;
					break;
				}
			}
		}
		bFileProblem = ! feof(pfConfig);
		fclose(pfConfig);
		pfConfig = NULL;
	}

	if (gcDestPath == "")
	{
		CString cMessage;
		if (bFileProblem)
		{
			cMessage  = "Unable to open and read ";
			cMessage += CONFIG_FILENAME;
			cMessage += " to find a value for the ";
			cMessage += CONFIG_VARNAME;
			cMessage += " variable";
		}
		else
		{
			cMessage  = "The variable ";
			cMessage += CONFIG_VARNAME;
			cMessage += " is not defined in the file ";
			cMessage += CONFIG_FILENAME;
		}
		cMessage += " (will use the current folder)";
		OSO_I_ShowMessage(SL_SEVERITY_WARNING, (char *) cMessage.GetString(), bFileProblem);
		gcDestPath = OSO_I_GetPwd();
		if (gpfDump != NULL) fprintf(gpfDump, "destpath=%s due to pwd\n", gcDestPath);
	}

	OSO_I_EnsureTrailingBackslash(&gcDestPath);

	return bPathWasDefined;
}


void CSsgsUtilGuiDlg::OSO_M_BuildPathTo(
	bool bAppendDmfGoes99
)
{
	/* We cannot perform the actual test before on-init-dialog has completed,
	 * because the test might hang for many seconds, and that would leave
	 * the user staring at a blank screen, with absolutely nothing visual
	 * to indicate that the application has even started.
	 *
	 * We also need a spacecraft name for appending, which is considered
	 * to be the normal manner of operating, so we test for that as well.
	 */
	if (gcChosenSpacecraft == "")
	{
		gcDestPath = "";
	}
	else
	{
		/* Now that the dialog box is visible and the user chose a GOES##,
		 * we can load the destination path and test its validity.  Override
		 * the "append" flag if the path was specified in the config file,
		 * because that's a system requirement.
		 */
		CString gcSavePath;
		if (gcDestPath == "")
		{
			m_bPathWasDefined = OSO_I_LoadDestPath();
			bAppendDmfGoes99  = m_bPathWasDefined;
		}

		/* We probably want to silently append the subdirectories
		 *               DMF\goes99\
		 * to the path, if they exist.  We should also replace such a token if it
		 * is already ending the path (i.e., change "DMF\goes14\" to "DMF\goes16\".
		 * The simplest way to do that is remove the old, then tack on the new.
		 */
		if (bAppendDmfGoes99)
		{
			gcSavePath = gcDestPath;
			if (gcDestPath.GetLength() > 11)
			{
				if (gcDestPath.Right(11).Left(8).MakeUpper() == "DMF\\GOES")
				{
					gcDestPath = gcDestPath.Left(gcDestPath.GetLength() - 11);
				}
			}
			gcDestPath += "DMF\\" + gcChosenSpacecraft + "\\";
		}
		else
		{
			gcSavePath = OSO_I_GetPwd();
		}
		if (! OSO_I_DirectoryIsWriteable(gcDestPath, m_bPathWasDefined))
		{
			/* An error message has already been displayed.
			 */
			gcDestPath = gcSavePath;
		}
	}

	GetDlgItem(IDC_TEXT_PATHTO)->SetWindowText(gcDestPath);
}


#if defined(AN_EXAMPLE_OF_ALL_THE_VARIABLES)

[user's choices]

     gcChosenServer     sgtacs05
     gcChosenSpacecraft goes14
     gcChosenStream     rt3
     m_ArcList          b2008042145320.arc,b2008042151350.arc

[generated by GUI callbacks]

     gcStreamPrefix     s5

[constructed internally]

     uniquename         ARC_sgtacs04_2008091-124003_00003452
     logfilename        ARC_sgtacs04_2008091-124003_00003452.log
     todofilename       ARC_sgtacs04_2008091-124003_00003452.todo
     workdir            ARC_sgtacs04_2008091-124003_00003452.d

	 logFile            p2008030150003.arc.log
     localLogPath       d:\SSGSUtil\log\
     localLogFile       d:\SSGSUtil\log\YYYYDDD.log

	 gcRemoteShare      \\sgtacs05\gtacsops\
     gcRemoteBinPath    \\sgtacs05\gtacsops\epoch\bin\
     gcRemoteOutPath    \\sgtacs05\gtacsops\epoch\output\
     gcRemoteArcDir                                      sgtacs05\goes14\s5rt3
     gcRemoteArcPath    \\sgtacs05\gtacsops\epoch\output\sgtacs05\goes14\s5rt3\
     remoteLog          \\sgtacs05\$GTACS_ROOT/export/home/gtacsops/p2008030150003.arc.log

#endif


void CSsgsUtilGuiDlg::OSO_M_TestOneSpacecraft(
	int nWhichButton,
	int nWhichSpacecraft
)
{
	bool bExists;
	char cTestPath[256];

	sprintf(cTestPath, "%s%s\\goes%d",
		gcRemoteOutPath,
		gcChosenServer,
		nWhichSpacecraft);
	bExists = OSO_I_FileExists(cTestPath);
	GetDlgItem(nWhichButton)->EnableWindow(bExists);
	if (! bExists)
	{
		CheckDlgButton(nWhichButton, BST_UNCHECKED);
	}
}


void CSsgsUtilGuiDlg::OSO_M_TestAllSpacecraft(
	void
)
{
	OSO_M_TestOneSpacecraft(IDC_RADIO_SC13, 13);
	OSO_M_TestOneSpacecraft(IDC_RADIO_SC14, 14);
	OSO_M_TestOneSpacecraft(IDC_RADIO_SC15, 15);
	OSO_M_TestOneSpacecraft(IDC_RADIO_SC16, 16);
}


void CSsgsUtilGuiDlg::OSO_M_TestOneStream(
	int nWhichButton,
	int nWhichStream
)
{
	bool bExists;
	char cTestDir[256];

	sprintf(cTestDir, "%s%s\\%s\\%srt%d",
		gcRemoteOutPath,
		gcChosenServer,
		gcChosenSpacecraft,
		gcStreamPrefix,
		nWhichStream);
	bExists = OSO_I_FileExists(cTestDir);
	if (bExists)
	{
		strcat(cTestDir, "\\*.arc");
		bExists = OSO_I_DoesWildcardExist(cTestDir);
	}
	GetDlgItem(nWhichButton)->EnableWindow(bExists);
	if (! bExists)
	{
		CheckDlgButton(nWhichButton, BST_UNCHECKED);
	}
}


void CSsgsUtilGuiDlg::OSO_M_TestAllStreams(
	void
)
{
	OSO_M_TestOneStream(IDC_RADIO_STR1, 1);
	OSO_M_TestOneStream(IDC_RADIO_STR2, 2);
	OSO_M_TestOneStream(IDC_RADIO_STR3, 3);
	OSO_M_TestOneStream(IDC_RADIO_STR4, 4);
	OSO_M_TestOneStream(IDC_RADIO_STR5, 5);
	OSO_M_TestOneStream(IDC_RADIO_STR6, 6);
	OSO_M_TestOneStream(IDC_RADIO_STR7, 7);
	OSO_M_TestOneStream(IDC_RADIO_STR8, 8);
}


bool CSsgsUtilGuiDlg::OSO_M_BuildPathFrom(
	void
)
{
	/* Reset the variables representing paths on the server
	 * depending upon which radio buttons the user clicked.
	 */
	gcRemoteShare   = "";
	gcRemoteBinPath = "";
	gcRemoteOutPath = "";
	gcRemoteArcDir  = "";
	gcRemoteArcPath = "";
	if (gcChosenServer != "")
	{
		gcRemoteShare   = "\\\\" + gcChosenServer + "\\" + gcRemoteUsername + "\\";
		gcRemoteBinPath = gcRemoteShare + gcRemoteBinDir + "\\";
		gcRemoteOutPath = gcRemoteShare + gcRemoteOutDir + "\\";
		if (gcChosenSpacecraft != "" && gcChosenStream != "" && gcStreamPrefix != "")
		{
			gcRemoteArcDir  = gcChosenServer  + "\\" + gcChosenSpacecraft + "\\" + gcStreamPrefix + gcChosenStream;
			gcRemoteArcPath = gcRemoteOutPath + gcRemoteArcDir;
			if (OSO_I_FileExists(gcRemoteArcPath.GetString()))
			{
				// all good
				gcRemoteArcPath += "\\";
			}
			else
			{
				OSO_I_ShowMessage(SL_SEVERITY_WARNING, gcRemoteArcPath.GetString(), true);
				return false;
			}
		}
	}

	GetDlgItem(IDC_TEXT_PATHFROM)->SetWindowText(gcRemoteArcPath);

	return true;
}


void CSsgsUtilGuiDlg::OSO_M_ClearAllButtons(
	int nLast
)
{
	int nButton = -1;

//	if (nLast == IDC_RADIO_CGT5) nButton = IDC_RADIO_SGT1;
	if (nLast == IDC_RADIO_CGT3) nButton = IDC_RADIO_SGT1;
	if (nLast == IDC_RADIO_SC16) nButton = IDC_RADIO_SC13;
	if (nLast == IDC_RADIO_STR8) nButton = IDC_RADIO_STR1;
	while (nButton <= nLast)
	{
		CheckDlgButton(nButton++, BST_UNCHECKED);
	}
}


void CSsgsUtilGuiDlg::OSO_M_EnableAllButtons(
	int nLast
)
{
	int nButton = -1;

//	if (nLast == IDC_RADIO_CGT5) nButton = IDC_RADIO_SGT1;
	if (nLast == IDC_RADIO_CGT3) nButton = IDC_RADIO_SGT1;	
	if (nLast == IDC_RADIO_SC16) nButton = IDC_RADIO_SC13;
	if (nLast == IDC_RADIO_STR8) nButton = IDC_RADIO_STR1;
	while (nButton <= nLast)
	{
		GetDlgItem(nButton++)->EnableWindow(true);
	}
}


void CSsgsUtilGuiDlg::OSO_M_DisableAllButtons(
	int nLast
)
{
	int nButton = -1;

//	if (nLast == IDC_RADIO_CGT5) nButton = IDC_RADIO_SGT1;
	if (nLast == IDC_RADIO_CGT3) nButton = IDC_RADIO_SGT1;
	if (nLast == IDC_RADIO_SC16) nButton = IDC_RADIO_SC13;
	if (nLast == IDC_RADIO_STR8) nButton = IDC_RADIO_STR1;
	while (nButton <= nLast)
	{
		GetDlgItem(nButton++)->EnableWindow(false);
	}
}


static bool OSO_I_IsSystemCommand(
	int nIDM
)
{
	/* Apparently, certain macros must be defined within the "system command" range
	 */
	if ((nIDM & SYSTEM_COMMAND_MASK) == nIDM)
	{
		if (nIDM < 0x0000F000)
		{
			return true;
		}
	}

	return false;
}

#define USER_CHOSE_NOTHING      0x0000
#define USER_CHOSE_SERVER       0x0001
#define USER_CHOSE_SPACECRAFT   0x0002
#define USER_CHOSE_STREAM       0x0004
#define USER_CHOSE_SELECT       0x0008

void CSsgsUtilGuiDlg::OSO_M_ResetUserInput(
	long nChosenMask
)
{
	if (nChosenMask & USER_CHOSE_SELECT)
	{
		nChosenMask |= USER_CHOSE_SERVER;
		nChosenMask |= USER_CHOSE_SPACECRAFT;
		nChosenMask |= USER_CHOSE_STREAM;
	}

	/* We're starting over . . .
	 * start by setting all of the user-input fields to be empty strings.
	 */
	if ((nChosenMask & USER_CHOSE_SERVER) == 0)
	{
		gcChosenServer     = "";
		gcStreamPrefix     = "";
	}
	if ((nChosenMask & USER_CHOSE_SPACECRAFT) == 0)
	{
		gcChosenSpacecraft = "";
	}
	if ((nChosenMask & USER_CHOSE_STREAM) == 0)
	{
		gcChosenStream     = "";
	}

	/* Uncheck every radio button.
	 */
	if ((nChosenMask & USER_CHOSE_SERVER) == 0)
	{
//		OSO_M_ClearAllButtons(IDC_RADIO_CGT5);
		OSO_M_ClearAllButtons(IDC_RADIO_CGT3);
	}
	if ((nChosenMask & USER_CHOSE_SPACECRAFT) == 0)
	{
		OSO_M_ClearAllButtons(IDC_RADIO_SC16);
	}
	if ((nChosenMask & USER_CHOSE_STREAM) == 0)
	{
		OSO_M_ClearAllButtons(IDC_RADIO_STR8);
	}

	/* The user is not allowed to choose a spacecraft or a stream
	 * until he has chosen a server, so grey them out.
	 */
	if ((nChosenMask & USER_CHOSE_SPACECRAFT) == 0)
	{
		OSO_M_DisableAllButtons(IDC_RADIO_SC16);
		OSO_M_DisableAllButtons(IDC_RADIO_STR8);
	}

	/* Always remove any previously-selected .ARC files from the list,
	 * along with any previously-generated DM4 files from _that_ list.
	 */
	m_ArcList.ResetContent();
	m_Dm4List.RemoveAll();

	/* Always reconstruct the "From" path, because it uses all three strings.
	 */
	OSO_M_BuildPathFrom();

	/* Clear the "To" path or reconstruct it or leave it alone.
	 */
	if ((nChosenMask & USER_CHOSE_STREAM) == 0)
	{
		OSO_M_BuildPathTo((nChosenMask & USER_CHOSE_SPACECRAFT) ? true : false);
	}

	/* The user is allowed to click on [BROWSE] after selecting a spacecraft.
	 * The user is allowed to click on [SELECT] if and only if a server
	 * _and_ a spacecraft _and_ a stream have all been chosen.
	 */
	if ((nChosenMask & USER_CHOSE_SELECT) == 0)
	{
		GetDlgItem(IDC_BUTTON_BROWSE)->EnableWindow((nChosenMask & USER_CHOSE_SPACECRAFT) ? true : false);
		GetDlgItem(IDC_BUTTON_SELECT)->EnableWindow((nChosenMask & USER_CHOSE_STREAM)     ? true : false);
	}

	/* Always disable the rest of the action buttons except EXIT.
	 */
	GetDlgItem(IDC_BUTTON_CONVERT)->EnableWindow(false);    // because no .ARC files have been selected yet
	GetDlgItem(IDC_BUTTON_SHOWRES)->EnableWindow(false);	// because no files have been converted yet
	GetDlgItem(IDC_BUTTON_SHOWLOG)->EnableWindow(false);	// because no files have been converted yet
}


/////////////////////////////////////////////////////////////////////////////
// CSsgsUtilGuiDlg message handlers

BOOL CSsgsUtilGuiDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

#if defined(DUMP_DESTPATH_INFO)
	gpfDump = fopen("D:\\SSGSUTIL_DUMP.TXT", "a");
#endif

	/* Add "About..." menu item to system menu.
	 */
	if (OSO_I_IsSystemCommand(IDM_ABOUTBOX))
	{
		CMenu* pSysMenu = GetSystemMenu(FALSE);
		if (pSysMenu != NULL)
		{
			CString strAboutMenu;
			strAboutMenu.LoadString(IDS_ABOUTBOX);
			if (! strAboutMenu.IsEmpty())
			{
				pSysMenu->AppendMenu(MF_SEPARATOR);
				pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
			}
		}
	}

	/* Set the icon for this dialog.  The framework does this automatically
	 * when the application's main window is not a dialog.
	 */
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	OSO_I_PurgeLocalLogFiles();

	OSO_M_ResetUserInput(USER_CHOSE_NOTHING);

	/* Where will we create the .TODO file before moving it to the GTACS box?
	 */
	gcLocalTempPath = getenv("TEMP");
	OSO_I_EnsureTrailingBackslash(&gcLocalTempPath);

#if DPM_TRACK_THE_CODE
	/* Fail silently if the file can't be created or opened
	 */
	gpfTracking = fopen(gcLocalTempPath + "track_ARC_to_DM4_converter.txt", "a");
	PRINT_1_TRACK("\n\n\nin %s\n", "Init()");
#endif

	UpdateData(false);

	return TRUE;
}


void CSsgsUtilGuiDlg::OnSysCommand(
	UINT nID,
	LPARAM lParam
)
{
	if ((nID & SYSTEM_COMMAND_MASK) == IDM_ABOUTBOX)
	{
		CAboutDlg sDialog;
		sDialog.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}


void CSsgsUtilGuiDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width()  - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}


// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSsgsUtilGuiDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


void CSsgsUtilGuiDlg::OSO_M_OnOk()
{
	// TODO: Add your control notification handler code here
	// D.B.  Subtle nuance of the dialog box.  The OK button captures any CR
	// from the dialog box.  If no OnOk method is specified, the application
	// exits.  So, we define an empty OnOk method to handle it just in case a
	// user enters a CR while modifying the Edit Box
}


void CSsgsUtilGuiDlg::OSO_M_OnExit()
{
#if DPM_TRACK_THE_CODE
	if (gpfTracking != NULL)
	{
		fclose(gpfTracking);
		gpfTracking = NULL;
	}
#endif

	// TODO: Add your control notification handler code here
	exit(1);
}


static char *OSO_I_WhatTimeIsIt(
	bool bIncludeHHMMSS,
	bool bUseColons
)
{
	static char cTimeBuffer[32];

	struct tm *psCurrentZuluTime;
	time_t nCurrentLocalTime = 0;

	/* create a string either "2008-123-14:56:27" or "2008123-145627" or "2008123"
	 */
	time(&nCurrentLocalTime);
	psCurrentZuluTime = gmtime(&nCurrentLocalTime);
	if (bIncludeHHMMSS)
	{
		sprintf(cTimeBuffer,
			(bUseColons) ? "%4d-%03d-%02d:%02d:%02d" : "%4d%03d-%02d%02d%02d",
			psCurrentZuluTime->tm_year + 1900,
			psCurrentZuluTime->tm_yday + 1,
			psCurrentZuluTime->tm_hour,
			psCurrentZuluTime->tm_min,
			psCurrentZuluTime->tm_sec);
	}
	else
	{
		sprintf(cTimeBuffer, "%4d%03d",
			psCurrentZuluTime->tm_year + 1900,
			psCurrentZuluTime->tm_yday + 1);
	}

	return cTimeBuffer;
}


static void OSO_I_BuildLogfileName(
	int nExtraCount
)
{
	char cExtra[16];

	cExtra[0] = '\0';
	if (nExtraCount)
	{
		sprintf(cExtra, "_%2d", nExtraCount);
	}
	gcLocalLogFile = gcLocalLogPath + OSO_I_WhatTimeIsIt(FALSE, FALSE) + cExtra + gcLogExt;
}


static bool OSO_I_TestServer(
	void
)
{
	bool bServerIsOkay = false;
	CString cRemoteToDoScript;

	/* If the user has chosen a host which is offline (to us, at least)
	 * then we're gonna sit here for 20 or 30 seconds until the
	 * access attempt times out, so show the hourglass.
	 */
	CWaitCursor CursorWaiting;

	/* This is the name of the new executable script ---
	 * even though we don't invoke it directly, we should
	 * verify that it's there before going on.
	 */
	cRemoteToDoScript = gcRemoteBinPath + "oso_convert_ARC_files.ksh";

	/* Determine if the desired GTACS machine is accessible or not by assuming
	 * that if we can access its root directory, it is, and if we can't, it's not.
	 */
	if (! OSO_I_DoesWildcardExist(gcRemoteBinPath + "*"))
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR,
			"The server you selected (" + gcChosenServer + ") is not available via username " + gcRemoteUsername,
			false);
	}
	else if (! OSO_I_FileExists(cRemoteToDoScript.GetString()))
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR,
			"no convert script was found on the server (" + cRemoteToDoScript + ")",
			false);
	}
	else
	{
		bServerIsOkay = true;
	}

	return bServerIsOkay;
}


void CSsgsUtilGuiDlg::OSO_M_WhichServer(
	CString pcPrefix,       /* "c" or "s"      */
	CString pcSuffix,       /* "1" through "5" */
	int nButtonToCheck
)
{
	gcChosenServer = pcPrefix + gcMagicNameServer + pcSuffix;
	gcStreamPrefix = pcPrefix +                     pcSuffix;

	OSO_M_ResetUserInput(USER_CHOSE_SERVER);

	/* Is the desired host accessible?
	 */
	if (OSO_I_TestServer())
	{
		/* Apparently, so check that server button and re-enable
		 * and all of the appropriate spacecraft buttons.
		 */
//		CheckRadioButton(IDC_RADIO_SGT1, IDC_RADIO_CGT5, nButtonToCheck);
		CheckRadioButton(IDC_RADIO_SGT1, IDC_RADIO_CGT3, nButtonToCheck);
		OSO_M_TestAllSpacecraft();
	}
	else
	{
		/* Apparently not, so uncheck *all* of the server buttons
		 * and prevent the user from choosing this one again.
		 */
		gcChosenServer = "";
		gcStreamPrefix = "";
//		OSO_M_ClearAllButtons(IDC_RADIO_CGT5);
		OSO_M_ClearAllButtons(IDC_RADIO_CGT3);
		GetDlgItem(nButtonToCheck)->EnableWindow(false);
	}
}


void CSsgsUtilGuiDlg::OSO_M_WhichSpacecraft(
	CString pcSuffix,       /* "13" through "16" */
	int nButtonToCheck
)
{
	/* We don't need to verify the user's choice, because any buttons that
	 * could be invalid were already disabled when the server was chosen.
	 */
	gcChosenSpacecraft = gcMagicNameSpacecraft + pcSuffix;
	CheckRadioButton(IDC_RADIO_SC13, IDC_RADIO_SC16, nButtonToCheck);

	OSO_M_ResetUserInput(USER_CHOSE_SERVER | USER_CHOSE_SPACECRAFT);

	/* Show which streams the user is allowed to choose from
	 * for this server+spacecraft combination.
	 */
	OSO_M_TestAllStreams();
}


void CSsgsUtilGuiDlg::OSO_M_WhichStream(
	CString pcSuffix,       /* "1" through "8" */
	int nButtonToCheck
)
{
	gcChosenStream = gcMagicNameStream + pcSuffix;

	CheckRadioButton(IDC_RADIO_STR1, IDC_RADIO_STR8, nButtonToCheck);

	OSO_M_ResetUserInput(USER_CHOSE_SERVER | USER_CHOSE_SPACECRAFT | USER_CHOSE_STREAM);
}


void CSsgsUtilGuiDlg::OSO_M_OnRadioSgt1() { OSO_M_WhichServer("s", "1", IDC_RADIO_SGT1); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioSgt2() { OSO_M_WhichServer("s", "2", IDC_RADIO_SGT2); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioSgt3() { OSO_M_WhichServer("s", "3", IDC_RADIO_SGT3); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioSgt4() { OSO_M_WhichServer("s", "4", IDC_RADIO_SGT4); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioSgt5() { OSO_M_WhichServer("s", "5", IDC_RADIO_SGT5); }

void CSsgsUtilGuiDlg::OSO_M_OnRadioCgt1() { OSO_M_WhichServer("c", "1", IDC_RADIO_CGT1); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioCgt2() { OSO_M_WhichServer("c", "2", IDC_RADIO_CGT2); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioCgt3() { OSO_M_WhichServer("c", "3", IDC_RADIO_CGT3); }
//void CSsgsUtilGuiDlg::OSO_M_OnRadioCgt4() { OSO_M_WhichServer("c", "4", IDC_RADIO_CGT4); }
//void CSsgsUtilGuiDlg::OSO_M_OnRadioCgt5() { OSO_M_WhichServer("c", "5", IDC_RADIO_CGT5); }

void CSsgsUtilGuiDlg::OSO_M_OnRadioSc13() { OSO_M_WhichSpacecraft("13", IDC_RADIO_SC13); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioSc14() { OSO_M_WhichSpacecraft("14", IDC_RADIO_SC14); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioSc15() { OSO_M_WhichSpacecraft("15", IDC_RADIO_SC15); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioSc16() { OSO_M_WhichSpacecraft("16", IDC_RADIO_SC16); }

void CSsgsUtilGuiDlg::OSO_M_OnRadioStr1() { OSO_M_WhichStream("1", IDC_RADIO_STR1); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioStr2() { OSO_M_WhichStream("2", IDC_RADIO_STR2); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioStr3() { OSO_M_WhichStream("3", IDC_RADIO_STR3); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioStr4() { OSO_M_WhichStream("4", IDC_RADIO_STR4); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioStr5() { OSO_M_WhichStream("5", IDC_RADIO_STR5); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioStr6() { OSO_M_WhichStream("6", IDC_RADIO_STR6); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioStr7() { OSO_M_WhichStream("7", IDC_RADIO_STR7); }
void CSsgsUtilGuiDlg::OSO_M_OnRadioStr8() { OSO_M_WhichStream("8", IDC_RADIO_STR8); }


#include <AclAPI.h>
#include <AccCtrl.h>

static void OSO_I_AllowEveryoneToReadWrite(
	CString cFileToAllow
)
{
#if defined(a_unix_platform)
	chmod(cFileToAllow, 0666);
#else
	DWORD dwRes = 0;
	PACL pOldDACL = NULL;
	PACL pNewDACL = NULL;
	PSECURITY_DESCRIPTOR pSD = NULL;
	char foo[132];
	dwRes = GetNamedSecurityInfo((LPSTR) cFileToAllow.GetString(), SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, NULL, NULL, &pOldDACL, NULL, &pSD);
	if (ERROR_SUCCESS != dwRes)
	{
		sprintf(foo, "GetNamedSecurityInfo(%s) failed = %d", cFileToAllow, dwRes);
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, foo, true);
	}
	else
	{
		EXPLICIT_ACCESS sExplicitAccess;
		ZeroMemory(&sExplicitAccess, sizeof(sExplicitAccess));
		sExplicitAccess.grfAccessPermissions = GENERIC_READ | GENERIC_WRITE;
		sExplicitAccess.grfAccessMode        = GRANT_ACCESS;  
		sExplicitAccess.grfInheritance       = NO_INHERITANCE;  
		sExplicitAccess.Trustee.TrusteeForm  = TRUSTEE_IS_NAME;
		sExplicitAccess.Trustee.ptstrName    = "EVERYONE";
		dwRes = SetEntriesInAcl(1, &sExplicitAccess, pOldDACL, &pNewDACL);
		if (ERROR_SUCCESS != dwRes)
		{
			sprintf(foo, "SetEntriesInAcl(%s) failed = %d", cFileToAllow, dwRes);
			OSO_I_ShowMessage(SL_SEVERITY_ERROR, foo, true);
		}
		else
		{
			dwRes = SetNamedSecurityInfo((LPSTR) cFileToAllow.GetString(), SE_FILE_OBJECT, DACL_SECURITY_INFORMATION, NULL, NULL, pNewDACL, NULL);
			if (ERROR_SUCCESS != dwRes)
			{
				sprintf(foo, "SetNamedSecurityInfo(%s) failed = %d", cFileToAllow, dwRes);
				OSO_I_ShowMessage(SL_SEVERITY_ERROR, foo, true);
			}
		}
	}
#endif
}


static void OSO_I_CopyLog(
	CString cFileToCopy
)
{
	int nExtra = 1;
	FILE *pfLocalLog;
	FILE *pfCopyThis;

	while (1)
	{
		PRINT_1_TRACK("looking for logfile (local)  %s\n", gcLocalLogFile);
		PRINT_1_TRACK("looking for logfile (remote) %s\n", remoteLogFilename);
		if ((pfLocalLog = fopen(gcLocalLogFile, "a")) == NULL)
		{
			if (errno == EACCES)
			{
				OSO_I_BuildLogfileName(nExtra++);
				continue;
			}
			OSO_I_ShowMessage(SL_SEVERITY_ERROR,
				"unable to open local logfile " + gcLocalLogFile + " for appending", true);
		}
		else if ((pfCopyThis = fopen(cFileToCopy, "r")) == NULL)
		{
			OSO_I_ShowMessage(SL_SEVERITY_ERROR, "unable to open " + cFileToCopy + " as input", true);
			fclose(pfLocalLog);
		}
		else
		{
			char cCopyBuffer[4096];
			while (fgets(cCopyBuffer, sizeof(cCopyBuffer), pfCopyThis) != NULL)
			{
				fprintf(pfLocalLog, "%s", cCopyBuffer);
			}
			if (ferror(pfCopyThis))
			{
				OSO_I_ShowMessage(SL_SEVERITY_ERROR, "unable to read remote logfile " + cFileToCopy, true);
			}
			fclose(pfCopyThis);
			fclose(pfLocalLog);
			OSO_I_AllowEveryoneToReadWrite(gcLocalLogFile);
		}
		break;
	}
}


static void OSO_I_AddEntryToLogfile(
	char *pcMessage
)
{
	FILE *pfLog;

	if ((pfLog = fopen(gcLocalLogFile, "a")) != NULL)
	{
		if (pcMessage == NULL)
		{
			fprintf(pfLog, "\n%s\n", gcLogEntryDelim);
		}
		else
		{
			fprintf(pfLog, "\n%s  %s\n", OSO_I_WhatTimeIsIt(TRUE, TRUE), pcMessage);
		}
		fclose(pfLog);
	}
}

static void OSO_I_AdjustWindow(
	CWnd *psWindow,
	RECT *psRect
)
{
	psWindow->SetWindowPos(NULL,
		psRect->left,
		psRect->top,
		psRect->right - psRect->left,
		psRect->bottom - psRect->top,
		SWP_NOACTIVATE | SWP_NOZORDER);
}

struct ADJUST_INFO
{
	int nDeltaWidth;
	int nDeltaHeight;
	RECT sZeroPoint;
};

static void OSO_I_AdjustItem(
	CWnd *pDlg,
	struct ADJUST_INFO *psAdjust,
	int nItemIndex,
	unsigned int nFlags
)
{
	CWnd *pItem;

	if (nFlags != 0 && (pItem = pDlg->GetDlgItem(nItemIndex)) != NULL)
	{
		RECT sWhere;
		pItem->GetWindowRect(&sWhere);
		if (nFlags & MOVE_DOWN)     sWhere.top    += psAdjust->nDeltaHeight;
		if (nFlags & MOVE_DOWN)     sWhere.bottom += psAdjust->nDeltaHeight;
		if (nFlags & STRETCH_DOWN)  sWhere.bottom += psAdjust->nDeltaHeight;
		if (nFlags & MOVE_RIGHT)    sWhere.left   += psAdjust->nDeltaWidth;
		if (nFlags & MOVE_RIGHT)    sWhere.right  += psAdjust->nDeltaWidth;
		if (nFlags & STRETCH_RIGHT) sWhere.right  += psAdjust->nDeltaWidth;
		sWhere.left   -= psAdjust->sZeroPoint.left;
		sWhere.right  -= psAdjust->sZeroPoint.left;
		sWhere.top    -= psAdjust->sZeroPoint.top;
		sWhere.bottom -= psAdjust->sZeroPoint.top;
		OSO_I_AdjustWindow(pItem, &sWhere);
	}
}


#if DPM_DEBUG_OPENFILE_HOOK

static void OSO_I_ShowItemInfo(
	CWnd *pDlg,
	RECT *pOrig
)
{
	int x;
	FILE *pfBoxxy;

	pfBoxxy = fopen("D:\\SSGSUtil\\log\\boxxy.txt", "w");
	for (x = 0; x < 0x0007FFFF; x++)
	{
		CWnd *pItem = pDlg->GetDlgItem(x);
		if (pItem != NULL)
		{
			RECT z;
			char poof[256];
			pItem->GetWindowRect(&z);
			pItem->GetWindowText(poof, sizeof(poof));
			char xx[234];
			sprintf(xx, "item %4d rect=[%3d,%3d,%3d,%3d] width=%3d height=%3d %s!",
				x,
				z.left - pOrig->left, z.right - pOrig->left,
				z.top - pOrig->top, z.bottom - pOrig->top,
				z.right - z.left, z.bottom - z.top, poof);
			AfxMessageBox(xx, MB_ICONINFORMATION);
			if (pfBoxxy != NULL)
			{
				fprintf(pfBoxxy, "%s\n", xx);
			}
		}
	}
	if (pfBoxxy != NULL)
	{
		fclose(pfBoxxy);
	}
}

#endif


UINT APIENTRY OSO_K_OpenFileDialog(
	HWND hWnd,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam
)
{
	CWnd *pWnd;
	CWnd *pDlg = NULL;
	CWnd *pItem = NULL;

	if ((pWnd = CWnd::FromHandle(hWnd)) != NULL)
	{
		pDlg = pWnd->GetParent();
	}

#if DPM_DEBUG_OPENFILE_HOOK
	if (gpfOK != NULL)
	{
		fprintf(gpfOK, "hook: hWnd=%8X uMsg=%08X %-20s wParam=%08X lParam=%08X",
			hWnd, uMsg, OSO_LookupWinMacro_WM(uMsg), wParam, lParam);
		if (hWnd != NULL && uMsg == WM_NOTIFY && lParam != 0)
		{
			LPNMHDR pnmh;
			pnmh = (LPNMHDR) lParam;
			fprintf(gpfOK, " code=%08X %-12s hWnd=%08X from=%08X",
				pnmh->code, OSO_LookupWinMacro_CDN(pnmh->code), pnmh->hwndFrom, pnmh->idFrom);
		}
		fprintf(gpfOK, "\n");
	}
#endif

	if (hWnd != NULL && uMsg == WM_INITDIALOG && pDlg != NULL)
	{
		bool bCenterIt = true;
		bool bResizeIt = true;
		RECT sOrigBox;
		RECT sNewBox;
		struct ADJUST_INFO sAdjust;

		sAdjust.nDeltaWidth  = 0;
		sAdjust.nDeltaHeight = 0;
		sAdjust.nDeltaWidth  = 0;

		/* Resize the box (moving or resizing most of its items) so that the user can see all the details at once.
		 */
		if (bCenterIt || bResizeIt)
		{
			pDlg->GetWindowRect(&sOrigBox);
			sNewBox = sOrigBox;
		}
		if (bResizeIt)
		{
			int nOrigWidth;
			int nOrigHeight;
			nOrigWidth   = sOrigBox.right  - sOrigBox.left;
			nOrigHeight  = sOrigBox.bottom - sOrigBox.top;
			if (nOrigWidth  < 666) sAdjust.nDeltaWidth  = 666 - nOrigWidth;
			if (nOrigHeight < 450) sAdjust.nDeltaHeight = 450 - nOrigHeight;
			if (sAdjust.nDeltaWidth == 0 && sAdjust.nDeltaHeight == 0)
			{
				bResizeIt = false;
			}
		}
		if (bResizeIt)
		{
			sNewBox.right  += sAdjust.nDeltaWidth;
			sNewBox.bottom += sAdjust.nDeltaHeight;
#if DPM_DEBUG_OPENFILE_HOOK
			OSO_I_ShowItemInfo(pDlg, &sOrigBox);
#endif
		}

		/* center it instead of jamming it into the monitor's top-left corner
		 */
		if (bCenterIt)
		{
			RECT sDesktop;
			GetWindowRect(GetDesktopWindow(), &sDesktop);
			int nNewWidth;
			int nNewHeight;
			nNewWidth   = sNewBox.right  - sNewBox.left;
			nNewHeight  = sNewBox.bottom - sNewBox.top;
			sNewBox.left   = (sDesktop.right  - nNewWidth)  / 2;
			sNewBox.top    = (sDesktop.bottom - nNewHeight) / 2;
			sNewBox.right  = sNewBox.left + nNewWidth;
			sNewBox.bottom = sNewBox.top  + nNewHeight;
		}
		if (bCenterIt || bResizeIt)
		{
			OSO_I_AdjustWindow(pDlg, &sNewBox);
		}

#if defined(THIS_IS_THE_OUTPUT_FROM_VisDev)
x item    1 rect=[336,411,201,224] width= 75 height= 23 &Open!                    from DLGS.H . . .
x item    2 rect=[336,411,230,253] width= 75 height= 23 Cancel!
. item 1038 rect=[336,411,258,281] width= 75 height= 23 &Help!                0x40E   psh15  pushbutton
. item 1040 rect=[ 84,195,263,279] width=111 height= 16 Open as &read-only!   0x410   chx1   checkbox
x item 1088 rect=[282,414, 27, 53] width=132 height= 26 !                     0x440   stc1   static text
x item 1089 rect=[ 11, 83,235,248] width= 72 height= 13 Files of &type:!      0x441   stc2      "
x item 1090 rect=[ 11, 83,204,217] width= 72 height= 13 File &name:!          0x442   stc3      "
x item 1091 rect=[ 14, 55, 32, 45] width= 41 height= 13 Look &in:!            0x443   stc4      "
x item 1120 rect=[  9,417, 55,193] width=408 height=138 !                     0x460   lst1   listbox
x item 1136 rect=[ 84,317,232,253] width=233 height= 21 Archive Files!        0x470   cmb1   combo box
x item 1137 rect=[ 57,279, 27, 49] width=222 height= 22 !                     0x471   cmb2      "
x item 1152 rect=[ 84,317,202,222] width=233 height= 20 !                     0x480   edt1   edit control
#endif
#if defined(THIS_IS_THE_OUTPUT_FROM_VisualStudio_2005)
x item    1 rect=[477,552,357,380] width= 75 height= 23 &Open!
x item    2 rect=[477,552,383,406] width= 75 height= 23 Cancel!
. item 1038 rect=[477,552,383,406] width= 75 height= 23 &Help!                 [hidden]
. item 1040 rect=[198,438,382,395] width=240 height= 13 Open as &read-only!    [hidden]
x item 1088 rect=[375,495, 36, 59] width=120 height= 23 !
x item 1089 rect=[104,191,388,401] width= 87 height= 13 Files of &type:!
x item 1090 rect=[104,191,362,375] width= 87 height= 13 File &name:!
x item 1091 rect=[  9, 95, 40, 53] width= 86 height= 13 Look &in:!
x item 1120 rect=[102,552, 65,348] width=450 height=283 !
x item 1136 rect=[198,444,385,406] width=246 height= 21 Archive Files!
x item 1137 rect=[102,363, 36, 58] width=261 height= 22 !
x item 1148 rect=[198,444,357,379] width=246 height= 22 !                      0x47C   cmb13   ?
x item 1184 rect=[  9, 96, 65,403] width= 87 height=338 !                      0x4A0   ctl1    ?
#endif

#define MY_OFDBOXITEM_LOOKIN_LABEL                 stc4
#define MY_OFDBOXITEM_LOOKIN_VALUE                 cmb2
#define MY_OFDBOXITEM_LISTBOX_LABEL                stc1
#define MY_OFDBOXITEM_LISTBOX_VALUE                lst1
#define MY_OFDBOXITEM_FILENAME_LABEL               stc3
#if USING_VS2005_OR_LATER
#define MY_OFDBOXITEM_FILENAME_VALUE               cmb13
#else
#define MY_OFDBOXITEM_FILENAME_VALUE               edt1
#endif
#define MY_OFDBOXITEM_FILESOFTYPE_LABEL            stc2
#define MY_OFDBOXITEM_FILESOFTYPE_VALUE            cmb1
#define MY_OFDBOXITEM_OPEN                         IDOK
#define MY_OFDBOXITEM_CANCEL                       IDCANCEL
#define MY_OFDBOXITEM_SIDEBAR                      ctl1

		if (bResizeIt)
		{
			/* For each item, decide whether it should move or stretch or both or neither.
			 *
			 * Unfortunately, we cannot stretch the "Look In:" value to the right
			 * because the UP/CREATE/VIEW controls get in the way and I cannot
			 * figure out how to move them.  There seems to be no item code for them.
			 *
			 * Under Windows XP Pro (2002 SP2) and/or Visual Studio 2005, the GetWindowRect call
			 * includes the frame when reporting offsets but the SetWindowPos call expects values
			 * which do NOT.  So for each _real_ item, we have to also detect item #0 which is a
			 * single point (currently [3,3,29,29]) representing the top-left-most corner of the
			 * window excluding the frame, and then subtract that many pixels during calculations.
			 */
			CWnd *psTopLeftCorner;
			if ((psTopLeftCorner = pDlg->GetDlgItem(0)) != NULL)
			{
				psTopLeftCorner->GetWindowRect(&sAdjust.sZeroPoint);
			}
			else
			{
				sAdjust.sZeroPoint.top    = 0;
				sAdjust.sZeroPoint.left   = 0;
				sAdjust.sZeroPoint.bottom = 0;
				sAdjust.sZeroPoint.right  = 0;
			}
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_LOOKIN_LABEL,      0);
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_LOOKIN_VALUE,      0);
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_LISTBOX_LABEL,     0);
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_LISTBOX_VALUE,     STRETCH_BOTH);
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_FILENAME_LABEL,    MOVE_DOWN);
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_FILENAME_VALUE,    MOVE_DOWN | STRETCH_RIGHT);
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_FILESOFTYPE_LABEL, MOVE_DOWN);
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_FILESOFTYPE_VALUE, MOVE_DOWN | STRETCH_RIGHT);
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_OPEN,              MOVE_BOTH);
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_CANCEL,            MOVE_BOTH);
			OSO_I_AdjustItem(pDlg, &sAdjust, MY_OFDBOXITEM_SIDEBAR,           0);
		}

		if ((pItem = pDlg->GetDlgItem(MY_OFDBOXITEM_LISTBOX_VALUE)) != NULL)
		{
			pItem->SetFocus();
		}

		/* Enable the detail view so user can see the size of each .ARC file ---
		 * I am unable to match any Windows macro to the magic number in hex or decimal (40964).
		 *
		 * NOTE:  this must come _after_ the moving and resizing or it does not take effect, I don't know why.
		 */
		pDlg->PostMessage(WM_COMMAND, 0x0000A004, NULL);

		return FALSE;
	}

	return FALSE;   /* let the default handler be invoked */
}


static void OSO_I_PrepareOpenFileBlock(
	OPENFILENAME *psInfo,
	char *pcBuffer,
	int nBufferSize
)
{
	ZeroMemory(psInfo, sizeof(*psInfo));
	psInfo->lStructSize       = sizeof(*psInfo);
	psInfo->hwndOwner         = NULL;
	psInfo->hInstance         = NULL;
	psInfo->lpstrFilter       = "Archive Files\0*.arc\0\0";
	psInfo->lpstrCustomFilter = NULL;
	psInfo->nMaxCustFilter    = 0;
	psInfo->nFilterIndex      = 0;
	psInfo->lpstrFile         = pcBuffer;
	psInfo->nMaxFile          = nBufferSize;
	psInfo->lpstrFileTitle    = NULL;
	psInfo->nMaxFileTitle     = 0;
	psInfo->lpstrInitialDir   = gcRemoteArcPath;
	psInfo->lpstrTitle        = "Select one or more archive files";
	psInfo->Flags             = OFN_ALLOWMULTISELECT   |
#if defined(OFN_DONTADDTORECENT)
	                                  OFN_DONTADDTORECENT    |
#endif
	                                  OFN_ENABLEHOOK         |
	                                  OFN_EXPLORER           |
	                                  OFN_FILEMUSTEXIST      |
	                                  OFN_PATHMUSTEXIST      |
	                                  OFN_NOCHANGEDIR        |
	                                  OFN_HIDEREADONLY       |
	                                  OFN_NONETWORKBUTTON;
	psInfo->nFileOffset       = 0;
	psInfo->nFileExtension    = 0;
	psInfo->lpstrDefExt       = "arc";
	psInfo->lCustData         = NULL;
	psInfo->lpfnHook          = OSO_K_OpenFileDialog;
	psInfo->lpTemplateName    = NULL;
#if (_WIN32_WINNT >= 0x0500)
	psInfo->pvReserved        = NULL;
	psInfo->dwReserved        = 0;
	psInfo->FlagsEx           = 0;
#endif

	*pcBuffer = '\0';    // we have to put a null byte here to say "no initialization necessary"
}


int CSsgsUtilGuiDlg::OSO_M_SelectArcFiles(
	void
)
{
	int nFilesSelected = 0;
	OPENFILENAME sOpenFileInfo;
	char cBigBuffer[16 * 1024];

#if DPM_DEBUG_OPENFILE_HOOK
	gpfOK = fopen("D:\\dpm\\hook.txt", "w");
#endif

	OSO_I_PrepareOpenFileBlock(&sOpenFileInfo, cBigBuffer, sizeof(cBigBuffer));
	BOOL xxx = GetOpenFileName(&sOpenFileInfo);
	if (xxx)
	{
		/* The buffer contains a series of null-terminated strings and ending with an extra null byte.
		 *
		 * If only one file was selected, then that full filename is the entire buffer:
		 *     C:\dpm\poof\test0.arc^^
		 *
		 * However, if multiple files were selected, then there is a null byte between the path
		 * and the first filename, and NO TRAILING BACKSLASH for the path:
		 *     C:\dpm\poof^test3.arc^test2.arc^test1.arc^^
		 *
		 * Note that the .RC file creates the list with a descending sort,
		 * no doubt due to the timestamp within true archive files.
		 */
		char cPostPath[512];
		memcpy(cPostPath, cBigBuffer, sOpenFileInfo.nFileOffset);
		if (cPostPath[sOpenFileInfo.nFileOffset - 1] == '\0')
		{
			cPostPath[sOpenFileInfo.nFileOffset - 1] = '\\';
		}
		cPostPath[sOpenFileInfo.nFileOffset] = '\0';
		/* There is no clean way to prevent the user from changing folders during GetOpen
		 * but they cannot be allowed to select files from outside the folder described
		 * by the three radio buttons they clicked --- this is my method of enforcement.
		 */
		if (strcmp(gcRemoteArcPath, cPostPath))
		{
			OSO_I_ShowMessage(SL_SEVERITY_ERROR,
				"You specified the " + gcRemoteArcPath + " directory, "
				"but then chose .ARC files from " + cPostPath + " ... please choose again",
				false);
		}
		else
		{
			char *pcCurr = &cBigBuffer[sOpenFileInfo.nFileOffset];
			while (*pcCurr != '\0')
			{
				m_ArcList.AddString(pcCurr);
				pcCurr += strlen(pcCurr) + 1;
				++nFilesSelected;
			}
		}
	}
	else if (CommDlgExtendedError() == 0)
	{
		/* "If the common dialog box function returned FALSE
		 *  because the user closed or canceled the dialog box,
		 *  the return value is zero."
		 */
	}
	else
	{
		char cMessage[512];
		sprintf(cMessage, "call to GetOpenFileName() failed --- CommDlgExtendedError=%d", CommDlgExtendedError());
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, cMessage, false);
	}

#if DPM_DEBUG_OPENFILE_HOOK
	if (gpfOK != NULL)
	{
		fclose(gpfOK);
	}
#endif

	return nFilesSelected;
}


void CSsgsUtilGuiDlg::OSO_M_OnButtonSelect()
{
	OSO_M_ResetUserInput(USER_CHOSE_SELECT);

	OSO_I_BuildLogfileName(0);

	/* When the "Select Archive Files" button is clicked
	 * _AND_ there are any .arc files in the chosen directory,
	 * give the user a standard "open file" dialog box already set
	 * for "detailed" view of that directory of archive files
	 * on the selected GTACS, for the selected SC and stream,
	 * and let them choose multiple .ARC files.
	 */
	if (OSO_I_DoesWildcardExist(gcRemoteArcPath + "*.arc"))
	{
		if (OSO_M_SelectArcFiles() > 0)
		{
			if (gcDestPath != "")
			{
				GetDlgItem(IDC_BUTTON_CONVERT)->EnableWindow(true);
			}
		}
	}
	else
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, "There are no .ARC files in the directory " + gcRemoteArcPath, false);
	}
}


void CSsgsUtilGuiDlg::OSO_M_OnButtonBrowse()
{
	/* The user wants a different target directory for the DM4 files.
	 */
	while (1)
	{
		/* An extra layer of scope to ensure the dialog box is created fresh each time,
		 * using the current target as the starting point.
		 */
		{
			CDlgFolderChooser dChoose(this);
			dChoose.SetTitle("Choose a folder for the DM4 files");
			dChoose.SetPath(gcDestPath);
			if (dChoose.DoModal() != IDOK)
			{
				break;
			}
			if (dChoose.GetPath().MakeUpper() == gcDestPath.MakeUpper())
			{
				break;
			}
			/* Do not allow the user to select a directory in which he cannot create a file ---
			 * the files would be converted correctly into the working area, but the following
			 * rename() calls would fail, so let's prevent that _now_.
			 */
			if (OSO_I_DirectoryIsWriteable(dChoose.GetPath(), true))
			{
				gcDestPath = dChoose.GetPath();
				if (gpfDump != NULL) fprintf(gpfDump, "destpath=%s due to choose.getpath()\n", gcDestPath);
				OSO_I_EnsureTrailingBackslash(&gcDestPath);
				OSO_M_BuildPathTo(false);
				if (AfxMessageBox("Change accepted for this session.  Do you want to write it to disk for future runs?", MB_YESNO) == IDYES)
				{
					OSO_I_SaveDestPath();
				}
				if (m_ArcList.GetCount())
				{
					GetDlgItem(IDC_BUTTON_CONVERT)->EnableWindow(true);
				}
				break;
			}
		}
	}
}


static CString OSO_I_SwitchPathDelims(
	CString cWindowsPath
)
{
	CString cUnixPath;

	cUnixPath = cWindowsPath;
	cUnixPath.Replace('\\', '/');

	return cUnixPath;
}


bool CSsgsUtilGuiDlg::OSO_M_CreateToDoFile(
	CString cFilename
)
{
	/* This is purely a data file, not an executable script,
	 * for obvious security reasons.  It would look like this:
	 *
	 *    version    1.1
	 *    wanted     DM4
	 *    user       dmurphy
	 *    domain     NCWS_DEV
	 *    computer   NSWKS87
	 *    log        D:\SSGSUtil\log\2008076.log
	 *    server     sgtacs04
	 *    spacecraft goes15
	 *    stream     rt3
	 *    prefix     s4
	 *    share      \\sgtacs04\gtacsops\
	 *    account    gtacsops
	 *    path       epoch/output/
	 *    directory  ARC_sgtacs04_2008091-145823_00002828.d
	 *    count      4
	 *    file       sgtacs04/goes15/s4rt3/b2008042180003.arc
	 *    file       sgtacs04/goes15/s4rt3/b2008042190003.arc
	 *    file       sgtacs04/goes15/s4rt3/b2008042200003.arc
	 *    file       sgtacs04/goes15/s4rt3/b2008042210003.arc
	 *
	 * The cron job will notice its existence within a few seconds
	 * and kick off a process which will read it in, run the
	 * conversions, and then delete the data file.  The non-existence
	 * of the data file is the signal to *this* process that the
	 * conversions have finished.
	 *
	 * Of course, it is first created in our "temp" folder
	 * and then moved to the server as a whole when we're
	 * finished writing to it, to prevent the cron child
	 * from trying to open & read the file while we're still
	 * busy with it.
	 */

	int nArcFiles = 0;
	FILE *pfToDo;

	m_ArcSizeTotal = 0;
	if ((pfToDo = fopen(cFilename, "w")) == NULL)
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, "Unable to create a TODO file named " + cFilename, true);
	}
	else
	{
		/* Write out the configuration information, some facts about this workstation,
		 * and then dump the contents of the list-box resulting from the OpenFile dialog.
		 */
		fprintf(pfToDo, "version    %s\n", TODO_SYNTAX_VERSION);
		fprintf(pfToDo, "wanted     %s\n", "DM4");
		fprintf(pfToDo, "user       %s\n", getenv("USERNAME"));
		fprintf(pfToDo, "domain     %s\n", getenv("USERDOMAIN"));
		fprintf(pfToDo, "computer   %s\n", getenv("COMPUTERNAME"));
		fprintf(pfToDo, "log        %s\n", gcLocalLogFile);
		fprintf(pfToDo, "server     %s\n", gcChosenServer);
		fprintf(pfToDo, "spacecraft %s\n", gcChosenSpacecraft);
		fprintf(pfToDo, "stream     %s\n", gcChosenStream);
		fprintf(pfToDo, "prefix     %s\n", gcStreamPrefix);
		fprintf(pfToDo, "share      %s\n", gcRemoteShare);
		fprintf(pfToDo, "account    %s\n", gcRemoteUsername);
		fprintf(pfToDo, "path       %s\n", OSO_I_SwitchPathDelims(gcRemoteOutDir + "\\"));
		fprintf(pfToDo, "directory  %s\n", OSO_I_SwitchPathDelims(strrchr(gcRemoteWorkDir, '\\') + 1));
		fprintf(pfToDo, "count      %d\n", m_ArcList.GetCount());
		int j;
		for (j = 0; j < m_ArcList.GetCount(); j++)
		{
			CString cOneFileName;
			unsigned long nOneFileSize;
			m_ArcList.GetText(j, cOneFileName);
			cOneFileName = gcRemoteArcDir + "\\" + cOneFileName;
			fprintf(pfToDo, "file       %s\n", OSO_I_SwitchPathDelims(cOneFileName));
			++nArcFiles;
			cOneFileName = gcRemoteOutPath + cOneFileName;
			nOneFileSize = OSO_I_HowBigIsThisFile(cOneFileName.GetString(), false);
			m_ArcSizes.AddTail(nOneFileSize);
			m_ArcSizeTotal += nOneFileSize / 1024;
		}
		fclose(pfToDo);
	}

	return (nArcFiles > 0);
}


static CString OSO_I_DescribeFile(
	const char *pcFilespec
)
{
	CString cDescription;
	char cSizeBuffer[132];
	char cDateBuffer[132];
	struct _stat sFileInfo;

	cSizeBuffer[0] = '?';
	cSizeBuffer[1] = '\0';
	cDateBuffer[0] = '?';
	cDateBuffer[1] = '\0';
	if (_stat(pcFilespec, &sFileInfo) == 0)
	{
		/* I want the description to look like
		 *
		 *      (99999 bytes, modified Tuesday, April 15 2008, 19:37:58)
		 */
		struct tm *psModified;
		sprintf(cSizeBuffer, "%d", (unsigned long) sFileInfo.st_size);
		if ((psModified = localtime(&sFileInfo.st_mtime)) != NULL)
		{
			strftime(cDateBuffer, sizeof(cDateBuffer), "%A, %B %#d %Y, %H:%M:%S", psModified);
		}
	}
	cDescription  = "        (";
	cDescription += cSizeBuffer;
	cDescription += " bytes, modified ";
	cDescription += cDateBuffer;
	cDescription += ")";

	return cDescription;
}


BOOL OSOOverwritingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	GetDlgItem(IDC_OVERWRITE_DESC1)->SetWindowText("The folder " + gcDestPath);
	GetDlgItem(IDC_OVERWRITE_DESC2)->SetWindowText("already contains a file named \"" + cBasename + "\".");
	GetDlgItem(IDC_OVERWRITE_DESC3)->SetWindowText("Would you like to replace the existing file");
	GetDlgItem(IDC_OVERWRITE_DESC4)->SetWindowText(OSO_I_DescribeFile(cFullDestName.GetString()));
	GetDlgItem(IDC_OVERWRITE_DESC5)->SetWindowText("with this newly converted one?");
	GetDlgItem(IDC_OVERWRITE_DESC6)->SetWindowText(OSO_I_DescribeFile(cFullWorkName.GetString()));

	UpdateData(false);

	return FALSE;
}


void OSOOverwritingDlg::OSO_M_OnButtonOverwriteYes()   { EndDialog(IDYES);                     }
void OSOOverwritingDlg::OSO_M_OnButtonOverwriteNo()    { EndDialog(IDNO);                      }
void OSOOverwritingDlg::OSO_M_OnButtonOverwriteAll()   { EndDialog(IDC_BUTTON_OVERWRITE_ALL);  }
void OSOOverwritingDlg::OSO_M_OnButtonOverwriteNone()  { EndDialog(IDC_BUTTON_OVERWRITE_NONE); }


static bool gbOverwriteAll  = false;
static bool gbOverwriteNone = false;


static bool OSO_I_MoveDM4(
	CString cBasename
)
{
	bool bTryRenaming = true;
	OSOOverwritingDlg sOverwriteBox;

	/* Use this dialog box's namespace even though we might not need
	 * to actually draw it and get input.
	 */
	sOverwriteBox.cBasename     = cBasename;
	sOverwriteBox.cFullWorkName = gcRemoteWorkDir + "\\" + cBasename;
	sOverwriteBox.cFullDestName = gcDestPath + cBasename;
	if (OSO_I_FileExists(sOverwriteBox.cFullDestName.GetString()))
	{
		bTryRenaming = false;
		bool bTryDeleting = false;
		if (gbOverwriteAll)
		{
			bTryDeleting = true;
		}
		else if (gbOverwriteNone)
		{
			bTryDeleting = false;
		}
		else
		{
			INT_PTR nButton = sOverwriteBox.DoModal();
			if (nButton == IDOK || nButton == IDYES)
			{
				bTryDeleting = true;
			}
			else if (nButton == IDNO || nButton == IDCANCEL)
			{
				bTryDeleting = false;
			}
			else if (nButton == IDC_BUTTON_OVERWRITE_ALL)
			{
				gbOverwriteAll = true;
				bTryDeleting = true;
			}
			else if (IDC_BUTTON_OVERWRITE_NONE)
			{
				gbOverwriteNone = true;
				bTryDeleting = false;
			}
		}
		if (bTryDeleting)
		{
			if (OSO_I_DeleteOneFile(sOverwriteBox.cFullDestName, true))
			{
				bTryRenaming = true;
			}
		}
	}

	if (bTryRenaming)
	{
		if (rename(sOverwriteBox.cFullWorkName, sOverwriteBox.cFullDestName) == 0)
		{
			return true;
		}
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, "Unable to move " + sOverwriteBox.cFullWorkName + " to " + gcDestPath, true);
	}

	return false;
}


static bool OSO_I_StartJobOnServer(
	const char *pcLocalTodoFilename,
	const char *pcRemoteTodoFilename
)
{
	gbOverwriteAll  = false;
	gbOverwriteNone = false;
	if (rename(pcLocalTodoFilename, pcRemoteTodoFilename) == 0)
	{
		return true;
	}

	CString cMessage = "Unable to move ";
	cMessage += pcLocalTodoFilename;
	cMessage += " to ";
	cMessage += pcRemoteTodoFilename;
	OSO_I_ShowMessage(SL_SEVERITY_ERROR, cMessage, true);
	return false;
}


static bool OSO_I_AnalyzeOneLogfile(
	CString remoteLogFilename,
	char *pcCurrentDm4File
)
{
	bool bConvertedOkay = false;
	FILE *pfRemoteLog;
	char cLogBuffer[1024];

	/* The line indicating success looks like this:
	 *
	 *    DMF Output File: /export/home/gtacsops/epoch/output/ARC_sgtacs04_2008090-151332_00004088.d/2008042160034_2008042170004.s4rt3
	 *
	 * The line indicating failure looks like this:
	 *
	 *    ERROR #16 in MAIN:
	 *
	 * or this:
	 *
	 *    WARNING: There were no processed telemtry frames, No frames in archive file?
	 */

	PRINT_1_TRACK("looking for logfile (remote) %s\n", remoteLogFilename);
	if ((pfRemoteLog = fopen(remoteLogFilename, "r")) == NULL)
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR,
			"unable to open remote logfile " + remoteLogFilename + " as input", true);
	}
	else
	{
		while (fgets(cLogBuffer, sizeof(cLogBuffer), pfRemoteLog) != NULL)
		{
			PRINT_1_TRACK("looking at logline %s\n", cLogBuffer);
			if (memcmp(cLogBuffer, "DMF Output File: ", 17) == 0)
			{
				char *pcLastSlash;
				char cDummy[512];
				char cFullDm4Filename[256];
				sscanf(cLogBuffer, "%s %s %s %s %s", cDummy, cDummy, cDummy, cFullDm4Filename, cDummy);
				if ((pcLastSlash = strrchr(cFullDm4Filename, '/')) != NULL)
				{
					strcpy(pcCurrentDm4File, pcLastSlash + 1);
					PRINT_1_TRACK("result is %s\n", pcCurrentDm4File);
					bConvertedOkay = true;
					break;
				}
			}
			if (strstr(cLogBuffer, "ERROR") != NULL)
			{
				strcpy(pcCurrentDm4File, cLogBuffer);
				break;
			}
			if (strstr(cLogBuffer, "WARNING:") != NULL)
			{
				strcpy(pcCurrentDm4File, cLogBuffer);
				break;
			}
		}
		if (ferror(pfRemoteLog))
		{
			OSO_I_ShowMessage(SL_SEVERITY_ERROR, "unable to read remote logfile " + remoteLogFilename, true);
		}
		fclose(pfRemoteLog);
	}

	return bConvertedOkay;
}

/* Macros for the controls in the CONVERTING dialog box and timer thereof.
 *
 * We set the range and position of each progress bar to --- and calculate
 * the "percentages" as --- 200 instead of 100 because the bars are
 * exactly 200 pixels wide, reflecting new values twice as often.
 */

#define IDT_ONE_SECOND   222
#define MILLISECONDS    1000

#define EXPECTED           0
#define SOFAR              1

#define CRON_SECONDS      15

#define CALC_PCT(a1)       (a1[SOFAR] > a1[EXPECTED]) ? 200 : (int) (200.0 * (double) a1[SOFAR] / (double) a1[EXPECTED])
#define SET_PCT(a1,a2)           ((CProgressCtrl *) GetDlgItem(a1))->SetPos(a2)
#define SHOW_PROGRESS(a1,a2,a3)   sprintf(cTemp, "%d", a2[a3]); GetDlgItem(a1)->SetWindowText(cTemp)
#define CLEAR_PROGRESS(a1)        GetDlgItem(a1)->SetWindowText("0")

static OSOConvertingDlg *gpConvertingDialogBox = NULL;

void CALLBACK EXPORT OSO_K_OneSecondTimer(
	HWND hWnd,
	UINT nMsg,
	UINT_PTR nIDEvent,
	DWORD dwTime
)
{
	/* I do not know how to obtain the CDialog from the first argument (a CWnd)
	 * so I am reduced to using a global variable.  I am ashamed.
	 */
	if (nMsg == WM_TIMER && nIDEvent == IDT_ONE_SECOND)
	{
		if (gpConvertingDialogBox != NULL)
		{
			gpConvertingDialogBox->OSO_M_TimerWentOff(nIDEvent, dwTime);
		}
	}
}


BOOL OSOConvertingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_nArcsFound = 0;
	m_nErrsFound = 0;
	m_nDm4sFound = 0;
	m_nDm4sMoved = 0;
	m_bIgnoreTimer = false;
	m_bWaitingForCron = true;

	((CProgressCtrl *) GetDlgItem(IDC_START_PROGRESS))->SetRange32(0, 200);
	((CProgressCtrl *) GetDlgItem(IDC_FILES_PROGRESS))->SetRange32(0, 200);
	((CProgressCtrl *) GetDlgItem(IDC_SIZES_PROGRESS))->SetRange32(0, 200);
	((CProgressCtrl *) GetDlgItem(IDC_TIMES_PROGRESS))->SetRange32(0, 200);

	SetWindowText("Waiting ...");

	char cTemp[32];
	CLEAR_PROGRESS(IDC_START_CURRENT);
	CLEAR_PROGRESS(IDC_FILES_CURRENT);
	CLEAR_PROGRESS(IDC_SIZES_CURRENT);
	CLEAR_PROGRESS(IDC_TIMES_CURRENT);
	SHOW_PROGRESS(IDC_START_TARGET, m_nStart, EXPECTED);
	SHOW_PROGRESS(IDC_FILES_TARGET, m_nFiles, EXPECTED);
	SHOW_PROGRESS(IDC_SIZES_TARGET, m_nSizes, EXPECTED);
	SHOW_PROGRESS(IDC_TIMES_TARGET, m_nTimes, EXPECTED);

	/* In theory, I should be able to pass NULL as the third argument and simply have
	 * an overriding copy of OnTimer() catch the message from the application queue,
	 * but when I try that, nothing happens.  Obviously I don't understand this and
	 * I'm out of time, so I do it with an explicit callback function.
	 */
	if ((m_pvOneSecond = SetTimer(IDT_ONE_SECOND, 1 * MILLISECONDS, OSO_K_OneSecondTimer)) == 0)
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, "Unable to start the one-second timer", false);
	}

	UpdateData(false);

	return FALSE;
}


void OSOConvertingDlg::OSO_M_StopTimer(
	void
)
{
	if (m_pvOneSecond != 0)
	{
		KillTimer(m_pvOneSecond);
		m_pvOneSecond = 0;
	}
}


void OSOConvertingDlg::OSO_M_TimerWentOff(
	UINT_PTR nIDEvent,
	DWORD dwTime
)
{
	/* If the user clicks on the ABORT button, the timer will keep
	 * ticking (meaning this function will continue to be invoked)
	 * while he/she stares at the "are you sure you want to abort?" prompt,
	 * so we must make sure we don't do anything during that time.
	 */
	if (m_bIgnoreTimer)
	{
		return;
	}

	bool bFinished = false;
	char cTemp[32];
	CString cTheText;
	long nSecondsSoFar = (dwTime - m_nBeginTime) / CLOCKS_PER_SEC;

	if (m_bWaitingForCron)
	{
		/* Upon noticing an unprocessed .TODO file, the cron job
		 * should create a matching .LOG file almost immediately ---
		 * we will not check for other signs of progress until
		 * that .LOG file can be seen.
		 */
		int nPercentage = 200;
		if (OSO_I_FileExists(gcRemoteLogFilename))
		{
			/* Now we start the clock over, to track how long it takes
			 * for the "convert" program to actually create the DM4 files.
			 */
			m_bWaitingForCron = false;
			m_nBeginTime = GetTickCount();
		}
		else if (nSecondsSoFar > ONE_MINUTE)
		{
			/* For some reason the .TODO file was not noticed, so
			 * we'll abort.  A message will be shown by WaitForJob()
			 * after this modal ends, but we don't need a new code
			 * to indicate what happened --- it will see that the
			 * waiting-for-cron flag is still set.
			 */
			OSO_M_StopTimer();
			EndDialog(IDOK);
		}
		else
		{
			m_nStart[SOFAR] = nSecondsSoFar;
			nPercentage = CALC_PCT(m_nStart);
		}
		SET_PCT(IDC_START_PROGRESS, nPercentage);
		SHOW_PROGRESS(IDC_START_CURRENT, m_nStart, SOFAR);
	}
	else
	{
		m_nTimes[SOFAR] = nSecondsSoFar;
		if (m_cCurrArcName == "")
		{
			/* Since there are two lists, it is vital to track through them in synch,
			 * so always get an archive's size at the same time that we get the name.
			 */
			m_wMain->m_ArcList.GetText(m_nArcsFound++, m_cCurrArcName);
			m_nCurrArcSize = (long) (m_wMain->m_ArcSizes.GetNext(m_pos) / 1024);
			m_cCurrLogfile = m_cCurrArcName;
			m_cCurrLogfile.Replace(".arc", ".log");
			m_cCurrLogfile = gcRemoteWorkDir + "\\" + m_cCurrLogfile;
		}
		if (OSO_I_FileExists(m_cCurrLogfile))
		{
			char cDm4Name[256];
			/* Always increment the "Size" total regardless of the analysis,
			 * because we are _not_ counting the amount of output data created,
			 * nor the amount of input data processed >successfully< ---
			 * we're simply counting the amount of data processed at all.
			 */
			m_nSizes[SOFAR] += m_nCurrArcSize;
			if (OSO_I_AnalyzeOneLogfile(m_cCurrLogfile, cDm4Name))
			{
				++m_nDm4sFound;
				m_bIgnoreTimer = true;
				if (OSO_I_MoveDM4(cDm4Name))
				{
					++m_nDm4sMoved;
					OSO_I_DeleteOneFile(m_cCurrLogfile, true);
				}
				m_bIgnoreTimer = false;
			}
			else
			{
				++m_nErrsFound;
			}
			m_wMain->m_Dm4List.AddTail(cDm4Name);
			m_nFiles[SOFAR] += 1;
			if (m_nArcsFound >= m_wMain->m_ArcList.GetCount())
			{
				bFinished = true;
			}
			else if (m_nFiles[SOFAR] >= 4)
			{
				/* Update the "expected" total time by extrapolating
				 * from how long it has taken to process this many files.
				 */
				char cTemp[32];
				double fNewSizePerSec = m_nSizes[SOFAR] * 1024.0 / m_nTimes[SOFAR];
				m_nTimes[EXPECTED] = (long) ((double) m_wMain->m_ArcSizeTotal * 1024.0 / fNewSizePerSec);
				SHOW_PROGRESS(IDC_TIMES_TARGET, m_nTimes, EXPECTED);
			}
			m_cCurrArcName = "";
		}
		else if (m_nTimes[SOFAR] > m_nTimes[EXPECTED] + 5 * ONE_MINUTE)
		{
			/* We are infinitely patient waiting for a conversion to finish
			 * (unlike the very short span we're willing to wait for cron above)
			 * but only because the user has a way to interrupt us.
			 */
			OSO_I_ShowMessage(SL_SEVERITY_ERROR,
				"The ARC-to-DM4 job on " + gcChosenServer.MakeUpper() +
				" is taking too long while processing " + m_cCurrArcName +
				" --- click [ABORT] if your patience is exhausted", false);
		}
		else
		{
			/* Nothing to do in this spot, we're just waiting for the _real_
			 * convert program on the server to finish the current archive file.
			 */
		}
		SET_PCT(IDC_FILES_PROGRESS, CALC_PCT(m_nFiles));
		SET_PCT(IDC_SIZES_PROGRESS, CALC_PCT(m_nSizes));
		SET_PCT(IDC_TIMES_PROGRESS, CALC_PCT(m_nTimes));
		SHOW_PROGRESS(IDC_FILES_CURRENT, m_nFiles, SOFAR);
		SHOW_PROGRESS(IDC_SIZES_CURRENT, m_nSizes, SOFAR);
		SHOW_PROGRESS(IDC_TIMES_CURRENT, m_nTimes, SOFAR);
		if (bFinished)
		{
			GetDlgItem(IDCANCEL)->EnableWindow(false);
			GetDlgItem(IDOK)->EnableWindow(true);
			GetDlgItem(IDOK)->SetFocus();
			OSO_M_StopTimer();
			SetWindowText("Finished");
		}
		else
		{
			char cTimeLeft[64];
			long nLeft = m_nTimes[EXPECTED] - m_nTimes[SOFAR];
			if (nLeft < 0) nLeft = 0;
			sprintf(cTimeLeft, "%d:%02d", (int) (nLeft / ONE_MINUTE), (int) (nLeft % ONE_MINUTE));
			SetWindowText("Converting " + m_cCurrArcName + " ...   (" + cTimeLeft + " remaining)");
		}
	}

	RedrawWindow();
}


static int OSO_I_SecondsUntilNoticed(
	const char *pcRemoteTodoFilename
)
{
	char cMessage[256];
	struct _stat sFileInfo;

	/* The cron job is supposed to run every minute, checking for
	 * new .TODO files exactly 0, 15, 30, and 45 seconds past the minute.
	 * So, by checking what time that remote file was last touched,
	 * we should be able to calculate how long until it is noticed.
	 */
	if (_stat(pcRemoteTodoFilename, &sFileInfo) == 0)
	{
		struct tm *psFileTime;
		if ((psFileTime = localtime(&sFileInfo.st_mtime)) != NULL)
		{
			return CRON_SECONDS - (psFileTime->tm_sec % CRON_SECONDS) + 1;
		}
		else
		{
			sprintf(cMessage, "Unable to localtime(%d)", sFileInfo.st_mtime);
		}
	}
	else
	{
		sprintf(cMessage, "Unable to stat(%s)", pcRemoteTodoFilename);
	}
	OSO_I_ShowMessage(SL_SEVERITY_ERROR, cMessage, true);

	return CRON_SECONDS;
}


void OSOConvertingDlg::OSO_M_ShowCounts(
	void
)
{
	char cMessage[512];

#define PLURAL(a1)   (a1 == 1) ? " was" : "s were"

	sprintf(cMessage,
		"%4d ARC file%s found\n"
		"%4d ARC files had errors\n"
		"%4d ARC file%s converted\n"
		"%4d DM4 file%s moved\nfrom\n        %s\nto\n        %s",
		m_nArcsFound, PLURAL(m_nArcsFound),
		m_nErrsFound,
		m_nDm4sFound, PLURAL(m_nDm4sFound),
		m_nDm4sMoved, PLURAL(m_nDm4sMoved),
		gcRemoteWorkDir,
		gcDestPath);
	OSO_I_ShowMessage(SL_SEVERITY_INFO, cMessage, false);
}


bool CSsgsUtilGuiDlg::OSO_M_WaitForJobToFinish(
	const char *pcRemoteTodoFilename
)
{
	bool bConverted = false;
	OSOConvertingDlg sConvertBox;

	gpConvertingDialogBox = &sConvertBox;
	sConvertBox.m_nBeginTime  = GetTickCount();
	sConvertBox.m_pvOneSecond = 0;
	sConvertBox.m_pos         = m_ArcSizes.GetHeadPosition();
	sConvertBox.m_wMain       = this;

	/* The "sofar" elements of each array are simply set to zero, but setting the "expected"s requires math.
	 * This output comes from an actual convert job, showing how long it took for real-world size files:
	 *
	 *         input file       bytes            output file                   bytes   seconds   bytes/sec
	 *     ------------------  -------     ---------------------------------  -------  -------  -----------
	 *     b2008042145320.arc (1978880) -> 2008042145319_2008042150035.s4rt3 (5635480)    5       395776
	 *     b2008042151350.arc (5861888) -> 2008042151421_2008042160035.s4rt3 (9452812)   16       366368
	 *     b2008042160003.arc (7303680) -> 2008042160034_2008042170004.s4rt3 (9744080)   19       384404
	 *     b2008042170003.arc (7291392) -> 2008042170003_2008042180004.s4rt3 (9706624)   19       383757
	 *
	 * Given these numbers, I decided that 375,000 bytes per second was a decent value for estimating.
	 *
	 * >>> one week later <<<
	 *
	 * Further testing has demonstrated two facts:  first is that the above value is a bit high.
	 * The second is that the value is average in name only; each .ARC file gets processed at
	 * vastly different rates, even with all other factors equal: they range from 300,000 to
	 * well over 450,000 on a regular basis.  So I now use the smallest value as a starting point,
	 * and then have TimerWentOff() re-evaluate the "expected" time using its own data
	 * each time a file (after the initial 3) have been processed.
	 */
	static const double fAverageSizePerSec = 300000.0;

	sConvertBox.m_nStart[SOFAR] = 0;
	sConvertBox.m_nFiles[SOFAR] = 0;
	sConvertBox.m_nSizes[SOFAR] = 0;
	sConvertBox.m_nTimes[SOFAR] = 0;

	sConvertBox.m_nStart[EXPECTED] = OSO_I_SecondsUntilNoticed(pcRemoteTodoFilename);
	sConvertBox.m_nFiles[EXPECTED] =                  m_ArcList.GetCount();
	sConvertBox.m_nSizes[EXPECTED] =                  m_ArcSizeTotal;
	sConvertBox.m_nTimes[EXPECTED] = (long) ((double) m_ArcSizeTotal * 1024.0 / fAverageSizePerSec);
	if (sConvertBox.m_nTimes[EXPECTED] < 5)
	{
		sConvertBox.m_nTimes[EXPECTED] = 5;
	}

	if (sConvertBox.DoModal() == IDCANCEL)
	{
		OSO_I_AddEntryToLogfile("user requested abort");
	}
	else if (sConvertBox.m_bWaitingForCron)
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR,
			"The OSO cron job on " +
			gcChosenServer.MakeUpper() +
			" should have noticed\n    " +
			pcRemoteTodoFilename +
			"\nby now, but it has not --- conversion aborted",
			false);
	}
	else
	{
		bConverted = (sConvertBox.m_nFiles[SOFAR] == sConvertBox.m_nFiles[EXPECTED]);
		sConvertBox.OSO_M_ShowCounts();
	}

	return bConverted;
}


static void OSO_I_RemoveWorkDirectory(
	void
)
{
	/* Try to delete the {uniquename}.d file ---
	 * watch for the error of being unable to remove it
	 * because it still contains files that could not be
	 * renamed to the destination folder, because there's
	 * no point in signalling that particular condition.
	 */
	if (_rmdir(gcRemoteWorkDir) < 0)
	{
		if (errno != EEXIST)
		{
			OSO_I_ShowMessage(SL_SEVERITY_WARNING,
				"unable to delete the directory named " + gcRemoteWorkDir, true);
		}
	}
}


bool CSsgsUtilGuiDlg::OSO_M_SubmitUsingToDo(
	void
)
{
	bool bToDoWorked = false;
	CString cLocalTodoFilename;
	CString cRemoteTodoFilename;
	CString cRemoteWorkDir;

	/* Make the .TODO file on a disk local to this Windows machine, fill it with info
	 * about the job and the names of the .ARC files to be converted, then move it to
	 * a disk that the server can see.  His cron job should see it within a few seconds
	 * and create a matching .LOG file for us to monitor.
	 *
	 * NOTE WELL:  The name of these files must begin with ARC_<servername>_ because the
	 * shared disk might be visible to more than one server, and we don't want multiple
	 * cron jobs trying to process the same .TODO file!  The unique name will look like
	 *
	 *     ARC_sgtacs04_2008094-185739_00003796
	 */
	{
		char cUniqueName[64];
		sprintf(cUniqueName, "ARC_%s_%s_%08d", gcChosenServer, OSO_I_WhatTimeIsIt(TRUE, FALSE), _getpid());
		cLocalTodoFilename  = gcLocalTempPath + cUniqueName + gcTodoExt;
		cRemoteTodoFilename = gcRemoteOutPath + cUniqueName + gcTodoExt;
		gcRemoteLogFilename = gcRemoteOutPath + cUniqueName + gcLogExt;
		gcRemoteWorkDir     = gcRemoteOutPath + cUniqueName + gcDirExt;
		PRINT_1_TRACK("TODO filename (local)  is %s\n", cLocalTodoFilename);
		PRINT_1_TRACK("TODO filename (remote) is %s\n", cRemoteTodoFilename);
	}
	/* We depend upon the server-side script to create the work directory.
	 * After the conversions are done, we move the DM4 files out of there
	 * to the user's target folder and delete the directory --- we don't
	 * need to worry about the logfiles in it, because we deleted them
	 * as we went along.
	 */
	if (OSO_M_CreateToDoFile(cLocalTodoFilename))
	{
		OSO_I_CopyLog(cLocalTodoFilename);
		if (OSO_I_StartJobOnServer(cLocalTodoFilename, cRemoteTodoFilename))
		{
			if (OSO_M_WaitForJobToFinish(cRemoteTodoFilename))
			{
				OSO_I_CopyLog(gcRemoteLogFilename);
				OSO_I_RemoveWorkDirectory();
				bToDoWorked = true;
			}
		}
	}

	return bToDoWorked;
}


void CSsgsUtilGuiDlg::OSO_M_OnButtonConvert(
	void
)
{
	/* This check shouldn't be necessary since we initially disabled the button,
	 * but paranoia is a good thing in a programmer.
	 */
	if (m_ArcList.GetCount() == 0)
	{
		OSO_I_ShowMessage(SL_SEVERITY_INFO, "No .ARC files have been selected", false);
	}
	else
	{
		/* Put a timestamped entry in the log indicating
		 * when the job has begun and when it ended.
		 */
		OSO_I_AddEntryToLogfile(NULL);
		OSO_I_AddEntryToLogfile("  Convert request initiated");
		OSO_M_SubmitUsingToDo();
		if (m_Dm4List.GetCount() > 0)
		{
			GetDlgItem(IDC_BUTTON_SHOWRES)->EnableWindow(true);
			GetDlgItem(IDC_BUTTON_SHOWLOG)->EnableWindow(true);
			OSO_I_AddEntryToLogfile("  Convert request complete");
		}
		else
		{
			OSO_I_AddEntryToLogfile("  Convert request failed");
		}
		OSO_I_AddEntryToLogfile(NULL);
	}
}


void CSsgsUtilGuiDlg::OSO_M_OnButtonShowResults(
	void
)
{
	int j = 0;
	char cTemp[512];
	char cOneFileName[256];
	CString cResults;
	POSITION posDm4 = m_Dm4List.GetHeadPosition();

	cResults  = "original file in                              converted file in\n";
	sprintf(cTemp, "%-34s %s\n", gcRemoteArcDir + "\\", gcDestPath);
	cResults += cTemp;
	cResults += "-----------------------------            --------------------------------------------------------\n";

	while (m_ArcList.GetText(j++, cOneFileName) > 0)
	{
		sprintf(cTemp, "%-34s %s\n", cOneFileName, m_Dm4List.GetNext(posDm4));
		cResults += cTemp;
	}
	AfxMessageBox(cResults, MB_ICONINFORMATION);
}


static CString OSO_I_ReadFileForTextWindow(
	CString pcFilespec
)
{
	FILE *pfTextFile;
	CString gcEntireFile = "";

	if ((pfTextFile = fopen(pcFilespec, "r")) == NULL)
	{
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, "unable to open " + pcFilespec + " as input", true);
	}
	else
	{
		char cReadBuffer[1024];
		while (fgets(cReadBuffer, sizeof(cReadBuffer), pfTextFile) != NULL)
		{
			int n;
			while ((n = strlen(cReadBuffer) - 1) >= 0)
			{
				if (cReadBuffer[n] == '\r') cReadBuffer[n] = '\0';
				else if (cReadBuffer[n] == '\n') cReadBuffer[n] = '\0';
				else
				{
					break;
				}
			}
			gcEntireFile += cReadBuffer;
			gcEntireFile += "\r\n";
		}
		fclose(pfTextFile);
	}

	return gcEntireFile;
}


BOOL OSOFileViewDlg::OnInitDialog()
{
	CWnd *pItem;

	CDialog::OnInitDialog();

	/* Update the window's title to show the filespec
	 */
	this->SetWindowText(m_cDescription + " " + m_cFilename);

	if ((pItem = this->GetDlgItem(IDC_TEXT_FILECONTENTS)) != NULL)
	{
		LOGFONT lf;
		memset(&lf, 0, sizeof(lf));
		lf.lfHeight = 14;
		strcpy(lf.lfFaceName, "Courier New");
		m_sCourierNewFont.CreateFontIndirect(&lf);
		pItem->SetFont(&m_sCourierNewFont, false);
		pItem->SetWindowText(OSO_I_ReadFileForTextWindow(m_cFilename));
	}
	if ((pItem = this->GetDlgItem(IDOK)) != NULL)
	{
		pItem->SetFocus();
	}

	UpdateData(false);

	return FALSE;
}


static void OSO_I_ShowTextFile(
	const char *pcFilespec,
	const char *pcDescription
)
{
	long nBytes;
	char cMessage[512];

	if ((nBytes = OSO_I_HowBigIsThisFile(pcFilespec, false)) < 0)
	{
		sprintf(cMessage, "Unable to open %s %s as input", pcDescription, pcFilespec);
		OSO_I_ShowMessage(SL_SEVERITY_ERROR, cMessage, true);
	}
	else if (nBytes == 0)
	{
		sprintf(cMessage, "The %s %s appears to be empty", pcDescription, pcFilespec);
		OSO_I_ShowMessage(SL_SEVERITY_WARNING, cMessage, false);
	}
	else
	{
		OSOFileViewDlg sDialog;
		sDialog.m_cFilename    = pcFilespec;
		sDialog.m_cDescription = pcDescription;
		sDialog.DoModal();
	}
}


void CSsgsUtilGuiDlg::OSO_M_OnButtonShowLog()
{
	OSO_I_ShowTextFile(gcRemoteLogFilename.GetString(), "remote logfile");
}
