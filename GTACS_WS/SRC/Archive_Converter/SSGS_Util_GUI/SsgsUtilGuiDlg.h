// SsgsUtilGuiDlg.h : header file
//

#ifndef SSGSUTILGUIDLG_DEFINED
#define SSGSUTILGUIDLG_DEFINED  1

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSsgsUtilGuiDlg dialog

class CSsgsUtilGuiDlg : public CDialog
{
// Construction
public:
	CSsgsUtilGuiDlg(CWnd* pParent = NULL);	// standard constructor
	int m_nSecondsPastTheMinute;
	CListBox m_ArcList;                              /* the name of each selected .ARC file */
	CList<unsigned long,unsigned long> m_ArcSizes;   /* the size of each selected .ARC file in bytes */
	unsigned long m_ArcSizeTotal;                    /* the sum of all the .ARC sizes in Kb */
	CStringList m_Dm4List;                           /* the name of each resultant DM4 file */

// Dialog Data
	//{{AFX_DATA(CSsgsUtilGuiDlg)
	enum { IDD = IDD_MAIN };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSsgsUtilGuiDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSsgsUtilGuiDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();

	afx_msg void OSO_M_OnRadioSgt1();
	afx_msg void OSO_M_OnRadioSgt2();
	afx_msg void OSO_M_OnRadioSgt3();
	afx_msg void OSO_M_OnRadioSgt4();
	afx_msg void OSO_M_OnRadioSgt5();

	afx_msg void OSO_M_OnRadioCgt1();
	afx_msg void OSO_M_OnRadioCgt2();
	afx_msg void OSO_M_OnRadioCgt3();
	afx_msg void OSO_M_OnRadioCgt4();
	afx_msg void OSO_M_OnRadioCgt5();

	afx_msg void OSO_M_OnRadioSc13();
	afx_msg void OSO_M_OnRadioSc14();
	afx_msg void OSO_M_OnRadioSc15();
	afx_msg void OSO_M_OnRadioSc16();

	afx_msg void OSO_M_OnRadioStr1();
	afx_msg void OSO_M_OnRadioStr2();
	afx_msg void OSO_M_OnRadioStr3();
	afx_msg void OSO_M_OnRadioStr4();
	afx_msg void OSO_M_OnRadioStr5();
	afx_msg void OSO_M_OnRadioStr6();
	afx_msg void OSO_M_OnRadioStr7();
	afx_msg void OSO_M_OnRadioStr8();

	afx_msg void OSO_M_OnExit();
	afx_msg void OSO_M_OnOk();
	afx_msg void OSO_M_OnButtonSelect();
	afx_msg void OSO_M_OnButtonBrowse();
	afx_msg void OSO_M_OnButtonConvert();
	afx_msg void OSO_M_OnButtonShowResults();
	afx_msg void OSO_M_OnButtonShowLog();
	//}}AFX_MSG

	void OSO_M_ResetUserInput(long nChosenMask);
	void OSO_M_WhichServer(    CString pcPrefix, CString pcSuffix, int nButtonToCheck);
	void OSO_M_WhichSpacecraft(                  CString pcSuffix, int nButtonToCheck);
	void OSO_M_WhichStream(                      CString pcSuffix, int nButtonToCheck);
	void OSO_M_BuildPathTo(bool bAppendDmfGoes99);
	bool OSO_M_BuildPathFrom(void);
	void OSO_M_ClearAllButtons(int nLast);
	void OSO_M_EnableAllButtons(int nLast);
	void OSO_M_DisableAllButtons(int nLast);

	int OSO_M_SelectArcFiles(void);
	bool OSO_M_SubmitUsingToDo(void);
	bool OSO_M_CreateToDoFile(CString pcFilename);
	bool OSO_M_WaitForJobToFinish(const char *pcRemoteTodoFilename);
	bool OSO_M_ReportFilesMoved(void);
	void OSO_M_TestAllSpacecraft(void);
	void OSO_M_TestOneSpacecraft(int nWhichButton, int nWhichSpacecraft);
	void OSO_M_TestAllStreams(void);
	void OSO_M_TestOneStream(int nWhichButton, int nWhichStream);

	DECLARE_MESSAGE_MAP()

private:
	bool m_bPathWasDefined;
};

#endif
