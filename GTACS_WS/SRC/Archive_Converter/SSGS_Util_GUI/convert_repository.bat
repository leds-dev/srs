rem
rem Modified:  D.Robinson     NOAA/SOCC   May 2013
rem Have a separate file for each domain because the
rem software picks the first occurance and easier to
rem change this file than to change and release software
rem - PR000609 Make ABE work at WCDA for SSGS and GIMTACS

rem ----------------------------------------------------


set NET_REPOSITORY=\\goesnop\ABE\CnvrtRepository
