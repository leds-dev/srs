//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SsgsUtilGui.rc
//
#define SSGSUTILDLG_MINOR_VERSION       0
#define SSGSUTILDLG_RESOURCE_DEFINED    1
#define FC_RESOURCE_DEFINED             1
#define SSGSUTILDLG_MAJOR_VERSION       2
#define IDM_ABOUTBOX                    0x0010
#define IDS_ABOUTBOX                    101
#define IDI_NOAA                        200
#define IDD_ABOUTBOX                    1000
#define IDD_MAIN                        2000
#define IDC_RADIO_SGT1                  2101
#define IDC_RADIO_SGT2                  2102
#define IDC_RADIO_SGT3                  2103
#define IDC_RADIO_SGT4                  2104
#define IDC_RADIO_SGT5                  2105
#define IDC_RADIO_CGT1                  2106
#define IDC_RADIO_CGT2                  2107
#define IDC_RADIO_CGT3                  2108
//#define IDC_RADIO_CGT4                  2109
//#define IDC_RADIO_CGT5                  2110
#define IDC_RADIO_SC13                  2201
#define IDC_RADIO_SC14                  2202
#define IDC_RADIO_SC15                  2203
#define IDC_RADIO_SC16                  2204
#define IDC_RADIO_STR1                  2301
#define IDC_RADIO_STR2                  2302
#define IDC_RADIO_STR3                  2303
#define IDC_RADIO_STR4                  2304
#define IDC_RADIO_STR5                  2305
#define IDC_RADIO_STR6                  2306
#define IDC_RADIO_STR7                  2307
#define IDC_RADIO_STR8                  2308
#define IDC_TEXT_PATHFROM               2401
#define IDC_TEXT_PATHTO                 2402
#define IDC_LIST_ARCFILES               2501
#define IDC_BUTTON_SELECT               2601
#define IDC_BUTTON_BROWSE               2602
#define IDC_BUTTON_CONVERT              2603
#define IDC_BUTTON_SHOWRES              2604
#define IDC_BUTTON_SHOWLOG              2605
#define IDC_BUTTON_EXIT                 2606
#define IDD_FC_GET_PATH                 3000
#define IDB_FC_FILETYPES                3011
#define IDC_FC_TREE                     3101
#define IDC_FC_CURRENTPATH              3102
#define IDD_CONVERTING                  4000
#define IDC_START_PROGRESS              4101
#define IDC_START_LABEL                 4102
#define IDC_START_CURRENT               4103
#define IDC_START_TARGET                4104
#define IDC_FILES_PROGRESS              4201
#define IDC_FILES_LABEL                 4202
#define IDC_FILES_CURRENT               4203
#define IDC_FILES_TARGET                4204
#define IDC_SIZES_PROGRESS              4301
#define IDC_SIZES_LABEL                 4302
#define IDC_SIZES_CURRENT               4303
#define IDC_SIZES_TARGET                4304
#define IDC_TIMES_PROGRESS              4401
#define IDC_TIMES_LABEL                 4402
#define IDC_TIMES_CURRENT               4403
#define IDC_TIMES_TARGET                4404
#define IDD_OVERWRITING                 5000
#define IDC_OVERWRITE_DESC1             5101
#define IDC_OVERWRITE_DESC2             5102
#define IDC_OVERWRITE_DESC3             5103
#define IDC_OVERWRITE_DESC4             5104
#define IDC_OVERWRITE_DESC5             5105
#define IDC_OVERWRITE_DESC6             5106
#define IDC_BUTTON_OVERWRITE_ALL        5201
#define IDC_BUTTON_OVERWRITE_NONE       5202
#define IDD_FILEVIEW                    6000
#define IDC_TEXT_FILECONTENTS           6001

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        202
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1603
#define _APS_NEXT_SYMED_VALUE           201
#endif
#endif
