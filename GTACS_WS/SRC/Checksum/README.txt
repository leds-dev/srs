CHECKSUM VERIFICATION FOR NT FILES

- Files included

	check_files.exe		: checksum program
	gtacs.xp.lis.txt        : filenames used to generate checksum reference file
	gtacs.xp.cfg.txt        : checksum reference file generated at time of release
	gtacs.xp.report.txt     : checksum report file. Output after compare operation
	gtacs.xp.generate.bat      : script to generate checksums (creates .xp.cfg.txt)
	gtacs.xp.compare.bat       : script to compare checksums  (creates .xp.report.txt)

- Execute compare.bat to verify that files have not been changed since release 
  
  Note: Files must be installed into correct locations for checksum to work
        because it uses full filenames (including pathname).



