/*
 ******************************************************************************
 *	Integral Systems Inc.
 *	Copyright (c) 2001, all rights reserved.
 ******************************************************************************
 *
 *	Source	: check_files.c
 *
 *      Date        Author            Comment
 *     -------  ---------------   -----------------------------------------------
 *     10MAY01  Erik Larson       Initial creation, adapted from mrss_list
 *     06JUN01  Chris Lauderdale  Rewritten, new input mode added
 *     08AUG01	Chris Lauderdale  Dekludged, added relative mode
 *     27SEP06	Fred Shaw		  Ported to Visual Studio 2005
 *	   18Feb13	Fred Shaw		  Ported to Visual Studio 2010
 *     10Apr13  Fred Shaw         Extensive changes due to porting
 *	                              Can not be ported to Unix - removed Unix if blocks to clean up code.
 *
 ******************************************************************************
 *
 * Verifies the current software configuration.
 *
 * Command line arguments:
 *   Help:
 *    check_files {-h | -? | /? | --help}
 *
 *   Verification (default):
 *    check_files [-r] [{-q | -v}] [-y] [-p[q]] [-w]
 *     [{-o | -a} <file>] <input-file>
 *       -p   Print a report of incorrect/missing files at the end
 *       -pq  Print a report only to the secondary output file
 *       -r   (Unnecessary) Place check_files in verification mode
 *
 *   Get checksum:
 *    check_files -s <input-file>
 *       -s   Instructs check_files to get a checksum
 *
 *   List-file:
 *    check_files -l [{-q | -v }] [-y] [-w] [{-o | -a} <file>]
 *               [{-c <dir> | -c.}] <input-file>
 *       -l   Place check_files in list-file mode
 *       -c   All paths should be taken relative to <dir>
 *       -c.  All paths should be taken relative to .
 *
 *   Both modes:
 *     [-y] [-w] [{-q | -v}] [{-o | -a} <file>] [--]
 *     -a   Create and/or append a secondary output file
 *     -o   Create and/or overwrite a secondary output file
 *     -q   Quiet mode (don't say much)
 *     -v   Verbose mode (talk about everything)
 *     -w   Show all warnings on-screen
 *     -y   Assume the answer will be "yes"
 *     --   End arguments (use this for files that start with hyphens)
 *
 *******************************************************************************/

#include "check_files.h"


int main(int argc,char *argv[])
{
	int i;
	int ioMode=0; /*Read*/
	int rPassed=0;

	/* They have to give at least a cfg file name. */
	if(argc<2)
		errorExit(1,"Error: Empty command line.\n%s",helpStr);

	getCWD(passed.originalDirectory,maxPathSize-1);
	
	/* Read command-line arguments */
	for(i=1;i<argc;i++)
	{	/* Scan through the arguments */
		if(argv[i][0]!='-' || striequal(argv[i],"--"))
			break;

		if(striequal(argv[i],"--help") || striequal(argv[i],"-h")
		   || striequal(argv[i],"-?")  || striequal(argv[i],"/?"))
			errorExit(0,helpStr);

		if(striequal(argv[i],"-pq"))
		{
			if(passed.dump!=oNo)
				errorExit(1,"Error: Specify either %s or %s.\n","-p","-pq");
			passed.dump=oLo;
			continue;
		}

		if(striequal(argv[i],"-c."))
		{
			if(passed.relativeMode)
				errorExit(1,"Error: Specify either %s or %s.\n","-c.","-c <dir>");
			passed.relativeMode=true;
			strcpy_s( passed.relDirectory, maxPathSize, passed.originalDirectory);
			continue;
		}

		/* Anything that's left has to be 2 characters */
		if(strlen(argv[i])!=2)
			errorExit(1,"Error: Invalid switch \"%s\".  Use -h for help.\n",argv[i]);

		if(argv[i][0]!='-' && i<argc-1)
			errorExit(1,"Error: Invalid command-line format.\n%s",helpStr);

		switch(argv[i][1])
		{
		case 'a':case 'A':
			if(++i>=argc-1)
				errorExit(1,"Error: Missing filename for -%c.\n",'a');
			if(passed.second!=soNone)
				errorExit(1,"Error: Specify either %s or %s.\n","-a","-o");

			passed.second=soAppd;
			passed.output2File=argv[i];
			continue;
			
		case 'c':case 'C':
			if(++i>=argc-1)
				errorExit(1,"Error: Missing directory name for -c.  Try \"-h\".\n");
			if(passed.relativeMode)
				errorExit(1,"Error: Specify either %s or %s.\n","-c","-c.");
			
			passed.relativeMode=true;
			if(chDir(argv[i++]))
				errorExit(1,"Error: Invalid directory \"%s\" for -c.\n",argv[--i]);
			getCWD(passed.relDirectory,maxPathSize);
			if(passed.relDirectory[strlen(passed.relDirectory)-1]!=slashCh)
				strcat_s(passed.relDirectory, maxPathSize,  slashSt);
			continue;

		case 'r':case 'R':
			if(ioMode)
				errorExit(1,"Error:  Specify either %s, %s, or %s.\n","-r","-s","-l");
			++rPassed;
			continue;

		case 'q':case 'Q':
			if(passed.outputLevel!=oLo)
				errorExit(1,"Error: Specify either %s or %s.\n","-q","-v");
			passed.outputLevel=oNo;
			continue;

		case 'v':case 'V':
			if(passed.outputLevel!=oLo)
				errorExit(1,"Error: Specify either %s or %s.\n","-q","-v");
			passed.outputLevel=oHi;
			continue;

		case 'p':case 'P':
			if(passed.dump!=oNo)
				errorExit(1,"Error: Specify either %s or %s.\n","-p","-pq");
			passed.dump=oHi;
			continue;
			
		case 'y':case 'Y':
			passed.yes=true;
			continue;

		case 'o':case 'O':
			if(++i>=argc-1)
				errorExit(1,"Error: Missing filename for -%c.\n",'o');
			if(passed.second!=soNone)
				errorExit(1,"Error: Specify either %s or %s.\n","-o","-a");

			passed.second=soOver;
			passed.output2File=argv[i];
			continue;

		case 'l':case 'L':
			if(rPassed || ioMode<0)
				errorExit(1,"Error: Specify either %s, %s, or %s.\n","-l","-r","-s");
			++ioMode;
			continue;

		case 'w':case 'W':
			passed.warn=true;
			continue;

		case 's':case 'S':
			if(ioMode || rPassed)
				errorExit(1,"Error:  Can't specify -s with -r or -l.\n");
			ioMode=-1;
			continue;
		}

		errorExit(1,"Error: Invalid switch \"%s\".  Use -h for help.\n",argv[i]);
		break;
	}

	/* Make sure all the arguments are compatible */
	if(passed.relativeMode && !ioMode)
		errorExit(1,"Error: Relative references only allowed in listing-file mode (-l).\n");
	if(ioMode && passed.dump!=pNo)
		errorExit(1,"Error: Verification dumps only allowed in verification mode (-r).\n");

	/* Assign the input filename */
	if(!argv[i])
		errorExit(1,"Error: No input filename given.  Use the -h switch for help.\n");
	passed.inputFile=argv[i];

	/* Single mode's simple and fast */
	if(ioMode<0)
	{
		int fileOK;unsigned int checksum;
		
		printf("Getting checksum for \"%s\"... ",passed.inputFile);
		checksum=getChecksum(passed.inputFile,&fileOK);

		if(!fileOK)
			printf("Error\n");
		else
			printf("Got 0x%08.8X\n",checksum);
		return 0;
	}

	/* Open the secondary output file if necessary */
	if(passed.second!=soNone)
	{
		/* See if it exists */
		errno_t err;
		// modified code - fjs
		// if((err = fopen_s(&passed.secondOutput, passed.output2File,"r")) !=0)
		if((err = fopen_s(&passed.secondOutput, passed.output2File,"r")) == 0)
		{
			if (passed.secondOutput){
				fclose(passed.secondOutput);
			}
			/* If they want to use it as output, see if they want to overwrite. */
			if(!passed.yes && passed.second!=soAppd)
			{
				printf("Output file \"%s\" exists.  Overwrite? (Yes/No/Append) ",
				       passed.output2File);
				switch(getOneOf("ynaYNA"))
				{
					case 'n':case 'N':
						errorExit(0,"Cancelled.\n");
					case 'a':case 'A':
						printf("Appending.\n");passed.second=soAppd;
				}
			}
		}
		/* They want to append to something that doesn't exist--just warn them */
		else if(passed.second==soAppd)
		{
			printf("Can't append; creating output file \"%s\".\n",passed.output2File);
			passed.second=soOver;
		}

		if(passed.second == soAppd)
			err = fopen_s(&passed.secondOutput, passed.output2File, "a");
		else
			err = fopen_s(&passed.secondOutput, passed.output2File, "w");

		if(err != 0)
			errorExit(1,"Can't open output file \"%s\".\n",passed.output2File);
	}

	/* Windows NT handles this funkily. */
#ifdef __UNIX__
	(void)signal(SIGINT,ctrlchandler);
#endif

	if(ioMode)
		createConfig();
	else
		checkConfig();

#ifdef __UNIX__
	signal(SIGINT,SIG_DFL);
#endif
		
	printf("?-> Continue ? (Y/N) ");
	if((i=getOneOf("yYnN"))=='n' || i=='N')
	{
		oprintf(oLow,"  > Skipping at user request. (Line )\n");
	}

	/* Clean up */
	if(passed.second!=soNone){
		if (passed.secondOutput){
			fclose(passed.secondOutput);
		}
	}
	if(passed.haveTempFile){
		remove(passed.tempFileName);
	}

	chDir(passed.originalDirectory);
	return 0;
}

/* Utilities */
char *getToken(const char *str,int n)
{
	static char buffer[maxLineSize];
	int i,j,sj;

	for(i=j=0;i<n;i++)
	{
		/* Skip any leading space */
		for(;str[j] && str[j]<=' ';j++);
		if(!str[j]) return 0;

		/* Save this location and find the token's end */
		for(sj=j;str[j]>' ';j++)
			if(str[j]=='"')         /* Quotes can surround space chunks */
				for(;str[j] && str[j]!='"';j++); /* Find the next quote */

		if(!str[j] && i<n-1) /* They want something past the last token */
			return 0;
	}

	if(str[j]) --j;
	for(i=sj;i<=j;i++)
		buffer[i-sj]=str[i];
	buffer[i]=0;
	return buffer;
}

/* Dump an error message and exit */
void errorExit(int code,const char *format, ...)
{
	va_list args;
	va_start(args,format);
	vfprintf(stderr,format,args);
	exit(code);
}

/* Case-insensitive string-equivalence test */
int striequal(const char *str1,const char *str2)
{
	int i;
	for(i=0;str1[i] && str2[i];i++)
		if(toupper(str1[i])!=toupper(str2[i])) return 0;
	return !(str1[i] || str2[i]);
}

/* Return nonzero if ch found in str, 0 otherwise */
int scas(const char *str,char ch)
{
	for(;*str && *str!=ch;str++);
	return *str;
}

/* Read one of the characters in srchstr from stdin */
char getOneOf(const char *srchstr)
{
	int i,k;

	for(;;)
	{
		k=fgetc(stdin);
		for(i=0;srchstr[i];i++)
			if(srchstr[i]==k)
				return srchstr[i];
	}
}

/* Special 3-level dual-output function */
void oprintf(int l,const char *format, ...)
{
	va_list args;

	if(l==oWarn)
		if(passed.warn)
			l=oHigh;
		else
			l=oLow;

	switch(passed.outputLevel)
	{
		case oLow:
			if(l==oLow) break;
		case oHigh:
			if(l==oNo) break;
			va_start(args,format);
			vprintf(format,args);
	}

	if(passed.second!=soNone)
	{
		va_start(args,format);
		vfprintf(passed.secondOutput,format,args);
	}
}

/* Strip whitespace from the end of the string */
void stripWSpace(char *str)
{
	int i;
	for(i=0;str[i];i++);
	if(!i) return;
	for(--i;i && str[i]<=' ';i--) str[i]=0;
}

/* Skip whitespace at the beginning of a string */
char *passWSpace(char *str)
{
	int i;
	for(i=0;str[i] && str[i]<=' ';i++);
	if(!str[i]) return null;
	return &(str[i]);
}

/* Allocate a temporary file */
FILE *allocateTempFile(const char *mode)
{
	FILE *rv;
	errno_t err;

	/* Already have a temp file */
	if(passed.haveTempFile)
		return null;
		
	if((err = tmpnam_s(passed.tempFileName, L_tmpnam+1))!= 0)
		return null;
	
	if((err =fopen_s(&rv, passed.tempFileName,mode)) != 0)
	{
		passed.tempFileName[0]=0;
		return null;
	}
	passed.haveTempFile=true;
	return rv;
}

/* Return the start of the filename, which is one after the / of the directory,
   or null if it is a directory
*/
char *extractDirectory(char *gozin)
{
	int i;

	if(isDir(gozin)) return null;

	for(i=strlen(gozin)-1;i>=0 && gozin[i]!=slashCh;i++);
	if(i<0)
		return gozin;
	return gozin+i;
}

/* This is a const-ed version of the doojobber above for functions that need it */
const char *extractDirectoryC(const char *gozin)
{
	int i;

	if(isDir(gozin)) return null;

	for(i=strlen(gozin)-1;i>=0 && gozin[i]!=slashCh;i++);
	if(i<0)
		return gozin;
	return gozin+i;
}

/* Is the passed name a directory? */
int isDir(const char *name)
{
	int i;
	/* Just do a simple chdir(); if it's successful, the directory exists */
#ifdef __UNIX__
	i=chdir(name);
	chdir(passed.originalDirectory);
	return !i;
#elif defined(__WINNT__)
	i=SetCurrentDirectory(name);
	SetCurrentDirectory(passed.originalDirectory);
	return i;
#endif
}


/* Recursively enumerate files through all subdirectories */
int enumFilesR(FILE *f,const char *directory)
{
	char *c;
	int numFiles=0;
	HANDLE hFind;
	WIN32_FIND_DATA fd;

	hFind=FindFirstFile("*.*",&fd);
	if(hFind==INVALID_HANDLE_VALUE)
		return 0;

	c=_getcwd(null,maxPathSize);

	do {
		if(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if(!strcmp(fd.cFileName,".") || !strcmp(fd.cFileName,".."))
				continue;
			SetCurrentDirectory(fd.cFileName);
			numFiles+=enumFilesR(f,"");
			SetCurrentDirectory("..");
		}
		else
		{
			strcpy_s(buffer2, maxLineSize, fd.cFileName);
			makeFullPath(buffer2,c);
			fprintf(f,"%s\n",buffer2);
			++numFiles;
		}
	} while(FindNextFile(hFind,&fd));
	FindClose(hFind);
	free(c);
	return numFiles;
}

int enumFiles(const char *spec,FILE *f)
{
	HANDLE hFind;
	WIN32_FIND_DATA fd;
	int numFiles=0;
	errno_t nError = 0;

	for(numFiles=strlen(spec)-1; numFiles>=0; numFiles--)
		if(spec[numFiles]=='\\')
			break;
	if(numFiles<0)
		strcpy_s(buffer1,(maxLineSize*2), passed.relDirectory);
	else
	{
		strcpy_s(buffer1, (maxLineSize), spec);
		buffer1[numFiles]=0;
	}

	hFind=FindFirstFile(spec,&fd);
	if(hFind==INVALID_HANDLE_VALUE)
		return 0;

	do {
		if(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			continue;

		strcpy_s(buffer2, (maxLineSize), fd.cFileName);
		nError = strcmp(buffer2,"");
		makeFullPath(buffer2,buffer1);
		fprintf(f,"%s\n",buffer2);
		++numFiles;
	} while(FindNextFile(hFind,&fd));
	FindClose(hFind);

	return numFiles;

}

/* Verification */
void checkConfig(void)
{
	unsigned int shouldBeCheck,isCheck;
	int missCount=0,badCount=0,nChecked=0;
	int fileOkay=1,curLineNm=0;
	char *tokPtr;
	FILE *configFP,*tempFP;
	errno_t err;

	/* Open the configuration file or die unhappily */
	
	if((err = fopen_s(&configFP, passed.inputFile,"r")) != 0)
	{
		oprintf(oHigh,"! Error: Can't open file \"%s\" for reading, verification aborted\n");
		return;
	}

	oprintf(oHigh,"> Checking configuration for ");printStamp(oHigh);

	if(passed.dump)
	{
		if(!(tempFP=allocateTempFile("w")))
		{
			oprintf(oHigh,"!-> Error: Can't open temporary file for dump, skipping dump\n");
			passed.dump=pNo;
		}
	}

	while(fgets(curLine,maxLineSize,configFP) && ++curLineNm)
	{
		/* Check for blank/comment; read the saved checksum if present */
		tokPtr=getToken(curLine,1);
		if(!tokPtr || tokPtr[0]=='#')
			continue;

		shouldBeCheck=(unsigned int)strtoul(tokPtr,NULL,16);

		/* Check for file pathname; if it's blank warn and continue */
		if(!getToken(curLine,2))
		{
			oprintf(oHigh,"!-> Error: Invalid line #%d in input file, skipping.\n",curLineNm);
			continue;
		}

		/* The filename is allowed to have spaces, so don't use the token function;
		   just skip and strip whitespace: */
		tokPtr=passWSpace(curLine); /* Pass whitespace at the beginning */
		for(;*tokPtr && *tokPtr>' ';tokPtr++); /* Pass the checksum */
		tokPtr=passWSpace(tokPtr); /* Pass whitespace before the filename */
		stripWSpace(tokPtr); /* Strip spaces off the end of the filename */

		/* If it's not an absolute path, give a warning */
		if(!isFullPath(tokPtr))
		{
			oprintf(passed.warn?oHigh:oLow,
			        "*-> Warning: \"%s\" isn't a full pathname on line #%d, skipping.\n",tokPtr,curLineNm);
			continue;
		}

		/* Compute the actual checksum for the file */
		oprintf(oLow,"  > Checksumming \"%s\": ",tokPtr);
		++nChecked;
		isCheck=getChecksum(tokPtr,&fileOkay);

		// Modified following line
		// if(!fileOkay) /* Missing file */
		if(fileOkay) /* Missing file */
		{
			++missCount;
			if(passed.dump)
				fprintf(tempFP,"M%s\n",tokPtr);
			oprintf(oLow,"\n*-> File missing (line #%d)",curLineNm);
		}
		else if(isCheck!=shouldBeCheck)
		{
			++badCount;
			if(passed.dump)
				fprintf(tempFP,"B%s\n",tokPtr);
			oprintf(oLow,"\n*-> Bad checksum: Should be 0x%08.8X, is 0x%08.8X (line #%d)",
					shouldBeCheck,isCheck,curLineNm);
		}
		else
			oprintf(oLow,"File OK (0x%08.8X)",shouldBeCheck);
		oprintf(oLow,"\n");
	}

	if(passed.dump) fclose(tempFP);
	fclose(configFP);
	printReport(nChecked,badCount,missCount);
}

/* List-file mode.  This is the fun one. */
void createConfig(void)
{
	FILE *inpFile,*outpFile;
	FILE *tempFile;
	char *fgetsStatus;
	char *tkPtr;
	static char curOutpFNm[maxPathSize];
	int numErrors=0,numWarnings=0;
	int curLineNumber=0;
	int i,fileOK;
	int numFiles = 0, numAllFiles = 0;
	unsigned int checksum = 0;
	errno_t err;

	/* Open input file */
	
	if((err = fopen_s(&inpFile, passed.inputFile, "r")) != 0)
	// if((err = fopen_s(&inpFile, passed.inputFile, "r")) == 0)
	{
		oprintf(oHigh,"! Error: Couldn't open input file \"%s\".\n",passed.inputFile);
		return;
	}

	/* Allocate temporary file */
	if(!(tempFile=allocateTempFile("w")))
	{
		oprintf(oHigh,"! Error: Couldn't allocate a temporary file.\n");
		fclose(inpFile);
		return;
	}
	fclose(tempFile);

	oprintf(oHigh,"> Creating configuration files from \"%s\"...\n",passed.inputFile);

	/* Find first ":file" line */
	while((fgetsStatus=fgets(curLine,maxLineSize,inpFile)) && ++curLineNumber)
	{
		tkPtr=passWSpace(curLine);
		if(!tkPtr) continue;
		if(tkPtr[0]=='#') continue;

		stripWSpace(curLine);

		/* Some arbitrary idiot-line? */
		if(tkPtr[0]!=':')
		{
			oprintf(oHigh,"!-> Error: Invalid line %d in input file (should be config-file name).\n",
					curLineNumber);
			++numErrors;
			continue;
		}

		break;
	}

	/* 've we hit the end of the file? */
	if(!fgetsStatus)
	{
		++numWarnings;
		oprintf(oWarn,"*-> Warning: Nothing to do for input file.\n");
		oprintf(oHigh,"> Complete: 0 files summed\n"
		              "> %d warning%s, %d error%s\n",numWarnings,(numWarnings==1)?"":"s",
					  numErrors,(numErrors==1)?"":"s");
		return;
	}

	for(;;)
	{
		/* Open the output file */
		tkPtr=passWSpace(tkPtr+1);
		/* - If the token was only a :, they included a space: */
		if(!tkPtr[1])
		{
			oprintf(oHigh,"!-> Error: No file name given after ':' in line %d, skipping section\n",
					curLineNumber);
			++numErrors;
			if(!skipToNextCfgFile(inpFile,&curLineNumber,&tkPtr))
				break;
			continue;
		}

		err = strcpy_s(curOutpFNm, maxPathSize, tkPtr);

		/* If they didn't auto-yes everything, check to see if the file exists: */
		// if(!passed.yes && ((err=fopen_s(&outpFile, curOutpFNm,"r")) !=0))
		if(!passed.yes && ((err=fopen_s(&outpFile, curOutpFNm,"r")) ==0))
		{
			if (outpFile != NULL){
				fclose(outpFile);
			}
			printf("?-> Overwrite configuration file \"%s\"? (Y/N) ",curOutpFNm);
			if((i=getOneOf("yYnN"))=='n' || i=='N')
			{
				oprintf(oLow,"  > Skipping \"%s\" at user request. (Line %d)\n",curOutpFNm,curLineNumber);
				if(!skipToNextCfgFile(inpFile,&curLineNumber,&tkPtr))
					break;
				continue;
			}
		}

		oprintf(oHigh,"  > Creating configuration file \"%s\"...\n",curOutpFNm);

		/* Error creating the file? */
		if((err = fopen_s(&outpFile, curOutpFNm, "w")) != 0)
		{
			++numErrors;
			oprintf(oHigh,"!-> Error: Couldn't open output file \"%s\" from line %d, skipping.\n",
					curOutpFNm,curLineNumber);
			if(!skipToNextCfgFile(inpFile,&curLineNumber,&tkPtr))
				break;
			continue;
		}

		numFiles=0;

		/* Read the spec lines: */
		while((fgetsStatus=fgets(curLine,maxLineSize,inpFile)) && ++curLineNumber)
		{
			/* Clean up the line */
			tkPtr=passWSpace(curLine);
			if(!tkPtr) continue;
			if(tkPtr[0]=='#')
			{
				if(tkPtr[1]!='#')
					fputs(curLine,outpFile);
				continue;
			}

			stripWSpace(curLine);
			if(tkPtr[0]==':')
			{
				oprintf(oHigh,"  > %d file%s summed.\n",
				        numFiles,(numFiles==1)?"":"s");
				numFiles=0;
				fclose(outpFile);
				break;
			}
			makeFullPath(tkPtr,passed.relDirectory);
			i=isDir(tkPtr);
			fileOK=(tkPtr[strlen(tkPtr)-1]==slashCh);

			/* Normal file? (No *, ?, etc.) */
			if(!i && !fileOK && !scas(tkPtr,'*') && !scas(tkPtr,'?'))
			{
				oprintf(oLow,"    > Checksumming \"%s\"... ",tkPtr);
				checksum=getChecksum(tkPtr,&fileOK);
				// if(!fileOK)
				if(fileOK)
				{
					++numErrors;
					oprintf(oLow,"\n");
					oprintf(oHigh,"!-----> Error: Couldn't sum file \"%s\" from line %d.\n",curLine,
							curLineNumber);
				}
				else
				{
					oprintf(oLow,"Got 0x%08.8X\n",checksum);
					++numFiles;++numAllFiles;
					fprintf(outpFile,"0x%08.8X\t%s\n",checksum,curLine);
				}				
				continue;
			}
			
			/* The rest of these will require a listing in a temp file. */
			if((err =fopen_s(&tempFile, passed.tempFileName, "w")) != 0)
			{
				++numErrors;
				oprintf(oHigh,"!---> Error: Couldn't open the temporary file for writing, line %d skipped.\n",
						curLineNumber);
				continue;					
			}

			/* Recursive directory listing? */
			if(i && fileOK)
			{
				adjSlash(tkPtr,0);
#ifdef __WINNT__
				if(chDir(tkPtr))
				{
					++numErrors;
					oprintf(oHigh,"!--->Error: Directory \"%s\" not found (from line %d).\n",tkPtr,
							curLineNumber);
					continue;
				}
#endif

				oprintf(oLow,"    > Obtaining recursive listing for \"%s\"...\n",tkPtr);
				i=enumFilesR(tempFile,tkPtr);

#ifdef __WINNT__
				chDir(passed.originalDirectory);
#endif
			}
			else
			{
			/* Nonrecursive listing? */
				if(i){
					err = strcat_s(tkPtr, strlen(slashSt wCard), slashSt wCard);
				}
				oprintf(oLow,"    > Obtaining listing for \"%s\"...\n",tkPtr);
				i=enumFiles(tkPtr,tempFile);
			}
			fclose(tempFile);

			/* Any files found? */
			if(!i)
			{
				++numWarnings;
				oprintf(oWarn,"*---> Warning: No files found for line %d.\n",curLineNumber);
				continue;
			}

			/* Open temp file for reading */
			if((err = fopen_s(&tempFile, passed.tempFileName, "r")) != 0)
			{
				++numErrors;
				oprintf(oHigh,"!---> Error: Couldn't open the temporary file for reading, line %d skipped.\n",
						curLineNumber);
				continue;
			}

			i=0;
			while(fgets(curLine,maxLineSize,tempFile))
			{
				if(striequal(curLine,"\n"))
					break;
				++i;
				stripWSpace(curLine);
				oprintf(oLow,"      > Checksumming \"%s\"... ",curLine);
				checksum=getChecksum(curLine,&fileOK);
				// Removed ! from fileOK
				// if (!fileOK){
				if(fileOK)
				{
					++numErrors;
					oprintf(oLow,"\n");
					oprintf(oHigh,"!-----> Error: Couldn't sum file \"%s\" from line %d.\n",curLine,
							curLineNumber);
				}
				else
				{
					oprintf(oLow,"Got 0x%08.8X\n",checksum);
					++numFiles;++numAllFiles;
					fprintf(outpFile,"0x%08.8X\t%s\n",checksum,curLine);
				}
			}
			fclose(tempFile);

			if(!i)
			{
				++numWarnings;
				oprintf(oWarn,"*---> Warning: No files found for line %d.\n",curLineNumber);
			}
		} /* while(fgets(...)) for this config file */
		
		if(!fgetsStatus)
		{
			oprintf(oHigh,"  > %d file%s summed.\n",
			        numFiles,(numFiles==1)?"":"s");
			if (outpFile != NULL){
				fclose(outpFile);
			}
			break;
		}
	} /* for(;;) */

	oprintf(oHigh,"> Total: %d file%s summed, %d warning%s, %d error%s\n",
	        numAllFiles,(numAllFiles==1)?"":"s",
	        numWarnings,(numWarnings==1)?"":"s",
	        numErrors,(numErrors==1)?"":"s");
	fclose(inpFile);
	oprintf(oHigh,"> Done with input file \"%s\".\n",passed.inputFile);
}

/* Compute the checksum for a file */
unsigned int getChecksum(const char *path, int *fileOK)
{
	unsigned int check=0;
	int fHandle =0,  i = 0;
	static unsigned char buffer[fileReadSize];
	long endPos = 0, remaining = 0, size = 0;
	// errno_t err  = 0;

	fflush(stdout);

	/* Try opening the file */
	// *fileOK = ((fHandle = _open(path, O_RDONLY))>=0);
	// err  = _sopen_s(&fHandle, path, O_RDONLY, _SH_DENYNO, _S_IREAD | _S_IWRITE );
	*fileOK  = (int)_sopen_s(&fHandle, path, O_RDONLY, _SH_DENYNO, _S_IREAD | _S_IWRITE );
	// if(err != 0){
	if(*fileOK != 0){
		return 0;
	}
	
	/* Does the file actually contain anything? */
	if((endPos=_lseek(fHandle,0,SEEK_END))>0)
	{
		/* Yup, seek to the top */
		_lseek(fHandle,0,SEEK_SET);
		remaining=endPos;

		while(remaining)
		{
			/* Read in at most fileReadSize bytes */
			size=min(remaining,fileReadSize);
			_read(fHandle, buffer, (int)size);
			remaining-=size;

			/* Sum the buffer */
			for(i=0;i<size;i++)
				check+=buffer[i];
		}
	}
	_close(fHandle);
	// *fileOK = (int)err;
	return check;
}

/* Print the report for the verification */
void printReport(int numChecked,int bad,int missed)
{
	int total;

	total=bad+missed;
	oprintf(oLow,HR);
	oprintf(oHigh," Configuration check complete for ");printStamp(oHigh);
	oprintf(oHigh," VERIFICATION %sSUCCESSFUL\n"
	              "   * %d file%s checked\n",
				  total?"UN":"",numChecked,(numChecked!=1)?"s":"");

	if(total)
	{
		if(bad)
			oprintf(oHigh,"   * %d file%s mismatched\n",bad,(bad!=1)?"s":"");
		if(missed)
			oprintf(oHigh,"   * %d file%s missing\n",missed,(missed!=1)?"s":"");
	}
	oprintf(oLow,HR);

	if(passed.dump)
	{
		int l=oHigh;
		FILE *tmpFP;
		int mp=0,bp=0;
		errno_t err;

		if(!passed.haveTempFile)
		{
			oprintf(oHigh,"   <FILE ERROR FOR DUMP>\n");
			return;
		}

		if(passed.dump==pLo) l=oLow;

		if((err = fopen_s(&tmpFP, passed.tempFileName, "r")) != 0)
		{
			oprintf(oHigh,"   <FILE ERROR FOR DUMP>\n");
			return;
		}

		while(fgets(curLine,maxLineSize,tmpFP))
			if(curLine[0]=='B')
			{
				if(!bp)
					oprintf(l,"   Mismatched files:\n",++bp);
				oprintf(l,"    %s",curLine+1);
			}
		rewind(tmpFP);

		while(fgets(curLine,maxLineSize,tmpFP))
			if(curLine[0]=='M')
			{
				if(!mp)
					oprintf(l,"  Missing files:\n",++mp);
				oprintf(l,"    %s",curLine+1);
			}
		fclose(tmpFP);
		if(mp+bp) oprintf(oLow,HR);
	}
}

/* Print the host/time stamp */
void printStamp(int level)
{
	char hostname[72]="";
	// unsigned int n=71;
	DWORD n = 72;
	time_t now;
	// wchar_t buf[26];
	char buf[26];
    errno_t err = 0;

	/*
		char buf[1024];
		DWORD dwCompNameLen = 1024;
		if (0 != GetComputerName(buf, &dwCompNameLen)) {
			printf("name %s\n", buf);
		}
	*/

	now=time(NULL);
	if(getHostName(hostname, n)){
		strcpy_s(hostname, n,  "[unknown host]");
	}

	// int err = 0;
	// _wctime_s( buf, 26, &now);
	//   errno_t ctime_s( char* buffer, size_t numberOfElements, const time_t *time);
	err = ctime_s( buf, 26, &now);

	// oprintf(level,"%s\n     %s\n",hostname?hostname:"[unknown host]", ctime_s(&now));
	oprintf(level,"%s\n     %s\n",hostname?hostname:"[unknown host]", buf);
}

int skipToNextCfgFile(FILE *input,int *curLineNm,char **tokPtr)
{
	char *c;
	while((c=fgets(curLine,maxLineSize,input)) && ++*curLineNm)
	{
		*tokPtr=getToken(curLine,1);
		if(*tokPtr && *tokPtr[0]==':')
			break;
	}
	if(!c) return 0;
	return 1;
}

void makeFullPath(char *buf,const char *from)
{
	char *c;
	char *cTemp; // added for debugging purposes - fjs
	size_t s;
	char sl=0;
	int nSize = 0;  // added for debugging purposes - fjs

	// nSize = strlen(buf);
	// buf[nSize] = {0};

	// nSize = strlen(from);
	// from[nSize] = {0};


	if(isFullPath(buf))
		return;

	if(buf[0]=='\\')
	{
		char c[3]={':',0,0};

		/* Reverse the string, concatenate :D, and reverse it again (prepends D:) */
		buf = _strrev(buf);
		c[1]=_getdrive()+'A'-1;
		strcat_s(buf, strlen(buf), c);
		buf = _strrev(buf);
		return;
	}

	/* If it doesn't start with the slash, prepend the current path to it */
	s=strlen(from)+1;
	if(passed.relDirectory[strlen(passed.relDirectory)-1]!=slashCh){
		s+=(sl=1);
	}
	c=(char *)malloc(s*sizeof(char));
	strcpy_s(c, s, from);
	if(sl){
		strcat_s(c, s, slashSt);
	}
	
	buf = _strrev(buf);
	c = _strrev(c);

	nSize = strlen(buf);
	// buf[nSize+1] ='\0';
	// buf[nSize] ='\0';
	
	// The next line crashes with error message string not NOT NULL TERMINATED
	// strcat_s(buf, strlen(buf), c); 
	// This function executes successfully
    cTemp = strcat(buf, c); 

	free(c);
	buf = _strrev(buf);
}

/* Concatenate a file name to a directory name, adding a slash as necessary */
/*
void catFileName(char *buf,const char *filename)
{
	if(buf[strlen(buf)-1]!=slashCh){
		strcat_s(buf, strlen(buf), slashSt);
	}
	strcat_s(buf,strlen(buf), filename);
}
*/


void adjSlash(char *buf,int have)
{
	if(have)
	{
		if(buf[strlen(buf)-1]!=slashCh){
			strcat_s (buf, strlen(buf), slashSt);
		}
	}
	else
	{
		int d;
		d=strlen(buf)-1;
		if(buf[d]==slashCh)
			buf[d]=0;
	}
}
