/*
 ******************************************************************************
 *	Integral Systems Inc.
 *	Copyright (c) 2001, all rights reserved.
 ******************************************************************************
 *
 *	Source	: check_files.c
 *
 *      Date        Author            Comment
 *     -------  ---------------   -----------------------------------------------
 *     10MAY01  Erik Larson       Initial creation, adapted from mrss_list
 *     06JUN01  Chris Lauderdale  Rewritten, new input mode added
 *     08AUG01	Chris Lauderdale  Dekludged, added relative mode
 *     27SEP06	Fred Shaw		  Ported to Visual Studio 2005
 *	   18Feb13	Fred Shaw		  Ported to Visual Studio 2010
 *
 ******************************************************************************
 *
 * Verifies the current software configuration.
 *
 * Command line arguments:
 *   Help:
 *    check_files {-h | -? | /? | --help}
 *
 *   Verification (default):
 *    check_files [-r] [{-q | -v}] [-y] [-p[q]] [-w]
 *     [{-o | -a} <file>] <input-file>
 *       -p   Print a report of incorrect/missing files at the end
 *       -pq  Print a report only to the secondary output file
 *       -r   (Unnecessary) Place check_files in verification mode
 *
 *   Get checksum:
 *    check_files -s <input-file>
 *       -s   Instructs check_files to get a checksum
 *
 *   List-file:
 *    check_files -l [{-q | -v }] [-y] [-w] [{-o | -a} <file>]
 *               [{-c <dir> | -c.}] <input-file>
 *       -l   Place check_files in list-file mode
 *       -c   All paths should be taken relative to <dir>
 *       -c.  All paths should be taken relative to .
 *
 *   Both modes:
 *     [-y] [-w] [{-q | -v}] [{-o | -a} <file>] [--]
 *     -a   Create and/or append a secondary output file
 *     -o   Create and/or overwrite a secondary output file
 *     -q   Quiet mode (don't say much)
 *     -v   Verbose mode (talk about everything)
 *     -w   Show all warnings on-screen
 *     -y   Assume the answer will be "yes"
 *     --   End arguments (use this for files that start with hyphens)
 *
 *******************************************************************************/

#define helpStr "Command line arguments:\n"\
                "  Help:\n"\
                "    check_files {-h | -? | /? | --help}\n"\
				"\n"\
                "  Verification (default):\n"\
				"    check_files [-r] [-p[q]] [...] <input-file>\n"\
				"      -p   Print a report of incorrect/missing files at the end\n"\
				"      -pq  Print a report only to the secondary output file\n"\
				"      -r   (Unnecessary) Place check_files in verification mode\n"\
				"\n"\
				"  Single checksum:\n"\
				"    check_files -s <input-file>\n"\
				"      -s   Instructs check_files to get a single checksum\n"\
				"\n"\
				"  List-file:\n"\
				"    check_files -l [{-c <dir> | -c.}] [...] <input-file> \n"\
				"      -l   Place check_files in list-file mode\n"\
				"      -c   All relative paths should be from <dir>\n"\
				"      -c.  All relative paths should be from .\n"\
				"\n"\
				"  [...]:\n"\
				"    [-y] [-w] [{-q | -v}] [{-o | -a} <file>] [--]\n"\
				"      -a   Create and/or append a secondary output file\n"\
				"      -o   Create and/or overwrite a secondary output file\n"\
				"      -q   Quiet mode (don't say much)\n"\
				"      -v   Verbose mode (talk about everything)\n"\
				"      -w   Show all warnings on-screen\n"\
				"      -y   Assume the answer will be \"yes\"\n"\
				"      --   End of arguments (e.g. check_files -- -hyphenated-file)\n"\
 
#if defined(WIN32)
#define __WINNT__
#define _UNICODE
#elif defined(__unix)
#define __UNIX__
#define _POSIX_THREAD_SEMANTICS
#endif

#if !defined(__WINNT__) && !defined(__UNIX__)
#error ** You must compile this on either Windows NT or Solaris! **
#endif

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>
#include <time.h>
#include <signal.h>
#include <share.h>
#include <io.h>
#include <tchar.h>
#include <io.h>
#include <windows.h>
#include <direct.h>
#include <sys/types.h>
#include <sys/stat.h>


#ifdef __DEBUG__
 #define dbgDump(str,var) printf(str,var)
#else
 #define dbgDump(str,var) ((void)0)
#endif

/* fileReadSize is the amount read at once from a file that's being summed. 
   Kick this up to a higher multiple of 512 to increase speed slightly.*/
#define fileReadSize 4096
#define maxLineSize 1024
#define whiteSpace(a) ((a)==' ' || (a)=='\t' || (a)=='\n')
#define oHi 2			/* High output level*/
#define oHigh 2
#define oLo 1			/* Low output level*/
#define oLow 1
#define oNo 0		    /* Silent output level (China)*/
#define oWarn -1		/* Warning output level */
#define pHi 2			/* Normal dump */
#define pLo 1           /* Quiet dump */
#define pNo 0           /* No dump */
#define soNone 0		/* No second output */
#define soOver 1		/* Overwrite second output */
#define soAppd 2		/* Append second output */
#define maxPathSize 1024
#define null 0

#ifndef min
 #define min(a,b) (((a)<(b))?(a):(b))
 #define max(a,b) (((a)>(b))?(a):(b))
#endif

#define HR "   ---------------------\n"
#define getCWD(a,n) _getcwd(a,n)
#define chDir(a) (!SetCurrentDirectory(a))
#define getHostName(a,b) (!GetComputerName(a,&b))
#define slashCh '\\'
#define slashSt "\\"
#define wCard "*.*"
#define isFullPath(x) (((x)[0] && (x)[1]==':') || ((x)[0]=='\\' && (x)[1]=='\\'))
#define statfunc _stat
#define statstruc _stat

#ifndef S_IFDIR
#define S_IFDIR _S_IFDIR
#define S_IFMT _S_IFMT
#endif


#define false 0
#define true 1

/* Large pieces */
void checkConfig(void);
void createConfig(void);
unsigned int getChecksum(const char *path,int *fileOK);
void printStamp(int level);
void printReport(int numChecked,int bad,int missed);
int skipToNextCfgFile(FILE *input,int *curLineNm,char **tokPtr);

/* Handy-dandy utility functions */
char *getToken(const char *str,int token);
void errorExit(int code,const char *format, ...);
int striequal(const char *str1, const char *str2);
int scas(const char *str,char ch);
char getOneOf(const char *srchstr);
void oprintf(int l,const char *format, ...);
void stripWSpace(char *str);
char *passWSpace(char *str);
FILE *allocateTempFile(const char *mode);
char *extractDirectory(char *gozin);
const char *extractDirectoryC(const char *gozin);
int isDir(const char *name);
void makeFullPath(char *buf,const char *from);
// void catFileName(char *buf,const char *filename);
void adjSlash(char *buf,int have);

/* File-finding stuff */
int enumFilesR(FILE *f,const char *dir);
int enumFiles(const char *spec,FILE *f);

/* We've got buffers... */
char curLine[maxLineSize] = {'\0'};
char buffer1[maxLineSize*2] = {'\0'};
char buffer2[maxLineSize] = {'\0'};

/* A nice global argument structure so we don't have to pass ten args
   to the big-piece functions */
static struct __passed_tag
{
	FILE *secondOutput;      /* Extra output file */
	char outputLevel;        /* Output level? Loud, normal, silent */
	char relativeMode;       /* Relative to the current path */
	char yes;                /* Auto-yes everything */
	char second;             /* Append to the extra output file */
	char dump;               /* Dump in the report */
	char warn;               /* Warn about warny things */
	char haveTempFile;       /* A temp file has been allocated */
	char tempFileName[L_tmpnam+1];
	char relDirectory[maxPathSize];
	char originalDirectory[maxPathSize];
	char *inputFile;
	char *output2File;
} passed={null,   /* No extra output file */
          oLow,    /* Normal output level */
          false,  /* Absolute mode */
          false,  /* Ask about everything */
          soNone, /* No second output */
          pNo,    /* No dump */
          false,  /* Don't warn */
          false,  /* No temp allocated yet */
          "","","",0,0};

