echo OFF

REM
REM  compare checksum generated in file ssgs.w7.cfg.
REM and write output to file ssgs.w7.report
REM

rem cd c:\Program Files\Integral Systems\checksumNT

checksum -v -p -o ssgs.w7.report.txt ssgs.w7.cfg.txt 

notepad ssgs.w7.report.txt


