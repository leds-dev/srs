; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CQuitDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "dirmapper.h"
LastPage=0

ClassCount=9
Class1=CDirMapperApp
Class2=CAboutDlg
Class3=CDirMapperDlg
Class4=CMapDialog
Class5=COptionsDialog
Class6=CQuitDlg
Class7=CServiceActiveDlg
Class8=CStopDlg

ResourceCount=9
Resource1=IDD_DIRMAPPER_DIALOG
Resource2=IDD_CREDITS_DIALOG
Resource3=IDD_STOP_DIALOG
Resource4=IDD_OPTIONS_DIALOG
Resource5=IDD_MAP_DIALOG
Resource6=IDD_SERVICEACTIVE_DIALOG
Resource7=IDD_QUIT_DIALOG
Resource8=IDD_ABOUTBOX
Class9=CTimeDlg
Resource9=IDR_TRAY_POPUP_MENU

[CLS:CDirMapperApp]
Type=0
BaseClass=CWinApp
HeaderFile=DirMapper.h
ImplementationFile=DirMapper.cpp
LastObject=ID_APP_EXIT
Filter=N
VirtualFilter=AC

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=DirMapperDlg.cpp
ImplementationFile=DirMapperDlg.cpp
LastObject=CAboutDlg

[CLS:CDirMapperDlg]
Type=0
BaseClass=CResizableDialog
HeaderFile=DirMapperDlg.h
ImplementationFile=DirMapperDlg.cpp
LastObject=CDirMapperDlg

[CLS:CMapDialog]
Type=0
BaseClass=CDialog
HeaderFile=MapDialog.h
ImplementationFile=MapDialog.cpp

[CLS:COptionsDialog]
Type=0
BaseClass=CDialog
HeaderFile=OptionsDialog.h
ImplementationFile=OptionsDialog.cpp

[CLS:CQuitDlg]
Type=0
BaseClass=CDialog
HeaderFile=QuitDlg.h
ImplementationFile=QuitDlg.cpp
Filter=D
VirtualFilter=dWC
LastObject=IDC_QUIT_CHECK

[CLS:CServiceActiveDlg]
Type=0
BaseClass=CDialog
HeaderFile=ServiceActiveDlg.h
ImplementationFile=ServiceActiveDlg.cpp

[CLS:CStopDlg]
Type=0
BaseClass=CDialog
HeaderFile=StopDlg.h
ImplementationFile=StopDlg.cpp

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=5
Control1=IDC_STATIC,static,1342177283
Control2=IDC_VERSION_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Control5=ID_CREDIT,button,1342373888

[DLG:IDD_DIRMAPPER_DIALOG]
Type=1
Class=CDirMapperDlg
ControlCount=6
Control1=IDC_SERVICE_LIST,SysListView32,1350631445
Control2=IDC_MAP_EDIT,edit,1350633600
Control3=IDC_STATIC,static,1342308352
Control4=IDC_OL_STATIC,static,1073741831
Control5=IDC_EVENT_LIST,SysListView32,1350631429
Control6=IDC_REFRESH_STATIC,static,1342308354

[DLG:IDD_MAP_DIALOG]
Type=1
Class=CMapDialog
ControlCount=12
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_SOCCGTACS_RADIO,button,1342311977
Control4=IDC_WALLOPSGTACS_RADIO,button,1342180905
Control5=IDC_GODDARDGTACS_RADIO,button,1342180905
Control6=IDC_UNMAP_RADIO,button,1342180905
Control7=IDC_SOCCGTACS_COMBO,combobox,1344339971
Control8=IDC_WALLOPSGTACS_COMBO,combobox,1344339971
Control9=IDC_GODDARDGTACS_COMBO,combobox,1344339971
Control10=IDC_MAP_EDIT,edit,1350568064
Control11=IDC_STATIC,button,1342177287
Control12=IDC_STATIC,button,1342177287

[DLG:IDD_OPTIONS_DIALOG]
Type=1
Class=COptionsDialog
ControlCount=7
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,button,1342177287
Control4=IDC_UPDATE_CHECK,button,1342242819
Control5=IDC_UPDATE_EDIT,edit,1350631552
Control6=IDC_UPDATE_SPIN,msctls_updown32,1342177334
Control7=IDC_UPDATE_STATIC,static,1342308864

[DLG:IDD_QUIT_DIALOG]
Type=1
Class=CQuitDlg
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1350569998
Control4=IDC_QUIT_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_QUIT_CHECK,button,1342242819

[DLG:IDD_SERVICEACTIVE_DIALOG]
Type=1
Class=CServiceActiveDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDC_ERROR_STATIC,static,1342177795
Control3=IDC_ERROR_LABEL,static,1342308352

[DLG:IDD_STOP_DIALOG]
Type=1
Class=CStopDlg
ControlCount=4
Control1=IDOK,button,1342242816
Control2=IDCANCEL,button,1342242817
Control3=IDC_STOP_STATIC,static,1342177795
Control4=IDC_STOP_LABEL,static,1342308352

[DLG:IDD_CREDITS_DIALOG]
Type=1
Class=?
ControlCount=1
Control1=IDC_CREDITS_STATIC,static,1350696960

[MNU:IDR_TRAY_POPUP_MENU]
Type=1
Class=?
Command1=ID_APP_ABOUT
Command2=ID_TRAY_MAXIMIZE
Command3=ID_TRAY_MINIMIZE
Command4=ID_APP_EXIT
CommandCount=4

[CLS:CTimeDlg]
Type=0
HeaderFile=TimeDlg.h
ImplementationFile=TimeDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_UPDATETIME_BUTTON
VirtualFilter=dWC

