/**
/////////////////////////////////////////////////////////////////////////////
// DirMapper.cpp : implementation of the DirMapper class.                  //
// (c) 2001 Don Sanders                                                    //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// DirMapper.cpp : Defines the class behaviors for the application.
//
/////////////////////////////////////////////////////////////////////////////
// This class is used to map a network dirve. 
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "DirMapper.h"
#include "DirMapperDlg.h"
#include "OXInstanceManager.h"
#include "OXRegistryItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDirMapperApp

BEGIN_MESSAGE_MAP(CDirMapperApp, CWinApp)
	//{{AFX_MSG_MAP(CDirMapperApp)
	ON_COMMAND(ID_APP_EXIT, OnAppExit)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDirMapperApp construction

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperApp::CDirMapperApp
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDirMapperApp::CDirMapperApp()
{
	m_bOkToExit = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CDirMapperApp object

CDirMapperApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CDirMapperApp initialization

COXInstanceManager instanceManager(_T("DirMapper"));

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CDirMapperApp::InitInstance()
//
//  Description :   Override InitInstance to initialize each new instance
//					of your application running under Windows.. 
//                  
//  Return :        BOOL	-	Nonzero if initialization is successful; 
//								otherwise 0.
//						
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
BOOL CDirMapperApp::InitInstance()
{
	// Allow only one copy of the software to run at a time
	if (instanceManager.CheckMaxAllowedInstances())
		return FALSE;

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.
/*
	Removed not needed for Microsoft Visual Studio 2005 
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
*/
	SetRegistryKey(strRegCompanyKey);
	LoadStdProfileSettings();  // Load standard INI file options (including MRU)
	SetRegistryBase (_T("Settings"));

	CDirMapperDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// ::BCGCBCleanUp ();
	::BCGCBProCleanUp ();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CDirMapperApp::OnAppExit()
//
//  Description :   This routine is called to exit this instance of 
//					the application.
//                  
//  Return :
//						
//  Parameters : 
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
void CDirMapperApp::OnAppExit() 
{
	m_bOkToExit = TRUE;
	AfxGetMainWnd()->SendMessage(WM_CLOSE);
}
