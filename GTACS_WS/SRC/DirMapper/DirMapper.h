// DirMapper.h : main header file for the DIRMAPPER application
//

#if !defined(AFX_DIRMAPPER_H__8A678895_0F33_11D5_9A65_0003472193C8__INCLUDED_)
#define AFX_DIRMAPPER_H__8A678895_0F33_11D5_9A65_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDirMapperApp:
// See DirMapper.cpp for the implementation of this class
//

/*
class CDirMapperApp : public CWinApp,
					  public CBCGWorkspace
*/

class CDirMapperApp : public CWinApp,
					  public CBCGPWorkspace
{
public:
	CDirMapperApp();
	BOOL	m_bOkToExit;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDirMapperApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDirMapperApp)
	afx_msg void OnAppExit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIRMAPPER_H__8A678895_0F33_11D5_9A65_0003472193C8__INCLUDED_)
