/**
/////////////////////////////////////////////////////////////////////////////
// DirMapperDlg.cpp : implementation of the DirMapperDlg class.            //
// (c) 2001 Don Sanders                                                    //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// DirMapperDlg.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is the interface for mapping a network dirve.
//
/////////////////////////////////////////////////////////////////////////////
**/
#include <shlwapi.h>

#include "StdAfx.h"
#include "DirMapper.h"
#include "DirMapperDlg.h"
#include "MapDialog.h"
#include "OptionsDialog.h"
#include "StopDlg.h"
#include "ServiceActiveDlg.h"
// #include "..\..\general\Credits.h"
#include "Credits.h"
#include "QuitDlg.h"
#include "OXRegistryItem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define ID_FIRST_OL_BUTTON		101
#define ID_REFRESH_OL_BUTTON	ID_FIRST_OL_BUTTON
#define ID_STOP_OL_BUTTON		(ID_REFRESH_OL_BUTTON+1)
#define ID_REMAP_OL_BUTTON		(ID_STOP_OL_BUTTON+1)
#define ID_OPTIONS_OL_BUTTON	(ID_REMAP_OL_BUTTON+1)
#define ID_LAST_OL_BUTTON		(ID_OPTIONS_OL_BUTTON+1)

#define	WM_ICON_NOTIFY			WM_APP+10
#define BUFSIZE					MAX_PATH

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About


class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	afx_msg void OnCredit();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	CString GetFileVersion (CString strFilePath);
};

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
//{{AFX_MSG_MAP(CAboutDlg)
ON_BN_CLICKED(ID_CREDIT, OnCredit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CAboutDlg::CAboutDlg
//	Description :
//	Return :		constructor
//	Parameters :
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CAboutDlg::DoDataExchange
//	Description :	This routine is called by the frameworks to
//					exchange data with the dialog.
//	Return :		void
//	Parameters :
//			CDataExchange* pDX	-	Pointer to the Data Exchange
//									Control
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CAboutDlg::OnCredit
//	Description :	This routine displays the scrolling credits box.
//	Return :		void
//	Parameters :	None.
//	Note :			For more information on how the pcArrCredits
//					is formed, see the comments in the Credits.cpp
//					Credits.h files
//////////////////////////////////////////////////////////////////////
**/
void CAboutDlg::OnCredit()
{
	CCredits	dlgCredits;
	TCHAR*		pcArrCredit[] = 
	{	
		_T("BITMAP_CREDITS2\b"),    // MYBITMAP is a quoted bitmap resource
		_T(""),
		_T("GOES NOPQ Directory Mapper \n"),
		_T(""),
		_T("Copyright (c) 2011 \f"),
		_T("DOC/NOAA/NESDIS/OSPO \f"),
		_T("NSOF Bldg.\f"),
		_T("4231 Suitland Rd. \f"),
		_T("Suitland, Maryland \f"),
		_T(""),
		_T("301-817-4133 \f"),
		_T("www.ospo.noaa.gov \f"),
		_T(""),
		_T("All Rights Reserved \f"),
		_T(""),
		_T(""),
		_T(""),
		_T("Project Lead \t"),
		_T(""),
		_T("Diane Robinson \f"),
		_T(""),
		_T(""),
		_T("Programmer \t"),
		_T(""),
		_T("Manan Dalal \f"),
		_T(""),
		_T(""),
		_T("BITMAP_CREDITS2\b"),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
	};

	dlgCredits.m_nArrCnt	= 37;
	dlgCredits.m_pcArrCredit= pcArrCredit;
	dlgCredits.DoModal();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CAboutDlg::GetFileVersion
//	Description :	This routine returns the version number of a
//					specified file.
//	Return :		CString	-	file version.
//	Parameters :
//		CString strFilePath	-	filename.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CAboutDlg::GetFileVersion (CString strFilePath)
{
	CString strVersion		= _T("Unavailable");
	LPTSTR 	pcFilepath		= strFilePath.GetBuffer(strFilePath.GetLength());
	DWORD 	nVersionInfoSize= GetFileVersionInfoSize(pcFilepath, 0);
	LPVOID	pData			= malloc(nVersionInfoSize);

	if( GetFileVersionInfo(pcFilepath, 0, nVersionInfoSize, pData) )
	{
		VS_FIXEDFILEINFO* pcVersion;
		UINT	pnLen;
		VerQueryValue(pData, TEXT("\\"), (LPVOID*)&pcVersion, &pnLen);
		WORD nMajor		= HIWORD(pcVersion->dwFileVersionMS);
		WORD nMinor		= LOWORD(pcVersion->dwFileVersionMS);
		WORD nSpecific	= HIWORD(pcVersion->dwFileVersionLS);
		WORD nCustom	= LOWORD(pcVersion->dwFileVersionLS);
		strVersion.Format (_T("Version %d.%d.%d.%d"), nMajor, nMinor, nSpecific, nCustom);
	}

	free (pData);
	return strVersion;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CAboutDlg::OnInitDialog
//  Description :	This routine create the about dialog window.
//	Return :		BOOL	-	TRUE if successful.
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString strVersion;
	strVersion.Format(_T("DirMapper %s"), GetFileVersion(_T("DirMapper.exe")));
	GetDlgItem(IDC_VERSION_STATIC)->SetWindowText(strVersion);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnAppAbout
//	Description :	This routine is called whenever the Help About
//					dialog box needs to be displayed.
//	Return :		void	-
//	Parameters :
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnAppAbout()
{
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::CDirMapperDlg
//	Description :	Constructor
//	Return :		constructor	-
//	Parameters :	CWnd* pParent	- Parent Window
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDirMapperDlg::CDirMapperDlg(CWnd* pParent /*=NULL*/)
	: CResizableDialog(CDirMapperDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDirMapperDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::~CDirMapperDlg
//	Description :	Destructor
//	Return :		destructor	-
//	Parameters :
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDirMapperDlg::~CDirMapperDlg()
{
//	int nMax = m_cMonitorArray.GetSize() - 1;
//	for (int nIndex=nMax; nIndex!=0; nIndex--)
//	{
//		CMonitor* pMonitor = (CMonitor*) &(m_cMonitorArray.GetAt(nIndex));
//		m_cMonitorArray.RemoveAt(nIndex);
//		delete pMonitor;
//	}
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is
//					generated and maintained by the code wizard.
//  Returns :		void	-
//  Parameters :
//           CDataExchange* pDX
//  Note :
//////////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::DoDataExchange(CDataExchange* pDX)
{
	CResizableDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDirMapperDlg)
	DDX_Control(pDX, IDC_EVENT_LIST, m_cEventList);
	DDX_Control(pDX, IDC_SERVICE_LIST, m_cServiceList);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDirMapperDlg, CResizableDialog)
	//{{AFX_MSG_MAP(CDirMapperDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_COMMAND(ID_TRAY_MAXIMIZE, OnTrayMaximize)
	ON_UPDATE_COMMAND_UI(ID_TRAY_MAXIMIZE, OnUpdateTrayMaximize)
	ON_COMMAND(ID_TRAY_MINIMIZE, OnTrayMinimize)
	ON_UPDATE_COMMAND_UI(ID_TRAY_MINIMIZE, OnUpdateTrayMinimize)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_ICON_NOTIFY, OnTrayNotification)
	ON_COMMAND_RANGE(ID_FIRST_OL_BUTTON, ID_LAST_OL_BUTTON, OnShortcut)
	ON_WM_QUERYENDSESSION()
	ON_STN_CLICKED(IDC_REFRESH_STATIC, &CDirMapperDlg::OnStnClickedRefreshStatic)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDirMapperDlg message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnInitDialog
//  Description :	This routine create the DirMapper dialog window.
//	Return :		BOOL	-	TRUE if successful.
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CDirMapperDlg::OnInitDialog()
{
	CResizableDialog::OnInitDialog();

	/*
		#define strGTACSDriveLetter			_T("S:")
		#define strSCHEDDriveLetter			_T("R:")
	*/

	m_strGTACSDriveMapping = GetMapString(strGTACSDriveLetter);
	m_strSCHEDDriveMapping = GetMapString(strSCHEDDriveLetter);

	// Add "About..." menu item to system menu.
	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// Create Outlook Bar:

//////////////////////////////////////////
// Changes by BCGSoft:
	DWORD dwStyle = WS_CHILD | WS_VISIBLE | CBRS_ALIGN_LEFT;
	DWORD dwBCGStyle = 0;
	m_wndOutlookBar.Create (_T(""), this, CRect (0, 0, 100, 100), 
		AFX_IDW_TOOLBAR, dwStyle, dwBCGStyle);

	CBCGPBaseTabWnd* pWndTab = m_wndOutlookBar.GetUnderlinedWindow ();
	ASSERT_VALID (pWndTab);

	pWndTab->HideSingleTab ();

	m_wndOutlookPane.Create (&m_wndOutlookBar, dwDefaultToolbarStyle, 1);

	// Add some shortcuts to the outlook bar
	m_wndOutlookPane.AddButton (IDB_REFRESH_BITMAP,		_T("Refresh EPOCH Service List"),	ID_REFRESH_OL_BUTTON);
	m_wndOutlookPane.AddButton (IDB_STOPSERVICES_BITMAP,_T("Stop All EPOCH Services"),		ID_STOP_OL_BUTTON);
	m_wndOutlookPane.AddButton (IDB_MAP_BITMAP,			_T("Re-map the drives"),		ID_REMAP_OL_BUTTON);
	m_wndOutlookPane.AddButton (IDB_OPTIONS_BITMAP,		_T("User Options"),					ID_OPTIONS_OL_BUTTON);
	m_wndOutlookPane.EnableTextLabels (TRUE);
	m_wndOutlookBar.AddTab (&m_wndOutlookPane);

/*
	m_wndOutlookBar.Create (this, WS_CHILD | WS_VISIBLE, AFX_IDW_TOOLBAR);
    m_wndOutlookBar.SetBarStyle (CBRS_ALIGN_LEFT | CBRS_TOOLTIPS | CBRS_FLYBY |
								 CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);

	m_wndOutlookBar.SetRouteCommandsViaFrame (FALSE);
	m_wndOutlookBar.SetOwner (this);

	// Add some shortcuts to the outlook bar
	m_wndOutlookBar.AddButton (IDB_REFRESH_BITMAP,		_T("Refresh EPOCH Service List"),	ID_REFRESH_OL_BUTTON);
	m_wndOutlookBar.AddButton (IDB_STOPSERVICES_BITMAP,	_T("Stop All EPOCH Services"),		ID_STOP_OL_BUTTON);
	m_wndOutlookBar.AddButton (IDB_MAP_BITMAP,			_T("Re-map the drives"),			ID_REMAP_OL_BUTTON);
	m_wndOutlookBar.AddButton (IDB_OPTIONS_BITMAP,		_T("User Options"),					ID_OPTIONS_OL_BUTTON);

	m_wndOutlookBar.EnableTextLabels (TRUE);
	m_wndOutlookBar.EnableSplitter (FALSE);
*/
//// End of changes

	// Add the column headings for the process/services list
	m_ilStatus.Create(IDB_STATUS_BITMAP, 16, 8, RGB(255, 0, 255));
	m_cServiceList.SetImageList(&m_ilStatus, LVSIL_SMALL);
	m_cServiceList.InsertColumn(0,	_T("Name"),				LVCFMT_LEFT,	120,	0);
	m_cServiceList.InsertColumn(1,	_T("Executable"),		LVCFMT_LEFT,	75,		0);
	m_cServiceList.InsertColumn(2,	_T("Service/Process"),	LVCFMT_LEFT,	90,		0);
	m_cServiceList.InsertColumn(3,	_T("Current Status"),	LVCFMT_LEFT,	90,		0);

	// Add the column headings for the event list
	m_ilEvent.Create(IDB_EVENT_BITMAP, 16, 8, RGB(255, 0, 255));
	m_cEventList.SetImageList(&m_ilEvent, LVSIL_SMALL);
	m_cEventList.InsertColumn(0,	_T("Time"),		LVCFMT_LEFT,	125,		0);
	m_cEventList.InsertColumn(1,	_T("Event"),	LVCFMT_LEFT,	600,		0);

	// Add the anchors so that the dialog box resizes properly
	AddAnchor(IDC_MAP_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_REFRESH_STATIC, TOP_RIGHT, TOP_RIGHT);
	AddAnchor(IDC_OL_STATIC, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SERVICE_LIST, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_EVENT_LIST, TOP_LEFT, BOTTOM_RIGHT);

	ShowSizeGrip(TRUE);
	EnableSaveRestore(_T("Resizable Sheets"), _T("Main Dialog"));

	// Create the list of all of the process and services that should be monitored
	m_cMonitorArray.RemoveAll();
	CMonitor*	pMonitor;
	pMonitor = new CMonitor(_T("Stream Service"),	_T("SSAPISrvc"),	CMonitor::SERVICE);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("Database Service"), _T("EpDBSrvc"),		CMonitor::SERVICE);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("Procedure Service"),_T("EpLPSrvc"),		CMonitor::SERVICE);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("Stream Monitor"),	_T("EpSMon"),		CMonitor::SERVICE);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("Command Browser"),	_T("EpCmdBrwsr"),	CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("Limit Alarm"),		_T("EpLimAlrm"),		CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("Mnemonic Browser"), _T("EpMnemBrwsr"),		CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("STOL Processor"),	_T("EpSTOL"),		CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("Events Viewer"),	_T("EpEvents"),		CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("Display Viewer"),	_T("EpViewer"),		CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("Stream Viewer"),	_T("EpStream"),		CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	
	UpdateMapString();
	UpdateProcessList();
	RepositionOutlookBar();

	OutputToEvents(IDS_WELCOME, INFO);

	COXRegistryItem	cReg;
	cReg.SetFullRegistryItem(strRegFullOptionsKey);
	m_nUpdateInterval	= cReg.GetNumberValue(strUpdateIntervalKey, TRUE, 5);
	m_bAutoUpdate		= cReg.GetNumberValue(strAutoUpdateKey, TRUE, TRUE);
	m_nRefreshDelay		= cReg.GetNumberValue(strRefreshDelay, TRUE, 20000);

	// strRefreshDelay

	//write out the default values in case none were found.
	cReg.SetNumberValue(m_nUpdateInterval, strUpdateIntervalKey);
	cReg.SetNumberValue(m_bAutoUpdate, strAutoUpdateKey);
	cReg.SetNumberValue(m_nRefreshDelay, strRefreshDelay);

	CString strText;

	if( m_bAutoUpdate )
		strText.Format(IDS_AUTOMATIC_REFRESH, m_nUpdateInterval);
	else
		strText.Format(IDS_MANUAL_REFRESH);

	GetDlgItem(IDC_REFRESH_STATIC)->SetWindowText(strText);

	if( m_bAutoUpdate )
		m_nTimer = SetTimer(1, m_nUpdateInterval*1000, 0);

    HICON hIcon = ::LoadIcon(AfxGetResourceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));  // Icon to use

	// Add the process to the system tray, minimize the window, and remove the process from the taskbar.
	m_cSystemTray.Create(NULL, WM_ICON_NOTIFY, _T("GOES NOPQ Directory Mapper"), hIcon, IDR_TRAY_POPUP_MENU);
	//ShowWindow(SW_MINIMIZE);
	ShowWindow(SW_SHOWNORMAL);
	//m_cSystemTray.RemoveTaskbarIcon(this);
	m_cSystemTray.SetMenuDefaultItem(ID_TRAY_MAXIMIZE, FALSE);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnSysCommand
//  Description :	This routine is called when the user selects a
//					command from the Control menu, or when the user
//					selects the Maximize or the Minimize button.
//	Return :		void	-
//	Parameters :
//		UINT nID		-	the type of system command requested.
//		LPARAM lParam	-	if a Control-menu command is chosen with
//							the mouse, lParam contains the cursor
//							coordinates. The low-order word contains
//							the x coordinate, and the high-order word
//							contains the y coordinate. Otherwise this
//							parameter is not used.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if( (nID & 0xFFF0) == IDM_ABOUTBOX )
		OnAppAbout();
	else
	{
		if( (nID & 0xFFF0) == SC_MINIMIZE )
			OnTrayMinimize();
		else
			CDialog::OnSysCommand(nID, lParam);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnDestroy
//  Description :	This routine is called to inform the CWnd object
//					that it is being destroyed.
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnDestroy()
{
	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
	CSystemTray::MinimiseToTray(this);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Fred Shaw		Date : 04/18/2007		version 2.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnTrayDotification()
//  Description :	This routine is called to inform the CWnd object
//					that it is being destroyed.
//	Return :		LRESULT	-
//	Parameters :	WPARAM wParam, LPARAM lParam
//	Note :
//////////////////////////////////////////////////////////////////////
**/
LRESULT CDirMapperDlg::OnTrayNotification(WPARAM wParam, LPARAM lParam)
{
	// Delegate all the work back to the default
	// implementation in CSystemTray.
	return m_cSystemTray.OnTrayNotification(wParam, lParam);
}



// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnPaint
//  Description :	This routine is called when Windows or an
//					application makes a request to repaint a portion
//					of an application�s window.
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnPaint()
{
	if( IsIconic() )
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
		CDialog::OnPaint();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnQueryDragIcon
//  Description :	This routine is called by the system to obtain
//					the cursor to display while the user drags the
//					minimized window.
//	Return :		HCURSOR	-	the handle of an icon or cursor.
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
HCURSOR CDirMapperDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnShortcut
//  Description :
//	Return :		void	-
//	Parameters :
//		UINT uiCmdID	-
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnShortcut(UINT uiCmdID)
{
	switch( uiCmdID )
	{
		case ID_REFRESH_OL_BUTTON:
		{
			CheckDrive();
			UpdateProcessList();
			break;
		}
		case ID_STOP_OL_BUTTON:
		{
			StopOption();
			break;
		}
		case ID_REMAP_OL_BUTTON:
		{
			RemapOption();
			break;
		}
		case ID_OPTIONS_OL_BUTTON:
		{
			OptionsOption();
			break;
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::StopOption
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::StopOption()
{
	BOOL bServiceFound = FALSE; // True if and actives processes are
								// CMonitor::SERVICE or CMonitor::SERVICE_AS_PROCESS
	BOOL bProcessFound = FALSE; // True if and actives processes are
								// CMonitor::PROCESS

	UpdateProcessList();

	for( int nIndex=0; nIndex<m_cMonitorArray.GetSize(); nIndex++ )
	{
		if( m_cMonitorArray[nIndex].GetActive() )
		{
			if( m_cMonitorArray[nIndex].GetType() == CMonitor::PROCESS )
				bProcessFound = TRUE;
			else
				bServiceFound = TRUE;
		}
	}

	// Nothing running
	if( (!bProcessFound) && (!bServiceFound) )
	{
		CStopDlg dlg;
		dlg.SetMessageID(IDS_STOPNOTHING_TEXT);
		dlg.DoModal();
		OutputToEvents(IDS_STOPNOTHING_EVENT, INFO);
	}

	// Processes but no Services
	if( (bProcessFound) && (!bServiceFound) )
	{
		CStopDlg dlg;
		dlg.SetMessageID(IDS_STOPPROCESS_TEXT);

		if( dlg.DoModal() == IDOK )
			StopAllProcesses();
	}

	// Services but no Processes
	if( (!bProcessFound) && (bServiceFound) )
	{
		CStopDlg dlg;
		dlg.SetMessageID(IDS_STOPSERVICE_TEXT);

		if( dlg.DoModal() == IDOK )
			StopAllServices();
	}

	// Processes and Services
	if( (bProcessFound) && (bServiceFound) )
	{
		CStopDlg dlg;
		dlg.SetMessageID(IDS_STOPPROCESSSERVICE_TEXT);

		if( dlg.DoModal() == IDOK )
		{
			StopAllProcesses();
			StopAllServices();
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::RemapOption
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::RemapOption()
{
	BOOL	bFound = FALSE;
	CString strErrorText;
	DWORD	dwGTACSStatus, dwSCHEDStatus;

	UpdateProcessList();

	for( int nIndex=0; nIndex<m_cMonitorArray.GetSize(); nIndex++ )
	{
		if( m_cMonitorArray[nIndex].GetActive() )
			bFound = TRUE;
	}

	if( bFound )
	{
		CServiceActiveDlg dlg(CServiceActiveDlg::MAP);
		dlg.DoModal();
	}
	else
	{
		CMapDialog dlg;

		if( dlg.DoModal() == IDOK )
		{
			CWaitCursor cWaitCursor;
			// 
			// dwStatus= WNetCancelConnection2(strDriveLetter, (DWORD)0, FALSE);
			dwGTACSStatus = WNetCancelConnection2(strGTACSDriveLetter, CONNECT_UPDATE_PROFILE, TRUE );
			dwSCHEDStatus = WNetCancelConnection2(strSCHEDDriveLetter, CONNECT_UPDATE_PROFILE, TRUE );

			// m_strCurrentMapping = _T("Unmapped");
			m_strGTACSDriveMapping = _T("Unmapped");
			m_strSCHEDDriveMapping = _T("Unmapped");

			//	m_strGTACSDriveMapping = GetMapString(strGTACSDriveLetter);
		//	m_strSCHEDDriveMapping = GetMapString(strSCHEDDriveLetter);
		
			COXRegistryItem	cRegMappedDrives;
			cRegMappedDrives.SetFullRegistryItem(cRegMappedDrives.m_pszLocalMachine);
			cRegMappedDrives.SetKeyNames(_T("\\SOFTWARE\\ISI\\EpochParams\\MappedDrives"));
//			int nNumValues = cRegMappedDrives.GetNumberOfSubkeys();

			/*
				#define strGTACSDriveLetter			_T("S:")
				#define strSchedDriveLetter			_T("R:")
			*/

			cRegMappedDrives.SetStringValue(_T(""), strGTACSDriveLetter, false);
			cRegMappedDrives.SetStringValue(_T(""), strSCHEDDriveLetter, false);
			UpdateMapString();

			// CString strNewMapping = GetMapString();

			// WNetGetConnection(_T("S:"), buffer, &nMaxSize);
			
			if( dlg.GetMap() )
			{
				Sleep(m_nRefreshDelay);

				// CString strNewMapping = GetMapString();

				CString 	strGTACSShare(dlg.GetGTACSShareName());
				CString 	strSCHEDShare(dlg.GetSCHEDShareName());
				
				NETRESOURCE nr_GTACS;

				// nr.dwType		= RESOURCETYPE_ANY;
				nr_GTACS.dwType		= RESOURCETYPE_DISK;
				nr_GTACS.lpLocalName	= strGTACSDriveLetter;
				nr_GTACS.lpRemoteName = (LPWSTR)strGTACSShare.GetBuffer(256);
				// nr.lpProvider	= NULL;
				nr_GTACS.lpProvider	= _T("");
				nr_GTACS.dwDisplayType = RESOURCEDISPLAYTYPE_SHARE;
				nr_GTACS.lpComment = _T("GTACS Root Drive");

				NETRESOURCE nr_Sched;

				// nr.dwType		= RESOURCETYPE_ANY;
				nr_Sched.dwType		= RESOURCETYPE_DISK;
				nr_Sched.lpLocalName	= strSCHEDDriveLetter;
				nr_Sched.lpRemoteName = (LPWSTR)strSCHEDShare.GetBuffer(256);
				// nr.lpProvider	= NULL;
				nr_Sched.lpProvider	= _T("");
				nr_Sched.dwDisplayType = RESOURCEDISPLAYTYPE_SHARE;
				nr_Sched.lpComment = _T("GTACS Database Drive");

				// dwStatus = WNetAddConnection2(&nr, NULL, NULL, (DWORD)0)))

				BOOL  bResult = SetVolumeMountPoint (_T("P"), strSCHEDShare.GetBuffer(256));
				
				//if (((dwGTACSStatus = WNetAddConnection2(&nr_GTACS, (LPCWSTR)NULL, (LPCWSTR)NULL, CONNECT_UPDATE_PROFILE)) == NO_ERROR)
				//&& ((dwSCHEDStatus = WNetAddConnection2(&nr_Sched, (LPCWSTR)NULL, (LPCWSTR)NULL, CONNECT_UPDATE_PROFILE)) == NO_ERROR))
				// SSGS-1142: Change the bit to FALSE instead of CONNECT_UPDATE_PROFILE. This forces the users to map the drives after each login.
				if (  ((dwGTACSStatus = WNetAddConnection2(&nr_GTACS, (LPCWSTR)NULL, (LPCWSTR)NULL, FALSE)) == NO_ERROR)
				   && ((dwSCHEDStatus = WNetAddConnection2(&nr_Sched, (LPCWSTR)NULL, (LPCWSTR)NULL, FALSE)) == NO_ERROR) )
				{
					strErrorText.Format(IDS_MAP_OK, strGTACSDriveLetter, strGTACSShare);
					OutputToEvents(strErrorText, INFO);
					strErrorText.Format(IDS_MAP_OK, strSCHEDDriveLetter, strSCHEDShare);
					OutputToEvents(strErrorText, INFO);
					// TODO:: Check return value of SetStringValue
					cRegMappedDrives.SetStringValue(strGTACSShare, strGTACSDriveLetter, false);
					cRegMappedDrives.SetStringValue(strSCHEDShare, strSCHEDDriveLetter, false);
				}
				else // SSGS 1142: Enhanced Error reporting
				{
					if (dwGTACSStatus == ERROR_ACCESS_DENIED || dwGTACSStatus == ERROR_LOGON_FAILURE || dwGTACSStatus == ERROR_INVALID_PASSWORD) 
					{
						strErrorText.Format(IDS_MAP_PASS_ERROR);
						OutputToEvents(strErrorText, FATAL);
					}
					else if (dwGTACSStatus == ERROR_NO_NETWORK || dwGTACSStatus == ERROR_BUSY || dwGTACSStatus == ERROR_NO_NET_OR_BAD_PATH) 
					{
						//strErrorText = _T("Error mapping network drives. Network error. Contact System Administrator.");
						strErrorText.Format(IDS_MAP_NET_ERROR);
						OutputToEvents(strErrorText, FATAL);
					}
					else if (dwGTACSStatus == ERROR_EXTENDED_ERROR) 
					{
						// Need special processing due to extended network error. Need to call WNetGetLastError()
						DWORD dwWNetResult, dwLastError;
						CHAR szDescription[256];
						CHAR szProvider[256];

						dwWNetResult = WNetGetLastError(&dwLastError, //error code
							(LPTSTR) szDescription, //Buffer for error description
							sizeof(szDescription), //size of error buffer
							(LPTSTR) szProvider, //Buffer for provider description
							sizeof(szProvider));
						
						if(dwWNetResult != NO_ERROR) {
							strErrorText.Format(IDS_MAP_ERROR, dwLastError, szDescription);
							OutputToEvents(strErrorText, FATAL);
						}
					}
					else
					{
						//Otherwise, print the additional error information.
						LPVOID lpMsgBuf;
						// Call FormatMessage to retrieve text for this unknown error code.
						FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, dwGTACSStatus,
							MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);
						strErrorText.Format(IDS_MAP_ERROR, dwGTACSStatus, (LPCTSTR) lpMsgBuf);
						OutputToEvents(strErrorText, FATAL);
					}
				}

//				m_strCurrentMapping = GetMapString();
				m_strGTACSDriveMapping = GetMapString(strGTACSDriveLetter);
				m_strSCHEDDriveMapping = GetMapString(strSCHEDDriveLetter);

			} else {
				cRegMappedDrives.SetStringValue(_T(""), strGTACSDriveLetter, false);
				cRegMappedDrives.SetStringValue(_T(""), strSCHEDDriveLetter, false);
			}

			UpdateMapString();
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OptionsOption
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OptionsOption()
{
	COptionsDialog dlg;
	dlg.m_bUpdateCheck= m_bAutoUpdate;
	dlg.m_nUpdateEdit = m_nUpdateInterval;

	if( dlg.DoModal() == IDOK )
	{
		CString strText;

		m_nUpdateInterval	= dlg.m_nUpdateEdit;
		m_bAutoUpdate		= dlg.m_bUpdateCheck;

		if( m_bAutoUpdate )
		{
			m_nTimer = SetTimer(1, m_nUpdateInterval*1000, 0);
			strText.Format(IDS_AUTOMATIC_REFRESH, m_nUpdateInterval);
		}
		else
		{
			KillTimer(m_nTimer);
			strText.Format(IDS_MANUAL_REFRESH);
		}

		GetDlgItem(IDC_REFRESH_STATIC)->SetWindowText(strText);
		COXRegistryItem	cReg;
		cReg.SetFullRegistryItem(strRegFullOptionsKey);
		cReg.SetNumberValue(m_nUpdateInterval, strUpdateIntervalKey);
		cReg.SetNumberValue(m_bAutoUpdate, strAutoUpdateKey);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 04/06/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnQueryEndSession
//  Description :
//	Return :		BOOL	-	Nonzero if an application can be
//								conveniently shut down; otherwise 0.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CDirMapperDlg::OnQueryEndSession()
{
	OnTrayMaximize();
	OnShortcut(ID_STOP_OL_BUTTON);
	OnTrayMinimize();
	return CWnd::OnQueryEndSession();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::StopAllProcesses
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::StopAllProcesses()
{
	CWaitCursor cWait;
	CString		strError;
	int			nStatus;

	OutputToEvents(IDS_STOPPINGPROCESSES, INFO);

	for( int nIndex=0; nIndex<m_cMonitorArray.GetSize(); nIndex++ )
	{
		if( (m_cMonitorArray[nIndex].GetActive()) &&
			((m_cMonitorArray[nIndex].GetType() == CMonitor::PROCESS) ||
			((m_cMonitorArray[nIndex].GetType() == CMonitor::SERVICE_AS_PROCESS))) )
		{
			strError.Format(IDS_STOPPROCESS, m_cMonitorArray[nIndex].GetName());
			OutputToEvents(strError, INFO);

			if( nStatus	= m_cMonitorArray[nIndex].Stop() != ERROR_SUCCESS )
			{
				if( nStatus == ERROR_TIMEOUT )
				{
					strError.Format(IDS_TIMEOUT_ERROR, m_cMonitorArray[nIndex].GetName());
					OutputToEvents(strError, FATAL);
				}
				else
				{
					strError.Format(IDS_STOPPROCESS_ERROR, m_cMonitorArray[nIndex].GetName(),
						GetLastError(), GetLastErrorText());
					OutputToEvents(strError, FATAL);
				}
			}
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::StopAllServices
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::StopAllServices()
{
	SC_HANDLE	hSCM;
	CString		strError;
	int			nStatus;

	CWaitCursor	cWait;
	OutputToEvents(IDS_STOPPINGSERVICES, INFO);

	if( hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT) )
	{
		for( int nIndex=0; nIndex<m_cMonitorArray.GetSize(); nIndex++ )
		{
			if( (m_cMonitorArray[nIndex].GetActive()) &&
				(m_cMonitorArray[nIndex].GetType() == CMonitor::SERVICE) )
			{
				strError.Format(IDS_STOPSERVICE, m_cMonitorArray[nIndex].GetName());
				OutputToEvents(strError, INFO);

				if( nStatus	= m_cMonitorArray[nIndex].Stop(hSCM) != ERROR_SUCCESS )
				{
					if( nStatus == ERROR_TIMEOUT )
					{
						strError.Format(IDS_TIMEOUT_ERROR, m_cMonitorArray[nIndex].GetName());
						OutputToEvents(strError, FATAL);
					}
					else
					{
						strError.Format(IDS_STOPSERVICE_ERROR, m_cMonitorArray[nIndex].GetName(),
										GetLastError(), GetLastErrorText());
						OutputToEvents(strError, FATAL);
					}
				}
			}
		}

		CloseServiceHandle(hSCM);
		OutputToEvents(IDS_REFRESH_LIST, INFO);
		UpdateProcessList();
	}
	else
	{
		strError.Format(IDS_SCMANAGER_ERROR, GetLastError(), GetLastErrorText());
		OutputToEvents(strError, FATAL);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::UpdateMapString
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::UpdateMapString()
{
	GetDlgItem(IDC_MAP_EDIT)->SetWindowText(m_strGTACSDriveMapping);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::UpdateProcessList
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::UpdateProcessList()
{
	CProcessSnapshot cProcessSnapshot;

	cProcessSnapshot.MakeSnapshot();
	m_cServiceList.DeleteAllItems();

	for( int nIndex=0; nIndex<m_cMonitorArray.GetSize(); nIndex++ )
	{
		m_cMonitorArray[nIndex].Update(&cProcessSnapshot);
		int nPos = m_cServiceList.InsertItem(nIndex, m_cMonitorArray[nIndex].GetTitle(),
											 m_cMonitorArray[nIndex].GetActive());
		m_cServiceList.SetItemText(nPos, 1, m_cMonitorArray[nIndex].GetName());

		switch( m_cMonitorArray[nIndex].GetType() )
		{
			case CMonitor::PROCESS				: m_cServiceList.SetItemText(nPos, 2, _T("Process"));				break;
			case CMonitor::SERVICE				: m_cServiceList.SetItemText(nPos, 2, _T("Service"));				break;
			case CMonitor::SERVICE_AS_PROCESS	: m_cServiceList.SetItemText(nPos, 2, _T("Service as Process"));	break;
		}

		m_cServiceList.SetItemText(nPos, 3, m_cMonitorArray[nIndex].GetActive() ? _T("Running") : _T("Not Running"));
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnSize
//  Description :	This routine is called after the window�s size
//					has changed.
//	Return :		void	-
//	Parameters :
//		UINT nType	-	the type of resizing requested.
//		int cx		-	the new width of the client area.
//		int cy		-	the new height of the client area.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnSize(UINT nType, int cx, int cy)
{
	CResizableDialog::OnSize(nType, cx, cy);
	RepositionOutlookBar();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::RepositionOutlookBar
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::RepositionOutlookBar()
{
	if( ::IsWindow(m_wndOutlookBar.m_hWnd) )
	{
		CRect rectOL;
		GetDlgItem(IDC_OL_STATIC)->GetWindowRect(rectOL);
		ScreenToClient(rectOL);
		const int nMargin = 0;

		((CWnd&)m_wndOutlookBar).MoveWindow(
			rectOL.top + nMargin, rectOL.left + nMargin,
			rectOL.Width() - 2 * nMargin, rectOL.Height() - 2 * nMargin);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OutputToEvents
//  Description :
//	Return :		void	-
//	Parameters :
//		int nResourceID		-
//		eSeverity nSeverity	-
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OutputToEvents(int nResourceID, eSeverity nSeverity)
{
	CString	strText;
	strText.Format(nResourceID);
	OutputToEvents(strText, nSeverity);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OutputToEvents
//  Description :
//	Return :		void	-
//	Parameters :
//		CString strText		-
//		eSeverity nSeverity	-
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OutputToEvents(CString strText, eSeverity nSeverity)
{
//	static COXSound	oxWarningSound;
//	static COXSound	oxFatalSound;
	static BOOL		bFirstTime = TRUE;

	if( bFirstTime )
	{
//		oxWarningSound.Open(IDW_RINGOUT);
//		oxFatalSound.Open(IDW_GLASS);
		bFirstTime = FALSE;
	}

	int		nPos;
	CString strTime;
	CTime	curTime(CTime::GetCurrentTime());

	strTime	= curTime.FormatGmt(_T("%Y-%j-%H:%M:%S"));
	nPos	= m_cEventList.InsertItem(LVIF_IMAGE | LVIF_TEXT,
								m_cEventList.GetItemCount(), strTime,
								0, 0, nSeverity, 0);
	m_cEventList.SetItemText(nPos, 1, strText);
	m_cEventList.EnsureVisible(nPos, FALSE);
	m_cEventList.UpdateWindow();

	/*
	switch( nSeverity )
	{
		case(INFO) :	break;
		case(WARNING) :	oxWarningSound.Play(FALSE, FALSE);	break;
		case(FATAL) :	oxFatalSound.Play(FALSE, FALSE);	break;
	}
	*/
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::GetLastErrorText
//  Description :
//	Return :		CString	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CDirMapperDlg::GetLastErrorText()
{
	LPVOID lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				  FORMAT_MESSAGE_FROM_SYSTEM |
				  FORMAT_MESSAGE_IGNORE_INSERTS,
				  NULL,
				  GetLastError(),
				  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				  (LPTSTR) &lpMsgBuf,
				  0,
				  NULL);
	CString strMsg((LPTSTR)lpMsgBuf);
	strMsg.Remove(_T('\x0D'));
	strMsg.Remove(_T('\x0A'));
	LocalFree(lpMsgBuf);
	return strMsg;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnTimer
//  Description :
//	Return :		void	-
//	Parameters :
//		UINT nIDEvent	-
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnTimer(UINT nIDEvent)
{
	UpdateProcessList();
	CheckDrive();

	CResizableDialog::OnTimer(nIDEvent);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnTrayMaximize
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnTrayMaximize()
{
	static BOOL bFirstTime = TRUE;

	if( bFirstTime )
	{
		bFirstTime = FALSE;
		CenterWindow();
	}

    // CSystemTray::MaximiseFromTray(this);
	m_cSystemTray.MaximiseFromTray(this);
    m_cSystemTray.SetMenuDefaultItem(ID_TRAY_MINIMIZE, FALSE);
	UpdateProcessList();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnTrayMinimize
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnTrayMinimize()
{
    // CSystemTray::MinimiseToTray(this);
	m_cSystemTray.MinimiseToTray(this);
    m_cSystemTray.SetMenuDefaultItem(ID_TRAY_MAXIMIZE, FALSE);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnUpdateTrayMaximize
//  Description :
//	Return :		void	-
//	Parameters :
//		CCmdUI* pCmdUI		-
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnUpdateTrayMaximize(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(!IsIconic() && !IsWindowVisible());
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnUpdateTrayMinimize
//  Description :
//	Return :		void	-
//	Parameters :
//		CCmdUI* pCmdUI		-
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnUpdateTrayMinimize(CCmdUI* pCmdUI)
{
    pCmdUI->Enable(!IsIconic() && IsWindowVisible());
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::CheckDrive
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::CheckDrive()
{
	/*
		#define strGTACSDriveLetter			_T("S:")
		#define strSchedDriveLetter			_T("R:")
	*/

//	m_strGTACSDriveMapping = GetMapString(strGTACSDriveLetter);
//	m_strSCHEDDriveMapping = GetMapString(strSCHEDDriveLetter);
	
	// First check GTACS drive
	CString strNewMapping = GetMapString(strGTACSDriveLetter);
	if( strNewMapping.Compare(m_strGTACSDriveMapping) != 0 )
	{
		CString strText;
		strText.Format(_T("S: Drive has been unexpectedly mapped to: %s"), strNewMapping);
		OutputToEvents(strText, WARNING);
		m_strGTACSDriveMapping = strNewMapping;
		UpdateMapString();
		m_cSystemTray.MaximiseFromTray(AfxGetMainWnd());
		AfxMessageBox(IDS_UNEXPECT_MAP, MB_ICONEXCLAMATION | MB_OK);
	}

	strNewMapping = GetMapString(strSCHEDDriveLetter);
	if( strNewMapping.Compare(m_strSCHEDDriveMapping) != 0 )
	{
		CString strText;
		strText.Format(_T("R: Drive has been unexpectedly mapped to: %s"), strNewMapping);
		OutputToEvents(strText, WARNING);
		m_strSCHEDDriveMapping = strNewMapping;
		UpdateMapString();
		m_cSystemTray.MaximiseFromTray(AfxGetMainWnd());
		AfxMessageBox(IDS_UNEXPECT_MAP, MB_ICONEXCLAMATION | MB_OK);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::OnClose
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDirMapperDlg::OnClose()
{
	if( ((CDirMapperApp*)AfxGetApp())->m_bOkToExit )
		CResizableDialog::OnClose();
	else
	{
		COXRegistryItem	cReg;
		cReg.SetFullRegistryItem(strRegFullOptionsKey);
		BOOL bDoNotShowExitDlgAgain = cReg.GetNumberValue(strDoNotShowExitDlgAgainKey, TRUE, FALSE);
		cReg.SetNumberValue(bDoNotShowExitDlgAgain, strDoNotShowExitDlgAgainKey);

		if( !bDoNotShowExitDlgAgain )
		{
			CQuitDlg dlg;
			dlg.m_bDoNotShowExitDlgAgain = FALSE;

			if( dlg.DoModal() == IDOK )
			{
				cReg.SetNumberValue(dlg.m_bDoNotShowExitDlgAgain, strDoNotShowExitDlgAgainKey);
				OnTrayMinimize();
			}
		}
		else
			OnTrayMinimize();
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::GetMapString
//  Description :
//	Return :		CString	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CDirMapperDlg::GetMapString()
{
	ULONG	nMaxSize = 132;
	TCHAR	buffer[132];
	DWORD   status;
	buffer[0] = 0;
	CString retstatus;
	/*
		#define strGTACSDriveLetter			_T("S:")
		#define strSchedDriveLetter			_T("R:")
	*/
	status = WNetGetConnection(strGTACSDriveLetter, buffer, &nMaxSize);
		
	switch (status)
	{
	case ERROR_NOT_CONNECTED:	
		retstatus = _T("Unmapped");
		break;
	case NO_ERROR:
		retstatus = buffer;
		break;
	case ERROR_CONNECTION_UNAVAIL:
		OutputToEvents(_T("Connection Unavailable. Please Remap."), FATAL);
		m_strGTACSDriveMapping = _T("Unmapped");
		UpdateMapString();
		m_cSystemTray.MaximiseFromTray(AfxGetMainWnd());
		retstatus = _T("Unmapped");
		break;
	default:
		retstatus = _T("Unmapped");
	}
	return retstatus;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::GetMapString
//  Description :
//	Return :		CString	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CDirMapperDlg::GetMapString(TCHAR *strDrive)
{
	ULONG	nMaxSize = 132;
	TCHAR	buffer[132];
	DWORD   status;
	buffer[0] = 0;
	CString retstatus;
	/*
		#define strGTACSDriveLetter			_T("S:")
		#define strSchedDriveLetter			_T("R:")
	*/
	status = WNetGetConnection(strDrive, buffer, &nMaxSize);

	switch (status)
	{
	case ERROR_NOT_CONNECTED:	
		retstatus = _T("Unmapped");
		break;
	case NO_ERROR:
		retstatus = buffer;
		break;
	case ERROR_CONNECTION_UNAVAIL:
		OutputToEvents(_T("Connection Unavailable. Please Remap."), FATAL);
		m_strGTACSDriveMapping = _T("Unmapped");
		UpdateMapString();
		m_cSystemTray.MaximiseFromTray(AfxGetMainWnd());
		retstatus = _T("Unmapped");
		break;
	default:
		retstatus = _T("Unmapped");
	}
	return retstatus;
}

void CDirMapperDlg::OnStnClickedRefreshStatic()
{
	// TODO: Add your control notification handler code here
}
