// DirMapperDlg.h : header file
//

#if !defined(AFX_DIRMAPPERDLG_H__8A678897_0F33_11D5_9A65_0003472193C8__INCLUDED_)
#define AFX_DIRMAPPERDLG_H__8A678897_0F33_11D5_9A65_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Monitor.h"
#include "SystemTray.h"

class CDirMapperDlg : public CResizableDialog
{
// Construction
public:
	CDirMapperDlg(CWnd* pParent = NULL);	// standard constructor
	~CDirMapperDlg();
	typedef enum eSeverity {INFO=0, WARNING=1, FATAL=2};
// Dialog Data
	//{{AFX_DATA(CDirMapperDlg)
	enum { IDD = IDD_DIRMAPPER_DIALOG };
	CListCtrl		m_cEventList;
	CListCtrl		m_cServiceList;
	//}}AFX_DATA

	// CBCGOutlookBar	m_wndOutlookBar;
	CBCGPOutlookBar	m_wndOutlookBar;
	CBCGPOutlookBarPane	m_wndOutlookPane;	// Changed by BCGSoft
	// CBCGPToolBar	m_wndOutlookBar;
	CSystemTray		m_cSystemTray;

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDirMapperDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON			m_hIcon;
	CStringArray	m_strActiveProcessList;
	CDWordArray		m_dwActiveProcessList;
	CImageList		m_ilStatus;
	CImageList		m_ilEvent;
	BOOL			m_bAutoUpdate;
	int				m_nUpdateInterval; // in seconds
	int				m_nRefreshDelay;   // in seconds
	UINT			m_nTimer;
	CString			m_strCurrentMapping;
	CString			m_strGTACSDriveMapping;
	CString			m_strSCHEDDriveMapping;
	CString			m_strScript;
	CString			m_strBinEpochAcc;
	CArray			<CMonitor, CMonitor&> m_cMonitorArray;



	// Generated message map functions
	//{{AFX_MSG(CDirMapperDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnTrayMaximize();
	afx_msg void OnUpdateTrayMaximize(CCmdUI* pCmdUI);
	afx_msg void OnTrayMinimize();
	afx_msg void OnUpdateTrayMinimize(CCmdUI* pCmdUI);
	afx_msg void OnAppAbout();
	afx_msg void OnClose();
	//}}AFX_MSG
	afx_msg void OnShortcut(UINT uiCmdID);
	afx_msg BOOL OnQueryEndSession( );
	afx_msg LRESULT OnTrayNotification(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
	void	DeleteAccBin();
	void	StopOption();
	void	RemapOption();
	void	OptionsOption();
	void	UpdateProcessList();
	void	UpdateMapString();
	void	CheckDrive();
	void	RepositionOutlookBar();
	void	StopAllServices();
	void	StopAllProcesses();
	void	OutputToEvents(CString strText, eSeverity nSeverity);
	void	OutputToEvents(int nResourceID, eSeverity nSeverity);
	CString GetLastErrorText();
	CString GetMapString();
	CString GetMapString(TCHAR *strDrive);
	DWORD	FindProcess(CString strService);
	DWORD	StopService(SC_HANDLE hSCM, SC_HANDLE hService, BOOL fStopDependencies, DWORD dwTimeout);
public:
	afx_msg void OnStnClickedRefreshStatic();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIRMAPPERDLG_H__8A678897_0F33_11D5_9A65_0003472193C8__INCLUDED_)
