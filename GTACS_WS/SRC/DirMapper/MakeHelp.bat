@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by DIRMAPPER.HPJ. >"hlp\DirMapper.hm"
echo. >>"hlp\DirMapper.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\DirMapper.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\DirMapper.hm"
echo. >>"hlp\DirMapper.hm"
echo // Prompts (IDP_*) >>"hlp\DirMapper.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\DirMapper.hm"
echo. >>"hlp\DirMapper.hm"
echo // Resources (IDR_*) >>"hlp\DirMapper.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\DirMapper.hm"
echo. >>"hlp\DirMapper.hm"
echo // Dialogs (IDD_*) >>"hlp\DirMapper.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\DirMapper.hm"
echo. >>"hlp\DirMapper.hm"
echo // Frame Controls (IDW_*) >>"hlp\DirMapper.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\DirMapper.hm"
REM -- Make help for Project DIRMAPPER


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\DirMapper.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\DirMapper.hlp" goto :Error
if not exist "hlp\DirMapper.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\DirMapper.hlp" Debug
if exist Debug\nul copy "hlp\DirMapper.cnt" Debug
if exist Release\nul copy "hlp\DirMapper.hlp" Release
if exist Release\nul copy "hlp\DirMapper.cnt" Release
echo.
goto :done

:Error
echo hlp\DirMapper.hpj(1) : error: Problem encountered creating help file

:done
echo.
