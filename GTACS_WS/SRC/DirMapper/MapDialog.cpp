// MapDialog.cpp : implementation file
//

#include "stdafx.h"
#include "dirmapper.h"
#include "MapDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMapDialog dialog


CMapDialog::CMapDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMapDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMapDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CMapDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMapDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMapDialog, CDialog)
	//{{AFX_MSG_MAP(CMapDialog)
	ON_BN_CLICKED(IDC_SOCCGTACS_RADIO, OnServerRadio)
	ON_BN_CLICKED(IDC_GODDARDGTACS_RADIO, OnServerRadio)
	ON_BN_CLICKED(IDC_WALLOPSGTACS_RADIO, OnServerRadio)
	ON_BN_CLICKED(IDC_SECGTACS_RADIO, OnServerRadio)
	ON_BN_CLICKED(IDC_FAIRBANKSGTACS_RADIO, OnServerRadio)
	ON_BN_CLICKED(IDC_UNMAP_RADIO, OnServerRadio)
	ON_CBN_SELCHANGE(IDC_WALLOPSGTACS_COMBO, OnServerRadio)
	ON_CBN_SELCHANGE(IDC_SOCCGTACS_COMBO, OnServerRadio)
	ON_CBN_SELCHANGE(IDC_GODDARDGTACS_COMBO, OnServerRadio)
	ON_CBN_SELCHANGE(IDC_SECGTACS_COMBO, OnServerRadio)
	ON_CBN_SELCHANGE(IDC_FAIRBANKSGTACS_COMBO, OnServerRadio)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMapDialog message handlers

BOOL CMapDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	ULONG	nMaxSize = 132;
	TCHAR	buffer[132];
	buffer[0] = 0;
	WNetGetConnection(_T("S:"), buffer, &nMaxSize);
	if (buffer[0] == 0)
		GetDlgItem(IDC_MAP_EDIT)->SetWindowText(_T("Unmapped"));
	else
		GetDlgItem(IDC_MAP_EDIT)->SetWindowText(buffer);
	
	int nGTACS;
	for (nGTACS=nMaxGTACS[nSOCC]-1; nGTACS>=0; nGTACS--)
		((CComboBox*)GetDlgItem(IDC_SOCCGTACS_COMBO))->InsertString(0, strRegGTACSTitleDefValue[nSOCC][nGTACS]);
	((CComboBox*)GetDlgItem(IDC_SOCCGTACS_COMBO))->SetCurSel(0);
	GetDlgItem(IDC_SOCCGTACS_RADIO)->SetWindowText(strRegSiteTitleDefValue[nSOCC]);
	
	for (nGTACS=nMaxGTACS[nWallops]-1; nGTACS>=0; nGTACS--)
		((CComboBox*)GetDlgItem(IDC_WALLOPSGTACS_COMBO))->InsertString(0, strRegGTACSTitleDefValue[nWallops][nGTACS]);
	((CComboBox*)GetDlgItem(IDC_WALLOPSGTACS_COMBO))->SetCurSel(0);
	GetDlgItem(IDC_WALLOPSGTACS_RADIO)->SetWindowText(strRegSiteTitleDefValue[nWallops]);
	
	for (nGTACS=nMaxGTACS[nGoddard]-1; nGTACS>=0; nGTACS--)
		((CComboBox*)GetDlgItem(IDC_GODDARDGTACS_COMBO))->InsertString(0, strRegGTACSTitleDefValue[nGoddard][nGTACS]);
	((CComboBox*)GetDlgItem(IDC_GODDARDGTACS_COMBO))->SetCurSel(0);
	GetDlgItem(IDC_GODDARDGTACS_RADIO)->SetWindowText(strRegSiteTitleDefValue[nGoddard]);
	
	for (nGTACS=nMaxGTACS[nSEC]-1; nGTACS>=0; nGTACS--)
		((CComboBox*)GetDlgItem(IDC_SECGTACS_COMBO))->InsertString(0, strRegGTACSTitleDefValue[nSEC][nGTACS]);
	((CComboBox*)GetDlgItem(IDC_SECGTACS_COMBO))->SetCurSel(0);
	GetDlgItem(IDC_SECGTACS_RADIO)->SetWindowText(strRegSiteTitleDefValue[nSEC]);
	
	for (nGTACS=nMaxGTACS[nFairbanks]-1; nGTACS>=0; nGTACS--)
		((CComboBox*)GetDlgItem(IDC_FAIRBANKSGTACS_COMBO))->InsertString(0, strRegGTACSTitleDefValue[nFairbanks][nGTACS]);
	((CComboBox*)GetDlgItem(IDC_FAIRBANKSGTACS_COMBO))->SetCurSel(0);
	GetDlgItem(IDC_FAIRBANKSGTACS_RADIO)->SetWindowText(strRegSiteTitleDefValue[nFairbanks]);
	
	((CButton*)GetDlgItem(IDC_SOCCGTACS_RADIO))->SetCheck(TRUE);
	
	OnServerRadio();

	return TRUE;
}

void CMapDialog::OnServerRadio() 
{
	CString strHost;
	if (((CButton*)GetDlgItem(IDC_SOCCGTACS_RADIO))->GetCheck())
	{
		GetDlgItem(IDC_SOCCGTACS_COMBO)->EnableWindow(TRUE);
		GetDlgItem(IDC_WALLOPSGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_GODDARDGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_SECGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_FAIRBANKSGTACS_COMBO)->EnableWindow(FALSE);
		strHost= strRegGTACSAbbrevDefValue[nSOCC][((CComboBox*)GetDlgItem(IDC_SOCCGTACS_COMBO))->GetCurSel()];
		m_bMap = TRUE;
	}
	if (((CButton*)GetDlgItem(IDC_WALLOPSGTACS_RADIO))->GetCheck())
	{
		GetDlgItem(IDC_SOCCGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_WALLOPSGTACS_COMBO)->EnableWindow(TRUE);
		GetDlgItem(IDC_GODDARDGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_SECGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_FAIRBANKSGTACS_COMBO)->EnableWindow(FALSE);
		strHost = strRegGTACSAbbrevDefValue[nWallops][((CComboBox*)GetDlgItem(IDC_WALLOPSGTACS_COMBO))->GetCurSel()];
		m_bMap = TRUE;
	}
	if (((CButton*)GetDlgItem(IDC_GODDARDGTACS_RADIO))->GetCheck())
	{
		GetDlgItem(IDC_SOCCGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_WALLOPSGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_GODDARDGTACS_COMBO)->EnableWindow(TRUE);
		GetDlgItem(IDC_SECGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_FAIRBANKSGTACS_COMBO)->EnableWindow(FALSE);
		strHost = strRegGTACSAbbrevDefValue[nGoddard][((CComboBox*)GetDlgItem(IDC_GODDARDGTACS_COMBO))->GetCurSel()];
		m_bMap = TRUE;
	}
	if (((CButton*)GetDlgItem(IDC_SECGTACS_RADIO))->GetCheck())
	{
		GetDlgItem(IDC_SOCCGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_WALLOPSGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_GODDARDGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_SECGTACS_COMBO)->EnableWindow(TRUE);
		GetDlgItem(IDC_FAIRBANKSGTACS_COMBO)->EnableWindow(FALSE);
		strHost = strRegGTACSAbbrevDefValue[nSEC][((CComboBox*)GetDlgItem(IDC_SECGTACS_COMBO))->GetCurSel()];
		m_bMap = TRUE;
	}
	if (((CButton*)GetDlgItem(IDC_FAIRBANKSGTACS_RADIO))->GetCheck())
	{
		GetDlgItem(IDC_SOCCGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_WALLOPSGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_GODDARDGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_SECGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_FAIRBANKSGTACS_COMBO)->EnableWindow(TRUE);
		strHost = strRegGTACSAbbrevDefValue[nFairbanks][((CComboBox*)GetDlgItem(IDC_FAIRBANKSGTACS_COMBO))->GetCurSel()];
		m_bMap = TRUE;
	}
	if (((CButton*)GetDlgItem(IDC_UNMAP_RADIO))->GetCheck())
	{
		GetDlgItem(IDC_SOCCGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_WALLOPSGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_GODDARDGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_SECGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_FAIRBANKSGTACS_COMBO)->EnableWindow(FALSE);
		strHost = _T("");
		m_bMap = FALSE;
	}

	m_strGTACSShareName.Format(_T("\\\\%s\\$GTACS_ROOT"), strHost);
	m_strSchedShareName.Format(_T("\\\\%s\\$GTACS_DBASE"), strHost);
}
