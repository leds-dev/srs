#if !defined(AFX_MAPDIALOG_H__7FD60585_10D6_11D5_B82B_00C04F45001D__INCLUDED_)
#define AFX_MAPDIALOG_H__7FD60585_10D6_11D5_B82B_00C04F45001D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MapDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMapDialog dialog

// #define CDialog CBCGDialog
#define CDialog CBCGPDialog

class CMapDialog : public CDialog
{
// Construction
public:
	CMapDialog(CWnd* pParent = NULL);   // standard constructor
	CString	m_strGTACSShareName;
	CString m_strSchedShareName;
	BOOL	m_bMap;

// Dialog Data
	//{{AFX_DATA(CMapDialog)
	enum { IDD = IDD_MAP_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	CString GetGTACSShareName() {return m_strGTACSShareName;};
	CString GetSCHEDShareName() {return m_strSchedShareName;};
	BOOL	GetMap() {return m_bMap;};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMapDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMapDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnServerRadio();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPDIALOG_H__7FD60585_10D6_11D5_B82B_00C04F45001D__INCLUDED_)
