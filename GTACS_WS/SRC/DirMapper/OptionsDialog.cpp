/**
/////////////////////////////////////////////////////////////////////////////
// OptionsDialog.cpp : implementation of the OptionsDialog class.          //
// (c) 2001 Don Sanders                                                    //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// OptionsDialog.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is the interface for user options. 
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "DirMapper.h"
#include "OptionsDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptionsDialog dialog


/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COptionsDialog::COptionsDialog
//	Description :	Constructor	
//	Return :		constructor
//	Parameters :
//		CWnd* pParent	-	Parent Window
//	Note :		
//////////////////////////////////////////////////////////////////////
**/
COptionsDialog::COptionsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(COptionsDialog)
	m_bUpdateCheck = FALSE;
	m_nUpdateEdit = 30;
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COptionsDialog::DoDataExchange  
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//  Returns :		void	-
//  Parameters : 
//           CDataExchange* pDX
//  Note :
//////////////////////////////////////////////////////////////////////////
**/
void COptionsDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionsDialog)
	DDX_Control(pDX, IDC_UPDATE_SPIN, m_cUpdateSpin);
	DDX_Check(pDX, IDC_UPDATE_CHECK, m_bUpdateCheck);
	DDX_Text(pDX, IDC_UPDATE_EDIT, m_nUpdateEdit);
	DDV_MinMaxInt(pDX, m_nUpdateEdit, 5, 999);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(COptionsDialog, CDialog)
	//{{AFX_MSG_MAP(COptionsDialog)
	ON_BN_CLICKED(IDC_UPDATE_CHECK, OnUpdateCheck)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionsDialog message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COptionsDialog::OnUpdateCheck
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void COptionsDialog::OnUpdateCheck() 
{
	BOOL bEnable = ((CButton*)GetDlgItem(IDC_UPDATE_CHECK))->GetCheck();
	GetDlgItem(IDC_UPDATE_STATIC)->EnableWindow(bEnable);
	GetDlgItem(IDC_UPDATE_EDIT)->EnableWindow(bEnable);
	GetDlgItem(IDC_UPDATE_SPIN)->EnableWindow(bEnable);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COptionsDialog::OnInitDialog
//  Description :	This routine create the DirMapper dialog window.  
//	Return :		BOOL	-	TRUE if successful.
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL COptionsDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_cUpdateSpin.SetRange(5, 999);
	m_cUpdateSpin.SetPos(m_nUpdateEdit);
	
	OnUpdateCheck();
	
	return TRUE;
}
