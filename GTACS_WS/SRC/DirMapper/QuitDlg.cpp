/**
/////////////////////////////////////////////////////////////////////////////
// QuitDlg.cpp : implementation of the QuitDlg class.                      //
// (c) 2001 Don Sanders                                                    //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// QuitDlg.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is the interface for mapping a network dirve. 
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "stdafx.h"
#include "dirmapper.h"
#include "QuitDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CQuitDlg dialog

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CQuitDlg::CQuitDlg
//	Description :		
//	Return :		constructor
//	Parameters :
//		CWnd* pParent	-	Parent Window
//	Note :		
//////////////////////////////////////////////////////////////////////
**/
CQuitDlg::CQuitDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CQuitDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CQuitDlg)
	m_bDoNotShowExitDlgAgain = FALSE;
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CQuitDlg::DoDataExchange
//	Description :	This routine is called by the frameworks to
//					exchange data with the dialog.	
//	Return :		void
//	Parameters :
//			CDataExchange* pDX	-	Pointer to the Data Exchange
//									Control
//	Note :		
//////////////////////////////////////////////////////////////////////
**/
void CQuitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CQuitDlg)
	DDX_Check(pDX, IDC_QUIT_CHECK, m_bDoNotShowExitDlgAgain);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CQuitDlg, CDialog)
	//{{AFX_MSG_MAP(CQuitDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CQuitDlg message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CQuitDlg::OnInitDialog
//  Description :	This routine create the about dialog window.  
//	Return :		BOOL	-	TRUE if successful.
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CQuitDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CString strText;
	strText.Format(IDS_QUIT_MSG);
	GetDlgItem(IDC_QUIT_STATIC)->SetWindowText(strText);
	
	return TRUE;
}
