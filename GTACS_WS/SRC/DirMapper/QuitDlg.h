#if !defined(AFX_QUITDLG_H__958FFA24_3339_11D5_9A75_0003472193C8__INCLUDED_)
#define AFX_QUITDLG_H__958FFA24_3339_11D5_9A75_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// QuitDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CQuitDlg dialog

class CQuitDlg : public CDialog
{
// Construction
public:
	CQuitDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CQuitDlg)
	enum { IDD = IDD_QUIT_DIALOG };
	BOOL	m_bDoNotShowExitDlgAgain;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CQuitDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CQuitDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_QUITDLG_H__958FFA24_3339_11D5_9A75_0003472193C8__INCLUDED_)
