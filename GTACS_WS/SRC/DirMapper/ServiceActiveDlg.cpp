/**
/////////////////////////////////////////////////////////////////////////////
// MapErrorDlg.cpp : implementation of the MapErrorDlg class.            //
// (c) 2001 Don Sanders                                                    //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// MapErrorDlg.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is the interface for mapping a network dirve. 
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "DirMapper.h"
#include "ServiceActiveDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CServiceActiveDlg dialog


/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CServiceActiveDlg::CServiceActiveDlg
//	Description :		
//	Return :		constructor
//	Parameters :
//		TYPE eType		-	
//		CWnd* pParent	-	Parent Window
//	Note :		
//////////////////////////////////////////////////////////////////////
**/
CServiceActiveDlg::CServiceActiveDlg(TYPE eType /*=MAP*/, CWnd* pParent /*=NULL*/)
	: CDialog(CServiceActiveDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CServiceActiveDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_eType = eType;
}


/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CServiceActiveDlg::DoDataExchange  
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//  Returns :		void	-
//  Parameters : 
//           CDataExchange* pDX
//  Note :
//////////////////////////////////////////////////////////////////////////
**/
void CServiceActiveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CServiceActiveDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CServiceActiveDlg, CDialog)
	//{{AFX_MSG_MAP(CServiceActiveDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CServiceActiveDlg message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CServiceActiveDlg::OnInitDialog
//  Description :	This routine create the DirMapper dialog window.  
//	Return :		BOOL	-	TRUE if successful.
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CServiceActiveDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CImageList	ilIcon;
	CString		strText;
	
	switch (m_eType)
	{
		case MAP:
		{
			ilIcon.Create(IDB_MAP_BITMAP, 32, 8, RGB(255, 0, 255));
			strText.Format(IDS_MAPERROR_SERVICESRUNNING_TEXT);
			break;
		}
	}

	
	((CStatic*)GetDlgItem(IDC_ERROR_STATIC))->SetIcon(ilIcon.ExtractIcon(0));
	GetDlgItem(IDC_ERROR_LABEL)->SetWindowText(strText);
	
	return TRUE;
}
