#if !defined(AFX_SERVICEACTIVEDLG_H__2D62B997_1189_11D5_B82D_00C04F45001D__INCLUDED_)
#define AFX_SERVICEACTIVEDLG_H__2D62B997_1189_11D5_B82D_00C04F45001D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ServiceActiveDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CServiceActiveDlg dialog

class CServiceActiveDlg : public CDialog
{
// Construction
public:
	typedef enum TYPE {MAP=0};
	CServiceActiveDlg(TYPE eType = MAP, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CServiceActiveDlg)
	enum { IDD = IDD_SERVICEACTIVE_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CServiceActiveDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CServiceActiveDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	TYPE	m_eType;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SERVICEACTIVEDLG_H__2D62B997_1189_11D5_B82D_00C04F45001D__INCLUDED_)
