// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//
//  Modified May 15, 2019  D.Robinson  
//  Made changes for new GOES-IO site.  Reduced the number of Wallops GTACS to 3 (from 4)
//  and switch SWPC GTACS to be the GOESIO GTACS (since SWPC no longer has a GTACS).
//  Also changed the name of the GTACS to dgtacs01

#if !defined(AFX_STDAFX_H__8A678899_0F33_11D5_9A65_0003472193C8__INCLUDED_)
#define AFX_STDAFX_H__8A678899_0F33_11D5_9A65_0003472193C8__INCLUDED_

#define WINVER 0x0500 
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxadv.h>
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

const int nMaxSites				= 5;
const int nMaxGTACSperSite		= 5;
const int nSOCC					= 0;
const int nWallops				= 1;
const int nGoddard				= 2;
const int nSEC					= 3;
const int nFairbanks			= 4;
const int nMaxGTACS[nMaxSites]	= {5, 3, 1, 1, 1};

#define strGTACSDriveLetter			_T("S:")
#define strSCHEDDriveLetter			_T("R:")


const CString strRegSiteTitleDefValue[nMaxSites] =
{_T("SOCC"),_T("Wallops CDA"),_T("Goddard"),_T("GOES-IO"),_T("Fairbanks CDA")};
const CString strRegGTACSTitleDefValue[nMaxSites][nMaxGTACSperSite] =
{	_T("SOCC GTACS 1"),_T("SOCC GTACS 2"),_T("SOCC GTACS 3"),_T("SOCC GTACS 4"),_T("SOCC GTACS 5"),
	_T("Wallops GTACS 1"),_T("Wallops GTACS 2"),_T("Wallops GTACS 3"),_T(""),_T(""),
	_T("Goddard GTACS 1"),_T(""),_T(""),_T(""),_T(""),
	_T("GOES-IO GTACS 1"),_T(""),_T(""),_T(""),_T(""),
	_T("Fairbanks GTACS 1"),_T(""),_T(""),_T(""),_T("")};
const CString strRegGTACSAbbrevDefValue[nMaxSites][nMaxGTACSperSite] =
{	_T("SGTACS01"),_T("SGTACS02"),_T("SGTACS03"),_T("SGTACS04"),_T("SGTACS05"),
	_T("CGTACS01"),_T("CGTACS02"),_T("CGTACS03"),_T(""),_T(""),
	_T("GGTACS01"),_T(""),_T(""),_T(""),_T(""),
	_T("DGTACS01"),_T(""),_T(""),_T(""),_T(""),
	_T("FGTACS01"),_T(""),_T(""),_T(""),_T("")};

const CString strRegCompanyKey				= _T("ISI");
const CString strRegFullOptionsKey			= _T("\\Software\\ISI\\DirMapper\\Options\\");
const CString strUpdateIntervalKey			= _T("UpdateRate");
const CString strAutoUpdateKey				= _T("AutoUpdate");
const CString strRefreshDelay				= _T("RefreshDelay");
const CString strDoNotShowExitDlgAgainKey	= _T("DoNotShowExitDlgAgain");
const CString strRegFullOptKey				= _T("\\HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\MountPoints\\S\\_LabelFromReg");

#include <windows.h>
#include <tlhelp32.h>

// #include <bcgcb.h>			// BCG Control Bar
#include "BCGCBProInc.h"
#include <winsvc.h>
#include <lm.h>
#include "ResizableDialog.h"
#include "OXUNC.h"
#include "OXSound.h"
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__8A678899_0F33_11D5_9A65_0003472193C8__INCLUDED_)
