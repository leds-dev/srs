/**
/////////////////////////////////////////////////////////////////////////////
// StopDlg.cpp : implementation of the StopDlg class.                      //
// (c) 2001 Don Sanders                                                    //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// StopDlg.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is the interface for mapping a network dirve. 
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "DirMapper.h"
#include "StopDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStopDlg dialog


/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CStopDlg::CStopDlg
//	Description :		
//	Return :		constructor
//	Parameters :
//		CWnd* pParent	-	Parent Window
//	Note :		
//////////////////////////////////////////////////////////////////////
**/
CStopDlg::CStopDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CStopDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CStopDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : CStopDlg::DoDataExchange  
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//  Returns :		void	-
//  Parameters : 
//           CDataExchange* pDX
//  Note :
//////////////////////////////////////////////////////////////////////////
**/
void CStopDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStopDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CStopDlg, CDialog)
	//{{AFX_MSG_MAP(CStopDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStopDlg message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CStopDlg::OnInitDialog
//  Description :	This routine create the DirMapper dialog window.  
//	Return :		BOOL	-	TRUE if successful.
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CStopDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CImageList	ilIcon;
	CString		strText;

	ilIcon.Create(IDB_STOPSERVICES_BITMAP, 32, 8, RGB(255, 0, 255));
	((CStatic*)GetDlgItem(IDC_STOP_STATIC))->SetIcon(ilIcon.ExtractIcon(0));
	strText.Format(m_dwMessageID);
	GetDlgItem(IDC_STOP_LABEL)->SetWindowText(strText);
	GetDlgItem(IDCANCEL)->SetFocus();
	return TRUE;
}
