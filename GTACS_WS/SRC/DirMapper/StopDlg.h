#if !defined(AFX_STOPDLG_H__2D62B995_1189_11D5_B82D_00C04F45001D__INCLUDED_)
#define AFX_STOPDLG_H__2D62B995_1189_11D5_B82D_00C04F45001D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StopDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStopDlg dialog

// #define CDialog CBCGDialog
#define CDialog CBCGPDialog

class CStopDlg : public CDialog
{
// Construction
public:
	CStopDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CStopDlg)
	enum { IDD = IDD_STOP_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	void	SetMessageID(DWORD dwMessageID){m_dwMessageID = dwMessageID;};

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStopDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	DWORD	m_dwMessageID;
	// Generated message map functions
	//{{AFX_MSG(CStopDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STOPDLG_H__2D62B995_1189_11D5_B82D_00C04F45001D__INCLUDED_)
