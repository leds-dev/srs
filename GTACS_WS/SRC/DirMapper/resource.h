//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DirMapper.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_DIRMAPPER_DIALOG            102
#define IDS_REFRESH_LIST                102
#define IDS_SCMANAGER_ERROR             103
#define IDS_OPENSERVICE_ERROR           104
#define IDS_STOPSERVICE                 105
#define IDS_STOPSERVICE_ERROR           106
#define IDS_STOPPINGSERVICE             107
#define IDS_STOPPINGSERVICES            107
#define IDS_WELCOME                     108
#define IDS_TIMEOUT_ERROR               109
#define IDS_STOPSERVICE_TEXT            110
#define IDS_STOPSERVICE_TITLE           111
#define IDS_MAPERROR_SERVICESRUNNING_TEXT 112
#define IDS_STOPNOTHING_TEXT            113
#define IDS_STOPPROCESS_TEXT            114
#define IDS_STOPPROCESSSERVICE_TEXT     115
#define IDS_STOPNOTHING_EVENT           116
#define IDS_STOPPROCESS_EVENT           117
#define IDS_MANUAL_REFRESH              118
#define IDS_AUTOMATIC_REFRESH           119
#define IDS_STOPPINGPROCESSES           120
#define IDS_STOPPROCESS                 121
#define IDS_STOPPROCESS_ERROR           122
#define IDS_MAP_OK                      123
#define IDS_MAP_ERROR                   124
#define IDS_QUIT_MSG                    125
#define IDS_UNEXPECT_MAP                126
#define IDS_MAP_PASS_ERROR              127
#define IDR_MAINFRAME                   128
#define IDS_MAP_NETERROR                128
#define IDS_MAP_NET_ERROR               128
#define IDR_TRAY_POPUP_MENU             130
#define IDD_CREDITS_DIALOG              130
#define IDB_OPTIONS_BITMAP              132
#define IDB_MAP_BITMAP                  133
#define IDD_OPTIONS_DIALOG              140
#define IDD_MAP_DIALOG                  141
#define ID_CREDIT                       141
#define IDD_STOPSERVICE_DIALOG          142
#define IDD_STOP_DIALOG                 142
#define IDD_MAPERROR_DIALOG             143
#define IDD_QUIT_DIALOG                 144
#define IDD_SERVICEACTIVE_DIALOG        145
#define IDB_STATUS_BITMAP               150
#define IDW_RINGOUT                     150
#define IDB_REFRESH_BITMAP              151
#define IDW_GLASS                       153
#define IDB_STOPSERVICES_BITMAP         153
#define IDB_EVENT_BITMAP                154
#define IDB_EVENTS_BITMAP               154
#define IDB_TASKBAR_BITMAP              157
#define IDC_SERVICE_LIST                1000
#define IDC_MAP_EDIT                    1001
#define IDC_EVENT_LIST                  1002
#define IDC_CREDITS_STATIC              1002
#define IDC_OL_STATIC                   1003
#define IDC_STOP_STATIC                 1005
#define IDC_STOP_LABEL                  1006
#define IDC_MAPERROR_LABEL              1007
#define IDC_ERROR_LABEL                 1007
#define IDC_MAPERROR_STATIC             1008
#define IDC_ERROR_STATIC                1008
#define IDC_UPDATE_CHECK                1010
#define IDC_UPDATE_EDIT                 1011
#define IDC_UPDATE_SPIN                 1012
#define IDC_UPDATE_STATIC               1013
#define IDC_REFRESH_STATIC              1014
#define IDC_QUIT_STATIC                 1015
#define IDC_VERSION_STATIC              1016
#define IDC_QUIT_CHECK                  1026
#define IDC_SOCCGTACS_COMBO             1095
#define IDC_WALLOPSGTACS_COMBO          1096
#define IDC_GODDARDGTACS_COMBO          1097
#define IDC_SOCCGTACS_RADIO             1098
#define IDC_WALLOPSGTACS_RADIO          1099
#define IDC_GODDARDGTACS_RADIO          1100
#define IDC_UNMAP_RADIO                 1101
#define IDC_SECGTACS_RADIO              1102
#define IDC_FAIRBANKSGTACS_RADIO        1103
#define IDC_SECGTACS_COMBO              1104
#define IDC_FAIRBANKSGTACS_COMBO        1105
#define ID_TRAY_MAXIMIZE                32780
#define ID_TRAY_MINIMIZE                32781

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        160
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
