// EpShutdown.cpp : Defines the class behaviors for the application.
//
#include "stdafx.h"
#include "EpShutdown.h"
#include "EpShutdownDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEpShutdownApp

BEGIN_MESSAGE_MAP(CEpShutdownApp, CWinApp)
	//{{AFX_MSG_MAP(CEpShutdownApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEpShutdownApp construction

CEpShutdownApp::CEpShutdownApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CEpShutdownApp object

CEpShutdownApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CEpShutdownApp initialization

BOOL CEpShutdownApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

/*
Not needed for Visual Studio 2005 
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

*/

	CEpShutdownDlg dlg;
	m_pMainWnd = &dlg;
	
	int nResponse = dlg.DoModal();

	if( nResponse == IDOK )
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
