// EpShutdown.h : main header file for the EPSHUTDOWN application
//

#if !defined(AFX_EPSHUTDOWN_H__4ABE065F_9AF0_4F51_8F86_5E56EF2F854A__INCLUDED_)
#define AFX_EPSHUTDOWN_H__4ABE065F_9AF0_4F51_8F86_5E56EF2F854A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CEpShutdownApp:
// See EpShutdown.cpp for the implementation of this class
//

class CEpShutdownApp : public CWinApp
{
public:
	CEpShutdownApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEpShutdownApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CEpShutdownApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EPSHUTDOWN_H__4ABE065F_9AF0_4F51_8F86_5E56EF2F854A__INCLUDED_)
