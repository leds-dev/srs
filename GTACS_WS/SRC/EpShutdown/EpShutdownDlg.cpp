// EpShutdownDlg.cpp : implementation file
//
// #define _WIN32_WINNT 0x0501

#include <windows.h>
#include <winbase.h>

#include "stdafx.h"
#include "EpShutdown.h"
#include "EpShutdownDlg.h"
#include "OXRegistryItem.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEpShutdownDlg dialog

CEpShutdownDlg::CEpShutdownDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEpShutdownDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEpShutdownDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CEpShutdownDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEpShutdownDlg)
	DDX_Control(pDX, IDC_LIST1, m_cEventList);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CEpShutdownDlg, CDialog)
	//{{AFX_MSG_MAP(CEpShutdownDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEpShutdownDlg message handlers

BOOL CEpShutdownDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// m_strBinEpochAcc	= _wgetenv(_T("EPOCH_BIN"));
	size_t len;
	TCHAR *pValue;
	errno_t err = _tdupenv_s( &pValue, &len, _T("EPOCH_BIN") );
	m_strBinEpochAcc	= pValue;
	free(pValue );

	if( m_strBinEpochAcc[0] == _T('/') )
	{
		m_strBinEpochAcc.Delete(0, 1);
		m_strBinEpochAcc.Replace(_T('='), _T(':'));
	}

	m_strBinEpochAcc.Replace(_T('/'), _T('\\'));
	m_strBinEpochAcc   += _T("\\bin.epoch.acc");

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon


	// Add the column headings for the event list
	m_ilEvent.Create(IDB_EVENT_BITMAP, 16, 8, RGB(255, 0, 255));
	m_cEventList.SetImageList(&m_ilEvent, LVSIL_SMALL);
//	m_cEventList.InsertColumn(0,_T("Time"),	LVCFMT_LEFT,125,0);
//	m_cEventList.InsertColumn(1,_T("Event"),LVCFMT_LEFT,600,0);
	m_cEventList.InsertColumn(0,_T("Event"),LVCFMT_LEFT,600,0);

	// Create the list of all of the process and services that should be monitored
	m_cMonitorArray.RemoveAll();
	CMonitor*	pMonitor;
	pMonitor = new CMonitor(_T("Stream Service"),	_T("SSApiSrvc"),CMonitor::SERVICE);
//	pMonitor = new CMonitor(_T("Stream Service"),	_T("SSApiSrvc"),CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("Database Service"),	_T("EpDBSrvc"),	CMonitor::SERVICE);
//	pMonitor = new CMonitor(_T("Database Service"),	_T("EpDBSrvc"),	CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
//	pMonitor = new CMonitor(_T("Node Manager"),		_T("EpNMSrvc"),	CMonitor::SERVICE);
//	m_cMonitorArray.Add(*pMonitor);
//	delete pMonitor;
	pMonitor = new CMonitor(_T("Procedure Service"),_T("EpLPSrvc"),	CMonitor::SERVICE);
//	pMonitor = new CMonitor(_T("Procedure Service"),_T("EpLPSrvc"),	CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
//	pMonitor = new CMonitor(_T("License Service"),	_T("EpLicSrvc"),CMonitor::SERVICE);
//	m_cMonitorArray.Add(*pMonitor);
//	delete pMonitor;
	pMonitor = new CMonitor(_T("Stream Monitor"),	_T("EpSMon"),	CMonitor::SERVICE);
//	pMonitor = new CMonitor(_T("Stream Monitor"),	_T("EpSMon"),	CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
/*
	pMonitor = new CMonitor(_T("DirMapper"),		_T("DirMapper"), CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
	pMonitor = new CMonitor(_T("NuTCRACKER 4"),		_T("nutsrv4"),	CMonitor::PROCESS);
	m_cMonitorArray.Add(*pMonitor);
	delete pMonitor;
*/
	m_bDone	= FALSE;
	m_nTimer= SetTimer(1, 1500, 0);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CEpShutdownDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CEpShutdownDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CEpShutdownDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 04/06/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEpShutdownDlg::OnTimer
//  Description :
//	Return :		void	-
//	Parameters :
//		UINT nIDEvent	-
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CEpShutdownDlg::OnTimer(UINT nIDEvent)
{
	if( m_bDone )
	{
		OnOK();
		return;
	}

	// True if and actives processes are
	// CMonitor::SERVICE or CMonitor::SERVICE_AS_PROCESS
	BOOL		bServiceFound = FALSE;
	// True if and actives processes are CMonitor::PROCESS
	BOOL		bProcessFound = FALSE;
	CMonitor	monitor;
	CWaitCursor	cWait;

	UpdateProcessList();

	for( int nIndex=0; nIndex<m_cMonitorArray.GetSize(); nIndex++ )
	{
		monitor = m_cMonitorArray[nIndex];

		if( m_cMonitorArray[nIndex].GetActive() )
		{
			if( m_cMonitorArray[nIndex].GetType() == CMonitor::PROCESS )
				bProcessFound = TRUE;
			else
				bServiceFound = TRUE;
		}
	}

	// Nothing running
//	if( (!bProcessFound) && (!bServiceFound) )
//		OutputToEvents(IDS_STOPNOTHING_EVENT, FATAL);

	// Processes but no Services
	if( (bProcessFound) && (!bServiceFound) )
		StopAllProcesses();

	// Services but no Processes
	if( (!bProcessFound) && (bServiceFound) )
		StopAllServices();

	// Processes and Services
	if( (bProcessFound) && (bServiceFound) )
	{
		StopAllProcesses();
		StopAllServices();
	}

	// Remove the mappings of the drive
	OutputToEvents(_T("Removing drive shares."), INFO);

	TCHAR szDriveName[132];
	ULONG bufSize = 132;
	CString msg;
	if((WNetGetConnection(strDriveLetter, szDriveName, &bufSize)) == NO_ERROR)
	{	
		DWORD dwGTACSStatus = WNetCancelConnection2(strDriveLetter, CONNECT_UPDATE_PROFILE, TRUE);
		if(dwGTACSStatus == NO_ERROR)
		{
			msg.Format(_T("Successfully unmapped %s drive."), strDriveLetter);
			OutputToEvents(msg, INFO);
		}
		else
		{
			msg.Format(_T("Error unmapping %s drive."), strDriveLetter);
			OutputToEvents(msg, WARNING);
		}
	}
	if((WNetGetConnection(strSCHEDDriveLetter, szDriveName, &bufSize)) == NO_ERROR)
	{
		DWORD dwSCHEDStatus = WNetCancelConnection2(strSCHEDDriveLetter, CONNECT_UPDATE_PROFILE, TRUE);
		if(dwSCHEDStatus == NO_ERROR)
		{
			msg.Format(_T("Successfully unmapped %s drive."), strSCHEDDriveLetter);
			OutputToEvents(msg, INFO);
		}
		else
		{
			msg.Format(_T("Error unmapping %s drive."), strSCHEDDriveLetter);
			OutputToEvents(msg, WARNING);
		}
	}
			
	COXRegistryItem	cRegMappedDrives;
	cRegMappedDrives.SetFullRegistryItem(cRegMappedDrives.m_pszLocalMachine);
	cRegMappedDrives.SetKeyNames(_T("\\SOFTWARE\\ISI\\EpochParams\\MappedDrives"));

	cRegMappedDrives.SetStringValue(_T(""), strDriveLetter, false);
	cRegMappedDrives.SetStringValue(_T(""), strSCHEDDriveLetter, false);

	if( DeleteFile(m_strBinEpochAcc) )
		OutputToEvents(_T("Deleted bin.epoch.acc file."), INFO);
	else
	{
		CString strTxt(_T("Unable to Delete bin.epoch.acc file. "));

		strTxt += GetLastErrorText();
		OutputToEvents(strTxt, WARNING);
	}

	CDialog::OnTimer(nIDEvent);
	KillTimer(m_nTimer);
	OutputToEvents(_T("Application process completed."), INFO);
	m_bDone = TRUE;
	m_nTimer= SetTimer(1, 3000, 0);

}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 04/06/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEpShutdownDlg::OnOK()
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CEpShutdownDlg::OnOK()
{
	KillTimer(m_nTimer);
	CDialog::OnOK();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 04/05/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		EpShutdownDlg::StopAllProcesses
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CEpShutdownDlg::StopAllProcesses()
{
	CString	strError;
	int		nStatus;

	OutputToEvents(IDS_STOPPINGPROCESSES, INFO);

	for( int nIndex=0; nIndex<m_cMonitorArray.GetSize(); nIndex++ )
	{
		if( (m_cMonitorArray[nIndex].GetActive()) &&
			((m_cMonitorArray[nIndex].GetType() == CMonitor::PROCESS) ||
			((m_cMonitorArray[nIndex].GetType() == CMonitor::SERVICE_AS_PROCESS))) )
		{
			strError.Format(IDS_STOPPROCESS, m_cMonitorArray[nIndex].GetName());
			OutputToEvents(strError, INFO);

			if( nStatus	= m_cMonitorArray[nIndex].Stop() != ERROR_SUCCESS )
			{
				if( nStatus == ERROR_TIMEOUT )
				{
					strError.Format(IDS_TIMEOUT_ERROR, m_cMonitorArray[nIndex].GetName());
					OutputToEvents(strError, FATAL);
				}
				else
				{
					strError.Format(IDS_STOPPROCESS_ERROR, m_cMonitorArray[nIndex].GetName(),
						GetLastError(), GetLastErrorText());
					OutputToEvents(strError, FATAL);
				}
			}
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 04/05/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		EpShutdownDlg::StopAllServices
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CEpShutdownDlg::StopAllServices()
{
	int			nStatus;
	CString		strError;
	SC_HANDLE	hSCM;

	OutputToEvents(IDS_STOPPINGSERVICES, INFO);

	if( hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_CONNECT) )
	//if(hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS))
	{
		for( int nIndex=0; nIndex<m_cMonitorArray.GetSize(); nIndex++ )
		{
			if( (m_cMonitorArray[nIndex].GetActive()) &&
				(m_cMonitorArray[nIndex].GetType() == CMonitor::SERVICE) )
			{
				strError.Format(IDS_STOPSERVICE, m_cMonitorArray[nIndex].GetName());
				OutputToEvents(strError, INFO);

				if( nStatus	= m_cMonitorArray[nIndex].Stop(hSCM) != ERROR_SUCCESS )
				{
					if( nStatus == ERROR_TIMEOUT )
					{
						strError.Format(IDS_TIMEOUT_ERROR, m_cMonitorArray[nIndex].GetName());
						OutputToEvents(strError, FATAL);
					}
					else
					{
						strError.Format(IDS_STOPSERVICE_ERROR, m_cMonitorArray[nIndex].GetName(),
										GetLastError(), GetLastErrorText());
						OutputToEvents(strError, FATAL);
					}
				}
			}
		}

		CloseServiceHandle(hSCM);
	}
	else
	{
		strError.Format(IDS_SCMANAGER_ERROR, GetLastError(), GetLastErrorText());
		OutputToEvents(strError, FATAL);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 3/21/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDirMapperDlg::GetLastErrorText
//  Description :
//	Return :		CString	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CEpShutdownDlg::GetLastErrorText()
{
	LPVOID lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				  FORMAT_MESSAGE_FROM_SYSTEM |
				  FORMAT_MESSAGE_IGNORE_INSERTS,
				  NULL,
				  GetLastError(),
				  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				  (LPTSTR) &lpMsgBuf,
				  0,
				  NULL);

	CString strMsg((LPTSTR)lpMsgBuf);
	strMsg.Remove(_T('\x0D'));
	strMsg.Remove(_T('\x0A'));
	LocalFree(lpMsgBuf);
	return strMsg;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 04/05/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEpShutdownDlg::OutputToEvents
//  Description :
//	Return :		void	-
//	Parameters :
//		CString strText		-
//		eSeverity nSeverity	-
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CEpShutdownDlg::OutputToEvents(CString strText, eSeverity nSeverity)
{
	static COXSound	oxWarningSound;
	static COXSound	oxFatalSound;
	static BOOL		bFirstTime = TRUE;

	if( bFirstTime )
	{
		oxWarningSound.Open(IDW_RINGOUT);
		oxFatalSound.Open(IDW_GLASS);
		bFirstTime = FALSE;
	}

	int		nPos = m_cEventList.InsertItem(m_cEventList.GetItemCount(), strText, nSeverity);

/*	CString strTime;
	CTime	curTime(CTime::GetCurrentTime());

	strTime = curTime.FormatGmt(_T("%Y-%j-%H:%M:%S"));	
	nPos	= m_cEventList.InsertItem(LVIF_IMAGE | LVIF_TEXT,
				m_cEventList.GetItemCount(), strTime,
				0, 0, nSeverity, 0);
	m_cEventList.SetItemText(nPos, 1, strText);
*/
	m_cEventList.EnsureVisible(nPos, FALSE);
	m_cEventList.UpdateWindow();

	switch( nSeverity )
	{
		case(INFO)		:									break;
		case(WARNING)	: oxWarningSound.Play(FALSE, FALSE);break;
		case(FATAL)		: oxFatalSound.Play(FALSE, FALSE);	break;
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 04/06/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEpShutdownDlg::OutputToEvents
//  Description :
//	Return :		void	-
//	Parameters :
//		int nResourceID		-
//		eSeverity nSeverity	-
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CEpShutdownDlg::OutputToEvents(int nResourceID, eSeverity nSeverity)
{
	CString	strText;
	strText.Format(nResourceID);
	OutputToEvents(strText, nSeverity);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 04/06/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEpShutdownDlg::UpdateProcessList
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CEpShutdownDlg::UpdateProcessList()
{
	CProcessSnapshot cProcessSnapshot;
	cProcessSnapshot.MakeSnapshot();

	for( int nIndex=0; nIndex<m_cMonitorArray.GetSize(); nIndex++ )
		m_cMonitorArray[nIndex].Update(&cProcessSnapshot);
}
