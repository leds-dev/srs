// EpShutdownDlg.h : header file
//

#if !defined(AFX_EPSHUTDOWNDLG_H__B1D149A7_98D6_4936_95EE_39D98EC0D4F4__INCLUDED_)
#define AFX_EPSHUTDOWNDLG_H__B1D149A7_98D6_4936_95EE_39D98EC0D4F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Monitor.h"

/////////////////////////////////////////////////////////////////////////////
// CEpShutdownDlg dialog

class CEpShutdownDlg : public CDialog
{
// Construction
public:
	CEpShutdownDlg(CWnd* pParent = NULL);	// standard constructor
	typedef enum eSeverity {INFO=0, WARNING=1, FATAL=2};

// Dialog Data
	//{{AFX_DATA(CEpShutdownDlg)
	enum { IDD = IDD_EPSHUTDOWN_DIALOG };
	CListCtrl	m_cEventList;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEpShutdownDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	UINT							m_nTimer;
	BOOL							m_bDone;
	HICON							m_hIcon;
	CImageList						m_ilEvent;
	CString							m_strBinEpochAcc;
	CArray	<CMonitor, CMonitor&>	m_cMonitorArray;

	// Generated message map functions
	//{{AFX_MSG(CEpShutdownDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void	UpdateProcessList();
	void	StopAllServices();
	void	StopAllProcesses();
	void	OutputToEvents(CString strText, eSeverity nSeverity);
	void	OutputToEvents(int nResourceID, eSeverity nSeverity);
	CString GetLastErrorText();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EPSHUTDOWNDLG_H__B1D149A7_98D6_4936_95EE_39D98EC0D4F4__INCLUDED_)
