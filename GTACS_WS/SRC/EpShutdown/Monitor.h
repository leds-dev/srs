// Monitor.h: interface for the CMonitor class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MONITOR_H__732EE59C_1168_11D5_B82C_00C04F45001D__INCLUDED_)
#define AFX_MONITOR_H__732EE59C_1168_11D5_B82C_00C04F45001D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CProcessSnapshot
{
public:
	CProcessSnapshot();
	virtual ~CProcessSnapshot();
	void	MakeSnapshot();
	int		GetSize(){return m_strActiveProcessList.GetSize();};
	CString	GetProcess(int nIndex);
	DWORD	GetID(int nIndex);
protected:
	CStringArray	m_strActiveProcessList;
	CDWordArray		m_dwActiveProcessList;
};

class CMonitor
{
public:
	typedef enum TYPE {SERVICE=0, PROCESS=1, SERVICE_AS_PROCESS=2};
	CMonitor();
	CMonitor(CString strTitle, CString strName, TYPE eType);
	virtual ~CMonitor();
	void	Update(CProcessSnapshot* pSnapShot);
	CString GetTitle(){return m_strTitle;};
	CString GetName(){return m_strName;};
	BOOL	GetActive(){return m_bActive;};
	TYPE	GetType(){return m_eType;};
	DWORD	Stop(SC_HANDLE hSCM = NULL);
protected:
	CString	m_strTitle;
	CString	m_strName;
	TYPE	m_eType;
	BOOL	m_bActive;
	DWORD	m_dwProcessID;
	DWORD	StopService(SC_HANDLE hSCM, SC_HANDLE hService);
	DWORD	StopProcess();
};

#endif // !defined(AFX_MONITOR_H__732EE59C_1168_11D5_B82C_00C04F45001D__INCLUDED_)
