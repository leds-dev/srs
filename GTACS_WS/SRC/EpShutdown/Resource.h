//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by EpShutdown.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_EPSHUTDOWN_DIALOG           102
#define IDS_STOPPINGPROCESSES           102
#define IDS_STOPPROCESS                 103
#define IDS_STOPPROCESS_ERROR           104
#define IDS_STOPPINGSERVICES            105
#define IDS_STOPSERVICE                 106
#define IDS_STOPSERVICE_ERROR           107
#define IDS_SCMANAGER_ERROR             108
#define IDS_TIMEOUT_ERROR               109
#define IDS_STOPNOTHING_EVENT           110
#define IDS_MAP_ERROR                   111
#define IDS_UNMAP_OK                    112
#define IDR_MAINFRAME                   128
#define IDB_EVENT_BITMAP                129
#define IDW_GLASS                       131
#define IDW_RINGOUT                     138
#define IDC_LIST1                       1000

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
