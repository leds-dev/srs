//
// @(#) epochnt source EpExtendedHelp/EpExHelp.cpp 1.3 01/06/08 15:56:22
//
// EpExHelp.cpp : Implementation of CEpExHelp
#include "stdafx.h"
#include "EpExtendedHelp.h"
#include "EpExHelp.h"
#include "epcommon_i.c"

//*****************************************
//** GOESNQ hyperlink extension Version 1.3
//** Begin data definition

#include <fstream>
#include <string.h>


    CString XHELP_mnemonic;
	CString XHELP_indexFile;
	CString XHELP_displayFile;
	CString XHELP_cmd;
	ULONG	XHELP_indexFile_nbyte;
//** End GOESNQ hyperlink data definition
//*****************************************

/////////////////////////////////////////////////////////////////////////////
// CEpExHelp

			

STDMETHODIMP CEpExHelp::EpHelpEvent(BSTR bstrStrm, BSTR bstrDb, EPEVTCAT epCatType, EPPTVARIETY epptVar, int iMnemIndex, BSTR bstrMnem, BSTR bstrPri, int iEventNum, BSTR bstrSubsys,  long LEventTimeSec, long LEventTimeUSec, BSTR bstrName, BSTR bstrMsg, BSTR bstrHost, int iAckStatus)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString strHelpText;
	strHelpText.LoadString(IDS_EXHELP_NOT_AVAILABLE);

	if(m_bInit)
	{
		m_bInit = FALSE;
		m_pdlgHelp = new EvtExHelpDialog();//(TRUE);
		m_pdlgHelp->Create(IDD_DLG_EXAMPLE_EXHELP);
	}

	m_pdlgHelp->m_strHost = CString(bstrHost);
	m_pdlgHelp->m_strMnem = CString(bstrMnem);
	m_pdlgHelp->m_strMsg = CString(bstrMsg);
	m_pdlgHelp->m_strName = CString(bstrName);
	m_pdlgHelp->m_strStrm = CString(bstrStrm);
	TCHAR*  t= NULL;
	if(iEventNum >=0)
	{
		t = new TCHAR[10];
		memset(t,NULL,10);
		t = _ltow(iEventNum,t,10);
		m_pdlgHelp->m_strNum = CString(t);
		if(t)
			delete [] t;
	}
	switch(epCatType)
	{
	case 0:
		m_pdlgHelp->m_strType = _T("None");
		break;
	case 1:
		m_pdlgHelp->m_strType = _T("Text");
		break;
	case 2:
		m_pdlgHelp->m_strType = _T("Command");
		break;
	case 3:
		m_pdlgHelp->m_strType = _T("Telemetry Limits");
		break;
	case 4:
		m_pdlgHelp->m_strType =  _T("Global Limits");
		break;
	case 5:
		m_pdlgHelp->m_strType = _T("Telemetry Verification");
		break;
	case 6:
		m_pdlgHelp->m_strType = _T("System Status");
		break;
	case 7:
		m_pdlgHelp->m_strType = _T("Directive");
		break;
	case 8:
		m_pdlgHelp->m_strType = _T("Directive Response");
		break;
	case 9:
		m_pdlgHelp->m_strType = _T("Archive Playback");
		break;
	case 10:
		m_pdlgHelp->m_strType = _T("Hardware");

		break;
	case 11:
		m_pdlgHelp->m_strType = _T("Telemetry");
		break;
	case 12:
		m_pdlgHelp->m_strType = _T("Node Manager");
		break;
	default:
		break;

	}
	m_pdlgHelp->m_strType.FreeExtra();



	if(epptVar != EPPTVARIETY_CMD && epptVar != EPPTVARIETY_TLM)
	{
		strHelpText.Empty();
		strHelpText.LoadString(IDS_EX_HELP_NOT_AVAILABLE);
	}

	else
	{
		IEpEnumPoints* pIEpEnumPts = NULL;
		IEpEnumCommands* pIEpEnumCmds = NULL;
		IEpDB* pIEpDB;
		EpDbPointDescription pd;//[1];
		EpDbCmdDescription cd;//[1];
		CLSID clsid;
		ULONG ulFetched;
		CLSIDFromProgID(_T("EpDBSrvc.EpDB"), &clsid);
		DWORD dwDBCookie = 0;
		ULONG ulDbHandle;
		int nHelpIndex = -1; // help index start from 1 in db, initialized as 0 so init to -1 here to tell diff
		char tlmhelpbuf[1024];
		memset((void*)tlmhelpbuf,NULL,1024);
			
		
		HRESULT hr = CoCreateInstance(clsid,NULL,CLSCTX_LOCAL_SERVER,IID_IEpDB,(void**)&pIEpDB);

		hr = pIEpDB->OpenDatabase(bstrDb,dwDBCookie,&ulDbHandle); 
	
		if(epptVar == EPPTVARIETY_TLM)
		{
			hr = pIEpDB->GetPointEnum(ulDbHandle, bstrMnem, &pIEpEnumPts);

			if( pIEpEnumPts != NULL )
			{

				// begin code added by khai
				//XHELPfile = "s:\\EPOCH_DATABASE\\help\\epextendedhelp.htm";
				//XHELParg  = "s:\\EPOCH_DATABASE\\help\\epextendedhelp.htm#_";
				//XHELParg += _T("#_Topic_2");
				// XHELParg += bstrMnem;
				//ShellExecute(NULL,_T("open"),XHELPfile,XHELParg,NULL,1);

				//* XHELPfile = "c:\\iexplore.exe.lnk s:\\EPOCH_DATABASE\\help\\epextendedhelp.htm#_Topic_2";
				//* XHELParg  = "Mnemonic=";
				//* XHELParg  += bstrMnem;
				//* _wsystem(XHELPfile);
				//_wsystem (_T("c:\\iexplore.exe.lnk s:\\EPOCH_DATABASE\\help\\epextendedhelp.htm#_Topic_2"));
				// _wsystem (_T("\"c:\\Program Files\\Plus!\\Microsoft Internet\\IEXPLORE.EXE"));
				// end code added by khai

				
				hr = pIEpEnumPts->Next(1, &pd, &ulFetched);
				SysFreeString(pd.bstrMnemonic);
				SysFreeString(pd.bstrDescription);
				SysFreeString(pd.bstrUnits);
				SysFreeString(pd.bstrContextName);
				pIEpEnumPts->Release();
				if(ulFetched >0)
				{
					nHelpIndex = pd.nHelpIndex; //pIEpDB->EpDbGetPointHelpIndex(pd);//pd.nHelpIndex;
									
				}
				if(nHelpIndex <= 0)
				{
					strHelpText.Empty();
					strHelpText.LoadString(IDS_NO_TLM_EX_HELP);
				}
				else  //then we have a valid help index, look for .tlm file now
				{
					CFile cfile;
					CFileException e;
					CString strTempDb(bstrDb);
					CString strFilename = strTempDb.Left(strTempDb.GetLength() - 3); // 3 for .lis
					strFilename += _T("tlm");
					
					if(! cfile.Open(strFilename,CFile::modeRead,&e))
					{
						strHelpText.Empty();
						strHelpText.LoadString(IDS_NO_TLM_FILE);
					}
					else  //successfully opened .tlm file, read help line if possible
					{
						cfile.Seek( ( 1024 * (nHelpIndex -1)) , CFile::begin);
						cfile.Read(tlmhelpbuf,1024);
						tlmhelpbuf[1023] = '\0';
						strHelpText.Empty();
						strHelpText = tlmhelpbuf;
						strHelpText.TrimLeft();
						strHelpText.TrimRight();
						cfile.Close();
					}
				}
			}
		}
		else //then is type EPPTVARIETY_CMD
		{
			hr = pIEpDB->GetCommandEnum(ulDbHandle, bstrMnem, &pIEpEnumCmds);
			
			if(pIEpEnumCmds != NULL)
			{
				hr = pIEpEnumCmds->Next(1,&cd, &ulFetched);
				if(ulFetched >0)
					nHelpIndex = cd.nHelpIndex; 
				
				SysFreeString(cd.bstrMnemonic);
				SysFreeString(cd.bstrDescription);
				SysFreeString(cd.bstrAlias);
				SysFreeString(cd.bstrExecMnemonic);
				SysFreeString(cd.bstrCmdFormat);
				SysFreeString(cd.bstrSpacecraftAddr);
				SysFreeString(cd.bstrPrivilegeGroup);

				if(nHelpIndex <=0)
				{
					strHelpText.Empty();
					strHelpText.LoadString(IDS_NO_CMD_EX_HELP);
				}
				else //then we have a valid help index, look for .cmd file now
				{
					CFile cfile;
					CFileException e;
					CString strTempDb(bstrDb);
					CString strFilename = strTempDb.Left(strTempDb.GetLength() - 3); //parse off lis
					strFilename += _T("cmd"); 
					if(! cfile.Open(strFilename, CFile::modeRead,&e) )
					{
						strHelpText.Empty();
						strHelpText.LoadString(IDS_NO_CMD_FILE);
					}
					else //successfully opened .cmd file, read help line if possible
					{
						cfile.Seek((1024 * (nHelpIndex -1)),CFile::begin);
						cfile.Read(tlmhelpbuf,1024);
						tlmhelpbuf[1023] = '\0';
						strHelpText.Empty();
						strHelpText = tlmhelpbuf;
						strHelpText.TrimLeft();
						strHelpText.TrimRight();
						cfile.Close();
					}
				}

				pIEpEnumCmds->Release();
			}

		}
		
		pIEpDB->CloseDatabase(ulDbHandle);
		pIEpDB->Release();
	}

	strHelpText.Replace(_T("\n"),_T("\r\n"));
	strHelpText.FreeExtra();
	//*strHelpText = XHELPfile;
	//*strHelpText += _T("\n");
	//*strHelpText += XHELParg;
	strHelpText = _T("Mnemonic is ");
	strHelpText += bstrMnem;
	m_pdlgHelp->m_strExHelp = strHelpText;
	m_pdlgHelp->SetText();
	//*m_pdlgHelp->ShowWindow(SW_SHOW);

	// TODO: Add your implementation code here

	//**********************************
	//****** Begin GOESNQ hyperlink code
	//**********************************

	char pointname[256];
	char filename[256];
	char buffer[256];
	char *tok1,*tok2;
	char *delim = " =,\t";
	int	i;
	ULONG	found;
	ULONG len;

	// open debug log
	ofstream debugfile ("c:\\temp\\goesnq\\EPextendedEvent.log");
	debugfile << "***                           " << endl;
	debugfile << "*** GOESNQ Event Viewer ExtendedHelp Hyperlink log" << endl;
	debugfile << "*** Version 1.3               " << endl;;
	debugfile << "***                           " << endl << endl;


	// get mnemonic attached to event
	sprintf(pointname,"%ls",bstrMnem);
	len = strlen(pointname);

	debugfile << "eventnum  = " << iEventNum << endl;
	debugfile << "pointname = " << pointname << endl;
	debugfile << "namelen   = " << len << endl;
	debugfile << "\n\n";
	
	//**** strictly debug path 
	//if (iEventNum == 703) {
	//	strcpy(pointname,"TESTPOINT");
	//	len = strlen(pointname);
	//	debugfile << "*** debug using pointname = " << pointname << endl << endl;
	//}
	//**** end debug

	//** exit if no mnemonic
	if (len <= 0) return S_OK;

	//** open hyperlink index file
	ifstream indexfile; 
	indexfile.open("s:\\EPOCH_DATABASE\\help\\goesnq\\hyperlink.index",ios::in); 
	if (! indexfile)
	{
		//** error reading index file
		debugfile << "Error reading index file";
		// show Epoch standard extended help 
		m_pdlgHelp->ShowWindow(SW_SHOW);
		return S_OK;
	}

	//LPTSTR pointname = new TCHAR[strHelpText.GetLength()+1];
	//_tcscpy(pointname,strHelpText);
	//pointname = (char *) _stdcall ConvertBSTRToString(BSTR bstrMnem) throw(_com_error);
	//pointname = (char *) (LPCTSTR) strHelpText;

	//** search index file for name match 
	found = 0;
	while (!found && !indexfile.eof())
	{
	
		//** read one line
		indexfile.getline(buffer,100);
		//** ignore comment line
		if (strlen(buffer) > 0 && buffer[0] != '#') 
		{
			//debugfile << buffer << endl;
			tok1 = strtok(buffer,delim);
			tok2 = strtok(NULL,delim);
			//debugfile << "...tok1 = " << tok1 << endl;
			//debugfile << "...tok2 = " << tok2 << endl;
			if (!strcmp(pointname,tok1))
			{
				found = 1;
				strcpy(filename,tok2);
				debugfile << "Found point=" << pointname << " --> " << filename << endl;
			}
		}

	}

	//** close index file
	indexfile.close();	

	if (found) 
	{
		//** open display file
		//** if file extension is htm then call with argument

		i = strlen(filename)-4;
		i = strlen(filename);
		//debugfile << "*** displayfilename=" << filename << endl;
		//debugfile << "*** display.ext    =" << &filename[i] << endl;
		if (!strcmp(&filename[i],".htm")  || !strcmp(&filename[i],".HTM")) 
		{
			debugfile << "htm file found = (" << filename << ")" << endl;
			XHELP_cmd = "c:\\iexplorer.exe.lnk s:\\EPOCH_DATABASE\\help\\goesnq\\";
			XHELP_cmd += (char *)filename;
			//XHELP_cmd += _T("#_");
			//XHELP_cmd += (char *)pointname;
			_wsystem(XHELP_cmd);
		} else {
			XHELP_cmd = "s:\\EPOCH_DATABASE\\help\\goesnq\\";
			XHELP_cmd += (char *) filename;
			ShellExecute(NULL,_T("open"),XHELP_cmd,NULL,NULL,1);
		}
	
	} else {
		debugfile << "Point=" << pointname << " NOT found in index file";
		// show Epoch standard extended help 
		m_pdlgHelp->ShowWindow(SW_SHOW);
	}

	debugfile << "\n\n*** End of log file\n";
	debugfile.close();

	//****** End GOESNQ hyperlink code
	//********************************
	return S_OK;

}

STDMETHODIMP CEpExHelp::EpHelpMnem(BSTR bstrStrm, BSTR bstrDb, BSTR bstrMnemIndex,EPPTVARIETY epptVar)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
		/* EPPTVARIETY struct
			{
			EPPTVARIETY_NULL,
			EPPTVARIETY_GLOBAL,
			EPPTVARIETY_TLM,
			EPPTVARIETY_SUBSYS,
			EPPTVARIETY_CMD
			}
		*/

	// TODO: Add your implementation code here

	return S_OK;
}

void CEpExHelp::FinalRelease()
{
	if(m_pdlgHelp)
		delete m_pdlgHelp;
	m_pdlgHelp = NULL;
}


STDMETHODIMP CEpExHelp::EpRetHelpMnem(BSTR bstrStrm, BSTR bstrDb, BSTR bstrMnem,EPPTVARIETY epptVar, BSTR* bstrHelpText)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	CString strHelpText;
	strHelpText.LoadString(IDS_EXHELP_NOT_AVAILABLE);

	IEpEnumPoints* pIEpEnumPts = NULL;
	IEpEnumCommands* pIEpEnumCmds = NULL;
	IEpDB* pIEpDB = NULL;
	EpDbPointDescription pd;//[1];
	EpDbCmdDescription cd;//[1];
	CLSID clsid;
	ULONG ulFetched,ulDbHandle;
	CLSIDFromProgID(_T("EpDBSrvc.EpDB"), &clsid);
	DWORD dwDBCookie = 0;
	int nHelpIndex = -1; // help index start from 1 in db, initialized as 0 so init to -1 here to tell diff
	char tlmhelpbuf[1024];
	memset((void*)tlmhelpbuf,NULL,1024);
	HRESULT hr = S_OK;	
	
	hr = CoCreateInstance(clsid,NULL,CLSCTX_LOCAL_SERVER,IID_IEpDB,(void**)&pIEpDB);
	hr = pIEpDB->OpenDatabase(bstrDb,dwDBCookie,&ulDbHandle); 
	

	if(SUCCEEDED(hr))
	{
		if( epptVar== EPPTVARIETY_TLM )
		{
			hr = pIEpDB->GetPointEnum(ulDbHandle, bstrMnem, &pIEpEnumPts);
			
			if( pIEpEnumPts != NULL )
			{
				hr = pIEpEnumPts->Next(1, &pd, &ulFetched);
				SysFreeString(pd.bstrMnemonic);
				SysFreeString(pd.bstrDescription);
				SysFreeString(pd.bstrUnits);
				SysFreeString(pd.bstrContextName);

				pIEpEnumPts->Release();
				if(ulFetched >0)
				{
					nHelpIndex = pd.nHelpIndex; //pIEpDB->EpDbGetPointHelpIndex(pd);//pd.nHelpIndex;
									
				}
				
			}
		}
		else if (epptVar == EPPTVARIETY_CMD)
		{
			pIEpDB->GetCommandEnum(ulDbHandle,bstrMnem, &pIEpEnumCmds);
			if(pIEpEnumCmds!= NULL)
			{
				hr = pIEpEnumCmds->Next(1,&cd, &ulFetched);
				
				epptVar = EPPTVARIETY_CMD;

				if(ulFetched >0)
					nHelpIndex = cd.nHelpIndex; 
				pIEpEnumCmds->Release();

				SysFreeString(cd.bstrMnemonic);
				SysFreeString(cd.bstrDescription);
				SysFreeString(cd.bstrAlias);
				SysFreeString(cd.bstrExecMnemonic);
				SysFreeString(cd.bstrCmdFormat);
				SysFreeString(cd.bstrSpacecraftAddr);
				SysFreeString(cd.bstrPrivilegeGroup);

			}
		}
		if(nHelpIndex <= 0 || (epptVar != EPPTVARIETY_TLM && epptVar != EPPTVARIETY_CMD))
		{
			strHelpText.Empty();
			strHelpText.LoadString(IDS_NO_CMD_EX_HELP);
		}
		else  //then we have a valid help index, look for .tlm file now
		{
			CFile cfile;
			CFileException e;
			CString strTempDb(bstrDb);
			CString strFilename = strTempDb.Left(strTempDb.GetLength() - 3); // 3 for .lis
			if(epptVar == EPPTVARIETY_CMD)
				strFilename += _T("cmd");

			else if(epptVar == EPPTVARIETY_TLM)
				strFilename += _T("tlm");

			
			if(! cfile.Open(strFilename,CFile::modeRead,&e))
			{
				strHelpText.Empty();
				if(epptVar == EPPTVARIETY_CMD)
					strHelpText.LoadString(IDS_NO_CMD_FILE);
				else
					strHelpText.LoadString(IDS_NO_TLM_FILE);
			}
			else  //successfully opened .tlm file, read help line if possible
			{
				cfile.Seek( ( 1024 * (nHelpIndex -1)) , CFile::begin);
				cfile.Read(tlmhelpbuf,1024);
				tlmhelpbuf[1023] = '\0';
				strHelpText.Empty();
				strHelpText = tlmhelpbuf;
				strHelpText.TrimLeft();
				strHelpText.TrimRight();
				cfile.Close();
			}
		}
			
	}
	// TODO: Add your implementation code here
			
	pIEpDB->CloseDatabase(ulDbHandle);
	pIEpDB->Release();
	strHelpText.Replace(_T("\n"),_T("\r\n"));
	strHelpText.FreeExtra();

	BSTR bstrRetVal = strHelpText.AllocSysString();
	*bstrHelpText = bstrRetVal;
	return S_OK;
}


STDMETHODIMP CEpExHelp::EpRetHelpEvent(BSTR bstrStrm,BSTR bstrDb, EPEVTCAT epCatType, EPPTVARIETY epptVar, int iMnemIndex, BSTR bstrMnem, BSTR bstrPri, int iEventNum, BSTR bstrSubsys, long LEventTimeSec, long LEventTimeUSec, BSTR bstrName, BSTR bstrMsg, BSTR bstrHost, int iAckStatus,BSTR* bstrHelpText)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	// TODO: Add your implementation code here

	return S_OK;
}
