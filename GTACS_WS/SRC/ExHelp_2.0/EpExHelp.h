//
// @(#) epochnt source EpExtendedHelp/EpExHelp.h 1.2 01/06/08 15:56:23
//
// EpExHelp.h : Declaration of the CEpExHelp

#ifndef __EPEXHELP_H_
#define __EPEXHELP_H_

#include "resource.h"       // main symbols
#include "EvtExHelpDialog.h"
/////////////////////////////////////////////////////////////////////////////
// CEpExHelp
class ATL_NO_VTABLE CEpExHelp : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CEpExHelp, &CLSID_EpExHelp>,
	public IDispatchImpl<IEpExHelp, &IID_IEpExHelp, &LIBID_EPEXTENDEDHELPLib>
{
public:
	CEpExHelp()
	{
		m_pdlgHelp = NULL;
		m_bInit = TRUE;
	}

DECLARE_REGISTRY_RESOURCEID(IDR_EPEXHELP)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CEpExHelp)
	COM_INTERFACE_ENTRY(IEpExHelp)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IEpExHelp
public:
	STDMETHOD(EpRetHelpEvent)(BSTR bstrStrm,BSTR bstrDb, EPEVTCAT epCatType, EPPTVARIETY epptVar, int iMnemIndex, BSTR bstrMnem, BSTR bstrPri, int iEventNum, BSTR bstrSubsys,  long LEventTimeSec, long LEventTimeUSec, BSTR bstrName, BSTR bstrMsg, BSTR bstrHost, int iAckStatus,/* [out, retval] */ BSTR* bstrHelpText);
	STDMETHOD(EpRetHelpMnem)(BSTR bstrStrm,BSTR bstrDb, BSTR bstrMnem,EPPTVARIETY epptVar, /*[out,retval] */ BSTR* bstrHelpText);
	STDMETHOD(EpHelpMnem)(BSTR bstrStrm, BSTR bstrDb, BSTR bstrMnemIndex,EPPTVARIETY epptVar);
	STDMETHOD(EpHelpEvent)(BSTR bstrStrm,BSTR bstrDb, EPEVTCAT epCatType, EPPTVARIETY epptVar, int iMnemIndex, BSTR bstrMnem, BSTR bstrPri, int iEventNum, BSTR bstrSubsys,  long LEventTimeSec, long LEventTimeUSec, BSTR bstrName, BSTR bstrMsg, BSTR bstrHost, int iAckStatus);
	void FinalRelease();
	EvtExHelpDialog* m_pdlgHelp;
	BOOL m_bInit;
};

#endif //__EPEXHELP_H_
