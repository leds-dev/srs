/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Wed Jun 20 09:40:57 2001
 */
/* Compiler settings for G:\EpochDev\EpExtendedHelp\EpExtendedHelp.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __EpExtendedHelp_h__
#define __EpExtendedHelp_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IEpExHelp_FWD_DEFINED__
#define __IEpExHelp_FWD_DEFINED__
typedef interface IEpExHelp IEpExHelp;
#endif 	/* __IEpExHelp_FWD_DEFINED__ */


#ifndef __EpExHelp_FWD_DEFINED__
#define __EpExHelp_FWD_DEFINED__

#ifdef __cplusplus
typedef class EpExHelp EpExHelp;
#else
typedef struct EpExHelp EpExHelp;
#endif /* __cplusplus */

#endif 	/* __EpExHelp_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "include/epcommon.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __IEpExHelp_INTERFACE_DEFINED__
#define __IEpExHelp_INTERFACE_DEFINED__

/* interface IEpExHelp */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IEpExHelp;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E91E707F-59B0-11D5-AC53-00B0D028604E")
    IEpExHelp : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EpHelpEvent( 
            BSTR bstrStrm,
            BSTR bstrDb,
            EPEVTCAT epCatType,
            EPPTVARIETY epptVar,
            int iMnemIndex,
            BSTR bstrMnem,
            BSTR bstrPri,
            int iEventNum,
            BSTR bstrSubsys,
            long LEventTimeSec,
            long LEventTimeUSec,
            BSTR bstrName,
            BSTR bstrMsg,
            BSTR bstrHost,
            int iAckStatus) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EpHelpMnem( 
            BSTR bstrStrm,
            BSTR bstrDb,
            BSTR bstrMnem,
            EPPTVARIETY epptVar) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EpRetHelpMnem( 
            BSTR bstrStrm,
            BSTR bstrDb,
            BSTR bstrMnem,
            EPPTVARIETY epptVar,
            /* [retval][out] */ BSTR __RPC_FAR *bstrHelpText) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE EpRetHelpEvent( 
            BSTR bstrStrm,
            BSTR bstrDb,
            EPEVTCAT epCatType,
            EPPTVARIETY epptVar,
            int iMnemIndex,
            BSTR bstrMnem,
            BSTR bstrPri,
            int iEventNum,
            BSTR bstrSubsys,
            long LEventTimeSec,
            long LEventTimeUSec,
            BSTR bstrName,
            BSTR bstrMsg,
            BSTR bstrHost,
            int iAckStatus,
            /* [retval][out] */ BSTR __RPC_FAR *bstrHelpText) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpExHelpVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpExHelp __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpExHelp __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpExHelp __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IEpExHelp __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IEpExHelp __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IEpExHelp __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IEpExHelp __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EpHelpEvent )( 
            IEpExHelp __RPC_FAR * This,
            BSTR bstrStrm,
            BSTR bstrDb,
            EPEVTCAT epCatType,
            EPPTVARIETY epptVar,
            int iMnemIndex,
            BSTR bstrMnem,
            BSTR bstrPri,
            int iEventNum,
            BSTR bstrSubsys,
            long LEventTimeSec,
            long LEventTimeUSec,
            BSTR bstrName,
            BSTR bstrMsg,
            BSTR bstrHost,
            int iAckStatus);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EpHelpMnem )( 
            IEpExHelp __RPC_FAR * This,
            BSTR bstrStrm,
            BSTR bstrDb,
            BSTR bstrMnem,
            EPPTVARIETY epptVar);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EpRetHelpMnem )( 
            IEpExHelp __RPC_FAR * This,
            BSTR bstrStrm,
            BSTR bstrDb,
            BSTR bstrMnem,
            EPPTVARIETY epptVar,
            /* [retval][out] */ BSTR __RPC_FAR *bstrHelpText);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EpRetHelpEvent )( 
            IEpExHelp __RPC_FAR * This,
            BSTR bstrStrm,
            BSTR bstrDb,
            EPEVTCAT epCatType,
            EPPTVARIETY epptVar,
            int iMnemIndex,
            BSTR bstrMnem,
            BSTR bstrPri,
            int iEventNum,
            BSTR bstrSubsys,
            long LEventTimeSec,
            long LEventTimeUSec,
            BSTR bstrName,
            BSTR bstrMsg,
            BSTR bstrHost,
            int iAckStatus,
            /* [retval][out] */ BSTR __RPC_FAR *bstrHelpText);
        
        END_INTERFACE
    } IEpExHelpVtbl;

    interface IEpExHelp
    {
        CONST_VTBL struct IEpExHelpVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpExHelp_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpExHelp_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpExHelp_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpExHelp_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEpExHelp_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEpExHelp_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEpExHelp_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEpExHelp_EpHelpEvent(This,bstrStrm,bstrDb,epCatType,epptVar,iMnemIndex,bstrMnem,bstrPri,iEventNum,bstrSubsys,LEventTimeSec,LEventTimeUSec,bstrName,bstrMsg,bstrHost,iAckStatus)	\
    (This)->lpVtbl -> EpHelpEvent(This,bstrStrm,bstrDb,epCatType,epptVar,iMnemIndex,bstrMnem,bstrPri,iEventNum,bstrSubsys,LEventTimeSec,LEventTimeUSec,bstrName,bstrMsg,bstrHost,iAckStatus)

#define IEpExHelp_EpHelpMnem(This,bstrStrm,bstrDb,bstrMnem,epptVar)	\
    (This)->lpVtbl -> EpHelpMnem(This,bstrStrm,bstrDb,bstrMnem,epptVar)

#define IEpExHelp_EpRetHelpMnem(This,bstrStrm,bstrDb,bstrMnem,epptVar,bstrHelpText)	\
    (This)->lpVtbl -> EpRetHelpMnem(This,bstrStrm,bstrDb,bstrMnem,epptVar,bstrHelpText)

#define IEpExHelp_EpRetHelpEvent(This,bstrStrm,bstrDb,epCatType,epptVar,iMnemIndex,bstrMnem,bstrPri,iEventNum,bstrSubsys,LEventTimeSec,LEventTimeUSec,bstrName,bstrMsg,bstrHost,iAckStatus,bstrHelpText)	\
    (This)->lpVtbl -> EpRetHelpEvent(This,bstrStrm,bstrDb,epCatType,epptVar,iMnemIndex,bstrMnem,bstrPri,iEventNum,bstrSubsys,LEventTimeSec,LEventTimeUSec,bstrName,bstrMsg,bstrHost,iAckStatus,bstrHelpText)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpExHelp_EpHelpEvent_Proxy( 
    IEpExHelp __RPC_FAR * This,
    BSTR bstrStrm,
    BSTR bstrDb,
    EPEVTCAT epCatType,
    EPPTVARIETY epptVar,
    int iMnemIndex,
    BSTR bstrMnem,
    BSTR bstrPri,
    int iEventNum,
    BSTR bstrSubsys,
    long LEventTimeSec,
    long LEventTimeUSec,
    BSTR bstrName,
    BSTR bstrMsg,
    BSTR bstrHost,
    int iAckStatus);


void __RPC_STUB IEpExHelp_EpHelpEvent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpExHelp_EpHelpMnem_Proxy( 
    IEpExHelp __RPC_FAR * This,
    BSTR bstrStrm,
    BSTR bstrDb,
    BSTR bstrMnem,
    EPPTVARIETY epptVar);


void __RPC_STUB IEpExHelp_EpHelpMnem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpExHelp_EpRetHelpMnem_Proxy( 
    IEpExHelp __RPC_FAR * This,
    BSTR bstrStrm,
    BSTR bstrDb,
    BSTR bstrMnem,
    EPPTVARIETY epptVar,
    /* [retval][out] */ BSTR __RPC_FAR *bstrHelpText);


void __RPC_STUB IEpExHelp_EpRetHelpMnem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpExHelp_EpRetHelpEvent_Proxy( 
    IEpExHelp __RPC_FAR * This,
    BSTR bstrStrm,
    BSTR bstrDb,
    EPEVTCAT epCatType,
    EPPTVARIETY epptVar,
    int iMnemIndex,
    BSTR bstrMnem,
    BSTR bstrPri,
    int iEventNum,
    BSTR bstrSubsys,
    long LEventTimeSec,
    long LEventTimeUSec,
    BSTR bstrName,
    BSTR bstrMsg,
    BSTR bstrHost,
    int iAckStatus,
    /* [retval][out] */ BSTR __RPC_FAR *bstrHelpText);


void __RPC_STUB IEpExHelp_EpRetHelpEvent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpExHelp_INTERFACE_DEFINED__ */



#ifndef __EPEXTENDEDHELPLib_LIBRARY_DEFINED__
#define __EPEXTENDEDHELPLib_LIBRARY_DEFINED__

/* library EPEXTENDEDHELPLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_EPEXTENDEDHELPLib;

EXTERN_C const CLSID CLSID_EpExHelp;

#ifdef __cplusplus

class DECLSPEC_UUID("E91E7080-59B0-11D5-AC53-00B0D028604E")
EpExHelp;
#endif
#endif /* __EPEXTENDEDHELPLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
