/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Wed Jun 20 09:40:57 2001
 */
/* Compiler settings for G:\EpochDev\EpExtendedHelp\EpExtendedHelp.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IEpExHelp = {0xE91E707F,0x59B0,0x11D5,{0xAC,0x53,0x00,0xB0,0xD0,0x28,0x60,0x4E}};


const IID LIBID_EPEXTENDEDHELPLib = {0xE91E706F,0x59B0,0x11D5,{0xAC,0x53,0x00,0xB0,0xD0,0x28,0x60,0x4E}};


const CLSID CLSID_EpExHelp = {0xE91E7080,0x59B0,0x11D5,{0xAC,0x53,0x00,0xB0,0xD0,0x28,0x60,0x4E}};


#ifdef __cplusplus
}
#endif

