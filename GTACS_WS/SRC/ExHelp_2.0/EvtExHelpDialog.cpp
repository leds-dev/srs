//
// @(#) epochnt source EpExtendedHelp/EvtExHelpDialog.cpp 1.1 01/06/07 14:05:12
//
// EvtExHelpDialog.cpp : implementation file
//

#include "stdafx.h"
#include "resource.h"
#include "EvtExHelpDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// EvtExHelpDialog dialog


EvtExHelpDialog::EvtExHelpDialog(CWnd* pParent /*=NULL*/)
	: CDialog(EvtExHelpDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(EvtExHelpDialog)
	m_strHost = _T("");
	m_strMnem = _T("");
	m_strName = _T("");
	m_strNum = _T("");
	m_strStrm = _T("");
	m_strType = _T("");
	m_strExHelp = _T("");
	m_strMsg = _T("");
	//}}AFX_DATA_INIT
}


void EvtExHelpDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EvtExHelpDialog)
	DDX_Text(pDX, IDC_STATIC_HOST, m_strHost);
	DDX_Text(pDX, IDC_STATIC_MNEM, m_strMnem);
	DDX_Text(pDX, IDC_STATIC_NAME, m_strName);
	DDX_Text(pDX, IDC_STATIC_NUM, m_strNum);
	DDX_Text(pDX, IDC_STATIC_STRM, m_strStrm);
	DDX_Text(pDX, IDC_STATIC_TYPE, m_strType);
	DDX_Text(pDX, IDC_EDIT_EXHELP, m_strExHelp);
	DDX_Text(pDX, IDC_EDIT_MSG, m_strMsg);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(EvtExHelpDialog, CDialog)
	//{{AFX_MSG_MAP(EvtExHelpDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EvtExHelpDialog message handlers





void EvtExHelpDialog::SetText()
{
	CEdit* pTemp = (CEdit*)GetDlgItem(IDC_EDIT_EXHELP);
	pTemp->SetWindowText(m_strExHelp);
	 pTemp = (CEdit*)GetDlgItem(IDC_EDIT_MSG);
	pTemp->SetWindowText(m_strMsg);
	CStatic* pStatic = (CStatic*) GetDlgItem(IDC_STATIC_HOST);
	pStatic->SetWindowText(m_strHost);
	pStatic = (CStatic*) GetDlgItem(IDC_STATIC_MNEM);
	pStatic->SetWindowText(m_strMnem);
	pStatic = (CStatic*) GetDlgItem(IDC_STATIC_NAME);
	pStatic->SetWindowText(m_strName);
	pStatic = (CStatic*) GetDlgItem(IDC_STATIC_NUM);
	pStatic->SetWindowText(m_strNum);
	pStatic = (CStatic*) GetDlgItem(IDC_STATIC_STRM);
	pStatic->SetWindowText(m_strStrm);
	pStatic = (CStatic*) GetDlgItem(IDC_STATIC_TYPE);
	pStatic->SetWindowText(m_strType);

}


