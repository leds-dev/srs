//
// @(#) epochnt source EpExtendedHelp/EvtExHelpDialog.h 1.2 01/06/07 14:09:12
//
#if !defined(AFX_EVTEXHELPDIALOG_H__4B3078AD_5A76_11D5_AC53_00B0D028604E__INCLUDED_)
#define AFX_EVTEXHELPDIALOG_H__4B3078AD_5A76_11D5_AC53_00B0D028604E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EvtExHelpDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// EvtExHelpDialog dialog

class EvtExHelpDialog : public CDialog
{
// Construction
public:
	void SetText();
	EvtExHelpDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(EvtExHelpDialog)
	enum { IDD = IDD_DLG_EXAMPLE_EXHELP };
	CString	m_strHost;
	CString	m_strMnem;
	CString	m_strName;
	CString	m_strNum;
	CString	m_strStrm;
	CString	m_strType;
	CString	m_strExHelp;
	CString	m_strMsg;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(EvtExHelpDialog)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(EvtExHelpDialog)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EVTEXHELPDIALOG_H__4B3078AD_5A76_11D5_AC53_00B0D028604E__INCLUDED_)
