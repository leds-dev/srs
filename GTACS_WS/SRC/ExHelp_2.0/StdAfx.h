//
// @(#) epochnt source EpExtendedHelp/StdAfx.h 1.1 01/06/07 14:05:15
//
// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if !defined(AFX_STDAFX_H__E91E7073_59B0_11D5_AC53_00B0D028604E__INCLUDED_)
#define AFX_STDAFX_H__E91E7073_59B0_11D5_AC53_00B0D028604E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#ifndef _WIN32_WINNT
// #define _WIN32_WINNT 0x0400
// FJS - changed value to 0x501
/*
	This targets the Windows 2000 operating system. 
	Other valid values include 0x0501 for Windows XP, 0x0502 for Windows Server 2003,
	0x0600 for Windows Vista, and 0x0601 for Windows 7.
*/
#define _WIN32_WINNT 0x0601
#endif
#define _ATL_APARTMENT_THREADED

#include <afxwin.h>
#include <afxdisp.h>

#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

using namespace std;

#endif // !defined(AFX_STDAFX_H__E91E7073_59B0_11D5_AC53_00B0D028604E__INCLUDED)
