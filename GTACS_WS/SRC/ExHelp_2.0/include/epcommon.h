/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Mon Aug 13 20:40:04 2007
 */
/* Compiler settings for C:\epochnt\eptypes\epcommon.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __epcommon_h__
#define __epcommon_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IEpSSAPI_FWD_DEFINED__
#define __IEpSSAPI_FWD_DEFINED__
typedef interface IEpSSAPI IEpSSAPI;
#endif 	/* __IEpSSAPI_FWD_DEFINED__ */


#ifndef ___IEpochAliveCB_FWD_DEFINED__
#define ___IEpochAliveCB_FWD_DEFINED__
typedef interface _IEpochAliveCB _IEpochAliveCB;
#endif 	/* ___IEpochAliveCB_FWD_DEFINED__ */


#ifndef ___IEpochCB_FWD_DEFINED__
#define ___IEpochCB_FWD_DEFINED__
typedef interface _IEpochCB _IEpochCB;
#endif 	/* ___IEpochCB_FWD_DEFINED__ */


#ifndef __IEpSrvcBase_FWD_DEFINED__
#define __IEpSrvcBase_FWD_DEFINED__
typedef interface IEpSrvcBase IEpSrvcBase;
#endif 	/* __IEpSrvcBase_FWD_DEFINED__ */


#ifndef __IEpData_FWD_DEFINED__
#define __IEpData_FWD_DEFINED__
typedef interface IEpData IEpData;
#endif 	/* __IEpData_FWD_DEFINED__ */


#ifndef __IEpEvent_FWD_DEFINED__
#define __IEpEvent_FWD_DEFINED__
typedef interface IEpEvent IEpEvent;
#endif 	/* __IEpEvent_FWD_DEFINED__ */


#ifndef __IEpSTOL_FWD_DEFINED__
#define __IEpSTOL_FWD_DEFINED__
typedef interface IEpSTOL IEpSTOL;
#endif 	/* __IEpSTOL_FWD_DEFINED__ */


#ifndef ___IEpStrmMonCB_FWD_DEFINED__
#define ___IEpStrmMonCB_FWD_DEFINED__
typedef interface _IEpStrmMonCB _IEpStrmMonCB;
#endif 	/* ___IEpStrmMonCB_FWD_DEFINED__ */


#ifndef __IEpStrmMon_FWD_DEFINED__
#define __IEpStrmMon_FWD_DEFINED__
typedef interface IEpStrmMon IEpStrmMon;
#endif 	/* __IEpStrmMon_FWD_DEFINED__ */


#ifndef __IEpEnumCalibrations_FWD_DEFINED__
#define __IEpEnumCalibrations_FWD_DEFINED__
typedef interface IEpEnumCalibrations IEpEnumCalibrations;
#endif 	/* __IEpEnumCalibrations_FWD_DEFINED__ */


#ifndef __IEpEnumConversions_FWD_DEFINED__
#define __IEpEnumConversions_FWD_DEFINED__
typedef interface IEpEnumConversions IEpEnumConversions;
#endif 	/* __IEpEnumConversions_FWD_DEFINED__ */


#ifndef __IEpEnumDeltas_FWD_DEFINED__
#define __IEpEnumDeltas_FWD_DEFINED__
typedef interface IEpEnumDeltas IEpEnumDeltas;
#endif 	/* __IEpEnumDeltas_FWD_DEFINED__ */


#ifndef __IEpEnumLimits_FWD_DEFINED__
#define __IEpEnumLimits_FWD_DEFINED__
typedef interface IEpEnumLimits IEpEnumLimits;
#endif 	/* __IEpEnumLimits_FWD_DEFINED__ */


#ifndef __IEpEnumLocations_FWD_DEFINED__
#define __IEpEnumLocations_FWD_DEFINED__
typedef interface IEpEnumLocations IEpEnumLocations;
#endif 	/* __IEpEnumLocations_FWD_DEFINED__ */


#ifndef __IEpEnumStates_FWD_DEFINED__
#define __IEpEnumStates_FWD_DEFINED__
typedef interface IEpEnumStates IEpEnumStates;
#endif 	/* __IEpEnumStates_FWD_DEFINED__ */


#ifndef __IEpEnumPoints_FWD_DEFINED__
#define __IEpEnumPoints_FWD_DEFINED__
typedef interface IEpEnumPoints IEpEnumPoints;
#endif 	/* __IEpEnumPoints_FWD_DEFINED__ */


#ifndef __IEpEnumSubsystems_FWD_DEFINED__
#define __IEpEnumSubsystems_FWD_DEFINED__
typedef interface IEpEnumSubsystems IEpEnumSubsystems;
#endif 	/* __IEpEnumSubsystems_FWD_DEFINED__ */


#ifndef __IEpEnumEvents_FWD_DEFINED__
#define __IEpEnumEvents_FWD_DEFINED__
typedef interface IEpEnumEvents IEpEnumEvents;
#endif 	/* __IEpEnumEvents_FWD_DEFINED__ */


#ifndef __IEpEnumTlmLimMsgs_FWD_DEFINED__
#define __IEpEnumTlmLimMsgs_FWD_DEFINED__
typedef interface IEpEnumTlmLimMsgs IEpEnumTlmLimMsgs;
#endif 	/* __IEpEnumTlmLimMsgs_FWD_DEFINED__ */


#ifndef __IEpEnumDatawords_FWD_DEFINED__
#define __IEpEnumDatawords_FWD_DEFINED__
typedef interface IEpEnumDatawords IEpEnumDatawords;
#endif 	/* __IEpEnumDatawords_FWD_DEFINED__ */


#ifndef __IEpEnumDatawordArgValues_FWD_DEFINED__
#define __IEpEnumDatawordArgValues_FWD_DEFINED__
typedef interface IEpEnumDatawordArgValues IEpEnumDatawordArgValues;
#endif 	/* __IEpEnumDatawordArgValues_FWD_DEFINED__ */


#ifndef __IEpEnumDatawordArgRanges_FWD_DEFINED__
#define __IEpEnumDatawordArgRanges_FWD_DEFINED__
typedef interface IEpEnumDatawordArgRanges IEpEnumDatawordArgRanges;
#endif 	/* __IEpEnumDatawordArgRanges_FWD_DEFINED__ */


#ifndef __IEpEnumDatawordArgTvContexts_FWD_DEFINED__
#define __IEpEnumDatawordArgTvContexts_FWD_DEFINED__
typedef interface IEpEnumDatawordArgTvContexts IEpEnumDatawordArgTvContexts;
#endif 	/* __IEpEnumDatawordArgTvContexts_FWD_DEFINED__ */


#ifndef __IEpEnumDatawordArgs_FWD_DEFINED__
#define __IEpEnumDatawordArgs_FWD_DEFINED__
typedef interface IEpEnumDatawordArgs IEpEnumDatawordArgs;
#endif 	/* __IEpEnumDatawordArgs_FWD_DEFINED__ */


#ifndef __IEpEnumCmdDatawordGroups_FWD_DEFINED__
#define __IEpEnumCmdDatawordGroups_FWD_DEFINED__
typedef interface IEpEnumCmdDatawordGroups IEpEnumCmdDatawordGroups;
#endif 	/* __IEpEnumCmdDatawordGroups_FWD_DEFINED__ */


#ifndef __IEpEnumCmdPrivilegeGroups_FWD_DEFINED__
#define __IEpEnumCmdPrivilegeGroups_FWD_DEFINED__
typedef interface IEpEnumCmdPrivilegeGroups IEpEnumCmdPrivilegeGroups;
#endif 	/* __IEpEnumCmdPrivilegeGroups_FWD_DEFINED__ */


#ifndef __IEpEnumTvEvents_FWD_DEFINED__
#define __IEpEnumTvEvents_FWD_DEFINED__
typedef interface IEpEnumTvEvents IEpEnumTvEvents;
#endif 	/* __IEpEnumTvEvents_FWD_DEFINED__ */


#ifndef __IEpEnumTvSequences_FWD_DEFINED__
#define __IEpEnumTvSequences_FWD_DEFINED__
typedef interface IEpEnumTvSequences IEpEnumTvSequences;
#endif 	/* __IEpEnumTvSequences_FWD_DEFINED__ */


#ifndef __IEpEnumPreTvStates_FWD_DEFINED__
#define __IEpEnumPreTvStates_FWD_DEFINED__
typedef interface IEpEnumPreTvStates IEpEnumPreTvStates;
#endif 	/* __IEpEnumPreTvStates_FWD_DEFINED__ */


#ifndef __IEpEnumPostTvStates_FWD_DEFINED__
#define __IEpEnumPostTvStates_FWD_DEFINED__
typedef interface IEpEnumPostTvStates IEpEnumPostTvStates;
#endif 	/* __IEpEnumPostTvStates_FWD_DEFINED__ */


#ifndef __IEpEnumPseudoTvStates_FWD_DEFINED__
#define __IEpEnumPseudoTvStates_FWD_DEFINED__
typedef interface IEpEnumPseudoTvStates IEpEnumPseudoTvStates;
#endif 	/* __IEpEnumPseudoTvStates_FWD_DEFINED__ */


#ifndef __IEpEnumCmdExecVerifiers_FWD_DEFINED__
#define __IEpEnumCmdExecVerifiers_FWD_DEFINED__
typedef interface IEpEnumCmdExecVerifiers IEpEnumCmdExecVerifiers;
#endif 	/* __IEpEnumCmdExecVerifiers_FWD_DEFINED__ */


#ifndef __IEpEnumCmdEchoVerifierEvents_FWD_DEFINED__
#define __IEpEnumCmdEchoVerifierEvents_FWD_DEFINED__
typedef interface IEpEnumCmdEchoVerifierEvents IEpEnumCmdEchoVerifierEvents;
#endif 	/* __IEpEnumCmdEchoVerifierEvents_FWD_DEFINED__ */


#ifndef __IEpEnumScAddrs_FWD_DEFINED__
#define __IEpEnumScAddrs_FWD_DEFINED__
typedef interface IEpEnumScAddrs IEpEnumScAddrs;
#endif 	/* __IEpEnumScAddrs_FWD_DEFINED__ */


#ifndef __IEpEnumCmdDatawordMaps_FWD_DEFINED__
#define __IEpEnumCmdDatawordMaps_FWD_DEFINED__
typedef interface IEpEnumCmdDatawordMaps IEpEnumCmdDatawordMaps;
#endif 	/* __IEpEnumCmdDatawordMaps_FWD_DEFINED__ */


#ifndef __IEpEnumCmdFrameFormats_FWD_DEFINED__
#define __IEpEnumCmdFrameFormats_FWD_DEFINED__
typedef interface IEpEnumCmdFrameFormats IEpEnumCmdFrameFormats;
#endif 	/* __IEpEnumCmdFrameFormats_FWD_DEFINED__ */


#ifndef __IEpEnumCmdChannels_FWD_DEFINED__
#define __IEpEnumCmdChannels_FWD_DEFINED__
typedef interface IEpEnumCmdChannels IEpEnumCmdChannels;
#endif 	/* __IEpEnumCmdChannels_FWD_DEFINED__ */


#ifndef __IEpEnumCmdFmtPointValue_FWD_DEFINED__
#define __IEpEnumCmdFmtPointValue_FWD_DEFINED__
typedef interface IEpEnumCmdFmtPointValue IEpEnumCmdFmtPointValue;
#endif 	/* __IEpEnumCmdFmtPointValue_FWD_DEFINED__ */


#ifndef __IEpEnumCmdFormats_FWD_DEFINED__
#define __IEpEnumCmdFormats_FWD_DEFINED__
typedef interface IEpEnumCmdFormats IEpEnumCmdFormats;
#endif 	/* __IEpEnumCmdFormats_FWD_DEFINED__ */


#ifndef __IEpEnumCommands_FWD_DEFINED__
#define __IEpEnumCommands_FWD_DEFINED__
typedef interface IEpEnumCommands IEpEnumCommands;
#endif 	/* __IEpEnumCommands_FWD_DEFINED__ */


#ifndef __IEpDB_FWD_DEFINED__
#define __IEpDB_FWD_DEFINED__
typedef interface IEpDB IEpDB;
#endif 	/* __IEpDB_FWD_DEFINED__ */


#ifndef __IEpAttrib_FWD_DEFINED__
#define __IEpAttrib_FWD_DEFINED__
typedef interface IEpAttrib IEpAttrib;
#endif 	/* __IEpAttrib_FWD_DEFINED__ */


#ifndef __IEpDBHelper_FWD_DEFINED__
#define __IEpDBHelper_FWD_DEFINED__
typedef interface IEpDBHelper IEpDBHelper;
#endif 	/* __IEpDBHelper_FWD_DEFINED__ */


#ifndef __IEpProcCtrl_FWD_DEFINED__
#define __IEpProcCtrl_FWD_DEFINED__
typedef interface IEpProcCtrl IEpProcCtrl;
#endif 	/* __IEpProcCtrl_FWD_DEFINED__ */


#ifndef ___IEpProcCB_FWD_DEFINED__
#define ___IEpProcCB_FWD_DEFINED__
typedef interface _IEpProcCB _IEpProcCB;
#endif 	/* ___IEpProcCB_FWD_DEFINED__ */


#ifndef ___IEpTrigSrvcModCB_FWD_DEFINED__
#define ___IEpTrigSrvcModCB_FWD_DEFINED__
typedef interface _IEpTrigSrvcModCB _IEpTrigSrvcModCB;
#endif 	/* ___IEpTrigSrvcModCB_FWD_DEFINED__ */


#ifndef __IEpTrigSrvcMod_FWD_DEFINED__
#define __IEpTrigSrvcMod_FWD_DEFINED__
typedef interface IEpTrigSrvcMod IEpTrigSrvcMod;
#endif 	/* __IEpTrigSrvcMod_FWD_DEFINED__ */


#ifndef __IEpTrigClntMod_FWD_DEFINED__
#define __IEpTrigClntMod_FWD_DEFINED__
typedef interface IEpTrigClntMod IEpTrigClntMod;
#endif 	/* __IEpTrigClntMod_FWD_DEFINED__ */


#ifndef __IEpTrigCtrl_FWD_DEFINED__
#define __IEpTrigCtrl_FWD_DEFINED__
typedef interface IEpTrigCtrl IEpTrigCtrl;
#endif 	/* __IEpTrigCtrl_FWD_DEFINED__ */


#ifndef ___IEpTrigSrvcCB_FWD_DEFINED__
#define ___IEpTrigSrvcCB_FWD_DEFINED__
typedef interface _IEpTrigSrvcCB _IEpTrigSrvcCB;
#endif 	/* ___IEpTrigSrvcCB_FWD_DEFINED__ */


#ifndef __IEpIR_FWD_DEFINED__
#define __IEpIR_FWD_DEFINED__
typedef interface IEpIR IEpIR;
#endif 	/* __IEpIR_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "epenums.h"
#include "include/epservertypes.h"
#include "epctrlidl.h"
#include "epdr2.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

/* interface __MIDL_itf_epcommon_0000 */
/* [local] */ 

typedef struct  tagEPSTOLRESP
    {
    ULONG ulUserData;
    EPSTOLCOMPSTAT scsStatus;
    BSTR bstrResponse;
    }	EPSTOLRESP;

typedef struct  tagEPSTATUSDATA
    {
    BOOL pbStrmUp;
    ULONG ulFlags;
    ULONG ulUserData;
    }	EPSTATUSDATA;

typedef unsigned short VARTYPE;

typedef struct  tagEPVAR
    {
    VARTYPE vt;
    /* [switch_is] */ /* [switch_type] */ union 
        {
        /* [case()] */  /* Empty union arm */ 
        /* [case()] */ ULONG ulVal;
        /* [case()] */ double dblVal;
        /* [case()] */ BSTR bstrVal;
        /* [case()] */ EPBLOB blob;
        /* [case()] */ TIMEVAL tvTime;
        /* [case()] */ EPSYSSTAT ssVal;
        }	;
    }	EPVAR;

typedef struct  tagEPPTDATA
    {
    EPVAR epvarValue;
    ULONG ulStatusBits;
    ULONG ulUserData;
    TIMEVAL tvTime;
    }	EPPTDATA;

typedef struct  tagEPFRMDATA
    {
    ULONG ulUserData;
    TIMEVAL tvReceiptTime;
    UINT uiQuality;
    EPVAR epvarFrame;
    }	EPFRMDATA;

typedef struct  EPACKINFO
    {
    BSTR bstrMnemonic;
    ULONG ulStatus;
    }	EPACKINFO;

typedef struct  EPDBMODINFO
    {
    BSTR bstrMnemonic;
    int nRecord;
    EPDBMODFCN eFunction;
    EPDBMODTYPE eType;
    double dValue;
    }	EPDBMODINFO;

typedef struct  EPLIMITINFOFULL
    {
    BSTR bstrMnemonic;
    TIMEVAL tvFirstTime;
    ULONG ulFirstStatus;
    double dblFirstLimit;
    double dblFirstValue;
    TIMEVAL tvCurrTime;
    ULONG ulCurrStatus;
    ULONG ulFlags;
    }	EPLIMITINFOFULL;

typedef struct  EPLIMITINFOUPDATE
    {
    BSTR bstrMnemonic;
    TIMEVAL tvCurrTime;
    ULONG ulCurrStatus;
    }	EPLIMITINFOUPDATE;

typedef struct  EPLIMITINFO
    {
    VARTYPE vt;
    /* [switch_is] */ /* [switch_type] */ union 
        {
        /* [case()] */ EPLIMITINFOFULL eplifRec;
        /* [case()] */ EPLIMITINFOUPDATE epliuRec;
        }	;
    }	EPLIMITINFO;

typedef struct  EpCLIMStatusRec
    {
    EPCLIMSTATUS eStatus;
    BSTR bstrUserID;
    BSTR bstrDirective;
    }	EpCLIMStatusRec;

typedef struct  EpCmdStatusInitRec
    {
    ULONG ulRefID;
    TIMEVAL tvTime;
    BSTR bstrDirective;
    BSTR bstrAddr;
    BSTR bstrMnemonic;
    EPCMDPVMODE ePVMode;
    EPCMDCVMODE eCVMode;
    EPCMDTVMODE eTVMode;
    EPCMDAEMODE eAEMode;
    EPCMDRTMODE eRTMode;
    EPCMDSTATUS eStatus;
    BOOL bDone;
    }	EpCmdStatusInitRec;

typedef struct  EpCmdStatusUpdateRec
    {
    ULONG ulRefID;
    TIMEVAL tvTime;
    EPCMDSTATUS eStatus;
    BOOL bDone;
    }	EpCmdStatusUpdateRec;

typedef struct  EpCmdStatusPVRec
    {
    ULONG ulRefID;
    TIMEVAL tvTime;
    EPCMDPVSTATUS ePVStatus;
    }	EpCmdStatusPVRec;

typedef struct  EpCmdStatusCVRec
    {
    ULONG ulRefID;
    TIMEVAL tvTime;
    EPCMDCVTYPE eCVType;
    EPCMDCVSTATUS eCVStatus;
    }	EpCmdStatusCVRec;

typedef struct  EpCmdStatusTVRec
    {
    ULONG ulRefID;
    TIMEVAL tvTime;
    BSTR bstrMnemonic;
    EPCMDTVSTATUS eTVStatus;
    DWORD dwValue;
    }	EpCmdStatusTVRec;

typedef struct  EpCmdStatusTVAllRec
    {
    ULONG ulRefID;
    TIMEVAL tvTime;
    EPCMDTVSTATUS eTVStatus;
    }	EpCmdStatusTVAllRec;

typedef struct  EpCmdStatusRec
    {
    VARTYPE vt;
    /* [switch_is] */ /* [switch_type] */ union 
        {
        /* [case()] */ EpCmdStatusInitRec epcsiRec;
        /* [case()] */ EpCmdStatusUpdateRec epcsuRec;
        /* [case()] */ EpCmdStatusPVRec epcspRec;
        /* [case()] */ EpCmdStatusCVRec epcscRec;
        /* [case()] */ EpCmdStatusTVRec epcstRec;
        /* [case()] */ EpCmdStatusTVAllRec epcstaRec;
        /* [case()] */  /* Empty union arm */ 
        }	;
    }	EpCmdStatusRec;

typedef struct  EPLIMITOFFINFO
    {
    ULONG ulFlags;
    ULONG ulArrySize;
    /* [size_is] */ INT __RPC_FAR *pIndices;
    }	EPLIMITOFFINFO;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0000_v0_0_s_ifspec;

#ifndef __IEpSSAPI_INTERFACE_DEFINED__
#define __IEpSSAPI_INTERFACE_DEFINED__

/* interface IEpSSAPI */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpSSAPI;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F3D3679D-B7A2-11D2-A336-00600867A0E2")
    IEpSSAPI : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE AcknowledgePoints( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulArraySize,
            /* [size_is][in] */ EPACKINFO __RPC_FAR *ackData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ConnectStream( 
            /* [in] */ BSTR bstrClientID,
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ DWORD dwCBCookie,
            /* [retval][out] */ ULONG __RPC_FAR *pulStrmHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE DisconnectStream( 
            /* [in] */ ULONG ulStrmHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestConStat( 
            /* [in] */ ULONG ulStrmHandle,
            /* [retval][out] */ EPSTATUSDATA __RPC_FAR *pStatusData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestConStatService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CancelService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetPoint( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [in] */ EPPTDATA eptData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPointInfo( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [retval][out] */ EPPTDATA __RPC_FAR *peptData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RefreshLimitOffService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestCLIMStatusService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestCmdStatusService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestLimitOffService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestLimitService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestPoint( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [retval][out] */ EPPTDATA __RPC_FAR *peptData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestPointService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [in] */ EPCBCOND cctType,
            /* [in] */ EPSERVICEMODE smtType,
            /* [in] */ LONG lAsyncUP,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestFrameService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ExecuteSTOLWait( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrDirective,
            /* [retval][out] */ EPSTOLRESP __RPC_FAR *psrResponse) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ExecuteSTOLNoWait( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrDirective,
            /* [in] */ SHORT sPriority,
            /* [in] */ ULONG ulUserData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SendEvent( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulMessageNum,
            /* [in] */ BSTR bstrMessage) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestEventService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ EPEVTSERVICE est,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestDBModService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RegisterRouter( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ IEpDirRtr __RPC_FAR *pIEpDirRtr,
            /* [in] */ BSTR bstrPCUser) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE WriteDBModsToFile( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrFile) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpSSAPIVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpSSAPI __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpSSAPI __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AcknowledgePoints )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulArraySize,
            /* [size_is][in] */ EPACKINFO __RPC_FAR *ackData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConnectStream )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ BSTR bstrClientID,
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ DWORD dwCBCookie,
            /* [retval][out] */ ULONG __RPC_FAR *pulStrmHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DisconnectStream )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestConStat )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [retval][out] */ EPSTATUSDATA __RPC_FAR *pStatusData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestConStatService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CancelService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetPoint )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [in] */ EPPTDATA eptData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPointInfo )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [retval][out] */ EPPTDATA __RPC_FAR *peptData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RefreshLimitOffService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestCLIMStatusService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestCmdStatusService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestLimitOffService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestLimitService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestPoint )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [retval][out] */ EPPTDATA __RPC_FAR *peptData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestPointService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [in] */ EPCBCOND cctType,
            /* [in] */ EPSERVICEMODE smtType,
            /* [in] */ LONG lAsyncUP,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestFrameService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExecuteSTOLWait )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrDirective,
            /* [retval][out] */ EPSTOLRESP __RPC_FAR *psrResponse);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExecuteSTOLNoWait )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrDirective,
            /* [in] */ SHORT sPriority,
            /* [in] */ ULONG ulUserData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SendEvent )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulMessageNum,
            /* [in] */ BSTR bstrMessage);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestEventService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ EPEVTSERVICE est,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestDBModService )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RegisterRouter )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ IEpDirRtr __RPC_FAR *pIEpDirRtr,
            /* [in] */ BSTR bstrPCUser);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *WriteDBModsToFile )( 
            IEpSSAPI __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrFile);
        
        END_INTERFACE
    } IEpSSAPIVtbl;

    interface IEpSSAPI
    {
        CONST_VTBL struct IEpSSAPIVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpSSAPI_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpSSAPI_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpSSAPI_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpSSAPI_AcknowledgePoints(This,ulStrmHandle,ulArraySize,ackData)	\
    (This)->lpVtbl -> AcknowledgePoints(This,ulStrmHandle,ulArraySize,ackData)

#define IEpSSAPI_ConnectStream(This,bstrClientID,bstrStrmName,dwCBCookie,pulStrmHandle)	\
    (This)->lpVtbl -> ConnectStream(This,bstrClientID,bstrStrmName,dwCBCookie,pulStrmHandle)

#define IEpSSAPI_DisconnectStream(This,ulStrmHandle)	\
    (This)->lpVtbl -> DisconnectStream(This,ulStrmHandle)

#define IEpSSAPI_RequestConStat(This,ulStrmHandle,pStatusData)	\
    (This)->lpVtbl -> RequestConStat(This,ulStrmHandle,pStatusData)

#define IEpSSAPI_RequestConStatService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestConStatService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpSSAPI_CancelService(This,ulStrmHandle,ulServiceHandle)	\
    (This)->lpVtbl -> CancelService(This,ulStrmHandle,ulServiceHandle)

#define IEpSSAPI_SetPoint(This,ulStrmHandle,bstrName,ctType,eptData)	\
    (This)->lpVtbl -> SetPoint(This,ulStrmHandle,bstrName,ctType,eptData)

#define IEpSSAPI_GetPointInfo(This,ulStrmHandle,bstrName,peptData)	\
    (This)->lpVtbl -> GetPointInfo(This,ulStrmHandle,bstrName,peptData)

#define IEpSSAPI_RefreshLimitOffService(This,ulStrmHandle,ulServiceHandle)	\
    (This)->lpVtbl -> RefreshLimitOffService(This,ulStrmHandle,ulServiceHandle)

#define IEpSSAPI_RequestCLIMStatusService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestCLIMStatusService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpSSAPI_RequestCmdStatusService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestCmdStatusService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpSSAPI_RequestLimitOffService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestLimitOffService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpSSAPI_RequestLimitService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestLimitService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpSSAPI_RequestPoint(This,ulStrmHandle,bstrName,ctType,peptData)	\
    (This)->lpVtbl -> RequestPoint(This,ulStrmHandle,bstrName,ctType,peptData)

#define IEpSSAPI_RequestPointService(This,ulStrmHandle,bstrName,ctType,cctType,smtType,lAsyncUP,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestPointService(This,ulStrmHandle,bstrName,ctType,cctType,smtType,lAsyncUP,ulUserData,pServiceHandle)

#define IEpSSAPI_RequestFrameService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestFrameService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpSSAPI_ExecuteSTOLWait(This,ulStrmHandle,bstrDirective,psrResponse)	\
    (This)->lpVtbl -> ExecuteSTOLWait(This,ulStrmHandle,bstrDirective,psrResponse)

#define IEpSSAPI_ExecuteSTOLNoWait(This,ulStrmHandle,bstrDirective,sPriority,ulUserData)	\
    (This)->lpVtbl -> ExecuteSTOLNoWait(This,ulStrmHandle,bstrDirective,sPriority,ulUserData)

#define IEpSSAPI_SendEvent(This,ulStrmHandle,ulMessageNum,bstrMessage)	\
    (This)->lpVtbl -> SendEvent(This,ulStrmHandle,ulMessageNum,bstrMessage)

#define IEpSSAPI_RequestEventService(This,ulStrmHandle,est,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestEventService(This,ulStrmHandle,est,ulUserData,pServiceHandle)

#define IEpSSAPI_RequestDBModService(This,ulStrmHandle,bstrName,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestDBModService(This,ulStrmHandle,bstrName,ulUserData,pServiceHandle)

#define IEpSSAPI_RegisterRouter(This,ulStrmHandle,pIEpDirRtr,bstrPCUser)	\
    (This)->lpVtbl -> RegisterRouter(This,ulStrmHandle,pIEpDirRtr,bstrPCUser)

#define IEpSSAPI_WriteDBModsToFile(This,ulStrmHandle,bstrFile)	\
    (This)->lpVtbl -> WriteDBModsToFile(This,ulStrmHandle,bstrFile)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_AcknowledgePoints_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulArraySize,
    /* [size_is][in] */ EPACKINFO __RPC_FAR *ackData);


void __RPC_STUB IEpSSAPI_AcknowledgePoints_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_ConnectStream_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ BSTR bstrClientID,
    /* [in] */ BSTR bstrStrmName,
    /* [in] */ DWORD dwCBCookie,
    /* [retval][out] */ ULONG __RPC_FAR *pulStrmHandle);


void __RPC_STUB IEpSSAPI_ConnectStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_DisconnectStream_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle);


void __RPC_STUB IEpSSAPI_DisconnectStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestConStat_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [retval][out] */ EPSTATUSDATA __RPC_FAR *pStatusData);


void __RPC_STUB IEpSSAPI_RequestConStat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestConStatService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSSAPI_RequestConStatService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_CancelService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulServiceHandle);


void __RPC_STUB IEpSSAPI_CancelService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_SetPoint_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrName,
    /* [in] */ EPCONVTYPE ctType,
    /* [in] */ EPPTDATA eptData);


void __RPC_STUB IEpSSAPI_SetPoint_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_GetPointInfo_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrName,
    /* [retval][out] */ EPPTDATA __RPC_FAR *peptData);


void __RPC_STUB IEpSSAPI_GetPointInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RefreshLimitOffService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulServiceHandle);


void __RPC_STUB IEpSSAPI_RefreshLimitOffService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestCLIMStatusService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSSAPI_RequestCLIMStatusService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestCmdStatusService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSSAPI_RequestCmdStatusService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestLimitOffService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSSAPI_RequestLimitOffService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestLimitService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSSAPI_RequestLimitService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestPoint_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrName,
    /* [in] */ EPCONVTYPE ctType,
    /* [retval][out] */ EPPTDATA __RPC_FAR *peptData);


void __RPC_STUB IEpSSAPI_RequestPoint_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestPointService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrName,
    /* [in] */ EPCONVTYPE ctType,
    /* [in] */ EPCBCOND cctType,
    /* [in] */ EPSERVICEMODE smtType,
    /* [in] */ LONG lAsyncUP,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSSAPI_RequestPointService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestFrameService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSSAPI_RequestFrameService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_ExecuteSTOLWait_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrDirective,
    /* [retval][out] */ EPSTOLRESP __RPC_FAR *psrResponse);


void __RPC_STUB IEpSSAPI_ExecuteSTOLWait_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_ExecuteSTOLNoWait_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrDirective,
    /* [in] */ SHORT sPriority,
    /* [in] */ ULONG ulUserData);


void __RPC_STUB IEpSSAPI_ExecuteSTOLNoWait_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_SendEvent_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulMessageNum,
    /* [in] */ BSTR bstrMessage);


void __RPC_STUB IEpSSAPI_SendEvent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestEventService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ EPEVTSERVICE est,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSSAPI_RequestEventService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RequestDBModService_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrName,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSSAPI_RequestDBModService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_RegisterRouter_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ IEpDirRtr __RPC_FAR *pIEpDirRtr,
    /* [in] */ BSTR bstrPCUser);


void __RPC_STUB IEpSSAPI_RegisterRouter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSSAPI_WriteDBModsToFile_Proxy( 
    IEpSSAPI __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrFile);


void __RPC_STUB IEpSSAPI_WriteDBModsToFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpSSAPI_INTERFACE_DEFINED__ */


#ifndef ___IEpochAliveCB_INTERFACE_DEFINED__
#define ___IEpochAliveCB_INTERFACE_DEFINED__

/* interface _IEpochAliveCB */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID__IEpochAliveCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7B843D45-0234-11d3-803B-006008C0949C")
    _IEpochAliveCB : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE AreYouAliveCB( void) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct _IEpochAliveCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            _IEpochAliveCB __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            _IEpochAliveCB __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            _IEpochAliveCB __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AreYouAliveCB )( 
            _IEpochAliveCB __RPC_FAR * This);
        
        END_INTERFACE
    } _IEpochAliveCBVtbl;

    interface _IEpochAliveCB
    {
        CONST_VTBL struct _IEpochAliveCBVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IEpochAliveCB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IEpochAliveCB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IEpochAliveCB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IEpochAliveCB_AreYouAliveCB(This)	\
    (This)->lpVtbl -> AreYouAliveCB(This)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochAliveCB_AreYouAliveCB_Proxy( 
    _IEpochAliveCB __RPC_FAR * This);


void __RPC_STUB _IEpochAliveCB_AreYouAliveCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* ___IEpochAliveCB_INTERFACE_DEFINED__ */


#ifndef ___IEpochCB_INTERFACE_DEFINED__
#define ___IEpochCB_INTERFACE_DEFINED__

/* interface _IEpochCB */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID__IEpochCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A84304D5-BCFF-11d2-A336-00600867A0E2")
    _IEpochCB : public _IEpochAliveCB
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ConStatServiceCB( 
            /* [in] */ EPSTATUSDATA conStatus) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE PointServiceCB( 
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPPTDATA __RPC_FAR *eptData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE FrameServiceCB( 
            /* [in] */ EPFRMDATA frmData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EventServiceCB( 
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPEVTDATA __RPC_FAR *evtData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CLIMStatusServiceCB( 
            /* [in] */ ULONG ulUserData,
            /* [in] */ EpCLIMStatusRec clsData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CmdStatusServiceCB( 
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EpCmdStatusRec __RPC_FAR *csData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE LimitOffServiceCB( 
            /* [in] */ ULONG ulUserData,
            /* [in] */ EPLIMITOFFINFO loiData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE LimitServiceCB( 
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPLIMITINFO __RPC_FAR *liData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE STOLCompStatCB( 
            /* [in] */ EPSTOLRESP scStatus) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE DBModServiceCB( 
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPDBMODINFO __RPC_FAR *dbmData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct _IEpochCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            _IEpochCB __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            _IEpochCB __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AreYouAliveCB )( 
            _IEpochCB __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConStatServiceCB )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ EPSTATUSDATA conStatus);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *PointServiceCB )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPPTDATA __RPC_FAR *eptData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *FrameServiceCB )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ EPFRMDATA frmData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EventServiceCB )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPEVTDATA __RPC_FAR *evtData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CLIMStatusServiceCB )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ ULONG ulUserData,
            /* [in] */ EpCLIMStatusRec clsData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CmdStatusServiceCB )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EpCmdStatusRec __RPC_FAR *csData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *LimitOffServiceCB )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ ULONG ulUserData,
            /* [in] */ EPLIMITOFFINFO loiData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *LimitServiceCB )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPLIMITINFO __RPC_FAR *liData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *STOLCompStatCB )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ EPSTOLRESP scStatus);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DBModServiceCB )( 
            _IEpochCB __RPC_FAR * This,
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPDBMODINFO __RPC_FAR *dbmData);
        
        END_INTERFACE
    } _IEpochCBVtbl;

    interface _IEpochCB
    {
        CONST_VTBL struct _IEpochCBVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IEpochCB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IEpochCB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IEpochCB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IEpochCB_AreYouAliveCB(This)	\
    (This)->lpVtbl -> AreYouAliveCB(This)


#define _IEpochCB_ConStatServiceCB(This,conStatus)	\
    (This)->lpVtbl -> ConStatServiceCB(This,conStatus)

#define _IEpochCB_PointServiceCB(This,ulArrySize,eptData)	\
    (This)->lpVtbl -> PointServiceCB(This,ulArrySize,eptData)

#define _IEpochCB_FrameServiceCB(This,frmData)	\
    (This)->lpVtbl -> FrameServiceCB(This,frmData)

#define _IEpochCB_EventServiceCB(This,ulArrySize,evtData)	\
    (This)->lpVtbl -> EventServiceCB(This,ulArrySize,evtData)

#define _IEpochCB_CLIMStatusServiceCB(This,ulUserData,clsData)	\
    (This)->lpVtbl -> CLIMStatusServiceCB(This,ulUserData,clsData)

#define _IEpochCB_CmdStatusServiceCB(This,ulUserData,ulArrySize,csData)	\
    (This)->lpVtbl -> CmdStatusServiceCB(This,ulUserData,ulArrySize,csData)

#define _IEpochCB_LimitOffServiceCB(This,ulUserData,loiData)	\
    (This)->lpVtbl -> LimitOffServiceCB(This,ulUserData,loiData)

#define _IEpochCB_LimitServiceCB(This,ulUserData,ulArrySize,liData)	\
    (This)->lpVtbl -> LimitServiceCB(This,ulUserData,ulArrySize,liData)

#define _IEpochCB_STOLCompStatCB(This,scStatus)	\
    (This)->lpVtbl -> STOLCompStatCB(This,scStatus)

#define _IEpochCB_DBModServiceCB(This,ulUserData,ulArrySize,dbmData)	\
    (This)->lpVtbl -> DBModServiceCB(This,ulUserData,ulArrySize,dbmData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochCB_ConStatServiceCB_Proxy( 
    _IEpochCB __RPC_FAR * This,
    /* [in] */ EPSTATUSDATA conStatus);


void __RPC_STUB _IEpochCB_ConStatServiceCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochCB_PointServiceCB_Proxy( 
    _IEpochCB __RPC_FAR * This,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPPTDATA __RPC_FAR *eptData);


void __RPC_STUB _IEpochCB_PointServiceCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochCB_FrameServiceCB_Proxy( 
    _IEpochCB __RPC_FAR * This,
    /* [in] */ EPFRMDATA frmData);


void __RPC_STUB _IEpochCB_FrameServiceCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochCB_EventServiceCB_Proxy( 
    _IEpochCB __RPC_FAR * This,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPEVTDATA __RPC_FAR *evtData);


void __RPC_STUB _IEpochCB_EventServiceCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochCB_CLIMStatusServiceCB_Proxy( 
    _IEpochCB __RPC_FAR * This,
    /* [in] */ ULONG ulUserData,
    /* [in] */ EpCLIMStatusRec clsData);


void __RPC_STUB _IEpochCB_CLIMStatusServiceCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochCB_CmdStatusServiceCB_Proxy( 
    _IEpochCB __RPC_FAR * This,
    /* [in] */ ULONG ulUserData,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EpCmdStatusRec __RPC_FAR *csData);


void __RPC_STUB _IEpochCB_CmdStatusServiceCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochCB_LimitOffServiceCB_Proxy( 
    _IEpochCB __RPC_FAR * This,
    /* [in] */ ULONG ulUserData,
    /* [in] */ EPLIMITOFFINFO loiData);


void __RPC_STUB _IEpochCB_LimitOffServiceCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochCB_LimitServiceCB_Proxy( 
    _IEpochCB __RPC_FAR * This,
    /* [in] */ ULONG ulUserData,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPLIMITINFO __RPC_FAR *liData);


void __RPC_STUB _IEpochCB_LimitServiceCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochCB_STOLCompStatCB_Proxy( 
    _IEpochCB __RPC_FAR * This,
    /* [in] */ EPSTOLRESP scStatus);


void __RPC_STUB _IEpochCB_STOLCompStatCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpochCB_DBModServiceCB_Proxy( 
    _IEpochCB __RPC_FAR * This,
    /* [in] */ ULONG ulUserData,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPDBMODINFO __RPC_FAR *dbmData);


void __RPC_STUB _IEpochCB_DBModServiceCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* ___IEpochCB_INTERFACE_DEFINED__ */


#ifndef __IEpSrvcBase_INTERFACE_DEFINED__
#define __IEpSrvcBase_INTERFACE_DEFINED__

/* interface IEpSrvcBase */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpSrvcBase;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A1311B8F-AB0F-11D2-9156-000000000000")
    IEpSrvcBase : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ConnectStream( 
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ DWORD dwCBCookie,
            /* [retval][out] */ ULONG __RPC_FAR *pulStrmHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE DisconnectStream( 
            /* [in] */ ULONG ulStrmHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestConStat( 
            /* [in] */ ULONG ulStrmHandle,
            /* [retval][out] */ EPSTATUSDATA __RPC_FAR *pStatusData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestConStatService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CancelService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpSrvcBaseVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpSrvcBase __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpSrvcBase __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpSrvcBase __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConnectStream )( 
            IEpSrvcBase __RPC_FAR * This,
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ DWORD dwCBCookie,
            /* [retval][out] */ ULONG __RPC_FAR *pulStrmHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DisconnectStream )( 
            IEpSrvcBase __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestConStat )( 
            IEpSrvcBase __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [retval][out] */ EPSTATUSDATA __RPC_FAR *pStatusData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestConStatService )( 
            IEpSrvcBase __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CancelService )( 
            IEpSrvcBase __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle);
        
        END_INTERFACE
    } IEpSrvcBaseVtbl;

    interface IEpSrvcBase
    {
        CONST_VTBL struct IEpSrvcBaseVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpSrvcBase_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpSrvcBase_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpSrvcBase_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpSrvcBase_ConnectStream(This,bstrStrmName,dwCBCookie,pulStrmHandle)	\
    (This)->lpVtbl -> ConnectStream(This,bstrStrmName,dwCBCookie,pulStrmHandle)

#define IEpSrvcBase_DisconnectStream(This,ulStrmHandle)	\
    (This)->lpVtbl -> DisconnectStream(This,ulStrmHandle)

#define IEpSrvcBase_RequestConStat(This,ulStrmHandle,pStatusData)	\
    (This)->lpVtbl -> RequestConStat(This,ulStrmHandle,pStatusData)

#define IEpSrvcBase_RequestConStatService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestConStatService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpSrvcBase_CancelService(This,ulStrmHandle,ulServiceHandle)	\
    (This)->lpVtbl -> CancelService(This,ulStrmHandle,ulServiceHandle)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSrvcBase_ConnectStream_Proxy( 
    IEpSrvcBase __RPC_FAR * This,
    /* [in] */ BSTR bstrStrmName,
    /* [in] */ DWORD dwCBCookie,
    /* [retval][out] */ ULONG __RPC_FAR *pulStrmHandle);


void __RPC_STUB IEpSrvcBase_ConnectStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSrvcBase_DisconnectStream_Proxy( 
    IEpSrvcBase __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle);


void __RPC_STUB IEpSrvcBase_DisconnectStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSrvcBase_RequestConStat_Proxy( 
    IEpSrvcBase __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [retval][out] */ EPSTATUSDATA __RPC_FAR *pStatusData);


void __RPC_STUB IEpSrvcBase_RequestConStat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSrvcBase_RequestConStatService_Proxy( 
    IEpSrvcBase __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSrvcBase_RequestConStatService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSrvcBase_CancelService_Proxy( 
    IEpSrvcBase __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulServiceHandle);


void __RPC_STUB IEpSrvcBase_CancelService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpSrvcBase_INTERFACE_DEFINED__ */


#ifndef __IEpData_INTERFACE_DEFINED__
#define __IEpData_INTERFACE_DEFINED__

/* interface IEpData */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpData;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BF32FAA7-B6D1-11D2-A334-00600867A0E2")
    IEpData : public IEpSrvcBase
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE AcknowledgePoints( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulArraySize,
            /* [size_is][in] */ EPACKINFO __RPC_FAR *ackData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetPoint( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [in] */ EPPTDATA eptData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPointInfo( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [retval][out] */ EPPTDATA __RPC_FAR *peptData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RefreshLimitOffService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestPoint( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [retval][out] */ EPPTDATA __RPC_FAR *peptData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestPointService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [in] */ EPCBCOND cctType,
            /* [in] */ EPSERVICEMODE smtType,
            /* [in] */ LONG lAsyncUP,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestFrameService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestLimitOffService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestLimitService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestDBModService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE WriteDBModsToFile( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrFile) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpDataVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpData __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpData __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpData __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConnectStream )( 
            IEpData __RPC_FAR * This,
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ DWORD dwCBCookie,
            /* [retval][out] */ ULONG __RPC_FAR *pulStrmHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DisconnectStream )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestConStat )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [retval][out] */ EPSTATUSDATA __RPC_FAR *pStatusData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestConStatService )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CancelService )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AcknowledgePoints )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulArraySize,
            /* [size_is][in] */ EPACKINFO __RPC_FAR *ackData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetPoint )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [in] */ EPPTDATA eptData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPointInfo )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [retval][out] */ EPPTDATA __RPC_FAR *peptData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RefreshLimitOffService )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestPoint )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [retval][out] */ EPPTDATA __RPC_FAR *peptData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestPointService )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ EPCONVTYPE ctType,
            /* [in] */ EPCBCOND cctType,
            /* [in] */ EPSERVICEMODE smtType,
            /* [in] */ LONG lAsyncUP,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestFrameService )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestLimitOffService )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestLimitService )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestDBModService )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrName,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *WriteDBModsToFile )( 
            IEpData __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrFile);
        
        END_INTERFACE
    } IEpDataVtbl;

    interface IEpData
    {
        CONST_VTBL struct IEpDataVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpData_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpData_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpData_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpData_ConnectStream(This,bstrStrmName,dwCBCookie,pulStrmHandle)	\
    (This)->lpVtbl -> ConnectStream(This,bstrStrmName,dwCBCookie,pulStrmHandle)

#define IEpData_DisconnectStream(This,ulStrmHandle)	\
    (This)->lpVtbl -> DisconnectStream(This,ulStrmHandle)

#define IEpData_RequestConStat(This,ulStrmHandle,pStatusData)	\
    (This)->lpVtbl -> RequestConStat(This,ulStrmHandle,pStatusData)

#define IEpData_RequestConStatService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestConStatService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpData_CancelService(This,ulStrmHandle,ulServiceHandle)	\
    (This)->lpVtbl -> CancelService(This,ulStrmHandle,ulServiceHandle)


#define IEpData_AcknowledgePoints(This,ulStrmHandle,ulArraySize,ackData)	\
    (This)->lpVtbl -> AcknowledgePoints(This,ulStrmHandle,ulArraySize,ackData)

#define IEpData_SetPoint(This,ulStrmHandle,bstrName,ctType,eptData)	\
    (This)->lpVtbl -> SetPoint(This,ulStrmHandle,bstrName,ctType,eptData)

#define IEpData_GetPointInfo(This,ulStrmHandle,bstrName,peptData)	\
    (This)->lpVtbl -> GetPointInfo(This,ulStrmHandle,bstrName,peptData)

#define IEpData_RefreshLimitOffService(This,ulStrmHandle,ulServiceHandle)	\
    (This)->lpVtbl -> RefreshLimitOffService(This,ulStrmHandle,ulServiceHandle)

#define IEpData_RequestPoint(This,ulStrmHandle,bstrName,ctType,peptData)	\
    (This)->lpVtbl -> RequestPoint(This,ulStrmHandle,bstrName,ctType,peptData)

#define IEpData_RequestPointService(This,ulStrmHandle,bstrName,ctType,cctType,smtType,lAsyncUP,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestPointService(This,ulStrmHandle,bstrName,ctType,cctType,smtType,lAsyncUP,ulUserData,pServiceHandle)

#define IEpData_RequestFrameService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestFrameService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpData_RequestLimitOffService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestLimitOffService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpData_RequestLimitService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestLimitService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpData_RequestDBModService(This,ulStrmHandle,bstrName,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestDBModService(This,ulStrmHandle,bstrName,ulUserData,pServiceHandle)

#define IEpData_WriteDBModsToFile(This,ulStrmHandle,bstrFile)	\
    (This)->lpVtbl -> WriteDBModsToFile(This,ulStrmHandle,bstrFile)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_AcknowledgePoints_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulArraySize,
    /* [size_is][in] */ EPACKINFO __RPC_FAR *ackData);


void __RPC_STUB IEpData_AcknowledgePoints_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_SetPoint_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrName,
    /* [in] */ EPCONVTYPE ctType,
    /* [in] */ EPPTDATA eptData);


void __RPC_STUB IEpData_SetPoint_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_GetPointInfo_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrName,
    /* [retval][out] */ EPPTDATA __RPC_FAR *peptData);


void __RPC_STUB IEpData_GetPointInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_RefreshLimitOffService_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulServiceHandle);


void __RPC_STUB IEpData_RefreshLimitOffService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_RequestPoint_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrName,
    /* [in] */ EPCONVTYPE ctType,
    /* [retval][out] */ EPPTDATA __RPC_FAR *peptData);


void __RPC_STUB IEpData_RequestPoint_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_RequestPointService_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrName,
    /* [in] */ EPCONVTYPE ctType,
    /* [in] */ EPCBCOND cctType,
    /* [in] */ EPSERVICEMODE smtType,
    /* [in] */ LONG lAsyncUP,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpData_RequestPointService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_RequestFrameService_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpData_RequestFrameService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_RequestLimitOffService_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpData_RequestLimitOffService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_RequestLimitService_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpData_RequestLimitService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_RequestDBModService_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrName,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpData_RequestDBModService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpData_WriteDBModsToFile_Proxy( 
    IEpData __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrFile);


void __RPC_STUB IEpData_WriteDBModsToFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpData_INTERFACE_DEFINED__ */


#ifndef __IEpEvent_INTERFACE_DEFINED__
#define __IEpEvent_INTERFACE_DEFINED__

/* interface IEpEvent */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEvent;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("33EB50D1-B78B-11D2-A336-00600867A0E2")
    IEpEvent : public IEpSrvcBase
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SendEvent( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulMessageNum,
            /* [in] */ BSTR bstrMessage) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestEventService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ EPEVTSERVICE est,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEventVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEvent __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEvent __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEvent __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConnectStream )( 
            IEpEvent __RPC_FAR * This,
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ DWORD dwCBCookie,
            /* [retval][out] */ ULONG __RPC_FAR *pulStrmHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DisconnectStream )( 
            IEpEvent __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestConStat )( 
            IEpEvent __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [retval][out] */ EPSTATUSDATA __RPC_FAR *pStatusData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestConStatService )( 
            IEpEvent __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CancelService )( 
            IEpEvent __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SendEvent )( 
            IEpEvent __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulMessageNum,
            /* [in] */ BSTR bstrMessage);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestEventService )( 
            IEpEvent __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ EPEVTSERVICE est,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        END_INTERFACE
    } IEpEventVtbl;

    interface IEpEvent
    {
        CONST_VTBL struct IEpEventVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEvent_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEvent_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEvent_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEvent_ConnectStream(This,bstrStrmName,dwCBCookie,pulStrmHandle)	\
    (This)->lpVtbl -> ConnectStream(This,bstrStrmName,dwCBCookie,pulStrmHandle)

#define IEpEvent_DisconnectStream(This,ulStrmHandle)	\
    (This)->lpVtbl -> DisconnectStream(This,ulStrmHandle)

#define IEpEvent_RequestConStat(This,ulStrmHandle,pStatusData)	\
    (This)->lpVtbl -> RequestConStat(This,ulStrmHandle,pStatusData)

#define IEpEvent_RequestConStatService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestConStatService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpEvent_CancelService(This,ulStrmHandle,ulServiceHandle)	\
    (This)->lpVtbl -> CancelService(This,ulStrmHandle,ulServiceHandle)


#define IEpEvent_SendEvent(This,ulStrmHandle,ulMessageNum,bstrMessage)	\
    (This)->lpVtbl -> SendEvent(This,ulStrmHandle,ulMessageNum,bstrMessage)

#define IEpEvent_RequestEventService(This,ulStrmHandle,est,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestEventService(This,ulStrmHandle,est,ulUserData,pServiceHandle)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEvent_SendEvent_Proxy( 
    IEpEvent __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulMessageNum,
    /* [in] */ BSTR bstrMessage);


void __RPC_STUB IEpEvent_SendEvent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEvent_RequestEventService_Proxy( 
    IEpEvent __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ EPEVTSERVICE est,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpEvent_RequestEventService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEvent_INTERFACE_DEFINED__ */


#ifndef __IEpSTOL_INTERFACE_DEFINED__
#define __IEpSTOL_INTERFACE_DEFINED__

/* interface IEpSTOL */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpSTOL;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A8BA1B3B-B76F-11D2-A334-00600867A0E2")
    IEpSTOL : public IEpSrvcBase
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ExecuteSTOLWait( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrDirective,
            /* [retval][out] */ EPSTOLRESP __RPC_FAR *psrResponse) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ExecuteSTOLNoWait( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrDirective,
            /* [in] */ SHORT sPriority,
            /* [in] */ ULONG ulUserData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RegisterRouter( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ IEpDirRtr __RPC_FAR *pIEpDirRtr,
            /* [in] */ BSTR bstrPCUser) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestCLIMStatusService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestCmdStatusService( 
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpSTOLVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpSTOL __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpSTOL __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConnectStream )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ DWORD dwCBCookie,
            /* [retval][out] */ ULONG __RPC_FAR *pulStrmHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DisconnectStream )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestConStat )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [retval][out] */ EPSTATUSDATA __RPC_FAR *pStatusData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestConStatService )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CancelService )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExecuteSTOLWait )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrDirective,
            /* [retval][out] */ EPSTOLRESP __RPC_FAR *psrResponse);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExecuteSTOLNoWait )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ BSTR bstrDirective,
            /* [in] */ SHORT sPriority,
            /* [in] */ ULONG ulUserData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RegisterRouter )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ IEpDirRtr __RPC_FAR *pIEpDirRtr,
            /* [in] */ BSTR bstrPCUser);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestCLIMStatusService )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestCmdStatusService )( 
            IEpSTOL __RPC_FAR * This,
            /* [in] */ ULONG ulStrmHandle,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        END_INTERFACE
    } IEpSTOLVtbl;

    interface IEpSTOL
    {
        CONST_VTBL struct IEpSTOLVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpSTOL_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpSTOL_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpSTOL_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpSTOL_ConnectStream(This,bstrStrmName,dwCBCookie,pulStrmHandle)	\
    (This)->lpVtbl -> ConnectStream(This,bstrStrmName,dwCBCookie,pulStrmHandle)

#define IEpSTOL_DisconnectStream(This,ulStrmHandle)	\
    (This)->lpVtbl -> DisconnectStream(This,ulStrmHandle)

#define IEpSTOL_RequestConStat(This,ulStrmHandle,pStatusData)	\
    (This)->lpVtbl -> RequestConStat(This,ulStrmHandle,pStatusData)

#define IEpSTOL_RequestConStatService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestConStatService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpSTOL_CancelService(This,ulStrmHandle,ulServiceHandle)	\
    (This)->lpVtbl -> CancelService(This,ulStrmHandle,ulServiceHandle)


#define IEpSTOL_ExecuteSTOLWait(This,ulStrmHandle,bstrDirective,psrResponse)	\
    (This)->lpVtbl -> ExecuteSTOLWait(This,ulStrmHandle,bstrDirective,psrResponse)

#define IEpSTOL_ExecuteSTOLNoWait(This,ulStrmHandle,bstrDirective,sPriority,ulUserData)	\
    (This)->lpVtbl -> ExecuteSTOLNoWait(This,ulStrmHandle,bstrDirective,sPriority,ulUserData)

#define IEpSTOL_RegisterRouter(This,ulStrmHandle,pIEpDirRtr,bstrPCUser)	\
    (This)->lpVtbl -> RegisterRouter(This,ulStrmHandle,pIEpDirRtr,bstrPCUser)

#define IEpSTOL_RequestCLIMStatusService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestCLIMStatusService(This,ulStrmHandle,ulUserData,pServiceHandle)

#define IEpSTOL_RequestCmdStatusService(This,ulStrmHandle,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestCmdStatusService(This,ulStrmHandle,ulUserData,pServiceHandle)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSTOL_ExecuteSTOLWait_Proxy( 
    IEpSTOL __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrDirective,
    /* [retval][out] */ EPSTOLRESP __RPC_FAR *psrResponse);


void __RPC_STUB IEpSTOL_ExecuteSTOLWait_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSTOL_ExecuteSTOLNoWait_Proxy( 
    IEpSTOL __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ BSTR bstrDirective,
    /* [in] */ SHORT sPriority,
    /* [in] */ ULONG ulUserData);


void __RPC_STUB IEpSTOL_ExecuteSTOLNoWait_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSTOL_RegisterRouter_Proxy( 
    IEpSTOL __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ IEpDirRtr __RPC_FAR *pIEpDirRtr,
    /* [in] */ BSTR bstrPCUser);


void __RPC_STUB IEpSTOL_RegisterRouter_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSTOL_RequestCLIMStatusService_Proxy( 
    IEpSTOL __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSTOL_RequestCLIMStatusService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpSTOL_RequestCmdStatusService_Proxy( 
    IEpSTOL __RPC_FAR * This,
    /* [in] */ ULONG ulStrmHandle,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpSTOL_RequestCmdStatusService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpSTOL_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_epcommon_0230 */
/* [local] */ 

typedef struct  tagEPSTRMINFO
    {
    ULONG eChgHint;
    FILETIME ftReceiptTime;
    BSTR bstrStrmName;
    BSTR bstrHost;
    BSTR bstrDBName;
    EPSTRMSTAT eStatus;
    EPSTRMTYPE eType;
    }	EPSTRMINFO;

typedef struct  _EPSTRMINFO_ALL
    {
    ULONG ulArrySize;
    /* [size_is] */ EPSTRMINFO __RPC_FAR *pArryStrmInfo;
    }	EPSTRMINFO_ALL;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0230_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0230_v0_0_s_ifspec;

#ifndef ___IEpStrmMonCB_INTERFACE_DEFINED__
#define ___IEpStrmMonCB_INTERFACE_DEFINED__

/* interface _IEpStrmMonCB */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID__IEpStrmMonCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B8D8E005-DD56-11D2-A33C-00600867A0E2")
    _IEpStrmMonCB : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE StrmInfoSrvcCB( 
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPSTRMINFO __RPC_FAR *siData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE StrmMessageSrvcCB( 
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ BSTR __RPC_FAR *bstrData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct _IEpStrmMonCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            _IEpStrmMonCB __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            _IEpStrmMonCB __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            _IEpStrmMonCB __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *StrmInfoSrvcCB )( 
            _IEpStrmMonCB __RPC_FAR * This,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPSTRMINFO __RPC_FAR *siData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *StrmMessageSrvcCB )( 
            _IEpStrmMonCB __RPC_FAR * This,
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ BSTR __RPC_FAR *bstrData);
        
        END_INTERFACE
    } _IEpStrmMonCBVtbl;

    interface _IEpStrmMonCB
    {
        CONST_VTBL struct _IEpStrmMonCBVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IEpStrmMonCB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IEpStrmMonCB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IEpStrmMonCB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IEpStrmMonCB_StrmInfoSrvcCB(This,ulArrySize,siData)	\
    (This)->lpVtbl -> StrmInfoSrvcCB(This,ulArrySize,siData)

#define _IEpStrmMonCB_StrmMessageSrvcCB(This,ulUserData,ulArrySize,bstrData)	\
    (This)->lpVtbl -> StrmMessageSrvcCB(This,ulUserData,ulArrySize,bstrData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpStrmMonCB_StrmInfoSrvcCB_Proxy( 
    _IEpStrmMonCB __RPC_FAR * This,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPSTRMINFO __RPC_FAR *siData);


void __RPC_STUB _IEpStrmMonCB_StrmInfoSrvcCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpStrmMonCB_StrmMessageSrvcCB_Proxy( 
    _IEpStrmMonCB __RPC_FAR * This,
    /* [in] */ ULONG ulUserData,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ BSTR __RPC_FAR *bstrData);


void __RPC_STUB _IEpStrmMonCB_StrmMessageSrvcCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* ___IEpStrmMonCB_INTERFACE_DEFINED__ */


#ifndef __IEpStrmMon_INTERFACE_DEFINED__
#define __IEpStrmMon_INTERFACE_DEFINED__

/* interface IEpStrmMon */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpStrmMon;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("B8D8E003-DD56-11D2-A33C-00600867A0E2")
    IEpStrmMon : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CancelService( 
            /* [in] */ ULONG ulServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ConnectHost( 
            /* [in] */ BSTR bstrHostName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ConnectStream( 
            /* [in] */ BSTR bstrStrmName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE DisconnectHost( 
            /* [in] */ BSTR HostName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE DisconnectStream( 
            /* [in] */ BSTR bstrStrmName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetStrmInfo( 
            /* [in] */ BSTR bstrStrmName,
            /* [retval][out] */ EPSTRMINFO __RPC_FAR *pStrmInfo) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetAllStrmInfo( 
            /* [retval][out] */ EPSTRMINFO_ALL __RPC_FAR *pStrmInfoAll) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestMessageSrvc( 
            /* [in] */ DWORD dwCBCookie,
            /* [in] */ int nHistory,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestStrmInfoSrvc( 
            /* [in] */ DWORD dwCBCookie) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE StartStrm( 
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ BSTR bstrHost,
            /* [in] */ BSTR bstrDBName,
            /* [retval][out] */ int __RPC_FAR *nStreamStatus) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE StopStrm( 
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ BSTR bstrHost,
            /* [retval][out] */ int __RPC_FAR *nStreamStatus) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpStrmMonVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpStrmMon __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpStrmMon __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CancelService )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ ULONG ulServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConnectHost )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ BSTR bstrHostName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConnectStream )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ BSTR bstrStrmName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DisconnectHost )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ BSTR HostName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DisconnectStream )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ BSTR bstrStrmName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetStrmInfo )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ BSTR bstrStrmName,
            /* [retval][out] */ EPSTRMINFO __RPC_FAR *pStrmInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetAllStrmInfo )( 
            IEpStrmMon __RPC_FAR * This,
            /* [retval][out] */ EPSTRMINFO_ALL __RPC_FAR *pStrmInfoAll);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestMessageSrvc )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ DWORD dwCBCookie,
            /* [in] */ int nHistory,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestStrmInfoSrvc )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ DWORD dwCBCookie);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *StartStrm )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ BSTR bstrHost,
            /* [in] */ BSTR bstrDBName,
            /* [retval][out] */ int __RPC_FAR *nStreamStatus);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *StopStrm )( 
            IEpStrmMon __RPC_FAR * This,
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ BSTR bstrHost,
            /* [retval][out] */ int __RPC_FAR *nStreamStatus);
        
        END_INTERFACE
    } IEpStrmMonVtbl;

    interface IEpStrmMon
    {
        CONST_VTBL struct IEpStrmMonVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpStrmMon_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpStrmMon_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpStrmMon_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpStrmMon_CancelService(This,ulServiceHandle)	\
    (This)->lpVtbl -> CancelService(This,ulServiceHandle)

#define IEpStrmMon_ConnectHost(This,bstrHostName)	\
    (This)->lpVtbl -> ConnectHost(This,bstrHostName)

#define IEpStrmMon_ConnectStream(This,bstrStrmName)	\
    (This)->lpVtbl -> ConnectStream(This,bstrStrmName)

#define IEpStrmMon_DisconnectHost(This,HostName)	\
    (This)->lpVtbl -> DisconnectHost(This,HostName)

#define IEpStrmMon_DisconnectStream(This,bstrStrmName)	\
    (This)->lpVtbl -> DisconnectStream(This,bstrStrmName)

#define IEpStrmMon_GetStrmInfo(This,bstrStrmName,pStrmInfo)	\
    (This)->lpVtbl -> GetStrmInfo(This,bstrStrmName,pStrmInfo)

#define IEpStrmMon_GetAllStrmInfo(This,pStrmInfoAll)	\
    (This)->lpVtbl -> GetAllStrmInfo(This,pStrmInfoAll)

#define IEpStrmMon_RequestMessageSrvc(This,dwCBCookie,nHistory,ulUserData,pServiceHandle)	\
    (This)->lpVtbl -> RequestMessageSrvc(This,dwCBCookie,nHistory,ulUserData,pServiceHandle)

#define IEpStrmMon_RequestStrmInfoSrvc(This,dwCBCookie)	\
    (This)->lpVtbl -> RequestStrmInfoSrvc(This,dwCBCookie)

#define IEpStrmMon_StartStrm(This,bstrStrmName,bstrHost,bstrDBName,nStreamStatus)	\
    (This)->lpVtbl -> StartStrm(This,bstrStrmName,bstrHost,bstrDBName,nStreamStatus)

#define IEpStrmMon_StopStrm(This,bstrStrmName,bstrHost,nStreamStatus)	\
    (This)->lpVtbl -> StopStrm(This,bstrStrmName,bstrHost,nStreamStatus)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_CancelService_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [in] */ ULONG ulServiceHandle);


void __RPC_STUB IEpStrmMon_CancelService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_ConnectHost_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [in] */ BSTR bstrHostName);


void __RPC_STUB IEpStrmMon_ConnectHost_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_ConnectStream_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [in] */ BSTR bstrStrmName);


void __RPC_STUB IEpStrmMon_ConnectStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_DisconnectHost_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [in] */ BSTR HostName);


void __RPC_STUB IEpStrmMon_DisconnectHost_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_DisconnectStream_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [in] */ BSTR bstrStrmName);


void __RPC_STUB IEpStrmMon_DisconnectStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_GetStrmInfo_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [in] */ BSTR bstrStrmName,
    /* [retval][out] */ EPSTRMINFO __RPC_FAR *pStrmInfo);


void __RPC_STUB IEpStrmMon_GetStrmInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_GetAllStrmInfo_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [retval][out] */ EPSTRMINFO_ALL __RPC_FAR *pStrmInfoAll);


void __RPC_STUB IEpStrmMon_GetAllStrmInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_RequestMessageSrvc_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [in] */ DWORD dwCBCookie,
    /* [in] */ int nHistory,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pServiceHandle);


void __RPC_STUB IEpStrmMon_RequestMessageSrvc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_RequestStrmInfoSrvc_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [in] */ DWORD dwCBCookie);


void __RPC_STUB IEpStrmMon_RequestStrmInfoSrvc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_StartStrm_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [in] */ BSTR bstrStrmName,
    /* [in] */ BSTR bstrHost,
    /* [in] */ BSTR bstrDBName,
    /* [retval][out] */ int __RPC_FAR *nStreamStatus);


void __RPC_STUB IEpStrmMon_StartStrm_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpStrmMon_StopStrm_Proxy( 
    IEpStrmMon __RPC_FAR * This,
    /* [in] */ BSTR bstrStrmName,
    /* [in] */ BSTR bstrHost,
    /* [retval][out] */ int __RPC_FAR *nStreamStatus);


void __RPC_STUB IEpStrmMon_StopStrm_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpStrmMon_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_epcommon_0232 */
/* [local] */ 

#define EPMAX_CONV_COEFF	8
typedef struct  EpDbCalibrationDescription
    {
    double dContextLow;
    double dContextHigh;
    int nNumCoeff;
    BSTR szUnit;
    /* [size_is] */ double __RPC_FAR *dRaw;
    /* [size_is] */ double __RPC_FAR *dEU;
    }	EpDbCalibrationDescription;

typedef struct  EpDbConversionDescription
    {
    double dContextLow;
    double dContextHigh;
    int nNumCoeff;
    BSTR szUnit;
    double dCoeff[ 8 ];
    }	EpDbConversionDescription;

typedef struct  EpDbDeltaDescription
    {
    double dContextLow;
    double dContextHigh;
    double dLow;
    double dHigh;
    int nLowMsg;
    int nHighMsg;
    EPLIMITVALUE nValueType;
    }	EpDbDeltaDescription;

typedef struct  EpDbLimitGlobal
    {
    EPLIMITSTATUS nStatus;
    double dStatusLow;
    double dStatusHigh;
    int nStatusMsg;
    }	EpDbLimitGlobal;

typedef struct  EpDbLimitTlm
    {
    double dLowRed;
    double dLowYellow;
    double dHighYellow;
    double dHighRed;
    int nLowRedMsg;
    int nLowYellowMsg;
    int nNormalMsg;
    int nHighYellowMsg;
    int nHighRedMsg;
    EPLIMITRANGE nRangeType;
    EPLIMITVALUE nValueType;
    int nLimitThreshold;
    }	EpDbLimitTlm;

typedef struct  EpDbLimitDescription
    {
    EPPTVARIETY nPointVariety;
    double dContextLow;
    double dContextHigh;
    /* [switch_is] */ /* [switch_type] */ union 
        {
        /* [case()] */  /* Empty union arm */ 
        /* [case()] */ EpDbLimitGlobal sGlobal;
        /* [case()] */ EpDbLimitTlm sTlm;
        }	;
    }	EpDbLimitDescription;

typedef struct  EpDbLocationDescription
    {
    BSTR bstrIndexName;
    BSTR bstrModeName;
    ULONG ulModeValue;
    int nSubcomDepth;
    int nSupercomDepth;
    int nStartBit;
    int nNumBits;
    int nDestBit;
    }	EpDbLocationDescription;

typedef struct  EpDbPointDescription
    {
    BSTR bstrMnemonic;
    BSTR bstrDescription;
    BSTR bstrUnits;
    BSTR bstrAlias;
    BSTR bstrContextName;
    EPPTVARIETY nPointVariety;
    EPDBDATATYPE nDataType;
    int nDataSize;
    EPDISPCONV nDefaultConversion;
    ULONG ulAllConversions;
    int nSamplingPeriod;
    BOOL bInverted;
    BOOL bReversed;
    BOOL bCritical;
    EPTLMTYPE nTlmType;
    int nIndex;
    int nHelpIndex;
    }	EpDbPointDescription;

typedef struct  EpDbStateDescription
    {
    BSTR bstrName;
    int nEventNum;
    double dLimitLow;
    double dLimitHigh;
    double dContextLow;
    double dContextHigh;
    }	EpDbStateDescription;

typedef struct  EpDbSubsystemDescription
    {
    EPSUBSYSTEM nType;
    BSTR bstrName;
    BSTR bstrDescription;
    int nIndex;
    }	EpDbSubsystemDescription;

typedef struct  EpDbEventDescription
    {
    int nEventNumber;
    BSTR bstrEventText;
    BSTR bstrEventName;
    EPSYSTEMTYPE eSystem;
    EPSUBSYSTEMTYPE eSubSystem;
    EPINDEXTYPE eIndexType;
    EPEVTMSG eMsgType;
    EPLIMITSTATUS eStatusType;
    }	EpDbEventDescription;

typedef struct  EpDbTlmLimMsgDescription
    {
    int nMsgID;
    BSTR bstrMessage;
    }	EpDbTlmLimMsgDescription;

typedef struct  EpDbCmdDescription
    {
    BSTR bstrMnemonic;
    BSTR bstrDescription;
    EPCMDTYPE nCmdType;
    BSTR bstrAlias;
    int nNumDatawords;
    BOOL bCritical;
    BOOL bProtected;
    int nRexmits;
    int nPulseWidth;
    BSTR bstrExecMnemonic;
    int nUplinkCode;
    BSTR bstrCmdFormat;
    int nHelpIndex;
    BSTR bstrSpacecraftAddr;
    LONG lCmdCntIncrement;
    int nCmdCntDuration;
    int nNumDatawordArgs;
    int nNumReqDatawordArgs;
    BSTR bstrPrivilegeGroup;
    int nIndex;
    }	EpDbCmdDescription;

typedef struct  EpDbCmdDatawordDescription
    {
    int nLength;
    int nSequenceNum;
    BSTR bstrDescription;
    BSTR bstrValue;
    }	EpDbCmdDatawordDescription;

typedef struct  EpDbCmdDatawordArgDescription
    {
    int nSequenceNum;
    BSTR bstrKeyword;
    BSTR bstrMnemonic;
    BSTR bstrDescription;
    EPDATAWORDARGTYPE nType;
    int nDatawordSequenceNum;
    int nDestBit;
    int nNumBits;
    int nPresetAndMask;
    int nPresetXorMask;
    BOOL bRequired;
    EPTLMTYPE nNumType;
    EPVALUESELECTIONTYPE nValueSelection;
    }	EpDbCmdDatawordArgDescription;

typedef struct  EpDbCmdDatawordArgValueDescription
    {
    BSTR bstrMnemonic;
    BSTR bstrDescription;
    BOOL bDefaultValue;
    ULONG ulValue;
    }	EpDbCmdDatawordArgValueDescription;

typedef struct  EpDbCmdDatawordArgRangeDescription
    {
    BSTR bstrDescription;
    double dblLow;
    double dblHigh;
    }	EpDbCmdDatawordArgRangeDescription;

typedef struct  EpDbTvStateDescription
    {
    BSTR bstrName;
    BSTR bstrDescription;
    BOOL bVerifyAny;
    EPTVSTATETYPE nType;
    }	EpDbTvStateDescription;

typedef struct  EpDbTvContextDescription
    {
    BSTR bstrName;
    BSTR bstrDescription;
    double dblLow;
    double dblHigh;
    BSTR bstrContext;
    }	EpDbTvContextDescription;

typedef struct  EpDbTvSequenceDescription
    {
    BSTR bstrMnemonic;
    BSTR bstrContext;
    }	EpDbTvSequenceDescription;

typedef struct  EpDbTvEventDescription
    {
    TLMVERIFICATIONTYPE nTVType;
    EPDBVALUETYPE nValueType;
    int nDuration;
    int nDelay;
    BOOL bOneShot;
    EPDBCMDTVEVTDATATYPE nDataType;
    double dblRangeLow;
    double dblRangeHigh;
    double dblBoolValue;
    BSTR bstrBoolId;
    BSTR bstrBoolDat1;
    BSTR bstrBoolDat2;
    double dblPseudoSetValue;
    ULONG ulPseudoClearMask;
    int nComStartBit;
    int nComStopBit;
    double dblComValue;
    double dblLvValue;
    BSTR bstrLvId;
    double dblIncPseudoValue;
    double dblIncPseudoLimit;
    double dblIncPseudoReset;
    ULONG ulMaskedCompareMask;
    ULONG ulMaskedCompareValue;
    }	EpDbTvEventDescription;

typedef struct  EpDbCmdChannelDescription
    {
    BSTR bstrUplinkProcessorName;
    BSTR bstrHigherLevelProcessorName;
    BSTR bstrUplinkProcessorMnemonic;
    BSTR bstrDataServiceName;
    BSTR bstrCmdCntMnemonic;
    BSTR bstrCmdEchoMnemonic;
    BOOL bBypassNullCmdEcho;
    BOOL bCmdEchoRepeat;
    BSTR bstrFrameSeqMnemonic;
    BSTR bstrFrameRcvMnemonic;
    BSTR bstrMissingFrameMnemonic;
    BSTR bstrFrameCntMnemonic;
    LONG lFrameRcvOffset;
    int nFrameSeqRollover;
    LONG lFrameCntIncrement;
    int nDuration;
    }	EpDbCmdChannelDescription;

typedef struct  EpDbCmdFormatDescription
    {
    BSTR bstrName;
    BSTR bstrDescription;
    BSTR bstrUplinkCodeMnemonic;
    BSTR bstrCmdFrameMnemonic;
    BSTR bstrUserDataMnemonic;
    BSTR bstrUplinkProcessorName;
    int nNumDatawords;
    }	EpDbCmdFormatDescription;

typedef struct  EpDbScAddrDescription
    {
    BSTR bstrAddrMnemonic;
    ULONG ulAddrCode;
    BOOL bDefaultAddr;
    BSTR bstrCmdEchoMnemonic;
    BOOL bBypassNullCmdEcho;
    BOOL bCmdEchoRepeat;
    }	EpDbScAddrDescription;

typedef struct  EpDbCmdExecVerifierDescription
    {
    BSTR bstrTlmMnemonic;
    BSTR bstrSimContextMnemonic;
    ULONG ulSimContextValue;
    int nDuration;
    }	EpDbCmdExecVerifierDescription;

typedef struct  EpDbCmdDatawordMapDescription
    {
    BSTR bstrDescription;
    BSTR bstrDatawordMnemonic;
    int nDatawordSeqNum;
    }	EpDbCmdDatawordMapDescription;

typedef struct  EpDbCmdFmtPointValueDescription
    {
    BSTR bstrDescription;
    BSTR bstrMnemonic;
    ULONG ulValue;
    int nIncrement;
    }	EpDbCmdFmtPointValueDescription;

typedef struct  EpDbCmdFrameFormatDescription
    {
    BSTR bstrCmdEchoMnemonic;
    BSTR bstrCmdEchoDescription;
    int nCmdEchoDuration;
    }	EpDbCmdFrameFormatDescription;

typedef struct  EpDbCmdDatawordGroupDescription
    {
    int nGroupIndex;
    BOOL bRequired;
    BSTR bstrDescription;
    }	EpDbCmdDatawordGroupDescription;

typedef struct  EpDbCmdPrivilegeGroupDescription
    {
    BSTR bstrGroup;
    }	EpDbCmdPrivilegeGroupDescription;

typedef struct  EpDbScFormatDescription
    {
    BSTR bstrCmdCntMnemonic;
    BSTR bstrCmdEchoMnemonic;
    BSTR bstrFrameFmtMnemonic;
    BSTR bstrFrameCtrMnemonic;
    BSTR bstrCmdDumpMnemonic;
    BSTR bstrCmdFrameMnemonic;
    BSTR bstrCmdXmittedFrameMnemonic;
    BSTR bstrEncryptFlagMnemonic;
    BSTR bstrUplinkProcessorMnemonic;
    BOOL bBypassNullCmdEcho;
    EPDBCMDMODETYPE nCmdMode;
    EPDBCMDPATHTYPE nCmdPath;
    EPDBCMDSCCLASSTYPE nScClass;
    }	EpDbScFormatDescription;

typedef struct  EpDbCmdEchoVerifierEventDescription
    {
    EPCMDECHOTYPE nType;
    ULONG ulValue;
    int nDatawordSeqNum;
    ULONG ulAndMask;
    ULONG ulXorMask;
    int nStartBit;
    int nNumBits;
    int nReverseBit;
    BSTR bstrGVName;
    }	EpDbCmdEchoVerifierEventDescription;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0232_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0232_v0_0_s_ifspec;

#ifndef __IEpEnumCalibrations_INTERFACE_DEFINED__
#define __IEpEnumCalibrations_INTERFACE_DEFINED__

/* interface IEpEnumCalibrations */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCalibrations;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7B843D42-0234-11d3-803B-006008C0949C")
    IEpEnumCalibrations : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCalibrationDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCalibrationsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCalibrations __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCalibrations __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCalibrations __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCalibrations __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCalibrationDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCalibrations __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumCalibrationsVtbl;

    interface IEpEnumCalibrations
    {
        CONST_VTBL struct IEpEnumCalibrationsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCalibrations_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCalibrations_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCalibrations_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCalibrations_Next(This,cElements,pDescription,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDescription,pcFetched)

#define IEpEnumCalibrations_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCalibrations_Next_Proxy( 
    IEpEnumCalibrations __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCalibrationDescription __RPC_FAR *pDescription,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCalibrations_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCalibrations_Skip_Proxy( 
    IEpEnumCalibrations __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCalibrations_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCalibrations_INTERFACE_DEFINED__ */


#ifndef __IEpEnumConversions_INTERFACE_DEFINED__
#define __IEpEnumConversions_INTERFACE_DEFINED__

/* interface IEpEnumConversions */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumConversions;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7B843D44-0234-11d3-803B-006008C0949C")
    IEpEnumConversions : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbConversionDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumConversionsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumConversions __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumConversions __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumConversions __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumConversions __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbConversionDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumConversions __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumConversionsVtbl;

    interface IEpEnumConversions
    {
        CONST_VTBL struct IEpEnumConversionsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumConversions_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumConversions_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumConversions_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumConversions_Next(This,cElements,pDescription,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDescription,pcFetched)

#define IEpEnumConversions_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumConversions_Next_Proxy( 
    IEpEnumConversions __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbConversionDescription __RPC_FAR *pDescription,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumConversions_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumConversions_Skip_Proxy( 
    IEpEnumConversions __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumConversions_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumConversions_INTERFACE_DEFINED__ */


#ifndef __IEpEnumDeltas_INTERFACE_DEFINED__
#define __IEpEnumDeltas_INTERFACE_DEFINED__

/* interface IEpEnumDeltas */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumDeltas;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EF8ECA00-B260-11d3-8060-006008C0949C")
    IEpEnumDeltas : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbDeltaDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumDeltasVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumDeltas __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumDeltas __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumDeltas __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumDeltas __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbDeltaDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumDeltas __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumDeltasVtbl;

    interface IEpEnumDeltas
    {
        CONST_VTBL struct IEpEnumDeltasVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumDeltas_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumDeltas_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumDeltas_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumDeltas_Next(This,cElements,pDescription,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDescription,pcFetched)

#define IEpEnumDeltas_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDeltas_Next_Proxy( 
    IEpEnumDeltas __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbDeltaDescription __RPC_FAR *pDescription,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumDeltas_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDeltas_Skip_Proxy( 
    IEpEnumDeltas __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumDeltas_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumDeltas_INTERFACE_DEFINED__ */


#ifndef __IEpEnumLimits_INTERFACE_DEFINED__
#define __IEpEnumLimits_INTERFACE_DEFINED__

/* interface IEpEnumLimits */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumLimits;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7B843D40-0234-11d3-803B-006008C0949C")
    IEpEnumLimits : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbLimitDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumLimitsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumLimits __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumLimits __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumLimits __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumLimits __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbLimitDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumLimits __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumLimitsVtbl;

    interface IEpEnumLimits
    {
        CONST_VTBL struct IEpEnumLimitsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumLimits_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumLimits_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumLimits_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumLimits_Next(This,cElements,pDescription,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDescription,pcFetched)

#define IEpEnumLimits_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumLimits_Next_Proxy( 
    IEpEnumLimits __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbLimitDescription __RPC_FAR *pDescription,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumLimits_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumLimits_Skip_Proxy( 
    IEpEnumLimits __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumLimits_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumLimits_INTERFACE_DEFINED__ */


#ifndef __IEpEnumLocations_INTERFACE_DEFINED__
#define __IEpEnumLocations_INTERFACE_DEFINED__

/* interface IEpEnumLocations */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumLocations;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7B843D41-0234-11d3-803B-006008C0949C")
    IEpEnumLocations : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbLocationDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumLocationsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumLocations __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumLocations __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumLocations __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumLocations __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbLocationDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumLocations __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumLocationsVtbl;

    interface IEpEnumLocations
    {
        CONST_VTBL struct IEpEnumLocationsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumLocations_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumLocations_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumLocations_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumLocations_Next(This,cElements,pDescription,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDescription,pcFetched)

#define IEpEnumLocations_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumLocations_Next_Proxy( 
    IEpEnumLocations __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbLocationDescription __RPC_FAR *pDescription,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumLocations_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumLocations_Skip_Proxy( 
    IEpEnumLocations __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumLocations_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumLocations_INTERFACE_DEFINED__ */


#ifndef __IEpEnumStates_INTERFACE_DEFINED__
#define __IEpEnumStates_INTERFACE_DEFINED__

/* interface IEpEnumStates */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumStates;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("45643C33-0172-11d3-803B-006008C0949C")
    IEpEnumStates : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbStateDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumStatesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumStates __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumStates __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumStates __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumStates __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbStateDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumStates __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumStatesVtbl;

    interface IEpEnumStates
    {
        CONST_VTBL struct IEpEnumStatesVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumStates_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumStates_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumStates_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumStates_Next(This,cElements,pDescription,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDescription,pcFetched)

#define IEpEnumStates_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumStates_Next_Proxy( 
    IEpEnumStates __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbStateDescription __RPC_FAR *pDescription,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumStates_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumStates_Skip_Proxy( 
    IEpEnumStates __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumStates_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumStates_INTERFACE_DEFINED__ */


#ifndef __IEpEnumPoints_INTERFACE_DEFINED__
#define __IEpEnumPoints_INTERFACE_DEFINED__

/* interface IEpEnumPoints */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumPoints;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("45643C32-0172-11d3-803B-006008C0949C")
    IEpEnumPoints : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbPointDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCalibrationEnum( 
            /* [retval][out] */ IEpEnumCalibrations __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetConversionEnum( 
            /* [retval][out] */ IEpEnumConversions __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDeltaEnum( 
            /* [retval][out] */ IEpEnumDeltas __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetLimitEnum( 
            /* [retval][out] */ IEpEnumLimits __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetLocationEnum( 
            /* [retval][out] */ IEpEnumLocations __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetStateEnum( 
            /* [retval][out] */ IEpEnumStates __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumPointsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumPoints __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumPoints __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumPoints __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumPoints __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbPointDescription __RPC_FAR *pDescription,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumPoints __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCalibrationEnum )( 
            IEpEnumPoints __RPC_FAR * This,
            /* [retval][out] */ IEpEnumCalibrations __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetConversionEnum )( 
            IEpEnumPoints __RPC_FAR * This,
            /* [retval][out] */ IEpEnumConversions __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDeltaEnum )( 
            IEpEnumPoints __RPC_FAR * This,
            /* [retval][out] */ IEpEnumDeltas __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetLimitEnum )( 
            IEpEnumPoints __RPC_FAR * This,
            /* [retval][out] */ IEpEnumLimits __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetLocationEnum )( 
            IEpEnumPoints __RPC_FAR * This,
            /* [retval][out] */ IEpEnumLocations __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetStateEnum )( 
            IEpEnumPoints __RPC_FAR * This,
            /* [retval][out] */ IEpEnumStates __RPC_FAR *__RPC_FAR *ppEnum);
        
        END_INTERFACE
    } IEpEnumPointsVtbl;

    interface IEpEnumPoints
    {
        CONST_VTBL struct IEpEnumPointsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumPoints_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumPoints_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumPoints_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumPoints_Next(This,cElements,pDescription,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDescription,pcFetched)

#define IEpEnumPoints_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumPoints_GetCalibrationEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetCalibrationEnum(This,ppEnum)

#define IEpEnumPoints_GetConversionEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetConversionEnum(This,ppEnum)

#define IEpEnumPoints_GetDeltaEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetDeltaEnum(This,ppEnum)

#define IEpEnumPoints_GetLimitEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetLimitEnum(This,ppEnum)

#define IEpEnumPoints_GetLocationEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetLocationEnum(This,ppEnum)

#define IEpEnumPoints_GetStateEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetStateEnum(This,ppEnum)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPoints_Next_Proxy( 
    IEpEnumPoints __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbPointDescription __RPC_FAR *pDescription,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumPoints_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPoints_Skip_Proxy( 
    IEpEnumPoints __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumPoints_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPoints_GetCalibrationEnum_Proxy( 
    IEpEnumPoints __RPC_FAR * This,
    /* [retval][out] */ IEpEnumCalibrations __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumPoints_GetCalibrationEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPoints_GetConversionEnum_Proxy( 
    IEpEnumPoints __RPC_FAR * This,
    /* [retval][out] */ IEpEnumConversions __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumPoints_GetConversionEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPoints_GetDeltaEnum_Proxy( 
    IEpEnumPoints __RPC_FAR * This,
    /* [retval][out] */ IEpEnumDeltas __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumPoints_GetDeltaEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPoints_GetLimitEnum_Proxy( 
    IEpEnumPoints __RPC_FAR * This,
    /* [retval][out] */ IEpEnumLimits __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumPoints_GetLimitEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPoints_GetLocationEnum_Proxy( 
    IEpEnumPoints __RPC_FAR * This,
    /* [retval][out] */ IEpEnumLocations __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumPoints_GetLocationEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPoints_GetStateEnum_Proxy( 
    IEpEnumPoints __RPC_FAR * This,
    /* [retval][out] */ IEpEnumStates __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumPoints_GetStateEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumPoints_INTERFACE_DEFINED__ */


#ifndef __IEpEnumSubsystems_INTERFACE_DEFINED__
#define __IEpEnumSubsystems_INTERFACE_DEFINED__

/* interface IEpEnumSubsystems */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumSubsystems;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("A58873B1-2325-11d3-803C-006008C0949C")
    IEpEnumSubsystems : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbSubsystemDescription __RPC_FAR *pSubsystem,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumSubsystemsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumSubsystems __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumSubsystems __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumSubsystems __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumSubsystems __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbSubsystemDescription __RPC_FAR *pSubsystem,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumSubsystems __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumSubsystemsVtbl;

    interface IEpEnumSubsystems
    {
        CONST_VTBL struct IEpEnumSubsystemsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumSubsystems_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumSubsystems_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumSubsystems_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumSubsystems_Next(This,cElements,pSubsystem,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pSubsystem,pcFetched)

#define IEpEnumSubsystems_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumSubsystems_Next_Proxy( 
    IEpEnumSubsystems __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbSubsystemDescription __RPC_FAR *pSubsystem,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumSubsystems_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumSubsystems_Skip_Proxy( 
    IEpEnumSubsystems __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumSubsystems_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumSubsystems_INTERFACE_DEFINED__ */


#ifndef __IEpEnumEvents_INTERFACE_DEFINED__
#define __IEpEnumEvents_INTERFACE_DEFINED__

/* interface IEpEnumEvents */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("2577BF1B-A675-4f8a-943C-5D022081C310")
    IEpEnumEvents : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbEventDescription __RPC_FAR *pEvent,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumEvents __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumEvents __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumEvents __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumEvents __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbEventDescription __RPC_FAR *pEvent,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumEvents __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumEventsVtbl;

    interface IEpEnumEvents
    {
        CONST_VTBL struct IEpEnumEventsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumEvents_Next(This,cElements,pEvent,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pEvent,pcFetched)

#define IEpEnumEvents_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumEvents_Next_Proxy( 
    IEpEnumEvents __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbEventDescription __RPC_FAR *pEvent,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumEvents_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumEvents_Skip_Proxy( 
    IEpEnumEvents __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumEvents_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumEvents_INTERFACE_DEFINED__ */


#ifndef __IEpEnumTlmLimMsgs_INTERFACE_DEFINED__
#define __IEpEnumTlmLimMsgs_INTERFACE_DEFINED__

/* interface IEpEnumTlmLimMsgs */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumTlmLimMsgs;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("3CF373D6-AE61-45e0-B7C8-8CB4FB8AAC38")
    IEpEnumTlmLimMsgs : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTlmLimMsgDescription __RPC_FAR *pTlmLimMsg,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumTlmLimMsgsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumTlmLimMsgs __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumTlmLimMsgs __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumTlmLimMsgs __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumTlmLimMsgs __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTlmLimMsgDescription __RPC_FAR *pTlmLimMsg,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumTlmLimMsgs __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumTlmLimMsgsVtbl;

    interface IEpEnumTlmLimMsgs
    {
        CONST_VTBL struct IEpEnumTlmLimMsgsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumTlmLimMsgs_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumTlmLimMsgs_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumTlmLimMsgs_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumTlmLimMsgs_Next(This,cElements,pTlmLimMsg,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pTlmLimMsg,pcFetched)

#define IEpEnumTlmLimMsgs_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumTlmLimMsgs_Next_Proxy( 
    IEpEnumTlmLimMsgs __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbTlmLimMsgDescription __RPC_FAR *pTlmLimMsg,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumTlmLimMsgs_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumTlmLimMsgs_Skip_Proxy( 
    IEpEnumTlmLimMsgs __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumTlmLimMsgs_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumTlmLimMsgs_INTERFACE_DEFINED__ */


#ifndef __IEpEnumDatawords_INTERFACE_DEFINED__
#define __IEpEnumDatawords_INTERFACE_DEFINED__

/* interface IEpEnumDatawords */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumDatawords;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("FD66AE26-C76E-11D3-A97F-00600895D0D8")
    IEpEnumDatawords : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordDescription __RPC_FAR *pDataword,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumDatawordsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumDatawords __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumDatawords __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumDatawords __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumDatawords __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordDescription __RPC_FAR *pDataword,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumDatawords __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumDatawordsVtbl;

    interface IEpEnumDatawords
    {
        CONST_VTBL struct IEpEnumDatawordsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumDatawords_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumDatawords_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumDatawords_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumDatawords_Next(This,cElements,pDataword,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDataword,pcFetched)

#define IEpEnumDatawords_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawords_Next_Proxy( 
    IEpEnumDatawords __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdDatawordDescription __RPC_FAR *pDataword,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumDatawords_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawords_Skip_Proxy( 
    IEpEnumDatawords __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumDatawords_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumDatawords_INTERFACE_DEFINED__ */


#ifndef __IEpEnumDatawordArgValues_INTERFACE_DEFINED__
#define __IEpEnumDatawordArgValues_INTERFACE_DEFINED__

/* interface IEpEnumDatawordArgValues */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumDatawordArgValues;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("2BA94F03-C79E-11D3-A97F-00600895D0D8")
    IEpEnumDatawordArgValues : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordArgValueDescription __RPC_FAR *pDatawordArgValue,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumDatawordArgValuesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumDatawordArgValues __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumDatawordArgValues __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumDatawordArgValues __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumDatawordArgValues __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordArgValueDescription __RPC_FAR *pDatawordArgValue,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumDatawordArgValues __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumDatawordArgValuesVtbl;

    interface IEpEnumDatawordArgValues
    {
        CONST_VTBL struct IEpEnumDatawordArgValuesVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumDatawordArgValues_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumDatawordArgValues_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumDatawordArgValues_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumDatawordArgValues_Next(This,cElements,pDatawordArgValue,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDatawordArgValue,pcFetched)

#define IEpEnumDatawordArgValues_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgValues_Next_Proxy( 
    IEpEnumDatawordArgValues __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdDatawordArgValueDescription __RPC_FAR *pDatawordArgValue,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumDatawordArgValues_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgValues_Skip_Proxy( 
    IEpEnumDatawordArgValues __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumDatawordArgValues_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumDatawordArgValues_INTERFACE_DEFINED__ */


#ifndef __IEpEnumDatawordArgRanges_INTERFACE_DEFINED__
#define __IEpEnumDatawordArgRanges_INTERFACE_DEFINED__

/* interface IEpEnumDatawordArgRanges */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumDatawordArgRanges;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0833A3A3-C8FA-11D3-A980-00600895D0D8")
    IEpEnumDatawordArgRanges : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordArgRangeDescription __RPC_FAR *pDatawordArgRange,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumDatawordArgRangesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumDatawordArgRanges __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumDatawordArgRanges __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumDatawordArgRanges __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumDatawordArgRanges __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordArgRangeDescription __RPC_FAR *pDatawordArgRange,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumDatawordArgRanges __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumDatawordArgRangesVtbl;

    interface IEpEnumDatawordArgRanges
    {
        CONST_VTBL struct IEpEnumDatawordArgRangesVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumDatawordArgRanges_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumDatawordArgRanges_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumDatawordArgRanges_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumDatawordArgRanges_Next(This,cElements,pDatawordArgRange,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDatawordArgRange,pcFetched)

#define IEpEnumDatawordArgRanges_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgRanges_Next_Proxy( 
    IEpEnumDatawordArgRanges __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdDatawordArgRangeDescription __RPC_FAR *pDatawordArgRange,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumDatawordArgRanges_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgRanges_Skip_Proxy( 
    IEpEnumDatawordArgRanges __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumDatawordArgRanges_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumDatawordArgRanges_INTERFACE_DEFINED__ */


#ifndef __IEpEnumDatawordArgTvContexts_INTERFACE_DEFINED__
#define __IEpEnumDatawordArgTvContexts_INTERFACE_DEFINED__

/* interface IEpEnumDatawordArgTvContexts */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumDatawordArgTvContexts;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("EE69E0CB-B888-44a7-9957-A0AE1F31A547")
    IEpEnumDatawordArgTvContexts : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvContextDescription __RPC_FAR *pTVContext,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumDatawordArgTvContextsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumDatawordArgTvContexts __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumDatawordArgTvContexts __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumDatawordArgTvContexts __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumDatawordArgTvContexts __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvContextDescription __RPC_FAR *pTVContext,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumDatawordArgTvContexts __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumDatawordArgTvContextsVtbl;

    interface IEpEnumDatawordArgTvContexts
    {
        CONST_VTBL struct IEpEnumDatawordArgTvContextsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumDatawordArgTvContexts_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumDatawordArgTvContexts_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumDatawordArgTvContexts_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumDatawordArgTvContexts_Next(This,cElements,pTVContext,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pTVContext,pcFetched)

#define IEpEnumDatawordArgTvContexts_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgTvContexts_Next_Proxy( 
    IEpEnumDatawordArgTvContexts __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbTvContextDescription __RPC_FAR *pTVContext,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumDatawordArgTvContexts_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgTvContexts_Skip_Proxy( 
    IEpEnumDatawordArgTvContexts __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumDatawordArgTvContexts_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumDatawordArgTvContexts_INTERFACE_DEFINED__ */


#ifndef __IEpEnumDatawordArgs_INTERFACE_DEFINED__
#define __IEpEnumDatawordArgs_INTERFACE_DEFINED__

/* interface IEpEnumDatawordArgs */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumDatawordArgs;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9229E035-C767-11D3-A97F-00600895D0D8")
    IEpEnumDatawordArgs : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordArgDescription __RPC_FAR *pDatawordArg,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDatawordArgValueEnum( 
            /* [retval][out] */ IEpEnumDatawordArgValues __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDatawordArgRangeEnum( 
            /* [retval][out] */ IEpEnumDatawordArgRanges __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDatawordArgTvContextEnum( 
            /* [retval][out] */ IEpEnumDatawordArgTvContexts __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumDatawordArgsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumDatawordArgs __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumDatawordArgs __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumDatawordArgs __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumDatawordArgs __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordArgDescription __RPC_FAR *pDatawordArg,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumDatawordArgs __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDatawordArgValueEnum )( 
            IEpEnumDatawordArgs __RPC_FAR * This,
            /* [retval][out] */ IEpEnumDatawordArgValues __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDatawordArgRangeEnum )( 
            IEpEnumDatawordArgs __RPC_FAR * This,
            /* [retval][out] */ IEpEnumDatawordArgRanges __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDatawordArgTvContextEnum )( 
            IEpEnumDatawordArgs __RPC_FAR * This,
            /* [retval][out] */ IEpEnumDatawordArgTvContexts __RPC_FAR *__RPC_FAR *ppEnum);
        
        END_INTERFACE
    } IEpEnumDatawordArgsVtbl;

    interface IEpEnumDatawordArgs
    {
        CONST_VTBL struct IEpEnumDatawordArgsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumDatawordArgs_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumDatawordArgs_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumDatawordArgs_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumDatawordArgs_Next(This,cElements,pDatawordArg,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDatawordArg,pcFetched)

#define IEpEnumDatawordArgs_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumDatawordArgs_GetDatawordArgValueEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetDatawordArgValueEnum(This,ppEnum)

#define IEpEnumDatawordArgs_GetDatawordArgRangeEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetDatawordArgRangeEnum(This,ppEnum)

#define IEpEnumDatawordArgs_GetDatawordArgTvContextEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetDatawordArgTvContextEnum(This,ppEnum)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgs_Next_Proxy( 
    IEpEnumDatawordArgs __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdDatawordArgDescription __RPC_FAR *pDatawordArg,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumDatawordArgs_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgs_Skip_Proxy( 
    IEpEnumDatawordArgs __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumDatawordArgs_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgs_GetDatawordArgValueEnum_Proxy( 
    IEpEnumDatawordArgs __RPC_FAR * This,
    /* [retval][out] */ IEpEnumDatawordArgValues __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumDatawordArgs_GetDatawordArgValueEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgs_GetDatawordArgRangeEnum_Proxy( 
    IEpEnumDatawordArgs __RPC_FAR * This,
    /* [retval][out] */ IEpEnumDatawordArgRanges __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumDatawordArgs_GetDatawordArgRangeEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumDatawordArgs_GetDatawordArgTvContextEnum_Proxy( 
    IEpEnumDatawordArgs __RPC_FAR * This,
    /* [retval][out] */ IEpEnumDatawordArgTvContexts __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumDatawordArgs_GetDatawordArgTvContextEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumDatawordArgs_INTERFACE_DEFINED__ */


#ifndef __IEpEnumCmdDatawordGroups_INTERFACE_DEFINED__
#define __IEpEnumCmdDatawordGroups_INTERFACE_DEFINED__

/* interface IEpEnumCmdDatawordGroups */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCmdDatawordGroups;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("823BE503-CF3E-11D3-8886-000000000000")
    IEpEnumCmdDatawordGroups : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordGroupDescription __RPC_FAR *pDataword,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCmdDatawordGroupsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCmdDatawordGroups __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCmdDatawordGroups __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCmdDatawordGroups __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCmdDatawordGroups __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordGroupDescription __RPC_FAR *pDataword,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCmdDatawordGroups __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumCmdDatawordGroupsVtbl;

    interface IEpEnumCmdDatawordGroups
    {
        CONST_VTBL struct IEpEnumCmdDatawordGroupsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCmdDatawordGroups_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCmdDatawordGroups_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCmdDatawordGroups_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCmdDatawordGroups_Next(This,cElements,pDataword,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDataword,pcFetched)

#define IEpEnumCmdDatawordGroups_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdDatawordGroups_Next_Proxy( 
    IEpEnumCmdDatawordGroups __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdDatawordGroupDescription __RPC_FAR *pDataword,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCmdDatawordGroups_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdDatawordGroups_Skip_Proxy( 
    IEpEnumCmdDatawordGroups __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCmdDatawordGroups_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCmdDatawordGroups_INTERFACE_DEFINED__ */


#ifndef __IEpEnumCmdPrivilegeGroups_INTERFACE_DEFINED__
#define __IEpEnumCmdPrivilegeGroups_INTERFACE_DEFINED__

/* interface IEpEnumCmdPrivilegeGroups */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCmdPrivilegeGroups;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("823BE504-CF3E-11D3-8886-000000000000")
    IEpEnumCmdPrivilegeGroups : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdPrivilegeGroupDescription __RPC_FAR *pDataword,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCmdPrivilegeGroupsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCmdPrivilegeGroups __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCmdPrivilegeGroups __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCmdPrivilegeGroups __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCmdPrivilegeGroups __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdPrivilegeGroupDescription __RPC_FAR *pDataword,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCmdPrivilegeGroups __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumCmdPrivilegeGroupsVtbl;

    interface IEpEnumCmdPrivilegeGroups
    {
        CONST_VTBL struct IEpEnumCmdPrivilegeGroupsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCmdPrivilegeGroups_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCmdPrivilegeGroups_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCmdPrivilegeGroups_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCmdPrivilegeGroups_Next(This,cElements,pDataword,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pDataword,pcFetched)

#define IEpEnumCmdPrivilegeGroups_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdPrivilegeGroups_Next_Proxy( 
    IEpEnumCmdPrivilegeGroups __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdPrivilegeGroupDescription __RPC_FAR *pDataword,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCmdPrivilegeGroups_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdPrivilegeGroups_Skip_Proxy( 
    IEpEnumCmdPrivilegeGroups __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCmdPrivilegeGroups_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCmdPrivilegeGroups_INTERFACE_DEFINED__ */


#ifndef __IEpEnumTvEvents_INTERFACE_DEFINED__
#define __IEpEnumTvEvents_INTERFACE_DEFINED__

/* interface IEpEnumTvEvents */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumTvEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4D098D9C-C927-11D3-8874-000000000000")
    IEpEnumTvEvents : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvEventDescription __RPC_FAR *pTvEvent,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumTvEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumTvEvents __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumTvEvents __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumTvEvents __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumTvEvents __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvEventDescription __RPC_FAR *pTvEvent,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumTvEvents __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumTvEventsVtbl;

    interface IEpEnumTvEvents
    {
        CONST_VTBL struct IEpEnumTvEventsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumTvEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumTvEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumTvEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumTvEvents_Next(This,cElements,pTvEvent,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pTvEvent,pcFetched)

#define IEpEnumTvEvents_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumTvEvents_Next_Proxy( 
    IEpEnumTvEvents __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbTvEventDescription __RPC_FAR *pTvEvent,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumTvEvents_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumTvEvents_Skip_Proxy( 
    IEpEnumTvEvents __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumTvEvents_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumTvEvents_INTERFACE_DEFINED__ */


#ifndef __IEpEnumTvSequences_INTERFACE_DEFINED__
#define __IEpEnumTvSequences_INTERFACE_DEFINED__

/* interface IEpEnumTvSequences */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumTvSequences;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4D098D9B-C927-11D3-8874-000000000000")
    IEpEnumTvSequences : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvSequenceDescription __RPC_FAR *pTvSequence,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetTvEventEnum( 
            /* [retval][out] */ IEpEnumTvEvents __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumTvSequencesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumTvSequences __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumTvSequences __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumTvSequences __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumTvSequences __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvSequenceDescription __RPC_FAR *pTvSequence,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumTvSequences __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTvEventEnum )( 
            IEpEnumTvSequences __RPC_FAR * This,
            /* [retval][out] */ IEpEnumTvEvents __RPC_FAR *__RPC_FAR *ppEnum);
        
        END_INTERFACE
    } IEpEnumTvSequencesVtbl;

    interface IEpEnumTvSequences
    {
        CONST_VTBL struct IEpEnumTvSequencesVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumTvSequences_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumTvSequences_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumTvSequences_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumTvSequences_Next(This,cElements,pTvSequence,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pTvSequence,pcFetched)

#define IEpEnumTvSequences_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumTvSequences_GetTvEventEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetTvEventEnum(This,ppEnum)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumTvSequences_Next_Proxy( 
    IEpEnumTvSequences __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbTvSequenceDescription __RPC_FAR *pTvSequence,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumTvSequences_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumTvSequences_Skip_Proxy( 
    IEpEnumTvSequences __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumTvSequences_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumTvSequences_GetTvEventEnum_Proxy( 
    IEpEnumTvSequences __RPC_FAR * This,
    /* [retval][out] */ IEpEnumTvEvents __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumTvSequences_GetTvEventEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumTvSequences_INTERFACE_DEFINED__ */


#ifndef __IEpEnumPreTvStates_INTERFACE_DEFINED__
#define __IEpEnumPreTvStates_INTERFACE_DEFINED__

/* interface IEpEnumPreTvStates */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumPreTvStates;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("0833A3A4-C8FA-11D3-A980-00600895D0D8")
    IEpEnumPreTvStates : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvStateDescription __RPC_FAR *pTvState,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetTvSequenceEnum( 
            /* [retval][out] */ IEpEnumTvSequences __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumPreTvStatesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumPreTvStates __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumPreTvStates __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumPreTvStates __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumPreTvStates __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvStateDescription __RPC_FAR *pTvState,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumPreTvStates __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTvSequenceEnum )( 
            IEpEnumPreTvStates __RPC_FAR * This,
            /* [retval][out] */ IEpEnumTvSequences __RPC_FAR *__RPC_FAR *ppEnum);
        
        END_INTERFACE
    } IEpEnumPreTvStatesVtbl;

    interface IEpEnumPreTvStates
    {
        CONST_VTBL struct IEpEnumPreTvStatesVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumPreTvStates_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumPreTvStates_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumPreTvStates_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumPreTvStates_Next(This,cElements,pTvState,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pTvState,pcFetched)

#define IEpEnumPreTvStates_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumPreTvStates_GetTvSequenceEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetTvSequenceEnum(This,ppEnum)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPreTvStates_Next_Proxy( 
    IEpEnumPreTvStates __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbTvStateDescription __RPC_FAR *pTvState,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumPreTvStates_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPreTvStates_Skip_Proxy( 
    IEpEnumPreTvStates __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumPreTvStates_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPreTvStates_GetTvSequenceEnum_Proxy( 
    IEpEnumPreTvStates __RPC_FAR * This,
    /* [retval][out] */ IEpEnumTvSequences __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumPreTvStates_GetTvSequenceEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumPreTvStates_INTERFACE_DEFINED__ */


#ifndef __IEpEnumPostTvStates_INTERFACE_DEFINED__
#define __IEpEnumPostTvStates_INTERFACE_DEFINED__

/* interface IEpEnumPostTvStates */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumPostTvStates;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4D098D91-C927-11D3-8874-000000000000")
    IEpEnumPostTvStates : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvStateDescription __RPC_FAR *pTvState,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetTvSequenceEnum( 
            /* [retval][out] */ IEpEnumTvSequences __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumPostTvStatesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumPostTvStates __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumPostTvStates __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumPostTvStates __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumPostTvStates __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvStateDescription __RPC_FAR *pTvState,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumPostTvStates __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTvSequenceEnum )( 
            IEpEnumPostTvStates __RPC_FAR * This,
            /* [retval][out] */ IEpEnumTvSequences __RPC_FAR *__RPC_FAR *ppEnum);
        
        END_INTERFACE
    } IEpEnumPostTvStatesVtbl;

    interface IEpEnumPostTvStates
    {
        CONST_VTBL struct IEpEnumPostTvStatesVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumPostTvStates_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumPostTvStates_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumPostTvStates_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumPostTvStates_Next(This,cElements,pTvState,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pTvState,pcFetched)

#define IEpEnumPostTvStates_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumPostTvStates_GetTvSequenceEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetTvSequenceEnum(This,ppEnum)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPostTvStates_Next_Proxy( 
    IEpEnumPostTvStates __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbTvStateDescription __RPC_FAR *pTvState,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumPostTvStates_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPostTvStates_Skip_Proxy( 
    IEpEnumPostTvStates __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumPostTvStates_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPostTvStates_GetTvSequenceEnum_Proxy( 
    IEpEnumPostTvStates __RPC_FAR * This,
    /* [retval][out] */ IEpEnumTvSequences __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumPostTvStates_GetTvSequenceEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumPostTvStates_INTERFACE_DEFINED__ */


#ifndef __IEpEnumPseudoTvStates_INTERFACE_DEFINED__
#define __IEpEnumPseudoTvStates_INTERFACE_DEFINED__

/* interface IEpEnumPseudoTvStates */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumPseudoTvStates;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4D098D94-C927-11D3-8874-000000000000")
    IEpEnumPseudoTvStates : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvStateDescription __RPC_FAR *pTvState,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetTvSequenceEnum( 
            /* [retval][out] */ IEpEnumTvSequences __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumPseudoTvStatesVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumPseudoTvStates __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumPseudoTvStates __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumPseudoTvStates __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumPseudoTvStates __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbTvStateDescription __RPC_FAR *pTvState,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumPseudoTvStates __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTvSequenceEnum )( 
            IEpEnumPseudoTvStates __RPC_FAR * This,
            /* [retval][out] */ IEpEnumTvSequences __RPC_FAR *__RPC_FAR *ppEnum);
        
        END_INTERFACE
    } IEpEnumPseudoTvStatesVtbl;

    interface IEpEnumPseudoTvStates
    {
        CONST_VTBL struct IEpEnumPseudoTvStatesVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumPseudoTvStates_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumPseudoTvStates_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumPseudoTvStates_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumPseudoTvStates_Next(This,cElements,pTvState,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pTvState,pcFetched)

#define IEpEnumPseudoTvStates_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumPseudoTvStates_GetTvSequenceEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetTvSequenceEnum(This,ppEnum)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPseudoTvStates_Next_Proxy( 
    IEpEnumPseudoTvStates __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbTvStateDescription __RPC_FAR *pTvState,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumPseudoTvStates_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPseudoTvStates_Skip_Proxy( 
    IEpEnumPseudoTvStates __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumPseudoTvStates_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumPseudoTvStates_GetTvSequenceEnum_Proxy( 
    IEpEnumPseudoTvStates __RPC_FAR * This,
    /* [retval][out] */ IEpEnumTvSequences __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumPseudoTvStates_GetTvSequenceEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumPseudoTvStates_INTERFACE_DEFINED__ */


#ifndef __IEpEnumCmdExecVerifiers_INTERFACE_DEFINED__
#define __IEpEnumCmdExecVerifiers_INTERFACE_DEFINED__

/* interface IEpEnumCmdExecVerifiers */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCmdExecVerifiers;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("823BE4F7-CF3E-11D3-8886-000000000000")
    IEpEnumCmdExecVerifiers : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdExecVerifierDescription __RPC_FAR *pCmdExecVerifier,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCmdExecVerifiersVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCmdExecVerifiers __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCmdExecVerifiers __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCmdExecVerifiers __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCmdExecVerifiers __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdExecVerifierDescription __RPC_FAR *pCmdExecVerifier,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCmdExecVerifiers __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumCmdExecVerifiersVtbl;

    interface IEpEnumCmdExecVerifiers
    {
        CONST_VTBL struct IEpEnumCmdExecVerifiersVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCmdExecVerifiers_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCmdExecVerifiers_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCmdExecVerifiers_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCmdExecVerifiers_Next(This,cElements,pCmdExecVerifier,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pCmdExecVerifier,pcFetched)

#define IEpEnumCmdExecVerifiers_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdExecVerifiers_Next_Proxy( 
    IEpEnumCmdExecVerifiers __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdExecVerifierDescription __RPC_FAR *pCmdExecVerifier,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCmdExecVerifiers_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdExecVerifiers_Skip_Proxy( 
    IEpEnumCmdExecVerifiers __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCmdExecVerifiers_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCmdExecVerifiers_INTERFACE_DEFINED__ */


#ifndef __IEpEnumCmdEchoVerifierEvents_INTERFACE_DEFINED__
#define __IEpEnumCmdEchoVerifierEvents_INTERFACE_DEFINED__

/* interface IEpEnumCmdEchoVerifierEvents */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCmdEchoVerifierEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4726F5A3-D009-11D3-A982-00600895D0D8")
    IEpEnumCmdEchoVerifierEvents : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdEchoVerifierEventDescription __RPC_FAR *pCmdExecVerifier,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCmdEchoVerifierEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCmdEchoVerifierEvents __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCmdEchoVerifierEvents __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCmdEchoVerifierEvents __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCmdEchoVerifierEvents __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdEchoVerifierEventDescription __RPC_FAR *pCmdExecVerifier,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCmdEchoVerifierEvents __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumCmdEchoVerifierEventsVtbl;

    interface IEpEnumCmdEchoVerifierEvents
    {
        CONST_VTBL struct IEpEnumCmdEchoVerifierEventsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCmdEchoVerifierEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCmdEchoVerifierEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCmdEchoVerifierEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCmdEchoVerifierEvents_Next(This,cElements,pCmdExecVerifier,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pCmdExecVerifier,pcFetched)

#define IEpEnumCmdEchoVerifierEvents_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdEchoVerifierEvents_Next_Proxy( 
    IEpEnumCmdEchoVerifierEvents __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdEchoVerifierEventDescription __RPC_FAR *pCmdExecVerifier,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCmdEchoVerifierEvents_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdEchoVerifierEvents_Skip_Proxy( 
    IEpEnumCmdEchoVerifierEvents __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCmdEchoVerifierEvents_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCmdEchoVerifierEvents_INTERFACE_DEFINED__ */


#ifndef __IEpEnumScAddrs_INTERFACE_DEFINED__
#define __IEpEnumScAddrs_INTERFACE_DEFINED__

/* interface IEpEnumScAddrs */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumScAddrs;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("823BE4F3-CF3E-11D3-8886-000000000000")
    IEpEnumScAddrs : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbScAddrDescription __RPC_FAR *pCmdChannel,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdExecVerifierEnum( 
            /* [retval][out] */ IEpEnumCmdExecVerifiers __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumScAddrsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumScAddrs __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumScAddrs __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumScAddrs __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumScAddrs __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbScAddrDescription __RPC_FAR *pCmdChannel,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumScAddrs __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdExecVerifierEnum )( 
            IEpEnumScAddrs __RPC_FAR * This,
            /* [retval][out] */ IEpEnumCmdExecVerifiers __RPC_FAR *__RPC_FAR *ppEnum);
        
        END_INTERFACE
    } IEpEnumScAddrsVtbl;

    interface IEpEnumScAddrs
    {
        CONST_VTBL struct IEpEnumScAddrsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumScAddrs_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumScAddrs_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumScAddrs_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumScAddrs_Next(This,cElements,pCmdChannel,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pCmdChannel,pcFetched)

#define IEpEnumScAddrs_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumScAddrs_GetCmdExecVerifierEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetCmdExecVerifierEnum(This,ppEnum)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumScAddrs_Next_Proxy( 
    IEpEnumScAddrs __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbScAddrDescription __RPC_FAR *pCmdChannel,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumScAddrs_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumScAddrs_Skip_Proxy( 
    IEpEnumScAddrs __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumScAddrs_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumScAddrs_GetCmdExecVerifierEnum_Proxy( 
    IEpEnumScAddrs __RPC_FAR * This,
    /* [retval][out] */ IEpEnumCmdExecVerifiers __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumScAddrs_GetCmdExecVerifierEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumScAddrs_INTERFACE_DEFINED__ */


#ifndef __IEpEnumCmdDatawordMaps_INTERFACE_DEFINED__
#define __IEpEnumCmdDatawordMaps_INTERFACE_DEFINED__

/* interface IEpEnumCmdDatawordMaps */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCmdDatawordMaps;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("823BE4FB-CF3E-11D3-8886-000000000000")
    IEpEnumCmdDatawordMaps : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordMapDescription __RPC_FAR *pCmdChannel,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCmdDatawordMapsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCmdDatawordMaps __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCmdDatawordMaps __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCmdDatawordMaps __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCmdDatawordMaps __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDatawordMapDescription __RPC_FAR *pCmdChannel,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCmdDatawordMaps __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumCmdDatawordMapsVtbl;

    interface IEpEnumCmdDatawordMaps
    {
        CONST_VTBL struct IEpEnumCmdDatawordMapsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCmdDatawordMaps_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCmdDatawordMaps_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCmdDatawordMaps_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCmdDatawordMaps_Next(This,cElements,pCmdChannel,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pCmdChannel,pcFetched)

#define IEpEnumCmdDatawordMaps_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdDatawordMaps_Next_Proxy( 
    IEpEnumCmdDatawordMaps __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdDatawordMapDescription __RPC_FAR *pCmdChannel,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCmdDatawordMaps_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdDatawordMaps_Skip_Proxy( 
    IEpEnumCmdDatawordMaps __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCmdDatawordMaps_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCmdDatawordMaps_INTERFACE_DEFINED__ */


#ifndef __IEpEnumCmdFrameFormats_INTERFACE_DEFINED__
#define __IEpEnumCmdFrameFormats_INTERFACE_DEFINED__

/* interface IEpEnumCmdFrameFormats */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCmdFrameFormats;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("823BE4FF-CF3E-11D3-8886-000000000000")
    IEpEnumCmdFrameFormats : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdFrameFormatDescription __RPC_FAR *pCmdChannel,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdDatawordMapEnum( 
            /* [retval][out] */ IEpEnumCmdDatawordMaps __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdEchoVerifierEventEnum( 
            /* [retval][out] */ IEpEnumCmdEchoVerifierEvents __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCmdFrameFormatsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCmdFrameFormats __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCmdFrameFormats __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCmdFrameFormats __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCmdFrameFormats __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdFrameFormatDescription __RPC_FAR *pCmdChannel,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCmdFrameFormats __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdDatawordMapEnum )( 
            IEpEnumCmdFrameFormats __RPC_FAR * This,
            /* [retval][out] */ IEpEnumCmdDatawordMaps __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdEchoVerifierEventEnum )( 
            IEpEnumCmdFrameFormats __RPC_FAR * This,
            /* [retval][out] */ IEpEnumCmdEchoVerifierEvents __RPC_FAR *__RPC_FAR *ppEnum);
        
        END_INTERFACE
    } IEpEnumCmdFrameFormatsVtbl;

    interface IEpEnumCmdFrameFormats
    {
        CONST_VTBL struct IEpEnumCmdFrameFormatsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCmdFrameFormats_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCmdFrameFormats_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCmdFrameFormats_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCmdFrameFormats_Next(This,cElements,pCmdChannel,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pCmdChannel,pcFetched)

#define IEpEnumCmdFrameFormats_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumCmdFrameFormats_GetCmdDatawordMapEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetCmdDatawordMapEnum(This,ppEnum)

#define IEpEnumCmdFrameFormats_GetCmdEchoVerifierEventEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetCmdEchoVerifierEventEnum(This,ppEnum)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdFrameFormats_Next_Proxy( 
    IEpEnumCmdFrameFormats __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdFrameFormatDescription __RPC_FAR *pCmdChannel,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCmdFrameFormats_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdFrameFormats_Skip_Proxy( 
    IEpEnumCmdFrameFormats __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCmdFrameFormats_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdFrameFormats_GetCmdDatawordMapEnum_Proxy( 
    IEpEnumCmdFrameFormats __RPC_FAR * This,
    /* [retval][out] */ IEpEnumCmdDatawordMaps __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCmdFrameFormats_GetCmdDatawordMapEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdFrameFormats_GetCmdEchoVerifierEventEnum_Proxy( 
    IEpEnumCmdFrameFormats __RPC_FAR * This,
    /* [retval][out] */ IEpEnumCmdEchoVerifierEvents __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCmdFrameFormats_GetCmdEchoVerifierEventEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCmdFrameFormats_INTERFACE_DEFINED__ */


#ifndef __IEpEnumCmdChannels_INTERFACE_DEFINED__
#define __IEpEnumCmdChannels_INTERFACE_DEFINED__

/* interface IEpEnumCmdChannels */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCmdChannels;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7459AFC3-CE7D-11D3-A982-00600895D0D8")
    IEpEnumCmdChannels : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdChannelDescription __RPC_FAR *pCmdChannel,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetScAddrEnum( 
            /* [retval][out] */ IEpEnumScAddrs __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdExecVerifierEnum( 
            /* [retval][out] */ IEpEnumCmdExecVerifiers __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCmdChannelsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCmdChannels __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCmdChannels __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCmdChannels __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCmdChannels __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdChannelDescription __RPC_FAR *pCmdChannel,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCmdChannels __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetScAddrEnum )( 
            IEpEnumCmdChannels __RPC_FAR * This,
            /* [retval][out] */ IEpEnumScAddrs __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdExecVerifierEnum )( 
            IEpEnumCmdChannels __RPC_FAR * This,
            /* [retval][out] */ IEpEnumCmdExecVerifiers __RPC_FAR *__RPC_FAR *ppEnum);
        
        END_INTERFACE
    } IEpEnumCmdChannelsVtbl;

    interface IEpEnumCmdChannels
    {
        CONST_VTBL struct IEpEnumCmdChannelsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCmdChannels_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCmdChannels_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCmdChannels_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCmdChannels_Next(This,cElements,pCmdChannel,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pCmdChannel,pcFetched)

#define IEpEnumCmdChannels_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumCmdChannels_GetScAddrEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetScAddrEnum(This,ppEnum)

#define IEpEnumCmdChannels_GetCmdExecVerifierEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetCmdExecVerifierEnum(This,ppEnum)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdChannels_Next_Proxy( 
    IEpEnumCmdChannels __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdChannelDescription __RPC_FAR *pCmdChannel,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCmdChannels_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdChannels_Skip_Proxy( 
    IEpEnumCmdChannels __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCmdChannels_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdChannels_GetScAddrEnum_Proxy( 
    IEpEnumCmdChannels __RPC_FAR * This,
    /* [retval][out] */ IEpEnumScAddrs __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCmdChannels_GetScAddrEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdChannels_GetCmdExecVerifierEnum_Proxy( 
    IEpEnumCmdChannels __RPC_FAR * This,
    /* [retval][out] */ IEpEnumCmdExecVerifiers __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCmdChannels_GetCmdExecVerifierEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCmdChannels_INTERFACE_DEFINED__ */


#ifndef __IEpEnumCmdFmtPointValue_INTERFACE_DEFINED__
#define __IEpEnumCmdFmtPointValue_INTERFACE_DEFINED__

/* interface IEpEnumCmdFmtPointValue */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCmdFmtPointValue;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("9A17C723-213F-4859-958E-0FAB1A28AE17")
    IEpEnumCmdFmtPointValue : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdFmtPointValueDescription __RPC_FAR *pCmdPointValue,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCmdFmtPointValueVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCmdFmtPointValue __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCmdFmtPointValue __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCmdFmtPointValue __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCmdFmtPointValue __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdFmtPointValueDescription __RPC_FAR *pCmdPointValue,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCmdFmtPointValue __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        END_INTERFACE
    } IEpEnumCmdFmtPointValueVtbl;

    interface IEpEnumCmdFmtPointValue
    {
        CONST_VTBL struct IEpEnumCmdFmtPointValueVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCmdFmtPointValue_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCmdFmtPointValue_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCmdFmtPointValue_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCmdFmtPointValue_Next(This,cElements,pCmdPointValue,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pCmdPointValue,pcFetched)

#define IEpEnumCmdFmtPointValue_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdFmtPointValue_Next_Proxy( 
    IEpEnumCmdFmtPointValue __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdFmtPointValueDescription __RPC_FAR *pCmdPointValue,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCmdFmtPointValue_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdFmtPointValue_Skip_Proxy( 
    IEpEnumCmdFmtPointValue __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCmdFmtPointValue_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCmdFmtPointValue_INTERFACE_DEFINED__ */


#ifndef __IEpEnumCmdFormats_INTERFACE_DEFINED__
#define __IEpEnumCmdFormats_INTERFACE_DEFINED__

/* interface IEpEnumCmdFormats */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCmdFormats;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7459AFC7-CE7D-11D3-A982-00600895D0D8")
    IEpEnumCmdFormats : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdFormatDescription __RPC_FAR *pCmdFormat,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdFrameFormatEnum( 
            /* [retval][out] */ IEpEnumCmdFrameFormats __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdFmtPointValueEnum( 
            /* [retval][out] */ IEpEnumCmdFmtPointValue __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCmdFormatsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCmdFormats __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCmdFormats __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCmdFormats __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCmdFormats __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdFormatDescription __RPC_FAR *pCmdFormat,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCmdFormats __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdFrameFormatEnum )( 
            IEpEnumCmdFormats __RPC_FAR * This,
            /* [retval][out] */ IEpEnumCmdFrameFormats __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdFmtPointValueEnum )( 
            IEpEnumCmdFormats __RPC_FAR * This,
            /* [retval][out] */ IEpEnumCmdFmtPointValue __RPC_FAR *__RPC_FAR *ppEnum);
        
        END_INTERFACE
    } IEpEnumCmdFormatsVtbl;

    interface IEpEnumCmdFormats
    {
        CONST_VTBL struct IEpEnumCmdFormatsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCmdFormats_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCmdFormats_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCmdFormats_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCmdFormats_Next(This,cElements,pCmdFormat,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pCmdFormat,pcFetched)

#define IEpEnumCmdFormats_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumCmdFormats_GetCmdFrameFormatEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetCmdFrameFormatEnum(This,ppEnum)

#define IEpEnumCmdFormats_GetCmdFmtPointValueEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetCmdFmtPointValueEnum(This,ppEnum)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdFormats_Next_Proxy( 
    IEpEnumCmdFormats __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdFormatDescription __RPC_FAR *pCmdFormat,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCmdFormats_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdFormats_Skip_Proxy( 
    IEpEnumCmdFormats __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCmdFormats_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdFormats_GetCmdFrameFormatEnum_Proxy( 
    IEpEnumCmdFormats __RPC_FAR * This,
    /* [retval][out] */ IEpEnumCmdFrameFormats __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCmdFormats_GetCmdFrameFormatEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCmdFormats_GetCmdFmtPointValueEnum_Proxy( 
    IEpEnumCmdFormats __RPC_FAR * This,
    /* [retval][out] */ IEpEnumCmdFmtPointValue __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCmdFormats_GetCmdFmtPointValueEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCmdFormats_INTERFACE_DEFINED__ */


#ifndef __IEpEnumCommands_INTERFACE_DEFINED__
#define __IEpEnumCommands_INTERFACE_DEFINED__

/* interface IEpEnumCommands */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpEnumCommands;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F3CADEA5-C384-11D3-A97B-00600895D0D8")
    IEpEnumCommands : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Next( 
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDescription __RPC_FAR *pCommand,
            /* [out] */ ULONG __RPC_FAR *pcFetched) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Skip( 
            /* [in] */ ULONG cElements) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDatawordEnum( 
            /* [retval][out] */ IEpEnumDatawords __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDatawordArgEnum( 
            /* [retval][out] */ IEpEnumDatawordArgs __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPreTvStateEnum( 
            /* [retval][out] */ IEpEnumPreTvStates __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPostTvStateEnum( 
            /* [retval][out] */ IEpEnumPostTvStates __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPseudoTvStateEnum( 
            /* [retval][out] */ IEpEnumPseudoTvStates __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdDatawordGroupEnum( 
            /* [retval][out] */ IEpEnumCmdDatawordGroups __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdPrivilegeGroupEnum( 
            /* [retval][out] */ IEpEnumCmdPrivilegeGroups __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdExecVerifierEnum( 
            /* [retval][out] */ IEpEnumCmdExecVerifiers __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdChannelEnum( 
            /* [in] */ BSTR bstrUplinkProcessor,
            /* [retval][out] */ IEpEnumCmdChannels __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdFormatEnum( 
            /* [in] */ BSTR bstrFormat,
            /* [retval][out] */ IEpEnumCmdFormats __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSpacecraftFormat( 
            /* [out] */ EpDbScFormatDescription __RPC_FAR *pScFormat) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpEnumCommandsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpEnumCommands __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpEnumCommands __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Next )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [in] */ ULONG cElements,
            /* [length_is][size_is][out] */ EpDbCmdDescription __RPC_FAR *pCommand,
            /* [out] */ ULONG __RPC_FAR *pcFetched);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Skip )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [in] */ ULONG cElements);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDatawordEnum )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [retval][out] */ IEpEnumDatawords __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDatawordArgEnum )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [retval][out] */ IEpEnumDatawordArgs __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPreTvStateEnum )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [retval][out] */ IEpEnumPreTvStates __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPostTvStateEnum )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [retval][out] */ IEpEnumPostTvStates __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPseudoTvStateEnum )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [retval][out] */ IEpEnumPseudoTvStates __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdDatawordGroupEnum )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [retval][out] */ IEpEnumCmdDatawordGroups __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdPrivilegeGroupEnum )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [retval][out] */ IEpEnumCmdPrivilegeGroups __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdExecVerifierEnum )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [retval][out] */ IEpEnumCmdExecVerifiers __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdChannelEnum )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [in] */ BSTR bstrUplinkProcessor,
            /* [retval][out] */ IEpEnumCmdChannels __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdFormatEnum )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [in] */ BSTR bstrFormat,
            /* [retval][out] */ IEpEnumCmdFormats __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetSpacecraftFormat )( 
            IEpEnumCommands __RPC_FAR * This,
            /* [out] */ EpDbScFormatDescription __RPC_FAR *pScFormat);
        
        END_INTERFACE
    } IEpEnumCommandsVtbl;

    interface IEpEnumCommands
    {
        CONST_VTBL struct IEpEnumCommandsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpEnumCommands_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpEnumCommands_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpEnumCommands_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpEnumCommands_Next(This,cElements,pCommand,pcFetched)	\
    (This)->lpVtbl -> Next(This,cElements,pCommand,pcFetched)

#define IEpEnumCommands_Skip(This,cElements)	\
    (This)->lpVtbl -> Skip(This,cElements)

#define IEpEnumCommands_GetDatawordEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetDatawordEnum(This,ppEnum)

#define IEpEnumCommands_GetDatawordArgEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetDatawordArgEnum(This,ppEnum)

#define IEpEnumCommands_GetPreTvStateEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetPreTvStateEnum(This,ppEnum)

#define IEpEnumCommands_GetPostTvStateEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetPostTvStateEnum(This,ppEnum)

#define IEpEnumCommands_GetPseudoTvStateEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetPseudoTvStateEnum(This,ppEnum)

#define IEpEnumCommands_GetCmdDatawordGroupEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetCmdDatawordGroupEnum(This,ppEnum)

#define IEpEnumCommands_GetCmdPrivilegeGroupEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetCmdPrivilegeGroupEnum(This,ppEnum)

#define IEpEnumCommands_GetCmdExecVerifierEnum(This,ppEnum)	\
    (This)->lpVtbl -> GetCmdExecVerifierEnum(This,ppEnum)

#define IEpEnumCommands_GetCmdChannelEnum(This,bstrUplinkProcessor,ppEnum)	\
    (This)->lpVtbl -> GetCmdChannelEnum(This,bstrUplinkProcessor,ppEnum)

#define IEpEnumCommands_GetCmdFormatEnum(This,bstrFormat,ppEnum)	\
    (This)->lpVtbl -> GetCmdFormatEnum(This,bstrFormat,ppEnum)

#define IEpEnumCommands_GetSpacecraftFormat(This,pScFormat)	\
    (This)->lpVtbl -> GetSpacecraftFormat(This,pScFormat)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_Next_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [in] */ ULONG cElements,
    /* [length_is][size_is][out] */ EpDbCmdDescription __RPC_FAR *pCommand,
    /* [out] */ ULONG __RPC_FAR *pcFetched);


void __RPC_STUB IEpEnumCommands_Next_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_Skip_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [in] */ ULONG cElements);


void __RPC_STUB IEpEnumCommands_Skip_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetDatawordEnum_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [retval][out] */ IEpEnumDatawords __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCommands_GetDatawordEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetDatawordArgEnum_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [retval][out] */ IEpEnumDatawordArgs __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCommands_GetDatawordArgEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetPreTvStateEnum_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [retval][out] */ IEpEnumPreTvStates __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCommands_GetPreTvStateEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetPostTvStateEnum_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [retval][out] */ IEpEnumPostTvStates __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCommands_GetPostTvStateEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetPseudoTvStateEnum_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [retval][out] */ IEpEnumPseudoTvStates __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCommands_GetPseudoTvStateEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetCmdDatawordGroupEnum_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [retval][out] */ IEpEnumCmdDatawordGroups __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCommands_GetCmdDatawordGroupEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetCmdPrivilegeGroupEnum_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [retval][out] */ IEpEnumCmdPrivilegeGroups __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCommands_GetCmdPrivilegeGroupEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetCmdExecVerifierEnum_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [retval][out] */ IEpEnumCmdExecVerifiers __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCommands_GetCmdExecVerifierEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetCmdChannelEnum_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [in] */ BSTR bstrUplinkProcessor,
    /* [retval][out] */ IEpEnumCmdChannels __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCommands_GetCmdChannelEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetCmdFormatEnum_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [in] */ BSTR bstrFormat,
    /* [retval][out] */ IEpEnumCmdFormats __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpEnumCommands_GetCmdFormatEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpEnumCommands_GetSpacecraftFormat_Proxy( 
    IEpEnumCommands __RPC_FAR * This,
    /* [out] */ EpDbScFormatDescription __RPC_FAR *pScFormat);


void __RPC_STUB IEpEnumCommands_GetSpacecraftFormat_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpEnumCommands_INTERFACE_DEFINED__ */


#ifndef __IEpDB_INTERFACE_DEFINED__
#define __IEpDB_INTERFACE_DEFINED__

/* interface IEpDB */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpDB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("45643C31-0172-11d3-803B-006008C0949C")
    IEpDB : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE OpenDatabase( 
            /* [in] */ BSTR bstrDBName,
            /* [in] */ DWORD dwCBCookie,
            /* [retval][out] */ ULONG __RPC_FAR *pulDBHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CloseDatabase( 
            /* [in] */ ULONG ulDBHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDatabaseType( 
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ EPDBTYPE __RPC_FAR *nType) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE FormatEvents( 
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ ULONG cElements,
            /* [size_is][in] */ EPEVTDATA __RPC_FAR *pUnformattedEvents,
            /* [size_is][out] */ BSTR __RPC_FAR *pFormattedEvents) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMnemonicFromIndex( 
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ EPPTVARIETY eVariety,
            /* [in] */ INT nIndex,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrMnemonic) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPointEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ BSTR bstrMnemonic,
            /* [retval][out] */ IEpEnumPoints __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPointListEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumPoints __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSubsystemListEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumSubsystems __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetEventEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ ULONG ulEventNumber,
            /* [retval][out] */ IEpEnumEvents __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetEventListEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumEvents __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetTlmLimMsgListEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumTlmLimMsgs __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCommandEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ BSTR bstrMnemonic,
            /* [retval][out] */ IEpEnumCommands __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCommandListEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumCommands __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdFormatEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ BSTR bstrFormat,
            /* [retval][out] */ IEpEnumCmdFormats __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdFormatListEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumCmdFormats __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdChannelEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ BSTR bstrUplinkProcessor,
            /* [retval][out] */ IEpEnumCmdChannels __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCmdChannelListEnum( 
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumCmdChannels __RPC_FAR *__RPC_FAR *ppEnum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ValidateUserDirective( 
            /* [in] */ BSTR bstrUserName,
            /* [in] */ BSTR bstrDirective,
            /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ValidateUserFunction( 
            /* [in] */ BSTR bstrUserName,
            /* [in] */ int nFunction,
            /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE UserInGroup( 
            /* [in] */ BSTR bstrUserName,
            /* [in] */ BSTR bstrGroupName,
            /* [retval][out] */ BOOL __RPC_FAR *pbIsMember) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ValidateUserDirectiveStream( 
            /* [in] */ BSTR bstrUserName,
            /* [in] */ BSTR bstrDirective,
            /* [in] */ BSTR bstrStream,
            /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ValidateUserFunctionStream( 
            /* [in] */ BSTR bstrUserName,
            /* [in] */ int nFunction,
            /* [in] */ BSTR bstrStream,
            /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE UserInGroupStream( 
            /* [in] */ BSTR bstrUserName,
            /* [in] */ BSTR bstrGroupName,
            /* [in] */ BSTR bstrStream,
            /* [retval][out] */ BOOL __RPC_FAR *pbIsMember) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpDBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpDB __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpDB __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *OpenDatabase )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ BSTR bstrDBName,
            /* [in] */ DWORD dwCBCookie,
            /* [retval][out] */ ULONG __RPC_FAR *pulDBHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CloseDatabase )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDatabaseType )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ EPDBTYPE __RPC_FAR *nType);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *FormatEvents )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ ULONG cElements,
            /* [size_is][in] */ EPEVTDATA __RPC_FAR *pUnformattedEvents,
            /* [size_is][out] */ BSTR __RPC_FAR *pFormattedEvents);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetMnemonicFromIndex )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ EPPTVARIETY eVariety,
            /* [in] */ INT nIndex,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrMnemonic);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPointEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ BSTR bstrMnemonic,
            /* [retval][out] */ IEpEnumPoints __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPointListEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumPoints __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetSubsystemListEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumSubsystems __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetEventEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ ULONG ulEventNumber,
            /* [retval][out] */ IEpEnumEvents __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetEventListEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumEvents __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTlmLimMsgListEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumTlmLimMsgs __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCommandEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ BSTR bstrMnemonic,
            /* [retval][out] */ IEpEnumCommands __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCommandListEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumCommands __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdFormatEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ BSTR bstrFormat,
            /* [retval][out] */ IEpEnumCmdFormats __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdFormatListEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumCmdFormats __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdChannelEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [in] */ BSTR bstrUplinkProcessor,
            /* [retval][out] */ IEpEnumCmdChannels __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCmdChannelListEnum )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ ULONG ulDBHandle,
            /* [retval][out] */ IEpEnumCmdChannels __RPC_FAR *__RPC_FAR *ppEnum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ValidateUserDirective )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ BSTR bstrUserName,
            /* [in] */ BSTR bstrDirective,
            /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ValidateUserFunction )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ BSTR bstrUserName,
            /* [in] */ int nFunction,
            /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *UserInGroup )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ BSTR bstrUserName,
            /* [in] */ BSTR bstrGroupName,
            /* [retval][out] */ BOOL __RPC_FAR *pbIsMember);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ValidateUserDirectiveStream )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ BSTR bstrUserName,
            /* [in] */ BSTR bstrDirective,
            /* [in] */ BSTR bstrStream,
            /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ValidateUserFunctionStream )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ BSTR bstrUserName,
            /* [in] */ int nFunction,
            /* [in] */ BSTR bstrStream,
            /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *UserInGroupStream )( 
            IEpDB __RPC_FAR * This,
            /* [in] */ BSTR bstrUserName,
            /* [in] */ BSTR bstrGroupName,
            /* [in] */ BSTR bstrStream,
            /* [retval][out] */ BOOL __RPC_FAR *pbIsMember);
        
        END_INTERFACE
    } IEpDBVtbl;

    interface IEpDB
    {
        CONST_VTBL struct IEpDBVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpDB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpDB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpDB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpDB_OpenDatabase(This,bstrDBName,dwCBCookie,pulDBHandle)	\
    (This)->lpVtbl -> OpenDatabase(This,bstrDBName,dwCBCookie,pulDBHandle)

#define IEpDB_CloseDatabase(This,ulDBHandle)	\
    (This)->lpVtbl -> CloseDatabase(This,ulDBHandle)

#define IEpDB_GetDatabaseType(This,ulDBHandle,nType)	\
    (This)->lpVtbl -> GetDatabaseType(This,ulDBHandle,nType)

#define IEpDB_FormatEvents(This,ulDBHandle,cElements,pUnformattedEvents,pFormattedEvents)	\
    (This)->lpVtbl -> FormatEvents(This,ulDBHandle,cElements,pUnformattedEvents,pFormattedEvents)

#define IEpDB_GetMnemonicFromIndex(This,ulDBHandle,eVariety,nIndex,pbstrMnemonic)	\
    (This)->lpVtbl -> GetMnemonicFromIndex(This,ulDBHandle,eVariety,nIndex,pbstrMnemonic)

#define IEpDB_GetPointEnum(This,ulDBHandle,bstrMnemonic,ppEnum)	\
    (This)->lpVtbl -> GetPointEnum(This,ulDBHandle,bstrMnemonic,ppEnum)

#define IEpDB_GetPointListEnum(This,ulDBHandle,ppEnum)	\
    (This)->lpVtbl -> GetPointListEnum(This,ulDBHandle,ppEnum)

#define IEpDB_GetSubsystemListEnum(This,ulDBHandle,ppEnum)	\
    (This)->lpVtbl -> GetSubsystemListEnum(This,ulDBHandle,ppEnum)

#define IEpDB_GetEventEnum(This,ulDBHandle,ulEventNumber,ppEnum)	\
    (This)->lpVtbl -> GetEventEnum(This,ulDBHandle,ulEventNumber,ppEnum)

#define IEpDB_GetEventListEnum(This,ulDBHandle,ppEnum)	\
    (This)->lpVtbl -> GetEventListEnum(This,ulDBHandle,ppEnum)

#define IEpDB_GetTlmLimMsgListEnum(This,ulDBHandle,ppEnum)	\
    (This)->lpVtbl -> GetTlmLimMsgListEnum(This,ulDBHandle,ppEnum)

#define IEpDB_GetCommandEnum(This,ulDBHandle,bstrMnemonic,ppEnum)	\
    (This)->lpVtbl -> GetCommandEnum(This,ulDBHandle,bstrMnemonic,ppEnum)

#define IEpDB_GetCommandListEnum(This,ulDBHandle,ppEnum)	\
    (This)->lpVtbl -> GetCommandListEnum(This,ulDBHandle,ppEnum)

#define IEpDB_GetCmdFormatEnum(This,ulDBHandle,bstrFormat,ppEnum)	\
    (This)->lpVtbl -> GetCmdFormatEnum(This,ulDBHandle,bstrFormat,ppEnum)

#define IEpDB_GetCmdFormatListEnum(This,ulDBHandle,ppEnum)	\
    (This)->lpVtbl -> GetCmdFormatListEnum(This,ulDBHandle,ppEnum)

#define IEpDB_GetCmdChannelEnum(This,ulDBHandle,bstrUplinkProcessor,ppEnum)	\
    (This)->lpVtbl -> GetCmdChannelEnum(This,ulDBHandle,bstrUplinkProcessor,ppEnum)

#define IEpDB_GetCmdChannelListEnum(This,ulDBHandle,ppEnum)	\
    (This)->lpVtbl -> GetCmdChannelListEnum(This,ulDBHandle,ppEnum)

#define IEpDB_ValidateUserDirective(This,bstrUserName,bstrDirective,pbHasPermission)	\
    (This)->lpVtbl -> ValidateUserDirective(This,bstrUserName,bstrDirective,pbHasPermission)

#define IEpDB_ValidateUserFunction(This,bstrUserName,nFunction,pbHasPermission)	\
    (This)->lpVtbl -> ValidateUserFunction(This,bstrUserName,nFunction,pbHasPermission)

#define IEpDB_UserInGroup(This,bstrUserName,bstrGroupName,pbIsMember)	\
    (This)->lpVtbl -> UserInGroup(This,bstrUserName,bstrGroupName,pbIsMember)

#define IEpDB_ValidateUserDirectiveStream(This,bstrUserName,bstrDirective,bstrStream,pbHasPermission)	\
    (This)->lpVtbl -> ValidateUserDirectiveStream(This,bstrUserName,bstrDirective,bstrStream,pbHasPermission)

#define IEpDB_ValidateUserFunctionStream(This,bstrUserName,nFunction,bstrStream,pbHasPermission)	\
    (This)->lpVtbl -> ValidateUserFunctionStream(This,bstrUserName,nFunction,bstrStream,pbHasPermission)

#define IEpDB_UserInGroupStream(This,bstrUserName,bstrGroupName,bstrStream,pbIsMember)	\
    (This)->lpVtbl -> UserInGroupStream(This,bstrUserName,bstrGroupName,bstrStream,pbIsMember)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_OpenDatabase_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ BSTR bstrDBName,
    /* [in] */ DWORD dwCBCookie,
    /* [retval][out] */ ULONG __RPC_FAR *pulDBHandle);


void __RPC_STUB IEpDB_OpenDatabase_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_CloseDatabase_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle);


void __RPC_STUB IEpDB_CloseDatabase_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetDatabaseType_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [retval][out] */ EPDBTYPE __RPC_FAR *nType);


void __RPC_STUB IEpDB_GetDatabaseType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_FormatEvents_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [in] */ ULONG cElements,
    /* [size_is][in] */ EPEVTDATA __RPC_FAR *pUnformattedEvents,
    /* [size_is][out] */ BSTR __RPC_FAR *pFormattedEvents);


void __RPC_STUB IEpDB_FormatEvents_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetMnemonicFromIndex_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [in] */ EPPTVARIETY eVariety,
    /* [in] */ INT nIndex,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrMnemonic);


void __RPC_STUB IEpDB_GetMnemonicFromIndex_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetPointEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [in] */ BSTR bstrMnemonic,
    /* [retval][out] */ IEpEnumPoints __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetPointEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetPointListEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [retval][out] */ IEpEnumPoints __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetPointListEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetSubsystemListEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [retval][out] */ IEpEnumSubsystems __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetSubsystemListEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetEventEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [in] */ ULONG ulEventNumber,
    /* [retval][out] */ IEpEnumEvents __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetEventEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetEventListEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [retval][out] */ IEpEnumEvents __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetEventListEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetTlmLimMsgListEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [retval][out] */ IEpEnumTlmLimMsgs __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetTlmLimMsgListEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetCommandEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [in] */ BSTR bstrMnemonic,
    /* [retval][out] */ IEpEnumCommands __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetCommandEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetCommandListEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [retval][out] */ IEpEnumCommands __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetCommandListEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetCmdFormatEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [in] */ BSTR bstrFormat,
    /* [retval][out] */ IEpEnumCmdFormats __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetCmdFormatEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetCmdFormatListEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [retval][out] */ IEpEnumCmdFormats __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetCmdFormatListEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetCmdChannelEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [in] */ BSTR bstrUplinkProcessor,
    /* [retval][out] */ IEpEnumCmdChannels __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetCmdChannelEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_GetCmdChannelListEnum_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ ULONG ulDBHandle,
    /* [retval][out] */ IEpEnumCmdChannels __RPC_FAR *__RPC_FAR *ppEnum);


void __RPC_STUB IEpDB_GetCmdChannelListEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_ValidateUserDirective_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ BSTR bstrUserName,
    /* [in] */ BSTR bstrDirective,
    /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission);


void __RPC_STUB IEpDB_ValidateUserDirective_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_ValidateUserFunction_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ BSTR bstrUserName,
    /* [in] */ int nFunction,
    /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission);


void __RPC_STUB IEpDB_ValidateUserFunction_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_UserInGroup_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ BSTR bstrUserName,
    /* [in] */ BSTR bstrGroupName,
    /* [retval][out] */ BOOL __RPC_FAR *pbIsMember);


void __RPC_STUB IEpDB_UserInGroup_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_ValidateUserDirectiveStream_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ BSTR bstrUserName,
    /* [in] */ BSTR bstrDirective,
    /* [in] */ BSTR bstrStream,
    /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission);


void __RPC_STUB IEpDB_ValidateUserDirectiveStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_ValidateUserFunctionStream_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ BSTR bstrUserName,
    /* [in] */ int nFunction,
    /* [in] */ BSTR bstrStream,
    /* [retval][out] */ BOOL __RPC_FAR *pbHasPermission);


void __RPC_STUB IEpDB_ValidateUserFunctionStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDB_UserInGroupStream_Proxy( 
    IEpDB __RPC_FAR * This,
    /* [in] */ BSTR bstrUserName,
    /* [in] */ BSTR bstrGroupName,
    /* [in] */ BSTR bstrStream,
    /* [retval][out] */ BOOL __RPC_FAR *pbIsMember);


void __RPC_STUB IEpDB_UserInGroupStream_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpDB_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_epcommon_0264 */
/* [local] */ 

typedef struct  _EPCTRL_CB_ITEM
    {
    EPITEMST st;
    wchar_t wzStrmName[ 100 ];
    wchar_t wzMnem[ 100 ];
    EPDISPCONV ct;
    EPITEMSP sp;
    LONG lSampRate;
    }	EPCTRL_CB_ITEM;

typedef struct  _EPATTRIB_ITEM
    {
    short sItemIndex;
    EPATTRIB_PAGE ePage;
    ULONG ulErrors;
    }	EPATTRIB_ITEM;

typedef struct  _EPATTRIB_ITEMS
    {
    SHORT sArrySize;
    /* [size_is] */ EPATTRIB_ITEM __RPC_FAR *pArryItems;
    }	EPATTRIB_ITEMS;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0264_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0264_v0_0_s_ifspec;

#ifndef __IEpAttrib_INTERFACE_DEFINED__
#define __IEpAttrib_INTERFACE_DEFINED__

/* interface IEpAttrib */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpAttrib;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("26348FEE-E907-11d2-A33C-00600867A0E2")
    IEpAttrib : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ShowDlg( 
            /* [in] */ IEpCtrl __RPC_FAR *pIEpCtrl,
            /* [in] */ LONG hWndParent,
            /* [in] */ EPATTRIB_ITEMS pPageInfo) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CkEpAttrib( 
            /* [in] */ IEpCtrl __RPC_FAR *pIEpCtrl,
            /* [retval][out] */ EPATTRIB_ITEMS __RPC_FAR *pErrData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CkItemEpAttrib( 
            /* [in] */ IEpCtrl __RPC_FAR *pIEpCtrl,
            /* [in] */ BSTR bstrDB,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPATTRIB_ITEMS __RPC_FAR *perrData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpAttribVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpAttrib __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpAttrib __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpAttrib __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ShowDlg )( 
            IEpAttrib __RPC_FAR * This,
            /* [in] */ IEpCtrl __RPC_FAR *pIEpCtrl,
            /* [in] */ LONG hWndParent,
            /* [in] */ EPATTRIB_ITEMS pPageInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CkEpAttrib )( 
            IEpAttrib __RPC_FAR * This,
            /* [in] */ IEpCtrl __RPC_FAR *pIEpCtrl,
            /* [retval][out] */ EPATTRIB_ITEMS __RPC_FAR *pErrData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CkItemEpAttrib )( 
            IEpAttrib __RPC_FAR * This,
            /* [in] */ IEpCtrl __RPC_FAR *pIEpCtrl,
            /* [in] */ BSTR bstrDB,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPATTRIB_ITEMS __RPC_FAR *perrData);
        
        END_INTERFACE
    } IEpAttribVtbl;

    interface IEpAttrib
    {
        CONST_VTBL struct IEpAttribVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpAttrib_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpAttrib_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpAttrib_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpAttrib_ShowDlg(This,pIEpCtrl,hWndParent,pPageInfo)	\
    (This)->lpVtbl -> ShowDlg(This,pIEpCtrl,hWndParent,pPageInfo)

#define IEpAttrib_CkEpAttrib(This,pIEpCtrl,pErrData)	\
    (This)->lpVtbl -> CkEpAttrib(This,pIEpCtrl,pErrData)

#define IEpAttrib_CkItemEpAttrib(This,pIEpCtrl,bstrDB,sItemIndex,perrData)	\
    (This)->lpVtbl -> CkItemEpAttrib(This,pIEpCtrl,bstrDB,sItemIndex,perrData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpAttrib_ShowDlg_Proxy( 
    IEpAttrib __RPC_FAR * This,
    /* [in] */ IEpCtrl __RPC_FAR *pIEpCtrl,
    /* [in] */ LONG hWndParent,
    /* [in] */ EPATTRIB_ITEMS pPageInfo);


void __RPC_STUB IEpAttrib_ShowDlg_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpAttrib_CkEpAttrib_Proxy( 
    IEpAttrib __RPC_FAR * This,
    /* [in] */ IEpCtrl __RPC_FAR *pIEpCtrl,
    /* [retval][out] */ EPATTRIB_ITEMS __RPC_FAR *pErrData);


void __RPC_STUB IEpAttrib_CkEpAttrib_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpAttrib_CkItemEpAttrib_Proxy( 
    IEpAttrib __RPC_FAR * This,
    /* [in] */ IEpCtrl __RPC_FAR *pIEpCtrl,
    /* [in] */ BSTR bstrDB,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ EPATTRIB_ITEMS __RPC_FAR *perrData);


void __RPC_STUB IEpAttrib_CkItemEpAttrib_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpAttrib_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_epcommon_0265 */
/* [local] */ 

typedef struct  _EPSTATES
    {
    SHORT sArrySize;
    /* [size_is] */ EpDbStateDescription __RPC_FAR *pArryStates;
    }	EPSTATES;

typedef struct  _EPLIMITS
    {
    SHORT sArrySize;
    /* [size_is] */ EpDbLimitDescription __RPC_FAR *pArryLimits;
    }	EPLIMITS;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0265_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0265_v0_0_s_ifspec;

#ifndef __IEpDBHelper_INTERFACE_DEFINED__
#define __IEpDBHelper_INTERFACE_DEFINED__

/* interface IEpDBHelper */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpDBHelper;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("80E8F3DB-2EDA-11D3-A34F-00600867A0E2")
    IEpDBHelper : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetDB( 
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrDBName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDB( 
            /* [in] */ ULONG ulCntId,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrDBName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetDBFromStrmName( 
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrStrmName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPointDesc( 
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrMnem,
            /* [retval][out] */ EpDbPointDescription __RPC_FAR *pDesc) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetStateDescs( 
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrMnem,
            /* [retval][out] */ EPSTATES __RPC_FAR *pStates) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetLimitDescs( 
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrMnem,
            /* [retval][out] */ EPLIMITS __RPC_FAR *pLimits) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE BreakConnections( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ShowPtAttr( 
            /* [in] */ ULONG ulCntId,
            /* [in] */ LONG hWndParent,
            /* [in] */ BSTR bstrMnem,
            /* [in] */ BOOL bShowIfNotVisible) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EditPtAttr( 
            /* [in] */ ULONG ulCntId,
            /* [in] */ LONG hWndParent,
            /* [in] */ BSTR bstrMnem,
            /* [in] */ BOOL bShowIfNotVisible,
            /* [in] */ BSTR bstrStream) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ShowCmdAttr( 
            /* [in] */ ULONG ulCntId,
            /* [in] */ LONG hWndParent,
            /* [in] */ BSTR bstrMnem,
            /* [in] */ BOOL bShowIfNotVisible) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE IsPtAttrDlgVisible( 
            /* [retval][out] */ BOOL __RPC_FAR *pbVisible) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPointEnum( 
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrMnem,
            /* [retval][out] */ IEpEnumPoints __RPC_FAR *__RPC_FAR *pIEpEnumPoints) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Connect( 
            /* [in] */ ULONG __RPC_FAR *pulClntHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Disconnect( 
            /* [in] */ ULONG ulClntHandle) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpDBHelperVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpDBHelper __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpDBHelper __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetDB )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrDBName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDB )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulCntId,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrDBName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetDBFromStrmName )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrStrmName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPointDesc )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrMnem,
            /* [retval][out] */ EpDbPointDescription __RPC_FAR *pDesc);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetStateDescs )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrMnem,
            /* [retval][out] */ EPSTATES __RPC_FAR *pStates);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetLimitDescs )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrMnem,
            /* [retval][out] */ EPLIMITS __RPC_FAR *pLimits);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *BreakConnections )( 
            IEpDBHelper __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ShowPtAttr )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulCntId,
            /* [in] */ LONG hWndParent,
            /* [in] */ BSTR bstrMnem,
            /* [in] */ BOOL bShowIfNotVisible);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EditPtAttr )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulCntId,
            /* [in] */ LONG hWndParent,
            /* [in] */ BSTR bstrMnem,
            /* [in] */ BOOL bShowIfNotVisible,
            /* [in] */ BSTR bstrStream);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ShowCmdAttr )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulCntId,
            /* [in] */ LONG hWndParent,
            /* [in] */ BSTR bstrMnem,
            /* [in] */ BOOL bShowIfNotVisible);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *IsPtAttrDlgVisible )( 
            IEpDBHelper __RPC_FAR * This,
            /* [retval][out] */ BOOL __RPC_FAR *pbVisible);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPointEnum )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulCntId,
            /* [in] */ BSTR bstrMnem,
            /* [retval][out] */ IEpEnumPoints __RPC_FAR *__RPC_FAR *pIEpEnumPoints);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Connect )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG __RPC_FAR *pulClntHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Disconnect )( 
            IEpDBHelper __RPC_FAR * This,
            /* [in] */ ULONG ulClntHandle);
        
        END_INTERFACE
    } IEpDBHelperVtbl;

    interface IEpDBHelper
    {
        CONST_VTBL struct IEpDBHelperVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpDBHelper_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpDBHelper_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpDBHelper_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpDBHelper_SetDB(This,ulCntId,bstrDBName)	\
    (This)->lpVtbl -> SetDB(This,ulCntId,bstrDBName)

#define IEpDBHelper_GetDB(This,ulCntId,pbstrDBName)	\
    (This)->lpVtbl -> GetDB(This,ulCntId,pbstrDBName)

#define IEpDBHelper_SetDBFromStrmName(This,ulCntId,bstrStrmName)	\
    (This)->lpVtbl -> SetDBFromStrmName(This,ulCntId,bstrStrmName)

#define IEpDBHelper_GetPointDesc(This,ulCntId,bstrMnem,pDesc)	\
    (This)->lpVtbl -> GetPointDesc(This,ulCntId,bstrMnem,pDesc)

#define IEpDBHelper_GetStateDescs(This,ulCntId,bstrMnem,pStates)	\
    (This)->lpVtbl -> GetStateDescs(This,ulCntId,bstrMnem,pStates)

#define IEpDBHelper_GetLimitDescs(This,ulCntId,bstrMnem,pLimits)	\
    (This)->lpVtbl -> GetLimitDescs(This,ulCntId,bstrMnem,pLimits)

#define IEpDBHelper_BreakConnections(This)	\
    (This)->lpVtbl -> BreakConnections(This)

#define IEpDBHelper_ShowPtAttr(This,ulCntId,hWndParent,bstrMnem,bShowIfNotVisible)	\
    (This)->lpVtbl -> ShowPtAttr(This,ulCntId,hWndParent,bstrMnem,bShowIfNotVisible)

#define IEpDBHelper_EditPtAttr(This,ulCntId,hWndParent,bstrMnem,bShowIfNotVisible,bstrStream)	\
    (This)->lpVtbl -> EditPtAttr(This,ulCntId,hWndParent,bstrMnem,bShowIfNotVisible,bstrStream)

#define IEpDBHelper_ShowCmdAttr(This,ulCntId,hWndParent,bstrMnem,bShowIfNotVisible)	\
    (This)->lpVtbl -> ShowCmdAttr(This,ulCntId,hWndParent,bstrMnem,bShowIfNotVisible)

#define IEpDBHelper_IsPtAttrDlgVisible(This,pbVisible)	\
    (This)->lpVtbl -> IsPtAttrDlgVisible(This,pbVisible)

#define IEpDBHelper_GetPointEnum(This,ulCntId,bstrMnem,pIEpEnumPoints)	\
    (This)->lpVtbl -> GetPointEnum(This,ulCntId,bstrMnem,pIEpEnumPoints)

#define IEpDBHelper_Connect(This,pulClntHandle)	\
    (This)->lpVtbl -> Connect(This,pulClntHandle)

#define IEpDBHelper_Disconnect(This,ulClntHandle)	\
    (This)->lpVtbl -> Disconnect(This,ulClntHandle)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_SetDB_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulCntId,
    /* [in] */ BSTR bstrDBName);


void __RPC_STUB IEpDBHelper_SetDB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_GetDB_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulCntId,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrDBName);


void __RPC_STUB IEpDBHelper_GetDB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_SetDBFromStrmName_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulCntId,
    /* [in] */ BSTR bstrStrmName);


void __RPC_STUB IEpDBHelper_SetDBFromStrmName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_GetPointDesc_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulCntId,
    /* [in] */ BSTR bstrMnem,
    /* [retval][out] */ EpDbPointDescription __RPC_FAR *pDesc);


void __RPC_STUB IEpDBHelper_GetPointDesc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_GetStateDescs_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulCntId,
    /* [in] */ BSTR bstrMnem,
    /* [retval][out] */ EPSTATES __RPC_FAR *pStates);


void __RPC_STUB IEpDBHelper_GetStateDescs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_GetLimitDescs_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulCntId,
    /* [in] */ BSTR bstrMnem,
    /* [retval][out] */ EPLIMITS __RPC_FAR *pLimits);


void __RPC_STUB IEpDBHelper_GetLimitDescs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_BreakConnections_Proxy( 
    IEpDBHelper __RPC_FAR * This);


void __RPC_STUB IEpDBHelper_BreakConnections_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_ShowPtAttr_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulCntId,
    /* [in] */ LONG hWndParent,
    /* [in] */ BSTR bstrMnem,
    /* [in] */ BOOL bShowIfNotVisible);


void __RPC_STUB IEpDBHelper_ShowPtAttr_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_EditPtAttr_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulCntId,
    /* [in] */ LONG hWndParent,
    /* [in] */ BSTR bstrMnem,
    /* [in] */ BOOL bShowIfNotVisible,
    /* [in] */ BSTR bstrStream);


void __RPC_STUB IEpDBHelper_EditPtAttr_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_ShowCmdAttr_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulCntId,
    /* [in] */ LONG hWndParent,
    /* [in] */ BSTR bstrMnem,
    /* [in] */ BOOL bShowIfNotVisible);


void __RPC_STUB IEpDBHelper_ShowCmdAttr_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_IsPtAttrDlgVisible_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [retval][out] */ BOOL __RPC_FAR *pbVisible);


void __RPC_STUB IEpDBHelper_IsPtAttrDlgVisible_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_GetPointEnum_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulCntId,
    /* [in] */ BSTR bstrMnem,
    /* [retval][out] */ IEpEnumPoints __RPC_FAR *__RPC_FAR *pIEpEnumPoints);


void __RPC_STUB IEpDBHelper_GetPointEnum_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_Connect_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG __RPC_FAR *pulClntHandle);


void __RPC_STUB IEpDBHelper_Connect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpDBHelper_Disconnect_Proxy( 
    IEpDBHelper __RPC_FAR * This,
    /* [in] */ ULONG ulClntHandle);


void __RPC_STUB IEpDBHelper_Disconnect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpDBHelper_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_epcommon_0266 */
/* [local] */ 

typedef struct  _EPPROCINFO
    {
    ULONG ulUserData;
    ULONG ulInterpHandle;
    ULONG ulChgHint;
    ULONG ulProcState;
    BOOL bClientIsOwner;
    EPPROCTYPE eProcType;
    BSTR bstrStream;
    BSTR bstrOwner;
    BSTR bstrSubroutine;
    BSTR bstrPathName;
    }	EPPROCINFO;

typedef struct  _EPPROCMSG
    {
    ULONG ulUserData;
    BSTR bstrMsg;
    }	EPPROCMSG;

typedef struct  _EPPROCCSINFO
    {
    BSTR bstrFileName;
    ULONG ulCharOffset;
    ULONG ulHighlightWidth;
    }	EPPROCCSINFO;

typedef struct  _EPPROCTRACE
    {
    ULONG ulUserData;
    ULONG ulChgHint;
    ULONG ulFlags;
    BOOL bIsGoTo;
    BSTR bstrStream;
    ULONG ulProcState;
    BOOL bClientIsOwner;
    FILETIME ftWaitExp;
    ULONG ulCharOffset;
    ULONG ulHighlightWidth;
    ULONG ulCSDepth;
    IEpDirRtr __RPC_FAR *pIEpDirRtr;
    ULONG ulArrySize;
    /* [size_is] */ EPPROCCSINFO __RPC_FAR *pArryProcInfo;
    }	EPPROCTRACE;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0266_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0266_v0_0_s_ifspec;

#ifndef __IEpProcCtrl_INTERFACE_DEFINED__
#define __IEpProcCtrl_INTERFACE_DEFINED__

/* interface IEpProcCtrl */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpProcCtrl;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("7007E6F1-AE34-11D3-926F-000000000000")
    IEpProcCtrl : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Connect( 
            /* [in] */ DWORD dwCBCookie,
            /* [in] */ IEpDirRtr __RPC_FAR *pIEpDirRtr,
            /* [retval][out] */ ULONG __RPC_FAR *pulClientID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Disconnect( 
            /* [in] */ ULONG ulClientID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE StartInterp( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ BSTR bstrDBName,
            /* [in] */ BSTR bstrProcName,
            /* [in] */ BSTR bstrCmdLine,
            /* [in] */ BSTR bstrOwner,
            /* [in] */ BOOL bStartPaused,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pulInterpID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE StopInterp( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulInterpID,
            /* [in] */ ULONG ulUserData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ExecuteDirective( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulInterpID,
            /* [in] */ BSTR bstrDirective,
            /* [in] */ ULONG ulUserData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Response( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulInterpID,
            /* [in] */ BSTR bstrResponse) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestInfoSrvc( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestMsgSrvc( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulInterpID,
            /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestTraceSrvc( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulInterpID,
            /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CancelService( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulSrvcHandle) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpProcCtrlVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpProcCtrl __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpProcCtrl __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Connect )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ DWORD dwCBCookie,
            /* [in] */ IEpDirRtr __RPC_FAR *pIEpDirRtr,
            /* [retval][out] */ ULONG __RPC_FAR *pulClientID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Disconnect )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *StartInterp )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ BSTR bstrStrmName,
            /* [in] */ BSTR bstrDBName,
            /* [in] */ BSTR bstrProcName,
            /* [in] */ BSTR bstrCmdLine,
            /* [in] */ BSTR bstrOwner,
            /* [in] */ BOOL bStartPaused,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pulInterpID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *StopInterp )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulInterpID,
            /* [in] */ ULONG ulUserData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExecuteDirective )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulInterpID,
            /* [in] */ BSTR bstrDirective,
            /* [in] */ ULONG ulUserData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Response )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulInterpID,
            /* [in] */ BSTR bstrResponse);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestInfoSrvc )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestMsgSrvc )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulInterpID,
            /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestTraceSrvc )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulUserData,
            /* [in] */ ULONG ulInterpID,
            /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CancelService )( 
            IEpProcCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulSrvcHandle);
        
        END_INTERFACE
    } IEpProcCtrlVtbl;

    interface IEpProcCtrl
    {
        CONST_VTBL struct IEpProcCtrlVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpProcCtrl_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpProcCtrl_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpProcCtrl_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpProcCtrl_Connect(This,dwCBCookie,pIEpDirRtr,pulClientID)	\
    (This)->lpVtbl -> Connect(This,dwCBCookie,pIEpDirRtr,pulClientID)

#define IEpProcCtrl_Disconnect(This,ulClientID)	\
    (This)->lpVtbl -> Disconnect(This,ulClientID)

#define IEpProcCtrl_StartInterp(This,ulClientID,bstrStrmName,bstrDBName,bstrProcName,bstrCmdLine,bstrOwner,bStartPaused,ulUserData,pulInterpID)	\
    (This)->lpVtbl -> StartInterp(This,ulClientID,bstrStrmName,bstrDBName,bstrProcName,bstrCmdLine,bstrOwner,bStartPaused,ulUserData,pulInterpID)

#define IEpProcCtrl_StopInterp(This,ulClientID,ulInterpID,ulUserData)	\
    (This)->lpVtbl -> StopInterp(This,ulClientID,ulInterpID,ulUserData)

#define IEpProcCtrl_ExecuteDirective(This,ulClientID,ulInterpID,bstrDirective,ulUserData)	\
    (This)->lpVtbl -> ExecuteDirective(This,ulClientID,ulInterpID,bstrDirective,ulUserData)

#define IEpProcCtrl_Response(This,ulClientID,ulInterpID,bstrResponse)	\
    (This)->lpVtbl -> Response(This,ulClientID,ulInterpID,bstrResponse)

#define IEpProcCtrl_RequestInfoSrvc(This,ulClientID,ulUserData,pulSrvcHandle)	\
    (This)->lpVtbl -> RequestInfoSrvc(This,ulClientID,ulUserData,pulSrvcHandle)

#define IEpProcCtrl_RequestMsgSrvc(This,ulClientID,ulUserData,ulInterpID,pulSrvcHandle)	\
    (This)->lpVtbl -> RequestMsgSrvc(This,ulClientID,ulUserData,ulInterpID,pulSrvcHandle)

#define IEpProcCtrl_RequestTraceSrvc(This,ulClientID,ulUserData,ulInterpID,pulSrvcHandle)	\
    (This)->lpVtbl -> RequestTraceSrvc(This,ulClientID,ulUserData,ulInterpID,pulSrvcHandle)

#define IEpProcCtrl_CancelService(This,ulClientID,ulSrvcHandle)	\
    (This)->lpVtbl -> CancelService(This,ulClientID,ulSrvcHandle)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpProcCtrl_Connect_Proxy( 
    IEpProcCtrl __RPC_FAR * This,
    /* [in] */ DWORD dwCBCookie,
    /* [in] */ IEpDirRtr __RPC_FAR *pIEpDirRtr,
    /* [retval][out] */ ULONG __RPC_FAR *pulClientID);


void __RPC_STUB IEpProcCtrl_Connect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpProcCtrl_Disconnect_Proxy( 
    IEpProcCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID);


void __RPC_STUB IEpProcCtrl_Disconnect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpProcCtrl_StartInterp_Proxy( 
    IEpProcCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ BSTR bstrStrmName,
    /* [in] */ BSTR bstrDBName,
    /* [in] */ BSTR bstrProcName,
    /* [in] */ BSTR bstrCmdLine,
    /* [in] */ BSTR bstrOwner,
    /* [in] */ BOOL bStartPaused,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pulInterpID);


void __RPC_STUB IEpProcCtrl_StartInterp_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpProcCtrl_StopInterp_Proxy( 
    IEpProcCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulInterpID,
    /* [in] */ ULONG ulUserData);


void __RPC_STUB IEpProcCtrl_StopInterp_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpProcCtrl_ExecuteDirective_Proxy( 
    IEpProcCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulInterpID,
    /* [in] */ BSTR bstrDirective,
    /* [in] */ ULONG ulUserData);


void __RPC_STUB IEpProcCtrl_ExecuteDirective_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpProcCtrl_Response_Proxy( 
    IEpProcCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulInterpID,
    /* [in] */ BSTR bstrResponse);


void __RPC_STUB IEpProcCtrl_Response_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpProcCtrl_RequestInfoSrvc_Proxy( 
    IEpProcCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle);


void __RPC_STUB IEpProcCtrl_RequestInfoSrvc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpProcCtrl_RequestMsgSrvc_Proxy( 
    IEpProcCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulUserData,
    /* [in] */ ULONG ulInterpID,
    /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle);


void __RPC_STUB IEpProcCtrl_RequestMsgSrvc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpProcCtrl_RequestTraceSrvc_Proxy( 
    IEpProcCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulUserData,
    /* [in] */ ULONG ulInterpID,
    /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle);


void __RPC_STUB IEpProcCtrl_RequestTraceSrvc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpProcCtrl_CancelService_Proxy( 
    IEpProcCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulSrvcHandle);


void __RPC_STUB IEpProcCtrl_CancelService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpProcCtrl_INTERFACE_DEFINED__ */


#ifndef ___IEpProcCB_INTERFACE_DEFINED__
#define ___IEpProcCB_INTERFACE_DEFINED__

/* interface _IEpProcCB */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID__IEpProcCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("E44C4B11-AE38-11d3-926F-000000000000")
    _IEpProcCB : public _IEpochAliveCB
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE InfoSrvcCB( 
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPPROCINFO __RPC_FAR *pInfoArry) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE MsgSrvcCB( 
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPPROCMSG __RPC_FAR *pMsgArry) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE TraceSrvcCB( 
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPPROCTRACE __RPC_FAR *pTraceArry) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct _IEpProcCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            _IEpProcCB __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            _IEpProcCB __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            _IEpProcCB __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AreYouAliveCB )( 
            _IEpProcCB __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *InfoSrvcCB )( 
            _IEpProcCB __RPC_FAR * This,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPPROCINFO __RPC_FAR *pInfoArry);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *MsgSrvcCB )( 
            _IEpProcCB __RPC_FAR * This,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPPROCMSG __RPC_FAR *pMsgArry);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *TraceSrvcCB )( 
            _IEpProcCB __RPC_FAR * This,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPPROCTRACE __RPC_FAR *pTraceArry);
        
        END_INTERFACE
    } _IEpProcCBVtbl;

    interface _IEpProcCB
    {
        CONST_VTBL struct _IEpProcCBVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IEpProcCB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IEpProcCB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IEpProcCB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IEpProcCB_AreYouAliveCB(This)	\
    (This)->lpVtbl -> AreYouAliveCB(This)


#define _IEpProcCB_InfoSrvcCB(This,ulArrySize,pInfoArry)	\
    (This)->lpVtbl -> InfoSrvcCB(This,ulArrySize,pInfoArry)

#define _IEpProcCB_MsgSrvcCB(This,ulArrySize,pMsgArry)	\
    (This)->lpVtbl -> MsgSrvcCB(This,ulArrySize,pMsgArry)

#define _IEpProcCB_TraceSrvcCB(This,ulArrySize,pTraceArry)	\
    (This)->lpVtbl -> TraceSrvcCB(This,ulArrySize,pTraceArry)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpProcCB_InfoSrvcCB_Proxy( 
    _IEpProcCB __RPC_FAR * This,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPPROCINFO __RPC_FAR *pInfoArry);


void __RPC_STUB _IEpProcCB_InfoSrvcCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpProcCB_MsgSrvcCB_Proxy( 
    _IEpProcCB __RPC_FAR * This,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPPROCMSG __RPC_FAR *pMsgArry);


void __RPC_STUB _IEpProcCB_MsgSrvcCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpProcCB_TraceSrvcCB_Proxy( 
    _IEpProcCB __RPC_FAR * This,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPPROCTRACE __RPC_FAR *pTraceArry);


void __RPC_STUB _IEpProcCB_TraceSrvcCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* ___IEpProcCB_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_epcommon_0268 */
/* [local] */ 

static const GUID CATID_EpTrigSrvcMod_1_00 = { 0xba5aede9, 0x76ea, 0x4711, { 0x97, 0x3c, 0x2d, 0xf1, 0x48, 0xca, 0xc7, 0x6e } };
typedef struct  EPPLUGININFO
    {
    EPTRIGMODTYPE eType;
    CLSID clsidClntModule;
    ULONG ulPluginID;
    }	EPPLUGININFO;

typedef struct  EPTRIGPLUGINS
    {
    ULONG ulArrySize;
    /* [size_is] */ EPPLUGININFO __RPC_FAR *pArryPlugins;
    }	EPTRIGPLUGINS;

typedef struct  EPTRIGVALUE
    {
    ULONG ulSubItem;
    EPPTVARIETY ePtVariety;
    EPDISPCONV eConvType;
    EPPTDATA eptValue;
    }	EPTRIGVALUE;

typedef struct  EPSMCBDATA
    {
    ULONG ulUserData;
    ULONG ulHint;
    ULONG ulStatus;
    EPTRIGVALUE trigValue;
    BSTR bstrDynaInfo;
    }	EPSMCBDATA;

typedef struct  EPSMMSG
    {
    ULONG ulUserData;
    BSTR bstrMsg;
    }	EPSMMSG;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0268_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0268_v0_0_s_ifspec;

#ifndef ___IEpTrigSrvcModCB_INTERFACE_DEFINED__
#define ___IEpTrigSrvcModCB_INTERFACE_DEFINED__

/* interface _IEpTrigSrvcModCB */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID__IEpTrigSrvcModCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("321049CE-5E0D-48b6-97B6-F67EB93D1AC4")
    _IEpTrigSrvcModCB : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SrvcModCB( 
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPSMCBDATA __RPC_FAR *pCBData) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE MessageCB( 
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPSMMSG __RPC_FAR *pMessage) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct _IEpTrigSrvcModCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            _IEpTrigSrvcModCB __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            _IEpTrigSrvcModCB __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            _IEpTrigSrvcModCB __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SrvcModCB )( 
            _IEpTrigSrvcModCB __RPC_FAR * This,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPSMCBDATA __RPC_FAR *pCBData);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *MessageCB )( 
            _IEpTrigSrvcModCB __RPC_FAR * This,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPSMMSG __RPC_FAR *pMessage);
        
        END_INTERFACE
    } _IEpTrigSrvcModCBVtbl;

    interface _IEpTrigSrvcModCB
    {
        CONST_VTBL struct _IEpTrigSrvcModCBVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IEpTrigSrvcModCB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IEpTrigSrvcModCB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IEpTrigSrvcModCB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IEpTrigSrvcModCB_SrvcModCB(This,ulArrySize,pCBData)	\
    (This)->lpVtbl -> SrvcModCB(This,ulArrySize,pCBData)

#define _IEpTrigSrvcModCB_MessageCB(This,ulArrySize,pMessage)	\
    (This)->lpVtbl -> MessageCB(This,ulArrySize,pMessage)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpTrigSrvcModCB_SrvcModCB_Proxy( 
    _IEpTrigSrvcModCB __RPC_FAR * This,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPSMCBDATA __RPC_FAR *pCBData);


void __RPC_STUB _IEpTrigSrvcModCB_SrvcModCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpTrigSrvcModCB_MessageCB_Proxy( 
    _IEpTrigSrvcModCB __RPC_FAR * This,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPSMMSG __RPC_FAR *pMessage);


void __RPC_STUB _IEpTrigSrvcModCB_MessageCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* ___IEpTrigSrvcModCB_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_epcommon_0269 */
/* [local] */ 

typedef struct  EPTRIGMODINFO
    {
    ULONG ulIndex;
    ULONG ulChgHint;
    ULONG ulStatus;
    ULONG ulPluginID;
    EPBLOB epbData;
    EPTRIGVALUE trigValue;
    }	EPTRIGMODINFO;

typedef struct  EPTOKENINFO
    {
    ULONG ulToken;
    BSTR bstrReplacement;
    }	EPTOKENINFO;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0269_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0269_v0_0_s_ifspec;

#ifndef __IEpTrigSrvcMod_INTERFACE_DEFINED__
#define __IEpTrigSrvcMod_INTERFACE_DEFINED__

/* interface IEpTrigSrvcMod */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpTrigSrvcMod;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D2E2604D-6B00-43e5-9106-D7C6A80595E7")
    IEpTrigSrvcMod : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetModType( 
            /* [retval][out] */ EPTRIGMODTYPE __RPC_FAR *peType) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetModInfo( 
            /* [retval][out] */ ULONG __RPC_FAR *pulInfo) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetClntPlugins( 
            /* [retval][out] */ EPTRIGPLUGINS __RPC_FAR *pPlugins) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RegisterCB( 
            /* [in] */ _IEpTrigSrvcModCB __RPC_FAR *pICB,
            /* [retval][out] */ ULONG __RPC_FAR *pulCBID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RevokeCB( 
            /* [in] */ ULONG ulCBID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetInfoForString( 
            /* [in] */ BSTR bstrDir,
            /* [retval][out] */ EPTRIGMODINFO __RPC_FAR *ptmInfo) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPointName( 
            /* [in] */ ULONG ulID,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetEventID( 
            /* [in] */ ULONG ulID,
            /* [retval][out] */ ULONG __RPC_FAR *pulEvntID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Register( 
            /* [in] */ ULONG ulCBID,
            /* [in] */ ULONG ulUserData,
            /* [in] */ BSTR bstrExtra,
            /* [in] */ EPTRIGMODINFO tmInfo,
            /* [retval][out] */ ULONG __RPC_FAR *pulID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Revoke( 
            /* [in] */ ULONG ulID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Start( 
            /* [in] */ ULONG ulID,
            /* [in] */ BOOL bSendDynaInfoStrgs,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPTOKENINFO __RPC_FAR *pTokens) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Stop( 
            /* [in] */ ULONG ulID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ReleasePointers( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetTokenFlags( 
            /* [in] */ EPTRIGMODINFO tmInfo,
            /* [retval][out] */ ULONG __RPC_FAR *pulTokenFlags) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetStaticTokenInfo( 
            /* [in] */ EPTRIGMODINFO tmInfo,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrStaticInfo) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpTrigSrvcModVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpTrigSrvcMod __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpTrigSrvcMod __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetModType )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [retval][out] */ EPTRIGMODTYPE __RPC_FAR *peType);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetModInfo )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [retval][out] */ ULONG __RPC_FAR *pulInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetClntPlugins )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [retval][out] */ EPTRIGPLUGINS __RPC_FAR *pPlugins);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RegisterCB )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ _IEpTrigSrvcModCB __RPC_FAR *pICB,
            /* [retval][out] */ ULONG __RPC_FAR *pulCBID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RevokeCB )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ ULONG ulCBID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetInfoForString )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ BSTR bstrDir,
            /* [retval][out] */ EPTRIGMODINFO __RPC_FAR *ptmInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPointName )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ ULONG ulID,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetEventID )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ ULONG ulID,
            /* [retval][out] */ ULONG __RPC_FAR *pulEvntID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Register )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ ULONG ulCBID,
            /* [in] */ ULONG ulUserData,
            /* [in] */ BSTR bstrExtra,
            /* [in] */ EPTRIGMODINFO tmInfo,
            /* [retval][out] */ ULONG __RPC_FAR *pulID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Revoke )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ ULONG ulID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Start )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ ULONG ulID,
            /* [in] */ BOOL bSendDynaInfoStrgs,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPTOKENINFO __RPC_FAR *pTokens);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Stop )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ ULONG ulID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ReleasePointers )( 
            IEpTrigSrvcMod __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTokenFlags )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ EPTRIGMODINFO tmInfo,
            /* [retval][out] */ ULONG __RPC_FAR *pulTokenFlags);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetStaticTokenInfo )( 
            IEpTrigSrvcMod __RPC_FAR * This,
            /* [in] */ EPTRIGMODINFO tmInfo,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrStaticInfo);
        
        END_INTERFACE
    } IEpTrigSrvcModVtbl;

    interface IEpTrigSrvcMod
    {
        CONST_VTBL struct IEpTrigSrvcModVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpTrigSrvcMod_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpTrigSrvcMod_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpTrigSrvcMod_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpTrigSrvcMod_GetModType(This,peType)	\
    (This)->lpVtbl -> GetModType(This,peType)

#define IEpTrigSrvcMod_GetModInfo(This,pulInfo)	\
    (This)->lpVtbl -> GetModInfo(This,pulInfo)

#define IEpTrigSrvcMod_GetClntPlugins(This,pPlugins)	\
    (This)->lpVtbl -> GetClntPlugins(This,pPlugins)

#define IEpTrigSrvcMod_RegisterCB(This,pICB,pulCBID)	\
    (This)->lpVtbl -> RegisterCB(This,pICB,pulCBID)

#define IEpTrigSrvcMod_RevokeCB(This,ulCBID)	\
    (This)->lpVtbl -> RevokeCB(This,ulCBID)

#define IEpTrigSrvcMod_GetInfoForString(This,bstrDir,ptmInfo)	\
    (This)->lpVtbl -> GetInfoForString(This,bstrDir,ptmInfo)

#define IEpTrigSrvcMod_GetPointName(This,ulID,pbstrName)	\
    (This)->lpVtbl -> GetPointName(This,ulID,pbstrName)

#define IEpTrigSrvcMod_GetEventID(This,ulID,pulEvntID)	\
    (This)->lpVtbl -> GetEventID(This,ulID,pulEvntID)

#define IEpTrigSrvcMod_Register(This,ulCBID,ulUserData,bstrExtra,tmInfo,pulID)	\
    (This)->lpVtbl -> Register(This,ulCBID,ulUserData,bstrExtra,tmInfo,pulID)

#define IEpTrigSrvcMod_Revoke(This,ulID)	\
    (This)->lpVtbl -> Revoke(This,ulID)

#define IEpTrigSrvcMod_Start(This,ulID,bSendDynaInfoStrgs,ulArrySize,pTokens)	\
    (This)->lpVtbl -> Start(This,ulID,bSendDynaInfoStrgs,ulArrySize,pTokens)

#define IEpTrigSrvcMod_Stop(This,ulID)	\
    (This)->lpVtbl -> Stop(This,ulID)

#define IEpTrigSrvcMod_ReleasePointers(This)	\
    (This)->lpVtbl -> ReleasePointers(This)

#define IEpTrigSrvcMod_GetTokenFlags(This,tmInfo,pulTokenFlags)	\
    (This)->lpVtbl -> GetTokenFlags(This,tmInfo,pulTokenFlags)

#define IEpTrigSrvcMod_GetStaticTokenInfo(This,tmInfo,pbstrStaticInfo)	\
    (This)->lpVtbl -> GetStaticTokenInfo(This,tmInfo,pbstrStaticInfo)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_GetModType_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [retval][out] */ EPTRIGMODTYPE __RPC_FAR *peType);


void __RPC_STUB IEpTrigSrvcMod_GetModType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_GetModInfo_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [retval][out] */ ULONG __RPC_FAR *pulInfo);


void __RPC_STUB IEpTrigSrvcMod_GetModInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_GetClntPlugins_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [retval][out] */ EPTRIGPLUGINS __RPC_FAR *pPlugins);


void __RPC_STUB IEpTrigSrvcMod_GetClntPlugins_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_RegisterCB_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ _IEpTrigSrvcModCB __RPC_FAR *pICB,
    /* [retval][out] */ ULONG __RPC_FAR *pulCBID);


void __RPC_STUB IEpTrigSrvcMod_RegisterCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_RevokeCB_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ ULONG ulCBID);


void __RPC_STUB IEpTrigSrvcMod_RevokeCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_GetInfoForString_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ BSTR bstrDir,
    /* [retval][out] */ EPTRIGMODINFO __RPC_FAR *ptmInfo);


void __RPC_STUB IEpTrigSrvcMod_GetInfoForString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_GetPointName_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ ULONG ulID,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrName);


void __RPC_STUB IEpTrigSrvcMod_GetPointName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_GetEventID_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ ULONG ulID,
    /* [retval][out] */ ULONG __RPC_FAR *pulEvntID);


void __RPC_STUB IEpTrigSrvcMod_GetEventID_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_Register_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ ULONG ulCBID,
    /* [in] */ ULONG ulUserData,
    /* [in] */ BSTR bstrExtra,
    /* [in] */ EPTRIGMODINFO tmInfo,
    /* [retval][out] */ ULONG __RPC_FAR *pulID);


void __RPC_STUB IEpTrigSrvcMod_Register_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_Revoke_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ ULONG ulID);


void __RPC_STUB IEpTrigSrvcMod_Revoke_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_Start_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ ULONG ulID,
    /* [in] */ BOOL bSendDynaInfoStrgs,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPTOKENINFO __RPC_FAR *pTokens);


void __RPC_STUB IEpTrigSrvcMod_Start_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_Stop_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ ULONG ulID);


void __RPC_STUB IEpTrigSrvcMod_Stop_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_ReleasePointers_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This);


void __RPC_STUB IEpTrigSrvcMod_ReleasePointers_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_GetTokenFlags_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ EPTRIGMODINFO tmInfo,
    /* [retval][out] */ ULONG __RPC_FAR *pulTokenFlags);


void __RPC_STUB IEpTrigSrvcMod_GetTokenFlags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigSrvcMod_GetStaticTokenInfo_Proxy( 
    IEpTrigSrvcMod __RPC_FAR * This,
    /* [in] */ EPTRIGMODINFO tmInfo,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrStaticInfo);


void __RPC_STUB IEpTrigSrvcMod_GetStaticTokenInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpTrigSrvcMod_INTERFACE_DEFINED__ */


#ifndef __IEpTrigClntMod_INTERFACE_DEFINED__
#define __IEpTrigClntMod_INTERFACE_DEFINED__

/* interface IEpTrigClntMod */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpTrigClntMod;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("4179B40B-E367-4c65-98BE-B7878D5D9586")
    IEpTrigClntMod : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetModuleName( 
            /* [retval][out] */ BSTR __RPC_FAR *pbstrName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Load( 
            /* [in] */ ULONG ulIndex,
            /* [in] */ EPTRIGMODINFO modInfo) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Retrieve( 
            /* [retval][out] */ EPTRIGMODINFO __RPC_FAR *pmodInfo) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetNumSubItems( 
            /* [in] */ ULONG ulIndex,
            /* [retval][out] */ short __RPC_FAR *psNum) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetItemText( 
            /* [in] */ ULONG ulIndex,
            /* [in] */ EPCLNTMODFLD eFld,
            /* [in] */ short sSubItem,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrText) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpTrigClntModVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpTrigClntMod __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpTrigClntMod __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpTrigClntMod __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetModuleName )( 
            IEpTrigClntMod __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Load )( 
            IEpTrigClntMod __RPC_FAR * This,
            /* [in] */ ULONG ulIndex,
            /* [in] */ EPTRIGMODINFO modInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Retrieve )( 
            IEpTrigClntMod __RPC_FAR * This,
            /* [retval][out] */ EPTRIGMODINFO __RPC_FAR *pmodInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNumSubItems )( 
            IEpTrigClntMod __RPC_FAR * This,
            /* [in] */ ULONG ulIndex,
            /* [retval][out] */ short __RPC_FAR *psNum);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetItemText )( 
            IEpTrigClntMod __RPC_FAR * This,
            /* [in] */ ULONG ulIndex,
            /* [in] */ EPCLNTMODFLD eFld,
            /* [in] */ short sSubItem,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrText);
        
        END_INTERFACE
    } IEpTrigClntModVtbl;

    interface IEpTrigClntMod
    {
        CONST_VTBL struct IEpTrigClntModVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpTrigClntMod_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpTrigClntMod_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpTrigClntMod_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpTrigClntMod_GetModuleName(This,pbstrName)	\
    (This)->lpVtbl -> GetModuleName(This,pbstrName)

#define IEpTrigClntMod_Load(This,ulIndex,modInfo)	\
    (This)->lpVtbl -> Load(This,ulIndex,modInfo)

#define IEpTrigClntMod_Retrieve(This,pmodInfo)	\
    (This)->lpVtbl -> Retrieve(This,pmodInfo)

#define IEpTrigClntMod_GetNumSubItems(This,ulIndex,psNum)	\
    (This)->lpVtbl -> GetNumSubItems(This,ulIndex,psNum)

#define IEpTrigClntMod_GetItemText(This,ulIndex,eFld,sSubItem,pbstrText)	\
    (This)->lpVtbl -> GetItemText(This,ulIndex,eFld,sSubItem,pbstrText)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigClntMod_GetModuleName_Proxy( 
    IEpTrigClntMod __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrName);


void __RPC_STUB IEpTrigClntMod_GetModuleName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigClntMod_Load_Proxy( 
    IEpTrigClntMod __RPC_FAR * This,
    /* [in] */ ULONG ulIndex,
    /* [in] */ EPTRIGMODINFO modInfo);


void __RPC_STUB IEpTrigClntMod_Load_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigClntMod_Retrieve_Proxy( 
    IEpTrigClntMod __RPC_FAR * This,
    /* [retval][out] */ EPTRIGMODINFO __RPC_FAR *pmodInfo);


void __RPC_STUB IEpTrigClntMod_Retrieve_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigClntMod_GetNumSubItems_Proxy( 
    IEpTrigClntMod __RPC_FAR * This,
    /* [in] */ ULONG ulIndex,
    /* [retval][out] */ short __RPC_FAR *psNum);


void __RPC_STUB IEpTrigClntMod_GetNumSubItems_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigClntMod_GetItemText_Proxy( 
    IEpTrigClntMod __RPC_FAR * This,
    /* [in] */ ULONG ulIndex,
    /* [in] */ EPCLNTMODFLD eFld,
    /* [in] */ short sSubItem,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrText);


void __RPC_STUB IEpTrigClntMod_GetItemText_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpTrigClntMod_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_epcommon_0271 */
/* [local] */ 

typedef struct  EPTRIGDEF
    {
    ULONG ulTrigID;
    ULONG ulUserData;
    ULONG ulChgHint;
    BSTR bstrTrigName;
    ULONG ulStatus;
    BSTR bstrUser;
    ULONG ulHystCnt;
    ULONG ulHyst;
    ULONG ulFiredCnt;
    ULONG ulDisableCnt;
    TIMEVAL tvTrigExpires;
    ULONG ulExecCnt;
    EPTRIGMCO eCondOpt;
    ULONG ulNumConditions;
    ULONG ulCondArrySize;
    /* [size_is] */ EPTRIGMODINFO __RPC_FAR *pCondArry;
    EPTRIGMAO eActOpt;
    ULONG ulNumActions;
    ULONG ulActArrySize;
    /* [size_is] */ EPTRIGMODINFO __RPC_FAR *pActArry;
    }	EPTRIGDEF;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0271_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0271_v0_0_s_ifspec;

#ifndef __IEpTrigCtrl_INTERFACE_DEFINED__
#define __IEpTrigCtrl_INTERFACE_DEFINED__

/* interface IEpTrigCtrl */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpTrigCtrl;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("13A149FF-CF5E-4e59-9CEA-5AAC1DBFC5F4")
    IEpTrigCtrl : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE AreYouAlive( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPluginInfo( 
            /* [retval][out] */ EPTRIGPLUGINS __RPC_FAR *pPlugins) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Connect( 
            /* [in] */ DWORD dwCBCookie,
            /* [in] */ IEpDirRtr __RPC_FAR *pDirRtr,
            /* [retval][out] */ ULONG __RPC_FAR *pulClientID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Disconnect( 
            /* [in] */ ULONG ulClientID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ExecuteDirective( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ BSTR bstrDir) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE InsertTrigger( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ EPTRIGDEF TrigDef,
            /* [retval][out] */ ULONG __RPC_FAR *pulTrigID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE DeleteTrigger( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulTrigID) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE UpdateTrigger( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ EPTRIGDEF TrigDef) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ControlTrigger( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulTrigID,
            /* [in] */ ULONG ulFlags) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE InsertItem( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulTrigID,
            /* [in] */ EPTRIGMODTYPE eModType,
            /* [in] */ EPTRIGMODINFO tmInfo) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE DeleteItem( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulTrigID,
            /* [in] */ EPTRIGMODTYPE eModType,
            /* [in] */ ULONG ulIndex) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE UpdateItem( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulTrigID,
            /* [in] */ EPTRIGMODTYPE eModType,
            /* [in] */ EPTRIGMODINFO tmInfo) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestTrigInfoSrvc( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RequestTrigLogSrvc( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE CancelService( 
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulSrvcHandle) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpTrigCtrlVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpTrigCtrl __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpTrigCtrl __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AreYouAlive )( 
            IEpTrigCtrl __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPluginInfo )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [retval][out] */ EPTRIGPLUGINS __RPC_FAR *pPlugins);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Connect )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ DWORD dwCBCookie,
            /* [in] */ IEpDirRtr __RPC_FAR *pDirRtr,
            /* [retval][out] */ ULONG __RPC_FAR *pulClientID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Disconnect )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExecuteDirective )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ BSTR bstrDir);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *InsertTrigger )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ EPTRIGDEF TrigDef,
            /* [retval][out] */ ULONG __RPC_FAR *pulTrigID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DeleteTrigger )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulTrigID);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *UpdateTrigger )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ EPTRIGDEF TrigDef);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ControlTrigger )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulTrigID,
            /* [in] */ ULONG ulFlags);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *InsertItem )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulTrigID,
            /* [in] */ EPTRIGMODTYPE eModType,
            /* [in] */ EPTRIGMODINFO tmInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DeleteItem )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulTrigID,
            /* [in] */ EPTRIGMODTYPE eModType,
            /* [in] */ ULONG ulIndex);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *UpdateItem )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulTrigID,
            /* [in] */ EPTRIGMODTYPE eModType,
            /* [in] */ EPTRIGMODINFO tmInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestTrigInfoSrvc )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *RequestTrigLogSrvc )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulUserData,
            /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *CancelService )( 
            IEpTrigCtrl __RPC_FAR * This,
            /* [in] */ ULONG ulClientID,
            /* [in] */ ULONG ulSrvcHandle);
        
        END_INTERFACE
    } IEpTrigCtrlVtbl;

    interface IEpTrigCtrl
    {
        CONST_VTBL struct IEpTrigCtrlVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpTrigCtrl_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpTrigCtrl_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpTrigCtrl_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpTrigCtrl_AreYouAlive(This)	\
    (This)->lpVtbl -> AreYouAlive(This)

#define IEpTrigCtrl_GetPluginInfo(This,pPlugins)	\
    (This)->lpVtbl -> GetPluginInfo(This,pPlugins)

#define IEpTrigCtrl_Connect(This,dwCBCookie,pDirRtr,pulClientID)	\
    (This)->lpVtbl -> Connect(This,dwCBCookie,pDirRtr,pulClientID)

#define IEpTrigCtrl_Disconnect(This,ulClientID)	\
    (This)->lpVtbl -> Disconnect(This,ulClientID)

#define IEpTrigCtrl_ExecuteDirective(This,ulClientID,bstrDir)	\
    (This)->lpVtbl -> ExecuteDirective(This,ulClientID,bstrDir)

#define IEpTrigCtrl_InsertTrigger(This,ulClientID,TrigDef,pulTrigID)	\
    (This)->lpVtbl -> InsertTrigger(This,ulClientID,TrigDef,pulTrigID)

#define IEpTrigCtrl_DeleteTrigger(This,ulClientID,ulTrigID)	\
    (This)->lpVtbl -> DeleteTrigger(This,ulClientID,ulTrigID)

#define IEpTrigCtrl_UpdateTrigger(This,ulClientID,TrigDef)	\
    (This)->lpVtbl -> UpdateTrigger(This,ulClientID,TrigDef)

#define IEpTrigCtrl_ControlTrigger(This,ulClientID,ulTrigID,ulFlags)	\
    (This)->lpVtbl -> ControlTrigger(This,ulClientID,ulTrigID,ulFlags)

#define IEpTrigCtrl_InsertItem(This,ulClientID,ulTrigID,eModType,tmInfo)	\
    (This)->lpVtbl -> InsertItem(This,ulClientID,ulTrigID,eModType,tmInfo)

#define IEpTrigCtrl_DeleteItem(This,ulClientID,ulTrigID,eModType,ulIndex)	\
    (This)->lpVtbl -> DeleteItem(This,ulClientID,ulTrigID,eModType,ulIndex)

#define IEpTrigCtrl_UpdateItem(This,ulClientID,ulTrigID,eModType,tmInfo)	\
    (This)->lpVtbl -> UpdateItem(This,ulClientID,ulTrigID,eModType,tmInfo)

#define IEpTrigCtrl_RequestTrigInfoSrvc(This,ulClientID,ulUserData,pulSrvcHandle)	\
    (This)->lpVtbl -> RequestTrigInfoSrvc(This,ulClientID,ulUserData,pulSrvcHandle)

#define IEpTrigCtrl_RequestTrigLogSrvc(This,ulClientID,ulUserData,pulSrvcHandle)	\
    (This)->lpVtbl -> RequestTrigLogSrvc(This,ulClientID,ulUserData,pulSrvcHandle)

#define IEpTrigCtrl_CancelService(This,ulClientID,ulSrvcHandle)	\
    (This)->lpVtbl -> CancelService(This,ulClientID,ulSrvcHandle)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_AreYouAlive_Proxy( 
    IEpTrigCtrl __RPC_FAR * This);


void __RPC_STUB IEpTrigCtrl_AreYouAlive_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_GetPluginInfo_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [retval][out] */ EPTRIGPLUGINS __RPC_FAR *pPlugins);


void __RPC_STUB IEpTrigCtrl_GetPluginInfo_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_Connect_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ DWORD dwCBCookie,
    /* [in] */ IEpDirRtr __RPC_FAR *pDirRtr,
    /* [retval][out] */ ULONG __RPC_FAR *pulClientID);


void __RPC_STUB IEpTrigCtrl_Connect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_Disconnect_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID);


void __RPC_STUB IEpTrigCtrl_Disconnect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_ExecuteDirective_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ BSTR bstrDir);


void __RPC_STUB IEpTrigCtrl_ExecuteDirective_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_InsertTrigger_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ EPTRIGDEF TrigDef,
    /* [retval][out] */ ULONG __RPC_FAR *pulTrigID);


void __RPC_STUB IEpTrigCtrl_InsertTrigger_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_DeleteTrigger_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulTrigID);


void __RPC_STUB IEpTrigCtrl_DeleteTrigger_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_UpdateTrigger_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ EPTRIGDEF TrigDef);


void __RPC_STUB IEpTrigCtrl_UpdateTrigger_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_ControlTrigger_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulTrigID,
    /* [in] */ ULONG ulFlags);


void __RPC_STUB IEpTrigCtrl_ControlTrigger_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_InsertItem_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulTrigID,
    /* [in] */ EPTRIGMODTYPE eModType,
    /* [in] */ EPTRIGMODINFO tmInfo);


void __RPC_STUB IEpTrigCtrl_InsertItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_DeleteItem_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulTrigID,
    /* [in] */ EPTRIGMODTYPE eModType,
    /* [in] */ ULONG ulIndex);


void __RPC_STUB IEpTrigCtrl_DeleteItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_UpdateItem_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulTrigID,
    /* [in] */ EPTRIGMODTYPE eModType,
    /* [in] */ EPTRIGMODINFO tmInfo);


void __RPC_STUB IEpTrigCtrl_UpdateItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_RequestTrigInfoSrvc_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle);


void __RPC_STUB IEpTrigCtrl_RequestTrigInfoSrvc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_RequestTrigLogSrvc_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulUserData,
    /* [retval][out] */ ULONG __RPC_FAR *pulSrvcHandle);


void __RPC_STUB IEpTrigCtrl_RequestTrigLogSrvc_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpTrigCtrl_CancelService_Proxy( 
    IEpTrigCtrl __RPC_FAR * This,
    /* [in] */ ULONG ulClientID,
    /* [in] */ ULONG ulSrvcHandle);


void __RPC_STUB IEpTrigCtrl_CancelService_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpTrigCtrl_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_epcommon_0272 */
/* [local] */ 

typedef struct  EPTRIGLOGDATA
    {
    ULONG ulUserData;
    ULONG ulArrySize;
    /* [size_is] */ BSTR __RPC_FAR *pArryLogMsgs;
    }	EPTRIGLOGDATA;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0272_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0272_v0_0_s_ifspec;

#ifndef ___IEpTrigSrvcCB_INTERFACE_DEFINED__
#define ___IEpTrigSrvcCB_INTERFACE_DEFINED__

/* interface _IEpTrigSrvcCB */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID__IEpTrigSrvcCB;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C5960CBB-1822-4af7-B8E3-E58D55341C50")
    _IEpTrigSrvcCB : public _IEpochAliveCB
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE TrigInfoSrvcCB( 
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPTRIGDEF __RPC_FAR *pArryTrigDef) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE TrigLogSrvcCB( 
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPTRIGLOGDATA __RPC_FAR *pArryLogData) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct _IEpTrigSrvcCBVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            _IEpTrigSrvcCB __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            _IEpTrigSrvcCB __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            _IEpTrigSrvcCB __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *AreYouAliveCB )( 
            _IEpTrigSrvcCB __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *TrigInfoSrvcCB )( 
            _IEpTrigSrvcCB __RPC_FAR * This,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPTRIGDEF __RPC_FAR *pArryTrigDef);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *TrigLogSrvcCB )( 
            _IEpTrigSrvcCB __RPC_FAR * This,
            /* [in] */ ULONG ulArrySize,
            /* [size_is][in] */ EPTRIGLOGDATA __RPC_FAR *pArryLogData);
        
        END_INTERFACE
    } _IEpTrigSrvcCBVtbl;

    interface _IEpTrigSrvcCB
    {
        CONST_VTBL struct _IEpTrigSrvcCBVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IEpTrigSrvcCB_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IEpTrigSrvcCB_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IEpTrigSrvcCB_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IEpTrigSrvcCB_AreYouAliveCB(This)	\
    (This)->lpVtbl -> AreYouAliveCB(This)


#define _IEpTrigSrvcCB_TrigInfoSrvcCB(This,ulArrySize,pArryTrigDef)	\
    (This)->lpVtbl -> TrigInfoSrvcCB(This,ulArrySize,pArryTrigDef)

#define _IEpTrigSrvcCB_TrigLogSrvcCB(This,ulArrySize,pArryLogData)	\
    (This)->lpVtbl -> TrigLogSrvcCB(This,ulArrySize,pArryLogData)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpTrigSrvcCB_TrigInfoSrvcCB_Proxy( 
    _IEpTrigSrvcCB __RPC_FAR * This,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPTRIGDEF __RPC_FAR *pArryTrigDef);


void __RPC_STUB _IEpTrigSrvcCB_TrigInfoSrvcCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE _IEpTrigSrvcCB_TrigLogSrvcCB_Proxy( 
    _IEpTrigSrvcCB __RPC_FAR * This,
    /* [in] */ ULONG ulArrySize,
    /* [size_is][in] */ EPTRIGLOGDATA __RPC_FAR *pArryLogData);


void __RPC_STUB _IEpTrigSrvcCB_TrigLogSrvcCB_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* ___IEpTrigSrvcCB_INTERFACE_DEFINED__ */


/* interface __MIDL_itf_epcommon_0273 */
/* [local] */ 

typedef struct  EPNVPair
    {
    BSTR bstrName;
    EPVAR vdValue;
    }	EPNVPair;

typedef struct  EPNVSeq
    {
    ULONG ulSize;
    /* [size_is] */ EPNVPair __RPC_FAR *pPairs;
    }	EPNVSeq;

typedef struct  EPIRRec
    {
    IUnknown __RPC_FAR *pUnk;
    EPNVSeq nvsAttrib;
    }	EPIRRec;

typedef struct  EPIRRecSeq
    {
    ULONG ulSize;
    /* [size_is] */ EPIRRec __RPC_FAR *pArry;
    }	EPIRRecSeq;



extern RPC_IF_HANDLE __MIDL_itf_epcommon_0273_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epcommon_0273_v0_0_s_ifspec;

#ifndef __IEpIR_INTERFACE_DEFINED__
#define __IEpIR_INTERFACE_DEFINED__

/* interface IEpIR */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IEpIR;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C6209DE4-9AA5-4508-B00F-689BCF9E6CC5")
    IEpIR : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDefaultApp( 
            /* [in] */ BSTR bstrName,
            /* [out] */ BSTR __RPC_FAR *pbstrName,
            /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetIRRecs( 
            /* [retval][out] */ EPIRRecSeq __RPC_FAR *pseqRecs) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetNewAppPtr( 
            /* [in] */ BSTR bstrExeName,
            /* [in] */ BSTR bstrCmdLineArgs,
            /* [out] */ BSTR __RPC_FAR *pbstrName,
            /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPtr( 
            /* [in] */ BSTR bstrName,
            /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPtrWithAttr( 
            /* [in] */ BSTR bstrPartialName,
            /* [in] */ EPNVSeq nvsAttrib,
            /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetPtr( 
            /* [in] */ BSTR bstrName,
            /* [in] */ BSTR bstrRetArg,
            /* [in] */ IUnknown __RPC_FAR *pIUnk,
            /* [in] */ EPNVSeq nvsAttrib) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpIRVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpIR __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpIR __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpIR __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDefaultApp )( 
            IEpIR __RPC_FAR * This,
            /* [in] */ BSTR bstrName,
            /* [out] */ BSTR __RPC_FAR *pbstrName,
            /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIRRecs )( 
            IEpIR __RPC_FAR * This,
            /* [retval][out] */ EPIRRecSeq __RPC_FAR *pseqRecs);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNewAppPtr )( 
            IEpIR __RPC_FAR * This,
            /* [in] */ BSTR bstrExeName,
            /* [in] */ BSTR bstrCmdLineArgs,
            /* [out] */ BSTR __RPC_FAR *pbstrName,
            /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPtr )( 
            IEpIR __RPC_FAR * This,
            /* [in] */ BSTR bstrName,
            /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPtrWithAttr )( 
            IEpIR __RPC_FAR * This,
            /* [in] */ BSTR bstrPartialName,
            /* [in] */ EPNVSeq nvsAttrib,
            /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetPtr )( 
            IEpIR __RPC_FAR * This,
            /* [in] */ BSTR bstrName,
            /* [in] */ BSTR bstrRetArg,
            /* [in] */ IUnknown __RPC_FAR *pIUnk,
            /* [in] */ EPNVSeq nvsAttrib);
        
        END_INTERFACE
    } IEpIRVtbl;

    interface IEpIR
    {
        CONST_VTBL struct IEpIRVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpIR_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpIR_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpIR_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpIR_GetDefaultApp(This,bstrName,pbstrName,ppIUnk)	\
    (This)->lpVtbl -> GetDefaultApp(This,bstrName,pbstrName,ppIUnk)

#define IEpIR_GetIRRecs(This,pseqRecs)	\
    (This)->lpVtbl -> GetIRRecs(This,pseqRecs)

#define IEpIR_GetNewAppPtr(This,bstrExeName,bstrCmdLineArgs,pbstrName,ppIUnk)	\
    (This)->lpVtbl -> GetNewAppPtr(This,bstrExeName,bstrCmdLineArgs,pbstrName,ppIUnk)

#define IEpIR_GetPtr(This,bstrName,ppIUnk)	\
    (This)->lpVtbl -> GetPtr(This,bstrName,ppIUnk)

#define IEpIR_GetPtrWithAttr(This,bstrPartialName,nvsAttrib,ppIUnk)	\
    (This)->lpVtbl -> GetPtrWithAttr(This,bstrPartialName,nvsAttrib,ppIUnk)

#define IEpIR_SetPtr(This,bstrName,bstrRetArg,pIUnk,nvsAttrib)	\
    (This)->lpVtbl -> SetPtr(This,bstrName,bstrRetArg,pIUnk,nvsAttrib)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpIR_GetDefaultApp_Proxy( 
    IEpIR __RPC_FAR * This,
    /* [in] */ BSTR bstrName,
    /* [out] */ BSTR __RPC_FAR *pbstrName,
    /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk);


void __RPC_STUB IEpIR_GetDefaultApp_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpIR_GetIRRecs_Proxy( 
    IEpIR __RPC_FAR * This,
    /* [retval][out] */ EPIRRecSeq __RPC_FAR *pseqRecs);


void __RPC_STUB IEpIR_GetIRRecs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpIR_GetNewAppPtr_Proxy( 
    IEpIR __RPC_FAR * This,
    /* [in] */ BSTR bstrExeName,
    /* [in] */ BSTR bstrCmdLineArgs,
    /* [out] */ BSTR __RPC_FAR *pbstrName,
    /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk);


void __RPC_STUB IEpIR_GetNewAppPtr_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpIR_GetPtr_Proxy( 
    IEpIR __RPC_FAR * This,
    /* [in] */ BSTR bstrName,
    /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk);


void __RPC_STUB IEpIR_GetPtr_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpIR_GetPtrWithAttr_Proxy( 
    IEpIR __RPC_FAR * This,
    /* [in] */ BSTR bstrPartialName,
    /* [in] */ EPNVSeq nvsAttrib,
    /* [out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk);


void __RPC_STUB IEpIR_GetPtrWithAttr_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpIR_SetPtr_Proxy( 
    IEpIR __RPC_FAR * This,
    /* [in] */ BSTR bstrName,
    /* [in] */ BSTR bstrRetArg,
    /* [in] */ IUnknown __RPC_FAR *pIUnk,
    /* [in] */ EPNVSeq nvsAttrib);


void __RPC_STUB IEpIR_SetPtr_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpIR_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
