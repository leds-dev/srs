//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by EpExtendedHelp.rc
//
#define IDS_PROJNAME                    100
#define IDS_NO_TLM_EX_HELP              101
#define IDR_EPEXHELP                    102
#define IDS_NO_TLM_FILE                 102
#define IDS_EX_HELP_NOT_AVAILABLE       103
#define IDS_EXHELP_NOT_AVAILABLE        104
#define IDS_NO_CMD_EX_HELP              105
#define IDS_NO_CMD_FILE                 106
#define IDD_DLG_EXAMPLE_EXHELP          201
#define IDC_STATIC_MNEM                 207
#define IDC_STATIC_TYPE                 208
#define IDC_STATIC_NAME                 210
#define IDC_STATIC_NUM                  211
#define IDC_STATIC_STRM                 212
#define IDC_STATIC_HOST                 213
#define IDC_EDIT_EXHELP                 215
#define IDC_EDIT_MSG                    217

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        202
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         218
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
