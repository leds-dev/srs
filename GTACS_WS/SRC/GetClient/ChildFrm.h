#if !defined(AFX_CHILDFRM_H__7A517F30_1B26_493A_BCC5_FAD796A4FCE4__INCLUDED_)
#define AFX_CHILDFRM_H__7A517F30_1B26_493A_BCC5_FAD796A4FCE4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChildFrm.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CChildFrame frame

class CChildFrame : public CBCGPMDIChildWnd
{
	DECLARE_DYNCREATE(CChildFrame)
protected:
	CChildFrame();           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChildFrame)
	public:
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL
	virtual BOOL Create(
					LPCTSTR			lpszClassName,
					LPCTSTR			lpszWindowName,
					DWORD			dwStyle		= WS_CHILD | WS_VISIBLE | WS_OVERLAPPEDWINDOW,
					const RECT&		rect		= rectDefault,
					CMDIFrameWnd*	pParentWnd	= NULL,
					CCreateContext* pContext	= NULL);

// Implementation
protected:
	virtual ~CChildFrame();

	// Generated message map functions
	//{{AFX_MSG(CChildFrame)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHILDFRM_H__7A517F30_1B26_493A_BCC5_FAD796A4FCE4__INCLUDED_)
