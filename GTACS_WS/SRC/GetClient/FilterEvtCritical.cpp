// FilterEvtCritical.cpp : implementation file
//

#include "stdafx.h"
#include "GET.h"
#include "FilterEvtCritical.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtCritical property page

IMPLEMENT_DYNCREATE(CFilterEvtCritical, CPropertyPage)

CFilterEvtCritical::CFilterEvtCritical() : CPropertyPage(CFilterEvtCritical::IDD)
{
	//{{AFX_DATA_INIT(CFilterEvtCritical)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CFilterEvtCritical::~CFilterEvtCritical()
{
}

void CFilterEvtCritical::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFilterEvtCritical)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFilterEvtCritical, CPropertyPage)
	//{{AFX_MSG_MAP(CFilterEvtCritical)
	ON_BN_CLICKED(IDC_SELECT_ALL_BUTTON, OnSelectAllButton)
	ON_BN_CLICKED(IDC_CLEAR_BUTTON, OnClearButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtCritical message handlers

void CFilterEvtCritical::OnSelectAllButton()
{
	((CButton*)GetDlgItem(IDC_ALARM_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_FATAL_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_NORMAL_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_INFO_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_WARNING_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_USC_CHECK))->SetCheck(1);
}

void CFilterEvtCritical::OnClearButton()
{
	((CButton*)GetDlgItem(IDC_ALARM_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_FATAL_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_NORMAL_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_INFO_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_WARNING_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_USC_CHECK))->SetCheck(0);
}

void CFilterEvtCritical::UpdateMsg(CPtrList* pList)
{
	CString		strTxt;
	POSITION	pos		= pList->FindIndex(0);
	MSG_GET*	pMsgGET = (MSG_GET*)pList->GetAt(pos);

	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_ALARM_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evtcrit_select_list[0] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_FATAL_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evtcrit_select_list[1] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_NORMAL_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evtcrit_select_list[2] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_INFO_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evtcrit_select_list[3] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_WARNING_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evtcrit_select_list[4] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_USC_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evtcrit_select_list[5] = (char)strTxt[0];
}

void CFilterEvtCritical::SaveOpt(CStdioFile* pfile)
{
	CString strTxt;

	pfile->WriteString(_T("[Event Criticality]\n"));
	strTxt.Format(_T("    Alarm     %d\n"), ((CButton*)GetDlgItem(IDC_ALARM_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Fatal     %d\n"), ((CButton*)GetDlgItem(IDC_FATAL_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Normal    %d\n"), ((CButton*)GetDlgItem(IDC_NORMAL_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Info      %d\n"), ((CButton*)GetDlgItem(IDC_INFO_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Warning   %d\n"), ((CButton*)GetDlgItem(IDC_WARNING_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    USC       %d\n"), ((CButton*)GetDlgItem(IDC_USC_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	pfile->WriteString(_T("[/Event Criticality]\n"));
	pfile->WriteString(_T("\n"));
}

void CFilterEvtCritical::RestoreOpt(CStringArray* p_sa)
{
	CString strLine;
	int		nIndex = 0;

	OnClearButton();

	while( nIndex < p_sa->GetSize() )
	{
		strLine = p_sa->GetAt(nIndex);
		strLine.TrimLeft();
		strLine.TrimRight();
		nIndex++;

		if( (!strLine.IsEmpty()) && strLine[0] != _T('#') )
		{
			if( strLine.Find(_T("[Event Criticality]")) != -1 )
			{
				while( strLine != _T("[/Event Criticality]") )
				{
					strLine = p_sa->GetAt(nIndex);
					strLine.TrimLeft();
					strLine.TrimRight();
					nIndex++;

					if( !strLine.IsEmpty() )
					{
						if( strLine.Find(_T("Alarm")) != -1 ||
								strLine.Find(_T("ALARM")) != -1 )
							SetRestoreItem(strLine,5,IDC_ALARM_CHECK);

						else if( strLine.Find(_T("Fatal")) != -1 ||
								strLine.Find(_T("FATAL")) != -1 )
							SetRestoreItem(strLine,5,IDC_FATAL_CHECK);

						else if( strLine.Find(_T("Normal")) != -1 ||
								strLine.Find(_T("NORMAL")) != -1 )
							SetRestoreItem(strLine,6,IDC_NORMAL_CHECK);

						else if( strLine.Find(_T("Info")) != -1 ||
								strLine.Find(_T("INFO")) != -1 )
							SetRestoreItem(strLine,4,IDC_INFO_CHECK);

						else if( strLine.Find(_T("Warning")) != -1 ||
								strLine.Find(_T("WARNING")) != -1 )
							SetRestoreItem(strLine,7,IDC_WARNING_CHECK);

						else if( strLine.Find(_T("USC")) != -1 ||
								strLine.Find(_T("usc")) != -1 )
							SetRestoreItem(strLine,3,IDC_USC_CHECK);
					}
				}

				break;
			}
		}
	}
}

void CFilterEvtCritical::SetRestoreItem(CString s, int d,UINT res)
{
	s.Delete(0, d);
	s.TrimLeft();
	s.TrimRight();

	if( !s.IsEmpty() && (s == _T("0") || s == _T("1")) )
		((CButton*)GetDlgItem(res))->SetCheck(((BOOL)_ttoi(s)));
}
