#if !defined(AFX_FILTEREVTCRITICAL_H__76E79102_DADA_4D7F_A94E_FEBA56901BDD__INCLUDED_)
#define AFX_FILTEREVTCRITICAL_H__76E79102_DADA_4D7F_A94E_FEBA56901BDD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FilterEvtCritical.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtCritical dialog

class CFilterEvtCritical : public CPropertyPage
{
	DECLARE_DYNCREATE(CFilterEvtCritical)

// Construction
public:
	CFilterEvtCritical();
	~CFilterEvtCritical();

	void UpdateMsg(CPtrList* pList);
	void SaveOpt(CStdioFile* pfile);
	void RestoreOpt(CStringArray* p_sa);

// Dialog Data
	//{{AFX_DATA(CFilterEvtCritical)
	enum { IDD = IDD_FILTER_EVTCRITICAL };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CFilterEvtCritical)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CFilterEvtCritical)
	afx_msg void OnSelectAllButton();
	afx_msg void OnClearButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void SetRestoreItem(CString s, int d,UINT res);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILTEREVTCRITICAL_H__76E79102_DADA_4D7F_A94E_FEBA56901BDD__INCLUDED_)
