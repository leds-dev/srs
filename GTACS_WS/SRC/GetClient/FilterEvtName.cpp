// FilterEvtName.cpp : implementation file
//

#include "stdafx.h"
#include "GET.h"
#include "GETFilterPropSheet.h"
#include "FilterEvtName.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtName property page

IMPLEMENT_DYNCREATE(CFilterEvtName, CPropertyPage)

CFilterEvtName::CFilterEvtName() : CPropertyPage(CFilterEvtName::IDD)
{
	//{{AFX_DATA_INIT(CFilterEvtName)
	//}}AFX_DATA_INIT
}

CFilterEvtName::~CFilterEvtName()
{
}

void CFilterEvtName::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFilterEvtName)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFilterEvtName, CPropertyPage)
	//{{AFX_MSG_MAP(CFilterEvtName)
	ON_BN_CLICKED(IDC_SET_BUTTON, OnSetButton)
	ON_BN_CLICKED(IDC_CLEAR_BUTTON, OnClearButton)
	ON_BN_CLICKED(IDC_CLEAR_ALL_BUTTON, OnClearAllButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtName message handlers

void CFilterEvtName::OnSetButton()
{
	CString strText;

	((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->GetWindowText(strText);

	if( strText.IsEmpty() )
		return;

	((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->AddString(strText);
	((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->SetEditSel(0, -1);
	((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->Clear();
}

void CFilterEvtName::OnClearButton()
{
	int nIndex = ((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->GetCurSel();

	if( nIndex != LB_ERR )
	{
		((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->DeleteString(nIndex);
		((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->SetEditSel(0, -1);
		((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->Clear();
	}
}

void CFilterEvtName::OnClearAllButton()
{
	((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->ResetContent();
}

void CFilterEvtName::UpdateMsg(CPtrList* pList)
{
	CString		strTxt;
	MSG_GET*	pMsgGET = NULL;
	int			nCnt	= ((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->GetCount();

	if( nCnt == 0 )
		return;

	((CGETFilterPropSheet*)GetParent())->NeedNewMsgs(pList, nCnt, EVT_NAME_LIST_SIZE);

	USES_CONVERSION;
	POSITION	pos		= pList->GetHeadPosition();
	int			nLen	= 0;
	int			nMsg	= 0;
	int			nIdx	= 0;

	while( pos != NULL )
	{
		pMsgGET	= (MSG_GET*)pList->GetNext(pos);

		for( int i = 0; i < nCnt; i++ )
		{
			if( nMsg < EVT_NAME_LIST_SIZE )
			{
				nLen = ((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->GetLBTextLen(nIdx);
				((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->GetLBText(nIdx, strTxt.GetBuffer(nLen));
				strcpy_s(pMsgGET->msg_get_filter_data.evtname_list[nMsg], T2A((LPTSTR)((LPCTSTR)strTxt)));
				strTxt.ReleaseBuffer();
				nMsg++; nIdx++;
			}
			else
			{
				nMsg = 0;
				break;
			}
		}
	}
}

void CFilterEvtName::SaveOpt(CStdioFile* pfile)
{
	CString strTxt;
	CString strTmp;
	int		nCnt = ((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->GetCount();
	int		nLen = 0;

	pfile->WriteString(_T("[Event Name]\n"));

	for( int i = 0; i < nCnt; i++ )
	{
		nLen = ((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->GetLBTextLen(i);
		((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->GetLBText(i, strTxt.GetBuffer(nLen));
		strTmp.Format(_T("    %s\n"), strTxt);
		pfile->WriteString(strTmp);
		strTxt.ReleaseBuffer();
	}

	pfile->WriteString(_T("[/Event Name]\n"));
	pfile->WriteString(_T("\n"));
}

void CFilterEvtName::RestoreOpt(CStringArray* p_sa)
{
	CString strLine;
	int		nIndex = 0;

	OnClearAllButton();

	while( nIndex < p_sa->GetSize() )
	{
		strLine = p_sa->GetAt(nIndex);
		strLine.TrimLeft();
		strLine.TrimRight();
		nIndex++;

		if( (!strLine.IsEmpty()) && strLine[0] != _T('#') )
		{
			if( strLine.Find(_T("[Event Name]")) != -1 )
			{
				while( strLine != _T("[/Event Name]") )
				{
					strLine = p_sa->GetAt(nIndex);
					strLine.TrimLeft();
					strLine.TrimRight();
					nIndex++;

					if( strLine != _T("[/Event Name]") )
						((CComboBox*)GetDlgItem(IDC_EVT_NAME_COMBO))->AddString(strLine);
				}

				break;
			}
		}
	}
}

