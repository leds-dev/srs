#if !defined(AFX_FILTEREVTNAME_H__D239C4AD_778F_4303_88D9_2A38138A6692__INCLUDED_)
#define AFX_FILTEREVTNAME_H__D239C4AD_778F_4303_88D9_2A38138A6692__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FilterEvtName.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtName dialog

class CFilterEvtName : public CPropertyPage
{
	DECLARE_DYNCREATE(CFilterEvtName)

// Construction
public:
	CFilterEvtName();
	~CFilterEvtName();

	void UpdateMsg(CPtrList* pList);
	void SaveOpt(CStdioFile* pfile);
	void RestoreOpt(CStringArray* p_sa);

// Dialog Data
	//{{AFX_DATA(CFilterEvtName)
	enum { IDD = IDD_FILTER_EVTNAME };
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CFilterEvtName)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CFilterEvtName)
	afx_msg void OnSetButton();
	afx_msg void OnClearButton();
	afx_msg void OnClearAllButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILTEREVTNAME_H__D239C4AD_778F_4303_88D9_2A38138A6692__INCLUDED_)
