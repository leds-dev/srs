// FilterEvtNum.cpp : implementation file
//

#include "stdafx.h"
#include "GET.h"
#include "GETFilterPropSheet.h"
#include "FilterEvtNum.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtNum property page

IMPLEMENT_DYNCREATE(CFilterEvtNum, CPropertyPage)

CFilterEvtNum::CFilterEvtNum() : CPropertyPage(CFilterEvtNum::IDD)
{
	//{{AFX_DATA_INIT(CFilterEvtNum)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CFilterEvtNum::~CFilterEvtNum()
{
}

void CFilterEvtNum::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFilterEvtNum)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFilterEvtNum, CPropertyPage)
	//{{AFX_MSG_MAP(CFilterEvtNum)
	ON_BN_CLICKED(IDC_CLEAR_ALL_BUTTON, OnClearAllButton)
	ON_BN_CLICKED(IDC_SET_BUTTON, OnSetButton)
	ON_BN_CLICKED(IDC_CLEAR_BUTTON, OnClearButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtNum message handlers

void CFilterEvtNum::OnClearAllButton()
{
	((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->ResetContent();
}

void CFilterEvtNum::OnSetButton()
{
	CString strText;

	((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->GetWindowText(strText);

	if( strText.IsEmpty() )
		return;

	((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->AddString(strText);
	((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->SetEditSel(0, -1);
	((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->Clear();
}

void CFilterEvtNum::OnClearButton()
{
	int nIndex = ((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->GetCurSel();

	if( nIndex != LB_ERR )
	{
		((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->DeleteString(nIndex);
		((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->SetEditSel(0, -1);
		((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->Clear();
	}
}

void CFilterEvtNum::UpdateMsg(CPtrList* pList)
{
	CString		strTxt;
	MSG_GET*	pMsgGET = NULL;
	int			nCnt	= ((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->GetCount();

	if( nCnt == 0 )
		return;

	((CGETFilterPropSheet*)GetParent())->NeedNewMsgs(pList, nCnt, EVENT_LIST_SIZE);

	POSITION	pos	= pList->GetHeadPosition();
	int			nLen= 0;
	int			nMsg= 0;
	int			nIdx= 0;

	while( pos != NULL )
	{
		pMsgGET	= (MSG_GET*)pList->GetNext(pos);

		while( nIdx < nCnt )
		{
			if( nMsg < EVENT_LIST_SIZE )
			{
				nLen = ((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->GetLBTextLen(nIdx);
				((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->GetLBText(nIdx, strTxt.GetBuffer(nLen));
				pMsgGET->msg_get_filter_data.evtnum_list[nMsg] = htonl((DWORD)_ttoi(strTxt));
				strTxt.ReleaseBuffer();
				nMsg++; nIdx++;
			}
			else
			{
				nMsg = 0;
				break;
			}
		}
	}
}

void CFilterEvtNum::SaveOpt(CStdioFile* pfile)
{
	CString strTxt;
	CString strTmp;
	int		nCnt = ((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->GetCount();
	int		nLen = 0;

	pfile->WriteString(_T("[Event Number]\n"));

	for( int i = 0; i < nCnt; i++ )
	{
		nLen = ((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->GetLBTextLen(i);
		((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->GetLBText(i, strTxt.GetBuffer(nLen));
		strTmp.Format(_T("    %s\n"), strTxt);
		pfile->WriteString(strTmp);
		strTxt.ReleaseBuffer();
	}

	pfile->WriteString(_T("[/Event Number]\n"));
	pfile->WriteString(_T("\n"));
}

void CFilterEvtNum::RestoreOpt(CStringArray* p_sa)
{
	CString strLine;
	int		nIndex = 0;

	OnClearAllButton();

	while( nIndex < p_sa->GetSize() )
	{
		strLine = p_sa->GetAt(nIndex);
		strLine.TrimLeft();
		strLine.TrimRight();
		nIndex++;

		if( (!strLine.IsEmpty()) && strLine[0] != _T('#') )
		{
			if( strLine.Find(_T("[Event Number]")) != -1 )
			{
				while( strLine != _T("[/Event Number]") )
				{
					strLine = p_sa->GetAt(nIndex);
					strLine.TrimLeft();
					strLine.TrimRight();
					nIndex++;

					if( strLine != _T("[/Event Number]") )
						((CComboBox*)GetDlgItem(IDC_EVT_NUM_COMBO))->AddString(strLine);
				}

				break;
			}
		}
	}
}

