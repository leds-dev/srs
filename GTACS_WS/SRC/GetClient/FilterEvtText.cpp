// FilterEvtText.cpp : implementation file
//

#include "stdafx.h"
#include "GET.h"
#include "FilterEvtText.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtText property page

IMPLEMENT_DYNCREATE(CFilterEvtText, CPropertyPage)

CFilterEvtText::CFilterEvtText() : CPropertyPage(CFilterEvtText::IDD)
{
	//{{AFX_DATA_INIT(CFilterEvtText)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CFilterEvtText::~CFilterEvtText()
{
}

void CFilterEvtText::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFilterEvtText)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFilterEvtText, CPropertyPage)
	//{{AFX_MSG_MAP(CFilterEvtText)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtText message handlers

void CFilterEvtText::UpdateMsg(CPtrList* pList)
{
	CString		strTxt;
	POSITION	pos		= pList->FindIndex(0);
	MSG_GET*	pMsgGET = (MSG_GET*)pList->GetAt(pos);

	((CEdit*)GetDlgItem(IDC_TEXT_STRING_EDIT))->GetWindowText(strTxt);

	USES_CONVERSION;

	if( !strTxt.IsEmpty() )
		strcpy_s(pMsgGET->msg_get_filter_data.evttxt, T2A((LPTSTR)((LPCTSTR)strTxt)));

	//strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_MATCH_WORD_CHECK))->GetCheck());
	//pMsgGET->msg_get_filter_data.evttxt_filterby[0] = (char)strTxt[0];
	//strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_MATCH_EXP_CHECK))->GetCheck());
	//pMsgGET->msg_get_filter_data.evttxt_filterby[1] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_MATCH_CASE_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttxt_filterby[2] = (char)strTxt[0];
}

void CFilterEvtText::SaveOpt(CStdioFile* pfile)
{
	CString strTxt(_T("    Text_String "));
	CString strOpt;

	pfile->WriteString(_T("[Event Text]\n"));
	((CEdit*)GetDlgItem(IDC_TEXT_STRING_EDIT))->GetWindowText(strOpt);
	strOpt += _T("\n");
	strTxt += strOpt;

	pfile->WriteString(strTxt);

//	strTxt.Format(_T("    Word        %d\n"),((CButton*)GetDlgItem(IDC_MATCH_WORD_CHECK))->GetCheck());
//	pfile->WriteString(strTxt);
//	strTxt.Format(_T("    Expr        %d\n"),((CButton*)GetDlgItem(IDC_MATCH_EXP_CHECK))->GetCheck());
//	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Case      %d\n"),((CButton*)GetDlgItem(IDC_MATCH_CASE_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	pfile->WriteString(_T("[/Event Text]\n"));
	pfile->WriteString(_T("\n"));
}

void CFilterEvtText::RestoreOpt(CStringArray* p_sa)
{
	CString strLine;
	int		nIndex = 0;

	((CEdit*)GetDlgItem(IDC_TEXT_STRING_EDIT))->SetWindowText(_T(""));
	((CButton*)GetDlgItem(IDC_MATCH_CASE_CHECK))->SetCheck(0);

	while( nIndex < p_sa->GetSize() )
	{
		strLine = p_sa->GetAt(nIndex);
		strLine.TrimLeft();
		strLine.TrimRight();
		nIndex++;

		if( (!strLine.IsEmpty()) && strLine[0] != _T('#') )
		{
			if( strLine.Find(_T("[Event Text]")) != -1 )
			{
				while( strLine != _T("[/Event Text]") )
				{
					strLine = p_sa->GetAt(nIndex);
					strLine.TrimLeft();
					strLine.TrimRight();
					nIndex++;

					if( !strLine.IsEmpty() )
					{
						//if( strLine.Find(_T("Word")) != -1 )
						//	SetRestoreItem(strLine,4,IDC_MATCH_WORD_CHECK);

						//else if( strLine.Find(_T("Expr")) != -1 )
						//	SetRestoreItem(strLine,4,IDC_MATCH_EXP_CHECK);

						if( strLine.Find(_T("Case")) != -1 ||
							strLine.Find(_T("CASE")) != -1 )
							SetRestoreItem(strLine,4,IDC_MATCH_CASE_CHECK);

						else if( strLine.Find(_T("Text_String")) != -1 )
						{
							strLine.Delete(0, 11);
							strLine.TrimLeft();
							strLine.TrimRight();

							if( !strLine.IsEmpty() )
								((CEdit*)GetDlgItem(IDC_TEXT_STRING_EDIT))->SetWindowText(strLine);
						}
					}
				}

				break;
			}
		}
	}
}

void CFilterEvtText::SetRestoreItem(CString s, int d,UINT res)
{
	s.Delete(0, d);
	s.TrimLeft();
	s.TrimRight();

	if( !s.IsEmpty() && (s == _T("0") || s == _T("1")) )
		((CButton*)GetDlgItem(res))->SetCheck(((BOOL)_ttoi(s)));
}
