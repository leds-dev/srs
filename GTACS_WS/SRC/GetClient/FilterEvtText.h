#if !defined(AFX_FILTEREVTTEXT_H__13591485_1FF2_4B7D_899D_F2D2C3EA0F2F__INCLUDED_)
#define AFX_FILTEREVTTEXT_H__13591485_1FF2_4B7D_899D_F2D2C3EA0F2F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FilterEvtText.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtText dialog

class CFilterEvtText : public CPropertyPage
{
	DECLARE_DYNCREATE(CFilterEvtText)

// Construction
public:
	CFilterEvtText();
	~CFilterEvtText();

	void UpdateMsg(CPtrList* pList);
	void SaveOpt(CStdioFile* pfile);
	void RestoreOpt(CStringArray* p_sa);

// Dialog Data
	//{{AFX_DATA(CFilterEvtText)
	enum { IDD = IDD_FILTER_EVTTEXT };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CFilterEvtText)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CFilterEvtText)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void SetRestoreItem(CString s, int d,UINT res);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILTEREVTTEXT_H__13591485_1FF2_4B7D_899D_F2D2C3EA0F2F__INCLUDED_)
