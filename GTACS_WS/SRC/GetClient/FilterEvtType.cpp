// FilterEvtType.cpp : implementation file
//

#include "stdafx.h"
#include "GET.h"
#include "FilterEvtType.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtType property page

IMPLEMENT_DYNCREATE(CFilterEvtType, CPropertyPage)

CFilterEvtType::CFilterEvtType() : CPropertyPage(CFilterEvtType::IDD)
{
	//{{AFX_DATA_INIT(CFilterEvtType)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CFilterEvtType::~CFilterEvtType()
{
}

void CFilterEvtType::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFilterEvtType)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFilterEvtType, CPropertyPage)
	//{{AFX_MSG_MAP(CFilterEvtType)
	ON_BN_CLICKED(IDC_SELECT_ALL_BUTTON, OnSelectAllButton)
	ON_BN_CLICKED(IDC_CLEAR_BUTTON, OnClearButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtType message handlers

void CFilterEvtType::OnSelectAllButton()
{
	((CButton*)GetDlgItem(IDC_NONE_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_TEXT_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_CMD_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_GBL_LIMITS_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_TLM_LIMITS_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_TLM_VER_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_TLM_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_DIR_CHECK))->SetCheck(1);

	((CButton*)GetDlgItem(IDC_DIR_RSP_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_ARCHIVE_PLAYBACK_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_HARDWARE_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_SYS_STATUS_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_NODE_MGR_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_ALL_TLM_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_ALL_DIR_CHECK))->SetCheck(1);
	((CButton*)GetDlgItem(IDC_ALL_LIMITS_CHECK))->SetCheck(1);
}

void CFilterEvtType::OnClearButton()
{
	((CButton*)GetDlgItem(IDC_NONE_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_TEXT_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_CMD_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_GBL_LIMITS_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_TLM_LIMITS_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_TLM_VER_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_TLM_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_DIR_CHECK))->SetCheck(0);

	((CButton*)GetDlgItem(IDC_DIR_RSP_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_ARCHIVE_PLAYBACK_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_HARDWARE_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_SYS_STATUS_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_NODE_MGR_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_ALL_TLM_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_ALL_DIR_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_ALL_LIMITS_CHECK))->SetCheck(0);
}

void CFilterEvtType::UpdateMsg(CPtrList* pList)
{
	CString		strTxt;
	POSITION	pos		= pList->FindIndex(0);
	MSG_GET*	pMsgGET = (MSG_GET*)pList->GetAt(pos);

	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_NONE_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[0] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_TEXT_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[1] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_CMD_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[2] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_GBL_LIMITS_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[3] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_TLM_LIMITS_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[4] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_TLM_VER_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[5] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_TLM_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[6] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_DIR_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[7] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_DIR_RSP_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[8] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_ARCHIVE_PLAYBACK_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[9] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_HARDWARE_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[10]= (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_SYS_STATUS_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[11]= (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_NODE_MGR_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[12]= (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_ALL_TLM_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[13]= (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_ALL_DIR_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[14]= (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_ALL_LIMITS_CHECK))->GetCheck());
	pMsgGET->msg_get_filter_data.evttype_select_list[15]= (char)strTxt[0];
}

void CFilterEvtType::SaveOpt(CStdioFile* pfile)
{
	CString strTxt;

	pfile->WriteString(_T("[Event Types]\n"));
	strTxt.Format(_T("    None      %d\n"), ((CButton*)GetDlgItem(IDC_NONE_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Text      %d\n"), ((CButton*)GetDlgItem(IDC_TEXT_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Command   %d\n"), ((CButton*)GetDlgItem(IDC_CMD_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Gbl_Lim   %d\n"), ((CButton*)GetDlgItem(IDC_GBL_LIMITS_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Tlm_Lim   %d\n"), ((CButton*)GetDlgItem(IDC_TLM_LIMITS_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Tlm_Ver   %d\n"), ((CButton*)GetDlgItem(IDC_TLM_VER_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Telemetry %d\n"), ((CButton*)GetDlgItem(IDC_TLM_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Directive %d\n"), ((CButton*)GetDlgItem(IDC_DIR_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Dir_Res   %d\n"), ((CButton*)GetDlgItem(IDC_DIR_RSP_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Ach_pbk   %d\n"), ((CButton*)GetDlgItem(IDC_ARCHIVE_PLAYBACK_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Hardware  %d\n"), ((CButton*)GetDlgItem(IDC_HARDWARE_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Sys_Sts   %d\n"), ((CButton*)GetDlgItem(IDC_SYS_STATUS_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    Nde_Mgr   %d\n"), ((CButton*)GetDlgItem(IDC_NODE_MGR_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    All_Tlm   %d\n"), ((CButton*)GetDlgItem(IDC_ALL_TLM_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    All_Dir   %d\n"), ((CButton*)GetDlgItem(IDC_ALL_DIR_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	strTxt.Format(_T("    All_Lim   %d\n"), ((CButton*)GetDlgItem(IDC_ALL_LIMITS_CHECK))->GetCheck());
	pfile->WriteString(strTxt);
	pfile->WriteString(_T("[/Event Types]\n"));
	pfile->WriteString(_T("\n"));
}

void CFilterEvtType::RestoreOpt(CStringArray* p_sa)
{
	CString strLine;
	int		nIndex = 0;

	OnClearButton();

	while( nIndex < p_sa->GetSize() )
	{
		strLine = p_sa->GetAt(nIndex);
		strLine.TrimLeft();
		strLine.TrimRight();
		nIndex++;

		if( (!strLine.IsEmpty()) && strLine[0] != _T('#') )
		{
			if( strLine.Find(_T("[Event Types]")) != -1 )
			{
				while( strLine != _T("[/Event Types]") )
				{
					strLine = p_sa->GetAt(nIndex);
					strLine.TrimLeft();
					strLine.TrimRight();
					nIndex++;

					if( !strLine.IsEmpty() )
					{
						if( strLine.Find(_T("None")) != -1 ||
							strLine.Find(_T("NONE")) != -1 )
							SetRestoreItem(strLine,4,IDC_NONE_CHECK);

						else if( strLine.Find(_T("Text")) != -1 ||
								strLine.Find(_T("TEXT")) != -1 )
							SetRestoreItem(strLine,4,IDC_TEXT_CHECK);

						else if( strLine.Find(_T("Command")) != -1 ||
								strLine.Find(_T("COMMAND")) != -1 )
							SetRestoreItem(strLine,7,IDC_CMD_CHECK);

						else if( strLine.Find(_T("Gbl_Lim")) != -1 ||
								strLine.Find(_T("GBL_LIM")) != -1 )
							SetRestoreItem(strLine,7,IDC_GBL_LIMITS_CHECK);

						else if( strLine.Find(_T("Tlm_Lim")) != -1 ||
								strLine.Find(_T("TLM_LIM")) != -1 )
							SetRestoreItem(strLine,7,IDC_TLM_LIMITS_CHECK);

						else if( strLine.Find(_T("Tlm_Ver")) != -1 ||
								strLine.Find(_T("TLM_VER")) != -1 )
							SetRestoreItem(strLine,7,IDC_TLM_VER_CHECK);

						else if( strLine.Find(_T("Telemetry")) != -1 ||
								strLine.Find(_T("TELEMETRY")) != -1 )
							SetRestoreItem(strLine,9,IDC_TLM_CHECK);

						else if( strLine.Find(_T("Directive")) != -1 ||
								strLine.Find(_T("DIRECTIVE")) != -1 )
							SetRestoreItem(strLine,9,IDC_DIR_CHECK);

						else if( strLine.Find(_T("Dir_Res")) != -1 ||
								strLine.Find(_T("DIR_RES")) != -1 )
							SetRestoreItem(strLine,7,IDC_DIR_RSP_CHECK);

						else if( strLine.Find(_T("Ach_pbk")) != -1 ||
								strLine.Find(_T("ACH_PBK")) != -1 )
							SetRestoreItem(strLine,7,IDC_ARCHIVE_PLAYBACK_CHECK);

						else if( strLine.Find(_T("Hardware")) != -1 ||
								strLine.Find(_T("HARDWARE")) != -1 )
							SetRestoreItem(strLine,8,IDC_HARDWARE_CHECK);

						else if( strLine.Find(_T("Sys_Sts")) != -1 ||
								strLine.Find(_T("SYS_STS")) != -1 )
							SetRestoreItem(strLine,7,IDC_SYS_STATUS_CHECK);

						else if( strLine.Find(_T("Nde_Mgr")) != -1 ||
								strLine.Find(_T("NDE_MGR")) != -1 )
							SetRestoreItem(strLine,7,IDC_NODE_MGR_CHECK);

						else if( strLine.Find(_T("All_Tlm")) != -1 ||
								strLine.Find(_T("ALL_TLM")) != -1 )
							SetRestoreItem(strLine,7,IDC_ALL_TLM_CHECK);

						else if( strLine.Find(_T("All_Dir")) != -1 ||
								strLine.Find(_T("ALL_DIR")) != -1 )
							SetRestoreItem(strLine,7,IDC_ALL_DIR_CHECK);

						else if( strLine.Find(_T("All_Lim")) != -1 ||
								strLine.Find(_T("ALL_LIM")) != -1 )
							SetRestoreItem(strLine,7,IDC_ALL_LIMITS_CHECK);
					}
				}

				break;
			}
		}
	}
}

void CFilterEvtType::SetRestoreItem(CString s, int d,UINT res)
{
	s.Delete(0, d);
	s.TrimLeft();
	s.TrimRight();

	if( !s.IsEmpty() && (s == _T("0") || s == _T("1")) )
		((CButton*)GetDlgItem(res))->SetCheck(((BOOL)_ttoi(s)));
}
