#if !defined(AFX_FILTEREVTTYPE_H__6C86625C_7505_4E98_9438_458225E8DCFD__INCLUDED_)
#define AFX_FILTEREVTTYPE_H__6C86625C_7505_4E98_9438_458225E8DCFD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FilterEvtType.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFilterEvtType dialog

class CFilterEvtType : public CPropertyPage
{
	DECLARE_DYNCREATE(CFilterEvtType)

// Construction
public:
	CFilterEvtType();
	~CFilterEvtType();

	void UpdateMsg(CPtrList* pList);
	void SaveOpt(CStdioFile* pfile);
	void RestoreOpt(CStringArray* p_sa);

// Dialog Data
	//{{AFX_DATA(CFilterEvtType)
	enum { IDD = IDD_FILTER_EVTTYPE };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CFilterEvtType)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CFilterEvtType)
	afx_msg void OnSelectAllButton();
	afx_msg void OnClearButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void SetRestoreItem(CString s, int d,UINT res);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILTEREVTTYPE_H__6C86625C_7505_4E98_9438_458225E8DCFD__INCLUDED_)
