// FilterMnem.cpp : implementation file
//

#include "stdafx.h"
#include "GET.h"
#include "GETFilterPropSheet.h"
#include "FilterMnem.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFilterMnem property page

IMPLEMENT_DYNCREATE(CFilterMnem, CPropertyPage)

CFilterMnem::CFilterMnem() : CPropertyPage(CFilterMnem::IDD)
{
	//{{AFX_DATA_INIT(CFilterMnem)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CFilterMnem::~CFilterMnem()
{
}

void CFilterMnem::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFilterMnem)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFilterMnem, CPropertyPage)
	//{{AFX_MSG_MAP(CFilterMnem)
	ON_BN_CLICKED(IDC_SET_BUTTON, OnSetButton)
	ON_BN_CLICKED(IDC_CLEAR_BUTTON, OnClearButton)
	ON_BN_CLICKED(IDC_CLEAR_ALL_BUTTON, OnClearAllButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFilterMnem message handlers

void CFilterMnem::OnClearAllButton()
{
	((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->ResetContent();
}

void CFilterMnem::OnSetButton()
{
	CString strText;

	((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->GetWindowText(strText);

	if( strText.IsEmpty() )
		return;

	((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->AddString(strText);
	((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->SetEditSel(0, -1);
	((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->Clear();
}

void CFilterMnem::OnClearButton()
{
	int nIndex = ((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->GetCurSel();

	if( nIndex != LB_ERR )
	{
		((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->DeleteString(nIndex);
		((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->SetEditSel(0, -1);
		((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->Clear();
	}
}

void CFilterMnem::UpdateMsg(CPtrList* pList)
{
	CString strTxt;
	MSG_GET*	pMsgGET = NULL;
	int			nCnt	= ((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->GetCount();

	if( nCnt == 0 )
		return;

	((CGETFilterPropSheet*)GetParent())->NeedNewMsgs(pList, nCnt, MNEM_LIST_SIZE);

	USES_CONVERSION;
	POSITION	pos		= pList->GetHeadPosition();
	int			nLen	= 0;
	int			nMsg	= 0;
	int			nIdx	= 0;


	while( pos != NULL )
	{
		pMsgGET	= (MSG_GET*)pList->GetNext(pos);

		for( int i = 0; i < nCnt; i++ )
		{
			if( nMsg < MNEM_LIST_SIZE )
			{
				nLen = ((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->GetLBTextLen(nIdx);
				((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->GetLBText(nIdx, strTxt.GetBuffer(nLen));
				strcpy_s(pMsgGET->msg_get_filter_data.mnem_list[nMsg], T2A((LPTSTR)((LPCTSTR)strTxt)));
				strTxt.ReleaseBuffer();
				nMsg++; nIdx++;
			}
			else
			{
				nMsg = 0;
				break;
			}
		}
	}
}

void CFilterMnem::SaveOpt(CStdioFile* pfile)
{
	CString strTxt;
	CString strTmp;
	int		nCnt = ((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->GetCount();
	int		nLen = 0;

	pfile->WriteString(_T("[Event Mnemonic]\n"));

	for( int i = 0; i < nCnt; i++ )
	{
		nLen = ((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->GetLBTextLen(i);
		((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->GetLBText(i, strTxt.GetBuffer(nLen));
		strTmp.Format(_T("    %s\n"), strTxt);
		pfile->WriteString(strTmp);
		strTxt.ReleaseBuffer();
	}

	pfile->WriteString(_T("[/Event Mnemonic]\n"));
	pfile->WriteString(_T("\n"));
}

void CFilterMnem::RestoreOpt(CStringArray* p_sa)
{
	CString strLine;
	int		nIndex = 0;

	OnClearAllButton();

	while( nIndex < p_sa->GetSize() )
	{
		strLine = p_sa->GetAt(nIndex);
		strLine.TrimLeft();
		strLine.TrimRight();
		nIndex++;

		if( (!strLine.IsEmpty()) && strLine[0] != _T('#') )
		{
			if( strLine.Find(_T("[Event Mnemonic]")) != -1 )
			{
				while( strLine != _T("[/Event Mnemonic]") )
				{
					strLine = p_sa->GetAt(nIndex);
					strLine.TrimLeft();
					strLine.TrimRight();
					nIndex++;

					if( strLine != _T("[/Event Mnemonic]") )
						((CComboBox*)GetDlgItem(IDC_MNEMONICS_COMBO))->AddString(strLine);
				}

				break;
			}
		}
	}
}
