#if !defined(AFX_FILTERMNEM_H__64B991EE_79D5_4540_B606_A3A4B2BA63AA__INCLUDED_)
#define AFX_FILTERMNEM_H__64B991EE_79D5_4540_B606_A3A4B2BA63AA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FilterMnem.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFilterMnem dialog

class CFilterMnem : public CPropertyPage
{
	DECLARE_DYNCREATE(CFilterMnem)

// Construction
public:
	CFilterMnem();
	~CFilterMnem();

	void UpdateMsg(CPtrList* pList);
	void SaveOpt(CStdioFile* pfile);
	void RestoreOpt(CStringArray* p_sa);

// Dialog Data
	//{{AFX_DATA(CFilterMnem)
	enum { IDD = IDD_FILTER_MNEM };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CFilterMnem)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CFilterMnem)
	afx_msg void OnSetButton();
	afx_msg void OnClearButton();
	afx_msg void OnClearAllButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILTERMNEM_H__64B991EE_79D5_4540_B606_A3A4B2BA63AA__INCLUDED_)
