; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CNewRequest
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "get.h"
LastPage=0

ClassCount=16
Class1=CChildFrame
Class2=CFilterEvtCritical
Class3=CFilterEvtName
Class4=CFilterEvtNum
Class5=CFilterEvtText
Class6=CFilterEvtType
Class7=CFilterMnem
Class8=CGETApp
Class9=CAboutDlg
Class10=CGETComm
Class11=CGETDoc
Class12=CGETFilterPropSheet
Class13=CGETView
Class14=CMainFrame
Class15=CNewRequest
Class16=CTimeoutDialog

ResourceCount=12
Resource1=IDD_FILTER_EVTTEXT
Resource2=IDD_FILTER_MNEM
Resource3=IDR_RPTTYPE
Resource4=IDD_CREDITS_DIALOG
Resource5=IDD_FILTER_EVTNUM
Resource6=IDD_ABOUTBOX
Resource7=IDR_MAINFRAME
Resource8=IDD_FILTER_EVTTYPE
Resource9=IDD_FILTER_EVTCRITICAL
Resource10=IDD_NEW_REQUEST
Resource11=IDD_FILTER_EVTNAME
Resource12=IDD_TIMEOUT_DIALOG

[CLS:CChildFrame]
Type=0
BaseClass=CBCGMDIChildWnd
HeaderFile=ChildFrm.h
ImplementationFile=ChildFrm.cpp

[CLS:CFilterEvtCritical]
Type=0
BaseClass=CPropertyPage
HeaderFile=FilterEvtCritical.h
ImplementationFile=FilterEvtCritical.cpp

[CLS:CFilterEvtName]
Type=0
BaseClass=CPropertyPage
HeaderFile=FilterEvtName.h
ImplementationFile=FilterEvtName.cpp

[CLS:CFilterEvtNum]
Type=0
BaseClass=CPropertyPage
HeaderFile=FilterEvtNum.h
ImplementationFile=FilterEvtNum.cpp

[CLS:CFilterEvtText]
Type=0
BaseClass=CPropertyPage
HeaderFile=FilterEvtText.h
ImplementationFile=FilterEvtText.cpp

[CLS:CFilterEvtType]
Type=0
BaseClass=CPropertyPage
HeaderFile=FilterEvtType.h
ImplementationFile=FilterEvtType.cpp

[CLS:CFilterMnem]
Type=0
BaseClass=CPropertyPage
HeaderFile=FilterMnem.h
ImplementationFile=FilterMnem.cpp

[CLS:CGETApp]
Type=0
BaseClass=CWinApp
HeaderFile=GET.h
ImplementationFile=GET.cpp

[CLS:CAboutDlg]
Type=0
BaseClass=CDialog
HeaderFile=GET.cpp
ImplementationFile=GET.cpp

[CLS:CGETComm]
Type=0
BaseClass=CCmdTarget
HeaderFile=GETComm.h
ImplementationFile=GETComm.cpp

[CLS:CGETDoc]
Type=0
BaseClass=CDocument
HeaderFile=GETDoc.h
ImplementationFile=GETDoc.cpp

[CLS:CGETFilterPropSheet]
Type=0
BaseClass=CPropertySheet
HeaderFile=GETFilterPropSheet.h
ImplementationFile=GETFilterPropSheet.cpp

[CLS:CGETView]
Type=0
BaseClass=CView
HeaderFile=GETView.h
ImplementationFile=GETView.cpp

[CLS:CMainFrame]
Type=0
BaseClass=CBCGMDIFrameWnd
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp

[CLS:CNewRequest]
Type=0
BaseClass=CDialog
HeaderFile=NewRequest.h
ImplementationFile=NewRequest.cpp
LastObject=IDC_FILTER_BUTTON
Filter=D
VirtualFilter=dWC

[CLS:CTimeoutDialog]
Type=0
BaseClass=CDialog
HeaderFile=TimeoutDialog.h
ImplementationFile=TimeoutDialog.cpp

[DLG:IDD_FILTER_EVTCRITICAL]
Type=1
Class=CFilterEvtCritical
ControlCount=10
Control1=IDC_EVT_CRIT_STATIC,static,1342308352
Control2=IDC_EVT_CRIT_GROUP_STATIC,button,1342177287
Control3=IDC_ALARM_CHECK,button,1342242819
Control4=IDC_FATAL_CHECK,button,1342242819
Control5=IDC_NORMAL_CHECK,button,1342242819
Control6=IDC_INFO_CHECK,button,1342242819
Control7=IDC_WARNING_CHECK,button,1342242819
Control8=IDC_SELECT_ALL_BUTTON,button,1342242816
Control9=IDC_CLEAR_BUTTON,button,1342242816
Control10=IDC_USC_CHECK,button,1342242819

[DLG:IDD_FILTER_EVTNAME]
Type=1
Class=CFilterEvtName
ControlCount=6
Control1=IDC_EVT_NAME_STATIC,static,1342308352
Control2=IDC_EVT_NAME_GROUP_STATIC,button,1342177287
Control3=IDC_EVT_NAME_COMBO,combobox,1344340225
Control4=IDC_SET_BUTTON,button,1342242816
Control5=IDC_CLEAR_BUTTON,button,1342242816
Control6=IDC_CLEAR_ALL_BUTTON,button,1342242816

[DLG:IDD_FILTER_EVTNUM]
Type=1
Class=CFilterEvtNum
ControlCount=6
Control1=IDC_EVT_NUM_STATIC,static,1342308352
Control2=IDC_EVT_NUM_GROUP_STATIC,button,1342177287
Control3=IDC_EVT_NUM_COMBO,combobox,1344340225
Control4=IDC_SET_BUTTON,button,1342242816
Control5=IDC_CLEAR_BUTTON,button,1342242816
Control6=IDC_CLEAR_ALL_BUTTON,button,1342242816

[DLG:IDD_FILTER_EVTTEXT]
Type=1
Class=CFilterEvtText
ControlCount=4
Control1=IDC_SUB_FILTER_STATIC,static,1342308352
Control2=IDC_TEXT_STRING_EDIT,edit,1350631552
Control3=IDC_TEXT_STRING_STATIC,static,1342308352
Control4=IDC_MATCH_CASE_CHECK,button,1342242819

[DLG:IDD_FILTER_EVTTYPE]
Type=1
Class=CFilterEvtType
ControlCount=20
Control1=IDC_EVENT_TYPE_STATIC,static,1342308352
Control2=IDC_EVENT_TYPE_GROUP_STATIC,button,1342177287
Control3=IDC_NONE_CHECK,button,1342242819
Control4=IDC_TEXT_CHECK,button,1342242819
Control5=IDC_CMD_CHECK,button,1342242819
Control6=IDC_GBL_LIMITS_CHECK,button,1342242819
Control7=IDC_TLM_LIMITS_CHECK,button,1342242819
Control8=IDC_TLM_VER_CHECK,button,1342242819
Control9=IDC_TLM_CHECK,button,1342242819
Control10=IDC_DIR_CHECK,button,1342242819
Control11=IDC_DIR_RSP_CHECK,button,1342242819
Control12=IDC_ARCHIVE_PLAYBACK_CHECK,button,1342242819
Control13=IDC_HARDWARE_CHECK,button,1342242819
Control14=IDC_SYS_STATUS_CHECK,button,1342242819
Control15=IDC_NODE_MGR_CHECK,button,1342242819
Control16=IDC_ALL_TLM_CHECK,button,1342242819
Control17=IDC_ALL_DIR_CHECK,button,1342242819
Control18=IDC_ALL_LIMITS_CHECK,button,1342242819
Control19=IDC_SELECT_ALL_BUTTON,button,1342242816
Control20=IDC_CLEAR_BUTTON,button,1342242816

[DLG:IDD_FILTER_MNEM]
Type=1
Class=CFilterMnem
ControlCount=6
Control1=IDC_MNEM_GROUP_STATIC,button,1342177287
Control2=IDC_ADD_MNEM_STATIC,static,1342308352
Control3=IDC_MNEMONICS_COMBO,combobox,1344340225
Control4=IDC_SET_BUTTON,button,1342242816
Control5=IDC_CLEAR_BUTTON,button,1342242816
Control6=IDC_CLEAR_ALL_BUTTON,button,1342242816

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=5
Control1=IDC_STATIC,static,1342177283
Control2=IDC_VERSION_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Control5=IDC_CREDITS,button,1342242816

[DLG:IDD_NEW_REQUEST]
Type=1
Class=CNewRequest
ControlCount=38
Control1=IDC_BASIC_GROUP_STATIC,button,1342177287
Control2=IDC_STATIC,static,1342308352
Control3=IDC_START_TIME,edit,1350631552
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STOP_TIME,edit,1350631552
Control6=IDC_FILTER_BUTTON,button,1342242816
Control7=IDC_STATIC,button,1342177287
Control8=IDC_SC13_CHECK,button,1342242819
Control9=IDC_SC14_CHECK,button,1342242819
Control10=IDC_SC15_CHECK,button,1342242819
Control11=IDC_SC16_CHECK,button,1342242819
Control12=IDC_SC17_CHECK,button,1342242819
Control13=IDC_SC18_CHECK,button,1342242819
Control14=IDC_SC19_CHECK,button,1342242819
Control15=IDC_SC20_CHECK,button,1342242819
Control16=IDC_STATIC,button,1342177287
Control17=IDC_ALL_CHECK,button,1342242819
Control18=IDC_GE_CHECK,button,1342242819
Control19=IDC_RT1_CHECK,button,1342242819
Control20=IDC_RT2_CHECK,button,1342242819
Control21=IDC_RT3_CHECK,button,1342242819
Control22=IDC_RT4_CHECK,button,1342242819
Control23=IDC_RT5_CHECK,button,1342242819
Control24=IDC_RT6_CHECK,button,1342242819
Control25=IDC_STATIC,static,1342308352
Control26=IDC_SERVER_NAME_COMBO,combobox,1344339969
Control27=IDC_MAX_EVENTS,static,1342308352
Control28=IDC_MAX_EVT_EDIT,edit,1350631552
Control29=IDC_OUTPUT_EDIT,edit,1350631552
Control30=IDC_STATIC,button,1342177287
Control31=IDC_TIME_RADIO,button,1342177289
Control32=IDC_STREAM_RADIO,button,1342177289
Control33=IDOK,button,1342242817
Control34=IDCANCEL,button,1342242816
Control35=IDC_OP_FILENAME,static,1342308352
Control36=IDC_TIME_GROUP_STATIC,button,1342177287
Control37=IDC_STREAM_GROUP_STATIC,button,1342177287
Control38=IDC_OUTPUT_GROUP_STATIC,button,1342177287

[DLG:IDD_TIMEOUT_DIALOG]
Type=1
Class=CTimeoutDialog
ControlCount=6
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_TIMEOUT_GROUP,button,1342177287
Control4=IDC_TIMEOUT_EDIT,edit,1350631552
Control5=IDC_TIMEOUT_SPIN,msctls_updown32,1342177334
Control6=IDC_TIMEOUT_STATIC,static,1342308864

[TB:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_FILE_PRINT
Command4=ID_APP_ABOUT
CommandCount=4

[MNU:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_FILE_NEW_REQUEST
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_MRU_FILE1
Command9=ID_APP_EXIT
Command10=ID_VIEW_TOOLBAR
Command11=ID_VIEW_STATUS_BAR
Command12=ID_CONNECTION_TIME_OUT
Command13=ID_APP_ABOUT
Command14=ID_HELP_CONTENT
CommandCount=14

[MNU:IDR_RPTTYPE]
Type=1
Class=?
Command1=ID_FILE_NEW_REQUEST
Command2=ID_FILE_OPEN
Command3=ID_FILE_CLOSE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_MRU_FILE1
Command9=ID_APP_EXIT
Command10=ID_VIEW_TOOLBAR
Command11=ID_VIEW_STATUS_BAR
Command12=ID_CONNECTION_TIME_OUT
Command13=ID_APP_ABOUT
Command14=ID_HELP_CONTENT
CommandCount=14

[ACL:IDR_MAINFRAME]
Type=1
Class=?
Command1=ID_EDIT_COPY
Command2=ID_CONNECTION_TIME_OUT
Command3=ID_FILE_NEW_REQUEST
Command4=ID_FILE_OPEN
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_SETUP
Command7=ID_FILE_SAVE_AS
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_NEXT_PANE
Command12=ID_PREV_PANE
Command13=ID_EDIT_COPY
Command14=ID_EDIT_PASTE
Command15=ID_EDIT_CUT
Command16=ID_EDIT_UNDO
CommandCount=16

[DLG:IDD_CREDITS_DIALOG]
Type=1
Class=?
ControlCount=1
Control1=IDC_CREDITS_STATIC,static,1350696960

