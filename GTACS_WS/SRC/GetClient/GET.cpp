// GEvtHist.cpp : Defines the class behaviors for the application.
//
// MSD: 08/2010: Increased the timeout interval from 1 min to 5 mintues.

#include "stdafx.h"
#include "GETComm.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "NewRequest.h"
#include "Credits.h"
#include "GETDoc.h"
#include "GETView.h"
#include "GET.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define BUFFER 256

/////////////////////////////////////////////////////////////////////////////
// CGETApp

BEGIN_MESSAGE_MAP(CGETApp, CWinApp)
	//{{AFX_MSG_MAP(CGETApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_NEW_REQUEST, OnFileNewRequest)
	ON_COMMAND(ID_HELP_CONTENT, OnHelpContent)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGETApp construction

CGETApp::CGETApp()
{
	m_nInterval = 300;
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CGETApp object

CGETApp	theApp;
WSADATA			wsaData;

/////////////////////////////////////////////////////////////////////////////
// CGETApp initialization

BOOL CGETApp::InitInstance()
{
	//For single instance checking
	HANDLE	hMutexOneInstance	= ::CreateMutex(NULL,TRUE,CreateExclusionName(GET_GUID,UNIQUE_TO_SESSION));
	bool	AlreadyRunning		= (GetLastError()==ERROR_ALREADY_EXISTS||GetLastError()==ERROR_ACCESS_DENIED);

	if( hMutexOneInstance != NULL )
		::ReleaseMutex(hMutexOneInstance);

	if( AlreadyRunning )
	{
		HWND hOther = NULL;

		EnumWindows(FindInstance, (LPARAM)&hOther);

		if( hOther != NULL ) /* pop up */
		{
			::SetForegroundWindow(hOther);

			if( IsIconic(hOther) )
				::ShowWindow(hOther, SW_RESTORE);
		}

		return FALSE; // terminates the creation
	}

	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

// #ifdef _AFXDLL
//	Enable3dControls();			// Call this when using MFC in a shared DLL
// #else
//	Enable3dControlsStatic();	// Call this when linking to MFC statically
// #endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("ISI"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	if( !InitAndStartSockets(&wsaData) )
	{
		CString strMsg(_T("Could not start socket library. "));
		strMsg += GetLastErrorText();
		AfxMessageBox(strMsg, MB_OK);
	}

	CMRegisterControl();
	LoadCodeMaxProfile(NULL);

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.
	m_pDocTemplate = new CGETMultiDocTemplate(
		IDR_RPTTYPE,
		RUNTIME_CLASS(CGETDoc),
		RUNTIME_CLASS(CChildFrame),      // main SDI frame window
		RUNTIME_CLASS(CGETView));
	AddDocTemplate(m_pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
//	COXCommandLine		cCmdLine(m_lpCmdLine);
//	COXCommandOptions	cCmdOptions;

//	cCmdOptions.Add(_T("nosplash"), COD_REP_LAST, _T("Do not show the splash screen"));
//	cCmdOptions.ParseCmdLine(cCmdLine);

//	if( !cCmdOptions.IsEnabled(_T("nosplash")) )
//	{
//		COXSplashWnd::EnableSplashScreen(TRUE);
//		COXSplashWnd::LoadGlobalBitmap(IDB_SPLASH, CLR_NONE);
//		COXSplashWnd::ShowGlobal(2000);
//	}

	CMainFrame* pMainFrame = new CMainFrame;

	if( !pMainFrame->LoadFrame(IDR_MAINFRAME) )
		return FALSE;

	m_pMainWnd = pMainFrame;

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(m_nCmdShow);
	m_pMainWnd->UpdateWindow();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Ray Mosley       Date : 11/2/2004       version 1.0
//////////////////////////////////////////////////////////////////////////
//
//  Function :      GetFileVersion
//
//  Description :   This routine retrieves the software version number.
//
//
//  Return :
//				CString -	Software version number
//
//  Parameters :
//				  	CString strFilePath	-	Path to	program
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
**/
CString GetFileVersion(CString strFilePath)
{
	CString strVersion(_T("Unavailable"));
	LPTSTR 	pcFilepath		= strFilePath.GetBuffer(strFilePath.GetLength());
	DWORD 	nVersionInfoSize= GetFileVersionInfoSize(pcFilepath, 0);
	LPVOID	pData			= malloc(nVersionInfoSize);

	if( GetFileVersionInfo(pcFilepath, 0, nVersionInfoSize, pData) )
	{
		VS_FIXEDFILEINFO*	pcVersion;
		UINT				pnLen;

		VerQueryValue(pData, TEXT("\\"), (LPVOID*)&pcVersion, &pnLen);
		WORD nMajor		= HIWORD(pcVersion->dwFileVersionMS);
		WORD nMinor		= LOWORD(pcVersion->dwFileVersionMS);
		WORD nSpecific	= HIWORD(pcVersion->dwFileVersionLS);
		WORD nCustom	= LOWORD(pcVersion->dwFileVersionLS);
		strVersion.Format (_T("Version %d.%d.%d.%d"), nMajor, nMinor, nSpecific, nCustom);
	}

	free(pData);

	return strVersion;
}

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnCredits();
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(IDC_CREDITS, OnCredits)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString strVersion;
	strVersion.Format(_T("GET %s"), GetFileVersion(_T("GET.exe")));

	if( strVersion.Find(_T("Unavailable")) == -1 )
		GetDlgItem(IDC_VERSION_STATIC)->SetWindowText(strVersion);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// App command to run the dialog
void CGETApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//
//  Function :      void CAboutDlg::OnCredits()
//
//  Description :   Displays the credits for the software
//
//
//  Return :        void	-	none.
//
//  Parameters :	void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
**/
void CAboutDlg::OnCredits()
{
	CCredits dlgCredits;
	TCHAR* pcArrCredit[] =
	{
		_T("BITMAP_CREDITS2\b"),    // MYBITMAP is a quoted bitmap resource
		_T(""),
		_T("GTACS Event Tool\n"),
		_T(""),
		_T("Copyright (c) 2012 \f"),
		_T("OSPO/NESDIS/NOAA/DOC \f"),
		_T("NSOF Bldg. \f"),
		_T("4213 Suitland Road \f"),
		_T("Suitland, Maryland \f"),
		_T(""),
		_T("301-817-4133 \f"),
		_T("www.ospo.noaa.gov \f"),
		_T(""),
		_T("All Rights Reserved \f"),
		_T(""),
		_T(""),
		_T(""),
		_T("Project Lead \t"),
		_T(""),
		_T("Diane Robinson \f"),
		_T(""),
		_T(""),
		_T("Team Programmer \t"),
		_T(""),
		_T("Manan Dalal \f"),
		_T(""),
		_T(""),
		_T("HISTORY: PR000508: Print Preview fixed."),
		_T(""),
		_T(""),
		_T("BITMAP_CREDITS2\b"),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
	};

	dlgCredits.m_nArrCnt	= 40;
	dlgCredits.m_pcArrCredit= pcArrCredit;
	dlgCredits.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CGETApp message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 09/23/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CGETApp::OnFileNewRequest
//	Description :	Processes a new report request.
//
//	Return :		void
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CGETApp::OnFileNewRequest()
{
	CNewRequest newRequestDlg;
	CPtrList	list;
	MSG_RES		msgRES;
	MSG_GET*	pGetMsg	= new MSG_GET;

	memset(pGetMsg, 0, sizeof(MSG_GET));
	list.AddHead(pGetMsg);
	newRequestDlg.SetRequestMsg(&list);

	if( IDOK == newRequestDlg.DoModal() )
	{
		CString	strMsg;
		WORD	nSeqNum	= 0;
		DWORD	cchBuff	= BUFFER;
		TCHAR	tchHostname[BUFFER];
		TCHAR	tchUsername[UNLEN + 1];

		if( !Connect() )
			return;

		::GetComputerName(tchHostname, &cchBuff);
		m_strRptNameS = m_strRptNameC;

		int nIndex = m_strRptNameS.ReverseFind(_T('.'));

		if( nIndex == -1 )
		{
			m_strRptNameS += _T("_");
			m_strRptNameS += tchHostname;
		}
		else
		{
			m_strRptNameS.Insert(nIndex, _T("_"));
			m_strRptNameS.Insert(nIndex+1, tchHostname);
		}

		cchBuff	= UNLEN + 1;
		::GetUserName(tchUsername, &cchBuff);

		m_strRptNameS.Insert(0, _T("W"));
		strMsg.Format(IDS_SERVER_RESPONSE, _T("GTACS"), m_strGTACSHost);
		((CMainFrame*)AfxGetMainWnd())->GetStatusBar()->SetWindowText(strMsg);

		USES_CONVERSION;
		POSITION	pos		= list.GetHeadPosition();
		MSG_GET*	pMsgGET	= NULL;
		CWaitCursor	wait;

		while( pos != NULL )
		{
			pMsgGET = (MSG_GET*)list.GetNext(pos);
			strcpy_s(pMsgGET->msg_get_header.wksname, T2A(tchHostname));
			strcpy_s(pMsgGET->msg_get_header.usrname, T2A(tchUsername));
			strcpy_s(pMsgGET->msg_get_header.id, T2A(ctstrPROCESS_ID));
			pMsgGET->msg_get_header.num_msg = htons((WORD)list.GetCount());
			pMsgGET->msg_get_header.seq_num = htons(nSeqNum++);

			if( !m_GETComm.SendMsg(pMsgGET) )
			{
				CString strErr(GetLastErrorText());

				GetLastError() == ERROR_SUCCESS ? strErr.Empty() : strErr += _T(" ");
				strMsg.LoadString(IDS_REPORT_GEN_ERROR);
				strErr += strMsg;
				AfxMessageBox(strErr, MB_OK);
				Disconnect();
				DeleteListElements(&list);
				return;
			}
		}

		if( !m_GETComm.ReadRespMsg(&msgRES) )
		{
			CString strErr(GetLastErrorText());

			GetLastError() == ERROR_SUCCESS ? strErr.Empty() : strErr += _T(" ");
			strMsg.LoadString(IDS_REPORT_GEN_ERROR);
			strErr += strMsg;
			AfxMessageBox(strErr, MB_OK);
			Disconnect();
			DeleteListElements(&list);
			return;
		}

		Disconnect();
		ProcessResponse(msgRES);
	}

	DeleteListElements(&list);
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Ray Mosley     Date : 09/23/04       version 1.0
//////////////////////////////////////////////////////////////////////////
//
//  Function :      CGETApp::InitAndStartSockets
//
//  Description :   Call this function to initialize Windows Sockets.
//
//
//  Return :        BOOL
//						TRUE	SUCCESS
//						FALSE	FAIL
//  Parameters :
//				  	WSADATA* pwsaData	-	A pointer to a WSADATA structure.
//					If lpwsaData is not equal to NULL, then the address of
//					the WSADATA structure is filled by the call to
//					::WSAStartup.
//  Notes :
//	This function also ensures that ::WSACleanup is called for you before
//	the application terminates.
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CGETApp::InitAndStartSockets(WSADATA* pwsaData)
{
	if( !AfxSocketInit() )
		return FALSE;

	if( WSAStartup(0x101, pwsaData) != 0 )
		return FALSE;

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 09/23/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CGETApp::Connect
//	Description :	Creates a socket connection with a remote host
//
//	Return :		BOOL	-	TRUE if connection is successful
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CGETApp::Connect()
{
	CString	strMsg;

	strMsg.Format(_T("Getting connection with GTACS %s."), m_strGTACSHost);
	((CMainFrame*)AfxGetMainWnd())->GetStatusBar()->SetWindowText(strMsg);

	if( !m_GETComm.CreateAndConnect(m_strGTACSHost) )
	{
		CString strErr;

		strErr = GetLastError()==ERROR_SUCCESS?_T(" Unknown Socket Error."):GetLastErrorText();
		strMsg.Format(IDS_COMM_ERROR,m_strGTACSHost,strErr);
		AfxMessageBox(strMsg, MB_OK);
		return FALSE;
	}

	strMsg.Format(IDS_COMM_OPEN_OK, m_strGTACSHost);
	((CMainFrame*)AfxGetMainWnd())->GetStatusBar()->SetWindowText(strMsg);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 09/23/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CGETApp::Disconnect
//	Description :	Disconnects from host and closes socket connection.
//
//	Return :		void
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CGETApp::Disconnect()
{
	CString strMsg;

	if( !m_GETComm.ShutdownAndClose() )
	{
		strMsg.Format(IDS_COMM_SHUTDOWN_ERROR, m_strGTACSHost, GetLastError());
		strMsg.TrimRight();
		AfxMessageBox(strMsg, MB_OK);
		return;
	}

	strMsg.Format(IDS_COMM_CLOSE_OK, m_strGTACSHost);
	((CMainFrame*)AfxGetMainWnd())->GetStatusBar()->SetWindowText(strMsg);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 09/23/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CGETApp::DeleteListElements
//	Description :	DeleteListElements is a clean up routine to delete
//					unused memory space.
//
//	Return :		void	-
//
//	Parameters :
//		CPtrList* list		-	pointer to a list of objects.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CGETApp::DeleteListElements(CPtrList* list)
{
	if( list->IsEmpty() )
		return;

	POSITION pos = list->GetHeadPosition();

	while( pos != NULL )
		delete list->GetNext(pos);

	list->RemoveAll();
}

CString CGETApp::GetLastErrorText()
{
	LPVOID lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				  FORMAT_MESSAGE_FROM_SYSTEM |
				  FORMAT_MESSAGE_IGNORE_INSERTS,
				  NULL,
				  GetLastError(),
				  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				  (LPTSTR) &lpMsgBuf,
				  0,
				  NULL);
	CString strMsg((LPTSTR)lpMsgBuf);
	strMsg.Remove(_T('\x0D'));
	strMsg.Remove(_T('\x0A'));
	LocalFree(lpMsgBuf);
	return strMsg;
}

void CGETApp::ProcessResponse(MSG_RES msgRES)
{
	CString strMsg;

	if( msgRES.status != eSUCCESS )
	{
		strMsg = msgRES.response;
		AfxMessageBox(strMsg, MB_OK);
		return;
	}

	CFileFind	finder;
	CString		strSrc;
	CString		strDes(GetEpochOut());

	if( strDes.IsEmpty() )
		return;

	strSrc  = m_strRptPath;
	strSrc += m_strRptNameS;
	strDes += ctstrRPT_LOC_DIR;

	if( !finder.FindFile(strDes) )
		CreateDirectory(strDes, NULL);

	strDes += _T("\\");
	strDes += m_strRptNameC;

	if( !CopyFile(strSrc, strDes, false) )
	{
		CString strErr(GetLastErrorText());

		GetLastError() == ERROR_SUCCESS ? strErr.Empty() : strErr;
		strMsg.Format(IDS_RPT_COPY_ERROR, strDes, strErr);
		AfxMessageBox(strMsg, MB_OK);
		return;
	}

	m_pDocTemplate->OpenDocumentFile(strDes);
}

CString CGETApp::GetEpochOut()
{
	CString strEnv(_wgetenv(ctstrEPOCH_OUT));  //_wdupenv_s

	if( strEnv.IsEmpty() )
	{
		CString strMsg;
		strMsg.Format(IDS_ENV_VAR_ERROR, ctstrEPOCH_OUT);
		AfxMessageBox(strMsg, MB_OK);
		return strEnv;
	}

	if( strEnv[0] == _T('/') )
		strEnv.Delete(0);

	if( strEnv[1] == _T('=') )
		strEnv.Replace(_T('='), _T(':'));

	strEnv.Replace(_T('/'), _T('\\'));

	return strEnv;
}

void CGETApp::OnHelpContent()
{
	if( ShellExecute(NULL,_T("open"),ctstrHlpPage,NULL,NULL,SW_SHOWNORMAL) <= (HINSTANCE)32 )
		AfxMessageBox(GetLastErrorText(), MB_OK);
}

// Codemax Registry keys
#define CODEMAX_CODE_SECTION	_T("CodeMax")
#define KEY_HSCROLLBAR			_T("HScrollBar")
#define KEY_VSCROLLBAR			_T("VScrollBar")
#define KEY_HOTKEYS				_T("HotKeys")
#define KEY_FINDMRULIST			_T("FindMRUList")
#define KEY_REPLMRULIST			_T("ReplaceMRUList")

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Ray Mosley       Date : 11/12/2004       version 1.0
/////////////////////////////////////////////////////////////////////////////
//
//  Function :      void CGETApp::LoadCodeMaxProfile
//
//  Description :	This routine loads the different code max profiles.
//
//
//  Return :        void	- none
//
//  Parameters :
//				  	HWND hWnd	-	This property returns a handle
//									to a form or control.
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
**/
void CGETApp::LoadCodeMaxProfile(HWND hWnd)
{
	if( hWnd )
	{
		// load window settings
		CM_SetReadOnly(hWnd, TRUE);
		CM_ShowScrollBar(hWnd, TRUE,  GetProfileInt(CODEMAX_CODE_SECTION, KEY_HSCROLLBAR, TRUE));
		CM_ShowScrollBar(hWnd, FALSE, GetProfileInt(CODEMAX_CODE_SECTION, KEY_VSCROLLBAR, TRUE));

		HDC		hDC		= GetDC(NULL);
		HFONT	hFont	= CreateFont(-MulDiv(10,GetDeviceCaps(hDC,LOGPIXELSY),72),
									0,0,0,FW_NORMAL,0,0,0,0,0,0,0,0,_T("Courier New"));
		ReleaseDC(NULL, hDC);
		SendMessage(hWnd, WM_SETFONT, (WPARAM)hFont, 0);

//		CM_LINENUMBERING sLineNumberingDef = { TRUE, 1, CM_DECIMAL };
//		CM_SetLineNumbering(hWnd, &sLineNumberingDef);
	}
	else
	{
		// load global settings
		// macros
		UINT unSize;

		for( int i = 0; i < CM_MAX_MACROS; i++ )
		{
			TCHAR	szKey[ 10 ];
			wsprintf(szKey, _T("Macro%d"), i);
			LPBYTE	pMacro;

			if( GetProfileBinary(CODEMAX_CODE_SECTION, szKey, &pMacro, &unSize) )
			{
				if( unSize )
					CMSetMacro(i, pMacro);

				delete pMacro;
			}
		}

		// hotkeys
		LPBYTE pHotKeys;

		if( GetProfileBinary(CODEMAX_CODE_SECTION, KEY_HOTKEYS, &pHotKeys, &unSize) )
		{
			if( unSize )
				CMSetHotKeys(pHotKeys);

			delete pHotKeys;
		}

		// Find/Replace MRU lists
		CMSetFindReplaceMRUList(GetProfileString(CODEMAX_CODE_SECTION, KEY_FINDMRULIST, _T("")), TRUE);
		CMSetFindReplaceMRUList(GetProfileString(CODEMAX_CODE_SECTION, KEY_REPLMRULIST, _T("")), FALSE);
	}
}

BOOL CALLBACK CGETApp::FindInstance(HWND hWnd, LPARAM lParam)
{
    DWORD	result;
    LRESULT ok = ::SendMessageTimeout(hWnd,
                                      UWM_NEW_INSTANCE,
                                      0, 0,
                                      SMTO_BLOCK |
                                      SMTO_ABORTIFHUNG,
                                      200,
                                      &result);

    if( ok == 0 )
       return TRUE; // ignore this and continue

    if( result == UWM_NEW_INSTANCE )
    {
		/* found it */
        HWND* target= (HWND*)lParam;
        *target		= hWnd;

		return FALSE; // stop search
    }

    return TRUE; // continue search
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Ray Mosley       Date : 11/18/2004       version 1.0
/////////////////////////////////////////////////////////////////////////////
//
//  Function :      void CGETApp::CreateExclusionName
//
//  Description :	This routine creates a exclusion mutex name.
//
//
//  Return :        CString	- Name to use for the exclusion mutex
//
//  Parameters :
//				  	LPCTSTR GUID -	The GUID for used for the exclusion
//					UNIT kind		Kind of exclusion
//  Notes :
//		The GUID is created by a declaration such as
//			#define UNIQUE _T("MyAppName-{44E678F7-DA79-11d3-9FE9-006067718D04}")
//
//////////////////////////////////////////////////////////////////////////
**/
CString CGETApp::CreateExclusionName(LPCTSTR GUID, UINT kind)
{
	CString s(GUID);

    switch(kind)
    {
		/* kind */
		case UNIQUE_TO_SYSTEM:
			break;

		case UNIQUE_TO_DESKTOP:
		{
			/* desktop */
			DWORD	len;
			HDESK	desktop	= GetThreadDesktop(GetCurrentThreadId());
			BOOL	result	= GetUserObjectInformation(desktop, UOI_NAME, NULL, 0, &len);
			DWORD	err		= ::GetLastError();

			if( !result && err == ERROR_INSUFFICIENT_BUFFER )
			{
				/* NT/2000 */
				LPBYTE data = new BYTE[len];
				result = GetUserObjectInformation(desktop, UOI_NAME, data, len, &len);
				s += _T("-");
				s += (LPCTSTR)data;
				delete [ ] data;
            }
			else /* Win9x */
				s += _T("-Win9x");

			break;
        }

		case UNIQUE_TO_SESSION:
		{
			/* session */
			HANDLE	token;
			DWORD	len;
			BOOL	result	= OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &token);

			if( result )
            {
				/* NT */
				GetTokenInformation(token, TokenStatistics, NULL, 0, &len);
				LPBYTE	data= new BYTE[len];
				GetTokenInformation(token, TokenStatistics, data, len, &len);
				LUID	uid = ((PTOKEN_STATISTICS)data)->AuthenticationId;
				delete [ ] data;
				CString t;
				t.Format(_T("-%08x%08x"), uid.HighPart, uid.LowPart);
				s += t;
			}
			/* 16-bit OS */

			break;
		}

		case UNIQUE_TO_TRUSTEE:
		{
			/* trustee */
#define NAMELENGTH 64
			TCHAR userName[NAMELENGTH];
			DWORD userNameLength	= NAMELENGTH;
			TCHAR domainName[NAMELENGTH];
			DWORD domainNameLength	= NAMELENGTH;

			if( GetUserName(userName, &userNameLength) )
			{
				/* get network name */
				// The NetApi calls are very time consuming
				// This technique gets the domain name via an
				// environment variable
				domainNameLength = ExpandEnvironmentStrings(
										_T("%USERDOMAIN%"),
										domainName,
										NAMELENGTH);

				CString t;
				t.Format(_T("-%s-%s"), domainName, userName);
				s += t;
			}

			break;
		}

		default:
			ASSERT(FALSE);
			break;
	}

	return s;
}


/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Fred Shaw      Date : 12/15/2006      version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CGETApp::ExitInstance
//
//  Description :   Called by the framework from within the Run member 
//					function to exit this instance of the application. 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// CGETApp message handlers
**/
int CGETApp::ExitInstance(void)
{
	::BCGCBProCleanUp();
	return CWinApp::ExitInstance();
}
