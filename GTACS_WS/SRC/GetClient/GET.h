// GET.h : main header file for the GET application
//

#if !defined(AFX_GET_H__31B8AB15_D894_4CA8_B504_6615E6E36D86__INCLUDED_)
#define AFX_GET_H__31B8AB15_D894_4CA8_B504_6615E6E36D86__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "MainFrm.h"

/////////////////////////////////////////////////////////////////////////////
// CGETApp:
// See GET.cpp for the implementation of this class
//

class CGETApp : public CWinApp
{
public:
	CGETApp();

	BOOL	InitAndStartSockets(WSADATA*);
	BOOL	Connect();
	void	Disconnect();
	void	DeleteListElements(CPtrList*);
	void	ProcessResponse(MSG_RES);
	void	LoadCodeMaxProfile(HWND hWnd);
	void	SetRptPath(CString strPath) { m_strRptPath   = strPath; }
	void	SetRptName(CString strName) { m_strRptNameC  = strName; }
	void	SetSrvName(CString strName) { m_strGTACSHost = strName; }
	void	SetTOInterval(int nInterval){ m_nInterval	 = nInterval; }
	int		GetTOInterval() { return m_nInterval; }
	CString CreateExclusionName(LPCTSTR GUID, UINT kind = UNIQUE_TO_SYSTEM);
	CString GetLastErrorText();
	CString GetEpochOut();

	static BOOL CALLBACK FindInstance(HWND hWnd, LPARAM lParam);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGETApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance(void);
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CGETApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFileNewRequest();
	afx_msg void OnHelpContent();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CString						m_strRptPath;
	CString						m_strRptNameC;
	CString						m_strRptNameS;
	CString						m_strGTACSHost;
	CGETMultiDocTemplate*		m_pDocTemplate;
	CGETComm					m_GETComm;
	int							m_nInterval;

	
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GET_H__31B8AB15_D894_4CA8_B504_6615E6E36D86__INCLUDED_)
