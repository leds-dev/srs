// CommEH.cpp : implementation file
//

#include "StdAfx.h"
#include "GET.h"
#include "GETComm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGETComm

IMPLEMENT_DYNCREATE(CGETComm, CCmdTarget)

CGETComm::CGETComm()
{
}

CGETComm::~CGETComm()
{
}

BEGIN_MESSAGE_MAP(CGETComm, CCmdTarget)
	//{{AFX_MSG_MAP(CGETComm)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGETComm member functions


bool CGETComm::CreateAndConnect(const CString strHost)
{
	USES_CONVERSION;

	if( !m_Socket.Create() )
		return false;

	LPSTR	lpszAscii	= T2A((LPTSTR)((LPCTSTR)strHost));
	struct	hostent* hp = gethostbyname(lpszAscii);

	if( hp == NULL )
		return false;

	struct sockaddr_in sktAddrServer;
	memset(&sktAddrServer, 0, sizeof(struct sockaddr_in));
	memcpy(&(sktAddrServer.sin_addr), hp->h_addr, hp->h_length);
	sktAddrServer.sin_family= hp->h_addrtype;
	sktAddrServer.sin_port	= htons(ctnPortNum);

	if( !m_Socket.Connect((struct sockaddr*)&sktAddrServer, sizeof(sktAddrServer)) )
		return false;

	return true;
}

bool CGETComm::ShutdownAndClose()
{
	if( !m_Socket.ShutDown(0x02) )
		return false;

	m_Socket.Close();

	return true;
}

bool CGETComm::SendMsg(void* pInMsg)
{
	int	nSize = sizeof(MSG_GET);
	int	nSent = m_Socket.Send(pInMsg, nSize);

	if( nSent != nSize )
		return false;

	return true;
}

bool CGETComm::ReadRespMsg(MSG_RES* pMsg)
{
	CString			strMsg;
	CGETApp*		pApp	= (CGETApp*)AfxGetApp();
	PUSHORT			phwArray= (PUSHORT)pMsg;
	MSG_RES*		pRespMsg= (MSG_RES*)&phwArray[0];
	int				nCount	= 1;
	int				nRead	= 0;

	if( !m_Socket.SetTimeOutVal(pApp->GetTOInterval()) )
		return false;

	m_Socket.InitializeTimer();
	nRead = ReadMsg((void*)pRespMsg, MSG_GET_RESP_HW_SIZE*2);

	if( nRead < 0 )
		return false;

	while( pRespMsg->status == ePENDING )
	{
		m_Socket.SetTimeOutVal(pApp->GetTOInterval());
		m_Socket.InitializeTimer();
		strMsg.Format(_T("Server processing request... %d"), nCount++);
		((CMainFrame*)AfxGetMainWnd())->GetStatusBar()->SetWindowText(strMsg);
		nRead = ReadMsg((void*)pRespMsg, MSG_GET_RESP_HW_SIZE*2);

		if( nRead != MSG_GET_RESP_HW_SIZE*2 )
			return false;
	}

	return true;
}

int CGETComm::ReadMsg(void* pObj, int nSize)
{
	int nRead = 0;

	if( nSize > 0 )
	{
		int nBytes = m_Socket.Receive(pObj, nSize);

		if( m_Socket.HadTimeOut() )
		{
			CString strMsg;
			strMsg.Format(_T("No response from GTACS, timeout period of %d seconds has been reached."),120000);
			AfxMessageBox(strMsg, MB_OK);
			return -1;
		}

		if( nBytes < 0 )
			return nBytes;

		nRead += nBytes;

		while( nRead < nSize )
		{
			nBytes = m_Socket.Receive(&((char*)pObj)[nRead], nSize-nRead);

			if( nBytes < 0 )
				return nBytes;

			nRead += nBytes;
		}
	}

	return nRead;
}

