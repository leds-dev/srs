#if !defined(AFX_GETCOMM_H__A205CD37_9644_11D2_B904_00034748C6CA__INCLUDED_)
#define AFX_GETCOMM_H__A205CD37_9644_11D2_B904_00034748C6CA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GETComm.h : header file
//

#include "TimeOutSocket.h"

/////////////////////////////////////////////////////////////////////////////
// CCommUtil command target

class CGETComm : public CCmdTarget
{
	DECLARE_DYNCREATE(CGETComm)

	CGETComm();           // protected constructor used by dynamic creation

// Attributes
public:
	bool	CreateAndConnect(const CString);
	bool	SendMsg(void*);
	bool	ReadRespMsg(MSG_RES*);
	bool	ShutdownAndClose();

// Operations
public:
	virtual ~CGETComm();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGETComm)
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CGETComm)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

// Implementation
protected:
	int		ReadMsg(void*, int);

private:
	CTimeOutSocket	m_Socket;
	UINT			m_nTimer;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GETCOMM_H__A205CD37_9644_11D2_B904_00034748C6CA__INCLUDED_)
