// GETDoc.cpp : implementation of the CGETDoc class
//

#include "stdafx.h"
#include "GET.h"
#include "GETDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGETDoc

IMPLEMENT_DYNCREATE(CGETDoc, CDocument)

BEGIN_MESSAGE_MAP(CGETDoc, CDocument)
	//{{AFX_MSG_MAP(CGETDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGETDoc construction/destruction

CGETDoc::CGETDoc()
{
}

CGETDoc::~CGETDoc()
{
}

BOOL CGETDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	return TRUE;
}

BOOL CGETDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	if( !CDocument::OnOpenDocument(lpszPathName) )
		return FALSE;

	if( !m_wndEdit.m_hWnd )
		return FALSE;

	if( m_wndEdit.OpenFile(lpszPathName) != CME_SUCCESS )
	{
		CGETApp*	pApp = (CGETApp*)AfxGetApp();
		CString		strMsg(pApp->GetLastErrorText());

		AfxMessageBox(strMsg, MB_OK);
		return FALSE;
	}

	return TRUE;
}

BOOL CGETDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	if( !CDocument::OnSaveDocument(lpszPathName) )
		return FALSE;

	if( m_wndEdit.SaveFile(lpszPathName, TRUE) != CME_SUCCESS)
	{
		CGETApp*	pApp = (CGETApp*)AfxGetApp();
		CString		strMsg(pApp->GetLastErrorText());

		AfxMessageBox(strMsg, MB_OK);
		return FALSE;
	}

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CGETDoc serialization

void CGETDoc::Serialize(CArchive& ar)
{
    if( ar.IsLoading() ) {}
}

/////////////////////////////////////////////////////////////////////////////
// CGETDoc diagnostics

#ifdef _DEBUG
void CGETDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CGETDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CGETDoc commands

void CGETDoc::DeleteContents()
{
	CDocument::DeleteContents();
}

UINT CGETDoc::GetLine(UINT nIndex, CString& string)
{
	UINT nLength = 0;

	m_wndEdit.GetLine(nIndex, string);
	nLength= string.GetLength();

    return nLength;
}

UINT CGETDoc::GetLineCount()
{
	UINT nLines = CM_GetLineCount(m_wndEdit) - 1;

	return nLines;
}