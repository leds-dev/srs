// GETDoc.h : interface of the CGETDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GETDOC_H__C70EBC16_2C17_4EF6_8010_E3D8B1CEFF11__INCLUDED_)
#define AFX_GETDOC_H__C70EBC16_2C17_4EF6_8010_E3D8B1CEFF11__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGETDoc : public CDocument
{
protected: // create from serialization only
	CGETDoc();
	DECLARE_DYNCREATE(CGETDoc)

// Attributes
public:

// Operations

public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGETDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual void Serialize(CArchive& ar);
	virtual void DeleteContents();
	//}}AFX_VIRTUAL

// Implementation
public:

	UINT GetLine(UINT nIndex, CString& string);
	UINT GetLineCount();
	virtual ~CGETDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	CCodeMaxCtrl	m_wndEdit;

protected:

// Generated message map functions
protected:

	//{{AFX_MSG(CGETDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GETDOC_H__C70EBC16_2C17_4EF6_8010_E3D8B1CEFF11__INCLUDED_)
