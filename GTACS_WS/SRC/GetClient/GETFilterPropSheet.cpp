// GetFilterPropSheet.cpp : implementation file
//

#include "stdafx.h"
#include "GET.h"
#include "GETFilterPropSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAdvFilterPropSheet

IMPLEMENT_DYNAMIC(CGETFilterPropSheet, CPropertySheet)

CGETFilterPropSheet::CGETFilterPropSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
}

CGETFilterPropSheet::CGETFilterPropSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	m_pMsgList = NULL;

	//add the property page to appear in the tab
	AddPage(&m_filterevttype);
	AddPage(&m_filterevtcritical);
	AddPage(&m_filterevtnum);
	AddPage(&m_filterevtname);
	AddPage(&m_filterevtmnem);
	AddPage(&m_filterevttext);
}

CGETFilterPropSheet::~CGETFilterPropSheet()
{
}


BEGIN_MESSAGE_MAP(CGETFilterPropSheet, CPropertySheet)
	//{{AFX_MSG_MAP(CGETFilterPropSheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_SAVE_BUTTON, OnButtonSave)
	ON_BN_CLICKED(IDC_RESTORE_BUTTON, OnButtonRestore)
	ON_BN_CLICKED(IDOK, OnButtonOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGetFilterPropSheet message handlers

void CGETFilterPropSheet::GetFilterOpt()
{
	m_filterevttype.UpdateMsg(m_pMsgList);
	m_filterevtcritical.UpdateMsg(m_pMsgList);
	m_filterevtnum.UpdateMsg(m_pMsgList);
	m_filterevtname.UpdateMsg(m_pMsgList);
	m_filterevtmnem.UpdateMsg(m_pMsgList);
	m_filterevttext.UpdateMsg(m_pMsgList);
}

BOOL CGETFilterPropSheet::OnInitDialog()
{
	BOOL	bResult = CPropertySheet::OnInitDialog();
	CRect	rect;
	CRect	tabrect;
	int		width;

	//Get button sizes and positions
	GetDlgItem(IDOK)->GetWindowRect(rect);
	GetTabControl()->GetWindowRect(tabrect);
	ScreenToClient(rect);
	ScreenToClient(tabrect);

	//New button - width, height and Y-coordiate of IDOK
	//           - X-coordinate of tab control
	width		= rect.Width();
	rect.left	= tabrect.left;
	rect.right	= tabrect.left + width;

	//Create new "Add" button and set standard font
	m_ButtonSave.Create(_T("Save"),
			BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE|WS_TABSTOP,
			rect, this, IDC_SAVE_BUTTON);
	m_ButtonSave.SetFont(GetFont());

	rect.left	= 6 + rect.right;
	rect.right	= rect.left + width;

	m_ButtonRestore.Create(_T("Restore"),
			BS_PUSHBUTTON|WS_CHILD|WS_VISIBLE|WS_TABSTOP,
			rect, this, IDC_RESTORE_BUTTON);
	m_ButtonRestore.SetFont(GetFont());

	SetActivePage(0);
	SetActivePage(1);
	SetActivePage(2);
	SetActivePage(3);
	SetActivePage(4);
	SetActivePage(5);
	SetActivePage(0);

	return bResult;
}

void CGETFilterPropSheet::OnButtonSave()
{
	CGETApp*	pApp = (CGETApp*)AfxGetApp();
	CFileDialog	dlg(FALSE,_T("flt"),_T(""),OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,ctstrFILTER_FLAGS);
	CString		strInitDir(pApp->GetEpochOut());
	CFileFind	finder;

	if( !strInitDir.IsEmpty() )
	{
		strInitDir += ctstrRPT_LOC_DIR;

		if( !finder.FindFile(strInitDir) )
			CreateDirectory(strInitDir, NULL);

		dlg.m_ofn.lpstrInitialDir= strInitDir;
	}

	if( dlg.DoModal() == IDOK )
	{
		CStdioFile	file;
		CString		strTxt;
		CString		strPathName(dlg.GetPathName());
		int			nMode = CFile::typeText | CFile::modeCreate | CFile::modeWrite | CFile::shareExclusive;

		if( !file.Open(strPathName, nMode) )
			return;

		TRY
		{
			file.WriteString(_T("#\n"));
			file.WriteString(_T("# @# GTACS Event History Report filter options configuration file.\n"));
			file.WriteString(_T("#\n"));
			file.WriteString(_T("#\n"));
			file.WriteString(_T("# GET filter options...\n"));
			file.WriteString(_T("#\n"));
			m_filterevttype.SaveOpt(&file);
			m_filterevtcritical.SaveOpt(&file);
			m_filterevtnum.SaveOpt(&file);
			m_filterevtname.SaveOpt(&file);
			m_filterevtmnem.SaveOpt(&file);
			m_filterevttext.SaveOpt(&file);
		}
		CATCH( CFileException, pE )
		{
			strTxt.Format(_T("Error writing file %s: %s"), pE->m_strFileName, pE->m_cause);
			AfxMessageBox(strTxt, MB_OK);
			file.Close();
			return;
		}
		END_CATCH

		file.Close();
	}
}

void CGETFilterPropSheet::OnButtonRestore()
{
	CGETApp*	pApp = (CGETApp*)AfxGetApp();
	CFileDialog dlg(TRUE,_T("flt"),_T(""),OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,ctstrFILTER_FLAGS);
	CString		strInitDir(pApp->GetEpochOut());
	CFileFind	finder;

	if( !strInitDir.IsEmpty() )
	{
		strInitDir += ctstrRPT_LOC_DIR;

		if( !finder.FindFile(strInitDir) )
			CreateDirectory(strInitDir, NULL);

		dlg.m_ofn.lpstrInitialDir= strInitDir;
	}

	if( dlg.DoModal() == IDOK )
	{
		CStdioFile	file;
		CString		strTxt;
		CString		strPathName(dlg.GetPathName());
		int			nMode = CFile::typeText | CFile::modeRead | CFile::shareDenyNone;

		if( !file.Open(strPathName, nMode) )
			return;

		TRY
		{
			while( file.ReadString(strTxt) )
				m_FileData.Add(strTxt);

			m_filterevttype.RestoreOpt(&m_FileData);
			m_filterevtcritical.RestoreOpt(&m_FileData);
			m_filterevtnum.RestoreOpt(&m_FileData);
			m_filterevtname.RestoreOpt(&m_FileData);
			m_filterevtmnem.RestoreOpt(&m_FileData);
			m_filterevttext.RestoreOpt(&m_FileData);
			m_FileData.RemoveAll();
		}
		CATCH( CFileException, pE )
		{
			strTxt.Format(_T("Error writing file %s: %s"), pE->m_strFileName, pE->m_cause);
			AfxMessageBox(strTxt, MB_OK);
			file.Close();
			return;
		}
		END_CATCH

		file.Close();
	}
}

void CGETFilterPropSheet::OnButtonOk()
{
	GetFilterOpt();
	EndDialog(IDOK);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CGETFilterPropSheet::NeedNewMsgs
//	Description :	NeedNewMsgs determines how many messages are
//					needed to send the GET message to GET Server.
//
//	Return :		void		-
//
//	Parameters :
//		CStringArray& strList	-	reference to a list of directives.
//		int nLength				-	message size limit
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CGETFilterPropSheet::NeedNewMsgs(CPtrList* pList, int nCount, int nLength)
{
	MSG_GET* pMsgGET = NULL;

	// Find out how many messages are needed to send to GET Server
	int nRemainder= (int)(nCount%nLength);
	int nMessages = (int)(nCount/nLength);

	nMessages -= pList->GetCount();

	if( nRemainder )
		nMessages++;

	for( int i=0; i < nMessages; i++ )
	{
		pMsgGET	= new MSG_GET;
		memset(pMsgGET, 0, sizeof(MSG_GET));
		pList->AddTail(pMsgGET);
	}

	pMsgGET = NULL;
}
