#if !defined(AFX_GETFILTERPROPSHEET_H__9A89ABBE_CC51_4F20_9414_DAA5081CB35B__INCLUDED_)
#define AFX_GETFILTERPROPSHEET_H__9A89ABBE_CC51_4F20_9414_DAA5081CB35B__INCLUDED_

#include "FilterEvtType.h"	// Added by ClassView
#include "FilterEvtCritical.h"	// Added by ClassView
#include "FilterEvtNum.h"	// Added by ClassView
#include "FilterEvtName.h"	// Added by ClassView
#include "FilterEvtText.h"	// Added by ClassView
#include "FilterMnem.h"	// Added by ClassView

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GETFilterPropSheet.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGETFilterPropSheet

class CGETFilterPropSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(CGETFilterPropSheet)

// Construction
public:
	CGETFilterPropSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CGETFilterPropSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:

// Operations
public:

	void	SetFilterOpt(CPtrList* pList) { m_pMsgList = pList; }
	void	GetFilterOpt();
	void	OnButtonSave();
	void	OnButtonRestore();
	void	OnButtonOk();
	void	NeedNewMsgs(CPtrList*, int, int);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGETFilterPropSheet)
	public:
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
public:
	CFilterMnem			m_filterevtmnem;
	CFilterEvtText		m_filterevttext;
	CFilterEvtName		m_filterevtname;
	CFilterEvtNum		m_filterevtnum;
	CFilterEvtCritical	m_filterevtcritical;
	CFilterEvtType		m_filterevttype;

	virtual ~CGETFilterPropSheet();

	// Generated message map functions
protected:
	//{{AFX_MSG(CGETFilterPropSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CStringArray		m_FileData;
	CButton				m_ButtonSave;
	CButton				m_ButtonRestore;
	CPtrList*			m_pMsgList;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GETFILTERPROPSHEET_H__9A89ABBE_CC51_4F20_9414_DAA5081CB35B__INCLUDED_)
