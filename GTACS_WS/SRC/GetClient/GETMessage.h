/********************************************************************************
 *
 * Purpose:
 *
 *       Declaration for GTACS Event Tool Message (Client Message)
 *
 *
 *       VERSION     DATE        Author          COMMENT
 *       -------     ------      -------         --------------------------------
 *
 *       1.0         09/23/04    Ray Mosley      Created.
 *
 ********************************************************************************/

#ifndef GETMESSAGE_H
#define GETMESSAGE_H

#define NAMELENL				80
#define EVENT_LIST_SIZE			50
#define EVT_NAME_LIST_SIZE		50
#define MNEM_LIST_SIZE			50
#define NUM_EVT_TXT_FILTER_OPT	 3
#define EVENT_NAME_SIZE 		32
#define MNEMONIC_NAME_SIZE		36
#define NUM_EVENT_TYPE			16
#define NUM_EVENT_CRITICALITY	 6
// not large enough need to increased from 10 to 18 but on the server side too
#define WKSNAMELEN				10
#define PROCESS_ID				 4
#define TIMESTRLEN				22

#define NUMBER_OF_SPACECRAFTS	 8
/* six real-time stream + ground stream */
#define NUMBER_OF_STREAMS		 8

typedef enum eServerStatus  
{		
	eSUCCESS	= '0',
	eMSG_ERROR	= '1',
	eSRV_ERROR	= '3',
	ePENDING	= '4'
};

/* struct of message to be sent to server */
typedef struct GET_Request_header
{
	char	id[PROCESS_ID];										//	  04 bytes
	char	wksname[WKSNAMELEN];								//	  10 bytes
	char	usrname[NAMELENL];									//	  80 bytes
	WORD	seq_num;											//	  02 bytes
	WORD	num_msg;											//	  02 bytes
} MSG_GET_HEADER;												//	  98 bytes	(100)

const int MSG_GET_HEADER_HW_SIZE = sizeof(MSG_GET_HEADER)/2;

typedef struct GET_Request_Search_Opt
{
	char	m_strttime[TIMESTRLEN];								//	  22 bytes
	char	m_stoptime[TIMESTRLEN];								//	  22 bytes
	char	scname_select_list[NUMBER_OF_SPACECRAFTS];			//	  08 bytes
	char	strmname_select_list[NUMBER_OF_STREAMS];			//	  08 bytes
	DWORD	max_evt;											//	  04 bytes
	char	ofilename[NAMELENL];								//	  80 bytes
	char	sortby;												//	  01 byte
	char	spare;												//	  01 byte
} MSG_GET_SEARCH_DATA;											//	 146 bytes  (148)

const int MSG_GET_SEARCH_DATA_HW_SIZE = sizeof(MSG_GET_SEARCH_DATA)/2;

typedef struct GET_Request_Filter_Opt
{
	char	evttype_select_list[NUM_EVENT_TYPE];				//	  16 bytes
	char	evtcrit_select_list[NUM_EVENT_CRITICALITY];			//	  06 bytes
	DWORD	evtnum_list[EVENT_LIST_SIZE];						//	 200 bytes
	char	evtname_list[EVT_NAME_LIST_SIZE][EVENT_NAME_SIZE];	//	1600 bytes
	char	mnem_list[MNEM_LIST_SIZE][MNEMONIC_NAME_SIZE];		//	1800 bytes
	char 	evttxt[NAMELENL];									//	  80 bytes
	char	evttxt_filterby[NUM_EVT_TXT_FILTER_OPT];			//	  03 bytes
	char	spare2;												//	  01 byte
} MSG_GET_FILTER_DATA;											//  3706 bytes (3708)

																// -----------
																//  3950 bytes (3956) <---> 989 HW
const int MSG_GET_FILTER_DATA_HW_SIZE = sizeof(MSG_GET_FILTER_DATA)/2;

typedef struct GET_Request_Msg
{
	MSG_GET_HEADER		msg_get_header;
	MSG_GET_SEARCH_DATA	msg_get_search_data;
	MSG_GET_FILTER_DATA	msg_get_filter_data;
} MSG_GET;

const int MSG_GET_HW_SIZE = sizeof(MSG_GET)/2;

typedef struct GET_Response_Msg
{
	char	status;
	char	response[NAMELENL+1];
} MSG_RES;

const int MSG_GET_RESP_HW_SIZE = sizeof(MSG_RES)/2;

#endif
