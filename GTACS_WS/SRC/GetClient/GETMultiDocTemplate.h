// GETMultiDocTemplate.h: interface for the EvtHistMultiDocTemplate class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GETMULTIDOCTEMPLATE_H__708B4B77_5A80_11D5_9A81_0003472193C8__INCLUDED_)
#define AFX_GETMULTIDOCTEMPLATE_H__708B4B77_5A80_11D5_9A81_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGETMultiDocTemplate : public CMultiDocTemplate
{
public:
	CGETMultiDocTemplate::CGETMultiDocTemplate(
		UINT			nIDResource,
		CRuntimeClass*	pDocClass,
		CRuntimeClass*	pFrameClass,
		CRuntimeClass*	pViewClass);
	virtual ~CGETMultiDocTemplate();
	CDocTemplate::Confidence CGETMultiDocTemplate::MatchDocType(
		const TCHAR*	pszPathName,
		CDocument*&		rpDocMatch);
};

#endif // !defined(AFX_GETMULTIDOCTEMPLATE_H__708B4B77_5A80_11D5_9A81_0003472193C8__INCLUDED_)
