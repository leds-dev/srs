// GETView.cpp implementation of the CGETView class
//
#include "stdafx.h"
#include "GET.h"
#include "GETDoc.h"
#include "GETView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define PRINTMARGIN  2

IMPLEMENT_DYNCREATE(CGETView, CView)

BEGIN_MESSAGE_MAP(CGETView, CView)
//{{AFX_MSG_MAP(CGETView)
ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
// Standard Printing Commands
ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

CGETView::CGETView()
{
	m_cxTabSize		= 0;
	m_cyPrinter		= 0;
	m_cyScreen		= 0;
	m_nLinesTotal	= 0;
	m_nPrintLines	= 0;
	m_nTextExtent	= 0;
	m_nLinesPerPage = 0;
	m_cxOffset		= 0;
	m_cxwidth		= 0;
}

CGETView::~CGETView()
{
}

BOOL CGETView::PreCreateWindow(CREATESTRUCT &cs)
{
	return CView::PreCreateWindow(cs);
}

int CGETView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	if( !GetDocument()->m_wndEdit.Create(WS_CHILD|WS_VISIBLE,CRect(0,0,100,100),this,IDC_CM_EDIT) )
		return -1;

	((CGETApp*)AfxGetApp())->LoadCodeMaxProfile(GetDocument()->m_wndEdit);

	return 0;
}

void CGETView::OnDraw(CDC* pDC)
{
}

void CGETView::OnInitialUpdate()
{
	CView::OnInitialUpdate();

	m_nLinesTotal= GetDocument()->GetLineCount();
	m_nTextExtent= 81;
	m_nPrintLines= m_nLinesTotal==0?0:TotalPrintLines(GetDocument());
}

void CGETView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);

	if( GetDocument()->m_wndEdit.m_hWnd )
		GetDocument()->m_wndEdit.SetWindowPos(NULL, -1, -1, cx, cy, SWP_NOZORDER | SWP_NOMOVE);
}

void CGETView::OnSetFocus(CWnd* pOldWnd)
{
	CView::OnSetFocus(pOldWnd);

	if( GetDocument()->m_wndEdit.m_hWnd )
		GetDocument()->m_wndEdit.SetFocus();
}

BOOL CGETView::OnPreparePrinting(CPrintInfo* pInfo)
{
	if( !DoPreparePrinting(pInfo) )
		return FALSE;
	else
	{
		LPDEVMODE pDM	= pInfo->m_pPD->GetDevMode();
		m_nTextExtent	= pDM->dmOrientation==DMORIENT_PORTRAIT?81:133;
		m_strTextExt	= m_nTextExtent==81?ctstrTEXT_EXT1:ctstrTEXT_EXT2;
		m_nPrintLines	= TotalPrintLines(GetDocument());
	}

	return TRUE;
}

void CGETView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	int nHeight = -MulDiv(10,GetDeviceCaps((HDC)pDC,LOGPIXELSY),72);
	// Create the printer font
	m_fontPrinter.CreateFont(nHeight,
		0,0,0,FW_NORMAL,0,0,0,0,0,0,0,FIXED_PITCH|FF_MODERN,_T("Courier New"));

	// compute the width and height of a line in the printer font
	TEXTMETRIC	tm;
	CFont*		pOldFont= pDC->SelectObject(&m_fontPrinter);

	pDC->GetTextMetrics(&tm);
	m_cxTabSize			= ((tm.tmAveCharWidth*10) * CM_GetTabSize(GetDocument()->m_wndEdit));
	m_cyPrinter			= tm.tmHeight + tm.tmExternalLeading;
	CSize	size		= pDC->GetTextExtent(m_strTextExt, m_nTextExtent);
	pDC->SelectObject(pOldFont);
	int		nDC			= pDC->GetDeviceCaps(HORZRES);
	int		nCX			= size.cx;
	m_cxwidth			= nCX>nDC?nDC:nCX;

	// compute the pq count
	m_nLinesPerPage		= (pDC->GetDeviceCaps(VERTRES) - (m_cyPrinter * (3 + (2*PRINTMARGIN))))/m_cyPrinter;
	UINT nMaxPage		= max(1, (m_nPrintLines + (m_nLinesPerPage-1))/ m_nLinesPerPage);
	pInfo->SetMaxPage(nMaxPage);

	// compute the horizontal offset required to center each line of output
	m_cxOffset			= (nDC - m_cxwidth)/2;
}

void CGETView::OnPrint(CDC* pDC, CPrintInfo* pInfo)
{
	CGETDoc* pDoc = GetDocument();
	PrintPageHeader(pDoc, pDC, pInfo->m_nCurPage);
	PrintPage(pDoc, pDC, pInfo->m_nCurPage);
}

void CGETView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	m_fontPrinter.DeleteObject();
}

#ifdef _DEBUG
void CGETView::AssertValid() const
{
	CView::AssertValid();
}

void CGETView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGETDoc* CGETView::GetDocument()
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGETDoc)));
	return (CGETDoc*)m_pDocument;
}
#endif //_DEBUG

void CGETView::FormatLine(CGETDoc* pDoc, UINT nLine, CString& string)
{
	UINT nCount = pDoc->GetLine(nLine, string);
}

void CGETView::PrintPageHeader(CGETDoc* pDoc, CDC* pDC, UINT nPageNumber)
{
	CString strHeader = pDoc->GetPathName();

	if( strHeader.GetLength() > 68 )
		strHeader = pDoc->GetTitle();

	CString strPageNumber;
	strPageNumber.Format(_T("Page %d"), nPageNumber);

	UINT nSpaces = m_nTextExtent - strPageNumber.GetLength() - strHeader.GetLength();

	for( UINT i=0; i < nSpaces; i++ )
		strHeader += _T(' ');

	strHeader	+=strPageNumber;

	// output the text
	UINT	y		= m_cyPrinter * PRINTMARGIN;
	CFont*	pOldFont= pDC->SelectObject(&m_fontPrinter);
	//PR000508: GET crashes when trying to do print preview.
	// Existing bug in TabbedTextOut in this release of MFC libs. Using TextOut instead.
	// Fixed by MSD, 06/2012, OSPO
	pDC->TextOut(m_cxOffset, y, strHeader,strHeader.GetLength());
	//pDC->TabbedTextOut(m_cxOffset, y, strHeader, strHeader.GetLength(), 1, &m_cxTabSize, 0);

	// Draw a horizontal line underneath the line of text
	y += (m_cyPrinter * 3) / 2;
	pDC->MoveTo(m_cxOffset, y);
	pDC->LineTo(m_cxOffset + m_cxwidth, y);
	pDC->SelectObject(pOldFont);
}

void CGETView::PrintPage(CGETDoc* pDoc, CDC* pDC, UINT nPageNumber)
{
	if( m_nLinesTotal != 0 )
	{
		UINT	nStart	= (nPageNumber-1) * m_nLinesPerPage;
		UINT	nEnd	= min(m_nPrintLines-1, nStart+m_nLinesPerPage-1);
		CFont*	pOldFont= pDC->SelectObject(&m_fontPrinter);

		for( UINT i=nStart; i <= nEnd; i++ )
		{
			CString string;

			if( i < (UINT)m_pPrnData.GetSize() )
			{
				string = m_pPrnData.GetAt(i);
				UINT y = ((i-nStart) + PRINTMARGIN + 3) * m_cyPrinter;
				pDC->TextOut(m_cxOffset, y, string, string.GetLength());
				//pDC->TabbedTextOut(m_cxOffset, y, string, string.GetLength(), 1, &m_cxTabSize, 0);
			}
		}

		pDC->SelectObject(pOldFont);
	}
}

UINT CGETView::TotalPrintLines(CGETDoc* pDoc)
{
	CString string;
	CString strsub;
	UINT	nLength;
	BOOL	bWrap	= FALSE;

	m_pPrnData.RemoveAll();

	for( UINT i=0; i<m_nLinesTotal; i++ )
	{
		FormatLine(pDoc, i, string);

		nLength = string.GetLength();

		if( nLength > m_nTextExtent )
		{
			strsub	= string.Right(nLength-m_nTextExtent);
			string.Delete(nLength-(nLength-m_nTextExtent), nLength-m_nTextExtent);
			bWrap	= TRUE;
		}

		m_pPrnData.Add(string);

		if( bWrap )
		{
			m_pPrnData.Add(strsub);
			bWrap = FALSE;
			strsub.Empty();
		}

		string.Empty();
	}

	return m_pPrnData.GetSize();
}

