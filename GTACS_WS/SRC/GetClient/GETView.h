// GETView.h : interface of the CGETView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_GETVIEW_H__043357FD_1704_40BD_AEE3_7FF457D3D032__INCLUDED_)
#define AFX_GETVIEW_H__043357FD_1704_40BD_AEE3_7FF457D3D032__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGETView : public CView
{
protected: // create from serialization only
	CGETView();
	DECLARE_DYNCREATE(CGETView)

// Attributes
public:
	CGETDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGETView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CGETView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	CStringArray	m_pPrnData;
	CString			m_strTextExt;
	CFont			m_fontScreen;
	CFont			m_fontPrinter;
	int				m_cxTabSize;
	UINT			m_cyPrinter;
	UINT			m_cyScreen;
	UINT			m_nLinesTotal;
	UINT			m_nPrintLines;
	UINT			m_nTextExtent;
	UINT			m_nLinesPerPage;
	UINT			m_cxOffset;
	UINT			m_cxwidth;

	UINT			TotalPrintLines(CGETDoc* pDoc);
	void			PrintPage(CGETDoc* pDoc, CDC* pDC, UINT nPageNumber);
	void			PrintPageHeader(CGETDoc* pDoc, CDC* pDC, UINT nPageNumber);
	void			FormatLine(CGETDoc* pDoc, UINT nLine, CString& string);

	//{{AFX_MSG(CGETView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in GETView.cpp
inline CGETDoc* CGETView::GetDocument()
   { return (CGETDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GETVIEW_H__043357FD_1704_40BD_AEE3_7FF457D3D032__INCLUDED_)
