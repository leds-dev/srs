// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__04302A40_AE0B_4D56_A269_018C3AF8328B__INCLUDED_)
#define AFX_MAINFRM_H__04302A40_AE0B_4D56_A269_018C3AF8328B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CMainFrame : public CBCGPMDIFrameWnd
{	
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

protected: // create from serialization only
//	CMainFrame();
//	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:

// Operations
public:
	CStatusBar* GetStatusBar() { return &m_wndStatusBar; }

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // control bar embedded members

	CBCGPMenuBar	m_wndMenuBar;
	CStatusBar		m_wndStatusBar;
	CBCGPToolBar	m_wndToolBar;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnConnectionTimeOut();
	//}}AFX_MSG
	afx_msg LRESULT OnNewInstance(WPARAM, LPARAM);
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__04302A40_AE0B_4D56_A269_018C3AF8328B__INCLUDED_)
