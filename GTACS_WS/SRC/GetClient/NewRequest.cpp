// NewRequest.cpp : implementation file
//

#include "stdafx.h"
#include "GET.h"
#include "GETFilterPropSheet.h"
#include "NewRequest.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewRequest dialog


CNewRequest::CNewRequest(CWnd* pParent /*=NULL*/)
	: CDialog(CNewRequest::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNewRequest)
	m_strStrtTime = _T("");
	m_strStopTime = _T("");
	m_strFilename = _T("");
	//}}AFX_DATA_INIT

	m_pMsgList = NULL;
}


void CNewRequest::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewRequest)
	DDX_Control(pDX, IDC_MAX_EVT_EDIT, m_cMaxEvtEdit);
	DDX_Control(pDX, IDC_STOP_TIME, m_cStopTimeEdit);
	DDX_Control(pDX, IDC_START_TIME, m_cStrtTimeEdit);
	DDX_Text(pDX, IDC_START_TIME, m_strStrtTime);
	DDX_Text(pDX, IDC_STOP_TIME, m_strStopTime);
	DDX_Text(pDX, IDC_OUTPUT_EDIT, m_strFilename);
	DDV_MaxChars(pDX, m_strFilename, 80);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewRequest, CDialog)
	//{{AFX_MSG_MAP(CNewRequest)
	ON_BN_CLICKED(IDC_FILTER_BUTTON, OnFilterButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewRequest message handlers

BOOL CNewRequest::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString strMask(_T("[12][09][01289][0-9]-[0-3][0-9][0-9]-"));
	strMask += _T("[012][0-9]:[0-5][0-9]:[0-5][0-9]");
	CTime t  = CTime::GetCurrentTime();
	m_cStrtTimeEdit.Init(strMask);
	m_cStrtTimeEdit.SetPromptChar(_T('0'));
	m_cStopTimeEdit.Init(strMask);
	m_cStopTimeEdit.SetPromptChar(_T('0'));
	m_strStopTime = t.FormatGmt(_T("%Y-%j-%H:%M:%S"));
	t -= CTimeSpan( 0, 3, 0, 0 );
	m_strStrtTime = t.FormatGmt(_T("%Y-%j-%H:%M:%S"));
	UpdateData(FALSE);
	m_cMaxEvtEdit.SetFractionalDigitCount(0);
	m_cMaxEvtEdit.SetWindowText(_T("15000"));
	((CButton*)GetDlgItem(IDC_ALL_CHECK))->SetCheck(1);
	((CEdit*)GetDlgItem(IDC_OUTPUT_EDIT))->SetWindowText(_T("GET_RPT.txt"));
	((CButton*)GetDlgItem(IDC_TIME_RADIO))->SetCheck(1);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CNewRequest::OnOK()
{
	CString		strTxt;
	CString		strErrorTxt;
	CTime		t_strt;
	CTime		t_stop;
	POSITION	pos		= m_pMsgList->FindIndex(0);
	MSG_GET*	pMsgGET = (MSG_GET*)m_pMsgList->GetAt(pos);

	
	m_cStrtTimeEdit.GetWindowText(strTxt);

	if( !IsTimeValid(strTxt, &t_strt, &strErrorTxt) )
	{
		AfxMessageBox(strErrorTxt, MB_OK);
		return;
	}
		
	USES_CONVERSION;

	strcpy_s(pMsgGET->msg_get_search_data.m_strttime, T2A((LPTSTR)((LPCTSTR)strTxt)));
	m_cStopTimeEdit.GetWindowText(strTxt);

	if( !IsTimeValid(strTxt, &t_stop, &strErrorTxt) )
	{
		AfxMessageBox(strErrorTxt, MB_OK);
		return;
	}

	if( t_strt >= t_stop )
	{
		AfxMessageBox(_T("Start time MUST BE EARILER than the stop time."), MB_OK);
		return;
	}
		
	strcpy_s(pMsgGET->msg_get_search_data.m_stoptime, T2A((LPTSTR)((LPCTSTR)strTxt)));

	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_SC13_CHECK))->GetCheck());
	pMsgGET->msg_get_search_data.scname_select_list[0] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_SC14_CHECK))->GetCheck());
	pMsgGET->msg_get_search_data.scname_select_list[1] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_SC15_CHECK))->GetCheck());
	pMsgGET->msg_get_search_data.scname_select_list[2] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_SC16_CHECK))->GetCheck());
	pMsgGET->msg_get_search_data.scname_select_list[3] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_SC17_CHECK))->GetCheck());
	pMsgGET->msg_get_search_data.scname_select_list[4] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_SC18_CHECK))->GetCheck());
	pMsgGET->msg_get_search_data.scname_select_list[5] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_SC19_CHECK))->GetCheck());
	pMsgGET->msg_get_search_data.scname_select_list[6] = (char)strTxt[0];
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_SC20_CHECK))->GetCheck());
	pMsgGET->msg_get_search_data.scname_select_list[7] = (char)strTxt[0];

	BOOL bAll = ((CButton*)GetDlgItem(IDC_ALL_CHECK))->GetCheck();
	strTxt.Format(_T("%d"), bAll);

	if( bAll )
	{
		pMsgGET->msg_get_search_data.strmname_select_list[0] = (char)strTxt[0];
		pMsgGET->msg_get_search_data.strmname_select_list[1] = '1';
		pMsgGET->msg_get_search_data.strmname_select_list[2] = '1';
		pMsgGET->msg_get_search_data.strmname_select_list[3] = '1';
		pMsgGET->msg_get_search_data.strmname_select_list[4] = '1';
		pMsgGET->msg_get_search_data.strmname_select_list[5] = '1';
		pMsgGET->msg_get_search_data.strmname_select_list[6] = '1';
		pMsgGET->msg_get_search_data.strmname_select_list[7] = '1';
	}
	else
	{
		pMsgGET->msg_get_search_data.strmname_select_list[0] = (char)strTxt[0];
		strTxt.Format(_T("%d"), ((CButton*)GetDlgItem(IDC_GE_CHECK))->GetCheck());
		pMsgGET->msg_get_search_data.strmname_select_list[1] = (char)strTxt[0];
		strTxt.Format(_T("%d"), ((CButton*)GetDlgItem(IDC_RT1_CHECK))->GetCheck());
		pMsgGET->msg_get_search_data.strmname_select_list[2] = (char)strTxt[0];
		strTxt.Format(_T("%d"), ((CButton*)GetDlgItem(IDC_RT2_CHECK))->GetCheck());
		pMsgGET->msg_get_search_data.strmname_select_list[3] = (char)strTxt[0];
		strTxt.Format(_T("%d"), ((CButton*)GetDlgItem(IDC_RT3_CHECK))->GetCheck());
		pMsgGET->msg_get_search_data.strmname_select_list[4] = (char)strTxt[0];
		strTxt.Format(_T("%d"), ((CButton*)GetDlgItem(IDC_RT4_CHECK))->GetCheck());
		pMsgGET->msg_get_search_data.strmname_select_list[5] = (char)strTxt[0];
		strTxt.Format(_T("%d"), ((CButton*)GetDlgItem(IDC_RT5_CHECK))->GetCheck());
		pMsgGET->msg_get_search_data.strmname_select_list[6] = (char)strTxt[0];
		strTxt.Format(_T("%d"), ((CButton*)GetDlgItem(IDC_RT6_CHECK))->GetCheck());
		pMsgGET->msg_get_search_data.strmname_select_list[7] = (char)strTxt[0];
	}

	if( m_cMaxEvtEdit.GetValue() > 15000 )
	{
		strTxt = _T("Please enter a number less than or equal to the\n");
		strTxt+= _T("maximum events value of 15,000.");
		AfxMessageBox(strTxt, MB_OK);
		return;
	}

	pMsgGET->msg_get_search_data.max_evt = htonl((DWORD)m_cMaxEvtEdit.GetValue());

	if( !IsSCSelected() )
	{
		strTxt = _T("Please select a spacecraft.");
		AfxMessageBox(strTxt, MB_OK);
		return;
	}

	if( !IsStrmSelected() )
	{
		strTxt = _T("Please select a stream.");
		AfxMessageBox(strTxt, MB_OK);
		return;
	}

	if( strTxt.IsEmpty() )
	{
		strTxt = _T("Please select output file name.");
		AfxMessageBox(strTxt, MB_OK);
		return;
	}

	int nCurSel = ((CComboBox*)GetDlgItem(IDC_SERVER_NAME_COMBO))->GetCurSel();

	if( nCurSel == -1 )
	{
		strTxt = _T("Please select GTACS server.");
		AfxMessageBox(strTxt, MB_OK);
		return;
	}

	CString strServer;
	CString strPath;

	((CComboBox*)GetDlgItem(IDC_SERVER_NAME_COMBO))->GetLBText(nCurSel, strServer);
	strPath.Format(_T("\\\\%s%s%s"), strServer, ctstrSHARE_NAME, ctstrRPT_RMT_DIR);

	COXUNC		unc(strPath+_T("\\"));
	CWaitCursor	wait;
			
	strTxt.Format(_T("Checking network path for GTACS server %s."), strServer);
	((CMainFrame*)AfxGetMainWnd())->GetStatusBar()->SetWindowText(strTxt);

	if( !unc.Exists() )
	{
		strTxt = _T("Network path does not exist!\n");
		strTxt+= _T("Please select another GTACS server.");
		AfxMessageBox(strTxt, MB_OK);
		return;
	}

	((CGETApp*)AfxGetApp())->SetSrvName(strServer);
	((CGETApp*)AfxGetApp())->SetRptPath(strPath);
	((CEdit*)GetDlgItem(IDC_OUTPUT_EDIT))->GetWindowText(strTxt);
	((CGETApp*)AfxGetApp())->SetRptName(strTxt);
	strcpy_s(pMsgGET->msg_get_search_data.ofilename, T2A((LPTSTR)((LPCTSTR)strTxt)));
	strTxt.Format(_T("%d"),((CButton*)GetDlgItem(IDC_TIME_RADIO))->GetCheck());
	pMsgGET->msg_get_search_data.sortby = (char)strTxt[0];

	CDialog::OnOK();
}

BOOL CNewRequest::IsSCSelected()
{
	if( !((CButton*)GetDlgItem(IDC_SC13_CHECK))->GetCheck() &&
		!((CButton*)GetDlgItem(IDC_SC14_CHECK))->GetCheck() &&
		!((CButton*)GetDlgItem(IDC_SC15_CHECK))->GetCheck() &&
		!((CButton*)GetDlgItem(IDC_SC16_CHECK))->GetCheck() &&
		!((CButton*)GetDlgItem(IDC_SC17_CHECK))->GetCheck() &&
		!((CButton*)GetDlgItem(IDC_SC18_CHECK))->GetCheck() &&
		!((CButton*)GetDlgItem(IDC_SC19_CHECK))->GetCheck() &&
		!((CButton*)GetDlgItem(IDC_SC20_CHECK))->GetCheck() )
		return FALSE;

	return TRUE;
}

BOOL CNewRequest::IsStrmSelected()
{
	if(	!((CButton*)GetDlgItem(IDC_ALL_CHECK))->GetCheck()&&
		!((CButton*)GetDlgItem(IDC_GE_CHECK))->GetCheck() &&
		!((CButton*)GetDlgItem(IDC_RT1_CHECK))->GetCheck()&&
		!((CButton*)GetDlgItem(IDC_RT2_CHECK))->GetCheck()&&
		!((CButton*)GetDlgItem(IDC_RT3_CHECK))->GetCheck()&&
		!((CButton*)GetDlgItem(IDC_RT4_CHECK))->GetCheck()&&
		!((CButton*)GetDlgItem(IDC_RT5_CHECK))->GetCheck()&&
		!((CButton*)GetDlgItem(IDC_RT6_CHECK))->GetCheck() )
		return FALSE;

	return TRUE;
}

void CNewRequest::OnFilterButton() 
{
	CGETFilterPropSheet ps(_T("Filter Options"));

	ps.m_psh.dwFlags |= PSH_NOAPPLYNOW;
	ps.SetFilterOpt(m_pMsgList);
	ps.DoModal();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author :Ray Mosley			Date : 12/14/04			version 1.0
//////////////////////////////////////////////////////////////////////////
//
//  Function :		CNewRequest::IsTimeValid
//	Description :	Determines if the input text string time is vaild.
//
//	Return :		
//		BOOL		-	TRUE if the string is valid.
//						FALSE if the string is invalid.  See reason for
//						being invalid is stored in the ErrorText
//						parameter.
//	Parameters :	
//		CString		strText			-	CString to check.
//		CTime*		pTime			-	If the time span is valid, this
//										parameter will contain a copy
//										of it.
//		CString*	pstrErrorText	-	Error text if the return
//										status is FALSE.
//	Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CNewRequest::IsTimeValid(CString strText, CTime* p_t, CString* strErrorTxt)
{
	CString strYear(strText.Mid(0,4));		// Extract the Year
	CString strGMTDay(strText.Mid(5,3));	// Extract the GMT Day
	CString strHour(strText.Mid(9,2));		// Extract the Hour
	CString strMin(strText.Mid(12,2));		// Extract the Minute
	CString strSec(strText.Mid(15,2));		// Extract the Second
	int		nYear		= _ttoi(strYear);
	int		nGMTDay		= _ttoi(strGMTDay);
	int		nHour		= _ttoi(strHour);
	int		nMin		= _ttoi(strMin);
	int		nSec		= _ttoi(strSec);
	BOOL	bOk			= TRUE;
	BOOL	bLeapYear	= (nYear % 4) == 0; // Good enough for the valid range
	
	// Make sure that the year field contain valid characters
	// Range checking will be done later
	if( nYear < 1971 || nYear > 2036 )
	{
		strErrorTxt->Format(_T("Invalid year <%s> specified, must be between 1971 and 2036."), strYear);
		bOk = FALSE;
	}

	// Make sure that the GMT day field contain valid characters
	// Range checking will be done later
	if( bOk )
	{
		if( nGMTDay < 0 || nGMTDay > (bLeapYear?366:365) )
		{
			strErrorTxt->Format(_T("Invalid GMT day <%s> specified, must be between 0 and 366."), strGMTDay);
			bOk = FALSE;
		}
	}

	// Make sure that the Hour field contain valid characters
	if( bOk )
	{
		if( nHour < 0 || nHour > 23 )
		{
			strErrorTxt->Format(_T("Invalid hour <%s> specified, must be between 0 and 23."), strHour);
			bOk = FALSE;
		}
	}

	if( bOk )
	{
		int nMon;
		int nDay;

		GMTDayToMonthDay(nGMTDay, nYear, &nMon, &nDay, bLeapYear);
		CTime t(nYear, nMon, nDay, nHour, nMin, nSec);
		*p_t = t;
	}
	
	return bOk;
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author :Ray Mosley			Date : 12/14/04			version 1.0
//////////////////////////////////////////////////////////////////////////
//
//	Function :		CNewRequest::GMTDayToMonthDay
//	Description :	Converts a GMT Day of Year to a Month/Day Pair.
//	Return :		
//		void		-	
//	Parameters :	
//		int nGMTDay				-	GMT Day of Year 0 - 365/366
//		int nYear				-	Year - 1971 - 2036
//		int* pnMonth			-	Converted month 1 (Jan) - 12 (Dec)
//		int* pnDay				-	Convert day of monthe 1 - 28/29/30/31
//		BOOL bLeapYear			-	Is this a leap year.
//	Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CNewRequest::GMTDayToMonthDay(int nGMTDay, int nYear, int* pnMonth, int* pnDay, BOOL bLeapYear)
{
	int nDaysInMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		
	if( bLeapYear )
		nDaysInMonth[1]++; // Increment Feb by one if necessary

	int nMonth = 0;
		
	while( nGMTDay > nDaysInMonth[nMonth] )
		nGMTDay -= nDaysInMonth[nMonth++];

	*pnMonth= nMonth+1;
	*pnDay	= nGMTDay;	
}

