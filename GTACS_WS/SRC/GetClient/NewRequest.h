#if !defined(AFX_NEWREQUEST_H__60A5B02E_169F_4CB6_B160_FA7C6925247D__INCLUDED_)
#define AFX_NEWREQUEST_H__60A5B02E_169F_4CB6_B160_FA7C6925247D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewRequest.h : header file
//

#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CNewRequest dialog

class CNewRequest : public CDialog
{
// Construction
public:
	CNewRequest(CWnd* pParent = NULL);   // standard constructor

	void SetRequestMsg(CPtrList* pList) { m_pMsgList = pList; }

// Dialog Data
	//{{AFX_DATA(CNewRequest)
	enum { IDD = IDD_NEW_REQUEST };
	COXNumericEdit	m_cMaxEvtEdit;
	CmEdit	m_cStopTimeEdit;
	CmEdit	m_cStrtTimeEdit;
	CString	m_strStrtTime;
	CString	m_strStopTime;
	CString	m_strFilename;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewRequest)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	BOOL IsSCSelected();
	BOOL IsStrmSelected();
	BOOL IsTimeValid(CString, CTime*, CString*);
	void GMTDayToMonthDay(int, int, int*, int*, BOOL);

	// Generated message map functions
	//{{AFX_MSG(CNewRequest)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnFilterButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CPtrList* m_pMsgList;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWREQUEST_H__60A5B02E_169F_4CB6_B160_FA7C6925247D__INCLUDED_)
