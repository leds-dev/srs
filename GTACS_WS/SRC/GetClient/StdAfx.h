// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__BD3E02BB_EF41_40D5_8E23_CBD0E96A59C7__INCLUDED_)
#define AFX_STDAFX_H__BD3E02BB_EF41_40D5_8E23_CBD0E96A59C7__INCLUDED_

// Must be compiled for WinNT 4.0 / Windows 2000 and Unicode
// sdm - beginning w/ VC++ 2008 valid values are 0x500=Win2k, 0x501=XP 
#define _WIN32_WINNT	0x0501

//#define _WIN32_WINNT 0x400
#define _UNICODE

#ifdef _DEBUG
#pragma message ("Compiling for WinNT/2000, Unicode, Debug")
#else
#pragma message ("Compiling for WinNT/2000, Unicode, Release")
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxadv.h>
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#include <afxcview.h>
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#include <afxsock.h>		// MFC socket extensions
#include <afximpl.h>


//Other library includes
#include <lmcons.h>
#include <BCGCBProInc.h>			// BCG Control Bar
#include <CodeMax.h>
#include <CMaxAfx.h>

#define GET_GUID				_T("GET-{44E678F7-DA79-11d3-9FE9-006067718D04}")
#define UNIQUE_TO_SYSTEM		0
#define UNIQUE_TO_DESKTOP		1
#define UNIQUE_TO_SESSION		2
#define UNIQUE_TO_TRUSTEE		4

const UINT	ctnPortNum			= 15901;

const TCHAR ctstrSHARE_NAME[]	= _T("\\$GTACS_ROOT");
const TCHAR ctstrRPT_RMT_DIR[]	= _T("\\EPOCH_OUTPUT\\get\\rpt\\");
const TCHAR ctstrEPOCH_OUT[]	= _T("EPOCH_OUT");
const TCHAR ctstrPROCESS_ID[]	= _T("GET");
const TCHAR ctstrPROCESS_TITLE[]= _T("GTACS EVENT TOOL - [GET]");
const TCHAR ctstrRPT_LOC_DIR[]	= _T("\\GET Client");
const TCHAR ctstrFILTER_FLAGS[] = _T("Filter files|*.flt||");
const TCHAR ctstrTEXT_EXT1[]	= _T("---------1---------2---------3---------4---------5---------6---------7---------8-");
const TCHAR ctstrTEXT_EXT2[]	= _T("---------1---------2---------3---------4---------5---------6---------7---------8---------9---------0---------A---------B---------C---");
// const TCHAR ctstrIE[]			= _T("C:\\Program Files\\Internet Explorer\\IEXPLORE.EXE");
const TCHAR ctstrHlpPage[]		= _T("C:\\Program Files (x86)\\Integral Systems\\GET Client\\Help\\GET.htm");
// const TCHAR ctstrIEdir[]			= _T("C:\\Program Files (x86)\\Integral Systems\\GET Client\\");
// const TCHAR ctstrHlpPage[]		= _T("..\\Help\\GET.htm");

#include "OXEdit.h"
#include "OXUNC.h"
#include "OXCmdOpt.h"
#include "OXSplashWnd.h"
#include "mEdit.h"
#include "atlbase.h"
#include "GETMessage.h"
#include "GETComm.h"
#include "GETMultiDocTemplate.h"

static const UINT UWM_NEW_INSTANCE = ::RegisterWindowMessage(_T("UWM_NEW_INSTANCE"));

#endif // _AFX_NO_AFXCMN_SUPPORT


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__BD3E02BB_EF41_40D5_8E23_CBD0E96A59C7__INCLUDED_)
