#if !defined(AFX_TIMEOUTSOCKET_H__7D8B9C0B_8546_4110_81A0_097CF06E052D__INCLUDED_)
#define AFX_TIMEOUTSOCKET_H__7D8B9C0B_8546_4110_81A0_097CF06E052D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TimeOutSocket.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTimeOutSocket command target

class CTimeOutSocket : public CSocket
{
// Attributes
public:
	BOOL		HadTimeOut() const	{ return m_bTimeOut; }
	BOOL		SetTimeOutVal(UINT);
	void		InitializeTimer();
	virtual int	Receive(void* lpBuf, int nBufLen, int nFlags=0);
	virtual int	Send(const void* lpBuf, int nBufLen, int nFlags=0);

private:
	int			SendChunk(const void* lpBuf, int nBufLen, int nFlags);
	BOOL		CheckTimer();

	CTime		m_initTime;
	BOOL		m_bTimeOut;
	int			m_nTOValue;

// Operations
public:
	CTimeOutSocket();
	virtual ~CTimeOutSocket();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimeOutSocket)
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CTimeOutSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TIMEOUTSOCKET_H__7D8B9C0B_8546_4110_81A0_097CF06E052D__INCLUDED_)
