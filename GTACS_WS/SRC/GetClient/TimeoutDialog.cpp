/**
/////////////////////////////////////////////////////////////////////////////
// TimeoutDialog.cpp : implementation of the TimeoutDialog class.          //
// (c) 2004 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// TimeoutDialog.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is the interface for the Timeout dialog.
//
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "GET.h"
#include "TimeoutDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTimeoutDialog dialog


/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 11/21/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeoutDialog::CTimeoutDialog
//	Description :	Constructor
//	Return :		constructor
//	Parameters :
//		CWnd* pParent	-	Parent Window
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CTimeoutDialog::CTimeoutDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CTimeoutDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTimeoutDialog)
	m_nTimeoutEdit = 2;
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 11/21/04		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CTimeoutDialog::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is
//					generated and maintained by the code wizard.
//  Returns :		void	-
//  Parameters :
//           CDataExchange* pDX
//  Note :
//////////////////////////////////////////////////////////////////////////
**/
void CTimeoutDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTimeoutDialog)
	DDX_Control(pDX, IDC_TIMEOUT_SPIN, m_cTimeoutSpin);
	DDX_Text(pDX, IDC_TIMEOUT_EDIT, m_nTimeoutEdit);
	DDV_MinMaxInt(pDX, m_nTimeoutEdit, 1, 720);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CTimeoutDialog, CDialog)
	//{{AFX_MSG_MAP(CTimeoutDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTimeoutDialog message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 11/21/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeoutDialog::OnTimeoutCheck
//  Description :
//	Return :		void	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CTimeoutDialog::OnTimeoutCheck()
{
//	GetDlgItem(IDC_TIMEOUT_STATIC)->EnableWindow(bEnable);
//	GetDlgItem(IDC_TIMEOUT_EDIT)->EnableWindow(bEnable);
//	GetDlgItem(IDC_TIMEOUT_SPIN)->EnableWindow(bEnable);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 11/21/04		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeoutDialog::OnInitDialog
//  Description :	This routine create the Timeout dialog window.
//	Return :		BOOL	-	TRUE if successful.
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CTimeoutDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_cTimeoutSpin.SetRange(1, 720);
	m_cTimeoutSpin.SetPos(m_nTimeoutEdit);

	OnTimeoutCheck();

	return TRUE;
}
