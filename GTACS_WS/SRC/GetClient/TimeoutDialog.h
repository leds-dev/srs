#if !defined(AFX_TIMEOUTDIALOG_H__17A36DA5_0F4C_11D5_9A65_0003472193C8__INCLUDED_)
#define AFX_TIMEOUTDIALOG_H__17A36DA5_0F4C_11D5_9A65_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TimeoutDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTimeoutDialog dialog

#define CDialog CBCGPDialog

class CTimeoutDialog : public CDialog
{
// Construction
public:
	CTimeoutDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTimeoutDialog)
	enum { IDD = IDD_TIMEOUT_DIALOG };
	CSpinButtonCtrl	m_cTimeoutSpin;
	int		m_nTimeoutEdit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimeoutDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CTimeoutDialog)
	afx_msg void OnTimeoutCheck();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TIMEOUTDIALOG_H__17A36DA5_0F4C_11D5_9A65_0003472193C8__INCLUDED_)
