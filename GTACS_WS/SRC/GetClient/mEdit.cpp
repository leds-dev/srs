/***************************
//////////////////////////////////////////////////////////////////////
// mEdit.cpp : implementation file
//
// (c) 2001 Frederick J. Shaw                                       // 
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
***************************/

/////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "mEdit.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CmEdit

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		DDX_CmEdit
//	Description :	The DDX_Control function manages the transfer of data 
//					between a subclassed control in a dialog box, form view,
//					or control view object and a CWnd data member of the 
//					dialog box, form view, or control view object.
//					
//	Return :		
//					void	-	none
//	Parameters :	
//		CDataExchange	-	A pointer to a CDataExchange object. 
//							The framework supplies this object to establish
//							the context of the data exchange, including its
//							direction.
//
//		int				-	The resource ID of the subclassed control
//							associated with the control property.
//
//		CmEdit&			-	A reference to a member variable of the dialog
//							box, form view, or control view object with
//							which data is exchanged.
//
//		CString&		-	Data string		
//		
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void AFXAPI DDX_CmEdit(CDataExchange* pDX, int nIDC, CmEdit& rControl, CString& data)
{
    DDX_Control(pDX, nIDC, (CWnd&)rControl);
    if (!pDX->m_bSaveAndValidate)
    {
        rControl.SetData(data);
    }
    else
    {
        data = rControl.GetData();
    }
}

IMPLEMENT_DYNCREATE(CmEdit, CEdit)

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::CmEdit()
//	Description :	Constructor
//					
//	Return :		
//				void	-	none
//	Parameters :	
//				 void	-	none	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CmEdit::CmEdit()
  : m_bInit(false),
    m_strPrompt("_"),
    m_iNumChars(0),
    m_iCurrentChar(0),
    m_bShiftDown(false)
{
    for (int i = 0; i < MAX_CHARS; i++)
        m_Char[i] = NULL;

    //SubclassWindow(pParent->m_hWnd);
    //pParent->SubclassWindow(this->m_hWnd);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::~CmEdit()
//	Description :	Destructor
//					
//	Return :		
//				void	-	none
//	Parameters :	
//				 void	-	none	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CmEdit::~CmEdit()
{
    for (int i = 0; i < MAX_CHARS; i++)
        if (m_Char[i] != NULL)
        {
            delete m_Char[i];
            m_Char[i] = NULL;
        }
}


BEGIN_MESSAGE_MAP(CmEdit, CEdit)
	//{{AFX_MSG_MAP(CmEdit)
	ON_WM_CHAR()
	ON_WM_KEYDOWN()
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_SETFOCUS()
	ON_WM_LBUTTONUP()
	ON_WM_KEYUP()
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CmEdit message handlers
/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::OnCreate
//	Description :	The framework calls this member function when an
//					application requests that the Windows window be created
//					by calling the Create or CreateEx member function.
//					
//	Return :		
//				int		0	-	continue the creation of the CWnd object.
//						-1	- the window will be destroyed.
//
//	Parameters :	
//				 LPCREATESTRUCT Pointer to a CREATESTRUCT structure 
//								that contains information about the
//								CWnd object being created.
//
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
int CmEdit::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CEdit::OnCreate(lpCreateStruct) == -1)
		return -1;
	
    return 0;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::OnDestroy()
//	Description :	The framework calls this member function to inform the 
//					CWnd object that it is being destroyed. OnDestroy is
//					called after the CWnd object is removed from the screen. 
//					
//	Return :		
//				void - none.
//						
//	Parameters :	
//				 void -	none 
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CmEdit::OnDestroy() 
{
	CEdit::OnDestroy();
}


/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::PreCreateWindow 
//	Description :	Called by the framework before the creation of the 
//					Windows window attached to this CWnd object. 
//					
//	Return :	BOOL	-	
//				Nonzero if the window creation should continue; 
//				0 to indicate creation failure.
//						
//	Parameters :	
//				 CREATESTRUCT& - A CREATESTRUCT structure. 
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CmEdit::PreCreateWindow(CREATESTRUCT& cs) 
{
    cs.style |= WS_TABSTOP | WS_VISIBLE | WS_DISABLED | ES_AUTOHSCROLL;
    cs.dwExStyle |= WS_EX_OVERLAPPEDWINDOW;
    
	return CEdit::PreCreateWindow(cs);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::SetData(const CString& data)
//	Description :	 
//					
//	Return :		
//				void - none.
//						
//	Parameters :	
//				 const CString&	-	address of the string to store in the
//									member variable
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CmEdit::SetData(const CString& data)
{
    CString strWindowText;
    CString strData;
    int     iNextDataChar = 0;
    
    for (int i = 1; i <= m_iNumChars; i++)
    {
        if (m_Char[i - 1]->m_bStaticChar)
        {
            strWindowText += m_Char[i - 1]->m_strValids;
        }
        else
        {
            if (iNextDataChar < data.GetLength())
            {
                strData = data[iNextDataChar++];
                if (m_Char[i - 1]->m_strValids.Find(strData[0]) < 0)
                    strData = m_strPrompt;
            }
            else
            {
                strData = m_strPrompt;
            }
            
            strWindowText += strData[0];
        }
    }

    SetWindowText(strWindowText);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::GetData()
//	Description :	 
//					
//					 
//					
//	Return :		
//				CString	-	The text string retrieved.	
//						
//	Parameters :	
//				 void -	none 
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CmEdit::GetData()
{
    CString strWindowText;
    GetWindowText(strWindowText);
    CString strData;
    
    for (int i = 1; i <= m_iNumChars; i++)
    {
        if (!m_Char[i - 1]->m_bStaticChar)
        {
            if (strWindowText.GetLength() >= i)
            {
                if (strWindowText[i - 1] != m_strPrompt)
                    strData += strWindowText[i - 1];
            }
            else
                strData += m_strPrompt;
        }
    }

    return strData;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		OnChar 
//	Description :	The framework calls this member function when a 
//					keystroke translates to a nonsystem character.  
//					
//	Return :		
//				void - none.
//						
//	Parameters :	
//				UINT -	Specifies the virtual-key code of the given key.
//				UINT -	Repeat count (the number of times the keystroke
//						is repeated as a result of the user holding down
//						the key).
//				UINT -	Specifies the scan code, key-transition code,
//						 previous key state, and context code.
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CmEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    switch (nChar)
    {
    case VK_TAB:
                //(((CWnd*)GetParent())->GetNextDlgTabItem(this, m_bShiftDown))->SetFocus();
                break;
    
    case VK_BACK:
                if (m_iCurrentChar > 0)
                {
                    int iNewNext = FindNextChar(m_iCurrentChar - 1, false, false);
                    if (iNewNext >= 0)
                    {
                        SetSel(iNewNext, iNewNext + 1);
                        ReplaceSel(m_strPrompt);
                        m_iCurrentChar = iNewNext;
                        SetSel(m_iCurrentChar, m_iCurrentChar);
                    }
                }
                break;

    default:    
                int x, y, z;
                GetSel(x,y);
                if (x != y)
                {
                    CString szReplaceText = "";
                    for (z = x; z < y; z++)
                    {
                        if (m_Char[z]->m_bStaticChar)
                        {
                            szReplaceText += m_Char[z]->m_strValids;
                        }
                        else
                        {
                            szReplaceText += m_strPrompt;
                        }
                    }
                    ReplaceSel(szReplaceText);
                    m_iCurrentChar = FindNextChar(x, false, true);
                    SetSel(m_iCurrentChar, m_iCurrentChar);
                }
        
        
                if (m_iCurrentChar < m_iNumChars)
                {
                    if ( m_Char[m_iCurrentChar]->m_strValids.Find((TCHAR)nChar) >= 0 )
                    {
                        int iNewNext = FindNextChar(m_iCurrentChar + 1, false, true);
                        SetSel(m_iCurrentChar, m_iCurrentChar + 1);
                        ReplaceSel((CString)(char)nChar);
                        if (iNewNext >= 0)
                        {
                            m_iCurrentChar = iNewNext;
                        }
                        else
                        {
                            m_iCurrentChar++;
                        }
                        SetSel(m_iCurrentChar, m_iCurrentChar);
                    }
					else
						MessageBeep(0xFFFFFFFF);
                }
				else
					MessageBeep(0xFFFFFFFF);

                break;
    }

    //CEdit::OnChar(nChar, nRepCnt, nFlags);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::OnKeyUp
//	Description :	The framework calls this member function when a
//					nonsystem key is released. 
//					
//	Return :		
//				void - none.
//						
//	Parameters :	
//				UINT -	Specifies the virtual-key code of the given key.
//				UINT -	Repeat count (the number of times the keystroke
//						is repeated as a result of the user holding down
//						the key).
//				UINT -	Specifies the scan code, key-transition code,
//						 previous key state, and context code.
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CmEdit::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    switch (nChar)
    {
    case VK_SHIFT:
                m_bShiftDown = false;
                break;
    
    default:
                break;
    }
    
	CEdit::OnKeyUp(nChar, nRepCnt, nFlags);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::OnKeyDown
//	Description :	The framework calls this member function when a
//					nonsystem key is pressed. 
//					
//	Return :		
//				void - none.
//						
//	Parameters :	
//				UINT -	Specifies the virtual-key code of the given key.
//				UINT -	Repeat count (the number of times the keystroke
//						is repeated as a result of the user holding down
//						the key).
//				UINT -	Specifies the scan code, key-transition code,
//						 previous key state, and context code.
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CmEdit::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    int iNext = 0;
    CWnd* pNextItem = NULL;
    
    switch (nChar)
    {
    // Tab key
    case VK_TAB:
                pNextItem = (((CWnd*)GetParent())->GetNextDlgTabItem(this, (m_bShiftDown) ? TRUE : FALSE));
                if (pNextItem->IsKindOf(RUNTIME_CLASS(CmEdit)))
                    ((CmEdit*)pNextItem)->m_bShiftDown = m_bShiftDown;
                (((CWnd*)GetParent())->GetNextDlgTabItem(this, (m_bShiftDown) ? TRUE : FALSE))->SetFocus();
                break;
        
    // Shift key
    case VK_SHIFT:
                m_bShiftDown = true;
                break;
        
    // Left arrow key
    case VK_LEFT:
                iNext = FindNextChar(m_iCurrentChar - 1, false, false);
                if (iNext >= 0)
                {
                    m_iCurrentChar = iNext;
                    SetSel(m_iCurrentChar, m_iCurrentChar);
                }
                break;
    
    // Right arrow key
    case VK_RIGHT:
                iNext = FindNextChar(m_iCurrentChar + 1, false, true);
                if (iNext >= 0)
                    m_iCurrentChar = iNext;
                else
                {
                    m_iCurrentChar = FindNextChar(m_iNumChars - 1, false, false) + 1;
                }
                SetSel(m_iCurrentChar, m_iCurrentChar);
                break;

    // Home key
    case VK_HOME:
                if (m_bShiftDown)
                {
                    SetSel(0, m_iCurrentChar);
                }
                else
                {
                    m_iCurrentChar = FindNextChar(0, false, true);
                    SetSel(m_iCurrentChar, m_iCurrentChar);
                }
                break;

    // End key
    case VK_END:
                if (m_bShiftDown)
                {
                    int iLast = FindNextChar(m_iNumChars - 1, false, false) + 1;
                    SetSel(m_iCurrentChar, iLast);
                }
                else
                {
                    m_iCurrentChar = FindNextChar(m_iNumChars - 1, false, false) + 1;
                    SetSel(m_iCurrentChar, m_iCurrentChar);
                }
                break;

    // Insert key
    case VK_INSERT:

                break;

    // Delete key
    case VK_DELETE:
                int x, y, z;
                GetSel(x,y);
                if (x != y)
                {
                    CString szReplaceText = "";
                    for (z = x; z < y; z++)
                    {
                        if (m_Char[z]->m_bStaticChar)
                        {
                            szReplaceText += m_Char[z]->m_strValids;
                        }
                        else
                        {
                            szReplaceText += m_strPrompt;
                        }
                    }
                    ReplaceSel(szReplaceText);
                    m_iCurrentChar = FindNextChar(x, false, true);
                    SetSel(m_iCurrentChar, m_iCurrentChar);
                }
                else if (m_iCurrentChar <= FindNextChar(m_iNumChars - 1, false, false))
                {
                    SetSel(m_iCurrentChar, m_iCurrentChar + 1);
                    ReplaceSel(m_strPrompt);
                    iNext = FindNextChar(m_iCurrentChar + 1, false, true);
                    if (iNext >= 0)
                        m_iCurrentChar = iNext;
                    else
                        m_iCurrentChar++;
                    SetSel(m_iCurrentChar, m_iCurrentChar);
                }
                break;

    default:
                break;

    }

    //CEdit::OnKeyDown(nChar, nRepCnt, nFlags);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::Init
//	Description :	 
//					
//					 
//					
//	Return :	bool	
//					always returns true	
//					
//						
//	Parameters :	
//				 CString strMask	-	
//				 CString strInitialData -	
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
bool CmEdit::Init(CString strMask, CString strInitialData)
{
    m_bInit = false;
    
    if (!Parse(strMask))
    {
        SetWindowText(_T("Init failed."));
        EnableWindow(FALSE);
        return false;
    }

    if (!SetValidChars(strMask))
    {
        SetWindowText(_T("SetValidChars failed."));
        EnableWindow(FALSE);
        return false;                          
    }
    
    SetData(strInitialData);
    m_iCurrentChar = FindNextChar(0, false, true);
    this->SetFocus();
    SetSel(m_iCurrentChar, m_iCurrentChar);

    EnableWindow(TRUE);  // this line is important!
    m_bInit = true;

    return true;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		int CmEdit::FindNextChar
//	Description :	 
//					
//					 
//					
//	Return :	int	
//				-1	-	
//				index	-
//						
//	Parameters :	
//				 int iStartPos	-	
//				 bool bStatic	-	
//				 bool bForward	-
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
int CmEdit::FindNextChar(int iStartPos, bool bStatic, bool bForward)
{
    int iIndex = 0;
    int iReturnVal = -1;
    
    if (bForward)
    {
        for (int iIndex = iStartPos; iIndex < m_iNumChars; iIndex++)
        {
            if (m_Char[iIndex]->m_bStaticChar == bStatic)
            {
                iReturnVal = iIndex;
                break;
            }
        }
    }
    else
    {
        for (int iIndex = iStartPos; iIndex >= 0; iIndex--)
        {
            if (m_Char[iIndex]->m_bStaticChar == bStatic)
            {
                iReturnVal = iIndex;
                break;
            }
        }
    }

    return iReturnVal;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		bool CmEdit::SetValidChars
//	Description :	 
//					
//					 
//					
//	Return :	bool	
//				Always true
//						
//	Parameters :	
//				 const CString&  
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
bool CmEdit::SetValidChars(const CString& strMask)
{
    CString strChars;
    CString strRangeLow; 
    bool    bInside       = false;
    bool    bInsideEscape = false;
    bool    bInsideRange  = false;
    int     iNextChar     = 0;
    int     i;
    
    // Clear valid chars
    for (i = 0; i < MAX_CHARS; i++)
        if (m_Char[i] != NULL)
        {
            delete m_Char[i];
            m_Char[i] = NULL;
        }

    // Walk through mask string
    for (i = 0; i < strMask.GetLength(); i++)
    {
        switch (strMask[i])
        {
        case '[':   bInside = true;
                    break;            //done

        case ']':   bInside = false;
                    m_Char[iNextChar++] = new CmEditChar(strChars, false);
                    strChars = "";
                    break;

        case '-':   if (bInside)
                    {
                        if (bInsideEscape)
                        {
                            strChars += "-";
                            bInsideEscape = false;  //done
                        }
                        else
                        {
                            if (strChars.GetLength() == 0)
                                return false;
                            bInsideRange = true;
                            strRangeLow = strChars.Right(1);
                            strChars = strChars.Left(strChars.GetLength() - 1); //done
                        }
                    }
                    else
                    {
                        m_Char[iNextChar++] = new CmEditChar("-", true); //done
                    }
                    break;

        case '\\':  if (bInside)
                    {
                        if (!bInsideEscape)
                        {
                            bInsideEscape = true;  //done
                        }
                        else
                        {
                            if (bInsideRange)
                            {
                                strChars += GetRange(strRangeLow, "\\");
                                strRangeLow = "";
                                bInsideRange = false;  //done
                            }
                            else
                            {
                                strChars += "\\";
                                bInsideEscape = false;  //done
                            }
                        }
                    }
                    else
                    {
                        m_Char[iNextChar++] = new CmEditChar("\\", true); //done
                    }
                    break;

        default:    if (bInside)
                    {
                        if (bInsideEscape)
                        {
                            bInsideEscape = false;        //done
                        }

                        if (bInsideRange)
                        {
                            strChars += GetRange(strRangeLow, strMask[i]);
                            strRangeLow = "";
                            bInsideRange = false;   //done
                        }
                        else
                        {
                            strChars += strMask[i]; //done
                        }
                    }
                    else
                    {
                        m_Char[iNextChar++] = new CmEditChar(strMask[i], true); //done
                    }
                    break;
        }
    }

    m_iNumChars = iNextChar;
    SetLimitText(m_iNumChars);
    
    return true;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::GetRange
//	Description :	 
//					
//					 
//					
//	Return :		
//				CString 
//						
//	Parameters :	
//				 CString szLow	-	
//				 CString szHigh	-	
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CmEdit::GetRange(CString szLow, CString szHigh)
{
    CString szReturn = _T("");

    if (szLow.GetLength() == 1 && szHigh.GetLength() == 1)
    {
        _TCHAR first = szLow[0];
        _TCHAR last  = szHigh[0];

        for (int i = first; i <= last; i++)
            szReturn += (CString)(char)i;
    }

    return szReturn;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::Parse
//	Description :	This routine parses the text line passed in. 
//					
//	Return :	bool	
//					true	-	succes.
//					false	-	fail.
//	
//	Parameters :	
//				 CString	- string to parse. 
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
bool CmEdit::Parse(const CString& strMask)
{
    bool bValid = true;
    int iBracketState = 0;
    //int iDashState = 0;
    
    for (int i = 1; i <= strMask.GetLength(); i++)
    {
        switch (strMask[i - 1])
        {
        case '[':   iBracketState++; break;

        case ']':   iBracketState--; break;

        //case '-':   if (iBracketState > 0)
        //                iDashState = 1;
        //            break;

        default:    //if (iBracketState > 0) && iDashState == 1)
                    //    iDashState = 0;
                    break;
        }

        // Make sure the [ ] are in sync
        if (iBracketState < 0 || iBracketState > 1)
            bValid = false;

        // Make sure there is a character after a '-' inside the [ ]
        //if (iBracketState == 0 && iDashState == 1)
        //    bValid = false;
    }

    return (iBracketState == 0 /*&& iDashState == 0*/ && bValid) ? true : false;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::SetPromptChar
//	Description :	This routine sets the character to be used for the prompt. 
//					
//					
//	Return :	bool	
//					true	- succes
//					false	-	fail.
//						
//	Parameters :	
//				 CString	the character to use as the prompt. 
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
bool CmEdit::SetPromptChar(CString strPromptChar)
{
    if (strPromptChar.GetLength() != 1)
        return false;

    m_strPrompt = strPromptChar;
    return true;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::OnDestroy()
//	Description :	Checks that the control is in-place active and has a
//					valid control site, then informs the container the
//					control has gained focus.
//
//	Return :		
//				void - none.
//						
//	Parameters :	
//				 CWnd* -	Contains the CWnd object that loses
//							the input focus (may be NULL). 
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CmEdit::OnSetFocus(CWnd* pOldWnd) 
{
	CEdit::OnSetFocus(pOldWnd);
	
    //SetSel(m_iCurrentChar, m_iCurrentChar);
    SetSel(0, -1);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::OnLButtonUp
//	Description :	The framework calls this member function when the user 
//					releases the left mouse button. 
//	Return :		
//				void - none.
//						
//	Parameters :	
//				UINT   -	Indicates whether various virtual keys are up.
//				CPoint -	Specifies the x- and y-coordinate of the cursor.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CmEdit::OnLButtonUp(UINT nFlags, CPoint point) 
{
	int x, y;
    GetSel(x, y);

    if (x == y)
    {
        m_iCurrentChar = x;
        SetSel(m_iCurrentChar, m_iCurrentChar);
    }
	
	CEdit::OnLButtonUp(nFlags, point);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CmEdit::OnLButtonDblClk
//	Description :	The framework calls this member function when the user 
//					double-clicks the left mouse button. 
//	Return :		
//				void - none.
//						
//	Parameters :	
//				UINT   -	Indicates whether various virtual keys are down.
//				CPoint -	Specifies the x- and y-coordinate of the cursor.
//					
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CmEdit::OnLButtonDblClk(UINT nFlags, CPoint point)
{
    SetSel(0, -1);
}
