# Microsoft Developer Studio Generated NMAKE File, Based on Codemax.dsp
!IF "$(CFG)" == ""
CFG=codemax - Win32 Release
!MESSAGE No configuration specified. Defaulting to codemax - Win32 Release.
!ENDIF 

!IF "$(CFG)" != "codemax - Win32 Release" && "$(CFG)" != "codemax - Win32 Debug" && "$(CFG)" != "codemax - Win32 Unicode Release" && "$(CFG)" != "codemax - Win32 Unicode Debug" && "$(CFG)" != "codemax - Win32 ActiveX Debug" && "$(CFG)" != "codemax - Win32 ActiveX Release" && "$(CFG)" != "codemax - Win32 Profile"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Codemax.mak" CFG="codemax - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "codemax - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "codemax - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "codemax - Win32 Unicode Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "codemax - Win32 Unicode Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "codemax - Win32 ActiveX Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "codemax - Win32 ActiveX Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "codemax - Win32 Profile" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "codemax - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\cmax20.dll"


CLEAN :
	-@erase "$(INTDIR)\bookmark.obj"
	-@erase "$(INTDIR)\buffer.obj"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\ceditmsg.obj"
	-@erase "$(INTDIR)\ceditx.obj"
	-@erase "$(INTDIR)\cmdarry.obj"
	-@erase "$(INTDIR)\Codemax.pch"
	-@erase "$(INTDIR)\codemax.res"
	-@erase "$(INTDIR)\ctlstate.obj"
	-@erase "$(INTDIR)\debug.obj"
	-@erase "$(INTDIR)\dragdrop.obj"
	-@erase "$(INTDIR)\editcmd1.obj"
	-@erase "$(INTDIR)\editdlgs.obj"
	-@erase "$(INTDIR)\editsel.obj"
	-@erase "$(INTDIR)\editview.obj"
	-@erase "$(INTDIR)\exports.obj"
	-@erase "$(INTDIR)\findrepl.obj"
	-@erase "$(INTDIR)\globalsx.obj"
	-@erase "$(INTDIR)\hkeyctrl.obj"
	-@erase "$(INTDIR)\hotkey.obj"
	-@erase "$(INTDIR)\hotkeyx.obj"
	-@erase "$(INTDIR)\language.obj"
	-@erase "$(INTDIR)\langx.obj"
	-@erase "$(INTDIR)\line.obj"
	-@erase "$(INTDIR)\macro.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\msgpump.obj"
	-@erase "$(INTDIR)\posx.obj"
	-@erase "$(INTDIR)\precomp.obj"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\props.obj"
	-@erase "$(INTDIR)\rangex.obj"
	-@erase "$(INTDIR)\rectx.obj"
	-@erase "$(INTDIR)\regexp.obj"
	-@erase "$(INTDIR)\selftest.obj"
	-@erase "$(INTDIR)\splitter.obj"
	-@erase "$(INTDIR)\syntax.obj"
	-@erase "$(INTDIR)\undo.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\cmax20.dll"
	-@erase "$(OUTDIR)\cmax20.exp"
	-@erase "$(OUTDIR)\cmax20.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /ML /W4 /GX /O2 /D "NDEBUG" /D "_WINDOWS" /D "WIN32" /D "_MBCS" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\codemax.res" /d "NDEBUG" /d "_MBCS" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Codemax.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib version.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\cmax20.pdb" /machine:I386 /def:".\codemax.def" /out:"$(OUTDIR)\cmax20.dll" /implib:"$(OUTDIR)\cmax20.lib" 
DEF_FILE= \
	".\codemax.def"
LINK32_OBJS= \
	"$(INTDIR)\bookmark.obj" \
	"$(INTDIR)\buffer.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\ceditmsg.obj" \
	"$(INTDIR)\ceditx.obj" \
	"$(INTDIR)\cmdarry.obj" \
	"$(INTDIR)\ctlstate.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\dragdrop.obj" \
	"$(INTDIR)\editcmd1.obj" \
	"$(INTDIR)\editdlgs.obj" \
	"$(INTDIR)\editsel.obj" \
	"$(INTDIR)\editview.obj" \
	"$(INTDIR)\exports.obj" \
	"$(INTDIR)\findrepl.obj" \
	"$(INTDIR)\globalsx.obj" \
	"$(INTDIR)\hkeyctrl.obj" \
	"$(INTDIR)\hotkey.obj" \
	"$(INTDIR)\hotkeyx.obj" \
	"$(INTDIR)\language.obj" \
	"$(INTDIR)\langx.obj" \
	"$(INTDIR)\line.obj" \
	"$(INTDIR)\macro.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\msgpump.obj" \
	"$(INTDIR)\posx.obj" \
	"$(INTDIR)\precomp.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\props.obj" \
	"$(INTDIR)\rangex.obj" \
	"$(INTDIR)\rectx.obj" \
	"$(INTDIR)\regexp.obj" \
	"$(INTDIR)\selftest.obj" \
	"$(INTDIR)\splitter.obj" \
	"$(INTDIR)\syntax.obj" \
	"$(INTDIR)\undo.obj" \
	"$(INTDIR)\codemax.res"

"$(OUTDIR)\cmax20.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\cmax20.dll" "$(OUTDIR)\Codemax.bsc"


CLEAN :
	-@erase "$(INTDIR)\bookmark.obj"
	-@erase "$(INTDIR)\bookmark.sbr"
	-@erase "$(INTDIR)\buffer.obj"
	-@erase "$(INTDIR)\buffer.sbr"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\cedit.sbr"
	-@erase "$(INTDIR)\ceditmsg.obj"
	-@erase "$(INTDIR)\ceditmsg.sbr"
	-@erase "$(INTDIR)\ceditx.obj"
	-@erase "$(INTDIR)\ceditx.sbr"
	-@erase "$(INTDIR)\cmdarry.obj"
	-@erase "$(INTDIR)\cmdarry.sbr"
	-@erase "$(INTDIR)\Codemax.pch"
	-@erase "$(INTDIR)\codemax.res"
	-@erase "$(INTDIR)\ctlstate.obj"
	-@erase "$(INTDIR)\ctlstate.sbr"
	-@erase "$(INTDIR)\debug.obj"
	-@erase "$(INTDIR)\debug.sbr"
	-@erase "$(INTDIR)\dragdrop.obj"
	-@erase "$(INTDIR)\dragdrop.sbr"
	-@erase "$(INTDIR)\editcmd1.obj"
	-@erase "$(INTDIR)\editcmd1.sbr"
	-@erase "$(INTDIR)\editdlgs.obj"
	-@erase "$(INTDIR)\editdlgs.sbr"
	-@erase "$(INTDIR)\editsel.obj"
	-@erase "$(INTDIR)\editsel.sbr"
	-@erase "$(INTDIR)\editview.obj"
	-@erase "$(INTDIR)\editview.sbr"
	-@erase "$(INTDIR)\exports.obj"
	-@erase "$(INTDIR)\exports.sbr"
	-@erase "$(INTDIR)\findrepl.obj"
	-@erase "$(INTDIR)\findrepl.sbr"
	-@erase "$(INTDIR)\globalsx.obj"
	-@erase "$(INTDIR)\globalsx.sbr"
	-@erase "$(INTDIR)\hkeyctrl.obj"
	-@erase "$(INTDIR)\hkeyctrl.sbr"
	-@erase "$(INTDIR)\hotkey.obj"
	-@erase "$(INTDIR)\hotkey.sbr"
	-@erase "$(INTDIR)\hotkeyx.obj"
	-@erase "$(INTDIR)\hotkeyx.sbr"
	-@erase "$(INTDIR)\language.obj"
	-@erase "$(INTDIR)\language.sbr"
	-@erase "$(INTDIR)\langx.obj"
	-@erase "$(INTDIR)\langx.sbr"
	-@erase "$(INTDIR)\line.obj"
	-@erase "$(INTDIR)\line.sbr"
	-@erase "$(INTDIR)\macro.obj"
	-@erase "$(INTDIR)\macro.sbr"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\misc.sbr"
	-@erase "$(INTDIR)\msgpump.obj"
	-@erase "$(INTDIR)\msgpump.sbr"
	-@erase "$(INTDIR)\posx.obj"
	-@erase "$(INTDIR)\posx.sbr"
	-@erase "$(INTDIR)\precomp.obj"
	-@erase "$(INTDIR)\precomp.sbr"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\print.sbr"
	-@erase "$(INTDIR)\props.obj"
	-@erase "$(INTDIR)\props.sbr"
	-@erase "$(INTDIR)\rangex.obj"
	-@erase "$(INTDIR)\rangex.sbr"
	-@erase "$(INTDIR)\rectx.obj"
	-@erase "$(INTDIR)\rectx.sbr"
	-@erase "$(INTDIR)\regexp.obj"
	-@erase "$(INTDIR)\regexp.sbr"
	-@erase "$(INTDIR)\selftest.obj"
	-@erase "$(INTDIR)\selftest.sbr"
	-@erase "$(INTDIR)\splitter.obj"
	-@erase "$(INTDIR)\splitter.sbr"
	-@erase "$(INTDIR)\syntax.obj"
	-@erase "$(INTDIR)\syntax.sbr"
	-@erase "$(INTDIR)\undo.obj"
	-@erase "$(INTDIR)\undo.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\cmax20.dll"
	-@erase "$(OUTDIR)\cmax20.exp"
	-@erase "$(OUTDIR)\cmax20.lib"
	-@erase "$(OUTDIR)\cmax20.pdb"
	-@erase "$(OUTDIR)\Codemax.bsc"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W4 /Gm /GX /Zi /Od /D "_DEBUG" /D "_WINDOWS" /D "WIN32" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\codemax.res" /d "_DEBUG" /d "_MBCS" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Codemax.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\bookmark.sbr" \
	"$(INTDIR)\buffer.sbr" \
	"$(INTDIR)\cedit.sbr" \
	"$(INTDIR)\ceditmsg.sbr" \
	"$(INTDIR)\ceditx.sbr" \
	"$(INTDIR)\cmdarry.sbr" \
	"$(INTDIR)\ctlstate.sbr" \
	"$(INTDIR)\debug.sbr" \
	"$(INTDIR)\dragdrop.sbr" \
	"$(INTDIR)\editcmd1.sbr" \
	"$(INTDIR)\editdlgs.sbr" \
	"$(INTDIR)\editsel.sbr" \
	"$(INTDIR)\editview.sbr" \
	"$(INTDIR)\exports.sbr" \
	"$(INTDIR)\findrepl.sbr" \
	"$(INTDIR)\globalsx.sbr" \
	"$(INTDIR)\hkeyctrl.sbr" \
	"$(INTDIR)\hotkey.sbr" \
	"$(INTDIR)\hotkeyx.sbr" \
	"$(INTDIR)\language.sbr" \
	"$(INTDIR)\langx.sbr" \
	"$(INTDIR)\line.sbr" \
	"$(INTDIR)\macro.sbr" \
	"$(INTDIR)\misc.sbr" \
	"$(INTDIR)\msgpump.sbr" \
	"$(INTDIR)\posx.sbr" \
	"$(INTDIR)\precomp.sbr" \
	"$(INTDIR)\print.sbr" \
	"$(INTDIR)\props.sbr" \
	"$(INTDIR)\rangex.sbr" \
	"$(INTDIR)\rectx.sbr" \
	"$(INTDIR)\regexp.sbr" \
	"$(INTDIR)\selftest.sbr" \
	"$(INTDIR)\splitter.sbr" \
	"$(INTDIR)\syntax.sbr" \
	"$(INTDIR)\undo.sbr"

"$(OUTDIR)\Codemax.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=version.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\cmax20.pdb" /debug /machine:I386 /def:".\codemax.def" /out:"$(OUTDIR)\cmax20.dll" /implib:"$(OUTDIR)\cmax20.lib" /pdbtype:sept 
DEF_FILE= \
	".\codemax.def"
LINK32_OBJS= \
	"$(INTDIR)\bookmark.obj" \
	"$(INTDIR)\buffer.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\ceditmsg.obj" \
	"$(INTDIR)\ceditx.obj" \
	"$(INTDIR)\cmdarry.obj" \
	"$(INTDIR)\ctlstate.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\dragdrop.obj" \
	"$(INTDIR)\editcmd1.obj" \
	"$(INTDIR)\editdlgs.obj" \
	"$(INTDIR)\editsel.obj" \
	"$(INTDIR)\editview.obj" \
	"$(INTDIR)\exports.obj" \
	"$(INTDIR)\findrepl.obj" \
	"$(INTDIR)\globalsx.obj" \
	"$(INTDIR)\hkeyctrl.obj" \
	"$(INTDIR)\hotkey.obj" \
	"$(INTDIR)\hotkeyx.obj" \
	"$(INTDIR)\language.obj" \
	"$(INTDIR)\langx.obj" \
	"$(INTDIR)\line.obj" \
	"$(INTDIR)\macro.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\msgpump.obj" \
	"$(INTDIR)\posx.obj" \
	"$(INTDIR)\precomp.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\props.obj" \
	"$(INTDIR)\rangex.obj" \
	"$(INTDIR)\rectx.obj" \
	"$(INTDIR)\regexp.obj" \
	"$(INTDIR)\selftest.obj" \
	"$(INTDIR)\splitter.obj" \
	"$(INTDIR)\syntax.obj" \
	"$(INTDIR)\undo.obj" \
	"$(INTDIR)\codemax.res"

"$(OUTDIR)\cmax20.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"

OUTDIR=.\UnicodeRel
INTDIR=.\UnicodeRel
# Begin Custom Macros
OutDir=.\UnicodeRel
# End Custom Macros

ALL : "$(OUTDIR)\cmax20u.dll"


CLEAN :
	-@erase "$(INTDIR)\bookmark.obj"
	-@erase "$(INTDIR)\buffer.obj"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\ceditmsg.obj"
	-@erase "$(INTDIR)\ceditx.obj"
	-@erase "$(INTDIR)\cmdarry.obj"
	-@erase "$(INTDIR)\Codemax.pch"
	-@erase "$(INTDIR)\codemax.res"
	-@erase "$(INTDIR)\ctlstate.obj"
	-@erase "$(INTDIR)\debug.obj"
	-@erase "$(INTDIR)\dragdrop.obj"
	-@erase "$(INTDIR)\editcmd1.obj"
	-@erase "$(INTDIR)\editdlgs.obj"
	-@erase "$(INTDIR)\editsel.obj"
	-@erase "$(INTDIR)\editview.obj"
	-@erase "$(INTDIR)\exports.obj"
	-@erase "$(INTDIR)\findrepl.obj"
	-@erase "$(INTDIR)\globalsx.obj"
	-@erase "$(INTDIR)\hkeyctrl.obj"
	-@erase "$(INTDIR)\hotkey.obj"
	-@erase "$(INTDIR)\hotkeyx.obj"
	-@erase "$(INTDIR)\language.obj"
	-@erase "$(INTDIR)\langx.obj"
	-@erase "$(INTDIR)\line.obj"
	-@erase "$(INTDIR)\macro.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\msgpump.obj"
	-@erase "$(INTDIR)\posx.obj"
	-@erase "$(INTDIR)\precomp.obj"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\props.obj"
	-@erase "$(INTDIR)\rangex.obj"
	-@erase "$(INTDIR)\rectx.obj"
	-@erase "$(INTDIR)\regexp.obj"
	-@erase "$(INTDIR)\selftest.obj"
	-@erase "$(INTDIR)\splitter.obj"
	-@erase "$(INTDIR)\syntax.obj"
	-@erase "$(INTDIR)\undo.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\cmax20u.dll"
	-@erase "$(OUTDIR)\cmax20u.exp"
	-@erase "$(OUTDIR)\cmax20u.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /ML /W4 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_UNICODE" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\codemax.res" /d "NDEBUG" /d "_UNICODE" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Codemax.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib version.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\cmax20u.pdb" /machine:I386 /def:".\codemax.def" /out:"$(OUTDIR)\cmax20u.dll" /implib:"$(OUTDIR)\cmax20u.lib" 
DEF_FILE= \
	".\codemax.def"
LINK32_OBJS= \
	"$(INTDIR)\bookmark.obj" \
	"$(INTDIR)\buffer.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\ceditmsg.obj" \
	"$(INTDIR)\ceditx.obj" \
	"$(INTDIR)\cmdarry.obj" \
	"$(INTDIR)\ctlstate.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\dragdrop.obj" \
	"$(INTDIR)\editcmd1.obj" \
	"$(INTDIR)\editdlgs.obj" \
	"$(INTDIR)\editsel.obj" \
	"$(INTDIR)\editview.obj" \
	"$(INTDIR)\exports.obj" \
	"$(INTDIR)\findrepl.obj" \
	"$(INTDIR)\globalsx.obj" \
	"$(INTDIR)\hkeyctrl.obj" \
	"$(INTDIR)\hotkey.obj" \
	"$(INTDIR)\hotkeyx.obj" \
	"$(INTDIR)\language.obj" \
	"$(INTDIR)\langx.obj" \
	"$(INTDIR)\line.obj" \
	"$(INTDIR)\macro.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\msgpump.obj" \
	"$(INTDIR)\posx.obj" \
	"$(INTDIR)\precomp.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\props.obj" \
	"$(INTDIR)\rangex.obj" \
	"$(INTDIR)\rectx.obj" \
	"$(INTDIR)\regexp.obj" \
	"$(INTDIR)\selftest.obj" \
	"$(INTDIR)\splitter.obj" \
	"$(INTDIR)\syntax.obj" \
	"$(INTDIR)\undo.obj" \
	"$(INTDIR)\codemax.res"

"$(OUTDIR)\cmax20u.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"

OUTDIR=.\UnicodeDbg
INTDIR=.\UnicodeDbg
# Begin Custom Macros
OutDir=.\UnicodeDbg
# End Custom Macros

ALL : "$(OUTDIR)\cmax20u.dll"


CLEAN :
	-@erase "$(INTDIR)\bookmark.obj"
	-@erase "$(INTDIR)\buffer.obj"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\ceditmsg.obj"
	-@erase "$(INTDIR)\ceditx.obj"
	-@erase "$(INTDIR)\cmdarry.obj"
	-@erase "$(INTDIR)\Codemax.pch"
	-@erase "$(INTDIR)\codemax.res"
	-@erase "$(INTDIR)\ctlstate.obj"
	-@erase "$(INTDIR)\debug.obj"
	-@erase "$(INTDIR)\dragdrop.obj"
	-@erase "$(INTDIR)\editcmd1.obj"
	-@erase "$(INTDIR)\editdlgs.obj"
	-@erase "$(INTDIR)\editsel.obj"
	-@erase "$(INTDIR)\editview.obj"
	-@erase "$(INTDIR)\exports.obj"
	-@erase "$(INTDIR)\findrepl.obj"
	-@erase "$(INTDIR)\globalsx.obj"
	-@erase "$(INTDIR)\hkeyctrl.obj"
	-@erase "$(INTDIR)\hotkey.obj"
	-@erase "$(INTDIR)\hotkeyx.obj"
	-@erase "$(INTDIR)\language.obj"
	-@erase "$(INTDIR)\langx.obj"
	-@erase "$(INTDIR)\line.obj"
	-@erase "$(INTDIR)\macro.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\msgpump.obj"
	-@erase "$(INTDIR)\posx.obj"
	-@erase "$(INTDIR)\precomp.obj"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\props.obj"
	-@erase "$(INTDIR)\rangex.obj"
	-@erase "$(INTDIR)\rectx.obj"
	-@erase "$(INTDIR)\regexp.obj"
	-@erase "$(INTDIR)\selftest.obj"
	-@erase "$(INTDIR)\splitter.obj"
	-@erase "$(INTDIR)\syntax.obj"
	-@erase "$(INTDIR)\undo.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\cmax20u.dll"
	-@erase "$(OUTDIR)\cmax20u.exp"
	-@erase "$(OUTDIR)\cmax20u.lib"
	-@erase "$(OUTDIR)\cmax20u.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W4 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_UNICODE" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\codemax.res" /d "_DEBUG" /d "_UNICODE" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Codemax.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib version.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\cmax20u.pdb" /debug /machine:I386 /def:".\codemax.def" /out:"$(OUTDIR)\cmax20u.dll" /implib:"$(OUTDIR)\cmax20u.lib" /pdbtype:sept 
DEF_FILE= \
	".\codemax.def"
LINK32_OBJS= \
	"$(INTDIR)\bookmark.obj" \
	"$(INTDIR)\buffer.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\ceditmsg.obj" \
	"$(INTDIR)\ceditx.obj" \
	"$(INTDIR)\cmdarry.obj" \
	"$(INTDIR)\ctlstate.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\dragdrop.obj" \
	"$(INTDIR)\editcmd1.obj" \
	"$(INTDIR)\editdlgs.obj" \
	"$(INTDIR)\editsel.obj" \
	"$(INTDIR)\editview.obj" \
	"$(INTDIR)\exports.obj" \
	"$(INTDIR)\findrepl.obj" \
	"$(INTDIR)\globalsx.obj" \
	"$(INTDIR)\hkeyctrl.obj" \
	"$(INTDIR)\hotkey.obj" \
	"$(INTDIR)\hotkeyx.obj" \
	"$(INTDIR)\language.obj" \
	"$(INTDIR)\langx.obj" \
	"$(INTDIR)\line.obj" \
	"$(INTDIR)\macro.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\msgpump.obj" \
	"$(INTDIR)\posx.obj" \
	"$(INTDIR)\precomp.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\props.obj" \
	"$(INTDIR)\rangex.obj" \
	"$(INTDIR)\rectx.obj" \
	"$(INTDIR)\regexp.obj" \
	"$(INTDIR)\selftest.obj" \
	"$(INTDIR)\splitter.obj" \
	"$(INTDIR)\syntax.obj" \
	"$(INTDIR)\undo.obj" \
	"$(INTDIR)\codemax.res"

"$(OUTDIR)\cmax20u.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"

OUTDIR=.\XDebug
INTDIR=.\XDebug
# Begin Custom Macros
OutDir=.\XDebug
# End Custom Macros

ALL : ".\editx_i.c" ".\editx.tlb" ".\editx.h" "$(OUTDIR)\cmax20.ocx" "$(OUTDIR)\Codemax.bsc" ".\XDebug\regsvr32.trg"


CLEAN :
	-@erase "$(INTDIR)\bookmark.obj"
	-@erase "$(INTDIR)\bookmark.sbr"
	-@erase "$(INTDIR)\buffer.obj"
	-@erase "$(INTDIR)\buffer.sbr"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\cedit.sbr"
	-@erase "$(INTDIR)\ceditmsg.obj"
	-@erase "$(INTDIR)\ceditmsg.sbr"
	-@erase "$(INTDIR)\ceditx.obj"
	-@erase "$(INTDIR)\ceditx.sbr"
	-@erase "$(INTDIR)\cmdarry.obj"
	-@erase "$(INTDIR)\cmdarry.sbr"
	-@erase "$(INTDIR)\Codemax.pch"
	-@erase "$(INTDIR)\codemax.res"
	-@erase "$(INTDIR)\ctlstate.obj"
	-@erase "$(INTDIR)\ctlstate.sbr"
	-@erase "$(INTDIR)\debug.obj"
	-@erase "$(INTDIR)\debug.sbr"
	-@erase "$(INTDIR)\dragdrop.obj"
	-@erase "$(INTDIR)\dragdrop.sbr"
	-@erase "$(INTDIR)\editcmd1.obj"
	-@erase "$(INTDIR)\editcmd1.sbr"
	-@erase "$(INTDIR)\editdlgs.obj"
	-@erase "$(INTDIR)\editdlgs.sbr"
	-@erase "$(INTDIR)\editsel.obj"
	-@erase "$(INTDIR)\editsel.sbr"
	-@erase "$(INTDIR)\editview.obj"
	-@erase "$(INTDIR)\editview.sbr"
	-@erase "$(INTDIR)\exports.obj"
	-@erase "$(INTDIR)\exports.sbr"
	-@erase "$(INTDIR)\findrepl.obj"
	-@erase "$(INTDIR)\findrepl.sbr"
	-@erase "$(INTDIR)\globalsx.obj"
	-@erase "$(INTDIR)\globalsx.sbr"
	-@erase "$(INTDIR)\hkeyctrl.obj"
	-@erase "$(INTDIR)\hkeyctrl.sbr"
	-@erase "$(INTDIR)\hotkey.obj"
	-@erase "$(INTDIR)\hotkey.sbr"
	-@erase "$(INTDIR)\hotkeyx.obj"
	-@erase "$(INTDIR)\hotkeyx.sbr"
	-@erase "$(INTDIR)\language.obj"
	-@erase "$(INTDIR)\language.sbr"
	-@erase "$(INTDIR)\langx.obj"
	-@erase "$(INTDIR)\langx.sbr"
	-@erase "$(INTDIR)\line.obj"
	-@erase "$(INTDIR)\line.sbr"
	-@erase "$(INTDIR)\macro.obj"
	-@erase "$(INTDIR)\macro.sbr"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\misc.sbr"
	-@erase "$(INTDIR)\msgpump.obj"
	-@erase "$(INTDIR)\msgpump.sbr"
	-@erase "$(INTDIR)\posx.obj"
	-@erase "$(INTDIR)\posx.sbr"
	-@erase "$(INTDIR)\precomp.obj"
	-@erase "$(INTDIR)\precomp.sbr"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\print.sbr"
	-@erase "$(INTDIR)\props.obj"
	-@erase "$(INTDIR)\props.sbr"
	-@erase "$(INTDIR)\rangex.obj"
	-@erase "$(INTDIR)\rangex.sbr"
	-@erase "$(INTDIR)\rectx.obj"
	-@erase "$(INTDIR)\rectx.sbr"
	-@erase "$(INTDIR)\regexp.obj"
	-@erase "$(INTDIR)\regexp.sbr"
	-@erase "$(INTDIR)\selftest.obj"
	-@erase "$(INTDIR)\selftest.sbr"
	-@erase "$(INTDIR)\splitter.obj"
	-@erase "$(INTDIR)\splitter.sbr"
	-@erase "$(INTDIR)\syntax.obj"
	-@erase "$(INTDIR)\syntax.sbr"
	-@erase "$(INTDIR)\undo.obj"
	-@erase "$(INTDIR)\undo.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\cmax20.exp"
	-@erase "$(OUTDIR)\cmax20.lib"
	-@erase "$(OUTDIR)\cmax20.ocx"
	-@erase "$(OUTDIR)\cmax20.pdb"
	-@erase "$(OUTDIR)\Codemax.bsc"
	-@erase ".\editx.h"
	-@erase ".\editx.tlb"
	-@erase ".\editx_i.c"
	-@erase ".\XDebug\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "_WINDOWS_USRDLL" /D "_ACTIVEX" /D "WIN32" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\codemax.res" /d "_DEBUG" /d "_ACTIVEX" /d "_MBCS" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Codemax.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\bookmark.sbr" \
	"$(INTDIR)\buffer.sbr" \
	"$(INTDIR)\cedit.sbr" \
	"$(INTDIR)\ceditmsg.sbr" \
	"$(INTDIR)\ceditx.sbr" \
	"$(INTDIR)\cmdarry.sbr" \
	"$(INTDIR)\ctlstate.sbr" \
	"$(INTDIR)\debug.sbr" \
	"$(INTDIR)\dragdrop.sbr" \
	"$(INTDIR)\editcmd1.sbr" \
	"$(INTDIR)\editdlgs.sbr" \
	"$(INTDIR)\editsel.sbr" \
	"$(INTDIR)\editview.sbr" \
	"$(INTDIR)\exports.sbr" \
	"$(INTDIR)\findrepl.sbr" \
	"$(INTDIR)\globalsx.sbr" \
	"$(INTDIR)\hkeyctrl.sbr" \
	"$(INTDIR)\hotkey.sbr" \
	"$(INTDIR)\hotkeyx.sbr" \
	"$(INTDIR)\language.sbr" \
	"$(INTDIR)\langx.sbr" \
	"$(INTDIR)\line.sbr" \
	"$(INTDIR)\macro.sbr" \
	"$(INTDIR)\misc.sbr" \
	"$(INTDIR)\msgpump.sbr" \
	"$(INTDIR)\posx.sbr" \
	"$(INTDIR)\precomp.sbr" \
	"$(INTDIR)\print.sbr" \
	"$(INTDIR)\props.sbr" \
	"$(INTDIR)\rangex.sbr" \
	"$(INTDIR)\rectx.sbr" \
	"$(INTDIR)\regexp.sbr" \
	"$(INTDIR)\selftest.sbr" \
	"$(INTDIR)\splitter.sbr" \
	"$(INTDIR)\syntax.sbr" \
	"$(INTDIR)\undo.sbr"

"$(OUTDIR)\Codemax.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=version.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib /nologo /subsystem:windows /dll /incremental:no /pdb:"$(OUTDIR)\cmax20.pdb" /debug /machine:I386 /def:".\codemaxx.def" /out:"$(OUTDIR)\cmax20.ocx" /implib:"$(OUTDIR)\cmax20.lib" /pdbtype:sept 
DEF_FILE= \
	".\codemaxx.def"
LINK32_OBJS= \
	"$(INTDIR)\bookmark.obj" \
	"$(INTDIR)\buffer.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\ceditmsg.obj" \
	"$(INTDIR)\ceditx.obj" \
	"$(INTDIR)\cmdarry.obj" \
	"$(INTDIR)\ctlstate.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\dragdrop.obj" \
	"$(INTDIR)\editcmd1.obj" \
	"$(INTDIR)\editdlgs.obj" \
	"$(INTDIR)\editsel.obj" \
	"$(INTDIR)\editview.obj" \
	"$(INTDIR)\exports.obj" \
	"$(INTDIR)\findrepl.obj" \
	"$(INTDIR)\globalsx.obj" \
	"$(INTDIR)\hkeyctrl.obj" \
	"$(INTDIR)\hotkey.obj" \
	"$(INTDIR)\hotkeyx.obj" \
	"$(INTDIR)\language.obj" \
	"$(INTDIR)\langx.obj" \
	"$(INTDIR)\line.obj" \
	"$(INTDIR)\macro.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\msgpump.obj" \
	"$(INTDIR)\posx.obj" \
	"$(INTDIR)\precomp.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\props.obj" \
	"$(INTDIR)\rangex.obj" \
	"$(INTDIR)\rectx.obj" \
	"$(INTDIR)\regexp.obj" \
	"$(INTDIR)\selftest.obj" \
	"$(INTDIR)\splitter.obj" \
	"$(INTDIR)\syntax.obj" \
	"$(INTDIR)\undo.obj" \
	"$(INTDIR)\codemax.res"

"$(OUTDIR)\cmax20.ocx" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

OutDir=.\XDebug
TargetPath=.\XDebug\cmax20.ocx
InputPath=.\XDebug\cmax20.ocx
SOURCE="$(InputPath)"

"$(OUTDIR)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
<< 
	

!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"

OUTDIR=.\XRelease
INTDIR=.\XRelease
# Begin Custom Macros
OutDir=.\XRelease
# End Custom Macros

ALL : ".\editx_i.c" ".\editx.tlb" ".\editx.h" "$(OUTDIR)\cmax20.ocx" ".\XRelease\regsvr32.trg"


CLEAN :
	-@erase "$(INTDIR)\bookmark.obj"
	-@erase "$(INTDIR)\buffer.obj"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\ceditmsg.obj"
	-@erase "$(INTDIR)\ceditx.obj"
	-@erase "$(INTDIR)\cmdarry.obj"
	-@erase "$(INTDIR)\Codemax.pch"
	-@erase "$(INTDIR)\codemax.res"
	-@erase "$(INTDIR)\ctlstate.obj"
	-@erase "$(INTDIR)\debug.obj"
	-@erase "$(INTDIR)\dragdrop.obj"
	-@erase "$(INTDIR)\editcmd1.obj"
	-@erase "$(INTDIR)\editdlgs.obj"
	-@erase "$(INTDIR)\editsel.obj"
	-@erase "$(INTDIR)\editview.obj"
	-@erase "$(INTDIR)\exports.obj"
	-@erase "$(INTDIR)\findrepl.obj"
	-@erase "$(INTDIR)\globalsx.obj"
	-@erase "$(INTDIR)\hkeyctrl.obj"
	-@erase "$(INTDIR)\hotkey.obj"
	-@erase "$(INTDIR)\hotkeyx.obj"
	-@erase "$(INTDIR)\language.obj"
	-@erase "$(INTDIR)\langx.obj"
	-@erase "$(INTDIR)\line.obj"
	-@erase "$(INTDIR)\macro.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\msgpump.obj"
	-@erase "$(INTDIR)\posx.obj"
	-@erase "$(INTDIR)\precomp.obj"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\props.obj"
	-@erase "$(INTDIR)\rangex.obj"
	-@erase "$(INTDIR)\rectx.obj"
	-@erase "$(INTDIR)\regexp.obj"
	-@erase "$(INTDIR)\selftest.obj"
	-@erase "$(INTDIR)\splitter.obj"
	-@erase "$(INTDIR)\syntax.obj"
	-@erase "$(INTDIR)\undo.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(OUTDIR)\cmax20.exp"
	-@erase "$(OUTDIR)\cmax20.lib"
	-@erase "$(OUTDIR)\cmax20.ocx"
	-@erase ".\editx.h"
	-@erase ".\editx.tlb"
	-@erase ".\editx_i.c"
	-@erase ".\XRelease\regsvr32.trg"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MT /W3 /GX /O1 /D "NDEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_ACTIVEX" /D "WIN32" /D "_MBCS" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\codemax.res" /d "NDEBUG" /d "_ACTIVEX" /d "_MBCS" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Codemax.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib version.lib /nologo /subsystem:windows /dll /pdb:none /machine:I386 /def:".\codemaxx.def" /out:"$(OUTDIR)\cmax20.ocx" /implib:"$(OUTDIR)\cmax20.lib" 
DEF_FILE= \
	".\codemaxx.def"
LINK32_OBJS= \
	"$(INTDIR)\bookmark.obj" \
	"$(INTDIR)\buffer.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\ceditmsg.obj" \
	"$(INTDIR)\ceditx.obj" \
	"$(INTDIR)\cmdarry.obj" \
	"$(INTDIR)\ctlstate.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\dragdrop.obj" \
	"$(INTDIR)\editcmd1.obj" \
	"$(INTDIR)\editdlgs.obj" \
	"$(INTDIR)\editsel.obj" \
	"$(INTDIR)\editview.obj" \
	"$(INTDIR)\exports.obj" \
	"$(INTDIR)\findrepl.obj" \
	"$(INTDIR)\globalsx.obj" \
	"$(INTDIR)\hkeyctrl.obj" \
	"$(INTDIR)\hotkey.obj" \
	"$(INTDIR)\hotkeyx.obj" \
	"$(INTDIR)\language.obj" \
	"$(INTDIR)\langx.obj" \
	"$(INTDIR)\line.obj" \
	"$(INTDIR)\macro.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\msgpump.obj" \
	"$(INTDIR)\posx.obj" \
	"$(INTDIR)\precomp.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\props.obj" \
	"$(INTDIR)\rangex.obj" \
	"$(INTDIR)\rectx.obj" \
	"$(INTDIR)\regexp.obj" \
	"$(INTDIR)\selftest.obj" \
	"$(INTDIR)\splitter.obj" \
	"$(INTDIR)\syntax.obj" \
	"$(INTDIR)\undo.obj" \
	"$(INTDIR)\codemax.res"

"$(OUTDIR)\cmax20.ocx" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

OutDir=.\XRelease
TargetPath=.\XRelease\cmax20.ocx
InputPath=.\XRelease\cmax20.ocx
SOURCE="$(InputPath)"

"$(OUTDIR)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
<< 
	

!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"

OUTDIR=.\Profile
INTDIR=.\Profile
# Begin Custom Macros
OutDir=.\Profile
# End Custom Macros

ALL : "$(OUTDIR)\cmax20.dll"


CLEAN :
	-@erase "$(INTDIR)\bookmark.obj"
	-@erase "$(INTDIR)\buffer.obj"
	-@erase "$(INTDIR)\cedit.obj"
	-@erase "$(INTDIR)\ceditmsg.obj"
	-@erase "$(INTDIR)\ceditx.obj"
	-@erase "$(INTDIR)\cmdarry.obj"
	-@erase "$(INTDIR)\Codemax.pch"
	-@erase "$(INTDIR)\codemax.res"
	-@erase "$(INTDIR)\ctlstate.obj"
	-@erase "$(INTDIR)\debug.obj"
	-@erase "$(INTDIR)\dragdrop.obj"
	-@erase "$(INTDIR)\editcmd1.obj"
	-@erase "$(INTDIR)\editdlgs.obj"
	-@erase "$(INTDIR)\editsel.obj"
	-@erase "$(INTDIR)\editview.obj"
	-@erase "$(INTDIR)\exports.obj"
	-@erase "$(INTDIR)\findrepl.obj"
	-@erase "$(INTDIR)\globalsx.obj"
	-@erase "$(INTDIR)\hkeyctrl.obj"
	-@erase "$(INTDIR)\hotkey.obj"
	-@erase "$(INTDIR)\hotkeyx.obj"
	-@erase "$(INTDIR)\language.obj"
	-@erase "$(INTDIR)\langx.obj"
	-@erase "$(INTDIR)\line.obj"
	-@erase "$(INTDIR)\macro.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\msgpump.obj"
	-@erase "$(INTDIR)\posx.obj"
	-@erase "$(INTDIR)\precomp.obj"
	-@erase "$(INTDIR)\print.obj"
	-@erase "$(INTDIR)\props.obj"
	-@erase "$(INTDIR)\rangex.obj"
	-@erase "$(INTDIR)\rectx.obj"
	-@erase "$(INTDIR)\regexp.obj"
	-@erase "$(INTDIR)\selftest.obj"
	-@erase "$(INTDIR)\splitter.obj"
	-@erase "$(INTDIR)\syntax.obj"
	-@erase "$(INTDIR)\undo.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(OUTDIR)\cmax20.dll"
	-@erase "$(OUTDIR)\cmax20.exp"
	-@erase "$(OUTDIR)\cmax20.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W4 /Gm /GX /Zi /Od /D "_DEBUG" /D "_WINDOWS" /D "WIN32" /D "_MBCS" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32 
RSC=rc.exe
RSC_PROJ=/l 0x409 /fo"$(INTDIR)\codemax.res" /d "_DEBUG" /d "_MBCS" 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\Codemax.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib comctl32.lib version.lib /nologo /subsystem:windows /dll /profile /debug /machine:I386 /def:".\codemax.def" /out:"$(OUTDIR)\cmax20.dll" /implib:"$(OUTDIR)\cmax20.lib" 
DEF_FILE= \
	".\codemax.def"
LINK32_OBJS= \
	"$(INTDIR)\bookmark.obj" \
	"$(INTDIR)\buffer.obj" \
	"$(INTDIR)\cedit.obj" \
	"$(INTDIR)\ceditmsg.obj" \
	"$(INTDIR)\ceditx.obj" \
	"$(INTDIR)\cmdarry.obj" \
	"$(INTDIR)\ctlstate.obj" \
	"$(INTDIR)\debug.obj" \
	"$(INTDIR)\dragdrop.obj" \
	"$(INTDIR)\editcmd1.obj" \
	"$(INTDIR)\editdlgs.obj" \
	"$(INTDIR)\editsel.obj" \
	"$(INTDIR)\editview.obj" \
	"$(INTDIR)\exports.obj" \
	"$(INTDIR)\findrepl.obj" \
	"$(INTDIR)\globalsx.obj" \
	"$(INTDIR)\hkeyctrl.obj" \
	"$(INTDIR)\hotkey.obj" \
	"$(INTDIR)\hotkeyx.obj" \
	"$(INTDIR)\language.obj" \
	"$(INTDIR)\langx.obj" \
	"$(INTDIR)\line.obj" \
	"$(INTDIR)\macro.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\msgpump.obj" \
	"$(INTDIR)\posx.obj" \
	"$(INTDIR)\precomp.obj" \
	"$(INTDIR)\print.obj" \
	"$(INTDIR)\props.obj" \
	"$(INTDIR)\rangex.obj" \
	"$(INTDIR)\rectx.obj" \
	"$(INTDIR)\regexp.obj" \
	"$(INTDIR)\selftest.obj" \
	"$(INTDIR)\splitter.obj" \
	"$(INTDIR)\syntax.obj" \
	"$(INTDIR)\undo.obj" \
	"$(INTDIR)\codemax.res"

"$(OUTDIR)\cmax20.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 


!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("Codemax.dep")
!INCLUDE "Codemax.dep"
!ELSE 
!MESSAGE Warning: cannot find "Codemax.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "codemax - Win32 Release" || "$(CFG)" == "codemax - Win32 Debug" || "$(CFG)" == "codemax - Win32 Unicode Release" || "$(CFG)" == "codemax - Win32 Unicode Debug" || "$(CFG)" == "codemax - Win32 ActiveX Debug" || "$(CFG)" == "codemax - Win32 ActiveX Release" || "$(CFG)" == "codemax - Win32 Profile"
SOURCE=.\bookmark.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\bookmark.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\bookmark.obj"	"$(INTDIR)\bookmark.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\bookmark.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\bookmark.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\bookmark.obj"	"$(INTDIR)\bookmark.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\bookmark.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\bookmark.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\buffer.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\buffer.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\buffer.obj"	"$(INTDIR)\buffer.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\buffer.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\buffer.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\buffer.obj"	"$(INTDIR)\buffer.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\buffer.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\buffer.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\cedit.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\cedit.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\cedit.obj"	"$(INTDIR)\cedit.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\cedit.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\cedit.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\cedit.obj"	"$(INTDIR)\cedit.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\cedit.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\cedit.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\ceditmsg.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\ceditmsg.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\ceditmsg.obj"	"$(INTDIR)\ceditmsg.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\ceditmsg.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\ceditmsg.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\ceditmsg.obj"	"$(INTDIR)\ceditmsg.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\ceditmsg.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\ceditmsg.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\ceditx.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\ceditx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\ceditx.obj"	"$(INTDIR)\ceditx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\ceditx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\ceditx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\ceditx.obj"	"$(INTDIR)\ceditx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\ceditx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\ceditx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\cmdarry.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\cmdarry.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\cmdarry.obj"	"$(INTDIR)\cmdarry.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\cmdarry.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\cmdarry.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\cmdarry.obj"	"$(INTDIR)\cmdarry.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\cmdarry.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\cmdarry.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\codemax.rc

"$(INTDIR)\codemax.res" : $(SOURCE) "$(INTDIR)"
	$(RSC) $(RSC_PROJ) $(SOURCE)


SOURCE=.\ctlstate.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\ctlstate.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\ctlstate.obj"	"$(INTDIR)\ctlstate.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\ctlstate.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\ctlstate.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\ctlstate.obj"	"$(INTDIR)\ctlstate.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\ctlstate.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\ctlstate.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\debug.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\debug.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\debug.obj"	"$(INTDIR)\debug.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\debug.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\debug.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\debug.obj"	"$(INTDIR)\debug.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\debug.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\debug.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\dragdrop.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\dragdrop.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\dragdrop.obj"	"$(INTDIR)\dragdrop.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\dragdrop.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\dragdrop.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\dragdrop.obj"	"$(INTDIR)\dragdrop.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\dragdrop.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\dragdrop.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\editcmd1.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\editcmd1.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\editcmd1.obj"	"$(INTDIR)\editcmd1.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\editcmd1.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\editcmd1.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\editcmd1.obj"	"$(INTDIR)\editcmd1.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\editcmd1.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\editcmd1.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\editdlgs.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\editdlgs.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\editdlgs.obj"	"$(INTDIR)\editdlgs.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\editdlgs.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\editdlgs.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\editdlgs.obj"	"$(INTDIR)\editdlgs.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\editdlgs.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\editdlgs.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\editsel.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\editsel.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\editsel.obj"	"$(INTDIR)\editsel.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\editsel.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\editsel.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\editsel.obj"	"$(INTDIR)\editsel.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\editsel.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\editsel.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\editview.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\editview.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\editview.obj"	"$(INTDIR)\editview.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\editview.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\editview.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\editview.obj"	"$(INTDIR)\editview.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\editview.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\editview.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\editx.idl

!IF  "$(CFG)" == "codemax - Win32 Release"

!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"

!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"

!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"

!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"

InputPath=.\editx.idl

".\editx.tlb"	".\editx.h"	".\editx_i.c" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	midl /Oicf /client none /server none /h "editx.h" /iid "editx_i.c" "editx.idl"
<< 
	

!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"

InputPath=.\editx.idl

".\editx.tlb"	".\editx.h"	".\editx_i.c" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	<<tempfile.bat 
	@echo off 
	midl /Oicf /client none /server none /h "editx.h" /iid "editx_i.c" "editx.idl"
<< 
	

!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"

!ENDIF 

SOURCE=.\exports.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\exports.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\exports.obj"	"$(INTDIR)\exports.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\exports.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\exports.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\exports.obj"	"$(INTDIR)\exports.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\exports.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\exports.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\findrepl.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\findrepl.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\findrepl.obj"	"$(INTDIR)\findrepl.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\findrepl.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\findrepl.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\findrepl.obj"	"$(INTDIR)\findrepl.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\findrepl.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\findrepl.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\globalsx.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\globalsx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\globalsx.obj"	"$(INTDIR)\globalsx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\globalsx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\globalsx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\globalsx.obj"	"$(INTDIR)\globalsx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\globalsx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\globalsx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\hkeyctrl.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\hkeyctrl.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\hkeyctrl.obj"	"$(INTDIR)\hkeyctrl.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\hkeyctrl.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\hkeyctrl.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\hkeyctrl.obj"	"$(INTDIR)\hkeyctrl.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\hkeyctrl.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\hkeyctrl.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\hotkey.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\hotkey.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\hotkey.obj"	"$(INTDIR)\hotkey.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\hotkey.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\hotkey.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\hotkey.obj"	"$(INTDIR)\hotkey.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\hotkey.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\hotkey.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\hotkeyx.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\hotkeyx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\hotkeyx.obj"	"$(INTDIR)\hotkeyx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\hotkeyx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\hotkeyx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\hotkeyx.obj"	"$(INTDIR)\hotkeyx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\hotkeyx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\hotkeyx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\language.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\language.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\language.obj"	"$(INTDIR)\language.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\language.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\language.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\language.obj"	"$(INTDIR)\language.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\language.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\language.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\langx.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\langx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\langx.obj"	"$(INTDIR)\langx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\langx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\langx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\langx.obj"	"$(INTDIR)\langx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\langx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\langx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\line.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\line.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\line.obj"	"$(INTDIR)\line.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\line.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\line.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\line.obj"	"$(INTDIR)\line.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\line.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\line.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\macro.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\macro.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\macro.obj"	"$(INTDIR)\macro.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\macro.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\macro.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\macro.obj"	"$(INTDIR)\macro.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\macro.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\macro.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\misc.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\misc.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\misc.obj"	"$(INTDIR)\misc.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\misc.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\misc.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\misc.obj"	"$(INTDIR)\misc.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\misc.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\misc.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\msgpump.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\msgpump.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\msgpump.obj"	"$(INTDIR)\msgpump.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\msgpump.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\msgpump.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\msgpump.obj"	"$(INTDIR)\msgpump.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\msgpump.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\msgpump.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\posx.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\posx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\posx.obj"	"$(INTDIR)\posx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\posx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\posx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\posx.obj"	"$(INTDIR)\posx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\posx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\posx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\precomp.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"

CPP_SWITCHES=/nologo /ML /W4 /GX /O2 /D "NDEBUG" /D "_WINDOWS" /D "WIN32" /D "_MBCS" /Fp"$(INTDIR)\Codemax.pch" /Yc"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\precomp.obj"	"$(INTDIR)\Codemax.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"

CPP_SWITCHES=/nologo /MLd /W4 /Gm /GX /Zi /Od /D "_DEBUG" /D "_WINDOWS" /D "WIN32" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\Codemax.pch" /Yc"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\precomp.obj"	"$(INTDIR)\precomp.sbr"	"$(INTDIR)\Codemax.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"

CPP_SWITCHES=/nologo /ML /W4 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_UNICODE" /Fp"$(INTDIR)\Codemax.pch" /Yc"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\precomp.obj"	"$(INTDIR)\Codemax.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"

CPP_SWITCHES=/nologo /MLd /W4 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_UNICODE" /Fp"$(INTDIR)\Codemax.pch" /Yc"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\precomp.obj"	"$(INTDIR)\Codemax.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"

CPP_SWITCHES=/nologo /MLd /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "_WINDOWS_USRDLL" /D "_ACTIVEX" /D "WIN32" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\Codemax.pch" /Yc"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\precomp.obj"	"$(INTDIR)\precomp.sbr"	"$(INTDIR)\Codemax.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"

CPP_SWITCHES=/nologo /MT /W3 /GX /O1 /D "NDEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_ACTIVEX" /D "WIN32" /D "_MBCS" /Fp"$(INTDIR)\Codemax.pch" /Yc"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\precomp.obj"	"$(INTDIR)\Codemax.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"

CPP_SWITCHES=/nologo /MLd /W4 /Gm /GX /Zi /Od /D "_DEBUG" /D "_WINDOWS" /D "WIN32" /D "_MBCS" /Fp"$(INTDIR)\Codemax.pch" /Yc"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\precomp.obj"	"$(INTDIR)\Codemax.pch" : $(SOURCE) "$(INTDIR)"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\print.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\print.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\print.obj"	"$(INTDIR)\print.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\print.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\print.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\print.obj"	"$(INTDIR)\print.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\print.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\print.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\props.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\props.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\props.obj"	"$(INTDIR)\props.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\props.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\props.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\props.obj"	"$(INTDIR)\props.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\props.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\props.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\rangex.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\rangex.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\rangex.obj"	"$(INTDIR)\rangex.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\rangex.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\rangex.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\rangex.obj"	"$(INTDIR)\rangex.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\rangex.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\rangex.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\rectx.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\rectx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\rectx.obj"	"$(INTDIR)\rectx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\rectx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\rectx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\rectx.obj"	"$(INTDIR)\rectx.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\rectx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\rectx.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\regexp.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"

CPP_SWITCHES=/nologo /ML /W1 /GX /O2 /D "NDEBUG" /D "_WINDOWS" /D "WIN32" /D "_MBCS" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\regexp.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"

CPP_SWITCHES=/nologo /MLd /W4 /Gm /GX /Zi /Od /D "_DEBUG" /D "_WINDOWS" /D "WIN32" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\regexp.obj"	"$(INTDIR)\regexp.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"

CPP_SWITCHES=/nologo /ML /W1 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_UNICODE" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\regexp.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"

CPP_SWITCHES=/nologo /MLd /W1 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_UNICODE" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\regexp.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"

CPP_SWITCHES=/nologo /MLd /W1 /Gm /GX /Zi /Od /D "_DEBUG" /D "_WINDOWS_USRDLL" /D "_ACTIVEX" /D "WIN32" /D "_MBCS" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\regexp.obj"	"$(INTDIR)\regexp.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"

CPP_SWITCHES=/nologo /MT /W1 /GX /O1 /D "NDEBUG" /D "_WINDOWS" /D "_USRDLL" /D "_ACTIVEX" /D "WIN32" /D "_MBCS" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\regexp.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"

CPP_SWITCHES=/nologo /MLd /W1 /Gm /GX /Zi /Od /D "_DEBUG" /D "_WINDOWS" /D "WIN32" /D "_MBCS" /Fp"$(INTDIR)\Codemax.pch" /Yu"precomp.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

"$(INTDIR)\regexp.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"
	$(CPP) @<<
  $(CPP_SWITCHES) $(SOURCE)
<<


!ENDIF 

SOURCE=.\selftest.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\selftest.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\selftest.obj"	"$(INTDIR)\selftest.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\selftest.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\selftest.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\selftest.obj"	"$(INTDIR)\selftest.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\selftest.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\selftest.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\splitter.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\splitter.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\splitter.obj"	"$(INTDIR)\splitter.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\splitter.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\splitter.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\splitter.obj"	"$(INTDIR)\splitter.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\splitter.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\splitter.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\syntax.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\syntax.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\syntax.obj"	"$(INTDIR)\syntax.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\syntax.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\syntax.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\syntax.obj"	"$(INTDIR)\syntax.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\syntax.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\syntax.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 

SOURCE=.\undo.cpp

!IF  "$(CFG)" == "codemax - Win32 Release"


"$(INTDIR)\undo.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Debug"


"$(INTDIR)\undo.obj"	"$(INTDIR)\undo.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Release"


"$(INTDIR)\undo.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Unicode Debug"


"$(INTDIR)\undo.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Debug"


"$(INTDIR)\undo.obj"	"$(INTDIR)\undo.sbr" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 ActiveX Release"


"$(INTDIR)\undo.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ELSEIF  "$(CFG)" == "codemax - Win32 Profile"


"$(INTDIR)\undo.obj" : $(SOURCE) "$(INTDIR)" "$(INTDIR)\Codemax.pch"


!ENDIF 


!ENDIF 

