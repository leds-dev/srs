// ==========================================================================
// 							Class Specification : CMFCToolBar
// ==========================================================================

// Header file : MFCToolBar.h

// Product Version: 9.0

// Copyright � Dundas Software Ltd. 1997 - 2003, All Rights Reserved
                          
// //////////////////////////////////////////////////////////////////////////

#pragma once

#include "OXDllExt.h"

#define TOOLBAR_BASE_CLASS CToolBar

// CMFCToolBar

class OX_CLASS_DECL CMFCToolBar : public TOOLBAR_BASE_CLASS
{
	DECLARE_DYNAMIC(CMFCToolBar)

public:
	void SetBarStyle(DWORD dwStyle);
	CMFCToolBar();
	virtual ~CMFCToolBar();
	virtual BOOL CreateEx(CWnd* pParentWnd, DWORD dwCtrlStyle = TBSTYLE_FLAT,
		DWORD dwStyle = WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP,
		CRect rcBorders = CRect(0, 0, 0, 0),
		UINT nID = AFX_IDW_TOOLBAR);

protected:
	DECLARE_MESSAGE_MAP()
};


