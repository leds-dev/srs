// ==========================================================================
//						Class Specification : 
//						COXAdvancedAssertMail
// ==========================================================================

// Header file : OXAdvancedAssertMail.h

// Product Version: 9.0

// Copyright � Dundas Software Ltd. 1997 - 2003, All Rights Reserved
                         

#ifndef __AdvancedAssertMail_h__
#define __AdvancedAssertMail_h__

// If this is not a debug or a beta version, then don't use any of this code. 
#if defined(_DEBUG) || defined(BETA)   // entire file

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "OXDllExt.h"

#include <mapi.h>  // MapiMessage

/////////////////////////////////////////////////////////////////////////////
// COXAdvancedAssertMail

class OX_CLASS_DECL COXAdvancedAssertMail
#ifdef _WIN32
 : public CNoTrackObject
#endif
	{
	public:
		COXAdvancedAssertMail() ;
		virtual ~COXAdvancedAssertMail() ;
	
	public:
		BOOL    IsLoaded()        const ;
		BOOL    IsValidSendMail() const ;
		BOOL    LoadMapiLibrary() ;
		void    FreeMapiLibrary() ;
		
		ULONG   Logon   (ULONG ulUIParam, LPSTR lpszName, LPSTR lpszPassword, FLAGS flFlags, ULONG ulReserved) ;
		ULONG   Logoff  (ULONG ulUIParam, FLAGS flFlags, ULONG ulReserved) ;
		ULONG   SendMail(ULONG ulUIParam, lpMapiMessage lpMessage, FLAGS flFlags, ULONG ulReserved) ;

	protected:
		HINSTANCE        m_hInstMail     ;      // handle to MAPI32.DLL
		LHANDLE          m_lhSession     ;
		ULONG   (PASCAL* m_lpfnMAPILogon   )(ULONG   ulUIParam, LPSTR lpszName, LPSTR lpszPassword, FLAGS flFlags, ULONG ulReserved, LPLHANDLE lplhSession) ;
		ULONG   (PASCAL* m_lpfnMAPILogoff  )(LHANDLE lhSession, ULONG ulUIParam, FLAGS flFlags, ULONG ulReserved) ;
		HRESULT (PASCAL* m_lpfnMAPISendMail)(LHANDLE lhSession, ULONG ulUIParam, lpMapiMessage lpMessage, FLAGS flFlags, ULONG ulReserved) ;
	} ;

#ifdef _WIN32
EXTERN_PROCESS_LOCAL(COXAdvancedAssertMail, appMailState)
#else
extern COXAdvancedAssertMail _appMailState ;
#endif

int SendMail      (LPCSTR pszTo, LPCSTR pszSubject, LPCSTR pszBody, LPCSTR pszAttachment=NULL) ;
int SendMailToBeta(              LPCSTR pszSubject, LPCSTR pszBody, LPCSTR pszAttachment=NULL) ;

#endif // _DEBUG || BETA

#endif // __AdvancedAssertMail_h__
/////////////////////////////////////////////////////////////////////////////
