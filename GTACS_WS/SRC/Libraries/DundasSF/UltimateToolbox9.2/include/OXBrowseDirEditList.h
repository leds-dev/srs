// ==========================================================================
// 					Class Specification : COXBrowseDirEditList
// ==========================================================================

// Header file : OXBrowseDirEditList.h

// Product Version: 9.0

// Copyright � Dundas Software Ltd. 1997 - 2003, All Rights Reserved
                         
// //////////////////////////////////////////////////////////////////////////

#ifndef __OXBROWSEDIREDITLIST_H__
#define __OXBROWSEDIREDITLIST_H__

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "OXDllExt.h"

#include "OXEditList.h"
#include "OXBrowseDirEdit.h"



class OX_CLASS_DECL COXBrowseDirGridEdit : public COXBaseBrowseDirEdit<COXGridEdit>
{
protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
};


class OX_CLASS_DECL COXBrowseDirGridEdit16 : public COXBaseBrowseDirEdit16<COXGridEdit>
{
protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
};


class OX_CLASS_DECL COXBrowseFileGridEdit : public COXBaseBrowseFileEdit<COXGridEdit>
{
protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
};


////////////////////////////////////////////////////////////////////////////////


class OX_CLASS_DECL COXBrowseDirEditList : public COXEditList
{
public:
	COXBrowseDirEditList(EToolbarPosition eToolbarPosition=TBPHorizontalTopRight,
		BOOL bAllowDuplicates=FALSE, BOOL bOrderedList=TRUE) :
	COXEditList(eToolbarPosition,bAllowDuplicates,bOrderedList) {};

protected:
	COXBrowseDirGridEdit m_browseDirEdit;

public:
	virtual COXGridEdit* GetGridEditCtrl() { return &m_browseDirEdit; }
};


class OX_CLASS_DECL COXBrowseDirEditList16 : public COXEditList
{
public:
	COXBrowseDirEditList16(EToolbarPosition eToolbarPosition=TBPHorizontalTopRight,
		BOOL bAllowDuplicates=FALSE, BOOL bOrderedList=TRUE) :
	COXEditList(eToolbarPosition,bAllowDuplicates,bOrderedList) {};

protected:
	COXBrowseDirGridEdit16 m_browseDirEdit16;

public:
	virtual COXGridEdit* GetGridEditCtrl() { return &m_browseDirEdit16; }
};


class OX_CLASS_DECL COXBrowseFileEditList : public COXEditList
{
public:
	COXBrowseFileEditList(EToolbarPosition eToolbarPosition=TBPHorizontalTopRight,
		BOOL bAllowDuplicates=FALSE, BOOL bOrderedList=TRUE) :
	COXEditList(eToolbarPosition,bAllowDuplicates,bOrderedList) {};

protected:
	COXBrowseFileGridEdit m_browseFileEdit;

public:
	virtual COXGridEdit* GetGridEditCtrl() { return &m_browseFileEdit; }
};


#endif      // __OXBROWSEDIREDITLIST_H__
