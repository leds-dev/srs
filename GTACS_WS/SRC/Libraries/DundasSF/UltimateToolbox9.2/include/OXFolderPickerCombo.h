// ==========================================================================
// 					Class Specification : COXFolderPickerCombo
// ==========================================================================

// Product Version: 9.0

// Copyright � Dundas Software Ltd. 1997 - 2003, All Rights Reserved
                          
// //////////////////////////////////////////////////////////////////////////

#ifndef _OX_FOLDERPICKERCOMBO__
#define _OX_FOLDERPICKERCOMBO__


#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "OXDllExt.h"

#include "OXFolderPickerDlg.h"


class OX_CLASS_DECL COXFolderPickerCombo : public COXHistoryCombo
{
public:
	virtual BOOL BrowseItem()
	{
		ASSERT_VALID(this);

		BOOL bBrowseOK = FALSE;
		CString sCurrentDirectory;
		GetWindowText(sCurrentDirectory);
		COXFolderPickerDlg folderPicker(sCurrentDirectory);
		if(folderPicker.DoModal()==IDOK)
		{
			sCurrentDirectory = folderPicker.GetFolderPath();
			SetWindowText(sCurrentDirectory);

			CWnd* pParentWnd=GetParent();
			if(pParentWnd!=NULL)
			{
				pParentWnd->SendMessage(WM_COMMAND,
					MAKEWPARAM(GetDlgCtrlID(),CBN_EDITCHANGE),
					(LPARAM)GetSafeHwnd());
			}
		
			bBrowseOK = TRUE;
		}

		ASSERT_VALID(this);

		return bBrowseOK;
	}
};

#endif	//	_OX_FOLDERPICKERCOMBO__