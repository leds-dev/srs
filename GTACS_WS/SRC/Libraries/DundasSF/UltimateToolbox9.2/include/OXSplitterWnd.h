// ==========================================================================
//		Class Specification:	COXSplitterWnd
// ==========================================================================

// Product Version: 9.0

// Copyright � Dundas Software Ltd. 2K, All Rights Reserved
                          
// //////////////////////////////////////////////////////////////////////////

#ifndef _OXSPLITTERWND_H
#define _OXSPLITTERWND_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "OXDllExt.h"


/////////////////////////////////////////////////////////////////////////////
// COXSplitterWnd

class OX_CLASS_DECL COXSplitterWnd : public CSplitterWnd
{
	DECLARE_DYNCREATE(COXSplitterWnd)

public:
	COXSplitterWnd();
	virtual ~COXSplitterWnd();

#ifdef _DEBUG
	virtual void AssertValid() const;
#endif

// Attributes
public:
	inline CSize GetSplitterBarSize() const 
	{ 
		return CSize(m_cxSplitter,m_cySplitter); 
	}

// Operations
public:
	BOOL SwapPanes(CWnd* pFirstWnd, CWnd* pSecondWnd);
	virtual BOOL InsertColumn(int nInsertBefore, int nWidth);
	virtual BOOL RemoveColumn(int nDelCol);
	virtual BOOL InsertRow(int nInsertBefore, int nHeight);
	virtual BOOL RemoveRow(int nDelRow);

	virtual void DeleteView(int row, int col);

protected:
	//{{AFX_MSG(COXSplitterWnd)
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};



/////////////////////////////////////////////////////////////

#endif // _OXSPLITTERWND_H

