/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Wed Mar 06 07:55:31 2013
 */
/* Compiler settings for EPDR2.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __EPDR2_h__
#define __EPDR2_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IEpDirRtr_FWD_DEFINED__
#define __IEpDirRtr_FWD_DEFINED__
typedef interface IEpDirRtr IEpDirRtr;
#endif 	/* __IEpDirRtr_FWD_DEFINED__ */


#ifndef __EpDirRtr2_FWD_DEFINED__
#define __EpDirRtr2_FWD_DEFINED__

#ifdef __cplusplus
typedef class EpDirRtr2 EpDirRtr2;
#else
typedef struct EpDirRtr2 EpDirRtr2;
#endif /* __cplusplus */

#endif 	/* __EpDirRtr2_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

/* interface __MIDL_itf_EPDR2_0000 */
/* [local] */ 

typedef 
enum EPDRP
    {	EPDRP_EPVIEWER	= 0,
	EPDRP_EPSTOL	= EPDRP_EPVIEWER + 1
    }	EPDRP;



extern RPC_IF_HANDLE __MIDL_itf_EPDR2_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_EPDR2_0000_v0_0_s_ifspec;

#ifndef __IEpDirRtr_INTERFACE_DEFINED__
#define __IEpDirRtr_INTERFACE_DEFINED__

/* interface IEpDirRtr */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IEpDirRtr;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("AD528EB7-C9E6-11D3-BB08-00600867A0E2")
    IEpDirRtr : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Connect( 
            /* [in] */ BSTR bstrPCUser,
            /* [in] */ BSTR bstrStream,
            /* [in] */ IUnknown __RPC_FAR *pUnkSink,
            /* [retval][out] */ ULONG __RPC_FAR *pulRtrHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE Disconnect( 
            /* [in] */ ULONG ulRtrHandle) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ExecuteDirective( 
            /* [in] */ ULONG ulRtrHandle,
            /* [in] */ BSTR bstrDir,
            /* [in] */ SHORT sPriority,
            /* [in] */ ULONG ulUserData) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_Parent( 
            /* [in] */ EPDRP newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IEpViewer( 
            /* [in] */ IDispatch __RPC_FAR *newVal) = 0;
        
        virtual /* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE put_IEpSTOLApp( 
            /* [in] */ IDispatch __RPC_FAR *newVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetEpIUnk( 
            /* [in] */ short sWhichIUnk,
            /* [in] */ BOOL bInstantiate,
            /* [retval][out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE SetEpIUnk( 
            /* [in] */ short sWhichIUnk,
            /* [in] */ IUnknown __RPC_FAR *pIUnk) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE ReleasePointers( void) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE VerifyCritical( 
            /* [in] */ int ptType,
            /* [in] */ ULONG ulInterpID,
            /* [in] */ BSTR bstrCritical) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE VerifyPTV( 
            /* [in] */ int ptType,
            /* [in] */ ULONG ulInterpID,
            /* [in] */ BSTR bstrPTV) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE VerifyProtected( 
            /* [in] */ BSTR bstrProtected,
            /* [out] */ BSTR __RPC_FAR *pbstrUserName) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE GetProcList( 
            /* [retval][out] */ BSTR __RPC_FAR *pbstrProcs) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpDirRtrVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpDirRtr __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpDirRtr __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IEpDirRtr __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Connect )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ BSTR bstrPCUser,
            /* [in] */ BSTR bstrStream,
            /* [in] */ IUnknown __RPC_FAR *pUnkSink,
            /* [retval][out] */ ULONG __RPC_FAR *pulRtrHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Disconnect )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ ULONG ulRtrHandle);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExecuteDirective )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ ULONG ulRtrHandle,
            /* [in] */ BSTR bstrDir,
            /* [in] */ SHORT sPriority,
            /* [in] */ ULONG ulUserData);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Parent )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ EPDRP newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_IEpViewer )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ IDispatch __RPC_FAR *newVal);
        
        /* [helpstring][id][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_IEpSTOLApp )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ IDispatch __RPC_FAR *newVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetEpIUnk )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ short sWhichIUnk,
            /* [in] */ BOOL bInstantiate,
            /* [retval][out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetEpIUnk )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ short sWhichIUnk,
            /* [in] */ IUnknown __RPC_FAR *pIUnk);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ReleasePointers )( 
            IEpDirRtr __RPC_FAR * This);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *VerifyCritical )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ int ptType,
            /* [in] */ ULONG ulInterpID,
            /* [in] */ BSTR bstrCritical);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *VerifyPTV )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ int ptType,
            /* [in] */ ULONG ulInterpID,
            /* [in] */ BSTR bstrPTV);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *VerifyProtected )( 
            IEpDirRtr __RPC_FAR * This,
            /* [in] */ BSTR bstrProtected,
            /* [out] */ BSTR __RPC_FAR *pbstrUserName);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetProcList )( 
            IEpDirRtr __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrProcs);
        
        END_INTERFACE
    } IEpDirRtrVtbl;

    interface IEpDirRtr
    {
        CONST_VTBL struct IEpDirRtrVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpDirRtr_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpDirRtr_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpDirRtr_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpDirRtr_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEpDirRtr_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEpDirRtr_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEpDirRtr_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEpDirRtr_Connect(This,bstrPCUser,bstrStream,pUnkSink,pulRtrHandle)	\
    (This)->lpVtbl -> Connect(This,bstrPCUser,bstrStream,pUnkSink,pulRtrHandle)

#define IEpDirRtr_Disconnect(This,ulRtrHandle)	\
    (This)->lpVtbl -> Disconnect(This,ulRtrHandle)

#define IEpDirRtr_ExecuteDirective(This,ulRtrHandle,bstrDir,sPriority,ulUserData)	\
    (This)->lpVtbl -> ExecuteDirective(This,ulRtrHandle,bstrDir,sPriority,ulUserData)

#define IEpDirRtr_put_Parent(This,newVal)	\
    (This)->lpVtbl -> put_Parent(This,newVal)

#define IEpDirRtr_put_IEpViewer(This,newVal)	\
    (This)->lpVtbl -> put_IEpViewer(This,newVal)

#define IEpDirRtr_put_IEpSTOLApp(This,newVal)	\
    (This)->lpVtbl -> put_IEpSTOLApp(This,newVal)

#define IEpDirRtr_GetEpIUnk(This,sWhichIUnk,bInstantiate,ppIUnk)	\
    (This)->lpVtbl -> GetEpIUnk(This,sWhichIUnk,bInstantiate,ppIUnk)

#define IEpDirRtr_SetEpIUnk(This,sWhichIUnk,pIUnk)	\
    (This)->lpVtbl -> SetEpIUnk(This,sWhichIUnk,pIUnk)

#define IEpDirRtr_ReleasePointers(This)	\
    (This)->lpVtbl -> ReleasePointers(This)

#define IEpDirRtr_VerifyCritical(This,ptType,ulInterpID,bstrCritical)	\
    (This)->lpVtbl -> VerifyCritical(This,ptType,ulInterpID,bstrCritical)

#define IEpDirRtr_VerifyPTV(This,ptType,ulInterpID,bstrPTV)	\
    (This)->lpVtbl -> VerifyPTV(This,ptType,ulInterpID,bstrPTV)

#define IEpDirRtr_VerifyProtected(This,bstrProtected,pbstrUserName)	\
    (This)->lpVtbl -> VerifyProtected(This,bstrProtected,pbstrUserName)

#define IEpDirRtr_GetProcList(This,pbstrProcs)	\
    (This)->lpVtbl -> GetProcList(This,pbstrProcs)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_Connect_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ BSTR bstrPCUser,
    /* [in] */ BSTR bstrStream,
    /* [in] */ IUnknown __RPC_FAR *pUnkSink,
    /* [retval][out] */ ULONG __RPC_FAR *pulRtrHandle);


void __RPC_STUB IEpDirRtr_Connect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_Disconnect_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ ULONG ulRtrHandle);


void __RPC_STUB IEpDirRtr_Disconnect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_ExecuteDirective_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ ULONG ulRtrHandle,
    /* [in] */ BSTR bstrDir,
    /* [in] */ SHORT sPriority,
    /* [in] */ ULONG ulUserData);


void __RPC_STUB IEpDirRtr_ExecuteDirective_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_put_Parent_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ EPDRP newVal);


void __RPC_STUB IEpDirRtr_put_Parent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_put_IEpViewer_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ IDispatch __RPC_FAR *newVal);


void __RPC_STUB IEpDirRtr_put_IEpViewer_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propput] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_put_IEpSTOLApp_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ IDispatch __RPC_FAR *newVal);


void __RPC_STUB IEpDirRtr_put_IEpSTOLApp_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_GetEpIUnk_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ short sWhichIUnk,
    /* [in] */ BOOL bInstantiate,
    /* [retval][out] */ IUnknown __RPC_FAR *__RPC_FAR *ppIUnk);


void __RPC_STUB IEpDirRtr_GetEpIUnk_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_SetEpIUnk_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ short sWhichIUnk,
    /* [in] */ IUnknown __RPC_FAR *pIUnk);


void __RPC_STUB IEpDirRtr_SetEpIUnk_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_ReleasePointers_Proxy( 
    IEpDirRtr __RPC_FAR * This);


void __RPC_STUB IEpDirRtr_ReleasePointers_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_VerifyCritical_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ int ptType,
    /* [in] */ ULONG ulInterpID,
    /* [in] */ BSTR bstrCritical);


void __RPC_STUB IEpDirRtr_VerifyCritical_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_VerifyPTV_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ int ptType,
    /* [in] */ ULONG ulInterpID,
    /* [in] */ BSTR bstrPTV);


void __RPC_STUB IEpDirRtr_VerifyPTV_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_VerifyProtected_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [in] */ BSTR bstrProtected,
    /* [out] */ BSTR __RPC_FAR *pbstrUserName);


void __RPC_STUB IEpDirRtr_VerifyProtected_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IEpDirRtr_GetProcList_Proxy( 
    IEpDirRtr __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrProcs);


void __RPC_STUB IEpDirRtr_GetProcList_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpDirRtr_INTERFACE_DEFINED__ */



#ifndef __EPDR2Lib_LIBRARY_DEFINED__
#define __EPDR2Lib_LIBRARY_DEFINED__

/* library EPDR2Lib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_EPDR2Lib;

EXTERN_C const CLSID CLSID_EpDirRtr2;

#ifdef __cplusplus

class DECLSPEC_UUID("D6CED88A-2B89-4659-826A-6D88186C182F")
EpDirRtr2;
#endif
#endif /* __EPDR2Lib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
