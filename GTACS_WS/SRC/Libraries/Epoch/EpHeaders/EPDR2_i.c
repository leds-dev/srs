/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Wed Mar 06 07:55:31 2013
 */
/* Compiler settings for EPDR2.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IEpDirRtr = {0xAD528EB7,0xC9E6,0x11D3,{0xBB,0x08,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID LIBID_EPDR2Lib = {0x393FAB79,0xE2A6,0x44DD,{0x89,0xEC,0x5B,0x40,0x4A,0x31,0x90,0x31}};


const CLSID CLSID_EpDirRtr2 = {0xD6CED88A,0x2B89,0x4659,{0x82,0x6A,0x6D,0x88,0x18,0x6C,0x18,0x2F}};


#ifdef __cplusplus
}
#endif

