/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Wed Mar 06 07:55:30 2013
 */
/* Compiler settings for EpCtrlIDL.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __EpCtrlIDL_h__
#define __EpCtrlIDL_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IEpCtrl_FWD_DEFINED__
#define __IEpCtrl_FWD_DEFINED__
typedef interface IEpCtrl IEpCtrl;
#endif 	/* __IEpCtrl_FWD_DEFINED__ */


#ifndef ___IEpCtrlEvents_FWD_DEFINED__
#define ___IEpCtrlEvents_FWD_DEFINED__
typedef interface _IEpCtrlEvents _IEpCtrlEvents;
#endif 	/* ___IEpCtrlEvents_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "epcommon.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

/* interface __MIDL_itf_EpCtrlIDL_0000 */
/* [local] */ 




extern RPC_IF_HANDLE __MIDL_itf_EpCtrlIDL_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_EpCtrlIDL_0000_v0_0_s_ifspec;

#ifndef __IEpCtrl_INTERFACE_DEFINED__
#define __IEpCtrl_INTERFACE_DEFINED__

/* interface IEpCtrl */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IEpCtrl;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("BC90855B-E8FC-11d2-A33C-00600867A0E2")
    IEpCtrl : public IDispatch
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMaxNumItems( 
            /* [retval][out] */ short __RPC_FAR *psNumItems) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetNumItems( 
            /* [retval][out] */ short __RPC_FAR *psNumItems) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetNumItems( 
            /* [in] */ short sNumItems) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCtrlName( 
            /* [retval][out] */ BSTR __RPC_FAR *pbstrName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetCtrlName( 
            /* [in] */ BSTR bstrName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPageDefStrm( 
            /* [retval][out] */ BSTR __RPC_FAR *pbstrStrmName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetPageDefStrm( 
            /* [in] */ BSTR bstrStrmName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCustStrm( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrStrmName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetCustStrm( 
            /* [in] */ short sItemIndex,
            /* [in] */ BSTR bstrStrmName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMnem( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrMnem) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetMnem( 
            /* [in] */ short sItemIndex,
            /* [in] */ BSTR bstrMnem) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetType( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPPTVARIETY __RPC_FAR *pePV) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetType( 
            /* [in] */ short sItemIndex,
            /* [in] */ EPPTVARIETY ePV) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetConv( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPDISPCONV __RPC_FAR *peCT) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetConv( 
            /* [in] */ short sItemIndex,
            /* [in] */ EPDISPCONV eCT) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDispFrmt( 
            /* [in] */ short sItemIndex,
            /* [in] */ EPDISPFRMT eDF,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrDF) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetDispFrmt( 
            /* [in] */ short sItemIndex,
            /* [in] */ EPDISPFRMT eDF,
            /* [in] */ BSTR bstrDF) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetDispOpts( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ ULONG __RPC_FAR *pulOptFlags) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetDispOpts( 
            /* [in] */ short sItemIndex,
            /* [in] */ ULONG ulOptFlags) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSampDefault( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ BOOL __RPC_FAR *pBUseDef) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetSampDefault( 
            /* [in] */ short sItemIndex,
            /* [in] */ BOOL bUseDef) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSampPolicy( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPITEMSP __RPC_FAR *peSP) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetSampPolicy( 
            /* [in] */ short sItemIndex,
            /* [in] */ EPITEMSP eSP) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSampRate( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ LONG __RPC_FAR *plmilliSecRate) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetSampRate( 
            /* [in] */ short sItemIndex,
            /* [in] */ LONG lmilliSecRate) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetStringVal( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrStringVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetStringVal( 
            /* [in] */ short sItemIndex,
            /* [in] */ BSTR bstrStringVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetLongVal( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ LONG __RPC_FAR *plVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetLongVal( 
            /* [in] */ short sItemIndex,
            /* [in] */ LONG lVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetEvntSrvcType( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPEVTSERVICE __RPC_FAR *peEST) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetEvntSrvcType( 
            /* [in] */ short sItemIndex,
            /* [in] */ EPEVTSERVICE eEST) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetPointCrossRef( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ LONG __RPC_FAR *plCrossRef) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetPointCrossRef( 
            /* [in] */ short sItemIndex,
            /* [in] */ LONG lCrossRef) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetLimitBandTypes( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ LONG __RPC_FAR *plELT) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetLimitBandTypes( 
            /* [in] */ short sItemIndex,
            /* [in] */ LONG eELT) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetLimitBandUsage( 
            /* [in] */ short sBandType,
            /* [retval][out] */ ULONG __RPC_FAR *plBandUsage) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetLimitBandUsage( 
            /* [in] */ short __RPC_FAR *cBandUsages) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EpDataIsSet_InitializeItem( 
            /* [in] */ short sItemIndex) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Connect( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ConnectItem( 
            /* [in] */ short sItemIndex) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Disconnect( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE DisconnectItem( 
            /* [in] */ short sItemIndex) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Freeze( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Thaw( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetItemState( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPCONSTATE __RPC_FAR *peState) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE IsItemDirty( 
            /* [in] */ short sItemIndex,
            /* [retval][out] */ BOOL __RPC_FAR *bDirty) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetFirstSelItem( 
            /* [retval][out] */ short __RPC_FAR *psItemIndex) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetNextSelItem( 
            /* [retval][out] */ short __RPC_FAR *psItemIndex) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetNumMenus( 
            /* [retval][out] */ short __RPC_FAR *psNumMenus) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMenuText( 
            /* [in] */ short sMenuIndex,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrMenuText) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetMenuFlags( 
            /* [in] */ short sMenuIndex,
            /* [retval][out] */ ULONG __RPC_FAR *pulFlags) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ExecuteMenuItem( 
            /* [in] */ short sMenuIndex) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ExecuteDirective( 
            /* [in] */ short sItemIndex,
            /* [in] */ BOOL bNoWait) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SendEvent( 
            /* [in] */ short sItemIndex) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ShowEpAttribDlg( 
            /* [in] */ EPATTRIB_PAGE ePage,
            /* [in] */ short sEditItemIndex,
            /* [in] */ LONG lMaskFocus,
            /* [in] */ LONG hWndParent) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ShowCtrlPropDlg( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetIntrnlCtrlDisp( 
            /* [retval][out] */ IDispatch __RPC_FAR *__RPC_FAR *ppIDispatch) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetNumCRs( 
            /* [in] */ short sNumCRs) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetNumCRs( 
            /* [retval][out] */ short __RPC_FAR *psNumCRs) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetCR( 
            /* [in] */ short sCRIndex,
            /* [in] */ short sItemIndex,
            /* [in] */ short sCompType,
            /* [in] */ VARIANT varLowEndPt,
            /* [in] */ BOOL bIncLow,
            /* [in] */ VARIANT varHighEndPt,
            /* [in] */ BOOL bIncHigh,
            /* [in] */ LONG lExtra) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCRItemIndex( 
            /* [in] */ short sCRIndex,
            /* [retval][out] */ short __RPC_FAR *psItemIndex) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCRCompType( 
            /* [in] */ short sCRIndex,
            /* [retval][out] */ short __RPC_FAR *psCompType) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCRLowEndPt( 
            /* [in] */ short sCRIndex,
            /* [retval][out] */ VARIANT __RPC_FAR *pvarLowEndPoint) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCRIncLow( 
            /* [in] */ short sCRIndex,
            /* [retval][out] */ BOOL __RPC_FAR *pbIncLow) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCRHighEndPt( 
            /* [in] */ short sCRIndex,
            /* [retval][out] */ VARIANT __RPC_FAR *pvarHighEndPoint) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCRIncHigh( 
            /* [in] */ short sCRIndex,
            /* [retval][out] */ BOOL __RPC_FAR *pbIncHigh) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetCRExtra( 
            /* [in] */ short sCRIndex,
            /* [retval][out] */ LONG __RPC_FAR *plExtra) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ShowErrDlgs( 
            /* [in] */ BOOL bShow) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE IsConnectCalled( 
            /* [retval][out] */ BOOL __RPC_FAR *bConnectCalled) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IEpCtrlVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IEpCtrl __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IEpCtrl __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IEpCtrl __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetMaxNumItems )( 
            IEpCtrl __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *psNumItems);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNumItems )( 
            IEpCtrl __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *psNumItems);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetNumItems )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sNumItems);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCtrlName )( 
            IEpCtrl __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetCtrlName )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ BSTR bstrName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPageDefStrm )( 
            IEpCtrl __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrStrmName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetPageDefStrm )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ BSTR bstrStrmName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCustStrm )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrStrmName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetCustStrm )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ BSTR bstrStrmName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetMnem )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrMnem);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetMnem )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ BSTR bstrMnem);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetType )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPPTVARIETY __RPC_FAR *pePV);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetType )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ EPPTVARIETY ePV);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetConv )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPDISPCONV __RPC_FAR *peCT);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetConv )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ EPDISPCONV eCT);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDispFrmt )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ EPDISPFRMT eDF,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrDF);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetDispFrmt )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ EPDISPFRMT eDF,
            /* [in] */ BSTR bstrDF);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetDispOpts )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ ULONG __RPC_FAR *pulOptFlags);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetDispOpts )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ ULONG ulOptFlags);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetSampDefault )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ BOOL __RPC_FAR *pBUseDef);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetSampDefault )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ BOOL bUseDef);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetSampPolicy )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPITEMSP __RPC_FAR *peSP);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetSampPolicy )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ EPITEMSP eSP);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetSampRate )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ LONG __RPC_FAR *plmilliSecRate);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetSampRate )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ LONG lmilliSecRate);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetStringVal )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrStringVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetStringVal )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ BSTR bstrStringVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetLongVal )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ LONG __RPC_FAR *plVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetLongVal )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ LONG lVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetEvntSrvcType )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPEVTSERVICE __RPC_FAR *peEST);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetEvntSrvcType )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ EPEVTSERVICE eEST);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetPointCrossRef )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ LONG __RPC_FAR *plCrossRef);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetPointCrossRef )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ LONG lCrossRef);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetLimitBandTypes )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ LONG __RPC_FAR *plELT);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetLimitBandTypes )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ LONG eELT);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetLimitBandUsage )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sBandType,
            /* [retval][out] */ ULONG __RPC_FAR *plBandUsage);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetLimitBandUsage )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short __RPC_FAR *cBandUsages);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EpDataIsSet_InitializeItem )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Connect )( 
            IEpCtrl __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ConnectItem )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Disconnect )( 
            IEpCtrl __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *DisconnectItem )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Freeze )( 
            IEpCtrl __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Thaw )( 
            IEpCtrl __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetItemState )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ EPCONSTATE __RPC_FAR *peState);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *IsItemDirty )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [retval][out] */ BOOL __RPC_FAR *bDirty);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetFirstSelItem )( 
            IEpCtrl __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *psItemIndex);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNextSelItem )( 
            IEpCtrl __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *psItemIndex);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNumMenus )( 
            IEpCtrl __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *psNumMenus);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetMenuText )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sMenuIndex,
            /* [retval][out] */ BSTR __RPC_FAR *pbstrMenuText);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetMenuFlags )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sMenuIndex,
            /* [retval][out] */ ULONG __RPC_FAR *pulFlags);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExecuteMenuItem )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sMenuIndex);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ExecuteDirective )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex,
            /* [in] */ BOOL bNoWait);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SendEvent )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sItemIndex);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ShowEpAttribDlg )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ EPATTRIB_PAGE ePage,
            /* [in] */ short sEditItemIndex,
            /* [in] */ LONG lMaskFocus,
            /* [in] */ LONG hWndParent);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ShowCtrlPropDlg )( 
            IEpCtrl __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIntrnlCtrlDisp )( 
            IEpCtrl __RPC_FAR * This,
            /* [retval][out] */ IDispatch __RPC_FAR *__RPC_FAR *ppIDispatch);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetNumCRs )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sNumCRs);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetNumCRs )( 
            IEpCtrl __RPC_FAR * This,
            /* [retval][out] */ short __RPC_FAR *psNumCRs);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetCR )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sCRIndex,
            /* [in] */ short sItemIndex,
            /* [in] */ short sCompType,
            /* [in] */ VARIANT varLowEndPt,
            /* [in] */ BOOL bIncLow,
            /* [in] */ VARIANT varHighEndPt,
            /* [in] */ BOOL bIncHigh,
            /* [in] */ LONG lExtra);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCRItemIndex )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sCRIndex,
            /* [retval][out] */ short __RPC_FAR *psItemIndex);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCRCompType )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sCRIndex,
            /* [retval][out] */ short __RPC_FAR *psCompType);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCRLowEndPt )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sCRIndex,
            /* [retval][out] */ VARIANT __RPC_FAR *pvarLowEndPoint);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCRIncLow )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sCRIndex,
            /* [retval][out] */ BOOL __RPC_FAR *pbIncLow);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCRHighEndPt )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sCRIndex,
            /* [retval][out] */ VARIANT __RPC_FAR *pvarHighEndPoint);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCRIncHigh )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sCRIndex,
            /* [retval][out] */ BOOL __RPC_FAR *pbIncHigh);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetCRExtra )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ short sCRIndex,
            /* [retval][out] */ LONG __RPC_FAR *plExtra);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ShowErrDlgs )( 
            IEpCtrl __RPC_FAR * This,
            /* [in] */ BOOL bShow);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *IsConnectCalled )( 
            IEpCtrl __RPC_FAR * This,
            /* [retval][out] */ BOOL __RPC_FAR *bConnectCalled);
        
        END_INTERFACE
    } IEpCtrlVtbl;

    interface IEpCtrl
    {
        CONST_VTBL struct IEpCtrlVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IEpCtrl_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IEpCtrl_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IEpCtrl_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IEpCtrl_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IEpCtrl_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IEpCtrl_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IEpCtrl_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IEpCtrl_GetMaxNumItems(This,psNumItems)	\
    (This)->lpVtbl -> GetMaxNumItems(This,psNumItems)

#define IEpCtrl_GetNumItems(This,psNumItems)	\
    (This)->lpVtbl -> GetNumItems(This,psNumItems)

#define IEpCtrl_SetNumItems(This,sNumItems)	\
    (This)->lpVtbl -> SetNumItems(This,sNumItems)

#define IEpCtrl_GetCtrlName(This,pbstrName)	\
    (This)->lpVtbl -> GetCtrlName(This,pbstrName)

#define IEpCtrl_SetCtrlName(This,bstrName)	\
    (This)->lpVtbl -> SetCtrlName(This,bstrName)

#define IEpCtrl_GetPageDefStrm(This,pbstrStrmName)	\
    (This)->lpVtbl -> GetPageDefStrm(This,pbstrStrmName)

#define IEpCtrl_SetPageDefStrm(This,bstrStrmName)	\
    (This)->lpVtbl -> SetPageDefStrm(This,bstrStrmName)

#define IEpCtrl_GetCustStrm(This,sItemIndex,pbstrStrmName)	\
    (This)->lpVtbl -> GetCustStrm(This,sItemIndex,pbstrStrmName)

#define IEpCtrl_SetCustStrm(This,sItemIndex,bstrStrmName)	\
    (This)->lpVtbl -> SetCustStrm(This,sItemIndex,bstrStrmName)

#define IEpCtrl_GetMnem(This,sItemIndex,pbstrMnem)	\
    (This)->lpVtbl -> GetMnem(This,sItemIndex,pbstrMnem)

#define IEpCtrl_SetMnem(This,sItemIndex,bstrMnem)	\
    (This)->lpVtbl -> SetMnem(This,sItemIndex,bstrMnem)

#define IEpCtrl_GetType(This,sItemIndex,pePV)	\
    (This)->lpVtbl -> GetType(This,sItemIndex,pePV)

#define IEpCtrl_SetType(This,sItemIndex,ePV)	\
    (This)->lpVtbl -> SetType(This,sItemIndex,ePV)

#define IEpCtrl_GetConv(This,sItemIndex,peCT)	\
    (This)->lpVtbl -> GetConv(This,sItemIndex,peCT)

#define IEpCtrl_SetConv(This,sItemIndex,eCT)	\
    (This)->lpVtbl -> SetConv(This,sItemIndex,eCT)

#define IEpCtrl_GetDispFrmt(This,sItemIndex,eDF,pbstrDF)	\
    (This)->lpVtbl -> GetDispFrmt(This,sItemIndex,eDF,pbstrDF)

#define IEpCtrl_SetDispFrmt(This,sItemIndex,eDF,bstrDF)	\
    (This)->lpVtbl -> SetDispFrmt(This,sItemIndex,eDF,bstrDF)

#define IEpCtrl_GetDispOpts(This,sItemIndex,pulOptFlags)	\
    (This)->lpVtbl -> GetDispOpts(This,sItemIndex,pulOptFlags)

#define IEpCtrl_SetDispOpts(This,sItemIndex,ulOptFlags)	\
    (This)->lpVtbl -> SetDispOpts(This,sItemIndex,ulOptFlags)

#define IEpCtrl_GetSampDefault(This,sItemIndex,pBUseDef)	\
    (This)->lpVtbl -> GetSampDefault(This,sItemIndex,pBUseDef)

#define IEpCtrl_SetSampDefault(This,sItemIndex,bUseDef)	\
    (This)->lpVtbl -> SetSampDefault(This,sItemIndex,bUseDef)

#define IEpCtrl_GetSampPolicy(This,sItemIndex,peSP)	\
    (This)->lpVtbl -> GetSampPolicy(This,sItemIndex,peSP)

#define IEpCtrl_SetSampPolicy(This,sItemIndex,eSP)	\
    (This)->lpVtbl -> SetSampPolicy(This,sItemIndex,eSP)

#define IEpCtrl_GetSampRate(This,sItemIndex,plmilliSecRate)	\
    (This)->lpVtbl -> GetSampRate(This,sItemIndex,plmilliSecRate)

#define IEpCtrl_SetSampRate(This,sItemIndex,lmilliSecRate)	\
    (This)->lpVtbl -> SetSampRate(This,sItemIndex,lmilliSecRate)

#define IEpCtrl_GetStringVal(This,sItemIndex,pbstrStringVal)	\
    (This)->lpVtbl -> GetStringVal(This,sItemIndex,pbstrStringVal)

#define IEpCtrl_SetStringVal(This,sItemIndex,bstrStringVal)	\
    (This)->lpVtbl -> SetStringVal(This,sItemIndex,bstrStringVal)

#define IEpCtrl_GetLongVal(This,sItemIndex,plVal)	\
    (This)->lpVtbl -> GetLongVal(This,sItemIndex,plVal)

#define IEpCtrl_SetLongVal(This,sItemIndex,lVal)	\
    (This)->lpVtbl -> SetLongVal(This,sItemIndex,lVal)

#define IEpCtrl_GetEvntSrvcType(This,sItemIndex,peEST)	\
    (This)->lpVtbl -> GetEvntSrvcType(This,sItemIndex,peEST)

#define IEpCtrl_SetEvntSrvcType(This,sItemIndex,eEST)	\
    (This)->lpVtbl -> SetEvntSrvcType(This,sItemIndex,eEST)

#define IEpCtrl_GetPointCrossRef(This,sItemIndex,plCrossRef)	\
    (This)->lpVtbl -> GetPointCrossRef(This,sItemIndex,plCrossRef)

#define IEpCtrl_SetPointCrossRef(This,sItemIndex,lCrossRef)	\
    (This)->lpVtbl -> SetPointCrossRef(This,sItemIndex,lCrossRef)

#define IEpCtrl_GetLimitBandTypes(This,sItemIndex,plELT)	\
    (This)->lpVtbl -> GetLimitBandTypes(This,sItemIndex,plELT)

#define IEpCtrl_SetLimitBandTypes(This,sItemIndex,eELT)	\
    (This)->lpVtbl -> SetLimitBandTypes(This,sItemIndex,eELT)

#define IEpCtrl_GetLimitBandUsage(This,sBandType,plBandUsage)	\
    (This)->lpVtbl -> GetLimitBandUsage(This,sBandType,plBandUsage)

#define IEpCtrl_SetLimitBandUsage(This,cBandUsages)	\
    (This)->lpVtbl -> SetLimitBandUsage(This,cBandUsages)

#define IEpCtrl_EpDataIsSet_InitializeItem(This,sItemIndex)	\
    (This)->lpVtbl -> EpDataIsSet_InitializeItem(This,sItemIndex)

#define IEpCtrl_Connect(This)	\
    (This)->lpVtbl -> Connect(This)

#define IEpCtrl_ConnectItem(This,sItemIndex)	\
    (This)->lpVtbl -> ConnectItem(This,sItemIndex)

#define IEpCtrl_Disconnect(This)	\
    (This)->lpVtbl -> Disconnect(This)

#define IEpCtrl_DisconnectItem(This,sItemIndex)	\
    (This)->lpVtbl -> DisconnectItem(This,sItemIndex)

#define IEpCtrl_Freeze(This)	\
    (This)->lpVtbl -> Freeze(This)

#define IEpCtrl_Thaw(This)	\
    (This)->lpVtbl -> Thaw(This)

#define IEpCtrl_GetItemState(This,sItemIndex,peState)	\
    (This)->lpVtbl -> GetItemState(This,sItemIndex,peState)

#define IEpCtrl_IsItemDirty(This,sItemIndex,bDirty)	\
    (This)->lpVtbl -> IsItemDirty(This,sItemIndex,bDirty)

#define IEpCtrl_GetFirstSelItem(This,psItemIndex)	\
    (This)->lpVtbl -> GetFirstSelItem(This,psItemIndex)

#define IEpCtrl_GetNextSelItem(This,psItemIndex)	\
    (This)->lpVtbl -> GetNextSelItem(This,psItemIndex)

#define IEpCtrl_GetNumMenus(This,psNumMenus)	\
    (This)->lpVtbl -> GetNumMenus(This,psNumMenus)

#define IEpCtrl_GetMenuText(This,sMenuIndex,pbstrMenuText)	\
    (This)->lpVtbl -> GetMenuText(This,sMenuIndex,pbstrMenuText)

#define IEpCtrl_GetMenuFlags(This,sMenuIndex,pulFlags)	\
    (This)->lpVtbl -> GetMenuFlags(This,sMenuIndex,pulFlags)

#define IEpCtrl_ExecuteMenuItem(This,sMenuIndex)	\
    (This)->lpVtbl -> ExecuteMenuItem(This,sMenuIndex)

#define IEpCtrl_ExecuteDirective(This,sItemIndex,bNoWait)	\
    (This)->lpVtbl -> ExecuteDirective(This,sItemIndex,bNoWait)

#define IEpCtrl_SendEvent(This,sItemIndex)	\
    (This)->lpVtbl -> SendEvent(This,sItemIndex)

#define IEpCtrl_ShowEpAttribDlg(This,ePage,sEditItemIndex,lMaskFocus,hWndParent)	\
    (This)->lpVtbl -> ShowEpAttribDlg(This,ePage,sEditItemIndex,lMaskFocus,hWndParent)

#define IEpCtrl_ShowCtrlPropDlg(This)	\
    (This)->lpVtbl -> ShowCtrlPropDlg(This)

#define IEpCtrl_GetIntrnlCtrlDisp(This,ppIDispatch)	\
    (This)->lpVtbl -> GetIntrnlCtrlDisp(This,ppIDispatch)

#define IEpCtrl_SetNumCRs(This,sNumCRs)	\
    (This)->lpVtbl -> SetNumCRs(This,sNumCRs)

#define IEpCtrl_GetNumCRs(This,psNumCRs)	\
    (This)->lpVtbl -> GetNumCRs(This,psNumCRs)

#define IEpCtrl_SetCR(This,sCRIndex,sItemIndex,sCompType,varLowEndPt,bIncLow,varHighEndPt,bIncHigh,lExtra)	\
    (This)->lpVtbl -> SetCR(This,sCRIndex,sItemIndex,sCompType,varLowEndPt,bIncLow,varHighEndPt,bIncHigh,lExtra)

#define IEpCtrl_GetCRItemIndex(This,sCRIndex,psItemIndex)	\
    (This)->lpVtbl -> GetCRItemIndex(This,sCRIndex,psItemIndex)

#define IEpCtrl_GetCRCompType(This,sCRIndex,psCompType)	\
    (This)->lpVtbl -> GetCRCompType(This,sCRIndex,psCompType)

#define IEpCtrl_GetCRLowEndPt(This,sCRIndex,pvarLowEndPoint)	\
    (This)->lpVtbl -> GetCRLowEndPt(This,sCRIndex,pvarLowEndPoint)

#define IEpCtrl_GetCRIncLow(This,sCRIndex,pbIncLow)	\
    (This)->lpVtbl -> GetCRIncLow(This,sCRIndex,pbIncLow)

#define IEpCtrl_GetCRHighEndPt(This,sCRIndex,pvarHighEndPoint)	\
    (This)->lpVtbl -> GetCRHighEndPt(This,sCRIndex,pvarHighEndPoint)

#define IEpCtrl_GetCRIncHigh(This,sCRIndex,pbIncHigh)	\
    (This)->lpVtbl -> GetCRIncHigh(This,sCRIndex,pbIncHigh)

#define IEpCtrl_GetCRExtra(This,sCRIndex,plExtra)	\
    (This)->lpVtbl -> GetCRExtra(This,sCRIndex,plExtra)

#define IEpCtrl_ShowErrDlgs(This,bShow)	\
    (This)->lpVtbl -> ShowErrDlgs(This,bShow)

#define IEpCtrl_IsConnectCalled(This,bConnectCalled)	\
    (This)->lpVtbl -> IsConnectCalled(This,bConnectCalled)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetMaxNumItems_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *psNumItems);


void __RPC_STUB IEpCtrl_GetMaxNumItems_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetNumItems_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *psNumItems);


void __RPC_STUB IEpCtrl_GetNumItems_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetNumItems_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sNumItems);


void __RPC_STUB IEpCtrl_SetNumItems_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetCtrlName_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrName);


void __RPC_STUB IEpCtrl_GetCtrlName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetCtrlName_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ BSTR bstrName);


void __RPC_STUB IEpCtrl_SetCtrlName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetPageDefStrm_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrStrmName);


void __RPC_STUB IEpCtrl_GetPageDefStrm_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetPageDefStrm_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ BSTR bstrStrmName);


void __RPC_STUB IEpCtrl_SetPageDefStrm_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetCustStrm_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrStrmName);


void __RPC_STUB IEpCtrl_GetCustStrm_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetCustStrm_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ BSTR bstrStrmName);


void __RPC_STUB IEpCtrl_SetCustStrm_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetMnem_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrMnem);


void __RPC_STUB IEpCtrl_GetMnem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetMnem_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ BSTR bstrMnem);


void __RPC_STUB IEpCtrl_SetMnem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetType_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ EPPTVARIETY __RPC_FAR *pePV);


void __RPC_STUB IEpCtrl_GetType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetType_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ EPPTVARIETY ePV);


void __RPC_STUB IEpCtrl_SetType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetConv_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ EPDISPCONV __RPC_FAR *peCT);


void __RPC_STUB IEpCtrl_GetConv_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetConv_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ EPDISPCONV eCT);


void __RPC_STUB IEpCtrl_SetConv_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetDispFrmt_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ EPDISPFRMT eDF,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrDF);


void __RPC_STUB IEpCtrl_GetDispFrmt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetDispFrmt_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ EPDISPFRMT eDF,
    /* [in] */ BSTR bstrDF);


void __RPC_STUB IEpCtrl_SetDispFrmt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetDispOpts_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ ULONG __RPC_FAR *pulOptFlags);


void __RPC_STUB IEpCtrl_GetDispOpts_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetDispOpts_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ ULONG ulOptFlags);


void __RPC_STUB IEpCtrl_SetDispOpts_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetSampDefault_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ BOOL __RPC_FAR *pBUseDef);


void __RPC_STUB IEpCtrl_GetSampDefault_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetSampDefault_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ BOOL bUseDef);


void __RPC_STUB IEpCtrl_SetSampDefault_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetSampPolicy_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ EPITEMSP __RPC_FAR *peSP);


void __RPC_STUB IEpCtrl_GetSampPolicy_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetSampPolicy_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ EPITEMSP eSP);


void __RPC_STUB IEpCtrl_SetSampPolicy_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetSampRate_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ LONG __RPC_FAR *plmilliSecRate);


void __RPC_STUB IEpCtrl_GetSampRate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetSampRate_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ LONG lmilliSecRate);


void __RPC_STUB IEpCtrl_SetSampRate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetStringVal_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrStringVal);


void __RPC_STUB IEpCtrl_GetStringVal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetStringVal_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ BSTR bstrStringVal);


void __RPC_STUB IEpCtrl_SetStringVal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetLongVal_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ LONG __RPC_FAR *plVal);


void __RPC_STUB IEpCtrl_GetLongVal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetLongVal_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ LONG lVal);


void __RPC_STUB IEpCtrl_SetLongVal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetEvntSrvcType_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ EPEVTSERVICE __RPC_FAR *peEST);


void __RPC_STUB IEpCtrl_GetEvntSrvcType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetEvntSrvcType_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ EPEVTSERVICE eEST);


void __RPC_STUB IEpCtrl_SetEvntSrvcType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetPointCrossRef_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ LONG __RPC_FAR *plCrossRef);


void __RPC_STUB IEpCtrl_GetPointCrossRef_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetPointCrossRef_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ LONG lCrossRef);


void __RPC_STUB IEpCtrl_SetPointCrossRef_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetLimitBandTypes_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ LONG __RPC_FAR *plELT);


void __RPC_STUB IEpCtrl_GetLimitBandTypes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetLimitBandTypes_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ LONG eELT);


void __RPC_STUB IEpCtrl_SetLimitBandTypes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetLimitBandUsage_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sBandType,
    /* [retval][out] */ ULONG __RPC_FAR *plBandUsage);


void __RPC_STUB IEpCtrl_GetLimitBandUsage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetLimitBandUsage_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short __RPC_FAR *cBandUsages);


void __RPC_STUB IEpCtrl_SetLimitBandUsage_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_EpDataIsSet_InitializeItem_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex);


void __RPC_STUB IEpCtrl_EpDataIsSet_InitializeItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_Connect_Proxy( 
    IEpCtrl __RPC_FAR * This);


void __RPC_STUB IEpCtrl_Connect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_ConnectItem_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex);


void __RPC_STUB IEpCtrl_ConnectItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_Disconnect_Proxy( 
    IEpCtrl __RPC_FAR * This);


void __RPC_STUB IEpCtrl_Disconnect_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_DisconnectItem_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex);


void __RPC_STUB IEpCtrl_DisconnectItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_Freeze_Proxy( 
    IEpCtrl __RPC_FAR * This);


void __RPC_STUB IEpCtrl_Freeze_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_Thaw_Proxy( 
    IEpCtrl __RPC_FAR * This);


void __RPC_STUB IEpCtrl_Thaw_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetItemState_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ EPCONSTATE __RPC_FAR *peState);


void __RPC_STUB IEpCtrl_GetItemState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_IsItemDirty_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [retval][out] */ BOOL __RPC_FAR *bDirty);


void __RPC_STUB IEpCtrl_IsItemDirty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetFirstSelItem_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *psItemIndex);


void __RPC_STUB IEpCtrl_GetFirstSelItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetNextSelItem_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *psItemIndex);


void __RPC_STUB IEpCtrl_GetNextSelItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetNumMenus_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *psNumMenus);


void __RPC_STUB IEpCtrl_GetNumMenus_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetMenuText_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sMenuIndex,
    /* [retval][out] */ BSTR __RPC_FAR *pbstrMenuText);


void __RPC_STUB IEpCtrl_GetMenuText_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetMenuFlags_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sMenuIndex,
    /* [retval][out] */ ULONG __RPC_FAR *pulFlags);


void __RPC_STUB IEpCtrl_GetMenuFlags_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_ExecuteMenuItem_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sMenuIndex);


void __RPC_STUB IEpCtrl_ExecuteMenuItem_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_ExecuteDirective_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex,
    /* [in] */ BOOL bNoWait);


void __RPC_STUB IEpCtrl_ExecuteDirective_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SendEvent_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sItemIndex);


void __RPC_STUB IEpCtrl_SendEvent_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_ShowEpAttribDlg_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ EPATTRIB_PAGE ePage,
    /* [in] */ short sEditItemIndex,
    /* [in] */ LONG lMaskFocus,
    /* [in] */ LONG hWndParent);


void __RPC_STUB IEpCtrl_ShowEpAttribDlg_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_ShowCtrlPropDlg_Proxy( 
    IEpCtrl __RPC_FAR * This);


void __RPC_STUB IEpCtrl_ShowCtrlPropDlg_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetIntrnlCtrlDisp_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [retval][out] */ IDispatch __RPC_FAR *__RPC_FAR *ppIDispatch);


void __RPC_STUB IEpCtrl_GetIntrnlCtrlDisp_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetNumCRs_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sNumCRs);


void __RPC_STUB IEpCtrl_SetNumCRs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetNumCRs_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [retval][out] */ short __RPC_FAR *psNumCRs);


void __RPC_STUB IEpCtrl_GetNumCRs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_SetCR_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sCRIndex,
    /* [in] */ short sItemIndex,
    /* [in] */ short sCompType,
    /* [in] */ VARIANT varLowEndPt,
    /* [in] */ BOOL bIncLow,
    /* [in] */ VARIANT varHighEndPt,
    /* [in] */ BOOL bIncHigh,
    /* [in] */ LONG lExtra);


void __RPC_STUB IEpCtrl_SetCR_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetCRItemIndex_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sCRIndex,
    /* [retval][out] */ short __RPC_FAR *psItemIndex);


void __RPC_STUB IEpCtrl_GetCRItemIndex_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetCRCompType_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sCRIndex,
    /* [retval][out] */ short __RPC_FAR *psCompType);


void __RPC_STUB IEpCtrl_GetCRCompType_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetCRLowEndPt_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sCRIndex,
    /* [retval][out] */ VARIANT __RPC_FAR *pvarLowEndPoint);


void __RPC_STUB IEpCtrl_GetCRLowEndPt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetCRIncLow_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sCRIndex,
    /* [retval][out] */ BOOL __RPC_FAR *pbIncLow);


void __RPC_STUB IEpCtrl_GetCRIncLow_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetCRHighEndPt_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sCRIndex,
    /* [retval][out] */ VARIANT __RPC_FAR *pvarHighEndPoint);


void __RPC_STUB IEpCtrl_GetCRHighEndPt_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetCRIncHigh_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sCRIndex,
    /* [retval][out] */ BOOL __RPC_FAR *pbIncHigh);


void __RPC_STUB IEpCtrl_GetCRIncHigh_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_GetCRExtra_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ short sCRIndex,
    /* [retval][out] */ LONG __RPC_FAR *plExtra);


void __RPC_STUB IEpCtrl_GetCRExtra_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_ShowErrDlgs_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [in] */ BOOL bShow);


void __RPC_STUB IEpCtrl_ShowErrDlgs_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IEpCtrl_IsConnectCalled_Proxy( 
    IEpCtrl __RPC_FAR * This,
    /* [retval][out] */ BOOL __RPC_FAR *bConnectCalled);


void __RPC_STUB IEpCtrl_IsConnectCalled_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IEpCtrl_INTERFACE_DEFINED__ */



#ifndef __EPCTRLEVENTSLib_LIBRARY_DEFINED__
#define __EPCTRLEVENTSLib_LIBRARY_DEFINED__

/* library EPCTRLEVENTSLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_EPCTRLEVENTSLib;

#ifndef ___IEpCtrlEvents_DISPINTERFACE_DEFINED__
#define ___IEpCtrlEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IEpCtrlEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IEpCtrlEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("3E245661-1395-11d3-A34A-00600867A0E2")
    _IEpCtrlEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IEpCtrlEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            _IEpCtrlEvents __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            _IEpCtrlEvents __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            _IEpCtrlEvents __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            _IEpCtrlEvents __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            _IEpCtrlEvents __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            _IEpCtrlEvents __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            _IEpCtrlEvents __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        END_INTERFACE
    } _IEpCtrlEventsVtbl;

    interface _IEpCtrlEvents
    {
        CONST_VTBL struct _IEpCtrlEventsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IEpCtrlEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IEpCtrlEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IEpCtrlEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IEpCtrlEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IEpCtrlEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IEpCtrlEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IEpCtrlEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IEpCtrlEvents_DISPINTERFACE_DEFINED__ */

#endif /* __EPCTRLEVENTSLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long __RPC_FAR *, unsigned long            , VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long __RPC_FAR *, VARIANT __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
