/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Wed Mar 06 07:55:30 2013
 */
/* Compiler settings for EpCtrlIDL.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IEpCtrl = {0xBC90855B,0xE8FC,0x11d2,{0xA3,0x3C,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID LIBID_EPCTRLEVENTSLib = {0x6A5706EB,0x8728,0x11d3,{0xA3,0x73,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID DIID__IEpCtrlEvents = {0x3E245661,0x1395,0x11d3,{0xA3,0x4A,0x00,0x60,0x08,0x67,0xA0,0xE2}};


#ifdef __cplusplus
}
#endif

