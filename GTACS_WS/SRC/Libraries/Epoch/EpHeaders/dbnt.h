/*
@(#) epochnt source dbnt/dbnt.h 1.2 03/02/12 16:09:09
*/
/******************************************************************************
 Filename: dbnt.h

 Author:		Jeff Shaffer - Integral Systems, Inc.
 Created:		April 9, 1999

 This header file contains the public function prototypes and data definitions
 for clients of the EPOCH NT database services dll (dbnt.dll).  The dll allows
 client programs to query the contents of an EPOCH database flat file.

 This version of dbnt.dll requires that the database flat file be physically
 accessible from the client machine.  Database information is read from the
 file into shared memory, where it is made available to multiple clients.  The
 implementation is therefore reasonably efficient, as only one shared memory
 exists at any time per database, and all queries are handled with client
 process local code.

 Database loading and access is done via NuTCRACKER versions of EPOCH libraries
 and processes, notably libsc.a and install_db.exe.

 This file should be used to build NT programs that link to the dll, as well
 as the dll itself.  By placing __declspec() statements in this header and
 including the header in the source files, the __declspec() statements do not
 need to be added before functions in the C++ source file(s).

 To build a dll using this header, make sure that symbol BUILD_DBNT_DLL is
 defined.  To build an application using this header, make sure that symbol
 BUILD_DBNT_DLL is not defined.

 Notes:	

 1. __declspec(dllimport) and __declspec(dllexport) are compiler specific. It
    is assumed that Microsoft's VC++ compiler is being used to build the dll
	and application(s) that use it.  If this is not the case, see Advanced
	Windows by Jeffrey Richter (pg. 564) to learn how to handle name decoration
	issues.

 2. Users of this library should NEVER free data returned by the functions, nor
	should they assume that the data remains constant across function calls.
	Similarly, users should NEVER write into a returned structure. If a client
	wishes to obtain information and use it later, then a private copy should
	be made prior to the next dbnt library call.

 3. !!!!!WARNING!!!!!  There are huge memory leaks when closing a telemetry
	database.  Until the EPOCH libraries are fixed, this will remain a problem.

******************************************************************************/

#ifndef DBNT_H_DEFINED
#define DBNT_H_DEFINED

#include <winnt.h>
#include "epservertypes.h"

//*****************************************************************************
//	Structures used in API:
//*****************************************************************************

typedef struct EpDbCalCoeffHandle *EpDbCalCoeffRecord;
typedef struct EpDbCalibrationHandle *EpDbCalibrationRecord;
typedef struct EpDbConCoeffHandle *EpDbConCoeffRecord;
typedef struct EpDbConversionHandle *EpDbConversionRecord;
typedef struct EpDbDBHandle *EpDbDatabase;
typedef struct EpDbDeltaHandle *EpDbDeltaRecord;
typedef struct EpDbDescHandle *EpDbDescriptionRecord;
typedef struct EpDbLimitHandle *EpDbLimitRecord;
typedef struct EpDbLocationHandle *EpDbLocationRecord;
typedef struct EpDbStateHandle *EpDbStateRecord;
typedef struct EpDbSubsystemHandle *EpDbSubsystemRecord;
typedef struct EpDbEventMsgHandle *EpDbEventMsg;
typedef struct EpDbTlmLimMsgHandle *EpDbTlmLimMsg;

typedef struct EpDbCmdDescHandle *EpDbCmdDescriptionRecord;
typedef struct EpDbCmdDatawordHandle *EpDbCmdDatawordRecord;
typedef struct EpDbCmdDatawordArgHandle *EpDbCmdDatawordArgRecord;
typedef struct EpDbCmdDatawordArgValueHandle *EpDbCmdDatawordArgValueRecord;
typedef struct EpDbCmdDatawordArgRangeHandle *EpDbCmdDatawordArgRangeRecord;
typedef struct EpDbCmdDatawordArgTVContextHandle *EpDbCmdDatawordArgTVContextRecord;
typedef struct EpDbCmdTlmVerStateHandle *EpDbCmdTlmVerStateRecord;
typedef struct EpDbCmdTlmVerSeqHandle *EpDbCmdTlmVerSeqRecord;
typedef struct EpDbCmdTlmVerEventHandle *EpDbCmdTlmVerEventRecord;
typedef struct EpDbCmdCntHandle *EpDbCmdCntRecord;
typedef struct EpDbCmdPrivilegeGroupHandle *EpDbCmdPrivilegeGroupRecord;
typedef struct EpDbCmdDatawordGroupHandle *EpDbCmdDatawordGroupRecord;
typedef struct EpDbCmdFmtHandle *EpDbCmdFmtRecord;
typedef struct EpDbCmdFrameFmtHandle *EpDbCmdFrameFmtRecord;
typedef struct EpDbCmdFmtDatawordMapHandle *EpDbCmdFmtDatawordMapRecord;
typedef struct EpDbCmdFmtPointValueHandle *EpDbCmdFmtPointValueRecord;
typedef struct EpDbCmdEchoEventHandle *EpDbCmdEchoEventRecord;
typedef struct EpDbCmdPointValueHandle *EpDbCmdPointValueRecord;
typedef struct EpDbScFmtHandle *EpDbScFmtRecord;
typedef struct EpDbCmdChannelHandle *EpDbCmdChannelRecord;
typedef struct EpDbScAddressHandle *EpDbScAddressRecord;
typedef struct EpDbCmdExecVerifierHandle *EpDbCmdExecVerifierRecord;
typedef struct EpDbCmdHexCmdHandle *EpDbCmdHexCmdRecord;

//*****************************************************************************
//	Composite structures used to return data in API:
//*****************************************************************************
typedef struct EpDbPointInfo {
	TCHAR *wszMnemonic;
	TCHAR *wszDescription;
	TCHAR *wszUnits;
	TCHAR *wszCTXName;
	TCHAR *wszAlias;
	EPPTVARIETY nPointVariety;
	EPDBDATATYPE nDataType;
	int nDataSize;
	EPDISPCONV nDefaultConversion;
	ULONG ulAllConversions;
	int nSamplingPeriod;
	BOOL bInverted;
	BOOL bReversed;
	BOOL bCritical;
	EPTLMTYPE nTlmType;
	int nPointIndex;
	int nHelpIndex;
} EpDbPointInfo;

typedef struct EpDbEventInfo {
	int nEventNumber;
	TCHAR *wszEventText;
	TCHAR *wszEventName;
	EPSYSTEMTYPE eSystem;
	EPSUBSYSTEMTYPE eSubSystem;
	EPINDEXTYPE eIndexType;
	EPEVTMSG eMsgType;
	EPLIMITSTATUS eStatusType;
} EpDbEventInfo;

typedef struct EpDbTlmLimMsgInfo {
	int nMsgID;
	TCHAR *wszMessage;
} EpDbTlmLimMsgInfo;

typedef struct EpDbCmdPointInfo {
	TCHAR *wszMnemonic;
	TCHAR *wszDescription;
	EPCMDTYPE nCmdType;
	TCHAR *wszAlias;
	int nNumDatawords;
	BOOL bCritical;
	BOOL bProtected;
	int nRexmits;
	int nPulseWidth;
	TCHAR *wszExecMnemonic;
	int nUplinkCode;
	TCHAR *wszCmdFormat;
	int nHelpIndex;
	TCHAR *wszSpacecraftAddr;
	int nNumDatawordArgs;
	int nNumReqDatawordArgs;
	TCHAR *wszPrivilegeGroup;
	int nPointIndex;
} EpDbCmdPointInfo;

typedef struct EpDbCmdDatawordInfo {
    int nLength;
    int nSequenceNum;
    TCHAR *wszDescription;
    TCHAR *wszValue;
} EpDbCmdDatawordInfo;

typedef struct EpDbCmdDatawordArgInfo {
    int nSequenceNum;
	EPDATAWORDARGTYPE nType;
    TCHAR *wszDescription;
	TCHAR *wszKeyword;
    int nDatawordSequenceNum;
    int nDestBit;
    int nNumBits;
    int nPresetAndMask;
    int nPresetXorMask;
    BOOL bRequired;
    EPTLMTYPE nNumType;
    EPVALUESELECTIONTYPE nValueSelection;
} EpDbCmdDatawordArgInfo;

typedef struct EpDbCmdDatawordArgValueInfo {
	TCHAR *wszMnemonic;
    TCHAR *wszDescription;
    BOOL bDefaultValue;
    ULONG ulValue;
} EpDbCmdDatawordArgValueInfo;

typedef struct EpDbCmdDatawordArgRangeInfo {
    TCHAR *wszDescription;
    double dblLow;
	double dblHigh;
} EpDbCmdDatawordArgRangeInfo;

typedef struct EpDbCmdDatawordArgTVContextInfo {
	TCHAR *wszName;
    TCHAR *wszDescription;
    double dblLow;
	double dblHigh;
	TCHAR *wszContext;
} EpDbCmdDatawordArgTVContextInfo;

typedef struct EpDbCmdDatawordGroupInfo {
    int nGroupIndex;
    BOOL bRequired;
    TCHAR *wszDescription;
} EpDbCmdDatawordGroupInfo;

typedef struct EpDbCmdTlmVerStateInfo {
    TCHAR *wszName;
    TCHAR *wszDescription;
    BOOL bVerifyAny;
	EPTVSTATETYPE nType;
} EpDbCmdTlmVerStateInfo;

typedef struct EpDbCmdTlmVerSeqInfo {
    TCHAR *wszMnemonic;
	TCHAR *wszContextName;
} EpDbCmdTlmVerSeqInfo;

typedef struct EpDbCmdTlmVerEventInfo {
	TLMVERIFICATIONTYPE nTVType;
	EPDBVALUETYPE nValueType;
	int nDuration;
	int nDelay;
    BOOL bOneShot;
} EpDbCmdTlmVerEventInfo;

typedef struct EpDbCmdPrivilegeGroupInfo {
    TCHAR *wszGroup;
} EpDbCmdPrivilegeGroupInfo;

typedef struct EpDbCmdFmtInfo {
	TCHAR *wszName;
	TCHAR *wszDescription;
	TCHAR *wszUplinkCodeMnemonic;
	TCHAR *wszCmdFrameMnemonic;
	TCHAR *wszUserDataMnemonic;
	TCHAR *wszUplinkProcessorName;
	int nNumDatawords;
} EpDbCmdFmtInfo;

typedef struct EpDbCmdFrameFmtInfo {
	TCHAR *wszCmdEchoMnemonic;
	TCHAR *wszCmdEchoDescription;
	int nCmdEchoDuration;
} EpDbCmdFrameFmtInfo;

typedef struct EpDbCmdEchoEventInfo {
	EPCMDECHOTYPE nType;
	ULONG ulValue;
	int nDatawordSeqNum;
	ULONG ulAndMask;
	ULONG ulXorMask;
	int nStartBit;
	int nNumBits;
	int nReverseBit;
	TCHAR *wszGVName;
} EpDbCmdEchoEventInfo;

typedef struct EpDbCmdFmtDatawordMapInfo {
	TCHAR *wszDescription;
	TCHAR *wszDatawordMnemonic;
	int nDatawordSeqNum;
} EpDbCmdFmtDatawordMapInfo;

typedef struct EpDbCmdFmtPointValueInfo {
	TCHAR *wszDescription;
	TCHAR *wszMnemonic;
	ULONG ulValue;
	int nIncrement;
} EpDbCmdFmtPointValueInfo;

typedef struct EpDbScFmtInfo {
	TCHAR *wszCmdCntMnemonic;
	TCHAR *wszCmdEchoMnemonic;
	TCHAR *wszFrameFmtMnemonic;
	TCHAR *wszFrameCtrMnemonic;
	TCHAR *wszCmdDumpMnemonic;
	TCHAR *wszCmdFrameMnemonic;
	TCHAR *wszCmdXmittedFrameMnemonic;
	TCHAR *wszEncryptFlagMnemonic;
	TCHAR *wszUplinkProcessorMnemonic;
	BOOL bBypassNullCmdEcho;
	EPDBCMDMODETYPE nCmdMode;
	EPDBCMDPATHTYPE nCmdPath;
	EPDBCMDSCCLASSTYPE nScClass;
} EpDbScFmtInfo;

typedef struct EpDbScAddressInfo {
	TCHAR *wszAddrMnemonic;
	ULONG ulAddrCode;
	int nDefaultAddr;
	TCHAR *wszCmdEchoMnemonic;
	BOOL bBypassNullCmdEcho;
	BOOL bCmdEchoRepeat;
} EpDbScAddressInfo;

typedef struct EpDbUplinkProcessorInfo {
	TCHAR *wszUplinkProcessorName;
	TCHAR *wszHigherLevelProcessorName;
	TCHAR *wszUplinkProcessorMnemonic;
	TCHAR *wszDataServiceName;
	TCHAR *wszCmdCntMnemonic;
	TCHAR *wszCmdEchoMnemonic;
	BOOL bBypassNullCmdEcho;
	BOOL bCmdEchoRepeat;
} EpDbUplinkProcessorInfo;

typedef struct EpDbFrameCntVerifierInfo {
	TCHAR *wszFrameSeqMnemonic;
	TCHAR *wszFrameRcvMnemonic;
	TCHAR *wszMissingFrameMnemonic;
	TCHAR *wszFrameCntMnemonic;
	LONG lFrameRcvOffset;
	int nFrameSeqRollover;
	LONG lFrameCntIncrement;
	int nDuration;
} EpDbFrameCntVerifierInfo;

typedef struct EpDbCmdExecVerifierInfo {
	TCHAR *wszTlmMnemonic;
	TCHAR *wszSimContextMnemonic;
	ULONG ulSimContextValue;
	int nDuration;
} EpDbCmdExecVerifierInfo;

//*****************************************************************************
//	DLL entry point prototypes:
//*****************************************************************************

#ifdef BUILD_DBNT_DLL
#    define EPOCH_DBNT_API __declspec(dllexport)
#else
#    define EPOCH_DBNT_API __declspec(dllimport)
#endif

// Database open and close
EPOCH_DBNT_API EPAPISTATUS EpDbOpenDatabase(TCHAR *wszDBName,
									 EpDbDatabase *pDatabase);
EPOCH_DBNT_API EPAPISTATUS EpDbCloseDatabase(EpDbDatabase pDatabase);

// Database information
EPOCH_DBNT_API EPDBTYPE EpDbGetDatabaseType(EpDbDatabase pDatabase);

// Point description information
EPOCH_DBNT_API EPLISTSTATUS EpDbDescriptionRecordInit(
										EpDbDatabase pDatabase,
										TCHAR *wszMnemonic,
										EpDbDescriptionRecord *pDescription);
EPOCH_DBNT_API EPLISTSTATUS EpDbDescriptionRecordListInit(EpDbDatabase pDatabase,
										EpDbDescriptionRecord *pDescription);
EPOCH_DBNT_API EPLISTSTATUS EpDbDescriptionRecordListNextEntry(
										EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API EPAPISTATUS EpDbDescriptionRecordListFree(
										EpDbDescriptionRecord pDescription);

// Point description data access
EPOCH_DBNT_API ULONG EpDbGetAllConversions(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API EPLISTSTATUS EpDbGetContextDescription(
										EpDbDescriptionRecord pDescription,
										EpDbDescriptionRecord *pCTXDescription);
EPOCH_DBNT_API int EpDbGetDataSize(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API EPDBDATATYPE EpDbGetDataType(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API EPDISPCONV EpDbGetDefaultConversion(
										EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetDefaultSamplingPeriod(
										EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API TCHAR *EpDbGetDescription(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetIndex(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API BOOL EpDbGetInvertedDataFlag(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API TCHAR *EpDbGetMnemonic(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API TCHAR *EpDbGetAlias(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetPointHelpIndex(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API EPPTVARIETY EpDbGetPointVariety(
										EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API BOOL EpDbGetReversedDataFlag(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API BOOL EpDbGetCritical(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API EPTLMTYPE EpDbGetTlmType(EpDbDescriptionRecord pDescription);
EPOCH_DBNT_API TCHAR *EpDbGetUnits(EpDbDescriptionRecord pDescription);

//Composite point description data access
EPOCH_DBNT_API EpDbPointInfo *EpDbGetPointInfo(EpDbDescriptionRecord pDescription);

// State information
EPOCH_DBNT_API EPLISTSTATUS EpDbStateListInit(
										EpDbDescriptionRecord pDescription,
										EpDbStateRecord *pState);
EPOCH_DBNT_API EPLISTSTATUS EpDbStateListNextEntry(EpDbStateRecord pState);
EPOCH_DBNT_API EPAPISTATUS EpDbStateListFree(EpDbStateRecord pState);

// State data access
EPOCH_DBNT_API int EpDbGetEventNum(EpDbStateRecord pState);
EPOCH_DBNT_API double EpDbGetLimitHigh(EpDbStateRecord pState);
EPOCH_DBNT_API double EpDbGetLimitLow(EpDbStateRecord pState);
EPOCH_DBNT_API TCHAR *EpDbGetMessage(EpDbStateRecord pState);
EPOCH_DBNT_API TCHAR *EpDbGetName(EpDbStateRecord pState);
EPOCH_DBNT_API double EpDbGetStateContextHigh(EpDbStateRecord pState);
EPOCH_DBNT_API double EpDbGetStateContextLow(EpDbStateRecord pState);

// Limit information
EPOCH_DBNT_API EPLISTSTATUS EpDbLimitListInit(
										EpDbDescriptionRecord pDescription,
										EpDbLimitRecord *pLimit);
EPOCH_DBNT_API EPLISTSTATUS EpDbLimitListNextEntry(EpDbLimitRecord pLimit);
EPOCH_DBNT_API EPAPISTATUS EpDbLimitListFree(EpDbLimitRecord pLimit);

// Limit data access
EPOCH_DBNT_API double EpDbGetLimitContextHigh(EpDbLimitRecord pLimit);
EPOCH_DBNT_API double EpDbGetLimitContextLow(EpDbLimitRecord pLimit);
EPOCH_DBNT_API double EpDbGetLimitHighRed(EpDbLimitRecord pLimit);
EPOCH_DBNT_API int EpDbGetLimitHighRedMsg(EpDbLimitRecord pLimit);
EPOCH_DBNT_API double EpDbGetLimitHighYellow(EpDbLimitRecord pLimit);
EPOCH_DBNT_API int EpDbGetLimitHighYellowMsg(EpDbLimitRecord pLimit);
EPOCH_DBNT_API double EpDbGetLimitLowRed(EpDbLimitRecord pLimit);
EPOCH_DBNT_API int EpDbGetLimitLowRedMsg(EpDbLimitRecord pLimit);
EPOCH_DBNT_API double EpDbGetLimitLowYellow(EpDbLimitRecord pLimit);
EPOCH_DBNT_API int EpDbGetLimitLowYellowMsg(EpDbLimitRecord pLimit);
EPOCH_DBNT_API int EpDbGetLimitNormalMsg(EpDbLimitRecord pLimit);
EPOCH_DBNT_API EPLIMITSTATUS EpDbGetLimitStatus(EpDbLimitRecord pLimit);
EPOCH_DBNT_API double EpDbGetLimitStatusHigh(EpDbLimitRecord pLimit);
EPOCH_DBNT_API double EpDbGetLimitStatusLow(EpDbLimitRecord pLimit);
EPOCH_DBNT_API int EpDbGetLimitThreshold(EpDbLimitRecord pLimit);
EPOCH_DBNT_API int EpDbGetLimitStatusMsg(EpDbLimitRecord pLimit);
EPOCH_DBNT_API EPLIMITRANGE EpDbGetRangeType(EpDbLimitRecord pLimit);
EPOCH_DBNT_API EPLIMITVALUE EpDbGetValueType(EpDbLimitRecord pLimit);

// Delta Limit information
EPOCH_DBNT_API EPLISTSTATUS EpDbDeltaListInit(
										EpDbDescriptionRecord pDescription,
										EpDbDeltaRecord *pDelta);
EPOCH_DBNT_API EPLISTSTATUS EpDbDeltaListNextEntry(EpDbDeltaRecord pDelta);
EPOCH_DBNT_API EPAPISTATUS EpDbDeltaListFree(EpDbDeltaRecord pDelta);

// Delta Limit data access
EPOCH_DBNT_API double EpDbGetDeltaContextHigh(EpDbDeltaRecord pDelta);
EPOCH_DBNT_API double EpDbGetDeltaContextLow(EpDbDeltaRecord pDelta);
EPOCH_DBNT_API double EpDbGetDeltaHigh(EpDbDeltaRecord pDelta);
EPOCH_DBNT_API int EpDbGetDeltaHighMsg(EpDbDeltaRecord pDelta);
EPOCH_DBNT_API double EpDbGetDeltaLow(EpDbDeltaRecord pDelta);
EPOCH_DBNT_API int EpDbGetDeltaLowMsg(EpDbDeltaRecord pDelta);
EPOCH_DBNT_API EPLIMITVALUE EpDbGetDeltaValueType(EpDbDeltaRecord pDelta);

// Location information
EPOCH_DBNT_API EPLISTSTATUS EpDbLocationListInit(
										EpDbDescriptionRecord pDescription,
										EpDbLocationRecord *pLocation);
EPOCH_DBNT_API EPLISTSTATUS EpDbLocationListNextEntry(
										EpDbLocationRecord pLocation);
EPOCH_DBNT_API EPAPISTATUS EpDbLocationListFree(EpDbLocationRecord pLocation);

// Location data access
EPOCH_DBNT_API int EpDbGetDestBit(EpDbLocationRecord pLocation);
EPOCH_DBNT_API EPLISTSTATUS EpDbGetIndexDescription(
										EpDbLocationRecord pLocation,
										EpDbDescriptionRecord *pDescription);
EPOCH_DBNT_API EPLISTSTATUS EpDbGetModeDescription(
										EpDbLocationRecord pLocation,
										EpDbDescriptionRecord *pDescription);
EPOCH_DBNT_API unsigned long EpDbGetModeValue(EpDbLocationRecord pLocation);
EPOCH_DBNT_API int EpDbGetNumBits(EpDbLocationRecord pLocation);
EPOCH_DBNT_API int EpDbGetStartBit(EpDbLocationRecord pLocation);
EPOCH_DBNT_API int EpDbGetSubcomDepth(EpDbLocationRecord pLocation);
EPOCH_DBNT_API int EpDbGetSupercomDepth(EpDbLocationRecord pLocation);

// Calibration information
EPOCH_DBNT_API EPLISTSTATUS EpDbCalibrationListInit(
										EpDbDescriptionRecord pDescription,
										EpDbCalibrationRecord *pCalibration);
EPOCH_DBNT_API EPLISTSTATUS EpDbCalibrationListNextEntry(
										EpDbCalibrationRecord pCalibration);
EPOCH_DBNT_API EPAPISTATUS EpDbCalibrationListFree(
										EpDbCalibrationRecord pCalibration);

// Calibration data access
EPOCH_DBNT_API double EpDbGetCalibrationContextHigh(
										EpDbCalibrationRecord pCalibration);
EPOCH_DBNT_API double EpDbGetCalibrationContextLow(
										EpDbCalibrationRecord pCalibration);
EPOCH_DBNT_API int EpDbGetCalibrationUnit(
										EpDbCalibrationRecord pCalibration,
										TCHAR* wszUnit, unsigned int nBufSize);
EPOCH_DBNT_API int EpDbGetCalibrationCoefficentsCount(EpDbCalibrationRecord pCalibration);

// ... Calibration coefficient information
EPOCH_DBNT_API EPLISTSTATUS EpDbCalibrationCoefficientListInit(
										EpDbCalibrationRecord pCalibration,
										EpDbCalCoeffRecord *pCalCoeff);
EPOCH_DBNT_API EPLISTSTATUS EpDbCalibrationCoefficientListNextEntry(
										EpDbCalCoeffRecord pCalCoeff);
EPOCH_DBNT_API EPAPISTATUS EpDbCalibrationCoefficientListFree(
										EpDbCalCoeffRecord pCalCoeff);

// ... Calibration coefficient data access
EPOCH_DBNT_API double EpDbGetCalibrationCoefficientEU(
										EpDbCalCoeffRecord pCalCoeff);
EPOCH_DBNT_API double EpDbGetCalibrationCoefficientRaw(
										EpDbCalCoeffRecord pCalCoeff);

// Conversion information
EPOCH_DBNT_API EPLISTSTATUS EpDbConversionListInit(
										EpDbDescriptionRecord pDescription,
										EpDbConversionRecord *pConversion);
EPOCH_DBNT_API EPLISTSTATUS EpDbConversionListNextEntry(
										EpDbConversionRecord pConversion);
EPOCH_DBNT_API EPAPISTATUS EpDbConversionListFree(
										EpDbConversionRecord pConversion);

// Conversion data access
EPOCH_DBNT_API double EpDbGetConversionContextHigh(
										EpDbConversionRecord pConversion);
EPOCH_DBNT_API double EpDbGetConversionContextLow(
										EpDbConversionRecord pConversion);
EPOCH_DBNT_API int EpDbGetConversionUnit(
										EpDbConversionRecord pConversion,
										TCHAR* wszUnit, unsigned int nBufSize);

// ... Conversion coefficient information
EPOCH_DBNT_API EPLISTSTATUS EpDbConversionCoefficientListInit(
										EpDbConversionRecord pConversion,
										EpDbConCoeffRecord *pConCoeff);
EPOCH_DBNT_API EPLISTSTATUS EpDbConversionCoefficientListNextEntry(
										EpDbConCoeffRecord pConCoeff);
EPOCH_DBNT_API EPAPISTATUS EpDbConversionCoefficientListFree(
										EpDbConCoeffRecord pConCoeff);

// ... Conversion coefficient data access
EPOCH_DBNT_API double EpDbGetConversionCoefficient(
										EpDbConCoeffRecord pConCoeff);


//Point subsystem information
EPOCH_DBNT_API EPLISTSTATUS EpDbSubsystemListInit(
										EpDbDatabase pDatabase,
										EpDbSubsystemRecord *pSubsystem);
EPOCH_DBNT_API EPLISTSTATUS EpDbSubsystemListNextEntry(
										EpDbSubsystemRecord pSubsystem);
EPOCH_DBNT_API EPAPISTATUS EpDbSubsystemListFree(
										EpDbSubsystemRecord pSubsystem);

// ... Point subsystem data access
EPOCH_DBNT_API TCHAR *EpDbGetSubsystemDescription(
										EpDbSubsystemRecord pSubsystem);
EPOCH_DBNT_API int EpDbGetSubsystemIndex(
										EpDbSubsystemRecord pSubsystem);
EPOCH_DBNT_API TCHAR *EpDbGetSubsystemName(
										EpDbSubsystemRecord pSubsystem);
EPOCH_DBNT_API EPSUBSYSTEM EpDbGetSubsystemType(
										EpDbSubsystemRecord pSubsystem);

// Database information
EPOCH_DBNT_API EPDBTYPE EpDbGetDatabaseType(EpDbDatabase pDatabase);

// Event information
EPOCH_DBNT_API int EpDbFormatEvent(EpDbDatabase pDatabase,
										EPEVTDATA *pUnformattedEvent,
										TCHAR **ppwszFormattedEvent);
EPOCH_DBNT_API TCHAR *EpDbGetMnemonicFromIndex(EpDbDatabase pDatabase,
										EPPTVARIETY eVariety,
										int nIndex);

// Event description information
EPOCH_DBNT_API EPLISTSTATUS EpDbEventMsgInit(
										EpDbDatabase pDatabase,
										int nEventNumber,
										EpDbEventMsg *pEventMsg);
EPOCH_DBNT_API EPLISTSTATUS EpDbEventMsgListInit(EpDbDatabase pDatabase,
										EpDbEventMsg *pEventMsg);
EPOCH_DBNT_API EPLISTSTATUS EpDbEventMsgListNextEntry(
										EpDbEventMsg pEventMsg);
EPOCH_DBNT_API EPAPISTATUS EpDbEventMsgListFree(
										EpDbEventMsg pEventMsg);
EPOCH_DBNT_API EpDbEventInfo *EpDbGetEventInfo(EpDbEventMsg pEventMsg);

//Telemetry limit message information
EPOCH_DBNT_API EPLISTSTATUS EpDbTlmLimMsgListInit(
										EpDbDatabase pDatabase,
										EpDbTlmLimMsg *pTlmLimMsg);
EPOCH_DBNT_API EPLISTSTATUS EpDbTlmLimMsgListNextEntry(
										EpDbTlmLimMsg pTlmLimMsg);
EPOCH_DBNT_API EPAPISTATUS EpDbTlmLimMsgListFree(
										EpDbTlmLimMsg pTlmLimMsg);
EPOCH_DBNT_API EpDbTlmLimMsgInfo *EpDbGetTlmLimMsgInfo(EpDbTlmLimMsg pTlmLimMsg);

//Command description information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDescriptionRecordInit(
										EpDbDatabase pDatabase,
										TCHAR *wszMnemonic,
										EpDbCmdDescriptionRecord *pDescription);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDescriptionListInit(
										EpDbDatabase pDatabase,
										EpDbCmdDescriptionRecord *pDescription);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDescriptionListNextEntry(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdDescriptionListFree(
										EpDbCmdDescriptionRecord pDescription);

EPOCH_DBNT_API EpDbCmdPointInfo *EpDbGetCmdInfo(
								 EpDbCmdDescriptionRecord pDescription);

//Command data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdMnemonic(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API TCHAR *EpDbGetCmdDescription(
										EpDbCmdDescriptionRecord pDescription);

EPOCH_DBNT_API EPCMDTYPE EpDbGetCmdType(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API TCHAR *EpDbGetCmdAlias(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API TCHAR *EpDbGetCmdExecMnemonic(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API TCHAR *EpDbGetCmdFormat(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API BOOL EpDbGetCmdCritical(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API BOOL EpDbGetCmdProtected(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetCmdNumDatawords(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetCmdRexmits(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetCmdPulseWidth(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetCmdUplinkCode(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetCmdHelpIndex(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetCmdIndex(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API TCHAR *EpDbGetCmdSpacecraftAddr(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetCmdNumDatawordArgs(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetCmdNumReqDatawordArgs(
										EpDbCmdDescriptionRecord pDescription);

//Command dataword information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordListInit(
										EpDbCmdDescriptionRecord pDescription,
										EpDbCmdDatawordRecord *pDataword);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordListNextEntry(
										EpDbCmdDatawordRecord pDataword);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdDatawordListFree(
										EpDbCmdDatawordRecord pDataword);

//Command dataword data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordDescription(
										EpDbCmdDatawordRecord pDataword);
EPOCH_DBNT_API int EpDbGetCmdDatawordLength(
										EpDbCmdDatawordRecord pDataword);
EPOCH_DBNT_API int EpDbGetCmdDatawordSequenceNum(
										EpDbCmdDatawordRecord pDataword);
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordValue(
										EpDbCmdDatawordRecord pDataword);

//Command dataword argument information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordArgListInit(
										EpDbCmdDescriptionRecord pDescription,
										EpDbCmdDatawordArgRecord *pDatawordArg);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordArgListNextEntry(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdDatawordArgListFree(
										EpDbCmdDatawordArgRecord pDatawordArg);

//Command dataword argument data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordArgDescription(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API int EpDbGetCmdDatawordArgSequenceNum(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordArgKeyword(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordArgMnemonic(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API EPDATAWORDARGTYPE EpDbGetCmdDatawordArgType(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API int EpDbGetCmdDatawordArgDestBit(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API int EpDbGetCmdDatawordArgNumBits(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API int EpDbGetCmdDatawordArgDatawordSequenceNum(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API int EpDbGetCmdDatawordArgPresetAndMask(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API int EpDbGetCmdDatawordArgPresetXorMask(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API BOOL EpDbGetCmdDatawordArgRequired(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API int EpDbGetCmdDatawordArgGroupIndex(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API EPVALUESELECTIONTYPE EpDbGetCmdDatawordArgValueSelection(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API EPTLMTYPE EpDbGetCmdDatawordArgNumType(
										EpDbCmdDatawordArgRecord pDatawordArg);
EPOCH_DBNT_API int EpDbGetCmdDatawordArgCoefficients(
										EpDbCmdDatawordArgRecord pDatawordArg, double coeff[EPDATAWORDARG_MAX_COEF]);

//Command dataword argument value information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordArgValueListInit(
										EpDbCmdDatawordArgRecord pDatawordArg,
										EpDbCmdDatawordArgValueRecord *pDatawordArgValue);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordArgValueListNextEntry(
										EpDbCmdDatawordArgValueRecord pDatawordArgValue);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdDatawordArgValueListFree(
										EpDbCmdDatawordArgValueRecord pDatawordArgValue);
//Command dataword argument value data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordArgValueMnemonic(
										EpDbCmdDatawordArgValueRecord pDatawordArgValue);
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordArgValueDescription(
										EpDbCmdDatawordArgValueRecord pDatawordArgValue);
EPOCH_DBNT_API ULONG EpDbGetCmdDatawordArgValueValue(
										EpDbCmdDatawordArgValueRecord pDatawordArgValue);
EPOCH_DBNT_API BOOL EpDbGetCmdDatawordArgValueDefault(
										EpDbCmdDatawordArgValueRecord pDatawordArgValue);

//Command dataword argument range information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordArgRangeListInit(
										EpDbCmdDatawordArgRecord pDatawordArg,
										EpDbCmdDatawordArgRangeRecord *pDatawordArgRange);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordArgRangeListNextEntry(
										EpDbCmdDatawordArgRangeRecord pDatawordArgRange);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdDatawordArgRangeListFree(
										EpDbCmdDatawordArgRangeRecord pDatawordArgRange);

//Command dataword argument range data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordArgRangeDescription(
										EpDbCmdDatawordArgRangeRecord pDatawordArgRange);
EPOCH_DBNT_API double EpDbGetCmdDatawordArgRangeLow(
										EpDbCmdDatawordArgRangeRecord pDatawordArgRange);
EPOCH_DBNT_API double EpDbGetCmdDatawordArgRangeHigh(
										EpDbCmdDatawordArgRangeRecord pDatawordArgRange);

//Command dataword argument TV context information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordArgTVContextListInit(
										EpDbCmdDatawordArgRecord pDatawordArg,
										EpDbCmdDatawordArgTVContextRecord *pDatawordArgTVContext);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordArgTVContextListNextEntry(
										EpDbCmdDatawordArgTVContextRecord pDatawordArgTVContext);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdDatawordArgTVContextListFree(
										EpDbCmdDatawordArgTVContextRecord pDatawordArgTVContext);

//Command dataword argument TV Context data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordArgTVContextName(
										EpDbCmdDatawordArgTVContextRecord pDatawordArgTVContext);
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordArgTVContextDescription(
										EpDbCmdDatawordArgTVContextRecord pDatawordArgTVContext);
EPOCH_DBNT_API double EpDbGetCmdDatawordArgTVContextLow(
										EpDbCmdDatawordArgTVContextRecord pDatawordArgTVContext);
EPOCH_DBNT_API double EpDbGetCmdDatawordArgTVContextHigh(
										EpDbCmdDatawordArgTVContextRecord pDatawordArgTVContext);
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordArgTVContextContext(
										EpDbCmdDatawordArgTVContextRecord pDatawordArgTVContext);

//Pre telemetry verification state information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdPreTlmVerStateListInit(
										EpDbCmdDescriptionRecord pDescription,
										EpDbCmdTlmVerStateRecord *pTlmVerState);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdPreTlmVerStateListNextEntry(
										EpDbCmdTlmVerStateRecord pTlmVerState);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdPreTlmVerStateListFree(
										EpDbCmdTlmVerStateRecord pTlmVerState);
//Post telemetry verification state information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdPostTlmVerStateListInit(
										EpDbCmdDescriptionRecord pDescription,
										EpDbCmdTlmVerStateRecord *pTlmVerState);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdPostTlmVerStateListNextEntry(
										EpDbCmdTlmVerStateRecord pTlmVerState);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdPostTlmVerStateListFree(
										EpDbCmdTlmVerStateRecord pTlmVerState);

//Pseudo telemetry verification state information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdPseudoTlmVerStateListInit(
										EpDbCmdDescriptionRecord pDescription,
										EpDbCmdTlmVerStateRecord *pTlmVerState);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdPseudoTlmVerStateListNextEntry(
										EpDbCmdTlmVerStateRecord pTlmVerState);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdPseudoTlmVerStateListFree(
										EpDbCmdTlmVerStateRecord pTlmVerState);

//Telemetry verification state data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdTlmVerStateName(
										EpDbCmdTlmVerStateRecord pTlmVerState);
EPOCH_DBNT_API TCHAR *EpDbGetCmdTlmVerStateDescription(
										EpDbCmdTlmVerStateRecord pTlmVerState);
EPOCH_DBNT_API EPTVSTATETYPE EpDbGetCmdTlmVerStateType(
										EpDbCmdTlmVerStateRecord pTlmVerState);
EPOCH_DBNT_API BOOL EpDbGetCmdTlmVerStateVerifyAny(
										EpDbCmdTlmVerStateRecord pTlmVerState);

//Telemetry verification sequence information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdTlmVerSeqListInit(
										EpDbCmdTlmVerStateRecord pTlmVerState,
										EpDbCmdTlmVerSeqRecord *pTlmVerSeq);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdTlmVerSeqListNextEntry(
										EpDbCmdTlmVerSeqRecord pTlmVerSeq);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdTlmVerSeqListFree(
										EpDbCmdTlmVerSeqRecord pTlmVerSeq);
EPOCH_DBNT_API TCHAR *EpDbGetCmdTlmVerSeqMnemonic(
										EpDbCmdTlmVerSeqRecord pTlmVerSeq);
EPOCH_DBNT_API TCHAR *EpDbGetCmdTlmVerSeqContextName(
										EpDbCmdTlmVerSeqRecord pTlmVerSeq);

//Telemetry verification event information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdTlmVerEventListInit(
										EpDbCmdTlmVerSeqRecord pTlmVerSeq,
										EpDbCmdTlmVerEventRecord *pTlmVerEvent);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdTlmVerEventListNextEntry(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdTlmVerEventListFree(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);

//Telemetry verification event data
EPOCH_DBNT_API TLMVERIFICATIONTYPE EpDbGetCmdTlmVerEventType(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API EPDBVALUETYPE EpDbGetCmdTlmVerEventValueType(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API BOOL EpDbGetCmdTlmVerEventOneShot(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API int EpDbGetCmdTlmVerEventDuration(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API int EpDbGetCmdTlmVerEventDelay(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API EPDBCMDTVEVTDATATYPE EpDbGetCmdTlmVerEventDataType(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API double EpDbGetCmdTlmVerEventRangeLow(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API double EpDbGetCmdTlmVerEventRangeHigh(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API double EpDbGetCmdTlmVerEventBoolValue(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API TCHAR *EpDbGetCmdTlmVerEventBoolId(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API TCHAR *EpDbGetCmdTlmVerEventBoolDat1(	
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API TCHAR *EpDbGetCmdTlmVerEventBoolDat2(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API double EpDbGetCmdTlmVerEventPseudoSetValue(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API ULONG EpDbGetCmdTlmVerEventPseudoClearMask(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API int EpDbGetCmdTlmVerEventComStartBit(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API int EpDbGetCmdTlmVerEventComStopBit(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API double EpDbGetCmdTlmVerEventComValue(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API TCHAR *EpDbGetCmdTlmVerEventLvId(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API double EpDbGetCmdTlmVerEventLvValue(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API double EpDbGetCmdTlmVerEventIncPseudoValue(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API double EpDbGetCmdTlmVerEventIncPseudoLimit(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API double EpDbGetCmdTlmVerEventIncPseudoReset(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API ULONG EpDbGetCmdTlmVerEventMaskedCompareMask(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);
EPOCH_DBNT_API ULONG EpDbGetCmdTlmVerEventMaskedCompareValue(
										EpDbCmdTlmVerEventRecord pTlmVerEvent);

//Command dataword group information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordGroupListInit(
										EpDbCmdDescriptionRecord pDescription,
										EpDbCmdDatawordGroupRecord *pDatawordGroup);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordGroupListNextEntry(
										EpDbCmdDatawordGroupRecord pDatawordGroup);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdDatawordGroupListFree(
										EpDbCmdDatawordGroupRecord pDatawordGroup);

//Command dataword group data
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordGroupDescription(
										EpDbCmdDatawordGroupRecord pDatawordGroup);
EPOCH_DBNT_API int EpDbGetCmdDatawordGroupIndex(
										EpDbCmdDatawordGroupRecord pDatawordGroup);
EPOCH_DBNT_API BOOL EpDbGetCmdDatawordGroupRequired(
										EpDbCmdDatawordGroupRecord pDatawordGroup);

//Command counter verifier data
EPOCH_DBNT_API BOOL EpDbGetCmdCntExistence(EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API LONG EpDbGetCmdCntIncrement(
										EpDbCmdDescriptionRecord pDescription);
EPOCH_DBNT_API int EpDbGetCmdCntDuration(
										EpDbCmdDescriptionRecord pDescription);

//Command privilege group information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdPrivilegeGroupListInit(
										EpDbCmdDescriptionRecord pDescription,
										EpDbCmdPrivilegeGroupRecord *pPrivilegeGroup);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdPrivilegeGroupListNextEntry(
										EpDbCmdPrivilegeGroupRecord pPrivilegeGroup);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdPrivilegeGroupListFree(
										EpDbCmdPrivilegeGroupRecord pPrivilegeGroup);

//Command privilege group data
EPOCH_DBNT_API TCHAR *EpDbGetCmdPrivilegeGroupGroup(
										EpDbCmdPrivilegeGroupRecord pPrivilegeGroup);

EPOCH_DBNT_API TCHAR *EpDbGetCmdPrivilegeGroups(
										EpDbCmdDescriptionRecord pDescription);
//Command format information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdFormatRecordInit(
										EpDbDatabase pDatabase,
										TCHAR *wszFormatName,
										EpDbCmdFmtRecord *pCmdFmt);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdFormatListInit(
										EpDbDatabase pDatabase,
										EpDbCmdFmtRecord *pCmdFmt);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdFormatListNextEntry(
										EpDbCmdFmtRecord pCmdFmt);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdFormatListFree(
										EpDbCmdFmtRecord pCmdFmt);

//Command format data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdFormatName(
										EpDbCmdFmtRecord pCmdFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdFormatDescription(
										EpDbCmdFmtRecord pCmdFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdFormatUplinkCodeMnemonic(
										EpDbCmdFmtRecord pCmdFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdFormatUserMnemonic(
										EpDbCmdFmtRecord pCmdFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdFormatCmdFrameMnemonic(
										EpDbCmdFmtRecord pCmdFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdFormatUplinkProcessorName(
										EpDbCmdFmtRecord pCmdFmt);
EPOCH_DBNT_API int EpDbGetCmdFormatNumDatawords(
										EpDbCmdFmtRecord pCmdFmt);

//Command frame format information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdFrameFormatListInit(
										EpDbCmdFmtRecord pCmdFmt,
										EpDbCmdFrameFmtRecord *pCmdFrmFmt);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdFrameFormatListNextEntry(
										EpDbCmdFrameFmtRecord pCmdFrmFmt);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdFrameFormatListFree(
										EpDbCmdFrameFmtRecord pCmdFrmFmt);

//Command echo verifier data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdEchoMnemonic(
										EpDbCmdFrameFmtRecord pCmdFrmFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdEchoDescription(
										EpDbCmdFrameFmtRecord pCmdFrmFmt);
EPOCH_DBNT_API int EpDbGetCmdEchoDuration(
										EpDbCmdFrameFmtRecord pCmdFrmFmt);

//Command echo verifier event information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdEchoEventListInit(
										EpDbCmdFrameFmtRecord pCmdFrmFmt,
										EpDbCmdEchoEventRecord *pCmdEchoEvt);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdEchoEventListNextEntry(
										EpDbCmdEchoEventRecord pCmdEchoEvt);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdEchoEventListFree(
										EpDbCmdEchoEventRecord pCmdEchoEvt);

//Command echo verifier event data access
EPOCH_DBNT_API EPCMDECHOTYPE EpDbGetCmdEchoEventType(
										EpDbCmdEchoEventRecord pCmdEchoEvt);
EPOCH_DBNT_API ULONG EpDbGetCmdEchoEventValue(
										EpDbCmdEchoEventRecord pCmdEchoEvt);
EPOCH_DBNT_API int EpDbGetCmdEchoEventDatawordSeqNum(
										EpDbCmdEchoEventRecord pCmdEchoEvt);
EPOCH_DBNT_API ULONG EpDbGetCmdEchoEventAndMask(
										EpDbCmdEchoEventRecord pCmdEchoEvt);
EPOCH_DBNT_API ULONG EpDbGetCmdEchoEventXorMask(
										EpDbCmdEchoEventRecord pCmdEchoEvt);
EPOCH_DBNT_API int EpDbGetCmdEchoEventStartBit(
										EpDbCmdEchoEventRecord pCmdEchoEvt);
EPOCH_DBNT_API int EpDbGetCmdEchoEventNumBits(
										EpDbCmdEchoEventRecord pCmdEchoEvt);
EPOCH_DBNT_API BOOL EpDbGetCmdEchoEventReverseBits(
										EpDbCmdEchoEventRecord pCmdEchoEvt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdEchoEventGVName(
										EpDbCmdEchoEventRecord pCmdEchoEvt);

//Command dataword map information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordMapListInit(
										EpDbCmdFrameFmtRecord pCmdFrmFmt,
										EpDbCmdFmtDatawordMapRecord *pDatawordMap);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdDatawordMapListNextEntry(
										EpDbCmdFmtDatawordMapRecord pDatawordMap);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdDatawordMapListFree(
										EpDbCmdFmtDatawordMapRecord pDatawordMap);
//Command dataword map data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordMapMnemonic(
										EpDbCmdFmtDatawordMapRecord pDatawordMap);
EPOCH_DBNT_API TCHAR *EpDbGetCmdDatawordMapDescription(
										EpDbCmdFmtDatawordMapRecord pDatawordMap);
EPOCH_DBNT_API int EpDbGetCmdDatawordMapSequenceNum(
										EpDbCmdFmtDatawordMapRecord pDatawordMap);

//Command point value information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdFmtPointValueListInit(
										EpDbCmdFmtRecord pCmdFrmFmt,
										EpDbCmdFmtPointValueRecord *pPointValue);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdFmtPointValueListNextEntry(
										EpDbCmdFmtPointValueRecord pPointValue);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdFmtPointValueListFree(
										EpDbCmdFmtPointValueRecord pPointValue);

//Command point value data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdFmtPointValueMnemonic(
										EpDbCmdFmtPointValueRecord pPointValue);
EPOCH_DBNT_API TCHAR *EpDbGetCmdFmtPointValueDescription(
										EpDbCmdFmtPointValueRecord pPointValue);
EPOCH_DBNT_API ULONG EpDbGetCmdFmtPointValueValue(
										EpDbCmdFmtPointValueRecord pPointValue);
EPOCH_DBNT_API BOOL EpDbGetCmdFmtPointValueIncrement(
										EpDbCmdFmtPointValueRecord pPointValue);

//Command frame format data access
EPOCH_DBNT_API TCHAR *EpDbGetFrameFmtCtrMnemonic(
										EpDbDatabase pDatabase);

//Spacecraft format information
EPOCH_DBNT_API EPAPISTATUS EpDbScFmtInit(EpDbDatabase pDatabase, EpDbScFmtRecord *pScFmt);
EPOCH_DBNT_API EPAPISTATUS EpDbScFmtFree(EpDbScFmtRecord pScFmt);

//Spacecraft format data access
EPOCH_DBNT_API TCHAR *EpDbGetFrameFmtName(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API TCHAR *EpDbGetFrameCtrMnemonic(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdCntMnemonic(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdEchoMnemonic(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdDumpMnemonic(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdFrameMnemonic(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdXmittedFrameMnemonic(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API TCHAR *EpDbGetCmdUplinkProcessorMnemonic(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API BOOL EpDbGetBypassNullCmdEcho(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API EPDBCMDMODETYPE EpDbGetCmdMode(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API EPDBCMDPATHTYPE EpDbGetCmdPath(EpDbScFmtRecord pScFmt);
EPOCH_DBNT_API EPDBCMDSCCLASSTYPE EpDbGetScClass(EpDbScFmtRecord pScFmt);

//Command channel information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdChannelRecordInit(
										EpDbDatabase pDatabase,
										TCHAR *wszFormatName,
										EpDbCmdChannelRecord *pCmdChannel);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdChannelListInit(
										EpDbDatabase pDatabase,
										EpDbCmdChannelRecord *pCmdChannel);
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdChannelListNextEntry(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdChannelListFree(
										EpDbCmdChannelRecord pCmdChannel);

//Uplink processor data access
EPOCH_DBNT_API BOOL EpDbGetCmdChannelBypassNullCmdEcho(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API BOOL EpDbGetCmdChannelCmdEchoRepeat(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API TCHAR *EpDbGetCmdChannelUplinkProcessorName(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API TCHAR *EpDbGetCmdChannelUplinkProcessorMnemonic(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API TCHAR *EpDbGetCmdChannelHigherLevelProcessorName(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API TCHAR *EpDbGetCmdChannelDataServiceName(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API TCHAR *EpDbGetCmdChannelCmdCounterMnemonic(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API TCHAR *EpDbGetCmdChannelCmdEchoMnemonic(
										EpDbCmdChannelRecord pCmdChannel);

//Frame counter data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdChannelFrameSequenceMnemonic(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API TCHAR *EpDbGetCmdChannelFramesReceivedMnemonic(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API TCHAR *EpDbGetCmdChannelMissingFrameMnemonic(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API TCHAR *EpDbGetCmdChannelFrameCounterMnemonic(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API LONG EpDbGetCmdChannelFrameReceiveOffset(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API int EpDbGetCmdChannelFrameSequenceRollover(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API LONG EpDbGetCmdChannelFrameCounterIncrement(
										EpDbCmdChannelRecord pCmdChannel);
EPOCH_DBNT_API int EpDbGetCmdChannelFrameCounterVerifierDuration(
										EpDbCmdChannelRecord pCmdChannel);

//Spacecraft address information
EPOCH_DBNT_API EPLISTSTATUS EpDbScAddressListInit(
										EpDbCmdChannelRecord pCmdChannel,
										EpDbScAddressRecord *pScAddr);
EPOCH_DBNT_API EPLISTSTATUS EpDbScAddressListNextEntry(
										EpDbScAddressRecord pScAddr);
EPOCH_DBNT_API EPAPISTATUS EpDbScAddressListFree(
										EpDbScAddressRecord pScAddr);

//Spacecraft address data access
EPOCH_DBNT_API TCHAR *EpDbGetScAddressMnemonic(
										EpDbScAddressRecord pScAddr);
EPOCH_DBNT_API TCHAR *EpDbGetScAddressCmdEchoMnemonic(
										EpDbScAddressRecord pScAddr);
EPOCH_DBNT_API ULONG EpDbGetScAddressCode(
										EpDbScAddressRecord pScAddr);
EPOCH_DBNT_API BOOL EpDbGetScAddressDefaultAddress(
										EpDbScAddressRecord pScAddr);
EPOCH_DBNT_API BOOL EpDbGetScAddressBypassNullCmdEcho(
										EpDbScAddressRecord pScAddr);
EPOCH_DBNT_API BOOL EpDbGetScAddressCmdEchoRepeat(
										EpDbScAddressRecord pScAddr);

//Command execute verifier (spacecraft format) information
EPOCH_DBNT_API EPLISTSTATUS EpDbScFmtCmdExecVerifierListInit(
										EpDbDatabase pDatabase,
										EpDbCmdExecVerifierRecord *pCmdExec);
//Command execute verifier (command channel) information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdChannelCmdExecVerifierListInit(
										EpDbCmdChannelRecord pCmdChannel,
										EpDbCmdExecVerifierRecord *pCmdExec);
//Command execute verifier (spacecraft address) information
EPOCH_DBNT_API EPLISTSTATUS EpDbScAddrCmdExecVerifierListInit(
										EpDbScAddressRecord pScAddr,
										EpDbCmdExecVerifierRecord *pCmdExec);
//Command execute verifier information
EPOCH_DBNT_API EPLISTSTATUS EpDbCmdExecVerifierListNextEntry(
										EpDbCmdExecVerifierRecord pCmdExec);
EPOCH_DBNT_API EPAPISTATUS EpDbCmdExecVerifierListFree(
										EpDbCmdExecVerifierRecord pCmdExec);

//Command execute verifier data access
EPOCH_DBNT_API TCHAR *EpDbGetCmdExecVerifierTlmMnemonic(
										EpDbCmdExecVerifierRecord pCmdExec);
EPOCH_DBNT_API TCHAR *EpDbGetCmdExecVerifierSimContextMnemonic(
										EpDbCmdExecVerifierRecord pCmdExec);
EPOCH_DBNT_API ULONG EpDbGetCmdExecVerifierSimContextValue(
										EpDbCmdExecVerifierRecord pCmdExec);
EPOCH_DBNT_API int EpDbGetCmdExecVerifierDuration(
										EpDbCmdExecVerifierRecord pCmdExec);

#endif
