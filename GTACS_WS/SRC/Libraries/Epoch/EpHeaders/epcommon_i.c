/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Wed Mar 06 07:56:09 2013
 */
/* Compiler settings for C:\epochnt\eptypes\epcommon.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IEpSSAPI = {0xF3D3679D,0xB7A2,0x11D2,{0xA3,0x36,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID IID__IEpochAliveCB = {0x7B843D45,0x0234,0x11d3,{0x80,0x3B,0x00,0x60,0x08,0xC0,0x94,0x9C}};


const IID IID__IEpochCB = {0xA84304D5,0xBCFF,0x11d2,{0xA3,0x36,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID IID_IEpSrvcBase = {0xA1311B8F,0xAB0F,0x11D2,{0x91,0x56,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpData = {0xBF32FAA7,0xB6D1,0x11D2,{0xA3,0x34,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID IID_IEpEvent = {0x33EB50D1,0xB78B,0x11D2,{0xA3,0x36,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID IID_IEpSTOL = {0xA8BA1B3B,0xB76F,0x11D2,{0xA3,0x34,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID IID__IEpStrmMonCB = {0xB8D8E005,0xDD56,0x11D2,{0xA3,0x3C,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID IID_IEpStrmMon = {0xB8D8E003,0xDD56,0x11D2,{0xA3,0x3C,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID IID_IEpEnumCalibrations = {0x7B843D42,0x0234,0x11d3,{0x80,0x3B,0x00,0x60,0x08,0xC0,0x94,0x9C}};


const IID IID_IEpEnumConversions = {0x7B843D44,0x0234,0x11d3,{0x80,0x3B,0x00,0x60,0x08,0xC0,0x94,0x9C}};


const IID IID_IEpEnumDeltas = {0xEF8ECA00,0xB260,0x11d3,{0x80,0x60,0x00,0x60,0x08,0xC0,0x94,0x9C}};


const IID IID_IEpEnumLimits = {0x7B843D40,0x0234,0x11d3,{0x80,0x3B,0x00,0x60,0x08,0xC0,0x94,0x9C}};


const IID IID_IEpEnumLocations = {0x7B843D41,0x0234,0x11d3,{0x80,0x3B,0x00,0x60,0x08,0xC0,0x94,0x9C}};


const IID IID_IEpEnumStates = {0x45643C33,0x0172,0x11d3,{0x80,0x3B,0x00,0x60,0x08,0xC0,0x94,0x9C}};


const IID IID_IEpEnumPoints = {0x45643C32,0x0172,0x11d3,{0x80,0x3B,0x00,0x60,0x08,0xC0,0x94,0x9C}};


const IID IID_IEpEnumSubsystems = {0xA58873B1,0x2325,0x11d3,{0x80,0x3C,0x00,0x60,0x08,0xC0,0x94,0x9C}};


const IID IID_IEpEnumEvents = {0x2577BF1B,0xA675,0x4f8a,{0x94,0x3C,0x5D,0x02,0x20,0x81,0xC3,0x10}};


const IID IID_IEpEnumTlmLimMsgs = {0x3CF373D6,0xAE61,0x45e0,{0xB7,0xC8,0x8C,0xB4,0xFB,0x8A,0xAC,0x38}};


const IID IID_IEpEnumDatawords = {0xFD66AE26,0xC76E,0x11D3,{0xA9,0x7F,0x00,0x60,0x08,0x95,0xD0,0xD8}};


const IID IID_IEpEnumDatawordArgValues = {0x2BA94F03,0xC79E,0x11D3,{0xA9,0x7F,0x00,0x60,0x08,0x95,0xD0,0xD8}};


const IID IID_IEpEnumDatawordArgRanges = {0x0833A3A3,0xC8FA,0x11D3,{0xA9,0x80,0x00,0x60,0x08,0x95,0xD0,0xD8}};


const IID IID_IEpEnumDatawordArgTvContexts = {0xEE69E0CB,0xB888,0x44a7,{0x99,0x57,0xA0,0xAE,0x1F,0x31,0xA5,0x47}};


const IID IID_IEpEnumDatawordArgs = {0x9229E035,0xC767,0x11D3,{0xA9,0x7F,0x00,0x60,0x08,0x95,0xD0,0xD8}};


const IID IID_IEpEnumCmdDatawordGroups = {0x823BE503,0xCF3E,0x11D3,{0x88,0x86,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpEnumCmdPrivilegeGroups = {0x823BE504,0xCF3E,0x11D3,{0x88,0x86,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpEnumTvEvents = {0x4D098D9C,0xC927,0x11D3,{0x88,0x74,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpEnumTvSequences = {0x4D098D9B,0xC927,0x11D3,{0x88,0x74,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpEnumPreTvStates = {0x0833A3A4,0xC8FA,0x11D3,{0xA9,0x80,0x00,0x60,0x08,0x95,0xD0,0xD8}};


const IID IID_IEpEnumPostTvStates = {0x4D098D91,0xC927,0x11D3,{0x88,0x74,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpEnumPseudoTvStates = {0x4D098D94,0xC927,0x11D3,{0x88,0x74,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpEnumCmdExecVerifiers = {0x823BE4F7,0xCF3E,0x11D3,{0x88,0x86,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpEnumCmdEchoVerifierEvents = {0x4726F5A3,0xD009,0x11D3,{0xA9,0x82,0x00,0x60,0x08,0x95,0xD0,0xD8}};


const IID IID_IEpEnumScAddrs = {0x823BE4F3,0xCF3E,0x11D3,{0x88,0x86,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpEnumCmdDatawordMaps = {0x823BE4FB,0xCF3E,0x11D3,{0x88,0x86,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpEnumCmdFrameFormats = {0x823BE4FF,0xCF3E,0x11D3,{0x88,0x86,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID_IEpEnumCmdChannels = {0x7459AFC3,0xCE7D,0x11D3,{0xA9,0x82,0x00,0x60,0x08,0x95,0xD0,0xD8}};


const IID IID_IEpEnumCmdFmtPointValue = {0x9A17C723,0x213F,0x4859,{0x95,0x8E,0x0F,0xAB,0x1A,0x28,0xAE,0x17}};


const IID IID_IEpEnumCmdFormats = {0x7459AFC7,0xCE7D,0x11D3,{0xA9,0x82,0x00,0x60,0x08,0x95,0xD0,0xD8}};


const IID IID_IEpEnumCommands = {0xF3CADEA5,0xC384,0x11D3,{0xA9,0x7B,0x00,0x60,0x08,0x95,0xD0,0xD8}};


const IID IID_IEpDB = {0x45643C31,0x0172,0x11d3,{0x80,0x3B,0x00,0x60,0x08,0xC0,0x94,0x9C}};


const IID IID_IEpAttrib = {0x26348FEE,0xE907,0x11d2,{0xA3,0x3C,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID IID_IEpDBHelper = {0x80E8F3DB,0x2EDA,0x11D3,{0xA3,0x4F,0x00,0x60,0x08,0x67,0xA0,0xE2}};


const IID IID_IEpProcCtrl = {0x7007E6F1,0xAE34,0x11D3,{0x92,0x6F,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID__IEpProcCB = {0xE44C4B11,0xAE38,0x11d3,{0x92,0x6F,0x00,0x00,0x00,0x00,0x00,0x00}};


const IID IID__IEpTrigSrvcModCB = {0x321049CE,0x5E0D,0x48b6,{0x97,0xB6,0xF6,0x7E,0xB9,0x3D,0x1A,0xC4}};


const IID IID_IEpTrigSrvcMod = {0xD2E2604D,0x6B00,0x43e5,{0x91,0x06,0xD7,0xC6,0xA8,0x05,0x95,0xE7}};


const IID IID_IEpTrigClntMod = {0x4179B40B,0xE367,0x4c65,{0x98,0xBE,0xB7,0x87,0x8D,0x5D,0x95,0x86}};


const IID IID_IEpTrigCtrl = {0x13A149FF,0xCF5E,0x4e59,{0x9C,0xEA,0x5A,0xAC,0x1D,0xBF,0xC5,0xF4}};


const IID IID__IEpTrigSrvcCB = {0xC5960CBB,0x1822,0x4af7,{0xB8,0xE3,0xE5,0x8D,0x55,0x34,0x1C,0x50}};


const IID IID_IEpIR = {0xC6209DE4,0x9AA5,0x4508,{0xB0,0x0F,0x68,0x9B,0xCF,0x9E,0x6C,0xC5}};


#ifdef __cplusplus
}
#endif

