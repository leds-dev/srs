//Copyright 1997-2003 Integral Systems, Inc. All rights reserved. 
/*
@(#) epochnt source eptypes/epenums.h 1.66 04/08/24 17:35:48
*/
/******************************************************************************
 Filename: epenums.h

 Author:		Jeff Shaffer - Integral Systems, Inc.
 Created:		April 22, 1999

 This header file contains enumerated types common to multiple EPOCH NT
 projects.  It is imported by epcommon.idl, and so is #include'd by
 epcommon.h as a result.  If a source file does not use epcommon.h, it may
 #include this file directly.
 
******************************************************************************/
#ifndef EPENUMS_H_DEFINED
#define EPENUMS_H_DEFINED

//Definition of ulFlags in EPLIMITOFFINFO
//Keep your old list of mnemonics, nothing has changed:
#define EPLIMITOFF_UNCHANGED			0x00000001
//This is the first record in the new list:
#define EPLIMITOFF_BEGINLIST			0x00000002
//This is the last record in the list:
#define EPLIMITOFF_ENDLIST				0x00000004

//Definition of ulFlags in EPLIMITREC
#define EPLIMITRECFLAGS_INITIALSET		0x00000001
#define EPLIMITRECFLAGS_STREAMSTART		0x00000002
#define EPLIMITRECFLAGS_NODATA			0x00000004

/******************************************************************************
	The following #define's mirror those in EPOCH server file access.h.  If the
	definitions in access.h change, then these must as well.
******************************************************************************/
#define	EPOCHACCESS_MODIFY_DB		0x0001
#define	EPOCHACCESS_MODIFY_DISPLAY	0x0002
#define	EPOCHACCESS_OTS_PUSH		0x0004
#define	EPOCHACCESS_OTS_PULL		0x0008
#define	EPOCHACCESS_MONITOR			0x0010
#define	EPOCHACCESS_NOCHECK			-1

//enum used to define behavior when multiple conditions are defined
typedef enum EPTRIGMCO
{
	EPTRIGMCO_ANY,
	EPTRIGMCO_ALL
} EPTRIGMCO;

//enum used to define behavior when multiple actions are defined
typedef enum EPTRIGMAO
{
	EPTRIGMAO_ALL,
	EPTRIGMAO_STOPONSUCCESS,
	EPTRIGMAO_STOPONFAILURE
} EPTRIGMAO;

//Used for mapping in IEpTrigCtrl methods
typedef enum EPTRIGMODTYPE
{
	EPTRIGMODTYPE_CONDITION,
	EPTRIGMODTYPE_ACTION
} EPTRIGMODTYPE;

//enum used with GetItemText() to allow EPOCH Trigger Viewer to
//populate static info in the TreeList control
typedef enum EPCLNTMODFLD
{
	EPCLNTMODFLD_TYPE, //Used by most modules
	EPCLNTMODFLD_DETAIL, //Used by most modules
	EPCLNTMODFLD_STRM //Initially added to support mnem to mnem comparisons
} EPCLNTMODFLD;

//Flags returned via IEpTrigSrvcMod::GetModInfo() to indicate functionality
#define EPSMFLAG_POINTHANDLER 0x00000001
#define EPSMFLAG_EVENTHANDLER 0x00000002

//Hints for Trigger (Condition/Action) Service Module
//These hints are sent from the Service to the Client
#define EPSMHINT_STATUS			0x00000001
#define EPSMHINT_ID				0x00000002
#define EPSMHINT_BLOBDATA		0x00000004
#define EPSMHINT_PTVALUE		0x00000008
#define EPSMHINT_ALL			0x0000000F //All members should be evaluated
#define EPSMHINT_EPPTDATA		0x01000000 //Can interpret as trigValue data similar to telemetry viewer.

//Definition of ulStatus for EPSMCBDATA
#define EPSMSTATUS_DEACTIVATED		0x00000001 //Means Monitoring for Conds and Executing for Acts
#define EPSMSTATUS_ACTIVATED		0x00000002
#define EPSMSTATUS_WAIT				0x00000004 //Acts
#define EPSMSTATUS_SATISFIED		0x00000008 //Conds
#define EPSMSTATUS_NOTSATISFIED		0x00000010 //Conds
#define EPSMSTATUS_SUCCEEDED		0x00000020 //Acts
#define EPSMSTATUS_FAILED			0x00000040 //Acts
#define EPSMSTATUS_ERROR			0x00000080 //Acts and Conds

//Flags for replacable tokens
#define EPSMTOKEN_n 0x00000001 //Name of trigger
#define EPSMTOKEN_c 0x00000002 //Condition info
#define EPSMTOKEN_a 0x00000004 //Action info
#define EPSMTOKEN_t 0x00000008 //Time trigger fired
#define EPSMTOKEN_h 0x00000010 //Host where trigger service is running
#define EPSMTOKEN_m 0x00000020 //Mode service is running in (fail over)
#define EPSMTOKEN_A 0x00000040 //All info in nice format.

//Hints for Trigger Defs
#define EPTDHINT_TRIGNAME		0x00000001
#define EPTDHINT_STATUS			0x00000002
#define EPTDHINT_USER			0x00000004
#define EPTDHINT_HYSTCNT		0x00000008
#define EPTDHINT_HYST			0x00000010
#define EPTDHINT_FIREDCNT		0x00000020
#define EPTDHINT_DISABLECNT		0x00000040
#define EPTDHINT_EXPIRES		0x00000080
#define EPTDHINT_EXECCNT		0x00000100
#define EPTDHINT_CONDOPT		0x00000200
#define EPTDHINT_NUMCONDS		0x00000400
#define EPTDHINT_CONDDATA		0x00000800
#define EPTDHINT_ACTOPT			0x00001000
#define EPTDHINT_NUMACTS		0x00002000
#define EPTDHINT_ACTDATA		0x00004000
#define EPTDHINT_ALL			0x00007FFF

//Definition of ulStatus for Triggers
#define EPTRIGSTATUS_DISABLED		0x00000001
#define EPTRIGSTATUS_ENABLED		0x00000002
#define EPTRIGSTATUS_DELETED		0x00000004
#define EPTRIGSTATUS_ERROR			0x00000008 //Error occurred, such as tried to start but couldn't

//Definition of ulStatusBits in EPPTDATA
#define EPPTSTATUS_NO_DATA_VALUE		0x00000001
#define EPPTSTATUS_QUESTIONABLE			0x00000002
#define EPPTSTATUS_CONVERSION_ERROR		0x00000004
#define EPPTSTATUS_STALE				0x00000008
#define EPPTSTATUS_PT_CONTEXT_CHANGED	0x00000010
#define EPPTSTATUS_IN_LIMITS			0x00000020
#define EPPTSTATUS_LOW_WARNING			0x00000040
#define EPPTSTATUS_HIGH_WARNING			0x00000080
#define EPPTSTATUS_LOW_ALARM			0x00000100
#define EPPTSTATUS_HIGH_ALARM			0x00000200
#define EPPTSTATUS_LOW_DELTA			0x00000400
#define EPPTSTATUS_HIGH_DELTA			0x00000800
//bit 0x00001000 is unused, as per libstrm/scs.h
#define EPPTSTATUS_NEED_RED_ACK			0x00002000
#define EPPTSTATUS_ACKED				0x00004000
#define EPPTSTATUS_YELLOW_ACK			0x00008000
#define EPPTSTATUS_ACK_BITS				0x0000E000

//Bitmasks used to interpret eChgHint member of EPSTRMINFO struct
#define STRMINFO_NONE		0x00
#define STRMINFO_STATUS		0x01
#define STRMINFO_DB			0x02
#define STRMINFO_RES1		0x04
#define STRMINFO_RES2		0x08
#define STRMINFO_ALL (STRMINFO_STATUS | STRMINFO_DB | STRMINFO_RES1 | STRMINFO_RES2)

//Procedure info/tracing #define and and enum values 
//Bitmasks used to interpret eChgHint member of EPPROCINFO struct
#define EPPIHINT_NONE			0x00000000
#define EPPIHINT_STATE			0x00000001
#define EPPIHINT_PROCTYPE		0x00000002
#define EPPIHINT_STREAM			0x00000004
#define EPPIHINT_OWNER			0x00000008
#define EPPIHINT_SUBROUTINE		0x00000010
#define EPPIHINT_PATHNAME		0x00000020
#define EPPIHINT_CLIENT			0x00000040
#define EPPIHINT_ALL			0x0000007F

//Procedure state bitmasks.  These should really be enum's since they are
//mutually exclusive, but they are used in too many places now.
#define EPPROCSTATE_STOPPED		0x00000000 //When received by EpSTOL.exe, remove from summary list.
#define EPPROCSTATE_RUNNING		0x00000001
#define EPPROCSTATE_PAUSED		0x00000002
#define EPPROCSTATE_STEPPING	0x00000004
#define EPPROCSTATE_WAITING		0x00000008
#define EPPROCSTATE_WAITINPUT	0x00000010
#define EPPROCSTATE_ERROR		0x00000020
//The following state is not in EPOCH server, our services generate it:
#define EPPROCSTATE_STARTING	0x00000040

//Bitmasks used for ulFlags member of EPPROCTRACE struct
//If EPPTFLAG_LINEOFFSET is set, then ulCharOffset members contain a line number,
//and ulHighlightWidth members should be ignored.
#define EPPTFLAG_LINEOFFSET		0x00000001

//Bitmasks used to interpret eChgHint member of EPPROCTRACE struct
#define EPPTHINT_NONE			0x00000000
#define EPPTHINT_STREAM			0x00000001
#define EPPTHINT_PROCSTATE		0x00000002
#define EPPTHINT_WAITEXP		0x00000004
#define EPPTHINT_OFFSET			0x00000008
#define EPPTHINT_CSDEPTH		0x00000010
#define EPPTHINT_DIRRTR			0x00000020
#define EPPTHINT_CLIENT			0x00000040
#define EPPTHINT_ALL			0x0000007F

typedef enum _EPPROCTYPE
{
	EPPROCTYPE_LOCAL,
	EPPROCTYPE_MASTERSCHED,
	EPPROCTYPE_NONE
} EPPROCTYPE;

//Callback condition types.  These must match values in enum cb_condition_type
//in libstrm/ss.h.
typedef enum EPCBCOND {
	EPCBCOND_ALL_SAMPLES,
	EPCBCOND_CHANGED_SAMPLES
} EPCBCOND;

//CLIM status
typedef enum EPCLIMSTATUS {
	EPCLIMSTATUS_OFF,
	EPCLIMSTATUS_FREE,
	EPCLIMSTATUS_INUSE
} EPCLIMSTATUS;

//Command auto execute modes
typedef enum EPCMDAEMODE {
	EPCMDAEMODE_N,
	EPCMDAEMODE_Y
} EPCMDAEMODE;

//Command CV modes
typedef enum EPCMDCVMODE {
	EPCMDCVMODE_OFF,
	EPCMDCVMODE_WAIT,
	EPCMDCVMODE_NOWAIT
} EPCMDCVMODE;

//Command CV status
typedef enum EPCMDCVSTATUS {
	EPCMDCVSTATUS_OFF,
	EPCMDCVSTATUS_PENDING,
	EPCMDCVSTATUS_VERIFIED,
	EPCMDCVSTATUS_REJECTED,
	EPCMDCVSTATUS_TIMEOUT,
	EPCMDCVSTATUS_FAILURE,
	EPCMDCVSTATUS_SUCCESS
} EPCMDCVSTATUS;

//Command CV types.  CV is the overall CV status (which may consist of more than
//one CV check).  FCV is frame counter verification.  CCV is command counter
//verification.  CEV is command echo verification.  CEXV is command execution
//verification.
typedef enum EPCMDCVTYPE {
	EPCMDCVTYPE_CV,
	EPCMDCVTYPE_FCV,
	EPCMDCVTYPE_CCV,
	EPCMDCVTYPE_CEV,
	EPCMDCVTYPE_CEXV
} EPCMDCVTYPE;

//Command PV modes
typedef enum EPCMDPVMODE {
	EPCMDPVMODE_OFF,
	EPCMDPVMODE_ON
} EPCMDPVMODE;

//Command PV status
typedef enum EPCMDPVSTATUS {
	EPCMDPVSTATUS_OFF,
	EPCMDPVSTATUS_PENDING,
	EPCMDPVSTATUS_SUCCESS,
	EPCMDPVSTATUS_FAILURE
} EPCMDPVSTATUS;

//Command status record types
typedef enum EPCMDREC {
	EPCMDREC_INIT,
	EPCMDREC_UPDATE,
	EPCMDREC_PV,
	EPCMDREC_CV,
	EPCMDREC_TV,
	EPCMDREC_TVALL,
	EPCMDREC_CLEAR
} EPCMDREC;

//Command retransmit modes
typedef enum EPCMDRTMODE {
	EPCMDRTMODE_N,
	EPCMDRTMODE_Y
} EPCMDRTMODE;

//Command execution status
typedef enum EPCMDSTATUS {
	EPCMDSTATUS_QUEUED,
	EPCMDSTATUS_UPLINKED,
	EPCMDSTATUS_PRIMED,
	EPCMDSTATUS_EXECUTED,
	EPCMDSTATUS_STORED,
	EPCMDSTATUS_LOADING,
	EPCMDSTATUS_OPENED,
	EPCMDSTATUS_CLOSED,
	EPCMDSTATUS_ATTACHED,
	EPCMDSTATUS_ERASED,
	EPCMDSTATUS_ERROR,
	EPCMDSTATUS_REXMITTED,
	EPCMDSTATUS_CLEARED,
	EPCMDSTATUS_FAILURE,
	EPCMDSTATUS_COMPLETED
} EPCMDSTATUS;

//Command TV modes
typedef enum EPCMDTVMODE {
	EPCMDTVMODE_OFF,
	EPCMDTVMODE_WAIT,
	EPCMDTVMODE_NOWAIT
} EPCMDTVMODE;

//Command TV status
typedef enum EPCMDTVSTATUS {
	EPCMDTVSTATUS_OFF,
	EPCMDTVSTATUS_PENDING,
	EPCMDTVSTATUS_TIMEOUT,
	EPCMDTVSTATUS_FAILURE,
	EPCMDTVSTATUS_SUCCESS
} EPCMDTVSTATUS;

//Conversion types.  These must match values in enum conversion_type in
//libstrm/ss.h.  NOTE:  These are NOT display conversion types, they are
//telemetry conversion types.
typedef enum EPCONVTYPE {
	EPCONVTYPE_NONE,
	EPCONVTYPE_RAW,
	EPCONVTYPE_EU,
	EPCONVTYPE_LINEAR,
	EPCONVTYPE_STATE,
	EPCONVTYPE_TIME,
	EPCONVTYPE_VOLTS,
	EPCONVTYPE_DEFAULT,
	EPCONVTYPE_UNKNOWN_RAW
} EPCONVTYPE;

//Stream database modify functions and types, used in struct EPDBMODINFO
typedef enum EPDBMODFCN {
	EPDBMODFCN_SET,
	EPDBMODFCN_RESET,
	EPDBMODFCN_CLEARALL
} EPDBMODFCN;

typedef enum EPDBMODTYPE {
	EPDBMODTYPE_STATEH,
	EPDBMODTYPE_STATEL,
	EPDBMODTYPE_STATECONTEXTH,
	EPDBMODTYPE_STATECONTEXTL,
	EPDBMODTYPE_REDH,
	EPDBMODTYPE_YELLOWH,
	EPDBMODTYPE_YELLOWL,
	EPDBMODTYPE_REDL,
	EPDBMODTYPE_LIMITCONTEXTH,
	EPDBMODTYPE_LIMITCONTEXTL,
	EPDBMODTYPE_LIMITCOUNT,
	EPDBMODTYPE_DELTAH,
	EPDBMODTYPE_DELTAL,
	EPDBMODTYPE_DELTACONTEXTH,
	EPDBMODTYPE_DELTACONTEXTL,
	EPDBMODTYPE_RAW,
	EPDBMODTYPE_ENG,
	EPDBMODTYPE_CALCONTEXTH,
	EPDBMODTYPE_CALCONTEXTL,
	EPDBMODTYPE_C0,
	EPDBMODTYPE_C1,
	EPDBMODTYPE_C2,
	EPDBMODTYPE_C3,
	EPDBMODTYPE_C4,
	EPDBMODTYPE_C5,
	EPDBMODTYPE_C6,
	EPDBMODTYPE_C7,
	EPDBMODTYPE_C8,
	EPDBMODTYPE_EUCONTEXTH,
	EPDBMODTYPE_EUCONTEXTL
} EPDBMODTYPE;

//Event filtering categories, for use in the event GUI.
typedef enum EPEVTCAT {
	EPEVTCAT_NONE,
	EPEVTCAT_TEXT,
	EPEVTCAT_CMD,
	EPEVTCAT_TLM_LIMIT,
	EPEVTCAT_GV_LIMIT,
	EPEVTCAT_TV,
	EPEVTCAT_SYSTEM_STATUS,
	EPEVTCAT_DIRECTIVE,
	EPEVTCAT_DIR_RESP,
	EPEVTCAT_ARCHIVE_PLAYBACK,
	EPEVTCAT_HW,
	EPEVTCAT_TLM,
	EPEVTCAT_NODE_MANAGER
} EPEVTCAT;

//Event service types.  These must match values in enum evt_service_type in
//libstrm/ss.h
typedef enum EPEVTSERVICE {
	EPEVTSERVICE_UNFORMATTED,
	EPEVTSERVICE_FORMATTED,
	EPEVTSERVICE_OOL_EVT,
	EPEVTSERVICE_CMD_EVT,
	EPEVTSERVICE_USC_EVT
} EPEVTSERVICE;

//Limit record types.  This enum is used in EPLIMITINFO records to specify
//the type of record being sent.
typedef enum EPLIMITREC {
	EPLIMITREC_FULL,
	EPLIMITREC_UPDATE
} EPLIMITREC;

//Callback service mode types.  These must match values in enum
//service_mode_type in libstrm/ss.h
typedef enum EPSERVICEMODE {
	EPSERVICEMODE_NONE,
	EPSERVICEMODE_SYNC,
	EPSERVICEMODE_ASYNC
} EPSERVICEMODE;

//Stream status returned within IEpStrmMon interface's struct
typedef enum EPSTRMSTAT
{
	STRMSTAT_STARTING,
	STRMSTAT_STOPPING,
	STRMSTAT_UP,
	STRMSTAT_DOWN,
	STRMSTAT_DISCONNECTED,
	STRMSTAT_DISCONNECTING,
	STRMSTAT_CONNECTING
} EPSTRMSTAT;

//Stream types returned within IEpStrmMon interface's struct
typedef enum EPSTRMTYPE
{
	EPSTRM_ALL,			//Unused for now
	EPSTRM_REALTIME,
	EPSTRM_GROUND,
	EPSTRM_PLAYBACK,
	EPSTRM_SIMULATION,
	EPSTRM_UNKNOWN
} EPSTRMTYPE;

//System status type of EPVAR
typedef enum EPSYSSTAT {
	EPSYSSTAT_NORMAL,
	EPSYSSTAT_WARNING,
	EPSYSSTAT_ALARM
} EPSYSSTAT;

//Update flags in ulFlags field of stream service update_status_new
//and update_status_current structures.
typedef enum EPUPDATEFLAGS {
	EPUPDATEFLAGS_END,
	EPUPDATEFLAGS_FULL,
	EPUPDATEFLAGS_STATUS_ONLY
} EPUPDATEFLAGS;

//EPVAR types
typedef enum EPVARENUM {
	EPVT_EMPTY = 0,
	EPVT_UI4 = 4,
	EPVT_R8 = 8,
	EPVT_BSTR = 10,
	EPVT_BLOB = 12,
	EPVT_TIMEVAL = 14,
	EPVT_SS = 16
} EPVARENUM;

//EPOCH (ActiveX Control) Item Stream types and bitmasks
//Bitmasks error codes returned in EPATTRIB_ERR structure

//Menu bitmasks
#define EPITEM_MENU_ENABLED 0x00000001
#define EPITEM_MENU_CHECKED 0x00000002
//additional defines go here as necessary to support types/states of menus

//General Page
#define EPATTRIB_ERR_NONE 0x0 //ORing this with any other flag obviously negates it
#define EPATTRIB_ERR_STRMNAME 0x00000001
#define EPATTRIB_ERR_MNEM 0x00000002
#define EPATTRIB_ERR_DB 0x00000004

//Format Page
#define EPATTRIB_ERR_CONV 0x00000001 //Format Property Page -> Conversion

//Sampling Page
#define EPATTRIB_ERR_SAMPPOLICY 0x00000001 //Sampleing Property Page -> Sampling Policy
#define EPATTRIB_ERR_SAMPRATE 0x00000002 // Sampleing Property Page -> Sampling Rate

//Identifiers used by IEpAttrib::DisableItems()
//General Property Page
#define EPATTRIB_PTINDEX 0x00000002
#define EPATTRIB_STRMNAME 0x00000004
#define EPATTRIB_MNEM 0x00000008

//Format Property Page
#define EPATTRIB_USEDBCONV 0x00000001
#define EPATTRIB_EUCONV 0x00000002
#define EPATTRIB_LINEARCONV 0x00000004
#define EPATRIB_FLOATCONV 0x00000008
#define EPATTRIB_STATECONV 0x00000010
#define EPATTRIB_TIMECONV 0x00000020
#define EPATTRIB_VOLTSCONV 0x00000040
#define EPATTRIB_STRINGCONV 0x00000080
#define EPATTRIB_HEXCONV 0x00000100
#define EPATTRIB_OCTCONV 0x00000200
#define EPATTRIB_DECCONV 0x00000400
#define EPATTRIB_BINCONV 0x00000800
#define EPATTRIB_SCCONV 0x00001000
#define EPATTRIB_ALLCONV 0x00002000

//Sampling Page
#define EPATTRIB_SAMP_POLICY 0x00000001
#define EPATTRIB_SAMP_RATE 0x000000002

//enums
typedef enum EPITEMSP
{
	EPITEMSP_ALL,
	EPITEMSP_ONCHANGE,
	EPITEMSP_PERIODIC,
} EPITEMSP;

/////////////////////////////
// Plot attribute page

// Limit band flags
#define EPATTRIB_REDLIMIT_BAND		0x0001
#define EPATTRIB_YELLOWLIMIT_BAND	0x0002
#define EPATTRIB_GREENLIMIT_BAND	0x0004
#define EPATTRIB_INDTAKESLIMITCOLOR	0x0008
#define EPATTRIB_USECAPTIONGOOD		0x0010
#define EPATTRIB_USECAPTION			0x0020
#define EPATTRIB_STAT_MIN			0x0100
#define EPATTRIB_STAT_MAX			0x0200
#define EPATTRIB_STAT_AVG			0x0400
#define EPATTRIB_STAT_STDDEV		0x0800

//EPOCH (Control) Item Stream types
typedef enum EPITEMST {
	EPITEMST_APPDEFAULT,
	EPITEMST_PAGEDEFAULT,
	EPITEMST_CUSTOM
} EPITEMST;

//Data stream (not EPOCH stream) connectivity enum
//used to notify clients about state of their point, event,
//etc. service requests.  Also, used as a general indicator
//as to whether or not an item in an EpCtrl is connected.
typedef enum EPCONSTATE {
	EPCONSTATE_DISCONNECTED, //Indicates connectivity between GUI and Service
	EPCONSTATE_CONNECTED, //Indicates connectivity between GUI and Service
	EPCONSTATE_STRMDISCONNECTED, //Indicates connectivity between Service and front-end
	EPCONSTATE_STRMCONNECTED //Indicates connectivity between Service and front-end
} EPCONSTATE;

typedef enum EPATTRIB_PAGE 
{
	EPATTRIB_PAGE_NULL, 
	EPATTRIB_PAGE_GENERAL,
	EPATTRIB_PAGE_FORMAT,
	EPATTRIB_PAGE_SAMPLING,
	EPATTRIB_PAGE_ATCK,
	EPATTRIB_PAGE_DIRECTIVE,
	EPATTRIB_EVENT
} EPATTRIB_PAGE;

typedef enum EPDISPFRMT 
{
	EPDISPFRMT_EU,
	EPDISPFRMT_LINEAR,
	EPDISPFRMT_FLOAT,
	EPDISPFRMT_VOLTS,
	EPDISPFRMT_HEX,
	EPDISPFRMT_OCT,
	EPDISPFRMT_DEC,
	EPDISPFRMT_SC
} EPDISPFRMT;

// Standard properties flags
#define EPPROP_FORECOLOR_USESYSTEM 0x00000001
#define EPPROP_BACKCOLOR_USESYSTEM 0x00000002
#define EPPROP_FONTS_SAVED         0x00000004	// Fonts have been saved
#define EPPROP_FORECOLOR_USEALARM  0x00000008
#define EPPROP_BACKCOLOR_USEALARM  0x00000010

//Command execution types. These must match the enum cmd_exec_verification_type
//in include/db_params.h.

typedef enum EPDBCMDEXECTYPE {
	EPDBCMDEXECTYPE_AUTO_CMD_EXEC,
	EPDBCMDEXECTYPE_TELE_CMD_EXEC,
	EPDBCMDEXECTYPE_HLCD_CMD_EXEC,
	EPDBCMDEXECTYPE_HS376_CMD_EXEC,
	EPDBCMDEXECTYPE_SPACEBUS3000_CMD_EXEC,
	EPDBCMDEXECTYPE_MAX_CMD_EXEC_VERIFICATION_TYPE
} EPDBCMDEXECTYPE;

#endif





