/*
@(#) epoch windows_api epochaccess/epochaccess.h 1.3 04/08/24 17:19:34
*/
/*
@(#) epochnt source epochaccess/epochaccess.h 1.7 02/11/19 14:18:59
*/

/******************************************************************************
 Filename: epochaccess.h

 Author:		Jeff Shaffer - Integral Systems, Inc.
 Created:		August 1, 2000

 This header file contains the function prototypes and data definitions for
 clients of the EPOCH NT access control dll (epochaccess.dll).

 This file should be used to build NT programs that link to the dll, as well
 as the dll itself.  By placing __declspec() statements in this header and
 including the header in the source files, the __declspec() statements do not
 need to be added before functions in the c source file(s).

 To build a dll using this header, make sure that symbol BUILD_EPOCHACCESS_DLL
 is defined.  To build an application using this header, make sure that symbol
 BUILD_EPOCHACCESS_DLL is not defined.

 Notes:	

 1) __declspec(dllimport) and __declspec(dllexport) are compiler specific. It
    is assumed that Microsoft's VC++ compiler is being used to build the dll
	and application(s) that use it.  If this is not the case, see Advanced
	Windows by Jeffrey Richter (pg. 564) to learn how to handle name decoration
	issues.

******************************************************************************/

#ifndef EPOCHACCESS_H_DEFINED
#define EPOCHACCESS_H_DEFINED

#include "epserverenums.h"

/******************************************************************************
	DLL entry point prototypes:  These match the functions in EPOCH server file
	access.c.
******************************************************************************/

#ifdef BUILD_EPOCHACCESS_DLL
#    define EPOCH_NT_API __declspec(dllexport)
#else
#    define EPOCH_NT_API __declspec(dllimport)
#endif

//Single user functions
EPOCH_NT_API int epochaccess_check_dir(char *szDirective);
EPOCH_NT_API int epochaccess_check_func(int nFunc);
EPOCH_NT_API int epochaccess_check_stream(char *stream);
EPOCH_NT_API int epochaccess_group(char *szGroupName);
EPOCH_NT_API int epochaccess_init(char *szUserName);

//Multi-user functions
EPOCH_NT_API int epochaccess_make_active_user(char *szUserName);
EPOCH_NT_API int epochaccess_user_check_dir(char *szUserName, char *szDirective);
EPOCH_NT_API int epochaccess_user_check_func(char *szUserName, int nFunc);
EPOCH_NT_API int epochaccess_user_group(char *szUserName, char *szGroupName);
EPOCH_NT_API int epochaccess_user_check_dir_stream(char *szUserName, char *szDirective, char *szStream);
EPOCH_NT_API int epochaccess_user_check_func_stream(char *szUserName, int nFunc, char *szStream);
EPOCH_NT_API int epochaccess_user_group_stream(char *szUserName, char *szGroupName, char *szStream);

EPOCH_NT_API int epochaccess_user_check_dir_stream(char *szUserName, char *szDirective, char *szStream);
EPOCH_NT_API int epochaccess_user_check_func_stream(char *szUserName, int nFunc, char *szStream);
EPOCH_NT_API int epochaccess_user_group_stream(char *szUserName, char *szGroupName, char *szStream);

#endif
