/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Fri Feb 04 13:20:48 2011
 */
/* Compiler settings for epservertypes.idl:
    Os (OptLev=s), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __epservertypes_h__
#define __epservertypes_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "epserverenums.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

/* interface __MIDL_itf_epservertypes_0000 */
/* [local] */ 

#ifdef _INC_TV
typedef struct  timeval
    {
    long tv_sec;
    long tv_usec;
    }	TIMEVAL;

#endif
typedef struct  tagEPBLOB
    {
    ULONG ulArrySize;
    /* [size_is] */ BYTE __RPC_FAR *pBlobData;
    }	EPBLOB;

#define EPOCH_LONGVAL_ARRAY_SIZE	4
typedef struct  ep_limits_msg_data
    {
    INT message_id;
    INT tlm_point_index;
    INT limit_type;
    DOUBLE raw_value;
    DOUBLE limit_trigger;
    ULONG status;
    }	ep_limits_msg_data;

typedef struct  ep_gv_limits_msg_data
    {
    INT condition;
    INT gv_index;
    DOUBLE gv_value;
    DOUBLE limit_trigger;
    ULONG status;
    }	ep_gv_limits_msg_data;

typedef struct  ep_enh_gv_limits_msg_data
    {
    INT condition;
    INT gv_index;
    INT new_violation;
    DOUBLE gv_value;
    TIMEVAL violation_time;
    DOUBLE limit_trigger;
    ULONG status;
    }	ep_enh_gv_limits_msg_data;

typedef struct  ep_cmd_echo_error_msg_data
    {
    ULONG error_code;
    }	ep_cmd_echo_error_msg_data;

typedef struct  ep_tlmver_msg_data
    {
    INT sc_address_index;
    INT tlm_point_index;
    INT cmd_index;
    UINT cmd_reference;
    }	ep_tlmver_msg_data;

typedef struct  ep_exec_tone_msg_data
    {
    LONG pulse_width;
    LONG period;
    INT num_pulses;
    }	ep_exec_tone_msg_data;

typedef struct  ep_command_msg_data
    {
    INT sc_address_index;
    INT cmd_index;
    ULONG cmd_code;
    UINT cmd_reference;
    ULONG cmd_status;
    }	ep_command_msg_data;

typedef struct  ep_command_time_msg_data
    {
    INT sc_address_index;
    INT cmd_index;
    ULONG cmd_code;
    TIMEVAL cmd_time;
    UINT cmd_reference;
    ULONG cmd_state;
    ULONG cmd_state_mask;
    }	ep_command_time_msg_data;

typedef struct  ep_command_long_msg_data
    {
    INT sc_address_index;
    INT cmd_index;
    ULONG cmd_code;
    INT num_longs;
    ULONG l[ 4 ];
    UINT cmd_reference;
    ULONG cmd_status;
    }	ep_command_long_msg_data;

typedef struct  ep_command_file_msg_data
    {
    UINT reference_id;
    ULONG cmd_status;
    BSTR aptr;
    BSTR file_name;
    }	ep_command_file_msg_data;

typedef struct  ep_command_frame_msg_data
    {
    UINT reference_id;
    UINT record_num;
    BSTR aptr;
    BSTR file_name;
    }	ep_command_frame_msg_data;

typedef struct  ep_command_frame_long_msg_data
    {
    UINT reference_id;
    UINT record_num;
    ULONG l;
    BSTR aptr;
    BSTR file_name;
    }	ep_command_frame_long_msg_data;

typedef struct  ep_command_frame_time_msg_data
    {
    UINT reference_id;
    UINT record_num;
    TIMEVAL time;
    BSTR aptr;
    BSTR file_name;
    }	ep_command_frame_time_msg_data;

typedef struct  ep_command_block_msg_data
    {
    UINT reference_id;
    UINT sequence_num;
    }	ep_command_block_msg_data;

typedef struct  ep_command_block_long_msg_data
    {
    UINT reference_id;
    UINT sequence_num;
    ULONG l;
    }	ep_command_block_long_msg_data;

typedef struct  ep_command_block_time_msg_data
    {
    UINT reference_id;
    UINT sequence_num;
    TIMEVAL time;
    }	ep_command_block_time_msg_data;

typedef struct  ep_command_int_msg_data
    {
    INT sc_address_index;
    INT cmd_index;
    INT i;
    UINT cmd_reference;
    ULONG cmd_status;
    }	ep_command_int_msg_data;

typedef struct  ep_command_strval_msg_data
    {
    INT sc_address_index;
    INT cmd_index;
    UINT cmd_reference;
    ULONG cmd_status;
    BSTR aptr;
    BSTR strval;
    }	ep_command_strval_msg_data;

typedef struct  ep_command_start_data
    {
    INT cmd_index;
    ULONG cmd_code;
    UINT cmd_reference;
    ULONG cmd_status;
    INT cmd_sequence;
    INT n_datawords;
    LONG dataword[ 4 ];
    }	ep_command_start_data;

typedef struct  ep_tlm_msg_data
    {
    UINT tlm_point_index;
    TIMEVAL reference_id;
    DOUBLE raw_value;
    DOUBLE eu_value;
    }	ep_tlm_msg_data;

typedef struct  ep_enh_tlm_msg_data
    {
    UINT tlm_point_index;
    TIMEVAL reference_id;
    DOUBLE raw_value;
    DOUBLE eu_value;
    BSTR aptr;
    BSTR state_value;
    }	ep_enh_tlm_msg_data;

typedef struct  ep_usc_ack_data
    {
    UINT tlm_point_index;
    TIMEVAL reference_id;
    }	ep_usc_ack_data;

typedef struct  ep_usc_data
    {
    UINT tlm_point_index;
    }	ep_usc_data;

typedef struct  ep_strval_msg_data
    {
    BSTR aptr;
    BSTR strval;
    }	ep_strval_msg_data;

typedef struct  ep_command_data_msg_data
    {
    INT sc_address_index;
    INT cmd_index;
    UINT cmd_reference;
    UINT msg_num;
    UINT num_msgs;
    UINT num_bytes;
    UCHAR data[ 20 ];
    }	ep_command_data_msg_data;

typedef struct  ep_cmd_frame_out_data
    {
    UINT reference_id;
    UINT sequence_num;
    BSTR aptr;
    BSTR strval;
    }	ep_cmd_frame_out_data;

typedef struct  ep_ss_limits_msg_data
    {
    INT condition;
    INT ss_index;
    }	ep_ss_limits_msg_data;

typedef struct  ep_limits_ack_msg_data
    {
    INT point_type;
    INT point_index;
    ULONG status;
    }	ep_limits_ack_msg_data;

typedef struct  ep_limits_ack_id
    {
    INT point_type;
    INT pid;
    }	ep_limits_ack_id;

typedef struct  ep_limits_ack_id_msg
    {
    INT num;
    /* [size_is] */ ep_limits_ack_id __RPC_FAR *ids;
    }	ep_limits_ack_id_msg;

typedef struct  ep_dbmod_value_msg_data
    {
    INT point_type;
    INT point_index;
    INT context;
    INT field;
    DOUBLE value;
    }	ep_dbmod_value_msg_data;

typedef struct  ep_dbmod_char_msg_data
    {
    INT point_type;
    INT point_index;
    INT context;
    INT field;
    BSTR aptr;
    BSTR value;
    }	ep_dbmod_char_msg_data;

typedef struct  tagEPEVT
    {
    VARTYPE vt;
    /* [switch_is] */ /* [switch_type] */ union 
        {
        /* [case()] */ ep_limits_msg_data lVal;
        /* [case()] */ ep_gv_limits_msg_data glVal;
        /* [case()] */ ep_enh_gv_limits_msg_data eglVal;
        /* [case()] */ ep_cmd_echo_error_msg_data ceVal;
        /* [case()] */ ep_tlmver_msg_data tvVal;
        /* [case()] */ ep_exec_tone_msg_data exec_toneVal;
        /* [case()] */ ep_command_msg_data cVal;
        /* [case()] */ ep_command_time_msg_data ctVal;
        /* [case()] */ ep_command_long_msg_data clVal;
        /* [case()] */ ep_command_file_msg_data cfVal;
        /* [case()] */ ep_command_frame_msg_data cfrVal;
        /* [case()] */ ep_command_frame_long_msg_data cfrlVal;
        /* [case()] */ ep_command_frame_time_msg_data cfrtVal;
        /* [case()] */ ep_command_block_msg_data cblkVal;
        /* [case()] */ ep_command_block_long_msg_data cblklVal;
        /* [case()] */ ep_command_block_time_msg_data cblktVal;
        /* [case()] */ ep_command_int_msg_data ciVal;
        /* [case()] */ ep_command_strval_msg_data cstVal;
        /* [case()] */ ep_command_data_msg_data cdVal;
        /* [case()] */ ep_command_start_data csVal;
        /* [case()] */ ep_tlm_msg_data tVal;
        /* [case()] */ ep_enh_tlm_msg_data etVal;
        /* [case()] */ LONG laVal[ 4 ];
        /* [case()] */ LONG longVal;
        /* [case()] */ DOUBLE dblVal;
        /* [case()] */ ep_strval_msg_data smdVal;
        /* [case()] */ ep_strval_msg_data dmdVal;
        /* [case()] */ ep_strval_msg_data drdVal;
        /* [case()] */ ep_strval_msg_data apdVal;
        /* [case()] */ ep_ss_limits_msg_data ssVal;
        /* [case()] */ INT hw_idVal;
        /* [case()] */ ep_limits_ack_msg_data lackVal;
        /* [case()] */ ep_limits_ack_id_msg lackmVal;
        /* [case()] */ ep_dbmod_value_msg_data dbVal;
        /* [case()] */ ep_dbmod_char_msg_data dbcharVal;
        /* [case()] */ ep_cmd_frame_out_data cfoVal;
        /* [case()] */ ep_usc_ack_data uaVal;
        /* [case()] */ ep_usc_data uVal;
        /* [case()] */  /* Empty union arm */ 
        /* [case()] */  /* Empty union arm */ 
        }	;
    }	EPEVT;

typedef struct  tagEPEVTDATA
    {
    ULONG ulUserData;
    TIMEVAL tvEventTime;
    INT nEventNum;
    EPEVT epevtMessage;
    }	EPEVTDATA;



extern RPC_IF_HANDLE __MIDL_itf_epservertypes_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_epservertypes_0000_v0_0_s_ifspec;

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
