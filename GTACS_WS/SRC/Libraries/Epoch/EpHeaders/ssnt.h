//Copyright 1997-2003 Integral Systems, Inc. All rights reserved. 
/*
@(#) epoch windows_api ssnt/ssnt.h 1.13 08/05/14 %U
*/

/******************************************************************************
 Filename: ssnt.h

 Author:	Jeff Jarriel - Integral Systems, Inc.
 Created:	June 18, 1998
 Modified:	January 14, 1999 JBS
		August 11, 2004 MRH	Added USC support for windows client.
		August 25, 2004 MRH	Added USC support for GOES USC GUI.
		September 2, 2004 MRH	Added raw_value to 
					epoch_usc_notification.
		November 15, 2004 MRH	Added epoch_enh_gv_limits_msg_data.
		December 10, 2004 MRH	Modified epoch_tlm_msg_data, 
					epoch_usc_ack_data, epoch_usc_data.
		June 19, 2007 MRH	Added state_value to epoch_tlm_msg_data.

 This header file contains the function prototypes and data definitions for
 clients of the EPOCH NT stream services dll (strmsrvc.dll).  It would be a
 lot more convenient if clients could use the standard EPOCH stream services
 header files, but this is not possible.  Doing so would require that we
 include a great many NuTCRACKER header files, and would also cause namespace
 collisions with Microsoft.

 The functions prototyped here are actually wrappers for the underlying stream
 services, where each stream services function name is prefixed with "ssnt_".

 This file should be used to build NT programs that link to the dll, as well
 as the dll itself.  By placing __declspec() statements in this header and
 including the header in the source files, the __declspec() statements do not
 need to be added before functions in the c source file(s).

 To build a dll using this header, make sure that symbol BUILD_SSNT_DLL is
 defined.  To build an application using this header, make sure that symbol
 BUILD_SSNT_DLL is not defined.

 Notes:	

 1) __declspec(dllimport) and __declspec(dllexport) are compiler specific. It
    is assumed that Microsoft's VC++ compiler is being used to build the dll
	and application(s) that use it.  If this is not the case, see Advanced
	Windows by Jeffrey Richter (pg. 564) to learn how to handle name decoration
	issues.

 2) The implementations of ssnt_create_node_connection() and
    ssnt_process_node_connections() require some explanation.  The underlying
	stream services functions expect an argument of type fd_set.  Since this
	is quite different from a Windows fd_set and cannot be used by the client,
	the ssnt functions use a private copy.

 3) A structure and methods have been added to access information derived from
    file epoch.cfg via strmsrv.c.  The methods are:

	ssnt_strmsrv_numstreams() - This returns the number of streams defined
		in epoch.cfg.

	ssnt_strmsrv_get_info() - This returns a pointer to a structure containing
		information about a stream.

******************************************************************************/

#ifndef SSNT_H_DEFINED
#define SSNT_H_DEFINED

#include <windows.h>
#include "epserverenums.h"

/******************************************************************************
	common request structures:
******************************************************************************/

typedef struct epoch_client_connection {
	char*				client;
	int					str_id;
	int					destroyed;
	int					need_to_flush;
	void				(*stol_response_cb) ();
	EPSERVERMODE		mode;
	struct epoch_client_connection*	redundant_connection;
	unsigned int		id;
} epoch_client_connection;

/******************************************************************************
	point request structures:
******************************************************************************/

typedef struct epoch_client_point {
	epoch_client_connection*	client_connection;
	void						(*service_cb) ();
	void*						user_data;
	unsigned int				server_point_id;
	int							settable;
	EPPTDATATYPE				data_type;
	int							size;
	void*						value;
	unsigned long				status_bits;
	struct timeval				time;
	struct epoch_client_point*	redundant_point;
	unsigned int				id;
} epoch_client_point;

/******************************************************************************
	event request structures:
******************************************************************************/

#define EPOCH_LONGVAL_ARRAY_SIZE	4

typedef struct epoch_limits_msg_data {
	int					message_id;
	int					tlm_point_index;
	int					limit_type;
	double				raw_value;
	double				limit_trigger;
	unsigned long		status;
} epoch_limits_msg_data;

typedef struct epoch_gv_limits_msg_data {
	int					condition;
	int					gv_index;
	double				gv_value;
	double				limit_trigger;
	unsigned long		status;
} epoch_gv_limits_msg_data;

typedef struct epoch_enh_gv_limits_msg_data {
	int			condition;
	int			gv_index;
	int			new_violation;
	double			gv_value;
	struct timeval		violation_time;
	double			limit_trigger;
	unsigned long		status;
} epoch_enh_gv_limits_msg_data;

typedef struct epoch_cmd_echo_error_msg_data {
	unsigned long		error_code;
} epoch_cmd_echo_error_msg_data;

typedef struct epoch_tlmver_msg_data {
	int					sc_address_index;
	int					tlm_point_index;
	int					cmd_index;
	unsigned int		cmd_reference;
} epoch_tlmver_msg_data;

typedef struct epoch_exec_tone_msg_data {
	long				pulse_width;
	long				period;
	int					num_pulses;
} epoch_exec_tone_msg_data;

typedef struct epoch_command_msg_data {
	int					sc_address_index;
	int					cmd_index;
	unsigned long		cmd_code;
	unsigned int		cmd_reference;
	unsigned long		cmd_status;
} epoch_command_msg_data;

typedef struct epoch_command_time_msg_data {
	int					sc_address_index;
	int					cmd_index;
	unsigned long		cmd_code;
	struct timeval		cmd_time;
	unsigned int		cmd_reference;
	unsigned long		cmd_state;
	unsigned long		cmd_state_mask;
} epoch_command_time_msg_data;

typedef struct epoch_command_long_msg_data {
	int					sc_address_index;
	int					cmd_index;
	unsigned long		cmd_code;
	int					num_longs;
	unsigned long		l[4];
	unsigned int		cmd_reference;
	unsigned long		cmd_status;
} epoch_command_long_msg_data;

typedef struct epoch_command_file_msg_data {
	unsigned int		reference_id;
	unsigned long		cmd_status;
	char*				aptr;
	char				file_name[1];
} epoch_command_file_msg_data;

typedef struct epoch_command_frame_msg_data {
	unsigned int		reference_id;
	unsigned int		record_num;
	char*				aptr;
	char				file_name[1];
} epoch_command_frame_msg_data;

typedef struct epoch_command_frame_long_msg_data {
	unsigned int		reference_id;
	unsigned int		record_num;
	unsigned long		l;
	char*				aptr;
	char				file_name[1];
} epoch_command_frame_long_msg_data;

typedef struct epoch_command_frame_time_msg_data {
	unsigned int		reference_id;
	unsigned int		record_num;
	struct timeval		time;
	char*				aptr;
	char				file_name[1];
} epoch_command_frame_time_msg_data;

typedef struct epoch_command_block_msg_data {
	unsigned int		reference_id;
	unsigned int		sequence_num;
} epoch_command_block_msg_data;

typedef struct epoch_command_block_long_msg_data {
	unsigned int		reference_id;
	unsigned int		sequence_num;
	unsigned long		l;
} epoch_command_block_long_msg_data;

typedef struct epoch_command_block_time_msg_data {
	unsigned int		reference_id;
	unsigned int		sequence_num;
	struct timeval		time;
} epoch_command_block_time_msg_data;

typedef struct epoch_command_int_msg_data {
	int					sc_address_index;
	int					cmd_index;
	int					i;
	unsigned int		cmd_reference;
	unsigned long		cmd_status;
} epoch_command_int_msg_data;

typedef struct epoch_command_strval_msg_data {
	int					sc_address_index;
	int					cmd_index;
	unsigned int		cmd_reference;
	unsigned long		cmd_status;
	char*				aptr;
	char				strval[1];
} epoch_command_strval_msg_data;

#define MAX_CMD_DATA 20

typedef struct epoch_command_data_msg_data {
	int					sc_address_index;
	int					cmd_index;
	unsigned int		cmd_reference;
	unsigned int		msg_num;
	unsigned int		num_msgs;
	unsigned int		num_bytes;
	unsigned char		data[MAX_CMD_DATA];
} epoch_command_data_msg_data;

typedef struct epoch_command_frame_out_data {
	unsigned int		reference_id;
	unsigned int		sequence_num;
	char*				aptr;
	char				strval[1];
} epoch_command_frame_out_data;

typedef struct epoch_command_start_data {
	int					cmd_index;
	unsigned long		cmd_code;
	unsigned int		cmd_reference;
	unsigned long		cmd_status;
	int					cmd_sequence;
	int					n_datawords;
	long				dataword[4];
} epoch_command_start_data;

#define USC_GMT_SIZE 22
#define USC_MNEMONIC_SIZE 36
#define USC_STATE_TEXT_SIZE 20

typedef struct epoch_tlm_msg_data {
	unsigned int	tlm_point_index;
	struct timeval	reference_id;
	double		raw_value;
	double		eu_value;
} epoch_tlm_msg_data;

typedef struct epoch_enh_tlm_msg_data {
	unsigned int	tlm_point_index;
	struct timeval	reference_id;
	double		raw_value;
	double		eu_value;
	char*		aptr;
	char		state_value[1];
} epoch_enh_tlm_msg_data;

typedef struct epoch_strval_msg_data {
	char*		aptr;
	char		strval[1];
} epoch_strval_msg_data;

typedef struct epoch_ss_limits_msg_data {
	int			condition;
	int			ss_index;
} epoch_ss_limits_msg_data;

typedef struct epoch_limits_ack_msg_data {
	int					point_type;
	int					point_index;
	unsigned long		status;
} epoch_limits_ack_msg_data;

typedef struct epoch_limits_ack_id {
	int					point_type;
	int					pid;
} epoch_limits_ack_id;

typedef struct epoch_limits_ack_id_msg {
	int						num;
	struct epoch_limits_ack_id	ids[1];
} epoch_limits_ack_id_msg;

typedef struct epoch_dbmod_value_msg_data {
	int			point_type;
	int			point_index;
	int			context;
	int			field;
	double		value;
} epoch_dbmod_value_msg_data;

typedef struct epoch_dbmod_char_msg_data {
	int			point_type;
	int			point_index;
	int			context;
	int			field;
	char*		aptr;
	char		value[1];
} epoch_dbmod_char_msg_data;

typedef struct epoch_usc_ack_data {
	unsigned int	tlm_point_index;
	struct timeval	reference_id;
} epoch_usc_ack_data;

typedef struct epoch_usc_data {
	unsigned int	tlm_point_index;
} epoch_usc_data;


typedef struct epoch_union_of_msgs {
	EPEVTMSG type;
	union {
		epoch_limits_msg_data				l;
		epoch_gv_limits_msg_data			gl;
		epoch_cmd_echo_error_msg_data		ce;
		epoch_tlmver_msg_data				tv;
		epoch_exec_tone_msg_data			exec_tone;
		epoch_command_msg_data				c;
		epoch_command_time_msg_data			ct;
		epoch_command_long_msg_data			cl;
		epoch_command_file_msg_data			cf;
		epoch_command_frame_msg_data		cfr;
		epoch_command_frame_long_msg_data	cfrl;
		epoch_command_frame_time_msg_data	cfrt;
		epoch_command_block_msg_data		cblk;
		epoch_command_block_long_msg_data	cblkl;
		epoch_command_block_time_msg_data	cblkt;
		epoch_command_int_msg_data			ci;
		epoch_command_strval_msg_data		cst;
		epoch_command_data_msg_data			cd;
		epoch_command_start_data			cs;
		epoch_tlm_msg_data					t;
		long								la[EPOCH_LONGVAL_ARRAY_SIZE];
		long								longval;
		double								dblval;
		epoch_strval_msg_data				smd;
		epoch_strval_msg_data				dmd;
		epoch_strval_msg_data				drd;
		epoch_strval_msg_data				apd;
		epoch_ss_limits_msg_data			ss;
		int									hw_id;
		epoch_limits_ack_msg_data			lack;
		epoch_limits_ack_id_msg				lackm;
		epoch_dbmod_value_msg_data			dbval;
		epoch_dbmod_char_msg_data			dbchar;
		epoch_command_frame_out_data		cfo;
		epoch_usc_ack_data					ua;
		epoch_usc_data						u;
		epoch_enh_gv_limits_msg_data			egl;
		epoch_enh_tlm_msg_data			et;
	} epoch_union_of_msgs_u;
} epoch_union_of_msgs;

typedef struct epoch_evt_msg {
	int						msg_number;
	struct timeval			msg_time;
	epoch_union_of_msgs		msg_data;
} epoch_evt_msg;

typedef struct epoch_update_event_args {
	unsigned int			client_event_id;
	epoch_evt_msg*			evt_msg;
} epoch_update_event_args;

typedef struct epoch_client_event {
	epoch_client_connection*	client_connection;
	void						(*service_cb) ();
	void*						user_data;
	unsigned int				server_event_id;
	epoch_update_event_args*	ueargs;
	struct epoch_client_event*	redundant_event;
	unsigned int				id;
} epoch_client_event;

/******************************************************************************
	frame request structures:
******************************************************************************/

typedef struct epoch_tlm_header {
	struct timeval			receipt_time;
	unsigned int			quality;
	unsigned int			size;
} epoch_tlm_header;

typedef struct epoch_frame_msg {
	unsigned int			frame_msg_len;
	char*					frame_msg_val;
} epoch_frame_msg;

typedef struct epoch_update_frame_args {
	unsigned int			client_frame_id;
	epoch_tlm_header*		header;
	epoch_frame_msg			frame_msg;
} epoch_update_frame_args;

typedef struct epoch_client_frame {
	epoch_client_connection*	client_connection;
	void						(*service_cb) ();
	void*						user_data;
	unsigned int				server_frame_id;
	epoch_update_frame_args*	ufargs;
	int							size;
	struct epoch_client_frame*	redundant_frame;
	unsigned int				id;
} epoch_client_frame;

/******************************************************************************
	limit status request structures:
******************************************************************************/

typedef struct epoch_update_status_new {
	char						*strMnemonic;
	double						dblValue;
	struct timeval				tvFirstTime;
	unsigned long				ulFirstStatus;
	double						dblFirstLimit;
	struct timeval				tvCurrTime;
	unsigned long				ulCurrStatus;
	unsigned long				ulFlags;
} epoch_update_status_new;

typedef struct epoch_update_status_current {
	char						*strMnemonic;
	double						dblValue;
	struct timeval				tvCurrTime;
	unsigned long				ulCurrStatus;
	double						dblCurrLimit;
	unsigned long				ulFlags;
} epoch_update_status_current;

typedef struct epoch_update_status_args {
	EPUPDATESTATUS				usType;
	union {
		epoch_update_status_new		usNew;
		epoch_update_status_current	usCurrent;
	};
} epoch_update_status_args;

typedef struct epoch_client_status {
	epoch_client_connection*	client_connection;
	void						(*service_cb) ();
	void*						user_data;
	unsigned int				server_status_id;
	epoch_update_status_args*	usargs;
	unsigned int				id;
} epoch_client_status;


/******************************************************************************
	usc status request structures:
******************************************************************************/
typedef struct epoch_usc_notification {
        char *                  point_name;
        unsigned int            point_index;
        double                  raw_value;
        char *                  state_text;
        char *                  reference_id;
} epoch_usc_notification;

typedef struct epoch_usc_acknowledgment {
        char *                  point_name;
        unsigned int            point_index;
        char *                  reference_id;
} epoch_usc_acknowledgment;

typedef struct epoch_usc_state {
        char *                  point_name;
        unsigned int            point_index;
} epoch_usc_state;

typedef struct epoch_usc_args {
        EPUSCSTATUS		type;
        union {
                epoch_usc_notification        notification;
                epoch_usc_acknowledgment      acknowledgment;
                epoch_usc_state               state;
        } update;
} epoch_usc_args;

typedef struct {
	epoch_client_connection*	client_connection;
	void					(*service_cb) ();
	void*					user_data;
	unsigned int			server_status_id;
	epoch_usc_args*			uscargs;
	unsigned int			id;
} epoch_client_usc;

/******************************************************************************
	limit off status request structure:
******************************************************************************/
typedef struct epoch_limit_check_status {
	int							numPointsThisUpdate;
	int*						pointList;
	void*						user_data;
	int							lastUpdate;
	int							lastListStillValid;
} epoch_limit_check_status;

/******************************************************************************
	Structure used to get epoch.cfg information.  The members are taken from
	the EPOCH streamsrv struct.
******************************************************************************/

typedef struct epoch_strmsrv_info {
	char*		streamid;
	char*		streamhost;
	char*		streamtype;
	char*		manager;
	char*		priority;
	int			broadcast;
	int			flag;
} epoch_strmsrv_info;


/******************************************************************************
	schedule execution status structures:
******************************************************************************/

typedef struct epoch_update_sch_file {
	long			location;
	char			*file;
	struct epoch_update_sch_file	*next;
} epoch_update_sch_file;

typedef struct epoch_update_sch_args {
	int				pid;
	unsigned int	mask;
	char			*message;
	struct epoch_update_sch_file	*uf;
} epoch_update_sch_args;


/******************************************************************************
	USC structures:
******************************************************************************/
//The following must match enum usc_service_type in ss.h:
typedef enum EPUSCSERVICE {
    EPUSCFULL,
    EPUSCUPDATE
} EPUSCSERVICE;

//The following must match enum usc_type in strmutil.h:
typedef enum EPUSCTYPE {
	EPUSCNOTIFICATION,
	EPUSCACK,
	EPUSCENABLE,
	EPUSCDISABLE
} EPUSCTYPE;

/******************************************************************************
	command verification status structures:
******************************************************************************/

//The following must match enum update_cmd_status_type in strmutil.h:
typedef enum EPUPDATECMDSTATUS {
    EPUPDATECMDSTATUS_INVALIDTYPE,
    EPUPDATECMDSTATUS_CTYPE,
    EPUPDATECMDSTATUS_CFTYPE,
    EPUPDATECMDSTATUS_CFRTYPE,
    EPUPDATECMDSTATUS_CFRLTYPE,
    EPUPDATECMDSTATUS_CLTYPE,
    EPUPDATECMDSTATUS_CSTTYPE,
    EPUPDATECMDSTATUS_CTTYPE,
    EPUPDATECMDSTATUS_TVTYPE
} EPUPDATECMDSTATUS;

//EPOCH command message types.  These must match update_cmd_types in sc.h:
typedef enum EPOCHUCM {
	EPOCHUCM_CCV_PENDING = 0,
	EPOCHUCM_CCV_SUCCESS = 1,
	EPOCHUCM_CCV_TIMEOUT = 2,
	EPOCHUCM_CEV_FAIL = 3,
	EPOCHUCM_CEV_PENDING = 4,
	EPOCHUCM_CEV_SUCCESS = 5,
	EPOCHUCM_CEV_TIMEOUT = 6,
	EPOCHUCM_CEXV_FAIL = 7,
	EPOCHUCM_CEXV_PENDING = 8,
	EPOCHUCM_CEXV_SUCCESS = 9,
	EPOCHUCM_CEXV_TIMEOUT = 10,
	EPOCHUCM_CLEAR_FAIL = 11,
	EPOCHUCM_CMD_CLEAR = 12,
	EPOCHUCM_CMD_COMPLETE = 13,
	EPOCHUCM_CMD_EXECUTED = 14,
	EPOCHUCM_CMD_FAIL = 15,
	EPOCHUCM_CMD_PRIMED = 16,
	EPOCHUCM_CMD_QUEUED = 17,
	EPOCHUCM_CMD_REXMIT = 18,
	EPOCHUCM_CMD_STORED = 19,
	EPOCHUCM_CMD_UPLINK = 20,
	EPOCHUCM_CV_FAIL = 21,
	EPOCHUCM_CV_PENDING = 22,
	EPOCHUCM_CV_SUCCESS = 23,
	EPOCHUCM_FAILED_CMD_EXECUTED = 24,
	EPOCHUCM_FCV_FAIL = 25,
	EPOCHUCM_FCV_PENDING = 26,
	EPOCHUCM_FCV_SUCCESS = 27,
	EPOCHUCM_FCV_TIMEOUT = 28,
	EPOCHUCM_HEX_CMD_COMPLETE = 29,
	EPOCHUCM_HEX_CMD_EXECUTED = 30,
	EPOCHUCM_HEX_CMD_FAIL = 31,
	EPOCHUCM_HEX_CMD_PRIMED = 32,
	EPOCHUCM_HEX_CMD_QUEUED = 33,
	EPOCHUCM_HEX_CMD_REXMIT = 34,
	EPOCHUCM_HEX_CMD_UPLINK = 35,
	EPOCHUCM_HEX_PV_FAIL = 36,
	EPOCHUCM_HEX_PV_PENDING = 37,
	EPOCHUCM_HEX_PV_SUCCESS = 38,
	EPOCHUCM_PV_FAIL = 39,
	EPOCHUCM_PV_PENDING = 40,
	EPOCHUCM_PV_SUCCESS = 41,
	EPOCHUCM_SC_CLEAR = 42,
	EPOCHUCM_TV_FAIL = 43,
	EPOCHUCM_TV_PENDING = 44,
	EPOCHUCM_TV_SUCCESS = 45,
	EPOCHUCM_CMD_FILE_COMPLETE = 46,
	EPOCHUCM_CMD_FILE_ERROR = 47,
	EPOCHUCM_CMD_FILE_INIT = 48,
	EPOCHUCM_CMD_FILE_UPLINK = 49,
	EPOCHUCM_CMD_FRAME_FAIL = 50,
	EPOCHUCM_CMD_FRAME_UPLINK = 51,
	EPOCHUCM_CMD_FRAME_NUM = 52,
	EPOCHUCM_CMD_FRAME_PV_FAIL = 53,
	EPOCHUCM_CMD_FRAME_PV_PENDING = 54,
	EPOCHUCM_CMD_FRAME_PV_SUCCESS = 55,
	EPOCHUCM_CMD_FRAME_QUEUED = 56,
	EPOCHUCM_EXPECTED_CE = 57,
	EPOCHUCM_EXPECTED_CEX = 58,
	EPOCHUCM_INVALID_CE = 59,
	EPOCHUCM_INVALID_CEX = 60,
	EPOCHUCM_CMD_CANCEL = 61,
	EPOCHUCM_CMD_SCHED = 62,
	EPOCHUCM_HEX_CMD_CANCEL = 63,
	EPOCHUCM_HEX_CMD_SCHED = 64,
	EPOCHUCM_TV_EVENT_FAIL = 65,
	EPOCHUCM_TV_EVENT_PENDING = 66,
	EPOCHUCM_TV_EVENT_SUCCESS = 67,
	EPOCHUCM_TV_EVENT_TIMEOUT = 68,
	EPOCHUCM_UCM_PTV_FAIL = 69,
	EPOCHUCM_UCM_CMD_UPLINK_TIME = 70,
	EPOCHUCM_UCM_CMD_LOAD_FILE = 71
} EPOCHUCM;

//Command status bit masks.  These must match cmd_status_mask in cmd_util.h:
#define CMDSTATMASK_PV					0x00000001
#define CMDSTATMASK_PV_WAIT				0x00000002
#define CMDSTATMASK_CV					0x00000004
#define CMDSTATMASK_CV_WAIT				0x00000008
#define CMDSTATMASK_TV					0x00000010
#define CMDSTATMASK_TV_WAIT				0x00000020
#define CMDSTATMASK_REXMIT				0x00000040
#define CMDSTATMASK_TIME_TAGGED			0x00000080
#define CMDSTATMASK_CMD_LOCK			0x00000100
#define CMDSTATMASK_AUTO_EXEC			0x00000200
#define	CMDSTATMASK_PV_FAILURE			0x00000400			/* PV Failure										*/
#define	CMDSTATMASK_CV_FAILURE			0x00000800			/* CV Failure										*/
#define	CMDSTATMASK_TV_FAILURE			0x00001000			/* TV Failure										*/
#define	CMDSTATMASK_SP_CLR_CMD			0x00002000			/* Spacecraft Clear command							*/
#define	CMDSTATMASK_INHIBIT_TIME_TAGGED	0x00004000			/* Inhibit time-tagged command processing			*/
#define	CMDSTATMASK_INHIBIT_PV			0x00008000			/* Inhibit probe verification processing			*/
#define	CMDSTATMASK_INHIBIT_CV			0x00010000			/* Inhibit comand verification processing			*/
#define	CMDSTATMASK_INHIBIT_TV			0x00020000			/* Inhibit telemetry verification processing		*/
#define	CMDSTATMASK_INHIBIT_REXMIT		0x00040000			/* Inhibit retransmits of failed commands			*/
#define	CMDSTATMASK_INHIBIT_AUTO_EXEC   0x00080000			/* Inhibit automatic execution of commands			*/
#define	CMDSTATMASK_OV_CMD_REJECT		0x00100000			/* Override command rejection			   			*/
#define	CMDSTATMASK_EXEC_TONE			0x00200000			/* Execute tone is being processed		   			*/
#define	CMDSTATMASK_ONE_IN_LOAD			0x00400000			/* Command is one in a series of load commands		*/
#define	CMDSTATMASK_EMBED_IN_LOAD		0x00800000			/* Command is embedded in a load command			*/
#define	CMDSTATMASK_STORED_CMD			0x01000000			/* Command is a stored command						*/
#define	CMDSTATMASK_MACRO_CMD			0x02000000			/* Command is macro command							*/
#define	CMDSTATMASK_ENCRYPTED			0x04000000			/* Command is encrypted								*/
#define	CMDSTATMASK_AUTO_DSN			0x08000000			/* Command is automatically sent to DSN				*/
#define	CMDSTATMASK_ORIG_CMD_LOAD_FILE	0x10000000			/* Command originated from a Command Load File	.	*/

typedef struct epoch_update_cmd_status_c { 
	char 			*cmd_mnemonic;	/*command mnemonic */
	char 			*sc_address;	/*spacecraft address */
	struct timeval 	msg_time;		/*time of cmd event message */
	unsigned int	cmd_reference;  /*command id used to link messages */
	unsigned int	cmd_msg_type;  	/*event message type being reported*/
	unsigned long 	cmd_code; 		/*command code */
	unsigned long 	cmd_status; 	/*Status bits */
} epoch_update_cmd_status_c;

typedef struct epoch_update_cmd_status_cf { 
	char 			*cmd_file;		/*command file */
	struct timeval 	msg_time; 		/*time of cmd event message */
	unsigned int	cmd_reference;  /*command id used to link messages */
	unsigned int	cmd_msg_type;  	/*event message type being reported*/
	unsigned long 	cmd_status; 	/*Status bits */
} epoch_update_cmd_status_cf;

typedef struct epoch_update_cmd_status_cfr { 
	char 			*cmd_file;		/*command file */
	struct timeval 	msg_time;		/*time of cmd event message */
	unsigned int	cmd_reference;  /*command id used to link messages */
	unsigned int	cmd_msg_type;  	/*event message type being reported*/
	unsigned int 	cmd_record_num;	/*record number*/
} epoch_update_cmd_status_cfr;

typedef struct epoch_update_cmd_status_cfrl { 
	char 			*cmd_file;		/*command file */
	struct timeval 	msg_time; 		/*time of cmd event message */
	unsigned int	cmd_reference;  /*command id used to link messages */
	unsigned int	cmd_msg_type;  	/*event message type being reported*/
	unsigned int 	cmd_record_num;	/*record number*/
	unsigned long 	cmd_long;		/*long value*/
} epoch_update_cmd_status_cfrl;

typedef struct epoch_update_cmd_status_cl { 
	char 			*cmd_mnemonic;	/*command mnemonic */
	char 			*sc_address;	/*spacecraft address */
	struct timeval 	msg_time;		/*time of cmd event message */
	unsigned int	cmd_reference;  /*command id used to link messages */
	unsigned int	cmd_msg_type;  	/*event message type being reported*/
	unsigned long 	cmd_code;		/*command code */
	unsigned long 	cmd_status; 	/*Status bits */
	unsigned int 	cmd_num_longs; 	/*number of long values */
	unsigned long 	cmd_long[4];	/*long values*/
} epoch_update_cmd_status_cl;

typedef struct epoch_update_cmd_status_cst { 
	char 			*cmd_mnemonic;	/*command mnemonic */
	char 			*sc_address;	/*spacecraft address */
	char 			*cmd_strval;	/*string value */
	struct timeval 	msg_time; 		/*time of cmd event message */
	unsigned int	cmd_reference;  /*command id used to link messages */
	unsigned int	cmd_msg_type;  	/*event message type being reported*/
	unsigned long 	cmd_status; 	/*Status bits */
} epoch_update_cmd_status_cst;

typedef struct epoch_update_cmd_status_ct { 
	char 			*cmd_mnemonic;	/*command mnemonic */
	char 			*sc_address;	/*spacecraft address */
	struct timeval 	msg_time; 		/*time of cmd event message */
	struct timeval 	cmd_time;		/*time of cmd */
	unsigned int	cmd_reference;  /*command id used to link messages */
	unsigned int	cmd_msg_type;  	/*event message type being reported*/
	unsigned long 	cmd_code; 		/*command code */
	unsigned long 	cmd_state;		/*command state */
	unsigned long 	cmd_state_mask;	/*command state mask*/
} epoch_update_cmd_status_ct;

typedef struct epoch_update_cmd_status_tv { 
	char 			*cmd_mnemonic;	/*command mnemonic */
	char 			*sc_address;	/*spacecraft address */
	char 			*point_name;	/*tv telem point name */
	double			tv_value;		/*tv point value */
	struct timeval 	msg_time; 		/*time of cmd event message */
	unsigned int	cmd_reference;  /*command id used to link messages */
	unsigned int	cmd_msg_type;  	/*event message type being reported*/
} epoch_update_cmd_status_tv;

typedef struct epoch_update_cmd_status_args { 
	enum EPUPDATECMDSTATUS type; 
	union{ 
		epoch_update_cmd_status_c 		ucs_c; 
		epoch_update_cmd_status_cf 		ucs_cf; 
		epoch_update_cmd_status_cfr 	ucs_cfr; 
		epoch_update_cmd_status_cfrl 	ucs_cfrl; 
		epoch_update_cmd_status_cl 		ucs_cl; 
		epoch_update_cmd_status_cst 	ucs_cst; 
		epoch_update_cmd_status_ct 		ucs_ct; 
		epoch_update_cmd_status_tv 		ucs_tv; 
	} update;
} epoch_update_cmd_status_args;

typedef struct epoch_client_cmd_status {
	epoch_client_connection*	client_connection;
	void						(*service_cb) ();
	void*						user_data;
	unsigned int				server_cmd_status_id;
	epoch_update_cmd_status_args*	ucsargs;
	unsigned int				id;
} epoch_client_cmd_status;


/* typedefs for callback function pointers: */

typedef void (*EPOCH_EVENT_SERVICE_CB_FUNC)(epoch_client_event *ceid);
typedef void (*EPOCH_FRAME_SERVICE_CB_FUNC)(epoch_client_frame *cfid);
typedef void (*EPOCH_MANAGER_CB_FUNC)(void *user_data, char *message);
typedef void (*EPOCH_NODE_CB_FUNC)(char *message);
typedef void (*EPOCH_OOL_SERVICE_CB_FUNC)(epoch_client_status *csid);
typedef void (*EPOCH_POINT_SERVICE_CB_FUNC)(epoch_client_point *cpid);
typedef void (*EPOCH_RESPONSE_CB_FUNC)(EPSTOLCOMPSTAT status, char *message);
typedef void (*EPOCH_RESPONSE_CB_FUNC2)(EPSTOLCOMPSTAT status, char *message,
										void *user_data);
typedef void (*EPOCH_SCHED_STATUS_CB_FUNC)(epoch_update_sch_args *usa);
typedef void (*EPOCH_CMD_STATUS_CB_FUNC)(epoch_client_cmd_status *ccs);
typedef void (*EPOCH_LO_SERVICE_CB_FUNC)(epoch_limit_check_status lcs);
typedef void (*EPOCH_USC_SERVICE_CB_FUNC)(epoch_client_usc* cusc);

/******************************************************************************
	Manager message request structure
******************************************************************************/
typedef struct epoch_manager_connection {
	void				*user_data;
	EPOCH_MANAGER_CB_FUNC	service_cb;
	void				*server_handle;
} epoch_manager_connection;


/******************************************************************************
	DLL entry point prototypes:
******************************************************************************/

#ifdef BUILD_SSNT_DLL
#    define EPOCH_NT_API __declspec(dllexport)
#else
#    define EPOCH_NT_API __declspec(dllimport)
#endif


EPOCH_NT_API int ssnt_cancel_cmd_status (
				epoch_client_cmd_status *status_id);

EPOCH_NT_API int ssnt_cancel_event (
				epoch_client_event *event_id);

EPOCH_NT_API int ssnt_cancel_frame (
				epoch_client_frame *frame_id);

EPOCH_NT_API int ssnt_cancel_point (
				epoch_client_point *point_id);

EPOCH_NT_API int ssnt_cancel_status (
				epoch_client_status *ool_status);

EPOCH_NT_API int ssnt_check_node_connection (
				char *streamid);

EPOCH_NT_API int ssnt_check_streamid (
				char *streamid);

EPOCH_NT_API int ssnt_connect_manager (
				char *client_name,
				char *streamid,
				EPOCH_MANAGER_CB_FUNC service_cb,
				void *user_data,
				char *host_name,
				epoch_manager_connection **connection_id);

EPOCH_NT_API int ssnt_connect_stream (
				char *streamid,
				struct timeval client_timeout, 
				epoch_client_connection **connection_id);

EPOCH_NT_API int ssnt_create_node_connection(
				char *streamid, 
				EPOCH_NODE_CB_FUNC node_cb);

EPOCH_NT_API int ssnt_destroy_node_connection (
				char *streamid);

EPOCH_NT_API void ssnt_destroy_node_connections ();

EPOCH_NT_API int ssnt_dir_init_stream (
				char *streamid,
				char *database);

EPOCH_NT_API int ssnt_dir_init_playback (
				char *streamid,
				char *archive);

EPOCH_NT_API int ssnt_dir_term_stream (
				char *streamid);

EPOCH_NT_API void ssnt_disconnect_manager (
				epoch_manager_connection **connection_id);

EPOCH_NT_API int ssnt_disconnect_stream (
				epoch_client_connection *connection_id);

EPOCH_NT_API void ssnt_disconnect_streams ();

EPOCH_NT_API int ssnt_execute_stol (
				char *directive,
			    struct timeval client_timeout,
			    EPOCH_RESPONSE_CB_FUNC response_cb);

EPOCH_NT_API int ssnt_execute_stol2 (
				char *directive,
			    struct timeval client_timeout,
			    EPOCH_RESPONSE_CB_FUNC2 response_cb,
				void *user_data);

EPOCH_NT_API int ssnt_flush_sets ();

EPOCH_NT_API int ssnt_get_limit_check_status (
				char *streamid,
				EPOCH_LO_SERVICE_CB_FUNC service_cb,
				void *user_data);

EPOCH_NT_API int ssnt_look_up_point (
				char *name,
				struct timeval client_timeout, 
				epoch_client_point **point_id);

EPOCH_NT_API int ssnt_poll_manager (
				epoch_manager_connection **connection_id);

EPOCH_NT_API int ssnt_poll_point (
				epoch_client_point *point_id);

EPOCH_NT_API void ssnt_process_node_connections ();

EPOCH_NT_API int ssnt_read_stream (
				epoch_client_connection *connection_id,
				struct timeval max_interval);

EPOCH_NT_API int ssnt_read_streams (
				struct timeval max_interval);

EPOCH_NT_API int ssnt_request_cmd_status (
				char *streamid,
				struct timeval client_timeout,
				EPOCH_CMD_STATUS_CB_FUNC service_cb,
				void *user_data,
				epoch_client_cmd_status **cmd_status);

EPOCH_NT_API int ssnt_request_event (
				char *streamid,
				enum EPEVTSERVICE service_type,
				struct timeval client_timeout,
				EPOCH_EVENT_SERVICE_CB_FUNC service_cb,
				void *user_data,
				epoch_client_event **event_id);

EPOCH_NT_API int ssnt_request_usc (
				char *streamid,
				EPUSCSERVICE service_type,
				struct timeval client_timeout,
				EPOCH_USC_SERVICE_CB_FUNC service_cb,
				void *user_data,
				epoch_client_usc **usc_data);

EPOCH_NT_API int ssnt_request_frame (
				char *streamid,
				struct timeval client_timeout, 
				EPOCH_FRAME_SERVICE_CB_FUNC service_cb,
				void *user_data,
				epoch_client_frame **frame_id);

EPOCH_NT_API int ssnt_request_ool_status (
				char *streamid,
				struct timeval client_timeout,
				EPOCH_OOL_SERVICE_CB_FUNC service_cb,
				void *user_data,
				epoch_client_status **ool_status);


EPOCH_NT_API int ssnt_request_point (
				char *name,
				enum EPCONVTYPE conv,
				enum EPCBCOND cond,
				enum EPSERVICEMODE mode,
				int period,
				struct timeval client_timeout, 
				EPOCH_POINT_SERVICE_CB_FUNC service_cb,
				void *user_data, 
				epoch_client_point **point_id);

EPOCH_NT_API int ssnt_sample_point (
				epoch_client_point *point_id);

EPOCH_NT_API int ssnt_send_event (
				int number,
			    char *event,
			    struct timeval client_timeout);

EPOCH_NT_API int ssnt_set_point (
				epoch_client_point *point_id);

EPOCH_NT_API int ssnt_set_sch_cb (
				EPOCH_SCHED_STATUS_CB_FUNC sch_cb);

EPOCH_NT_API int ssnt_write_manager (
				epoch_manager_connection **connection_id,
				char *directive);


// epoch.cfg information access functions:

EPOCH_NT_API int ssnt_strmsrv_get_info(int index,
									   epoch_strmsrv_info **pesiData);
EPOCH_NT_API int ssnt_strmsrv_numstreams(int *nStreams);

#endif
