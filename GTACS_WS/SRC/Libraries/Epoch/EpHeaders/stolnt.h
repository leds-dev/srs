/*
@(#) epoch windows_api stolnt/stolnt.h 1.2 04/08/24 17:19:43
*/
/*
@(#) epochnt source stolnt/stolnt.h 1.1 00/02/11 16:33:33
*/

/******************************************************************************
 Filename: stolnt.h

 Author:		Jeff Shaffer - Integral Systems, Inc.
 Created:		January 25, 2000

 This header file contains the function prototypes and data definitions for
 clients of the EPOCH NT STOL execution dll (stolnt.dll).

 This file should be used to build NT programs that link to the dll, as well
 as the dll itself.  By placing __declspec() statements in this header and
 including the header in the source files, the __declspec() statements do not
 need to be added before functions in the c source file(s).

 To build a dll using this header, make sure that symbol BUILD_STOLNT_DLL is
 defined.  To build an application using this header, make sure that symbol
 BUILD_STOLNT_DLL is not defined.

 Notes:	

 1) __declspec(dllimport) and __declspec(dllexport) are compiler specific. It
    is assumed that Microsoft's VC++ compiler is being used to build the dll
	and application(s) that use it.  If this is not the case, see Advanced
	Windows by Jeffrey Richter (pg. 564) to learn how to handle name decoration
	issues.

******************************************************************************/

#ifndef STOLNT_H_DEFINED
#define STOLNT_H_DEFINED

#include <windows.h>

/******************************************************************************
	The following is a data structure with the same shape as proc_shmem.  We
	can't actually include proc_shmem.h here, or it will collide with windows
	header files used by clients.
******************************************************************************/

//Maximum call stack depth
#define STOLNT_PROCSTACK			32

//Procedure execution states
#define STOLNT_STATE_ERROR			6
#define STOLNT_STATE_WAITTIME		5
#define STOLNT_STATE_WAITINPUT		4
#define STOLNT_STATE_WAITING		3
#define STOLNT_STATE_STEPPING		2
#define STOLNT_STATE_PAUSING		1
#define STOLNT_STATE_PROCESSING		0

//Bit masks and shift counts for location information
#define STOLNT_LOC_POS_MASK			0x03FFFFC0
#define STOLNT_LOC_POS_SHIFT		6
#define STOLNT_LOC_LEN_MASK			0x0000003F
#define STOLNT_LOC_LEN_SHIFT		0

//The following value MUST match the value of NuTCRACKER's MAXPATHLEN, since
//users of this library will be using struct stolnt_proc_shmem without benefit
//of the NuTCRACKER include files.  This is a configuration headache, but I
//don't know how to avoid it.
#define STOLNT_MAXPATHLEN			256

typedef	struct	stolnt_proc_shmem {
	int		pid;					//process ID
	int		depth;					//call stack depth
	int		state;					//current execution mode: STOLNT_STATE_XXX
	int		count;					//current wait time count
	char	message[STOLNT_MAXPATHLEN];	//most recent message to client
	long	location[STOLNT_PROCSTACK];	//current proc execution location:	
									//sign bit reserved,
									//next 5 bits for level,
									//next 20 bits for highlight position,
									//low 6 bits for highlight length.
	char	callstack[STOLNT_PROCSTACK][STOLNT_MAXPATHLEN]; //filename for each level
} stolnt_proc_shmem;

//Constants used by master schedule service:
//  STOLNT_MSID_BASE is lowest interpreter ID to use for schedules
//  STOLNT_MAXSCHEDS is the maximum number of schedules per stream, and should
//					match NSCH_SHMEM in proc_shmem.h
#define STOLNT_MSID_BASE			100
#define STOLNT_MAXSCHEDS			1


//Executable procedure types
typedef enum STOLNTPROCTYPE {
	STOLNTPROCTYPE_STOL,
	STOLNTPROCTYPE_CECIL,
	STOLNTPROCTYPE_TCL
} STOLNTPROCTYPE;

/******************************************************************************
	DLL entry point prototypes:
******************************************************************************/

#ifdef BUILD_STOLNT_DLL
#    define EPOCH_NT_API __declspec(dllexport)
#else
#    define EPOCH_NT_API __declspec(dllimport)
#endif

/* typedef for callback function pointer: */

typedef void (*STOLNT_MSG_CB_FUNC)(char *szMessage, int nIndex, ULONG ulUserData);

EPOCH_NT_API int stolnt_create_shmem(char *szPath);
EPOCH_NT_API int stolnt_destroy_shmem();
EPOCH_NT_API int stolnt_get_max_procs();
EPOCH_NT_API int stolnt_get_proc_shmem(int nIndex, stolnt_proc_shmem **ppShmem);
EPOCH_NT_API int stolnt_get_proc_status(int nIndex);
EPOCH_NT_API int stolnt_poll_proc(int nIndex, STOLNT_MSG_CB_FUNC msg_cb, ULONG ulUserData);
EPOCH_NT_API int stolnt_poll_procs(STOLNT_MSG_CB_FUNC msg_cb, ULONG ulUserData);
EPOCH_NT_API int stolnt_send_directive(char *szDirective, int nIndex);
EPOCH_NT_API int stolnt_start_proc(STOLNTPROCTYPE eProcType, char *szDirective,
								   char *szStream, char *szDatabase, int *nIndex,
								   char **szResponse);
EPOCH_NT_API int stolnt_stop_proc(int nIndex);

#endif
