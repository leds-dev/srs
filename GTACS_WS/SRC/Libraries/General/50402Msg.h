//
// %Z% goes_n-q sched Software/UtilityLib/%M% %I% %E% %U%
//
//////////////////////////////////////////////////////////////////////
//
// Module name:			50402Msg
//
// Module type:			Include File
//
// Module scope:
//
// Module description:
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_50402MSG_H__9B8C63A0_CCE4_11D2_BB6E_00104B79D0AC__INCLUDED_)
#define AFX_50402MSG_H__9B8C63A0_CCE4_11D2_BB6E_00104B79D0AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif
//*********************************************************
// 50402 Message header
//*********************************************************
typedef struct tagMsgHdr
{
	unsigned	__int16	m_nSequence;
	unsigned	__int8	m_nSatelliteID;
	unsigned	__int8	m_nSource;
				__int32	m_nDestination;
				__int32	m_nNumHalfWords;
	unsigned	__int8	m_nMsgType;
	unsigned	__int8	m_nMsgSubType;
				char	m_cTracer[10];
	unsigned	__int8	m_nMsgCnt;
	unsigned	__int8	m_nMsgEndFlag;
				__int16	m_nSpare;
				__int32	m_nRoutingID;
} MSG_HEADER;
const int MSG_HEADER_HW_SIZE = sizeof (MSG_HEADER)/2;
//*********************************************************
// Internal Msg 51 and 151 structures for Star requests
//*********************************************************
typedef struct tagMsg51Header
{
				char	m_cIMCSetID[4];
				__int8	m_nSpare[4];
	unsigned	__int32	m_nNumWindows;
	unsigned	__int32 m_nNumBadWindows;
} MSG_51_HEADER;
const int MSG_51_HEADER_HW_SIZE = sizeof (MSG_51_HEADER)/2;

typedef struct tagMsg51WindowData
{
	unsigned	__int16	m_nSequenceNum;
	unsigned	__int16 m_nBadFlag;
	unsigned	__int32	m_nNumStarsInWindow;
} MSG_51_WINDOW_DATA;
const int MSG_51_WINDOW_DATA_HW_SIZE = sizeof (MSG_51_WINDOW_DATA)/2;

typedef struct tagMsg51StarData
{
	unsigned	__int32	m_nStarID;
	unsigned	__int32	m_nNumLooksForStar;
} MSG_51_STAR_DATA;
const int MSG_51_STAR_DATA_HW_SIZE = sizeof (MSG_51_STAR_DATA)/2;

typedef struct tagMsg51LookData
{
	unsigned	__int32	m_nLookNum;
	unsigned	__int32	m_nDwellRepeats;
	unsigned	__int32	m_nBCDTime[2];
				__int32	m_nEWCyc;
				__int32	m_nEWInc;
				__int32	m_nNSCyc;
				__int32	m_nNSInc;
} MSG_51_LOOK_DATA;
const int MSG_51_LOOK_DATA_HW_SIZE = sizeof (MSG_51_LOOK_DATA)/2;

typedef struct tagMsg151Hdr
{
	char	m_cIMCSetID[4];
	__int16	m_nNumWindows;
	__int8	m_nSpare[6];
} MSG_151_HEADER;
const int MSG_151_HEADER_HW_SIZE = sizeof (MSG_151_HEADER)/2;

typedef struct tagMsg151Data
{
				__int32	m_nSequenceNum;
	unsigned	__int32	m_nBCDTime[2];
				float	m_fDuration;

} MSG_151_DATA;
const int MSG_151_DATA_HW_SIZE = sizeof (MSG_151_DATA)/2;
//**************************************************************************************
// Internal Msg 53 and 153 structures for IMC Coefficient Command Parameter requests
//**************************************************************************************
typedef struct tagMsg53Header
{
	unsigned	__int32	m_nBCDTime[2];
} MSG_53_HEADER;
const int MSG_53_HEADER_HW_SIZE = sizeof (MSG_53_HEADER)/2;

typedef struct tagMsg53Data
{
	__int16	m_nDynamicRangeFlag;
	__int16	m_nIMCType;
	char	m_cIMCSet[4];
	__int32	m_nYawFlipFlag;
	__int32	m_nEpochDay;
	float	m_fTimeOfDay;
	float	m_fFundFourierFreq;
	float	m_fNominalEarthRate;
	float	m_fOrbitalCoefficients[24];
	float	m_fLongitudeReference;
	float	m_fGreenwichHourAngle;
	float	m_fNominalGeosynchRadius;
	float	m_fPresessionNutation[4];
	float	m_fExpStartTime;
} MSG_53_DATA;
const int MSG_53_DATA_HW_SIZE = sizeof (MSG_53_DATA)/2;

typedef struct tagMsgIMC53Data
{
	float	m_fExpMag;
	float	m_fExpTimeConstant;
	float	m_fMagZeroOrder;
	float	m_fMagSine[15];
	float	m_fMagCosine[15];
	__int32	m_nNumMonomialSinusoids;
	__int32	m_nOrderSinusoid[4];
	__int32	m_nOrderMonomial[4];
	float	m_fMagMonomialSinusoid[4];
	float	m_fPhaseAngleSinusoid[4];
	float	m_fAngleFromZero[4];
} MSG_53_IMC_DATA;
const int MSG_53_IMC_DATA_HW_SIZE = sizeof (MSG_53_IMC_DATA)/2;
/*
typedef struct tagMsgISAO53Data
{
	float	m_fImgEWOrig;
	float	m_fImgNSOrig;
	float	m_fSndEWOrig;
	float	m_fSndNSOrig;
} MSG_53_ISAO_DATA;
const int MSG_53_ISAO_DATA_HW_SIZE = sizeof (MSG_53_ISAO_DATA)/2;
*/
typedef struct tagMsg153Data
{
	char	m_cIMCSet[4];
	__int16	m_nSpare[6];
} MSG_153_DATA;
const int MSG_153_DATA_HW_SIZE = sizeof (MSG_153_DATA)/2;
//**********************************************************
// Internal Msg 55 and 155 structures for Frame requests
//**********************************************************
typedef struct tagMsg55Hdr
{
	char	m_cIMCSetID[4];
	__int32	m_nNumFrames;
	__int32	m_nSpare;
	__int32	m_nIMCStatus;
} MSG_55_HEADER;
const int MSG_55_HEADER_HW_SIZE = sizeof (MSG_55_HEADER)/2;

typedef struct tagMsg55Data
{
	unsigned	__int32	m_nBCDTime[2];
				float	m_fDuration;
				float	m_fFrequency;
				__int32	m_nCoordinateType;
				__int32	m_nSpacelookSide;
				__int32	m_nSpacelookMode;
				__int32	m_nDefaultFrameFlag;
				__int32	m_nSounderStepMode;
				__int32	m_nOutputStatus;
				__int32	m_nCriticalLimit;
				__int32	m_nStartEWCyc;
				__int32	m_nStartEWInc;
				__int32	m_nStartNSCyc;
				__int32	m_nStartNSInc;
				__int32	m_nStopEWCyc;
				__int32	m_nStopEWInc;
				__int32	m_nStopNSCyc;
				__int32	m_nStopNSInc;
				__int32	m_nSpare;
} MSG_55_DATA;
const int MSG_55_DATA_HW_SIZE = sizeof (MSG_55_DATA)/2;

typedef struct tagMsg155Hdr
{
	char	m_cIMCSetID[4];
	__int32	m_nNumFrames;
	__int32	m_nSpare;
	__int32	m_nIMCStatus;
} MSG_155_HEADER;
const int MSG_155_HEADER_HW_SIZE = sizeof (MSG_155_HEADER)/2;

typedef struct tagMsg155Data
{
	unsigned	__int32	m_nBCDTime[2];
				__int32	m_nCoordinateType;
				__int32	m_nSpacelookSide;
				__int32	m_nSpacelookMode;
				__int32	m_nDefaultFrameFlag;
				__int32	m_nSounderStepMode;
				float	m_fStartNS;
				float	m_fStartEW;
				float	m_fStopNS;
				float	m_fStopEW;
				__int32	m_nRepeatCount;
} MSG_155_DATA;
const int MSG_155_DATA_HW_SIZE = sizeof (MSG_155_DATA)/2;

//******************************************************************
// Internal Msg 59 and 159 structures Eclipse Prediction requests
//******************************************************************
typedef struct tagMsg59Hdr
{
	__int32	m_nNumEvents;
	__int32	m_nStatus;
} MSG_59_HEADER;
const int MSG_59_HEADER_HW_SIZE = sizeof (MSG_59_HEADER)/2;

typedef struct tagMsg59Data
{
				__int32	m_nIntrudingBody;
				__int32	m_nSpare;
	unsigned	__int32	m_nPenumbralEntranceTime[2];
	unsigned	__int32	m_nUmbralEntranceTime[2];
	unsigned	__int32	m_nUmbralExitTime[2];
	unsigned	__int32	m_nPenumbralExitTime[2];
} MSG_59_DATA;
const int MSG_59_DATA_HW_SIZE = sizeof (MSG_59_DATA)/2;

typedef struct tagMsg159Data
{
	unsigned	__int32	m_nStartTime[2];
				float	m_fDuration;
} MSG_159_DATA;
const int MSG_159_DATA_HW_SIZE = sizeof (MSG_159_DATA)/2;

//***************************************************************************
// Internal Msg 60, 61 and 160 structures Sensor/Frame Intrusion requests
//***************************************************************************
typedef struct tagMsg60Hdr
{
	__int32	m_nNumIntrusions;
	__int32	m_nStatus;
} MSG_60_HEADER;
const int MSG_60_HEADER_HW_SIZE = sizeof (MSG_60_HEADER)/2;

typedef struct tagMsg60Data
{
				__int32	m_nIntrudingBody;
				__int32	m_nIntrudingID;
	unsigned	__int32	m_nStartTime[2];
	unsigned	__int32	m_nStopTime[2];
} MSG_60_DATA;
const int MSG_60_DATA_HW_SIZE = sizeof (MSG_60_DATA)/2;

typedef struct tagMsg61Hdr
{
	__int32	m_nNumRemainingFrames;
	__int32	m_nNumFrames;
} MSG_61_HEADER;
const int MSG_61_HEADER_HW_SIZE = sizeof (MSG_61_HEADER)/2;

typedef struct tagMsg61FrameData
{
	char	m_cFrameName[32];
	__int32	m_nStatus;
	__int32	m_nNumIntrusions;
} MSG_61_FRAME_DATA;
const int MSG_61_FRAME_DATA_HW_SIZE = sizeof (MSG_61_FRAME_DATA)/2;

typedef struct tagMsg61IntrusionData
{
	unsigned	__int32	m_nStartTime[2];
	unsigned	__int32	m_nStopTime[2];
} MSG_61_INTRUSION_DATA;
const int MSG_61_INTRUSION_DATA_HW_SIZE = sizeof (MSG_61_INTRUSION_DATA)/2;

typedef struct tagMsg160Data
{
	unsigned	__int32	m_nStartTime[2];
				float	m_fDuration;
} MSG_160_DATA;
const int MSG_160_DATA_HW_SIZE = sizeof (MSG_160_DATA)/2;


//**************************************************************************************
// Internal Msg 75 and 175 structures for Scale Factor Calibration Schedule requests
//**************************************************************************************
typedef struct tagMsg75Header
{
	unsigned	__int32	m_nBCDTime[2];
	unsigned	__int32	m_nNumOffsets;
				__int8	m_nSpare[4];
} MSG_75_HEADER;
const int MSG_75_HEADER_HW_SIZE = sizeof (MSG_75_HEADER)/2;

typedef struct tagMsg75OffsetData
{
				float	m_fImcCalOffset;
	unsigned	__int32	m_nNumStarsInOffset;
} MSG_75_OFFSET_DATA;
const int MSG_75_OFFSET_DATA_HW_SIZE = sizeof (MSG_75_OFFSET_DATA)/2;

typedef struct tagMsg75StarData
{
	unsigned	__int32	m_nStarID;
	unsigned	__int32	m_nNumLooksForStar;
} MSG_75_STAR_DATA;
const int MSG_75_STAR_DATA_HW_SIZE = sizeof (MSG_75_STAR_DATA)/2;

typedef struct tagMsg75LookData
{
	unsigned	__int32	m_nLookNum;
	unsigned	__int32	m_nDwellRepeats;
	unsigned	__int32	m_nBCDTime[2];
				__int32	m_nEWCyc;
				__int32	m_nEWInc;
				__int32	m_nNSCyc;
				__int32	m_nNSInc;
} MSG_75_LOOK_DATA;
const int MSG_75_LOOK_DATA_HW_SIZE = sizeof (MSG_75_LOOK_DATA)/2;

typedef struct tagMsg175Data
{
	unsigned	__int32	m_nBCDTime[2];
				float	m_fDuration;

} MSG_175_DATA;
const int MSG_175_DATA_HW_SIZE = sizeof (MSG_175_DATA)/2;


//*******************************************************************
// Public Msg 51, 151, 55, 155, 59, 159, 60, 160, 75, 175 structures
//*******************************************************************
const int MAX_51_WINDOW_DATA	= 72;
const int MAX_51_STAR_DATA		= 100;
const int MAX_51_LOOK_DATA		= 16;
const int MAX_151_WINDOWS		= 200;
const int MAX_75_OFFSET_DATA	= 200;
const int MAX_75_STAR_DATA		= 100;
const int MAX_75_LOOK_DATA		= 16;
const int MAX_61_FRAME_DATA		= 20;
const int MAX_61_INTRUSION_DATA = 90;

typedef struct tagMsg51
{
	MSG_HEADER			m_hdr;
	MSG_51_HEADER		m_51Hdr;
	struct
	{
		MSG_51_WINDOW_DATA	m_data;
		struct
		{
			MSG_51_STAR_DATA	m_data;
			struct
			{
				MSG_51_LOOK_DATA	m_data;
			} m_51LookData[MAX_51_LOOK_DATA];
		} m_51StarData[MAX_51_STAR_DATA];
	} m_51WindowData[MAX_51_WINDOW_DATA];
} MSG_51;
const int MSG_51_HW_SIZE = sizeof (MSG_51)/2;

typedef struct tagMsg151
{
	MSG_HEADER		m_hdr;
	MSG_151_HEADER	m_151Hdr;
	MSG_151_DATA	m_151Data[200];
} MSG_151;
const int MSG_151_HW_SIZE = sizeof (MSG_151)/2;

typedef struct tagMsg53
{
	MSG_HEADER			m_hdr;
	MSG_53_HEADER		m_53Hdr;
	MSG_53_DATA			m_53Data;
	MSG_53_IMC_DATA		m_53IMCData[10];
//	MSG_53_ISAO_DATA	m_53ISAOData;
} MSG_53;
const int MSG_53_HW_SIZE = sizeof (MSG_53)/2;

typedef struct tagMsg153
{
	MSG_HEADER		m_hdr;
	MSG_153_DATA	m_153Data; //Max number limited by msg55 data size
} MSG_153;
const int MSG_153_HW_SIZE = sizeof (MSG_153)/2;

const int MAX_55_DATA = 40;
typedef struct tagMsg55
{
	MSG_HEADER		m_hdr;
	MSG_55_HEADER	m_55Hdr;
	MSG_55_DATA		m_55Data[MAX_55_DATA];
} MSG_55;
const int MSG_55_HW_SIZE = sizeof (MSG_55)/2;

const int MAX_155_DATA = MAX_55_DATA;
typedef struct tagMsg155
{
	MSG_HEADER		m_hdr;
	MSG_155_HEADER	m_155Hdr;
	MSG_155_DATA	m_155Data[MAX_155_DATA]; //Max number limited by msg55 data size
} MSG_155;
const int MSG_155_HW_SIZE = sizeof (MSG_155)/2;

const int MAX_59_DATA = 10;
typedef struct tagMsg59
{
	MSG_HEADER		m_hdr;
	MSG_59_HEADER	m_59Hdr;
	MSG_59_DATA		m_59Data[MAX_59_DATA];
} MSG_59;
const int MSG_59_HW_SIZE = sizeof (MSG_59)/2;

typedef struct tagMsg159
{
	MSG_HEADER		m_hdr;
	MSG_159_DATA	m_159Data;
} MSG_159;
const int MSG_159_HW_SIZE = sizeof (MSG_159)/2;

const int MAX_60_DATA = 10;
typedef struct tagMsg60
{
	MSG_HEADER		m_hdr;
	MSG_60_HEADER	m_60ImagerHdr;
	MSG_60_DATA		m_60ImagerData[MAX_60_DATA];
	MSG_60_HEADER	m_60SounderHdr;
	MSG_60_DATA		m_60SounderData[MAX_60_DATA];
	MSG_60_HEADER	m_60AntennaHdr;
	MSG_60_DATA		m_60AntennaData[MAX_60_DATA];
} MSG_60;
const int MSG_60_HW_SIZE = sizeof (MSG_60)/2;

typedef struct tagMsg61
{
	MSG_HEADER			m_hdr;
	MSG_61_HEADER		m_61Hdr;
	struct
	{
		MSG_61_FRAME_DATA	m_data;
		struct
		{
			MSG_61_INTRUSION_DATA	m_data;
		} m_61IntrusionData[MAX_61_INTRUSION_DATA];
	} m_61FrameData[MAX_61_FRAME_DATA];
} MSG_61;
const int MSG_61_HW_SIZE = sizeof (MSG_61)/2;

typedef struct tagMsg160
{
	MSG_HEADER		m_hdr;
	MSG_160_DATA	m_160Data;
} MSG_160;
const int MSG_160_HW_SIZE = sizeof (MSG_160)/2;

typedef struct tagMsg75
{
	MSG_HEADER			m_hdr;
	MSG_75_HEADER		m_75Hdr;
	struct
	{
		MSG_75_OFFSET_DATA	m_data;
		struct
		{
			MSG_75_STAR_DATA	m_data;
			struct
			{
				MSG_75_LOOK_DATA	m_data;
			} m_75LookData[MAX_75_LOOK_DATA];
		} m_75StarData[MAX_75_STAR_DATA];
	} m_75OffsetData[MAX_75_OFFSET_DATA];
} MSG_75;
const int MSG_75_HW_SIZE = sizeof (MSG_75)/2;

typedef struct tagMsg175
{
	MSG_HEADER		m_hdr;
	MSG_175_DATA	m_175Data;
} MSG_175;
const int MSG_175_HW_SIZE = sizeof (MSG_175)/2;

const int MAX_82_DATA_SIZE = 80;
typedef struct tagMsg82
{
	MSG_HEADER		m_hdr;
	char			m_82Data[MAX_82_DATA_SIZE];
} MSG_82;
const int MSG_82_HW_SIZE = sizeof (MSG_82)/2;

#endif // !defined(AFX_50402MSG_H__9B8C63A0_CCE4_11D2_BB6E_00104B79D0AC__INCLUDED_)