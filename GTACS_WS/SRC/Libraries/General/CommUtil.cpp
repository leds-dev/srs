// CommUtil.cpp : implementation file
//

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "CommUtil.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCommUtil

IMPLEMENT_DYNCREATE(CCommUtil, CCmdTarget)

/**
////////////////////////////////////////////////////////////////////////////
// The CCommUtil class

////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::CCommUtil()
//
//	Description :	Constructor
//
//	Return :
//
//	Parameters :
//
//	Note :
////////////////////////////////////////////////////////////////////////////
**/
CCommUtil::CCommUtil()
{
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::~CCommUtil()
//
//	Description :	Destructor
//
//	Return :
//
//	Parameters :
//
//	Note :
////////////////////////////////////////////////////////////////////////////
**/
CCommUtil::~CCommUtil()
{
}

BEGIN_MESSAGE_MAP(CCommUtil, CCmdTarget)
	//{{AFX_MSG_MAP(CCommUtil)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCommUtil member functions


/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::SetTimeOutVal
//	Description :	This routine is called to create a timer with the
//					specified time-out value.
//
//	Return :		BOOL	-	Nonzero if successful.
//
//	Parameters :
//		UINT uTimeOutVal	-	time-out value.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
/*
BOOL CCommUtil::SetTimeOutVal(UINT uTimeOutVal)
{
	if( uTimeOutVal )
		m_nTimeOutVal = uTimeOutVal;

	return TRUE;
}
*/
/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::InitializeTimer
//	Description :	This routine is called to initialize the timer.
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
/*
void CCommUtil::InitializeTimer()
{
	m_initTime = CTime::GetCurrentTime();
	m_bTimeOut = FALSE;
}
*/
/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::CheckTimer
//	Description :	This routine is called to initialize the timer.
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
/*
BOOL CCommUtil::CheckTimer()
{
	CTimeSpan	elaspedtime= CTime::GetCurrentTime() - m_initTime;
	LONG		elaspedsecs= elaspedtime.GetTotalSeconds();

	if( elaspedsecs > m_nTimeOutVal )
	{
		m_bTimeOut = TRUE;
		SetLastError(WSAECONNABORTED);
		return FALSE;
	}

	return TRUE;
}
*/
/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Receive
//	Description :	This routine is called to receive data from a
//					socket. This function is used for connected
//					stream or datagram sockets and is used to read
//					incoming data.
//
//	Return :		int	-	returns the number of bytes received.
//
//	Parameters :
//		void* lpBuf		-	A buffer containing the data to
//							be transmitted.
//		int nBufLen		-	The length of the data in lpBuf
//							in bytes.
//		int nFlags		-	Specifies the way in which the
//							call is made.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
/*
int CCommUtil::Receive(void* lpBuf, int nBufLen, int nFlags)
{
	m_nTimeOut = 0;

	if( m_pbBlocking != NULL )
	{
		WSASetLastError(WSAEINPROGRESS);
		return FALSE;
	}

	int nResult;

	while( (nResult = CAsyncSocket::Receive(lpBuf, nBufLen, nFlags)) == SOCKET_ERROR )
	{
		if( GetLastError() == WSAEWOULDBLOCK )
		{
			if( !PumpMessages(FD_READ) )
				return SOCKET_ERROR;
		}
		else
			return SOCKET_ERROR;

		if( !CheckTimer() )
			return SOCKET_ERROR;
	}

	return nResult;
}
*/
/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Send
//	Description :	This routine is called to write outgoing data
//					on connected stream or datagram sockets.
//
//	Return :		int	-	returns the total number of characters
//							sent.
//
//	Parameters :
//		const void* lpBuf	-	A buffer containing the data to
//								be transmitted.
//		int nBufLen			-	The length of the data in lpBuf
//								in bytes.
//		int nFlags			-	Specifies the way in which the
//								call is made.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
/*
int CCommUtil::Send(const void* lpBuf, int nBufLen, int nFlags)
{
	if( m_pbBlocking != NULL )
	{
		WSASetLastError(WSAEINPROGRESS);
		return  FALSE;
	}

	int		nLeft;
	int		nWritten;
	PBYTE	pBuf = (PBYTE)lpBuf;

	nLeft = nBufLen;

	while( nLeft > 0 )
	{
		nWritten = SendChunk(pBuf, nLeft, nFlags);

		if( nWritten == SOCKET_ERROR )
			return nWritten;

		nLeft-= nWritten;
		pBuf += nWritten;
	}

	return nBufLen - nLeft;
}
*/
/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::SendChunk
//	Description :	This routine is called to write outgoing data
//					on connected stream or datagram sockets.
//
//	Return :		int	-	returns the total number of characters
//							sent.
//
//	Parameters :
//		const void* lpBuf	-	A buffer containing the data to
//								be transmitted.
//		int nBufLen			-	The length of the data in lpBuf
//								in bytes.
//		int nFlags			-	Specifies the way in which the
//								call is made.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
/*
int CCommUtil::SendChunk(const void* lpBuf, int nBufLen, int nFlags)
{
	int nResult;

	m_nTimeOut = 0;

	while( (nResult = CAsyncSocket::Send(lpBuf, nBufLen, nFlags)) == SOCKET_ERROR )
	{
		if( GetLastError() == WSAEWOULDBLOCK )
		{
			if( !PumpMessages(FD_WRITE) )
				return SOCKET_ERROR;
		}
		else
			return SOCKET_ERROR;

		if( !CheckTimer() )
			return SOCKET_ERROR;
	}

	return nResult;
}
*/
/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::CreateAndConnect
//
//	Description :	Creates a IP socket.  Once the socket is created,
//					routine attempts to connect to the socket.
//
//	Return :	bool	-	true if successful
//
//	Parameters :
//		const CString strHost	-	name of IP host
//		UINT nPort				-	port number
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::CreateAndConnect(const CString strHost, UINT nPort)
{
	USES_CONVERSION;

	if (!m_Socket.Create()){
		CString strMsg;
		strMsg.Format(_T("Error creating a socket connection."));
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return false;
	}

	LPSTR	lpszAscii	= T2A((LPTSTR)((LPCTSTR)strHost));
	struct	hostent* hp = gethostbyname(lpszAscii);

	if(hp == NULL){
		CString strMsg;
		strMsg.Format(_T("Error retrieving the OATS host name."));
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return false;
	}

	struct sockaddr_in sktAddrServer;
	memset(&sktAddrServer, 0, sizeof(struct sockaddr_in));
	memcpy(&(sktAddrServer.sin_addr), hp->h_addr, hp->h_length);
	sktAddrServer.sin_family= hp->h_addrtype;
	sktAddrServer.sin_port	= htons((u_short)nPort);

	if(!m_Socket.Connect((struct sockaddr*)&sktAddrServer, sizeof(sktAddrServer))){
		CString strMsg;
		strMsg.Format(_T("Unable to get a socket connection."));
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return false;
	}

	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::ShutdownAndClose
//
//	Description :	Closes the IP socket.
//
//
//	Return :	bool	-	true if successful
//
//	Parameters :
//
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::ShutdownAndClose()
{
	if( !m_Socket.ShutDown(0x02) )
		return false;

	m_Socket.Close();

	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
//           Fred Shaw             11-17-05					2.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::SendMsg
//
//	Description :	Sends a 504-02 message to OATS, Then waits for a
//					504-02 message response from OATS.
//
//
//	Return :	bool	- true if successful
//
//	Parameters :
//				void* pInMsg	- pointer to 504-02 input message.
//				int   nMsgType	- type of 504-02 message.
//				void* pOutMsg	- pointer to 504-02 output message.
//
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::SendMsg(void* pInMsg, int nMsgType, void* pOutMsg)
{
	unsigned __int32*	wordArray	= (unsigned __int32*)pInMsg;
	int					nSize		= (wordArray[2]*2) + (MSG_HEADER_HW_SIZE*2);
	int					nSent		= m_Socket.Send(pInMsg, nSize);

	if( nSent != nSize )
		return false;

	if( !m_Socket.SetTimeOutVal(GetAppRegOATS()->m_nTimeout) )
		return false;

	m_Socket.InitializeTimer();

	unsigned __int16*	phwArray[MSG_HEADER_HW_SIZE*2];
	MSG_HEADER*         pMsgHdr	= (MSG_HEADER*)&phwArray[0];
	int					nRead	= ReadMsg((void*)pMsgHdr,MSG_HEADER_HW_SIZE*2);

	if(nRead != MSG_HEADER_HW_SIZE*2){
		return false;
	}

	switch( pMsgHdr->m_nMsgType )
	{
	case 51:
		if (pMsgHdr->m_nMsgType != nMsgType){
			// Need error message type received is different than what was expected.
				CString strMsg;
				strMsg.Format(_T("Message type recieved from OATS does not match what was expected."));
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			return false;
		} else if(!Read51Msg((CPtrList*)pOutMsg, pMsgHdr)){
			return false;
		}
		break;
	case 53:
		if (pMsgHdr->m_nMsgType != nMsgType){
			// Need error message type received is different than what was expected.
				CString strMsg;
				strMsg.Format(_T("Message type recieved from OATS does not match what was expected."));
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			return false;
		} else if(!Read53Msg((MSG_53*)pOutMsg, pMsgHdr)){
			return false;
		}
		break;
	case 55:
		if (pMsgHdr->m_nMsgType != nMsgType){
			// Need error message type received is different than what was expected.
				CString strMsg;
				strMsg.Format(_T("Message type recieved from OATS does not match what was expected."));
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			return false;
		} else if( !Read55Msg((MSG_55*)pOutMsg, pMsgHdr)){
			return false;
		}
		break;
	case 59:
		if (pMsgHdr->m_nMsgType != nMsgType){
			// Need error message - type received is different than what was expected.
				CString strMsg;
				strMsg.Format(_T("Message type recieved from OATS does not match what was expected."));
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			return false;
		} else if(!Read59Msg((MSG_59*)pOutMsg, pMsgHdr)){
			return false;
		}
		break;
	case 60: // Subtype 1
	case 61: // Subtype 2
		if (60 != pMsgHdr->m_nMsgType){
			// Need error message - type received is different than what was expected.
				CString strMsg;
				strMsg.Format(_T("Message type recieved from OATS does not match what was expected."));
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			return false;
		} else if (pMsgHdr->m_nMsgSubType == 1){
			if( !Read60Msg((MSG_60*)pOutMsg, pMsgHdr)){
				return false;
			}
		} else if (pMsgHdr->m_nMsgSubType == 2){
			// case 61: // Actually a 60 subtype 2
			if( !Read61Msg((CPtrList*)	pOutMsg, pMsgHdr)){
				return false;
			}
		} else {
			// Need error message - type received is different than what was expected.
				CString strMsg;
				strMsg.Format(_T("Message type recieved from OATS does not match what was expected."));
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			return false;
		}

		break;
	case 75:
		if (pMsgHdr->m_nMsgType != nMsgType){
			// Need error message - type received is different than what was expected.
				CString strMsg;
				strMsg.Format(_T("Message type recieved from OATS does not match what was expected."));
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			return false;
		} else if( !Read75Msg((CPtrList*)pOutMsg, pMsgHdr)) {
			return false;
		}
		break;

	// Status message from OATS only sent if error occurred.
	case 82:
		Read82Msg(); 
		return false;
		break;

	default:
		// Need error message - type is not valid.
		//	CString strMsg;
		//	strMsg.Format(_T("Not enough star windows received from OATS."));
		//	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
		return false;
	}

	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
//           Fred Shaw             11-17-05					2.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Read51Msg
//
//	Description :	Reads a 504-02 message 51 from the socket.
//
//	Return :	bool	     -	true if successful
//
//	Parameters :
//		CPtrList* pList	     -	pointer to a List object.
//							    message is stored in this object.
//
//		MSG_HEADER* pMsgHdr	 -	pointer to a MSG_HEADER object.
//							    header message is stored in this object.
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::Read51Msg(CPtrList* pList, MSG_HEADER* pMsgHdr)
{
	MSG_51_HEADER		msg51Hdr;
	MSG_51_WINDOW_DATA	msg51Window;
	MSG_51_STAR_DATA	msg51Star;
	MSG_51_LOOK_DATA	msg51Look;
	MSG_51*				p51Msg	= NULL;
	CPtrList			NewList;

	m_nWinCnt					= 0;
	int  nRead = MSG_HEADER_HW_SIZE*2;  // Prime pump

	while(nRead == MSG_HEADER_HW_SIZE*2 )
	{
		p51Msg= new MSG_51;
		pList->AddTail((void*)p51Msg);
		memcpy(&p51Msg->m_hdr, pMsgHdr, MSG_HEADER_HW_SIZE*2);
		nRead = ReadMsg((void*)&msg51Hdr,MSG_51_HEADER_HW_SIZE*2);

		if( nRead != MSG_51_HEADER_HW_SIZE*2 )
			return false;

		m_nWinCnt += msg51Hdr.m_nNumWindows;
		memcpy(&p51Msg->m_51Hdr, &msg51Hdr, MSG_51_HEADER_HW_SIZE*2);
		UINT nCurWindow = 0;

		while( nCurWindow < msg51Hdr.m_nNumWindows )
		{
			nRead = ReadMsg((void*)&msg51Window,MSG_51_WINDOW_DATA_HW_SIZE*2);

			if( nRead != MSG_51_WINDOW_DATA_HW_SIZE*2 )
				return false;

			memcpy(&p51Msg->m_51WindowData[nCurWindow].m_data,
				&msg51Window, MSG_51_WINDOW_DATA_HW_SIZE*2);
			UINT nCurStar = 0;

			while( nCurStar < msg51Window.m_nNumStarsInWindow )
			{
				nRead = ReadMsg((void*)&msg51Star,MSG_51_STAR_DATA_HW_SIZE*2);

				if( nRead != MSG_51_STAR_DATA_HW_SIZE*2 )
					return false;

				memcpy(&p51Msg->m_51WindowData[nCurWindow].
					m_51StarData[nCurStar].m_data,
					&msg51Star, MSG_51_STAR_DATA_HW_SIZE*2);
				UINT nCurLook = 0;

				while( nCurLook < msg51Star.m_nNumLooksForStar )
				{
					nRead = ReadMsg((void*)&msg51Look,MSG_51_LOOK_DATA_HW_SIZE*2);

					if( nRead != MSG_51_LOOK_DATA_HW_SIZE*2 )
						return false;

					memcpy(&p51Msg->m_51WindowData[nCurWindow].
						m_51StarData[nCurStar].m_51LookData[nCurLook].
						m_data, &msg51Look, MSG_51_LOOK_DATA_HW_SIZE*2);
					nCurLook++;
				}

				nCurStar++;
			}

			nCurWindow++;
		}

		nRead=0;

		if( m_nWinCnt < m_nWindows )
		{
			if( (nRead = ReadMsg((void*)pMsgHdr,MSG_HEADER_HW_SIZE*2)) < 0 ){
				CString strMsg;
				strMsg.Format(_T("Not enough star windows received from OATS."));
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				return false;
			}
		}
	}

	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
//           Fred Shaw             11-17-05					2.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Read53Msg
//
//	Description :	Reads a 504-02 message 53 from the socket.
//
//	Return :	bool	    - true if successful
//
//	Parameters :
//		MSG_53* p53Msg	    - pointer to a message 53 object.
//		MSG_HEADER* pMsgHdr - pointer to the common message header
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::Read53Msg(MSG_53* p53Msg, MSG_HEADER* pMsgHdr)
{
	unsigned __int16*	phwArray= (unsigned __int16*)p53Msg;

	MSG_53_HEADER*	pMsg53Hdr	= (MSG_53_HEADER*)&phwArray[MSG_HEADER_HW_SIZE];
	memcpy(&p53Msg->m_hdr, pMsgHdr, MSG_HEADER_HW_SIZE*2); // Copy common message header into specific message
	int				nRead		= ReadMsg((void*)pMsg53Hdr,MSG_53_HEADER_HW_SIZE*2);

	if (nRead != MSG_53_HEADER_HW_SIZE*2)
		return false;

	MSG_53_DATA*	pMsg53Data	= (MSG_53_DATA*)&phwArray[MSG_HEADER_HW_SIZE+MSG_53_HEADER_HW_SIZE];
					nRead		= ReadMsg((void*)pMsg53Data, MSG_53_DATA_HW_SIZE*2);

	if (nRead != MSG_53_DATA_HW_SIZE*2)
		return false;

	MSG_53_IMC_DATA*	pMsg53IMCData	= (MSG_53_IMC_DATA*)
						&phwArray[MSG_HEADER_HW_SIZE+MSG_53_HEADER_HW_SIZE+MSG_53_DATA_HW_SIZE];
						nRead			= ReadMsg((void*)pMsg53IMCData, MSG_53_IMC_DATA_HW_SIZE*2*10);

	if (nRead !=  MSG_53_IMC_DATA_HW_SIZE*2*10)
		return false;

	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
//           Fred Shaw             11-17-05					2.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Read55Msg
//
//	Description :	Reads a 504-02 message 55 from the socket.
//
//	Return :	bool	-	true if successful
//
//	Parameters :
//		MSG_55*     p55Msg	 -	pointer to message 55 object.
//
//		MSG_HEADER* pMsgHdr	 -	pointer to a MSG_HEADER object.
//							    header message is stored in this object.
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::Read55Msg(MSG_55* p55Msg, MSG_HEADER* pMsgHdr)
{
	MSG_55_HEADER		msg55Hdr;
	MSG_55_DATA			msg55Data[MAX_55_DATA];
	unsigned __int16*	phwArray = (unsigned __int16*)p55Msg;

	memcpy(&p55Msg->m_hdr, pMsgHdr, MSG_HEADER_HW_SIZE*2);

	MSG_55_HEADER*	pMsg55Hdr	= (MSG_55_HEADER*)&phwArray[MSG_HEADER_HW_SIZE];
	int				nRead		= ReadMsg((void*)&msg55Hdr, MSG_55_HEADER_HW_SIZE*2);

	if( nRead != MSG_55_HEADER_HW_SIZE*2 )
		return false;

	memcpy(pMsg55Hdr, &msg55Hdr, MSG_55_HEADER_HW_SIZE*2);
	int				nSize		= msg55Hdr.m_nNumFrames*MSG_55_DATA_HW_SIZE*2;
	MSG_55_DATA*	pMsg55Data	= (MSG_55_DATA*)&phwArray[MSG_HEADER_HW_SIZE+MSG_55_HEADER_HW_SIZE];

	nRead = ReadMsg((void*)&msg55Data[0], nSize);
	if( nRead != nSize )
		return false;

	memcpy(pMsg55Data, &msg55Data[0], nSize);
	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
//           Fred Shaw             11-17-05					2.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Read59Msg
//
//	Description :	Reads a 504-02 message 59 from the socket.
//
//	Return :	bool	- true if successful
//
//	Parameters :
//		CPtrList* pList	-	pointer to a List object.
//							message is stored in this object.
//		MSG_HEADER* pMsgHdr	 -	pointer to a MSG_HEADER object.
//							    header message is stored in this object.
//
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::Read59Msg(MSG_59* p59Msg, MSG_HEADER* pMsgHdr)
{
	unsigned __int16*	phwArray = (unsigned __int16*)p59Msg;

	memcpy(&p59Msg->m_hdr, pMsgHdr, MSG_HEADER_HW_SIZE*2);
	MSG_59_HEADER*	pMsg59Hdr	= (MSG_59_HEADER*)&phwArray[MSG_HEADER_HW_SIZE];

	int 			nRead		= ReadMsg((void*)pMsg59Hdr,MSG_59_HEADER_HW_SIZE*2);
	if( nRead != MSG_59_HEADER_HW_SIZE*2 )
		return false;

	int				nSize		= pMsg59Hdr->m_nNumEvents*MSG_59_DATA_HW_SIZE*2;
	MSG_59_DATA*	pMsg59Data	= (MSG_59_DATA*)&phwArray[MSG_HEADER_HW_SIZE+MSG_59_HEADER_HW_SIZE];

	nRead = ReadMsg((void*)pMsg59Data, nSize);
	if( nRead != nSize )
		return false;

	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
//           Fred Shaw             11-17-05					2.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Read60Msg
//
//	Description :	Reads a 504-02 message 60 from the socket.
//
//	Return :	bool	-	true if successful
//
//	Parameters :
//		MSG_60* p60Msg	-	pointer to a 504-02 message 60
//
//		MSG_HEADER* pMsgHdr	 -	pointer to a MSG_HEADER object.
//							    header message is stored in this object.
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::Read60Msg(MSG_60* p60Msg, MSG_HEADER* pMsgHdr)
{
	MSG_60_HEADER	msg60Hdr;
	MSG_60			msg60;

	memcpy(&msg60.m_hdr, pMsgHdr, MSG_HEADER_HW_SIZE*2);

	int nRead = ReadMsg((void*)&msg60Hdr, MSG_60_HEADER_HW_SIZE*2);
	if (nRead != MSG_60_HEADER_HW_SIZE*2){
		return false;
	}

	// Get Imager Data
	memcpy(&msg60.m_60ImagerHdr, &msg60Hdr, MSG_60_HEADER_HW_SIZE*2);

	if (!Read60Data(&msg60, msg60Hdr.m_nNumIntrusions, 0)){
		return false;
	}

	nRead = ReadMsg((void*)&msg60Hdr, MSG_60_HEADER_HW_SIZE*2);
	if (nRead != MSG_60_HEADER_HW_SIZE*2){
		return false;
	}

	// Get Sounder Data
	memcpy(&msg60.m_60SounderHdr, &msg60Hdr, MSG_60_HEADER_HW_SIZE*2);
	if(!Read60Data(&msg60, msg60Hdr.m_nNumIntrusions, 1)){
		return false;
	}

	nRead = ReadMsg((void*)&msg60Hdr, MSG_60_HEADER_HW_SIZE*2);
	if (nRead != MSG_60_HEADER_HW_SIZE*2){
		return false;
	}

	// Get Antenna Data
	memcpy(&msg60.m_60AntennaHdr, &msg60Hdr, MSG_60_HEADER_HW_SIZE*2);
	if (!Read60Data(&msg60, msg60Hdr.m_nNumIntrusions, 2)){
		return false;
	}

	memcpy(p60Msg, &msg60, MSG_60_HW_SIZE*2);
	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Read60Data
//
//	Description :	Reads a 504-02 message 60 from the socket.
//
//	Return :	bool	-	true successful
//
//	Parameters :
//		MSG_60* p60Msg			-	pointer to message 60 object
//		__int32 nNumIntrusions	-	Number of intrusions
//		int nInstr				-	instrument number
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::Read60Data(MSG_60* p60Msg, __int32 nNumIntrusions, int nInstr)
{
	MSG_60_DATA	msg60Data;
	int			nRead = 0;

	for( int i=0; i<nNumIntrusions; i++ )
	{
		nRead = ReadMsg((void*)&msg60Data, MSG_60_DATA_HW_SIZE*2);

		if( nRead != MSG_60_DATA_HW_SIZE*2 )
			return false;

		switch( nInstr )
		{
			case 0:
				memcpy(&p60Msg->m_60ImagerData[i], &msg60Data,MSG_60_DATA_HW_SIZE*2);
				break;

			case 1:
				memcpy(&p60Msg->m_60SounderData[i],&msg60Data,MSG_60_DATA_HW_SIZE*2);
				break;

			case 2:
				memcpy(&p60Msg->m_60AntennaData[i],&msg60Data,MSG_60_DATA_HW_SIZE*2);
		}
	}

	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Read61Msg
//
//	Description :	Reads a 504-02 message 61 from the socket.
//
//	Return :	bool	-	true if successful
//
//	Parameters :
//		CPtrList* pList	-	pointer to a List object.
//							message is stored in this object.
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::Read61Msg(CPtrList* pList, MSG_HEADER* pMsgHdr)
{
//	MSG_HEADER				msgHdr;
	MSG_61_HEADER			msg61Hdr;
	MSG_61_FRAME_DATA		msg61Frame;
	MSG_61_INTRUSION_DATA	msg61Intrusion;

	MSG_61*	p61Msg	= NULL;
//	int		nRead	= ReadMsg((void*)&msgHdr,MSG_HEADER_HW_SIZE*2);
	int     nRead   = MSG_HEADER_HW_SIZE*2; // Prime the pump

//	if( nRead < 0 )
//		return false;

	while( nRead == MSG_HEADER_HW_SIZE*2 )
	{
		p61Msg	= new MSG_61;
		pList->AddTail((void*)p61Msg);
		memcpy(&p61Msg->m_hdr, pMsgHdr, MSG_HEADER_HW_SIZE*2);
		nRead	= ReadMsg((void*)&msg61Hdr,MSG_61_HEADER_HW_SIZE*2);

		if( nRead != MSG_61_HEADER_HW_SIZE*2 )
			return false;

		memcpy(&p61Msg->m_61Hdr, &msg61Hdr, MSG_61_HEADER_HW_SIZE*2);
		int nCurFrame = 0;

		while( nCurFrame < msg61Hdr.m_nNumFrames )
		{
			nRead = ReadMsg((void*)&msg61Frame,MSG_61_FRAME_DATA_HW_SIZE*2);

			if( nRead != MSG_61_FRAME_DATA_HW_SIZE*2 )
				return false;

			memcpy(&p61Msg->m_61FrameData[nCurFrame].m_data,
				&msg61Frame, MSG_61_FRAME_DATA_HW_SIZE*2);
			int nCurIntrusion = 0;

			while( nCurIntrusion < msg61Frame.m_nNumIntrusions )
			{
				nRead = ReadMsg((void*)&msg61Intrusion,MSG_61_INTRUSION_DATA_HW_SIZE*2);

				if( nRead != MSG_61_INTRUSION_DATA_HW_SIZE*2 )
					return false;

				memcpy(&p61Msg->m_61FrameData[nCurFrame].
					m_61IntrusionData[nCurIntrusion].m_data,
					&msg61Intrusion, MSG_61_INTRUSION_DATA_HW_SIZE*2);
				nCurIntrusion++;
			}

			nCurFrame++;
		}

		nRead=0;

		if( pMsgHdr->m_nMsgEndFlag == 0 )
		{
			if( (nRead = ReadMsg((void*)pMsgHdr,MSG_HEADER_HW_SIZE*2)) < 0 ){
				CString strMsg;
				strMsg.Format(_T("Last message not received from OATS."));
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				return false;
			}
		}
	}

	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Read75Msg
//
//	Description :	Reads a 504-02 message 75 from the socket.
//
//	Return :		bool-	true if successful
//
//	Parameters :
//		CPtrList* pList	-	pointer to a List object.
//							message is stored in this object.
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::Read75Msg(CPtrList* pList, MSG_HEADER* pMsgHdr)
{
// 	MSG_HEADER			msgHdr;
	MSG_75_HEADER		msg75Hdr;
	MSG_75_OFFSET_DATA	msg75Offset;
	MSG_75_STAR_DATA	msg75Star;
	MSG_75_LOOK_DATA	msg75Look;

	MSG_75*				p75Msg	= NULL;
//	int					nRead	= ReadMsg((void*)&msgHdr,MSG_HEADER_HW_SIZE*2);
	int					nRead	= MSG_HEADER_HW_SIZE*2;  // Prime pump

//	if( nRead < 0 )
//		return false;

	while( nRead == MSG_HEADER_HW_SIZE*2 )
	{
		p75Msg = new MSG_75;
		pList->AddTail((void*)p75Msg);
		memcpy(&p75Msg->m_hdr, pMsgHdr, MSG_HEADER_HW_SIZE*2);
		nRead = ReadMsg((void*)&msg75Hdr, MSG_75_HEADER_HW_SIZE*2);

		if( nRead != MSG_75_HEADER_HW_SIZE*2 )
			return false;

		memcpy(&p75Msg->m_75Hdr, &msg75Hdr, MSG_75_HEADER_HW_SIZE*2);
		UINT nCurOffset = 0;

		while( nCurOffset < msg75Hdr.m_nNumOffsets )
		{
			nRead = ReadMsg((void*)&msg75Offset,MSG_75_OFFSET_DATA_HW_SIZE*2);

			if( nRead != MSG_75_OFFSET_DATA_HW_SIZE*2 )
				return false;

			memcpy(&p75Msg->m_75OffsetData[nCurOffset].m_data,
				&msg75Offset, MSG_75_OFFSET_DATA_HW_SIZE*2);
			UINT nCurStar = 0;

			while( nCurStar < msg75Offset.m_nNumStarsInOffset )
			{
				nRead = ReadMsg((void*)&msg75Star,MSG_75_STAR_DATA_HW_SIZE*2);

				if( nRead != MSG_75_STAR_DATA_HW_SIZE*2 )
					return false;

				memcpy(&p75Msg->m_75OffsetData[nCurOffset].
					m_75StarData[nCurStar].m_data,
					&msg75Star, MSG_75_STAR_DATA_HW_SIZE*2);
				UINT nCurLook = 0;

				while( nCurLook < msg75Star.m_nNumLooksForStar )
				{
					nRead = ReadMsg((void*)&msg75Look,MSG_75_LOOK_DATA_HW_SIZE*2);

					if( nRead != MSG_75_LOOK_DATA_HW_SIZE*2 )
						return false;

					memcpy(&p75Msg->m_75OffsetData[nCurOffset].
						m_75StarData[nCurStar].m_75LookData[nCurLook].
						m_data, &msg75Look, MSG_75_LOOK_DATA_HW_SIZE*2);
					nCurLook++;
				}

				nCurStar++;
			}

			nCurOffset++;
		}

		nRead=0;

		if( pMsgHdr->m_nMsgEndFlag == 0 )
		{
			if((nRead = ReadMsg((void*)pMsgHdr,MSG_HEADER_HW_SIZE*2)) < 0 ){
				CString strMsg;
				strMsg.Format(_T("Last message not received from OATS."));
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				return false;
			}
		}
	}

	return true;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::Read82Msg()
//
//	Description :	Reads a 504-02 message 82 from the socket.
//
//	Return :		bool -	true if successful
//							false if failure			
//
//	Parameters : VOID
//		
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
bool CCommUtil::Read82Msg()
{
	char		cMsg82Data[MAX_82_DATA_SIZE + 1];
	int nRead = ReadMsg(cMsg82Data, MAX_82_DATA_SIZE);
	// Once the error message has been read, output it to the events log.
	if( nRead != MAX_82_DATA_SIZE){
		return false;
	}

	cMsg82Data[MAX_82_DATA_SIZE] = '\0';
		
	CString strErrorMsg(cMsg82Data);
	strErrorMsg.TrimLeft();
	strErrorMsg.TrimRight();
	GetAppOutputBar()->ClearBuild();
	GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
	return true;
}


/**
////////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////////
//	Function :		CCommUtil::ReadMsg
//
//	Description :	Receives a 504-02 message from the socket.
//
//	Return :	int	-	the number of bytes received from socket
//
//	Parameters :
//		void* pObj	-	pointer to
//		int nSize	-	number of bytes to read from socket.
//
//	Note :
//
////////////////////////////////////////////////////////////////////////////
**/
int CCommUtil::ReadMsg(void* pObj, int nSize)
{
	int nRead = 0;

	if( nSize > 0 )
	{
		int nBytes = m_Socket.Receive(pObj, nSize);

		if( m_Socket.HadTimeOut() )
		{
			CString strMsg;
			strMsg.Format(_T("No response from OATS, timeout period of %d seconds has been reached."),
				GetAppRegOATS()->m_nTimeout);
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return -1;
		}

		if( nBytes < 0 )
			return nBytes;

		nRead += nBytes;

		while( nRead < nSize )
		{
			nBytes = m_Socket.Receive(&((char*)pObj)[nRead], nSize-nRead);

			if( nBytes < 0 )
				return nBytes;

			nRead += nBytes;
		}
	}

	return nRead;
}
