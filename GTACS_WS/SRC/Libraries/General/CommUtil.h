#if !defined(AFX_COMMUTIL_H__A205CD37_9644_11D2_B904_00034748C6CA__INCLUDED_)
#define AFX_COMMUTIL_H__A205CD37_9644_11D2_B904_00034748C6CA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommUtil.h : header file
//

#include "TimeOutSocket.h"
#include "50402Msg.h"

/////////////////////////////////////////////////////////////////////////////
// CCommUtil command target

class CCommUtil : public CCmdTarget
{
	DECLARE_DYNCREATE(CCommUtil)

	CCommUtil();           // protected constructor used by dynamic creation

// Attributes
public:
	bool	CreateAndConnect(const CString, UINT);
	bool	SendMsg(void*, int, void*);
	bool	ShutdownAndClose();
//	BOOL	SetTimeOutVal(UINT);
//	BOOL	HadTimeOut() const	{ return m_bTimeOut; }
//	void	InitializeTimer();
	void	WinCnt(int nWinCnt)	{ m_nWindows = nWinCnt;	}
	int		WinCnt()			{ return m_nWinCnt;	}

// Operations
public:
	virtual ~CCommUtil();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommUtil)
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CCommUtil)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

// Implementation
protected:
	bool		Read53Msg(MSG_53*, MSG_HEADER*);
	bool		Read55Msg(MSG_55*, MSG_HEADER*);
	bool		Read59Msg(MSG_59*, MSG_HEADER*);
	bool		Read60Msg(MSG_60*, MSG_HEADER*);
	bool		Read60Data(MSG_60*, __int32, int);
	bool		Read51Msg(CPtrList*, MSG_HEADER*);
	bool		Read61Msg(CPtrList*, MSG_HEADER*);
	bool		Read75Msg(CPtrList*, MSG_HEADER*);
	bool		Read82Msg();
	int			ReadMsg(void*, int);
//	int			SendChunk(const void* lpBuf, int nBufLen, int nFlags=0);
//	virtual int	Receive(void* lpBuf, int nBufLen, int nFlags=0);
//	virtual int	Send(const void* lpBuf, int nBufLen, int nFlags=0);

private:
//	CTime	m_initTime;
//	BOOL	m_bTimeOut;
//	int		m_nTimeOutVal;
	CTimeOutSocket	m_Socket;
	int				m_nWindows;
	int				m_nWinCnt;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMUTIL_H__A205CD37_9644_11D2_B904_00034748C6CA__INCLUDED_)
