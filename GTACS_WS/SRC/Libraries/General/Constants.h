#if !defined(CONSTANTS_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_)
#define CONSTANTS_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_

// D.Robinson  NOAA/SOCC   Added changes for GOES-IO.  Changed number of GTACS
// at Wallops from 4 to 3, and replaced bgtacs01 with dgtacs01  
// June 6, 2019

// Doc Types
const int nSTOL							= 0;
const int nCLS							= 1;
const int nRpt							= 2;
const int nSector						= 3;
const int nFrame						= 4;
const int nStar							= 5;
const int nBinary						= 6;
const int nSTOLMap						= 7;
const int nIMC							= 8;
const int nRTCSSet						= 9;
const int nLis							= 10;
const int nRTCS							= 11;
const int nRTCSCmd						= 12;
const int nRTCSMap						= 13;
const int nPaceCmd						= 14;
const int nSTOLCmd						= 15;
const int nErr							= 16;
const int nMaxDocTypes					= 17;
const int nInvalidType					= 18;

// Folder Types
const int nFolderSTOL					= 0;
const int nFolderCLS					= 1;
const int nFolderFrame					= 2;
const int nFolderStar					= 3;
const int nFolderOATS					= 4;
const int nFolderRTCS					= 5;
const int nFolderData					= 6;
const int nFolderCLS01					= 7;
const int nFolderCLS02					= 8;
const int nFolderCLS03	 				= 9;
const int nFolderCLS04 					= 10;
const int nFolderCLS05 					= 11;
const int nFolderCLS06					= 12;
const int nFolderCLS07 					= 13;
const int nFolderCLS08					= 14;
const int nFolderCLS09 					= 15;
const int nFolderCLS10					= 16;
const int nMaxFolderTypes				= 17;

const int nMaxSC						= 8;
const int nMaxSites						= 5;
const int nSOCC							= 0;
const int nWallops						= 1;
const int nGoddard						= 2;
const int nSEC							= 3;
const int nFairbanks					= 4;
const int nMaxGTACSperSite				= 5; //Max per site
// removed cgtacs04 from Wallops and changed SWPC to GOES-IO
// const int nMaxGTACS[nMaxSites]			= {5, 4, 1, 1, 1};
const int nMaxGTACS[nMaxSites]			= {5, 3, 1, 1, 1};
const int nMaxOATSperSite				= 7; //Max per site
const int nMaxOATS[nMaxSites]			= {7, 3, 1, 0, 0};

const int nInstObjSector				= 0;
const int nInstObjFrame					= 1;
const int nInstObjStar					= 2;
const int nMaxInstObj					= 3;
const int nMaxInstObjPropsPerObj		= 22;
const int nMaxInstObjProps[nMaxInstObj] = {11, 22, 14}; //Sector Frame Star

const int nMapObjSTOL					= 0;
const int nMapObjRTCS					= 1;
const int nMaxMapObj					= 2;
const int nMaxMapObjPropsPerObj			= 11;
const int nMaxMapObjProps[nMaxMapObj]	= {9, 11}; //STOL RTCS

const int nInvalid						= -1;
const int nPackage						= 0;
const int nValCLS						= 1;
const int nValSTOL						= 2;
const int nValRTCS						= 3;
const int nValRTCSSet					= 4;
const int nUpdFrame						= 5;
const int nUpdSTOL						= 6;
const int nReqIMC						= 7;
const int nReqIntrusion					= 8;
const int nReqEclipse					= 9;
const int nReqSFCS						= 10;
const int nGenSTOL						= 11;

//const int nOnboard					= 1;
//const int nGround						= 2;
//const int nBoth						= nOnboard & nGround;

// RTF formatting constants
const CString strBB			= _T("{\\b ");					// Bold Begin
const CString strBE			= _T("}");						// Bold End
const CString strTB			= _T("{\\fs24 ");				// Tall Begin
const CString strTE			= _T("}");						// Tall End
const CString strPE			= _T("\n\\par");				// Paragraph end
const CString str2PE		= _T("\n\\par\n\\par");			// Two paragraph ends
const CString strTab		= _T("	");						// Tab
const CString str2Tab		= _T("		");					// Tab Tab
const CString str2TabBTab	= _T("		-	");			// Tab Tab Bullet Tab
const CString rtfPrefix		= _T("{\\rtf1\\ansi\\deff0\\deftab720{\\fonttbl{\\f0\\froman ")
							  _T("Times New Roman;}}\n{\\colortbl\\red0\\green0\\blue0;}\n")
							  _T("\\deflang1033\\pard\\tx180\\tx360\\tx540\\tx720\\plain\\f3\\fs20 ");
const CString rtfPostfix	= _T("\n\\par }");


#endif // !defined(CONSTANTS_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_)




