/**
/////////////////////////////////////////////////////////////////////////////
// EditListCtrl.cpp : implementation of the EditListCtrl class.            //
// (c) 2001 Don Sanders                                                    //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// EditListCtrl.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class creates a list control for general use.
// 
/////////////////////////////////////////////////////////////////////////////
**/
#include "StdAfx.h"
#include "EditListCtrl.h"
#include "Resource.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInPlaceList::CInPlaceList
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//		int iItem				-	
//		int iSubItem			-
//		CStringList *plstItems	-
//		int nSel				-
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CInPlaceList::CInPlaceList(int iItem, int iSubItem, CStringList *plstItems, int nSel)
{
	m_iItem = iItem;
	m_iSubItem = iSubItem;
	m_lstItems.AddTail(plstItems);
	m_nSel = nSel;
	m_bESC = FALSE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInPlaceList::~CInPlaceList
//	Description :	Destructor
//
//	Return :		Destructor -
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CInPlaceList::~CInPlaceList()
{
}

BEGIN_MESSAGE_MAP(CInPlaceList, CComboBox)
	//{{AFX_MSG_MAP(CInPlaceList)
	ON_WM_CREATE()
	ON_WM_KILLFOCUS()
	ON_WM_CHAR()
	ON_WM_NCDESTROY()
	ON_CONTROL_REFLECT(CBN_CLOSEUP, OnCloseup)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInPlaceList::OnCreate
//	Description :	This routine is called when an application 
//					requests that the Windows window be created.
//
//	Return :	int	-	0 to continue the creation of the CWnd object.
//
//	Parameters :
//		LPCREATESTRUCT lpCreateStruct -	pointer to a CREATESTRUCT 
//										structure that contains 
//										information about the CWnd 
//										object being created.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CInPlaceList::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CComboBox::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// Set the proper font
	CFont* font = GetParent()->GetFont();
	SetFont(font);

	for (POSITION pos = m_lstItems.GetHeadPosition(); pos != NULL;)
	{
		AddString((LPCTSTR)(m_lstItems.GetNext(pos)));
	}
	SetCurSel(m_nSel);
	SetFocus();
	return 0;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInPlaceList::PreTranslateMessage
//	Description :	This routine performs accelerator-key translation.
//
//	Return :	BOOL	-	Zero if the message should be processed 
//							in the normal way.
//
//	Parameters :
//		MSG* pMsg	-	pointer to a MSG structure that contains the 
//						message to process.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CInPlaceList::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN || pMsg->wParam == VK_ESCAPE)
		{
			::TranslateMessage(pMsg);
			::DispatchMessage(pMsg);
			return TRUE;				// DO NOT process further
		}
	}
	
	return CComboBox::PreTranslateMessage(pMsg);
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInPlaceList::OnKillActive  
//  Description :   This routine is called before losing input foucs.
//					
//  Returns :
//
//  Parameters : 
//       CWnd* pNewWnd	-	pointer to the window that receives the 
//							input focus.	
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CInPlaceList::OnKillFocus(CWnd* pNewWnd) 
{
	CComboBox::OnKillFocus(pNewWnd);
	
	CString str;
	GetWindowText(str);

	// Send Notification to parent of ListView ctrl
	LV_DISPINFO dispinfo;
	dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
	dispinfo.hdr.idFrom = GetDlgCtrlID();
	dispinfo.hdr.code = LVN_ENDLABELEDIT;

	dispinfo.item.mask = LVIF_TEXT;
	dispinfo.item.iItem = m_iItem;
	dispinfo.item.iSubItem = m_iSubItem;
	dispinfo.item.pszText = m_bESC ? NULL : LPTSTR((LPCTSTR)str);
	dispinfo.item.cchTextMax = str.GetLength();

	GetParent()->GetParent()->SendMessage(WM_NOTIFY, GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo);

	PostMessage(WM_CLOSE);
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInPlaceList::OnChar  
//  Description :   This routine is called when a keystroke translates 
//					to a nonsystem character. 
//					
//  Returns :
//
//  Parameters : 
//		UINT nChar		-	Contains the character code value of the key. 
//		UINT nRepCnt	-	Contains the repeat count.
//		UINT nFlags		-	Contains the scan code, key-transition code, 
//							previous key state, and context code.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CInPlaceList::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	if (nChar == VK_ESCAPE || nChar == VK_RETURN)
	{
		if(nChar == VK_ESCAPE)
			m_bESC = TRUE;
		GetParent()->SetFocus();
		return;
	}
	
	CComboBox::OnChar(nChar, nRepCnt, nFlags);
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInPlaceList::OnNcDestroy  
//  Description :   This routine is called when the nonclient area is 
//					being destroyed, and is the last member function 
//					called when the Windows window is destroyed. 
//					
//  Returns :
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CInPlaceList::OnNcDestroy() 
{
	CComboBox::OnNcDestroy();
	delete this;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInPlaceList::OnCloseup  
//  Description :   This routine is called to set focus to the parent
//					control. 
//					
//  Returns :
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CInPlaceList::OnCloseup() 
{
	GetParent()->SetFocus();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInPlaceEdit::CInPlaceEdit
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//		int iItem			-	
//		int iSubItem		-
//		CString sInitText	-
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CInPlaceEdit::CInPlaceEdit(int iItem, int iSubItem, CString sInitText)
:m_sInitText(sInitText)
{
	m_iItem = iItem;
	m_iSubItem = iSubItem;
	m_bESC = FALSE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInPlaceEdit::~CInPlaceEdit
//	Description :	Destructor
//
//	Return :		Destructor -
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CInPlaceEdit::~CInPlaceEdit()
{
}

BEGIN_MESSAGE_MAP(CInPlaceEdit, CEdit)
	//{{AFX_MSG_MAP(CInPlaceEdit)
	ON_WM_KILLFOCUS()
	ON_WM_NCDESTROY()
	ON_WM_CHAR()
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInPlaceEdit::PreTranslateMessage
//	Description :	This routine performs accelerator-key translation.
//
//	Return :	BOOL	-	Zero if the message should be processed 
//							in the normal way.
//
//	Parameters :
//		MSG* pMsg	-	pointer to a MSG structure that contains the 
//						message to process.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CInPlaceEdit::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if ((pMsg->wParam == VK_RETURN) ||
			(pMsg->wParam == VK_DELETE) ||
			(pMsg->wParam == VK_ESCAPE) ||
			(GetKeyState(VK_CONTROL)))
		{
			::TranslateMessage(pMsg);
			::DispatchMessage(pMsg);
			return TRUE;		    	// DO NOT process further
		}
	}

	return CEdit::PreTranslateMessage(pMsg);
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInPlaceEdit::OnKillActive  
//  Description :   This routine is called before losing input foucs.
//					
//  Returns :
//
//  Parameters : 
//       CWnd* pNewWnd	-	pointer to the window that receives the 
//							input focus.	
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CInPlaceEdit::OnKillFocus(CWnd* pNewWnd)
{
	CEdit::OnKillFocus(pNewWnd);

	CString str;
	GetWindowText(str);

	// Send Notification to parent of ListView ctrl
	LV_DISPINFO dispinfo;
	dispinfo.hdr.hwndFrom = GetParent()->m_hWnd;
	dispinfo.hdr.idFrom = GetDlgCtrlID();
	dispinfo.hdr.code = LVN_ENDLABELEDIT;

	dispinfo.item.mask = LVIF_TEXT;
	dispinfo.item.iItem = m_iItem;
	dispinfo.item.iSubItem = m_iSubItem;
	dispinfo.item.pszText = m_bESC ? NULL : LPTSTR((LPCTSTR)str);
	dispinfo.item.cchTextMax = str.GetLength();

	GetParent()->GetParent()->SendMessage(WM_NOTIFY, GetParent()->GetDlgCtrlID(), (LPARAM)&dispinfo);

	DestroyWindow();
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInPlaceEdit::OnNcDestroy  
//  Description :   This routine is called when the nonclient area is 
//					being destroyed, and is the last member function 
//					called when the Windows window is destroyed. 
//					
//  Returns :
//
//  Parameters : 
//
//  Note :
//////////////////////////////////////////////////////////////////////////
**/
void CInPlaceEdit::OnNcDestroy()
{
	CEdit::OnNcDestroy();

	delete this;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInPlaceEdit::OnChar  
//  Description :   This routine is called when a keystroke translates 
//					to a nonsystem character. 
//					
//  Returns :
//
//  Parameters : 
//		UINT nChar		-	Contains the character code value of the key. 
//		UINT nRepCnt	-	Contains the repeat count.
//		UINT nFlags		-	Contains the scan code, key-transition code, 
//							previous key state, and context code.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CInPlaceEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if ((nChar == VK_ESCAPE) || (nChar == VK_RETURN))
	{
		if (nChar == VK_ESCAPE)
			m_bESC = TRUE;
		GetParent()->SetFocus();
		return;
	}

	CEdit::OnChar(nChar, nRepCnt, nFlags);

	// Resize edit control if needed
	// Get text extent
	CString str;

	GetWindowText( str );
	CWindowDC dc(this);
	CFont *pFont = GetParent()->GetFont();
	CFont *pFontDC = dc.SelectObject(pFont);
	CSize size = dc.GetTextExtent(str);
	dc.SelectObject(pFontDC);
	size.cx += 5;			   	// add some extra buffer

	// Get client rect
	CRect rect, parentrect;
	GetClientRect(&rect);
	GetParent()->GetClientRect(&parentrect);

	// Transform rect to parent coordinates
	ClientToScreen(&rect);
	GetParent()->ScreenToClient(&rect);

	// Check whether control needs to be resized
	// and whether there is space to grow
	if (size.cx > rect.Width())
	{
		if (size.cx + rect.left < parentrect.right)
			rect.right = rect.left + size.cx;
		else
			rect.right = parentrect.right;
		MoveWindow(&rect);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInPlaceList::OnCreate
//	Description :	This routine is called when an application 
//					requests that the Windows window be created.
//
//	Return :	int	-	0 to continue the creation of the CWnd object.
//
//	Parameters :
//		LPCREATESTRUCT lpCreateStruct -	pointer to a CREATESTRUCT 
//										structure that contains 
//										information about the CWnd 
//										object being created.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CInPlaceEdit::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CEdit::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Set the proper font
	CFont* font = GetParent()->GetFont();
	SetFont(font);

	SetWindowText(m_sInitText);
	SetFocus();
	SetSel(0, -1);
	return 0;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEditListCtrl::CEditListCtrl
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CEditListCtrl::CEditListCtrl()
{
	m_bReadOnly = FALSE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEditListCtrl::~CEditListCtrl
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CEditListCtrl::~CEditListCtrl()
{
}

BEGIN_MESSAGE_MAP(CEditListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CEditListCtrl)
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEditListCtrl::HitTestEx
//	Description :	This method determines which list view item, 
//					if any, is at a specified position.
//
//	Return :		int	-	the index of the item at the position 
//							specified.
//
//	Parameters :
//		CPoint &point	-	point to be tested.
//		int *col		-	pointer to an integer that receives 
//							information about the results of the test.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CEditListCtrl::HitTestEx(CPoint &point, int *col) const
{
	int colnum = 0;
	int row = HitTest(point, NULL);

	if (col) *col = 0;

	// Make sure that the ListView is in LVS_REPORT
	if ((GetWindowLong(m_hWnd, GWL_STYLE) & LVS_TYPEMASK) != LVS_REPORT)
		return row;

	// Get the top and bottom row visible
	row = GetTopIndex();
	int bottom = row + GetCountPerPage();
	if (bottom > GetItemCount())
		bottom = GetItemCount();

	// Get the number of columns
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColumnCount = pHeader->GetItemCount();

	// Loop through the visible rows
	for (;row <= bottom;row++)
	{
		// Get bounding rect of item and check whether point falls in it.
		CRect rect;
		GetItemRect(row, &rect, LVIR_BOUNDS);
		if (rect.PtInRect(point))
		{
			// Now find the column
			for (colnum = 0; colnum < nColumnCount; colnum++)
			{
				int colwidth = GetColumnWidth(colnum);
				if (point.x >= rect.left 
					&& point.x <= (rect.left + colwidth))
				{
					if (col) *col = colnum;
					return row;
				}
				rect.left += colwidth;
			}
		}
	}
	return -1;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEditListCtrl::EditSubLabel
//	Description :	
//
//	Return :		CEdit*	-	
//
//	Parameters :
//		int nItem	-	
//		int nCol	-	
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CEdit* CEditListCtrl::EditSubLabel( int nItem, int nCol )
{
	// The returned pointer should not be saved

	// Make sure that the item is visible
	if (!EnsureVisible(nItem, TRUE)) return NULL;

	// Make sure that nCol is valid
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColumnCount = pHeader->GetItemCount();
	if (nCol >= nColumnCount || GetColumnWidth(nCol) < 5)
		return NULL;

	// Get the column offset
	int offset = 0;
	for (int i = 0; i < nCol; i++)
		offset += GetColumnWidth( i );

	CRect rect;
	GetItemRect(nItem, &rect, LVIR_BOUNDS);

	// Now scroll if we need to expose the column
	CRect rcClient;
	GetClientRect(&rcClient);
	if (offset + rect.left < 0 || offset + rect.left > rcClient.right)
	{
		CSize size;
		size.cx = offset + rect.left;
		size.cy = 0;
		Scroll(size);
		rect.left -= size.cx;
	}

	// Get Column alignment
	LV_COLUMN lvcol;
	lvcol.mask = LVCF_FMT;
	GetColumn(nCol, &lvcol);
	DWORD dwStyle ;
	if ((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_LEFT)
		dwStyle = ES_LEFT;
	else if ((lvcol.fmt&LVCFMT_JUSTIFYMASK) == LVCFMT_RIGHT)
		dwStyle = ES_RIGHT;
	else dwStyle = ES_CENTER;

	rect.left += offset+4;
	rect.right = rect.left + GetColumnWidth( nCol ) - 3 ;
	if (rect.right > rcClient.right) rect.right = rcClient.right;

	dwStyle |= WS_BORDER|WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL;
	CEdit *pEdit = new CInPlaceEdit(nItem, nCol, GetItemText(nItem, nCol));
	pEdit->Create(dwStyle, rect, this, IDC_IPEDIT);

	return pEdit;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEditListCtrl::ShowInPlaceList
//	Description :	
//
//	Return :		CComboBox*	-	
//
//	Parameters :
//		int nItem				-	
//		int nCol				-
//		CStringList &lstItems	-
//		int nSel				-	
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CComboBox* CEditListCtrl::ShowInPlaceList( int nItem, int nCol, CStringList &lstItems, int nSel )
{
	// The returned pointer should not be saved

	// Make sure that the item is visible
	if (!EnsureVisible(nItem, TRUE)) return NULL;

	// Make sure that nCol is valid 
	CHeaderCtrl* pHeader = (CHeaderCtrl*)GetDlgItem(0);
	int nColumnCount = pHeader->GetItemCount();
	if ((nCol >= nColumnCount) || (GetColumnWidth(nCol) < 10) || (nCol==0)) 
		return NULL;

	// Get the column offset
	int offset = 0;
	for (int i = 0; i < nCol; i++)
		offset += GetColumnWidth(i);

	CRect rect;
	GetItemRect(nItem, &rect, LVIR_BOUNDS);

	// Now scroll if we need to expose the column
	CRect rcClient;
	GetClientRect(&rcClient);
	if (offset + rect.left < 0 || offset + rect.left > rcClient.right)
	{
		CSize size;
		size.cx = offset + rect.left;
		size.cy = 0;
		Scroll( size );
		rect.left -= size.cx;
	}

	rect.left += offset+4;
	rect.right = rect.left + GetColumnWidth(nCol) - 3 ;
	int height = rect.bottom-rect.top;
	rect.bottom += 5*height;
	if (rect.right > rcClient.right) rect.right = rcClient.right;

	DWORD dwStyle = WS_BORDER|WS_CHILD|WS_VISIBLE|WS_VSCROLL|CBS_DROPDOWNLIST|CBS_DISABLENOSCROLL;
	CComboBox *pList = new CInPlaceList(nItem, nCol, &lstItems, nSel);
	pList->Create(dwStyle, rect, this, IDC_IPEDIT);
	pList->SetItemHeight(-1, height);
	pList->SetHorizontalExtent(GetColumnWidth(nCol));

	return pList;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEditListCtrl::OnHScroll
//	Description :	This routine is called when the user clicks a 
//					window�s horizontal scroll bar.
//
//	Return :
//
//	Parameters :
//		UINT nSBCode			-	specifies a scroll-bar code that 
//									indicates the user�s scrolling 
//									request.	
//		UINT nPos				-	specifies the scroll-box position.
//		CScrollBar* pScrollBar	-	pointer to the scrollbar control.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CEditListCtrl::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (GetFocus() != this) SetFocus();
	CListCtrl::OnHScroll(nSBCode, nPos, pScrollBar);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEditListCtrl::OnVScroll
//	Description :	This routine is called when the user clicks a 
//					window�s horizontal scroll bar.
//
//	Return :
//
//	Parameters :
//		UINT nSBCode			-	specifies a scroll-bar code that 
//									indicates the user�s scrolling 
//									request.	
//		UINT nPos				-	specifies the scroll-box position.
//		CScrollBar* pScrollBar	-	pointer to the scrollbar control.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CEditListCtrl::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (GetFocus() != this) SetFocus();
	CListCtrl::OnVScroll(nSBCode, nPos, pScrollBar);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CEditListCtrl::OnEndLabelEdit
//	Description :	
//
//	Return :
//
//	Parameters :
//		NMHDR* pNMHDR		-	
//		LRESULT* pResult	-
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CEditListCtrl::OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO  *plvDispInfo = (LV_DISPINFO *)pNMHDR;
 	LV_ITEM		 *plvItem = &plvDispInfo->item;

	if (plvItem->pszText != NULL)
	{
		SetItemText(plvItem->iItem, plvItem->iSubItem, plvItem->pszText);
	}
	*pResult = FALSE;
}

//
//void CEditListCtrl::OnLButtonDown(UINT nFlags, CPoint point)
//{
//	int index;
//	CListCtrl::OnLButtonDown(nFlags, point);
//
//	int colnum;
//	if ((index = HitTestEx(point, &colnum)) != -1)
//	{
//		UINT flag = LVIS_FOCUSED;
//		if ((GetItemState(index, flag) & flag) == flag && colnum > 0)
//		{
//			// Add check for LVS_EDITLABELS
//			if (GetWindowLong(m_hWnd, GWL_STYLE) & LVS_EDITLABELS)
//			{
//				if (index == 2) // Only row two
//				{
//					CStringList lstItems;
//					lstItems.AddTail("None");
//					lstItems.AddTail("C:");
//					lstItems.AddTail("D:");
//					CString strItem = GetItemText(index, colnum);
//					int			nTopItem =-1;
//					bool		bFound = false;
//					POSITION	pos=lstItems.GetHeadPosition();
//					while ((pos!=NULL)&&(!bFound))
//					{
//						nTopItem++;
//						if (lstItems.GetAt(pos) == strItem)
//							bFound = true;
//						else
//							lstItems.GetNext(pos);
//					}
//					if (!bFound) nTopItem = 0;
//					ShowInPlaceList(index, colnum, lstItems, nTopItem);
//				}
//				else
//					EditSubLabel(index, colnum);
//			}
//		}
//		else
//			SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED); 
//	}
//}

