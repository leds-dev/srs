

class CInPlaceList : public CComboBox
{
public:
	CInPlaceList(int iItem, int iSubItem, CStringList *plstItems, int nSel);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInPlaceList)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL
public:
	virtual ~CInPlaceList();
protected:
	//{{AFX_MSG(CInPlaceList)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnNcDestroy();
	afx_msg void OnCloseup();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int			m_iItem;
	int			m_iSubItem;
	CStringList m_lstItems;
	int			m_nSel;
	BOOL		m_bESC;				// To indicate whether ESC key was pressed
};

/////////////////////////////////////////////////////////////////////////////
//
// CInPlaceEdit window
//
/////////////////////////////////////////////////////////////////////////////
class CInPlaceEdit : public CEdit
{
public:
	CInPlaceEdit(int iItem, int iSubItem, CString sInitText);
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInPlaceEdit)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL
public:
	virtual ~CInPlaceEdit();
protected:
	//{{AFX_MSG(CInPlaceEdit)
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnNcDestroy();
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int		m_iItem;
	int		m_iSubItem;
	CString	m_sInitText;
	BOOL	m_bESC;	 	// To indicate whether ESC key was pressed
};

/////////////////////////////////////////////////////////////////////////////
//
// CEditListCtrl control
//
/////////////////////////////////////////////////////////////////////////////
class CEditListCtrl : public CListCtrl
{
public:
	CEditListCtrl();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditListCtrl)
	//}}AFX_VIRTUAL
public:
	virtual ~CEditListCtrl();
	void OnEndLabelEdit(NMHDR* pNMHDR, LRESULT* pResult);
	void SetReadOnly(){m_bReadOnly = TRUE;};
protected:
	//{{AFX_MSG(CEditListCtrl)
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
//	afx_msg void OnLButtonDown(UINT nFlags, CPoint point); //Add this function
	//}}AFX_MSG
	int HitTestEx(CPoint &point, int *col) const;
	CEdit* EditSubLabel(int nItem, int nCol);
	CComboBox* ShowInPlaceList( int nItem, int nCol, CStringList &lstItems, int nSel);
	DECLARE_MESSAGE_MAP()
	BOOL m_bReadOnly;
};
