/***************************
//////////////////////////////////////////////////////////////////////
// // HeaderCtrlExt.cpp : implementation file.                       //
// (c) 20001 Frederick J. Shaw                                      //
// Prolog updated											        //
//////////////////////////////////////////////////////////////////////
***************************/

#include "stdafx.h"
#include "HeaderCtrlExt.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/************************
//////////////////////////////////////////////////////////////////////////
// CHeaderCtrlEx Class

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CHeaderCtrlEx::CHeaderCtrlEx()
//
//	Description :	Class constructor. 
//					
//	Return :		void -	none
//
//	Parameters :	void -	none
//					
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CHeaderCtrlEx::CHeaderCtrlEx(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CHeaderCtrlEx::~CHeaderCtrlEx()
//
//	Description :	Class destructor. 
//					
//	Return :		void -	none
//
//	Parameters :	void -	none
//
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CHeaderCtrlEx::~CHeaderCtrlEx(){}

BEGIN_MESSAGE_MAP(CHeaderCtrlEx, CHeaderCtrl)
	//{{AFX_MSG_MAP(CHeaderCtrlEx)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/************************
/////////////////////////////////////////////////////////////////////////////
// CHeaderCtrlEx message handlers
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		void CHeaderCtrlEx::DrawItem
//
//	Description :	Draws the header titile.. 
//					.
//					
//	Return :		void	-	none
//	Parameters :	
//					LPDRAWITEMSTRUCT lpDrawItemStruct -
//						Long pointer to a DRAWITEMSTRUCT object.  
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CHeaderCtrlEx::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
   ASSERT(lpDrawItemStruct->CtlType == ODT_HEADER);

   HDITEM hdi;
   TCHAR  lpBuffer[256];

   hdi.mask = HDI_TEXT;
   hdi.pszText = lpBuffer;
   hdi.cchTextMax = 256;

   GetItem(lpDrawItemStruct->itemID, &hdi);

   	
	CDC* pDC;
	pDC = CDC::FromHandle(lpDrawItemStruct->hDC);

	//THIS FONT IS ONLY FOR DRAWING AS LONG AS WE DON'T DO A SetFont(...)
	pDC->SelectObject(GetStockObject(DEFAULT_GUI_FONT));
   // Draw the button frame.
   ::DrawFrameControl(lpDrawItemStruct->hDC, 
      &lpDrawItemStruct->rcItem, DFC_BUTTON, DFCS_BUTTONPUSH);


	UINT uFormat = DT_CENTER;
	//DRAW THE TEXT
   ::DrawText(lpDrawItemStruct->hDC, lpBuffer, wcslen(lpBuffer), 
      &lpDrawItemStruct->rcItem, uFormat);

   pDC->SelectStockObject(SYSTEM_FONT);
}
