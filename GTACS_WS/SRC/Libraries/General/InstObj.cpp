/**
////////////////////////////////////////////////////////
// InstObj.cpp : implementation file                 //
// (c) 2001 Ray Mosley                                //
//	Prolog Updated                                    //
////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "Parse.h"
#include "InstObj.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/**
//////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////
//	Function :		Set151FromString
//					 	
//	Description :	Creates a message 151 object.
//                
//	Return :
//					void	none	
//	Parameters :	
//			MSG_151*		p151		-	Pointer to MSG_151 object
//			CStringArray*	pData		-	Pointer to CString Array
//			const int		nOATSSCID	-	OATS s/c id 
//			const BOOL		bIMCStatus	-	IMC status 
//			const CString*	pstrIMCSet	-	IMC set id
//			const int		nInstrument	-	Instrument id
//			const int		nIndex		-	message index
//               
//	Note :			
//			
//////////////////////////////////////////////////////////////////
**/
void Set151FromString (
	MSG_151*		p151,
	CStringArray*	pData,
	const int		nOATSSCID,
	const BOOL		bIMCStatus,
	const CString*	pstrIMCSet,
	const int		nInstrument,
	const int		nIndex)
{
	if (nIndex == 0)
	{
		p151->m_hdr.m_nSequence				= 0;
		p151->m_hdr.m_nSatelliteID			= (unsigned char)nOATSSCID;
		p151->m_hdr.m_nSource				= 10;
		p151->m_hdr.m_nDestination			= 32;
		p151->m_hdr.m_nNumHalfWords			= MSG_151_HEADER_HW_SIZE + MSG_151_DATA_HW_SIZE;
		p151->m_hdr.m_nMsgType				= 151;
		p151->m_hdr.m_nMsgSubType			= (unsigned char)nInstrument;
		strncpy_s (p151->m_hdr.m_cTracer, "         ", 10);
		p151->m_hdr.m_cTracer[9] = ' ';
		p151->m_hdr.m_nMsgCnt				= 1;
		p151->m_hdr.m_nMsgEndFlag			= 0xFF;
		p151->m_hdr.m_nSpare				= 0;
		p151->m_hdr.m_nRoutingID			= 0;
		p151->m_151Hdr.m_cIMCSetID[0]		= (char)pstrIMCSet->GetAt(0);
		p151->m_151Hdr.m_cIMCSetID[1]		= (char)pstrIMCSet->GetAt(1);
		p151->m_151Hdr.m_cIMCSetID[2]		= (char)pstrIMCSet->GetAt(2);
		p151->m_151Hdr.m_cIMCSetID[3]		= (char)pstrIMCSet->GetAt(3);
		p151->m_151Hdr.m_nNumWindows		= 1;
		p151->m_151Hdr.m_nSpare[0]			= 0;
		p151->m_151Hdr.m_nSpare[1]			= 0;
		p151->m_151Hdr.m_nSpare[2]			= 0;
		p151->m_151Hdr.m_nSpare[3]			= 0;
		p151->m_151Hdr.m_nSpare[4]			= 0;
		p151->m_151Hdr.m_nSpare[5]			= 0;
	}
	else
	{
		p151->m_hdr.m_nNumHalfWords			= p151->m_hdr.m_nNumHalfWords + MSG_151_DATA_HW_SIZE;
		p151->m_151Hdr.m_nNumWindows		= p151->m_151Hdr.m_nNumWindows + 1;
	}

	p151->m_151Data[nIndex].m_nSequenceNum	= nIndex;
	unsigned __int32 timeBCD[2];
	ConvertStringTimeToBCD (pData->GetAt(0), timeBCD);
	p151->m_151Data[nIndex].m_nBCDTime[0]	= timeBCD[0];
	p151->m_151Data[nIndex].m_nBCDTime[1]	= timeBCD[1];

	int		nSize	= pData->GetAt(1).GetLength();
	char*	strflt	= new char[nSize+1];
	for(int i=0; i<nSize; i++)
		strflt[i] = (char)pData->GetAt(1)[i];
	strflt[nSize]	= NULL;
	float tmpflt	= (float)atof(strflt);
	p151->m_151Data[nIndex].m_fDuration	= tmpflt;
	delete strflt;
	strflt = NULL;
}

/**
/////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
/////////////////////////////////////////////////////////////////////
//	Function :		Set155FromString
//					 	
//	Description :	Creates a 155 message object.
//                
//	Return :
//					void	none	
//	Parameters :	
//					MSG_155*		p155	-
//										Pointer to MSG_155 object.
//					CStringArray*	pData	- 
//										Pointer to a CString array
//					const int		nOATSSCID -	OATS s/c id
//					const int		nRepeatCnt - OATS Repeat count
//					const BOOL		bIMCStatus	- The IMC status 
//					const CString*	pstrIMCSet	- IMC set name
//					const int		nInstrument	- Instrument id
//					const int		nIndex	-	Message index
//               
//	Note :			
//			
//////////////////////////////////////////////////////////////////
**/
void Set155FromString (
	MSG_155*		p155,
	CStringArray*	pData,
	const int		nOATSSCID,
	const int		nRepeatCnt,
	const BOOL		bIMCStatus,
	const CString*	pstrIMCSet,
	const int		nInstrument,
	const int		nIndex)
{
	int		nSearchIndex= 0;
	int		nMaxIndex	= 0;
	bool	bFound		= false;

	if (nIndex == 0)
	{
		p155->m_hdr.m_nSequence					= 0;
		p155->m_hdr.m_nSatelliteID				= (unsigned char)nOATSSCID;
		p155->m_hdr.m_nSource					= 10;
		p155->m_hdr.m_nDestination				= 32;
		p155->m_hdr.m_nNumHalfWords				= MSG_155_HEADER_HW_SIZE + MSG_155_DATA_HW_SIZE;
		p155->m_hdr.m_nMsgType					= 155;
		p155->m_hdr.m_nMsgSubType				= (unsigned char)nInstrument;
		strncpy_s (p155->m_hdr.m_cTracer, "         ", 10);
		p155->m_hdr.m_cTracer[9] = ' ';
		p155->m_hdr.m_nMsgCnt					= 1;
		p155->m_hdr.m_nMsgEndFlag				= 0xFF;
		p155->m_hdr.m_nSpare					= 0;
		p155->m_hdr.m_nRoutingID				= 0;
		p155->m_155Hdr.m_cIMCSetID[0]			= (char)pstrIMCSet->GetAt(0);
		p155->m_155Hdr.m_cIMCSetID[1]			= (char)pstrIMCSet->GetAt(1);
		p155->m_155Hdr.m_cIMCSetID[2]			= (char)pstrIMCSet->GetAt(2);
		p155->m_155Hdr.m_cIMCSetID[3]			= (char)pstrIMCSet->GetAt(3);
		p155->m_155Hdr.m_nNumFrames				= 1;
		p155->m_155Hdr.m_nSpare					= 0;
		p155->m_155Hdr.m_nIMCStatus				= bIMCStatus ? 0 : 1;
	}
	else
	{
		p155->m_hdr.m_nNumHalfWords				= p155->m_hdr.m_nNumHalfWords + MSG_155_DATA_HW_SIZE;
		p155->m_155Hdr.m_nNumFrames				= p155->m_155Hdr.m_nNumFrames + 1;
	}

	unsigned __int32 timeBCD[2];
	ConvertStringTimeToBCD (pData->GetAt(eFrameTime),timeBCD);
	p155->m_155Data[nIndex].m_nBCDTime[0]		= timeBCD[0];
	p155->m_155Data[nIndex].m_nBCDTime[1]		= timeBCD[1];
	nMaxIndex									= nMaxCoordType;
	nSearchIndex								= 0;
	p155->m_155Data[nIndex].m_nCoordinateType	= 1;
	CString strFrameCoordType					= pData->GetAt(eFrameCoordType);

	while ((!bFound) && (nSearchIndex < nMaxIndex))
	{
		if (pData->GetAt(eFrameCoordType).CompareNoCase(strCoordType[nSearchIndex]) == 0)
		{
			p155->m_155Data[nIndex].m_nCoordinateType = nSearchIndex;
			bFound = true;
		}
		else
			nSearchIndex++;
	}

	nMaxIndex									= nMaxSpacelookSide;
	bFound										= false;
	nSearchIndex								= 0;
	p155->m_155Data[nIndex].m_nSpacelookSide	= 0;

	while ((!bFound) && (nSearchIndex < nMaxIndex))
	{
		if (pData->GetAt(eFrameSpacelookSide).CompareNoCase(strSpacelookSide[nSearchIndex]) == 0)
		{
			p155->m_155Data[nIndex].m_nSpacelookSide = nSearchIndex;
			bFound = true;
		}
		else
			nSearchIndex++;
	}

	nMaxIndex									= nMaxSpacelookMode;
	bFound										= false;
	nSearchIndex								= 0;
	p155->m_155Data[nIndex].m_nSpacelookMode	= 0;

	while ((!bFound) && (nSearchIndex < nMaxIndex))
	{
		if (pData->GetAt(eFrameSpacelookMode).CompareNoCase(strSpacelookMode[nSearchIndex]) == 0)
		{
			p155->m_155Data[nIndex].m_nSpacelookMode = nSearchIndex;
			bFound = true;
		}
		else
			nSearchIndex++;
	}

	p155->m_155Data[nIndex].m_nDefaultFrameFlag	= 0;
	nMaxIndex									= nMaxSndrStepMode;
	bFound										= false;
	nSearchIndex								= 0;
	p155->m_155Data[nIndex].m_nSounderStepMode	= 0;

	while ((!bFound) && (nSearchIndex < nMaxIndex))
	{
		if (pData->GetAt(eFrameSndrStepMode).CompareNoCase(strSndrStepMode[nSearchIndex]) == 0)
		{
			p155->m_155Data[nIndex].m_nSounderStepMode = nSearchIndex;
			bFound = true;
		}
		else
			nSearchIndex++;
	}

	if (p155->m_155Data[nIndex].m_nSounderStepMode == 4)
		p155->m_155Data[nIndex].m_nSounderStepMode = -1;

	int		nSize	= pData->GetAt(eFrameStartNS).GetLength();
	char*	strflt	= new char[nSize+1];
	for(int i=0; i<nSize; i++)
		strflt[i] = (char)pData->GetAt(eFrameStartNS)[i];
	strflt[nSize]	= NULL;
	float tmpflt	= (float)atof(strflt);
	p155->m_155Data[nIndex].m_fStartNS			= tmpflt;
	delete strflt;
	strflt = NULL;
	
	nSize			= pData->GetAt(eFrameStartEW).GetLength();
	strflt			= new char[nSize+1];
	for(int i=0; i<nSize; i++)
		strflt[i] = (char)pData->GetAt(eFrameStartEW)[i];
	strflt[nSize]	= NULL;
	tmpflt			= (float)atof(strflt);
	p155->m_155Data[nIndex].m_fStartEW			= tmpflt;
	delete strflt;
	strflt = NULL;
	
	nSize			= pData->GetAt(eFrameStopNS).GetLength();
	strflt			= new char[nSize+1];
	for(int i=0; i<nSize; i++)
		strflt[i] = (char)pData->GetAt(eFrameStopNS)[i];
	strflt[nSize]	= NULL;
	tmpflt			= (float)atof(strflt);
	p155->m_155Data[nIndex].m_fStopNS			= tmpflt;
	delete strflt;
	strflt = NULL;
	
	nSize			= pData->GetAt(eFrameStopEW).GetLength();
	strflt			= new char[nSize+1];
	for(int i=0; i<nSize; i++)
		strflt[i] = (char)pData->GetAt(eFrameStopEW)[i];
	strflt[nSize]	= NULL;
	tmpflt			= (float)atof(strflt);
	p155->m_155Data[nIndex].m_fStopEW			= tmpflt;
	delete strflt;
	strflt = NULL;
	
	p155->m_155Data[nIndex].m_nRepeatCount		= nRepeatCnt;
}

/**
/////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////
//	Function :		SetListFrom55
//					 	
//	Description :	Creates a list object from a message 55 object.
//                
//	Return :
//					void	none	
//	Parameters :	
//				const MSG_55* p55	-	Pointer to MSG_55 object.
//				CListCtrl* pListCtrl -	Pointer to List Control Object.
//				const CWordArray* pnListIndex	-
//					Pointer to a Word Array
//				const int nIndex	-	Message index 
//               
//	Note :			
//			
//////////////////////////////////////////////////////////////////
**/
void SetListFrom55 (
	const MSG_55* p55,
	CListCtrl* pListCtrl,
	const CWordArray* pnListIndex,
	const int nIndex)
{
	CString strStartNSCyc; strStartNSCyc.Format (_T("%d"), p55->m_55Data[nIndex].m_nStartNSCyc);
	CString strStartNSInc; strStartNSInc.Format (_T("%d"), p55->m_55Data[nIndex].m_nStartNSInc);
	CString strStartEWCyc; strStartEWCyc.Format (_T("%d"), p55->m_55Data[nIndex].m_nStartEWCyc);
	CString strStartEWInc; strStartEWInc.Format (_T("%d"), p55->m_55Data[nIndex].m_nStartEWInc);
	CString strStopNSCyc;  strStopNSCyc.Format  (_T("%d"), p55->m_55Data[nIndex].m_nStopNSCyc);
	CString strStopNSInc;  strStopNSInc.Format  (_T("%d"), p55->m_55Data[nIndex].m_nStopNSInc);
	CString strStopEWCyc;  strStopEWCyc.Format  (_T("%d"), p55->m_55Data[nIndex].m_nStopEWCyc);
	CString strStopEWInc;  strStopEWInc.Format  (_T("%d"), p55->m_55Data[nIndex].m_nStopEWInc);
	CString strDuration;   strDuration.Format   (_T("%f"), p55->m_55Data[nIndex].m_fDuration);
	pListCtrl->SetItemText(pnListIndex->GetAt(nIndex), eFrameStartCycNS,  strStartNSCyc);
	pListCtrl->SetItemText(pnListIndex->GetAt(nIndex), eFrameStartIncrNS, strStartNSInc);
	pListCtrl->SetItemText(pnListIndex->GetAt(nIndex), eFrameStartCycEW,  strStartEWCyc);
	pListCtrl->SetItemText(pnListIndex->GetAt(nIndex), eFrameStartIncrEW, strStartEWInc);
	pListCtrl->SetItemText(pnListIndex->GetAt(nIndex), eFrameStopCycNS,   strStopNSCyc);
	pListCtrl->SetItemText(pnListIndex->GetAt(nIndex), eFrameStopIncrNS,  strStopNSInc);
	pListCtrl->SetItemText(pnListIndex->GetAt(nIndex), eFrameStopCycEW,   strStopEWCyc);
	pListCtrl->SetItemText(pnListIndex->GetAt(nIndex), eFrameStopIncrEW,  strStopEWInc);
	pListCtrl->SetItemText(pnListIndex->GetAt(nIndex), eFrameDur,         strDuration);
}

/**
/////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////
//	Function :		ConvertStringTimeToBCD 
//					 	
//	Description :	Converts a string time into a BCD object.
//                
//	Return :
//					void	none	
//	Parameters :	
//				 const CString strTime	-	String time
//				 unsigned __int32* pTimeBCD	- pointer to BCD time object.
//               
//	Note :			
//			
//////////////////////////////////////////////////////////////////
**/
void ConvertStringTimeToBCD (const CString strTime, unsigned __int32* pTimeBCD)
{
	//strTime must be in the format of yyyy/ddd/hh:mm:ss.ttt
	if (!strTime.IsEmpty())
	{
		pTimeBCD[0] = MAKELONG (MAKEWORD (((strTime[ 1] - 48) | ((strTime[ 0] - 48) << 4)),
										  ((strTime[ 3] - 48) | ((strTime[ 2] - 48) << 4))),
								MAKEWORD (((strTime[ 6] - 48) | ((strTime[ 5] - 48) << 4)),
										  ((strTime[ 9] - 48) | ((strTime[ 7] - 48) << 4))));
		pTimeBCD[1] = MAKELONG (MAKEWORD (((strTime[12] - 48) | ((strTime[10] - 48) << 4)),
										  ((strTime[15] - 48) | ((strTime[13] - 48) << 4))),
								MAKEWORD (((strTime[18] - 48) | ((strTime[16] - 48) << 4)),
										  ((strTime[20] - 48) | ((strTime[19] - 48) << 4))));
	}
	else
	{
		pTimeBCD[0] = 0;
		pTimeBCD[1] = 0;
	}
}

/**
/////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 06-04-01			version 1.0
////////////////////////////////////////////////////////////////////////
//	Function :		ConvertBCDToStringTime 
//					 	
//	Description :	Converts a BCD time into a string time.
//                
//	Return :
//					void	none	
//	Parameters :	
//				 const unsigned __int32 timeBCD[] -	BCD time.
//				 CString* pstrTime - Pointer to a string time. 
//               
//	Note :			
//			
//////////////////////////////////////////////////////////////////
**/
void ConvertBCDToStringTime (const unsigned __int32 timeBCD[], CString* pstrTime)
{
	*pstrTime = "---------------------";
	pstrTime->SetAt(0,  (LOBYTE(LOWORD(timeBCD[0])) >> 4) + 48);
	pstrTime->SetAt(1,  (LOBYTE(LOWORD(timeBCD[0])) & 0x0F) + 48);
	pstrTime->SetAt(2,  (HIBYTE(LOWORD(timeBCD[0])) >> 4) + 48);
	pstrTime->SetAt(3,  (HIBYTE(LOWORD(timeBCD[0])) & 0x0F) + 48);
	pstrTime->SetAt(4,  '-');
	pstrTime->SetAt(5,  (LOBYTE(HIWORD(timeBCD[0])) >> 4) + 48);
	pstrTime->SetAt(6,  (LOBYTE(HIWORD(timeBCD[0])) & 0x0F) + 48);
	pstrTime->SetAt(7,  (HIBYTE(HIWORD(timeBCD[0])) >> 4) + 48);
	pstrTime->SetAt(8,  '-');
	pstrTime->SetAt(9,  (HIBYTE(HIWORD(timeBCD[0])) & 0x0F) + 48);
	pstrTime->SetAt(10, (LOBYTE(LOWORD(timeBCD[1])) >> 4) + 48);
	pstrTime->SetAt(11, ':');
	pstrTime->SetAt(12,  (LOBYTE(LOWORD(timeBCD[1])) & 0x0F) + 48);
	pstrTime->SetAt(13,  (HIBYTE(LOWORD(timeBCD[1])) >> 4) + 48);
	pstrTime->SetAt(14, ':');
	pstrTime->SetAt(15,  (HIBYTE(LOWORD(timeBCD[1])) & 0x0F) + 48);
	pstrTime->SetAt(16,  (LOBYTE(HIWORD(timeBCD[1])) >> 4) + 48);
	pstrTime->SetAt(17, '.');
	pstrTime->SetAt(18,  (LOBYTE(HIWORD(timeBCD[1])) & 0x0F) + 48);
	pstrTime->SetAt(19,  (HIBYTE(HIWORD(timeBCD[1])) >> 4) + 48);
	pstrTime->SetAt(20,  (HIBYTE(HIWORD(timeBCD[1])) & 0x0F) + 48);
}
