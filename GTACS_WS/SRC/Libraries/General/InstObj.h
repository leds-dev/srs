#if !defined(AFX_INSTOBJ_H__2644490B_90FF_11D2_98EB_00104B79D0AC__INCLUDED_)
#define AFX_INSTOBJ_H__2644490B_90FF_11D2_98EB_00104B79D0AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "50402Msg.h"
void Set151FromString (MSG_151* p151, CStringArray* pData, const int nOATSSCID,
					   const BOOL bIMCStatus, const CString* pstrIMCSet, const int nInstrument, const int nIndex);
void Set155FromString (MSG_155* p155, CStringArray* pData, const int nOATSSCID, const int nRepeatCnt,
					   const BOOL nIMCStatus, const CString* pstrIMCSet, const int nInstrument, const int nIndex);
void ConvertStringTimeToBCD (const CString strTime, unsigned __int32* pTimeBCD);
void ConvertBCDToStringTime (const unsigned __int32 timeBCD[], CString* pstrTime);

void SetListFrom55 (const MSG_55* p55, CListCtrl* pListCtrl, const CWordArray* pnListIndex, const int nIndex);

#endif // !defined(AFX_INSTOBJ_H__2644490B_90FF_11D2_98EB_00104B79D0AC__INCLUDED_)
