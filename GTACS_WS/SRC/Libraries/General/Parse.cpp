/***************************
//////////////////////////////////////////////////////////////////////
// mEdit.cpp : implementation file
//
// (c) 2001 Frederick J. Shaw                                       // 
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
***************************/

#include "StdAfx.h"
#include "Parse.h"
#include "SchedMisc.h"
#include "MapView.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/***************************
////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
////////////////////////////////////////////////////////////////////
//	Function :		CParse::CParse
//	Description :	Constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
///////////////////////////////////////////////////////////////////
***************************/
CParse::CParse()
{
	m_strHeaderLine1.Empty();
	m_strHeaderLine2.Empty();
	m_strData.Empty();
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParse::~CParse
//	Description :	Destructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParse::~CParse(){}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParse::RemoveFirstToken
//	Description :	This routine removes the first token from the string.
//					Tokens are seperated by commas.  Once removed, the
//					token is trimmed of any leading and trailing white
//					space.  An empty string is returned if there are no
//					token in the input string
//	Return :		
//		CString		-	
//	Parameters :	
//		CString *pstrInput	-	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParse::RemoveFirstToken (CString *pstrInput)
{
	CString strToken;
	int nPos = pstrInput->Find(_T(','));
	if (nPos == -1)
	{
		strToken = *pstrInput;
		pstrInput->Empty();
	}
	else
	{
		strToken = pstrInput->Left(nPos);
		strToken.Remove(_T('"'));
		*pstrInput = pstrInput->Right(pstrInput->GetLength() - nPos - 1);
	}
	strToken.TrimLeft();
	strToken.TrimRight();
	return strToken;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParse::RemoveFirstToken
//	Description :	This routine removes the first token from the string.
//					Tokens are seperated by commas.  Once removed, the
//					token is trimmed of any leading and trailing white
//					space.  An empty string is returned if there are no
//					token in the input string
//	Return :		
//		CString		-	
//	Parameters :	
//		CString *pstrInput	- string to be parsed.
//      TCHAR   ch          - delimiter character	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParse::RemoveFirstToken (CString *pstrInput, TCHAR ch)
{
	CString strToken;
	int nPos = pstrInput->Find(ch);
	if (nPos == -1)
	{
		strToken = *pstrInput;
		pstrInput->Empty();
	}
	else
	{
		strToken = pstrInput->Left(nPos);
		strToken.Remove(_T('"'));
		*pstrInput = pstrInput->Right(pstrInput->GetLength() - nPos - 1);
		pstrInput->TrimLeft();
	}
	strToken.TrimLeft();
	strToken.TrimRight();
	return strToken;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParse::SetHeader
//	Description :	This functions initializes the two header strings.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strHeaderLine1	-	The ID record
//		CString strHeaderLine2	-	The header record	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CParse::SetHdr(CString strHeaderLine1, CString strHeaderLine2)
{
	m_strHeaderLine1 = strHeaderLine1;
	m_strHeaderLine2 = strHeaderLine2;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParse::SetData
//	Description :	This routine initializes the data record.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strData	-	Data record
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CParse::SetData(CString strData)
{
	m_strData = strData;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParse::CvtTimeTtoStr
//	Description :	This routine converts a time_t value into a CString
//	Return :		
//		CString		-	Formatted time string	
//	Parameters :	
//		time_t tTime	-	time_t to convert
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParse::CvtTimeTtoStr(time_t tTime)
{
	CString strTemp;
	if (tTime == 0)
		strTemp = _T("Not Modified");
	else
	{
		CTime		cTime(tTime);
		SYSTEMTIME	cSystemTime;
		cTime.GetAsSystemTime(cSystemTime);
		strTemp	= CSchedMisc::ConvertSTtoString(cSystemTime);
	}
	return strTemp;
}

/***************************
///////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
///////////////////////////////////////////////////////////////////
//	Function :		CParse::GetInstrumentIndex
//	Description :	This routine returns an integer index that 
//					corresponds to the Instrument string passed in
//	Return :		
//		int		-	Corresponding integer index
//
//	Parameters :	
//		CString strTestValue	-	Instrument to look up
//
//	Note :			
////////////////////////////////////////////////////////////////////////
***************************/
int CParse::GetInstrumentIndex(CString strTestValue)
{
	if (strTestValue.IsEmpty())
		return -1;

	BOOL bFound = FALSE;
	int nIndex=0;
	while ((!bFound) && (nIndex < nMaxInstrument))
	{
		if (strTestValue.CompareNoCase(strInstrument[nIndex]) == 0)
			bFound = TRUE;
		else
			nIndex++;
	}

	return bFound ? nIndex : -1;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParse::GetSpacelookModeIndex
//	Description :	This routine returns an integer index that corresponds
//					to the Spacelook Mode string passed in
//	Return :		
//		int		-	Corresponding integer index
//	Parameters :	
//		CString strTestValue	-	Spacelook Mode to look up
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
int CParse::GetSpacelookModeIndex(CString strTestValue)
{
	if (strTestValue.IsEmpty())
		return -1;

	BOOL bFound = FALSE;
	int nIndex=0;
	while ((!bFound) && (nIndex < nMaxSpacelookMode))
	{
		if (strTestValue.CompareNoCase(strSpacelookMode[nIndex]) == 0)
			bFound = TRUE;
		else
			nIndex++;
	}

	return bFound ? nIndex : -1;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParse::GetSpacelookSideIndex
//	Description :	This routine returns an integer index that corresponds
//					to the Spacelook Side string passed in
//	Return :		
//		int		-	Corresponding integer index
//	Parameters :	
//		CString strTestValue	-	Spacelook Side to look up
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
int CParse::GetSpacelookSideIndex(CString strTestValue)
{
	if (strTestValue.IsEmpty())
		return -1;

	BOOL bFound = FALSE;
	int nIndex=0;
	while ((!bFound) && (nIndex < nMaxSpacelookSide))
	{
		if (strTestValue.CompareNoCase(strSpacelookSide[nIndex]) == 0)
			bFound = TRUE;
		else
			nIndex++;
	}

	return bFound ? nIndex : -1;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParse::GetSndrStepModeIndex
//	Description :	This routine returns an integer index that corresponds
//					to the Sounder Step Mode string passed in
//	Return :		
//		int		-	Corresponding integer index
//	Parameters :	
//		CString strTestValue	-	Sounder Step Mode to look up
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
int CParse::GetSndrStepModeIndex(CString strTestValue)
{
	if (strTestValue.IsEmpty())
		return -1;

	BOOL bFound = FALSE;
	int nIndex=0;
	while ((!bFound) && (nIndex < nMaxSndrStepMode))
	{
		if (strTestValue.CompareNoCase(strSndrStepMode[nIndex]) == 0)
			bFound = TRUE;
		else
			nIndex++;
	}

	return bFound ? nIndex : -1;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParse::GetCoordTypeIndex
//	Description :	This routine returns an integer index that corresponds
//					to the Coordinate Type string passed in
//	Return :		
//		int		-	Corresponding integer index
//	Parameters :	
//		CString strTestValue	-	Coordinate Type to look up
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
int CParse::GetCoordTypeIndex(CString strTestValue)
{
	if (strTestValue.IsEmpty())
		return -1;

	BOOL bFound = FALSE;
	int nIndex=0;
	while ((!bFound) && (nIndex < nMaxCoordType))
	{
		if (strTestValue.CompareNoCase(strCoordType[nIndex]) == 0)
			bFound = TRUE;
		else
			nIndex++;
	}

	return bFound ? nIndex : -1;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::CParseFrame
//	Description :	Class constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseFrame::CParseFrame()
{
	m_fVersion = -1.0;
	m_nNumObj = -1;
	m_nSRSOFirst = -1;
	m_tModTime = -1;
	m_strModUser.Empty();
	m_tUpdTime = -1;
	m_strUpdUser.Empty();

	m_nIndex = -1;
	m_strTime.Empty();
	m_strLabel.Empty();
	m_nInstrument = -1;
	m_nSpacelookMode = -1;
	m_nSpacelookSide = -1;
	m_nSndrStepMode = -1;
	m_nCoordType = -1;
	m_fStartNS = -1.0;
	m_fStartEW = -1.0;
	m_fStopNS = -1.0;
	m_fStopEW = -1.0;
	m_nStartCycNS = -1;
	m_nStartIncNS = -1;
	m_nStartCycEW = -1;
	m_nStartIncEW = -1;
	m_nStopCycNS = -1;
	m_nStopIncNS = -1;
	m_nStopCycEW = -1;
	m_nStopIncEW = -1;
	m_fDuration = -1.0;
	m_strDesc.Empty();
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::~CParseFrame
//	Description :	Class destructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseFrame::~CParseFrame(){}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::ParseHdr
//	Description :	This routine parses the two header strings previously
//					set and updates the object's member variables as
//					appripriate.  
//	Return :		
//		BOOL		-	TRUE if the header strings contained valid data,
//						otherwise FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CParseFrame::ParseHdr()
{
	BOOL bRtnStatus = FALSE;
	CString strLine = m_strHeaderLine1;

	_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fVersion);
	m_strType = RemoveFirstToken(&strLine);
	strLine = m_strHeaderLine2;
	m_nNumObj = _ttoi(RemoveFirstToken(&strLine));
	m_nSRSOFirst = _ttoi(RemoveFirstToken(&strLine));
	m_tModTime = _ttoi(RemoveFirstToken(&strLine));
	m_strModUser = RemoveFirstToken(&strLine);
	m_tUpdTime = _ttoi(RemoveFirstToken(&strLine));
	m_strUpdUser = RemoveFirstToken(&strLine);

	if ((m_strType.CompareNoCase(_T("Frame")) == 0) &&
		(m_fVersion == 1.0) &&
		(m_nNumObj != -1) &&
		(m_nSRSOFirst != -1) &&
		(m_tModTime != -1) &&
		(!m_strModUser.IsEmpty()) &&
		(m_tUpdTime != -1) &&
		(!m_strUpdUser.IsEmpty()))
		bRtnStatus = TRUE;

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::GetHdrRec1
//	Description :	This routine will format the member variables needed
//					for the first header record into a CString.
//	Return :		
//		CString		-	Header record 1
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseFrame::GetHdrRec1()
{
	CString strHdr;
	strHdr.Format(_T("%.1f, Frame"), m_fVersion);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::GetHdrRec2
//	Description :	This routine will format the member variables needed
//					for the second header record into a CString.
//	Return :		
//		CString		-	Header record 2
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseFrame::GetHdrRec2()
{
	CString strHdr;
	strHdr.Format(_T("%d, %d, %d, %s, %d, %s"),
		m_nNumObj, m_nSRSOFirst, (int)m_tModTime, m_strModUser, (int)m_tUpdTime, m_strUpdUser);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::GetHdrVersion
//	Function :		CParseFrame::GetHdrNumObj
//	Function :		CParseFrame::GetHdrSRSOFirst
//	Function :		CParseFrame::GetHdrModTimeFmt
//	Function :		CParseFrame::GetHdrModTime
//	Function :		CParseFrame::GetHdrModUser
//	Function :		CParseFrame::GetHdrUpdTimeFmt
//	Function :		CParseFrame::GetHdrUpdTime
//	Function :		CParseFrame::GetHdrUpdUser
//	Description :	These routines are use to get a string version of the
//					various member variables.  To get the member variables
//					in their native format, just access the variables
//					themselves (they are public).
//	Return :		
//		CString		-	Member variable formatted as a CString.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseFrame::GetHdrVersion(){CString s; s.Format(_T("%.3f"), m_fVersion); return s;}
CString CParseFrame::GetHdrNumObj(){CString s; s.Format(_T("%d"), m_nNumObj); return s;}
CString CParseFrame::GetHdrSRSOFirst(){CString s; s.Format(_T("%d"), m_nSRSOFirst); return s;}
CString	CParseFrame::GetHdrModTimeFmt(){return CvtTimeTtoStr(m_tModTime);}
CString CParseFrame::GetHdrModTime(){CString s; s.Format(_T("%d"), m_tModTime); return s;}
CString CParseFrame::GetHdrModUser(){return m_strModUser;}
CString	CParseFrame::GetHdrUpdTimeFmt(){return CvtTimeTtoStr(m_tUpdTime);}
CString CParseFrame::GetHdrUpdTime(){CString s; s.Format(_T("%d"), m_tUpdTime); return s;}
CString CParseFrame::GetHdrUpdUser(){return m_strUpdUser;}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::SetHdrVersion
//	Function :		CParseFrame::SetHdrNumObj
//	Function :		CParseFrame::SetHdrSRSOFirst
//	Function :		CParseFrame::SetHdrModTime
//	Function :		CParseFrame::SetHdrModUser
//	Function :		CParseFrame::SetHdrUpdTime
//	Function :		CParseFrame::SetHdrUpdUser
//	Description :	This routine sets member variable specified.  It 
//					converts the CString input into the variables native
//					format.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strValue	-	Value to convert and set.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CParseFrame::SetHdrVersion(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fVersion);}
void CParseFrame::SetHdrNumObj(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nNumObj);}
void CParseFrame::SetHdrSRSOFirst(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nSRSOFirst);}
void CParseFrame::SetHdrModTime(CString strValue){_stscanf_s (strValue, _T("%d"), &m_tModTime);}
void CParseFrame::SetHdrModUser(CString strValue){m_strModUser = strValue;}
void CParseFrame::SetHdrUpdTime(CString strValue){_stscanf_s (strValue, _T("%d"), &m_tUpdTime);}
void CParseFrame::SetHdrUpdUser(CString strValue){m_strUpdUser = strValue;}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		BOOL	CParseFrame::ParseData
//	Description :	This routine will parse the intput data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CSTring parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseFrame::ParseData()
{
	BOOL bRtnStatus = TRUE;

	CString strLine;
	if (bRtnStatus)
	{
		strLine = m_strData;
		if (strLine.Replace(',', ' ') != 21)
			bRtnStatus = FALSE;
	}

	if (bRtnStatus)
	{
		strLine = m_strData;
		m_nIndex = _ttoi(RemoveFirstToken(&strLine));
		m_strTime = RemoveFirstToken(&strLine);
		m_strLabel = RemoveFirstToken(&strLine);
		m_nInstrument = GetInstrumentIndex(RemoveFirstToken(&strLine));
		m_nSpacelookMode = GetSpacelookModeIndex(RemoveFirstToken(&strLine));
		m_nSpacelookSide = GetSpacelookSideIndex(RemoveFirstToken(&strLine));
		m_nSndrStepMode = GetSndrStepModeIndex(RemoveFirstToken(&strLine));
		m_nCoordType = GetCoordTypeIndex(RemoveFirstToken(&strLine));
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fStartNS);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fStartEW);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fStopNS);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fStopEW);
		m_nStartCycNS = _ttoi(RemoveFirstToken(&strLine));
		m_nStartIncNS = _ttoi(RemoveFirstToken(&strLine));
		m_nStartCycEW = _ttoi(RemoveFirstToken(&strLine));
		m_nStartIncEW = _ttoi(RemoveFirstToken(&strLine));
		m_nStopCycNS = _ttoi(RemoveFirstToken(&strLine));
		m_nStopIncNS = _ttoi(RemoveFirstToken(&strLine));
		m_nStopCycEW = _ttoi(RemoveFirstToken(&strLine));
		m_nStopIncEW = _ttoi(RemoveFirstToken(&strLine));
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fDuration);
		m_strDesc = RemoveFirstToken(&strLine);
		bRtnStatus = TRUE;  //Assume it was good
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::HasData
//	Description :	This routine will return TRUE if, after calling
//					ParseData, the String actually contained valid data.
//					To determine this, all we do is check to see if a
//					valid instrument.
//	Return :		
//		BOOL		-	TRUE if valid, otherwise FALSE
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CParseFrame::HasData()
{
	return ((m_nInstrument != -1) ? TRUE : FALSE);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CString	CParseFrame::GetData
//	Description :	This routine returns a CString that contains all of
//					object's data.
//	Return :		
//		CString		-	CSTring that contains the data.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseFrame::GetData()
{
	CString strTemp;
	strTemp.Format(_T("%d, %s, %s, %s, %s, %s, %s, %s, %.3f, %.3f, %.3f, %.3f, , , , , , , , , , %s"),
					 m_nIndex, m_strTime, m_strLabel, strInstrument[m_nInstrument], 
					 strSpacelookMode[m_nSpacelookMode], strSpacelookSide[m_nSpacelookSide],
					 strSndrStepMode[m_nSndrStepMode], strCoordType[m_nCoordType], 
					 m_fStartNS, m_fStartEW, m_fStopNS, m_fStopEW, m_strDesc);
	return strTemp;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CString	CParseFrame::GetDataNew
//	Description :	This routine will format a new blank CString's worth
//					of data.  All that it contains is the index
//	Return :		
//		CString		-	Formatted CString
//	Parameters :	
//		int nIndex	-	Index of the data.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseFrame::GetDataNew(int nIndex)
{
	CString strDataRecord;
	strDataRecord.Format(_T("%d, , , , , , , , , , , , , , , , , , , , ,"), nIndex+1);
	return (strDataRecord);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::GetDataIndex
//	Function :		CParseFrame::GetDataTime
//	Function :		CParseFrame::GetDataLabel
//	Function :		CParseFrame::GetDataInstrument
//	Function :		CParseFrame::GetDataSpacelookMode
//	Function :		CParseFrame::GetDataSpacelookSide
//	Function :		CParseFrame::GetDataSndrStepMode
//	Function :		CParseFrame::GetDataCoordType
//	Function :		CParseFrame::GetDataStartNS
//	Function :		CParseFrame::GetDataStartEW
//	Function :		CParseFrame::GetDataStopNS
//	Function :		CParseFrame::GetDataStopEW
//	Function :		CParseFrame::GetDataStartCycNS
//	Function :		CParseFrame::GetDataStartIncNS
//	Function :		CParseFrame::GetDataStartCycEW
//	Function :		CParseFrame::GetDataStartIncEW
//	Function :		CParseFrame::GetDataStopCycNS
//	Function :		CParseFrame::GetDataStopIncNS
//	Function :		CParseFrame::GetDataStopIncNS
//	Function :		CParseFrame::GetDataStopIncEW
//	Function :		CParseFrame::GetDataDuration
//	Function :		CParseFrame::GetDataDesc
//	Description :	These routines are use to get a string version of the
//					various member variables.  To get the member variables
//					in their native format, just access the variables
//					themselves (they are public).
//	Return :		
//		CString		-	Member variable formatted as a CString.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseFrame::GetDataIndex(){CString s; s.Format(_T("%d"), m_nIndex); return s;}
CString	CParseFrame::GetDataTime(){return m_strTime;}
CString	CParseFrame::GetDataLabel(){return m_strLabel;}
CString	CParseFrame::GetDataInstrument(){return ((m_nInstrument == -1) ? _T("") : strInstrument[m_nInstrument]);}
CString	CParseFrame::GetDataSpacelookMode(){return ((m_nSpacelookMode == -1) ? _T("") : strSpacelookMode[m_nSpacelookMode]);}
CString	CParseFrame::GetDataSpacelookSide(){return ((m_nSpacelookSide == -1) ? _T("") : strSpacelookSide[m_nSpacelookSide]);}
CString	CParseFrame::GetDataSndrStepMode(){return ((m_nSndrStepMode == -1) ? _T("") : strSndrStepMode[m_nSndrStepMode]);}
CString	CParseFrame::GetDataCoordType(){return ((m_nCoordType == -1) ? _T("") : strCoordType[m_nCoordType]);}
CString	CParseFrame::GetDataStartNS(){CString s; s.Format(_T("%.3f"), m_fStartNS); return s;}
CString	CParseFrame::GetDataStartEW(){CString s; s.Format(_T("%.3f"), m_fStartEW); return s;}
CString	CParseFrame::GetDataStopNS(){CString s; s.Format(_T("%.3f"), m_fStopNS); return s;}
CString	CParseFrame::GetDataStopEW(){CString s; s.Format(_T("%.3f"), m_fStopEW); return s;}
CString	CParseFrame::GetDataStartCycNS(){CString s; s.Format(_T("%d"), m_nStartCycNS); return s;}
CString	CParseFrame::GetDataStartIncNS(){CString s; s.Format(_T("%d"), m_nStartIncNS); return s;}
CString	CParseFrame::GetDataStartCycEW(){CString s; s.Format(_T("%d"), m_nStartCycEW); return s;}
CString	CParseFrame::GetDataStartIncEW(){CString s; s.Format(_T("%d"), m_nStartIncEW); return s;}
CString	CParseFrame::GetDataStopCycNS(){CString s; s.Format(_T("%d"), m_nStopCycNS); return s;}
CString	CParseFrame::GetDataStopIncNS(){CString s; s.Format(_T("%d"), m_nStopIncNS); return s;}
CString	CParseFrame::GetDataStopCycEW(){CString s; s.Format(_T("%d"), m_nStopCycEW); return s;}
CString	CParseFrame::GetDataStopIncEW(){CString s; s.Format(_T("%d"), m_nStopIncEW); return s;}
CString	CParseFrame::GetDataDuration(){CString s; s.Format(_T("%.3f"), m_fDuration); return s;}
CString	CParseFrame::GetDataDesc(){return m_strDesc;}
CString CParseFrame::GetData(int nDataTypeToGet)
{
	switch(nDataTypeToGet)
	{
		case eFrameIndex			: return GetDataIndex()			; break;
		case eFrameTime				: return GetDataTime()			; break;
		case eFrameLabel			: return GetDataLabel()			; break;
		case eFrameInstrument		: return GetDataInstrument()	; break;
		case eFrameSpacelookMode	: return GetDataSpacelookMode()	; break;
		case eFrameSpacelookSide	: return GetDataSpacelookSide()	; break;
		case eFrameSndrStepMode		: return GetDataSndrStepMode()	; break;
		case eFrameCoordType		: return GetDataCoordType()		; break;
		case eFrameStartNS			: return GetDataStartNS()		; break;
		case eFrameStartEW			: return GetDataStartEW()		; break;
		case eFrameStopNS			: return GetDataStopNS()		; break;
		case eFrameStopEW			: return GetDataStopEW()		; break;
		case eFrameStartCycNS		: return GetDataStartCycNS()	; break;
		case eFrameStartIncrNS		: return GetDataStartIncNS()	; break;
		case eFrameStartCycEW		: return GetDataStartCycEW()	; break;
		case eFrameStartIncrEW		: return GetDataStartIncEW()	; break;
		case eFrameStopCycNS		: return GetDataStopCycNS()		; break;
		case eFrameStopIncrNS		: return GetDataStopIncNS()		; break;
		case eFrameStopCycEW		: return GetDataStopCycEW()		; break;
		case eFrameStopIncrEW		: return GetDataStopIncEW()		; break;
		case eFrameDur				: return GetDataDuration()		; break;
		case eFrameDesc				: return GetDataDesc()			; break;
		default						: ASSERT (FALSE);return (_T("")); break;
	}
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::SetDataIndex
//	Function :		CParseFrame::SetDataTime
//	Function :		CParseFrame::SetDataLabel
//	Function :		CParseFrame::SetDataInstrument
//	Function :		CParseFrame::SetDataSpacelookMode
//	Function :		CParseFrame::SetDataSpacelookSide
//	Function :		CParseFrame::SetDataSndrStepMode
//	Function :		CParseFrame::SetDataCoordType
//	Function :		CParseFrame::SetDataStartNS
//	Function :		CParseFrame::SetDataStartEW
//	Function :		CParseFrame::SetDataStopNS
//	Function :		CParseFrame::SetDataStopEW
//	Function :		CParseFrame::SetDataStartCycNS
//	Function :		CParseFrame::SetDataStartIncNS
//	Function :		CParseFrame::SetDataStartCycEW
//	Function :		CParseFrame::SetDataStartIncEW
//	Function :		CParseFrame::SetDataStopCycNS
//	Function :		CParseFrame::SetDataStopIncNS
//	Function :		CParseFrame::SetDataStopCycEW
//	Function :		CParseFrame::SetDataStopIncEW
//	Function :		CParseFrame::SetDataDuration
//	Function :		CParseFrame::SetDataDesc
//	Description :	This routine sets member variable specified.  It 
//					converts the CString input into the variables native
//					format.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strValue	-	Value to convert and set.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void	CParseFrame::SetDataIndex(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nIndex);}
void	CParseFrame::SetDataTime(CString strValue){m_strTime = strValue;}
void	CParseFrame::SetDataLabel(CString strValue){m_strLabel = strValue;}
void	CParseFrame::SetDataInstrument(CString strValue){m_nInstrument = GetInstrumentIndex(strValue);}
void	CParseFrame::SetDataSpacelookMode(CString strValue){m_nSpacelookMode = GetSpacelookModeIndex(strValue);}
void	CParseFrame::SetDataSpacelookSide(CString strValue){m_nSpacelookSide = GetSpacelookSideIndex(strValue);}
void	CParseFrame::SetDataSndrStepMode(CString strValue){m_nSndrStepMode = GetSndrStepModeIndex(strValue);}
void	CParseFrame::SetDataCoordType(CString strValue){m_nCoordType = GetCoordTypeIndex(strValue);}
void	CParseFrame::SetDataStartNS(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fStartNS);}
void	CParseFrame::SetDataStartEW(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fStartEW);}
void	CParseFrame::SetDataStopNS(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fStopNS);}
void	CParseFrame::SetDataStopEW(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fStopEW);}
void	CParseFrame::SetDataStartCycNS(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nStartCycNS);}
void	CParseFrame::SetDataStartIncNS(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nStartIncNS);}
void	CParseFrame::SetDataStartCycEW(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nStartCycEW);}
void	CParseFrame::SetDataStartIncEW(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nStartIncEW);}
void	CParseFrame::SetDataStopCycNS(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nStopCycNS);}
void	CParseFrame::SetDataStopIncNS(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nStopIncNS);}
void	CParseFrame::SetDataStopCycEW(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nStopCycEW);}
void	CParseFrame::SetDataStopIncEW(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nStopIncEW);}
void	CParseFrame::SetDataDuration(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fDuration);}
void	CParseFrame::SetDataDesc(CString strValue){m_strDesc = m_strDesc;}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSector::CParseSector
//	Description :	Class Constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseSector::CParseSector()
{
	m_strLabel.Empty();
	m_nInstrument = -1;
	m_nSpacelookMode = -1;
	m_nSpacelookSide = -1;
	m_nSndrStepMode = -1;
	m_nCoordType = -1;
	m_fStartNS = -1.0;
	m_fStartEW = -1.0;
	m_fStopNS = -1.0;
	m_fStopEW = -1.0;
	m_strDesc.Empty();
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSector::~CParseSector
//	Description :	Class Destuctor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseSector::~CParseSector(){}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSector::ParseHdr
//	Description :	This routine parses the two header strings previously
//					set and updates the object's member variables as
//					appripriate.  
//	Return :		
//		BOOL		-	TRUE if the header strings contained valid data,
//						otherwise FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CParseSector::ParseHdr()
{
	BOOL bRtnStatus = FALSE;
	CString strLine = m_strHeaderLine1;
	_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fVersion);
	m_strType = RemoveFirstToken(&strLine);

	strLine = m_strHeaderLine2;
	m_nNumObj = _ttoi(RemoveFirstToken(&strLine));
	m_tModTime = _ttoi(RemoveFirstToken(&strLine));
	m_strModUser = RemoveFirstToken(&strLine);

	if ((m_strType.CompareNoCase(_T("Sector")) == 0) &&
		(m_fVersion == 1.0) &&
		(m_nNumObj != -1) &&
		(m_tModTime != -1) &&
		(!m_strModUser.IsEmpty()))
		bRtnStatus = TRUE;

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSector::GetHdrRec1
//	Description :	This routine will format the member variables needed
//					for the first header record into a CString.
//	Return :		
//		CString		-	Header record 1
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseSector::GetHdrRec1()
{
	CString strHdr;
	strHdr.Format(_T("%.1f, Sector"), m_fVersion);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSector::GetHdrRec2
//	Description :	This routine will format the member variables needed
//					for the second header record into a CString.
//	Return :		
//		CString		-	Header record 2
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseSector::GetHdrRec2()
{
	CString strHdr;
	strHdr.Format(_T("%d, %d, %s"),
		m_nNumObj,  m_tModTime, m_strModUser);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSector::GetHdrVersion
//	Function :		CParseSector::GetHdrNumObj
//	Function :		CParseSector::GetHdrModTimeFmt
//	Function :		CParseSector::GetHdrModTime
//	Function :		CParseSector::GetHdrModUser
//	Description :	These routines are use to get a string version of the
//					various member variables.  To get the member variables
//					in their native format, just access the variables
//					themselves (they are public).
//	Return :		
//		CString		-	Member variable formatted as a CString.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseSector::GetHdrVersion(){CString s; s.Format(_T("%.3f"), m_fVersion); return s;}
CString CParseSector::GetHdrNumObj(){CString s; s.Format(_T("%d"), m_nNumObj); return s;}
CString	CParseSector::GetHdrModTimeFmt(){return CvtTimeTtoStr(m_tModTime);}
CString CParseSector::GetHdrModTime(){CString s; s.Format(_T("%d"), m_tModTime); return s;}
CString CParseSector::GetHdrModUser(){return m_strModUser;}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		BOOL	CParseSector::ParseData
//	Description :	This routine will parse the intput data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CSTring parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseSector::ParseData()
{
	BOOL bRtnStatus = TRUE;

	CString strLine;
	if (bRtnStatus)
	{
		strLine = m_strData;
		if (strLine.Replace(',', ' ') != 10)
			bRtnStatus = FALSE;
	}

	if (bRtnStatus)
	{
		strLine = m_strData;
		m_strLabel = RemoveFirstToken(&strLine);
		m_nInstrument = GetInstrumentIndex(RemoveFirstToken(&strLine));
		m_nSpacelookMode = GetSpacelookModeIndex(RemoveFirstToken(&strLine));
		m_nSpacelookSide = GetSpacelookSideIndex(RemoveFirstToken(&strLine));
		m_nSndrStepMode = GetSndrStepModeIndex(RemoveFirstToken(&strLine));
		m_nCoordType = GetCoordTypeIndex(RemoveFirstToken(&strLine));
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fStartNS);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fStartEW);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fStopNS);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fStopEW);
		m_strDesc = RemoveFirstToken(&strLine);
		bRtnStatus = TRUE;  //Assume it was good
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSector::HasData
//	Description :	This routine will return TRUE if, after calling
//					ParseData, the String actually contained valid data.
//					To determine this, all we do is check to see if a
//					valid instrument.
//	Return :		
//		BOOL		-	TRUE if valid, otherwise FALSE
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CParseSector::HasData()
{
	return ((m_nInstrument != -1) ? TRUE : FALSE);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CString	CParseSector::GetData
//	Description :	This routine will format the dataa and return it as a
//					CString.
//	Return :		
//		CString		-	Formatted CString
//	Parameters :	
//		int nIndex	-	Index of the data.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseSector::GetData()
{
	CString strDataRecord;
	strDataRecord.Format(_T("%s, %s, %s, %s, %s, %s, %.3f, %.3f, %.3f, %.3f , %s"),
		m_strLabel, strInstrument[m_nInstrument], strSpacelookMode[m_nSpacelookMode],
		strSpacelookSide[m_nSpacelookSide],	strSndrStepMode[m_nSndrStepMode],
		strCoordType[m_nCoordType], m_fStartNS, m_fStartEW, m_fStopNS, m_fStopEW, m_strDesc);
	return (strDataRecord);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CString	CParseSector::GetDataNew
//	Description :	This routine will format a new blank CString's worth
//					of data.
//	Return :		
//		CString		-	Formatted CString
//	Parameters :	
//		int nIndex	-	Index of the data.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseSector::GetDataNew()
{
	CString strDataRecord(_T(", , , , , , , , , ,"));
	return (strDataRecord);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSector::SetHdrVersion
//	Function :		CParseSector::SetHdrNumObj
//	Function :		CParseSector::SetHdrModTime
//	Function :		CParseSector::SetHdrModUser
//	Description :	This routine sets member variable specified.  It 
//					converts the CString input into the variables native
//					format.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strValue	-	Value to convert and set.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CParseSector::SetHdrVersion(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fVersion);}
void CParseSector::SetHdrNumObj(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nNumObj);}
void CParseSector::SetHdrModTime(CString strValue){_stscanf_s (strValue, _T("%d"), &m_tModTime);}
void CParseSector::SetHdrModUser(CString strValue){m_strModUser = strValue;}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSector::GetDataLabel
//	Function :		CParseSector::GetDataInstrument
//	Function :		CParseSector::GetDataSpacelookMode
//	Function :		CParseSector::GetDataSpacelookSide
//	Function :		CParseSector::GetDataSndrStepMode
//	Function :		CParseSector::GetDataCoordType
//	Function :		CParseSector::GetDataStartNS
//	Function :		CParseSector::GetDataStartEW
//	Function :		CParseSector::GetDataStopNS
//	Function :		CParseSector::GetDataStopEW
//	Function :		CParseSector::GetDataDesc
//	Description :	These routines are use to get a string version of the
//					various member variables.  To get the member variables
//					in their native format, just access the variables
//					themselves (they are public).
//	Return :		
//		CString		-	Member variable formatted as a CString.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseSector::GetDataLabel(){return m_strLabel;}
CString	CParseSector::GetDataInstrument(){return ((m_nInstrument == -1) ? _T("") : strInstrument[m_nInstrument]);}
CString	CParseSector::GetDataSpacelookMode(){return ((m_nSpacelookMode == -1) ? _T("") : strSpacelookMode[m_nSpacelookMode]);}
CString	CParseSector::GetDataSpacelookSide(){return ((m_nSpacelookSide == -1) ? _T("") : strSpacelookSide[m_nSpacelookSide]);}
CString	CParseSector::GetDataSndrStepMode(){return ((m_nSndrStepMode == -1) ? _T("") : strSndrStepMode[m_nSndrStepMode]);}
CString	CParseSector::GetDataCoordType(){return ((m_nCoordType == -1) ? _T("") : strCoordType[m_nCoordType]);}
CString	CParseSector::GetDataStartNS(){CString s; s.Format(_T("%.3f"), m_fStartNS); return s;}
CString	CParseSector::GetDataStartEW(){CString s; s.Format(_T("%.3f"), m_fStartEW); return s;}
CString	CParseSector::GetDataStopNS(){CString s; s.Format(_T("%.3f"), m_fStopNS); return s;}
CString	CParseSector::GetDataStopEW(){CString s; s.Format(_T("%.3f"), m_fStopEW); return s;}
CString	CParseSector::GetDataDesc(){return m_strDesc;}
CString CParseSector::GetData(int nDataTypeToGet)
{
	switch(nDataTypeToGet)
	{
		case eSectorLabel			: return GetDataLabel()			; break;
		case eSectorInstrument		: return GetDataInstrument()	; break;
		case eSectorSpacelookMode	: return GetDataSpacelookMode()	; break;
		case eSectorSpacelookSide	: return GetDataSpacelookSide()	; break;
		case eSectorSndrStepMode	: return GetDataSndrStepMode()	; break;
		case eSectorCoordType		: return GetDataCoordType()		; break;
		case eSectorStartNS			: return GetDataStartNS()		; break;
		case eSectorStartEW			: return GetDataStartEW()		; break;
		case eSectorStopNS			: return GetDataStopNS()		; break;
		case eSectorStopEW			: return GetDataStopEW()		; break;
		case eSectorDesc			: return GetDataDesc()			; break;
		default						: ASSERT (FALSE);return (_T("")); break;
	}
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSector::SetDataLabel
//	Function :		CParseSector::SetDataInstrument
//	Function :		CParseSector::SetDataSpacelookMode
//	Function :		CParseSector::SetDataSpacelookSide
//	Function :		CParseSector::SetDataSndrStepMode
//	Function :		CParseSector::SetDataCoordType
//	Function :		CParseSector::SetDataStartNS
//	Function :		CParseSector::SetDataStartEW
//	Function :		CParseSector::SetDataStopNS
//	Function :		CParseSector::SetDataStopEW
//	Function :		CParseSector::SetDataDesc
//	Description :	This routine sets member variable specified.  It 
//					converts the CString input into the variables native
//					format.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strValue	-	Value to convert and set.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void	CParseSector::SetDataLabel(CString strValue){m_strLabel = strValue;}
void	CParseSector::SetDataInstrument(CString strValue){m_nInstrument = GetInstrumentIndex(strValue);}
void	CParseSector::SetDataSpacelookMode(CString strValue){m_nSpacelookMode = GetSpacelookModeIndex(strValue);}
void	CParseSector::SetDataSpacelookSide(CString strValue){m_nSpacelookSide = GetSpacelookSideIndex(strValue);}
void	CParseSector::SetDataSndrStepMode(CString strValue){m_nSndrStepMode = GetSndrStepModeIndex(strValue);}
void	CParseSector::SetDataCoordType(CString strValue){m_nCoordType = GetCoordTypeIndex(strValue);}
void	CParseSector::SetDataStartNS(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fStartNS);}
void	CParseSector::SetDataStartEW(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fStartEW);}
void	CParseSector::SetDataStopNS(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fStopNS);}
void	CParseSector::SetDataStopEW(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fStopEW);}
void	CParseSector::SetDataDesc(CString strValue){m_strDesc = m_strDesc;}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStar::CParseStar
//	Description :	Class Constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseStar::CParseStar()
{
	m_strTime.Empty();
	m_strDuration.Empty();
	m_nWindowNum = -1;
	m_nInstrument = -1;
	m_strIMCSet.Empty();
	m_nIndex = -1;
	m_nID = -1;
	m_nNumLooks = -1;
	m_nLookNum = -1;
	m_nRepeats = -1;
	m_nCycNS = -1;
	m_nIncNS = -1;
	m_nCycEW = -1;
	m_nIncEW = -1;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStar::~CParseStar
//	Description :	Class Destructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseStar::~CParseStar(){}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStar::ParseHdr
//	Description :	This routine parses the two header strings previously
//					set and updates the object's member variables as
//					appripriate.  
//	Return :		
//		BOOL		-	TRUE if the header strings contained valid data,
//						otherwise FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CParseStar::ParseHdr()
{
	BOOL bRtnStatus = FALSE;
	CString strLine = m_strHeaderLine1;
	_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fVersion);
	m_strType = RemoveFirstToken(&strLine);

	strLine = m_strHeaderLine2;
	m_nNumObj = _ttoi(RemoveFirstToken(&strLine));
	m_tModTime = _ttoi(RemoveFirstToken(&strLine));
	m_strModUser = RemoveFirstToken(&strLine);

	if ((m_strType.CompareNoCase(_T("Star")) == 0) &&
		(m_fVersion = 1.0) &&
		(m_nNumObj != -1) &&
		(m_tModTime != -1) &&
		(!m_strModUser.IsEmpty()))
		bRtnStatus = TRUE;

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStar::GetHdrRec1
//	Description :	This routine will format the member variables needed
//					for the first header record into a CString.
//	Return :		
//		CString		-	Header record 1
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseStar::GetHdrRec1()
{
	CString strHdr;
	strHdr.Format(_T("%.1f, Star"), m_fVersion);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStar::GetHdrRec2
//	Description :	This routine will format the member variables needed
//					for the second header record into a CString.
//	Return :		
//		CString		-	Header record 2
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseStar::GetHdrRec2()
{
	CString strHdr;
	strHdr.Format(_T("%d, %d, %s"),
		m_nNumObj,  m_tModTime, m_strModUser);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStar::GetHdrVersion
//	Function :		CParseStar::GetHdrNumObj
//	Function :		CParseStar::GetHdrModTimeFmt
//	Function :		CParseStar::GetHdrModTime
//	Function :		CParseStar::GetHdrModUser
//	Description :	These routines are use to get a string version of the
//					various member variables.  To get the member variables
//					in their native format, just access the variables
//					themselves (they are public).
//	Return :		
//		CString		-	Member variable formatted as a CString.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseStar::GetHdrVersion(){CString s; s.Format(_T("%.3f"), m_fVersion); return s;}
CString CParseStar::GetHdrNumObj(){CString s; s.Format(_T("%d"), m_nNumObj); return s;}
CString	CParseStar::GetHdrModTimeFmt(){return CvtTimeTtoStr(m_tModTime);}
CString CParseStar::GetHdrModTime(){CString s; s.Format(_T("%d"), m_tModTime); return s;}
CString CParseStar::GetHdrModUser(){return m_strModUser;}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStar::SetHdrVersion
//	Function :		CParseStar::SetHdrNumObj
//	Function :		CParseStar::SetHdrModTime
//	Function :		CParseStar::SetHdrModUser
//	Description :	This routine sets member variable specified.  It 
//					converts the CString input into the variables native
//					format.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strValue	-	Value to convert and set.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CParseStar::SetHdrVersion(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fVersion);}
void CParseStar::SetHdrNumObj(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nNumObj);}
void CParseStar::SetHdrModTime(CString strValue){_stscanf_s (strValue, _T("%d"), &m_tModTime);}
void CParseStar::SetHdrModUser(CString strValue){m_strModUser = strValue;}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		BOOL	CParseStar::ParseData
//	Description :	This routine will parse the intput data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CSTring parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseStar::ParseData()
{
	BOOL bRtnStatus = TRUE;

	CString strLine;
	if (bRtnStatus)
	{
		strLine = m_strData;
		if (strLine.Replace(',', ' ') != 13)
			bRtnStatus = FALSE;
	}

	if (bRtnStatus)
	{
		strLine = m_strData;
		m_strTime = RemoveFirstToken(&strLine);
		m_strDuration = RemoveFirstToken(&strLine);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nWindowNum);
		m_nInstrument = GetInstrumentIndex(RemoveFirstToken(&strLine));
		m_strIMCSet = RemoveFirstToken(&strLine);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nIndex);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nID);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nNumLooks);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nLookNum);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nRepeats);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nCycNS);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nIncNS);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nCycEW);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nIncEW);
		bRtnStatus = TRUE;  //Assume it was good
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrame::HasData
//	Description :	This routine will return TRUE if, after calling
//					ParseData, the String actually contained valid data.
//					To determine this, all we do is check to see if a
//					valid instrument.
//	Return :		
//		BOOL		-	TRUE if valid, otherwise FALSE
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CParseStar::HasData()
{
	return ((m_nWindowNum != -1) ? TRUE : FALSE);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CString	CParseStar::GetData
//	Description :	This routine will format the dataa and return it as a
//					CString.
//	Return :		
//		CString		-	Formatted CString
//	Parameters :	
//		int nIndex	-	Index of the data.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseStar::GetData()
{
	CString strDataRecord;
	strDataRecord.Format(_T("%s, %s, %d, %s, %s, %d, %d, %d, %d, %d, %d, %d, %d, %d"),
		m_strTime, m_strDuration, m_nWindowNum, strInstrument[m_nInstrument],
		m_strIMCSet, m_nIndex, m_nID, m_nNumLooks, m_nLookNum, m_nRepeats, m_nCycNS,
		m_nIncNS, m_nCycEW, m_nIncEW);
	return (strDataRecord);
}


/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CString	CParseStar::GetDataNew
//	Description :	This routine will format a new blank CString's worth
//					of data.  All that it contains is the index
//	Return :		
//		CString		-	Formatted CString
//	Parameters :	
//		int nIndex	-	Index of the data.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseStar::GetDataNew(int nIndex)
{
	CString strDataRecord;
	strDataRecord.Format(_T(", , , , ,%d, , , , , , , ,"), nIndex+1);
	return (strDataRecord);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStar::GetDataTime
//	Function :		CParseStar::GetDataDuration
//	Function :		CParseStar::GetDataWindowNum
//	Function :		CParseStar::GetDataInstrument
//	Function :		CParseStar::GetDataIMCSet
//	Function :		CParseStar::GetDataIndex
//	Function :		CParseStar::GetDataID
//	Function :		CParseStar::GetDataNumLooks
//	Function :		CParseStar::GetDataLookNum
//	Function :		CParseStar::GetDataRepeats
//	Function :		CParseStar::GetDataCycNS
//	Function :		CParseStar::GetDataIncNS
//	Function :		CParseStar::GetDataCycEW
//	Function :		CParseStar::GetDataIncEW
//	Description :	These routines are use to get a string version of the
//					various member variables.  To get the member variables
//					in their native format, just access the variables
//					themselves (they are public).
//	Return :		
//		CString		-	Member variable formatted as a CString.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseStar::GetDataTime(){return m_strTime;}
CString	CParseStar::GetDataDuration(){return m_strDuration;}
CString	CParseStar::GetDataWindowNum(){CString s; s.Format(_T("%d"), m_nWindowNum); return s;}
CString	CParseStar::GetDataInstrument(){return ((m_nInstrument == -1) ? _T("") : strInstrument[m_nInstrument]);}
CString	CParseStar::GetDataIMCSet(){return m_strIMCSet;}
CString	CParseStar::GetDataIndex(){CString s; s.Format(_T("%d"), m_nIndex); return s;}
CString	CParseStar::GetDataID(){CString s; s.Format(_T("%d"), m_nID); return s;}
CString	CParseStar::GetDataNumLooks(){CString s; s.Format(_T("%d"), m_nNumLooks); return s;}
CString	CParseStar::GetDataLookNum(){CString s; s.Format(_T("%d"), m_nLookNum); return s;}
CString	CParseStar::GetDataRepeats(){CString s; s.Format(_T("%d"), m_nRepeats); return s;}
CString	CParseStar::GetDataCycNS(){CString s; s.Format(_T("%d"), m_nCycNS); return s;}
CString	CParseStar::GetDataIncNS(){CString s; s.Format(_T("%d"), m_nIncNS); return s;}
CString	CParseStar::GetDataCycEW(){CString s; s.Format(_T("%d"), m_nCycEW); return s;}
CString	CParseStar::GetDataIncEW(){CString s; s.Format(_T("%d"), m_nIncEW); return s;}
CString CParseStar::GetData(int nDataTypeToGet)
{
	switch(nDataTypeToGet)
	{
		case eStarTime			: return GetDataTime()			; break;
		case eStarDuration		: return GetDataDuration()		; break;
		case eStarWindowNum		: return GetDataWindowNum()		; break;
		case eStarInstrument	: return GetDataInstrument()	; break;
		case eStarIMCSet		: return GetDataIMCSet()		; break;
		case eStarIndex			: return GetDataIndex()			; break;
		case eStarID			: return GetDataID()			; break;
		case eStarNumLooks		: return GetDataNumLooks()		; break;
		case eStarLookNum		: return GetDataLookNum()		; break;
		case eStarRepeats		: return GetDataRepeats()		; break;
		case eStarCycNS			: return GetDataCycNS()			; break;
		case eStarIncNS			: return GetDataIncNS()			; break;
		case eStarCycEW			: return GetDataCycEW()			; break;
		case eStarIncEW			: return GetDataIncEW()			; break;
		default					: ASSERT (FALSE);return (_T("")); break;
	}
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStar::SetDataVersion
//	Function :		CParseStar::SetDataDuration
//	Function :		CParseStar::SetDataWindowNum
//	Function :		CParseStar::SetDataInstrument
//	Function :		CParseStar::SetDataIMCSet
//	Function :		CParseStar::SetDataIndex
//	Function :		CParseStar::SetDataID
//	Function :		CParseStar::SetDataNumLooks
//	Function :		CParseStar::SetDataLookNum
//	Function :		CParseStar::SetDataRepeats
//	Function :		CParseStar::SetDataCycNS
//	Function :		CParseStar::SetDataIncNS
//	Function :		CParseStar::SetDataCycEW
//	Function :		CParseStar::SetDataIncEW
//	Description :	This routine sets member variable specified.  It 
//					converts the CString input into the variables native
//					format.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strValue	-	Value to convert and set.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CParseStar::SetDataTime(CString strValue){m_strTime = strValue;}
void CParseStar::SetDataDuration(CString strValue){m_strDuration = strValue;}
void CParseStar::SetDataWindowNum(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nWindowNum);}
void CParseStar::SetDataInstrument(CString strValue){m_nNumObj = GetInstrumentIndex(strValue);}
void CParseStar::SetDataIMCSet(CString strValue){m_strIMCSet = strValue;}
void CParseStar::SetDataIndex(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nIndex);}
void CParseStar::SetDataID(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nID);}
void CParseStar::SetDataNumLooks(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nNumLooks);}
void CParseStar::SetDataLookNum(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nLookNum);}
void CParseStar::SetDataRepeats(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nRepeats);}
void CParseStar::SetDataCycNS(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nCycNS);}
void CParseStar::SetDataIncNS(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nIncNS);}
void CParseStar::SetDataCycEW(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nCycEW);}
void CParseStar::SetDataIncEW(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nIncEW);}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSTOLMap::CParseSTOLMap
//	Description :	Class Constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :	
//     FJS modified for version 2.0		
//////////////////////////////////////////////////////////////////////////
***************************/
CParseSTOLMap::CParseSTOLMap()
{
	m_nLisLine = -1;
	m_nNextLisLine = -1;
	m_nOBStartIndex= -1;
	m_nOBEndIndex = -1;
	m_strSchedCmd.Empty();
	m_strExecTime.Empty();
	m_fCmdExecDuration = -1.0;
	m_nPrevLisLine = -1;
	m_nSchedLine = -1;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSTOLMap::~CParseSTOLMap
//	Description :	Class Destructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseSTOLMap::~CParseSTOLMap(){}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSTOLMap::ParseHdr
//	Description :	This routine parses the two header strings previously
//					set and updates the object's member variables as
//					appripriate.  
//	Return :		
//		BOOL		-	TRUE if the header strings contained valid data,
//						otherwise FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CParseSTOLMap::ParseHdr()
{
	BOOL bRtnStatus = FALSE;
	CString strLine = m_strHeaderLine1;
	_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fVersion);
	m_strType = RemoveFirstToken(&strLine);

	strLine = m_strHeaderLine2;
	m_nNumObj = _ttoi(RemoveFirstToken(&strLine));
	m_tModTime = _ttoi(RemoveFirstToken(&strLine));
	m_strModUser = RemoveFirstToken(&strLine);

	if ((m_strType.CompareNoCase(_T("STOLMap")) == 0) &&
		(m_fVersion = 2.0) &&
		(m_nNumObj != -1) &&
		(m_tModTime != -1) &&
		(!m_strModUser.IsEmpty()))
		bRtnStatus = TRUE;

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSTOLMap::GetHdrRec1
//	Description :	This routine will format the member variables needed
//					for the first header record into a CString.
//	Return :		
//		CString		-	Header record 1
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseSTOLMap::GetHdrRec1()
{
	CString strHdr;
	strHdr.Format(_T("%.1f, Map"), m_fVersion);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSTOLMap routine will format the member variables needed
//					for the second header record into a CString.
//	Return :		
//		CString		-	Header record 2
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseSTOLMap::GetHdrRec2()
{
	CString strHdr;
	strHdr.Format(_T("%d, %d, %s"),
		m_nNumObj,  m_tModTime, m_strModUser);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSTOLMap::GetHdrVersion
//	Function :		CParseSTOLMap::GetHdrNumObj
//	Function :		CParseSTOLMap::GetHdrModTimeFmt
//	Function :		CParseSTOLMap::GetHdrModTime
//	Function :		CParseSTOLMap::GetHdrModUser
//	Description :	These routines are use to get a string version of the
//					various member variables.  To get the member variables
//					in their native format, just access the variables
//					themselves (they are public).
//	Return :		
//		CString		-	Member variable formatted as a CString.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseSTOLMap::GetHdrVersion(){CString s; s.Format(_T("%.3f"), m_fVersion); return s;}
CString CParseSTOLMap::GetHdrNumObj(){CString s; s.Format(_T("%d"), m_nNumObj); return s;}
CString	CParseSTOLMap::GetHdrModTimeFmt(){return CvtTimeTtoStr(m_tModTime);}
CString CParseSTOLMap::GetHdrModTime(){CString s; s.Format(_T("%d"), m_tModTime); return s;}
CString CParseSTOLMap::GetHdrModUser(){return m_strModUser;}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSTOLMap::SetHdrVersion
//	Function :		CParseSTOLMap::SetHdrNumObj
//	Function :		CParseSTOLMap::SetHdrModTime
//	Function :		CParseSTOLMap::SetHdrModUser
//	Description :	This routine sets member variable specified.  It 
//					converts the CString input into the variables native
//					format.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strValue	-	Value to convert and set.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CParseSTOLMap::SetHdrVersion(CString strValue){_stscanf_s (strValue, _T("%f"), &m_fVersion);}
void CParseSTOLMap::SetHdrNumObj(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nNumObj);}
void CParseSTOLMap::SetHdrModTime(CString strValue){_stscanf_s (strValue, _T("%d"), &m_tModTime);}
void CParseSTOLMap::SetHdrModUser(CString strValue){m_strModUser = strValue;}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		BOOL	CParseSTOLMap::ParseData
//	Description :	This routine will parse the intput data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CSTring parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseSTOLMap::ParseData()
{
	BOOL bRtnStatus = TRUE;

	CString strLine;
	if (bRtnStatus)
	{
		strLine = m_strData;
		if (strLine.Replace(_T(','), _T(' ')) != 8)
			bRtnStatus = FALSE;
	}

	if (bRtnStatus)
	{
		strLine = m_strData;
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nLisLine);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nNextLisLine);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nOBStartIndex);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nOBEndIndex);
		m_strSchedCmd = RemoveFirstToken(&strLine);
		m_strExecTime = RemoveFirstToken(&strLine);
		m_strExecTime.Replace(_T("-"), _T("/"));
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fCmdExecDuration);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nPrevLisLine);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nSchedLine);
		bRtnStatus = TRUE;  //Assume it was good
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CString	CParseSTOLMap::GetData
//	Description :	This routine will format the data and return it as a
//					CString.
//	Return :		
//		CString		-	Formatted CString
//	Parameters :	
//		int nIndex	-	Index of the data.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseSTOLMap::GetData()
{
	CString strDataRecord;
	strDataRecord.Format(_T("%d, %d, %d, %d, %s, %s, %.3f, %d, %d"),
		m_nLisLine, m_nNextLisLine, m_nOBStartIndex, m_nOBEndIndex,
		m_strSchedCmd, m_strExecTime, m_fCmdExecDuration, m_nPrevLisLine, m_nSchedLine);
	return (strDataRecord);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSTOLMap::GetDataLisLine
//	Function :		CParseSTOLMap::GetDataNextLisLine
//	Function :		CParseSTOLMap::GetDataOBStartIndex
//	Function :		CParseSTOLMap::GetDataOBEndIndex
//	Function :		CParseSTOLMap::GetDataSchedCmd
//	Function :		CParseSTOLMap::GetDataCmdExecDuration
//	Function :		CParseSTOLMap::GetDataPrevLisLine
//	Function :		CParseSTOLMap::GetDataSchedLine
//	Description :	These routines are use to get a string version of the
//					various member variables.  To get the member variables
//					in their native format, just access the variables
//					themselves (they are public).
//	Return :		
//		CString		-	Member variable formatted as a CString.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseSTOLMap::GetDataLisLine(){CString s; s.Format(_T("%d"), m_nLisLine); return s;}
CString	CParseSTOLMap::GetDataNextLisLine(){CString s; s.Format(_T("%d"), m_nNextLisLine); return s;}
CString	CParseSTOLMap::GetDataOBStartIndex(){CString s; s.Format(_T("%d"), m_nOBStartIndex); return s;}
CString	CParseSTOLMap::GetDataOBEndIndex(){CString s; s.Format(_T("%d"), m_nOBEndIndex); return s;}
CString	CParseSTOLMap::GetDataSchedCmd(){return m_strSchedCmd;}
CString	CParseSTOLMap::GetDataExecTime(){return m_strExecTime;}
CString	CParseSTOLMap::GetDataCmdExecDuration(){CString s; s.Format(_T("%f"), m_fCmdExecDuration); return s;}
CString	CParseSTOLMap::GetDataPrevLisLine(){CString s; s.Format(_T("%d"), m_nPrevLisLine); return s;}
CString	CParseSTOLMap::GetDataSchedLine(){CString s; s.Format(_T("%d"), m_nSchedLine); return s;}
CString CParseSTOLMap::GetData(int nDataTypeToGet)
{
	switch(nDataTypeToGet)
	{
		case eSTOLMapCurLisLine		: return GetDataLisLine()			; break;
		case eSTOLMapNextLisLine	: return GetDataNextLisLine()		; break;
		case eSTOLMapOBStartIndex	: return GetDataOBStartIndex()		; break;
		case eSTOLMapOBEndIndex		: return GetDataOBEndIndex()		; break;
		case eSTOLMapSchedCmd		: return GetDataSchedCmd()			; break;
		case eSTOLMapSchedTime		: return GetDataExecTime()			; break;
		case eSTOLMapCmdExecTime	: return GetDataCmdExecDuration()	; break;
		case eSTOLMapPrevLisLine	: return GetDataPrevLisLine()		; break;
		case eSTOLMapSchedLine		: return GetDataSchedLine()			; break;
		default						: ASSERT (FALSE);return (_T(""))	; break;
	}
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseSTOLMap::SetDataSchedStartLine
//	Function :		CParseSTOLMap::SetDataSchedEndLine
//	Function :		CParseSTOLMap::SetDataOBStartLine
//	Function :		CParseSTOLMap::SetDataOBEndLine
//	Function :		CParseSTOLMap::SetDataSchedCmd
//	Function :		CParseSTOLMap::SetDataExecTime
//	Function :		CParseSTOLMap::SetDataExecWindow
//	Function :		CParseSTOLMap::SetDataPreviousSchedStartLine
//	Description :	This routine sets member variable specified.  It 
//					converts the CString input into the variables native
//					format.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strValue	-	Value to convert and set.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CParseSTOLMap::SetDataLisLine(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nLisLine);}
void CParseSTOLMap::SetDataNextLisLine(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nNextLisLine);}
void CParseSTOLMap::SetDataOBStartIndex(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nOBStartIndex);}
void CParseSTOLMap::SetDataOBEndIndex(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nOBEndIndex);}
void CParseSTOLMap::SetDataSchedCmd(CString strValue){m_strSchedCmd = strValue;}
void CParseSTOLMap::SetDataExecTime(CString strValue){m_strExecTime = strValue;}
void CParseSTOLMap::SetDataCmdExecDuration(CString strValue){_stscanf_s (strValue, _T("%.3f"), &m_fCmdExecDuration);}
void CParseSTOLMap::SetDataPrevLisLine(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nPrevLisLine);}
void CParseSTOLMap::SetDataSchedLine(CString strValue){_stscanf_s (strValue, _T("%d"), &m_nSchedLine);}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseRtcsMap::CParseRtcsMap
//	Description :	Class Constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseRTCSMap::CParseRTCSMap()
{
	m_nLisLine			= -1;
	m_nNextLisLine		= -1;
	m_nOBStartIndex		= -1;
	m_nOBEndIndex		= -1;
	m_strCmd.Empty();
	m_strLabel.Empty();
	m_strHexCmdValue.Empty();
	m_strRamAddress.Empty();
	m_strRtcsExecDuration.Empty();
	m_fCmdExecDuration	= -1.0;
	m_nNumber			= -1;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseRtcsMap::~CParseRtcsMap
//	Description :	Class Destructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseRTCSMap::~CParseRTCSMap(){}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseRtcsMap::ParseHdr
//	Description :	This routine parses the two header strings previously
//					set and updates the object's member variables as
//					appripriate.  
//	Return :		
//		BOOL		-	TRUE if the header strings contained valid data,
//						otherwise FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CParseRTCSMap::ParseHdr()
{
	BOOL	bRtnStatus	= FALSE;
	CString strLine		= m_strHeaderLine1;

	_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fVersion);
	m_strType	= RemoveFirstToken(&strLine);

	strLine		= m_strHeaderLine2;
	m_nNumObj	= _ttoi(RemoveFirstToken(&strLine));
	m_tModTime	= _ttoi(RemoveFirstToken(&strLine));
	m_strModUser= RemoveFirstToken(&strLine);

	if ((m_strType.CompareNoCase(_T("RTCSMap")) == 0) &&
		(m_fVersion = 1.0) &&
		(m_nNumObj != -1) &&
		(m_tModTime != -1) &&
		(!m_strModUser.IsEmpty()))
		bRtnStatus = TRUE;

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseRtcsMap::GetHdrRec1
//	Description :	This routine will format the member variables needed
//					for the first header record into a CString.
//	Return :		
//		CString		-	Header record 1
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseRTCSMap::GetHdrRec1()
{
	CString strHdr;

	strHdr.Format(_T("%.1f, MapRTCS"), m_fVersion);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseRtcsMap::GetHdrRec2
//	Description :	This routine will format the member variables needed
//					for the second header record into a CString.
//	Return :		
//		CString		-	Header record 2
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseRTCSMap::GetHdrRec2()
{
	CString strHdr;

	strHdr.Format(_T("%d, %d, %s"), m_nNumObj,  m_tModTime, m_strModUser);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseRtcsMap::GetHdrVersion
//	Function :		CParseRtcsMap::GetHdrNumObj
//	Function :		CParseRtcsMap::GetHdrModTimeFmt
//	Function :		CParseRtcsMap::GetHdrModTime
//	Function :		CParseRtcsMap::GetHdrModUser
//	Description :	These routines are use to get a string version of the
//					various member variables.  To get the member variables
//					in their native format, just access the variables
//					themselves (they are public).
//	Return :		
//		CString		-	Member variable formatted as a CString.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseRTCSMap::GetHdrVersion()		{ CString s; s.Format(_T("%.3f"), m_fVersion); return s; }
CString CParseRTCSMap::GetHdrNumObj()		{ CString s; s.Format(_T("%d"), m_nNumObj); return s; }
CString	CParseRTCSMap::GetHdrModTimeFmt()	{ return CvtTimeTtoStr(m_tModTime);}
CString CParseRTCSMap::GetHdrModTime()		{ CString s; s.Format(_T("%d"), m_tModTime); return s; }
CString CParseRTCSMap::GetHdrModUser()		{ return m_strModUser; }

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseRtcsMap::SetHdrVersion
//	Function :		CParseRtcsMap::SetHdrNumObj
//	Function :		CParseRtcsMap::SetHdrModTime
//	Function :		CParseRtcsMap::SetHdrModUser
//	Description :	This routine sets member variable specified.  It 
//					converts the CString input into the variables native
//					format.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strValue	-	Value to convert and set.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CParseRTCSMap::SetHdrVersion(CString strValue)	{ _stscanf_s (strValue, _T("%f"), &m_fVersion); }
void CParseRTCSMap::SetHdrNumObj(CString strValue)	{ _stscanf_s (strValue, _T("%d"), &m_nNumObj); }
void CParseRTCSMap::SetHdrModTime(CString strValue)	{ _stscanf_s (strValue, _T("%d"), &m_tModTime); }
void CParseRTCSMap::SetHdrModUser(CString strValue)	{ m_strModUser = strValue; }

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		BOOL	CParseRtcsMap::ParseData
//	Description :	This routine will parse the intput data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CSTring parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CParseRTCSMap::ParseData()
{
	BOOL	bRtnStatus = TRUE;
	CString strLine;

	if (bRtnStatus)
	{
		strLine = m_strData;

		if (strLine.Replace(_T(','), _T(' ')) != 10)
			bRtnStatus = FALSE;
	}

	if (bRtnStatus)
	{
		strLine = m_strData;
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nLisLine);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nNextLisLine);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nOBStartIndex);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nOBEndIndex);
		m_strCmd				= RemoveFirstToken(&strLine);
		m_strLabel				= RemoveFirstToken(&strLine);
		m_strHexCmdValue		= RemoveFirstToken(&strLine);
		m_strRamAddress			= RemoveFirstToken(&strLine);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%d"), &m_nNumber);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fCmdExecDuration);
		m_strRtcsExecDuration	= RemoveFirstToken(&strLine);
		bRtnStatus = TRUE;  //Assume it was good
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CString	CParseRtcsMap::GetData
//	Description :	This routine will format the data and return it as a
//					CString.
//	Return :		
//		CString		-	Formatted CString
//	Parameters :	
//		int nIndex	-	Index of the data.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseRTCSMap::GetData()
{
	CString strDataRecord;
	strDataRecord.Format(_T("%d, %d, %d, %d, %s, %s, %s, %s, %d, %.3f, %s"),
		m_nLisLine, m_nNextLisLine, m_nOBStartIndex, m_nOBEndIndex,
		m_strCmd, m_strLabel, m_strHexCmdValue, m_strRamAddress, 
		m_nNumber, m_fCmdExecDuration, m_strRtcsExecDuration);
	return (strDataRecord);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseRtcsMap::GetDataRtcsStartLine
//	Function :		CParseRtcsMap::GetDataRtcsEndLine
//	Function :		CParseRtcsMap::GetDataOBStartLine
//	Function :		CParseRtcsMap::GetDataOBEndLine
//	Function :		CParseRtcsMap::GetDataRtcsCmd
//	Function :		CParseRtcsMap::GetDataLabel
//	Function :		CParseRtcsMap::GetDataCmdExecDuration
//	Function :		CParseRtcsMap::GetDataHexCmdValue
//	Function :		CParseRtcsMap::GetDataRtcsNumber
//	Function :		CParseRtcsMap::GetDataRtcsExecDuration
//	Function :		CParseRtcsMap::GetDataRamAddress
//	Description :	These routines are use to get a string version of the
//					various member variables.  To get the member variables
//					in their native format, just access the variables
//					themselves (they are public).
//	Return :		
//		CString		-	Member variable formatted as a CString.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseRTCSMap::GetDataLisLine()			{ CString s; s.Format(_T("%d"), m_nLisLine);		return s; }
CString	CParseRTCSMap::GetDataNextLisLine()		{ CString s; s.Format(_T("%d"), m_nNextLisLine);	return s; }
CString	CParseRTCSMap::GetDataOBStartIndex()	{ CString s; s.Format(_T("%d"), m_nOBStartIndex);	return s; }
CString	CParseRTCSMap::GetDataOBEndIndex()		{ CString s; s.Format(_T("%d"), m_nOBEndIndex);		return s; }
CString	CParseRTCSMap::GetDataCmd()				{ return m_strCmd;}
CString	CParseRTCSMap::GetDataLabel()			{ return m_strLabel;}
CString	CParseRTCSMap::GetDataHexCmdValue()		{ return m_strHexCmdValue;}
CString	CParseRTCSMap::GetDataRtcsExecDuration(){ return m_strRtcsExecDuration;}
CString	CParseRTCSMap::GetDataRamAddress()		{ return m_strRamAddress;}
CString	CParseRTCSMap::GetDataCmdExecDuration()	{ CString s; s.Format(_T("%f"), m_fCmdExecDuration);return s; }
CString	CParseRTCSMap::GetDataNumber()			{ CString s; s.Format(_T("%d"), m_nNumber);			return s; }
CString CParseRTCSMap::GetData(int nDataTypeToGet)
{
	switch(nDataTypeToGet)
	{
		case eRTCSMapCurLisLine			: return GetDataLisLine()			; break;
		case eRTCSMapNextLisLine		: return GetDataNextLisLine()		; break;
		case eRTCSMapOBStartIndex		: return GetDataOBStartIndex()		; break;
		case eRTCSMapOBEndIndex			: return GetDataOBEndIndex()		; break;
		case eRTCSMapCmd				: return GetDataCmd()				; break;
		case eRTCSMapLabel				: return GetDataLabel()				; break;
		case eRTCSMapHexCmdValue		: return GetDataHexCmdValue()		; break;
		case eRTCSMapCmdExecDuration	: return GetDataCmdExecDuration()	; break;
		case eRTCSMapNumber				: return GetDataNumber()			; break;
		case eRTCSMapRamAddress			: return GetDataRamAddress()		; break;
		case eRTCSMapRtcsExecDuration	: return GetDataRtcsExecDuration()	; break;
		default							: ASSERT (FALSE);return (_T(""))	; break;
	}
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :			Date : 1/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseRtcsMap::SetDataLisLine
//	Function :		CParseRtcsMap::SetDataEndLisLine
//	Function :		CParseRtcsMap::SetDataOBStartLine
//	Function :		CParseRtcsMap::SetDataOBEndLine
//	Function :		CParseRtcsMap::SetDataCmd
//	Function :		CParseRtcsMap::SetDataLabel
//	Function :		CParseRtcsMap::SetDataHexCmdValue
//	Function :		CParseRtcsMap::SetDataCmdExecDuration
//	Function :		CParseRtcsMap::SetDataNumber
//	Function :		CParseRtcsMap::SetDataRtcsExecDuration
//	Function :		CParseRtcsMap::SetDataRamAddress
//	Description :	This routine sets member variable specified.  It 
//					converts the CString input into the variables native
//					format.
//	Return :		
//		void		-	
//	Parameters :	
//		CString strValue	-	Value to convert and set.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CParseRTCSMap::SetDataLisLine(CString strValue)			{ _stscanf_s (strValue, _T("%d"),	&m_nLisLine);}
void CParseRTCSMap::SetDataEndLisLine(CString strValue)			{ _stscanf_s (strValue, _T("%d"),	&m_nNextLisLine); }
void CParseRTCSMap::SetDataOBStartIndex(CString strValue)		{ _stscanf_s (strValue, _T("%d"),	&m_nOBStartIndex); }
void CParseRTCSMap::SetDataOBEndIndex(CString strValue)			{ _stscanf_s (strValue, _T("%d"),	&m_nOBEndIndex);}
void CParseRTCSMap::SetDataCmd(CString strValue)				{ m_strCmd				= strValue;}
void CParseRTCSMap::SetDataLabel(CString strValue)				{ m_strLabel			= strValue;}
void CParseRTCSMap::SetDataHexCmdValue(CString strValue)		{ m_strHexCmdValue		= strValue;}
void CParseRTCSMap::SetDataRamAddress(CString strValue)			{ m_strRamAddress		= strValue;}
void CParseRTCSMap::SetDataRtcsExecDuration(CString strValue)	{ m_strRtcsExecDuration	= strValue;}
void CParseRTCSMap::SetDataCmdExecDuration(CString strValue)	{ _stscanf_s (strValue, _T("%.3f"),	&m_fCmdExecDuration);}
void CParseRTCSMap::SetDataNumber(CString strValue)				{ _stscanf_s (strValue, _T("%d"),	&m_nNumber);}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseCMDPace::CParseCMDPace
//	Description :	Class Constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseCMDPace::CParseCMDPace(){}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseCMDPace::~CParseCMDPace
//	Description :	Class Destructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseCMDPace::~CParseCMDPace(){}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseCMDPace::ParseHdr
//	Description :	This routine parses the two header strings previously
//					set and updates the object's member variables as
//					appropriate.  
//	Return :		
//		BOOL		-	TRUE if the header strings contained valid data,
//						otherwise FALSE.
//	Parameters : none	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CParseCMDPace::ParseHdr()
{
	BOOL bRtnStatus = FALSE;
	CString strLine = m_strHeaderLine1;
	_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fVersion);
	m_strType = RemoveFirstToken(&strLine);

	strLine = m_strHeaderLine2;
	m_nNumObj = _ttoi(RemoveFirstToken(&strLine));
	m_strModTime = RemoveFirstToken(&strLine);
	m_strModUser = RemoveFirstToken(&strLine);

	if ((m_strType.CompareNoCase(_T("PCMD")) == 0) &&
		(m_fVersion = 2.0) &&
		(m_nNumObj != -1) &&
		(!m_strModTime.IsEmpty()) &&
		(!m_strModUser.IsEmpty()))
		bRtnStatus = TRUE;

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseCMDPace::GetHdrRec1
//	Description :	This routine will format the member variables needed
//					for the first header record into a CString.
//	Return :		
//		CString		-	Header record 1
//
//	Parameters :	none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseCMDPace::GetHdrRec1()
{
	CString strHdr;
	strHdr.Format(_T("%.1f, %s"), m_fVersion, m_strType);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseCMDPace::GetHdrRec2
//	Description :	This routine will format the member variables needed
//					for the second header record into a CString.
//	Return :		
//		CString		-	Header record 2
//
//	Parameters :	none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString CParseCMDPace::GetHdrRec2()
{
	CString strHdr;
	strHdr.Format(_T("%d, %s, %s"),
		m_nNumObj,  m_strModTime, m_strModUser);
	return strHdr;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseCMDPace::ParseData
//	Description :	This routine will parse the intput data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CSTring parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseCMDPace::ParseData()
{
	BOOL bRtnStatus = TRUE;

	CString strLine;
	if (bRtnStatus)
	{
		strLine = m_strData;
		if (strLine.Replace(_T(','), _T(' ')) < 1)
			bRtnStatus = FALSE;
	}

	if (bRtnStatus)
	{
		strLine = m_strData;
		m_strCMDPaceMnemonic01 = RemoveFirstToken(&strLine);
		_stscanf_s(RemoveFirstToken(&strLine), _T("%f"), &m_fCMDPaceDelay);
		m_strCMDPaceMnemonic02 = RemoveFirstToken(&strLine);
		bRtnStatus = TRUE;  //Assume it was good
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseCMDPace::GetData
//	Description :	This routine will format the data and return it as a
//					CString.
//	Return :		
//		CString		-	Formatted CString
//	Parameters :	
//					none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CString	CParseCMDPace::GetData()
{
	CString strDataRecord;
	strDataRecord.Format(_T("%s, %.3f, %s"), m_strCMDPaceMnemonic01, m_fCMDPaceDelay,
		m_strCMDPaceMnemonic02);

	return (strDataRecord);
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrameIOMS::
//	Description :	
//					
//	Return :		
//		
//	Parameters :	
//					none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseFrameIOMS::CParseFrameIOMS(){
//	m_objstrParams.SetSize(eMaxFrameIOMSColumns);
};

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrameIOMS::~CParseFrameIOMS()
//	Description :	Destructor.
//					
//	Return :		
//					void	-	none.
//	Parameters :	
//					void	-	none
//	Note :			
////////////////////////////////////////////////////////////////////
***************************/
CParseFrameIOMS::~CParseFrameIOMS(){};

/***************************
///////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01			version 1.0
///////////////////////////////////////////////////////////////////////
//	Function :		CParseFrameIOMS::ParseData(CString strStartLine)
//	Description :	This routine will parse the input data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CString parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
///////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseFrameIOMS::ParseData(CString strLine)
{
	BOOL bRtnStatus = TRUE;
	int nPos= 0;

	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove directive
	CString strUpperCaseCMDLine(strLine);
	strUpperCaseCMDLine.MakeUpper();
	nPos = (strUpperCaseCMDLine.Find(ctstrSTART, 0));
	if (nPos < 0){
		bRtnStatus = FALSE;
	} else {
		int nLen = _tcsclen(ctstrSTART);
		strLine.Delete(0, nPos + nLen);
		strLine.TrimLeft();
		strLine.TrimRight();
		
		int nProcNameIndex = -1;
		// First look for a "(".
		if (-1 == (nProcNameIndex = strLine.Find(_T('('), 0))){
			// If a "(" is not found, then look for space.
			// This is an error, since no passed parameters are present.
			nProcNameIndex = strLine.Find(_T(' '), 0);
			bRtnStatus = FALSE;
		}  

		// Extract out the Proc name
		if (nProcNameIndex > 0){
			// Remove the STOL proc name for parsing the passed parameters.
			strLine.Delete(0, nProcNameIndex);
			strLine.TrimLeft();
			strLine.TrimRight();
		}
	}

	strLine.Replace(_T('('), _T(' '));
	strLine.Replace(_T(')'), _T(' '));
	

	if (TRUE == bRtnStatus)
	{
		bRtnStatus = TRUE;  // Assume it was good

		while(!strLine.IsEmpty()){
			m_objstrParams.Add(RemoveFirstToken(&strLine));
		}

		// Check number of tokens found againest number expected.
		if (m_objstrParams.GetSize() < eMaxFrameIOMSColumns){
			bRtnStatus = FALSE;
		}
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrameMS
//	Description :	
//					
//	Return :		
//		
//	Parameters :	
//					none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseFrameMS::CParseFrameMS(){};
CParseFrameMS::~CParseFrameMS(){};

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrameMS::ParseData(CString strStartLine)
//	Description :	This routine will parse the input data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CString parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseFrameMS::ParseData(CString strLine)
{
	BOOL bRtnStatus = TRUE;
	int nPos= 0;

	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove directive
	CString strUpperCaseCMDLine(strLine);
	strUpperCaseCMDLine.MakeUpper();
	nPos = (strUpperCaseCMDLine.Find(ctstrSTART, 0));
	if (nPos < 0){
		bRtnStatus = FALSE;
	} else {
		int nLen = _tcsclen(ctstrSTART);
		strLine.Delete(0, nPos + nLen);
		strLine.TrimLeft();
		strLine.TrimRight();
		
		int nProcNameIndex = -1;
		// First look for a "(".
		if (-1 == (nProcNameIndex = strLine.Find(_T('('), 0))){
			// If a "(" is not found, then look for space.
			// This is an error, since no passed parameters are present.
			nProcNameIndex = strLine.Find(_T(' '), 0);
			bRtnStatus = FALSE;
		}  

		// Extract out the Proc name
		if (nProcNameIndex > 0){
			// Remove the STOL proc name for parsing the passed parameters.
			strLine.Delete(0, nProcNameIndex);
			strLine.TrimLeft();
			strLine.TrimRight();
		}
	}

	strLine.Replace(_T('('), _T(' '));
	strLine.Replace(_T(')'), _T(' '));
	

	if (TRUE == bRtnStatus)
	{
		bRtnStatus = TRUE;  // Assume it was good
		while (!strLine.IsEmpty()){
			m_objstrParams.Add(RemoveFirstToken(&strLine));
		}

		// Check number of tokens found againest expected.
				// Check number of tokens found againest number expected.
		if (m_objstrParams.GetSize() < eMaxFrameMSColumns){
			bRtnStatus = FALSE;
		}
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStarIOMS
//	Description :	
//					
//	Return :		
//		
//	Parameters :	
//					none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseStarIOMS::CParseStarIOMS(){};
CParseStarIOMS::~CParseStarIOMS(){};

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseStarIOMS::ParseData(CString strStartLine)
//	Description :	This routine will parse the input data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CString parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseStarIOMS::ParseData(CString strLine)
{
	BOOL bRtnStatus = TRUE;
	int nPos= 0;

	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove directive
	CString strUpperCaseCMDLine(strLine);
	strUpperCaseCMDLine.MakeUpper();
	nPos = (strUpperCaseCMDLine.Find(ctstrSTART, 0));
	if (nPos < 0){
		bRtnStatus = FALSE;
	} else {
		int nLen = _tcsclen(ctstrSTART);
		strLine.Delete(0, nPos + nLen);
		strLine.TrimLeft();
		strLine.TrimRight();
		
		int nProcNameIndex = -1;
		// First look for a "(".
		if (-1 == (nProcNameIndex = strLine.Find(_T('('), 0))){
			// If a "(" is not found, then look for space.
			// This is an error, since no passed parameters are present.
			nProcNameIndex = strLine.Find(_T(' '), 0);
			bRtnStatus = FALSE;
		}  

		// Extract out the Proc name
		if (nProcNameIndex > 0){
			// Remove the STOL proc name for parsing the passed parameters.
			strLine.Delete(0, nProcNameIndex);
			strLine.TrimLeft();
			strLine.TrimRight();
		}
	}

	strLine.Replace(_T('('), _T(' '));
	strLine.Replace(_T(')'), _T(' '));
	

	if (TRUE == bRtnStatus)
	{
		bRtnStatus = TRUE;  // Assume it was good

		while (!strLine.IsEmpty()){
			m_objstrParams.Add(RemoveFirstToken(&strLine));
		}

		// Check number of tokens to expected number.
		if (m_objstrParams.GetSize() < eMaxStarIOMSColumns){
			bRtnStatus = FALSE;
		}
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrameIOMS::
//	Description :	
//					
//	Return :		
//		
//	Parameters :	
//					none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseStarMS::CParseStarMS(){};
CParseStarMS::~CParseStarMS(){};

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseFrameIOMS::ParseData(CString strStartLine)
//	Description :	This routine will parse the input data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CString parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseStarMS::ParseData(CString strLine)
{
	BOOL bRtnStatus = TRUE;
	int nPos= 0;

	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove directive
	CString strUpperCaseCMDLine(strLine);
	strUpperCaseCMDLine.MakeUpper();
	nPos = (strUpperCaseCMDLine.Find(ctstrSTART, 0));
	if (nPos < 0){
		bRtnStatus = FALSE;
	} else {
		int nLen = _tcsclen(ctstrSTART);
		strLine.Delete(0, nPos + nLen);
		strLine.TrimLeft();
		strLine.TrimRight();
		
		int nProcNameIndex = -1;
		// First look for a "(".
		if (-1 == (nProcNameIndex = strLine.Find(_T('('), 0))){
			// If a "(" is not found, then look for space.
			// This is an error, since no passed parameters are present.
			nProcNameIndex = strLine.Find(_T(' '), 0);
			bRtnStatus = FALSE;
		}  

		// Extract out the Proc name
		if (nProcNameIndex > 0){
			// Remove the STOL proc name for parsing the passed parameters.
			strLine.Delete(0, nProcNameIndex);
			strLine.TrimLeft();
			strLine.TrimRight();
		}
	}

	strLine.Replace(_T('('), _T(' '));
	strLine.Replace(_T(')'), _T(' '));
	

	if (TRUE == bRtnStatus)
	{
		bRtnStatus = TRUE;  // Assume it was good
		
		while(!strLine.IsEmpty()){
			m_objstrParams.Add(RemoveFirstToken(&strLine));
		}

		// Check number of tokens found againest number expected.
		if (m_objstrParams.GetSize() < eMaxStarMSColumns){
			bRtnStatus = FALSE;
		}
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		
//	Description :	
//					
//	Return :		
//		
//	Parameters :	
//					none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseIntrRpt::CParseIntrRpt(){};
CParseIntrRpt::~CParseIntrRpt(){};

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseIntrRpt::ParseData(CString strStartLine)
//	Description :	This routine will parse the input data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CString parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseIntrRpt::ParseData(CString strLine)
{
	BOOL bRtnStatus = TRUE;

	while(!strLine.IsEmpty()){
		m_objstrParams.Add(RemoveFirstToken(&strLine, _T(' ')));
	}

	if (m_objstrParams.GetCount() != eMaxIntrRptColumns){
		bRtnStatus = FALSE;
	}

	return bRtnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		
//	Description :	
//					
//	Return :		
//		
//	Parameters :	
//					none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CParseCMDFrmNum::CParseCMDFrmNum(){};
CParseCMDFrmNum::~CParseCMDFrmNum(){};
	
/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 09/10/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CParseIntrRpt::ParseData(CString strStartLine)
//	Description :	This routine will parse the input data CString and
//					break up all of the individual pieces into member
//					variables
//	Return :		
//		BOOL	-	TRUE if the CString parsed OK, otherwise
//								FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL	CParseCMDFrmNum::ParseData(CString strLine)
{
	BOOL bRtnStatus = TRUE;

	while(!strLine.IsEmpty()){
		m_objstrCMDFrmNum.Add(RemoveFirstToken(&strLine, _T(' ')));
	}

	return bRtnStatus;
}

