// Parse.h: interface for the CParse class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PARSE_H__2ECE95B3_F15B_11D4_8016_00609704053C__INCLUDED_)
#define AFX_PARSE_H__2ECE95B3_F15B_11D4_8016_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef enum eFrameColumnEnum  {eFrameIndex				= 0,
								eFrameTime				= 1,
								eFrameLabel				= 2,
								eFrameInstrument		= 3,
								eFrameSpacelookMode		= 4,
								eFrameSpacelookSide		= 5,
								eFrameSndrStepMode		= 6,
								eFrameCoordType			= 7,
								eFrameStartNS			= 8,
								eFrameStartEW			= 9,
								eFrameStopNS			= 10,
								eFrameStopEW			= 11,
								eFrameStartCycNS		= 12,
								eFrameStartIncrNS		= 13,
								eFrameStartCycEW		= 14,
								eFrameStartIncrEW		= 15,
								eFrameStopCycNS			= 16,
								eFrameStopIncrNS		= 17,
								eFrameStopCycEW			= 18,
								eFrameStopIncrEW		= 19,
								eFrameDur				= 20,
								eFrameDesc				= 21,
								eMaxFrameColumns		= 22};

typedef enum eSectorColumnEnum	{	eSectorLabel			= 0,
									eSectorInstrument		= 1,
									eSectorSpacelookMode	= 2,
									eSectorSpacelookSide	= 3,
									eSectorSndrStepMode		= 4,
									eSectorCoordType		= 5,
									eSectorStartNS			= 6,
									eSectorStartEW			= 7,
									eSectorStopNS			= 8,
									eSectorStopEW			= 9,
									eSectorDesc				= 10,
									eMaxSectorColumns		= 11};

typedef enum eStarColumnsEnum	{	eStarTime		= 0,
									eStarDuration	= 1,
									eStarWindowNum	= 2,
									eStarInstrument	= 3,
									eStarIMCSet		= 4,
									eStarIndex		= 5,
									eStarID			= 6,
									eStarNumLooks	= 7,
									eStarLookNum	= 8,
									eStarRepeats	= 9,
									eStarCycNS		= 10,
									eStarIncNS		= 11,
									eStarCycEW		= 12,
									eStarIncEW		= 13,
									eMaxStarColumns	= 14};

typedef enum eMapColumnsEnum	{eSTOLMapCurLisLine				= 0,
								 eSTOLMapNextLisLine			= 1,
								 eSTOLMapOBStartIndex			= 2,
								 eSTOLMapOBEndIndex				= 3,
								 eSTOLMapSchedCmd				= 4,
								 eSTOLMapSchedTime				= 5,
								 eSTOLMapCmdExecTime			= 6,
								 eSTOLMapPrevLisLine			= 7,
								 eSTOLMapSchedLine				= 8,
								 eSTOLMaxMapColumns				= 9};
			

typedef enum eMapRtcsColumnsEnum{eRTCSMapCurLisLine			= 0,
								 eRTCSMapNextLisLine		= 1,
								 eRTCSMapOBStartIndex		= 2,
								 eRTCSMapOBEndIndex			= 3,
								 eRTCSMapCmd				= 4,
								 eRTCSMapLabel				= 5,
								 eRTCSMapHexCmdValue		= 6,
								 eRTCSMapCmdExecDuration	= 7,
								 eRTCSMapNumber				= 8,
								 eRTCSMapRamAddress			= 9,
								 eRTCSMapRtcsExecDuration	= 10,
								 eRtcsMaxMapColumns			= 11};

typedef enum eCMDPaceColumnsEnum {eCMDPaceMnemonic01	= 0,
								  eCMDPaceDelay			= 1,
								  eCMDPaceMnemonic02	= 2,
								  eMaxCMDPaceColumns	= 3};



const CString	strInstrument[]		= {_T(""), _T("Imager"), _T("Sounder")};
const int		nMaxInstrument		= 3;
const CString	strSpacelookMode[]	= {_T("N/A-Sounder"),_T("Fast Space"), _T("Slow Space"), _T("Scan")};
const int		nMaxSpacelookMode	= 4;
const CString	strSpacelookSide[]	= {_T("OATS"), _T("East"), _T("West")};
const int		nMaxSpacelookSide	= 3;
const CString	strSndrStepMode[]	= {_T("Normal"), _T("0.2s Dwell"), _T("0.4s Dwell"), _T("Skip"), _T("N/A-Imager")};
const int		nMaxSndrStepMode	= 5;
const CString	strCoordType[]		= {_T(""), _T("Lat/Long"), _T("Line/Pixel"), _T("SRSO Lat/Long")};
const int		nMaxCoordType		= 4;

class CParse  
{
public:
	CParse();
	virtual ~CParse();
	void	SetHdr(CString strHeaderLine1, CString strHeaderLine2);
	void	SetData(CString strData);
	static CString CvtTimeTtoStr(time_t tTime);
	static CString RemoveFirstToken (CString *pstrInput);
	CString RemoveFirstToken (CString *pstrInput, TCHAR ch);
	static int GetInstrumentIndex(CString strTestValue);
	static int GetSpacelookModeIndex(CString strTestValue);
	static int GetSpacelookSideIndex(CString strTestValue);
	static int GetSndrStepModeIndex(CString strTestValue);
	static int GetCoordTypeIndex(CString strTestValue);
protected:
	CString m_strHeaderLine1;
	CString m_strHeaderLine2;
	CString m_strData;
};

class CParseFrame : public CParse
{
public:
	CParseFrame();
	virtual ~CParseFrame();

	// Header Routines and Variables
	float	m_fVersion;
	CString	m_strType;
	int		m_nNumObj;
	int		m_nSRSOFirst;
	time_t	m_tModTime;
	CString	m_strModUser;
	time_t	m_tUpdTime;
	CString	m_strUpdUser;

	BOOL	ParseHdr();
	CString	GetHdrRec1();
	CString GetHdrRec2();
	CString	GetHdrVersion();
	CString	GetHdrNumObj();
	CString	GetHdrSRSOFirst();
	CString	GetHdrModTimeFmt();
	CString	GetHdrModTime();
	CString	GetHdrModUser();
	CString	GetHdrUpdTimeFmt();
	CString	GetHdrUpdTime();
	CString	GetHdrUpdUser();
	void	SetHdrVersion(CString strValue);
	void	SetHdrNumObj(CString strValue);
	void	SetHdrSRSOFirst(CString strValue);
	void	SetHdrModTime(CString strValue);
	void	SetHdrModUser(CString strValue);
	void	SetHdrUpdTime(CString strValue);
	void	SetHdrUpdUser(CString strValue);
	
	// Data Routines and Variables
	int		m_nIndex;
	CString	m_strTime;
	CString	m_strLabel;
	int		m_nInstrument;
	int		m_nSpacelookMode;
	int		m_nSpacelookSide;
	int		m_nSndrStepMode;
	int		m_nCoordType;
	float	m_fStartNS;
	float	m_fStartEW;
	float	m_fStopNS;
	float	m_fStopEW;
	int		m_nStartCycNS;
	int		m_nStartIncNS;
	int		m_nStartCycEW;
	int		m_nStartIncEW;
	int		m_nStopCycNS;
	int		m_nStopIncNS;
	int		m_nStopCycEW;
	int		m_nStopIncEW;
	float	m_fDuration;
	CString	m_strDesc;

	BOOL	ParseData();
	BOOL	HasData();
	CString	GetData();
	CString	GetDataNew(int nIndex);
	CString	GetDataIndex();
	CString	GetDataTime();
	CString	GetDataLabel();
	CString	GetDataInstrument();
	CString	GetDataSpacelookMode();
	CString	GetDataSpacelookSide();
	CString	GetDataSndrStepMode();
	CString	GetDataCoordType();
	CString	GetDataStartNS();
	CString	GetDataStartEW();
	CString	GetDataStopNS();
	CString	GetDataStopEW();
	CString	GetDataStartCycNS();
	CString	GetDataStartIncNS();
	CString	GetDataStartCycEW();
	CString	GetDataStartIncEW();
	CString	GetDataStopCycNS();
	CString	GetDataStopIncNS();
	CString	GetDataStopCycEW();
	CString	GetDataStopIncEW();
	CString	GetDataDuration();
	CString	GetDataDesc();
	CString GetData(int nDataTypeToGet);

	void	SetDataIndex(CString strValue);
	void	SetDataTime(CString strValue);
	void	SetDataLabel(CString strValue);
	void	SetDataInstrument(CString strValue);
	void	SetDataSpacelookMode(CString strValue);
	void	SetDataSpacelookSide(CString strValue);
	void	SetDataSndrStepMode(CString strValue);
	void	SetDataCoordType(CString strValue);
	void	SetDataStartNS(CString strValue);
	void	SetDataStartEW(CString strValue);
	void	SetDataStopNS(CString strValue);
	void	SetDataStopEW(CString strValue);
	void	SetDataStartCycNS(CString strValue);
	void	SetDataStartIncNS(CString strValue);
	void	SetDataStartCycEW(CString strValue);
	void	SetDataStartIncEW(CString strValue);
	void	SetDataStopCycNS(CString strValue);
	void	SetDataStopIncNS(CString strValue);
	void	SetDataStopCycEW(CString strValue);
	void	SetDataStopIncEW(CString strValue);
	void	SetDataDuration(CString strValue);
	void	SetDataDesc(CString strValue);
};

class CParseStar : public CParse
{
public:
	CParseStar();
	virtual ~CParseStar();
	// Header Routines and Variables
	float	m_fVersion;
	CString	m_strType;
	int		m_nNumObj;
	time_t	m_tModTime;
	CString	m_strModUser;

	BOOL	ParseHdr();
	CString	GetHdrRec1();
	CString GetHdrRec2();
	CString	GetHdrVersion();
	CString	GetHdrNumObj();
	CString	GetHdrModTimeFmt();
	CString	GetHdrModTime();
	CString	GetHdrModUser();
	void	SetHdrVersion(CString strValue);
	void	SetHdrNumObj(CString strValue);
	void	SetHdrModTime(CString strValue);
	void	SetHdrModUser(CString strValue);

	// Data Routines and Variables
	CString	m_strTime;
	CString m_strDuration;
	int		m_nWindowNum;
	int		m_nInstrument;
	CString	m_strIMCSet;
	int		m_nIndex;
	int		m_nID;
	int		m_nNumLooks;
	int		m_nLookNum;
	int		m_nRepeats;
	int		m_nCycNS;
	int		m_nIncNS;
	int		m_nCycEW;
	int		m_nIncEW;

	BOOL	ParseData();
	BOOL	HasData();
	CString	GetData();
	CString	GetDataNew(int nIndex);
	CString	GetDataTime();
	CString	GetDataDuration();
	CString	GetDataWindowNum();
	CString	GetDataInstrument();
	CString	GetDataIMCSet();
	CString	GetDataIndex();
	CString	GetDataID();
	CString	GetDataNumLooks();
	CString	GetDataLookNum();
	CString GetDataRepeats();
	CString	GetDataCycNS();
	CString	GetDataIncNS();
	CString	GetDataCycEW();
	CString	GetDataIncEW();
	CString GetData(int nDataTypeToGet);
	void	SetDataTime(CString strValue);
	void	SetDataDuration(CString strValue);
	void	SetDataWindowNum(CString strValue);
	void	SetDataInstrument(CString strValue);
	void	SetDataIMCSet(CString strValue);
	void	SetDataIndex(CString strValue);
	void	SetDataID(CString strValue);
	void	SetDataNumLooks(CString strValue);
	void	SetDataLookNum(CString strValue);
	void	SetDataRepeats(CString strValue);
	void	SetDataCycNS(CString strValue);
	void	SetDataIncNS(CString strValue);
	void	SetDataCycEW(CString strValue);
	void	SetDataIncEW(CString strValue);
};

class CParseSector : public CParse
{
public:
	CParseSector();
	virtual ~CParseSector();

	// Header Routines and Variables
	float	m_fVersion;
	CString	m_strType;
	int		m_nNumObj;
	time_t	m_tModTime;
	CString	m_strModUser;

	BOOL	ParseHdr();
	CString	GetHdrRec1();
	CString GetHdrRec2();
	CString	GetHdrVersion();
	void	SetHdrVersion(CString strValue);
	CString	GetHdrNumObj();
	void	SetHdrNumObj(CString strValue);
	CString	GetHdrModTimeFmt();
	CString	GetHdrModTime();
	void	SetHdrModTime(CString strValue);
	CString	GetHdrModUser();
	void	SetHdrModUser(CString strValue);

	// Data Routines and Variables
	CString	m_strLabel;
	int		m_nInstrument;
	int		m_nSpacelookMode;
	int		m_nSpacelookSide;
	int		m_nSndrStepMode;
	int		m_nCoordType;
	float	m_fStartNS;
	float	m_fStartEW;
	float	m_fStopNS;
	float	m_fStopEW;
	CString m_strDesc;

	BOOL	ParseData();
	BOOL	HasData();
	CString	GetData();
	CString	GetDataNew();
	CString	GetDataLabel();
	CString	GetDataInstrument();
	CString	GetDataSpacelookMode();
	CString	GetDataSpacelookSide();
	CString	GetDataSndrStepMode();
	CString	GetDataCoordType();
	CString	GetDataStartNS();
	CString	GetDataStartEW();
	CString	GetDataStopNS();
	CString	GetDataStopEW();
	CString	GetDataDesc();
	CString GetData(int nDataTypeToGet);
	void	SetDataLabel(CString strValue);
	void	SetDataInstrument(CString strValue);
	void	SetDataSpacelookMode(CString strValue);
	void	SetDataSpacelookSide(CString strValue);
	void	SetDataSndrStepMode(CString strValue);
	void	SetDataCoordType(CString strValue);
	void	SetDataStartNS(CString strValue);
	void	SetDataStartEW(CString strValue);
	void	SetDataStopNS(CString strValue);
	void	SetDataStopEW(CString strValue);
	void	SetDataDesc(CString strValue);
};

class CParseSTOLMap : public CParse
{
public:
	CParseSTOLMap();
	virtual ~CParseSTOLMap();

	// Header Routines and Variables
	float	m_fVersion;
	CString	m_strType;
	int		m_nNumObj;
	time_t	m_tModTime;
	CString	m_strModUser;

	BOOL	ParseHdr();
	CString	GetHdrRec1();
	CString GetHdrRec2();
	CString	GetHdrVersion();
	void	SetHdrVersion(CString strValue);
	CString	GetHdrNumObj();
	void	SetHdrNumObj(CString strValue);
	CString	GetHdrModTimeFmt();
	CString	GetHdrModTime();
	void	SetHdrModTime(CString strValue);
	CString	GetHdrModUser();
	void	SetHdrModUser(CString strValue);

	// Data Routines and Variables
	int		m_nLisLine;
	int		m_nNextLisLine;
	int		m_nOBStartIndex;
	int		m_nOBEndIndex;
	CString m_strSchedCmd;
	CString m_strExecTime;
	float	m_fCmdExecDuration;
	int		m_nPrevLisLine;
	int		m_nSchedLine;

	BOOL	ParseData();
	CString	GetData();
	CString	GetDataLisLine();
	CString	GetDataNextLisLine();
	CString	GetDataOBStartIndex();
	CString	GetDataOBEndIndex();
	CString	GetDataSchedCmd();
	CString	GetDataExecTime();
	CString	GetDataCmdExecDuration();
	CString	GetDataPrevLisLine();
	CString GetDataSchedLine();
	CString GetData(int nDataTypeToGet);
	void	SetDataLisLine(CString strValue);
	void	SetDataNextLisLine(CString strValue);
	void	SetDataOBStartIndex(CString strValue);
	void	SetDataOBEndIndex(CString strValue);
	void	SetDataSchedCmd(CString strValue);
	void	SetDataExecTime(CString strValue);
	void	SetDataCmdExecDuration(CString strValue);
	void	SetDataPrevLisLine(CString strValue);
	void	SetDataSchedLine(CString strValue);
};

class CParseRTCSMap : public CParse
{
public:
	CParseRTCSMap();
	virtual ~CParseRTCSMap();

	// Header Routines and Variables
	float	m_fVersion;
	int		m_nNumObj;
	time_t	m_tModTime;
	CString	m_strType;
	CString	m_strModUser;

	BOOL	ParseHdr();
	CString	GetHdrRec1();
	CString GetHdrRec2();
	CString	GetHdrVersion();
	CString	GetHdrNumObj();
	CString	GetHdrModTimeFmt();
	CString	GetHdrModTime();
	CString	GetHdrModUser();
	void	SetHdrVersion(CString strValue);
	void	SetHdrNumObj(CString strValue);
	void	SetHdrModTime(CString strValue);
	void	SetHdrModUser(CString strValue);

	// Data Routines and Variables
	
	int		m_nLisLine;
	int		m_nNextLisLine;
	int		m_nOBStartIndex;
	int		m_nOBEndIndex;
	CString m_strCmd;
	CString m_strLabel;
	CString m_strHexCmdValue;
	CString m_strRtcsExecDuration;
	CString m_strRamAddress;
	float	m_fCmdExecDuration;
	int		m_nNumber;
	
	BOOL	ParseData();
	CString	GetData();
	CString	GetDataLisLine();
	CString	GetDataNextLisLine();
	CString GetDataRamAddress();
	CString	GetDataOBStartIndex();
	CString	GetDataOBEndIndex();
	CString	GetDataCmd();
	CString	GetDataLabel();
	CString	GetDataHexCmdValue();
	CString	GetDataCmdExecDuration();
	CString GetDataRtcsExecDuration();
	CString GetDataNumber();
	CString GetData(int nDataTypeToGet);
	void	SetDataLisLine(CString strValue);
	void	SetDataEndLisLine(CString strValue);
	void	SetDataRamAddress(CString strValue);
	void	SetDataOBStartIndex(CString strValue);
	void	SetDataOBEndIndex(CString strValue);
	void	SetDataCmd(CString strValue);
	void	SetDataLabel(CString strValue);
	void	SetDataHexCmdValue(CString strValue);
	void	SetDataCmdExecDuration(CString strValue);
	void	SetDataRtcsExecDuration(CString strValue);
	void	SetDataNumber(CString strValue);
};

class CParseCMDPace : public CParse
{
public:
	CParseCMDPace();
	virtual ~CParseCMDPace();

	// Header Routines and Variables
	float	m_fVersion;
	CString	m_strType;
	int		m_nNumObj;
	CString	m_strModTime;
	CString	m_strModUser;

	BOOL	ParseHdr();
	float	GetHdrVersion(){return m_fVersion;};
	void	SetHdrVersion(float fValue){m_fVersion = fValue;};
	int		GetHdrNumObj(){return m_nNumObj;};
	void	SetHdrNumObj(int nValue){m_nNumObj = nValue;};
	CString	GetHdrModTime(){return m_strModTime;};
	void	SetHdrModTime(CString strValue){m_strModTime = strValue;};
	CString	GetHdrModUser(){return m_strModUser;};
	void	SetHdrModUser(CString strValue){m_strModUser = strValue;};
	CString GetHdrRec1();
	CString GetHdrRec2();

	// Data Routines and Variables
	
	CString		m_strCMDPaceMnemonic01;
	float		m_fCMDPaceDelay;
	CString		m_strCMDPaceMnemonic02;

	BOOL	ParseData();
	CString	GetData();
	CString	GetDataCMDPaceMnemonic01(){return m_strCMDPaceMnemonic01;};
	float	GetDataCMDPaceDelay(){return m_fCMDPaceDelay;};
	CString	GetDataCMDPaceMnemonic02(){return m_strCMDPaceMnemonic02;};
	void	SetDataCMDPaceMnemonic01(CString strValue){m_strCMDPaceMnemonic01 = strValue;};
	void	SetDataCMDPaceDelay(float fValue){m_fCMDPaceDelay = fValue;};
	void	SetDataCMDPaceMnemonic02(CString strValue){m_strCMDPaceMnemonic02 =  strValue;};
};


class CParseFrameIOMS : public CParse
{

typedef enum eFrameIOMSColumnsEnum {eFrameIOMS_INDEX	 = 0,
									eFrameIOMS_PRIOR	 = 1,
									eFrameIOMS_SCN_MODE  = 2,
									eFrameIOMS_INST		 = 3,
									eFrameIOMS_STATUS	 = 4,
									eFrameIOMS_REPEATS   = 5,
									eFrameIOMS_SND_BBCAL = 6,
									eFrameIOMS_SPL_REQ   = 7,
									eFrameIOMS_RT_ADDR	 = 8,
									eMaxFrameIOMSColumns = 9};
public:
	CParseFrameIOMS();
	virtual ~CParseFrameIOMS();

	// Data Routines and Variables

	BOOL	ParseData(CString strStartLine);
	CString	GetRT_ADDR(){return m_objstrParams[eFrameIOMS_RT_ADDR];};
	CString	GetINST(){return m_objstrParams[eFrameIOMS_INST];};
	CString	GetINDEX(){return m_objstrParams[eFrameIOMS_INDEX];};
	CString	GetREPEATS(){return m_objstrParams[eFrameIOMS_REPEATS];};
	CString	GetSPLSIDE(){return m_objstrParams[eFrameIOMS_SPL_REQ];};
	CString	GetPRIOR(){return m_objstrParams[eFrameIOMS_PRIOR];};
	CString	GetSCN_MODE(){return m_objstrParams[eFrameIOMS_SCN_MODE];};
	CString	GetSTATUS(){return m_objstrParams[eFrameIOMS_STATUS];};

private:

	CStringArray m_objstrParams;

};

class CParseFrameMS : public CParse
{

typedef enum eFrameMSColumnsEnum {eFrameMS_INST		 = 0,
								  eFrameMS_SPLSIDE   = 1,
								  eFrameMS_BBCAL	 = 2,	
                                  eFrameMS_SMODE	 = 3,
								  eFrameMS_PRIOR	 = 4,
								  eFrameMS_REPT      = 5,
								  eFrameMS_X1CYC     = 6,
					              eFrameMS_X1INC     = 7,
								  eFrameMS_X2CYC     = 8,
								  eFrameMS_X2INC     = 9,
								  eFrameMS_Y1CYC     = 10,
								  eFrameMS_Y1INC     = 11,
								  eFrameMS_Y2CYC     = 12,
								  eFrameMS_Y2INC     = 13,
								  eMaxFrameMSColumns = 14};
public:
	CParseFrameMS();
	virtual ~CParseFrameMS();

	// Data Routines and Variables

	BOOL	ParseData(CString strStartLine);
  	CString	GetINST(){return m_objstrParams[eFrameMS_INST];};
	CString	GetSPLSIDE(){return m_objstrParams[eFrameMS_SPLSIDE];};
	CString	GetREPEATS(){return m_objstrParams[eFrameMS_REPT];};
	CString	GetX1CYC(){return m_objstrParams[eFrameMS_X1CYC];};
	CString	GetX1INC(){return m_objstrParams[eFrameMS_X1INC];};
	CString	GetX2CYC(){return m_objstrParams[eFrameMS_X2CYC];};
	CString	GetX2INC(){return m_objstrParams[eFrameMS_X2INC];};
	CString	GetY1CYC(){return m_objstrParams[eFrameMS_Y1CYC];};
	CString	GetY1INC(){return m_objstrParams[eFrameMS_Y1INC];};
	CString	GetY2CYC(){return m_objstrParams[eFrameMS_Y2CYC];};
	CString	GetY2INC(){return m_objstrParams[eFrameMS_Y2INC];};
	CString	GetBBCAL(){return m_objstrParams[eFrameMS_BBCAL];};
	CString	GetSMODE(){return m_objstrParams[eFrameMS_SMODE];};
	CString	GetPRIOR(){return m_objstrParams[eFrameMS_PRIOR];};
    CString	GetMS_REPT(){return m_objstrParams[eFrameMS_REPT];}; 

private:

	CStringArray m_objstrParams;

};


class CParseStarIOMS : public CParse
{

	typedef enum eStarIOMSColumnsEnum { eStarIOMS_INDEX	     = 0,
										eStarIOMS_INST	     = 1,
										eStarIOMS_STAR_CLASS = 2,
										eStarIOMS_LOADREG    = 3,
										eStarIOMS_RT_ADDR    = 4,
										eMaxStarIOMSColumns  = 5};
public:
	CParseStarIOMS();
	virtual ~CParseStarIOMS();

	// Data Routines and Variables

	BOOL	ParseData(CString strStartLine);
  	CString	GetRT_ADDR(){return m_objstrParams[eStarIOMS_RT_ADDR];};
	CString	GetINST(){return m_objstrParams[eStarIOMS_INST];};
	CString	GetINDEX(){return m_objstrParams[eStarIOMS_INDEX];};
	CString	GetLOADREG(){return m_objstrParams[eStarIOMS_LOADREG];};
							
private:
	CStringArray m_objstrParams;

};

class CParseStarMS : public CParse
{

typedef enum eStarMSColumnsEnum {eStarMS_INST	   = 0,
								 eStarMS_STIME	   = 1,
								 eStarMS_REPS	   = 2,	
                                 eStarMS_STARBIT   = 3,
								 eStarMS_XCYC	   = 4,
								 eStarMS_XINC      = 5,
								 eStarMS_YCYC      = 6,
								 eStarMS_YINC      = 7,
							     eMaxStarMSColumns = 8};

public:
	CParseStarMS();
	virtual ~CParseStarMS();

	// Data Routines and Variables

	BOOL	ParseData(CString strStartLine);
  	CString	GetINST(){return m_objstrParams[eStarMS_INST];};
	CString	GetSTIME(){return m_objstrParams[eStarMS_STIME];};
	CString	GetXCYC(){return m_objstrParams[eStarMS_XCYC];};
	CString	GetXINC(){return m_objstrParams[eStarMS_XINC];};
	CString	GetYCYC(){return m_objstrParams[eStarMS_YCYC];};
	CString	GetYINC(){return m_objstrParams[eStarMS_YINC];};
	CString	GetRECUR(){return m_objstrParams[eStarMS_REPS];};
 	CString	GetSTARBIT(){return m_objstrParams[eStarMS_STARBIT];};

private:

	CStringArray m_objstrParams;

};


class CParseIntrRpt : public CParse
{

typedef enum eIntrRptColumnsEnum {eIntrRpt_Sensor	 = 0,
						   		  eIntrRpt_Body	     = 1,
								  eIntrRpt_Edge	     = 2,	
                                  eIntrRpt_Start     = 3,
								  eIntrRpt_Stop	     = 4,
							      eMaxIntrRptColumns = 5};

public:
	CParseIntrRpt();
	virtual ~CParseIntrRpt();

	// Data Routines and Variables

	BOOL	ParseData(CString strStartLine);
  	CString	GetSensor(){return m_objstrParams[eIntrRpt_Sensor];};
	CString	GetBody(){return m_objstrParams[eIntrRpt_Body];};
	CString	GetEdge(){return m_objstrParams[eIntrRpt_Edge];};
	CString	GetStart(){return m_objstrParams[eIntrRpt_Start];};
	CString	GetStop(){return m_objstrParams[eIntrRpt_Stop];};

private:

	CStringArray m_objstrParams;
};


class CParseCMDFrmNum : public CParse
{

typedef enum eCMDFrmNumColumnsEnum {eCMDFrmNum_Mnemonic = 0,
						   		    eCMDFrmNum_Format	= 1,
								    eCMDFrmNum_NumFrms  = 2,	
                                    eMaxCMDFrmNumRptColumns  = 3};

public:
	CParseCMDFrmNum();
	virtual ~CParseCMDFrmNum();

	// Data Routines and Variables

	BOOL	ParseData(CString strStartLine);
  	CString	GetMnemonic(){return m_objstrCMDFrmNum[eCMDFrmNum_Mnemonic];};
	CString	GetFormat(){return m_objstrCMDFrmNum[eCMDFrmNum_Format];};
	CString	GetNumFrms(){return m_objstrCMDFrmNum[eCMDFrmNum_NumFrms];};

private:

	CStringArray m_objstrCMDFrmNum;
};


#endif // !defined(AFX_PARSE_H__2ECE95B3_F15B_11D4_8016_00609704053C__INCLUDED_)


