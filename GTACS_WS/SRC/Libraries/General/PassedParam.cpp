/***************************
// PassedParam.cpp: implementation of the CPassedParam class.
// (c) 2001 Frederick J. Shaw                                       // 
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
***************************/

#include "stdafx.h"
#include "PassedParam.h"

/***************************
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//	Author :	Fred Shaw			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CPassedParam::CPassedParam
//
//	Description :	 Class Constructor
//					
//	Return :		
//					void	-	none
//	Parameters :	
//					void	-	none
//		
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CPassedParam::CPassedParam()
{
	m_nSize = 0;
	m_nIndex = 0;
}


/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :	Fred Shaw			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CPassedParam::Add
//	Description :	Adds a parameter name to the array.
//					
//					
//	Return :		BOOL	
//						TRUE	-	SUCCESS
//						FALSE	-	FAIL
//					
//	Parameters :	
//					const CString strTemp	-	
//						parameter name and value pair
//		
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CPassedParam::Add(const CString strTemp){

	BOOL eReturnStatus = TRUE;
	
	// Find where passed parameter name ends.
	// nPos = strTemp.Find('=', 0);
	m_strParamName.Add(strTemp);

	// Remove trailing characters.
	// Only the name of the passed parameter portion
	// remains in the CString 
	int nPos = m_strParamName[m_nIndex].Find('=', 0);
	int nLen = m_strParamName[m_nIndex].GetLength() - nPos;

	if ((-1 != nPos) && (0 != nLen)){
		m_strParamName[m_nIndex].Delete(nPos, nLen);
		m_strParamName[m_nIndex].TrimLeft();
		m_strParamName[m_nIndex].TrimRight();
	//	m_strParamName[m_nIndex].Insert(0, '$');
		m_strParamName[m_nIndex].MakeUpper();
	
		// Copy the value
		m_strParamValue.Add(strTemp);
		m_strParamValue[m_nIndex].TrimLeft();
		m_strParamValue[m_nIndex].TrimRight();
		nPos = m_strParamValue[m_nIndex].Find('=', 0);
		m_strParamValue[m_nIndex].Delete(0, nPos + 1);
		m_strParamValue[m_nIndex].TrimLeft();
		m_strParamValue[m_nIndex].TrimRight();
			
		m_nIndex++;
		m_nSize = m_strParamName.GetSize();

	} else {
		eReturnStatus = FALSE;
	}

	return eReturnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :	Fred Shaw			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CPassedParam::Process_PP
//	Description :	Processes parameters being passed into the STOL 
//					procedure. 
//					
//	Return :		BOOL	
//						TRUE	-	SUCCESS
//						FALSE	-	FAIL
//					
//	Parameters :	
//					const CString strTemp	-	
//						Parameters being passed into STOL proc
//		
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CPassedParam::Process_PP(CString strTemp){
	strTemp.TrimRight();
	strTemp.TrimLeft();
	BOOL eReturnStatus = TRUE;
//	bool bDone = false;

	int nEquPos = 0;
	while( -1 != (nEquPos = strTemp.Find('=', 0))){
		int nPos = 0;
		CString strBuffer(strTemp);
		// Find where passed parameter ends.
		if (-1 != (nPos = (strBuffer.Find(_T(" "), 0)))){
			// Extract the passed parameter
			int nLen = strBuffer.GetLength() - nPos;
			strBuffer.Delete(nPos, nLen);
			// strBuffer.TrimLeft();
			strBuffer.TrimRight();
			// strBuffer.MakeUpper();
		} 
		
		if (TRUE != Add(strBuffer)){
			eReturnStatus = FALSE;
		}
		
		strTemp.TrimLeft();
		nPos = strTemp.Find(_T(" "), 0);
		if (nPos < 0) nPos = strTemp.GetLength();
		strTemp.Delete(0, nPos);
		strTemp.TrimLeft();
		strTemp.TrimRight();
	}
	
	return eReturnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :	Fred Shaw			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CPassedParam::Replace_PP(CString &strTemp){
//	Description :	Replaces passed parameter on a directive line. 
//					 
//					
//	Return :		BOOL	
//						Always returns true.
//					
//	Parameters :	
//					CString &strTemp	-	
//						Directive line which will have the passed
//						parameters replaced.
//		
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CPassedParam::Replace_PP(CString &strTemp){
	
	BOOL eReturnStatus = TRUE;

	for (int i=0; i < m_strParamName.GetSize(); i++){
		m_strParamName[i].MakeUpper();
		strTemp.Replace(m_strParamName[i], m_strParamValue[i]);
		m_strParamName[i].MakeLower();
		strTemp.Replace(m_strParamName[i], m_strParamValue[i]);	
	}
	return eReturnStatus;
}
