// PassedParam.h: interface for the CPassedParam class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PASSEDPARAM_H__ED0953F5_3DD4_11D3_9771_00104B8CF942__INCLUDED_)
#define AFX_PASSEDPARAM_H__ED0953F5_3DD4_11D3_9771_00104B8CF942__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CPassedParam  
{
public:
	CPassedParam();
	virtual ~CPassedParam(){};

	BOOL Process_PP(CString strTemp);
	BOOL Replace_PP(CString &strTemp);
    CString &GetParamName(int i){return m_strParamName[i];};
    CString &GetParamValue(int i){return m_strParamValue[i];};
	CStringArray &GetParamName(void){return m_strParamName;};
    CStringArray &GetParamValue(void){return m_strParamValue;};
	int GetParamSize(void){return m_strParamName.GetSize();};
	void Remove(int nIndex){m_strParamName.RemoveAt(nIndex); m_strParamValue.RemoveAt(nIndex); m_nSize--;};  

private:
	int m_nSize;
	CStringArray m_strParamValue;
	CStringArray m_strParamName;
	int m_nIndex;

	BOOL Add(const CString strTemp);
};

#endif // !defined(AFX_PASSEDPARAM_H__ED0953F5_3DD4_11D3_9771_00104B8CF942__INCLUDED_)
