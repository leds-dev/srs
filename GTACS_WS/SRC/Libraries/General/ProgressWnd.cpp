/**
////////////////////////////////////////////////////////
// ProgressWnd.cpp : implementation file
//
// Written by Chris Maunder (cmaunder@mail.com)
// Copyright 1998.
//
// CProgressWnd is a drop-in popup progress window for use in
// programs that a time consuming. Check out the header file
// or the accompanying HTML doc file for details.
//
// This code may be used in compiled form in any way you desire. This
// file may be redistributed by any means PROVIDING it is not sold for
// profit without the authors written consent, and providing that this
// notice and the authors name is included. If the source code in 
// this file is used in any commercial application then an email to
// the me would be nice.
//
// This file is provided "as is" with no expressed or implied warranty.
// The author accepts no liability if it causes any damage to your
// computer, causes your pet cat to fall ill, increases baldness or
// makes you car start emitting strange noises when you start it up.
//
// Expect bugs.
// 
// Please use and enjoy. Please let me know of any bugs/mods/improvements 
// that you have found/implemented and I will fix/incorporate them into this
// file. 
//
// Updated May 18 1998 - added PeekAndPump function to allow modal operation,
//                       with optional "Cancel on ESC" (Michael <mbh-ep@post5.tele.dk>)
//         Nov 27 1998 - Removed modal stuff from PeekAndPump
//         Dec 18 1998 - added WS_EX_TOPMOST to the creation flag
**/


#include "StdAfx.h"
#include "ProgressWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define IDC_CANCEL   10
#define IDC_TEXT     11
#define IDC_PROGRESS 12

LPCTSTR szSection = _T("Settings");   
LPCTSTR szEntryX  = _T("X");
LPCTSTR szEntryY  = _T("Y");

/**
////////////////////////////////////////////////////////////////////////////
//	Author:				Date:				version:
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::CProgressWnd()
//					 	
//	Description :	Constructor   	
//                
//	Return :	void	-	none
//						
//	Parameters :	void	-	none
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
CProgressWnd::CProgressWnd()
{
    CommonConstruct();
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author:		Date : 			version:
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::CProgressWnd
//					 	
//	Description :	Constructor   	
//                
//	Return :	void	-	 none
//						
//	Parameters :
//				CWnd* pParent	-		pointer to parent window
//				LPCTSTR pszTitle	-	Progress window title	
//				BOOL bSmooth	-	Smooth scrolling?
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
CProgressWnd::CProgressWnd(CWnd* pParent, LPCTSTR pszTitle, BOOL bSmooth /* = FALSE */)
{
    CommonConstruct();
    m_strTitle = pszTitle;

    Create(pParent, pszTitle, bSmooth);
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author :			Date :			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::CommonConstruct()
//					 	
//	Description :	Common Constructor   	
//                
//	Return :	void	-	none
//						
//	Parameters :	void	-	none
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
void CProgressWnd::CommonConstruct()
{
    m_nNumTextLines        = 2;
    m_nOverallPrevPos      = 0;
    m_nSpecificPrevPos     = 0;
    m_nOverallPrevPercent  = 0;
    m_nSpecificPrevPercent = 0;
    m_nOverallStep	       = 1;
    m_nSpecificStep        = 1;
    m_nOverallMinValue     = 0;
    m_nSpecificMinValue    = 0;
    m_nOverallMaxValue     = 100;
    m_nSpecificMaxValue    = 100;

    m_strTitle       = _T("Progress");
    m_strCancelLabel = _T(" Cancel ");
    m_bCancelled     = FALSE;
    m_bModal         = FALSE;

    m_bPersistantPosition = TRUE;   // saves and restores position automatically
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author :		Date : 			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::~CProgressWnd()
//					 	
//	Description :	Destructor   	
//                
//	Return :	
//				void	-	none
//						
//	Parameters :
//				void	-	none
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
CProgressWnd::~CProgressWnd()
{
    DestroyWindow();
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : 		Date : 				version:
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::Create
//					 	
//	Description :	   	
//                
//	Return :		BOOL
//						
//	Parameters :
//					CWnd* pParent,
//					LPCTSTR pszTitle,
//					BOOL bSmooth 
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
BOOL CProgressWnd::Create(CWnd* pParent, LPCTSTR pszTitle, BOOL bSmooth /* = FALSE */)
{
    BOOL bSuccess;

    // Register window class
    CString csClassName = AfxRegisterWndClass(CS_OWNDC|CS_HREDRAW|CS_VREDRAW,
                                              ::LoadCursor(NULL, IDC_APPSTARTING),
                                              CBrush(::GetSysColor(COLOR_BTNFACE)));

    // Get the system window message font for use in the cancel button and text area
    NONCLIENTMETRICS ncm;
    ncm.cbSize = sizeof(NONCLIENTMETRICS);
    VERIFY(SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &ncm, 0));
    m_font.CreateFontIndirect(&(ncm.lfMessageFont)); 

    // If no parent supplied then try and get a pointer to it anyway
    if (!pParent)
        pParent = AfxGetMainWnd();

    // Create popup window
    bSuccess = CreateEx(WS_EX_DLGMODALFRAME|WS_EX_TOPMOST, // Extended style
                        csClassName,                       // Classname
                        pszTitle,                          // Title
                        WS_POPUP|WS_BORDER|WS_CAPTION,     // style
                        0,0,                               // position - updated soon.
                        390,130,                           // Size - updated soon
                        pParent->GetSafeHwnd(),            // handle to parent
                        0,                                 // No menu
                        NULL);    
    if (!bSuccess) return FALSE;

    // Now create the controls
    CRect TempRect(0,0,10,10);

    bSuccess = m_cOverallText.Create(_T(""), WS_CHILD|WS_VISIBLE|SS_NOPREFIX|SS_LEFTNOWORDWRAP,
									 TempRect, this, IDC_TEXT);
    if (!bSuccess) return FALSE;

    bSuccess = m_cSpecificText.Create(_T(""), WS_CHILD|WS_VISIBLE|SS_NOPREFIX|SS_LEFTNOWORDWRAP,
									 TempRect, this, IDC_TEXT);
    if (!bSuccess) return FALSE;

    DWORD dwProgressStyle = WS_CHILD|WS_VISIBLE;
#ifdef PBS_SMOOTH    
    if (bSmooth)
       dwProgressStyle |= PBS_SMOOTH;
#endif
    bSuccess = m_wndSpecificProgress.Create(dwProgressStyle, TempRect, this, IDC_PROGRESS);
    if (!bSuccess) return FALSE;

    bSuccess = m_wndOverallProgress.Create(dwProgressStyle, TempRect, this, IDC_PROGRESS);
    if (!bSuccess) return FALSE;

    bSuccess = m_CancelButton.Create(m_strCancelLabel, 
                                     WS_CHILD|WS_VISIBLE|WS_TABSTOP| BS_PUSHBUTTON, 
                                     TempRect, this, IDC_CANCEL);
    if (!bSuccess) return FALSE;

    m_CancelButton.SetFont(&m_font, TRUE);
    m_cOverallText.SetFont(&m_font, TRUE);
    m_cSpecificText.SetFont(&m_font, TRUE);

    // Resize the whole thing according to the number of text lines, desired window
    // width and current font.
    SetWindowSize(m_nNumTextLines, 390);

    // Center and show window
    if (m_bPersistantPosition)
        GetPreviousSettings();
    else
        CenterWindow();

    Show();

    return TRUE;
}

/**
////////////////////////////////////////////////////////////////////
//	Author :		Date : 			version 
////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::GoModal
//					 	
//	Description :	   	
//                
//	Return :		BOOL
//						
//	Parameters :
//					LPCTSTR pszTitle 
//					BOOL bSmooth 
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
BOOL CProgressWnd::GoModal(LPCTSTR pszTitle /*=_T("Progress")"*/, BOOL bSmooth /*=FALSE*/)
{
    CWnd *pMainWnd = AfxGetMainWnd();

    if (!::IsWindow(m_hWnd) && !Create(pMainWnd, pszTitle, bSmooth))
        return FALSE;

    // Disable main window
    if (pMainWnd)
        pMainWnd->EnableWindow(FALSE);

    // Re-enable this window
    EnableWindow(TRUE);

    m_bModal = TRUE;

    return TRUE;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author :			Date :			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :	CProgressWnd::SetWindowSize
//					 	
//	Description :	  	
//                
//	Return :
//						
//	Parameters :
//					int nNumTextLines,
//					int nWindowWidth                
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
void CProgressWnd::SetWindowSize(int nNumTextLines, int nWindowWidth /*=390*/)
{
    int nMargin = 10;
    CSize EdgeSize(::GetSystemMetrics(SM_CXEDGE), ::GetSystemMetrics(SM_CYEDGE));

    CRect OverallTextRect;
	CRect SpecificTextRect;
	CRect CancelRect;
	CRect SpecificProgressRect;
	CRect OverallProgressRect;
    CSize CancelSize;

    // Set up a default size for the text area in case things go wrong
    OverallTextRect.SetRect(nMargin, nMargin, nWindowWidth-2*nMargin, 100+2*nMargin);
    SpecificTextRect.SetRect(nMargin, nMargin, nWindowWidth-2*nMargin, 100+2*nMargin);

    // Get DrawText to tell us how tall the text area will be (while we're at
    // it, we'll see how big the word "Cancel" is)
    CDC* pDC = GetDC();
    if (pDC)
	{
        CFont* pOldFont = pDC->SelectObject(&m_font);
        CString str = _T("M");
        for (int i = 0; i < nNumTextLines-1; i++) str += _T("\nM");
        pDC->DrawText(str, OverallTextRect, DT_CALCRECT|DT_NOCLIP|DT_NOPREFIX);
        pDC->DrawText(str, SpecificTextRect, DT_CALCRECT|DT_NOCLIP|DT_NOPREFIX);
        OverallTextRect.right = OverallTextRect.left + nWindowWidth;
        SpecificTextRect.right = SpecificTextRect.left + nWindowWidth;
        CancelSize = pDC->GetTextExtent(m_strCancelLabel + _T("  ")) +
										CSize(EdgeSize.cx*4, EdgeSize.cy*3);
        pDC->SelectObject(pOldFont);
        ReleaseDC(pDC);
    }
    
	int nTextHeight		= OverallTextRect.Height();
	int nProgressHeight = CancelSize.cy;

	OverallProgressRect.SetRect(OverallTextRect.left,
								OverallTextRect.bottom + nMargin,
								OverallTextRect.right,
								OverallTextRect.bottom + nMargin + nProgressHeight);
    
    SpecificTextRect.SetRect(OverallProgressRect.left,
							 OverallProgressRect.bottom + nMargin, 
							 OverallProgressRect.right,
							 OverallProgressRect.bottom + nMargin + nTextHeight);

	SpecificProgressRect.SetRect(SpecificTextRect.left,
								 SpecificTextRect.bottom + nMargin,
							 	 SpecificTextRect.right,
								 SpecificTextRect.bottom + nMargin + nProgressHeight);

    CancelRect.SetRect(SpecificProgressRect.right - CancelSize.cx - (nMargin),
					   SpecificProgressRect.bottom + nMargin,
					   SpecificProgressRect.right,
					   SpecificProgressRect.bottom + CancelSize.cy + (nMargin));


    // Resize the main window to fit the controls
    CSize ClientSize(nMargin + OverallTextRect.Width() + nMargin,
                     nMargin + OverallTextRect.Height() +
					 nMargin + OverallProgressRect.Height() +
					 nMargin + SpecificTextRect.Height() + 
					 nMargin + SpecificProgressRect.Height() +
					 nMargin + CancelRect.Height() + nMargin);

    CRect WndRect, ClientRect;
    GetWindowRect(WndRect); GetClientRect(ClientRect);
    WndRect.right = WndRect.left + WndRect.Width()-ClientRect.Width()+ClientSize.cx;
    WndRect.bottom = WndRect.top + WndRect.Height()-ClientRect.Height()+ClientSize.cy;
    MoveWindow(WndRect);

    // Now reposition the controls...
    m_wndOverallProgress.MoveWindow(OverallProgressRect);
    m_wndSpecificProgress.MoveWindow(SpecificProgressRect);
    m_CancelButton.MoveWindow(CancelRect);
    m_cOverallText.MoveWindow(OverallTextRect);
    m_cSpecificText.MoveWindow(SpecificTextRect);
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author :			Date :			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::Clear() 
//					 	
//	Description :	
//                
//	Return :	
//				void	-	none
//						
//	Parameters :
//				void	-	 none
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
void CProgressWnd::Clear() 
{ 
    SetText(TRUE, _T(""));
    SetText(FALSE, _T(""));
    SetPos(0, TRUE);
    SetPos(0, FALSE);
    m_bCancelled = FALSE; 
    m_nOverallPrevPos = 0;
    m_nSpecificPrevPos = 0;

    if (::IsWindow(GetSafeHwnd()))
        UpdateWindow();
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author :			Date : 			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :	CProgressWnd::Hide()  	
//					 	
//	Description :	
//                
//	Return :	void	-	none
//						
//	Parameters :	void	-	none
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
void CProgressWnd::Hide()  
{ 
    if (!::IsWindow(GetSafeHwnd())) 
        return;

    if (IsWindowVisible())
    {
        ShowWindow(SW_HIDE);
        ModifyStyle(WS_VISIBLE, 0);
    }
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : 		Date : 			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::Show()  
//					 	
//	Description :	   	
//                
//	Return :	
//				void	-	none
//						
//	Parameters :
//				void	-	none
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
void CProgressWnd::Show()  
{ 
    if (!::IsWindow(GetSafeHwnd()))
        return;

    if (!IsWindowVisible())
    {
        ModifyStyle(0, WS_VISIBLE);
        ShowWindow(SW_SHOWNA);
        RedrawWindow(NULL,NULL,RDW_ERASE|RDW_FRAME|RDW_INVALIDATE);
    }
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author :			Date : 			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::SetRange    
//					 	
//	Description :	   	
//                
//	Return :
//				void	-	none
//						
//	Parameters :
//				int nLower
//				int nUpper
//				BOOL bOverall
//				int nStep 
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
void CProgressWnd::SetRange( int nLower, int nUpper, BOOL bOverall, int nStep /* = 1 */)    
{
    if (!::IsWindow(GetSafeHwnd())) 
        return;

    // To take advantage of the Extended Range Values we use the PBM_SETRANGE32
    // message intead of calling CProgressCtrl::SetRange directly. If this is
    // being compiled under something less than VC 5.0, the necessary defines
    // may not be available.
#ifdef PBM_SETRANGE32
    ASSERT(-0x7FFFFFFF <= nLower && nLower <= 0x7FFFFFFF);
    ASSERT(-0x7FFFFFFF <= nUpper && nUpper <= 0x7FFFFFFF);
	if (bOverall)
	    m_wndOverallProgress.SendMessage(PBM_SETRANGE32, (WPARAM) nLower, (LPARAM) nUpper);
	else
		m_wndSpecificProgress.SendMessage(PBM_SETRANGE32, (WPARAM) nLower, (LPARAM) nUpper);
#else
    ASSERT(0 <= nLower && nLower <= 65535);
    ASSERT(0 <= nUpper && nUpper <= 65535);
	if (bOverall)
	    m_wndOverallProgress.SetRange(nLower, nUpper);
	else
		m_wndSpecificProgress.SetRange(nLower, nUpper);
#endif

    if (bOverall)
	{
		m_nOverallMaxValue = nUpper;
		m_nOverallMinValue = nLower;
		m_nOverallStep     = nStep;
	}
	else
	{
		m_nSpecificMaxValue = nUpper;
		m_nSpecificMinValue = nLower;
		m_nSpecificStep     = nStep;
	}

	if (bOverall)
	    m_wndOverallProgress.SetStep(nStep);
    else
		m_wndSpecificProgress.SetStep(nStep);
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : 		Date : 			version:	
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::OffsetPos
//					 	
//	Description :	
//                
//	Return :		int
//						
//	Parameters :
//					int nPos
//					BOOL bOverall
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
int CProgressWnd::OffsetPos(int nPos, BOOL bOverall)
{ 
    if (!::IsWindow(GetSafeHwnd())) 
	{
		if (bOverall)
			return m_nOverallPrevPos;
		else
			return m_nSpecificPrevPos;
	}

    Show();

	if (bOverall)
		return SetPos(m_nOverallPrevPos + nPos, TRUE); 
	else
		return SetPos(m_nSpecificPrevPos + nPos, FALSE); 
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author :		Date :			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :	 CProgressWnd::StepIt                	
//					 	
//	Description :	Steps the progress bar.	   	
//                
//	Return :	int	-  returns postion of progress bar.
//						
//	Parameters :	BOOL bOverall	-	which window flag
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
int CProgressWnd::StepIt(BOOL bOverall)                
{
    if (!::IsWindow(GetSafeHwnd())) 
	{
		if (bOverall)
			return m_nOverallPrevPos;
		else
			return m_nSpecificPrevPos;
	}

    Show();

	if (bOverall)
		return SetPos(m_nOverallPrevPos + m_nOverallStep, TRUE); 
	else
		return SetPos(m_nSpecificPrevPos + m_nSpecificStep, FALSE); 
}

/**
///////////////////////////////////////////////////////////////
//	Author :			Date : 			version 
///////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::SetStep
//					 	
//	Description :	Sets the progress bar step size.    	
//                
//	Return :		int 
//						
//	Parameters :	
//					int nStep
//					BOOL bOverall
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
int CProgressWnd::SetStep(int nStep, BOOL bOverall)
{
    int nOldStep;
	if (bOverall)
	{
		nOldStep= m_nOverallStep;
		m_nOverallStep = nStep;
	}
	else
	{
		nOldStep= m_nSpecificStep;
		m_nSpecificStep = nStep;
	}
    if (!::IsWindow(GetSafeHwnd())) 
        return nOldStep;

	if (bOverall)
		return m_wndOverallProgress.SetStep(nStep);
	else
		return m_wndSpecificProgress.SetStep(nStep); 
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author :			Date : 			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::SetPos                    
//					 	
//	Description :	Sets the progress bar on the widnow.   	
//                
//	Return :		int	-	Returns the progress bar postion.
//						
//	Parameters :	
//					int nPos	-	Position of the progress bar
//					BOOL bOverall	-	Top window flag
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
int CProgressWnd::SetPos(int nPos, BOOL bOverall)                    
{
#ifdef PBM_SETRANGE32
    ASSERT(-0x7FFFFFFF <= nPos && nPos <= 0x7FFFFFFF);
#else
    ASSERT(0 <= nPos && nPos <= 65535);
#endif

    if (!::IsWindow(GetSafeHwnd())) 
	{
		if (bOverall)
			return m_nOverallPrevPos;
		else
			return m_nSpecificPrevPos;
	}

    Show();

    CString strTitle;
    int nPercentage;
	int nPrevPrecent;
    int nMinValue;
	int nMaxValue;

	if (bOverall)
	{
		m_nOverallPrevPos = nPos;
		nPrevPrecent = m_nOverallPrevPercent;
		nMinValue = m_nOverallMinValue;
		nMaxValue = m_nOverallMaxValue;
	}
	else
	{
		m_nSpecificPrevPos = nPos;
		nPrevPrecent = m_nSpecificPrevPercent;
		nMinValue = m_nSpecificMinValue;
		nMaxValue = m_nSpecificMaxValue;
	}
 
    if (nMaxValue > nMinValue)
        nPercentage = (nPos*100)/(nMaxValue - nMinValue);
    else
        nPercentage = 0;

    if (nPercentage != nPrevPrecent) 
    {
		if (bOverall)
			m_nOverallPrevPercent = nPercentage;
		else
			m_nSpecificPrevPercent = nPercentage;

        strTitle.Format(_T("%s [%d%%]/[%d%%]"),m_strTitle,
			m_nOverallPrevPercent, m_nSpecificPrevPercent);
        SetWindowText(strTitle);
    }

	if (bOverall)
		return m_wndOverallProgress.SetPos(nPos);
	else
		return m_wndSpecificProgress.SetPos(nPos);        
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : 		Date : 			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::SetText
//					 	
//	Description :	Sets the text for the progress window bar.   	
//                
//	Return :	void	-	 none
//						
//	Parameters :	
//					BOOL bOverall	top progress bar flag
//					LPCTSTR fmt		long pointer to constant string 
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
void CProgressWnd::SetText(BOOL bOverall, LPCTSTR fmt, ...)
{
    if (!::IsWindow(GetSafeHwnd())) 
        return;

    va_list args;
    TCHAR buffer[512];

    va_start(args, fmt);
    _vstprintf_s(buffer, fmt, args);
    va_end(args);

    if (bOverall)
		m_cOverallText.SetWindowText(buffer);
	else
		m_cSpecificText.SetWindowText(buffer);
}

BEGIN_MESSAGE_MAP(CProgressWnd, CWnd)
    //{{AFX_MSG_MAP(CProgressWnd)
    ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
    ON_BN_CLICKED(IDC_CANCEL, OnCancel)
END_MESSAGE_MAP()

/**
/////////////////////////////////////////////////////////////////////////////
// CProgressWnd message handlers
////////////////////////////////////////////////////////////////////////////
//	Author :			Date : 			version:	
////////////////////////////////////////////////////////////////////////////
//	Function :		BOOL CProgressWnd::OnEraseBkgnd 
//					 	
//	Description :	   	
//                
//	Return :	BOOL
//						
//	Parameters :  CDC* pDC	-	Pointer to a device-context object.
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
BOOL CProgressWnd::OnEraseBkgnd(CDC* pDC) 
{
    // Fill background with Catchment background colour
    CBrush backBrush(GetSysColor(COLOR_BTNFACE));
    CBrush* pOldBrush = pDC->SelectObject(&backBrush);
    CRect rect;
    pDC->GetClipBox(&rect);     // Erase the area needed
    pDC->PatBlt(rect.left, rect.top, rect.Width(), rect.Height(), PATCOPY);
    pDC->SelectObject(pOldBrush);

    return TRUE;
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : 		Date : 			version:
////////////////////////////////////////////////////////////////////////////
//	Function :		void CProgressWnd::OnCancel() 
//					 	
//	Description :	
//                
//	Return :	void	- none
//						
//	Parameters :	void	-	 none
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
void CProgressWnd::OnCancel() 
{
    m_bCancelled = TRUE;
    Hide();

    if (m_bModal)
        SendMessage(WM_CLOSE);

    CWnd *pWnd = AfxGetMainWnd();
    if (pWnd && ::IsWindow(pWnd->m_hWnd))
        pWnd->SetForegroundWindow();
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author : 		Date : 			version:
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::DestroyWindow() 
//					 	
//	Description :	Destroys the Windows window attached to the CWnd object.	  	
//                
//	Return :	BOOL
//				Nonzero if the window is destroyed; otherwise 0.
//						
//	Parameters :	void	-	none.
//               
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
BOOL CProgressWnd::DestroyWindow() 
{
    if (m_bPersistantPosition)
        SaveCurrentSettings();

    if (m_bModal)
    {
        m_bModal = FALSE;
        CWnd *pMainWnd = AfxGetMainWnd();

        if (pMainWnd)
            pMainWnd->EnableWindow(TRUE);
    }
	
	return CWnd::DestroyWindow();
}

/**
////////////////////////////////////////////////////////////////
//	Author : 				Date : 			version: 
/////////////////////////////////////////////////////////////////
//	Function :	CProgressWnd::PeekAndPump	
//					 	
//	Description :	   	
//			Message pumping function that can either be used to pump
//			messages during long operations. This version will only
//			pass messages to this window (and all child windows).
//			(Thanks to Michael <mbh-ep@post5.tele.dk> for this)
//                
//	Return :
//						
//	Parameters :	
//					BOOL bCancelOnESCkey	- 
//						Escape key has been pressed.
//	Note :			
//			
///////////////////////////////////////////////////////////////////
**/
void CProgressWnd::PeekAndPump(BOOL bCancelOnESCkey /*= TRUE*/)
{
//    if (m_bModal && ::GetFocus() != m_hWnd)
//      SetFocus();

    MSG msg;
    while (!m_bCancelled && ::PeekMessage(&msg, NULL,0,0,PM_NOREMOVE)) 
    {
        if (bCancelOnESCkey && (msg.message == WM_CHAR) && (msg.wParam == VK_ESCAPE))
            OnCancel();

        // Cancel button disabled if modal, so we fake it.
        if (m_bModal && (msg.message == WM_LBUTTONUP))
        {
            CRect rect;
            m_CancelButton.GetWindowRect(rect);
            if (rect.PtInRect(msg.pt))
                OnCancel();
        }
  
        if (!AfxGetApp()->PumpMessage()) 
        {
            ::PostQuitMessage(0);
            return;
        } 
    }
}

/**
///////////////////////////////////////////////////////////////
//	Author : 		Date : 			version: 
///////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::GetPreviousSettings
//					 	
//	Description :	   	
//			Restores the previous window size from the registry
//
//	Return :
//				void	-	 none.
//						
//	Parameters :
//				void	-	 none.
//               
//	Note :			
//			
///////////////////////////////////////////////////////////////
**/
void CProgressWnd::GetPreviousSettings()
{
    int x = AfxGetApp()->GetProfileInt(szSection, szEntryX, -1);
    int y = AfxGetApp()->GetProfileInt(szSection, szEntryY, -1);

    if (x >= 0 && x < GetSystemMetrics(SM_CXSCREEN) &&
        y >= 0 && y < GetSystemMetrics(SM_CYSCREEN))
    {
        SetWindowPos(NULL, x,y, 0,0, SWP_NOSIZE|SWP_NOZORDER);
    }
    else
        CenterWindow();
}

/**
////////////////////////////////////////////////////////////////////////////
//	Author :			Date :			version: 
////////////////////////////////////////////////////////////////////////////
//	Function :		CProgressWnd::SaveCurrentSettings
//					 	
//	Description :	   	
//					Saves the current window position registry
//	Return :
//				void	-	 none.		
//	Parameters :
//               void	-	none
//	Note :			
//			
////////////////////////////////////////////////////////////////////////////
**/
void CProgressWnd::SaveCurrentSettings()
{   
    if (!IsWindow(m_hWnd))
        return;

    CRect rect;
    GetWindowRect(rect);

    AfxGetApp()->WriteProfileInt(szSection, szEntryX, rect.left);
    AfxGetApp()->WriteProfileInt(szSection, szEntryY, rect.top);
}