/**
/////////////////////////////////////////////////////////////////////////////
// RegistryGbl.cpp : implementation of the Registry class.                 //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
//																		   //
// Revision History:													   //
// Manan Dalal - July-August 2014										   //
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   //
// Release in Build16.2													   //
/////////////////////////////////////////////////////////////////////////////

// RegistryGbl.cpp: implementation of the various registory classes
//
/////////////////////////////////////////////////////////////////////////////
// This class is used for updating and restoring the registry database. 
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "stdafx.h"
#include "RegistryGbl.h"
#include "RegistryUsr.h"
#include "shlwapi.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegEx::CRegEx
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegGblEx::CRegGblEx(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegEx::~CRegEx
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegGblEx::~CRegGblEx(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegEx::DeleteKey
//	Description :	This routine that deletes a registry key.
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CRegGblEx::DeleteKey()
{
	ASSERT_VALID(this);

	m_nLastError = ERROR_SUCCESS;
	// Delete key

	// Get the key name
	CString sKeyName = GetKeyNames();

	// Get the root key
	if( (m_hRootKey == NULL) && !OpenRootKey() )
	{
		ASSERT(!SUCCEEDED(m_nLastError));
		return SUCCEEDED(m_nLastError);
	}

	// Remove possible leading and trailing backslash from key names
	if( !sKeyName.IsEmpty() )
	{
		// ... Non empty key name must begin with and end in back slash
		ASSERT(sKeyName.GetAt(0) == m_cBackslash);
		ASSERT(sKeyName.GetAt(sKeyName.GetLength() - 1) == m_cBackslash);
		sKeyName = sKeyName.Mid(1, sKeyName.GetLength() - 2);
	}

	// Close the key (not the rootkey) before deleting it
	CloseKey();
	HRESULT nLastError = HResultFromWin32(::SHDeleteKey(m_hRootKey, sKeyName));
	// ... Close all keys now
	Close();
	// ... Use the result of the failed Open (not the succeeded Close)
	m_nLastError = nLastError;

	ASSERT_VALID(this);
	return SUCCEEDED(m_nLastError);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegFolderType::CRegFolderType
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegFolderType::CRegFolderType(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegFolderType::~CRegFolderType
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegFolderType::~CRegFolderType(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegFolderType::Init
//	Description :	This routine initializes all the folder type
//					registry values.
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegFolderType::Init()
{
	InitializeFromApplication(strRegFolderKey);
	m_strFullRegKey	= GetFullRegistryItem();
	SetRegistryName(m_pszLocalMachine);
	m_strDayFormat	= GetStringValue(strRegFolderDayName, TRUE, TRUE, strRegFolderDayNameDefValue);
	
	for (int nIndex=0; nIndex < nMaxFolderTypes; nIndex++)
	{
		SetKeyNames(m_strFullRegKey + strRegFolderTypeKey[nIndex]);

		m_strTitle[nIndex]	= GetStringValue(strRegFolderTitleName, TRUE, TRUE, strRegFolderTitleDefValue[nIndex]);
		m_strDir[nIndex]	= GetStringValue(strRegFolderDirName,	TRUE, TRUE, strRegFolderDirDefValue[nIndex]);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegFolderType::SetValues
//	Description :	This routine sets all the folder type
//					registry values.
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegFolderType::SetValues()
{
	SetKeyNames(m_strFullRegKey);
	SetValueName(strRegFolderDayName);
	SetStringValue(m_strDayFormat, strRegFolderDayName);

	for (int nIndex=0; nIndex < nMaxFolderTypes; nIndex++)
	{
		SetKeyNames(m_strFullRegKey + strRegFolderTypeKey[nIndex]);
		SetValueName(strRegFolderDirName);
		SetStringValue(m_strDir[nIndex], strRegFolderDirName);
		SetValueName(strRegFolderTitleName);
		SetStringValue(m_strTitle[nIndex], strRegFolderTitleName);
	}
	
	Flush();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegSC::CRegSC
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegSC::CRegSC(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegSC::~CRegSC
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegSC::~CRegSC(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegSC::Init
//	Description :	This routine initializes all the spacecraft
//					registry values.
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegSC::Init()
{
	InitializeFromApplication(strRegSCKey);
	m_strFullRegKey	= GetFullRegistryItem();
	SetRegistryName(m_pszLocalMachine);
//	m_nDefault		= GetNumberValue(strRegSCDefaultName, TRUE, nRecSCDefaultDefValue);

	for (int nIndex=0; nIndex < nMaxSC; nIndex++)
	{
		CString strSCKey;

		strSCKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegSCKey, nIndex);
		SetKeyNames(strSCKey);

		m_strTitle[nIndex]				= GetStringValue(strRegSCTitleName,				TRUE, TRUE,	strRegSCTitleDefValue[nIndex]);
		m_strAbbrev[nIndex]				= GetStringValue(strRegSCAbbrevName,			TRUE, TRUE,	strRegSCAbbrevDefValue[nIndex]);
		m_strDir[nIndex]				= GetStringValue(strRegSCDirName,				TRUE, TRUE,	strRegSCDirDefValue[nIndex]);
		m_nID[nIndex]					= GetNumberValue(strRegSCIDName,				TRUE,		nRegSCIDDefValue[nIndex]);
		m_nFrameSize[nIndex]			= GetNumberValue(strRegSCFrameSizeName,			TRUE,		nRegSCFrameSizeDefValue[nIndex]);
		m_nFrameSRSOStart[nIndex]		= GetNumberValue(strRegSCFrameSRSOStartName,	TRUE,		nRegSCFrameSRSOStartDefValue[nIndex]);
		m_nStarSize[nIndex]				= GetNumberValue(strRegSCStarSizeName,			TRUE,		nRegSCStarSizeDefValue[nIndex]);
		m_nSTOSize[nIndex]				= GetNumberValue(strRegSCSTOSizeName,			TRUE,		nRegSCSTOSizeDefValue[nIndex]);
		m_nNumRTCS[nIndex]				= GetNumberValue(strRegSCNumRTCSName,			TRUE,		nRegSCNumRTCSValue[nIndex]);
		m_nSTORTCSSize[nIndex]			= GetNumberValue(strRegSCSTORTCSSizeName,		TRUE,		nRegSCSTORTCSSizeDefValue[nIndex]);
		m_nSKBSize[nIndex]				= GetNumberValue(strRegSCSKBSizeName,			TRUE,		nRegSCSKBSizeDefValue[nIndex]);
		m_nSKBSegSize[nIndex]			= GetNumberValue(strRegSCSKBSegSizeName,		TRUE,		nRegSCSKBSegSizeDefValue[nIndex]);
		m_nCTCUID[nIndex]				= GetNumberValue(strRegSCCTCUIDName,			TRUE,		nRegSCCTCUIDDefValue[nIndex]);
		m_strRTAddress[nIndex]			= GetStringValue(strRegSCRTAddressName,			TRUE, TRUE, strRegSCRTAddrDefValue[nIndex]);
		m_strDatabaseName[nIndex]		= GetStringValue(strRegSCDatabaseName,			TRUE, TRUE, strRegSCDatabaseDefValue[nIndex]);
		m_strMaxCmdGap[nIndex]			= GetStringValue(strRegSCMaxCmdGapName,			TRUE, TRUE, strRegSCMaxCmdGapDefValue[nIndex]);
		m_strPreHKGap[nIndex]			= GetStringValue(strRegSCPreHKGapName,			TRUE, TRUE, strRegSCPreHKGapDefValue[nIndex]);
		m_nRetryCnt[nIndex]				= GetNumberValue(strRegSCRetryCntName,			TRUE,		nRegSCRetryCntDefValue[nIndex]);
		m_fCmdExeTime[nIndex]			= (((float)GetNumberValue(strRegCmdExeTimeName, TRUE,		nRegCmdExeTimeDefValue[nIndex]))/1000.0f);	
		m_strSTOGotoTopCmd[nIndex]		= GetStringValue(strRegSCSTOGotoTopCmdName,		TRUE, TRUE, strRegSCSTOGotoTopCmdDefValue[nIndex]);
		m_strRTCSCmd[nIndex]			= GetStringValue(strRTCSCmdName,				TRUE, TRUE, strRTCSCmdValue[nIndex]);
		m_strPaceCmd[nIndex]			= GetStringValue(strPaceCmdName,				TRUE, TRUE, strPaceCmdValue[nIndex]);
		m_strFrameMiniSched[nIndex]		= GetStringValue(strFrameMiniSchedName,			TRUE, TRUE, strFrameMiniSchedValue[nIndex]);
		m_strFrameIOMiniSched[nIndex]	= GetStringValue(strFrameIOMiniSchedName,		TRUE, TRUE, strFrameIOMiniSchedValue[nIndex]);
		m_strStarMiniSched[nIndex]		= GetStringValue(strStarMiniSchedName,			TRUE, TRUE, strStarMiniSchedValue[nIndex]);
		m_strStarIOMiniSched[nIndex]	= GetStringValue(strStarIOMiniSchedName,		TRUE, TRUE, strStarIOMiniSchedValue[nIndex]);
		m_strSFCSMiniSched[nIndex]		= GetStringValue(strSFCSMiniSchedName,			TRUE, TRUE, strSFCSMiniSchedValue[nIndex]);
		m_strIntrusionRpt[nIndex]		= GetStringValue(strIntrusionRptName,			TRUE, TRUE, strIntrusionRptValue[nIndex]);
		m_nYawFlipFlg[nIndex]			= GetNumberValue(strYawFlipFlgName,				TRUE,		nYawFlipFlgValue[nIndex]);
		m_strScanDegsEWNadirImgr[nIndex]= GetStringValue(strScanDegsEWNadirImgrName,	TRUE, TRUE, strScanDegsEWNadirImgrValue[nIndex]);
		m_strScanDegsNSNadirImgr[nIndex]= GetStringValue(strScanDegsNSNadirImgrName,	TRUE, TRUE, strScanDegsNSNadirImgrValue[nIndex]); 
		m_strScanDegsEWNadirSdr[nIndex]	= GetStringValue(strScanDegsEWNadirSdrName,		TRUE, TRUE, strScanDegsEWNadirSdrValue[nIndex]); 
		m_strScanDegsNSNadirSdr[nIndex]	= GetStringValue(strScanDegsNSNadirSdrName,		TRUE, TRUE, strScanDegsNSNadirSdrValue[nIndex]); 
		m_strStarDegsEWNadirImgr[nIndex]= GetStringValue(strStarDegsEWNadirImgrName,	TRUE, TRUE, strStarDegsEWNadirImgrValue[nIndex]); 
		m_strStarDegsNSNadirImgr[nIndex]= GetStringValue(strStarDegsNSNadirImgrName,	TRUE, TRUE, strStarDegsNSNadirImgrValue[nIndex]); 
		m_strStarDegsEWNadirSdr[nIndex]	= GetStringValue(strStarDegsEWNadirSdrName,		TRUE, TRUE, strStarDegsEWNadirSdrValue[nIndex]);
		m_strStarDegsNSNadirSdr[nIndex]	= GetStringValue(strStarDegsNSNadirSdrName,		TRUE, TRUE, strStarDegsNSNadirSdrValue[nIndex]);
		m_nInstrCenterPtImgrNS[nIndex]	= GetNumberValue(strInstrCenterPtImgrNSName,	TRUE,		nInstrCenterPtImgrNSValue[nIndex]);
		m_nInstrCenterPtImgrEW[nIndex]	= GetNumberValue(strInstrCenterPtImgrEWName,	TRUE,		nInstrCenterPtImgrEWValue[nIndex]);
		m_nInstrCenterPtSdrNS[nIndex]	= GetNumberValue(strInstrCenterPtSdrNSName,		TRUE,		nInstrCenterPtSdrNSValue[nIndex]); 
		m_nInstrCenterPtSdrEW[nIndex]	= GetNumberValue(strInstrCenterPtSdrEWName,		TRUE,		nInstrCenterPtSdrEWValue[nIndex]);
		m_nSpaceLookSdrWest[nIndex]		= GetNumberValue(strSpaceLookSdrWestName,		TRUE,		nSpaceLookSdrWestValue[nIndex]);
		m_nSpaceLookSdrEast[nIndex]		= GetNumberValue(strSpaceLookSdrEastName,		TRUE,		nSpaceLookSdrEastValue[nIndex]);
		m_nSpaceLookImgrWest[nIndex]	= GetNumberValue(strSpaceLookImgrWestName,		TRUE,		nSpaceLookImgrWestValue[nIndex]);
		m_nSpaceLookImgrEast[nIndex]	= GetNumberValue(strSpaceLookImgrEastName,		TRUE,		nSpaceLookImgrEastValue[nIndex]);
		m_strTimeTagSpacing[nIndex]		= GetStringValue(strTimeTagSpacingName,			TRUE, TRUE, strTimeTagSpacingValue[nIndex]);
		m_strNumCmdFrameFile[nIndex]	= GetStringValue(strNumCmdFrameFileName,		TRUE, TRUE, strNumCmdFrameFileValue[nIndex]);
		m_nCmdFrameUplinkRate[nIndex]	= GetNumberValue(strCmdFrameUplinkRateName,		TRUE,		nCmdFrameUplinkRateValue[nIndex]);
		m_strSTOLCmd[nIndex]			= GetStringValue(strSTOLCmdName,				TRUE, TRUE, strSTOLCmdValue[nIndex]);
		m_strSRSOSpacelookMode[nIndex]	= GetStringValue(strSRSOSpacelookModeName,		TRUE, TRUE,	strSRSOSpacelookModeValue[nIndex]);
		m_strSRSOFrameLabel[nIndex]		= GetStringValue(strSRSOFrameLabelName,			TRUE, TRUE,	strSRSOFrameLabelValue[nIndex]);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegSC::SetValues
//	Description :	This routine sets all the spacecraft
//					registry values.
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegSC::SetValues()
{
	SetKeyNames(m_strFullRegKey);
//	SetNumberValue(m_nDefault, strRegSCDefaultName);

	for (int nIndex=0; nIndex < nMaxSC; nIndex++)
	{
		CString strSCKey;

		strSCKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegSCKey, nIndex);
		SetKeyNames(strSCKey);
		SetStringValue(m_strTitle[nIndex],				strRegSCTitleName);
		SetStringValue(m_strAbbrev[nIndex],				strRegSCAbbrevName);
		SetStringValue(m_strDir[nIndex],				strRegSCDirName);
		SetNumberValue(m_nID[nIndex],					strRegSCIDName);
		SetNumberValue(m_nFrameSize[nIndex],			strRegSCFrameSizeName);
		SetNumberValue(m_nFrameSRSOStart[nIndex],		strRegSCFrameSRSOStartName);
		SetNumberValue(m_nStarSize[nIndex],				strRegSCStarSizeName);
		SetNumberValue(m_nNumRTCS[nIndex],				strRegSCNumRTCSName);
		SetNumberValue(m_nSTOSize[nIndex],				strRegSCSTOSizeName);
		SetNumberValue(m_nSTORTCSSize[nIndex],			strRegSCSTORTCSSizeName);
		SetNumberValue(m_nSKBSize[nIndex],				strRegSCSKBSizeName);
		SetNumberValue(m_nSKBSegSize[nIndex],			strRegSCSKBSegSizeName);
		SetNumberValue(m_nCTCUID[nIndex],				strRegSCCTCUIDName);
		SetStringValue(m_strRTAddress[nIndex],			strRegSCRTAddressName);
		SetStringValue(m_strDatabaseName[nIndex],		strRegSCDatabaseName);
		SetStringValue(m_strMaxCmdGap[nIndex],			strRegSCMaxCmdGapName);
		SetStringValue(m_strPreHKGap[nIndex],			strRegSCPreHKGapName);
		SetNumberValue(m_nRetryCnt[nIndex],				strRegSCRetryCntName);
		int nRegCmdExeTimeTemp = (int)(m_fCmdExeTime[nIndex] * 1000.0f);
		SetNumberValue(nRegCmdExeTimeTemp,				strRegCmdExeTimeName);	
		SetStringValue(m_strSTOGotoTopCmd[nIndex],		strRegSCSTOGotoTopCmdName);
		SetStringValue(m_strRTCSCmd[nIndex],			strRTCSCmdName);
		SetStringValue(m_strPaceCmd[nIndex],			strPaceCmdName);
		SetStringValue(m_strFrameMiniSched[nIndex],		strFrameMiniSchedName);
		SetStringValue(m_strFrameIOMiniSched[nIndex],	strFrameIOMiniSchedName);
		SetStringValue(m_strStarMiniSched[nIndex],		strStarMiniSchedName);
		SetStringValue(m_strStarIOMiniSched[nIndex],	strStarIOMiniSchedName);
		SetStringValue(m_strSFCSMiniSched[nIndex],		strSFCSMiniSchedName);
		SetStringValue(m_strIntrusionRpt[nIndex],		strIntrusionRptName);
		SetNumberValue(m_nYawFlipFlg[nIndex],			strYawFlipFlgName);
		SetStringValue(m_strScanDegsEWNadirImgr[nIndex],strScanDegsEWNadirImgrName);
		SetStringValue(m_strScanDegsNSNadirImgr[nIndex],strScanDegsNSNadirImgrName);
		SetStringValue(m_strScanDegsEWNadirSdr[nIndex], strScanDegsEWNadirSdrName);
		SetStringValue(m_strScanDegsNSNadirSdr[nIndex], strScanDegsNSNadirSdrName);
		SetStringValue(m_strStarDegsEWNadirImgr[nIndex],strStarDegsEWNadirImgrName);
		SetStringValue(m_strStarDegsNSNadirImgr[nIndex],strStarDegsNSNadirImgrName);
		SetStringValue(m_strStarDegsEWNadirSdr[nIndex], strStarDegsEWNadirSdrName);
		SetStringValue(m_strStarDegsNSNadirSdr[nIndex], strStarDegsNSNadirSdrName);
		SetNumberValue(m_nInstrCenterPtImgrNS[nIndex],	strInstrCenterPtImgrNSName);
		SetNumberValue(m_nInstrCenterPtImgrEW[nIndex],	strInstrCenterPtImgrEWName);
		SetNumberValue(m_nInstrCenterPtSdrNS[nIndex],	strInstrCenterPtSdrNSName);
		SetNumberValue(m_nInstrCenterPtSdrEW[nIndex],	strInstrCenterPtSdrEWName);
		SetNumberValue(m_nSpaceLookSdrWest[nIndex],		strSpaceLookSdrWestName);
		SetNumberValue(m_nSpaceLookSdrEast[nIndex],		strSpaceLookSdrEastName);
		SetNumberValue(m_nSpaceLookImgrWest[nIndex],	strSpaceLookImgrWestName);
		SetNumberValue(m_nSpaceLookImgrEast[nIndex],	strSpaceLookImgrEastName);
		SetStringValue(m_strTimeTagSpacing[nIndex],		strTimeTagSpacingName);
		SetStringValue(m_strNumCmdFrameFile[nIndex],	strNumCmdFrameFileName);
		SetNumberValue(m_nCmdFrameUplinkRate[nIndex],	strCmdFrameUplinkRateName);
		SetStringValue(m_strSTOLCmd[nIndex],			strSTOLCmdName);
		SetStringValue(m_strSRSOSpacelookMode[nIndex],	strSRSOSpacelookModeName);
		SetStringValue(m_strSRSOFrameLabel[nIndex],		strSRSOFrameLabelName);
	}
	
	Flush();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegGTACS::CRegGTACS
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegGTACS::CRegGTACS(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegGTACS::~CRegGTACS
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegGTACS::~CRegGTACS(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegGTACS::Init
//	Description :	This routine initializes all GTACS
//					registry values.
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegGTACS::Init()
{
	InitializeFromApplication(strRegGTACSKey);
	m_strFullRegKey = GetFullRegistryItem();
	SetRegistryName(m_pszLocalMachine);

	for (int nSite=0; nSite < nMaxSites; nSite++)
	{
		CString strSiteKey;
		strSiteKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegSiteKey, nSite);
		SetKeyNames(strSiteKey);

		m_nDefault[nSite] = GetNumberValue(strRegGTACSDefaultName, TRUE, nRecGTACSDefaultDefValue[nSite]);

		for (int nGTACS=0; nGTACS<nMaxGTACS[nSite]; nGTACS++)
		{
			CString strGTACSKey;
			strGTACSKey.Format(_T("%s%s-%d\\%s-%d"), m_strFullRegKey, strRegSiteKey, nSite, strRegGTACSKey, nGTACS);
			SetKeyNames(strGTACSKey);

			m_strTitle[nSite][nGTACS]		= GetStringValue(strRegGTACSTitleName,		TRUE, TRUE, strRegGTACSTitleDefValue[nSite][nGTACS]);
			m_strAbbrev[nSite][nGTACS]		= GetStringValue(strRegGTACSAbbrevName,		TRUE, TRUE, strRegGTACSAbbrevDefValue[nSite][nGTACS]);
			m_strProcShare[nSite][nGTACS]	= GetStringValue(strRegGTACSProcShareName,	TRUE, TRUE, strRegGTACSProcShareDefValue[nSite][nGTACS]);
			m_strDBShare[nSite][nGTACS]		= GetStringValue(strRegGTACSDBShareName,	TRUE, TRUE, strRegGTACSDBShareDefValue[nSite][nGTACS]);
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegGTACS::SetValues
//	Description :	This routine sets all the GTACS registry values.
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegGTACS::SetValues()
{
	SetKeyNames(m_strFullRegKey);

	for (int nSite=0; nSite < nMaxSites; nSite++)
	{
		CString strSiteKey;
		strSiteKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegSiteKey, nSite);
		SetKeyNames(strSiteKey);
		SetNumberValue(m_nDefault[nSite], strRegGTACSDefaultName);

		for (int nGTACS=0; nGTACS<nMaxGTACS[nSite]; nGTACS++)
		{
			CString strGTACSKey;
			strGTACSKey.Format(_T("%s%s-%d\\%s-%d"), m_strFullRegKey, strRegSiteKey, nSite, strRegGTACSKey, nGTACS);
			SetKeyNames(strGTACSKey);
			SetStringValue(m_strTitle[nSite][nGTACS],		strRegGTACSTitleName);
			SetStringValue(m_strAbbrev[nSite][nGTACS],		strRegGTACSAbbrevName);
			SetStringValue(m_strProcShare[nSite][nGTACS],	strRegGTACSProcShareName);
			SetStringValue(m_strDBShare[nSite][nGTACS],		strRegGTACSDBShareName);
		}
	}
	
	Flush();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegOATS::CRegOATS
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegOATS::CRegOATS(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegOATS::~CRegOATS
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegOATS::~CRegOATS(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegOATS::Init
//	Description :	This routine initializes all OATS registry values.
//					
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegOATS::Init()
{
	InitializeFromApplication(strRegOATSKey);
	m_strFullRegKey	= GetFullRegistryItem();
	SetRegistryName(m_pszLocalMachine);
	m_nPort			= GetNumberValue(strRegOATSPortName,	TRUE, nRegOATSPortDefValue);
	m_nTimeout		= GetNumberValue(strRegOATSTimeoutName, TRUE, nRegOATSTimeoutDefValue);

	for (int nSite=0; nSite < nMaxSites; nSite++)
	{
		CString strSiteKey;
		strSiteKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegSiteKey, nSite);
		SetKeyNames(strSiteKey);

		m_nDefault[nSite] = GetNumberValue(strRegOATSDefaultName, TRUE, nRegOATSDefaultDefValue[nSite]);

		for (int nOATS=0; nOATS<nMaxOATS[nSite]; nOATS++)
		{
			CString strOATSKey;
			strOATSKey.Format(_T("%s%s-%d\\%s-%d"), m_strFullRegKey, strRegSiteKey, nSite, strRegOATSKey, nOATS);
			SetKeyNames(strOATSKey);

			m_strTitle[nSite][nOATS]	= GetStringValue(strRegOATSTitleName,	TRUE, TRUE, strRegOATSTitleDefValue[nSite][nOATS]);
			m_strAbbrev[nSite][nOATS]	= GetStringValue(strRegOATSAbbrevName,	TRUE, TRUE, strRegOATSAbbrevDefValue[nSite][nOATS]);
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegOATS::SetValues
//	Description :	This routine sets all OATS registry values.
//					
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegOATS::SetValues()
{
	SetKeyNames(m_strFullRegKey);
	SetNumberValue(m_nPort,		strRegOATSPortName);
	SetNumberValue(m_nTimeout,	strRegOATSTimeoutName);
	
	for (int nSite=0; nSite < nMaxSites; nSite++)
	{
		CString strSiteKey;
		strSiteKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegSiteKey, nSite);
		SetKeyNames(strSiteKey);
		SetNumberValue(m_nDefault[nSite], strRegOATSDefaultName);

		for (int nOATS=0; nOATS<nMaxOATS[nSite]; nOATS++)
		{
			CString strOATSKey;
			strOATSKey.Format(_T("%s%s-%d\\%s-%d"), m_strFullRegKey, strRegSiteKey, nSite, strRegOATSKey, nOATS);
			SetKeyNames(strOATSKey);
			SetStringValue(m_strTitle[nSite][nOATS],	strRegOATSTitleName);
			SetStringValue(m_strAbbrev[nSite][nOATS],	strRegOATSAbbrevName);
		}
	}

	Flush();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegRSO::CRegRSO
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegRSO::CRegRSO(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegRSO::~CRegRSO
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegRSO::~CRegRSO(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegRSO::Init
//	Description :	This routine initializes all OATS registry values.
//					
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegRSO::Init()
{
	InitializeFromApplication(strRegRSOKey);
	m_strFullRegKey	= GetFullRegistryItem();
	SetRegistryName(m_pszLocalMachine);

	for (int nIndex=0; nIndex < nMaxSC; nIndex++)
	{
		CString strRSOKey;

		strRSOKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegSCKey, nIndex);
		SetKeyNames(strRSOKey);

		m_strTitle[nIndex]				= GetStringValue(strRegRSOSCName,				TRUE, TRUE,	strRegRSOSCNameDefValue[nIndex]);
		m_strFrmTable[nIndex]			= GetStringValue(strRegRSOFrmTableName,			TRUE, TRUE, strRegRSOFrmTableDefValue[nIndex]);
		m_strCurRSOName[nIndex]			= GetStringValue(strRegRSOCurRSOLabelName,		TRUE, TRUE, strRegRSOCurRSOLabelDefValue[nIndex]);
		m_strNewRSOName1[nIndex]		= GetStringValue(strRegRSONewRSOLabel1Name,		TRUE, TRUE, strRegRSONewRSOLabel1DefValue[nIndex]);
		m_strNewRSOName2[nIndex]		= GetStringValue(strRegRSONewRSOLabel2Name,		TRUE, TRUE, strRegRSONewRSOLabel2DefValue[nIndex]);
		m_strNewRSOName3[nIndex]		= GetStringValue(strRegRSONewRSOLabel3Name,		TRUE, TRUE, strRegRSONewRSOLabel3DefValue[nIndex]);
		m_strNewRSOName4[nIndex]		= GetStringValue(strRegRSONewRSOLabel4Name,		TRUE, TRUE, strRegRSONewRSOLabel4DefValue[nIndex]);
		m_strNewRSOName5[nIndex]		= GetStringValue(strRegRSONewRSOLabel5Name,		TRUE, TRUE, strRegRSONewRSOLabel5DefValue[nIndex]);
		m_strNewRSOName6[nIndex]		= GetStringValue(strRegRSONewRSOLabel6Name,		TRUE, TRUE,	strRegRSONewRSOLabel6DefValue[nIndex]);
		m_strNewRSOName7[nIndex]		= GetStringValue(strRegRSONewRSOLabel7Name,		TRUE, TRUE, strRegRSONewRSOLabel7DefValue[nIndex]);
		m_strNewRSOName8[nIndex]		= GetStringValue(strRegRSONewRSOLabel8Name,		TRUE, TRUE, strRegRSONewRSOLabel8DefValue[nIndex]);
		m_strNewRSOName9[nIndex]		= GetStringValue(strRegRSONewRSOLabel9Name,		TRUE, TRUE,	strRegRSONewRSOLabel9DefValue[nIndex]);
		m_strNewRSOName10[nIndex]		= GetStringValue(strRegRSONewRSOLabel10Name,	TRUE, TRUE, strRegRSONewRSOLabel10DefValue[nIndex]);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegRSO::SetValues
//	Description :	This routine sets all RSO registry values.
//					
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegRSO::SetValues()
{
	SetKeyNames(m_strFullRegKey);

	for (int nIndex=0; nIndex < nMaxSC; nIndex++)
	{
		CString strRSOKey;

		strRSOKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegSCKey, nIndex);
		SetKeyNames(strRSOKey);
		SetStringValue(m_strTitle[nIndex],				strRegRSOSCName);
		SetStringValue(m_strFrmTable[nIndex],			strRegRSOFrmTableName);
		SetStringValue(m_strCurRSOName[nIndex],			strRegRSOCurRSOLabelName);
		SetStringValue(m_strNewRSOName1[nIndex],		strRegRSONewRSOLabel1Name);
		SetStringValue(m_strNewRSOName2[nIndex],		strRegRSONewRSOLabel2Name);
		SetStringValue(m_strNewRSOName3[nIndex],		strRegRSONewRSOLabel3Name);
		SetStringValue(m_strNewRSOName4[nIndex],		strRegRSONewRSOLabel4Name);
		SetStringValue(m_strNewRSOName5[nIndex],		strRegRSONewRSOLabel5Name);
		SetStringValue(m_strNewRSOName6[nIndex],		strRegRSONewRSOLabel6Name);
		SetStringValue(m_strNewRSOName7[nIndex],		strRegRSONewRSOLabel7Name);
		SetStringValue(m_strNewRSOName8[nIndex],		strRegRSONewRSOLabel8Name);
		SetStringValue(m_strNewRSOName9[nIndex],		strRegRSONewRSOLabel9Name);
		SetStringValue(m_strNewRSOName10[nIndex],		strRegRSONewRSOLabel10Name);
	}
	
	Flush();
}


/**
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw		Date : 11/28/05		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMakeSched::CRegMakeSched
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
// CRegMakeSched::CRegMakeSched(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw		Date : 11/28/05		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMakeSched::~CRegMakeSched
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
// CRegMakeSched::~CRegMakeSched(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegOATS::Init
//	Description :	This routine initializes all OATS registry values.
//					
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
/*
void CRegMakeSched::Init()
{
	InitializeFromApplication(strRegMakeSchedKey);
	m_strFullRegKey	= GetFullRegistryItem();
	SetRegistryName(m_pszLocalMachine);

	m_nCLSYear           = GetNumberValue(strRegMakeSchedCLSYearName, TRUE, nRegMakeSchedCLSYearDefValue);
	m_nFrmGen            = GetNumberValue(strRegMakeSchedFrmGenName	, TRUE, nRegMakeSchedFrmGenDefValue);
	m_nFrmUpdate         = GetNumberValue(strRegMakeSchedFrmUpdateName,	TRUE, nRegMakeSchedFrmUpdateDefValue);
	m_nFrmSRSO           = GetNumberValue(strRegMakeSchedFrmSRSOName,	TRUE, nRegMakeSchedFrmSRSODefValue);
	m_nNewFrmTbl         = GetNumberValue(strRegMakeSchedNewFrmTblName,	TRUE, nRegMakeSchedNewFrmTblDefValue);
	m_strSecTblFileName  = GetStringValue(strRegMakeSchedSecTblFileNameName,	TRUE, TRUE, strRegMakeSchedSecTblFileNameDefValue);
	m_strFrmTblFileName  = GetStringValue(strRegMakeSchedFrmTblFileNameName,	TRUE, TRUE, strRegMakeSchedFrmTblFileNameDefValue);
	m_nRTCSGen           = GetNumberValue(strRegMakeSchedRTCSGenName,	TRUE, nRegMakeSchedRTCSGenDefValue);;
	m_strRTCSMapFileName = GetStringValue(strRegMakeSchedRTCSMapFileNameName, TRUE, TRUE, strRegMakeSchedRTCSMapFileNameDefValue);
	m_strRTCSSetFileName = GetStringValue(strRegMakeSchedRTCSSetFileNameName,	TRUE, TRUE, strRegMakeSchedRTCSSetFileNameDefValue);
	m_nOBSchedGen        = GetNumberValue(strRegMakeSchedOBSchedGenName,	TRUE, nRegMakeSchedOBSchedGenDefValue);;
    m_nValSched          = GetNumberValue(strRegMakeSchedValSchedName,	TRUE, nRegMakeSchedValSchedDefValue);;
	m_nRTCSVal           = GetNumberValue(strRegMakeSchedOBSchedGenName	,	TRUE, nRegMakeSchedOBSchedGenDefValue);;
	m_nValSchedType      = GetNumberValue(strRegMakeSchedValSchedName,	TRUE, nRegMakeSchedValSchedDefValue);;
	m_nStarUpdate        = GetNumberValue(strRegMakeSchedRTCSValName,	TRUE, nRegMakeSchedRTCSValDefValue);;
	m_nStarGen           = GetNumberValue(strRegMakeSchedValSchedTypeName,	TRUE, nRegMakeSchedValSchedTypeDefValue);;
	m_nStaticFrames      = GetNumberValue(strRegMakeSchedStaticFramesName,	TRUE, nRegMakeSchedStaticFramesDefValue);
	m_nRTCSUpdate	     = GetNumberValue(strRegMakeSchedRTCSUpdateName,	TRUE, nRegMakeSchedRTCSUpdateDefValue);

}
*/

/**
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw		Date : 11/28/05		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMakeSched::SetValues
//	Description :	This routine sets all MakeSched registry values.
//					
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
/*
void CRegMakeSched::SetValues()
{
	SetKeyNames(m_strFullRegKey);
	SetNumberValue(m_nCLSYear, strRegMakeSchedCLSYearName);
	SetNumberValue(m_nFrmGen, strRegMakeSchedFrmGenName);
	SetNumberValue(m_nFrmUpdate, strRegMakeSchedFrmUpdateName);
	SetNumberValue(m_nFrmSRSO, strRegMakeSchedFrmSRSOName);
	SetNumberValue(m_nNewFrmTbl, strRegMakeSchedNewFrmTblName);
	SetStringValue(m_strFrmTblFileName, strRegMakeSchedFrmTblFileNameName);
	SetNumberValue(m_nRTCSGen, strRegMakeSchedRTCSGenName);
	SetNumberValue(m_nRTCSUpdate, strRegMakeSchedRTCSUpdateName	);
	SetNumberValue(m_nOBSchedGen, strRegMakeSchedOBSchedGenName);
	SetNumberValue(m_nValSched, strRegMakeSchedValSchedName);
	SetNumberValue(m_nRTCSVal, strRegMakeSchedRTCSValName);
	SetNumberValue(m_nValSchedType, strRegMakeSchedValSchedTypeName);
	SetNumberValue(m_nStarUpdate, strRegMakeSchedStarUpdateName);
	SetNumberValue(m_nStarGen, strRegMakeSchedStarGenName);
	SetNumberValue(m_nStaticFrames, strRegMakeSchedStaticFramesName);
	SetStringValue(m_strSecTblFileName, strRegMakeSchedSecTblFileNameName);
	SetStringValue(m_strRTCSMapFileName, strRegMakeSchedRTCSMapFileNameName);
	SetStringValue(m_strRTCSSetFileName, strRegMakeSchedRTCSSetFileNameName);
	
	Flush();
}
*/

