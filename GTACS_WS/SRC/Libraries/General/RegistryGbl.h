// RegistryGbl.h: interface for the CSchedMgrECPRegistryFileType class.
//
// Revision History:
// Manan Dalal - August 2014
// PR000951 - Enhance SchedMgrECP with RSO Sectors
// Added CRegRSO to registry.
// Released in Build16.2 on Windows 7 and XP
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REGISTRYGBL_H__FCB3AF3A_D5CA_11D4_800D_00609704053C__INCLUDED_)
#define AFX_REGISTRYGBL_H__FCB3AF3A_D5CA_11D4_800D_00609704053C__INCLUDED_

#include "OXRegistryItem.h"
#include "Constants.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Registry constants
//
// Registory constants are made up of three parts.  The first part is the
// registory key.  This is followed by the registory name.  Finally, the
// registory names are assigned values.
//
// General Keys
//
const CString strRegCompanyKey = _T("ISI");

class CRegGblEx : public COXRegistryItem
{
public:
	CRegGblEx();
	virtual ~CRegGblEx();
	BOOL DeleteKey();
	CString m_strFullRegKey;
};

//
// Keys and names associated with Folder Types
//
const CString strRegFolderKey														= _T("Folder Types");
const CString strRegFolderDayName													= _T("Day Format");
const CString strRegFolderDayNameDefValue											= _T("day%03d");
const CString strRegFolderTypeKey[nMaxFolderTypes] 									=
		{_T("PROCS"),_T("CLS"),_T("Frame"),_T("Stars"),_T("OATS"),_T("RTCS"),_T("Data")};
const CString strRegFolderDirName													= _T("Directory");
const CString strRegFolderDirDefValue[nMaxFolderTypes] 								=
		{_T("procs"),_T("cls"),_T("frame"),_T("star"),_T("oats"),_T("rtcs"),_T("data")};
const CString strRegFolderTitleName													= _T("Title");
const CString strRegFolderTitleDefValue[nMaxFolderTypes] 							=
		{_T("STOL Procedures"),_T("CLS Files"),_T("Frame Files"),
		 _T("Star Files"),_T("OATS Files"),_T("RTCS Files"),_T("Data Files")};

class CRegFolderType : public CRegGblEx
{
public:
	CRegFolderType();
	virtual ~CRegFolderType();
	virtual void Init();
	virtual void SetValues();
	CString m_strDayFormat;
	CString m_strDir[nMaxFolderTypes];
	CString m_strTitle[nMaxFolderTypes];
	//Other Data for documents is stored in the string resource template for each doc type
};

const CString strRegSCKey 															= _T("Spacecraft");
const CString strRegSCDefaultName 													= _T("Default");
const int	  nRecSCDefaultDefValue 												= 0;
const CString strRegSCTitleName 													= _T("Title");
const CString strRegSCTitleDefValue[nMaxSC] 										=
		{_T("Goes 13"),_T("Goes 14"),_T("Goes 15"),_T("Goes 16"),
	 	 _T("Goes 17 (13)"),_T("Goes 18 (14)"),_T("Goes 19 (15)"),_T("Goes 20 (16)")};
const CString strRegSCAbbrevName 													= _T("Abbreviation");
const CString strRegSCAbbrevDefValue[nMaxSC] 										=
		{_T("N"),_T("O"),_T("P"),_T("Q"),_T("Na"),_T("Oa"),_T("Pa"),_T("Qa")};
const CString strRegSCDirName 														= _T("Directory");
const CString strRegSCDirDefValue[nMaxSC] 											=
		{_T("goes13"),_T("goes14"),_T("goes15"),_T("goes16"),
	 	 _T("goes17"),_T("goes18"),_T("goes19"),_T("goes20")};
const CString strRegSCIDName 														= _T("ID");
const int	  nRegSCIDDefValue[nMaxSC] 												= {13,14,15,16,13,14,15,16};
const CString strRegSCFrameSizeName 												= _T("Frame Size");
const int	  nRegSCFrameSizeDefValue[nMaxSC] 										=
		{500, 500, 500, 500, 500, 500, 500, 500};
const CString strRegSCFrameSRSOStartName 											= _T("SRSO Start Frame");
const int	  nRegSCFrameSRSOStartDefValue[nMaxSC] 									=
		{200, 200, 200, 200, 200, 200, 200, 200};
const CString strRegSCStarSizeName 													= _T("Star Size");
const int	  nRegSCStarSizeDefValue[nMaxSC] 										=
		{4000, 4000, 4000, 4000, 4000, 4000, 4000, 4000};
const CString strRegSCSTOSizeName 													= _T("STO Size");
const int	  nRegSCSTOSizeDefValue[nMaxSC] 										=
		{1535, 1535, 1535, 1535, 1535, 1535, 1535, 1535};
const CString strRegSCSTORTCSSizeName 												= _T("STO RTCS Size");
const int	  nRegSCSTORTCSSizeDefValue[nMaxSC] 									=
		{256, 256, 256, 256, 256, 256, 256, 256};
const CString strRegSCNumRTCSName 													= _T("Number of RTCSs");
const int	  nRegSCNumRTCSValue[nMaxSC] 											= {25, 25, 25, 25, 25, 25, 25, 25};
const CString strRegSCSKBSizeName 													= _T("SKB Size");
const int	  nRegSCSKBSizeDefValue[nMaxSC] 										=
		{3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000};
const CString strRegSCSKBSegSizeName 												= _T("SKB Segment Size");
const int	  nRegSCSKBSegSizeDefValue[nMaxSC] 										= {-1, -1, -1, -1, -1, -1, -1, -1};
const CString strRegSCCTCUIDName 													= _T("CTCU ID");
const int	  nRegSCCTCUIDDefValue[nMaxSC] 											= {1, 1, 1, 1, 1, 1, 1, 1};
const CString strRegSCRTAddressName 												= _T("RT Address");
const CString strRegSCRTAddrDefValue[nMaxSC] 										=
		{_T("ACE_1"),_T("ACE_1"),_T("ACE_1"),_T("ACE_1"),
	 	 _T("ACE_1"),_T("ACE_1"),_T("ACE_1"),_T("ACE_1")};
const CString strRegSCDatabaseName 													= _T("Database");
const CString strRegSCDatabaseDefValue[nMaxSC] 										=
		{_T("ssgs13"),_T("ssgs14"),_T("ssgs15"),_T("ssgs16"),
	 	 _T("ssgs17"),_T("ssgs18"),_T("ssgs19"),_T("ssgs20")};
const CString strRegSCMaxCmdGapName 												= _T("Max Cmd Gap");
const CString strRegSCMaxCmdGapDefValue[nMaxSC] 									=
		{_T("00:00:30"),_T("00:00:30"),_T("00:00:30"),_T("00:00:30"),
	 	 _T("00:00:30"),_T("00:00:30"),_T("00:00:30"),_T("00:00:30")};
const CString strRegSCPreHKGapName 													= _T("Pre HK Gap");
const CString	  strRegSCPreHKGapDefValue[nMaxSC] 									=
		{_T("00:01:30"),_T("00:01:30"),_T("00:01:30"),_T("00:01:30"),
	 	 _T("00:01:30"),_T("00:01:30"),_T("00:01:30"),_T("00:01:30")};
const CString strRegSCRetryCntName 													= _T("Retry Cnt");
const int	  nRegSCRetryCntDefValue[nMaxSC] 										= {1, 1, 1, 1, 1, 1, 1, 1};
const CString strRegCmdExeTimeName 													= _T("Cmd Exe Time");
const int  nRegCmdExeTimeDefValue[nMaxSC] 											= {10, 10, 10, 10, 10, 10, 10, 10};
const CString strRegSCSTOGotoTopCmdName 											= _T("STO Goto Top Cmd");
const CString strRegSCSTOGotoTopCmdDefValue[nMaxSC] 								=
		{_T("ACE_STO_MOVE_TOP_08 STO_TOP_INDEX %s RT ACE_1"),
		 _T("ACE_STO_MOVE_TOP_08 STO_TOP_INDEX %s RT ACE_1"),
		 _T("ACE_STO_MOVE_TOP_08 STO_TOP_INDEX %s RT ACE_1"),
		 _T("ACE_STO_MOVE_TOP_08 STO_TOP_INDEX %s RT ACE_1"),
		 _T("ACE_STO_MOVE_TOP_08 STO_TOP_INDEX %s RT ACE_1"),
		 _T("ACE_STO_MOVE_TOP_08 STO_TOP_INDEX %s RT ACE_1"),
		 _T("ACE_STO_MOVE_TOP_08 STO_TOP_INDEX %s RT ACE_1"),
		 _T("ACE_STO_MOVE_TOP_08 STO_TOP_INDEX %s RT ACE_1")};
const CString strRTCSCmdName 														= _T("RTCS Cmd File");
const CString strRTCSCmdValue[nMaxSC] 												=
		{_T("RTCSCmd"),_T("RTCSCmd"),_T("RTCSCmd"),_T("RTCSCmd"),
		 _T("RTCSCmd"),_T("RTCSCmd"),_T("RTCSCmd"),_T("RTCSCmd")};
const CString strPaceCmdName 														= _T("Pace Cmd File");
const CString strPaceCmdValue[nMaxSC] 												=
		{_T("PaceCmd"),_T("PaceCmd"),_T("PaceCmd"),_T("PaceCmd"),
		 _T("PaceCmd"),_T("PaceCmd"),_T("PaceCmd"),_T("PaceCmd")};
const CString strFrameMiniSchedName 												= _T("FrameMiniSched File");
const CString strFrameMiniSchedValue[nMaxSC] 										=
		{_T("FrameMS"),_T("FrameMS"),_T("FrameMS"),_T("FrameMS"),
		 _T("FrameMS"),_T("FrameMS"),_T("FrameMS"),_T("FrameMS")};
const CString strFrameIOMiniSchedName 												= _T("FrameMiniSchedIO File");
const CString strFrameIOMiniSchedValue[nMaxSC] 										=
		{_T("FrameIOMS"),_T("FrameIOMS"),_T("FrameIOMS"),_T("FrameIOMS"),
		 _T("FrameIOMS"),_T("FrameIOMS"),_T("FrameIOMS"),_T("FrameIOMS")};
const CString strStarMiniSchedName 													= _T("StarMiniSched File");
const CString strStarMiniSchedValue[nMaxSC] 										=
		{_T("StarMS"),_T("StarMS"),_T("StarMS"),_T("StarMS"),
		 _T("StarMS"),_T("StarMS"),_T("StarMS"),_T("StarMS")};
const CString strStarIOMiniSchedName 												= _T("StarMiniSchedIO File");
const CString strStarIOMiniSchedValue[nMaxSC] 										=
		{_T("StarIOMS"),_T("StarIOMS"),_T("StarIOMS"),_T("StarIOMS"),
		 _T("StarIOMS"),_T("StarIOMS"),_T("StarIOMS"),_T("StarIOMS")};
const CString strSFCSMiniSchedName 													= _T("SFCSMiniSched File");
const CString strSFCSMiniSchedValue[nMaxSC] 										=
		{_T("SFCSMS"),_T("SFCSMS"),_T("SFCSMS"),_T("SFCSMS"),
		 _T("SFCSMS"),_T("SFCSMS"),_T("SFCSMS"),_T("SFCSMS")};
const CString strIntrusionRptName 													= _T("Sensor Intrusion Report");
const CString strIntrusionRptValue[nMaxSC] 											=
		{_T("Intrusion"),_T("Intrusion"),_T("Intrusion"),_T("Intrusion"),
		 _T("Intrusion"),_T("Intrusion"),_T("Intrusion"),_T("Intrusion")};
const CString strYawFlipFlgName 													= _T("Yaw Flip Flag");
const int nYawFlipFlgValue[nMaxSC] 													= {1,1,1,1,1,1,1,1};

const CString strScanDegsEWNadirImgrName 											= _T("Scan Degs EW Nadir Imgr ");
const CString strScanDegsEWNadirImgrValue[nMaxSC] 									=
		{_T("10.48"),_T("10.41"),_T("10.48"),_T("10.4"),
		 _T("10.4"),_T("10.4"),_T("10.4"),_T("10.4")};

const CString strScanDegsNSNadirImgrName 											= _T("ScanDegsNSNadirImgr");
const CString strScanDegsNSNadirImgrValue[nMaxSC] 									=
		{_T("10.5"),_T("10.5"),_T("10.5"),_T("10.5"),
		 _T("10.5"),_T("10.5"),_T("10.5"),_T("10.5")};

const CString strScanDegsEWNadirSdrName 											= _T("Scan Degrees EW Nadir Sdr");
const CString strScanDegsEWNadirSdrValue[nMaxSC] 									=
		{_T("10.6"),_T("10.4"),_T("10.6"),_T("10.4"),
		 _T("10.4"),_T("10.4"),_T("10.4"),_T("10.4")};

const CString strScanDegsNSNadirSdrName 											= _T("Scan Degrees NS Nadir Sdr");
const CString strScanDegsNSNadirSdrValue[nMaxSC] 									=
		{_T("10.5"),_T("10.5"),_T("10.5"),_T("10.5"),
		 _T("10.5"),_T("10.5"),_T("10.5"),_T("10.5")};

const CString strStarDegsEWNadirImgrName 											= _T("Star Degrees EW Nadir Imgr");
const CString strStarDegsEWNadirImgrValue[nMaxSC] 									=
		{_T("11.5"),_T("11.5"),_T("11.5"),_T("11.5"),
		 _T("11.5"),_T("11.5"),_T("11.5"),_T("11.5")};

const CString strStarDegsNSNadirImgrName 											= _T("StarDegsNSNadirImgr");
const CString strStarDegsNSNadirImgrValue[nMaxSC] 									=
		{_T("10.5"),_T("10.5"),_T("10.5"),_T("10.5"),
		 _T("10.5"),_T("10.5"),_T("10.5"),_T("10.5")};

const CString strStarDegsEWNadirSdrName 											= _T("Star Degrees EW Nadir Sdr");
const CString strStarDegsEWNadirSdrValue[nMaxSC] 									=
		{_T("11.5"),_T("11.5"),_T("11.5"),_T("11.5"),
		 _T("11.5"),_T("11.5"),_T("11.5"),_T("11.5")};

const CString strStarDegsNSNadirSdrName 											= _T("Star Degrees NS Nadir Sdr");
const CString strStarDegsNSNadirSdrValue[nMaxSC] 									=
		{_T("10.5"),_T("10.5"),_T("10.5"),_T("10.5"),
		 _T("10.5"),_T("10.5"),_T("10.5"),_T("10.5")};

const CString strInstrCenterPtImgrNSName 											= _T("Instr Center Pt Imgr NS");
const int nInstrCenterPtImgrNSValue[nMaxSC] 										=
		{28166, 27557, 27556, 27612, 27612, 27612, 27612, 27612};

const CString strInstrCenterPtImgrEWName 											= _T("Instr Center Pt Imgr EW");
const int nInstrCenterPtImgrEWValue[nMaxSC] 										=
		{15352, 15464, 15399, 15340, 15340, 15340, 15340, 15340};

const CString strInstrCenterPtSdrNSName  											= _T("Instr Center Pt Sdr NS");
const int nInstrCenterPtSdrNSValue[nMaxSC] 											=
		{12441, 12490, 12566, 12622, 12622, 12622, 12622, 12622};

const CString strInstrCenterPtSdrEWName  											= _T("Instr Center Pt Sdr EW");
const int nInstrCenterPtSdrEWValue[nMaxSC] 											=
		{7058, 7040, 7013, 7012, 7012, 7012, 7012, 7012};

const CString strSpaceLookSdrWestName  												= _T("SpaceLook Sdr West");
const int nSpaceLookSdrWestValue[nMaxSC] 											=
		{1824, 1824, 1824, 1824, 1824, 1824, 1824, 1824};

const CString strSpaceLookSdrEastName  												= _T("SpaceLook Sdr East");
const int nSpaceLookSdrEastValue[nMaxSC] 											=
		{12202, 12202, 12202, 12202, 12202, 12202, 12202, 12202};

const CString strSpaceLookImgrWestName  											= _T("SpaceLook Imgr West");
const int nSpaceLookImgrWestValue[nMaxSC] 											=
		{3996, 3996, 3996, 3996, 3996, 3996, 3996, 3996};

const CString strSpaceLookImgrEastName  											= _T("SpaceLook Imgr East");
const int nSpaceLookImgrEastValue[nMaxSC] 											=
		{26684, 26684, 26684, 26684, 26684, 26684, 26684, 26684};

const CString strTimeTagSpacingName  												= _T("TimeTagSpacing");
const CString strTimeTagSpacingValue[nMaxSC] 										=
		{"2.0", "2.0", "2.0", "2.0", "2.0", "2.0", "2.0", "2.0"};

const CString strNumCmdFrameFileName												= _T("Number CMD Frame File Name");
const CString strNumCmdFrameFileValue[nMaxSC] =
		{"goes13_a_and_b_cmdsize","goes14_a_and_b_cmdsize","goes15_a_and_b_cmdsize",
		 "goes16_a_and_b_cmdsize","goes17_a_and_b_cmdsize","goes18_a_and_b_cmdsize",
		 "goes19_a_and_b_cmdsize","goes20_a_and_b_cmdsize"};

const TCHAR strCmdFrameUplinkRateName[]  											= _T("Number of Datawords per Second");
const int nCmdFrameUplinkRateValue[nMaxSC] 											= {13,13,13,13,13,13,13,13};

const CString strSTOLCmdName 														= _T("STOL Cmd File");
const CString strSTOLCmdValue[nMaxSC] 												=
		{_T("STOLCmd"),_T("STOLCmd"),_T("STOLCmd"),_T("STOLCmd"),
		 _T("STOLCmd"),_T("STOLCmd"),_T("STOLCmd"),_T("STOLCmd")};

const CString strSRSOSpacelookModeName 												= _T("SRSO Spacelook Mode");
const CString strSRSOSpacelookModeValue[nMaxSC]										=
		{_T("Slow Space"), _T("Slow Space"), _T("Slow Space"), _T("Slow Space"),
		 _T("Slow Space"), _T("Slow Space"), _T("Slow Space"), _T("Slow Space")};

const CString strSRSOFrameLabelName 												= _T("SRSO Scan Frame Label");
const CString strSRSOFrameLabelValue[nMaxSC]										=
		{_T("SRSO"), _T("SRSO"), _T("SRSO"), _T("SRSO"),
		 _T("SRSO"), _T("SRSO"), _T("SRSO"), _T("SRSO")};

class CRegSC : public CRegGblEx
{
public:
	CRegSC();
	virtual ~CRegSC();
	void Init();
	void SetValues();
//	int		m_nDefault;
	CString	m_strTitle[nMaxSC];
	CString	m_strAbbrev[nMaxSC];
	CString	m_strDir[nMaxSC];
	int		m_nID[nMaxSC];
	int		m_nFrameSize[nMaxSC];				// Size of an individual onboard Frame Table
	int		m_nFrameSRSOStart[nMaxSC];			// Start SRSO Frame number
	int		m_nStarSize[nMaxSC];				// Size of the onboard Star Table
	int		m_nSTOSize[nMaxSC];					// Size of an individual onboard STO Buffer
	int		m_nSTORTCSSize[nMaxSC];				// Start of the RTCS area within the STO
	int		m_nSKBSize[nMaxSC];					// Size of an individual onboard SKB Buffer
	int		m_nSKBSegSize[nMaxSC];				// Segment size to use when upload to the SKB Buffer
	int		m_nNumRTCS[nMaxSC];					// Number of RTCS that can be in the STO Buffer (for validation)
	int		m_nCTCUID[nMaxSC];					// The CTCU ID
	CString	m_strRTAddress[nMaxSC];				// Command line RT Address
	CString m_strDatabaseName[nMaxSC];			// Name of the database (no directory or extension)
	CString	m_strMaxCmdGap[nMaxSC];				// Spans greater than this without cmds will generate warnings - hh:mm:ss
	CString	m_strPreHKGap[nMaxSC];				// Setup time before a HK maneuver - hh:mm:ss
	int		m_nRetryCnt[nMaxSC];				// Count used during validatation only
	float	m_fCmdExeTime[nMaxSC];				// Cmd execution time used during validation only - floating point seconds
	CString	m_strSTOGotoTopCmd[nMaxSC];			// Command used to seperate RTCSs
	CString	m_strRTCSCmd[nMaxSC];				// RTCS Command Restriction file
	CString	m_strPaceCmd[nMaxSC];				// Command Pacing File
	CString	m_strFrameMiniSched[nMaxSC];		// Frame Mini Schedule File
	CString	m_strFrameIOMiniSched[nMaxSC];		// Frame Mini Schedule File that uses Instrument Object commands
	CString	m_strStarMiniSched[nMaxSC];			// Star Mini Schedule File
	CString	m_strStarIOMiniSched[nMaxSC];		// Star Mini Schedule File that used Instrument Object commands
	CString	m_strSFCSMiniSched[nMaxSC];			// SFCS Mini Schedule File
	CString m_strIntrusionRpt[nMaxSC];      	// Sensor Intrusion Reprot file name; used by validation.
	int     m_nYawFlipFlg[nMaxSC];          	// Yaw flip flag used by validation.
	CString m_strScanDegsEWNadirImgr[nMaxSC];
	CString m_strScanDegsNSNadirImgr[nMaxSC];
	CString m_strScanDegsEWNadirSdr[nMaxSC];
	CString m_strScanDegsNSNadirSdr[nMaxSC];
	CString m_strStarDegsEWNadirImgr[nMaxSC];
	CString m_strStarDegsNSNadirImgr[nMaxSC];
	CString m_strStarDegsEWNadirSdr[nMaxSC];
	CString m_strStarDegsNSNadirSdr[nMaxSC];
	int     m_nInstrCenterPtImgrNS[nMaxSC];
	int     m_nInstrCenterPtImgrEW[nMaxSC];
	int     m_nInstrCenterPtSdrNS[nMaxSC];
	int     m_nInstrCenterPtSdrEW[nMaxSC];
	int		m_nSpaceLookSdrWest[nMaxSC];
	int     m_nSpaceLookSdrEast[nMaxSC];
	int     m_nSpaceLookImgrWest[nMaxSC];
	int     m_nSpaceLookImgrEast[nMaxSC];
	CString m_strTimeTagSpacing[nMaxSC];
	CString m_strNumCmdFrameFile[nMaxSC];		// This is the name of the file containing the command frames.
	int     m_nCmdFrameUplinkRate[nMaxSC];		// This is the number of command frames can be uplinked per second (13/sec).
	CString	m_strSTOLCmd[nMaxSC];				// STOL Command Restriction file
	CString	m_strSRSOSpacelookMode[nMaxSC];		// SRSO Spacelook Mode (Slow Space)
	CString	m_strSRSOFrameLabel[nMaxSC];		// SRSO Scan Frame Schedule Label
};

const CString strRegGTACSKey 														= _T("GTACS");
const CString strRegGTACSDefaultName 												= _T("Default");
const int	  nRecGTACSDefaultDefValue[nMaxSites] 									= {0, 0, 0};
const CString strRegGTACSTitleName 													= _T("Title");
const CString strRegGTACSTitleDefValue[nMaxSites][nMaxGTACSperSite] 				=
		{_T("SOCC GTACS 1"),_T("SOCC GTACS 2"),_T("SOCC GTACS 3"),_T("SOCC GTACS 4"),_T("SOCC GTACS 5"),
		 _T("Wallops GTACS 1"),_T("Wallops GTACS 2"),_T("Wallops GTACS 3"),_T(""),_T(""),
		 _T("Goddard GTACS 1"),_T(""),_T(""),_T(""),_T(""),
		 _T("GOES-IO GTACS 1"),_T(""),_T(""),_T(""),_T(""),
		 _T("Fairbanks GTACS 1"),_T(""),_T(""),_T(""),_T("")};
const CString strRegGTACSAbbrevName 												= _T("Abbreviation");
const CString strRegGTACSAbbrevDefValue[nMaxSites][nMaxGTACSperSite]				=
		{_T("SGTACS01"),_T("SGTACS02"),_T("SGTACS03"),_T("SGTACS04"),_T("SGTACS05"),
		 _T("CGTACS01"),_T("CGTACS02"),_T("CGTACS03"),_T(""),_T(""),
		 _T("GGTACS01"),_T(""),_T(""),_T(""),_T(""),
		 _T("DGTACS01"),_T(""),_T(""),_T(""),_T(""),
		 _T("FGTACS01"),_T(""),_T(""),_T(""),_T("")};
const CString strRegGTACSProcShareName 												= _T("Procedure Share");
const CString strRegGTACSProcShareDefValue[nMaxSites][nMaxGTACSperSite] 			=
		{_T("$GTACS_PROCS"),_T("$GTACS_PROCS"),_T("$GTACS_PROCS"),_T("$GTACS_PROCS"),_T("$GTACS_PROCS"),
		 _T("$GTACS_PROCS"),_T("$GTACS_PROCS"),_T("$GTACS_PROCS"),_T(""),_T(""),
		 _T("$GTACS_PROCS"),_T(""),_T(""),_T(""),_T(""),
		 _T("$GTACS_PROCS"),_T(""),_T(""),_T(""),_T(""),
		 _T("$GTACS_PROCS"),_T(""),_T(""),_T(""),_T("")};
const CString strRegGTACSDBShareName 												= _T("Database Share");
const CString strRegGTACSDBShareDefValue[nMaxSites][nMaxGTACSperSite] 				=
		{_T("$GTACS_DBASE"),_T("$GTACS_DBASE"),_T("$GTACS_DBASE"),_T("$GTACS_DBASE"),_T("$GTACS_DBASE"),
		 _T("$GTACS_DBASE"),_T("$GTACS_DBASE"),_T("$GTACS_DBASE"),_T(""),_T(""),
		 _T("$GTACS_DBASE"),_T(""),_T(""),_T(""),_T(""),
		 _T("$GTACS_DBASE"),_T(""),_T(""),_T(""),_T(""),
		 _T("$GTACS_DBASE"),_T(""),_T(""),_T(""),_T("")};

class CRegGTACS : public CRegGblEx
{
public:
	CRegGTACS();
	virtual ~CRegGTACS();
	virtual void Init();
	virtual void SetValues();
	CString	m_strTitle[nMaxSites][nMaxGTACSperSite];
	CString	m_strAbbrev[nMaxSites][nMaxGTACSperSite];
	CString	m_strProcShare[nMaxSites][nMaxGTACSperSite];
	CString	m_strDBShare[nMaxSites][nMaxGTACSperSite];
	int		m_nDefault[nMaxSites];
};

const CString strRegOATSKey 														= _T("OATS");
const CString strRegOATSDefaultName 												= _T("Default");
const int	  nRegOATSDefaultDefValue[nMaxSites] 									= {0, 0, 0};
const CString strRegOATSPortName 													= _T("Port");
const int	  nRegOATSPortDefValue 													= 5001;
const CString strRegOATSTimeoutName 												= _T("Timeout");
const int	  nRegOATSTimeoutDefValue 												= 45;
const CString strRegOATSTitleName 													= _T("Title");
const CString strRegOATSTitleDefValue[nMaxSites][nMaxOATSperSite] 					=
		{_T("SOCC OATS 11"),_T("SOCC OATS 12"),_T("SOCC OATS 13"),_T("SOCC OATS 14"),_T("SOCC OATS 15"),_T("SOCC OATS 16"),_T("SOCC OATS 17"),
		 _T("Wallops OATS 11"),_T("Wallops OATS 12"),_T("Wallops OATS 13"),_T(""),_T(""),_T(""),_T(""),
		 _T("Goddard OATS 11"),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),
		 _T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),
		 _T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T("")};
const CString strRegOATSAbbrevName 													= _T("Abbreviation");
const CString strRegOATSAbbrevDefValue[nMaxSites][nMaxOATSperSite] 					=
		{_T("SOAT11"),_T("SOAT12"),_T("SOAT13"),_T("SOAT14"),_T("SOAT15"),_T("SOAT16"),_T("SOAT17"),
		 _T("COAT11"),_T("COAT12"),_T("COAT13"),_T(""),_T(""),_T(""),_T(""),
		 _T("GOAT11"),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),
		 _T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),
		 _T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T("")};

// Manan Dalal - OSPO - August 2014
// Added new registry values for RSO
// Globals used to store RSO update info.

class CRegOATS : public CRegGblEx
{
public:
	CRegOATS();
	virtual ~CRegOATS();
	virtual void Init();
	virtual void SetValues();
	CString	m_strTitle[nMaxSites][nMaxOATSperSite];
	CString	m_strAbbrev[nMaxSites][nMaxOATSperSite];
	int		m_nPort;
	int		m_nTimeout;
	int		m_nDefault[nMaxSites];
};

const CString strRegRSOKey															= _T("RSO");
const CString strRegRSOSCName														= _T("SCName");
const CString strRegRSOSCNameDefValue[nMaxSC] 										=
		{_T("Goes 13"),_T("Goes 14"),_T("Goes 15"),_T("Goes 16"),
	 	 _T("Goes 17 (13)"),_T("Goes 18 (14)"),_T("Goes 19 (15)"),_T("Goes 20 (16)")};
const CString strRegRSOFrmTableName													= _T("Frame Table Name");
const CString strRegRSOFrmTableDefValue[nMaxSC]										= 
		{_T("Default"), _T("Default"), _T("Default"), _T("Default"), 
		 _T("Default"), _T("Default"), _T("Default"), _T("Default")};
const CString strRegRSOCurRSOLabelName												= _T("Current RSO Label");
const CString strRegRSOCurRSOLabelDefValue[nMaxSC]									=
		{_T("CW_PACUS_SUB_RSO_366"), _T("CW_PACUS_SUB_RSO_366"), _T("CW_PACUS_SUB_RSO_366"), _T("CW_PACUS_SUB_RSO_366"),
		 _T("CW_PACUS_SUB_RSO_366"), _T("CW_PACUS_SUB_RSO_366"), _T("CW_PACUS_SUB_RSO_366"), _T("CW_PACUS_SUB_RSO_366")};
const CString strRegRSONewRSOLabel1Name												= _T("New RSO Label 1");
const CString strRegRSONewRSOLabel1DefValue[nMaxSC]									=
		{_T("CW_ALASKA_RSO_366"), _T("CW_ALASKA_RSO_366"), _T("CW_ALASKA_RSO_366"), _T("CW_ALASKA_RSO_366"),
		 _T("CW_ALASKA_RSO_366"), _T("CW_ALASKA_RSO_366"), _T("CW_ALASKA_RSO_366"), _T("CW_ALASKA_RSO_366")};
const CString strRegRSONewRSOLabel2Name												= _T("New RSO Label 2");
const CString strRegRSONewRSOLabel2DefValue[nMaxSC]									=
		{_T("CW_HAWAII_RSO_366"), _T("CW_HAWAII_RSO_366"), _T("CW_HAWAII_RSO_366"), _T("CW_HAWAII_RSO_366"),
		 _T("CW_HAWAII_RSO_366"), _T("CW_HAWAII_RSO_366"), _T("CW_HAWAII_RSO_366"), _T("CW_HAWAII_RSO_366")};
const CString strRegRSONewRSOLabel3Name												= _T("New RSO Label 3");
const CString strRegRSONewRSOLabel3DefValue[nMaxSC]									=
		{_T("CW_SITKA_RSO_366"), _T("CW_SITKA_RSO_366"), _T("CW_SITKA_RSO_366"), _T("CW_SITKA_RSO_366"),
		 _T("CW_SITKA_RSO_366"), _T("CW_SITKA_RSO_366"), _T("CW_SITKA_RSO_366"), _T("CW_SITKA_RSO_366")};
const CString strRegRSONewRSOLabel4Name												= _T("New RSO Label 4");
const CString strRegRSONewRSOLabel4DefValue[nMaxSC]									=
		{_T("CW_TPARC_RSO_366"), _T("CW_TPARC_RSO_366"), _T("CW_TPARC_RSO_366"), _T("CW_TPARC_RSO_366"),
		 _T("CW_TPARC_RSO_366"), _T("CW_TPARC_RSO_366"), _T("CW_TPARC_RSO_366"), _T("CW_TPARC_RSO_366")};
const CString strRegRSONewRSOLabel5Name												= _T("New RSO Label 5");
const CString strRegRSONewRSOLabel5DefValue[nMaxSC]									=
		{_T(""), _T(""), _T(""), _T(""),
		 _T(""), _T(""), _T(""), _T("")};
const CString strRegRSONewRSOLabel6Name												= _T("New RSO Label 6");
const CString strRegRSONewRSOLabel6DefValue[nMaxSC]									=
		{_T(""), _T(""), _T(""), _T(""),
		 _T(""), _T(""), _T(""), _T("")};
const CString strRegRSONewRSOLabel7Name												= _T("New RSO Label 7");
const CString strRegRSONewRSOLabel7DefValue[nMaxSC]									=
		{_T(""), _T(""), _T(""), _T(""),
		 _T(""), _T(""), _T(""), _T("")};
const CString strRegRSONewRSOLabel8Name												= _T("New RSO Label 8");
const CString strRegRSONewRSOLabel8DefValue[nMaxSC]									=
		{_T(""), _T(""), _T(""), _T(""),
		 _T(""), _T(""), _T(""), _T("")};
const CString strRegRSONewRSOLabel9Name												= _T("New RSO Label 9");
const CString strRegRSONewRSOLabel9DefValue[nMaxSC]									=
		{_T(""), _T(""), _T(""), _T(""),
		 _T(""), _T(""), _T(""), _T("")};
const CString strRegRSONewRSOLabel10Name											= _T("New RSO Label 10");
const CString strRegRSONewRSOLabel10DefValue[nMaxSC]								=
		{_T(""), _T(""), _T(""), _T(""),
		 _T(""), _T(""), _T(""), _T("")};

class CRegRSO : public CRegGblEx
{
public:
	CRegRSO();
	virtual ~CRegRSO();
	virtual void Init();
	virtual void SetValues();
	//Variables
	CString	m_strTitle[nMaxSC];									//SC title
	CString	m_strFrmDir[nMaxSC];								//Database directory
	CString m_strFrmTable[nMaxSC];								//Frame table name
	CString m_strCurRSOName[nMaxSC];							//Current RSO label to replace
	CString m_strNewRSOName1[nMaxSC];							//New RSO label to replace with
	CString m_strNewRSOName2[nMaxSC];
	CString m_strNewRSOName3[nMaxSC];
	CString m_strNewRSOName4[nMaxSC];
	CString m_strNewRSOName5[nMaxSC];
	CString m_strNewRSOName6[nMaxSC];
	CString m_strNewRSOName7[nMaxSC];
	CString m_strNewRSOName8[nMaxSC];
	CString m_strNewRSOName9[nMaxSC];
	CString m_strNewRSOName10[nMaxSC];
};
/*
	const CString strRegMakeSchedKey							= _T("MakeSched");
	const CString strRegMakeSchedCLSYearName					= _T("CLS Year");
	const int     nRegMakeSchedCLSYearDefValue					= 2000;
	const CString strRegMakeSchedFrmGenName						= _T("Gen Frame Table");
	const int	  nRegMakeSchedFrmGenDefValue					= 0;
	const CString strRegMakeSchedFrmUpdateName					= _T("Frame Update");
	const int	  nRegMakeSchedFrmUpdateDefValue				= 0;
	const CString strRegMakeSchedFrmSRSOName					= _T("SRSO Frames");
	const int	  nRegMakeSchedFrmSRSODefValue					= 0;
	const CString strRegMakeSchedNewFrmTblName					= _T("New Frame Table");
	const int	  nRegMakeSchedNewFrmTblDefValue				= 0;
	const CString strRegMakeSchedSecTblFileNameName				= _T("Sector Table Name");
	const CString strRegMakeSchedSecTblFileNameDefValue			= _T("MakeSched");
	const CString strRegMakeSchedFrmTblFileNameName				= _T("Frame Table Name");
	const CString strRegMakeSchedFrmTblFileNameDefValue			= _T("Default");
	const CString strRegMakeSchedRTCSGenName					= _T("RTCS Gen");
	const int	  nRegMakeSchedRTCSGenDefValue					= 0;
	const CString strRegMakeSchedRTCSUpdateName					= _T("RTCS Update");
	const int	  nRegMakeSchedRTCSUpdateDefValue				= 0;
	const CString strRegMakeSchedRTCSMapFileNameName			= _T("RTCS Map File Name");
	const CString strRegMakeSchedRTCSMapFileNameDefValue		= _T("Default");
	const CString strRegMakeSchedRTCSSetFileNameName			= _T("RTCS Set File Name");
	const CString strRegMakeSchedRTCSSetFileNameDefValue		= _T("Default");
	const CString strRegMakeSchedOBSchedGenName					= _T("OB Sched Gen");
	const int	  nRegMakeSchedOBSchedGenDefValue				= 0;
	const CString strRegMakeSchedValSchedName					= _T("Sched Val");
	const int	  nRegMakeSchedValSchedDefValue					= 0;
	const CString strRegMakeSchedRTCSValName					= _T("RTCS Val");
	const int	  nRegMakeSchedRTCSValDefValue					= 0;
	const CString strRegMakeSchedValSchedTypeName				= _T("Sched Val Type");
	const int	  nRegMakeSchedValSchedTypeDefValue				= 0;
	const CString strRegMakeSchedStarUpdateName					= _T("Star Update");
	const int	  nRegMakeSchedStarUpdateDefValue				= 0;
	const CString strRegMakeSchedStarGenName					= _T("Star Gen");
	const int	  nRegMakeSchedStarGenDefValue					= 0;
	const CString strRegMakeSchedStaticFramesName				= _T("Static Frames");
	const int	  nRegMakeSchedStaticFramesDefValue				= 0;


class CRegMakeSched : public CRegGblEx
{
public:
	CRegMakeSched();
	virtual ~CRegMakeSched();
	virtual void Init();
	virtual void SetValues();

	int     m_nCLSYear;
	int     m_nFrmGen;
	int     m_nFrmUpdate;
	int     m_nFrmSRSO;
	int     m_nNewFrmTbl;
	CString m_strSecTblFileName;
	CString m_strFrmTblFileName;
	int     m_nRTCSGen;
	int		m_nRTCSUpdate;
	CString m_strRTCSMapFileName;
	CString m_strRTCSSetFileName;
	int		m_nOBSchedGen;
    int		m_nValSched;
	int		m_nRTCSVal;
	int		m_nValSchedType;
	int		m_nStarUpdate;
	int		m_nStarGen;
	int     m_nStaticFrames;
};
*/
#endif // !defined(AFX_REGISTRYGBL_H__FCB3AF3A_D5CA_11D4_800D_00609704053C__INCLUDED_)
