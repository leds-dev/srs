/**
/////////////////////////////////////////////////////////////////////////////
// RegistryUsr.cpp : implementation of the Registry class.                 //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
// Revision History														   //
// Manan Dalal - OSPO - August 2014										   //
// PR000951 - Enhance SchedMgrECP with RSO Sectors						   //
// Added few globals for user registry used to determine if user has	   //
// selected to do RSO sector update										   //
// Released in Build16.2												   //
/////////////////////////////////////////////////////////////////////////////

// RegistryUsr.cpp: implementation of the various registory classes
//
/////////////////////////////////////////////////////////////////////////////
// This class is used for updating and restoring the registry database. 
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "stdafx.h"
#include "RegistryUsr.h"
#include "registryGbl.h"
#include "shlwapi.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegUsrEx::CRegEx
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegUsrEx::CRegUsrEx(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegUsrEx::~CRegEx
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegUsrEx::~CRegUsrEx(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegUsrEx::DeleteKey
//	Description :	This routine that deletes a registry key.
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CRegUsrEx::DeleteKey()
{
	ASSERT_VALID(this);

	m_nLastError = ERROR_SUCCESS;
	// Delete key

	// Get the key name
	CString sKeyName = GetKeyNames();

	// Get the root key
	if( (m_hRootKey == NULL) && !OpenRootKey() )
	{
		ASSERT(!SUCCEEDED(m_nLastError));
		return SUCCEEDED(m_nLastError);
	}

	// Remove possible leading and trailing backslash from key names
	if( !sKeyName.IsEmpty() )
	{
		// ... Non empty key name must begin with and end in back slash
		ASSERT(sKeyName.GetAt(0) == m_cBackslash);
		ASSERT(sKeyName.GetAt(sKeyName.GetLength() - 1) == m_cBackslash);
		sKeyName = sKeyName.Mid(1, sKeyName.GetLength() - 2);
	}

	// Close the key (not the rootkey) before deleting it
	CloseKey();
	HRESULT nLastError = HResultFromWin32(::SHDeleteKey(m_hRootKey, sKeyName));
	// ... Close all keys now
	Close();
	// ... Use the result of the failed Open (not the succeeded Close)
	m_nLastError = nLastError;

	ASSERT_VALID(this);
	return SUCCEEDED(m_nLastError);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegSite::CRegSite
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegSite::CRegSite(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegSite::~CRegSite
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegSite::~CRegSite(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegSite::Init
//	Description :	This routine initializes site
//					registry values.
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegSite::Init()
{
	InitializeFromApplication(strRegSiteKey);
	m_strFullRegKey = GetFullRegistryItem();

	m_nDefaultGTACS = GetNumberValue(strRegSiteDefaultGTACSName, TRUE, nRecSiteDefaultGTACSDefValue);
	m_nDefaultOATS  = GetNumberValue(strRegSiteDefaultOATSName, TRUE, nRecSiteDefaultOATSDefValue);
	m_nDefaultSC    = GetNumberValue(strRegSiteDefaultSCName, TRUE, nRecSiteDefaultSCDefValue);
	
	for (int nIndex=0; nIndex < nMaxSites; nIndex++)
	{
		m_strTitle[nIndex] = strRegSiteTitleDefValue[nIndex];
		m_strAbbrev[nIndex] = strRegSiteAbbrevDefValue[nIndex];
		m_nDefaultGTACSPerSite[nIndex] = GetNumberValue(strRegGTACSDefaultName, TRUE, nRecGTACSDefaultDefValue[nIndex]);
	    m_nDefaultOATSPerSite[nIndex]= GetNumberValue(strRegOATSDefaultName, TRUE, nRegOATSDefaultDefValue[nIndex]);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegSite::SetValues
//	Description :	This routine sets all site registry values.
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegSite::SetValues()
{
	SetKeyNames(m_strFullRegKey);
	SetNumberValue(m_nDefaultGTACS, strRegSiteDefaultGTACSName);
	SetNumberValue(m_nDefaultOATS,  strRegSiteDefaultOATSName);
	SetNumberValue(m_nDefaultSC,    strRegSiteDefaultSCName);

	for (int nIndex=0; nIndex < nMaxSites; nIndex++)
	{
		CString strSiteKey;
		strSiteKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegSiteKey, nIndex);
		SetKeyNames(strSiteKey);
		SetStringValue(m_strTitle[nIndex], strRegSiteTitleName);
		SetStringValue(m_strAbbrev[nIndex], strRegSiteAbbrevName);
		SetNumberValue(m_nDefaultGTACSPerSite[nIndex], strRegGTACSDefaultName);
	    SetNumberValue(m_nDefaultOATSPerSite[nIndex], strRegOATSDefaultName);
	}
	
	Flush();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMisc::CRegMisc
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegMisc::CRegMisc(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMisc::~CRegMisc
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegMisc::~CRegMisc(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMisc::Init
//	Description :	This routine initializes all Misc registry values.					
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegMisc::Init()
{
	InitializeFromApplication(strRegMiscKey);

	m_strFullRegKey		= GetFullRegistryItem();
	m_nDOY				= GetNumberValue(strRegMiscDayName,				TRUE,		nRecMiscDayDefValue);
	m_strLeftMargin		= GetStringValue(strRegMiscLeftMarginName,		TRUE, TRUE, strRegMiscLeftMarginDefValue);
	m_strRightMargin	= GetStringValue(strRegMiscRightMarginName,		TRUE, TRUE, strRegMiscRightMarginDefValue);
	m_strTopMargin		= GetStringValue(strRegMiscTopMarginName,		TRUE, TRUE, strRegMiscTopMarginDefValue);
	m_strBottomMargin	= GetStringValue(strRegMiscBottomMarginName,	TRUE, TRUE, strRegMiscBottomMarginDefValue);
	m_bShowExt			= GetNumberValue(strRegMiscShowExtName,			TRUE,		nRegMiscShowExtValue);
	m_bSortByExt		= GetNumberValue(strRegMiscSortByExtName,		TRUE,		nRegMiscSortByExtValue);
	m_strDBDrive		= GetStringValue(strRegMiscDBDriveName,			TRUE, TRUE, strRegMiscDBDriveValue);
	m_strCmdExe			= GetStringValue(strRegMiscCmdExeName,			TRUE, TRUE, strRegMiscCmdExeValue);
	m_strFontFaceName	= GetStringValue(strRegMiscFontFaceName,		TRUE, TRUE, strRegMiscFontFaceValue);
	m_nFontSize			= GetNumberValue(strRegMiscFontSizeName,		TRUE,		nRegMiscFontSizeValue);
	m_nFontWeight		= GetNumberValue(strRegMiscFontWeightName,		TRUE,		nRegMiscFontWeightValue);
	m_bFontIsStrikeOut	= GetNumberValue(strRegMiscFontStrikeOutName,	TRUE,		nRegMiscFontStrikeOutValue);
	m_bFontIsUnderline	= GetNumberValue(strRegMiscFontUnderlineName,	TRUE,		nRegMiscFontUnderlineValue);
	m_bFontIsItalic		= GetNumberValue(strRegMiscFontItalicName,		TRUE,		nRegMiscFontItalicValue);
	m_bMuteSound		= GetNumberValue(strRegMiscMuteSoundName,		TRUE,		nRegMiscMuteSoundValue);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMisc::SetValues
//	Description :	This routine sets all Misc registry values.					
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegMisc::SetValues()
{
	SetKeyNames(m_strFullRegKey);
	SetNumberValue(m_nDOY,				strRegMiscDayName);
	SetStringValue(m_strLeftMargin,		strRegMiscLeftMarginName);
	SetStringValue(m_strRightMargin,	strRegMiscRightMarginName);
	SetStringValue(m_strTopMargin,		strRegMiscTopMarginName);
	SetStringValue(m_strBottomMargin,	strRegMiscBottomMarginName);
	SetNumberValue(m_bShowExt,			strRegMiscShowExtName);
	SetNumberValue(m_bSortByExt,		strRegMiscSortByExtName);
	SetStringValue(m_strDBDrive,		strRegMiscDBDriveName);
	SetStringValue(m_strCmdExe,			strRegMiscCmdExeName);
	SetStringValue(m_strFontFaceName,	strRegMiscFontFaceName);
	SetNumberValue(m_nFontSize,			strRegMiscFontSizeName);
	SetNumberValue(m_nFontWeight,		strRegMiscFontWeightName);
	SetNumberValue(m_bFontIsStrikeOut,	strRegMiscFontStrikeOutName);
	SetNumberValue(m_bFontIsUnderline,	strRegMiscFontUnderlineName);
	SetNumberValue(m_bFontIsItalic,		strRegMiscFontItalicName);
	SetNumberValue(m_bMuteSound,		strRegMiscMuteSoundName);
	Flush();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegInstObj::CRegInstObj
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegInstObj::CRegInstObj(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegInstObj::~CRegInstObj
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegInstObj::~CRegInstObj(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegInstObj::Init
//	Description :	This routine initializes all Instrument Object 
//					registry values.					
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegInstObj::Init()
{
	InitializeFromApplication(strRegInstObjKey);

	m_strFullRegKey = GetFullRegistryItem();

	for (int nInstObj=0; nInstObj < nMaxInstObj; nInstObj++)
	{
		CString strInstObjKey;
		strInstObjKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegInstObjObjKey, nInstObj);
		SetKeyNames(strInstObjKey);

		m_strTitle[nInstObj]		= GetStringValue(strRegInstObjTitleName,		TRUE, TRUE, strRegInstObjTitleDefValue[nInstObj]);
		m_strLeftMargin[nInstObj]	= GetStringValue(strRegInstObjLeftMarginName,	TRUE, TRUE, strRegInstObjLeftMarginDefValue[nInstObj]);
		m_strRightMargin[nInstObj]	= GetStringValue(strRegInstObjRightMarginName,	TRUE, TRUE, strRegInstObjRightMarginDefValue[nInstObj]);
		m_strTopMargin[nInstObj]	= GetStringValue(strRegInstObjTopMarginName,	TRUE, TRUE, strRegInstObjTopMarginDefValue[nInstObj]);
		m_strBottomMargin[nInstObj] = GetStringValue(strRegInstObjBottomMarginName, TRUE, TRUE, strRegInstObjBottomMarginDefValue[nInstObj]);
		m_strFontFaceName[nInstObj] = GetStringValue(strRegInstObjFontFaceName,		TRUE, TRUE, strRegInstObjFontFaceValue[nInstObj]);
		m_nFontSize[nInstObj]		= GetNumberValue(strRegInstObjFontSizeName,		TRUE,		nRegInstObjFontSizeValue[nInstObj]);
		m_nFontWeight[nInstObj]		= GetNumberValue(strRegInstObjFontWeightName,	TRUE,		nRegInstObjFontWeightValue[nInstObj]);
		m_bFontIsStrikeOut[nInstObj]= GetNumberValue(strRegInstObjFontStrikeOutName,TRUE,		nRegInstObjFontStrikeOutValue[nInstObj]);
		m_bFontIsUnderline[nInstObj]= GetNumberValue(strRegInstObjFontUnderlineName,TRUE,		nRegInstObjFontUnderlineValue[nInstObj]);
		m_bFontIsItalic[nInstObj]	= GetNumberValue(strRegInstObjFontItalicName,	TRUE,		nRegInstObjFontItalicValue[nInstObj]);

		for (int nProperty=0; nProperty<nMaxInstObjProps[nInstObj]; nProperty++)
		{
			CString strInstObjPropKey;
			strInstObjPropKey.Format(_T("%s%s-%d\\%s-%d"), m_strFullRegKey, strRegInstObjObjKey, nInstObj, strRegInstObjPropKey, nProperty);
			SetKeyNames(strInstObjPropKey);

			m_strPropTitle[nInstObj][nProperty]		= GetStringValue(strRegInstObjTitleName,			TRUE, TRUE, strRegInstObjPropTitleDefValue[nInstObj][nProperty]);
			m_nDisplay[nInstObj][nProperty]			= GetNumberValue(strRegInstObjDisplayName,			TRUE,		nRegInstObjDisplayDefValue[nInstObj][nProperty]);
			m_nDisplayWidth[nInstObj][nProperty]	= GetNumberValue(strRegInstObjDisplayWidthName,		TRUE,		nRegInstObjDisplayWidthDefValue[nInstObj][nProperty]);
			m_nDisplayJustify[nInstObj][nProperty]	= GetNumberValue(strRegInstObjDisplayJustifyName,	TRUE,		nRegInstObjDisplayJustifyDefValue[nInstObj][nProperty]);
			m_nPrint[nInstObj][nProperty]			= GetNumberValue(strRegInstObjPrintName,			TRUE,		nRegInstObjPrintDefValue[nInstObj][nProperty]);
			m_nPrintWidth[nInstObj][nProperty]		= GetNumberValue(strRegInstObjPrintWidthName,		TRUE,		nRegInstObjPrintWidthDefValue[nInstObj][nProperty]);
			m_nPrintJustify[nInstObj][nProperty]	= GetNumberValue(strRegInstObjPrintJustifyName,		TRUE,		nRegInstObjPrintJustifyDefValue[nInstObj][nProperty]);
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegInstObj::SetValues
//	Description :	This routine sets all Instrument Object registry 
//					values.					
//
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegInstObj::SetValues()
{
	SetKeyNames(m_strFullRegKey);

	for (int nInstObj=0; nInstObj < nMaxInstObj; nInstObj++)
	{
		CString strInstObjKey;
		strInstObjKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegInstObjObjKey, nInstObj);
		SetKeyNames(strInstObjKey);
		SetStringValue(m_strTitle[nInstObj],		strRegInstObjTitleName);
		SetStringValue(m_strLeftMargin[nInstObj],	strRegInstObjLeftMarginName);
		SetStringValue(m_strRightMargin[nInstObj],	strRegInstObjRightMarginName);
		SetStringValue(m_strTopMargin[nInstObj],	strRegInstObjTopMarginName);
		SetStringValue(m_strBottomMargin[nInstObj], strRegInstObjBottomMarginName);
		SetStringValue(m_strFontFaceName[nInstObj], strRegInstObjFontFaceName);
		SetNumberValue(m_nFontSize[nInstObj],		strRegInstObjFontSizeName);
		SetNumberValue(m_nFontWeight[nInstObj],		strRegInstObjFontWeightName);
		SetNumberValue(m_bFontIsStrikeOut[nInstObj],strRegInstObjFontStrikeOutName);
		SetNumberValue(m_bFontIsUnderline[nInstObj],strRegInstObjFontUnderlineName);
		SetNumberValue(m_bFontIsItalic[nInstObj],	strRegInstObjFontItalicName);

		for (int nProperty=0; nProperty<nMaxInstObjProps[nInstObj]; nProperty++)
		{
			CString strInstObjPropKey;
			strInstObjPropKey.Format(_T("%s%s-%d\\%s-%d"), m_strFullRegKey, strRegInstObjObjKey, nInstObj, strRegInstObjPropKey, nProperty);
			SetKeyNames(strInstObjPropKey);
			SetStringValue(m_strPropTitle[nInstObj][nProperty],		strRegInstObjTitleName);
			SetNumberValue(m_nDisplay[nInstObj][nProperty],			strRegInstObjDisplayName);
			SetNumberValue(m_nDisplayWidth[nInstObj][nProperty],	strRegInstObjDisplayWidthName);
			SetNumberValue(m_nDisplayJustify[nInstObj][nProperty],	strRegInstObjDisplayJustifyName);
			SetNumberValue(m_nPrint[nInstObj][nProperty],			strRegInstObjPrintName);
			SetNumberValue(m_nPrintWidth[nInstObj][nProperty],		strRegInstObjPrintWidthName);
			SetNumberValue(m_nPrintJustify[nInstObj][nProperty],	strRegInstObjPrintJustifyName);
		}
	}
	
	Flush();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMapObj::CRegMapObj
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegMapObj::CRegMapObj(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMapObj::~CRegMapObj
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegMapObj::~CRegMapObj(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMapObj::Init
//	Description :	This routine initializes all Map Object 
//					registry values.
//					
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegMapObj::Init()
{
	InitializeFromApplication(strRegMapObjKey);
	
	m_strFullRegKey = GetFullRegistryItem();

	for (int nMapObj=0; nMapObj < nMaxMapObj; nMapObj++)
	{
		CString strMapObjKey;
		strMapObjKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegMapObjObjKey, nMapObj);
		SetKeyNames(strMapObjKey);

		m_strTitle[nMapObj]			= GetStringValue(strRegMapObjTitleName,			TRUE, TRUE, strRegMapObjTitleDefValue[nMapObj]);
		m_strLeftMargin[nMapObj]	= GetStringValue(strRegMapObjLeftMarginName,	TRUE, TRUE, strRegMapObjLeftMarginDefValue[nMapObj]);
		m_strRightMargin[nMapObj]	= GetStringValue(strRegMapObjRightMarginName,	TRUE, TRUE, strRegMapObjRightMarginDefValue[nMapObj]);
		m_strTopMargin[nMapObj]		= GetStringValue(strRegMapObjTopMarginName,		TRUE, TRUE, strRegMapObjTopMarginDefValue[nMapObj]);
		m_strBottomMargin[nMapObj]	= GetStringValue(strRegMapObjBottomMarginName,	TRUE, TRUE, strRegMapObjBottomMarginDefValue[nMapObj]);
		m_strFontFaceName[nMapObj]	= GetStringValue(strRegMapObjFontFaceName,		TRUE, TRUE, strRegMapObjFontFaceValue[nMapObj]);
		m_nFontSize[nMapObj]		= GetNumberValue(strRegMapObjFontSizeName,		TRUE,		nRegMapObjFontSizeValue[nMapObj]);
		m_nFontWeight[nMapObj]		= GetNumberValue(strRegMapObjFontWeightName,	TRUE,		nRegMapObjFontWeightValue[nMapObj]);
		m_bFontIsStrikeOut[nMapObj] = GetNumberValue(strRegMapObjFontStrikeOutName, TRUE,		nRegMapObjFontStrikeOutValue[nMapObj]);
		m_bFontIsUnderline[nMapObj] = GetNumberValue(strRegMapObjFontUnderlineName, TRUE,		nRegMapObjFontUnderlineValue[nMapObj]);
		m_bFontIsItalic[nMapObj]	= GetNumberValue(strRegMapObjFontItalicName,	TRUE,		nRegMapObjFontItalicValue[nMapObj]);

		for (int nProperty=0; nProperty<nMaxMapObjProps[nMapObj]; nProperty++)
		{
			CString strMapObjPropKey;
			strMapObjPropKey.Format(_T("%s%s-%d\\%s-%d"), m_strFullRegKey, strRegMapObjObjKey, nMapObj, strRegMapObjPropKey, nProperty);
			SetKeyNames(strMapObjPropKey);

			m_strPropTitle[nMapObj][nProperty]		= GetStringValue(strRegMapObjTitleName,			TRUE, TRUE, strRegMapObjPropTitleDefValue[nMapObj][nProperty]);
			m_nDisplay[nMapObj][nProperty]			= GetNumberValue(strRegMapObjDisplayName,		TRUE,		nRegMapObjDisplayDefValue[nMapObj][nProperty]);
			m_nDisplayWidth[nMapObj][nProperty]		= GetNumberValue(strRegMapObjDisplayWidthName,	TRUE,		nRegMapObjDisplayWidthDefValue[nMapObj][nProperty]);
			m_nDisplayJustify[nMapObj][nProperty]	= GetNumberValue(strRegMapObjDisplayJustifyName,TRUE,		nRegMapObjDisplayJustifyDefValue[nMapObj][nProperty]);
			m_nPrint[nMapObj][nProperty]			= GetNumberValue(strRegMapObjPrintName,			TRUE,		nRegMapObjPrintDefValue[nMapObj][nProperty]);
			m_nPrintWidth[nMapObj][nProperty]		= GetNumberValue(strRegMapObjPrintWidthName,	TRUE,		nRegMapObjPrintWidthDefValue[nMapObj][nProperty]);
			m_nPrintJustify[nMapObj][nProperty]		= GetNumberValue(strRegMapObjPrintJustifyName,	TRUE,		nRegMapObjPrintJustifyDefValue[nMapObj][nProperty]);
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMapObj::SetValues
//	Description :	This routine sets all Map Object registry 
//					values.
//					
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegMapObj::SetValues()
{
	SetKeyNames(m_strFullRegKey);

	for (int nMapObj=0; nMapObj < nMaxMapObj; nMapObj++)
	{
		CString strMapObjKey;
		strMapObjKey.Format(_T("%s%s-%d"), m_strFullRegKey, strRegMapObjObjKey, nMapObj);
		SetKeyNames(strMapObjKey);

		SetStringValue(m_strTitle[nMapObj],			strRegMapObjTitleName);
		SetStringValue(m_strLeftMargin[nMapObj],	strRegMapObjLeftMarginName);
		SetStringValue(m_strRightMargin[nMapObj],	strRegMapObjRightMarginName);
		SetStringValue(m_strTopMargin[nMapObj],		strRegMapObjTopMarginName);
		SetStringValue(m_strBottomMargin[nMapObj],	strRegMapObjBottomMarginName);
		SetStringValue(m_strFontFaceName[nMapObj],	strRegMapObjFontFaceName);
		SetNumberValue(m_nFontSize[nMapObj],		strRegMapObjFontSizeName);
		SetNumberValue(m_nFontWeight[nMapObj],		strRegMapObjFontWeightName);
		SetNumberValue(m_bFontIsStrikeOut[nMapObj], strRegMapObjFontStrikeOutName);
		SetNumberValue(m_bFontIsUnderline[nMapObj], strRegMapObjFontUnderlineName);
		SetNumberValue(m_bFontIsItalic[nMapObj],	strRegMapObjFontItalicName);

		for (int nProperty=0; nProperty<nMaxMapObjProps[nMapObj]; nProperty++)
		{
			CString strMapObjPropKey;
			strMapObjPropKey.Format(_T("%s%s-%d\\%s-%d"), m_strFullRegKey, strRegMapObjObjKey, nMapObj, strRegMapObjPropKey, nProperty);
			SetKeyNames(strMapObjPropKey);
			SetStringValue(m_strPropTitle[nMapObj][nProperty],		strRegMapObjTitleName);
			SetNumberValue(m_nDisplay[nMapObj][nProperty],			strRegMapObjDisplayName);
			SetNumberValue(m_nDisplayWidth[nMapObj][nProperty],		strRegMapObjDisplayWidthName);
			SetNumberValue(m_nDisplayJustify[nMapObj][nProperty],	strRegMapObjDisplayJustifyName);
			SetNumberValue(m_nPrint[nMapObj][nProperty],			strRegMapObjPrintName);
			SetNumberValue(m_nPrintWidth[nMapObj][nProperty],		strRegMapObjPrintWidthName);
			SetNumberValue(m_nPrintJustify[nMapObj][nProperty],		strRegMapObjPrintJustifyName);
		}
	}
	
	Flush();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw		Date : 11/28/05		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMakeSched::CRegMakeSched
//	Description :	Constructor
//
//	Return :		Constructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/

CRegMakeSched::CRegMakeSched(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw		Date : 11/28/05		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMakeSched::~CRegMakeSched
//	Description :	Destructor
//
//	Return :		Destructor
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRegMakeSched::~CRegMakeSched(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMakeSched::Init
//	Description :	This routine initializes all OATS registry values.
//					
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegMakeSched::Init()
{
	InitializeFromApplication(strRegMakeSchedKey);
	m_strFullRegKey	= GetFullRegistryItem();
	
	m_nCLSYear           = GetNumberValue(strRegMakeSchedCLSYearName,   TRUE, nRegMakeSchedCLSYearDefValue);
	m_nFrmGen            = GetNumberValue(strRegMakeSchedFrmGenName	,   TRUE, nRegMakeSchedFrmGenDefValue);
	m_nFrmUpdate         = GetNumberValue(strRegMakeSchedFrmUpdateName,	TRUE, nRegMakeSchedFrmUpdateDefValue);
	m_nFrmSRSO           = GetNumberValue(strRegMakeSchedFrmSRSOName,	TRUE, nRegMakeSchedFrmSRSODefValue);
	m_nFrmRSO			 = GetNumberValue(strRegMakeSchedFrmRSOName,    TRUE, nRegMakeSchedFrmRSODefValue);
	m_nNewFrmTbl         = GetNumberValue(strRegMakeSchedNewFrmTblName,	TRUE, nRegMakeSchedNewFrmTblDefValue);
	m_strSecTblFileName  = GetStringValue(strRegMakeSchedSecTblFileNameName,	TRUE, TRUE, strRegMakeSchedSecTblFileNameDefValue);
	m_strFrmTblFileName  = GetStringValue(strRegMakeSchedFrmTblFileNameName,	TRUE, TRUE, strRegMakeSchedFrmTblFileNameDefValue);
	m_nRTCSGen           = GetNumberValue(strRegMakeSchedRTCSGenName,	TRUE, nRegMakeSchedRTCSGenDefValue);
	m_strRTCSMapFileName = GetStringValue(strRegMakeSchedRTCSMapFileNameName, TRUE, TRUE, strRegMakeSchedRTCSMapFileNameDefValue);
	m_strRTCSSetFileName = GetStringValue(strRegMakeSchedRTCSSetFileNameName,	TRUE, TRUE, strRegMakeSchedRTCSSetFileNameDefValue);
	m_nOBSchedGen        = GetNumberValue(strRegMakeSchedOBSchedGenName,	TRUE, nRegMakeSchedOBSchedGenDefValue);
    m_nValSched          = GetNumberValue(strRegMakeSchedValSchedName,	    TRUE, nRegMakeSchedValSchedDefValue);
	m_nRTCSVal           = GetNumberValue(strRegMakeSchedRTCSValName,		TRUE, nRegMakeSchedRTCSValDefValue);
	m_nValSchedType      = GetNumberValue(strRegMakeSchedValSchedTypeName,	TRUE, nRegMakeSchedValSchedTypeDefValue);
	m_nStarUpdate        = GetNumberValue(strRegMakeSchedStarUpdateName,	TRUE, nRegMakeSchedStarUpdateDefValue);
	m_nStarGen           = GetNumberValue(strRegMakeSchedStarGenName,	    TRUE, nRegMakeSchedStarGenDefValue);
	m_nRTCSUpdate	     = GetNumberValue(strRegMakeSchedRTCSUpdateName,	TRUE, nRegMakeSchedRTCSUpdateDefValue);
	m_nUsePreDef		 = GetNumberValue(strRegMakeSchedUsePreDefName,	    TRUE, nRegMakeSchedUsePreDefDefValue);
}


/**
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw		Date : 11/28/05		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRegMakeSched::SetValues
//	Description :	This routine sets all MakeSched registry values.
//					
//	Return :
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CRegMakeSched::SetValues()
{
	SetKeyNames(m_strFullRegKey);
	SetNumberValue(m_nCLSYear, strRegMakeSchedCLSYearName);
	SetNumberValue(m_nFrmGen, strRegMakeSchedFrmGenName);
	SetNumberValue(m_nFrmUpdate, strRegMakeSchedFrmUpdateName);
	SetNumberValue(m_nFrmSRSO, strRegMakeSchedFrmSRSOName);
	SetNumberValue(m_nFrmRSO, strRegMakeSchedFrmRSOName);
	SetNumberValue(m_nNewFrmTbl, strRegMakeSchedNewFrmTblName);
	SetStringValue(m_strFrmTblFileName, strRegMakeSchedFrmTblFileNameName);
	SetNumberValue(m_nRTCSGen, strRegMakeSchedRTCSGenName);
	SetNumberValue(m_nRTCSUpdate, strRegMakeSchedRTCSUpdateName	);
	SetNumberValue(m_nOBSchedGen, strRegMakeSchedOBSchedGenName);
	SetNumberValue(m_nValSched, strRegMakeSchedValSchedName);
	SetNumberValue(m_nRTCSVal, strRegMakeSchedRTCSValName);
	SetNumberValue(m_nValSchedType, strRegMakeSchedValSchedTypeName);
	SetNumberValue(m_nStarUpdate, strRegMakeSchedStarUpdateName);
	SetNumberValue(m_nStarGen, strRegMakeSchedStarGenName);
	SetStringValue(m_strSecTblFileName, strRegMakeSchedSecTblFileNameName);
	SetStringValue(m_strRTCSMapFileName, strRegMakeSchedRTCSMapFileNameName);
	SetStringValue(m_strRTCSSetFileName, strRegMakeSchedRTCSSetFileNameName);
    SetNumberValue(m_nUsePreDef, strRegMakeSchedUsePreDefName);
	Flush();
}
