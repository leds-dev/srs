// RegistryUsr.h: interface for the RegistryUsr class.
//
// Revision History														   
// Manan Dalal - OSPO - August 2014										   
// PR000951 - Enhance SchedMgrECP with RSO Sectors						   
// Added few globals for user registry used to determine if user has	   
// selected to do RSO sector update										   
// Released in Build16.2												   
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_REGISTRYUSR_H__FCB3AF3A_D5CA_11D4_800D_00609704053C__INCLUDED_)
#define AFX_REGISTRYUSR_H__FCB3AF3A_D5CA_11D4_800D_00609704053C__INCLUDED_

#include "OXRegistryItem.h"
#include "Constants.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Registry constants
//
// Registory constants are made up of three parts.  The first part is the
// registory key.  This is followed by the registory name.  Finally, the
// registory names are assigned values.
//
// General Keys
//
// const CString strRegCompanyKey = _T("ISI");

class CRegUsrEx : public COXRegistryItem
{
public:
	CRegUsrEx();
	virtual ~CRegUsrEx();
	BOOL DeleteKey();
	CString m_strFullRegKey;
};

const CString strRegSiteKey = _T("Site");
const CString strRegSiteDefaultGTACSName = _T("Default GTACS");
const int  nRecSiteDefaultGTACSDefValue = 0;
const CString strRegSiteDefaultOATSName = _T("Default OATS");
const int  nRecSiteDefaultOATSDefValue = 0;
const CString strRegSiteDefaultSCName = _T("Default S/C");
const int  nRecSiteDefaultSCDefValue = 0;
const CString strRegSiteTitleName = _T("Title");
const CString strRegSiteTitleDefValue[nMaxSites] =
	{_T("SOCC"),_T("Wallops CDA"),_T("Goddard"),_T("SEC"),_T("Fairbanks CDA")};
const CString strRegSiteAbbrevName = _T("Abbreviation");
const CString strRegSiteAbbrevDefValue[nMaxSites] =
	{_T("S"),_T("C"),_T("G"),_T("E"),_T("F")};

class CRegSite : public CRegUsrEx
{
public:
	CRegSite();
	virtual ~CRegSite();
	void Init();
	void SetValues();
	CString	m_strTitle[nMaxSites];
	CString	m_strAbbrev[nMaxSites];
	int		m_nDefaultGTACSPerSite[nMaxSites];
	int		m_nDefaultOATSPerSite[nMaxSites];
	int		m_nDefaultGTACS;
	int		m_nDefaultOATS;
	int     m_nDefaultSC;
};

const CString strRegMiscKey 														= _T("Misc");
const CString strRegMiscDayName 													= _T("Day of Year");
const int	  nRecMiscDayDefValue 													= 1;
const CString strRegMiscFontFaceName 												= _T("Font Face");
const CString strRegMiscFontFaceValue 												= _T("Courier New");
const CString strRegMiscFontSizeName 												= _T("Font Size");
const int	  nRegMiscFontSizeValue 												= 12;
const CString strRegMiscFontWeightName 												= _T("Font Weight");
const int	  nRegMiscFontWeightValue 												= FW_NORMAL;
const CString strRegMiscFontStrikeOutName 											= _T("Font Strike Out");
const int	  nRegMiscFontStrikeOutValue 											= 0;
const CString strRegMiscFontUnderlineName 											= _T("Font Underline");
const int	  nRegMiscFontUnderlineValue 											= 0;
const CString strRegMiscFontItalicName 												= _T("Font Italic");
const int	  nRegMiscFontItalicValue 												= 0;
const CString strRegMiscLeftMarginName 												= _T("Left Margin");
const CString strRegMiscLeftMarginDefValue 											= _T("0.5");
const CString strRegMiscRightMarginName 											= _T("Right Margin");
const CString strRegMiscRightMarginDefValue 										= _T("0.5");
const CString strRegMiscTopMarginName 												= _T("Top Margin");
const CString strRegMiscTopMarginDefValue 											= _T("0.5");
const CString strRegMiscBottomMarginName 											= _T("Bottom Margin");
const CString strRegMiscBottomMarginDefValue 										= _T("0.5");
const CString strRegMiscShowExtName 												= _T("Show Extension");
const int nRegMiscShowExtValue 														= FALSE;
const CString strRegMiscSortByExtName 												= _T("Sort By Extension");
const int nRegMiscSortByExtValue 													= FALSE;
const CString strRegMiscMuteSoundName 												= _T("Mute Sound");
const int nRegMiscMuteSoundValue 													= TRUE;
const CString strRegMiscDBDriveName 												= _T("Internal DB Drive");
const CString strRegMiscDBDriveValue 												= _T("R:");
const CString strRegMiscCmdExeName 													= _T("Command Exe Time");
const CString strRegMiscCmdExeValue 												= _T("0.010");

class CRegMisc : public CRegUsrEx
{
public:
	CRegMisc();
	virtual ~CRegMisc();
	void Init();
	void SetValues();
	int		m_nDOY;
	CString	m_strLeftMargin;
	CString	m_strRightMargin;
	CString	m_strTopMargin;
	CString	m_strBottomMargin;
	CString	m_strFontFaceName;
	int		m_nFontSize;
	int		m_nFontWeight;
	BOOL	m_bFontIsStrikeOut;
	BOOL	m_bFontIsUnderline;
	BOOL	m_bFontIsItalic;
	BOOL	m_bShowExt;
	BOOL	m_bSortByExt;
	BOOL	m_bMuteSound;
	CString	m_strDBDrive;
	CString	m_strCmdExe;
};

const CString strRegInstObjKey 														= _T("Instrument Object");
const CString strRegInstObjObjKey 													= _T("Object");
const CString strRegInstObjTitleName 												= _T("Title");
const CString strRegInstObjTitleDefValue[nMaxInstObj] 								={_T("Sector"), _T("Frame"), _T("Star")};
const CString strRegInstObjFontFaceName 											= _T("Font Face");
const CString strRegInstObjFontFaceValue[nMaxInstObj] 								={_T("Courier New"),_T("Courier New"),_T("Courier New")};
const CString strRegInstObjFontSizeName 											= _T("Font Size");
const int	  nRegInstObjFontSizeValue[nMaxInstObj] 								= {12,12,12};
const CString strRegInstObjFontWeightName 											= _T("Font Weight");
const int	  nRegInstObjFontWeightValue[nMaxInstObj] 								= {FW_NORMAL,FW_NORMAL,FW_NORMAL};
const CString strRegInstObjFontStrikeOutName 										= _T("Font Strike Out");
const int	  nRegInstObjFontStrikeOutValue[nMaxInstObj]							= {0,0,0};
const CString strRegInstObjFontUnderlineName 										= _T("Font Underline");
const int	  nRegInstObjFontUnderlineValue[nMaxInstObj]							= {0,0,0};
const CString strRegInstObjFontItalicName 											= _T("Font Italic");
const int	  nRegInstObjFontItalicValue[nMaxInstObj] 								= {0,0,0};
const CString strRegInstObjLeftMarginName 											= _T("Left Margin");
const CString strRegInstObjLeftMarginDefValue[nMaxInstObj] 							= {_T("0.5"),_T("0.5"),_T("0.5")};
const CString strRegInstObjRightMarginName 											= _T("Right Margin");
const CString strRegInstObjRightMarginDefValue[nMaxInstObj] 						= {_T("0.5"),_T("0.5"),_T("0.5")};
const CString strRegInstObjTopMarginName 											= _T("Top Margin");
const CString strRegInstObjTopMarginDefValue[nMaxInstObj] 							= {_T("0.5"),_T("0.5"),_T("0.5")};
const CString strRegInstObjBottomMarginName 										= _T("Bottom Margin");
const CString strRegInstObjBottomMarginDefValue[nMaxInstObj]						= {_T("0.5"),_T("0.5"),_T("0.5")};
const CString strRegInstObjPropKey 													= _T("Property");
const CString strRegInstObjPropTitleDefValue[nMaxInstObj][nMaxInstObjPropsPerObj] 	=
		{_T("Label"),_T("Inst"),_T("SL Mode"),_T("SL Side"),_T("Sndr Step"),_T("Coordinate"),_T("Start/Center NS"),_T("Start/Center EW"),_T("Stop NS/Aspect"),_T("Stop EW/Dur"),_T("Description"),	//Sector
		 _T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),
		 _T("Index"),_T("Time"),_T("Label"),_T("Inst"),_T("SL Mode"),_T("SL Side"),_T("Sndr Step"),_T("Coordinate"),_T("Start/Center NS"),_T("Start/Center EW"),_T("Stop NS/Aspect"),				//Frame
		 _T("Stop EW/Dur"),_T("Start Cyc NS"),_T("Start Inc NS"),_T("Start Cyc EW"),_T("Start Inc EW"),_T("Stop Cyc NS"),_T("Stop Inc NS"),_T("Stop Cyc EW"),_T("Stop Inc EW"),_T("Duration"),_T("Description"),
		 _T("Time"),_T("Duration"),_T("Window Num"),_T("Instrument"),_T("IMC Set"),_T("Index"),_T("Star ID"),_T("Num Looks"),_T("Look Num"),_T("Repeats"),_T("Cyc NS"),_T("Inc NS"),				//Star
		 _T("Cyc EW"),_T("Inc EW"),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T(""),_T("")};
const CString strRegInstObjDisplayName 												= _T("Display");
const int nRegInstObjDisplayDefValue[nMaxInstObj][nMaxInstObjPropsPerObj] 			=
		{1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,
		 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		 1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0};
const CString strRegInstObjDisplayWidthName 										= _T("Display Width");
const int nRegInstObjDisplayWidthDefValue[nMaxInstObj][nMaxInstObjPropsPerObj]		=
		{10,10,10,10,10,10,10,10,10,10,10,0,0,0,0,0,0,0,0,0,0,0,
		 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,
		 10,10,10,10,10,10,10,10,10,10,10,10,10,10,0,0,0,0,0,0,0,0};
const CString strRegInstObjDisplayJustifyName 										= _T("Display Justify");
const int nRegInstObjDisplayJustifyDefValue[nMaxInstObj][nMaxInstObjPropsPerObj] 	=
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
const CString strRegInstObjPrintName 												= _T("Print");
const int nRegInstObjPrintDefValue[nMaxInstObj][nMaxInstObjPropsPerObj] 			=
		{1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,
		 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
		 1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0};
const CString strRegInstObjPrintWidthName 											= _T("Print Width");
const int nRegInstObjPrintWidthDefValue[nMaxInstObj][nMaxInstObjPropsPerObj] 		=
		{10,10,10,10,10,10,10,10,10,10,10,0,0,0,0,0,0,0,0,0,0,0,
		 10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,
		 10,10,10,10,10,10,10,10,10,10,10,10,10,10,0,0,0,0,0,0,0,0};
const CString strRegInstObjPrintJustifyName 										= _T("Print Justify");
const int nRegInstObjPrintJustifyDefValue[nMaxInstObj][nMaxInstObjPropsPerObj] 		=
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

class CRegInstObj : public CRegUsrEx
{
public:
	CRegInstObj();
	virtual ~CRegInstObj();
	virtual void Init();
	virtual void SetValues();
	CString			m_strTitle[nMaxInstObj];
	CString			m_strLeftMargin[nMaxInstObj];
	CString			m_strRightMargin[nMaxInstObj];
	CString			m_strTopMargin[nMaxInstObj];
	CString			m_strBottomMargin[nMaxInstObj];
	CString			m_strFontFaceName[nMaxInstObj];
	int				m_nFontSize[nMaxInstObj];
	int				m_nFontWeight[nMaxInstObj];
	BOOL			m_bFontIsStrikeOut[nMaxInstObj];
	BOOL			m_bFontIsUnderline[nMaxInstObj];
	BOOL			m_bFontIsItalic[nMaxInstObj];
	CString			m_strPropTitle[nMaxInstObj][nMaxInstObjPropsPerObj];
	int				m_nDisplay[nMaxInstObj][nMaxInstObjPropsPerObj];
	int				m_nDisplayWidth[nMaxInstObj][nMaxInstObjPropsPerObj];
	int				m_nDisplayJustify[nMaxInstObj][nMaxInstObjPropsPerObj];
	int				m_nPrint[nMaxInstObj][nMaxInstObjPropsPerObj];
	int				m_nPrintWidth[nMaxInstObj][nMaxInstObjPropsPerObj];
	int				m_nPrintJustify[nMaxInstObj][nMaxInstObjPropsPerObj];
};

const CString strRegMapObjKey 														= _T("Map Object");
const CString strRegMapObjObjKey 													= _T("Map");
const CString strRegMapObjTitleName 												= _T("Title");
const CString strRegMapObjTitleDefValue[nMaxMapObj] 								= {_T("STOL"), _T("RTCS")};
const CString strRegMapObjFontFaceName 												= _T("Font Face");
const CString strRegMapObjFontFaceValue[nMaxMapObj] 								= {_T("Courier New"),_T("Courier New")};
const CString strRegMapObjFontSizeName 												= _T("Font Size");
const int	  nRegMapObjFontSizeValue[nMaxMapObj] 									= {12,12};
const CString strRegMapObjFontWeightName 											= _T("Font Weight");
const int	  nRegMapObjFontWeightValue[nMaxMapObj] 								= {FW_NORMAL,FW_NORMAL};
const CString strRegMapObjFontStrikeOutName 										= _T("Font Strike Out");
const int	  nRegMapObjFontStrikeOutValue[nMaxMapObj] 								= {0,0};
const CString strRegMapObjFontUnderlineName 										= _T("Font Underline");
const int	  nRegMapObjFontUnderlineValue[nMaxMapObj] 								= {0,0};
const CString strRegMapObjFontItalicName 											= _T("Font Italic");
const int	  nRegMapObjFontItalicValue[nMaxMapObj] 								= {0,0};
const CString strRegMapObjLeftMarginName 											= _T("Left Margin");
const CString strRegMapObjLeftMarginDefValue[nMaxMapObj] 							= {_T("0.5"),_T("0.5")};
const CString strRegMapObjRightMarginName 											= _T("Right Margin");
const CString strRegMapObjRightMarginDefValue[nMaxMapObj] 							= {_T("0.5"),_T("0.5")};
const CString strRegMapObjTopMarginName 											= _T("Top Margin");
const CString strRegMapObjTopMarginDefValue[nMaxMapObj] 							= {_T("0.5"),_T("0.5")};
const CString strRegMapObjBottomMarginName 											= _T("Bottom Margin");
const CString strRegMapObjBottomMarginDefValue[nMaxMapObj] 							= {_T("0.5"),_T("0.5")};
const CString strRegMapObjPropKey 													= _T("Property");

// Map view specific string constants
const CString strRegMapObjPropTitleDefValue[nMaxMapObj][nMaxMapObjPropsPerObj] 		=
		{_T("Time"),_T("Schedule Line"),_T("Command"),_T("OB Start Index"),_T("OB End Index"),
		 _T("Lis Line"),_T("Prev Lis Line"),_T("Next Lis Line"),_T("Cmd Exec Duration"),_T(""),_T(""),
		 _T("RTCS Number"),_T("Label"),_T("Lis Line"),_T("Next Lis Line"),_T("Ram Address"),
		 _T("OB Start Index"),_T("OB Stop Index"),_T("Command"),_T("Command Hex"),
		 _T("Rel Time Between Cmds (Secs)"),_T("RTCS Exec Duration (hh:mm:ss)")};
const CString strRegMapObjDisplayName 												= _T("Display");
const int nRegMapObjDisplayDefValue[nMaxMapObj][nMaxMapObjPropsPerObj] 				=
		{1,1,1,1,1,1,1,1,1,1,1,
		 1,1,1,1,1,1,1,1,1,1,1};
const CString strRegMapObjDisplayWidthName 											= _T("Display Width");
const int nRegMapObjDisplayWidthDefValue[nMaxMapObj][nMaxMapObjPropsPerObj] 		=
		{10,10,10,10,10,10,10,10,10,10,10,
		 10,10,10,10,10,10,10,10,10,10,10};
const CString strRegMapObjDisplayJustifyName 										= _T("Display Justify");
const int nRegMapObjDisplayJustifyDefValue[nMaxMapObj][nMaxMapObjPropsPerObj] 		=
		{0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0};
const CString strRegMapObjPrintName 												= _T("Print");
const int nRegMapObjPrintDefValue[nMaxMapObj][nMaxMapObjPropsPerObj] 				=
		{1,1,1,1,1,1,1,1,1,1,1,
		 1,1,1,1,1,1,1,1,1,1,1};
const CString strRegMapObjPrintWidthName 											= _T("Print Width");
const int nRegMapObjPrintWidthDefValue[nMaxMapObj][nMaxMapObjPropsPerObj] 			=
		{10,10,10,10,10,10,10,10,10,10,10,
		 10,10,10,10,10,10,10,10,10,10,10};
const CString strRegMapObjPrintJustifyName 											= _T("Print Justify");
const int nRegMapObjPrintJustifyDefValue[nMaxMapObj][nMaxMapObjPropsPerObj] 		=
		{0,0,0,0,0,0,0,0,0,0,0,
		 0,0,0,0,0,0,0,0,0,0,0};

class CRegMapObj : public CRegUsrEx
{
public:
	CRegMapObj();
	virtual ~CRegMapObj();
	virtual void Init();
	virtual void SetValues();
	CString			m_strTitle[nMaxMapObj];
	CString			m_strLeftMargin[nMaxMapObj];
	CString			m_strRightMargin[nMaxMapObj];
	CString			m_strTopMargin[nMaxMapObj];
	CString			m_strBottomMargin[nMaxMapObj];
	CString			m_strFontFaceName[nMaxMapObj];
	int				m_nFontSize[nMaxMapObj];
	int				m_nFontWeight[nMaxMapObj];
	BOOL			m_bFontIsStrikeOut[nMaxMapObj];
	BOOL			m_bFontIsUnderline[nMaxMapObj];
	BOOL			m_bFontIsItalic[nMaxMapObj];
	CString			m_strPropTitle[nMaxMapObj][nMaxMapObjPropsPerObj];
	int				m_nDisplay[nMaxMapObj][nMaxMapObjPropsPerObj];
	int				m_nDisplayWidth[nMaxMapObj][nMaxMapObjPropsPerObj];
	int				m_nDisplayJustify[nMaxMapObj][nMaxMapObjPropsPerObj];
	int				m_nPrint[nMaxMapObj][nMaxMapObjPropsPerObj];
	int				m_nPrintWidth[nMaxMapObj][nMaxMapObjPropsPerObj];
	int				m_nPrintJustify[nMaxMapObj][nMaxMapObjPropsPerObj];
};

	const CString strRegMakeSchedKey							= _T("MakeSched");
	const CString strRegMakeSchedCLSYearName					= _T("CLS Year");
	const int     nRegMakeSchedCLSYearDefValue					= 2007;
	const CString strRegMakeSchedFrmGenName						= _T("Gen Frame Table");
	const int	  nRegMakeSchedFrmGenDefValue					= 1;
	const CString strRegMakeSchedFrmUpdateName					= _T("Frame Update");
	const int	  nRegMakeSchedFrmUpdateDefValue				= 1;
	const CString strRegMakeSchedFrmSRSOName					= _T("SRSO Frames");
	const int	  nRegMakeSchedFrmSRSODefValue					= 0;
	const CString strRegMakeSchedFrmRSOName						= _T("RSO Frames");
	const int	  nRegMakeSchedFrmRSODefValue					= 0;
	const CString strRegMakeSchedNewFrmTblName					= _T("New Frame Table");
	const int	  nRegMakeSchedNewFrmTblDefValue				= 0;
	const CString strRegMakeSchedSecTblFileNameName				= _T("Sector Table Name");
	const CString strRegMakeSchedSecTblFileNameDefValue			= _T("Default");
	const CString strRegMakeSchedFrmTblFileNameName				= _T("Frame Table Name");
	const CString strRegMakeSchedFrmTblFileNameDefValue			= _T("Default");
	const CString strRegMakeSchedRTCSGenName					= _T("RTCS Gen");
	const int	  nRegMakeSchedRTCSGenDefValue					= 1;
	const CString strRegMakeSchedRTCSUpdateName					= _T("RTCS Update");
	const int	  nRegMakeSchedRTCSUpdateDefValue				= 1;
	const CString strRegMakeSchedRTCSMapFileNameName			= _T("RTCS Map File Name");
	const CString strRegMakeSchedRTCSMapFileNameDefValue		= _T("Default");
	const CString strRegMakeSchedRTCSSetFileNameName			= _T("RTCS Set File Name");
	const CString strRegMakeSchedRTCSSetFileNameDefValue		= _T("Default");
	const CString strRegMakeSchedOBSchedGenName					= _T("OB Sched Gen");
	const int	  nRegMakeSchedOBSchedGenDefValue				= 1;
	const CString strRegMakeSchedValSchedName					= _T("Sched Val");
	const int	  nRegMakeSchedValSchedDefValue					= 1;
	const CString strRegMakeSchedRTCSValName					= _T("RTCS Val");
	const int	  nRegMakeSchedRTCSValDefValue					= 1;
	const CString strRegMakeSchedValSchedTypeName				= _T("Sched Val Type");
	const int	  nRegMakeSchedValSchedTypeDefValue				= 2;
	const CString strRegMakeSchedStarUpdateName					= _T("Star Update");
	const int	  nRegMakeSchedStarUpdateDefValue				= 1;
	const CString strRegMakeSchedStarGenName					= _T("Star Gen");
	const int	  nRegMakeSchedStarGenDefValue					= 1;
	const CString strRegMakeSchedUsePreDefName					= _T("Use Predefined Frames");
	const int	  nRegMakeSchedUsePreDefDefValue				= 0;

class CRegMakeSched : public CRegUsrEx
{
public:
	CRegMakeSched();
	virtual ~CRegMakeSched();
	virtual void Init();
	virtual void SetValues();

	int     m_nCLSYear;
	int     m_nFrmGen;
	int     m_nFrmUpdate;
	int     m_nFrmSRSO;
	int		m_nFrmRSO;
	int     m_nNewFrmTbl;
	CString m_strSecTblFileName;
	CString m_strFrmTblFileName;
	int     m_nRTCSGen;
	int		m_nRTCSUpdate;
	CString m_strRTCSMapFileName;
	CString m_strRTCSSetFileName;
	int		m_nOBSchedGen;
    int		m_nValSched;
	int		m_nRTCSVal;
	int		m_nValSchedType;
	int		m_nStarUpdate;
	int		m_nStarGen;
	int		m_nUsePreDef;
};



#endif // !defined(AFX_REGISTRYUSR_H__FCB3AF3A_D5CA_11D4_800D_00609704053C__INCLUDED_)
