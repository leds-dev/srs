// Parse.cpp: implementation of the CParse class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "SchedMisc.h"
#include "Shlwapi.h"  //Need to link with Shlwapi.lib

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CSchedMisc::CSchedMisc(){}
CSchedMisc::~CSchedMisc(){}

CString CSchedMisc::ConvertSTtoString (SYSTEMTIME st)
{
	CString	strTime;
	TCHAR szLocalDate[255], szLocalTime[255];
	CString strLocatDate;
	GetDateFormat(LOCALE_USER_DEFAULT, DATE_LONGDATE, &st, NULL, szLocalDate, 255);
	GetTimeFormat(LOCALE_USER_DEFAULT, 0, &st, NULL, szLocalTime, 255);
	strTime.Format(_T("%s %s"), szLocalDate, szLocalTime);
	return strTime;
}

CString CSchedMisc::ConvertFTtoString (FILETIME ft)
{
	SYSTEMTIME st;
	FileTimeToLocalFileTime(&ft, &ft);
	FileTimeToSystemTime(&ft, &st);
	return ConvertSTtoString (st);
}
