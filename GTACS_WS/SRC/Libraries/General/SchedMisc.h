// Parse.h: interface for the CParse class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCHEDMISC_H__2ECE95B3_F15B_11D4_8016_00609704053C__INCLUDED_)
#define AFX_SCHEDMISC_H__2ECE95B3_F15B_11D4_8016_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSchedMisc  
{
public:
	CSchedMisc();
	virtual ~CSchedMisc();
	static CString ConvertSTtoString (SYSTEMTIME st);
	static CString ConvertFTtoString (FILETIME ft);
};

#endif // !defined(AFX_SCHEDMISC_H__2ECE95B3_F15B_11D4_8016_00609704053C__INCLUDED_)
