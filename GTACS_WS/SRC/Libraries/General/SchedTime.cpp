// SchedTime.cpp: implementation of the CSchedTime class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SchedTime.h"
#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/*======================================================================*\
	Class CSchedTime
  ======================================================================
	Function :	This time class is used for storing absolute time
				periods.  It includes a CTime member object that holds the
				general time period plus a millisecond member to hold
				the fractional part.
\*======================================================================*/

/*======================================================================*\
	Author :Don Sanders			Date : 12/15/00			version 1.0
  ======================================================================
	Function :		CSchedTime::CSchedTime
	Description :	Default constructor.  Set the time to 0.
	Return :		
		constructor		-	Default constructor.
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CSchedTime::CSchedTime()
{
	m_cTime = CTime(1971, 1, 1, 0, 0, 0, 0);
	m_nMilliSec = 0;
	m_strErrorText = _T("");
}

/*======================================================================*\
	Author :Don Sanders		Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::CSchedTime
	Description :	Constructs a object based upon the parameters passed
					in.
	Return :		
		constructor			-	Constructor.
	Parameters :	
		CTime cTime		-	A CTime object used to base the new object on.
		int nMilliSec	-	The millisecond portion of the object.
	Note :			The nMilliSec parameter must be in the range of 0 to
					999.  The debug version causes an assertion.  It is
					programmers responsibility to range check this value
\*======================================================================*/
CSchedTime::CSchedTime(CTime cTime, int nMilliSec)
{
	ASSERT ((nMilliSec >= 0) && (nMilliSec <= 999));
	m_cTime = cTime;
	m_nMilliSec = nMilliSec;
	m_strErrorText = _T("");
}

/*======================================================================*\
	Author :Don Sanders		Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::CSchedTime
	Description :	Constructs a object based upon the parameters passed
					in.
	Return :		
		constructor		-	Constructor.
	Parameters :	
		int nYear		-	Number of years in the range of 1971 to 2036.
		int nGMTDay		-	GMT Day of year in the range of 1 to 365/366.
		int nHour		-	Number of hours in the range of 0 to 23.
		int nMin		-	Number of minutes in the range of 0 to 59.
		int nSec		-	Number of seconds in the range of 0 to 59.
		int nMilliSec	-	Number of milliseconds in the range of 0 to 999.
	Note :			The input parameters cause debug assertions if they
					are out of range.  It is the programmer's
					responsibility to range check the parameters.
\*======================================================================*/
CSchedTime::CSchedTime(int nYear, int nGMTDay, int nHour, int nMin, int nSec, int nMilliSec)
{
	ASSERT ((nMilliSec >= 0) && (nMilliSec <= 999));
	int nDay;
	int nMonth;
	BOOL bValid = GMTDayToMonthDay(nGMTDay, nYear, &nMonth, &nDay);
	ASSERT (bValid);
	m_cTime = CTime(nYear, nMonth, nDay, nHour, nMin, nSec, 0);
	m_nMilliSec = nMilliSec;
	m_strErrorText = _T("");
}

/*======================================================================*\
	Author :Frederick J. Shaw	Date : 04-08-01		version 1.0
  ======================================================================
	Function :		CSchedTime::CSchedTime
	Description :	Constructs a object based upon the parameters passed
					in.
	Return :		
		constructor		-	Constructor.
	Parameters :	
		CString :  time string
	Note :			The input parameters cause debug assertions if they
					are out of range.  It is the programmer's
					responsibility to range check the parameters.
\*======================================================================*/
CSchedTime::CSchedTime(CString strText){
//	if (IsValid(strText)){
	if (Parser(strText)){
		Create(strText);
	} else {
		m_cTime = CTime(1971, 1, 1, 0, 0, 0, 0);
		m_nMilliSec = 0;
		m_strErrorText = _T("");
	}
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::~CSchedTime
	Description :	Default destructor
	Return :		
		destructor		-	Descturctor.
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CSchedTime::~CSchedTime(){}

/*======================================================================*\
	Author :Don Sanders			Date : 12/15/00			version 1.0
  ======================================================================
	Function :		CSchedTime::GMTDayToMonthDay
	Description :	Converts a GMT Day of Year to a Month/Day Pair.
	Return :		
		BOOL		-	TRUE if the conversion took place.
						FALSE if there was an error.  The reason for the
						error is written in the ErrorText parameter.
	Parameters :	
		int nGMTDay				-	GMT Day of Year 0 - 365/366
		int nYear				-	Year - 1971 - 2036
		int* pnMonth			-	Converted month 1 (Jan) - 12 (Dec)
		int* pnDay				-	Convert day of monthe 1 - 28/29/30/31
		CString* pstrErrorText	-	Text associated with conversion error.
	Note :			GMT days start at midnight with Jan 1 being day 1.
					This is a static function.
\*======================================================================*/
BOOL CSchedTime::GMTDayToMonthDay(int nGMTDay, int nYear, int* pnMonth, int* pnDay,
								  CString* pstrErrorText)
{
	BOOL bRtnStatus = TRUE;
	BOOL bLeapYear = FALSE;
	CString strErrorText = _T("");

	// Make sure that the year is within range
	// Determine if this is a leap year
	if (bRtnStatus)
	{
		if ((nYear < 1971) || (nYear > 2036))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format(_T("Invalid year <%d>"), nYear);
			strErrorText.Format(_T("Invalid year <%d> specified, must be between 1971 and 2036."), nYear);
		}
		else
			bLeapYear = ((nYear % 4) == 0); // Good enough for the valid range
	}

	// Make sure that the day is within range
	if (bRtnStatus)
	{
		if ((nGMTDay < 0) || (nGMTDay > (bLeapYear?366:365)))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format(_T("Invalid julian day <%d>"), nGMTDay);
			strErrorText.Format(_T("Invalid julian day <%d> specified, must be between 0 and 366."), nGMTDay);
		}
	}
	
	// Calculate the month and day
	if (bRtnStatus)
	{
		int nDaysInMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		if (bLeapYear)
			nDaysInMonth[1]++; //Increment Feb by one if necessary

		int nMonth = 0;
		while (nGMTDay > nDaysInMonth[nMonth])
		{
			nGMTDay -= nDaysInMonth[nMonth++];
		}

		*pnMonth = nMonth+1;
		*pnDay = nGMTDay;
	}
	
	if (pstrErrorText)
		*pstrErrorText = strErrorText;

	return bRtnStatus;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::Create
	Description :	Create a CSchedTime object from the specified
					parameters.
	Return :
		BOOL	-	TRUE if the parameters passed in were valid.
					FALSE if the parameters passed in were invalid or out
					of range.  Call GetLastErrorText() to retrieve the
					reason.
	Parameters :
		int nYear		-	Year value in the range of 1971-2036
		int nGMTDay		-	Day value in the range of 1-365/366
		int nHour		-	Hour value in the range of 0-23
		int nMin		-	Minute value in the range of 0-59
		int nSec		-	Second value in the range of 0-59
		int nMilliSec	-	Milli-Sec value in the range of 0-999
	Note :			This routine ensures that all parameters are within
					range by calling the static function IsValid.  If
					the parameters are valid, the object is initialized
					to the parameters and TRUE is returned, otherwise
					FALSE is returned and GetLastErrorText() should be
					called to determine the reason.
\*======================================================================*/
BOOL CSchedTime::Create(int nYear, int nGMTDay, int nHour, int nMin, int nSec, int nMilliSec)
{
	CSchedTime	cSchedTime;
	BOOL bRtnStatus = IsValid(nYear, nGMTDay, nHour, nMin, nSec, nMilliSec, &cSchedTime, &m_strErrorText);

	if (bRtnStatus)
		*this = cSchedTime;

	return bRtnStatus;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::Create
	Description :	Create a CSchedTime object from the specified
					parameter.
	Return :
		BOOL	-	TRUE if the parameters passed in were valid.
					FALSE if the parameters passed in were invalid or out
					of range.  Call GetLastErrorText() to retrieve the
					reason.
	Parameters :
		CString strText	-	The text to create the object from.
	Note :			This routine ensures that the input text is valid
					by calling the static function IsValid.  If the text
					is valid, the object is initialized with the values
					contained with the string and TRUE is returned,
					otherwise FALSE is returned and GetLastErrorText()
					should be called to determine the reason.
\*======================================================================*/
BOOL CSchedTime::Create(CString strText)
{
	CSchedTime	cSchedTime;
//	BOOL bRtnStatus = IsValid(strText, &cSchedTime, &m_strErrorText);
	BOOL bRtnStatus = Parser(strText, &cSchedTime, &m_strErrorText);


	if (bRtnStatus)
		*this = cSchedTime;

	return bRtnStatus;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::IsValid
	Description :	Determines if all of the input parameters are valid.
	Return :		
		BOOL		-	TRUE if the parameters are valid.
						FALSE if any one of the parameters are invalid.
	Parameters :	
		int nYear		-	Year value in the range of 1971-2036
		int nGMTDay		-	GMT Day value in the range of 1-365/366
		int nHour		-	Hour value in the range of 0-23
		int nMin		-	Minute value in the range of 0-59
		int nSec		-	Second value in the range of 0-59
		int nMilliSec	-	Milli-Sec value in the range of 0-999
		CSchedTime* pSchedTime	-	A pointer to a CSchedTime that gets
									initialized if the input parametes
									specify a valid span.
		CString* pstrErrorText	-	Error text if the return status
									is FALSE.
	Note :			This is a static function.  The two pointers are
					optional and are only set if their values are not
					NULL.
\*======================================================================*/
BOOL CSchedTime::IsValid(int nYear, int nGMTDay, int nHour, int nMin, int nSec,
						 int nMilliSec, CSchedTime* pSchedTime, CString* pstrErrorText)
{
	CString strErrorText = _T("");
	BOOL bRtnStatus = TRUE;
		
	// Check the Year range
	if (bRtnStatus)
	{
		if ((nYear < 1970) || (nYear > 2036))
		{
			bRtnStatus = FALSE;
			strErrorText.Format (_T("Invalid year <%d> specified, must be between 1971 and 2036."), nYear);
		}
	}

	// Check the day range
	if (bRtnStatus)
	{
		int nMaxDay = ((nYear % 4) == 0) ? 366 : 365; //Good enough for the valid range
		if ((nGMTDay < 1) || (nGMTDay > nMaxDay))
		{
			bRtnStatus = FALSE;
			strErrorText.Format (_T("Invalid day <%d> specified, must be between 1 and %d."), nGMTDay, nMaxDay);
		}
	}

	// Check the hour range
	if (bRtnStatus)
	{
		if ((nHour < 0) || (nHour > 23))
		{
			bRtnStatus = FALSE;
			strErrorText.Format (_T("Invalid hour <%d> specified, must be between 0 and 23.  Proper format DDD-HH:MM:SS"), nHour);
		}
	}

	// Check the minute range
	if (bRtnStatus)
	{
		if ((nMin < 0) || (nMin > 59))
		{
			bRtnStatus = FALSE;
			strErrorText.Format (_T("Invalid minute <%d> specified, must be between 0 and 59.  Proper format HH:MM:SS"), nMin);
		}
	}

	// Check the second range
	if (bRtnStatus)
	{
		if ((nSec < 0) || (nSec > 59))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format (_T("Invalid second <%d> specified, must be between 0 and 59"), nSec);
			strErrorText.Format(_T("Invalid seconds <%d> specified, must be between 0 and 59.  Proper format HH:MM:SS"), nSec);
		}
	}

	// Check the millisecond range
	if (bRtnStatus)
	{
		if ((nMilliSec < 0) || (nMilliSec > 999))
		{
			bRtnStatus = FALSE;
			strErrorText.Format (_T("Invalid millisec <%d> specified, must be between 0 and 999.  Proper format HH:MM:SS.mmm"), nMilliSec);
		}
	}

	// If the data is valid and the pointer to the object is not NULL, then set the object
	if (bRtnStatus)
	{
		if (pSchedTime)
			*pSchedTime = CSchedTime(nYear, nGMTDay, nHour, nMin, nSec, nMilliSec);
	}

	if (pstrErrorText)
		*pstrErrorText = strErrorText;

	return bRtnStatus;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::IsValid
	Description :	Determines if the input text string is formatted
					properly for a CSchedTime object.
	Return :		
		BOOL		-	TRUE if the string is valid.
						FALSE if the string is invalid.  See reason for
						being invalid is stored in the ErrorText
						parameter.
	Parameters :	
		CString strText	-	CString to check.
		CSchedTimeSpan* pSchedTime	-	If the time span is valid, this
										parameter will contain a copy
										of it.
		CSchedTimeSpan* pSchedTime	-	A pointer to a CSchedTime
										that gets initialited if the
										input parametes specify a
										valid time.
		CString* pstrErrorText		-	Error text if the return
										status is FALSE.
	Note :			This is a static function.  The two pointers are
					optional and are only set if their values are not
					NULL. The CString must be in one of the follow formats:
						YYYY-DDD-HH:MM:SS.mmm
						YYYY-DDD-HH:MM:SS
\*======================================================================*/
BOOL CSchedTime::IsValid(CString strText, CSchedTime* pSchedTime, CString* pstrErrorText)
{
	CString strErrorText = _T("");
	BOOL bRtnStatus = TRUE;
	
	CString strYear		= _T("1971");
	CString strGMTDay	= _T("001");
	CString strHour		= _T("00");
	CString strMin		= _T("00");
	CString strSec		= _T("00");
	CString strMilliSec	= _T("000");

	int		nYear		= 0;
	int		nGMTDay		= 0;
	int		nHour		= 0;
	int		nMin		= 0;
	int		nSec		= 0;
	int		nMilliSec	= 0;

	// Remove quotes if present.
	int nPos = 0;
	if ( -1 != (nPos = strText.Find(_T("\""), 0))){
		strText.Delete(nPos, 1);
	}
	if ( -1 != (nPos = strText.Find(_T("\""), 0))){
		strText.Delete(nPos, 1);
	}
	
	int nLength = strText.GetLength();

	switch (nLength)
	{
		case 21	:	//YYYY-DDD-HH:MM:SS.mmm
		{
			if ((strText[4] == '-') && (strText[8] == '-') && (strText[11] == ':') &&
				(strText[14] == ':') && (strText[17] == '.'))
			{
				strYear		= strText.Mid(0,4);		//Extract the Year
				strGMTDay	= strText.Mid(5,3);		//Extract the GMT Day
				strHour		= strText.Mid(9,2);		//Extract the Hour
				strMin		= strText.Mid(12,2);	//Extract the Minute
				strSec		= strText.Mid(15,2);	//Extract the Second
				strMilliSec	= strText.Mid(18,3);	//Extract the Millisecond
			}
			else
			{
				bRtnStatus = FALSE;
				// strErrorText.Format(_T("Invalid time format <%s>"), strText);
				strErrorText.Format(_T("Invalid time format <%s>.  Proper format YYYY-DDD-HH:MM:SS.mmm"), strText);
			}
			break;
		}
		case 17	:	//YYYY-DDD-HH:MM:SS
		{
			if ((strText[4] == '-') && (strText[8] == '-') && (strText[11] == ':') && (strText[14] == ':'))
			{
				strYear		= strText.Mid(0,4);		//Extract the Year
				strGMTDay	= strText.Mid(5,3);		//Extract the GMT Day
				strHour		= strText.Mid(9,2);		//Extract the Hour
				strMin		= strText.Mid(12,2);	//Extract the Minute
				strSec		= strText.Mid(15,2);	//Extract the Second
			}
			else
			{
				bRtnStatus = FALSE;
				strErrorText.Format(_T("Invalid time format <%s>.   Proper format YYYY-DDD-HH:MM:SS.mmm"), strText);
			}
			break;
		}
		case 16	:	// DDD-HH:MM:SS.ttt
		{
			if ((strText[3] == '-') && (strText[6] == ':') &&
				(strText[9] == ':') && (strText[12] == '.'))
			{
			//	strYear		= strText.Mid(0,4);		//Extract the Year
				strGMTDay	= strText.Mid(0,3);		//Extract the GMT Day
				strHour		= strText.Mid(4,2);		//Extract the Hour
				strMin		= strText.Mid(7,2);		//Extract the Minute
				strSec		= strText.Mid(10,2);	//Extract the Second
				strMilliSec	= strText.Mid(13,3);	//Extract the Millisecond
			}
			else
			{
				bRtnStatus = FALSE;
				strErrorText.Format(_T("Invalid time format <%s>.  Proper format YYYY-DDD-HH:MM:SS.mmm"), strText);
			}
			break;
		}

		default	:	 
		{
			bRtnStatus = FALSE;
			strErrorText.Format(_T("Invalid time format <%s>.  Proper format YYYY-DDD-HH:MM:SS.mmm"), strText);
			break;
		}
	}

	// Make sure that the year field contain valid characters
	// Range checking will be done later
	if (bRtnStatus)
	{
		if ((strYear[0] < '1') || (strYear[0] > '2') ||
			(strYear[1] < '0') || (strYear[1] > '9') ||
			(strYear[2] < '0') || (strYear[2] > '9') ||
			(strYear[3] < '0') || (strYear[3] > '9'))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format(_T("Invalid year <%s>"), strYear);
			strErrorText.Format (_T("Invalid year <%s> specified, must be between 1971 and 2036."), strYear);
		}

	}
	// Make sure that the GMT day field contain valid characters
	// Range checking will be done later
	if (bRtnStatus)
	{
		if ((strGMTDay[0] < '0') || (strGMTDay[0] > '3') ||
			(strGMTDay[1] < '0') || (strGMTDay[1] > '9') ||
			(strGMTDay[2] < '0') || (strGMTDay[2] > '9'))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format(_T("Invalid GMT day <%s>"), strGMTDay);
			strErrorText.Format(_T("Invalid GMT day <%s> specified, must be between 0 and 366.  Proper format DDD-HH:MM:SS"), strGMTDay);
		}
	}
	// Make sure that the Hour field contain valid characters
	if (bRtnStatus)
	{
		if ((strHour[0] < '0') || (strHour[0] > '2') ||
			(strHour[1] < '0') || (strHour[1] > '9'))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format(_T("Invalid hour <%s>"), strHour);
			strErrorText.Format(_T("Invalid hour <%s> specified, must be between 0 and 23.  Proper format HH:MM:SS"), strHour);
		}
	}
	// Make sure that the minute field contain valid characters
	if (bRtnStatus)
	{
		if ((strMin[0] < '0') || (strMin[0] > '5') ||
			(strMin[1] < '0') || (strMin[1] > '9'))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format(_T("Invalid minute <%s>"), strMin);
			strErrorText.Format(_T("Invalid minutes <%s> specified, must be between 0 and 59.  Proper format HH:MM:SS"), strMin);

		}
	}
	// Make sure that the second field contain valid characters
	if (bRtnStatus)
	{
		if ((strSec[0] < '0') || (strSec[0] > '5') ||
			(strSec[1] < '0') || (strSec[1] > '9'))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format(_T("Invalid second <%s>"), strSec);
			strErrorText.Format(_T("Invalid seconds <%s>, must be between 0 and 59. Proper format HH:MM:SS"), strSec);
		}
	}
	// Make sure that the day field contain valid characters
	if (bRtnStatus)
	{
		if ((strMilliSec[0] < '0') || (strMilliSec[0] > '9') ||
			(strMilliSec[1] < '0') || (strMilliSec[1] > '9') ||
			(strMilliSec[2] < '0') || (strMilliSec[2] > '9'))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format(_T("Invalid milli second <%s>"), strMilliSec);
			strErrorText.Format(_T("Invalid milliseconds <%s> specified, must be between 0 and 999.  Proper format HH:MM:SS.mmm"), strMilliSec);
		}
	}

	// Convert the strings to integers and create the time span object
	// Range check the year and GMT Day
	if (bRtnStatus)
	{
		nYear       = _ttoi(strYear);
		nGMTDay		= _ttoi(strGMTDay);
		nHour		= _ttoi(strHour);
		nMin		= _ttoi(strMin);
		nSec		= _ttoi(strSec);
		nMilliSec	= _ttoi(strMilliSec);

		if ((nYear < 1971) || (nYear > 2036))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format(_T("Invalid year <%d>"), nYear);
			strErrorText.Format(_T("Invalid year <%d> specified, must be between 1971 and 2036."), nYear);

		}

		if (bRtnStatus)
		{
			int nMaxDay = ((nYear % 4) == 0) ? 366 : 365; //Good enough for the valid range
			if ((nGMTDay < 1) || (nGMTDay > nMaxDay))
			{
				bRtnStatus = FALSE;
				// strErrorText.Format(_T("Invalid GMT day <%d>"), nGMTDay);
				strErrorText.Format(_T("Invalid GMT day <%d> specified, must be between 0 and 366."), nGMTDay);
			}
		}
	}

	// If the data is valid and the pointer to the object is not NULL, then set the object
	if (bRtnStatus)
	{
		if (pSchedTime) {
			*pSchedTime = CSchedTime(nYear, nGMTDay, nHour, nMin, nSec, nMilliSec);
		} 
	}

	if (pstrErrorText)
		*pstrErrorText = strErrorText;

	return bRtnStatus;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::GetTimeString
	Description :	Returns a CString representation of the object.  All
					fields are left filled with '0' if necessary.
	Return :		
		CString	-	The CString representation of the object.
	Parameters :
		BOOL bShowMilliSec	-	If TRUE. the millisecond field
								is included.
	Note :			None.
\*======================================================================*/
CString CSchedTime::GetTimeString(BOOL bShowMilliSec)
{
	CString strTime = m_cTime.Format(_T("%Y-%j-%H:%M:%S"));
	CString strMilliSec = _T("");

	if (bShowMilliSec)
		strMilliSec.Format(_T(".%0.3d"), m_nMilliSec);

	return strTime + strMilliSec;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::operator=
	Description :	Copy operator
	Return :		
		CSchedTime			-	Copy operator
	Parameters :	
		CSchedTime &other	-	Object to copy
	Note :			
\*======================================================================*/
CSchedTime CSchedTime::operator=(const CSchedTime &other)
{
	m_nMilliSec = other.m_nMilliSec;
	m_cTime = other.m_cTime;
	return *this;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::operator+
	Description :	Adds a CSchedTimeSpan object to a CSchedTime object.
	Return :		
		CSchedTime				-	The result.
	Parameters :	
		CSchedTimeSpan &other	-	Object to add.
	Note :			None.
\*======================================================================*/
CSchedTime CSchedTime::operator+(const CSchedTimeSpan &other)
{
	int nMilliSec = m_nMilliSec + other.m_nMilliSec;
	if (nMilliSec >999)
	{
		CTimeSpan cOneSec = CTimeSpan(0,0,0,1);
		nMilliSec -= 1000;
		return (CSchedTime ((m_cTime + other.m_cTimeSpan + cOneSec), nMilliSec));
	}
	else
		return (CSchedTime ((m_cTime + other.m_cTimeSpan), nMilliSec));
}


/*======================================================================*\
	Author :Frederick J. Shaw		Date : 06/13/01			version 1.0
  ======================================================================
	Function :		CSchedTime::operator+=
	Description :	Adds a CSchedTime object to a CSchedTime object.
	Return :		
		CSchedTime				-	The result.
	Parameters :	
		CSchedTime &other	-	Object to add.
	Note :			None.
\*======================================================================*/

/*
CSchedTime CSchedTime::operator+=(const CSchedTimeSpan &other)
{
	int nMilliSec = m_nMilliSec + other.m_nMilliSec;
	if (nMilliSec >999)
	{
		CTimeSpan cOneSec = CTimeSpan(0,0,0,1);
		nMilliSec -= 1000;
		return (CSchedTime ((m_cTime + other.m_cTimeSpan + cOneSec), nMilliSec));
	}
	else
		return (CSchedTime ((m_cTime + other.m_cTimeSpan), m_nMilliSec));
}
*/  


/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::operator<
	Description :	Less than comparison operator.
	Return :		
		BOOL	-	TRUE if this is less than other, otherwise FALSE.
	Parameters :	
		CSchedTime &other	-	comparison operator
	Note :			
\*======================================================================*/
BOOL CSchedTime::operator<(CSchedTime &other)
{
	if (m_cTime == other.m_cTime)
		return (m_nMilliSec < other.m_nMilliSec);
	else
		return (m_cTime < other.m_cTime);
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 05/11/2001		version 1.0
  ======================================================================
	Function :		CSchedTime::operator+
	Description :	Adds an  integer number of seconds to a CSchedTime object.
	Return :		
		CSchedTime				-	The result.
	Parameters :	
		int nSecs	-	vaule in seconds no milliseconds
	Note :			None.
\*======================================================================*/

CSchedTime CSchedTime::operator+(const int nSecs)
{
	CTimeSpan cSecs2Add = CTimeSpan(0,0,0,nSecs);
	return (CSchedTime ((m_cTime + cSecs2Add), m_nMilliSec));
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 05/11/2001		version 1.0
  ======================================================================
	Function :		CSchedTime::operator+
	Description :	Adds a floating point number of seconds to a CSchedTime object.
	Return :		
		CSchedTime				-	The result.
	Parameters :	
		float   -         number of seconds to add.
	Note :			None.
\*======================================================================*/
CSchedTime CSchedTime::operator+(const float fSecs)
{
	double dSec = 0.0;
	double dMilliSec  = 0.0;
	int nMilliSec = 0;
	int nSec = 0;

	dMilliSec = modf(fSecs, &dSec);
	nMilliSec = (int)(dMilliSec * 1000.0);
	nSec = (int)dSec;

	m_nMilliSec += nMilliSec;

	// Add milliseconds togeather.  
	// Keep subtracting while greater than 1000.
	// At same time add 1 to seconds.
	bool bDone = false;
	while (!bDone){
		if (m_nMilliSec > 999){
			nSec += 1;
			m_nMilliSec -= 1000;
		} else {
			bDone = true;
		}
	}

	
	CTimeSpan cSecs2Add = CTimeSpan(0, 0, 0, nSec);
	return (CSchedTime (m_cTime + cSecs2Add, m_nMilliSec));
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::operator<=
	Description :	Less than or equal comparison operator.
	Return :		
		BOOL	-	TRUE if this is less or equal to other, otherwise FALSE.
	Parameters :	
		CSchedTime &other	-	comparison operator
	Note :			
\*======================================================================*/
BOOL CSchedTime::operator<=(CSchedTime &other)
{
	if (m_cTime == other.m_cTime)
		return (m_nMilliSec <= other.m_nMilliSec);
	else
		return (m_cTime <= other.m_cTime);
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::operator>
	Description :	Greater than comparison operator.
	Return :		
		BOOL	-	TRUE if this is greater than other, otherwise FALSE.
	Parameters :	
		CSchedTime &other	-	comparison operator
	Note :			
\*======================================================================*/
BOOL CSchedTime::operator>(CSchedTime &other)
{
	if (m_cTime == other.m_cTime)
		return (m_nMilliSec > other.m_nMilliSec);
	else
		return (m_cTime > other.m_cTime);
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::operator>=
	Description :	Greater than or equal comparison operator.
	Return :		
		BOOL	-	TRUE if this is greater than or equal to other,
					otherwise FALSE.
	Parameters :	
		CSchedTime &other	-	comparison operator
	Note :			
\*======================================================================*/
BOOL CSchedTime::operator>=(CSchedTime &other)
{
	if (m_cTime == other.m_cTime)
		return (m_nMilliSec >= other.m_nMilliSec);
	else
		return (m_cTime >= other.m_cTime);
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTime::operator==
	Description :	Equality comparison operator
	Return :		
		BOOL	-	TRUE the two objects are equal in value.
	Parameters :	
		CSchedTime &other	-	comparison operator
	Note :			
\*======================================================================*/
BOOL CSchedTime::operator==(CSchedTime &other)
{
	if (m_cTime == other.m_cTime)
		return (m_nMilliSec == other.m_nMilliSec);
	else
		return (FALSE);
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
	======================================================================
	Function :		CSchedTime::operator+
	Description :	Adds a CSchedTimeSpan object to a CSchedTime object.
	Return :		
		CSchedTime				-	The result.
	Parameters :	
		CSchedTimeSpan &other	-	Object to add.
	Note :			None.
	\*======================================================================*/
CSchedTime CSchedTime::operator-(const CSchedTimeSpan &other){
		int nMilliSec = m_nMilliSec - other.m_nMilliSec;
		if (nMilliSec < 0)
		{
			CTimeSpan cOneSec = CTimeSpan(0,0,0,1);
			nMilliSec += 1000;
			return (CSchedTime ((m_cTime - other.m_cTimeSpan - cOneSec), nMilliSec));
		}
		else{
			return (CSchedTime ((m_cTime - other.m_cTimeSpan), m_nMilliSec));
		}
}

/*======================================================================*\
	Author :Frederick J. Shaw			Date : 			version 1.0
	======================================================================
	Function :		CSchedTime::operator-
	Description :	Determines difference between two times.
	Return :		
		CSchedTimeSpan		-	The result.
	Parameters :	
		CSchedTime &other	-	Object to subtract.
	Note :			None.
\*======================================================================*/
CSchedTimeSpan CSchedTime::operator-(const CSchedTime &other){
		int nMilliSec = m_nMilliSec - other.m_nMilliSec;
		if (nMilliSec < 0)
		{
			CTimeSpan cOneSec = CTimeSpan(0,0,0,1);
			nMilliSec += 1000;
			return (CSchedTimeSpan ((m_cTime - other.m_cTime - cOneSec), nMilliSec));
		}
		else{
			return (CSchedTimeSpan ((m_cTime - other.m_cTime), m_nMilliSec));
		}
}


/*======================================================================*\
	Author: Frederick J. Shaw		Date : 			version 1.0
	======================================================================
	Function :		CSchedTime::operator+
	Description :	Adds a CSchedTimeSpan object to a CSchedTime object.
	Return :		
		CSchedTime				-	The result.
	Parameters :	
		CSchedTimeSpan &other	-	Object to add.
	Note :			None.
\*======================================================================*/
/*
CSchedTime CSchedTime::operator-=(const CSchedTimeSpan &other)
{
	int nMilliSec = m_nMilliSec + other.m_nMilliSec;
	if (nMilliSec >999)
	{
		CTimeSpan cOneSec = CTimeSpan(0,0,0,1);
		nMilliSec -= 1000;
		return (CSchedTime ((m_cTime + other.m_cTimeSpan + cOneSec), nMilliSec));
	}
	else
		return (CSchedTime ((m_cTime + other.m_cTimeSpan), m_nMilliSec));
}
*/


/*======================================================================*\
	Author :Frederick J. Shaw		Date : 0/05/2001		version 1.0
  ======================================================================
	Function :	GetSchedTimeFromNoonInSecs()
	Description : Returns the number of seconds from midnight.  
	              This is the time the ACE uses.
	Return : seconds as a float.		
		
	Parameters :	
		none
	Note :			
\*======================================================================*/

double CSchedTime::GetSchedTimeFromNoonInSecs(void){
	
	double dTotalSecs = 0.0;
	CString strTime = m_cTime.Format(_T("%H:%M:%S"));
		
	// Determine if it is AM or PM
    if (( m_cTime.GetHour() < 12) && (m_cTime.GetHour() >= 0)){
        // Time is am.  Need to add 43200 seconds to value
        dTotalSecs = 43200.000; // Number of seconds in 12 hours
		   dTotalSecs += (double)m_cTime.GetHour() * 3600.0;
		   dTotalSecs += (double)m_cTime.GetMinute() * 60.0;
		   dTotalSecs += (double)m_nMilliSec/1000;
		   dTotalSecs += (double)m_cTime.GetSecond();
		
    } else if ( m_cTime.GetHour() >= 12) {
        // Time is pm.  Need to subtract 43200 seconds to value
        dTotalSecs = -43200.000; // Number of seconds in 12 hours
		 dTotalSecs += (double)m_cTime.GetHour() * 3600.0;
		 dTotalSecs += (double)m_cTime.GetMinute() * 60.0;
		   dTotalSecs += (double)m_nMilliSec/1000;
		   dTotalSecs += (double)m_cTime.GetSecond();
	}
  
    return dTotalSecs;
};

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 0/05/2001		version 1.0
  ======================================================================
	Function :	GetSchedTimeFromNoonInSecs()
	Description : Returns the number of seconds from midnight.  
	              This is the time the ACE uses.
	Return : seconds as a float.		
		
	Parameters :	
		none
	Note :			
\*======================================================================*/

double  CSchedTime::GetSchedTimeInSeconds(void){

	double dTotalSecs = 0.0;
	CString strTime = m_cTime.Format(_T("%H:%M:%S"));
	dTotalSecs += (double)m_cTime.GetHour() * 3600.0;
	dTotalSecs += (double)m_cTime.GetMinute() * 60.0;
	dTotalSecs += (double)m_cTime.GetSecond();
	dTotalSecs += (double)m_nMilliSec/1000.0;

	return dTotalSecs;

};

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 06/06/2001		version 1.0
  ======================================================================
	Function :	GetCurrSchedTime()
	Description :   
	              
	Return : 		
		
	Parameters :	
		none
	Note :			
\*======================================================================*/
CString  CSchedTime::GetCurrSchedTime(void){

	CTime t = CTime::GetCurrentTime();
	return t.Format (_T("%Y-%j-%H:%M:%S"));
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 06/06/2001		version 1.0
  ======================================================================
	Function :	CSchedTime::GetSchedTimeInMinutes()

	Description :  Calculates the number of minutes from midnight.
	              
	Return :	  double		
		
	Parameters : 	none
	
    Revision history:	Frederick J. Shaw Sept 2, 1999
                        Initial release.
	Note :			
\*======================================================================*/
double CSchedTime::GetSchedTimeInMinutes(void){

	double dTotalMinutes = 0.0;
	CString strTime = m_cTime.Format(_T("%H:%M:%S"));

	dTotalMinutes += (double)m_cTime.GetHour() * 60.0;
	dTotalMinutes += (double)m_cTime.GetMinute();
	dTotalMinutes += (double)m_cTime.GetSecond()/60;
	dTotalMinutes += (double)m_nMilliSec/60000;
		
	return dTotalMinutes;
};

/*======================================================================*\
	Author :Frederick J. Shaw		Date : Sept 2, 1999		version 1.0
  ======================================================================
	Function :	CSchedTime::CreateSTOLTimeTag

	Description :  
	              
	Return :	  bool		
		
	Parameters : 	none
	
    Revision history:  Initial Release	 
                        
	Note :			
\*======================================================================*/
bool CSchedTime::CreateSTOLTimeTag(CString &strTimeTag){

	// TODO: Add error checking
	bool bReturn_Status = true;

	// Check if timetag is present.
	strTimeTag  = _T("WAIT @(time(\"");
	strTimeTag += GetTimeString(TRUE);
	strTimeTag += _T("\") + $"); 
	strTimeTag += ctstrSCHEDX_BIAS;
	strTimeTag += _T(")");

	return bReturn_Status;
}


/*======================================================================*\
	Author :Frederick J. Shaw		Date : 02/27/2002		version 1.0
  ======================================================================
	Function :	CSchedTime::Parser()

	Description :  
	              
	Return :	  	
		
	Parameters : 	
	
    Revision history:	Frederick J. Shaw Febuary 27,2002
    Initial release.
	Note :			
\*======================================================================*/
BOOL CSchedTime::Parser(CString strText, CSchedTime* pSchedTime, CString* pstrErrorText){

	BOOL bReturnStatus = TRUE;
	CString strErrorText = _T("");

	// Remove quotes if present.
	strText.Replace(_T("\""), _T(""));
	strText.Replace(_T("\""), _T(""));
	strText.TrimLeft();
	strText.TrimRight();

	int nPos      = 0;
	int nYear     = 1971;
	int nDay      = 1;
	int nHour     = 0;
	int nMin      = 0;
	int nSec      = 0;
	int nMilliSec = 0;

	if (-1 != (nPos = strText.Find(_T('-')))){
		// At least one dash is found
		CString strTemp01;
		// Extract up to the dash
		strTemp01 = strText.Left(nPos);
		strText.Delete(0, nPos + 1);
		if (-1 != (nPos = strText.Find(_T('-')))){
			// At least one dash is found
			CString strTemp02;
			// Extract up to the dash
			strTemp02 = strText.Left(nPos);
			nDay  = _ttoi(strTemp02);
			nYear = _ttoi(strTemp01);
			strText.Delete(0, nPos + 1);
		} else {
			nDay = _ttoi(strTemp01);
			strText.Delete(0, nPos + 1);
		}
	} 

	if (-1 != (nPos = strText.Find(_T(':')))){
		// At least one dash is found
		CString strTemp01;
		strTemp01 = strText.Left(nPos);
		strText.Delete(0, nPos + 1);

		if (-1 != (nPos = strText.Find(_T(':')))){
			// At least one dash is found
			CString strTemp02;
			strTemp02 = strText.Left(nPos);
			// Extract up to the dash
			nMin = _ttoi(strTemp02);
			nHour   = _ttoi(strTemp01);
			strText.Delete(0, nPos + 1);
		} else {
			// Extract up to the colon
			nMin = _ttoi(strTemp01);
		}
	} 

	if (-1 != (nPos = strText.Find(_T('.')))){
		// Decimal found.

		// Need to round out milisecs or round off
		// greater than 999.
		CString strTemp01, strMSec;
		strTemp01 = strText.Left(nPos);
		strText.Delete(0, nPos + 1);
		if (strText.GetLength() >= 3) {
			strMSec = strText.Right(3);
		} else if (strText.GetLength() == 2){
			strMSec = strText + _T('0');			
		} else if (strText.GetLength() == 1){
			strMSec = strText + _T("00");
		}
		nSec      = (_ttoi(strTemp01));
		nMilliSec = (_ttoi(strText));
	} else {
		// No decimal present
		// Verify that string contains alphanumeric characters.
		if ((!strText.IsEmpty()) && (0 == _istdigit(strText[0]))){
			int nTemp = _istdigit(strText[0]);
			bReturnStatus = FALSE;
			strErrorText.Format(_T("Invalid format for time string! (%s).  Proper format HH:MM:SS.mmm"), strText);
		} else if (!strText.IsEmpty()){
			nSec = _ttoi(strText);
		}
	}

	// Make sure that the year field contain valid characters
	if ((nYear < 1971) || (nYear > 2036)){
		bReturnStatus = FALSE;
		// strErrorText.Format(_T("Invalid year <%d>"), nYear);
		strErrorText.Format(_T("Invalid year <%d> specified, must be between 1971 and 2036."), nYear);
	}

	// Make sure that the day field contain valid characters
	if (bReturnStatus == TRUE){
		if ((nDay < 1) || (nDay > 366)){
			bReturnStatus = FALSE;
			// strErrorText.Format(_T("Invalid day <%d>"), nDay);
			strErrorText.Format(_T("Invalid day <%d> specified, must be between 1 and 366."), nDay);
		}
	}

	// Make sure that the Hour field contain valid characters
	if (bReturnStatus == TRUE){
		if ((nHour < 0) || (nHour > 23)){
			bReturnStatus = FALSE;
			// strErrorText.Format(_T("Invalid hour <%d>"), nHour);
			strErrorText.Format(_T("Invalid hour <%d> specified, must be between 0 and 23. Proper format: HH:MM:SS"), nHour);
		}
	}
	// Make sure that the minute field contain valid characters
	if (bReturnStatus == TRUE){
		if ((nMin < 0) || (nMin > 59)){
			bReturnStatus = FALSE;
			// strErrorText.Format(_T("Invalid minute <%d>"), nMin);
			strErrorText.Format(_T("Invalid minute <%d> specified, must be between 0 and 59. Proper format: HH:MM:SS"), nMin);
		}
	}
	// Make sure that the second field contain valid characters
	if (bReturnStatus == TRUE)
	{
		if ((nSec < 0) || (nSec > 59)){
			bReturnStatus = FALSE;
			// strErrorText.Format(_T("Invalid second <%d>"), nSec);
			strErrorText.Format(_T("Invalid seconds <%d> specified, must be between 0 and 59. Proper format HH:MM:SS"), nSec);
		}
	}

	// Make sure that the millsec field contain valid characters
	/*
	if (bReturnStatus == TRUE)
	{
		if ((nMilliSec < 0) || (nMilliSec > 999))
		{
			bReturnStatus = FALSE;
			strErrorText.Format(_T("Invalid milli second <%d>"), nMilliSec);	
		}
	}
	*/

	CSchedTime	cSchedTime(nYear, nDay, nHour, nMin, nSec, nMilliSec);

	if (pSchedTime)
		*pSchedTime = cSchedTime;

	if (pstrErrorText)
		*pstrErrorText = strErrorText;

	return bReturnStatus;
};



/*======================================================================*\
	Class CSchedTimeSpan
  ======================================================================
	Function :	This time class is used for storing relative time
				periods.  It includes a CTimeSpan member object that holds
				the	general time period plus a millisecond member to hold
				the fractional part.
\*======================================================================*/
/*======================================================================*\
	Author :Don Sanders		Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::CSchedTimeSpan
	Description :	Default constructor.  Sets the time span to 0.
	Return :		
		constructor		-	Default constructor.
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CSchedTimeSpan::CSchedTimeSpan()
{
	m_cTimeSpan = CTimeSpan(0, 0, 0, 0);
	m_nMilliSec = 0;
	m_strErrorText = _T("");
}

/*======================================================================*\
	Author :Don Sanders		Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::CSchedTimeSpan
	Description :	Constructs a object based upon the parameters passed
					in.
	Return :		
		constructor			-	Constructor.
	Parameters :	
		CTimeSpan cTimeSpan	-	A CTimeSpan object used to base the new
								object on.
		int nMilliSec		-	The millisecond portion of the object.
	Note :			The nMilliSec parameter must be in the range of 0 to
					999.  The debug version causes an assertion.  It is
					programmers responsibility to range check this value
\*======================================================================*/
CSchedTimeSpan::CSchedTimeSpan(CTimeSpan cTimeSpan, int nMilliSec)
{
	ASSERT ((nMilliSec >= 0) && (nMilliSec <= 999));
	m_cTimeSpan = cTimeSpan;
	m_nMilliSec = nMilliSec;
	m_strErrorText = _T("");
}

/*======================================================================*\
	Author :Don Sanders		Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::CSchedTimeSpan
	Description :	Constructs a object based upon the parameters passed
					in.
	Return :		
		constructor		-	Constructor.
	Parameters :	
		int nDay		-	Number of days in the range of 0 to 999.
		int nHour		-	Number of hours in the range of 0 to 23.
		int nMin		-	Number of minutes in the range of 0 to 59.
		int nSec		-	Number of seconds in the range of 0 to 59.
		int nMilliSec	-	Number of milliseconds in the range of 0 to 999.
	Note :			The input parameters cause debug assertions if they
					are out of range.  It is the programmer's
					responsibility to range check the parameters.
\*======================================================================*/
CSchedTimeSpan::CSchedTimeSpan(int nDay, int nHour, int nMin, int nSec, int nMilliSec)
{
	ASSERT ((nMilliSec >= 0) && (nMilliSec <= 999));
	m_cTimeSpan = CTimeSpan(nDay, nHour, nMin, nSec);
	m_nMilliSec = nMilliSec;
	m_strErrorText = _T("");
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::~CSchedTimeSpan
	Description :	Default destructor
	Return :		
		destructor		-	Destructor.
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CSchedTimeSpan::~CSchedTimeSpan(){}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::Create
	Description :	Create a CSchedTimeSpan object from the specified
					parameters.
	Return :
		BOOL	-	TRUE if the parameters passed in were valid.
					FALSE if the parameters passed in were invalid or out
					of range.  Call GetLastErrorText() to retrieve the
					reason.
	Parameters :
		int nDay		-	Day value in the range of 0-999
		int nHour		-	Hour value in the range of 0-23
		int nMin		-	Minute value in the range of 0-59
		int nSec		-	Second value in the range of 0-59
		int nMilliSec	-	Milli-Sec value in the range of 0-999
	Note :			This routine ensures that all parameters are within
					range by calling the static function IsValid.  If
					the parameters are valid, the object is initialized
					to the parameters and TRUE is returned, otherwise
					FALSE is returned and GetLastErrorText() should be
					called to determine the reason.
\*======================================================================*/
BOOL CSchedTimeSpan::Create(int nDay, int nHour, int nMin, int nSec, int nMilliSec)
{
	CSchedTimeSpan	cSchedTimeSpan;
	BOOL bRtnStatus = IsValid(nDay, nHour, nMin, nSec, nMilliSec, &cSchedTimeSpan, &m_strErrorText);

	if (bRtnStatus)
		*this = cSchedTimeSpan;

	return bRtnStatus;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::Create
	Description :	Create a CSchedTimeSpan object from the specified
					parameter.
	Return :
		BOOL	-	TRUE if the parameters passed in were valid.
					FALSE if the parameters passed in were invalid or out
					of range.  Call GetLastErrorText() to retrieve the
					reason.
	Parameters :
		CString strText	-	The text to create the object from.
	Note :			This routine ensures that the input text is valid
					by calling the static function IsValid.  If the text
					is valid, the object is initialized with the values
					contained with the string and TRUE is returned,
					otherwise FALSE is returned and GetLastErrorText()
					should be called to determine the reason.
\*======================================================================*/
BOOL CSchedTimeSpan::Create(CString strText)
{
	CSchedTimeSpan	cSchedTimeSpan;;
//	BOOL bRtnStatus = IsValid(strText, &cSchedTimeSpan, &m_strErrorText);
	BOOL bRtnStatus = Parser(strText,&cSchedTimeSpan, &m_strErrorText);

	if (bRtnStatus)
		*this = cSchedTimeSpan;

	return bRtnStatus;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::IsValid
	Description :	Determines if all of the input parameters are valid.
	Return :		
		BOOL		-	TRUE if the parameters are valid.
						FALSE if any one of the parameters are invalid.
	Parameters :	
		int nDay		-	Day value in the range of 0-999
		int nHour		-	Hour value in the range of 0-23
		int nMin		-	Minute value in the range of 0-59
		int nSec		-	Second value in the range of 0-59
		int nMilliSec	-	Milli-Sec value in the range of 0-999
		CSchedTimeSpan* pSchedTimeSpan	-	A pointer to a CSchedTimeSpan
											that gets initialited if the
											input parametes specify a
											valid time span.
		CString* pstrErrorText			-	Error text if the return
											status is FALSE.
	Note :			This is a static function.  The two pointers are
					optional and are only set if their values are not
					NULL.
\*======================================================================*/
BOOL CSchedTimeSpan::IsValid(int nDay, int nHour, int nMin, int nSec, int nMilliSec, 
							 CSchedTimeSpan* pSchedTimeSpan, CString* pstrErrorText)
{
	CString strErrorText = _T("");
	BOOL bRtnStatus = TRUE;
	
	// Check the day range
	if (bRtnStatus)
	{
		if ((nDay < 0) || (nDay > 999))
		{
			bRtnStatus = FALSE;
			strErrorText.Format (_T("Invalid day <%d> specified, must be between 0 and 999"), nDay);
		}
	}

	// Check the hour range
	if (bRtnStatus)
	{
		if ((nHour < 0) || (nHour > 23))
		{
			bRtnStatus = FALSE;
			strErrorText.Format (_T("Invalid hour <%d> specified, must be between 0 and 23.  Proper format: HH:MM:SS"), nHour);
		}
	}

	// Check the minute range
	if (bRtnStatus)
	{
		if ((nMin < 0) || (nMin > 59))
		{
			bRtnStatus = FALSE;
			strErrorText.Format (_T("Invalid minute <%d> specified, must be between 0 and 59. Proper format: HH:MM:SS"), nMin);
		}
	}

	// Check the second range
	if (bRtnStatus)
	{
		if ((nSec < 0) || (nSec > 59))
		{
			bRtnStatus = FALSE;
//			strErrorText.Format (_T("Invalid second <%d> specified, must be between 0 and 59"), nSec);
			strErrorText.Format (_T("Invalid seconds <%d> specified, must be between 0 and 59. Proper format 00:00:00"), nSec);
			// Error validating time tag; Invalid format for time. Seconds exceed 59 <70>. Proper format 00:00:00.
		}
	}

	// Check the millisecond range
	if (bRtnStatus)
	{
		if ((nMilliSec < 0) || (nMilliSec > 999))
		{
			bRtnStatus = FALSE;
			strErrorText.Format (_T("Invalid millisec <%d> specified, must be between 0 and 999.  Proper format: HH:MM:SS.mmm"), nMilliSec);
		}
	}

	CSchedTimeSpan	cSchedTimeSpan(nDay, nHour, nMin, nSec, nMilliSec);

	// If the data is valid and the pointer to the object is not NULL, then set the object
	if (bRtnStatus)
	{
		if (pSchedTimeSpan)
			*pSchedTimeSpan = cSchedTimeSpan;
	}

	if (pstrErrorText)
		*pstrErrorText = strErrorText;

	return bRtnStatus;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::IsValid
	Description :	Determines if the input text string is formatted
					properly for a CSchedTimeSpan object.
	Return :		
		BOOL		-	TRUE if the string is valid.
						FALSE if the string is invalid.  See reason for
						being invalid is stored in the ErrorText
						parameter.
	Parameters :	
		CString strText	-	CString to check.
		CSchedTimeSpan* pTimeSpan	-	If the time span is valid, this
										parameter will contain a copy
										of it.
		CSchedTimeSpan* pSchedTimeSpan	-	A pointer to a CSchedTimeSpan
											that gets initialited if the
											input parametes specify a
											valid time span.
		CString* pstrErrorText			-	Error text if the return
											status is FALSE.
	Note :			This is a static function.  The two pointers are
					optional and are only set if their values are not
					NULL. The CString must be in one of the follow formats:
						DDD-HH:MM:SS.mmm
						DDD-HH:MM:SS
						HH:MM:SS.mmm
						HH:MM:SS
\*======================================================================*/
BOOL CSchedTimeSpan::IsValid(CString strText, CSchedTimeSpan* pSchedTimeSpan, CString* pstrErrorText)
{
	CString strErrorText = _T("");
	BOOL bRtnStatus = TRUE;
	
	CString strDay		= _T("000");
	CString strHour		= _T("00");
	CString strMin		= _T("00");
	CString strSec		= _T("00");
	CString strMilliSec	= _T("000");

	int		nDay		= 0;
	int		nHour		= 0;
	int		nMin		= 0;
	int		nSec		= 0;
	int		nMilliSec	= 0;

	// Remove quotes if present.
	/*
	int nPos = 0;
	if ( -1 != (nPos = strText.Find(_T("\""), 0))){
		strText.Delete(nPos, 1);
	}
	if ( -1 != (nPos = strText.Find(_T("\""), 0))){
		strText.Delete(nPos, 1);
	}
	*/

	strText.Replace(_T("\""), _T(""));
	strText.Replace(_T("\""), _T(""));
	strText.TrimLeft();
	strText.TrimRight();

	int nLength = strText.GetLength();

	switch (nLength)
	{
		case 16	:	//DDD-HH:MM:SS.mmm
		{
			if ((strText[3] == '-') && (strText[6] == ':') && (strText[9] == ':') && (strText[12] == '.'))
			{
				strDay		= strText.Mid(0,3);		//Extract the Day
				strHour		= strText.Mid(4,2);		//Extract the Hour
				strMin		= strText.Mid(7,2);		//Extract the Minute
				strSec		= strText.Mid(10,2);	//Extract the Second
				strMilliSec	= strText.Mid(13,3);	//Extract the Millisecond
			}
			else
			{
				bRtnStatus = FALSE;
				strErrorText.Format(_T("Invalid time span format <%s>.  Proper format: DDD-HH:MM:SS.mmm"), strText);

			}
			break;
		}
		case 12	:	//DDD-HH:MM:SS or HH:MM:SS.mmm
		{
			if ((strText[3] == '-') && (strText[6] == ':') && (strText[9] == ':'))
			{
				strDay		= strText.Mid(0,3);		//Extract the Day
				strHour		= strText.Mid(4,2);		//Extract the Hour
				strMin		= strText.Mid(7,2);		//Extract the Minure
				strSec		= strText.Mid(10,2);	//Extract the Millisecond
			}
			else if ((strText[2] == ':') && (strText[5] == ':') && (strText[8] == '.'))
			{
				strHour		= strText.Mid(0,2);		//Extract the Hour
				strMin		= strText.Mid(3,2);		//Extract the Minure
				strSec		= strText.Mid(6,2);		//Extract the Second
				strMilliSec	= strText.Mid(9,3);		//Extract the Millisecond
			}
			else
			{
				bRtnStatus = FALSE;
				strErrorText.Format(_T("Invalid time span format <%s>. Proper format: DDD-HH:MM:SS.mmm"), strText);
			}
			break;
		}

		case 9	:	// MM:SS.ttt
		{
			if ((strText[2] == ':') && (strText[5] == '.'))
			{
				strMin		= strText.Mid(0,2);	// Extract the Minute
				strSec		= strText.Mid(3,2);	// Extract the Second
				strMilliSec	= strText.Mid(6,3);	// Extract the Millisecond
			}
			else
			{
				bRtnStatus = FALSE;
				strErrorText.Format(_T("Invalid time format <%s>. Proper format: HH:MM:SS.mmm"), strText);
			}
			break;
		}

		case 8	:	// M:SS.ttt && HH:MM:SS
		{
			if (-1 != strText.Find(_T('.'))){
				if ((strText[1] == ':') && (strText[4] == '.'))
				{
					strMin		= _T("0") + strText.Mid(0,1);	// Extract the Minute
					strSec		= strText.Mid(2,2);	            // Extract the Second
					strMilliSec	= strText.Mid(5,3);	            // Extract the Millisecond
				}
				else
				{
					bRtnStatus = FALSE;
					strErrorText.Format(_T("Invalid time format <%s>. Proper format: HH:MM:SS.mmm"), strText);
				}
			} else {
				if ((strText[2] == ':') && (strText[5] == ':'))
				{
					strHour		= strText.Mid(0,2);		//Extract the Hour
					strMin		= strText.Mid(3,2);		//Extract the Minute
					strSec		= strText.Mid(6,2);		//Extract the Second
				}
				else
				{
					bRtnStatus = FALSE;
					strErrorText.Format(_T("Invalid time span format <%s>.  Proper format: HH:MM:SS"), strText);
				}
			}
			break;
		}


		case 6	:	// SS.ttt
		{
			if (strText[2] == '.')
			{
				strSec		= strText.Mid(0,2);	// Extract the Second
				strMilliSec	= strText.Mid(3,3);	// Extract the Millisecond
			}
			else
			{
				bRtnStatus = FALSE;
				strErrorText.Format(_T("Invalid time format <%s>. Proper format: SS.mmm"), strText);
			}
			break;
		}

		case 5	:	// MM:SS
		{
			if (strText[2] == ':')
			{
				strMin		= strText.Mid(0,2);			// Extract the Minute
				strSec		= strText.Mid(3,2);			// Extract the Second
			}
			else
			{
				bRtnStatus = FALSE;
				strErrorText.Format(_T("Invalid time format <%s>.  Proper format: MM:SS"), strText);
			}
			break;
		}

		case 4	:	// S.ttt
		{
			if (strText[1] == '.')
			{
				strSec		= _T("0") + strText.Mid(0,1);	// Extract the Second
				strMilliSec	= strText.Mid(2,3);	// Extract the Millisecond
			}
			else
			{
				bRtnStatus = FALSE;
				strErrorText.Format(_T("Invalid time format <%s>.  Proper format: S.mmm"), strText);
			}
			break;
		}

		case 2	:	// SS
		{
			strSec 	= strText;			// Extract the second
			break;
		}

		case 1	:	// S
		{
			strSec		= _T("0") + strText; // Extract the second
			break;
		}

		default	:	
		{
			bRtnStatus = FALSE;
			strErrorText.Format(_T("Invalid time format <%s>. Proper format: YYYY-DDD-HH:MM:SS"), strText);
			break;
		}
	} 				

	// Make sure that the day field contain valid characters
	if (bRtnStatus)
	{
		if ((strDay[0] < '0') || (strDay[0] > '9') ||
			(strDay[1] < '0') || (strDay[1] > '9') ||
			(strDay[2] < '0') || (strDay[2] > '9'))
		{
			bRtnStatus = FALSE;
			strErrorText.Format(_T("Invalid day <%s>. must be between 0 and 59. Proper format: DDD-HH:MM:SS"), strDay);
		}
	}
	// Make sure that the Hour field contain valid characters
	if (bRtnStatus)
	{
		if ((strHour[0] < '0') || (strHour[0] > '9') ||
			(strHour[1] < '0') || (strHour[1] > '9'))
		{
			bRtnStatus = FALSE;
			strErrorText.Format(_T("Invalid hour <%s>, must be between 0 and 59. Proper format: HH:MM:SS"), strHour);
		}
	}
	// Make sure that the minute field contain valid characters
	if (bRtnStatus)
	{
		if ((strMin[0] < '0') || (strMin[0] > '9') ||
			(strMin[1] < '0') || (strMin[1] > '9'))
		{
			bRtnStatus = FALSE;
			strErrorText.Format(_T("Invalid minute <%s>, must be between 0 and 59. Proper format: HH:MM:SS"), strMin);
		}
	}
	// Make sure that the second field contain valid characters
	if (bRtnStatus)
	{
		if ((strSec[0] < '0') || (strSec[0] > '9') ||
			(strSec[1] < '0') || (strSec[1] > '9'))
		{
			bRtnStatus = FALSE;
			//strErrorText.Format(_T("Invalid second <%s>"), strSec);
			strErrorText.Format(_T("Invalid seconds <%s>, must be between 0 and 59. Proper format: HH:MM:SS"), strSec);
		}
	}
	// Make sure that the day field contain valid characters
	if (bRtnStatus)
	{
		if ((strMilliSec[0] < '0') || (strMilliSec[0] > '9') ||
			(strMilliSec[1] < '0') || (strMilliSec[1] > '9') ||
			(strMilliSec[2] < '0') || (strMilliSec[2] > '9'))
		{
			bRtnStatus = FALSE;
			// strErrorText.Format(_T("Invalid milli second <%s>"), strMilliSec);
			strErrorText.Format(_T("Invalid milliseconds <%s>, must be between 0 and 59. Proper format: HH:MM:SS"), strSec);
		}
	}



	// Convert the strings to integers and create the time span object
	// If the data is valid and the pointer to the object is not NULL, then set the object
	if (bRtnStatus)
	{
		nDay		= _ttoi(strDay);
		nHour		= _ttoi(strHour);
		nMin		= _ttoi(strMin);
		nSec		= _ttoi(strSec);
		nMilliSec	= _ttoi(strMilliSec);
	}

	CSchedTimeSpan	cSchedTimeSpan(nDay, nHour, nMin, nSec, nMilliSec);

	if (pSchedTimeSpan)
		*pSchedTimeSpan = cSchedTimeSpan;


	if (pstrErrorText)
		*pstrErrorText = strErrorText;

	return bRtnStatus;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::GetTimeSpanString
	Description :	Returns a CString representation of the object.  All
					fields are left filled with '0' if necessary.
	Return :		
		CString	-	The CString representation of the object.
	Parameters :
		BOOL bShowDay		-	If TRUE, the day field is included.
		BOOL bShowMilliSec	-	If TRUE. the millisecond field
								is included.
	Note :			None.
\*======================================================================*/
CString CSchedTimeSpan::GetTimeSpanString(BOOL bShowDay, BOOL bShowMilliSec)
{
	CString strTimeSpan = m_cTimeSpan.Format(_T("%H:%M:%S"));
	CString strDay		= _T("");
	CString strMilliSec = _T("");

	if (bShowDay)
		strDay.Format(_T("%0.3d-"), m_cTimeSpan.GetDays());

	if (bShowMilliSec)
		strMilliSec.Format(_T(".%0.3d"), m_nMilliSec);

	return strDay + strTimeSpan + strMilliSec;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::operator=
	Description :	Copy operator
	Return :		
		CSchedTimeSpan		-	Copy operator
	Parameters :	
		CSchedTimeSpan &other	-	Object to copy
	Note :			
\*======================================================================*/
CSchedTimeSpan CSchedTimeSpan::operator=(const CSchedTimeSpan &other)
{
	m_nMilliSec = other.m_nMilliSec;
	m_cTimeSpan = other.m_cTimeSpan;
	return *this;
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::operator+
	Description :	Adds two CSchedTimeSpanObjects together.
	Return :		
		CSchedTimeSpan		-	The result.
	Parameters :	
		CSchedTimeSpan &other	-	Object to add.
	Note :			None.
\*======================================================================*/
CSchedTimeSpan CSchedTimeSpan::operator+(CSchedTimeSpan &other)
{
	int nMilliSec = m_nMilliSec + other.m_nMilliSec;
	if (nMilliSec >999)
	{
		CTimeSpan cOneSec = CTimeSpan(0,0,0,1);
		nMilliSec -= 1000;
		return (CSchedTimeSpan ((m_cTimeSpan + other.m_cTimeSpan + cOneSec), nMilliSec));
	}
	else
		return (CSchedTimeSpan ((m_cTimeSpan + other.m_cTimeSpan), m_nMilliSec));
}

	/*======================================================================*\
		Author :Don Sanders			Date : 12/14/00			version 1.0
	  ======================================================================
		Function :		CSchedTimeSpan::operator-
		Description :	subtarction operator.
		Return :		
			CSchedTimeSpan	
		Parameters :	
			CSchedTimeSpan &other	-	object to be subtracted 
		Note :			
	\*======================================================================*/
	CSchedTimeSpan CSchedTimeSpan::operator-(const CSchedTimeSpan &other)
	{
		int nMilliSec = m_nMilliSec - other.m_nMilliSec;
		if (nMilliSec < 0)
		{
			CTimeSpan cOneSec = CTimeSpan(0,0,0,1);
			nMilliSec += 1000;
			return (CSchedTimeSpan ((m_cTimeSpan - other.m_cTimeSpan - cOneSec), nMilliSec));
		}
		else
			return (CSchedTimeSpan ((m_cTimeSpan - other.m_cTimeSpan), m_nMilliSec));
	}

	/*======================================================================*\
		Author:	Frederick J. Shaw	Date : 07-26-01			version 1.0
	======================================================================
	Function :		CSchedTimeSpan::operator+
	Description :	addition operator.
	Return :		
		CSchedTimeSpan.
	Parameters :	
		const int nSecs	-	addition operator.
	Note :			
	\*======================================================================*/
	CSchedTimeSpan CSchedTimeSpan::operator+(const int nSecs){
		CTimeSpan cSecs2Add = CTimeSpan(0,0,0,nSecs);
		return (CSchedTimeSpan ((m_cTimeSpan + cSecs2Add), m_nMilliSec));
	}

	/*======================================================================*\
		Author:	Frederick J. Shaw	Date : 07-26-01		version 1.0
	======================================================================
	Function :		CSchedTimeSpan::operator+
	Description :	Less than comparison operator.
	Return :		
		CSchedTimeSpan.
	Parameters :	
		const float fSecs	-	addition operator.
	Note :			
	\*======================================================================*/
	CSchedTimeSpan CSchedTimeSpan::operator+(const float fSecs){
	
		double dSec = 0.0;
		double dMilliSec  = 0.0;
		int nMilliSec = 0;
		int nSec = 0;

		dMilliSec = modf(fSecs, &dSec);
		nMilliSec = (int)(dMilliSec * 1000);
		nSec = (int)dSec;

		if (nMilliSec >999)
		{
			nSec += 1;
			nMilliSec -= 1000;
		}

		m_nMilliSec += nMilliSec;
		CTimeSpan cSecs2Add = CTimeSpan(0, 0, 0, nSec);
		return (CSchedTimeSpan (m_cTimeSpan + cSecs2Add, m_nMilliSec));
	}

	/*======================================================================*\
		Author :Don Sanders			Date : 12/14/00			version 1.0
	======================================================================
	Function :		CSchedTimeSpan::operator<
	Description :	Less than comparison operator.
	Return :		
		BOOL	-	TRUE if this is less than other, otherwise FALSE.
	Parameters :	
		CSchedTimeSpan &other	-	comparison operator
	Note :			
	\*======================================================================*/
BOOL CSchedTimeSpan::operator<(CSchedTimeSpan &other)
{
	if (m_cTimeSpan == other.m_cTimeSpan)
		return (m_nMilliSec < other.m_nMilliSec);
	else
		return (m_cTimeSpan < other.m_cTimeSpan);
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::operator<=
	Description :	Less than or equal comparison operator.
	Return :		
		BOOL	-	TRUE if this is less or equal to other, otherwise FALSE.
	Parameters :	
		CSchedTimeSpan &other	-	comparison operator
	Note :			
\*======================================================================*/
BOOL CSchedTimeSpan::operator<=(CSchedTimeSpan &other)
{
	if (m_cTimeSpan == other.m_cTimeSpan)
		return (m_nMilliSec <= other.m_nMilliSec);
	else
		return (m_cTimeSpan <= other.m_cTimeSpan);
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::operator>
	Description :	Greater than comparison operator.
	Return :		
		BOOL	-	TRUE if this is greater than other, otherwise FALSE.
	Parameters :	
		CSchedTimeSpan &other	-	comparison operator
	Note :			
\*======================================================================*/
BOOL CSchedTimeSpan::operator>(CSchedTimeSpan &other)
{
	if (m_cTimeSpan == other.m_cTimeSpan)
		return (m_nMilliSec > other.m_nMilliSec);
	else
		return (m_cTimeSpan > other.m_cTimeSpan);
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::operator>=
	Description :	Greater than or equal comparison operator.
	Return :		
		BOOL	-	TRUE if this is greater than or equal to other,
					otherwise FALSE.
	Parameters :	
		CSchedTimeSpan &other	-	comparison operator
	Note :			
\*======================================================================*/
BOOL CSchedTimeSpan::operator>=(CSchedTimeSpan &other)
{
	if (m_cTimeSpan == other.m_cTimeSpan)
		return (m_nMilliSec >= other.m_nMilliSec);
	else
		return (m_cTimeSpan >= other.m_cTimeSpan);
}

/*======================================================================*\
	Author :Don Sanders			Date : 12/14/00			version 1.0
  ======================================================================
	Function :		CSchedTimeSpan::operator==
	Description :	Equality comparison operator
	Return :		
		BOOL	-	TRUE the two objects are equal in value.
	Parameters :	
		CSchedTimeSpan &other	-	comparison operator
	Note :			
\*======================================================================*/
BOOL CSchedTimeSpan::operator==(CSchedTimeSpan &other)
{
	if (m_cTimeSpan == other.m_cTimeSpan)
		return (m_nMilliSec == other.m_nMilliSec);
	else
		return (FALSE);
}


/*======================================================================*\
	Author :Frederick J. Shaw		Date : 0/05/2001		version 1.0
  ======================================================================
	Function :	GetSchedTimeFromNoonInSecs()
	Description : Returns the number of seconds from noon.  
	              This is the time the ACE uses.
	Return : seconds as a double.		
		
	Parameters :	
		none
	Note :			
\*======================================================================*/

double CSchedTimeSpan::GetSchedTimeFromNoonInSecs(void){
	
	double dTotalSecs = 0.0;

	CString strTime = m_cTimeSpan.Format(_T("%H:%M:%S"));
		
	// Determine if it is AM or PM
	
    if (( m_cTimeSpan.GetHours() < 12) && (m_cTimeSpan.GetHours() >= 0)){
        // Time is am.  Need to add 43200 seconds to value
        dTotalSecs = 43200.000; // Number of seconds in 12 hours
//		dTotalSecs += (double)m_cTimeSpan.GetTotalHours() * 3600.0;
//		dTotalSecs += (double)m_cTimeSpan.GetTotalMinutes() * 60.0;
		dTotalSecs += (double)m_nMilliSec/1000;
		dTotalSecs += (double)m_cTimeSpan.GetTotalSeconds();
		
    } else if ( m_cTimeSpan.GetHours() >= 12) {
        // Time is pm.  Need to subtract 43200 seconds to value
        dTotalSecs = -43200.000; // Number of seconds in 12 hours
//		dTotalSecs += (double)m_cTimeSpan.GetTotalHours() * 3600.0;
//		dTotalSecs += (double)m_cTimeSpan.GetTotalMinutes() * 60.0;
		dTotalSecs += (double)m_nMilliSec/1000;
		dTotalSecs += (double)m_cTimeSpan.GetTotalSeconds();
	}
  
    return dTotalSecs;
};

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 0/05/2001		version 1.0
  ======================================================================
	Function :	GetSchedTimeInSecs()
	Description : Returns the number of seconds from midnight.  
	              
	Return : seconds as a double.		
		
	Parameters :	
		none
	Note :			
\*======================================================================*/

double  CSchedTimeSpan::GetSchedTimeInSeconds(void){

	double dTotalSecs = 0.0;
	CString strTime = m_cTimeSpan.Format(_T("%H:%M:%S"));
//	dTotalSecs += (double)m_cTimeSpan.GetTotalHours() * 3600.0;
//	dTotalSecs += (double)m_cTimeSpan.GetTotalMinutes() * 60;
	dTotalSecs += (double)m_cTimeSpan.GetTotalSeconds();
	dTotalSecs += (double)m_nMilliSec/1000;

	return dTotalSecs;
};

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 06/06/2001		version 1.0
  ======================================================================
	Function :	CSchedTimeSpan::GetSchedTimeInMinutes()

	Description :  Calculates the number of minutes from midnight.
	              
	Return :	  double		
		
	Parameters : 	none
	
    Revision history:	Frederick J. Shaw Sept 2, 1999
                        Initial release.
	Note :			
\*======================================================================*/
double CSchedTimeSpan::GetSchedTimeInMinutes(void){

	double dTotalMinutes = 0.0;

	CString strTime = m_cTimeSpan.Format(_T("%H:%M:%S"));
	dTotalMinutes += (double)m_cTimeSpan.GetHours() * 60.0;
	dTotalMinutes += (double)m_cTimeSpan.GetMinutes();
	dTotalMinutes += (double)m_cTimeSpan.GetSeconds()/60.0;
	dTotalMinutes += (double)m_nMilliSec/60000;
		
	return dTotalMinutes;
};

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 02/27/2002		version 1.0
  ======================================================================
	Function :	CSchedTimeSpan::Parser()

	Description :  
	              
	Return :	  	
		
	Parameters : 	
	
    Revision history:	Frederick J. Shaw Febuary 27,2002
    Initial release.
	Note :			
\*======================================================================*/
BOOL CSchedTimeSpan::Parser(CString strText, CSchedTimeSpan* pSchedTimeSpan, CString* pstrErrorText){

	BOOL bReturnStatus = TRUE;
	CString strErrorText = _T("");

	// Remove quotes if present.
	strText.Replace(_T("\""), _T(""));
	strText.Replace(_T("\""), _T(""));
	strText.TrimLeft();
	strText.TrimRight();

	int nPos     = 0;
	int nDay     = 0;
	int nHour    = 0;
	int nMin     = 0;
	int nSec     = 0;
	int nMilliSec = 0;

	if (-1 != (nPos = strText.Find(_T('-')))){
		// At least one dash is found
		CString strTemp01;
		strTemp01 = strText.Left(nPos);
		// Extract up to the dash
		nDay = _ttoi(strTemp01);
		strText.Delete(0, nPos + 1);
	} 

	if (-1 != (nPos = strText.Find(_T(':')))){
		// At least one colon is found
		CString strTemp01;
		strTemp01 = strText.Left(nPos);
		strText.Delete(0, nPos + 1);

		if (-1 != (nPos = strText.Find(_T(':')))){
			// A second colon is found
			CString strTemp02;
			strTemp02 = strText.Left(nPos);
			// Extract up to the colon
			nMin = _ttoi(strTemp02);
			nHour   = _ttoi(strTemp01);
			strText.Delete(0, nPos + 1);
		} else {
			// Extract up to the colon
			nMin = _ttoi(strTemp01);
		}
	} 

	if (-1 != (nPos = strText.Find(_T('.')))){
		// Decimal found.
		// Need to round out milisecs or round off
		// greater than 999.
		CString strTemp01, strMSec;
		strTemp01 = strText.Left(nPos);
		strText.Delete(0, nPos + 1);
		if (strText.GetLength() >= 3) {
			strMSec = strText.Right(3);
		} else if (strText.GetLength() == 2){
			strMSec = strText + _T('0');			
		} else if (strText.GetLength() == 1){
			strMSec = strText + _T("00");
		}

		nSec      = _ttoi(strTemp01);
		nMilliSec = (_ttoi(strMSec));
	} else {
		int nTemp = 0;
		// No decimal present
		// Verify that string contains alphanumeric characters.
		if ((!strText.IsEmpty()) && (0 == (nTemp = isdigit(strText[0])))){
//			int nTemp = _istdigit(strText[0]);
			bReturnStatus = FALSE;
			// strErrorText.Format(_T("Invalid format for time string! (%s)"), strText);
			strErrorText.Format(_T("Invalid format for time string! <%s>, Proper format HH:MM:SS"), strText);

		} else if (!strText.IsEmpty()){
			nSec = _ttoi(strText);
		}
	}
	
	// Make sure that the day field contain valid characters
	if ((nDay < 0) || (nDay > 366)){
		bReturnStatus = FALSE;
		// strErrorText.Format(_T("Invalid day <%d>"), nDay);
		strErrorText.Format(_T("Invalid day <%d>, must be between 0 and 366. Proper format DDD-HH:MM:SS"), nDay);
	}
	// Make sure that the Hour field contain valid characters
	if (bReturnStatus == TRUE){
		if ((nHour < 0) || (nHour > 23)){
			bReturnStatus = FALSE;
			// strErrorText.Format(_T("Invalid hour <%d>"), nHour);
			strErrorText.Format(_T("Invalid hours <%d>, must be between 0 and 23. Proper format HH:MM:SS"), nHour);
		}
	}
	// Make sure that the minute field contain valid characters
	if (bReturnStatus == TRUE){
		if ((nMin < 0) || (nMin > 59)){
			bReturnStatus = FALSE;
			// strErrorText.Format(_T("Invalid minute <%d>"), nMin);
			strErrorText.Format(_T("Invalid minutes <%d>, must be between 0 and 59. Proper format HH:MM:SS"), nMin);
		}
	}
	// Make sure that the second field contain valid characters
	if (bReturnStatus == TRUE)
	{
		if ((nSec < 0) || (nSec > 59)){
			bReturnStatus = FALSE;
		//	strErrorText.Format(_T("Invalid second <%d>"), nSec);
			strErrorText.Format(_T("Invalid seconds <%d>, must be between 0 and 59. Proper format HH:MM:SS"), nSec);
		}
	}
	// Make sure that the millsec field contain valid characters
	/*
	if (bReturnStatus == TRUE)
	{
		if ((nMilliSec < 0) || (nMilliSec > 999))
		{
			bReturnStatus = FALSE;
			strErrorText.Format(_T("Invalid milli second <%d>"), nMilliSec);	
		}
	} */

	CSchedTimeSpan	cSchedTimeSpan(nDay, nHour, nMin, nSec, nMilliSec);

	if (pSchedTimeSpan)
		*pSchedTimeSpan = cSchedTimeSpan;


	if (pstrErrorText)
		*pstrErrorText = strErrorText;
  

	return bReturnStatus;
};
  