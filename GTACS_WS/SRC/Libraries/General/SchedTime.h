// SchedTime.h: interface for the CSchedTime class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCHEDTIME_H__6B3D5625_D035_11D4_800C_00609704053C__INCLUDED_)
#define AFX_SCHEDTIME_H__6B3D5625_D035_11D4_800C_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSchedTimeSpan;

class CSchedTime
{
public:
	CSchedTime();
	virtual ~CSchedTime();
	CSchedTime(CTime cTime, int nMilliSec);
	CSchedTime(int nYear, int nGMTDay, int nHour, int nMin, int nSec, int nMilliSec);
	CSchedTime(CString strText);
	static BOOL GMTDayToMonthDay(int nGMTDay, int nYear, int* pMonth, int* pDay,
								 CString* pstrErrorText=NULL);
	BOOL	Create(int nYear, int nGMTDay, int nHour, int nMin, int nSec, int nMilliSec);
	BOOL	Create(CString strText);
	CString	GetLastErrorText(){return m_strErrorText;};
	CString	GetTimeString(BOOL bShowMilliSec);
	CSchedTime operator=(const CSchedTime &other);
	CSchedTime operator+(const CSchedTimeSpan &other);
	CSchedTime operator+(const int nSecs);
	CSchedTime operator+(const float fSecs);
	CSchedTime operator-(const CSchedTimeSpan &other);
	CSchedTimeSpan operator-(const CSchedTime &other);
//	CSchedTime operator+=(const CSchedTimeSpan &other);
//	CSchedTime operator-=(const CSchedTimeSpan &other);

	BOOL operator<(CSchedTime &other);
	BOOL operator<=(CSchedTime &other);
	BOOL operator>(CSchedTime &other);
	BOOL operator>=(CSchedTime &other);
	BOOL operator==(CSchedTime &other);
	static BOOL	IsValid(int nYear, int nGMTDay, int nHour, int nMin, int nSec, int nMilliSec, 
						CSchedTime* pSchedTime=NULL, CString* pstrErrorText=NULL);
	static BOOL	IsValid(CString strText, CSchedTime* pSchedTime=NULL, CString* pstrErrorText=NULL);
    double GetSchedTimeFromNoonInSecs(void);
	double GetSchedTimeInSeconds(void);
	double GetSchedTimeInMinutes(void);
	static CString GetCurrSchedTime(void);
	bool   CreateSTOLTimeTag(CString & strTimeTag);
	BOOL   Parser(CString strText, CSchedTime* pSchedTime=NULL, CString* pstrErrorText=NULL);

protected:
	CString	m_strErrorText;
	CTime	m_cTime;
	int		m_nMilliSec;
};

class CSchedTimeSpan
{
	friend CSchedTime;
public:
	CSchedTimeSpan();
	CSchedTimeSpan(CTimeSpan cTimeSpan, int nMilliSec);
	CSchedTimeSpan(int nDay, int nHour, int nMin, int nSec, int nMilliSec);
	virtual ~CSchedTimeSpan();
	BOOL	Create(int nDay, int nHour, int nMin, int nSec, int nMilliSec);
	BOOL	Create(CString strText);
	CString	GetLastErrorText(){return m_strErrorText;};
	CString	GetTimeSpanString(BOOL bShowDay, BOOL bShowMilliSec);
	CSchedTimeSpan operator=(const CSchedTimeSpan &other);
	CSchedTimeSpan operator+(CSchedTimeSpan &other);
	CSchedTimeSpan operator-(const CSchedTimeSpan &other);
	CSchedTimeSpan operator+(const int nSecs);
	CSchedTimeSpan operator+(const float fSecs);
//	CSchedTimeSpan operator+=(const CSchedTimeSpan &other);
//	CSchedTimeSpan operator-=(const CSchedTimeSpan &other);
	BOOL operator<(CSchedTimeSpan &other);
	BOOL operator<=(CSchedTimeSpan &other);
	BOOL operator>(CSchedTimeSpan &other);
	BOOL operator>=(CSchedTimeSpan &other);
	BOOL operator==(CSchedTimeSpan &other);
	static BOOL	IsValid(int nDay, int nHour, int nMin, int nSec, int nMilliSec, 
						CSchedTimeSpan* pSchedTimeSpan=NULL, CString* pstrErrorText=NULL);
	static BOOL	IsValid(CString strText, CSchedTimeSpan* pSchedTimeSpan=NULL, CString* pstrErrorText=NULL);
    double GetSchedTimeFromNoonInSecs(void);
	double GetSchedTimeInSeconds(void);
	double GetSchedTimeInMinutes(void);
	BOOL   Parser(CString strText, CSchedTimeSpan* pSchedTimeSpan=NULL, CString* pstrErrorText=NULL);


protected:
	CString		m_strErrorText;
	CTimeSpan	m_cTimeSpan;
	int			m_nMilliSec;
};

#endif // !defined(AFX_SCHEDTIME_H__6B3D5625_D035_11D4_800C_00609704053C__INCLUDED_)
