//////////////////////////////////////////////////////////////////////
// @(#)
//////////////////////////////////////////////////////////////////////
// SearchConstants: include file for global constants,
//
// Header: SeacrchConstants.h
//
// Routine description:	
//   Global string search constants.
//
// Revision history:
//
// Frederick Shaw 1999
// Initial release.
// 	
// Fixed Issues #76 & 77
// Dec 9, 1999
// Added cstr$OBJID.
// 

#if !defined(SEARCHCONSTANTS_H__26444908_90FF_11D2_98EB_00104B79D0AC__INCLUDED_)
#define SEARCHCONSTANTS_H__26444908_90FF_11D2_98EB_00104B79D0AC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

const TCHAR ctcEPOCHPROCS[] =  _T("EPOCH_PROCEDURES");
const TCHAR ctcEPOCHREPORTS[] = _T("EPOCH_REPORTS");

const TCHAR ctcDBASEDrive[] = _T("R:");
const TCHAR ctcProcDrive[]  =  _T("S:");
const TCHAR ctcMountPoint[] = _T("EPOCH_DATABASE");
const TCHAR ctcProcsDir[]   = _T("procs");

const TCHAR    ctchCOMMENT		  = _T('#');
const TCHAR    ctstrWRITE[]       = _T("WRITE");

// Prolog directives
const TCHAR    ctstrOVERRIDE[]    = _T("##override");

// Instrument objects
// S/C Commanding string constants

const TCHAR  ctstrCMDWAITDOY[]			= _T("ACE_STO_SKB_SUSPEND_BFR_DAY_03");
const TCHAR  ctstrCMDWAITTOD[]			= _T("ACE_STO_SKB_SUSPEND_BFR_TIME_04");
const TCHAR  ctstrCMDPAUSE[]			= _T("ACE_STO_SKB_CMD_BUFFER_PAUSE_02");
const TCHAR  ctstrSCANEXE[]				= _T("ACE_INSTR_FRAME_EXECUTE_C9");
const TCHAR  ctstrSTAREXE[]				= _T("ACE_INSTR_STAR_EXECUTE_C8");
// const TCHAR  ctstrACTIVE_RT[]		= _T("local active_rt");

// S/C commanding keywords

const TCHAR ctstrIMG_SCAN_MODEEQ[]		= _T("IMG_SCAN_MODE=");
const TCHAR ctstrSELECTED_INSTREQ[]	    = _T("INSTRUMENT=");
const TCHAR ctstrPAUSE_TIMEEQ[]		    = _T("PAUSE_TIME=");
const TCHAR ctstrDAYEQ[]				= _T("DAY=");
const TCHAR ctstrTIMEEQ[]				= _T("SUSPEND_TIME=");
const TCHAR ctstrFIRST_STAR_IDXEQ[]	    = _T("FIRST_STAR_INDEX=");
const TCHAR ctstrLOAD_REGEQ[]			= _T("LOAD_REG=");
const TCHAR ctstrINSTRUMENTEQ[]		    = _T("INSTRUMENT=");
const TCHAR ctstrFRAME_PRIORITYEQ[]	    = _T("FRAME_PRIORITY=");
const TCHAR ctstrSND_BBCAL_SUPPRESSEQ[] = _T("SND_BBCAL_SUPPRESS=");							  
const TCHAR ctstrREPSEQ[]				= _T("REPS=");	
const TCHAR ctstrSND_DWELL_MODEEQ[]	    = _T("SND_DWELL_MODE=");
const TCHAR ctstrSPACE_LOOK_REQEQ[]	    = _T("SPACE_LOOK_REQ=");	                 
const TCHAR ctstrFRAME_IDXEQ[]			= _T("FRAME_INDEX=");  
const TCHAR ctstrON[]                   = _T("ON");
const TCHAR ctstrOFF[]                  = _T("OFF");
const TCHAR ctstrSUN[]					= _T("SUN");
const TCHAR ctstrMOON[]				    = _T("MOON");
const TCHAR ctstrSCHEDX[]			    = _T("SCHEDX");
const TCHAR ctstrSCHEDMON[]             = _T("MONSCHED");
const TCHAR ctstrNEXTSCHED[]            = _T("NEXTSCHED");
const TCHAR ctstrSET[]                  = _T("SET");
const TCHAR ctstrGO[]                   = _T("GO");
const TCHAR ctstrSCHEDX_BIAS[]          = _T("SCHEDX_BIAS");
const TCHAR ctstrPRC_STRT[]             = _T("PRC_STRT");
const TCHAR ctstrLOCAL[]				= _T("LOCAL");
const TCHAR ctstrARGS[]                 = _T("ARGS(");
const TCHAR ctstrNARGS[]                 = _T("NARGS(");
const TCHAR ctstrSCHED[]				= _T("SCHEDULE");

// Load gen string constants
const TCHAR ctstrGENLOAD[]				= _T("GENLOAD");
const TCHAR ctstrSCID[] 				= _T(" SCID=");
const TCHAR ctstrDBASE[]				= _T(" DBASE=");
const TCHAR ctstrRT[]					= _T(" RT=");
const TCHAR ctstrCTCU[] 				= _T(" CTCU=");
const TCHAR ctstrTYPE[]					= _T(" TYPE=");
const TCHAR ctstrMAXEQ[]				= _T(" MAX=");
const TCHAR ctstrSEGSIZE[]				= _T(" SEGSIZE=");
const TCHAR ctstrFILE[]					= _T(" FILE=");
const TCHAR ctstrSTART_RTCSEQ[]			= _T(" RTCS_START=");
const TCHAR ctstrRAM[]					= _T("RAM");
const TCHAR ctstrSTO[]					= _T("STO");
const TCHAR ctstrSKB[]					= _T("SKB");
const TCHAR ctstrIMC[]					= _T("IMC");
const TCHAR ctstr$QBACE_SEL[]			= _T("$QBACE_SEL");
const TCHAR ctstr$QBCTCU_SEL[]			= _T("$QBCTCU_SEL");
const TCHAR ctstrHEXCMD[]				= _T("HEXCMD");
const TCHAR ctstrCMDALLOW[]				= _T("##CMDALLOW");

// Instrument object Directives and keywords
const CString ctstrSTAR					(_T("STAR"));
const CString ctstrSCAN					(_T("SCAN"));
const TCHAR  ctstrFRAME[]				= _T("FRAME");
const TCHAR  ctstrTIME[]				= _T("TIME");
const TCHAR  ctstrGROUND[]				= _T("##GROUND");
const TCHAR  ctstrBEGIN[]				= _T("BEGIN");
const TCHAR  ctstrEND[]					= _T("END");
const TCHAR  ctstrNOW[]					= _T("NOW");
const TCHAR  ctstrDUR[]					= _T("DURATION");
const TCHAR  ctstrDWELL[]				= _T("DWELL");
const TCHAR  ctstrLAST_STAR_CLASS[]		= _T("LAST_STAR_CLASS");
const TCHAR  ctstrLOOKNUM[]				= _T("LOOKNUM");
const TCHAR  ctstrMAX[]					= _T("MAX");
const TCHAR  ctstrMODE[]				= _T("MODE");
const TCHAR  ctstrMODE_0[]				= _T("MODE_0");
const TCHAR  ctstrMODE_1[]				= _T("MODE_1");
const TCHAR  ctstrNUMLOOKS[]			= _T("NUMLOOKS");
const TCHAR  ctstrOBJID[]				= _T("OBJID");
const TCHAR  ctstrPRIORITY[]			= _T("PRIORITY");
const TCHAR  ctstrNORMAL[]				= _T("NORMAL");
const TCHAR  ctstrPRIORITY_1[]			= _T("PRIORITY_1");
const TCHAR  ctstrPRIORITY_2[]			= _T("PRIORITY_2");
const TCHAR  ctstrSINGLE_0_1[]			= _T("SINGLE_NS_0_1");
const TCHAR  ctstrSINGLE_0_2[]			= _T("SINGLE_NS_0_2");
const TCHAR  ctstrSINGLE_0_4[]			= _T("SINGLE_NS_0_4");
const TCHAR  ctstrDOUBLE_0_1[]			= _T("DOUBLE_NS_0_1");
const TCHAR  ctstrEXECUTE[]				= _T("EXECUTE"); 
const TCHAR  ctstrLOAD_AND_EXEC[]		= _T("LOAD_AND_EXEC");
const TCHAR  ctstrLOAD_REG_ONLY[]		= _T("LOAD_REG_ONLY");
const TCHAR  ctstrREPEAT[]				= _T("REPEAT");
const TCHAR  ctstrSC[]					= _T("SCID");
const TCHAR  ctstrSTARID[]				= _T("STARID");
const TCHAR  ctstrSCANDATA[]			= _T("SCANDATA");
const TCHAR  ctstrSCANDIR[]				= _T("SCANDATA");

const TCHAR ctstrSTARDATA[]				= _T("STARDATA");
const TCHAR ctstrSTARDIR[]				= _T("STARDATA");
const TCHAR ctstrSTARINFO[]				= _T("STARINFO");
const TCHAR ctstrINSTRUMENT[]			= _T("INSTRUMENT");
const TCHAR ctstrIMGR[]					= _T("IMAGER");
const TCHAR ctstrSDR[]					= _T("SOUNDER");
const TCHAR ctstrBBCAL[]				= _T("BBCAL");
const TCHAR ctstrDO_NOT_SUPP[]			= _T("DO_NOT_SUPPRESS");
const TCHAR ctstrSUPP[]					= _T("SUPPRESS_BBCAL");
const TCHAR ctstrSIDE[]					= _T("SIDE");
const TCHAR ctstrEAST[]					= _T("EAST");
const TCHAR ctstrWEST[]					= _T("WEST");
const TCHAR ctstrOATS[]					= _T("OATS");
const TCHAR ctstrPLUS_X[]				= _T("PLUS_X");
const TCHAR ctstrMINUS_X[]				= _T("MINUS_X");
const TCHAR ctstrLOAD_REG[]				= _T("LOAD_REG");
const TCHAR ctstrSENSE[]				= _T("SENSE");
const TCHAR ctstrSEQUENCE[]				= _T("SEQUENCE");
const TCHAR ctstrPWVALUE[]				= _T("PW_160");
const TCHAR ctstrRTCSDATA[]				= _T("RTCSDATA");
const TCHAR ctstrADDRESS[]				= _T("ADDRESS");
const TCHAR ctstrNO_OP[]				= _T("ACE_NO_OP_FF");
const TCHAR ctstrMovTopPtr[]			= _T("ACE_STO_MOVE_TOP_08");
const TCHAR ctstrStoTopIdx[]			= _T("STO_TOP_INDEX=");
const TCHAR ctstrFastSpace[]			= _T("Fast Space");
const TCHAR ctstrScanClamp[]			= _T("Scan Clamp");
const TCHAR ctstrSlowSpace[]			= _T("Slow Space");

// Scheduling specific directives and keywords

const TCHAR ctstrWAIT[]					= _T("WAIT");
const TCHAR ctstrCMD[]					= _T("CMD");
const TCHAR ctstrCLS[]					= _T("CLS");
const TCHAR ctstrERRORMSG[]				= _T("##error ");
const TCHAR ctstrNEXT[]					= _T("NEXT");
const TCHAR ctstrSTARTEQ[]				= _T("START=");
const TCHAR ctstrSTOPEQ[]				= _T("STOP=");
const TCHAR ctstrSTART[]				= _T("START ");
const TCHAR ctstrSTOP[]					= _T("STOP ");
const TCHAR ctstrWAITAT[]				= _T("WAIT@");
const TCHAR ctstrZeroTimeTag[]			= _T("001-00:00:00.000");
const TCHAR ctstrPREHSEKPG[]			= _T("PreHseKpg");
const TCHAR ctstrHSEKPG[]				= _T("HseKpg");
const TCHAR ctstrACTIVITY[]				= _T("##ACTIVITY");
const TCHAR ctstrPROCSTART[]			= _T("PROC START ");
const TCHAR ctstrExecTime[]				= _T("##exectime");
const TCHAR ctstrPrlgEnd[]				= _T("##prolog end");
const TCHAR ctstrPrlgBeg[]				= _T("##prolog begin");
const TCHAR ctstrValid[]				= _T("##valid");
const TCHAR ctstrParam[]				= _T("##param");
const TCHAR ctstrDesc[]					= _T("##desc");
const TCHAR ctstrUpdate[]				= _T("##update");
const TCHAR ctstrWARNINGMSG[]			= _T("##warning ");
const TCHAR ctstrGround[]				= _T("ground");
const TCHAR ctstrOnBoard[]				= _T("onboard");
const TCHAR ctstrRTCS[]					= _T("RTCS ");
const TCHAR ctstrBoth[]					= _T("both");
const TCHAR ctstrPassed[]				= _T("passed");
const TCHAR ctstrWarning[]				= _T("warning");
const TCHAR ctstrFail[]					= _T("fail");
const TCHAR ctstrUnvalidated[]			= _T("unvalidated");
const TCHAR ctstrInclude[]				= _T("include");
const TCHAR ctstrLabelEq[]				= _T("label=");
const TCHAR ctstrStartTime[]			= _T("##starttime");
const TCHAR ctstrStopTime[]				= _T("##stoptime");


const int ctnCPTTLength					= 13;
const int ctnCMDTTLength				= 22;
const int ctnDOYLength					= 8;
const int ctnNumValOpts					= 5;
const int ctnNumScanParams				= 4;
const int ctnNumStarParams				= 3;
const int ctnMaxStarId					= 9999;
const int ctnMaxImgDwellRepeats			= 63;
const int ctnMaxSndDwellRepeats			= 15;
const int ctnMaxNSCycles				= 9;
const int ctnMaxEWCycles				= 7;
const int ctnMaxImgIncrements			= 6136;
const int ctnMaxSndIncrements			= 2805;
const int ctnMaxScanFrequency			= 5;
const int ctnMaxOutputStatus			= 13;
const int ctnMaxCritialLimit			= 5;
const int ctnMaxCmdScanEWCyc			= 5;
const int ctnMaxCmdScanNSCyc			= 9;
const int ctnMaxRepeatCntImg			= 64;
const int ctnMaxRepeatCntSnd			= 16;
const double ctnMinFrameDuration		= 0.0;
const double ctnMinImcCalOffset			= -0.008;
const double ctnMaxImcCalOffset			= 0.008;
const double ctnMaxStarDwell			= 64.0;
const double ctdDegrees2Cycles          = 2.8125;
const double ctdIncr2CyclesImgr         = 6136.0;
const double ctdIncr2CyclesSdr			= 2805.0;

// These constants were removed due to rounding errors
// Instead the full calculation is done in the validation procedure 
// N/S imager: 2.8125 degrees/cycle / 6136 incr/cycle
// const double ctdDegs2IncrNSImgr         = 4.584e-4;
// const double ctdDegs2IncrNSImgr		    = 4.5836049543676662320730117340287e-4;

// N/S sounder: 2.8125 degrees/cycle / 2805 incr/cycle
// const double ctdDegs2IncrNSSdr          = 1.0026737967914438502673796791444e-3;

// E/W imager: 2 * 2.8125 degrees/cycle / 6136 incr/cycle
// const double ctdDegs2IncrEWImgr         = 9.1672099087353324641460234680574e-4;

// E/W sounder: 2 * 2.8125 degrees/cycle / 2805 incr/cycle
// const double ctdDegs2IncrEWSdr          = 15778.125;

// Engineering contraint constants
// const double ctdScanDegsEWNadirImgr     = 10.4;
// const double ctdScanDegsNSNadirImgr     = 10.5;
// const double ctdScanDegsEWNadirSdr      = 10.4;
// const double ctdScanDegsNSNadirSdr      = 10.5;

// const double ctdStarDegsEWNadirImgr     = 11.5;
// const double ctdStarDegsNSNadirImgr     = 10.5;
// const double ctdStarDegsEWNadirSdr      = 11.5;
// const double ctdStarDegsNSNadirSdr      = 10.5;

// Instrument center points.
// 6136 * 4.5 Imager N/S
// 6136 * 2.5 Imager E/W
// 2805 * 4.5 Sdr    N/S
// 2805 * 2.5 Sdr    E/W
// const double ctdInstrCenterPtImgrNS       = 27612.0;
// const double ctdInstrCenterPtImgrEW       = 15340.0;
// const double ctdInstrCenterPtSdrNS        = 12622.0;
// const double ctdInstrCenterPtSdrEW        = 7012.0;

// Maximum size of the sensor intrusion array
// Change this value if the maximum number entries increases.
// const int ctnMaxSensIntrNum = 20;

const CString ctstrEPOCHPROCS(_T("EPOCH_PROCEDURES"));
const CString ctstrEPOCHREPORTS(_T("EPOCH_REPORTS"));

#endif // !defined(SEARCHCONSTANTS_H__26444908_90FF_11D2_98EB_00104B79D0AC__INCLUDED_)


