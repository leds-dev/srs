// TextFile.cpp: implementation of the CTextFile class.
//
// PR000507 - MSD - Modified the way ##desc are parsed in prolog for
//					CP validation.
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "TextFile.h"
#include "SearchConstants.h"
#include "SchedTime.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// The CTextFile class works by reading the contents of a text file into a CStringArray
// when the file is opened.  The CStringArray can then be modified as appropriate and is
// written back out when the file is closed.

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::CTextFile
//	Description :	The CTextFile class works by reading the contents of a
//					text file into a CStringArray when the file is opened.
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			Create the class and mark the file as being invalid.
//////////////////////////////////////////////////////////////////////////
CTextFile::CTextFile()
{
	m_eOpenMode        = CTextFile::INV;
	m_bPrologExtracted = FALSE;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 04/19/2001			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::CTextFile(const CTextFile &Other)
//	Description :	Copy constructor
//					
//	Return :		constructor		
//	
//	Parameters :	const CTextFile & Other
//
//	Notes :			Create the class and mark the file as being invalid.
//////////////////////////////////////////////////////////////////////////

CTextFile::CTextFile(const CTextFile & Other)
{
	m_cProlog     = Other.m_cProlog;
	m_eOpenMode   = Other.m_eOpenMode;
	m_hFile       = Other.m_hFile;
	m_pStream     = Other.m_pStream;
	m_strFileName = Other.m_strFileName;
	m_strFileText.Copy(Other.m_strFileText);
	m_bPrologExtracted = Other.m_bPrologExtracted;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 04/19/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::CTextFile(const CProlog & Other)
//	Description :	Equal operator
//					Copies a prolog from one text file to another. 
//	Return :		constructor		-	
//
//	Parameters :	const CProlog & Other
//
//	Notes :			
//////////////////////////////////////////////////////////////////////////
CTextFile  &    CTextFile::operator=(const CProlog & Other)
{
	m_cProlog  = Other;
	return *this;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 04/19/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::CTextFile(const CTextFile &Other)
//
//	Description :	Equal operator
//					
//	Returns :		nothing			
//			
//	Parameters :	const CTextFile &Other
//
//	Notes :			
//////////////////////////////////////////////////////////////////////////
CTextFile  & CTextFile::operator=(const CTextFile & Other)
{
	if (this == &Other)  // object assigned to itself
		return *this; // Nothing to do.
	m_cProlog     = Other.m_cProlog;
	m_eOpenMode   = Other.m_eOpenMode;
	m_hFile       = Other.m_hFile;
	m_pStream     = Other.m_pStream;
	m_strFileName = Other.m_strFileName;
	m_strFileText.Copy(Other.m_strFileText);
	return *this;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::~CTextFile
//
//	Description :	Destructor
//
//	Returns :		nothing		
//	
//	Parameters :	none
//
//	Notes :			
//////////////////////////////////////////////////////////////////////////
CTextFile::~CTextFile()
{}

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::Load
//	Description :	Open the file and read in its contents.
//	Return :		
//		BOOL		-	TRUE if the file opened OK.
//						FALSE if there is an error.  See pException for
//						more detailed information
//	Parameters :	
//		CString strFileName			- Name of the file to open	
//		eOpenFT eOpenMode			- Specify one of the following:
//										CR - Create the file, overwritting
//											 on it if it already exists.
//										RW - Open an existing file for
//											 Read/write access.
//										RO - Open an existing file for
//											 readonly access.
//										PR - Open an existing file for
//											 readonly access but stops
//											 reading the file after the
//											 prolog has been found.
//		CFileException* pException	-	Returns a pointer to an exception
//										if there is a problem.
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::Load(CString strFileName, eOpenFT eOpenMode, CFileException* pException)
{
	int	nMode		    = CFile::typeText;
	m_eOpenMode		    = eOpenMode;
	m_strFileName	    = strFileName;

	// Set the necessary open flags
	switch (m_eOpenMode)
	{
		case CTextFile::CR :
		{
			nMode = CFile::typeText | CFile::modeCreate | CFile::shareExclusive;
			TRY { CStdioFile::Remove(strFileName); }
			CATCH( CFileException, e ) {}
			END_CATCH
			break;
		}
		case CTextFile::RW : nMode = CFile::typeText | CFile::modeReadWrite | CFile::shareExclusive;	break;
		case CTextFile::RO : nMode = CFile::typeText | CFile::modeRead | CFile::shareDenyNone;			break;
		case CTextFile::PR : nMode = CFile::typeText | CFile::modeRead | CFile::shareDenyNone;			break;
	}

	// Open the file
	if (!CStdioFile::Open(strFileName, nMode, pException)){
		return FALSE;
	}
	
	// Read the file in
	return ReadFile(pException);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 10/02/2002			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::LoadCaseSensitive
//	Description :	Open the file and read in its contents.
//                  Checks if the case of filename matches.
//	Return :		
//		BOOL		-	TRUE if the file opened OK.
//						FALSE if there is an error.  See pException for
//						more detailed information
//                      INVALID_CASE  Case doesn't match.
//	Parameters :	
//		CString strFileName			- Name of the file to open	
//		eOpenFT eOpenMode			- Specify one of the following:
//										CR - Create the file, overwritting
//											 on it if it already exists.
//										RW - Open an existing file for
//											 Read/write access.
//										RO - Open an existing file for
//											 readonly access.
//										PR - Open an existing file for
//											 readonly access but stops
//											 reading the file after the
//											 prolog has been found.
//		CFileException* pException	-	Returns a pointer to an exception
//										if there is a problem.
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::LoadCaseSensitive(CString strFileName, eOpenFT eOpenMode, CFileException* pException)
{
	int	nMode		    = CFile::typeText;
	m_eOpenMode		    = eOpenMode;
	m_strFileName	    = strFileName;

	// Set the necessary open flags
	switch (m_eOpenMode)
	{
		case CTextFile::CR :
		{
			nMode = CFile::typeText | CFile::modeCreate | CFile::shareExclusive;
			TRY { CStdioFile::Remove(strFileName); }
			CATCH( CFileException, e ) {}
			END_CATCH
			break;
		}
		case CTextFile::RW : nMode = CFile::typeText | CFile::modeReadWrite | CFile::shareExclusive;	break;
		case CTextFile::RO : nMode = CFile::typeText | CFile::modeRead | CFile::shareDenyNone;			break;
		case CTextFile::PR : nMode = CFile::typeText | CFile::modeRead | CFile::shareDenyNone;			break;
	}

	// Open the file
	if (!CStdioFile::Open(strFileName, nMode, pException)){
		return FALSE;
	}

	CString strFileTitle;
	CString strFilePath;
	CFileFind finder;
	BOOL bWorking = finder.FindFile(strFileName);
	while (bWorking){
	  bWorking = finder.FindNextFile();
	  strFileTitle = finder.GetFileTitle();
	  strFilePath = finder.GetFilePath();	
	}
		
	COXUNC	UNCPrcPath (strFileName);	
	CString strNameOnly = UNCPrcPath.File();
	// CString strCheckName = CStdioFile::GetFileName();
	CString strCheckName = finder.GetFileName();
	
	if (strCheckName.Compare(strNameOnly)){
		Close(pException);
		return FALSE;
	}

	// Read the file in
	return ReadFile(pException);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::Close
//	Description :	This routine closes the file and if necessary write
//					the data back out to disk.
//	Return :		
//		BOOL		-	TRUE if ok.
//						FALSE if there was an error.  See pException for
//						more detailed information
//	Parameters :	
//		eCloseFT eCloseMode			- One of the following:
//										NOWRITE - Just close the file.
//										WRITE - Write the data out.  This
//										  option will fail if the file was
//										  not opened for write access	
//		CFileException* pException	- Returns a ponter to an exception
//									  if there is a problem.	
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::Close(eCloseFT eCloseMode, CFileException* pException)
{
	if (eCloseMode == CTextFile::NOWRITE)
	{
		TRY
		{
			CStdioFile::Close();
			return TRUE;
		}
		CATCH (CFileException, pE)
		{
			pException->m_bAutoDelete	= pE->m_bAutoDelete;
			pException->m_cause			= pE->m_cause;
			pException->m_lOsError		= pE->m_lOsError;
			pException->m_strFileName	= pE->m_strFileName;
			return FALSE;
		}
		END_CATCH
	}
	else
	{
		TRY
		{
			int nMode = nMode = CFile::typeText | CFile::modeWrite | CFile::modeCreate | CFile::shareExclusive;
			CStdioFile::Close();
			
			// Open the file
//			CStdioFile::Open(m_strFileName, nMode, pException);
			if (!CStdioFile::Open(m_strFileName, nMode, pException)){
				return FALSE;
			}

			// Need to write the prolog out to the file
			if (m_bPrologExtracted){
				WriteProlog();
			}
			
			if (WriteFile(pException))
			{
				CStdioFile::Close();
				return TRUE;
			}
			else
			{
				CStdioFile::Close();
				return FALSE;
			}
		}
		CATCH (CFileException, pE)
		{
			pException->m_bAutoDelete	= pE->m_bAutoDelete;
			pException->m_cause			= pE->m_cause;
			pException->m_lOsError		= pE->m_lOsError;
			pException->m_strFileName	= pE->m_strFileName;
			return FALSE;
		}
		END_CATCH
	}
	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::Close
//	Description :	This routine closes the file and if necessary write
//					the data back out to disk.  If the file was opened for
//					write access, the data will be written out, otherwise
//					the file is just closed.
//	Return :		
//		BOOL		-	TRUE if ok.
//						FALSE if there was an error.  See pException for
//						more detailed information
//	Parameters :	
//		CFileException* pException	- Returns a ponter to an exception
//									  if there is a problem.	
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::Close(CFileException* pException)
{
	eCloseFT eMode = NOWRITE;

	if (m_eOpenMode == CTextFile::INV)
	{
		pException->m_cause = CFileException::genericException;
		return FALSE;
	}

	switch (m_eOpenMode)
	{
		case CTextFile::CR : eMode = CTextFile::WRITE;		break;
		case CTextFile::RW : eMode = CTextFile::WRITE;		break;
		case CTextFile::RO : eMode = CTextFile::NOWRITE;	break;
		case CTextFile::PR : eMode = CTextFile::NOWRITE;	break;
	}
	return Close(eMode, pException);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::Clear
//	Description :	This routine clears the file buffer.
//
//	Return :		
//		void
//
//	Parameters :	
//		None
//
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CTextFile::Clear()
{
	m_strFileText.RemoveAll();
	m_strFileName.Empty();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CString	CTextFile::ExceptionErrorText
//	Description :	Returns a string that corresponds to the Exception.
//	Return :		
//		CString	-	Text associated with the CFileException.
//	Parameters :	
//		CFileException cException	-	CFileException to check.
//	Note :			
//////////////////////////////////////////////////////////////////////////
CString	CTextFile::ExceptionErrorText(CFileException* pException)
{
	switch (pException->m_cause)
	{
		case CFileException::none				: return _T("No error occurred"); break;
		case CFileException::genericException	: return _T("An unspecified error occurred"); break;
		case CFileException::fileNotFound		: return _T("The file could not be located"); break;
		case CFileException::badPath			: return _T("All or part of the path is invalid"); break;
		case CFileException::tooManyOpenFiles	: return _T("The permitted number of open files was exceeded"); break;
		case CFileException::accessDenied		: return _T("The file could not be written to"); break;
		case CFileException::invalidFile		: return _T("There was an attempt to use an invalid file handle"); break;
		case CFileException::removeCurrentDir	: return _T("The current working directory cannot be removed"); break;
		case CFileException::directoryFull		: return _T("There are no more directory entries"); break;
		case CFileException::badSeek			: return _T("There was an error trying to set the file pointer"); break;
		case CFileException::hardIO				: return _T("There was a hardware error"); break;
		case CFileException::sharingViolation	: return _T("SHARE.EXE was not loaded, or a shared region was locked"); break;
		case CFileException::lockViolation		: return _T("There was an attempt to lock a region that was already locked"); break;
		case CFileException::diskFull			: return _T("The disk is full"); break;
		case CFileException::endOfFile			: return _T("The end of file was reached"); break;
		default									: return _T("Invalid exception m_cause value"); break;
	}
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::ReadFile
//	Description :	Reads in the file
//	Return :		
//		BOOL		-	TRUE if the file was read.
//						FALSE if there was a problem.  See pException for
//						more detailed information.
//	Parameters :	
//		CFileException* pException	- Returns a ponter to an exception
//									  if there is a problem.	
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::ReadFile(CFileException* pException)
{
	CString strText;
	TRY
	{
		if (m_eOpenMode == CTextFile::PR)
		{
			BOOL bContinue = ReadString(strText);
			while (bContinue)
			{
				CString strError;
				m_strFileText.Add(strText);
				if (m_cProlog.IsEnd(strText, &strError) != REC_IGNORED)
					bContinue = FALSE;
				else
					bContinue = ReadString(strText);
			}
		}
		else
		{
			while (ReadString(strText))
			{
				m_strFileText.Add(strText);
			}
		}
	}
	CATCH (CFileException, pE)
	{
		pException->m_bAutoDelete	= pE->m_bAutoDelete;
		pException->m_cause			= pE->m_cause;
		pException->m_lOsError		= pE->m_lOsError;
		pException->m_strFileName	= pE->m_strFileName;
		return FALSE;
	}
	END_CATCH

//	ExtractProlog();

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::ExtractProlog
//	Description :	Extract the prolog from the CStringArray
//	Return :		
//		void		-	
//	Parameters :	
//	Note :	This routine updates the CProlog member variable as
//			appropriate.
//////////////////////////////////////////////////////////////////////////
void CTextFile::ExtractProlog()
{
	CString	strErrorText;
	int		nIndex=0;
	int		nSize = m_strFileText.GetSize();
	BOOL	bContinue = nIndex < nSize ? TRUE : FALSE;
	m_bPrologExtracted = TRUE;
	while (bContinue)
	{
		switch (m_cProlog.Consider(m_strFileText[nIndex], &strErrorText))
		{
			case REC_VALID		:	m_strFileText.RemoveAt(nIndex);
									nSize--;
									m_cProlog.IncLineCount();
									break;
			case REC_IGNORED	:	nIndex++;
									break;
			case REC_DONE		:	bContinue = FALSE;
									break;
			case REC_WARNING	:	nIndex++;
									TRACE1("Warning : %s\n", strErrorText);
									break;
			case REC_ERROR		:	nIndex++;
									TRACE1("Error   : %s\n", strErrorText);
									break;
		}
		if (bContinue)
			bContinue = nIndex < nSize ? TRUE : FALSE;
	}
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::WriteFile
//	Description :	Writes the file out to disk.
//	Return :		
//		BOOL		-	
//	Parameters :	
//		CFileException* pException	- Returns a ponter to an exception
//									  if there is a problem.	
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::WriteFile(CFileException* pException)
{
	int nCnt = m_strFileText.GetSize();
	CString strText;
	TRY
	{
		for (int nIndex=0; nIndex<nCnt; nIndex++)
		{
			strText.Format(_T("%s\n"), m_strFileText[nIndex]);
			WriteString(strText);
		}
	}
	CATCH (CFileException, pE)
	{
		pException->m_bAutoDelete	= pE->m_bAutoDelete;
		pException->m_cause			= pE->m_cause;
		pException->m_lOsError		= pE->m_lOsError;
		pException->m_strFileName	= pE->m_strFileName;
		return FALSE;
	}
	END_CATCH

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::AddText
//	Description :	Adds a line of text to the end of the CStringArray
//	Return :		
//		BOOL		-	TRUE if the line was added.
//						FALSE if there was a problem.
//	Parameters :	
//		CString strText	- Text to add.	
//	Note :	If this function fails a CMemoryException is thrown but not
//			returned.		
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::AppendText(CString strText)
{
	TRY
	{
		m_strFileText.Add(strText);
		return TRUE;
	}
	CATCH (CMemoryException, pE)
	{
		return FALSE;
	}
	END_CATCH
}

//////////////////////////////////////////////////////////////////////////
//	Author :				Date : 8/28/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::AddText
//	Description :	Adds a line of text to the CStringArray at the
//					given index.
//	Return :
//		BOOL		-	TRUE if the line was added.
//						FALSE if there was a problem.
//	Parameters :
//		int nIndex		- Index of line to insert text.
//		CString strText	- Text to add.
//	Note :	If this function fails a CMemoryException is thrown but not
//			returned.
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::InsertAt(int nIndex, CString strText)
{
	TRY
	{
		m_strFileText.InsertAt(nIndex, strText);
		return TRUE;
	}
	CATCH (CMemoryException, pE)
	{
		return FALSE;
	}
	END_CATCH
}

//////////////////////////////////////////////////////////////////////////
//	Author :				Date : 8/28/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::SetAt
//	Description :	Replaces a current element in the CStringArray at the
//					given index.
//	Return :
//		BOOL		-	TRUE if the line was added.
//						FALSE if there was a problem.
//	Parameters :
//		int nIndex		- Index of line to replace.
//		CString strText	- Text.
//	Note :	If this function fails a CMemoryException is thrown but not
//			returned.
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::SetAt(int nIndex, CString strText)
{
	TRY
	{
		m_strFileText.SetAt(nIndex, strText);
		return TRUE;
	}
	CATCH (CMemoryException, pE)
	{
		return FALSE;
	}
	END_CATCH
}


//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/25/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::GetTextAt
//	Description :	Returns the text at the given location
//	Return :		
//		CString		-	Text string
//	Parameters :	
//		int nIndex	-	Index of line to return
//	Note :			Debug version throws an assertion if the index is 
//					invalid.
//////////////////////////////////////////////////////////////////////////
CString CTextFile::GetTextAt(int nIndex)
{
	if ((nIndex >=0) && (nIndex < m_strFileText.GetSize()))
		return m_strFileText[nIndex];
	else
	{
		ASSERT (FALSE);
		return _T("");
	}
}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::GetDirLine
//	Description :	Searches input line number for continuation character.  
//					If one is found, gets next line and appends to strBuffer.
//					Repeats until a line is retrieved which does not have a
//					continuation char.
//	Return :		
//		CString		-	Return the current directive line being processed
//	Parameters :	
//		int nIndex	-	Index of line to return
//	Note :			Debug version throws an assertion if the index is 
//					invalid.
//////////////////////////////////////////////////////////////////////////
CString	 CTextFile::GetDirLine(const int nLine, int &nRtnLine)
{	
	int nMaxLines = GetLineCount();
	int nLineToGet = nLine;
	int nTemp = 0;
	CString strBuffer;

	if (nLine < nMaxLines){
		strBuffer = GetTextAt(nLineToGet);
		CString strTrimmedLine(strBuffer);
		strTrimmedLine.TrimRight();
		strTrimmedLine.TrimLeft();
		if (!strTrimmedLine.IsEmpty() && ('#' != strTrimmedLine[0]))
		{
			// Remove trailing comment.
			int nPos = 0;
			if ( -1 != (nPos = (strBuffer.Find('#', 0))))
			{
				strBuffer.Delete(nPos, strBuffer.GetLength() - nPos);
			}
		}
	}

	strBuffer.TrimRight();

	// Don't forget the newline.  Subtract one character from end.
	int nEndOfLine = strBuffer.GetLength()-1;

	if (nEndOfLine < 0) 
		nEndOfLine = 0;

	// (nLineToGet < nMaxLines)
	++nLineToGet;
	while ((nLineToGet < nMaxLines) && ((nTemp = strBuffer.Find('\\')) == nEndOfLine))
	{
		int nPos = 0;
		strBuffer.Replace('\\', ' ');

		CString strTemp = GetTextAt(nLineToGet);
		// Remove commnts after continuation character.
		if ( -1 != (nPos = (strTemp.Find('#', 0)))){
			strTemp.Delete(nPos, strTemp.GetLength() - nPos);
		}
		strTemp.TrimRight();
		strTemp.TrimLeft();
		strBuffer += " " + strTemp;
		strBuffer.TrimRight();

		nEndOfLine = strBuffer.GetLength() - 1;

		if (nEndOfLine < 0) 
			nEndOfLine = 0;
		++nLineToGet;
	}

	nRtnLine = nLineToGet - nLine;

	return strBuffer;
}

//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::GetDirLineAtEOF
//	Description :	Searches input line number for continuation character.  
//					If one is found, gets next line and appends to strBuffer.
//					Repeats until a line is retrieved which does not have a
//					continuation char.
//	Return :		
//		CString		-	Return the current directive line being processed
//	Parameters :	
//		int nIndex	-	Index of line to return
//	Note :			Debug version throws an assertion if the index is 
//					invalid.
//////////////////////////////////////////////////////////////////////////
CString	 CTextFile::GetDirLineAtEOF(int &nLine, int &nRtnLine)
{	
	int nMaxLines = GetLineCount();
//	int nLineToGet = nLine;
//	int nTemp = 0;

	// Need to add error checking that nLine is between 1 and nMaxLines.
	
//	CString strBuffer = GetTextAt(nLineToGet);
	int nPrevLine = nLine - 2;
	CString strPrevLine = GetTextAt(nPrevLine);
//	CString strTrimmedLine(strBuffer);
//	CString strPrevLineTrimmed(strPrevLine);

	// Remove comments after continuation character.
	int nPos = 0;
	if ( -1 != (nPos = (strPrevLine.Find('#', 0)))){
		strPrevLine.Delete(nPos, strPrevLine.GetLength() - nPos);
	}
	strPrevLine.TrimRight();
	strPrevLine.TrimLeft();

	// Don't forget the newline.  Subtract one character from end.
	int nEndOfLine = strPrevLine.GetLength()-1;
	if (nEndOfLine < 0) {
		nEndOfLine = 0;
	}
	
	// (nLineToGet < nMaxLines)
	int nTemp = 0;
	while ((nPrevLine > 0) && ((nTemp = strPrevLine.Find('\\')) == nEndOfLine))
	{
		--nPrevLine;

		int nPos = 0;
		strPrevLine.Replace('\\', ' ');
		CString strTemp = GetTextAt(nPrevLine);
		// Remove comments after continuation character.
		if ( -1 != (nPos = (strTemp.Find('#', 0)))){
			strTemp.Delete(nPos, strTemp.GetLength() - nPos);
		}
		strTemp.TrimRight();
		strTemp.TrimLeft();
		strPrevLine += " " + strTemp;
		strPrevLine.TrimRight();

		nEndOfLine = strPrevLine.GetLength() - 1;

		if (nEndOfLine < 0) 
			nEndOfLine = 0;
	}

	int nLineToGet = nPrevLine + 1;
	CString strBuffer = GetTextAt(nLineToGet);
	if (!strBuffer.IsEmpty() && ('#' != strBuffer[0]))
	{
		int nPos = 0;
		if ( -1 != (nPos = (strBuffer.Find('#', 0))))
		{
			strBuffer.Delete(nPos, strBuffer.GetLength() - nPos);
			strBuffer.TrimRight();
		}
	}

	// Don't forget the newline.  Subtract one character from end.
    nEndOfLine = strBuffer.GetLength()-1;
	if (nEndOfLine < 0) 
		nEndOfLine = 0;

	// (nLineToGet < nMaxLines)
	nTemp = 0;
 //   nLineToGet = nLine;
	while ((nLineToGet < nMaxLines) && ((nTemp = strBuffer.Find('\\')) == nEndOfLine))
	{
		++nLineToGet;

		int nPos = 0;
		strBuffer.Replace('\\', ' ');
		CString strTemp = GetTextAt(nLineToGet);
		// Remove comments after continuation character.
		if ( -1 != (nPos = (strTemp.Find('#', 0)))){
			strTemp.Delete(nPos, strTemp.GetLength() - nPos);
		}
		strTemp.TrimRight();
		strTemp.TrimLeft();
		strBuffer += " " + strTemp;
		strBuffer.TrimRight();

		nEndOfLine = strBuffer.GetLength() - 1;

		if (nEndOfLine < 0) 
			nEndOfLine = 0;
	}

	nRtnLine = nLine - nLineToGet;

	return strBuffer;
}


//////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::RemoveAt
//	Description :	Removes the text at the given location
//	Return :		
//		BOOL		-	TRUE if the line was removed.
//						FALSE if there was a problem.
//	Parameters :	
//		int nIndex	-	Index of line to remove
//	Note :			Debug version throws an assertion if the index is 
//					invalid.
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::RemoveAt(const int nIndex)
{
	if ((nIndex >= 0) && (nIndex < m_strFileText.GetSize()))
	{
		m_strFileText.RemoveAt(nIndex);
		return TRUE;
	}
	
	ASSERT (FALSE);
	return FALSE;
}


//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/25/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::ScanLine
//
//	Description :	Scans line to determine if token is present in line.
//
//	Returns :		bool: true if token is present; otherwise false.
//
//	Parameters :    const CString &pstrLine, const CString &strScan	
//	
//	Notes :	case insensitive		
//					
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::ScanLine (const CString &pstrLine, const CString &strScan){

	CString strTempLine = pstrLine;
	CString strSearch	= strScan;
	BOOL bReturn_Status = FALSE;

//	strTempLine.TrimLeft ();
//	strTempLine.TrimRight ();
	strTempLine.MakeLower ();

	strSearch.TrimRight();
	strSearch.TrimLeft();
	strSearch.MakeLower ();

	int nScanPos = strTempLine.Find (strSearch, 0);
	if (nScanPos != -1){
		int nSizeScan = strSearch.GetLength ();
		int nSizeLine = strTempLine.GetLength ();
		if (nSizeLine == nSizeScan) {
			// Scan string is same as the search string
			bReturn_Status = TRUE;
		} else if (nSizeLine > nSizeScan){
			// Make sure the scan string is not part of a larger string
			// Must have either spaces before and after scan string.
			// or scan string is beginning or end of line.

			// Search string starts at begining of line.
			if (nScanPos == 0) { 
				// 1st check space is at end of search string. 
				if (strTempLine.GetAt (nScanPos + nSizeScan) == _T(' ')){
					bReturn_Status = TRUE;
				}
			} else if (strTempLine.GetAt (nScanPos - 1) == _T(' ')){
				if ((nScanPos + nSizeScan) == nSizeLine){
					// Scan string is at end of the line and space at begining
					bReturn_Status = TRUE;
				} else if (strTempLine.GetAt (nScanPos + nSizeScan) == _T(' ')){
					// Scan string has spaces at beginning and end.
					bReturn_Status = TRUE;
				}
			} 
		}
	}
	return bReturn_Status;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw	Date : 12/12/02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::WhereIsToken
//
//	Description :	Scans line to determine where token is in text line.
//
//	Returns :		Column where token starts in line.  -1 if token is not found.
//
//	Parameters :    const CString &pstrLine, const CString &strScan	
//	
//	Notes :	case insensitive; 		
//					
//////////////////////////////////////////////////////////////////////////
int CTextFile::WhereIsToken (const CString &pstrLine, const CString &strScan){

	CString strTempLine = pstrLine;
	CString strSearch	= strScan;
	//	int bReturn_Status = FALSE;

//	strTempLine.TrimLeft ();
//	strTempLine.TrimRight ();
	strTempLine.MakeLower ();

	strSearch.TrimRight();
	strSearch.TrimLeft();
	strSearch.MakeLower ();

	int nScanPos = strTempLine.Find (strSearch, 0);
	if (nScanPos != -1){
		int nSizeScan = strSearch.GetLength ();
		int nSizeLine = strTempLine.GetLength ();
		// if (nSizeLine == nSizeScan) {
			// Scan string is same as the search string
			// bReturn_Status = TRUE;
		if (nSizeLine > nSizeScan){
			// Make sure the scan string is not part of a larger string
			// Must have either spaces before and after scan string.
			// or scan string is beginning or end of line.

			// Search string starts at begining of line.
			if (nScanPos == 0) { 
				// 1st check space is at end of search string. 
				if (strTempLine.GetAt (nScanPos + nSizeScan) != _T(' ')){
					nScanPos = -1;
				}
			} else if (strTempLine.GetAt (nScanPos - 1) == _T(' ')){
				// Space at beginning, now check if space at end or at end of line
				 if (((nScanPos + nSizeScan) != nSizeLine) &&
					(strTempLine.GetAt (nScanPos + nSizeScan) != _T(' '))){
					// Scan string is not at end of the line and space at begining
				//	bReturn_Status = TRUE;
					nScanPos =  -1;
				}
			} 
		}
	}
	return nScanPos;
}

#ifdef _DEBUG
//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::DumpProlog
//	Description :	Debug function to dump the object.  Currently only
//					the prolog is dumped.
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CTextFile::DumpProlog()
{
	m_cProlog.Dump();
}
#endif

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 1/17/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::TokenizeString
//
//	Description :	
//					
//	Returns :		BOOL		
//			
//	Parameters :	const CString strTemp, CStringArray &strTokenData
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::TokenizeString(const CString strTemp, CStringArray &strTokenData)
{
	int nCt = 0, nLen = 0, i = 0;
	bool bDone = FALSE,  bReturn_Status = TRUE;
	CString strSave(strTemp);

	while (!bDone){
		strSave.TrimRight();
		strSave.TrimLeft();

		// Find first space
		nCt = strSave.Find(' ',0);

		if (nCt == -1){
			// No space found
			// Must be at end of line.
			strTokenData.Add(strSave);
			bDone = TRUE;

		}else {
			// store token found
			strTokenData.Add(strSave);
	
			// remove rest of line leave only token
			nLen = strSave.GetLength() - nCt;
			strTokenData[i].Delete(nCt, nLen);
			strTokenData[i].TrimLeft();
			strTokenData[i].TrimRight();
			i++;

			// Delete saved token
			strSave.Delete(0, nCt);
			strSave.TrimLeft();
		}
	}
	return bReturn_Status;
}

//////////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw		Date : 05/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		IsFileValid
//
//	Description :	enum eValidStatus &eVStatus
//					
//	Returns :		BOOL		
//			
//	Parameters :	
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::IsFileValid(enum eValidStatus &eVStatus)
{
	if (!m_bPrologExtracted){
		ExtractProlog();
	}
	return m_cProlog.m_cPrologValid.IsFileValid(eVStatus);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw			Date : 05/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::IsFileValid
//
//	Description :	
//					
//	Returns :		BOOL		
//			
//	Parameters :	enum eValidStatus &eVStatus, enum eValidType &eVType
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::IsFileValid(enum eValidStatus &eVStatus, enum eValidLoc &eVLoc){
	if (!m_bPrologExtracted){
		ExtractProlog();
	}
	return m_cProlog.m_cPrologValid.IsFileValid(eVStatus, eVLoc);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw			Date : 05/31/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::IsFileValid(enum eValidType &eVType)
//	Description :	
//					
//	Return :		BOOL		
//			
//	Parameters :	enum eValidType &eVType
//	
//	Note :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::IsFileValid(enum eValidLoc &eVLoc)
{
	if (!m_bPrologExtracted){
		ExtractProlog();
	}
	return m_cProlog.m_cPrologValid.IsFileValid(eVLoc);
}

//////////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw		Date : 06/05/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CTextFile::WriteValidationStatus	
//
//	Description :	
//  This routine updates the validation directive in a prolog.
//	It first determines if a validation directive exists.
//	If no validation directive exist, a new one is created and 
//	inserted into the file.
//					
//	Returns :		BOOL		
//			
//	Parameters :	enum eValidStatus eStatus, enum eValidType eType
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::WriteValidationStatus(const enum eValidStatus eStatus, const enum eValidLoc eLoc){
	
	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	// IsFileValid(enum eValidLoc &eVLoc);

	BOOL eReturn_Status =  TRUE;
	CSchedTime schTime;
	CString strValidDirLine;
	eReturn_Status = CreateValidStrLine(eStatus, eLoc, strValidDirLine); // Entire line without processing directive
    m_cProlog.m_cPrologValid.SetText(strValidDirLine);

	m_cProlog.m_cPrologValid.SetValid(TRUE);
	m_cProlog.m_cPrologValid.SetValidStatus(eStatus);
	m_cProlog.m_cPrologValid.SetValidLoc(eLoc);
	DWORD nUserNameSize = UNLEN + 1;
	TCHAR cUserName[UNLEN + 1];
	::GetUserName (cUserName, &nUserNameSize);
	m_cProlog.m_cPrologValid.SetName(cUserName);
	m_cProlog.m_cPrologValid.SetTime(schTime.GetCurrSchedTime());
//	m_strValidText.format(_T("%s"), ctstrValid);
	return eReturn_Status;
}


//////////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw		Date : 06/05/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CTextFile::WriteValidationStatus	
//
//	Description :	
//  This routine updates the validation directive in a prolog.
//	It first determines if a validation directive exists.
//	If no validation directive exist, a new one is created and 
//	inserted into the file.
//					
//	Returns :		BOOL		
//			
//	Parameters :	enum eValidStatus eStatus
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::WriteValidationStatus(const enum eValidStatus eStatus){
	
	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	enum eValidLoc eVLoc = ONBOARD;
	IsFileValid(eVLoc);

	BOOL eReturn_Status =  TRUE;
	CSchedTime schTime;
	CString strValidDirLine;
	eReturn_Status = CreateValidStrLine(eStatus, eVLoc, strValidDirLine); // Entire line without processing directive
    m_cProlog.m_cPrologValid.SetText(strValidDirLine);

	m_cProlog.m_cPrologValid.SetValid(TRUE);
	m_cProlog.m_cPrologValid.SetValidStatus(eStatus);
	m_cProlog.m_cPrologValid.SetValidLoc(eVLoc);
	DWORD nUserNameSize = UNLEN + 1;
	TCHAR cUserName[UNLEN + 1];
	::GetUserName (cUserName, &nUserNameSize);
	m_cProlog.m_cPrologValid.SetName(cUserName);
	m_cProlog.m_cPrologValid.SetTime(schTime.GetCurrSchedTime());
//	m_strValidText.format(_T("%s"), ctstrValid);
	return eReturn_Status;
}


//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 06/04/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::CreateValidStrLine
//		
//	Description :	
//					
//	Returns :		BOOL		
//			
//	Parameters :	enum eValidType &eVType
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::CreateValidStrLine(const enum eValidStatus eStatus, const enum eValidLoc eLoc,
						CString &strLine){
	BOOL eReturn_Status = TRUE;
	CTime t = CTime::GetCurrentTime();
	
	if (PASSED == eStatus){
		strLine += _T("passed "); 
	} else if (WARNING == eStatus){
		strLine += _T("warning ");
	} else if (FAIL == eStatus){
		strLine += _T("fail ");
	} else if (UNVALIDATED == eStatus){
		strLine += _T("unvalidated ");
	} else {
		eReturn_Status = FALSE;
	}
	
	if (GROUND == eLoc){
		strLine += _T("ground ");
	} else if (ONBOARD == eLoc){
		strLine += _T("onboard ");
	} else if (BOTH == eLoc){
		strLine += _T("both ");
	}  else if (RTCS == eLoc){
		strLine += _T("rtcs ");
	}else {
		eReturn_Status = FALSE;
	}

	DWORD nUserNameSize = UNLEN + 1;
	TCHAR cUserName[UNLEN + 1];
	::GetUserName (cUserName, &nUserNameSize);
	strLine += cUserName;
	strLine += t.Format (_T(" %Y-%j-%H:%M:%S"));

	return eReturn_Status;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 06/22/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		AddUpdateDir(const CString strText)	
//
//	Description :	Adds a update directive to the prolog
//					
//	Returns :		BOOL		
//			
//	Parameters :	const CString strText
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////

	BOOL CTextFile::AddUpdateDir(const CString strText)
	{
		BOOL bReturn_Status = TRUE;
		CString       strErrorText;

		if (!m_bPrologExtracted){
			ExtractProlog();
		}
		
		m_cProlog.m_cPrologUpdate.AddDir(strText, &strErrorText);
		return bReturn_Status;
	}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 06/22/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		
//	Description :	Adds a parameter directive to the prolog
//					
//	Return :		BOOL		
//			
//	Parameters :	const CString strText
//	
//	Note :	
//		
//////////////////////////////////////////////////////////////////////////

/*
	BOOL CTextFile::AddParamDir(const CString strText)
	{
		BOOL bReturn_Status = TRUE;
		CString       strErrorText;

		if (!m_bPrologExtracted){
			ExtractProlog();
		}

		m_cProlog.m_cPrologParam.AddDir(strText, &strErrorText);
		return bReturn_Status;
	}

  */

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 05/16/01			version 1.0
  ======================================================================
	Function :		CTextFile::WriteProlog(
	Description :	
					
	Return :		
								
	Parameters :	
											 
	Note :			None.
\*======================================================================*/
BOOL  CTextFile::WriteProlog()
{
	BOOL bReturnStatus = TRUE;
	CString strTemp;

	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	// Write Begin directive
	if (TRUE == m_cProlog.m_bPrologBeginFound){
		strTemp = ctstrPrlgBeg;
		strTemp += _T("\n");
		WriteString(strTemp);
	}

	// Write override
	if (TRUE == m_cProlog.m_bPrologOverRideFound){
		strTemp.Format(_T("%s %s\n"),  ctstrOVERRIDE, m_cProlog.m_cPrologOverRide.GetText());
		WriteString(strTemp);
	}

	// Write validate m_cPrologValid
	strTemp.Format(_T("%s %s\n"), ctstrValid, m_cProlog.m_cPrologValid.GetText()); 
	WriteString(strTemp);

	// Write description  m_cPrologDesc
	CStringArray * strDescArray = m_cProlog.m_cPrologDesc.GetTextArray();
	int nNumEntries = strDescArray->GetSize(); 
	for (int nIndex=0; nIndex<nNumEntries; nIndex++)
	{
		//PR000507: MSD Fixed to write out the whole ##desc line.
		//strTemp.Format(_T("%s %s\n"), ctstrDesc, (*strDescArray)[nIndex]);
		strTemp.Format(_T("%s\n"), (*strDescArray)[nIndex]);
		WriteString(strTemp);
	}

	// Write exectime
	if (TRUE == m_cProlog.m_bPrologExectimeFound){
		strTemp.Format(_T("%s %s\n"), ctstrExecTime, m_cProlog.m_cPrologExectime.GetTextString()); 
		WriteString(strTemp);
	}

	// Write starttime
	// m_cProlog.m_cPrologStarttime.AddDir(strText)
	if (TRUE == m_cProlog.m_bPrologStarttimeFound){
		strTemp.Format(_T("%s %s\n"), ctstrStartTime, m_cProlog.m_cPrologStarttime.GetTextString()); 
		WriteString(strTemp);
	}

	// Write stoptime
	if (TRUE == m_cProlog.m_bPrologStoptimeFound){
		strTemp.Format(_T("%s %s\n"), ctstrStopTime, m_cProlog.m_cPrologStoptime.GetTextString()); 
		WriteString(strTemp);
	}

	// Write param
	CStringArray *strParamArray = m_cProlog.m_cPrologParam.GetTextArray();
	nNumEntries = strParamArray->GetSize(); 
	for (int nIndex=0; nIndex<nNumEntries; nIndex++)
	{
		strTemp.Format(_T("%s %s\n"), ctstrParam, (*strParamArray)[nIndex]);
		WriteString(strTemp);
	}

	// Write update  m_cPrologUpdate
	CStringArray * strUpdateArray = m_cProlog.m_cPrologUpdate.GetTextArray();
	nNumEntries = strUpdateArray->GetSize(); 
	for (int nIndex=0; nIndex<nNumEntries; nIndex++)
	{
		// Only write out those records that have values
		if ( !(*strUpdateArray)[nIndex].IsEmpty()){ 
			strTemp.Format(_T("%s %s\n"), ctstrUpdate, (*strUpdateArray)[nIndex]);
			WriteString(strTemp);
		}
	}

	// Write end
	if (TRUE == m_cProlog.m_bPrologEndFound){
		strTemp = ctstrPrlgEnd;
		strTemp += _T(" \n");
		WriteString(strTemp);
	}
	return bReturnStatus;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 06/22/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		AddPrologDesc
//
//	Description :	Adds a description directive to the prolog 
//					
//	Returns :		BOOL		
//			
//	Parameters :	CString strText
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL CTextFile::AddPrologDesc(const CString strText)
{
	BOOL bReturn_Status = TRUE;
	CString       strErrorText;
//	CString       strDescText = strText;

	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	bReturn_Status = m_cProlog.m_cPrologDesc.AddDir(strText, &strErrorText);
	return bReturn_Status;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 06/22/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		AddPrologStarttime
//
//	Description :	Adds a start time directive to the prolog 
//					
//	Returns :		BOOL		
//			
//	Parameters :	CString strText
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL   CTextFile::AddPrologStarttime(const CString strText)
{
	BOOL bReturn_Status = TRUE;

	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	m_cProlog.m_bPrologStarttimeFound = TRUE;
	bReturn_Status = m_cProlog.m_cPrologStarttime.AddDir(strText);
	return bReturn_Status;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 06/22/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		AddPrologStoptime
//
//	Description :	Adds a stop time directive to the prolog 
//					
//	Returns :		BOOL		
//			
//	Parameters :	CString strText
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL   CTextFile::AddPrologStoptime(const CString strText)
{
	BOOL bReturn_Status = TRUE;

	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	m_cProlog.m_bPrologStoptimeFound = TRUE;
	bReturn_Status = m_cProlog.m_cPrologStoptime.AddDir(strText);
	return bReturn_Status;
}


//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 06/22/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		AddPrologExectime
//
//	Description :	Adds a start time directive to the prolog 
//					
//	Returns :		BOOL		
//			
//	Parameters :	CString strText
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL   CTextFile::AddPrologExectime(const CString strText)
{
	BOOL bReturn_Status = TRUE;

	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	m_cProlog.m_bPrologExectimeFound = TRUE;
	bReturn_Status = m_cProlog.m_cPrologExectime.AddDir(strText);
	return bReturn_Status;
}


//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 09-10-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CProlog GetProlog()
//
//	Description :	Returns pointer to the CProlog class. 
//					
//	Returns :		CProlog *		
//			
//	Parameters :	none.
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
	CProlog *	CTextFile::GetProlog()
{
	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	return &m_cProlog;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 09-10-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CTextFile::GetPrologLineCount()
//
//	Description :	Returns the number of lines in the prolog class. 
//					
//	Returns :		int
//			
//	Parameters :	none.
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
int CTextFile::GetPrologLineCount()
{
	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	return m_cProlog.GetLineCount();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 09-10-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : CTextFile::GetNumberPassedParams ()		
//
//	Description : Retruns the number of passed parameters declared in the prolog.	. 
//					
//	Returns :	int			
//			
//	Parameters :	none.
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
	int CTextFile::GetNumberPassedParams ()	
{
	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	return m_cProlog.GetNumberPassedParams();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 03-6-2002	version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : CTextFile::GetPassedParamObj()		
//
//	Description :  
//					
//	Returns :	  Returns the an array of passed param structs.			
//			
//	Parameters :	none.
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
CObArray       &CTextFile::GetPassedParamObj(){
	if (!m_bPrologExtracted){
		ExtractProlog();
	}

	return m_cProlog.m_cPrologParam.GetPassedParamObj();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw	Date : 03-08-2002	version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : CTextFile::		
//
//	Description :  
//					
//	Returns :	  			
//			
//	Parameters :	none.
//	
//	Notes :	
//		
//////////////////////////////////////////////////////////////////////////
BOOL       CTextFile::CheckPassedParams(CString strVal){

	BOOL eReturn_Status = TRUE;
	int  nPos  = 0;
	int nNumValuesFound = 0;

	strVal.Replace(_T("("), _T(""));
	strVal.TrimLeft();
	strVal.TrimRight();
	nPos = strVal.Find(_T(','));
	while( nPos != -1){
		nNumValuesFound++;
		strVal = strVal.Right(strVal.GetLength() - nPos - 1);
		strVal.TrimLeft();
		strVal.TrimRight();
		nPos = strVal.Find(_T(','));
	}

	if (-1 != (nPos = strVal.Find(_T(')')))){
		nNumValuesFound++;
	} 

	/*
		Get number of passed parameters in file
		Compare to values passed in
	 */
	int nNumParam       = GetNumberPassedParams ();
	if ( nNumValuesFound != nNumParam){
		// output an error
		eReturn_Status = FALSE;
	}
	
	return eReturn_Status;
}


//////////////////////////////////////////////////////////////////////////
// CProlog Class
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//	Author :Don sanders			Date : 12/13/00			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CProlog::CProlog
//	Description :	Class constructor.  Set the initial state of the
//					object to BEGIN
//	Return :		constructor	-
//	Parameters :	None.
//	Note :			The object can be in one of three states.  The
//					first state is STATE_BEGIN which means we are
//					searching for a begin record.  Once the begin
//					record is found, the state becomes PROLOG.  At
//					this point we then search for any valid prolog
//					records.  When the end record is found, the state
//					is changed to DONE.
//////////////////////////////////////////////////////////////////////////
CProlog::CProlog()
{
	m_eState = STATE_BEGIN;
	m_nLineCnt = 0;
	m_bPrologBeginFound     = FALSE;
	m_bPrologExectimeFound  = FALSE;
	m_bPrologStarttimeFound = FALSE;
	m_bPrologStoptimeFound  = FALSE;
	m_bPrologEndFound       = FALSE;
	m_bPrologOverRideFound  = FALSE;
	m_bPrologUpdateFound	= FALSE;
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CProlog::~CProlog
	Description :	Class destructor.
	Return :		destructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CProlog::~CProlog(){}

/*======================================================================*\
	Author :Don sanders			Date : 05/03/01	   	   version 1.0
  ======================================================================
	Function :		CProlog::CProlog
	Description :	Copy constructor.
	Return :		constructor
	Parameters :	const CProlog &.
	Note :			None.
\*======================================================================*/
CProlog::CProlog(const CProlog & Other)
{
	m_eState                = Other.m_eState;
	m_nLineCnt              = Other.m_nLineCnt;
	m_cPrologDesc           = Other.m_cPrologDesc;
	m_cPrologExectime       = Other.m_cPrologExectime;
	m_cPrologStarttime      = Other.m_cPrologStarttime;
	m_cPrologStoptime       = Other.m_cPrologStoptime;
	m_cPrologUpdate         = Other.m_cPrologUpdate;
	m_cPrologValid          = Other.m_cPrologValid;
	m_cPrologParam          = Other.m_cPrologParam;     
	m_bPrologBeginFound     = Other.m_bPrologBeginFound;
	m_bPrologExectimeFound  = Other.m_bPrologExectimeFound;
	m_bPrologStarttimeFound = Other.m_bPrologStarttimeFound;
	m_bPrologStoptimeFound  = Other.m_bPrologStoptimeFound;
	m_bPrologEndFound       = Other.m_bPrologEndFound;
	m_bPrologOverRideFound  = Other.m_bPrologOverRideFound;
	m_bPrologUpdateFound	= Other.m_bPrologUpdateFound;
}

/*======================================================================*\
	Author :Fred Shaw			Date : 05/03/01			version 1.0
  ======================================================================
	Function :		CProlog & CProlog::operator=
	Description :	Assignment operator
	Return :		CProlog &.
	Parameters :	const CProlog &.
	Note :			None.
\*======================================================================*/
CProlog & CProlog::operator=(const CProlog & Other)
{
	if (this == &Other)  // object assigned to itself
		return *this; // Nothing to do.

	m_eState                = Other.m_eState;
	m_nLineCnt              = Other.m_nLineCnt;
	m_cPrologDesc           = Other.m_cPrologDesc;
	m_cPrologExectime       = Other.m_cPrologExectime;
	m_cPrologStarttime      = Other.m_cPrologStarttime;
	m_cPrologStoptime       = Other.m_cPrologStoptime;
	m_cPrologUpdate         = Other.m_cPrologUpdate;
	m_cPrologValid          = Other.m_cPrologValid;
	m_cPrologParam          = Other.m_cPrologParam;     
	m_bPrologBeginFound     = Other.m_bPrologBeginFound;
	m_bPrologExectimeFound  = Other.m_bPrologExectimeFound;
	m_bPrologStarttimeFound = Other.m_bPrologStarttimeFound;
	m_bPrologStoptimeFound  = Other.m_bPrologStoptimeFound;
	m_bPrologEndFound       = Other.m_bPrologEndFound;
	m_bPrologOverRideFound  = Other.m_bPrologOverRideFound;
	m_bPrologUpdateFound	= Other.m_bPrologUpdateFound;
	return *this;
}

#ifdef _DEBUG
/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CProlog::Dump
	Description :	Dumps the various prolog records.
	Return :		void
	Parameters :	None.
	Note :			Only used if _DEBUG is defined.
\*======================================================================*/
void CProlog::Dump()
{
	TRACE0("Dumping the Prolog...\n");
	m_cPrologDesc.Dump();
	m_cPrologExectime.Dump();
	TRACE0("Prolog dump complete\n");
}
#endif

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CProlog::Consider
	Description :	This routine considers whether the CString passed
					in is a valid prolog record.  If it is valid, it
					will then add the record into the prolog object as
					appropriate.
	Return :		eRecord	- REC_VALID is returned when a valid
							  record is found.
							  REC_IGNORED is returned if the string
							  passed in is not one of the records types
							  that we are searching for.
							  REC_ERROR is returned whenever an error
							  is found.  The ErrorText variable will
							  contain a string describing the error.
							  REC_WARNING is returned whenever a 
							  warning is found.  The ErrorText variable
							  will contain a string describing the
							  warning.
							  REC_DONE is returned whenever a 
							  prolog begin/end record pair has been
							  found.  It does not mean that the prolog
							  is valid, but that we no longer need to
							  search the file for more possible
							  records
	Parameters :	CString strText	-	The CString to consider.
					CString pstrErrorText - Contains an Error or Warning
											text message.  On valid of the
											return status is REC_ERROR or
											REC_WARNING.
	Note :			None.
\*======================================================================*/
eRecord CProlog::Consider(CString strText, CString* pstrErrorText)
{
	TRACE1 ("Considering <%s> for prolog inclusion\n", strText);
	eRecord eRtnStatus = REC_VALID;
	switch (m_eState)
	{
		case STATE_BEGIN:
		{
			// The only thing we need to check for now is a valid prolog begin record
			switch (eRtnStatus = IsBegin(strText, pstrErrorText))		//Set the return status
			{
				case REC_VALID		: m_eState = STATE_PROLOG;	break;  //Move to the PROLOG state
				case REC_IGNORED	: break;							//Do nothing
				case REC_WARNING	: break;							//Do nothing
				case REC_ERROR		: break;							//Do nothing
				default				: eRtnStatus = REC_ERROR;			//Unknown return status
									  *pstrErrorText = _T("Invalid return status from IsBegin()");
									  break;
			}
			break;
		}
		case STATE_PROLOG:
		{
			eRtnStatus = REC_IGNORED;

			// Check for duplicate prolog begin records
			if (eRtnStatus == REC_IGNORED)
			{
				switch (eRtnStatus = IsBegin(strText, pstrErrorText))		//Set the return status
				{
					case REC_VALID		: eRtnStatus = REC_WARNING;			//Duplicate prolog begin record
										  *pstrErrorText = _T("Duplicate prolog begin record found");
										  break;
					case REC_IGNORED	: break;							//Do nothing
					case REC_WARNING	: break;							//Do nothing
					case REC_ERROR		: break;							//Do nothing
					default				: eRtnStatus = REC_ERROR;			//Unknown return status
										  *pstrErrorText = _T("Invalid return status from IsBegin()");
										  break;
				}
			}

			// Check for an end record
			if (eRtnStatus == REC_IGNORED)
			{
				switch (eRtnStatus = IsEnd(strText, pstrErrorText))
				{
				case REC_VALID		: m_eState = STATE_END;	break;  //Move to the END state
				case REC_IGNORED	: break;						//Do nothing
				case REC_WARNING	: break;						//Do nothing
				case REC_ERROR		: break;						//Do nothing
				default				: eRtnStatus = REC_ERROR;		//Unknown return status
									  *pstrErrorText = _T("Invalid return status from IsBegin()");
									  break;
				}
			}
			

			// Check for a description record
			if (eRtnStatus == REC_IGNORED)
			{
				switch (eRtnStatus = IsDesc(strText, pstrErrorText))
				{
					case REC_VALID		: break;	//Do nothing
					case REC_IGNORED	: break;	//Do nothing
					case REC_ERROR		: break;	//Do nothing
					default				: eRtnStatus = REC_ERROR;
										  *pstrErrorText = _T("Invalid return status from IsDesc()");
										  break;
				}
			}

			// Check for a exectime record
			if (eRtnStatus == REC_IGNORED)
			{
				switch (eRtnStatus = IsExectime(strText, pstrErrorText))
				{
					case REC_VALID		: break;	//Do nothing
					case REC_IGNORED	: break;	//Do nothing
					case REC_ERROR		: break;	//Do nothing
					default				: eRtnStatus = REC_ERROR;
										  *pstrErrorText = _T("Invalid return status from IsExectime()");
										  break;
				}
			}

			// Check for a starttime record
			if (eRtnStatus == REC_IGNORED)
			{
				switch (eRtnStatus = IsStarttime(strText, pstrErrorText))
				{
					case REC_VALID		: break;	//Do nothing
					case REC_IGNORED	: break;	//Do nothing
					case REC_ERROR		: break;	//Do nothing
					default				: eRtnStatus = REC_ERROR;
										  *pstrErrorText = _T("Invalid return status from IsStarttime()");
										  break;
				}
			}

			// Check for a stoptime record
			if (eRtnStatus == REC_IGNORED)
			{
				switch (eRtnStatus = IsStoptime(strText, pstrErrorText))
				{
					case REC_VALID		: break;	//Do nothing
					case REC_IGNORED	: break;	//Do nothing
					case REC_ERROR		: break;	//Do nothing
					default				: eRtnStatus = REC_ERROR;
										  *pstrErrorText = _T("Invalid return status from IsStoptime()");
										  break;
				}
			}

			// Check for a validation record
			if (eRtnStatus == REC_IGNORED)
			{
				switch (eRtnStatus = IsValid(strText, pstrErrorText))
				{
					case REC_VALID		: break;	//Do nothing
					case REC_IGNORED	: break;	//Do nothing
					case REC_ERROR		: break;	//Do nothing
					default				: eRtnStatus = REC_ERROR;
										  *pstrErrorText = _T("Invalid return status from IsValid()");
										  break;
				}
			}

			// Check for passed parameters record
			if (eRtnStatus == REC_IGNORED)
			{
				switch (eRtnStatus = IsParam(strText, pstrErrorText))
				{
					case REC_VALID		: break;	//Do nothing
					case REC_IGNORED	: break;	//Do nothing
					case REC_ERROR		: break;	//Do nothing
					default				: eRtnStatus = REC_ERROR;
										  *pstrErrorText = _T("Invalid return status from IsParam()");
										  break;
				}
			}

			// Check for an update record
			if (eRtnStatus == REC_IGNORED)
			{
				switch (eRtnStatus = IsUpdate(strText, pstrErrorText))
				{
					case REC_VALID		: break;	//Do nothing
					case REC_IGNORED	: break;	//Do nothing
					case REC_ERROR		: break;	//Do nothing
					default				: eRtnStatus = REC_ERROR;
										  *pstrErrorText = _T("Invalid return status from IsUpdate()");
										  break;
				}
			}

			// Check for an override record
			if (eRtnStatus == REC_IGNORED)
			{
				switch (eRtnStatus = IsOverRide(strText, pstrErrorText))
				{
					case REC_VALID		: break;	//Do nothing
					case REC_IGNORED	: break;	//Do nothing
					case REC_ERROR		: break;	//Do nothing
					default				: eRtnStatus = REC_ERROR;
										  *pstrErrorText = _T("Invalid return status from IsUpdate()");
										  break;
				}
			}
			break;
		}
		case STATE_END:
		{
			eRtnStatus = REC_DONE;
			break;
		}
	}
	return eRtnStatus;
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CProlog::IsBegin
	Description :	Determines if the the CString is a valid prolog	begin
					record.  A valid prolog begin record is:
					##prolog begin
	Return :		eRecord	-	REC_VALID if the record is a valid prolog
								begin record.
								REC_WARNING if the record appears to be a
								valid prolog begin record.
								REC_IGNORE if the record is not a valid
								prolog begin record.
								REC_ERROR if there was an internal error.
	Parameters :	CString strText - The CString to check.
					CString* pstrErrorText - The error (or warning) text.
											 Only valid if the return
											 status is REC_WARNING or
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CProlog::IsBegin(CString strText, CString* pstrErrorText)
{
	const CString	strToken = ctstrPrlgBeg;
	eRecord			eRtnStatus = REC_ERROR;
	*pstrErrorText = _T("Invalid condition found in IsBegin()");
	strText.TrimLeft();
	strText.TrimRight();
	// The input text must exactly match the token string
	if (strText.CompareNoCase(strToken) == 0)
	{
		eRtnStatus = REC_VALID;
		m_bPrologBeginFound = TRUE;
	} else
	{
		// If the input text length is greater than the token string
		if(strText.GetLength() > (strToken.GetLength()))
		{
			// If the begining of the input text matches the token string (plus a blank
			// space), then there are extra characters on the line.  This is a warning.
	 		if (strText.Left(strToken.GetLength()+1).CompareNoCase(strToken + _T(" ")) == 0)
			{
				eRtnStatus = REC_WARNING;
				pstrErrorText->Format(_T("Extraneous characters after %s will be ignored."), strToken);
			}
		}
		eRtnStatus = REC_IGNORED;
	}
	return eRtnStatus;
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CProlog::IsEnd
	Description :	Determines if the the CString is a valid prolog	end
					record.  A valid prolog end record is:
					##prolog end
	Return :		eRecord	-	REC_VALID if the record is a valid prolog
								end record.
								REC_WARNING if the record appears to be a
								valid prolog end record.
								REC_IGNORE if the record is not a valid
								prolog end record.
								REC_ERROR if there is an internal error.
	Parameters :	CString strText - The CString to check.
					CString* pstrErrorText - The error (or warning) text.
											 Only valid if the return
											 status is REC_WARNING or
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CProlog::IsEnd(CString strText, CString* pstrErrorText)
{
	const CString	strToken = ctstrPrlgEnd;
	eRecord			eRtnStatus = REC_ERROR;
	*pstrErrorText = _T("Invalid condition found in IsEnd()");
	strText.TrimLeft();
	strText.TrimRight();
	// The input text must exactly match the token string
	if (strText.CompareNoCase(strToken) == 0)
	{
		eRtnStatus = REC_VALID;
		m_bPrologEndFound = TRUE;
	} else
	{
		// If the input text length is greater than the token string
		if(strText.GetLength() > (strToken.GetLength()))
		{
			// If the begining of the input text matches the token string (plus a blank
			// space), then there are extra characters on the line.  This is a warning.
	 		if (strText.Left(strToken.GetLength()+1).CompareNoCase(strToken + _T(" ")) == 0)
			{
				eRtnStatus = REC_WARNING;
				pstrErrorText->Format(_T("Extraneous characters after %s will be ignored."), strToken);
			}
		}
		eRtnStatus = REC_IGNORED;
	}
	return eRtnStatus;
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CProlog::IsDesc
	Description :	Determines if the the CString is a valid prolog
					description record.  If the CString is a valid
					description record, the associated description text is
					added to the array of the description records.
					A valid prolog description record is:
					##desc
					##desc <description text>
	Return :		eRecord	-	REC_VALID if the record is a valid prolog
								description record.
								REC_IGNORE if the record is not a valid
								prolog description record.
								REC_ERROR if a valid description was found
								but there was an error adding the text
								description to the list of description
								records.
	Parameters :	CString strText	-	The CString to check.
					CString* pstrErrorText - The error (or warning) text.
											 Only valid if the return
											 status is REC_WARNING or
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CProlog::IsDesc(CString strText, CString* pstrErrorText)
{
	const CString	strToken = ctstrDesc;
	eRecord			eRtnStatus = REC_ERROR;
	*pstrErrorText = _T("Invalid condition found in IsDesc()");
	strText.TrimLeft();
	strText.TrimRight();
	// If the input text must exactly matchs the token string 
	// we have a blank description record.
	if (strText.CompareNoCase(strToken) == 0)
		// Add a blank string to the String Array.
		//eRtnStatus = m_cPrologDesc.Add(_T(""), pstrErrorText);
		// PR000507: MSD Fixed to retrieve the whole ##desc line.
		eRtnStatus = m_cPrologDesc.Add(strText, pstrErrorText);
	else
	{
		// The input text must be greater in length than the token string
		if(strText.GetLength() > (strToken.GetLength()))
		{
			// If the begining of the input text matches the token string (plus a blank
			// space), then remove the token string and add the remainder of the text to
			// the array of description records.
	 		//if (strText.Left(strToken.GetLength()+1).CompareNoCase(strToken + _T(" ")) == 0)
			if (strText.Left(strToken.GetLength()).CompareNoCase(strToken) == 0)
			{
				//CString strNewText = strText;
				//strNewText.Delete(0, strToken.GetLength() + 1);
				//eRtnStatus = m_cPrologDesc.Add(strNewText, pstrErrorText);
				//PR000507: MSD Fixed to retrieve the entire ##desc line.
				eRtnStatus = m_cPrologDesc.Add(strText, pstrErrorText);
			}
			else {
				eRtnStatus = REC_IGNORED;
			}
		} else {
			eRtnStatus = REC_IGNORED;
		}
	}
	return eRtnStatus;
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CProlog::IsExectime
	Description :	Determines if the CString is a valid prolog exectime
					record.  If the CString is a valid exectime record,
					the associated time string is added to the exectime
					record.
					##exectime <hh:mm:ss>
					##exectime <ddd:hh:mm:ss>
	Return :		eRecord	-	REC_VALID if the record is a valid prolog
								exectime record.
								REC_IGNORE if the record is not a valid
								prolog exectime record.
								REC_ERROR if a valid description was found
								but there was an error converting the text
								time to a valid CTimeSpan object.
	Parameters :	CString strText	-	The CString to check.
					CString* pstrErrorText - The error (or warning) text.
											 Only valid if the return
											 status is REC_WARNING or
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CProlog::IsExectime(CString strText, CString* pstrErrorText)
{
	const CString	strToken = ctstrExecTime;
	eRecord			eRtnStatus = REC_ERROR;
	*pstrErrorText = _T("Invalid condition found in IsExectime()");

	strText.TrimLeft();
	strText.TrimRight();
	// If the input text exactly matchs the token string then the time
	// field is missing and we have an error.
	if (strText.CompareNoCase(strToken) == 0)
	{
		eRtnStatus             = REC_ERROR;
		*pstrErrorText         = _T("No time specified");
	}
	else
	{
		
		// The input text must be greater in length than the token string
		if(strText.GetLength() > (strToken.GetLength()))
		{
			// If the begining of the input text matches the token string (plus a blank
			// space), then remove the token string and add the remainder of the text to
			// the array of description records.
			CString strTime = ctstrExecTime;
			strTime += _T(" ");
	 		if (strText.Left(strToken.GetLength()+1).CompareNoCase(strTime) == 0)
			{
				// CString strNewText = strText;
				strText.Delete(0, strToken.GetLength() + 1);
				strText.TrimLeft();
				strText.TrimRight();
				eRtnStatus = m_cPrologExectime.SetTime(strText, pstrErrorText);
				m_bPrologExectimeFound = TRUE;
			} else {
				eRtnStatus = REC_IGNORED;
			}
		} else {
			eRtnStatus = REC_IGNORED;
		}
	}
	return eRtnStatus;
}

/*======================================================================*\
	Author :Frederick J. Shaw			Date : 08/06/00			version 1.0
  ======================================================================
	Function :		CProlog::IsStarttime
	Description :	Determines if the CString is a valid prolog starttime
					record.  If the CString is a valid starttime record,
					the associated time string is added to the starttime
					record.
					##starttime <hh:mm:ss>
					##starttime <ddd:hh:mm:ss>
	Return :		eRecord	-	REC_VALID if the record is a valid prolog
								exectime record.
								REC_IGNORE if the record is not a valid
								prolog exectime record.
								REC_ERROR if a valid description was found
								but there was an error converting the text
								time to a valid CTimeSpan object.
	Parameters :	CString strText	-	The CString to check.
					CString* pstrErrorText - The error (or warning) text.
											 Only valid if the return
											 status is REC_WARNING or
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CProlog::IsStarttime(CString strText, CString* pstrErrorText)
{
	const CString	strToken = ctstrStartTime;
	eRecord			eRtnStatus = REC_ERROR;
	*pstrErrorText = _T("Invalid condition found in IsStarttime()");
	strText.TrimLeft();
	strText.TrimRight();
	// If the input text exactly matchs the token string then the time
	// field is missing and we have an error.
	if (strText.CompareNoCase(strToken) == 0)
	{
		eRtnStatus = REC_ERROR;
		*pstrErrorText = _T("No time specified");
	}
	else
	{
		// The input text must be greater in length than the token string
		if(strText.GetLength() > (strToken.GetLength()))
		{
			// If the begining of the input text matches the token string (plus a blank
			// space), then remove the token string and add the remainder of the text to
			// the array of description records.
	 		if (strText.Left(strToken.GetLength()+1).CompareNoCase(strToken + _T(" ")) == 0)
			{
			//	CString strNewText = strText;
				strText.Delete(0, strToken.GetLength() + 1);
				strText.TrimRight();
				m_bPrologStarttimeFound = TRUE;
				eRtnStatus = m_cPrologStarttime.SetTime(strText, pstrErrorText);
			} else {
				eRtnStatus = REC_IGNORED;
			}
		} else {
			eRtnStatus = REC_IGNORED;
		}
	}
	return eRtnStatus;
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 08/06/01			version 1.0
  ======================================================================
	Function :		CProlog::IsStoptime
	Description :	Determines if the CString is a valid prolog stoptime
					record.  If the CString is a valid stoptime record,
					the associated time string is added to the stoptime
					record.
					##stoptime <hh:mm:ss>
					##stoptime <ddd:hh:mm:ss>
	Return :		eRecord	-	REC_VALID if the record is a valid prolog
								exectime record.
								REC_IGNORE if the record is not a valid
								prolog exectime record.
								REC_ERROR if a valid description was found
								but there was an error converting the text
								time to a valid CTimeSpan object.
	Parameters :	CString strText	-	The CString to check.
					CString* pstrErrorText - The error (or warning) text.
											 Only valid if the return
											 status is REC_WARNING or
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CProlog::IsStoptime(CString strText, CString* pstrErrorText)
{
	const CString	strToken = ctstrStopTime;
	eRecord			eRtnStatus = REC_ERROR;
	*pstrErrorText = _T("Invalid condition found in IsStoptime()");
	strText.TrimLeft();
	strText.TrimRight();
	// If the input text exactly matchs the token string then the time
	// field is missing and we have an error.
	if (strText.CompareNoCase(strToken) == 0)
	{
		strText.Delete(0, strToken.GetLength() + 1);
		strText.TrimRight();
		m_bPrologStoptimeFound = TRUE;
		eRtnStatus = REC_ERROR;
		*pstrErrorText = _T("No time specified");
	}
	else
	{
		// The input text must be greater in length than the token string
		if(strText.GetLength() > (strToken.GetLength()))
		{
			// If the begining of the input text matches the token string (plus a blank
			// space), then remove the token string and add the remainder of the text to
			// the array of description records.
	 		if (strText.Left(strToken.GetLength()+1).CompareNoCase(strToken + _T(" ")) == 0)
			{
				strText.Delete(0, strToken.GetLength() + 1);
				strText.TrimRight();
				m_cPrologStoptime.m_bFound  = TRUE;
				// CString strNewText = strText;
				eRtnStatus = m_cPrologStoptime.SetTime(strText, pstrErrorText);
			} else {
				eRtnStatus = REC_IGNORED;
			}
		} else {
			eRtnStatus = REC_IGNORED;
		}
	}
	return eRtnStatus;
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 05/15/01			version 1.0
  ======================================================================
	Function :		CProlog::IsParam
	Description :	Determines if the the CString is a valid prolog	param
					record.  A valid prolog param record is:
					##param name desc
	Return :		eRecord	-	REC_VALID if the record is a valid prolog
								begin record.
								REC_WARNING if the record appears to be a
								valid prolog begin record.
								REC_IGNORE if the record is not a valid
								prolog begin record.
								REC_ERROR if there was an internal error.
	Parameters :	CString strText - The CString to check.
					CString* pstrErrorText - The error (or warning) text.
											 Only valid if the return
											 status is REC_WARNING or
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CProlog::IsParam(const CString strSave, CString* pstrErrorText)
{
	const CString	strToken = ctstrParam;
	eRecord			eRtnStatus = REC_ERROR;
	*pstrErrorText = _T("Invalid condition found in IsParam()");
	CString strText(strSave);
	if (strText.GetLength() <= strToken.GetLength())
		eRtnStatus = REC_IGNORED;
	else
	{
		strText.Delete(0, strToken.GetLength());
		strText.TrimLeft();
		strText.TrimRight();
		int nPosOfSpace = strText.Find (_T(' '));

		if (!CTextFile::ScanLine (strSave, strToken)){
			eRtnStatus = REC_IGNORED;
		} 
		/* 
		else if (strSave.CompareNoCase(strToken) == 0){
			eRtnStatus = REC_ERROR;
			*pstrErrorText = _T("Invalid param directive format.");
		} */
		else {
			/*
			// If the input text length is greater than the token string
			if(strText.IsEmpty())
			{
				*pstrErrorText = _T("Invalid param directive format.");
				eRtnStatus = REC_ERROR;
			} else
			*/
			if (nPosOfSpace == -1) {
				// No description 
				strctPassedParamList *pObjParam = new strctPassedParamList; 
				m_cPrologParam.m_strParamText.Add(strText);
				pObjParam->strParamText = strText; 
				
				m_cPrologParam.m_strName.Add(strText);
				pObjParam->strName = strText;      
				
				m_cPrologParam.m_objParam.Add((CObject *)pObjParam); 
				eRtnStatus = REC_VALID;
			} else if (nPosOfSpace == strText.GetLength()){ 
				// No description but spaces
				CString strTemp = strText.Left(nPosOfSpace); 
				strctPassedParamList *pObjParam = new strctPassedParamList; 
			
				m_cPrologParam.m_strParamText.Add(strText);
				pObjParam->strParamText = strText; 

				m_cPrologParam.m_strName.Add(strText.Left(nPosOfSpace));
  			    pObjParam->strName = strText.Left(nPosOfSpace); 

				m_cPrologParam.m_objParam.Add((CObject *)pObjParam); 
				eRtnStatus = REC_VALID;
			} else {
				// Description present
				CString strTemp = strText.Left(nPosOfSpace); 
				strctPassedParamList *pObjParam = new strctPassedParamList; 

				m_cPrologParam.m_strParamText.Add(strText);
				pObjParam->strParamText = strText; 

				m_cPrologParam.m_strName.Add(strText.Left(nPosOfSpace));
  			    pObjParam->strName = strText.Left(nPosOfSpace); 
				
				strTemp = strText.Right (strText.GetLength() - nPosOfSpace - 1);
				m_cPrologParam.m_strDesc.Add(strText.Right (strText.GetLength() - nPosOfSpace - 1));
			    pObjParam->strDesc = strText.Right (strText.GetLength() - nPosOfSpace - 1);       

				m_cPrologParam.m_objParam.Add((CObject *)pObjParam); 
				eRtnStatus = REC_VALID; 
			}	
		}
	}
	return eRtnStatus;
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 05/16/01			version 1.0
  ======================================================================
	Function :		CProlog::IsValid
	Description :	Determines if the the CString is a valid prolog	param
					record.  A valid prolog valid record is:
					##valid 
	Return :		eRecord	-	REC_VALID if the record is a valid prolog
								begin record.
								REC_WARNING if the record appears to be a
								valid prolog begin record.
								REC_IGNORE if the record is not a valid
								prolog begin record.
								REC_ERROR if there was an internal error.
	Parameters :	CString strText - The CString to check.
					CString* pstrErrorText - The error (or warning) text.
											 Only valid if the return
											 status is REC_WARNING or
											 REC_ERROR.
	Note :			
			 This function copies a parsed validation directive line into 
			 the PROLOG_VALID structure.  The number of elements (m_nValidElems)
			 is initialized to zero in the CBuffer constructor. The values
			 from the strTokenArray are copied into the appropriate member
			 of the structure.  The number of elements member variable is 
			 incremented by one.  
			 A directive line is only marked as valid if it contains at 
			 least 5 tokens and these tokens must be in the correct order 
			 and type.  See the User guide for the correct format for the 
			 validation directive line.

\*======================================================================*/
eRecord CProlog::IsValid(const CString strSave, CString* pstrErrorText)
{
	CString strText(strSave);
	const CString	strToken = ctstrValid;
	eRecord			eRtnStatus = REC_ERROR;
	*pstrErrorText = _T("");
	int nPos       = 0;

	strText.TrimLeft();
	strText.TrimRight();

	/*
	if (CTextFile::ScanLine (strText, strToken))
	{
	*/

	if (strText.CompareNoCase(ctstrValid) == 0)
	{
		nPos = (strText.Find(ctstrValid));
		int nLen = _tcsclen(ctstrValid);
		strText.Delete(nPos, nLen - nPos);
		strText.TrimLeft();
		strText.TrimRight();
		m_cPrologValid.m_strValidText = strText;
		eRtnStatus = REC_VALID;
	} else if (-1 != (nPos = (strText.Find(ctstrValid))))
	{
		CStringArray strTokenArray;
		CTextFile::TokenizeString (strText, strTokenArray);
		int nElements = strTokenArray.GetSize();
		int nLen = _tcsclen(ctstrValid);
		strText.Delete(nPos, nLen - nPos);
		strText.TrimLeft();
		strText.TrimRight();
		m_cPrologValid.m_strValidText = strText;

		switch (nElements)
		{
			case 2: {
				m_cPrologValid.m_eDirType  = VALIDTYPE_DIR;
				m_cPrologValid.m_bValid    = TRUE;
				eRtnStatus                 = REC_VALID;
				
				if (CTextFile::ScanLine (strTokenArray[1], ctstrGround)){
					m_cPrologValid.m_eValidLoc = GROUND;
				} else if (CTextFile::ScanLine (strTokenArray[1], ctstrOnBoard)){
					m_cPrologValid.m_eValidLoc = ONBOARD;
				} else if (CTextFile::ScanLine (strTokenArray[1], ctstrBoth)) {
					m_cPrologValid.m_eValidLoc = BOTH;
				} else if (CTextFile::ScanLine (strTokenArray[1], ctstrRTCS)) {
					m_cPrologValid.m_eValidLoc = RTCS;
				} else {
					m_cPrologValid.m_eDirType  = VALIDQUAL_DIR;
					m_cPrologValid.m_strQual   = strTokenArray[1];
				}
				*pstrErrorText = _T("Found one parameter in valid directive");
			}
			break;
			
			case 5:{
				m_cPrologValid.m_eDirType     = VALIDSTATUS_DIR;
				m_cPrologValid.m_strName      = strTokenArray[3];
				m_cPrologValid.m_strTime      = strTokenArray[4];
				m_cPrologValid.m_bValid       = TRUE;
				eRtnStatus                    = REC_VALID;

				if (CTextFile::ScanLine (strTokenArray[1], ctstrPassed)){
					m_cPrologValid.m_eValidStatus = PASSED;

					if (CTextFile::ScanLine (strTokenArray[2], ctstrGround)){
						m_cPrologValid.m_eValidLoc = GROUND;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrOnBoard)){
						m_cPrologValid.m_eValidLoc = ONBOARD;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrBoth)){
						m_cPrologValid.m_eValidLoc = BOTH;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrRTCS)){
						m_cPrologValid.m_eValidLoc = RTCS;
					} 
				} else if (CTextFile::ScanLine (strTokenArray[1], ctstrWarning)){
					m_cPrologValid.m_eValidStatus  = WARNING;
	
					if (CTextFile::ScanLine (strTokenArray[2], ctstrGround)){
						m_cPrologValid.m_eValidLoc = GROUND;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrOnBoard)){
						m_cPrologValid.m_eValidLoc = ONBOARD;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrBoth)){
						m_cPrologValid.m_eValidLoc = BOTH;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrRTCS)){
						m_cPrologValid.m_eValidLoc = RTCS;
					} 
				} else if (CTextFile::ScanLine (strTokenArray[1], ctstrFail)){
					m_cPrologValid.m_eValidStatus = FAIL;
					
					if (CTextFile::ScanLine (strTokenArray[2], ctstrGround)){
						m_cPrologValid.m_eValidLoc = GROUND;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrOnBoard)){
						m_cPrologValid.m_eValidLoc = ONBOARD;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrBoth)){
						m_cPrologValid.m_eValidLoc = BOTH;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrRTCS)){
						m_cPrologValid.m_eValidLoc = RTCS;
					} 
				} else if (CTextFile::ScanLine (strTokenArray[1], ctstrUnvalidated)){
					m_cPrologValid.m_eValidStatus = UNVALIDATED;
					
					if (CTextFile::ScanLine (strTokenArray[2], ctstrGround)){
						m_cPrologValid.m_eValidLoc = GROUND;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrOnBoard)){
						m_cPrologValid.m_eValidLoc = ONBOARD;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrBoth)){
						m_cPrologValid.m_eValidLoc = BOTH;
					} else if (CTextFile::ScanLine (strTokenArray[2], ctstrRTCS)){
						m_cPrologValid.m_eValidLoc = RTCS;
					} 
				}
			}
			break;
	
			default:{
				*pstrErrorText = _T("Found 4 parameters in valid directive.");
			}
			break;

		}
	} else  {
		eRtnStatus = REC_IGNORED;
	}
	return eRtnStatus;
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 05/16/01			version 1.0
  ======================================================================
	Function :		CProlog::IsUpdate
	Description :	Determines if the CString is a valid prolog update
					record.  If the CString is a valid update record,
					
					
					
	Return :		eRecord	-	REC_VALID if the record is a valid prolog
								exectime record.
								REC_IGNORE if the record is not a valid
								prolog exectime record.
								REC_ERROR if a valid description was found
								but there was an error converting the text
								time to a valid CTimeSpan object.
	Parameters :	CString strText	-	The CString to check.
					CString* pstrErrorText - The error (or warning) text.
											 Only valid if the return
											 status is REC_WARNING or
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CProlog::IsUpdate(CString strText, CString* pstrErrorText)
{
	const CString	strToken = ctstrUpdate;
	eRecord			eRtnStatus = REC_ERROR;
	*pstrErrorText = _T("Invalid condition found in IsUpdate()");
	strText.TrimLeft();
	strText.TrimRight();
	// The input text must exactly match the token string 
	// we have a blank description record.
	if (strText.CompareNoCase(strToken) == 0){
		eRtnStatus = m_cPrologUpdate.Add(_T(""), pstrErrorText);
		m_bPrologUpdateFound	= TRUE;
	}else {
		// The input text must be greater in length than the token string
		if(strText.GetLength() > (strToken.GetLength()))
		{
			// If the begining of the input text matches the token string (plus a blank
			// space), then remove the token string and add the remainder of the text to
			// the array of description records.
	 		if (strText.Left(strToken.GetLength()+1).CompareNoCase(strToken + _T(" ")) == 0)
			{
				CString strNewText = strText;
				strNewText.Delete(0, strToken.GetLength() + 1);
				eRtnStatus = m_cPrologUpdate.Add(strNewText, pstrErrorText);
				m_bPrologUpdateFound	= TRUE;
			}
			else
				eRtnStatus = REC_IGNORED;
		}
		else
			eRtnStatus = REC_IGNORED;
	}
	return eRtnStatus;
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 06/24/2002			version 1.0
  ======================================================================
	Function :		CProlog::IsOverRide
	Description :	Determines if the the CString is an override directive	
					record.  
					##override 
	Return :		eRecord	-	REC_VALID if the record is a valid directive
								REC_WARNING if the record appears to be a
								valid directive.
								REC_IGNORE if the record is not a valid
								directive.
								REC_ERROR if there was an internal error.
	Parameters :	CString strText - The CString to check.
					CString* pstrErrorText - The error (or warning) text.
											 Only valid if the return
											 status is REC_WARNING or
											 REC_ERROR.
	Note :			
\*======================================================================*/
eRecord CProlog::IsOverRide(CString strSave, CString* pstrErrorText)
{
	CString strText(strSave);
	CString	strToken = ctstrOVERRIDE;
	eRecord			eRtnStatus = REC_ERROR;
	*pstrErrorText = _T("");
	strText.TrimLeft();
	strText.TrimRight();
	strSave.TrimLeft();
	strSave.TrimRight();

	if (CTextFile::ScanLine (strText, strToken))
	{
		m_bPrologOverRideFound = TRUE;
		int nPos       = 0;
		strText.MakeUpper();
		strToken.MakeUpper();
		nPos = (strText.Find(strToken));
		strSave.Delete(nPos, strToken.GetLength() - nPos);
		strSave.TrimLeft();
		strSave.TrimRight();
		m_cPrologOverRide.m_strOverRideText = strSave;
		if (!strSave.IsEmpty()){
			m_cPrologOverRide.m_bSet = TRUE;
		}
		eRtnStatus   = REC_VALID;

	} else  {
		eRtnStatus = REC_IGNORED;
	}
	return eRtnStatus;
}

/////////////////////////////////////////////////////////////////////////
// CPrologDesc Class
/////////////////////////////////////////////////////////////////////////
/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CPrologDesc::CPrologDesc
	Description :	Class constructor.  Mark the valid flag as being
					FALSE.
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologDesc::CPrologDesc()
{
	m_bValid = FALSE;
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CPrologDesc::~CPrologDesc
	Description :	Class descructor.
	Return :		destructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologDesc::~CPrologDesc(){}

/*======================================================================*\
	Author :Frederikc J. Shaw	Date : 04/19/2001		version 1.0
  ======================================================================
	Function :		CPrologDesc::CPrologDesc(const CPrologDesc & Other)
	Description :	Copy Constructor
	Return :		constructor
	Parameters :	const CPrologDesc & Other.
	Note :			None.
\*======================================================================*/
CPrologDesc::CPrologDesc(const CPrologDesc & Other){

	m_bValid  = Other.m_bValid;
	m_strDescText.Copy(Other.m_strDescText);
}
/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CPrologDesc::operator= 
	Description :	Assignment operator
	Return :		pointer to new class
	Parameters :	const CPrologDesc & Other.
	Note :			None.
\*======================================================================*/
CPrologDesc & CPrologDesc::operator=(const CPrologDesc & Other){

	if (this == & Other)  // object assigned to itself
		return *this; // Nothing to do.

	m_bValid = Other.m_bValid;
	m_strDescText.Copy(Other.m_strDescText);
	return *this;
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		AddDir(CString strText, CString* pstrErrorText)	
	Description :	
	Return :        BOOL 		
	Parameters :	CString strText, CString* pstrErrorText
	Note :			None.
\*======================================================================*/
	BOOL CPrologDesc::AddDir(CString strText, CString* pstrErrorText)
{
	BOOL bReturn_Status = TRUE;
//	CString       strErrorText;
//	CString       strDescText = strText;
	Add(strText, pstrErrorText);

	return bReturn_Status;
}


#ifdef _DEBUG
/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CPrologDesc::Dump
	Description :	Dumps the prolog description records.
	Return :		void
	Parameters :	None.
	Note :			Only used if _DEBUG is defined
\*======================================================================*/
void CPrologDesc::Dump()
{
	TRACE0("  Dumping the Description records...\n");
	if (IsValid())
	{
		int nMax = m_strDescText.GetSize();

		if (nMax == 0)
			TRACE0("    No Description records found\n");
		else
		{
			for (int nIndex=0; nIndex<nMax; nIndex++)
			{
				TRACE1("    Desc     : %s\n", m_strDescText[nIndex]);
			}
		}
	}
	else
		TRACE0("    Description record is invalid\n");
	TRACE0("  Description records dump complete\n");
}
#endif

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CPrologDesc::Add
	Description :	Attempt to add a CString the array or description
					records.
	Return :		eRecord	-	REC_VALID if the text was added.
								REC_ERROR if there was an error adding
								the text (CMemoryException).
	Parameters :	CString strText - CString to add.
					CString* pstrErrorText - The error text.  Only valid
											 if the return status is
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CPrologDesc::Add(CString strText, CString* pstrErrorText)
{
	eRecord eRtnStatus;
	TRY
	{
		m_strDescText.Add(strText);
		m_bValid = TRUE;
		eRtnStatus = REC_VALID;
	}
	CATCH (CMemoryException, pE)
	{
		TCHAR szCause[255];
		eRtnStatus = REC_ERROR;
		pE->GetErrorMessage(szCause, 255);
		*pstrErrorText = szCause;
	}
	END_CATCH
	return eRtnStatus;
}


/////////////////////////////////////////////////////////////////////////
// CPrologExectime Class
/////////////////////////////////////////////////////////////////////////
/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CPrologExectime::CPrologExectime
	Description :	Class constructor.  Mark the valid flag as being
					FALSE.
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologExectime::CPrologExectime()
{
	m_bValid = FALSE;
	m_bFound = false;
	m_cTimeSpan = CTimeSpan(0, 0, 0,0);
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CPrologExectime::~CPrologExectime
	Description :	Class descructor.
	Return :		destructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologExectime::~CPrologExectime(){}


/*======================================================================*\
	Author :Frederick J. Shaw		Date : 04/19/2001	version 1.0
  ======================================================================
	Function :		CPrologExectime::CPrologExectime(const CPrologExectime & Other)
	Description :	Copy Constructor
	Return :		constructor
	Parameters :	CPrologExectime class
	Note :			None.
\*======================================================================*/
CPrologExectime::CPrologExectime(const CPrologExectime & Other){
	m_bValid    = Other.m_bValid;
	m_cTimeSpan = Other.m_cTimeSpan;
	m_bFound    = Other.m_bFound;
}

	/*======================================================================*\
	Author :Frederick J. Shaw	Date : 04/19/2001	  version 1.0
  ======================================================================
	Function :		CPrologExectime::operator=
	Description :	assignment operator
	Return :		CPrologExectime class
	Parameters :	CPrologExectime class.
	Note :			None.
\*======================================================================*/
CPrologExectime & CPrologExectime::operator=(const CPrologExectime & Other){

if (this == &Other)  // object assigned to itself
	return *this; // Nothing to do.

	m_bValid    = Other.m_bValid;
	m_cTimeSpan = Other.m_cTimeSpan;
	m_bFound    = Other.m_bFound;
	return *this;
}

#ifdef _DEBUG
/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CPrologExectime::Dump
	Description :	Dumps the prolog Exectime record.
	Return :		void
	Parameters :	None.
	Note :			Only used if _DEBUG is defined
\*======================================================================*/
void CPrologExectime::Dump()
{	
	TRACE0("  Dumping the Exectime record...\n");
	if (IsValid())
	{
		CString strText = m_cTimeSpan.Format(_T("%D-%H:%M:%S\n"));
		TRACE1("    Exectime : %s", strText);
	}
	else
		TRACE0("    Exectime record is invalid\n");
	TRACE0("  Exectime record dump complete\n");
}
#endif

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CPrologExectime::SetTime
	Description :	Attempt to set the exectime.
	Return :		eRecord - REC_VALID if the time passed in is a valid
							  CTimeSpan.
							  REC_ERROR if the time passed in is not a
							  valid CTimeSpan.
	Parameters :	CString strText	- CString to convert to a CTimeSpan
									  object and add to exectime record.
					CString* pstrErrorText - The error text.  Only valid
											 if the return status is
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CPrologExectime::SetTime(CString strText, CString* pstrErrorText)
{
	eRecord eRtnStatus;
	strText.TrimLeft();
	strText.TrimRight();

	int nlength = strText.GetLength();

	// Check for ddd-hh:mm:ss version
	if (nlength == 12)
	{
		if ((strText[3] != _T('-')) || (strText[6] != _T(':')) || (strText[9] != _T(':')))
		{
			eRtnStatus = REC_ERROR;
			pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
		}
		else
		{
			CString strDay	= strText.Mid(0,3);
			CString strHour	= strText.Mid(4,2);
			CString strMin	= strText.Mid(7,2);
			CString strSec	= strText.Mid(10,2);
			if ((strDay[0] < _T('0'))  || (strDay[0] > _T('9'))  ||
				(strDay[1] < _T('0'))  || (strDay[1] > _T('9'))  ||
				(strDay[2] < _T('0'))  || (strDay[2] > _T('9'))  ||
				(strHour[0] < _T('0')) || (strHour[0] > _T('2')) ||
				((strHour[0] == _T('0')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('1')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('2')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('3')))) ||
				(strMin[0] < _T('0')) || (strMin[0] > _T('5')) ||
				(strMin[1] < _T('0')) || (strMin[1] > _T('9')) ||
				(strSec[0] < _T('0')) || (strSec[0] > _T('5')) ||
				(strSec[1] < _T('0')) || (strSec[1] > _T('9')))
			{
				eRtnStatus = REC_ERROR;
				pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
			}
			else
			{
				eRtnStatus = REC_VALID;
				m_cTimeSpan = CTimeSpan(_ttoi(strDay), _ttoi(strHour), _ttoi(strMin), _ttoi(strSec));
			}
		}
	}
	// Check for hh:mm:ss version
	else if (nlength == 8)
	{
		if ((strText[2] != _T(':')) || (strText[5] != _T(':')))
		{
			eRtnStatus = REC_ERROR;
			pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
		}
		else
		{
			CString strHour	= strText.Mid(0,2);
			CString strMin	= strText.Mid(3,2);
			CString strSec	= strText.Mid(6,2);
			if ((strHour[0] < _T('0')) || (strHour[0] > _T('2')) ||
				((strHour[0] == _T('0')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('1')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('2')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('3')))) ||
				(strMin[0] < _T('0')) || (strMin[0] > _T('5')) ||
				(strMin[1] < _T('0')) || (strMin[1] > _T('9')) ||
				(strSec[0] < _T('0')) || (strSec[0] > _T('5')) ||
				(strSec[1] < _T('0')) || (strSec[1] > _T('9')))
			{
				eRtnStatus = REC_ERROR;
				pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
			}
			else
			{
				eRtnStatus = REC_VALID;
				m_cTimeSpan = CTimeSpan(0, _ttoi(strHour), _ttoi(strMin), _ttoi(strSec));
			}
		}
	}
	else
	{
		eRtnStatus = REC_VALID;
		pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
	}

	return eRtnStatus;
}

/*======================================================================*\
	Author :Frederick J. Shaw			Date : 08/07/2001		version 1.0
  ======================================================================
	Function :		CPrologExectime::AddDir
	Description :	Adds a execute time directive to prolog.
	Return :		BOOL
	Parameters :	CTimeSpan cTimeSpan.
	Note :			
\*======================================================================*/
BOOL CPrologExectime::AddDir(const CString strText){
	BOOL bReturn_Status = TRUE;
	m_bValid = TRUE;
	m_bFound = TRUE;
	// m_cTimeSpan = cTimeSpan;
	CString pstrErrorText;
	SetTime(strText, &pstrErrorText);

	return bReturn_Status;
}


/////////////////////////////////////////////////////////////////////////
// CPrologStarttime Class
/////////////////////////////////////////////////////////////////////////
/*======================================================================*\
	Author :Frederick J. Shaw			Date : 08/07/2001		version 1.0
  ======================================================================
	Function :		CPrologStarttime::CPrologStarttime
	Description :	Class constructor.  Mark the valid flag as being
					FALSE.
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologStarttime::CPrologStarttime()
{
	m_bValid = FALSE;
	m_bFound =  false;
	m_cTimeSpan = CTimeSpan(0, 0, 0,0);
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 08/07/2001	version 1.0
  ======================================================================
	Function :		CPrologStarttime::~CPrologStarttime
	Description :	Class descructor.
	Return :		destructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologStarttime::~CPrologStarttime(){}


/*======================================================================*\
	Author :Frederick J. Shaw		Date : 08/07/2001	version 1.0
  ======================================================================
	Function :		CPrologStarttime::CPrologStarttime(const CPrologStarttime & Other)
	Description :	Copy Constructor
	Return :		constructor
	Parameters :	CPrologStarttime class
	Note :			None.
\*======================================================================*/
CPrologStarttime::CPrologStarttime(const CPrologStarttime & Other){
	m_bValid    = Other.m_bValid;
	m_cTimeSpan = Other.m_cTimeSpan;
	m_bFound    = Other.m_bFound;
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 08/07/2001	  version 1.0
======================================================================
	Function :		CPrologStarttime::operator=
	Description :	assignment operator
	Return :		CPrologStarttime class
	Parameters :	CPrologStarttime class.
	Note :			None.
\*======================================================================*/
CPrologStarttime & CPrologStarttime::operator=(const CPrologStarttime & Other){

if (this == &Other)  // object assigned to itself
	return *this; // Nothing to do.

	m_bValid    = Other.m_bValid;
	m_cTimeSpan = Other.m_cTimeSpan;
	m_bFound    = Other.m_bFound;
	return *this;
}

#ifdef _DEBUG
/*======================================================================*\
	Author :Frederick J. Shaw			Date : 08/07/2001		version 1.0
  ======================================================================
	Function :		CPrologStarttime::Dump
	Description :	Dumps the prolog Exectime record.
	Return :		void
	Parameters :	None.
	Note :			Only used if _DEBUG is defined
\*======================================================================*/
void CPrologStarttime::Dump()
{	
	TRACE0("  Dumping the Starttime record...\n");
	if (IsValid())
	{
		CString strText = m_cTimeSpan.Format(_T("%D-%H:%M:%S\n"));
		TRACE1("    Starttime : %s", strText);
	}
	else
		TRACE0("    Starttime record is invalid\n");
	TRACE0("  Starttime record dump complete\n");
}
#endif

/*======================================================================*\
	Author :Frederick J. Shaw			Date : 08/07/2001		version 1.0
  ======================================================================
	Function :		CPrologStarttime::SetTime
	Description :	Attempt to set the exectime.
	Return :		eRecord - REC_VALID if the time passed in is a valid
							  CTimeSpan.
							  REC_ERROR if the time passed in is not a
							  valid CTimeSpan.
	Parameters :	CString strText	- CString to convert to a CTimeSpan
									  object and add to exectime record.
					CString* pstrErrorText - The error text.  Only valid
											 if the return status is
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CPrologStarttime::SetTime(CString strText, CString* pstrErrorText)
{
	eRecord eRtnStatus;
	strText.TrimLeft();
	strText.TrimRight();

	int nlength = strText.GetLength();

	// Check for ddd-hh:mm:ss version
	if (nlength == 12)
	{
		if ((strText[3] != _T('-')) || (strText[6] != _T(':')) || (strText[9] != _T(':')))
		{
			eRtnStatus = REC_ERROR;
			pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
		}
		else
		{
			CString strDay	= strText.Mid(0,3);
			CString strHour	= strText.Mid(4,2);
			CString strMin	= strText.Mid(7,2);
			CString strSec	= strText.Mid(10,2);
			if ((strDay[0] < _T('0'))  || (strDay[0] > _T('9'))  ||
				(strDay[1] < _T('0'))  || (strDay[1] > _T('9'))  ||
				(strDay[2] < _T('0'))  || (strDay[2] > _T('9'))  ||
				(strHour[0] < _T('0')) || (strHour[0] > _T('2')) ||
				((strHour[0] == _T('0')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('1')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('2')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('3')))) ||
				(strMin[0] < _T('0')) || (strMin[0] > _T('5')) ||
				(strMin[1] < _T('0')) || (strMin[1] > _T('9')) ||
				(strSec[0] < _T('0')) || (strSec[0] > _T('5')) ||
				(strSec[1] < _T('0')) || (strSec[1] > _T('9')))
			{
				eRtnStatus = REC_ERROR;
				pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
			}
			else
			{
				eRtnStatus = REC_VALID;
				m_cTimeSpan = CTimeSpan(_ttoi(strDay), _ttoi(strHour), _ttoi(strMin), _ttoi(strSec));
			}
		}
	}
	// Check for hh:mm:ss version
	else if (nlength == 8)
	{
		if ((strText[2] != _T(':')) || (strText[5] != _T(':')))
		{
			eRtnStatus = REC_ERROR;
			pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
		}
		else
		{
			CString strHour	= strText.Mid(0,2);
			CString strMin	= strText.Mid(3,2);
			CString strSec	= strText.Mid(6,2);
			if ((strHour[0] < _T('0')) || (strHour[0] > _T('2')) ||
				((strHour[0] == _T('0')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('1')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('2')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('3')))) ||
				(strMin[0] < _T('0')) || (strMin[0] > _T('5')) ||
				(strMin[1] < _T('0')) || (strMin[1] > _T('9')) ||
				(strSec[0] < _T('0')) || (strSec[0] > _T('5')) ||
				(strSec[1] < _T('0')) || (strSec[1] > _T('9')))
			{
				eRtnStatus = REC_ERROR;
				pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
			}
			else
			{
				eRtnStatus = REC_VALID;
				m_cTimeSpan = CTimeSpan(0, _ttoi(strHour), _ttoi(strMin), _ttoi(strSec));
			}
		}
	}
	else
	{
		eRtnStatus = REC_VALID;
		pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
	}

	return eRtnStatus;
}

/*======================================================================*\
	Author :Frederick J. Shaw			Date : 08/07/2001		version 1.0
  ======================================================================
	Function :		CPrologStarttime::AddDir
	Description :	Adds a start time directive to prolog.
	Return :		BOOL
	Parameters :	CTimeSpan cTimeSpan.
	Note :			
\*======================================================================*/
BOOL CPrologStarttime::AddDir(const CString strText){
	BOOL bReturn_Status = TRUE;
	m_bValid = TRUE;
	m_bFound = TRUE;
	// m_cTimeSpan = cTimeSpan;
	CString pstrErrorText;
	SetTime(strText, &pstrErrorText);

	return bReturn_Status;
}


/////////////////////////////////////////////////////////////////////////
// CPrologStoptime Class
/////////////////////////////////////////////////////////////////////////
/*======================================================================*\
	Author :Frederick J. Shaw			Date : 08/07/2001		version 1.0
  ======================================================================
	Function :		CPrologStoptime::CPrologStoptime
	Description :	Class constructor.  Mark the valid flag as being
					FALSE.
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologStoptime::CPrologStoptime()
{
	m_bValid = FALSE;
	m_bFound = false;
	m_cTimeSpan = CTimeSpan(0, 0, 0,0);
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 08/07/2001	version 1.0
  ======================================================================
	Function :		CPrologStoptime::~CPrologStoptime
	Description :	Class destructor.
	Return :		destructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologStoptime::~CPrologStoptime(){}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 08/07/2001	version 1.0
  ======================================================================
	Function :		CPrologStoptime::CPrologStoptime(const CPrologStoptime & Other)
	Description :	Copy Constructor
	Return :		constructor
	Parameters :	CPrologStoptime class
	Note :			None.
\*======================================================================*/
CPrologStoptime::CPrologStoptime(const CPrologStoptime & Other){
	m_bValid    = Other.m_bValid;
	m_cTimeSpan = Other.m_cTimeSpan;
	m_bFound    = Other.m_bFound;
}

/*======================================================================*\
	Author :Frederick J. Shaw		Date : 08/07/2001	  version 1.0
======================================================================
	Function :		CPrologStoptime::operator=
	Description :	assignment operator
	Return :		CPrologStoptime class
	Parameters :	CPrologStoptime class.
	Note :			None.
\*======================================================================*/
CPrologStoptime & CPrologStoptime::operator=(const CPrologStoptime & Other){

if (this == &Other)  // object assigned to itself
	return *this; // Nothing to do.

	m_bValid    = Other.m_bValid;
	m_cTimeSpan = Other.m_cTimeSpan;
	m_bFound    = Other.m_bFound;
	return *this;
}

#ifdef _DEBUG
/*======================================================================*\
	Author :Frederick J. Shaw			Date : 08/07/2001		version 1.0
  ======================================================================
	Function :		CPrologStoptime::Dump
	Description :	Dumps the prolog Stoptime record.
	Return :		void
	Parameters :	None.
	Note :			Only used if _DEBUG is defined
\*======================================================================*/
void CPrologStoptime::Dump()
{	
	TRACE0("  Dumping the Stoptime record...\n");
	if (IsValid())
	{
		CString strText = m_cTimeSpan.Format(_T("%D-%H:%M:%S\n"));
		TRACE1("    Stoptime : %s", strText);
	}
	else
		TRACE0("    Stoptime record is invalid\n");
	TRACE0("  Stoptime record dump complete\n");
}
#endif

/*======================================================================*\
	Author :Frederick J. Shaw			Date : 08/07/2001		version 1.0
  ======================================================================
	Function :		CPrologStoptime::SetTime
	Description :	Attempt to set the exectime.
	Return :		eRecord - REC_VALID if the time passed in is a valid
							  CTimeSpan.
							  REC_ERROR if the time passed in is not a
							  valid CTimeSpan.
	Parameters :	CString strText	- CString to convert to a CTimeSpan
									  object and add to exectime record.
					CString* pstrErrorText - The error text.  Only valid
											 if the return status is
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CPrologStoptime::SetTime(CString strText, CString* pstrErrorText)
{
	eRecord eRtnStatus;
	strText.TrimLeft();
	strText.TrimRight();

	int nlength = strText.GetLength();

	// Check for ddd-hh:mm:ss version
	if (nlength == 12)
	{
		if ((strText[3] != _T('-')) || (strText[6] != _T(':')) || (strText[9] != _T(':')))
		{
			eRtnStatus = REC_ERROR;
			pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
		}
		else
		{
			CString strDay	= strText.Mid(0,3);
			CString strHour	= strText.Mid(4,2);
			CString strMin	= strText.Mid(7,2);
			CString strSec	= strText.Mid(10,2);
			if ((strDay[0] < _T('0'))  || (strDay[0] > _T('9'))  ||
				(strDay[1] < _T('0'))  || (strDay[1] > _T('9'))  ||
				(strDay[2] < _T('0'))  || (strDay[2] > _T('9'))  ||
				(strHour[0] < _T('0')) || (strHour[0] > _T('2')) ||
				((strHour[0] == _T('0')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('1')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('2')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('3')))) ||
				(strMin[0] < _T('0')) || (strMin[0] > _T('5')) ||
				(strMin[1] < _T('0')) || (strMin[1] > _T('9')) ||
				(strSec[0] < _T('0')) || (strSec[0] > _T('5')) ||
				(strSec[1] < _T('0')) || (strSec[1] > _T('9')))
			{
				eRtnStatus = REC_ERROR;
				pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
			}
			else
			{
				eRtnStatus = REC_VALID;
				m_cTimeSpan = CTimeSpan(_ttoi(strDay), _ttoi(strHour), _ttoi(strMin), _ttoi(strSec));
			}
		}
	}
	// Check for hh:mm:ss version
	else if (nlength == 8)
	{
		if ((strText[2] != _T(':')) || (strText[5] != _T(':')))
		{
			eRtnStatus = REC_ERROR;
			pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
		}
		else
		{
			CString strHour	= strText.Mid(0,2);
			CString strMin	= strText.Mid(3,2);
			CString strSec	= strText.Mid(6,2);
			if ((strHour[0] < _T('0')) || (strHour[0] > _T('2')) ||
				((strHour[0] == _T('0')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('1')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('9')))) ||
				((strHour[0] == _T('2')) && (((strHour[1]) < _T('0')) || (strHour[1] > _T('3')))) ||
				(strMin[0] < _T('0')) || (strMin[0] > _T('5')) ||
				(strMin[1] < _T('0')) || (strMin[1] > _T('9')) ||
				(strSec[0] < _T('0')) || (strSec[0] > _T('5')) ||
				(strSec[1] < _T('0')) || (strSec[1] > _T('9')))
			{
				eRtnStatus = REC_ERROR;
				pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
			}
			else
			{
				eRtnStatus = REC_VALID;
				m_cTimeSpan = CTimeSpan(0, _ttoi(strHour), _ttoi(strMin), _ttoi(strSec));
			}
		}
	}
	else
	{
		eRtnStatus = REC_VALID;
		pstrErrorText->Format(_T("Invalid time format <%s>"), strText);
	}

	return eRtnStatus;
}

/*======================================================================*\
	Author :Frederick J. Shaw			Date : 08/07/2001		version 1.0
  ======================================================================
	Function :		CPrologStoptime::
	Description :	Adds a stop time directive to the prolog.
	Return :		BOOL
	Parameters :	
	Note :			
\*======================================================================*/
BOOL CPrologStoptime::AddDir(const CString strText){

	BOOL bReturn_Status = TRUE;
	m_bValid = TRUE;
	m_bFound = TRUE;
	// m_cTimeSpan = cTimeSpan;

	CString pstrErrorText;
	SetTime(strText, &pstrErrorText);
	return bReturn_Status;
}

/////////////////////////////////////////////////////////////////////////
//	CPrologValid Class
/////////////////////////////////////////////////////////////////////////
/*======================================================================*\
	Author: Frederick J. Shaw		Date : 05/16/01		version 1.0
  ======================================================================
	Function :		CPrologValid::CPrologValid()
	Description :	Class constructor.  Mark the valid flag as being
					FALSE.
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologValid::CPrologValid()
{
	m_bValid = FALSE;
	m_eValidLoc     = INVALID_TYPE;
	m_eValidStatus  = INVALID_STATUS;
	m_eDirType      = INVALID_DIR;
}

/*======================================================================*\
	Author: Frederick J. Shaw		Date : 05/16/01		version 1.0
  ======================================================================
	Function :		CPrologValid::~CPrologValid()
	Description :	Class descructor.
	Return :		destructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologValid::~CPrologValid(){}

/*======================================================================*\
	Author :Frederikc J. Shaw	Date : 04/19/2001		version 1.0
  ======================================================================
	Function :		CPrologValid::CPrologValid(const CPrologValid & Other)
	Description :	Copy Constructor
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologValid::CPrologValid(const CPrologValid & Other){
	m_bValid        = Other.m_bValid;
	m_strValidText  = Other.m_strValidText;
	m_eValidLoc     = Other.m_eValidLoc;
	m_eValidStatus  = Other.m_eValidStatus;
	m_eDirType      = Other.m_eDirType;
	m_strName       = Other.m_strName;
	m_strTime       = Other.m_strTime;
	m_strQual       = Other.m_strQual;
}
/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		CPrologValid::operator= 
	Description :	Assignment operator
	Return :		pointer to new class
	Parameters :	Values to assign to class.
	Note :			None.
\*======================================================================*/
CPrologValid & CPrologValid::operator=(const CPrologValid & Other){

	if (this == & Other)  // object assigned to itself
		return *this; // Nothing to do.

	m_bValid       = Other.m_bValid;
	m_strValidText = Other.m_strValidText;
	m_eValidLoc    = Other.m_eValidLoc;
	m_eValidStatus = Other.m_eValidStatus;
	m_eDirType     = Other.m_eDirType;
	m_strName      = Other.m_strName;
	m_strTime      = Other.m_strTime;
	m_strQual      = Other.m_strQual;
	return *this;
}

#ifdef _DEBUG
/*======================================================================*\
	Author :Frederick J. Shaw		Date : 05/16/2001		version 1.0
  ======================================================================
	Function :		CPrologValid::Dump
	Description :	Dumps the prolog validation records.
	Return :		void
	Parameters :	None.
	Note :			Only used if _DEBUG is defined
\*======================================================================*/
void CPrologValid::Dump()
{
	TRACE0("  Dumping the Validation records...\n");
	if (IsValid())
	{
		TRACE1("    Validation Status   : %s\n", m_strValidText);
	}
	else{
		TRACE0("    Validation record is invalid\n");
	}
	TRACE0("  Validation records dump complete\n");
}
#endif

/*======================================================================*\
	Author: Frederick J. Shaw			Date : 05/16/01		version 1.0
  ======================================================================
	Function :		 IsFileValid()
	Description :	
					
	Return :	BOOL		
							
	Parameters : enum eValidStatus &eVStatus	
					
	Note :			None.
\*======================================================================*/

BOOL CPrologValid::IsFileValid(enum eValidStatus &eVStatus)
{
	eVStatus = m_eValidStatus;
	return m_bValid;
} 

/*======================================================================*\
	Author: Frederick J. Shaw			Date : 05/16/01		version 1.0
  ======================================================================
	Function :		 IsFileValid(enum eValidType &eVType)
	Description :	
					
	Return :	BOOL		
							
	Parameters : enum eValidType &eVType	
					
	Note :			None.
\*======================================================================*/
BOOL CPrologValid::IsFileValid(enum eValidLoc &eVLoc)
{
	eVLoc = m_eValidLoc;
	return m_bValid;
} 

/*======================================================================*\
	Author: Frederick J. Shaw			Date : 05/16/01		version 1.0
  ======================================================================
	Function :		 IsFileValid(enum eValidStatus &eVStatus, enum eValidType &eVType)
	Description :	
					
	Return :	BOOL		
							
	Parameters : enum eValidStatus &eVStatus, enum eValidType &eVType
					
	Note :			None.
\*======================================================================*/	
BOOL CPrologValid::IsFileValid(enum eValidStatus &eVStatus, enum eValidLoc &eVLoc)
{
	eVStatus = m_eValidStatus;
	eVLoc = m_eValidLoc;
	return m_bValid;
} 

/////////////////////////////////////////////////////////////////////////
// CPrologUpdate Class
/////////////////////////////////////////////////////////////////////////
/*======================================================================*\
	Author: Frederick J. Shaw		Date : 05/16/01		version 1.0
  ======================================================================
	Function :		CPrologUpdate::CPrologUpdate()
	Description :	Class constructor.  Mark the valid flag as being
					FALSE.
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologUpdate::CPrologUpdate()
{
	m_bValid = FALSE;
}

/*======================================================================*\
	Author: Frederick J. Shaw		Date : 05/16/01		version 1.0
  ======================================================================
	Function :		CPrologUpdate::~CPrologUpdate()
	Description :	Class destructor.
	Return :		destructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologUpdate::~CPrologUpdate(){}

/*======================================================================*\
	Author :Frederick J. Shaw	Date : 05/16/2001		version 1.0
  ======================================================================
	Function :		CPrologValid::CPrologValid(const CPrologValid & Other)
	Description :	Copy Constructor
	Return :		constructor
	Parameters :	const CPrologValid & Other.
	Note :			None.
\*======================================================================*/
CPrologUpdate::CPrologUpdate(const CPrologUpdate & Other){
	m_bValid = Other.m_bValid;
	m_strUpdateText.Copy(Other.m_strUpdateText);
}
/*======================================================================*\
	Author: Frederick J. Shaw		Date : 05/16/01			version 1.0
  ======================================================================
	Function :		CPrologUpdate::operator= 
	Description :	Assignment operator
	Return :		pointer to new class
	Parameters :	const CPrologUpdate & Other.
	Note :			None.
\*======================================================================*/
CPrologUpdate & CPrologUpdate::operator=(const CPrologUpdate & Other){

	if (this == & Other)  // object assigned to itself
		return *this; // Nothing to do.

	m_bValid = Other.m_bValid;
	m_strUpdateText.Copy(Other.m_strUpdateText);
	return *this;
}

#ifdef _DEBUG
/*======================================================================*\
	Author :Frederick J. Shaw		Date : 05/16/2001		version 1.0
  ======================================================================
	Function :		CPrologUpdate::Dump
	Description :	Dumps the prolog validation records.
	Return :		void
	Parameters :	None.
	Note :			Only used if _DEBUG is defined
\*======================================================================*/
void CPrologUpdate::Dump()
{
	TRACE0("  Dumping the Update record...\n");
	if (IsValid())
	{
		TRACE1("    Update Status : %s\n", m_strUpdateText);
	}
	else{
		TRACE0("    Update record is invalid\n");
	}
	TRACE0("  Update record dump complete\n");
}
#endif

/*======================================================================*\
	Author : Frederick J. Shaw			Date : 05/16/01		version 1.0
  ======================================================================
	Function :		CPrologUpdate::Add
	Description :	Attempt to add a CString the array or description
					records.
	Return :		eRecord	-	REC_VALID if the text was added.
								REC_ERROR if there was an error adding
								the text (CMemoryException).
	Parameters :	CString strText - CString to add.
					CString* pstrErrorText - The error text.  Only valid
											 if the return status is
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CPrologUpdate::Add(CString strText, CString* pstrErrorText)
{
	eRecord eRtnStatus;
	TRY
	{
		m_strUpdateText.Add(strText);
		m_bValid = TRUE;
		eRtnStatus = REC_VALID;
	}
	CATCH (CMemoryException, pE)
	{
		TCHAR szCause[255];
		eRtnStatus = REC_ERROR;
		pE->GetErrorMessage(szCause, 255);
		*pstrErrorText = szCause;
	}
	END_CATCH
	return eRtnStatus;
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		AddDir(CString strText, CString* pstrErrorText)	
	Description :	
	Return :        BOOL 		
	Parameters :	CString strText, CString* pstrErrorText
	Note :			None.
\*======================================================================*/
BOOL  CPrologUpdate::AddDir(CString strText, CString* pstrErrorText)
{
	BOOL bReturn_Status = TRUE;
//	CString       strErrorText;
	CString       strDescText = strText;

	Add(strText, pstrErrorText);

	return bReturn_Status;
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		DelStarDir(void)
	Description :	
	Return :        BOOL 		
	Parameters :	void
	Note :			None.
\*======================================================================*/
BOOL            CPrologUpdate::DelStarDir(void)
{
	BOOL nRetStatus = TRUE;

	// Get number of entries
	// Increment through list looking for star directive
	// if star is found remove entry

	int nNumRecs = m_strUpdateText.GetSize();

	for (int i = 0; i < nNumRecs; i++){
		if (-1 != m_strUpdateText[i].Find(ctstrSTAR, 0)){
			// Clear all star entries  
			m_strUpdateText[i].Empty();
		}
	}

	return nRetStatus;
}

/*======================================================================*\
	Author: Fred Shaw		Date : 09/07/06			version 1.0
  ======================================================================
	Function :		DelScanDir(void)
	Description :	
	Return :        BOOL 		
	Parameters :	None
	Note :			None.
\*======================================================================*/
BOOL            CPrologUpdate::DelScanDir(void)
{
	BOOL nRetStatus = true;

	int nNumRecs = m_strUpdateText.GetSize();

	for (int i = 0; i < nNumRecs; i++){

		if (-1 != m_strUpdateText[i].Find(ctstrSCAN, 0)){
			// Clear all scan entries  
			m_strUpdateText[i].Empty();
		}
	}

	// Get number of entries
	// Increment through list looking for star directive
	// if star is found remove entry

	return nRetStatus;

}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
	======================================================================
	Function :		AddDir(CString strText, CString* pstrErrorText)	
	Description :	
	Return :        BOOL 		
	Parameters :	CString strText, CString* pstrErrorText
	Note :			None.
\*======================================================================*/
BOOL            CPrologUpdate::DelRTCSDir(void)
{
	BOOL nRetStatus = true;

	int nNumRecs = m_strUpdateText.GetSize();

	for (int i = 0; i < nNumRecs; i++){

		if (-1 != m_strUpdateText[i].Find(ctstrRTCS, 0)){
			// Clear all RTCS entries  
			m_strUpdateText[i].Empty();
		}
	}

	// Get number of entries
	// Increment through list looking for star directive
	// if star is found remove entry

	return nRetStatus;
}

/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
	======================================================================
	Function :		CPrologUpdate::DelSectorDir(void)
	Description :	
	Return :        BOOL 		
	Parameters :	None.
	Note :			None.
\*======================================================================*/
BOOL            CPrologUpdate::DelSectorDir(void)
{
	BOOL nRetStatus = true;

	int nNumRecs = m_strUpdateText.GetSize();

	for (int i = 0; i < nNumRecs; i++){

		if (-1 != m_strUpdateText[i].Find(_T("SECTOR"), 0)){
			// Clear all sector entries  
			m_strUpdateText[i].Empty();
		}
	}

	// Get number of entries
	// Increment through list looking for star directive
	// if star is found remove entry

	return nRetStatus;
}

/////////////////////////////////////////////////////////////////////////
// CPrologParam Class
/////////////////////////////////////////////////////////////////////////
/*======================================================================*\
	Author: Frederick J. Shaw		Date : 05/16/01		version 1.0
  ======================================================================
	Function :		CPrologParam::CPrologParam()
	Description :	Class constructor.  Mark the valid flag as being
					FALSE.
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologParam::CPrologParam()
{
	m_bValid = FALSE;
		// Clean up object arrays when starting.
	// First free up memory allocated
	int nMaxSize = m_objParam.GetSize();
	int nObjIdx = 0;
	while ( nObjIdx < nMaxSize){
		delete ((strctPassedParamList *) m_objParam[nObjIdx]);
		nObjIdx++;
	}

	// Then remove elements from array.
	m_objParam.RemoveAll();

}

/*======================================================================*\
	Author: Frederick J. Shaw		Date : 05/16/01		version 1.0
  ======================================================================
	Function :		CPrologParam::~CPrologParam()
	Description :	Class descructor.
	Return :		destructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologParam::~CPrologParam(){

	// Clean up object arrays 
	// Free memory allocated
	int nMaxSize = m_objParam.GetSize();
	int nObjIdx = 0;
	while ( nObjIdx < nMaxSize){
		delete ((strctPassedParamList *) m_objParam[nObjIdx]);
		nObjIdx++;
	}

	// Then remove elements from array.
	m_objParam.RemoveAll();
}

/*======================================================================*\
	Author :Frederick J. Shaw	Date : 05/16/2001		version 1.0
  ======================================================================
	Function :		CPrologParam::CPrologParam(const CPrologParam & Other)
	Description :	Copy Constructor
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologParam::CPrologParam(const CPrologParam & Other){
	m_bValid = Other.m_bValid;
	m_strParamText.Copy(Other.m_strParamText);
	m_strName.Copy(Other.m_strName);
	m_strDesc.Copy(Other.m_strDesc);
	m_objParam.Copy(Other.m_objParam);
}
/*======================================================================*\
	Author : Frederick J. Shaw		Date : 05/16/01		version 1.0
  ======================================================================
	Function :		CPrologParam::operator= 
	Description :	Assignment operator
	Return :		pointer to new class
	Parameters :	Values to assign to class.
	Note :			None.
\*======================================================================*/
CPrologParam & CPrologParam::operator=(const CPrologParam & Other){

	if (this == & Other)  // object assigned to itself
		return *this; // Nothing to do.

	m_bValid = Other.m_bValid;
	m_strParamText.Copy(Other.m_strParamText);
	m_strName.Copy(Other.m_strName);
	m_strDesc.Copy(Other.m_strDesc);
	m_objParam.Copy(Other.m_objParam);

	return *this;
}

#ifdef _DEBUG
/*======================================================================*\
	Author :Frederick J. Shaw		Date : 05/16/2001		version 1.0
  ======================================================================
	Function :		CPrologParam::Dump
	Description :	Dumps the prolog validation records.
	Return :		void
	Parameters :	None.
	Note :			Only used if _DEBUG is defined
\*======================================================================*/
void CPrologParam::Dump()
{
	TRACE0("  Dumping the parameter records...\n");
	if (IsValid())
	{
		int nMax = m_strParamText.GetSize();
		if (nMax == 0)
			TRACE0(" No Parameter records found\n");
		else
		{
			for (int nIndex=0; nIndex < nMax; nIndex++)
			{
				TRACE1("    Parameter : %s\n", m_strParamText[nIndex]);
			}
		}
	}
	TRACE0("  Parameter records dump complete\n");
}

#endif


/*======================================================================*\
	Author :Don sanders			Date : 12/13/00			version 1.0
  ======================================================================
	Function :		AddDir(CString strText, CString* pstrErrorText)	
	Description :	
	Return :        BOOL 		
	Parameters :	CString strText, CString* pstrErrorText
	Note :			None.
\*======================================================================*/
BOOL  CPrologParam::AddDir(CString strText, CString* pstrErrorText)
{
	BOOL          bReturn_Status = TRUE;
	// CString       strErrorText;

	Add(strText, pstrErrorText);

	return bReturn_Status;
}

/*======================================================================*\
	Author : Frederick J. Shaw			Date : 08/10/01		version 1.0
  ======================================================================
	Function :		CPrologParam::Add
	Description :	Attempt to add a CString the array or description
					records.
	Return :		eRecord	-	REC_VALID if the text was added.
								REC_ERROR if there was an error adding
								the text (CMemoryException).
	Parameters :	CString strText - CString to add.
					CString* pstrErrorText - The error text.  Only valid
											 if the return status is
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
eRecord CPrologParam::Add(CString strText, CString* pstrErrorText)
{
	eRecord eRtnStatus;
	TRY
	{
		m_strParamText.Add(strText);
		m_bValid = TRUE;
		eRtnStatus = REC_VALID;
	}
	CATCH (CMemoryException, pE)
	{
		TCHAR szCause[255];
		eRtnStatus = REC_ERROR;
		pE->GetErrorMessage(szCause, 255);
		*pstrErrorText = szCause;
	}
	END_CATCH
	return eRtnStatus;
}


/////////////////////////////////////////////////////////////////////////
// CPrologOverRide Class
/////////////////////////////////////////////////////////////////////////
/*======================================================================*\
	Author: Frederick J. Shaw		Date : 06/25/2002		version 1.0
  ======================================================================
	Function :		CPrologOverRide::CPrologOverRide()
	Description :	Class constructor.  Mark the set flag as being
					FALSE.
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologOverRide::CPrologOverRide()
{
	m_bSet = FALSE;    
	m_strOverRideText.Empty(); 
}

/*======================================================================*\
	Author: Frederick J. Shaw		Date : 06/25/2002		version 1.0
  ======================================================================
	Function :		CPrologOverRide::~CPrologOverRide()
	Description :	Class descructor.
	Return :		destructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologOverRide::~CPrologOverRide(){}

/*======================================================================*\
	Author :Frederick J. Shaw	Date : 06/25/2002		version 1.0
  ======================================================================
	Function :		CPrologOverRide::CPrologOverRide(const CPrologOverRide & Other)
	Description :	Copy Constructor
	Return :		constructor
	Parameters :	None.
	Note :			None.
\*======================================================================*/
CPrologOverRide::CPrologOverRide(const CPrologOverRide & Other){
	m_bSet = Other.m_bSet;
	m_strOverRideText = Other.m_strOverRideText;
}
/*======================================================================*\
	Author : Frederick J. Shaw		Date : 06/25/2002		version 1.0
  ======================================================================
	Function :		CPrologOverRide::operator= 
	Description :	Assignment operator
	Return :		pointer to new class
	Parameters :	Values to assign to class.
	Note :			None.
\*======================================================================*/
CPrologOverRide & CPrologOverRide::operator=(const CPrologOverRide & Other){

	if (this == & Other)  // object assigned to itself
		return *this; // Nothing to do.

	m_bSet = Other.m_bSet;
	m_strOverRideText = Other.m_strOverRideText;

	return *this;
}


/*======================================================================*\
	Author : Frederick J. Shaw			Date : 08/10/01		version 1.0
  ======================================================================
	Function :		CPrologOverRide::Add
	Description :	Attempt to add a CString the array or description
					records.
	Return :		eRecord	-	REC_VALID if the text was added.
								REC_ERROR if there was an error adding
								the text (CMemoryException).
	Parameters :	CString strText - CString to add.
					CString* pstrErrorText - The error text.  Only valid
											 if the return status is
											 REC_ERROR.
	Note :			None.
\*======================================================================*/
 eRecord CPrologOverRide::Add(CString strText, CString* pstrErrorText)
{
	eRecord eRtnStatus;
	TRY
	{
		m_strOverRideText = strText;
		m_bSet = FALSE;
		eRtnStatus = REC_VALID;
	}
	CATCH (CMemoryException, pE)
	{
		TCHAR szCause[255];
		eRtnStatus = REC_ERROR;
		pE->GetErrorMessage(szCause, 255);
		*pstrErrorText = szCause;
	}
	END_CATCH
	return eRtnStatus;
}

 /*======================================================================*\
	Author :Frederick J. Shaw		Date : 06/25/2002		version 1.0
  ======================================================================
	Function :		CPrologOverride::Dump
	Description :	Dumps the prolog validation records.
	Return :		void
	Parameters :	None.
	Note :			Only used if _DEBUG is defined
\*======================================================================*/
VOID  CPrologOverRide::RemoveOverRide(){
	m_bSet = FALSE;
 }


#ifdef _DEBUG
/*======================================================================*\
	Author :Frederick J. Shaw		Date : 06/25/2002		version 1.0
  ======================================================================
	Function :		CPrologOverride::Dump
	Description :	Dumps the prolog validation records.
	Return :		void
	Parameters :	None.
	Note :			Only used if _DEBUG is defined
\*======================================================================*/
void CPrologOverRide::Dump()
{
	TRACE0("  Dumping the parameter records...\n");
	TRACE1("  Override : %s\n", m_strOverRideText);
	TRACE0("  Override records dump complete\n");
}

#endif


