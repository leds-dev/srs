// TextFile.h: interface for the CTextFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXTFILE_H__9B9AC662_CF71_11D4_800C_00609704053C__INCLUDED_)
#define AFX_TEXTFILE_H__9B9AC662_CF71_11D4_800C_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef enum eRecord	{REC_VALID, REC_IGNORED, REC_ERROR, REC_WARNING, REC_DONE};
typedef enum eState	{STATE_BEGIN, STATE_PROLOG, STATE_END};
typedef enum eValidStatus {PASSED, WARNING, FAIL, UNVALIDATED, INVALID_STATUS};
typedef	enum eValidLoc {GROUND, ONBOARD, BOTH, RTCS, INVALID_TYPE};
typedef enum eDirType {VALIDTYPE_DIR, VALIDSTATUS_DIR, VALIDQUAL_DIR, INVALID_DIR};

typedef	struct PassedParamList{
	           CString strParamText; // Combine name and description
  			   CString strName;      // Passed parameter name from ##param
			   CString strDesc;       // proc name associated with the label.
		}strctPassedParamList;

class CPrologValid
{
	public:
	friend class      CProlog;
	void              SetValid(const BOOL bValid){m_bValid = bValid;};
	CString 	      GetText(){return m_strValidText;};
	void              SetText(const CString strVText){m_strValidText = strVText;};
	enum eValidLoc    GetValidLoc(){return m_eValidLoc;};
	void              SetValidLoc(const enum eValidLoc eVLoc){m_eValidLoc = eVLoc;};
	enum eValidStatus GetValidStatus(){return m_eValidStatus;};
	void              SetValidStatus(const enum eValidStatus eVStatus){m_eValidStatus = eVStatus;};
	CString           GetName(){return m_strName;};
	void              SetName(const CString strName){m_strName = strName;};
	CString           GetTime(){return m_strTime;};
	void              SetTime(const CString strTime){m_strTime = strTime;};
	BOOL	          IsValid(){return m_bValid;};
	BOOL		      IsFileValid(enum eValidStatus &eVStatus);
	BOOL		      IsFileValid(enum eValidLoc &eVLoc);
	BOOL		      IsFileValid(enum eValidStatus &eVStatus, enum eValidLoc &eVLoc);

	protected:
	CPrologValid();
	CPrologValid(const CPrologValid & Other);
	virtual ~CPrologValid();
	CPrologValid & operator=(const CPrologValid & Other);

	private	:

	// functions
	eRecord			  Add(CString strText, CString* pstrErrorText);
	
	// variables
	BOOL              m_bValid;       // Is object valid?
	enum eDirType     m_eDirType;     // What type of valid directive is this object?
	CString      	  m_strValidText; // Entire line without processing directive
	enum eValidLoc    m_eValidLoc;	  // Where will file be executed?
	enum eValidStatus m_eValidStatus; // Outcome of validation
	CString           m_strName;	  // Who last did the validation
	CString           m_strTime;	  // Time of last validation
	CString           m_strQual;      // 

#ifdef _DEBUG
	void			Dump();
#endif
};

class CPrologOverRide
{
	public:
	friend class      CProlog;
	BOOL	          IsSet(){return m_bSet;};
	CString 	      GetText(){return m_strOverRideText;};
	void              RemoveOverRide();

	protected:
	CPrologOverRide();
	CPrologOverRide(const CPrologOverRide & Other);
	virtual ~CPrologOverRide();
	CPrologOverRide & operator=(const CPrologOverRide & Other);

	private	:

	// functions
	eRecord			  Add(CString strText, CString* pstrErrorText);
	
	// variables
	BOOL              m_bSet;       // Is object set?
	CString      	  m_strOverRideText; // Entire line without processing directive

#ifdef _DEBUG
	void			Dump();
#endif
};


class CPrologUpdate
{
	public:
	friend class    CProlog;
	CStringArray *	GetTextArray(){return &m_strUpdateText;};
	BOOL	        IsValid(){return m_bValid;};
	BOOL            AddDir(CString strText, CString* pstrErrorText);
	BOOL            DelStarDir(void);
	BOOL            DelScanDir(void);
	BOOL            DelRTCSDir(void);
	BOOL            DelSectorDir(void);

	protected:
	CPrologUpdate();
	CPrologUpdate(const CPrologUpdate & Other);
	virtual ~CPrologUpdate();
	CPrologUpdate & operator=(const CPrologUpdate & Other);

	private:
	// Functions
	eRecord			Add(CString strText, CString* pstrErrorText);

	// Variables
	BOOL			m_bValid;
	CStringArray	m_strUpdateText;

#ifdef _DEBUG
	void			Dump();
#endif
};

class CPrologParam
{
	public:
	friend class    CProlog;
	CStringArray *	GetTextArray(){return &m_strParamText;};
	CStringArray *	GetNameArray(){return &m_strName;};
	CStringArray *	GetDescArray(){return &m_strDesc;};
	CObArray &      GetPassedParamObj(){return m_objParam;};
	BOOL	        IsValid(){return m_bValid;};
	BOOL            AddDir(CString strText, CString* pstrErrorText);

	protected:
	CPrologParam();
	CPrologParam(const CPrologParam & Other);
	virtual ~CPrologParam();
	CPrologParam & operator=(const CPrologParam & Other);

	private:

	// Functions
	eRecord			Add(CString strText, CString* pstrErrorText);

	// Variables
	BOOL			m_bValid;
	CStringArray	m_strParamText;		// Parameter name and description
	CStringArray	m_strName;			// Parameter name
	CStringArray	m_strDesc;			// Parameter description
	CObArray        m_objParam;

#ifdef _DEBUG
	void			Dump();
#endif
};

class CPrologDesc
{
	public:
	friend class CProlog;
	CStringArray*	GetTextArray(){return &m_strDescText;};
	BOOL			IsValid(){return m_bValid;}; 
	BOOL            AddDir(CString strText, CString* pstrErrorText);

	protected:
	CPrologDesc();
	CPrologDesc(const CPrologDesc & Other);
	virtual ~CPrologDesc();
	CPrologDesc & operator=(const CPrologDesc & Other);

	private:

	// Functions
	eRecord			Add(CString strText, CString* pstrErrorText);

	// Variables
	BOOL			m_bValid;
	CStringArray	m_strDescText;

#ifdef _DEBUG
	void			Dump();
#endif
};

class CPrologExectime
{
public:
	friend class CProlog;
	BOOL	IsValid(){return m_bValid;};
	CString GetTextString(){return m_cTimeSpan.Format(_T("%H:%M:%S"));};
	CTimeSpan GetTimeSpan(){return m_cTimeSpan;};
	BOOL    AddDir(const CString strText);


protected:
	CPrologExectime();
	CPrologExectime(const CPrologExectime & Other);
	virtual ~CPrologExectime();
	CPrologExectime & operator=(const CPrologExectime & Other);

	private:
	// Functions
	eRecord			SetTime(CString strText, CString* pstrErrorText);

	// Variables
	BOOL			m_bValid;
	CTimeSpan		m_cTimeSpan;
	bool            m_bFound;

#ifdef _DEBUG
	void			Dump();
#endif
};

class CPrologStarttime
{
public:
	friend class CProlog;
	BOOL	IsValid(){return m_bValid;};
	CString GetTextString(){return m_cTimeSpan.Format(_T("%H:%M:%S"));};
	BOOL    AddDir(const CString strText);
	CTimeSpan GetTimeSpan(){return m_cTimeSpan;};

protected:
	CPrologStarttime();
	CPrologStarttime(const CPrologStarttime & Other);
	virtual ~CPrologStarttime();
	CPrologStarttime & operator=(const CPrologStarttime & Other);

	private:
	// Functions
	eRecord			SetTime(CString strText, CString* pstrErrorText);

	// Variables
	BOOL			m_bValid;
	CTimeSpan		m_cTimeSpan;
	bool            m_bFound;

#ifdef _DEBUG
	void			Dump();
#endif
};

class CPrologStoptime
{
public:
	friend class CProlog;
	BOOL	IsValid(){return m_bValid;};
	CString GetTextString(){return m_cTimeSpan.Format(_T("%H:%M:%S"));};
	BOOL    AddDir(const CString strText);
	CTimeSpan GetTimeSpan(){return m_cTimeSpan;};

protected:
	CPrologStoptime();
	CPrologStoptime(const CPrologStoptime & Other);
	virtual ~CPrologStoptime();
	CPrologStoptime & operator=(const CPrologStoptime & Other);

	private:

	// Functions
	eRecord			SetTime(CString strText, CString* pstrErrorText);

	// Variables
	BOOL			m_bValid;
	CTimeSpan		m_cTimeSpan;
	bool            m_bFound;

#ifdef _DEBUG
	void			Dump();
#endif
};

class CProlog
{
public:
	friend class		CTextFile;
	CPrologDesc*		GetDescription(){return &m_cPrologDesc;};
	CPrologExectime*	GetExectime(){return &m_cPrologExectime;};
	CPrologStarttime*	GetStarttime(){return &m_cPrologStarttime;};
	CPrologStoptime*	GetStoptime(){return &m_cPrologStoptime;};
	CPrologUpdate*		GetUpdate(){return &m_cPrologUpdate;};
	CPrologValid*	    GetValid(){return &m_cPrologValid;};
	CPrologParam*		GetParam(){return &m_cPrologParam;};
	CPrologOverRide*	GetOverRide(){return &m_cPrologOverRide;};
	int					GetLineCount(){return m_nLineCnt;};
	void				IncLineCount(){m_nLineCnt++;};
	int					GetNumberPassedParams(){ return m_cPrologParam.m_strParamText.GetSize();};
	BOOL                WasBeginFound(){return m_bPrologBeginFound;};
	BOOL                WasEndFound(){return m_bPrologEndFound;};
	BOOL                WasExectimeFound(){return m_bPrologExectimeFound;};
	BOOL                WasUpdateFound(){return m_bPrologUpdateFound;};
protected:
	// Functions
	CProlog();
	CProlog(const CProlog & Other);
	virtual ~CProlog();
	CProlog & operator=(const CProlog & Other);
	eRecord			 Consider (CString strText, CString* pstrErrorText);
	eRecord			 IsBegin(CString strText, CString* pstrErrorText);
	eRecord			 IsEnd(CString strText, CString* pstrErrorText);
	eRecord			 IsDesc(CString strText, CString* pstrErrorText);
	eRecord			 IsExectime(CString strText, CString* pstrErrorText);
	eRecord			 IsStarttime(CString strText, CString* pstrErrorText);
	eRecord			 IsStoptime(CString strText, CString* pstrErrorText);
	eRecord			 IsUpdate(CString strText, CString* pstrErrorText);
	eRecord			 IsValid(CString strText, CString* pstrErrorText);
	eRecord			 IsParam(CString strText, CString* pstrErrorText);
	eRecord          IsOverRide(CString strSave, CString* pstrErrorText);

	// Variables
	eState			 m_eState;
	CPrologDesc		 m_cPrologDesc;
	CPrologExectime	 m_cPrologExectime;
	CPrologStarttime m_cPrologStarttime;
	CPrologStoptime	 m_cPrologStoptime;
	CPrologUpdate	 m_cPrologUpdate;
	CPrologValid	 m_cPrologValid;
	CPrologParam	 m_cPrologParam;
	CPrologOverRide	 m_cPrologOverRide;
	int				 m_nLineCnt;
	BOOL             m_bPrologBeginFound;
	BOOL             m_bPrologExectimeFound;
	BOOL             m_bPrologStarttimeFound;
	BOOL             m_bPrologStoptimeFound;
	BOOL             m_bPrologEndFound;
	BOOL             m_bPrologOverRideFound;
	BOOL			 m_bPrologUpdateFound;

#ifdef _DEBUG
	void			Dump();
#endif
};

class CTextFile : public CStdioFile  
{
// Public typedefs:
public:
	typedef enum eOpenFT  {INV, RW, CR, RO, PR};
	typedef enum eCloseFT  {WRITE, NOWRITE};

// Public Functions:
public:
	CTextFile();
	CTextFile(const CTextFile &Other);
	CTextFile(const CProlog &Other){m_cProlog = Other;};
	CTextFile  &    operator=(const CTextFile & Other);
	CTextFile  &    operator=(const CProlog & Other);
	virtual			~CTextFile();
	BOOL			Load(CString strName, eOpenFT eOpenMode, CFileException* pException);
	BOOL            LoadCaseSensitive(CString strFileName, eOpenFT eOpenMode, CFileException* pException);
	static CString	ExceptionErrorText(CFileException* pException);
	BOOL			Close(CFileException* pException);
	BOOL			Close(eCloseFT eCloseMode, CFileException* pException);
	BOOL			AppendText(const CString strText);
	BOOL			InsertAt(int nIndex, CString strText);
	BOOL            SetAt(int nIndex, CString strText);
	CProlog*		GetProlog();
	int				GetLineCount(){return m_strFileText.GetSize();};
	CString			GetTextAt(int nIndex);
	CString			GetDirLine(const int nLine, int &nRtnLine);
	CString         GetDirLineAtEOF( int &nLine, int &nRtnLine);
	BOOL			RemoveAt(int nIndex);
	static BOOL     ScanLine (const CString &pstrLine, const CString &strScan);
	static int      WhereIsToken (const CString &pstrLine, const CString &strScan);
	int             GetPrologLineCount();
	static BOOL		TokenizeString(const CString strTemp, CStringArray &strTokenData);
	BOOL		    IsFileValid(enum eValidStatus &eVStatus);
	BOOL		    IsFileValid(enum eValidLoc &eVLoc);
	BOOL		    IsFileValid(enum eValidStatus &eVStatus, enum eValidLoc &eVLoc);
	BOOL			CreateValidStrLine(const enum eValidStatus eStatus, const enum eValidLoc eLoc,
						CString &strLine);
	BOOL			WriteValidationStatus(const enum eValidStatus eStatus, const enum eValidLoc eLoc);
	BOOL			WriteValidationStatus(const enum eValidStatus eStatus);
	BOOL            AddDescDir(const CString strText);
	BOOL            AddUpdateDir(const CString strText);
	BOOL            AddParamDir(const CString strText);
	BOOL            AddPrologDesc(const CString strText);
	BOOL            AddPrologStarttime(const CString strText);
	BOOL            AddPrologStoptime(const CString strText);
	BOOL            AddPrologExectime(const CString strText);
	int             GetNumberPassedParams ();
	CObArray &      GetPassedParamObj ();
	void			Clear();
	void			ExtractProlog();
	BOOL            CheckPassedParams(CString strVal);
	BOOL            IsOverRideSet(){return m_cProlog.m_cPrologOverRide.IsSet();};
	BOOL            DelStarUpdateDir(void){return m_cProlog.m_cPrologUpdate.DelStarDir();};
	BOOL            DelScanUpdateDir(void){return m_cProlog.m_cPrologUpdate.DelScanDir();};
	BOOL            DelRTCSUpdateDir(void){return m_cProlog.m_cPrologUpdate.DelRTCSDir();};
	BOOL            DelSectorUpdateDir(void){return m_cProlog.m_cPrologUpdate.DelSectorDir();};


#ifdef _DEBUG
	void			DumpProlog();
#endif


private:
	BOOL			ReadFile(CFileException* pException);
	BOOL			WriteFile(CFileException* pException);
	BOOL			WriteProlog();

// Private Data:
	eOpenFT			m_eOpenMode;
	CStringArray	m_strFileText;
	CString			m_strFileName;
	CProlog			m_cProlog;
	BOOL            m_bPrologExtracted;
};


#endif // !defined(AFX_TEXTFILE_H__9B9AC662_CF71_11D4_800C_00609704053C__INCLUDED_)
