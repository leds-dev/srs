/**
/////////////////////////////////////////////////////////////////////////////
// TimeOutSocket.cpp : implementation of the TimeOutSocket class.          //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// TimeOutSocket.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is creates a timer with the specified time-out value.
//
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "TimeOutSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTimeOutSocket

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/03/02		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeOutSocket::CTimeOutSocket
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CTimeOutSocket::CTimeOutSocket()
{
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeOutSocket::~CTimeOutSocket
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CTimeOutSocket::~CTimeOutSocket()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CTimeOutSocket, CSocket)
	//{{AFX_MSG_MAP(CTimeOutSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CTimeOutSocket member functions

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeOutSocket::SetTimeOutVal
//	Description :	This routine is called to set the timer time-out 
//					value.
//
//	Return :		BOOL	-	Nonzero if successful.
//
//	Parameters :
//		UINT uTimeOutVal	-	time-out value.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CTimeOutSocket::SetTimeOutVal(UINT uTOValue)
{
	if( uTOValue )
		m_nTOValue = uTOValue;

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeOutSocket::InitializeTimer
//	Description :	This routine is called to initialize the timer.
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CTimeOutSocket::InitializeTimer()
{
	m_initTime = CTime::GetCurrentTime();
	m_bTimeOut = FALSE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeOutSocket::CheckTimer
//	Description :	This routine is called to check if the timer has
//					expired.
//
//	Return :		BOOL	- TRUE if the timer has not expired.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CTimeOutSocket::CheckTimer()
{
	CTimeSpan	elaspedtime= CTime::GetCurrentTime() - m_initTime;
	LONGLONG	elaspedsecs =  elaspedtime.GetTotalSeconds();

	if( elaspedsecs > m_nTOValue )
	{
		m_bTimeOut = TRUE;
		SetLastError(WSAECONNABORTED);
		return FALSE;
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeOutSocket::Receive
//	Description :	This routine is called to receive data from a
//					socket. This function is used for connected
//					stream or datagram sockets and is used to read
//					incoming data.
//
//	Return :		int	-	returns the number of bytes received.
//
//	Parameters :
//		void* lpBuf		-	A buffer containing the data to
//							be transmitted.
//		int nBufLen		-	The length of the data in lpBuf
//							in bytes.
//		int nFlags		-	Specifies the way in which the
//							call is made.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CTimeOutSocket::Receive(void* lpBuf, int nBufLen, int nFlags)
{
	m_nTimeOut = 0;

	if( m_pbBlocking != NULL )
	{
		WSASetLastError(WSAEINPROGRESS);
		return FALSE;
	}

	int nResult;

	while( (nResult = CAsyncSocket::Receive(lpBuf, nBufLen, nFlags)) == SOCKET_ERROR )
	{
		if( GetLastError() == WSAEWOULDBLOCK )
		{
			if( !PumpMessages(FD_READ) )
				return SOCKET_ERROR;
		}
		else
			return SOCKET_ERROR;

		if( !CheckTimer() )
			return SOCKET_ERROR;
	}

	return nResult;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeOutSocket::Send
//	Description :	This routine is called to write outgoing data
//					on connected stream or datagram sockets.
//
//	Return :		int	-	returns the total number of characters
//							sent.
//
//	Parameters :
//		const void* lpBuf	-	A buffer containing the data to
//								be transmitted.
//		int nBufLen			-	The length of the data in lpBuf
//								in bytes.
//		int nFlags			-	Specifies the way in which the
//								call is made.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CTimeOutSocket::Send(const void* lpBuf, int nBufLen, int nFlags)
{
	if( m_pbBlocking != NULL )
	{
		WSASetLastError(WSAEINPROGRESS);
		return  FALSE;
	}

	int		nLeft;
	int		nWritten;
	PBYTE	pBuf = (PBYTE)lpBuf;

	nLeft = nBufLen;

	while( nLeft > 0 )
	{
		nWritten = SendChunk(pBuf, nLeft, nFlags);

		if( nWritten == SOCKET_ERROR )
			return nWritten;

		nLeft-= nWritten;
		pBuf += nWritten;
	}

	return nBufLen - nLeft;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CTimeOutSocket::SendChunk
//	Description :	This routine is called to write outgoing data
//					on connected stream or datagram sockets.
//
//	Return :		int	-	returns the total number of characters
//							sent.
//
//	Parameters :
//		const void* lpBuf	-	A buffer containing the data to be 
//								transmitted.
//		int nBufLen			-	The length of the data in lpBuf in 
//								bytes.
//		int nFlags			-	Specifies the way in which the call 
//								is made.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CTimeOutSocket::SendChunk(const void* lpBuf, int nBufLen, int nFlags)
{
	int nResult;

	m_nTimeOut = 0;

	while( (nResult = CAsyncSocket::Send(lpBuf, nBufLen, nFlags)) == SOCKET_ERROR )
	{
		if( GetLastError() == WSAEWOULDBLOCK )
		{
			if( !PumpMessages(FD_WRITE) )
				return SOCKET_ERROR;
		}
		else
			return SOCKET_ERROR;

		if( !CheckTimer() )
			return SOCKET_ERROR;
	}

	return nResult;
}
