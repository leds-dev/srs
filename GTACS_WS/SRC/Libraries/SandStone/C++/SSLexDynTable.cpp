#include "stdafx.h"
#include "SSLexDynTable.h"
#include "SSLexDll.h"

SSLexRegExpr::SSLexRegExpr( const char* q_expr, int q_id, int q_flags, int q_ptoken, const char* q_table, const char* q_keyTable)
{
	m_hRegExpr = SSLexRegExprCreate( q_expr, q_id, q_flags, q_ptoken, q_table, q_keyTable);
}

SSLexRegExpr::~SSLexRegExpr( void)
{
	SSLexRegExprDestroy( m_hRegExpr);
}

SSLexRegExprList::SSLexRegExprList( const char* q_name)
{
	m_hRegExprList = SSLexRegExprListCreate( q_name);
}

int SSLexRegExprList::add( SSLexRegExpr* q_expr)
{
	return SSLexRegExprListAdd( m_hRegExprList, q_expr->handle());
}

SSLexRegExprList::~SSLexRegExprList( void)
{
	SSLexRegExprListDestroy( m_hRegExprList);
}

SSLexKeyword::SSLexKeyword( const char* q_key, int q_id, int q_flags)
{
	m_hKeyword = SSLexKeywordCreate( q_key, q_id, q_flags);
}

SSLexKeyword::~SSLexKeyword( void)
{
	SSLexKeywordDestroy( m_hKeyword);
}

SSLexKeywordList::SSLexKeywordList( const char* q_name)
{
	m_hKeywordList = SSLexKeywordListCreate( q_name);
}

int SSLexKeywordList::add( SSLexKeyword* q_key)
{
	return SSLexKeywordListAdd( m_hKeywordList, q_key->handle());
}

SSLexKeywordList::~SSLexKeywordList( void)
{
	SSLexKeywordListDestroy( m_hKeywordList);
}

SSLexDynTable::SSLexDynTable( void)
{
	m_hDynTable = SSLexDynTableCreate();
}

int SSLexDynTable::add( SSLexKeywordList* q_keyList)
{
	return SSLexDynTableAddKeywordList( m_hDynTable, q_keyList->handle());
}

int SSLexDynTable::add( SSLexRegExprList* q_regList)
{
	return SSLexDynTableAddRegExprList( m_hDynTable, q_regList->handle());
}

SSLexTable* SSLexDynTable::makeLexTable( const char* q_name, int q_ret)
{
	return ( SSLexTable*) SSLexDynTableMakeLexTable( m_hDynTable, q_name, q_ret);
}

SSLexDynTable::~SSLexDynTable( void)
{
	SSLexDynTableDestroy( m_hDynTable);
}