/*---------------------------------------------------------------------------

					Copyright (c) 1994-1997 SandStone Technology Inc.
								  All rights reserved

		THIS PROGRAM CONSISTS OF TRADE SECRETS THAT ARE  THE PROPERTY
		OF SANDSTONE SOFTWARE INC..  THE CONTENTS  MAY NOT BE USED OR
		DISCLOSED WITHOUT THE EXPRESS WRITTEN PERMISSION OF THE OWNER.

----------------------------------------------------------------------------*/
#include<sssto.hpp>
#include<ssexcept.hpp>
#if defined( __BORLANDC__)
#     pragma warn -rch
#     pragma warn -aus
#     pragma warn -ccc
#     pragma warn -par
#endif
#if defined( __OS2__)
#  define INCL_DOSPROCESS
#  include<os2.h>
#  define SSStoSpecialThreshold 0xffffffff
#  define SSEnterCritSec        DosEnterCritSec()
#  define SSExitCritSec         DosExitCritSec()
#  define farmalloc( n)         malloc( n)
#  define farfree( n)           free( n)
#  define mallocSSHuge( n, s)   0
#  define freeSSHuge( p)
#elif defined( __BORLANDC__) && defined( __WIN32__)
#  include<malloc.h>
#  define SSStoSpecialThreshold 0xffffffff
#  define SSEnterCritSec
#  define SSExitCritSec
#  define mallocSSHuge( n, s)   0
#  define freeSSHuge( p)
#elif ( _MSC_VER >= 900)
#  include<malloc.h>
#  define SSStoSpecialThreshold 0xffffffff
#  define SSEnterCritSec
#  define SSExitCritSec
#  define farmalloc( n)         malloc( n)
#  define farfree( n)           free( n)
#  define mallocSSHuge( n, s)   0
#  define freeSSHuge( p)
#elif ( _MSC_VER == 800)
#  include<malloc.h>
#  define SSStoSpecialThreshold 0x0000ff00
#  define SSEnterCritSec
#  define SSExitCritSec
#  define farmalloc( n)         malloc( n)
#  define farfree( n)           free( n)
#  define mallocSSHuge( n, s)   _halloc( n, s)
#  define freeSSHuge( p)        _hfree( p)
#elif defined( __BORLANDC__)
#  include<malloc.h>
#  define SSStoSpecialThreshold 0xffffffff
#  define SSEnterCritSec
#  define SSExitCritSec
#  define mallocSSHuge( n, s)   0
#  define freeSSHuge( p)
#else
#  include<malloc.h>
#  define SSStoSpecialThreshold 0xffffffff
#  define SSEnterCritSec
#  define SSExitCritSec
#  define farmalloc( n)         malloc( n)
#  define farfree( n)           free( n)
#  define mallocSSHuge( n, s)   0
#  define freeSSHuge( p)
#endif

   void* mallocSS( SSUnsigned32 qulSize)
      {
      void* zpBlock = farmalloc( qulSize);
      if ( !zpBlock)
         {
         SSExceptionOutOfStorage zOut;
			if ( getSSExceptionInfo().isThrowPointer())
				SSTHROW0( &zOut);
			else
				SSTHROW1( zOut);
         }

      return zpBlock;
      }

   void* mallocSSSpecial( SSUnsigned32 qulSize)
      {                                 
//      qulSize += sizeof( SSUnsigned32);
      void* zpBlock = 0;
      if ( qulSize > SSStoSpecialThreshold)
      	zpBlock = mallocSSHuge(( qulSize + 1) / 2, 2);
      else           
      	zpBlock = mallocSS( qulSize);
      
      if ( !zpBlock)
         {
         SSExceptionOutOfStorage zOut;
			if ( getSSExceptionInfo().isThrowPointer())
				SSTHROW0( &zOut);
			else
				SSTHROW1( zOut);
         }                              
         
//      SSUnsigned32* zpulBlock = ( SSUnsigned32*) zpBlock;
//      *zpulBlock = qulSize;
//      return ++zpulBlock;
		return zpBlock;
		}

	void freeSSSpecial( void* qpBlock, SSUnsigned32 qulLen)
		{
//		SSUnsigned32* zpulBlock = ( SSUnsigned32*) qpBlock;
//		--zpulBlock;
		if ( qulLen /**zpulBlock*/ > SSStoSpecialThreshold)
			freeSSHuge( qpBlock);
		else
			freeSS( qpBlock);
		}

	void freeSS( void* qpFree)
		{
		farfree( qpFree);
      }

   SSStoStack::SSStoStack( SSUnsigned32 qulSize, SSUnsigned32 qulNum)
      : oulSize( qulSize), oulNum( qulNum), opBlock( 0)
      {
      init();
      }

   void SSStoStack::init( void)
      {
      SSStoStackElement* zpBlock = ( SSStoStackElement*) mallocSS(
         oulSize * oulNum + sizeof( SSStoStackElement));
      zpBlock->opNext = opBlock;
      opBlock = zpBlock;
      opTop = ++zpBlock;

      for ( unsigned int i = 0; i < oulNum - 1; i++)
         {
         SSStoStackElement* zpNext = ( SSStoStackElement*)
            (( char*) zpBlock + oulSize);
         zpBlock->opNext = zpNext;
         zpBlock = ( SSStoStackElement*) (( char*) zpBlock + oulSize);
         }
      zpBlock->opNext = 0;
      }

   void* SSStoStack::alloc( void)
      {
      SSEnterCritSec;
		if ( !opTop)
			init();
      void* zpRet = opTop;
      opTop = opTop->opNext;
      SSExitCritSec;
      return zpRet;
      }

   void SSStoStack::freeBlock( void* qpFree)
      {
      SSStoStackElement* zpSto = ( SSStoStackElement*) qpFree;
      SSEnterCritSec;
      zpSto->opNext = opTop;
      opTop = zpSto;
      SSExitCritSec;
      }

   SSStoStack::~SSStoStack( void)
      {
      while ( opBlock)
         {
         SSStoStackElement* zpBlock = opBlock;
         opBlock = opBlock->opNext;
         freeSS( zpBlock);
         }
      }
