/*---------------------------------------------------------------------------

					Copyright (c) 1994 - 1997 SandStone Technology Inc.
                          All rights reserved

----------------------------------------------------------------------------*/
#if !defined( SSEXCEPTION)
#  define SSEXCEPTION

#  include<stdio.h>
#  include<string.h>
#  include<sssto.hpp>
#  include<ssexcept.h>
#  include<ssglobal.h>

#  define SSExceptionParmArray  8
#  define SSExceptionTextLength 127
#  define SSExceptionDefault    SSTrue

#  if _MSC_VER == 800
#     include<afx.h>
#     define SSTry                              TRY
#     define SSCatch( c, v)                     CATCH( c, v)
#     define SSCatchEnd                         END_CATCH
#     define SSExceptionThrowPointerDefault     SSTrue
#     define SSTHROW0( class)                   THROW( class)
#     define SSTHROW1( class)
#     define SSExceptionDeclareDynamic( e)      DECLARE_DYNAMIC( e)
#     define SSExceptionImplementDynamic( b, d) IMPLEMENT_DYNAMIC( b, d)
#     define SSExceptionCException              : public CException
#  else
#     define SSTry                              try
#     define SSCatch( c, v)                     catch ( c v)
#     define SSCatchEnd
#     define SSExceptionCException
#     define SSExceptionDeclareDynamic( e)
#     define SSExceptionImplementDynamic( b, d)
#     define SSExceptionThrowPointerDefault     SSFalse
#     if defined( SSNOEXCEPTION)
#        define SSTHROW0                        SSExceptionUserHandler( class)
#        define SSTHROW1                        SSExceptionUserHandler( class)
#     else
#        define SSTHROW0( class)
#        define SSTHROW1( class)                throw class
#     endif
#  endif

	class SSClass SSException;
	class SSClass SSExceptionInfo;
	class SSClass SSExceptionParm;
	class SSClass SSExceptionPresent;

#  if defined( SSNOEXCEPTION)
	   void SSExceptionUserHandler( SSException&);
	   void SSExceptionUserHandler( SSException*);
#  endif

	class SSClass SSExceptionParm
		{
		public:

         enum SSExceptionParmType
            {
            constVoid,
            constChar,
            unsigned32
            };

			SSInline SSConstr            SSExceptionParm( void);
			SSInline SSConstr            SSExceptionParm( const char*);
			SSInline SSConstr            SSExceptionParm( const void*);
			SSInline SSConstr            SSExceptionParm( SSUnsigned32);
			SSInline SSConstr            SSExceptionParm( const SSExceptionParm&);

			SSInline SSExceptionParm&    operator=( const char*);
			SSInline SSExceptionParm&    operator=( const void*);
			SSInline SSExceptionParm&    operator=( SSUnsigned32);

			SSInline SSExceptionParmType type( void) const;
			SSInline SSBooleanValue      isConstChar( void) const;
			SSInline SSBooleanValue      isConstVoid( void) const;
			SSInline SSBooleanValue      isUnsigned32( void) const;

			SSInline const char*         asConstChar( void) const;
			SSInline const void*         asConstVoid( void) const;
			SSInline SSUnsigned32        asUnsigned32( void) const;

			SSInline 					 operator const char*( void);
			SSInline 	     			 operator const void*( void);
			SSInline                     operator SSUnsigned32( void);

			SSInline SSDestr             SSExceptionParm( void);

		protected:
			SSExceptionParmType          oType;
			union
				{
				const void*              opParm;
				SSUnsigned32             oulParm;
				char*                    opchParm;
				};
		};

	class SSClass SSExceptionInfo
		{
		public:
			SSConstr       SSExceptionInfo( void);

			SSBooleanValue isThrowPointer( void);
			SSBooleanValue hasException( void) const;
			SSBooleanValue setException( SSBooleanValue = SSTrue);
			SSBooleanValue setThrowPointer( SSBooleanValue = SSTrue);
			~SSExceptionInfo();

		protected:
			SSBooleanValue oException;
			SSBooleanValue oThrowPointer;
		};

	class SSClass SSException SSExceptionCException
		{
		friend class SSExceptionInfo;
		SSExceptionDeclareDynamic( SSException)

		public:

			SSInline SSConstr         SSException( const SSException&);
			SSInline SSConstr         SSException( const char*, SSUnsigned32 = 0);

			SSInline const char*      text( void) const;
			SSInline SSExceptionParm* parm( SSUnsigned32);
			SSInline SSUnsigned32     errorId( void) const;
			void                      setText( const char*);
			static void			 	  throwException( const char*);
			static void				  throwException( const char*, int);
			static void				  throwException( SSUnsigned32, const char*);

			SSInline SSDestr          SSException( void);

		protected:
			SSUnsigned32              oulId;
			SSExceptionParm           oaParm[ SSExceptionParmArray];
			char                      oachText[ SSExceptionTextLength + 1];
		};

	class SSClass SSExceptionOutOfStorage : public SSException
		{
		SSExceptionDeclareDynamic( SSExceptionOutOfStorage)

		public:
			SSConstr          SSExceptionOutOfStorage( void);
		};

	class SSClass SSExceptionPresent
      {
      public:
			SSInline SSConstr     SSExceptionPresent( void);

			void                  throwException( SSUnsigned32, const char*);
			void                  throwException( SSUnsigned32, const char*, SSUnsigned32);
			void                  throwException( SSUnsigned32, const char*, const char*);
			void                  throwException( SSUnsigned32, const char*, const char*, SSUnsigned32);
			void                  throwException( SSUnsigned32, const char*, SSUnsigned32, SSUnsigned32, const char*);

			SSInline SSException* getException( void);
			SSInline SSException* hasException( void) const;
			SSInline SSException* setException( SSException*);

		protected:
			SSException*          opException;
		};

	SSExceptionInfo&            getSSExceptionInfo( void);
	void                        setSSExceptionNewHandler( void);

#  include<ssexcept.inl>

#endif
