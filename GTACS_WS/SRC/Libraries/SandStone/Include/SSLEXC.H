/*---------------------------------------------------------------------------

               Copyright (c) 1994-1997 SandStone Technology Inc.
                          All rights reserved

----------------------------------------------------------------------------*/
#if !defined( SSLEXCH)
#  define SSLEXCH
#  include<stdio.h>
#  include<ssglobal.h>

#  if __cplusplus
      extern "C" {
#  endif

#  if !defined( SSLexTokenNotFound)
#     define SSLexTokenNotFound "SSLexTokenNotFound"
#  endif

   /*---------------------------------------------------------------------
   --  SSLexTable functions 
   ---------------------------------------------------------------------*/

   void*          SSLexTableCreate( const char*, void**);
	void*          SSLexResourceTableCreate( unsigned long, const char*,
							const char*, void**);
	void           SSLexTableDestroy( void*);

   /*---------------------------------------------------------------------
   --  SSLexLexeme functions
   ---------------------------------------------------------------------*/

#  define         SSLexLexemeTypeDBCS    0
#  define         SSLexLexemeTypeUnicode 1

   void*          SSLexLexemeCreate( void*, SSUnsigned32, SSUnsigned32, 
                     void**);
   void           SSLexLexemeRefInc( void*);
   SSBooleanValue SSLexLexemeRefDec( void*);
   void           SSLexLexemeDestroy( void*);

   SSUnsigned32   SSLexLexemeGetType( void*);
   SSUnsigned32   SSLexLexemeGetLine( void*);
   SSUnsigned32   SSLexLexemeGetToken( void*);
   SSUnsigned32   SSLexLexemeGetOffset( void*);
   SSUnsigned32   SSLexLexemeGetLength( void*);
   void*          SSLexLexemeGetBuffer( void*);
   SSUnsigned32   SSLexLexemeGetExtraLength( void*);
   void*          SSLexLexemeGetExtraBuffer( void*);

   void           SSLexLexemeSetType( void*, SSUnsigned32);
   void           SSLexLexemeSetLine( void*, SSUnsigned32);
   void           SSLexLexemeSetToken( void*, SSUnsigned32);
   void           SSLexLexemeSetOffset( void*, SSUnsigned32);
   void           SSLexLexemeSetLength( void*, SSUnsigned32);
   void           SSLexLexemeSetBuffer( void*, void*);

   /*---------------------------------------------------------------------
   --  SSLexConsumer and SSLexFileConsumer functions
   ---------------------------------------------------------------------*/

#  if !defined( SSLexConsumerAll)
#     define      SSLexConsumerAll            0xffffffff
#  endif
#  define         SSLexConsumerTypeBinary     0
#  define         SSLexConsumerTypeUnicode    1
#  define         SSLexConsumerTypeCharacter  2

   typedef        SSUnsigned32 ( *SSLexConsumerNextBufferCallback)( void*,
                     void*, SSUnsigned32, void*);

   void*          SSLexConsumerCreate( void*, SSUnsigned32, SSUnsigned32, 
                     SSUnsigned32, void**);
   void*          SSLexFileConsumerCreate( const char*, SSUnsigned32,
                     SSUnsigned32, SSUnsigned32, void**);
   void           SSLexConsumerDestroy( void*);

   FILE*          SSLexFileConsumerGetFile( void*);
   void           SSLexFileConsumerCloseFile( void*);

   void           SSLexConsumerSetNextBufferCallback( void*, 
                     SSLexConsumerNextBufferCallback, void*);

   /*---------------------------------------------------------------------
   --  SSLex functions
   ---------------------------------------------------------------------*/

   typedef        SSBooleanValue ( *SSLexErrorCallback)( void*, void*, void*);
   typedef        SSBooleanValue ( *SSLexCompleteCallback)( void*, void*, void*);

   void*          SSLexCreate( void*, void*, void**);
   void           SSLexDestroy( void*);

   void*          SSLexGetNextLexeme( void*);
   void*          SSLexGetLastException( void*);
   void           SSLexClearLastException( void*);
   SSBooleanValue SSLexIsCurrentExpressionList( void*, SSUnsigned32);

   void           SSLexSetLexemeExtra( void*, SSUnsigned32);
   void           SSLexSetErrorCallback( void*, SSLexErrorCallback, void*);
   void           SSLexSetCompleteCallback( void*, SSLexCompleteCallback, void*);

   void           SSLexReset( void*);
   SSBooleanValue SSLexPopExpressionList( void*);
   SSBooleanValue SSLexPushExpressionList( void*, SSUnsigned32);
   SSBooleanValue SSLexGotoExpressionList( void*, SSUnsigned32);

   /*---------------------------------------------------------------------
   --  SSException functions
   ---------------------------------------------------------------------*/

   typedef        void ( *SSExceptionBadCorrelatorCallback)( void*, const char*,
                     void*);

   void           SSExceptionSetBadCorrelatorCallback( 
                     SSExceptionBadCorrelatorCallback, void*);
   const char*    SSExceptionGetMessage( void*);
   SSUnsigned32   SSExceptionGetErrorId( void*);
   void           SSExceptionDestroy( void*);

#  if __cplusplus
      }
#  endif

#endif
/*---------------------------------------------------------------------------

               Copyright (c) 1994-1997 SandStone Technology Inc.
                          All rights reserved

----------------------------------------------------------------------------*/
