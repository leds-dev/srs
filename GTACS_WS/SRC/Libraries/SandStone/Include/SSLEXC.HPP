/*---------------------------------------------------------------------------

               Copyright (c) 1994-1997 SandStone Technology Inc.
                          All rights reserved

----------------------------------------------------------------------------*/
#if !defined( SSLEXCHPP)
#  define SSLEXCHPP
#  include<sslexc.h>
#if defined( _Windows) || defined( __WIN32__) || defined( WIN32) || defined( _WINDOWS)
#  include<windows.h>
#endif

#  define SSExceptionCorr   "SSEX"
#  define SSLexCorrTable    "SSLT"
#  define SSLexCorrConsumer "SSCO"
#  define SSLexCorrLexeme   "SSLM"
#  define SSLexCorrLex      "SSLX"

   class SSLexCapi;
   class SSLexCapiTable;
   class SSLexCapiLexeme;
   class SSLexCapiConsumer;

   void SSExceptionCallBadCorrelatorCallback( void*, const char*);

   class SSCorrelator
      {
      public:
         SSConstr              SSCorrelator( const char*);

         SSBooleanValue        verify( void*, const char*);

      protected:
         SSUnsigned32          oulCorr;
      };

   SSInline SSCorrelator::SSCorrelator( const char* qpszVer)
      {
      oulCorr = *(( SSUnsigned32*) qpszVer);
      }

   class SSLexCapiTable : public SSLexTable, public SSCorrelator
      {
      public:
         SSConstr      SSLexCapiTable( const char*);
#if defined( _Windows) || defined( __WIN32__) || defined( WIN32) || defined( _WINDOWS)
			SSConstr      SSLexCapiTable( HINSTANCE, const char*, const char*);
#endif
		};

	SSInline SSLexCapiTable::SSLexCapiTable( const char* qpszName) :
		SSLexTable( qpszName), SSCorrelator( SSLexCorrTable)
		{
		}

#if defined( _Windows) || defined( __WIN32__) || defined( WIN32) || defined( _WINDOWS)
	SSInline SSLexCapiTable::SSLexCapiTable( HINSTANCE qhInst,
		const char* qpszName, const char* qpszType) :
		SSLexTable( qhInst, qpszName, qpszType), SSCorrelator( SSLexCorrTable)
		{
		}
#endif

	class SSLexCapiLexeme : public SSLexLexeme, public SSCorrelator
		{
		public:
			SSConstr      SSLexCapiLexeme( void*, SSUnsigned32, SSUnsigned32);
			SSConstr      SSLexCapiLexeme( SSLexConsumer&, SSUnsigned32,
								  SSLexMark*, SSUnsigned32, SSUnsigned32);

			void          refFree( void);
			char*         extraBuffer( void);
			SSUnsigned32  extraLength( void);

		private:
         char*         ochExtra;
         SSUnsigned32  oulExtra;
      };

   class SSLexCapiConsumer : public SSLexConsumer, public SSCorrelator
      {
      public:
         SSConstr      SSLexCapiConsumer( void*, SSUnsigned32, SSUnsigned32,
                          SSUnsigned32);
         SSConstr      SSLexCapiConsumer( const char*, SSUnsigned32,
                          SSUnsigned32, SSUnsigned32);

         void          close( void);
         FILE*         file( void) const;
         SSUnsigned32  nextBuffer( void);
         void          setNextBufferCallback( SSLexConsumerNextBufferCallback,
                          void* qpParm);

         SSDestr       SSLexCapiConsumer( void);

      private:
         void*                           opParm;
         FILE*                           opFile;
         SSLexConsumerNextBufferCallback opCallback;
      };

   class SSLexCapi : public SSLex, public SSCorrelator
      {
      public:
         SSConstr       SSLexCapi( SSLexTable&, SSLexCapiConsumer&);

         SSLexLexeme*   error( SSLexConsumer&);
         SSLexLexeme*   complete( SSLexConsumer&, SSUnsigned32, SSLexMark&);
         void           setLexemeExtra( SSUnsigned32);
         void           setErrorCallback( SSLexErrorCallback, void*);
         void           setCompleteCallback( SSLexCompleteCallback, void*);

      private:
         SSLexErrorCallback    opError;
         SSUnsigned32          oulExtra;
         SSLexCompleteCallback opComplete;
         void*                 opErrorParm;
         void*                 opCompleteParm;
      };

   class SSExceptionCapi : public SSException, public SSCorrelator
      {
      SSExceptionDeclareDynamic( SSExceptionCapi)
      
      public:
         SSConstr         SSExceptionCapi( SSException&);
         SSConstr         SSExceptionCapi( SSException*);
      };

   SSInline SSExceptionCapi::SSExceptionCapi( SSException& qExcept) :
      SSException( qExcept), SSCorrelator( SSExceptionCorr)
      {
      }

   SSInline SSExceptionCapi::SSExceptionCapi( SSException* qExcept) :
      SSException( *qExcept), SSCorrelator( SSExceptionCorr)
      {
      }

#endif
/*---------------------------------------------------------------------------

               Copyright (c) 1994 SandStone Technology Inc.
                          All rights reserved

----------------------------------------------------------------------------*/
