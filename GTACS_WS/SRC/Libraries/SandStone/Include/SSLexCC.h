#ifndef SSLEXCCH
#define SSLEXCCH

#include "string.h"
#include "stdlib.h"
#include "SSLexTableC.h"
#include "SSLexConsumerC.h"

typedef struct _SSLex
{
	SSLexStringConsumer*	opCon;
	SSUnsigned32			oulTop;
	SSBooleanValue			oError;
	SSLexTable*				opTable;
	SSUnsigned32			oulState;
	SSLexPairTableHeader*	opPairTable;
	SSLexPairTableHeader*   oapExprStack[ 256];
} SSLex;


SSLex*			SSLexCreate( SSLexTable*, SSLexStringConsumer*);
SSLexLexeme*	SSLexGetNext( SSLex*);
SSUnsigned32	SSLexLexemeGetToken( SSLexLexeme* q_pLexeme);
SSLexLexeme*	SSLexLexemeCopy( SSLexLexeme* q_pLexeme);
void			SSLexPopExpressionList( SSLex* q_pLex, SSUnsigned32 q_index);
void			SSLexPushExpressionList( SSLex* q_pLex, SSUnsigned32 q_index);
void			SSLexGotoExpressionList( SSLex* q_pLex, SSUnsigned32 q_index);
SSLexLexeme*	SSLexLexemeCreate( SSLexStringConsumer*, SSUnsigned32, SSLexMark*);
SSLexLexeme*	SSLexLexemeCreateFromString( const char*, SSUnsigned32);
void			SSLexLexemeDestroy( SSLexLexeme* q_pLexeme);
void			SSLexStringConsumerFlushLexeme( SSLexStringConsumer* q_pCon, SSLexMark* q_pMark);
void			SSLexSetKeywordIndex( SSLex* q_pLex, SSUnsigned32* q_pArray, SSUnsigned32 q_count);
void			SSLexSetKeywordTable( SSLex*, SSUnsigned32*, SSUnsigned32);
void			SSLexKeywordTableFindToken( SSLexTable*, SSUnsigned32* q_pToken);
void			SSLexTableSetKeywordIndex( SSLexTable* q_pTable, SSUnsigned32* q_pArray, SSUnsigned32 q_count);
int				SSLexKeywordIndexSortByToken( void* q_pElem0, void* q_pElem1);
int				SSLexKeywordIndexSortByRealIndex( void* q_pElem0, void* q_pElem1);
void			SSLexDestroy( SSLex*);

#endif
