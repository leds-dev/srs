#ifndef SSLEXCONSUMERCH
#define SSLEXCONSUMERCH
#include "SSLexTableC.h"

#define SSLexStringConsumerBuffInc 512

typedef unsigned short SSUnicode;
typedef int ( *SSLexStringConsumerCallback)( void*, unsigned char*);
typedef int ( *SSLexStringConsumerUnicodeCallback)( void*, SSUnicode*);

typedef struct _SSLexStringConsumer
{
	void*                               opParm;
	SSUnsigned32                        oulInc;
	SSUnsigned32                        oulLine;
	SSUnsigned32                        oulBuff;
	SSUnsigned32                        oulMark;
	char*                               opchBuff;
	SSUnicode*                          opuchBuff;
	SSUnsigned32                        oulStart;
	SSUnsigned32                        oulIndex;
	SSUnsigned32                        oulLength;
	SSUnsigned32                        oulOffset;
	SSUnsigned32                        oulCurrent;
	SSBooleanValue                      oEndOfData;
	SSLexStringConsumerCallback         opCallback;
	SSLexStringConsumerUnicodeCallback  opUCallback;
	SSUnsigned32                        oulConsumed;
	SSUnsigned32                        oulScanLine;
	SSUnsigned32                        oulMarkLine;
	SSUnsigned32                        oulScanOffset;
	SSUnsigned32                        oulMarkOffset;
} SSLexStringConsumer;

typedef struct _SSLexMark
{
	SSUnsigned32	oulMark;
	SSUnsigned32	oulLine;
	SSUnsigned32	oulOffset;
} SSLexMark;


SSLexStringConsumer*	SSLexCreateStringConsumer( const char*);
SSLexStringConsumer*	SSLexCreateUnicodeStringConsumer( SSUnicode*, int);
void					SSLexStringConsumerSetCallback( SSLexStringConsumer*, SSLexStringConsumerCallback, void*);
void					SSLexStringConsumerSetUnicodeCallback( SSLexStringConsumer*, SSLexStringConsumerUnicodeCallback, void*);
SSBooleanValue			SSLexStringConsumerGetNext( SSLexStringConsumer* q_pCon);
SSLexMark				SSLexStringConsumerGetMark( SSLexStringConsumer* q_pCon);
void					SSLexStringConsumerFlushLexeme( SSLexStringConsumer* q_pCon, SSLexMark*);
void					SSLexDestroyStringConsumer( SSLexStringConsumer*);

#endif