
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the SSLEXDLL_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// SSLEXDLL_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
#ifdef SSLEXDLL_EXPORTS
#define SSLEXDLL_API __declspec(dllexport)
#else
#define SSLEXDLL_API __declspec(dllimport)
#endif

#define SSLexRegExprPop		0x00000001
#define SSLexRegExprIgnore	0x00000002
#define SSLexKeywordNocase	0x00000004

SSLEXDLL_API void*	SSLexRegExprCreate( const char* q_expr, int q_id, int q_flags, int q_ptoken, const char* q_table, const char* q_keyTable);
SSLEXDLL_API void	SSLexRegExprDestroy( void* q_handle);
SSLEXDLL_API void*	SSLexRegExprListCreate( const char* q_name);
SSLEXDLL_API void	SSLexRegExprListDestroy( void* q_handle);
SSLEXDLL_API int	SSLexRegExprListAdd( void* q_list, void* q_expr);
SSLEXDLL_API void*	SSLexKeywordCreate( const char* q_key, int q_id, int q_flags);
SSLEXDLL_API void	SSLexKeywordDestroy( void* q_handle);
SSLEXDLL_API void*	SSLexKeywordListCreate( const char* q_name);
SSLEXDLL_API int	SSLexKeywordListAdd( void* q_list, void* q_key);
SSLEXDLL_API void	SSLexKeywordListDestroy( void* q_handle);
SSLEXDLL_API void*	SSLexDynTableCreate( void);
SSLEXDLL_API int	SSLexDynTableAddKeywordList( void* q_dyn, void* q_keyList);
SSLEXDLL_API int	SSLexDynTableAddRegExprList( void* q_dyn, void* q_regList);
SSLEXDLL_API void	SSLexDynTableDestroy( void* q_handle);
SSLEXDLL_API void*	SSLexDynTableMakeLexTable( void* q_dyn, const char* q_name, int q_ret);



