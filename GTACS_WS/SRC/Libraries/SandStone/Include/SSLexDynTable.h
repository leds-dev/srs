#ifndef SSLEXDYNTABLEH
#define SSLEXDYNTABLEH
#include "sslex.hpp"
#include "SSLexDll.h"

class SSLexRegExpr
{
public:
	SSLexRegExpr( const char*, int, int = 0, int = 0, const char* = 0, const char* = 0);
	void* handle( void) { return m_hRegExpr;}
	~SSLexRegExpr( void);
protected:
	void* m_hRegExpr;
};

class SSLexRegExprList
{
public:
	SSLexRegExprList( const char*);
	int add( SSLexRegExpr*);
	void* handle( void) { return m_hRegExprList;}
	~SSLexRegExprList( void);
protected:
	void* m_hRegExprList;
};

class SSLexKeyword
{
public:
	SSLexKeyword( const char*, int, int);
	void* handle( void) { return m_hKeyword;}
	~SSLexKeyword( void);
protected:
	void* m_hKeyword;
};

class SSLexKeywordList
{
public:
	SSLexKeywordList( const char*);
	int add( SSLexKeyword*);
	void* handle( void) { return m_hKeywordList;}
	~SSLexKeywordList( void);
protected:
	void* m_hKeywordList;
};

class SSLexDynTable
{
public:
	SSLexDynTable( void);
	int add( SSLexRegExprList*);
	int add( SSLexKeywordList*);
	SSLexTable* makeLexTable( const char* = 0, int = 1);
	~SSLexDynTable( void);
protected:
	void* m_hDynTable;
};


#endif