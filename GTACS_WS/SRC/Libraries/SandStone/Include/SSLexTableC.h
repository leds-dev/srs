#if !defined( SSLEXTABLECH)
#define SSLEXTABLECH
#include "stdlib.h"
#include "string.h"

#define SSLexStateInvalid         0xffffffff

#define SSLexFinalStateFlagsContextStart 0x00000001
#define SSLexFinalStateFlagsStartOfLine  0x00000002
#define SSLexFinalStateFlagsPop          0x00000008
#define SSLexFinalStateFlagsFinal        0x00000010
#define SSLexFinalStateFlagsPush         0x00000020
#define SSLexFinalStateFlagsIgnore       0x00000040
#define SSLexFinalStateFlagsContextEnd   0x00000080
#define SSLexFinalStateFlagsReduce		 0x00000100
#define SSLexFinalStateFlagsKeyword      0x00000200
#define SSLexFinalStateFlagsParseToken   0x00000400

#define SSRetVoidP void* 
typedef unsigned int SSUnsigned32;
typedef unsigned int SSBooleanValue;

#define SSTrue	1
#define SSFalse	0

typedef struct _SSLexTableHeader
{
	SSUnsigned32 oulSize;
	SSUnsigned32 oulType;
	SSUnsigned32 oulKeywordSize;
	SSUnsigned32 oulClassesSize;
	SSUnsigned32 aoulReserved[ 6];
} SSLexTableHeader;

typedef struct _SSLexPairTableHeader
{
	SSUnsigned32 oulType;
	SSUnsigned32 oulSize;
	SSUnsigned32 oaulReserved[ 8];
	SSUnsigned32 oulRows;
	SSUnsigned32 oulPush;
	SSUnsigned32 oulIndex;
	SSUnsigned32 oulFinal;
	SSUnsigned32 ulReserved[ 8];
} SSLexPairTableHeader;

typedef struct _SSLexPairTableRowEntry
{
	SSUnsigned32 oulStart;
	SSUnsigned32 oulEnd;
	SSUnsigned32 oulState;
} SSLexPairTableRowEntry;

typedef struct _SSLexPairTableRow
{
	SSUnsigned32 oulSize;
} SSLexPairTableRow;

typedef struct _SSLexFinalState
{
	SSUnsigned32 oulId;
	SSUnsigned32 oulPush;
	SSUnsigned32 oulFlags;
	SSUnsigned32 oulKeyword;
	SSUnsigned32 oulParseToken;
	SSUnsigned32 oulReserved[ 2];
} SSLexFinalState;

typedef struct _SSLexKeywordTable
{
	SSUnsigned32 oSize;
	SSUnsigned32 oIndexSize;
	SSUnsigned32 oReserved[ 8];
} SSLexKeywordTable;

typedef struct _SSLexKeywordIndex
{
	SSUnsigned32 oToken;
	SSUnsigned32 oNocase;
	SSUnsigned32 oParseToken;
	SSUnsigned32 oRealIndex;
} SSLexKeywordIndex;

typedef struct _SSLexCharacterClassEntry
{
	SSUnsigned32				oulMin;
	SSUnsigned32				oulMax;
} SSLexCharacterClassEntry;

typedef struct _SSLexCharacterClass
{
	SSUnsigned32				oulMin;
	SSUnsigned32				oulMax;
	SSUnsigned32				oulSize;
	SSLexCharacterClassEntry*	opArray;
} SSLexCharacterClass;

typedef struct _SSLexTable
{
	SSLexTableHeader*		opTable;
	const char***			oachKeys;
	SSLexKeywordTable**		oapKeywordList;
	SSUnsigned32			oulKeywordList;
	SSLexKeywordIndex**		oapKeywordIndex;
	SSLexPairTableHeader**	oapExpressionList;
	SSUnsigned32			oulExpressionList;
/*
	Added for character classes
*/
	SSLexCharacterClass**	oapCharacterClass;
	SSUnsigned32			oulCharacterClass;
	SSUnsigned32			oulCharacterClassMin;
	SSUnsigned32			oulCharacterClassMax;
/*
	Added for new keyword search
*/
	SSUnsigned32			oIndirectLen;
	SSUnsigned32*			opIndirectKey;
	SSLexKeywordIndex*		opKeywordSortByToken;
} SSLexTable;

typedef struct _SSLexLexeme
{
	SSUnsigned32    oulLine;
	char*			opchBuff;
	SSUnsigned32	oulToken;
	SSUnsigned32    oulOffset;
	SSUnsigned32    oulLength;
	SSUnsigned32	oulParseToken;
} SSLexLexeme;

void*				SSLexCreateTable( SSUnsigned32*, SSUnsigned32**, SSUnsigned32**, SSUnsigned32**, const char***, SSUnsigned32**);
SSUnsigned32		SSLexTableLookupState( SSLexPairTableHeader* q_pLex, SSUnsigned32 qulRow, SSUnsigned32 qulTrans);
SSLexFinalState*	SSLexTableLookupFinal( SSLexPairTableHeader* q_pLex, SSUnsigned32 qulState);
void				SSLexTableFindKeyword( SSLexTable* q_pTable, SSUnsigned32 q_index, SSLexLexeme* q_pLexeme);
void				SSLexTableFindKeywordIndirect( SSLexTable* q_pTable, SSLexLexeme* q_pLexeme);
SSBooleanValue		SSLexTableTranslateClass( SSLexTable*, SSUnsigned32*);
void				SSLexDestroyTable( void* q_pTable);

#endif