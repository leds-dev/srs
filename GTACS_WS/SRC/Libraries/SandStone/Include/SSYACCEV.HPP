/*---------------------------------------------------------------------------

					Copyright (c) 1994-1997 SandStone Technology Inc.
								  All rights reserved

		THIS PROGRAM CONSISTS OF TRADE SECRETS THAT ARE  THE PROPERTY
		OF SANDSTONE SOFTWARE INC..  THE CONTENTS  MAY NOT BE USED OR
		DISCLOSED WITHOUT THE EXPRESS WRITTEN PERMISSION OF THE OWNER.

----------------------------------------------------------------------------*/
#if !defined( SSYACCEVHPP)
#  define SSYACCEVHPP
#  include<ssyacc.hpp>
#  include<sslexev.hpp>
#  include<ssyacctb.hpp>

#  define SSYaccEventExceptionGetLexeme  1
#  define SSYaccEventExceptionPastAccept 2
#  define SSYaccEventMsgGetLexeme        "SSYaccEv0001e: Invalid return from lex, %ld"
#  define SSYaccEventMsgPastAccept       "SSYaccEv0002e: Already accepted"
#  define SSYaccEventMsgReduce			  "SSYaccEv0003e: Must override reduce function"

	class SSClass SSYaccEvent : public SSYacc
		{
		public:

			enum SSYaccEventType
				{
				start = 0,
				lexOk = SSLexEvent::lexOk,
				lexMore = SSLexEvent::lexMore,
				lexDone = SSLexEvent::lexDone,
				lexError = SSLexEvent::lexError,
				yaccError = SSLexEvent::lexLast,
				yaccShift = SSLexEvent::lexLast << 1,
				yaccReduce = SSLexEvent::lexLast << 2,
				yaccAccept = SSLexEvent::lexLast << 3,
				yaccPermError = SSLexEvent::lexLast << 4,
				yaccLarError = SSLexEvent::lexLast << 5,
				yaccLarLook = SSLexEvent::lexLast << 6,
				yaccConflict = SSLexEvent::lexLast << 7
				};

			SSConstr					SSYaccEvent( SSLexEvent&, const char*,
											SSUnsigned32 = lexMore);
			SSInline SSConstr           SSYaccEvent( SSLexEvent&, SSYaccTable&,
													  SSUnsigned32 = lexMore);

			SSBooleanValue               error( SSUnsigned32, SSLexLexeme&);
			SSYaccStackElement*          reduce( SSUnsigned32, SSUnsigned32);

			SSLexEvent&                  lex( void);
			SSYaccEventType 	           parse( void);
			void								  doLook( void);
			SSBooleanValue				 recover( void);
			SSInline SSYaccStackElement* element( void);
			SSYaccEventType              getLexeme( void);
			void								  getLexLook( void);
			void								  getLookLexeme( void);
			SSYaccEventType              translateAction( void);
			SSInline void                setElement( SSYaccStackElement&);

		protected:
			SSUnsigned32					  oulLar;
			SSUnsigned32                 oEvents;
			SSYaccEventType              oReturn;
			SSBooleanValue					  oConflict;
			SSLexEvent::SSLexEventType   oLexReturn;
		};

	SSInline SSLexEvent& SSYaccEvent::lex( void)
		{
		return ( SSLexEvent&) ( *(( SSLexEvent*) opLex));
		}

	SSInline SSYaccEvent::SSYaccEvent( SSLexEvent& qLex, const char* qpszTable,
		SSUnsigned32 qEvents) :	SSYacc( qLex, qpszTable), oReturn( start),
		oEvents( qEvents), oConflict( SSFalse), oulLar( 0)
		{
		oEvents |= yaccAccept | yaccPermError;
		lex().setEvents( oEvents);
		}

	SSInline SSYaccEvent::SSYaccEvent( SSLexEvent& qLex, SSYaccTable& qTable,
		SSUnsigned32 qEvents) :	SSYacc( qLex, qTable), oReturn( start),
		oEvents( qEvents), oConflict( SSFalse), oulLar( 0)
		{
		oEvents |= yaccAccept | yaccPermError;
		lex().setEvents( oEvents);
		}

	SSInline SSYaccStackElement* SSYaccEvent::element( void)
		{
		return oElement;
		}

	SSInline void SSYaccEvent::setElement( SSYaccStackElement& qEle)
		{
		oElement = qEle;
		}

#endif
