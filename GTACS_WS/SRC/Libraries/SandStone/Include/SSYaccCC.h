#if !defined( SSYACCCCH)
#define SSYACCCCH

#include "SSLexCC.h"
#include "SSYaccTableC.h"

#define SSYaccEofLexeme				"eof"
#define SSYaccEofToken				0xffffffff
#define SSYaccStackElementMaxLen	256
#define SSYaccMaxChild				16

enum SSYaccErrorType
{
	SSYaccErrorLookahead = 1,
	SSYaccErrorStackElement,
	SSYaccErrorGoto,
	SSYaccErrorParse,
	SSYaccErrorConflict,
	SSYaccErrorAction
};

typedef struct _SSYaccTreeNode
{
	int						oSize;
	SSLexLexeme*			opLexeme;
	int						oProduction;
	struct _SSYaccTreeNode*	opChild[ SSYaccMaxChild];
} SSYaccTreeNode;

typedef struct _SSYaccStackElement
{
	struct _SSYaccStackElement*	opNext;
	SSYaccTreeNode*				opTree;
	SSLexLexeme*				opLexeme;
	SSUnsigned32				oulState;
	char						oExtra[ SSYaccStackElementMaxLen];
} SSYaccStackElement;

typedef void* ( *SSYaccReduceCallback)( void*, SSUnsigned32, SSUnsigned32, void*);
typedef struct _SSYacc
{
	SSLex*					opLex;
	SSUnsigned32			oAction;
	SSYaccStackElement*		opStack;
	SSYaccTable*			opTable;
	SSYaccReduceCallback	oReduce;
	SSUnsigned32			oulState;
	SSYaccStackElement*		oElement;
	SSLexLexeme*			oLookahead;
	SSLexLexeme*			opEndLexeme;
	SSBooleanValue			oEndOfInput;
	SSUnsigned32			oulLeftside;
	SSUnsigned32			oulProduction;
	SSUnsigned32			oulProductionSize;
} SSYacc;

SSYacc*					SSYaccCreate( SSYaccTable* q_pTable, SSLex* q_pLex, SSYaccReduceCallback q_pReduce);
void					SSYaccDestroy( SSYacc* q_pYacc);
int						SSYaccParse( SSYacc* q_pYacc);
void					SSYaccLookupAction( SSYacc* q_pYacc, SSUnsigned32 qulState, SSUnsigned32 qulToken);
SSYaccPairTableEntry*	SSYaccLookupGoto( SSYacc* q_pYacc, SSUnsigned32 qulState, SSUnsigned32 qulToken);
SSYaccStackElement*		SSYaccStackElementCreate( SSLexLexeme* q_pLexeme, SSUnsigned32 qulState);
void*					SSYaccStackElementGetExtra( SSYaccStackElement* q_pEle);
SSYaccStackElement*		SSYaccGetElementFromProduction( SSYacc* q_pYacc, int q_index);
SSLexLexeme*			SSYaccGetNextLexeme( SSYacc* q_pYacc);
void					SSYaccDestroyStackElement( SSYaccStackElement* q_pEle);
SSUnsigned32*			SSYaccGetValidLookaheads( SSYacc* q_pYacc, SSUnsigned32 qulState, SSUnsigned32* q_pCount);
SSYaccStackElement*		SSYaccPropagateLexeme( SSYacc *pYacc, int i);
SSLexLexeme*			SSYaccGetLexeme( SSYacc*, int i);
SSLexLexeme*			SSYaccCopyLexeme( SSYacc*, int i);
SSYaccTreeNode*			SSYaccBuildTree( SSYacc*);
void					SSYaccPrintTree( SSYaccTreeNode*);

#endif
