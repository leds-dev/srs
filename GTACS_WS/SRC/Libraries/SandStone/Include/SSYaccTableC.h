#ifndef SSYACCTABLECH
#define SSYACCTABLECH
#include "SSLexTableC.h"
#include "stdlib.h"
#include "string.h"

#define SSYaccPairTableEntryMask		0xf8000000
#define SSYaccPairTableEntrySync		0x80000000
#define SSYaccPairTableEntryShift		0x40000000
#define SSYaccPairTableEntryReduce		0x20000000
#define SSYaccPairTableEntryAccept		0x10000000
#define SSYaccPairTableEntryConflict	0x08000000
#define SSYaccPairTableEntryMax			~SSYaccPairTableEntryMask

enum SSYaccActionType
{
	SSYaccActionShift,
	SSYaccActionError,
	SSYaccActionReduce,
	SSYaccActionAccept,
	SSYaccActionConflict
};

typedef struct _SSYaccPairTable
{
	SSUnsigned32 oulType;
	SSUnsigned32 oulProd;
	SSUnsigned32 oulStates;
	SSUnsigned32 oulRowOffset;
	SSUnsigned32 oulProdOffset;
} SSYaccPairTable;

typedef struct _SSYaccPairTableRow
{
	SSUnsigned32 oulFlag;
	SSUnsigned32 oulGoto;
	SSUnsigned32 oulAction;
} SSYaccPairTableRow;

typedef struct _SSYaccPairTableEntry
{
	SSUnsigned32 oulEntry;
	SSUnsigned32 oulToken;
} SSYaccPairTableEntry;

typedef struct _SSYaccPairTableProd
{
	SSUnsigned32 oulSize;
	SSUnsigned32 oulLeftside;
} SSYaccPairTableProd;

typedef struct _SSYaccTable
{
	SSYaccPairTable* opPair;
} SSYaccTable;

SSYaccTable* SSYaccCreateTable( void*);
SSYaccPairTableEntry* SSYaccPairTableRowLookupAction( SSYaccPairTableRow* q_pRow, SSUnsigned32 qulToken);
SSYaccPairTableEntry* SSYaccPairTableRowLookupGoto( SSYaccPairTableRow* q_pRow, SSUnsigned32 qulGoto);
SSYaccPairTableRow* SSYaccTableLookupRow( SSYaccTable* q_pTable, SSUnsigned32 qulRow);
SSYaccPairTableProd* SSYaccTableLookupProd( SSYaccTable* q_pPair, SSUnsigned32 qulProd);
int SSYaccPairTableEntryGetAction( SSYaccPairTableEntry* q_pEntry);
void SSYaccDestroyTable( SSYaccTable*);

#endif
