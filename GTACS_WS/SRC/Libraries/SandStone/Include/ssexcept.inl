/*---------------------------------------------------------------------------

               Copyright (c) 1994 - 1997 SandStone Technology Inc.
                          All rights reserved

----------------------------------------------------------------------------*/
   SSInline SSException::SSException( const char* qpszText, 
      SSUnsigned32 qulId) : oulId( qulId)
      {
      setText( qpszText);
      }

   SSInline SSException::SSException( const SSException& qExcept) :
      oulId( qExcept.oulId)
      {
      setText( qExcept.oachText);
      }

   SSInline const char* SSException::text( void) const
      {
      return oachText;
      }

   SSInline SSUnsigned32 SSException::errorId( void) const
      {
      return oulId;
      }

   SSInline SSExceptionParm* SSException::parm( SSUnsigned32 qulIndex)
      {
      if ( qulIndex >= SSExceptionParmArray) return 0;
      return &oaParm[ qulIndex];
      }

   SSInline SSException::~SSException( void)
      {
      }

   SSInline SSExceptionParm::SSExceptionParm( void) :
      oType( unsigned32), oulParm( 0)
      {
      }

   SSInline SSExceptionParm::SSExceptionParm( const void* qpParm) : 
      oType( constVoid), opParm( qpParm)
      {
      }

   SSInline SSExceptionParm::SSExceptionParm( const char* qpchParm) :
      oType( constChar)
      {
      opchParm = new char[ strlen( qpchParm) + 1];
      strcpy_s( opchParm,(strlen( qpchParm) + 1),  qpchParm);
      }

   SSInline SSExceptionParm::SSExceptionParm( SSUnsigned32 qulParm) :
      oType( unsigned32), oulParm( qulParm)
      {
      }

   SSInline SSExceptionParm::SSExceptionParm( const SSExceptionParm& qParm) :
      oType( qParm.oType), opParm( qParm.opParm)
      {
      if ( oType == constChar)
         {
         opchParm = new char[ strlen( qParm.opchParm) + 1];
         strcpy_s( opchParm, (strlen( qParm.opchParm) + 1),  qParm.opchParm);
         }
      }

   SSInline const void* SSExceptionParm::asConstVoid( void) const
      {
      return opParm;
      }

   SSInline const char* SSExceptionParm::asConstChar( void) const
      {
      return opchParm;
      }

   SSInline SSUnsigned32 SSExceptionParm::asUnsigned32( void) const
      {
      return oulParm;
      }

   SSInline SSExceptionParm::operator const void*( void)
      {
      return opParm;
      }

   SSInline SSExceptionParm::operator const char*( void)
      {
      return opchParm;
      }

   SSInline SSExceptionParm::operator SSUnsigned32( void)
      {
      return oulParm;
      }

   SSInline SSExceptionParm::SSExceptionParmType SSExceptionParm::type( void)
      const
      {
      return oType;
      }

   SSInline SSBooleanValue SSExceptionParm::isConstVoid( void) const
      {
      return oType == constVoid ? SSTrue : SSFalse;
      }

   SSInline SSBooleanValue SSExceptionParm::isConstChar( void) const
      {
      return oType == constChar ? SSTrue : SSFalse;
      }

   SSInline SSBooleanValue SSExceptionParm::isUnsigned32( void) const
      {
      return oType == unsigned32 ? SSTrue : SSFalse;
      }

   SSInline SSExceptionParm& SSExceptionParm::operator=( const void* qpParm)
      {
      oType = constVoid;
      opParm = qpParm;
      return *this;
      }

   SSInline SSExceptionParm& SSExceptionParm::operator=( const char* qpchParm)
      {
      if ( oType == constChar && opchParm) delete [] opchParm;
      oType = constChar;
      opchParm = new char[ strlen( qpchParm) + 1];
      strcpy_s( opchParm, strlen( qpchParm) + 1, qpchParm);
      return *this;
      }

   SSInline SSExceptionParm& SSExceptionParm::operator=( SSUnsigned32 qulParm)
      {
      oType = unsigned32;
      oulParm = qulParm;
      return *this;
      }

   SSInline SSExceptionParm::~SSExceptionParm( void)
      {
      if ( isConstChar()) delete [] opchParm;
      }

   SSInline SSExceptionPresent::SSExceptionPresent( void) : opException( 0)
      {
      }

   SSInline SSException* SSExceptionPresent::hasException( void) const
      {
      return opException;
      }

   SSInline SSException* SSExceptionPresent::getException( void)
      {
      SSException* zpException = opException;
      opException = 0;
      return zpException;
      }

   SSInline SSException* SSExceptionPresent::setException( 
      SSException* qpException)
      {
      opException = qpException;
      return opException;
      }
