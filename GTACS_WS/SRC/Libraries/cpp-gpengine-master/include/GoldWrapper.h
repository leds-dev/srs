#pragma once

#include "ssyacc.hpp"
#include "ssyacctb.hpp"
#include "cgtFile.h"

class ErrorTable;


// this class uses the astudillo gold parser library in conjunction with the visual parse++
// library. the goal was to support legacy code that used parse++ and to minimize the
// changes but to replace the guts with a parser whose grammar could be changed. 

class GoldWrapper : public SSYacc
{
public:
	/* ctor takes grammar file */
	GoldWrapper(const char *f);

	/* this is file to parse */
	void loadSource(char* filename);

	/* get the next token from the input file */
	SSLexLexeme* nextToken();

	/* looks up the next action (shift, reduce, etc.) based on the given state and the upcoming token*/
	SSYaccPairTableEntry* lookupGoldAction(SSUnsigned32 qulState, SSUnsigned32 qulToken);

	/* gets the production rule based on the current rule index*/
	SSYaccPairTableProd* GoldWrapper::lookupProd(SSUnsigned32 entry);
	integer lookupGoldGoto(SSUnsigned32 state, SSUnsigned32 token);
	SSUnsigned32* validLookaheads( SSUnsigned32 qulState, SSUnsigned32& qulCount);

	ErrorTable* getErrors();

	// methods for lexing
	void lookupAction( SSUnsigned32, SSUnsigned32);
	SSBooleanValue lookupGoto( SSUnsigned32, SSUnsigned32);
	SSLexLexeme* nextLexeme( void);
	SSBooleanValue error(SSUnsigned32 qulState, SSLexLexeme& qLookahead);

	virtual SSDestr GoldWrapper(void);

private:
	CGTFile myGrammarFile;
	char* myFilename;
	DFA* myDFA;
	// keep track of things allocated so proper cleanup can be done
	vector <Token*> myTokens;
	vector<SSYaccPairTableProd*> myProds;
	vector<SSYaccPairTableEntry*> myPairs;
	vector <SSUnsigned32*> myLAHs;
	vector <SSLexLexeme*> myLexemes;
	// keeps track of location inside token vector
	unsigned int currentIndex;

};
