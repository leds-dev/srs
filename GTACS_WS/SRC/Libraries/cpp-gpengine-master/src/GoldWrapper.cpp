#include <map>
#include "SSEXCEPT.h"
#include "SSSETARR.hpp"
#include "GoldWrapper.h"

#include "SSLIFO.hpp"
typedef SSLifoArray< SSYaccStackElementRef> SSYaccStack;

char* read_source(char*);

/**
/////////////////////////////////////////////////////////////////////////////
// GoldWrapper.cpp :                                                       //
//                                                                         //
/////////////////////////////////////////////////////////////////////////////
// This class is the wrapper that encapsulates the gold parser and the parse++ library.
// Essentially it reads the gold compiled grammar file and creates rules tables so that
// the parse++ algorithm can execute as designed while the underlying functions are
// implemented using the gold engine. The parse++ code calls methods to extract information
// from the rules tables. Inheritance of this wrapper class enables these methods to be
// overridden because they return what is expected by the parse++ code. 
// 
/////////////////////////////////////////////////////////////////////////////
**/

GoldWrapper::GoldWrapper(const char* filename) : SSYacc(0), currentIndex(0), 
	myTokens(0), myProds(0), myPairs(0), myLAHs(0), myLexemes(0) 
{
	if (myGrammarFile.load(const_cast<char *>(filename)) == NULL)
	{
		throwException( SSExceptionYaccResource, SSYaccMsgResource, filename);
	}
}

void GoldWrapper::loadSource(char* filename)
{
	myFilename = filename;
	currentIndex = 0;
	myTokens.clear();
	//open file and get tokens
	char* source = read_source(filename);
	if (source == NULL)
	{
		throwException( SSExceptionYaccResource, SSLexMsgFileOpen, filename);
	}
	// Get DFA (Deterministic Finite Automata) from the cgt file
	myDFA = myGrammarFile.getScanner();
	// Scan the source in search of tokens
	myDFA->scan(source);
	myTokens = myDFA->getTokens();
	delete [] source;
}

// get the next token from the file 
SSLexLexeme* GoldWrapper::nextToken()
{
	SSLexLexeme* lexeme = 0;
	if (currentIndex < myTokens.size())
	{
		Token *token = myTokens[currentIndex];

		wstring tokenString = token->image;
		size_t sz = tokenString.size() + 1; // add place for nulll terminator
		char *ctok = (char*)malloc(sz);
		size_t actSz;
		wcstombs_s(&actSz, ctok,sz, tokenString.c_str(), _TRUNCATE);
		lexeme = new SSLexLexeme((SSBooleanValue)SSTrue);
		lexeme->setBuffer(ctok);
		lexeme->setLine(token->line);
		lexeme->setOffset(token->col);
		int t = token->symbolIndex;
		lexeme->setToken(t);
		currentIndex++;

		myLexemes.push_back(lexeme);
	}
	else
	{
		oEndOfInput = SSTrue;
		oLookahead = orEndLexeme;
	}
	return lexeme;
}

SSLexLexeme* GoldWrapper::nextLexeme( void)
{
	return nextToken();
}

// get next action based on the given state and token. look it up in the LALR state table
SSYaccPairTableEntry *GoldWrapper::lookupGoldAction (SSUnsigned32 qulState, SSUnsigned32 qulToken)
{
	LALR* lalr = myGrammarFile.getParser();
	Action* currentAction = lalr->getNextAction((unsigned short)qulToken, (unsigned short)qulState);
	if (!currentAction)
		return NULL;

	SSYaccPairTableEntry* pte = new SSYaccPairTableEntry();
	myPairs.push_back(pte);
	pte->setEntry(currentAction->target);
	pte->setToken(qulToken);
	switch (currentAction->action)
	{
		case ACTION_SHIFT:
			pte->setShift(currentAction->target);
			break;
		case ACTION_REDUCE:
			pte->setReduce(currentAction->target);
			break;
		case ACTION_GOTO:
			pte->setGoto(currentAction->target);
			break;
		case ACTION_ACCEPT:
			pte->setAccept();
			break;
	}
	return pte;
}

void GoldWrapper::lookupAction( SSUnsigned32 qulState, SSUnsigned32 qulToken)
{
	SSYaccPairTableEntry* zpEntry =  lookupGoldAction(qulState, qulToken);
	SSYaccPairTable* zpTable = ( SSYaccPairTable*) opTab;

	if ( !zpEntry) 
		oAction = SSYaccAction::error;
	else
	{
		switch ( oAction = zpEntry->action())
		{
		case SSYaccAction::shift:
			SSYacc::oulState = zpEntry->entry();
			break;

		case SSYaccAction::reduce:
			{
				SSYaccPairTableProd* zpProd = lookupProd(zpEntry->entry());
				oulProduction = zpEntry->entry();
				oulLeftside = zpProd->leftside();
				oulProductionSize = zpProd->size();
				break;
			}

		case SSYaccAction::conflict:
			SSYacc::opIterator = SSYacc::opTable->larIterator( zpEntry->entry());
			break;

		default:
			break;
		}
	}
}

SSBooleanValue GoldWrapper::lookupGoto( SSUnsigned32 qulState, 
   SSUnsigned32 qulToken)
{
   integer newState = lookupGoldGoto(qulState, qulToken);
   if (newState == 0)
   {
	   SSYacc::throwException( SSExceptionYaccGoto, SSYaccMsgGoto, qulToken);
	   return SSTrue;
   }
   else
	   SSYacc::oulState = newState;

   return SSFalse;
}

// this method is used when doing a goto. the target in the LALR table points to the appropriate
// rule
SSYaccPairTableProd* GoldWrapper::lookupProd(SSUnsigned32 e)
{
	LALR* lalr = myGrammarFile.getParser();

	SSYaccPairTableProd *prod = new SSYaccPairTableProd();
	prod->setSize((SSUnsigned32) lalr->ruleTable->rules[e].symbols.size());
	prod->setLeftside((SSUnsigned32) lalr->ruleTable->rules[e].symbolIndex);

	myProds.push_back(prod);
	return prod;
}

// returns a vector of valid symbols based on the given state. fills in the count and returns
// the vector. used in error processing.
SSUnsigned32* GoldWrapper::validLookaheads( SSUnsigned32 qulState, SSUnsigned32& qulCount)
{
	LALR* lalr = myGrammarFile.getParser();

	if (lalr->stateTable->nbrEntries > qulState)
	{
		qulCount = lalr->stateTable->states[qulState].actions.size();
		SSUnsigned32* zpArray = new SSUnsigned32[qulCount];
		myLAHs.push_back(zpArray);
		for (unsigned i = 0; i < qulCount; i++)
		{
			zpArray[i] = lalr->stateTable->states[qulState].actions[i].symbolIndex;
		}
		return zpArray;
	}
	else
		SSYacc::throwException( SSExceptionYaccLook, SSYaccMsgLook, qulState);
	return 0;
}

// lookup the next state given the current state and the given token
integer GoldWrapper::lookupGoldGoto(SSUnsigned32 qulState, SSUnsigned32 qulToken)
{
	LALR* lalr = myGrammarFile.getParser();

	if (lalr->stateTable->nbrEntries > qulState)
	{
		LALRState currentState = lalr->stateTable->states[qulState];
		for (unsigned long i = 0; i < currentState.actions.size(); i++)
			if (currentState.actions[i].symbolIndex == qulToken)
				return currentState.actions[i].target;
	}
	return -1;
}

ErrorTable* GoldWrapper::getErrors()
{
	// Get the error table
	return myDFA->getErrors();
}

SSBooleanValue GoldWrapper::error(SSUnsigned32 qulState, SSLexLexeme& qLookahead)
{
	set<integer> possibleTokens = myGrammarFile.getParser()->getPossibleTokenSymbols(integer(qulState));
	map<SSUnsigned32, SSUnsigned32> possibleMap;
	// look at stack to build set of tokens that can have an error after them
	SSYaccStackElement* elem;
	SSYaccStack& zStack = ( *(( SSYaccStack*) opStack));
	SSYaccPairTable* zpTable = ( SSYaccPairTable*) opTab;
	for ( unsigned int i = 0; i < zStack.elements(); i++)
	{
		elem = elementFromStack(i);
		SSYaccPairTableEntry* pte = lookupGoldAction(elem->state(), myGrammarFile.getErrorSymbol());
		if (pte && pte->action() == SSYaccAction::shift)
		{
			Action act;
			act.action = pte->action();
			act.symbolIndex = (integer) pte->token();
			act.target = (integer) pte->entry();
			vector<Action> moreAction = myGrammarFile.getParser()->getPossibleActions(act.target);
			for (vector<Action>::const_iterator p = moreAction.begin(); p != moreAction.end(); ++p)
			{
				possibleMap.insert(make_pair(p->symbolIndex, act.target));
			}
		}
	}

	// if not possible tokens, we must exit
	if (!possibleMap.size())
	{
		throwException(SSExceptionYaccTokenState, SSYaccMsgTokenState);
		return SSTrue;
	}
	// now look at the input and skip tokens till one found that can follow the error one
	for (;;)
	{
		if (possibleMap.find(lookahead().token()) == possibleMap.end())
		{
			if (doGetLexeme( SSFalse))
			{
				throwException(SSExceptionYaccErrorTokenEof, SSYaccMsgErrorTokenEof);
				return SSTrue;
			}
		}
		else
			break;
	}
	// pop tokens off the stack until get to one that has action other than error
	for (;;)
	{
		boolean found = false;
		lookupAction(oulState, lookahead().token());
		if ( oAction == SSYaccAction::shift)
		{
			SSUnsigned32 zulLen = strlen( SSYaccErrorLexeme);
			SSLexLexeme* zpLexeme = new( zulLen) SSLexLexeme( zulLen,
				myGrammarFile.getErrorSymbol(), "%error", 0, 0);
			zpLexeme->refDec( SSFalse);
			oElement = stackElement();
			oElement->setLexeme(*zpLexeme);
			// find the action to get the new state
			map<SSUnsigned32, SSUnsigned32>::iterator it = possibleMap.find(oulState);
			if (it != possibleMap.end())
			{
				oElement->setState(it->second);
				break;
			}
			push();
			break;
		}
		pop();
	}
	return SSFalse;
}


char *read_source (char *filename) {
	errno_t err;
	FILE *file;
	if (err =  fopen_s (&file, filename, "rb"))
	{
	  return NULL;
	}

	fseek (file, 0, SEEK_END);
	int fsize = ftell(file);
	fseek (file, 0, SEEK_SET);

	char *src_code;
	src_code = new char[fsize+1];
	fread (src_code, 1, fsize, file);
	src_code[fsize] = 0;

	fclose (file);
	return src_code;
}

GoldWrapper::~GoldWrapper(void)
{
	while (!myProds.empty())
		delete myProds.back(), myProds.pop_back();
	myProds.clear();

	while (!myLAHs.empty())
		delete myLAHs.back(), myLAHs.pop_back();
	myLAHs.clear();

	while (!myPairs.empty())
		delete myPairs.back(), myPairs.pop_back();
	myPairs.clear();

	for (size_t i = 0; i < myLexemes.size(); i++)
	{
		myLexemes.at(i)->refDec(true);
	}
	myLexemes.clear();
	myTokens.clear();

}
