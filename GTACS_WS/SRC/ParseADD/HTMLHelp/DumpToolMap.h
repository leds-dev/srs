// Microsoft Developer Studio generated Help ID include file.
// Used by ParseADD.rc
//
#define HIDC_BTN_CLEARLOG               0x806603fc    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_CONVERT                0x806603f0    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_HELP                   0x806603fb    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_INPFILE                0x806603e9    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_INTOOUT                0x806603f6    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_MINIMIZE               0x80660403    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_OUTFILE                0x806603eb    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_OUTTOIN                0x806603f5    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_SAVELOG                0x806603fa    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_SWAPIO                 0x806603f7    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_VIEWINPUT              0x80660065    // IDD_PARSEADD_DIALOG
#define HIDC_BTN_VIEWOUTPUT             0x80660066    // IDD_PARSEADD_DIALOG
#define HIDC_CBO_CANTVALUE              0x80660409    // IDD_PARSEADD_DIALOG
#define HIDC_CBO_INPFORM                0x80660400    // IDD_PARSEADD_DIALOG
#define HIDC_CBO_OUTFORM                0x806603ee    // IDD_PARSEADD_DIALOG
#define HIDC_CBO_PITCHBIAS              0x8066040f    // IDD_PARSEADD_DIALOG
#define HIDC_CHK_APPOUT                 0x806603f3    // IDD_PARSEADD_DIALOG
#define HIDC_CHK_FORCE                  0x806603f9    // IDD_PARSEADD_DIALOG
#define HIDC_EDT_CANTVALUE              0x80660401    // IDD_PARSEADD_DIALOG
#define HIDC_EDT_INPFILE                0x806603e8    // IDD_PARSEADD_DIALOG
#define HIDC_EDT_OUTFILE                0x806603ea    // IDD_PARSEADD_DIALOG
#define HIDC_EDT_PITCHBIAS              0x8066040e    // IDD_PARSEADD_DIALOG
#define HIDC_EDT_SCID                   0x806603fd    // IDD_PARSEADD_DIALOG
#define HIDC_EDT_STATUSDUMP             0x806603f2    // IDD_PARSEADD_DIALOG
#define HIDC_PRG_OUTPRGSS               0x806603f1    // IDD_PARSEADD_DIALOG
#define HIDOK                           0x80660001    // IDD_PARSEADD_DIALOG
