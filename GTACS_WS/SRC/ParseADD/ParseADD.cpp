// ParseADD.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "ParseADD.h"
#include "ParseADDDlg.h"
#include <htmlhelp.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CParseADDApp

BEGIN_MESSAGE_MAP(CParseADDApp, CWinApp)
	//{{AFX_MSG_MAP(CParseADDApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CParseADDApp construction

CParseADDApp::CParseADDApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CParseADDApp object

CParseADDApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CParseADDApp initialization

BOOL CParseADDApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

	DWORD dwCookie;

	::HtmlHelp(NULL,NULL,HH_INITIALIZE,(DWORD)&dwCookie);
	CParseADDDlg dlg;
	dlg.DoModal();
	::HtmlHelp(NULL,NULL,HH_UNINITIALIZE,dwCookie);
	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
