// ParseADD.h : main header file for the PARSEADD application
//

#if !defined(AFX_PARSEADD_H__43693E94_7AEA_11D5_9071_00104B335B84__INCLUDED_)
#define AFX_PARSEADD_H__43693E94_7AEA_11D5_9071_00104B335B84__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CParseADDApp:
// See ParseADD.cpp for the implementation of this class
//

class CParseADDApp : public CWinApp
{
public:
	CParseADDApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParseADDApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CParseADDApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARSEADD_H__43693E94_7AEA_11D5_9071_00104B335B84__INCLUDED_)
