// ParseADDDlg.cpp : implementation file
// Please disregard any $functionName lines--they're there so I don't have
// to scroll through tunnz o' functions, just find $functionName, to get there.

// #define OEMRESOURCE
#include "stdafx.h"
#include "ParseADD.h"
#include "ParseADDDlg.h"
#include "resource.hm"
#include "mappings.h"
#include <htmlhelp.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <direct.h>

#define HTMLHelpFile "DumpTool.chm"
#define CVUpdatePd 100	//Update Cant Value text box every 200ms

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#ifdef UNICODE
#define __tfprintf _wfprintf
#else
#define __tfprintf fprintf
#endif

#define __MYDEBUG__

#define NFORMATS 6

//What can take input from these formats?
#define CONV_IBINMAPPINGS  {0,1,0,0,0,0}
#define CONV_IHEXMAPPINGS  {0,0,1,1,1,1}
#define CONV_IPPIXMAPPINGS {0,0,0,0,0,0}
#define CONV_IPPROMAPPINGS {0,0,0,0,0,0}
#define CONV_IPSPIMAPPINGS {0,0,0,0,0,1}
#define CONV_ICSPIMAPPINGS {0,0,0,0,0,0}

//What can output these formats? (flop the above)
#define CONV_OBINMAPPINGS  {0,0,0,0,0,0}
#define CONV_OHEXMAPPINGS  {1,0,0,0,0,0}
#define CONV_OPPIXMAPPINGS {0,1,0,0,0,0}
#define CONV_OPPROMAPPINGS {0,1,0,0,0,0}
#define CONV_OPSPIMAPPINGS {0,1,0,0,0,0}
#define CONV_OCSPIMAPPINGS {0,1,0,0,1,0}

#define C_BINARY	0
#define C_HEXASCII	1
#define C_PPIXMAP	2
#define C_PPROPULSN	3
#define C_PSPIN		4
#define C_CSPIN		5

#define nullblock
#define ExtrBitField(n,l,h) (((n)>>(l))&((1<<(h))-1))
#define countof(thing) (sizeof(thing)/sizeof(thing[0]))

#ifndef M_PI
#define M_E			2.7182818284590452354
#define M_LOG2E		1.4426950408889634074
#define M_LOG10E	0.43429448190325182765
#define M_LN2		0.69314718055994530942
#define M_LN10		2.30258509299404568402
#define M_PI		3.14159265358979323846
#define M_TWOPI		(M_PI * 2.0)
#define M_PI_2		1.57079632679489661923
#define M_PI_4		0.78539816339744830962
#define M_3PI_4		2.3561944901923448370E0
#define M_SQRTPI	1.77245385090551602792981
#define M_1_PI		0.31830988618379067154
#define M_2_PI		0.63661977236758134308
#define M_2_SQRTPI	1.12837916709551257390
#define M_SQRT2		1.41421356237309504880
#define M_SQRT1_2	0.70710678118654752440
#define M_LN2LO		1.9082149292705877000E-10
#define M_LN2HI		6.9214718036912381649E-1
#define M_SQRT3		1.73205080756887719000
#define M_IVLN10	0.43429448190325182765
#define M_LOG2_E	0.693147180559945309417
#define M_INVLN2	1.4426950408889633870E0
#endif // def M_PI

#define M_2DEGREES	(180.0/M_PI)
#define M_2RADIANS	(M_PI/180.0)

/////////////////////////////////////////////////////////////////////////////
// CParseADDDlg dialog

//$Constructor $CParseADDDlg
CParseADDDlg::CParseADDDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CParseADDDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CParseADDDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon=AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bForce=false;
	m_bInHelpOnItem=false;
	m_bUnixFmt=false;
	m_hCursor=m_hIcon=m_hOrigCursor=0;
	m_nErrors=m_nWarnings=0;
	m_pCOutForm=m_pCInpForm=0;
	m_pEInpFile=m_pEOutFile=m_pEStatusDump=0;
	m_pInpFile=m_pOutFile=0;
	m_pPOutProgress=0;
	blockCountPos=0;
	m_bWrHeader=true;
	m_fCantValue=0.0;
	m_nCVTimer=0;
}

//$DoDataExchange
void CParseADDDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CParseADDDlg)
	//}}AFX_DATA_MAP
}

//$MessageMap
BEGIN_MESSAGE_MAP(CParseADDDlg, CDialog)
	//{{AFX_MSG_MAP(CParseADDDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_INPFILE, OnBtnInpFile)
	ON_BN_CLICKED(IDC_BTN_OUTFILE, OnBtnOutFile)
	ON_BN_CLICKED(IDC_BTN_INTOOUT, OnBtnInToOut)
	ON_BN_CLICKED(IDC_BTN_OUTTOIN, OnBtnOutToIn)
	ON_BN_CLICKED(IDC_BTN_SWAPIO, OnBtnSwapIO)
	ON_BN_CLICKED(IDC_BTN_CONVERT, OnBtnConvert)
	ON_CBN_EDITCHANGE(IDC_CBO_OUTFORM, OnEditChangeCboOutForm)
	ON_CBN_SELCHANGE(IDC_CBO_OUTFORM, OnSelchangeCboOutForm)
	ON_BN_CLICKED(IDC_CHK_FORCE, OnChkForce)
	ON_BN_CLICKED(IDC_BTN_SAVELOG, OnBtnSaveLog)
	ON_BN_CLICKED(IDC_BTN_HELP, OnBtnHelp)
	ON_COMMAND(ID_HELP_CONTENTS, OnHelpContents)
	ON_COMMAND(ID_HELP_ONITEM, OnHelpOnitem)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_WM_LBUTTONUP()
	ON_WM_SETCURSOR()
	ON_BN_CLICKED(IDC_BTN_CLEARLOG, OnBtnClearLog)
	ON_COMMAND(ID_HELP_FORMATS, OnHelpFormats)
	ON_BN_CLICKED(IDC_BTN_VIEWINPUT, OnBtnViewInput)
	ON_BN_CLICKED(IDC_BTN_VIEWOUTPUT, OnBtnViewOutput)
	ON_COMMAND(ID_HELP_MESSAGES, OnHelpMessages)
	ON_BN_CLICKED(IDC_BTN_MINIMIZE, OnBtnMinimize)
	ON_CBN_EDITCHANGE(IDC_CBO_INPFORM, OnEditChangeCboInpForm)
	ON_CBN_SELCHANGE(IDC_CBO_INPFORM, OnSelChangeCboInpForm)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

const TCHAR *const CParseADDDlg::m_ioFormatStrs[]=
{
	_T("Binary memory dump"),
	_T("Hexadecimal ASCII dump"),
	_T("Parsed pixel map data"),
	_T("Parsed propulsion data"),
	_T("Parsed spinning attitude data"),
	_T("FDS spinning attitude data"),
	0
};

const int CParseADDDlg::m_inIndices[]=
	{0,1,4,-1};

const int CParseADDDlg::m_outIndices[]=
	{1,2,3,4,5,-1};

/////////////////////////////////////////////////////////////////////////////
// CParseADDDlg message handlers
/////////////////////////////////////////////////////////////////////////////

//$OnInitDialog $OnDlgInit
BOOL CParseADDDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu--IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	m_hCursor=AfxGetApp()->LoadStandardCursor(IDC_HELP);
	m_hOrigCursor=AfxGetApp()->LoadStandardCursor(IDC_ARROW);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.
	SetIcon(m_hIcon, TRUE);SetIcon(m_hIcon, FALSE);

	//Save all the frequently accessed dialog items
	nullblock
	{
		int i;
		CWnd **members[]=
		{	(CWnd **)&m_pCOutForm,(CWnd **)&m_pCInpForm,
			(CWnd **)&m_pEInpFile,(CWnd **)&m_pEOutFile,
			(CWnd **)&m_pEStatusDump,(CWnd **)&m_pPOutProgress};
		UINT targets[]=
		{	IDC_CBO_OUTFORM,IDC_CBO_INPFORM,IDC_EDT_INPFILE,IDC_EDT_OUTFILE,
			IDC_EDT_STATUSDUMP,IDC_PRG_OUTPRGSS};

		for(i=0;i<countof(members);i++)
			*(members[i])=GetDlgItem(targets[i]);
	}

	//Load icons n' such
	nullblock
	{
		CWinApp *thisApp=AfxGetApp();
		int i;
		UINT targets[]=
		{	IDC_BTN_INTOOUT,IDC_BTN_OUTTOIN,IDC_BTN_SWAPIO,IDC_BTN_SAVELOG,
			IDC_BTN_CLEARLOG,IDC_BTN_MINIMIZE,IDOK,IDC_BTN_HELP,
			IDC_BTN_VIEWINPUT,IDC_BTN_VIEWOUTPUT};
		UINT icons[]=
		{	IDI_DNICON,IDI_UPICON,IDI_UPDNICON,IDI_SAVEICON,
			IDI_CLEARICON,IDI_MINICON,IDI_CLOSEICON,IDI_HELPICON,
			IDI_IEDITICON,IDI_OEDITICON};

		for(i=0;i<countof(targets);i++)
			((CButton *)GetDlgItem(targets[i]))->SetIcon(thisApp->LoadIcon(icons[i]));
	}

	//Set up the combo boxes
	int i;
	for(i=0;m_inIndices[i]>=0;i++)
		m_pCInpForm->AddString(m_ioFormatStrs[m_inIndices[i]]);
	for(i=0;m_outIndices[i]>=0;i++)
		m_pCOutForm->AddString(m_ioFormatStrs[m_outIndices[i]]);

	m_pCOutForm->SetCurSel(0);
	m_pCInpForm->SetCurSel(0);
	((CComboBox *)GetDlgItem(IDC_CBO_CANTVALUE))->SetCurSel(0);
    ((CComboBox *)GetDlgItem(IDC_CBO_PITCHBIAS))->SetCurSel(0);

	m_pEInpFile->SetWindowText(_T(""));
	m_pEOutFile->SetWindowText(_T(""));
	m_pEStatusDump->SetWindowText(_T(""));
	SetDlgItemText(IDC_EDT_CANTVALUE,_T(""));

	GetDlgItem(IDC_STC_CANTVALUE)->EnableWindow(FALSE);
	GetDlgItem(IDC_EDT_CANTVALUE)->EnableWindow(FALSE);
	GetDlgItem(IDC_CBO_CANTVALUE)->EnableWindow(FALSE);
    GetDlgItem(IDC_STC_PITCHBIAS)->EnableWindow(FALSE);
    GetDlgItem(IDC_EDT_PITCHBIAS)->EnableWindow(FALSE);
    GetDlgItem(IDC_CBO_PITCHBIAS)->EnableWindow(FALSE);

	m_font.CreatePointFont(80,_T("Courier New"));
	m_pEStatusDump->SetFont(&m_font);
	
	m_pPOutProgress->SetRange(0,100);
	m_pPOutProgress->SetPos(0);
	
	((CEdit *)GetDlgItem(IDC_EDT_SCID))
		->SetLimitText(16);

	m_pCInpForm->SetFocus();
	return FALSE;
}

//$OnSysCommand
void CParseADDDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
		::HtmlHelp(m_hWnd,HTMLHelpFile "::/about.htm",HH_DISPLAY_TOPIC,NULL);
	else
		CDialog::OnSysCommand(nID, lParam);
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.
//$OnPaint
void CParseADDDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
		CDialog::OnPaint();
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.  Good ol' Win3.1.
//$OnQueryDragIcon
HCURSOR CParseADDDlg::OnQueryDragIcon()
{
	return (HCURSOR)m_hIcon;
}


void CParseADDDlg::OnCancel() 
{
	return;
}

// The user wants to browse for an input file
//$OnBtnInpFile
void CParseADDDlg::OnBtnInpFile() 
{
	LPCTSTR filters,appendthis;

	if(GetInpConvIndex()==C_BINARY)
	{
		filters=_T("Dump files (*.dmp, *.bin)|*.dmp; *.bin|"
			       "All files (*.*)|*.*||");
		appendthis=NULL;
	}
	else
	{
		filters=_T("Text files (*.txt)|*.txt|"
			       "All files (*.*)|*.*||");
		appendthis=_T("txt");
	}

	CFileDialog dlg(TRUE,appendthis,NULL,OFN_FILEMUSTEXIST,filters,this);
	CString filename;

	if(dlg.DoModal()==IDOK)
	{
		filename=dlg.GetPathName();
		m_pEInpFile->SetWindowText(filename);
	}
}

// The user wants to browse for an output file
//$OnBtnOutFile
void CParseADDDlg::OnBtnOutFile() 
{
	DWORD flags=OFN_HIDEREADONLY;
	LPCTSTR filters,appendthis;
	//If they're appending output, no need to worry about overwriting stuff
	if(((CButton *)GetDlgItem(IDC_CHK_APPOUT))->GetCheck()!=BST_CHECKED)
		flags|=OFN_OVERWRITEPROMPT;

	if(GetOutConvIndex()==C_BINARY)
	{
		filters=_T("Dump files (*.dmp, *.bin)|*.dmp; *.bin|"
			       "All files (*.*)|*.*||");
		appendthis=NULL;
	}
	else
	{
		filters=_T("Text files (*.txt)|*.txt|All files (*.*)|*.*||");
		appendthis=_T("txt");
	}

	CFileDialog dlg(FALSE,appendthis,NULL,flags,filters,this);
	CString filename;

	if(dlg.DoModal()==IDOK)
	{
		filename=dlg.GetPathName();
		m_pEOutFile->SetWindowText(filename);
	}	
}

// The user wants to save the log edit box
//$OnBtnSaveLog
void CParseADDDlg::OnBtnSaveLog() 
{
	CFileDialog dlg(FALSE,_T("log"),NULL,OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,
		_T("Log files (*.log)|*.log|All files (*.*)|*.*||"),this);
	CString filename;

	if(dlg.DoModal()==IDOK)
	{
		FILE *output;
		CString str;
		CTime now;

		now=CTime::GetCurrentTime();
		filename=dlg.GetFileName();
		errno_t err;
		if((err =_tfopen_s(&output, (LPCTSTR)filename,"wb"))!=0)
		{
			MessageBox(_T("Error opening output file."),_T("Error"),MB_OK);
			return;
		}
		fprintf(output,"# DumpTool conversion log file\r\n"
					   "# Creation date:\t%02.2d-%02.2d-%04.4d\r\n",
				       now.GetDay(),now.GetMonth(),now.GetYear());
		fprintf(output,"# Creation time:\t%02.2d:%02.2d:%02.2d\r\n\r\n",
				now.GetHour(),now.GetMinute(),now.GetSecond());
		m_pEStatusDump->GetWindowText(str);
		__tfprintf(output,(LPCTSTR)str);
		fprintf(output,"\r\n# End of log");
		fclose(output);
	}
}

// Clear the log's contents
//$OnBtnClearLog
void CParseADDDlg::OnBtnClearLog() 
{
	m_pEStatusDump->SetWindowText(_T(""));
	Progress(0,100);
}

// Move the input filename to the output
//$OnBtnInToOut
void CParseADDDlg::OnBtnInToOut() 
{
	CString a;

	m_pEInpFile->GetWindowText(a);
	m_pEOutFile->SetWindowText(a);
	m_pEInpFile->SetWindowText(_T(""));
}

// Output to input
//$OnBtnOutToIn
void CParseADDDlg::OnBtnOutToIn() 
{
	CString a;
	m_pEOutFile->GetWindowText(a);
	m_pEInpFile->SetWindowText(a);
	m_pEOutFile->SetWindowText(_T(""));
}

// Swap input and output files
//$OnBtnSwapIO
void CParseADDDlg::OnBtnSwapIO() 
{
	CString a,b;
	
	m_pEInpFile->GetWindowText(a);
	m_pEOutFile->GetWindowText(b);
	m_pEInpFile->SetWindowText(b);
	m_pEOutFile->SetWindowText(a);
}

// Do the conversion
//$OnBtnConvert
void CParseADDDlg::OnBtnConvert() 
{
	CString inpfilename,outfilename;
	_TCHAR mode[3]="wb";
	int i,oselitem,iselitem;
	bool success=true;

	static CWnd *const pDisablers[]={
		m_pEInpFile,m_pEOutFile,m_pCOutForm,m_pCInpForm,
		GetDlgItem(IDC_BTN_INPFILE),GetDlgItem(IDC_BTN_OUTFILE),
		GetDlgItem(IDC_BTN_INTOOUT),GetDlgItem(IDC_BTN_OUTTOIN),
		GetDlgItem(IDC_BTN_SWAPIO),GetDlgItem(IDC_CHK_FORCE),
		GetDlgItem(IDC_BTN_VIEWINPUT),GetDlgItem(IDC_BTN_VIEWOUTPUT),
		GetDlgItem(IDC_CHK_APPOUT),GetDlgItem(IDC_BTN_CONVERT),
		GetDlgItem(IDC_BTN_HELP),GetDlgItem(IDOK),
		GetDlgItem(IDC_BTN_CLEARLOG),GetDlgItem(IDC_BTN_SAVELOG),
		GetDlgItem(IDC_BTN_MINIMIZE)};
	static BOOL oldStates[countof(pDisablers)];
	
	//Get the conversion indices:
	oselitem=GetOutConvIndex();
	iselitem=GetInpConvIndex();
	if(!SupportsConv(iselitem,oselitem))
	{
		MessageBox(_T("Invalid conversion selected."),_T("Error"),MB_OK);
		return;
	}

	m_nErrors=m_nWarnings=0;
	m_bWrHeader=true;

	//Get the filenames:
	m_pEInpFile->GetWindowText(inpfilename);
	m_pEOutFile->GetWindowText(outfilename);

	//Open the input and output files:
	if(inpfilename.IsEmpty())
	{
		MessageBox(_T("Please select an input file."),_T("Error"),MB_OK);
		return;
	}

	if(outfilename.IsEmpty())
	{
		MessageBox(_T("Please select an output file."),_T("Error"),MB_OK);
		return;
	}

	//Append?
	if(((CButton *)GetDlgItem(IDC_CHK_APPOUT))->GetCheck()==BST_CHECKED
	   && oselitem!=C_BINARY && oselitem!=C_CSPIN)
		mode[0]='a';

	errno_t err;
	if((err = _tfopen_s(&m_pInpFile, (LPCTSTR)inpfilename,_T("rb")))!=0)
	{
		MessageBox(_T("Error opening input file."),_T("Error"),MB_OK);
		return;
	}
	if(( err = _tfopen_s(&m_pOutFile, (LPCTSTR)outfilename,mode))!=0)
	{
		MessageBox(_T("Error opening output file."),_T("Error"),MB_OK);
		fclose(m_pInpFile);
		return;
	}

	//Busy cursor and disable the disable-able controls
	BeginWaitCursor();
	for(i=0;i<countof(pDisablers);i++)
	{
		oldStates[i]=pDisablers[i]->IsWindowEnabled();
		pDisablers[i]->EnableWindow(FALSE);
	}

	//More than 100 lines? Clear the status dump:
	if(m_pEStatusDump->GetLineCount()>100)
		m_pEStatusDump->SetWindowText(_T(""));
	m_bUnixFmt=false;
	
	DumpV(_T("-----Start-----\r\n: Input file \""));DumpV((LPCTSTR)inpfilename);
	
	DumpV(_T("\"\r\n: Output file \""));DumpV((LPCTSTR)outfilename);DumpV(_T("\"\r\n"));
	
	if(m_bForce && oselitem!=C_CSPIN)
		DumpV(_T(": Force mode ON\r\n"));
	
	if(mode[0]!=_T('w'))
		DumpV(_T(": Appending output\r\n"));

	//Check spacecraft ID if we aren't writing binary
	if(oselitem!=C_BINARY)
	{
		CString scid;
		GetDlgItemText(IDC_EDT_SCID,scid);
		//If we're writing hex ASCII, this is terribly optional.
		if(scid!=CString(_T("")))
		{
			DumpV(_T(": Spacecraft ID \""));
			DumpV((LPCTSTR)scid);
			DumpV(_T("\"\r\n"));
		}
		else
		{
			DumpV(_T("* Warning: No spacecraft ID given\r\n"));
			if(oselitem!=C_HEXASCII
			&& MessageBox(_T("No spacecraft ID entered.  Continue?"),_T("Warning"),MB_YESNO)==IDNO)
			{
				DumpV(_T("-----Aborted-----\r\n"));
				for(i=0;i<countof(pDisablers);i++)
					pDisablers[i]->EnableWindow(oldStates[i]);
				return;
			}
		}
	}

	if(oselitem==C_CSPIN)
	{
		CString cv,pb;
		OnTimer(0xDEADBEEF);
		GetDlgItemText(IDC_EDT_CANTVALUE,cv);
        GetDlgItemText(IDC_EDT_PITCHBIAS,pb);
		if(cv == _T("") || _stscanf_s((LPCTSTR)cv, _T("%lf"), &m_fCantValue)!=1)
		{
			success=false;
			MessageBox(_T("Please enter a cant value."),_T("Error"),MB_OK);
			DumpV(_T("! Error: Cant value omitted.\r\n"));
		}
		else
		{
			//If they've selected "Degrees" instead of "Radians", convert
			if(!((CComboBox *)GetDlgItem(IDC_CBO_CANTVALUE))->GetCurSel())
			{
				m_fCantValue*=M_2RADIANS;
			}
			GetDlgItem(IDC_EDT_CANTVALUE)->EnableWindow(FALSE);
			GetDlgItem(IDC_CBO_CANTVALUE)->EnableWindow(FALSE);
			DumpV(_T(": Cant value: %lf degrees/%lf radians\r\n"),M_2DEGREES*m_fCantValue,
				  m_fCantValue);
		}

        if(cv==_T("") || _stscanf_s((LPCTSTR)pb,_T("%lf"),&m_fPitchBias)!=1) //raj
        {
            success=false;
            MessageBox(_T("Please enter a pitch bias."),_T("Error"),MB_OK);
            DumpV(_T("! Error: Pitch bias omitted.\r\n"));
        }
        else
        {
            if(!((CComboBox *)GetDlgItem(IDC_CBO_PITCHBIAS))->GetCurSel())
			{
                //m_fCantValue*=M_2RADIANS;
				m_fPitchBias*=M_2RADIANS; // raj
			}
            GetDlgItem(IDC_EDT_PITCHBIAS)->EnableWindow(FALSE);
            GetDlgItem(IDC_CBO_PITCHBIAS)->EnableWindow(FALSE);
            DumpV(_T(": Pitch bias: %lf degrees/%lf radians\r\n"),M_2DEGREES*m_fPitchBias,
                m_fPitchBias);
        }
	}

	if(success)
	{
		DumpV(_T("Beginning "));Progress(0,100);
		switch(iselitem)
		{
		case C_BINARY:
			DumpV(_T("binary to "));
			switch(oselitem)
			{
			case C_HEXASCII:
				DumpV(_T("ASCII hex conversion...\r\n"));
	
				//Check for MEMORY MAP-format files
				nullblock
				{
					//They have a header: adst address data
					char buff[81];
					DWORD p=ftell(m_pInpFile);
			
					if(lfgetsc(buff,81,m_pInpFile))
					{
						char s1[5],s2[8],s3[5];
						if(sscanf_s(buff,"%4s %7s %4s",s1,s2,s3)==3 &&
						!strcmp(s1,"adst") && !strcmp(s2,"address") && !strcmp(s3,"data"))
							DumpV(_T("> Input file in MEMORY MAP format, conversion ON\r\n"))
							&& ++m_bUnixFmt;
					}
					fseek(m_pInpFile,p,SEEK_SET);
				}

				DoBin2Hex(m_pInpFile,m_pOutFile,inpfilename,outfilename);
				break;
			default:success=false;
			}
			break;

		case C_HEXASCII:
			DumpV(_T("ASCII hex to "));
			switch(oselitem)
			{
			case C_PPIXMAP:
				DumpV(_T("pixel map data conversion...\r\n"));
				DoHex2PrPixmap(m_pInpFile,m_pOutFile,inpfilename,outfilename);
				break;
			case C_PPROPULSN:
				DumpV(_T("propulsion data conversion...\r\n"));
				DoHex2PrProp(m_pInpFile,m_pOutFile,inpfilename,outfilename);
				break;
			case C_PSPIN:
				DumpV(_T("parsed spinning attitude data conversion...\r\n"));
				DoHex2PrSpinR(m_pInpFile,m_pOutFile,inpfilename,outfilename);
				break;
			case C_CSPIN:
				DumpV(_T("FDS spinning attitude data conversion...\r\n"));
				DoHex2PrSpinC(m_pInpFile,m_pOutFile,inpfilename,outfilename);
				GetDlgItem(IDC_EDT_CANTVALUE)->EnableWindow();
				GetDlgItem(IDC_CBO_CANTVALUE)->EnableWindow();
                GetDlgItem(IDC_EDT_PITCHBIAS)->EnableWindow();
                GetDlgItem(IDC_CBO_PITCHBIAS)->EnableWindow();
				break;
			default:success=false;
			}
			break;

		case C_PSPIN:
			DumpV(_T("parsed spinning attitude data to "));
			switch(oselitem)
			{
			case C_CSPIN:
				DumpV(_T("FDS spinning attitude data conversion...\r\n"));
				DoSpinR2PrSpinC(m_pInpFile,m_pOutFile,inpfilename,outfilename);
				GetDlgItem(IDC_EDT_CANTVALUE)->EnableWindow();
				GetDlgItem(IDC_CBO_CANTVALUE)->EnableWindow();
                GetDlgItem(IDC_EDT_PITCHBIAS)->EnableWindow();
                GetDlgItem(IDC_CBO_PITCHBIAS)->EnableWindow();
				break;
			default:success=false;
			}
			break;

		default:success=false;
		}
	}
	fclose(m_pInpFile);fclose(m_pOutFile);
	
	DumpV(_T(": %d error%s, %d warning%s\r\n-----%s-----\r\n"),
		m_nErrors,(m_nErrors!=1)?"s":"",
		m_nWarnings,(m_nWarnings!=1)?"s":"",
		success?"Done":"Abort");
	Progress(100,100);

	EndWaitCursor();

	for(i=0;i<countof(pDisablers);i++)
		pDisablers[i]->EnableWindow(oldStates[i]);
}

//$OnBtnViewInput
void CParseADDDlg::OnBtnViewInput() 
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	CString runthis(_T("Notepad "));
	LPTSTR pRunthis;
	CString infilename;

	m_pEInpFile->GetWindowText(infilename);
		
	runthis+=infilename;
	::ZeroMemory(&si,sizeof(STARTUPINFO));
	si.cb=sizeof(STARTUPINFO);

	pRunthis=runthis.GetBuffer(8+infilename.GetLength());
	if(::CreateProcess(NULL,pRunthis,
		               NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi))
	{
		::CloseHandle(pi.hThread);
		::CloseHandle(pi.hProcess);
	}
	else
		MessageBox(_T("Couldn't view output file!"),_T("Error"),MB_OK);
	runthis.ReleaseBuffer();
}

//$OnBtnViewOutput
void CParseADDDlg::OnBtnViewOutput() 
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	CString runthis(_T("Notepad "));
	LPTSTR pRunthis;
	CString outfilename;m_pEOutFile->GetWindowText(outfilename);
		
	runthis+=outfilename;
	::ZeroMemory(&si,sizeof(STARTUPINFO));
	si.cb=sizeof(STARTUPINFO);

	pRunthis=runthis.GetBuffer(8+outfilename.GetLength());
	if(::CreateProcess(NULL,pRunthis,
			           NULL,NULL,FALSE,NORMAL_PRIORITY_CLASS,NULL,NULL,&si,&pi))
	{
		::CloseHandle(pi.hThread);
		::CloseHandle(pi.hProcess);
	}
	else
		MessageBox(_T("Couldn't view output file!"),_T("Error"),MB_OK);
	runthis.ReleaseBuffer();
}

// Change the output format, link it to the input format
//$OnEditChangeCboOutForm
void CParseADDDlg::OnEditChangeCboOutForm() 
{
	int i,o;
	BOOL c=FALSE;
	o=GetOutConvIndex();
	i=GetInpConvIndex();

	if(o==C_CSPIN)
	{
		c=TRUE;
		//Set a timer to handle the user's input to the text box
		m_nCVTimer=SetTimer(1,CVUpdatePd,NULL);
	}
	else
	{
		if(m_nCVTimer)
		{
			KillTimer(m_nCVTimer);
			m_nCVTimer=0;
		}
	}

	GetDlgItem(IDC_STC_CANTVALUE)->EnableWindow(c);
	GetDlgItem(IDC_EDT_CANTVALUE)->EnableWindow(c);
	GetDlgItem(IDC_CBO_CANTVALUE)->EnableWindow(c);
    GetDlgItem(IDC_STC_PITCHBIAS)->EnableWindow(c);
    GetDlgItem(IDC_EDT_PITCHBIAS)->EnableWindow(c);
    GetDlgItem(IDC_CBO_PITCHBIAS)->EnableWindow(c);
	GetDlgItem(IDC_CHK_FORCE)->EnableWindow(!c);
	if(o==C_BINARY || o==C_CSPIN)
		GetDlgItem(IDC_CHK_APPOUT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_CHK_APPOUT)->EnableWindow();

	if(SupportsConv(i,o)) return;

	for(i=C_BINARY;i<=C_CSPIN;i++)
		if(SupportsConv(i,o))
		{
			SetInpConvIndex(i);
			break;
		}

	if(i>C_CSPIN)
		SetInpConvIndex(i=C_BINARY);
}

// Change selection of the output format
//$OnSelchangeCboOutForm $OnSelChangeCboOutForm
void CParseADDDlg::OnSelchangeCboOutForm() 
{
	OnEditChangeCboOutForm();	
}

void CParseADDDlg::OnEditChangeCboInpForm() 
{
	int i,o;
	BOOL c=FALSE;
	o=GetOutConvIndex();
	i=GetInpConvIndex();

	if(!SupportsConv(i,o))
	{
		for(o=C_BINARY;o<=C_CSPIN;o++)
			if(SupportsConv(i,o))
			{
				SetOutConvIndex(o);
				break;
			}

		if(o>C_CSPIN)
			SetOutConvIndex(o=C_BINARY);
	}

	if(o==C_CSPIN)
	{
		c=TRUE;
		//Set a timer to handle the user's input to the text box
		m_nCVTimer=SetTimer(1,CVUpdatePd,NULL);
	}
	else
	{
		if(m_nCVTimer)
		{
			KillTimer(m_nCVTimer);
			m_nCVTimer=0;
		}
	}


	GetDlgItem(IDC_STC_CANTVALUE)->EnableWindow(c);
	GetDlgItem(IDC_EDT_CANTVALUE)->EnableWindow(c);
	GetDlgItem(IDC_CBO_CANTVALUE)->EnableWindow(c);
    GetDlgItem(IDC_STC_PITCHBIAS)->EnableWindow(c);
    GetDlgItem(IDC_EDT_PITCHBIAS)->EnableWindow(c);
    GetDlgItem(IDC_CBO_PITCHBIAS)->EnableWindow(c);
	((CButton *)GetDlgItem(IDC_CHK_FORCE))->EnableWindow(!c);
	if(o==C_BINARY || o==C_CSPIN)
		GetDlgItem(IDC_CHK_APPOUT)->EnableWindow(FALSE);
	else
		GetDlgItem(IDC_CHK_APPOUT)->EnableWindow();
}

void CParseADDDlg::OnSelChangeCboInpForm() 
{
	OnEditChangeCboInpForm();	
}

// Check/uncheck the Force box
//$OnChkForce
void CParseADDDlg::OnChkForce() 
{
	m_bForce=(((CButton *)GetDlgItem(IDC_CHK_FORCE))->GetCheck()==BST_CHECKED);	
}

// The user's lost
//$OnBtnHelp
void CParseADDDlg::OnBtnHelp() 
{
	CMenu menu;
	menu.LoadMenu(IDR_CONTEXTMENU);
	CMenu *pCtxtMenu=menu.GetSubMenu(0);
	CRect rect;
	CButton *thisButton;

	thisButton=(CButton *)GetDlgItem(IDC_BTN_HELP);
	
	thisButton->GetClientRect(&rect);
	thisButton->ClientToScreen(&rect);

	pCtxtMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON,
		rect.right,rect.top,this);
}

//$OnHelpContents
void CParseADDDlg::OnHelpContents() 
{
	::HtmlHelp(m_hWnd,HTMLHelpFile "::/index.htm",HH_DISPLAY_TOPIC,NULL);
}

//$OnHelpFormats
void CParseADDDlg::OnHelpFormats() 
{
	::HtmlHelp(m_hWnd,HTMLHelpFile "::/formats/index.htm",HH_DISPLAY_TOPIC,NULL);
}

//$OnHelpAbout
void CParseADDDlg::OnAppAbout() 
{
	::HtmlHelp(m_hWnd,HTMLHelpFile "::/about.htm",HH_DISPLAY_TOPIC,NULL);
}

//$OnHelpMessages
void CParseADDDlg::OnHelpMessages() 
{
	::HtmlHelp(m_hWnd,HTMLHelpFile "::/messages/index.htm",HH_DISPLAY_TOPIC,NULL);	
}

//$OnHelpOnitem $OnHelpOnItem
void CParseADDDlg::OnHelpOnitem() 
{
	SetCapture(); //Capture mouse for hit testing
	m_bInHelpOnItem=true;
	::SetCursor(m_hCursor);
	SetWindowText(_T("DumpTool: Select an item for help..."));
}

//$OnLButtonUp
void CParseADDDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if(m_bInHelpOnItem)
	{
		int i;
		CRect rect;
		CWnd *p;

		ClientToScreen(&point);

		for(i=0;i<countof(dlgIDs);i++)
		{
			p=GetDlgItem(dlgIDs[i]);
			p->GetWindowRect(&rect);
			if(rect.PtInRect(point))
			{
				::HtmlHelp(m_hWnd,HTMLHelpFile,HH_HELP_CONTEXT,mappings[i]);
				break;
			}
		}
		m_bInHelpOnItem=false;
		::ReleaseCapture();
		::SetCursor(m_hOrigCursor);
		SetWindowText(_T("DumpTool"));
	}
	else
		CDialog::OnLButtonUp(nFlags, point);
}

//$OnSetCursor
BOOL CParseADDDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}

//$OnBtnMinimize
void CParseADDDlg::OnBtnMinimize()
{
	WINDOWPLACEMENT wp;

	GetWindowPlacement(&wp);
	wp.showCmd=SW_MINIMIZE;
	SetWindowPlacement(&wp);
}

//$OnTimer
void CParseADDDlg::OnTimer(UINT nIDEvent) 
{
	CString txt,out;
	int start=0,i,l;
    int q=0;
	TCHAR c;
	bool hadPoint=false;
	int oldStart,oldEnd;
	CEdit *pEValue=(CEdit *)GetDlgItem(IDC_EDT_CANTVALUE);

    do {
        pEValue->GetSel(oldStart,oldEnd);
        pEValue->GetWindowText(txt);
        if(txt.IsEmpty()) return;

        if(txt[0]=='-')
	    {
       		start++;
		    out=txt.Left(1);
	    }

        l=txt.GetLength();
        for(i=start;i<l;i++)
        {
    		c=txt[i];
		    if(c>='0' && c<='9')
		    {
        		out+=CString(c);
			    continue;
		    }
		    if(c=='.' && !hadPoint)
		    {
        		hadPoint=true;
			    out+=CString(c);
			    continue;
		    }
	    }

        if(txt!=out)
        {
		    pEValue->SetWindowText(out);
		    pEValue->SetSel(oldEnd,oldEnd);
	    }
	    txt.Empty();
        out.Empty();
		hadPoint=false; //Raj 12-01-01
        ++q;
        pEValue=(CEdit *)GetDlgItem(IDC_EDT_PITCHBIAS);
    } while(q<2);
	
	//If it's called with nIDEvent==0xDEADBEEF, it's slightly pre-conversion
	if(nIDEvent!=0xDEADBEEF)
		CDialog::OnTimer(nIDEvent);
}

/////////////////////////////////////////////////////////////////////////////
//// Utilities
/////////////////////////////////////////////////////////////////////////////
// Set the progress bar's value, v out of q
//$Progress
void CParseADDDlg::Progress(int v,int q)
{
	m_pPOutProgress->SetPos((100*v)/q);
}

// Basically a printf to the status dump box
//$DumpV
int CParseADDDlg::DumpV(LPCTSTR format, ...)
{
	va_list va_args;
	CString p;
	int i=0;

	va_start(va_args,format);
	p.FormatV(format,va_args);
	m_pEStatusDump->ReplaceSel((LPCTSTR)p,FALSE);

	if(format[0]=='\r' && format[1]=='\n') i=2;
	if(format[i]=='!') ++m_nErrors;
	else if(format[i]=='*') ++m_nWarnings;

	return 1;
}

// Dump to the box and the file
//$DumpVF
int CParseADDDlg::DumpVF(LPCTSTR format, ...)
{
	va_list va_args;
	CString p;
	int i=0;

	va_start(va_args,format);
	p.FormatV(format,va_args);
	m_pEStatusDump->ReplaceSel((LPCTSTR)p,FALSE);

	if(format[0]=='\r' && format[1]=='\n') i=2;
	if(format[i]=='!') ++m_nErrors;
	else if(format[i]=='*') ++m_nWarnings;

	va_start(va_args,format);
	vfprintf(m_pOutFile,format,va_args);
	return 1;
}

// Reverse byte ordering in a doubleword
//$Little2BigEndian32
void CParseADDDlg::Little2BigEndian32(DWORD &d) const
{
	unsigned char q,*p=(unsigned char *)&d;

	q=p[0];p[0]=p[3];p[3]=q;
	q=p[1];p[1]=p[2];p[2]=q;
}

// Reverse byte ordering in a word
//$Little2BigEndian16
void CParseADDDlg::Little2BigEndian16(WORD &n) const
{
	unsigned char q,*p=(unsigned char *)&n;

	q=p[0];p[0]=p[1];p[1]=q;
}

// Reverse word ordering in a doubleword
//$SwapHalves32
void CParseADDDlg::SwapHalves32(DWORD &n) const
{
	unsigned short q,*p=(unsigned short *)&n;
	q=p[0];p[0]=p[1];p[1]=q;
}

// Reverse nibble ordering in a byte
//$SwapHalves8
void CParseADDDlg::SwapHalves8(BYTE &n) const
{
	unsigned int q = 0;
	q = n & 0xF;
	n >>= 4;
	n |= q<<4;
}

// Get IEEE floating-point from MIL-1750A floating-point
//$GetFrom1750A
double CParseADDDlg::GetFrom1750A(DWORD d) const
{
	unsigned int exponent,mantissa;
	
	mantissa=d>>8;
	exponent=d&0xFF;

	if(exponent&0x80)
		exponent|=0xFFFFFF00;
	if(mantissa&0x800000)
		mantissa|=0xFF000000;

	exponent -= 23;

	int iExponent = (signed)exponent;
	int iMantissa = (signed)mantissa;
	double dMantissa = (double)iMantissa;

	double dRetVal = ldexp(dMantissa, iExponent);

	return dRetVal;
}

// Get the length of the data field in the string
//$LengthOfData
int CParseADDDlg::LengthOfData(const char *buf)
{
	int i,j;
	//Skip to the first "word"
	for(i=0;buf[i] && buf[i]<=' ';i++);
	if(!buf[i]) return 0;
	
	//Skip to the space after the first "word"
	for(;buf[i]>' ';i++);
	if(!buf[i]) return 0;
	
	//Skip to the second "word"
	for(;buf[i] && buf[i]<=' ';i++);
	if(!buf[i]) return 0;

	//Unix-format has three "words"
	if(m_bUnixFmt)
	{
		//See above
		for(;buf[i]>' ';i++);
		if(!buf[i]) return 0;
		for(;buf[i] && buf[i]<=' ';i++);
		if(!buf[i]) return 0;
	}
	
	//Save this position and skip to the end of the data
	for(j=i;buf[i]>' ';i++);
	return (i-j);
}

// Print the time from seconds
//$PrintTmSec
void CParseADDDlg::PrintTmSec(FILE *out,double time) const
{
	int hr,min,sec,ms;

	hr=(int)(time/3600.0F);time-=hr*3600.0F;
	min=(int)(time/60.0F);time-=min*60.0F;
	sec=(int)time;time-=(double)sec;
	ms=(int)(time*1000.0F);

	fprintf(out,"%02.2d:%02.2d:%02.2d.%03.3d",hr,min,sec,ms);
}

//$lfgetsc
char *CParseADDDlg::lfgetsc(char *buf,int max,FILE *f) const
{
	int l;
	
	if(!fgets(buf,max,f)) return 0;
	l=strlen(buf);

	//Either the line's too long or we're at EOF
	if(!l) return buf;
	if(buf[--l]!='\n')
	{
		//If the line's too long, scan to the end
		while(!feof(f) && fgetc(f)!='\n');
		return buf;
	}
	else
	{
		//Clip the LF from the end of the line
		buf[l]=0;
		if(!l) return buf;

		//Clip the CR from the end of the line
		if(buf[--l]=='\r') buf[l]=0;
		return buf;
	}
}

//Extract parts of a 1750A number
//$xparts
void CParseADDDlg::xparts(DWORD in,int &iout,int &fout,int ndig) const
{
	double i,f;
	int j,k=ndig>>1;

	f=modf((double)GetFrom1750A(in),&i);
	
	//Slightly faster: k==ndig/2
	for(j=1;j<=k;j++) f*=100.0F;

	//ndig&1==ndig%2
	if(ndig&1) f*=10.0F;
	iout=(int)i;
	fout=(int)f;
}

//Extract parts of an IEEE number
void CParseADDDlg::xparts(double &in,int &iout,int &fout,int ndig) const
{
	double i,f;
	int j,k=ndig>>1;

	f=modf((double)in,&i);

	for(j=1;j<=k;j++) f*=100.0F;

	if(ndig&1) f*=10.0F;
	iout=(int)i;
	fout=(int)f;
}

//Get day number with respect to the beginning of the year, not month
//$getYDay
int CParseADDDlg::getYDay(int day,int month,int year)
{
	int ii,i;
	CTime now=CTime::GetCurrentTime();
	static char daysofmonths[]=
		{31,28,31,30,31,30,31,31,30,31,30,31};
		
	//Leap-year fun!
	if(!(year&3))
		if(!(year%100))
			if(!(year%400)) daysofmonths[1]=29;
			else daysofmonths[1]=28;
		else daysofmonths[1]=29;
	else daysofmonths[1]=28;
		
	ii=day;--month;
	//Add the days of each month to the day to get the year-wise day...
	if(month>0 && month<12)	for(i=month-1;i>=0;i--) ii+=daysofmonths[i];

	//If the current month is funky (yoo'hstubbi <1 or >12), alert somebody to it
	if(month>11 || month<0)
	{
		DumpV(_T("* Warning: This program should only be run from Earth.\r\n"));
		ii=0;
	}

	return ii;
}

int CParseADDDlg::GetInpConvIndex()
{
	int cursel,i;

	cursel=m_pCInpForm->GetCurSel();
	if(cursel==LB_ERR) return -1;

	for(i=0;m_inIndices[i]>=0;i++)
		if(i==cursel) break;
	return m_inIndices[i];
}

int CParseADDDlg::GetOutConvIndex()
{
	int cursel,i;

	cursel=m_pCOutForm->GetCurSel();
	if(cursel==LB_ERR) return -1;

	for(i=0;m_outIndices[i]>=0;i++)
		if(i==cursel) break;
	return m_outIndices[i];
}

bool CParseADDDlg::SupportsConv(int from,int to) const
{
	static char supported[][NFORMATS]=
	{
		CONV_IBINMAPPINGS,
		CONV_IHEXMAPPINGS,
		CONV_IPPIXMAPPINGS,
		CONV_IPPROMAPPINGS,
		CONV_IPSPIMAPPINGS,
		CONV_ICSPIMAPPINGS
	};

	if(from>=0 && from<countof(supported) && to>=0 && to<countof(supported[0]))
		return (supported[from][to]!=0);

	return 0;
}

void CParseADDDlg::SetInpConvIndex(int cursel)
{
	int i;
	for(i=0;m_inIndices[i]>=0;i++)
		if(m_inIndices[i]==cursel)
		{
			m_pCInpForm->SetCurSel(i);
			return;
		}
}

void CParseADDDlg::SetOutConvIndex(int cursel)
{
	int i;
	for(i=0;m_outIndices[i]>=0;i++)
		if(m_outIndices[i]==cursel)
		{
			m_pCOutForm->SetCurSel(i);
			return;
		}
}

// Approximately compare with a file's contents (only useful for parsed file
// input):
//   withThis is a string containing the following characters:
//    \_    At least one space or tab
//    \-    Maybe one or more space or tab
//    \=    At least one hyphen
//    \N    At least one newline or CR/LF pair
//    \n    Exactly one newline or CR/LF pair
//    \$    A string of some length, ending at a whitespace
//    \*	Anything until the end of the line
//    \#    A decimal integer of some length, ending at a non-digit or whitespace
//    \X    A hexadecimal integer of some length, ending at a non-digit or whitespace
//    \.    A decimal real number of some length, ending at a non-digit or whitespace
//    \\    A slash
//    \!    Check case for the following character
//    \%    A decimal digit
//    \^    A hexadecimal digit
//    \@    A character
//$FCheckApprox
int CParseADDDlg::FCheckApprox(FILE *in,const char *withThis,DWORD &savedPos)
{
	int c,cc,xflag;
	int curLoc=0;
	char curFunc=0;
	DWORD beginning=ftell(in);
	int rv=0;

	c=fgetc(in);
	for(;;)
	{
		cc = withThis[curLoc++];
		if(!cc) {
			++rv;
			break;
		}
		if(c==EOF) break;

		if(cc=='\\' && withThis[curLoc])
		{
			curFunc=withThis[curLoc++];
			switch(curFunc)
			{
			case '_':		// \_: At least one space or tab
				if(c!=' ' && c!='\t')
					c=EOF;
				else
					while((c=fgetc(in))==' ' || c=='\t');
				break;

			case '-':		// \-: Skip spaces or tabs
				while(c==' ' || c=='\t') c=fgetc(in);
				break;

			case '=':		// \=: At least one hyphen
				if(c!='-')
					c=EOF;
				else
					while((c=fgetc(in))=='-');
				break;

			case 'N':		// \N: At least one newline or CR/LF
				if(c!='\n' && c!='\r')
					c=EOF;
				else
					while((c=fgetc(in))=='\n' || c=='\r');
				break;

			case 'n':		// \n: One newline or CR/LF
				if(c=='\r')
					c=fgetc(in);
				if(c!='\n')
					c=EOF;
				else
					c=fgetc(in);
				break;

			case '$':		// \$: A string of non-space characters
				if(c<=' ')
					c=EOF;
				else
					while((c=fgetc(in))>' ' && c!=EOF);
				break;

			case '*':		// \*: Anything until the end of the line
				while(c!='\n' && c!=EOF) c=fgetc(in);
				break;

			case '#':		// \#: An integer
				if(c<'0' || c>'9')
					c=EOF;
				else
					while((c=fgetc(in))>='0' && c<='9');
				break;

			case 'X':		// \X: A hexadecimal number
				xflag=0;
				if(!isxdigit(c))
					c=EOF;
				else
				{
					c=fgetc(in);
					// Sigh.  The input's in the form "0xNNNNN".
					if(c=='x' || c=='X')
					{
						++xflag;
						c=fgetc(in);
						if(!isxdigit(c))
							c=EOF;
					}
					if(c==EOF) break;

					do c=fgetc(in); while(isxdigit(c));
					// They put an "h" afterwards...
					if(c=='h' || c=='H' && !xflag)
						c=fgetc(in);
				}
				break;

			case '.':		// \.: A decimal real number
				if(c<'0' || c>'9')
					c=EOF;
				else
				{
					do c=fgetc(in); while(isdigit(c));
					if(c=='.')
						do c=fgetc(in); while(isdigit(c));
				}

			case '\\':		// \\: A slash
				if(c!='\\')
					c=EOF;
				else
					c=fgetc(in);
				break;

			case '!':		// \!: The next character's case sensitive
				cc=withThis[curLoc++];
				if(c!=cc)
					c=EOF;
				else
					c=fgetc(in);
				break;
			
			case '%':		// \%: A decimal digit
				if(c<'0' || c>'9')
					c=EOF;
				else
					c=fgetc(in);
				break;

			case '^':		// \^: A hexadecimal digit
				if(!isxdigit(c))
					c=EOF;
				else
					c=fgetc(in);
				break;
			
			case '@':		// \@: A character
				c=fgetc(in);
				break;
			}
			continue;
		}
		
		//Case-insensitive compare...
		c=toupper(c);cc=toupper(cc);
		if(c!=cc)
			break;
		c=fgetc(in);
	}

	savedPos=ftell(in);
	if(savedPos) --savedPos;
	fseek(in,beginning,SEEK_SET);
	return rv;
}

//////////////////////////////////////////////////////////////////////////////
//// Conversion
//////////////////////////////////////////////////////////////////////////////
// Convert memory dump to hex dump
//$DoBin2Hex
void CParseADDDlg::DoBin2Hex(FILE *in, FILE *out,CString &inname,CString &outname)
{
	DWORD startAddx,endAddx,curAddx;
	DWORD data;
	CTime now;

	now=CTime::GetCurrentTime();

	if(!fread(&startAddx,4,1,in))
	{
		DumpVF(_T("! Error: Couldn't read start address.\r\n"));
		return;
	}
	if(!fread(&endAddx,4,1,in))
	{
		DumpVF(_T("! Error: Couldn't read end address.\r\n"));
		return;
	}

	// Reverse the byte ordering in the addresses so they make sense
	Little2BigEndian32(startAddx);
	Little2BigEndian32(endAddx);

	if(startAddx>(1<<20)-1)
	{
		if(m_bForce)
			DumpV(_T("* Warning: Invalid start address.\r\n"));
		else
		{
			DumpVF(_T("! Error: Invalid start address.\r\n"));
			return;
		}
	}
	if(endAddx>(1<<20)-1 || endAddx<startAddx)
	{
		if(m_bForce && endAddx>=startAddx)
			DumpV(_T("* Warning: Invalid end address.\r\n"));
		else
		{
			DumpVF(_T("! Error: Invalid end address.\r\n"));
			return;
		}
	}

	curAddx=startAddx;

	DumpV(_T("> Start address: 0x%08.8X, end address: 0x%08.8X\r\n"
		     "> Writing comment block...\r\n"),startAddx,endAddx);

	fprintf(out,"# Output file:         ");
	__tfprintf(out,(LPCTSTR)outname);fprintf(out,"\r\n"
		        "# Source file:         ");
	__tfprintf(out,(LPCTSTR)inname);fprintf(out,"\r\n"
		        "# Start address:       0x%08.8X\r\n"
		        "# End address:         0x%08.8X\r\n"
				"# Creation date:       %02.2d-%02.2d-%04.4d\r\n",
				startAddx,endAddx,now.GetDay(),now.GetMonth(),now.GetYear());
	fprintf(out,"# Creation time:       %02.2d:%02.2d:%02.2d\r\n"
			    "# Spacecraft ID:       ",
				now.GetHour(),now.GetMinute(),now.GetSecond());
	
	nullblock
	{
		CString scid;
		GetDlgItemText(IDC_EDT_SCID,scid);
		if(scid!=CString(_T("")))
			__tfprintf(out,(LPCTSTR)scid);
		else
			fprintf(out,"(none given)");
	}
	fprintf(out,"\r\n\r\n");

	DumpV(_T("> Writing addresses and data...\r\n"));
	for(curAddx=startAddx;curAddx<=endAddx;curAddx+=2)
	{
		Progress(curAddx-startAddx,endAddx-startAddx);
		//Fill the lower half of data:
		if(!fread(&data,2,1,in))
		{
			DumpVF(_T("! Error: Unexpected EOF in input file.\r\n"));
			return;
		}

		//Scoot it up for half swapping
		data<<=16;
		if(curAddx<endAddx)
		{
			if(!fread(&data,2,1,in))
			{
				DumpVF(_T("! Error: Unexpected EOF in input file.\r\n"));
				return;
			}
			Little2BigEndian32(data);
			SwapHalves32(data);
			fprintf(out,"%08.8X\t%08.8X\r\n",curAddx,data);
		}
		else
		{
			//It's only a word
			Little2BigEndian32(data);
			fprintf(out,"%08.8X\t%04.4X\r\n",curAddx,data);
		}
	}
	
	curAddx=ftell(in);
	fseek(in,0,SEEK_END);
	endAddx=ftell(in);
	curAddx!=endAddx
		&& DumpV(_T("* Warning: %d unused bytes in input file.\r\n"),endAddx-curAddx);
}

// Hex dump -> Parsed pixel map
//$DoHex2PrPixmap
void CParseADDDlg::DoHex2PrPixmap(FILE *in, FILE *out,CString &innm,CString &outnm)
{
	int numBlocks,nnumBlocks,i,j,k;
	DWORD lowAddx,highAddx,lenFile;
	CParseADDDlg::pixmapdata pmdata,buf;
	short *Rows[8][8]=
	{
		&pmdata.Data.R0C7,&pmdata.Data.R0C6,&pmdata.Data.R0C5,&pmdata.Data.R0C4,
			&pmdata.Data.R0C3,&pmdata.Data.R0C2,&pmdata.Data.R0C1,&pmdata.Data.R0C0,
		&pmdata.Data.R1C7,&pmdata.Data.R1C6,&pmdata.Data.R1C5,&pmdata.Data.R1C4,
			&pmdata.Data.R1C3,&pmdata.Data.R1C2,&pmdata.Data.R1C1,&pmdata.Data.R1C0,
		&pmdata.Data.R2C7,&pmdata.Data.R2C6,&pmdata.Data.R2C5,&pmdata.Data.R2C4,
			&pmdata.Data.R2C3,&pmdata.Data.R2C2,&pmdata.Data.R2C1,&pmdata.Data.R2C0,
		&pmdata.Data.R3C7,&pmdata.Data.R3C6,&pmdata.Data.R3C5,&pmdata.Data.R3C4,
			&pmdata.Data.R3C3,&pmdata.Data.R3C2,&pmdata.Data.R3C1,&pmdata.Data.R3C0,
		&pmdata.Data.R4C7,&pmdata.Data.R4C6,&pmdata.Data.R4C5,&pmdata.Data.R4C4,
			&pmdata.Data.R4C3,&pmdata.Data.R4C2,&pmdata.Data.R4C1,&pmdata.Data.R4C0,
		&pmdata.Data.R5C7,&pmdata.Data.R5C6,&pmdata.Data.R5C5,&pmdata.Data.R5C4,
			&pmdata.Data.R5C3,&pmdata.Data.R5C2,&pmdata.Data.R5C1,&pmdata.Data.R5C0,
		&pmdata.Data.R6C7,&pmdata.Data.R6C6,&pmdata.Data.R6C5,&pmdata.Data.R6C4,
			&pmdata.Data.R6C3,&pmdata.Data.R6C2,&pmdata.Data.R6C1,&pmdata.Data.R6C0,
		&pmdata.Data.R7C7,&pmdata.Data.R7C6,&pmdata.Data.R7C5,&pmdata.Data.R7C4,
			&pmdata.Data.R7C3,&pmdata.Data.R7C2,&pmdata.Data.R7C1,&pmdata.Data.R7C0,
	};

	if(!DoFirstHalf(in,out,innm,outnm,96,numBlocks,lowAddx,highAddx,lenFile))
		return;

	nnumBlocks=numBlocks;

	fprintf(out,"\r\n");

	highAddx=lowAddx + 95;
	for(j=0;j<numBlocks;j++,lowAddx+=96,highAddx+=96)
	{
		DumpV(_T("> Reading block %d/%d of %d/%d... "),
			  j-(numBlocks-nnumBlocks)+1,j+1,nnumBlocks,numBlocks);
		Progress(j,numBlocks);

		//Clear the data buffer
		::ZeroMemory(&pmdata,sizeof(pmdata));

		TRACE("numBlock = %i lowAddx = %i highAddx = %i\n",j, lowAddx, highAddx);

		if(!ReadAddressRange(in,pmdata.Bytes,buf.Bytes,lowAddx,highAddx))
		{
			DWORD here=ftell(out);
			fseek(out,blockCountPos,SEEK_SET);
			fprintf(out,"%03.3d",--nnumBlocks);
			fseek(out,here,SEEK_SET);
			continue;
		}

		DumpV(_T("Writing...\r\n"));

		//Print the header (it's all one line)
		fprintf(out,"=========================== "
			        "PIXEL MAP # %03.3d "
					"===========================\r\n"
					"Pixel Map Time:       ",j-(numBlocks-nnumBlocks)+1);
		PrintTmSec(out,GetFrom1750A(pmdata.Data.Current_Time));
		pmdata.Data.RT_Address>>=11;
		fprintf(out,"\r\n"
			        "Star Tracker:         %d\r\n",
			(pmdata.Data.RT_Address<22 || pmdata.Data.RT_Address>24)?0
			                       :(pmdata.Data.RT_Address-21));
		fprintf(out,"Virtual Tracker:      %d\r\n"
		            "Top CCD Row:          %03.3d\r\n"
			        "Right CCD Column:     %03.3d\r\n\r\n",
					pmdata.Data.Virtual_Tracker&((1<<5)-1),
					pmdata.Data.Starting_CCD_Row,
					pmdata.Data.Starting_CCD_Column);

		//Dump the bitmap
		for(i=0;i<8;i++)
			for(k=0;k<8;k++){
				// int nTemp = *Rows[i][k];
				fprintf(out,"%06.6d%s",*Rows[i][k],(k<7)?" ":"\r\n");
			}
	}
	Progress(100,100);
}

// Hex dump -> Propulsion data
//$DoHex2PrProp
void CParseADDDlg::DoHex2PrProp(FILE *in, FILE *out,CString &innm,CString &outnm)
{
	int numBlocks  = 0;
	DWORD lowAddx  = 0;
	DWORD highAddx = 0;
	DWORD lenFile  = 0;
//	int ii,ff;

	if(!DoFirstHalf(in,out,innm,outnm,28,numBlocks,lowAddx,highAddx,lenFile))
		return;

	int nnumBlocks=numBlocks;

	fprintf(out,"\r\n"
		        "Time    F1Pr F2Pr O1Pr O2Pr HePr F1Tm F2Tm O1Tm O2Tm H1Tm H2Tm LIjT LOVT\r\n"
		        "seconds cnt  cnt  cnt  cnt  cnt  cnt  cnt  cnt  cnt  cnt  cnt  cnt  cnt\r\n"
				"------- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----\r\n");

	highAddx = lowAddx + 27;
	for(int j=0;j<numBlocks;j++,lowAddx += 28,highAddx += 28)
	{
		CParseADDDlg::propulsiondata prdata,buf;

		DumpV(_T("> Reading block %d/%d of %d/%d... "),
			  j-(numBlocks-nnumBlocks)+1,j+1,nnumBlocks,numBlocks);
		Progress(j,numBlocks);

		//Clear the data buffer
		::ZeroMemory(&prdata,sizeof(prdata));

		TRACE("Before ReadAddressRange: lowAddx = %i highAddx = %i\n", lowAddx, highAddx);

		if(!ReadAddressRange(in,prdata.Bytes, buf.Bytes, lowAddx, highAddx))
		{
			DWORD here=ftell(out);
			fseek(out,blockCountPos,SEEK_SET);
			fprintf(out,"%03.3d",--nnumBlocks);
			fseek(out,here,SEEK_SET);
			continue;
		}
		

		TRACE("After ReadAddressRange: lowAddx = %i highAddx = %i\n", lowAddx, highAddx);

		DumpV(_T("Writing...\r\n"));

		// xparts(prdata.Data.Current_Time,ii,ff,1);
		double dVal = GetFrom1750A(prdata.Data.Current_Time);

		fprintf(out," %6.1f %4d %4d %4d %4d ",
			dVal,
			prdata.Data.ThisLPT1Pressr,prdata.Data.ThisLPT2Pressr,
			prdata.Data.ThisLPT3Pressr,prdata.Data.ThisLPT4Pressr);
		fprintf(out,"%4d %4d %4d %4d %4d ",
			prdata.Data.ThisLPT7Pressr,prdata.Data.ThisFuelTank1Pressr,
			prdata.Data.ThisFuelTank2Pressr,prdata.Data.ThisOxidizerTank1Temp,
			prdata.Data.ThisOxidizerTank2Temp);
		fprintf(out,"%4d %4d %4d %4d\r\n",
			prdata.Data.ThisHeliumTank1Temp,prdata.Data.ThisHeliumTank2Temp,
			prdata.Data.ThisLAMInjTemp,prdata.Data.ThisLAMOxTemp);
	}
	Progress(100,100);
}

// Hex dump -> Raw spinning attitude data
//$DoHex2PrSpin
void CParseADDDlg::DoHex2PrSpinR(FILE *in, FILE *out,CString &innm,CString &outnm)
{
	int numBlocks,nnumBlocks,j;
	DWORD lowAddx,highAddx,lenFile;
	spindata spdata,buf;
//	int ii,ff;

	if(!DoFirstHalf(in,out,innm,outnm,14, numBlocks,lowAddx,highAddx,lenFile))
		return;

	nnumBlocks=numBlocks;

	fprintf(out,"\r\n"
				"Blk #  Time     SSPER    SREF  SAA12     SAA21     HWEC      HWSEA\r\n"
				"----- --------- -------- ---- --------- --------- --------- ---------\r\n");

	highAddx=lowAddx + 13;
	for(j=0;j<numBlocks;j++,lowAddx += 14, highAddx += 14)
	{
		DumpV(_T("> Reading block %d/%d of %d/%d... "),
			  j-(numBlocks-nnumBlocks)+1,j+1,nnumBlocks,numBlocks);
		Progress(j,numBlocks);

		//Clear the data buffer
		::ZeroMemory(&spdata,sizeof(spdata));

		if(!ReadAddressRange(in,spdata.Bytes,buf.Bytes,lowAddx,highAddx))
		{
			DWORD here=ftell(out);
			fseek(out,blockCountPos,SEEK_SET);
			fprintf(out,"%03.3d",--nnumBlocks);
			fseek(out,here,SEEK_SET);
			continue;
		}

		DumpV(_T("Writing...\r\n"));
//		xparts(spdata.Data.Current_Time,ii,ff,1);
//		fprintf(out,"  %03d-a %6d-b %01.1d-c ",j+1, ii, ff);

		double dValue = GetFrom1750A(spdata.Data.Current_Time);
		fprintf(out,"  %03d %9.3f",j+1, dValue);
		
//		xparts(spdata.Data.Sun_Spin_Period,ii,ff,3);
//		fprintf(out,"%4d-/d. %03.3d-e    %01.1d-f ",ii,ff,
//			spdata.Data.Spin_Reference != 0);

		dValue = GetFrom1750A(spdata.Data.Sun_Spin_Period);
		fprintf(out,"%9.3f %3d ",dValue, spdata.Data.Spin_Reference != 0);
	
//		xparts(spdata.Data.Sun_Aspect_Angle_1_to_2,ii,ff,3);
//		fprintf(out,"%4d-g %03.3d-h ",ii,ff);

		dValue = GetFrom1750A(spdata.Data.Sun_Aspect_Angle_1_to_2);
		fprintf(out,"%9.3f", dValue);

//		xparts(spdata.Data.Sun_Aspect_Angle_2_to_1,ii,ff,3);
//		fprintf(out,"%4d-i %03.3d-j ",ii,ff);

		dValue = GetFrom1750A(spdata.Data.Sun_Aspect_Angle_2_to_1);
		fprintf(out,"%9.3f ", dValue);

//		xparts(spdata.Data.Hardware_Earth_Chord,ii,ff,3);
//		fprintf(out,"%4d-k %03.3d-l ",ii,ff);

		dValue = GetFrom1750A(spdata.Data.Hardware_Earth_Chord);
		fprintf(out,"%9.3f ", dValue);

//		xparts(spdata.Data.Hardware_Sun_to_Earth_Angle,ii,ff,3);
//		fprintf(out,"%4d-m %03.3d-n\r\n",ii,ff);

		dValue = GetFrom1750A(spdata.Data.Hardware_Sun_to_Earth_Angle);
		fprintf(out,"%9.3f\r\n", dValue);
	}
	Progress(100,100);
}

// Hex dump -> Cooked spinning attitude data
//$DoHex2PrSpinC
void CParseADDDlg::DoHex2PrSpinC(FILE *in, FILE *out,CString &innm,CString &outnm)
{
	int numBlocks,nnumBlocks,j;
	DWORD lowAddx,highAddx,lenFile;
	DWORD savedLoc;
	spindata spdata,buf;
	double smax = 0.0;
	double smin = 0.0;
	double curTime;

	m_bWrHeader=false;
	if(!DoFirstHalf(in,out,innm,outnm,14,numBlocks,lowAddx,highAddx,lenFile))
		return;

	nnumBlocks=numBlocks;

	DumpV(_T("> Writing header...\r\n"));
	fwriteSpinCHeader(out,savedLoc);

	highAddx=lowAddx + 13;
	for(j=0;j<numBlocks;j++,lowAddx += 14,highAddx += 14)
	{
		DumpV(_T("> Reading block %d/%d of %d/%d... "),
			  j-(numBlocks-nnumBlocks)+1,j+1,nnumBlocks,numBlocks);
		Progress(j,numBlocks);

		//Clear the data buffer
		::ZeroMemory(&spdata,sizeof(spdata));

		if(!ReadAddressRange(in,spdata.Bytes,buf.Bytes,lowAddx,highAddx))
		{
			--nnumBlocks;
			continue;
		}

		curTime=GetFrom1750A(spdata.Data.Current_Time);
		if(!j)
			smax=smin=curTime;
		else
		{
			if(curTime<smin) smin=curTime;
			if(curTime>smax) smax=curTime;
		}

		DumpV(_T("Writing...\r\n"));
		DoWriteSpinCRecord(out,spdata);
	}
	DumpV(_T("> Updating header end time...\r\n"));
	fwriteSpinCHeader(out,savedLoc,smin,smax);
}

// Raw spinning attitude data -> Cooked spinning attitude data
//$DoHex2PrSpinC
void CParseADDDlg::DoSpinR2PrSpinC(FILE *in, FILE *out,CString &,CString &)
{
	int j=0,valid=0,i;
	DWORD savedLoc;
	pspindata spdata;
	char buffer[81];
	double smin=5.0F,smax=0.0F;

	//This is the string used in FCheckApprox() to make sure the file's OK.
	//Ctrl-Home, Ctrl-F, "$FCheckApprox", Enter for more. :)
	const char *approxCmpStr="\\-OUTPUT\\_FILE\\-:\\*\\N"
	                         "\\-SOURCE\\_DATA\\_FILE\\-:\\*\\N"
							 "\\-SPACECRAFT\\-:\\*\\N"
							 "\\-DATA\\_BLOCKS\\-:\\-\\#\\-\\N"
							 "\\-EXECUTION\\_DATE\\-:\\-\\#-\\#-\\#\\-\\N"
							 "\\-EXECUTION\\_TIME\\-:\\-\\#:\\#:\\#\\-\\N"
							 "\\-BLK\\_#\\_TIME\\_SSPER\\_SREF\\_SAA12\\_SAA21\\_HWEC\\_HWSEA\\-\\N"
							 "\\-\\=\\_\\=\\_\\=\\_\\=\\_\\=\\_\\=\\_\\=\\_\\=\\-\\N";

	m_bWrHeader=false;
	DumpV(_T("> Verifying format and skipping header...\r\n"));
	if(!FCheckApprox(in,approxCmpStr,savedLoc))
	{
		DumpV(_T("! Error: Invalid input file format.\r\n"));
		return;
	}
	fseek(in,savedLoc,SEEK_SET);

	DumpV(_T("> Writing header...\r\n"));
	fwriteSpinCHeader(out,savedLoc);

	while(lfgetsc(buffer,80,in) && ++j)
	{
		// If the line's blank, skip it
		for(i=0;buffer[i]<=' ';i++);
		if(!buffer[i]) {--j;continue;}

		DumpV(_T("> Reading..."));
		if(sscanf_s(buffer,"%d %lf %lf %d %lf %lf %lf %lf",
		      &i,&spdata.Time,&spdata.SSPER,&spdata.SSREF,&spdata.SAA12,&spdata.SAA21,
			  &spdata.HWEC,&spdata.HWSEA)!=8)
		{
			DumpV(_T("\r\n* Warning: Invalid block (#%d) in input file, skipping...\r\n"),j);
			continue;
		}
		++valid;

		if(smin>smax)
			smin=smax=spdata.Time;
		else
		{
			if(spdata.Time<smin) smin=spdata.Time;
			if(spdata.Time>smax) smax=spdata.Time;
		}

		DumpV(_T(" Writing block %d/%d...\r\n"),valid,j);
		DoWriteSpinCRecord(out,spdata);
	}

	DumpV(_T("> Updating header end time...\r\n"));
	fwriteSpinCHeader(out,savedLoc,smin,smax);
}

// Write a single cooked spinning 'tude data record
//$DoWriteSpinCRecord
void CParseADDDlg::DoWriteSpinCRecord(FILE *out,spindata &spdata)
{
	pspindata passthis;

	passthis.HWEC=GetFrom1750A(spdata.Data.Hardware_Earth_Chord);
	passthis.HWSEA=GetFrom1750A(spdata.Data.Hardware_Sun_to_Earth_Angle);
	passthis.SAA12=GetFrom1750A(spdata.Data.Sun_Aspect_Angle_1_to_2);
	passthis.SAA21=GetFrom1750A(spdata.Data.Sun_Aspect_Angle_2_to_1);
	passthis.SSPER=GetFrom1750A(spdata.Data.Sun_Spin_Period);
	passthis.SSREF=spdata.Data.Spin_Reference?1:0;
	passthis.Time=GetFrom1750A(spdata.Data.Current_Time);
	
	DoWriteSpinCRecord(out,passthis);
}

void CParseADDDlg::DoWriteSpinCRecord(FILE *out,pspindata &spdata)
{
	double SPA = 90.0 + (m_fPitchBias*M_2DEGREES); // raj
	double ES1 = 0.0, ES2 = 0.0, SE1 = 0.0, SE2 = 0.0;
	double *pES = 0, *pSE = 0, *pnES = 0, *pnSE = 0;
	int    ii = 0;
	double SpinAng = 0;
    double v = 0;

	//	xparts(spdata.Time,ii,ff,3);
	//	fprintf(out," %6d.%03.3d 1 ",ii,ff);

	fprintf(out," %9.3f 1 ", spdata.Time);

	// Check for divide by zero.
	if (spdata.SSPER != 0){
		SpinAng = M_TWOPI/spdata.SSPER;
	} else {
		SpinAng = 0;
	}
    v=sin(SpinAng*min(spdata.SAA12,spdata.SAA21));


	DumpV(_T("m_fCant is %lf; SpinAng is %lf, spdata.SAA21 is %lf\r\n"),
	  m_fCantValue, SpinAng, spdata.SAA21);//raj

	if(spdata.SSPER>0.0 && spdata.SAA12>=0.0 && spdata.SAA21>=0.0)
	{
        if(spdata.SAA12<=spdata.SAA21 && fabs(v)>0.000001)
		{
            SPA=atan(m_fCantValue/v)*M_2DEGREES;
		}
        else if(fabs(v)>0.000001)
		{
            SPA=180.0-atan(tan(m_fCantValue)/sin(SpinAng*spdata.SAA21))*M_2DEGREES;
		}
	}
	else
	{
		printf("m_fPitchBias is %f\n",m_fPitchBias);
		if (spdata.SSPER == 0)
			SPA = 0; //araj
	}

	if(spdata.SSREF) {pnES=&ES1;pnSE=&SE1;pES=&ES2;pSE=&SE2;}
	else {pES=&ES1;pSE=&SE1;pnES=&ES2;pnSE=&SE2;}
	
	*pnES=*pnSE=0.0;
	*pES=spdata.HWEC;
	*pSE=spdata.HWSEA;

	ii=spdata.SSREF?2:1;
	fprintf(out,"%01.1d %01.1d ",ii,ii);

	nullblock
	{
		double *outputs[]={&SPA,&spdata.SSPER,&ES1,&SE1,&ES2,&SE2};
		int i;

		for(i=0;i<countof(outputs);i++)
		{
			// xparts(*outputs[i],ii,ff,6);
			// fprintf(out,"%3d.%06.6d ",ii,ff);

			// double dValue = GetFrom1750A(*outputs[i]);
			// fprintf(out," %9.3f ", *outputs[i]);
			fprintf(out," %9.06f ", *outputs[i]);
		}
	}

	fprintf(out,"  0.000000   0.000000   0.000000   0.000000 1 1\r\n");
}

// Write the cooked spinning 'tude header
//$fwriteSpinCHeader#1 $WriteSpinCHeader
void CParseADDDlg::fwriteSpinCHeader(FILE *out,DWORD &replloc)
{
	CString scid;

	GetDlgItemText(IDC_EDT_SCID,scid);
	if(scid.IsEmpty())
		scid=CString("??");
	else
	{
		if(scid.GetLength()>2)
			DumpV(_T("* Warning: Spacecraft ID truncated to 2 characters (\"%s\").\r\n"),
			      (LPCTSTR)scid);
		scid=scid.Left(2);
	}

	fprintf(out,"SENSORS %02.2s ", (LPCTSTR)scid);
	replloc=ftell(out);
	fprintf(out,"SSSSSS.SSS SSSSSS.SSS\r\n");
}

// Update the finish date/time field in the cooked spinning 'tude header
//$fwriteSpinCHeader#2 $UpdateSpinCHeader
void CParseADDDlg::fwriteSpinCHeader(FILE *out,DWORD seekhere,double smin,double smax)
{
	int iim,ffm,iiM,ffM;
	DWORD old=ftell(out);

	fseek(out,seekhere,SEEK_SET);

	xparts(smin,iim,ffm,3);xparts(smax,iiM,ffM,3);
	fprintf(out,"%6d.%03.3d %6d.%03.3d",iim,ffm,iiM,ffM);
	
	fseek(out,old,SEEK_SET);
}

// Write the header portion of the parsed file
//$fwriteHeader $WriteHeader
void CParseADDDlg::fwriteHeader(FILE *f, CString &innm, CString &outnm,DWORD &dblkptr,DWORD &end)
{
	CTime now;

	now=CTime::GetCurrentTime();
	fprintf(f,"Output File:         ");
	__tfprintf(f,(LPCTSTR)outnm);fprintf(f,"\r\n"
		      "Source Data File:    ");
	__tfprintf(f,(LPCTSTR)innm);fprintf(f,"\r\n"
              "Spacecraft:          ");
	nullblock
	{
		CString scid;
		GetDlgItemText(IDC_EDT_SCID,scid);
		if(scid!=CString(_T("")))
			__tfprintf(f,(LPCTSTR)scid);
		else
			fprintf(f,"(none given)");
	}

	fprintf(f,"\r\n"
		      "Data Blocks:         ");dblkptr=ftell(f);
	fprintf(f,"   \r\n"
		      "Execution Date:      %02.2d-%02.2d-%04.4d\r\n",
				now.GetDay(),now.GetMonth(),now.GetYear());
	fprintf(f,"Execution Time:      %02.2d:%02.2d:%02.2d\r\n",now.GetHour(),
				now.GetMinute(),now.GetSecond());
	end=ftell(f);
}

// This has to be done for every hex dump file--find the address range
// and write the header
//$DoFirstHalf
int CParseADDDlg::DoFirstHalf(FILE *in,FILE *out,CString &infn,CString &outfn,
							  int blksz,int &num,DWORD &low,DWORD &high,DWORD &filelen)
{
	DWORD topPos,address,data,endPos=0;
	int i,j;
	char buffer[81];

	if(blksz)
	{
		DumpV(_T("> Block size set at %d (0x%X).\r\n"),blksz,blksz);
		if(m_bWrHeader)
		{
			DumpV(_T("> Writing header...\r\n"));
			fwriteHeader(out,infn,outfn,blockCountPos,endPos);
		}
	}

	topPos=ftell(in);
	fseek(in,0,SEEK_END);
	filelen=ftell(in);
	fseek(in,topPos,SEEK_SET);
	
	//Skip Unix header
	if(m_bUnixFmt) lfgetsc(buffer,80,in);

	DumpV(_T("> Finding address range...\r\n"));
	low=high=0xFFFFFFFF;j=0;
	while(lfgetsc(buffer,80,in))
	{
		++j;
		for(i=0;buffer[i] && buffer[i]<=' ';i++);
		if(!buffer[i] || buffer[i]=='#') continue;

		if(m_bUnixFmt)
		{
			unsigned int address2;
			//First chunk gets poked into the top half of the address
			if(sscanf_s(buffer,"%x %x %x",&address2,&address,&data)!=3)
			{
				DumpV(_T("* Warning: Invalid line in input. (%d)\r\n"),j);
				continue;
			}
			address|=(address2<<16);
		}
		else
		{
			if(sscanf_s(buffer,"%x %x",&address,&data)!=2)
			{
				DumpV(_T("* Warning: Invalid line in input. (%d)\r\n"),j);
				continue;
			}
		}

		if(address>((1<<20)-1))
		{
			DumpV(_T("* Warning: Invalid address in input. (%d)\r\n"),j);
			continue;
		}

		i=LengthOfData(buffer);
		if(i!=4 && i!=8)
		{
			DumpV(_T("* Warning: Invalid data length in input. (%d)\r\n"),j);
			continue;
		}

//		((i==4 && address&1) || (i==8 && address&3))
//			&&DumpV(_T("* Warning: Possibly incorrect address alignment in input. (%d)\r\n"),j);

		if(i==4)
			i=1;
		else 
			i=2;

		if(low==0xFFFFFFFF)
		{
			low=address;
			// high=address+i;
			high = address;
		}

		if(address<low) low=address;
		// if(address+i>high) high=address+i;
		if(address>high) high=address;

//		TRACE("In DoFirstHalf: address = %x low = %x high =%x\n", address, low, high);

	}

	if(low==0xFFFFFFFF)
	{
		DumpVF(_T("! Error: No valid lines in input file.\r\n"));
		return 0;
	}

	DumpV(_T(">> Address range: 0x%08.8X to 0x%08.8X\r\n"),low,high);
	if(blksz && high<low+blksz-1)
	{
		if(m_bForce)
			DumpV(_T("* Warning: Address range too small for selected data format.\r\n"));
		else
		{
			DumpVF(_T("! Error: Address range too small for selected data format.\r\n"));
			return 0;
		}
	}

	num=1;
	if(blksz)
	{

		int nRemainder = (high-low)%blksz;

		if((high-low + 2)%blksz)
			DumpV(_T("* Warning: Address range not aligned on correct boundary.\r\n"));
		if(high>=low+blksz-1)
			num=(high-low + 2)/blksz;

		if(m_bWrHeader)
		{
			fseek(out,blockCountPos,SEEK_SET);
			fprintf(out,"%03.3d",num);
		}

		if(!num)
			DumpV(_T(">> No blocks found.\r\n"));
		else
			DumpV(_T(">> %d block%s found.\r\n"),num,(num==1)?"":"s");
	
		if(m_bWrHeader)
			fseek(out,endPos,SEEK_SET);
	}

	fseek(in,topPos,SEEK_SET);
	return 1;
}

// Read a range of addresses into a buffer
//$ReadAddressRange
int CParseADDDlg::ReadAddressRange(FILE *in,void *mem,void *buf,DWORD lowAddx,DWORD highAddx)
{
	char buffer[81];
	int i,j,mapsz,nl=0;
	DWORD addx,data;
	unsigned char c;
	int closeThis=0;
	int errors=0;

	mapsz=highAddx+1-lowAddx;

	::ZeroMemory(buf,mapsz);

	//Go to the top
	fseek(in,0,SEEK_SET);

	//Read the file, finding appropriate addresses
	while(lfgetsc(buffer,80,in))
	{
		//If first non-whitespace character is # or if blank, skip
		for(i=0;buffer[i] && buffer[i]<=' ';i++);
		if(!buffer[i] || buffer[i]=='#') continue;
	
		//Read the hex values (don't warn them again if there are bad lines)
		if(m_bUnixFmt)
		{
			unsigned int addx2;
			if(sscanf_s(buffer,"%x %x %x",&addx2,&addx,&data)!=3)
				continue;
			addx|=(addx2<<16);
		}
		else
		{
			if(sscanf_s(buffer,"%x %x",&addx,&data)!=2)
				continue;
		}
	
		//Read size of data in buffer
		i=LengthOfData(buffer);
		if(i!=4 && i!=8)
			//Same...
			continue;
	
		//1,2,3,4 byte=2,4,6,8 digits, high offset 0,1,2,3
		if(i<2) i=2;
		if(i>8)	i=8;
		i>>=1;
		--i;

		//Is any portion in this block?
		if(addx+i<lowAddx || addx>highAddx)
			continue;

		TRACE("Before for loop: lowAddx = %i highAddx = %i\n", lowAddx, highAddx);

		
		//Set the appropriate bytes in the output/test buffers
		for(j=0,++i;j<i;j++,data>>=8){
		//	if((addx+j>=lowAddx) && (addx+j <= highAddx))

			if((addx >=lowAddx) && (addx <= highAddx))
			{
				int nIndex = ((addx-lowAddx) * 2) + j;
				++((unsigned char *)buf)[nIndex];
				((unsigned char *)mem)[nIndex]=(unsigned char)(data&0xFF);
				TRACE("nIndex = %i\n", nIndex);
				TRACE("addx =%i \n", addx);
				TRACE("j = %i\n", j);
				TRACE("data = %x\n", ((unsigned char *)mem)[nIndex]);
			}
//			TRACE("nIndex = %i\n", ((addx-lowAddx) * 2) + j);
//			TRACE("In for loop: lowAddx = %i highAddx = %i\n", lowAddx, highAddx);
		}
//		TRACE("nIndex = %i\n", ((addx-lowAddx) * 2) + j);
		TRACE("After for loop: lowAddx = %i highAddx = %i\n", lowAddx, highAddx);
	}

	for(i=errors=0;i<mapsz;i++)
	{
		//c=count of memory writes at this location:
		c=((unsigned char *)buf)[i];

		//If exactly one write to this location, finish output and continue
		if(c==1)
		{
			switch(closeThis)
			{
			case 1:
				DumpVF(_T("%08.8X.\r\n"),i-1+lowAddx);
				closeThis=0;
				break;

			case 2:
			case 3:
				DumpV(_T("%08.8X.\r\n"),i-1+lowAddx);
				closeThis=0;
			}
			continue;
		}

		//There's an error, so bump to the next line if necessary:
		if(!nl) DumpV(_T("\r\n")) && ++nl;

		//If there were multiple writes, write a warning
		if(c>1)
		{
			switch(closeThis)
			{
			case 1:
				fprintf(this->m_pOutFile,"%08.8X.\r\n",i-1+lowAddx);
			case 3:
				DumpV(_T("%08.8X.\r\n"),i-1+lowAddx);
			case 0:
				DumpV(_T("* Warning: Memory overwrite from 0x%08.8X to 0x"),i+lowAddx);
				closeThis=2;
				break;
			}
			continue;
		}

		//There were no writes (c==0), write an error
		switch(closeThis)
		{
		case 2:
			DumpV(_T("%08.8X.\r\n"),i-1+lowAddx);
		case 0:
			if(m_bForce)
			{
				DumpV(_T("* Warning: Missing data from 0x%08.8X to 0x"),i+lowAddx);
				closeThis=3;
			}
			else
			{
				DumpVF(_T("! Error: Missing data from 0x%08.8X to 0x"),i+lowAddx);
				closeThis=1;
				errors=1;
			}
			break;
		}
	}


	TRACE("End of ReadAddressRange: lowAddx = %i highAddx = %i\n", lowAddx, highAddx);


	//Finish off any output
	switch(closeThis)
	{
	case 1:
		DumpVF(_T("%08.8X.\r\n"),i-1+lowAddx);break;

	case 2:
	case 3:
		DumpV(_T("%08.8X.\r\n"),i-1+lowAddx);
	}

	//If no errors, return happily
	if(!errors)
	{
		if(nl) DumpV(_T(">> "));
		return 1;
	}

	//Otherwise, skip the block and return 0.
	if(!nl) DumpV(_T("\r\n"));

	DumpV(_T(">> Skipping block.\r\n"));
	return 0;
}