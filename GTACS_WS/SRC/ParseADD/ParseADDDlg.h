// ParseADDDlg.h : header file
//

#if !defined(AFX_PARSEADDDLG_H__43693E96_7AEA_11D5_9071_00104B335B84__INCLUDED_)
#define AFX_PARSEADDDLG_H__43693E96_7AEA_11D5_9071_00104B335B84__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CParseADDDlg dialog

class CParseADDDlg : public CDialog
{
public:
	CParseADDDlg(CWnd *pParent=NULL);

	//{{AFX_DATA(CParseADDDlg)
	enum { IDD = IDD_PARSEADD_DIALOG };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParseADDDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

protected:
#pragma pack(push,happy____,1)
	//The data structures:
	//  Some of the structure member locations may look a little funky; they're
	//  a result of the little-endian <-> big-endian fun with doublewords.
	union pixmapdata
	{
		unsigned char Bytes[192];
		unsigned short Words[96];
		struct pmDataTag
		{
			unsigned int Current_Time;
			unsigned short __0;
			unsigned short RT_Address;
			short Virtual_Tracker;
			unsigned short __1;

			short Starting_CCD_Row;
			short Starting_CCD_Column;
			short R0C1,R0C0,R0C3,R0C2,R0C5,R0C4,R0C7,R0C6;
			short R1C1,R1C0,R1C3,R1C2,R1C5,R1C4,R1C7,R1C6;
			short R2C1,R2C0,R2C3,R2C2,R2C5,R2C4,R2C7,R2C6;

			unsigned int __2;
			unsigned int __3;

			//Here's where the ordering gets fun and why we can't have arrays:
			// The doubleword gets read in, but the shorts are reversed.  Rows
			// 3-7 all start at odd-word addresses, so they get swapped with
			// useless data.
			short R3C0;
			unsigned short __4;
			short R3C2,R3C1,R3C4,R3C3,R3C6,R3C5,R4C0,R3C7;
			short R4C2,R4C1,R4C4,R4C3,R4C6,R4C5,R5C0,R4C7;
			short R5C2,R5C1,R5C4,R5C3,R5C6,R5C5,__5,R5C7;
			
			unsigned int __6,__7,__8;
			short R6C0;
			unsigned short __9;

			short R6C2,R6C1,R6C4,R6C3,R6C6,R6C5,R7C0,R6C7;
			short R7C2,R7C1,R7C4,R7C3,R7C6,R7C5,__A,R7C7;
		} Data;
	};

	union propulsiondata
	{
		unsigned char Bytes[56];
		unsigned short Words[28];
		struct prDataTag
		{
			unsigned int Current_Time;
			short __0,ThisLPT1Pressr;
			short __1,ThisLPT2Pressr;
			short __2,ThisLPT3Pressr;
			short __3,ThisLPT4Pressr;
			short __4,ThisLPT7Pressr;
			short __5,ThisFuelTank1Pressr;
			short __6,ThisFuelTank2Pressr;
			short __7,ThisOxidizerTank1Temp;
			short __8,ThisOxidizerTank2Temp;
			short __9,ThisHeliumTank1Temp;
			short __A,ThisHeliumTank2Temp;
			short __B,ThisLAMInjTemp;
			short __C,ThisLAMOxTemp;
		} Data;
	};

	union spindata
	{
		unsigned char Bytes[28];unsigned short Words[14];
		struct spDataTag
		{
			unsigned int Current_Time;
			unsigned int Sun_Spin_Period;
			short __here;
			unsigned short Spin_Reference;
			unsigned int Sun_Aspect_Angle_1_to_2;
			unsigned int Sun_Aspect_Angle_2_to_1;
			unsigned int Hardware_Earth_Chord;
			unsigned int Hardware_Sun_to_Earth_Angle;
		} Data;
	};

	struct pspindata
	{
		double Time,SSPER,SAA12,SAA21,HWEC,HWSEA;
		int SSREF;
	};

#pragma pack(pop,happy____)

	//Utilities
	//Swap between little/big endian by word and byte:
	void Little2BigEndian16(WORD &) const;
	void Little2BigEndian32(DWORD &) const;

	//Swap halves of a doubleword and a byte:
	void SwapHalves32(DWORD &) const;
	void SwapHalves8(BYTE &) const;

	//Get the length of the data field on a hex dump line:
	int LengthOfData(const char *buf);

	//Print the time of day from the seconds:
	void PrintTmSec(FILE *,double time) const;

	//Get a double from a 1750A floating-point:
	double GetFrom1750A(DWORD) const;

	//Dump to the status output and/or output file:
	int DumpV(LPCTSTR format,...);
	int DumpVF(LPCTSTR format,...);

	//Set the progress display:
	void Progress(int,int);

	//Extract parts of a 1750A/double to n fractional digits
	void xparts(DWORD in,int &iout,int &fout,int ndig) const;
	void xparts(double &in,int &iout,int &fout,int ndig) const;

	//Read an entire line (to the newline, not just to the max. number of chars)
	char *lfgetsc(char *,int,FILE *) const;

	//Get the day of year w/ leap year stuff
	int getYDay(int day,int month,int year);

	//Compare the contents of a file to a ~regular expression.
	int FCheckApprox(FILE *in,const char *withThis,DWORD &savedPos);

	//Get input/output conversion indices:
	int GetInpConvIndex();
	int GetOutConvIndex();

	//Does this program support this conversion?
	bool SupportsConv(int from,int to) const;

	//Set the input/output conversion indices:
	void SetInpConvIndex(int cursel);
	void SetOutConvIndex(int cursel);
	
	//Conversion
	void DoBin2Hex(FILE *,FILE *,CString &,CString &);
	void DoHex2PrPixmap(FILE *,FILE *,CString &,CString &);
	void DoHex2PrProp(FILE *,FILE *,CString &,CString &);
	void DoHex2PrSpinR(FILE *,FILE *,CString &,CString &);
	void DoHex2PrSpinC(FILE *,FILE *,CString &,CString &);
	void DoSpinR2PrSpinC(FILE *in, FILE *out,CString &,CString &);

	//Write the parsed data header
	void fwriteHeader(FILE *,CString &,CString &,DWORD &,DWORD &);

	//Find the address range of a hex dump file, check it for errors, etc.
	int DoFirstHalf(FILE *,FILE *,CString &,CString &,int,int &,DWORD &,DWORD &,DWORD &);

	//Read a block from a hex dump into memory
	int ReadAddressRange(FILE *,void *,void *,DWORD,DWORD);

	//Write cooked spinning attitude data records (with conversion to pspindata)
	void DoWriteSpinCRecord(FILE *,spindata &spdata);
	void DoWriteSpinCRecord(FILE *,pspindata &spdata);

	//Write/rewrite the cooked spinning attitude data header
	void fwriteSpinCHeader(FILE *,DWORD &);
	void fwriteSpinCHeader(FILE *,DWORD,double m,double M);

	//Various pointers to controls for fast access
	CEdit *m_pEStatusDump;						//IDC_EDT_STATUSDUMP
	CEdit *m_pEOutFile,*m_pEInpFile;			//IDC_EDT_OUTFILE,IDC_EDT_INPFILE
	CComboBox *m_pCOutForm,*m_pCInpForm;		//IDC_CBO_OUTFORM,IDC_CBO_INPFORM
	CProgressCtrl *m_pPOutProgress;				//IDC_PRG_OUTPRGSS
	
	//The input and output files
	FILE *m_pInpFile,*m_pOutFile;

	BOOL m_bInHelpOnItem,m_bForce;	//In context help? Force conversion?

	//Font for the status dump
	CFont m_font;
	HICON m_hIcon;
	//Saved cursors for context help
	HCURSOR m_hOrigCursor,m_hCursor;
	
	DWORD blockCountPos;
	unsigned int m_nWarnings,m_nErrors;
	double m_fCantValue;			//Cant value input by user
    double m_fPitchBias;            //Pitch bias input by user
	bool m_bWrHeader;				//Write header?
	int m_nCVTimer;					//The current IDC_EDT_CANTVALUE update timer
	
	//The Unix-created hex dumps have a separate format.
	BOOL m_bUnixFmt;

	//The text strings used by the input/output format box handlers:
	static const TCHAR *const m_ioFormatStrs[];
	static const int m_inIndices[];
	static const int m_outIndices[];

	// Generated message map functions
	//{{AFX_MSG(CParseADDDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBtnInpFile();
	afx_msg void OnBtnOutFile();
	afx_msg void OnBtnInToOut();
	afx_msg void OnBtnOutToIn();
	afx_msg void OnBtnSwapIO();
	afx_msg void OnBtnConvert();
	afx_msg void OnEditChangeCboOutForm();
	afx_msg void OnSelchangeCboOutForm();
	afx_msg void OnChkForce();
	afx_msg void OnBtnSaveLog();
	afx_msg void OnBtnHelp();
	afx_msg void OnHelpContents();
	afx_msg void OnHelpOnitem();
	afx_msg void OnAppAbout();
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnBtnClearLog();
	afx_msg void OnHelpFormats();
	afx_msg void OnBtnViewInput();
	afx_msg void OnBtnViewOutput();
	afx_msg void OnHelpMessages();
	virtual void OnCancel();
	afx_msg void OnBtnMinimize();
	afx_msg void OnEditChangeCboInpForm();
	afx_msg void OnSelChangeCboInpForm();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARSEADDDLG_H__43693E96_7AEA_11D5_9071_00104B335B84__INCLUDED_)
