//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ParseADD.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDC_BTN_VIEWINPUT               101
#define IDD_PARSEADD_DIALOG             102
#define IDC_BTN_VIEWOUTPUT              102
#define IDR_MAINFRAME                   128
#define IDI_UPICON                      130
#define IDI_DNICON                      131
#define IDI_UPDNICON                    132
#define IDR_CONTEXTMENU                 133
#define IDI_SAVEICON                    134
#define IDI_CLEARICON                   135
#define IDI_EDITICON                    137
#define IDI_IEDITICON                   137
#define IDD_GETCANTVALUE                139
#define IDI_RTICON                      140
#define IDI_MINICON                     144
#define IDI_CLOSEICON                   145
#define IDI_HELPICON                    146
#define IDI_OEDITICON                   150
#define IDC_EDT_INPFILE                 1000
#define IDC_BTN_INPFILE                 1001
#define IDC_EDT_OUTFILE                 1002
#define IDC_BTN_OUTFILE                 1003
#define IDC_CBO_OUTFORM                 1006
#define IDC_BTN_CONVERT                 1008
#define IDC_PRG_OUTPRGSS                1009
#define IDC_EDT_STATUSDUMP              1010
#define IDC_CHK_APPOUT                  1011
#define IDC_BTN_OUTTOIN                 1013
#define IDC_BTN_INTOOUT                 1014
#define IDC_BTN_SWAPIO                  1015
#define IDC_CHK_FORCE                   1017
#define IDC_BTN_SAVELOG                 1018
#define IDC_BTN_HELP                    1019
#define IDC_BTN_CLEARLOG                1020
#define IDC_EDT_SCID                    1021
#define IDC_CHK_TESTING                 1023
#define IDC_CBO_INPFORM                 1024
#define IDC_EDT_CANTVALUE               1025
#define IDC_BTN_MINIMIZE                1027
#define IDC_CBO_CANTVALUE               1033
#define IDC_STC_CANTVALUE               1034
#define IDC_SPN_CANTVALUE               1037
#define IDC_EDT_PITCHBIAS               1038
#define IDC_CBO_PITCHBIAS               1039
#define IDC_STC_PITCHBIAS               1040
#define IDC_LIST1                       1041
#define ID_HELP_CONTENTS                32771
#define ID_HELP_ONITEM                  32772
#define ID_HELP_FORMATS                 32773
#define ID_HELP_MESSAGES                32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        151
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1042
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
