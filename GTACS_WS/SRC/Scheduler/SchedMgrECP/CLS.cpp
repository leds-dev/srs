/***************************
//////////////////////////////////////////////////////////////////////
// CLS.cpp : implementation of the CLS class.                       //
// (c) 20001 Frederick J. Shaw                                      //
// Prolog updated											        //
//////////////////////////////////////////////////////////////////////
***************************/
#include "stdafx.h"
#include "CLS.h"
#include "SchedMgrECP.h"
#include "ProgressWnd.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCLS::CCLS(CString strCLSFileName, CNewSchedFile &ProdFile)
//	Description :	Class constructor. 
//					Get the search path(s) from the NT environmental variable.
//                  Extract all search paths and prepend drive letter to path.
//                  Store in a CString array for traversing.
//					
//	Return :		constructor	-
//	Parameters :	
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CCLS::CCLS(CString strCLSFileName, CTextFile *ProdFile): CNewSchedFile(strCLSFileName){
   m_eCLSLoc = INVALID_TYPE;
   m_ProdFile = ProdFile;

   m_CLSTimeTag.Create(1971,1,0,0,0,0);
   m_StartTime.Create(0,0,0,0,0);
   m_StopTime.Create(400,23,59,59,999);
   m_PPData   = NULL;

   // Get the search path(s) from the NT environmental variable.
   // CString strEPOCH_PROCS = _wgetenv(ctstrEPOCHPROCS);

   TCHAR * libvar;
	size_t requiredSize;

	_tgetenv_s( &requiredSize, NULL, 0, ctcEPOCHPROCS);

	libvar = (TCHAR *) malloc(requiredSize * sizeof(TCHAR));
	if (!libvar){
		// printf("Failed to allocate memory!\n");
		// exit(1);
	}

	// Get the value of the LIB environment variable.
   _tgetenv_s( &requiredSize, libvar, requiredSize, ctcEPOCHPROCS );

   CString strEPOCH_PROCS(libvar);
   CString strDrive;
   CString strPath;
   bool bDone = false;
   m_bBeforeStartTime = false;

   // Now extract all search paths and prepend drive letter to path.
   while (!bDone && (!strEPOCH_PROCS.IsEmpty())){
	   int nPos = 0;
		if ( -1 != (nPos = strEPOCH_PROCS.Find(_T(':')))){
			strPath = strEPOCH_PROCS.Left(nPos);
			strEPOCH_PROCS = strEPOCH_PROCS.Right(strEPOCH_PROCS.GetLength() - nPos - 1);
			strEPOCH_PROCS.TrimLeft();
			strEPOCH_PROCS.TrimRight();
			strPath.TrimLeft();
			strPath.TrimRight();
		} else { 
			strPath = strEPOCH_PROCS;
			bDone = true;
		}

		if ( -1 != (nPos = strPath.Find(_T('=')))){
			// Remove the drive letter.
			strDrive = strPath.Left(nPos + 1);
			strDrive.Replace(_T("/"), _T(""));
			strDrive.Replace(_T("="), _T(":"));

			strPath.Delete(0, nPos + 1);
			strPath.TrimLeft();
			strPath.TrimRight();
		} else {
			// output messge error with search path in environmental variable.
			CString strErrorMsg = _T("Error interpreting environmental variable.");
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			bDone = true;
		}
		m_SearchPathArray.Add(strDrive + strPath + _T('/'));
   }

	m_UNCCLSPath.File()         = strCLSFileName; 
	free(libvar);
}
/****************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 05/30/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : Expand		
//	Description :	This routine converts a CLS into STOL procedure.
//                  It scans file and based on the directive found. 
//                  All lines containing CLS directives processed by 
//                  another member function.
//					
//	Return :  BOOL:    TRUE	- successful
//					   FALSE -	errors
//	Parameters :	
//         Void		-	nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*****************/
BOOL CCLS::Expand(){

	BOOL eReturnStatus = TRUE;
	m_NewGblValData.SetNumPrologLines(GetPrologLineCount());
	int nNumPrologLines = GetPrologLineCount();
	int nLineCount      = GetLineCount() + nNumPrologLines;
	CString strFormat = _T("CMD schedule generation in progress. \n")
						_T("Currently on line %d of %d\n");

#ifndef _NO_PROGRESS
	CProgressWnd  wndProgress(AfxGetMainWnd(), _T("Schedule Generation"), TRUE);
	wndProgress.SetRange(0, nLineCount, FALSE);
#endif

	int nCurrentLine = 0;
	int nLineNum     = 0;
	int nTotalNumLines = GetLineCount();
	bool bDone         = false;

	while ( nCurrentLine < nTotalNumLines && !bDone){

		nLineNum = nCurrentLine + nNumPrologLines + 1;
		int nRtnLine = 0;
		CString strLine = GetDirLine(nCurrentLine, nRtnLine);
		CString strLineTrimmed(strLine);
		strLineTrimmed.TrimLeft ();
		
		CString strText;
		strText.Format(strFormat, nLineNum, nLineCount);
		#ifndef _NO_PROGRESS
		  wndProgress.SetText (FALSE, strText);
		  wndProgress.SetPos (nLineNum, FALSE);
		#endif
			
		// Process CLS line.
		if (!strLineTrimmed.IsEmpty() && ctchCOMMENT != strLineTrimmed[0]){
			
			// Extract time tag.
			CString strTimeTag;
			if (strLineTrimmed.GetLength () >= ctnCPTTLength){
				strTimeTag = strLine.Left(ctnCPTTLength);
				strLine.Delete(0, ctnCPTTLength);
				strTimeTag.TrimRight();
				strTimeTag.TrimLeft();
									
				if (!strTimeTag.IsEmpty()) {  // A time tag is present.
					CSchedTimeSpan cST;
					CSchedTime     cBaseTime(m_CLSTimeTag);
					if (!cST.Create(strTimeTag)){
						CString strErrorMsg = cST.GetLastErrorText();
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
						eReturnStatus = FALSE;
						strTimeTag.Empty();
					} else { // Valid time tag.
						cBaseTime = cBaseTime + cST;
					}
					
					if (TRUE == eReturnStatus){
						CString strStopTime(m_StopTime.GetTimeSpanString(FALSE, FALSE));
						CString strStartTime(m_StartTime.GetTimeSpanString(FALSE, FALSE));
						CString strST(cST.GetTimeSpanString(FALSE, FALSE));
						CString strBaseTime(cBaseTime.GetTimeString(FALSE));

						if ( cST > m_StopTime) {
							// Finished processing this CLS, past the stop time.
							bDone = true;
						} else if (cST >= m_StartTime){
							// First check for comment character.
							if (ctchCOMMENT == strLineTrimmed[0]){
								strLine = GetTextAt(nCurrentLine);
								// Extract time tag.
								if (strLine.GetLength () >= ctnCPTTLength){
									strLine.Delete(0, ctnCPTTLength);
									// strLine.Insert(0, _T("    "));
								}

								// Write line to product file.
								m_ProdFile->AppendText(strLine);
								// Write line(s) to product file.
								for (int i = 1; i < nRtnLine; i++){
									CString strLine = GetTextAt(nCurrentLine + i);
									strLine.Delete(0, ctnCPTTLength);
									m_ProdFile->AppendText(strLine);
								}
							} else if (TRUE == ScanLine(strLine, ctstrCLS)){

								// Pass in the CLS line with a continuation character removed.  
								// Line must contain the timetag.
								if (TRUE != Process_CLSLine(strLineTrimmed)){
									// bDone         = false;
									eReturnStatus = FALSE;
								}
								
							} else if (TRUE == ScanLine(strLine, ctstrSTART)){

								// Create STOL timetag and write to product file.
								m_bBeforeStartTime = false;
								CString  strSTOLTimeTag;
								cBaseTime.CreateSTOLTimeTag(strSTOLTimeTag);
								m_ProdFile->AppendText(strSTOLTimeTag);
				
								if (TRUE != ChkProcValStatus(strLine)){
									// bDone         = false;
									eReturnStatus = FALSE;
								} else {

									// Now only get first text line; remove time tag.
									strLine = GetTextAt(nCurrentLine);
									if (strLine.GetLength () >= ctnCPTTLength){
										strLine.Delete(0, ctnCPTTLength);
										// strLine.Insert(0, _T("    "));
									}
									
									// Expand all macros in the line.
									m_PPData->Replace_PP(strLine);
									// Write line to product file.
									m_ProdFile->AppendText(strLine);
									// Next get remaining continuation lines from teh file.
									// Write line(s) to product file.
									for (int i = 1; i < nRtnLine; i++){
										strLine = GetTextAt(nCurrentLine + i);
										m_PPData->Replace_PP(strLine);
										strLine.Delete(0, ctnCPTTLength);
										m_ProdFile->AppendText(strLine);
									}
								}
											
							} else { 
								// Create STOL timetag and write to product file.
								CString  strSTOLTimeTag;
								cBaseTime.CreateSTOLTimeTag(strSTOLTimeTag);
								m_ProdFile->AppendText(strSTOLTimeTag);
								// Now only get first text line; remove time tag.
								strLine = GetTextAt(nCurrentLine);
								if (strLine.GetLength () >= ctnCPTTLength){
									strLine.Delete(0, ctnCPTTLength);
									//strLine.Insert(0, _T("    "));
								}

								// Expand all macros in the line.
								m_PPData->Replace_PP(strLine);
								// Write line to product file.
								m_ProdFile->AppendText(strLine);
								// Next get remaining continuation lines from teh file.
								// Write line(s) to product file.
								for (int i = 1; i < nRtnLine; i++){
									strLine = GetTextAt(nCurrentLine + i);
									m_PPData->Replace_PP(strLine);
									strLine.Delete(0, ctnCPTTLength);
									m_ProdFile->AppendText(strLine);
								}
							}
						}
					}
				} else  {  // Time tag not present.
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 4);
					eReturnStatus = FALSE;
				}
			}
		} else if ((!strLineTrimmed.IsEmpty()) && (2 < strLineTrimmed.GetLength())
			&& (!m_bBeforeStartTime)){
			// Check if comment that needs to be copied to the master schedule
			// Only comments that are single # are copied
			if (ctchCOMMENT == strLineTrimmed[0]){
				strLineTrimmed.MakeUpper();
				if (-1 != strLineTrimmed.Find(ctstrGROUND)){ 
				// Also need to check for ground directive and copy those over.
					m_ProdFile->AppendText(strLine);
				} else if (ctchCOMMENT != strLineTrimmed[1]){
					m_ProdFile->AppendText(strLine);
				}
			}
		} 

		#ifndef _NO_PROGRESS
			wndProgress.PeekAndPump(false);
			if (wndProgress.Cancelled()){
				break;
			}
		#endif
		nCurrentLine += nRtnLine;
	}

	return eReturnStatus;
}
/********************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 05/30/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : Process_CLSLine
//	Description :	
//           This routine processes lines that contain the CLS directive.
//           When a CLS directive is found preliminary data is initialized.
//           This class is then recursively called.
//					
//	Return :  BOOL:    TRUE	- Successful
//					   FALSE - Fail	
//	Parameters :
//              CString strCLSLine the CLS direcitve line 
//
//	
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
********************/
BOOL  CCLS::Process_CLSLine(CString strDirLine){

	// Remove time tag.
	CString strLineTrimmed(strDirLine);
	strLineTrimmed.TrimLeft ();
	CString strCLSName;
	BOOL eReturn_Status = TRUE;
	CSchedTimeSpan cST;
	CSchedTime     cBaseTime(m_CLSTimeTag);
		
	// Extract time tag.
	if (strDirLine.GetLength () >= ctnCPTTLength){

		CString	strTimeTag = strDirLine.Left(ctnCPTTLength);
		strDirLine.Delete(0, ctnCPTTLength);
		strTimeTag.TrimRight();
		strTimeTag.TrimLeft();
								
		if (!strTimeTag.IsEmpty()) {  // A time tag is present.
			if (!cST.Create(strTimeTag)){
				CString strErrorMsg = cST.GetLastErrorText();
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
				eReturn_Status = FALSE;
				strTimeTag.Empty();
			} else { // Valid time tag.
				cBaseTime = cBaseTime + cST;
			}
		}
	}

	// Find the position of the CLS directive.
	int nPos = WhereIsToken(strDirLine, ctstrCLS);

	if (nPos < 0){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 29);
		eReturn_Status = FALSE;
	} else {
		// Remove the CLS directive.
		int nLen = _tcsclen(ctstrCLS);
		strDirLine.Delete(0, nPos + nLen);
		strDirLine.TrimLeft();
		strDirLine.TrimRight();

		// Replace all ',', '(', and ')' with a space.
		// Passed parameter class can then process the line.
		strDirLine.Replace(_T(','), _T(' '));
		strDirLine.Replace(_T('('), _T(' '));
		strDirLine.Replace(_T(')'), _T(' '));

		int nCLSNameIndex = -1;
		strCLSName = strDirLine;

		// Find where the CLS name ends.  
		nCLSNameIndex = strDirLine.Find(_T(' '), 0);	

		// Extract out the name
		if (nCLSNameIndex > 0){
			int nLength = strCLSName.GetLength();
			strCLSName.Delete(nCLSNameIndex, nLength - nCLSNameIndex);
			// Remove the name, then parse the passed parameters.
			strDirLine.Delete(0, nCLSNameIndex);
			strDirLine.TrimLeft();
			strDirLine.TrimRight();
			strCLSName.TrimLeft();
			strCLSName.TrimRight();
		}
	}

	bool bAbsPath = false;
	if ((-1 != strCLSName.Find(_T('\\'), 0)) || (-1 != strCLSName.Find(_T('/'), 0))){
		bAbsPath = true;
	}  

	// Get the CLS extension.
	CString strExt(_T(".cls"));
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nCLS])->GetDocString(strExt, CDocTemplate::filterExt);

	// File paths 
	COXUNC	UNCCLSPath (m_UNCCLSPath);	
//	COXUNC  UNCSearchPath (m_UNCSearchPath);


	UNCCLSPath.Full();
	UNCCLSPath.File () = strCLSName + strExt;

//	UNCSearchPath.Full();
//	UNCSearchPath.File () = strCLSName + strExt;

	CCLS CLS(UNCCLSPath.Full(), m_ProdFile);

	CLS.SetCLSLocation(m_eCLSLoc);
	CLS.SetTimeTag(cBaseTime);

	CLS.SetCLSPath(UNCCLSPath.Full());
//	CLS.SetSearchPath(UNCSearchPath.Full());

	// Process passed parameters.
	CPassedParam PPData;
	if (TRUE != PPData.Process_PP(strDirLine)){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 36, strLineTrimmed);
		eReturn_Status = FALSE;
	} else {

		for (int i = 0; i < PPData.GetParamSize(); i++ ){
			CString strParamName = PPData.GetParamName(i);
			CString strStart(ctstrSTART);
			strStart.TrimRight();

			CString strStop(ctstrSTOP);
			strStop.TrimRight();

			// Remove and save start time.
			if ((strParamName.CompareNoCase(strStart)) == 0){
				CSchedTimeSpan cST;
				if (!cST.Create(PPData.GetParamValue(i))){
					CString strErrorMsg = cST.GetLastErrorText();
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
					eReturn_Status = FALSE;
				} else {
					CLS.SetStartTime(cST);
				}
				PPData.Remove(i);
			// Remove and save stop time.
			} else if ((strParamName.CompareNoCase(strStop)) == 0) {
				CSchedTimeSpan cST;	
				if (!cST.Create(PPData.GetParamValue(i))){
					CString strErrorMsg = cST.GetLastErrorText();
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
					eReturn_Status = FALSE;
				} else {
					CLS.SetStopTime(cST);
				}
				PPData.Remove(i);
			}
		}
	}

	CLS.SetPassedParamData(&PPData);
	BOOL eSuccessful = TRUE;
	CFileException Exception;
	CString strFullPath = UNCCLSPath.Full();
	if (TRUE != CLS.Load(strFullPath, CTextFile::RO, &Exception)){
		if (!bAbsPath){
			eReturn_Status = FALSE;
			eSuccessful = FALSE;
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		} else {
			COXUNC  UNCSearchPath;
			int nMaxNumSearchPaths = m_SearchPathArray.GetSize();
			int nNumPaths = 0;
			bool bDone = false; 
			while (!bDone && (nNumPaths < nMaxNumSearchPaths)){
				UNCSearchPath.Directory() = m_SearchPathArray[nNumPaths];
				UNCSearchPath.File () = strCLSName + strExt;
				UNCSearchPath.Full();

				if (TRUE !=	CLS.Load(UNCSearchPath.Full(), CTextFile::RO, &Exception)){
					eReturn_Status = FALSE;
					eSuccessful = FALSE;
				} else {
					eReturn_Status = TRUE;
					eSuccessful    = TRUE;
					bDone          = true;
				}
				nNumPaths++;
			}

			// Save the last filepath for printing out error messges. 
			strFullPath = UNCSearchPath.Full();
			if (TRUE != eReturn_Status){
				CString strErrorMsg = ExceptionErrorText(&Exception);
				CString strMsg;
				strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
			}
		}
	} 
	
	if (TRUE == eSuccessful){
		eValidLoc eVLoc		   = INVALID_TYPE;
		eValidStatus  eVStatus = INVALID_STATUS;
		if (TRUE != CLS.IsFileValid(eVStatus, eVLoc)){
			eReturn_Status = FALSE;
			CString strErrorMsg = _T("The CLS file has a invalid prolog.");
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			strErrorMsg = _T("Schedule generation is aborting.");
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::INFO);
		} else if (TRUE == CLS.IsOverRideSet()){
			// Override directive in file is set.
			CString strMsg(_T("Validation status was overridden!"));
			CString strTitle;
			strTitle.Format(_T("%s %s"), strMsg, strFullPath);
			GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
			GetAppOutputBar()->OutputToEvents(strTitle, COutputBar::INFO);
		} else if (((PASSED != eVStatus) && (WARNING != eVStatus))){  
			eReturn_Status = FALSE;
			CString strErrorMsg = _T("The CLS file failed validation or is unvalidated.");
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
		} else if (WARNING == eVStatus){
			CString strErrorMsg = _T("The CLS's validation status is warning.");
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::WARNING);
			strErrorMsg = _T("Schedule generation will proceed.");
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::INFO);
		} 

		if (eVLoc != m_eCLSLoc && eVLoc != BOTH){
			eReturn_Status = FALSE;
//			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 25, strCLSName);
			CString strErrorMsg = _T("The CLS's execution location does not match.");
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::WARNING);
			strErrorMsg = _T("Schedule generation will abort.");
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::WARNING);
		} else if (TRUE !=	CLS.Expand()){
				eReturn_Status = FALSE;
		}

		if (TRUE != CLS.Close(&Exception)){
			eReturn_Status = FALSE;
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	}
	return eReturn_Status;
}
/*****************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 05/30/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : ChkProcValStatus(strLine)
//	Description :	
//                This routine verifies the routine exists, is valid, and
//                match the exectuion location
//					
//	Return :  BOOL:    TRUE	- Successful
//					   FALSE - Fail	
//	Parameters :  CString strCLSLine
//
//	
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*******************/
BOOL CCLS::ChkProcValStatus(CString strDirLine){

	BOOL eReturn_Status = TRUE;

	CString strProcName;

	// Find the position of the START directive.
	int nPos = WhereIsToken(strDirLine, ctstrSTART);

	if (nPos < 0){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 29);
		eReturn_Status = FALSE;
	} else {
		// Remove the START directive.
		int nLen = _tcsclen(ctstrSTART);
		strDirLine.Delete(0, nPos + nLen);
		strDirLine.TrimLeft();
		strDirLine.TrimRight();

		// Replace all ',', '(', and ')' with a space.
		// Passed parameter class can then process the line.
		strDirLine.Replace(_T(','), _T(' '));
		strDirLine.Replace(_T('('), _T(' '));
		strDirLine.Replace(_T(')'), _T(' '));

		strProcName = strDirLine;

		// Find where the STOL Proc name ends.  
		int nProcNameIndex = strDirLine.Find(_T(' '), 0);	

		// Extract out the name
		if (nProcNameIndex > 0){
			int nLength = strProcName.GetLength();
			strProcName.Delete(nProcNameIndex, nLength - nProcNameIndex);
			// Remove the name, then parse the passed parameters.
			strDirLine.Delete(0, nProcNameIndex);
			strDirLine.TrimLeft();
			strDirLine.TrimRight();
			strProcName.TrimLeft();
			strProcName.TrimRight();
		}
	}

	/*
	bool bAbsPath = false;
	if ((-1 != strProcName.Find(_T('\\'), 0)) || (-1 != strProcName.Find(_T('/'), 0))){
		bAbsPath = true;
	} 
    */

	CString	strFilepath;
//	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	
//	strFilepath.Format(_T("\\\\%s\\%s\\%s\\%s\\"),
//			GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//			GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
	strFilepath.Format(_T("%s\\%s\\%s\\%s\\%s\\"),
			ctcProcDrive, ctcMountPoint, ctcProcsDir,
			GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],			// Spacecraft
			GetAppRegFolder()->m_strDir[nFolderSTOL]);					// Specific dir												

	// Get the Proc extension.
	CString strExt(_T(".prc"));
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nSTOL])->GetDocString(strExt, CDocTemplate::filterExt);

	// Set the procedure path to current path.
	COXUNC	UNCProcPath (strFilepath);	
	UNCProcPath.Full();
	UNCProcPath.File () = strProcName + strExt;
	UNCProcPath.Full();
	
	CTextFile Proc;

	BOOL eSuccessful = TRUE;
	CFileException Exception;
	CString strFullPath;
	if (TRUE != Proc.Load(UNCProcPath.Full(), CTextFile::RO, &Exception)){
		COXUNC  UNCSearchPath;
		int nMaxNumSearchPaths = m_SearchPathArray.GetSize();
		int nNumPaths = 0;
		bool bDone = false; 
		while (!bDone && (nNumPaths < nMaxNumSearchPaths)){
			// UNCSearchPath.Directory() = m_SearchPathArray[nNumPaths];
			UNCSearchPath.File () = m_SearchPathArray[nNumPaths] + strProcName + strExt;
			UNCSearchPath.Full();

			if (TRUE !=	Proc.Load(UNCSearchPath.Full(), CTextFile::RO, &Exception)){
				eReturn_Status = FALSE;
				eSuccessful = FALSE;
			} else {
				eReturn_Status = TRUE;
				eSuccessful    = TRUE;
				bDone          = true;
			}
			nNumPaths++;
		}
		
		// Save the last filepath for printing out error messges. 
		strFullPath = UNCSearchPath.Full();
		if (TRUE != eReturn_Status){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
			// GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
		}
	} 
	
	if (TRUE == eSuccessful){
		eValidLoc eVLoc		   = INVALID_TYPE;
		eValidStatus  eVStatus = INVALID_STATUS;
		if (TRUE != Proc.IsFileValid(eVStatus, eVLoc)){
			eReturn_Status = FALSE;
			CString strErrorMsg = _T("The STOL procedure has a invalid prolog.");
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			strErrorMsg = _T("Schedule generation is aborting.");
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::INFO);
		}  else if (TRUE == Proc.IsOverRideSet()){
			// Override directive in file is set.
			CString strMsg(_T("Validation status was overridden!"));
			CString strTitle;
			strTitle.Format(_T("%s %s"), strMsg, strFullPath);
			GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
			GetAppOutputBar()->OutputToEvents(strTitle, COutputBar::INFO);
		} else if (((PASSED != eVStatus) && (WARNING != eVStatus))){  
			eReturn_Status = FALSE;
			CString strErrorMsg = _T("The STOL proc failed validation or is unvalidated.");
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
		} else if (WARNING == eVStatus){
			CString strErrorMsg = _T("The STOL proc's validation status is warning.");
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::WARNING);
			strErrorMsg = _T("Schedule generation will proceed.");
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::INFO);
		} 
	
		if (eVLoc != m_eCLSLoc && eVLoc != BOTH){
			eReturn_Status = FALSE;
			CString strErrorMsg = _T("The STOL procedure's execution location is incompatiable.");
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::WARNING);
			strErrorMsg = _T("Schedule generation is aborting.");
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::INFO);
		} 

		if (TRUE != Proc.Close(&Exception)){
			eReturn_Status = FALSE;
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::WARNING);
		}
	}
	return eReturn_Status;
}

