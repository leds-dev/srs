//////////////////////////////////////////////////////////////////////
// CLS.h : interface for the CLS class.               
// (c) 2001 Frederick J. Shaw   
// modifications:
//                                         
//////////////////////////////////////////////////////////////////////

#ifndef __CLS_H__
#define __CLS_H__

#include "NewGblValData.h"
#include "PassedParam.h"
#include "SchedTime.h"
#define _NO_PROGRESS 1

#if _MSC_VER 
#pragma once
#endif // _MSC_VER 

class CCLS : public CNewSchedFile
{ 

// Construction
public:

//	CCLS();
	CCLS(CString strCLSFileName, CTextFile *ProdFile);
	BOOL Expand();
	void SetProdFile(CTextFile *ProdFile){m_ProdFile = ProdFile;};
	CTextFile * GetProdFile(){return m_ProdFile;};

	void SetCLSLocation(enum eValidLoc eCLSLoc){m_eCLSLoc = eCLSLoc;};
	enum eValidLoc GetCLSLocation(){return m_eCLSLoc;};
	void SetTimeTag(CSchedTime CTimeTag){m_CLSTimeTag = CTimeTag;};
	CSchedTime GetTimeTag(void){return m_CLSTimeTag;};
	CSchedTimeSpan GetStartTime(void){return m_StartTime;};
	void SetStartTime(CSchedTimeSpan CTimeTag){m_StartTime = CTimeTag;};
	void SetStopTime(CSchedTimeSpan CTimeTag){m_StopTime = CTimeTag;};
	CSchedTimeSpan GetStopTime(void){return m_StopTime;};
	void SetPassedParamData(CPassedParam * PPData){m_PPData = PPData;};
	CPassedParam * GetPassedParamData(void){return m_PPData;};

// Operations

// Attributes
protected:
	BOOL Process_CLSLine(CString strCLSLine);
	void SetCLSPath(CString strCLSPath){m_UNCCLSPath.Full() = strCLSPath;};
	COXUNC GetCLSPath(void){return m_UNCCLSPath;};
	void SetSearchPath(CString strSearchPath){m_UNCSearchPath.Full() = strSearchPath;};
	COXUNC GetSearchPath(void){return m_UNCSearchPath;};
	void SetSTOLPath(CString strSTOLPath){m_UNCSTOLPath.Full() = strSTOLPath;};
	COXUNC GetSTOLPath(void){return m_UNCSTOLPath;};
	BOOL ChkProcValStatus(CString strLine);

private:
	
	CNewGblValData	m_NewGblValData;
	CSchedTime      m_CLSTimeTag;
	CPassedParam *  m_PPData;
	CTextFile *		m_ProdFile;
	enum eValidLoc  m_eCLSLoc;
	COXUNC			m_UNCSTOLPath;
	COXUNC			m_UNCCLSPath;
	COXUNC			m_UNCSearchPath;
	CSchedTimeSpan  m_StartTime;
	CSchedTimeSpan  m_StopTime;
	CStringArray    m_SearchPathArray;
	bool            m_bBeforeStartTime;

}; 

#endif  // __CLS_H__
