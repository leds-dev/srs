/***************
//////////////////////////////////////////////////////////////////////
// CLSPage.cpp  : implementation of the CLSPage class.              //
// (c) 20001 Frederick J. Shaw                                      //
//  PDL Updated
//////////////////////////////////////////////////////////////////////

// CLSPage.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class will show the user the name of the CLS file that has been
// selected.  It will also prompt the user as to whether the CLS file
// should be validated before being used.
/////////////////////////////////////////////////////////////////////////////
**************/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"
#include "FinishDialog.h"
#include "CLSPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/****************
/////////////////////////////////////////////////////////////////////////////
// This class will show the user the name of the CLS file that has been
// selected.  It will also prompt the user as to whether the CLS file
// should be validated before being used.
/////////////////////////////////////////////////////////////////////////////
****************/
/////////////////////////////////////////////////////////////////////////////
// CCLSPage property page

IMPLEMENT_DYNCREATE(CCLSPage, CResizablePage)

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : CCLSPage  
//  Description :  Constructor 
//
//  Returns :  
//           nothing.
//  Parameters : 
//           void	none.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
CCLSPage::CCLSPage(): CResizablePage(CCLSPage::IDD)
{
	//{{AFX_DATA_INIT(CCLSPage)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : ~CCLSPage  
//  Description :  Destructor 
//
//  Returns :  
//           nothing.
//  Parameters : 
//           void	none.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
CCLSPage::~CCLSPage()
{
}
/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : DoDataExchange  
//  Description :   
//					This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables.
//                  This routine is generated and maintained by the 
//                  code wizard.
//  Returns :  
//           nothing.
//  Parameters : 
//           CDataExchange* pDX
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
void CCLSPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCLSPage)
	DDX_Control(pDX, IDC_CLS_DATE_DOY_EDIT, m_cDOYEdit);
	DDX_Control(pDX, IDC_CLS_DATE_YEAR_EDIT, m_cYearEdit);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCLSPage, CResizablePage)
	//{{AFX_MSG_MAP(CCLSPage)
	ON_BN_CLICKED(IDC_CLS_VAL_CHECK, OnCLSValCheck)
	ON_BN_CLICKED(IDC_CLS_VAL_GROUND_RADIO, OnCLSValRadio)
	ON_BN_CLICKED(IDC_CLS_VAL_ONBOARD_RADIO, OnCLSValRadio)
	ON_BN_CLICKED(IDC_CLS_VAL_BOTH_RADIO, OnCLSValRadio)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CLS_DATE_DOY_SPIN, OnDeltaPosDateDOYSpin)
	ON_EN_UPDATE(IDC_CLS_DATE_DOY_EDIT, OnUpdateDateDOYEdit)
	ON_NOTIFY(UDN_DELTAPOS, IDC_CLS_DATE_YEAR_SPIN, OnDeltaPosDateYearSpin)
	ON_EN_UPDATE(IDC_CLS_DATE_YEAR_EDIT, OnUpdateDateYearEdit)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCLSPage message handlers
//////////////////////////////////////////////
/******************
/////////////////////////////////////////////////////////////////////////////
// CCLSPage property page
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : Init
//  Description :
//                This routine initializes the member variables based on 
//                the passed parameters.
//
//  Returns :  
//                void   nothing
//  Parameters :
//                BOOL bFirstWizPage: Is this the first display page?
//                BOOL bLastWizPage: Is this the last display page?
//                BOOL bVal:         Is the file valid?
//                eValidLoc eValLoc. What is the execution location?
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
***************/
void CCLSPage::Init(BOOL bFirstWizPage, BOOL bLastWizPage, BOOL bVal, eValidLoc eValLoc)
{
	m_bFirstWizPage	= bFirstWizPage;
	m_bLastWizPage	= bLastWizPage;
	m_bVal			= bVal;
	m_eValLoc		= eValLoc;
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnInitDialog()
//  Description :  This routine create the CLS page dialog window.  
//
//  Returns : 	BOOL
//              TRUE - Successful
//              FALSE - Fail
//  Parameters : 
//                void nothing.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
BOOL CCLSPage::OnInitDialog() 
{
	m_bInInit = TRUE;
	CResizablePage::OnInitDialog();
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	// Add all of the necessary anchors
	AddAnchor(IDC_CLS_GROUP, TOP_LEFT, BOTTOM_RIGHT);
	
	AddAnchor(IDC_CLS_DATE_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_CLS_DATE_DOY_STATIC, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_DATE_DOY_EDIT, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_DATE_DOY_SPIN, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_DATE_YEAR_STATIC, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_DATE_YEAR_EDIT, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_DATE_YEAR_SPIN, TOP_LEFT, TOP_LEFT);

	
	AddAnchor(IDC_CLS_VAL_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_CLS_VAL_CHECK, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_VAL_GROUND_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_VAL_ONBOARD_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_VAL_BOTH_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_LOC_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_CLS_LOC_OLD_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_LOC_OLD_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_CLS_LOC_GEN_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_LOC_GEN_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_CLS_LOC_UPD_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_CLS_LOC_UPD_STATIC, TOP_LEFT, TOP_RIGHT);
	
	// Create the image lists for the icons
	CImageList imageList;
	imageList.Create(IDR_CLS_MENUIMAGES, 16, 0, RGB (192,192,192));
	((CStatic*)GetDlgItem(IDC_CLS_VAL_ICON))->SetIcon(imageList.ExtractIcon(0));
	
	// Ensure that the icons and check boxes are on top of the static group control
	GetDlgItem(IDC_CLS_VAL_ICON)->BringWindowToTop();
	GetDlgItem(IDC_CLS_VAL_CHECK)->BringWindowToTop();
	
	CString strTemp;
	m_cDOYEdit.SetMask(_T("###"));
	m_cDOYEdit.SetPromptSymbol(_T('0'));
	((CSpinButtonCtrl*)GetDlgItem(IDC_CLS_DATE_DOY_SPIN))->SetRange(0, 366);
	((CSpinButtonCtrl*)GetDlgItem(IDC_CLS_DATE_DOY_SPIN))->SetPos(pSheet->GetDay());
	strTemp.Format(_T("%03d"), pSheet->GetDay());
	m_cDOYEdit.SetWindowText(strTemp);
	m_nDOY = pSheet->GetDay();

	m_nYear = 	GetAppRegMakeSched()->m_nCLSYear;
	m_cYearEdit.SetMask(_T("####"));
	m_cYearEdit.SetPromptSymbol(_T('0'));
	((CSpinButtonCtrl*)GetDlgItem(IDC_CLS_DATE_YEAR_SPIN))->SetRange(1980, 2020);
	((CSpinButtonCtrl*)GetDlgItem(IDC_CLS_DATE_YEAR_SPIN))->SetPos(m_nYear);
//	strTemp.Format(_T("%04d"), 2001);
	strTemp.Format(_T("%04d"), m_nYear);
	m_cYearEdit.SetWindowText(strTemp);
//	m_nYear = 2001;

	BOOL bValEnable = TRUE;
	
	// Enable the appropriate windows
	switch (pSheet->GetFunc())
	{
		case nPackage:	bValEnable = TRUE;	break;
		case nValCLS:
		{
			bValEnable = TRUE;
			GetDlgItem(IDC_CLS_DATE_GROUP)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_DATE_DOY_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_DATE_DOY_EDIT)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_DATE_DOY_SPIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_DATE_YEAR_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_DATE_YEAR_EDIT)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_DATE_YEAR_SPIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_LOC_GEN_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_LOC_GEN_LABEL)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_LOC_UPD_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_LOC_UPD_LABEL)->EnableWindow(FALSE);
			break;
		}

		case nGenSTOL:
		{
			bValEnable = FALSE;
			GetDlgItem(IDC_CLS_DATE_GROUP)->EnableWindow(TRUE);
			GetDlgItem(IDC_CLS_DATE_DOY_STATIC)->EnableWindow(TRUE);
			GetDlgItem(IDC_CLS_DATE_DOY_EDIT)->EnableWindow(TRUE);
			GetDlgItem(IDC_CLS_DATE_DOY_SPIN)->EnableWindow(TRUE);
			GetDlgItem(IDC_CLS_DATE_YEAR_STATIC)->EnableWindow(TRUE);
			GetDlgItem(IDC_CLS_DATE_YEAR_EDIT)->EnableWindow(TRUE);
			GetDlgItem(IDC_CLS_DATE_YEAR_SPIN)->EnableWindow(TRUE);
			GetDlgItem(IDC_CLS_LOC_GEN_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_LOC_GEN_LABEL)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_LOC_UPD_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_CLS_LOC_UPD_LABEL)->EnableWindow(FALSE);
			break;
		}

		default:		bValEnable = FALSE;	ASSERT(FALSE);	break;
	}	

	GetDlgItem(IDC_CLS_VAL_GROUP)->EnableWindow(bValEnable);
	GetDlgItem(IDC_CLS_VAL_CHECK)->EnableWindow(bValEnable);
	GetDlgItem(IDC_CLS_VAL_GROUND_RADIO)->EnableWindow(bValEnable);
	GetDlgItem(IDC_CLS_VAL_ONBOARD_RADIO)->EnableWindow(bValEnable);
	GetDlgItem(IDC_CLS_VAL_BOTH_RADIO)->EnableWindow(bValEnable);

	m_bInInit = FALSE;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnSetActive  
//  Description :   
//				  This routine disables/enables, as appropriate, the 
//                different window controls.
//                  
//  Returns : BOOL 
//					TRUE  success
//                  FALSE fail
//  Parameters : 
//           none
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
BOOL CCLSPage::OnSetActive()
{
	CString strTemp;
	
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	GetDlgItem(IDC_CLS_LOC_OLD_STATIC)->SetWindowText(pSheet->GetFull());
	((CButton*)GetDlgItem(IDC_CLS_VAL_CHECK))->SetCheck(m_bVal);
	((CButton*)GetDlgItem(IDC_CLS_VAL_GROUND_RADIO))->SetCheck(m_eValLoc == GROUND ? TRUE : FALSE);
	((CButton*)GetDlgItem(IDC_CLS_VAL_ONBOARD_RADIO))->SetCheck(m_eValLoc == ONBOARD ? TRUE : FALSE);
	((CButton*)GetDlgItem(IDC_CLS_VAL_BOTH_RADIO))->SetCheck(m_eValLoc == BOTH ? TRUE : FALSE);
	OnCLSValCheck();

	// Setup the proper Wizard Buttons
	DWORD dWizFlags= PSWIZB_FINISH;
	if ((m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_FINISH;
	else if ((m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_NEXT;
	else if ((!m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_FINISH;
	else if ((!m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_NEXT;
	
	pSheet->SetWizardButtons(dWizFlags);
	ResizeFilenames();	
	
	return CResizablePage::OnSetActive();
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnWizardNext  
//  Description :   This routine is called when the user selects next   
//					on the dialog window.   The specific window controls
//                  are updated as needed.
//                   
//                  
//  Returns :  
//           LRESULT  
//  Parameters : 
//           void nothing
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
LRESULT CCLSPage::OnWizardNext()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	if (pSheet->GetFunc() == nPackage)
	{
		COXUNC cGenCLSFilename(pSheet->MakeCLSGenFilepath());
		COXUNC cUpdCLSFilename(pSheet->MakeFilepath(nFolderSTOL, nSTOL, FALSE));

		CString strExistingFiles;

		BOOL bGen = cGenCLSFilename.Exists();
		BOOL bUpd = cUpdCLSFilename.Exists();
		
		if (bGen)
			strExistingFiles = cGenCLSFilename.Full() + _T('\n');

		if (bUpd)
			strExistingFiles = cUpdCLSFilename.Full() + _T('\n');
		
		if (!strExistingFiles.IsEmpty())
		{
			CString strError;
			strError.Format(IDS_FILE_EXISTS, strExistingFiles);
			if (AfxMessageBox(strError, MB_OKCANCEL) == IDCANCEL)
				return -1;
		}
	}

	return CResizablePage::OnWizardNext();
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnKill Active  
//  Description :   
//					This routine calls the CResizablePage::OnKillActive routine.
//  Returns :  BOOL 
//					TRUE  success
//					FALSE fail
//  Parameters : 
//           void none
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
BOOL CCLSPage::OnKillActive()
{
	return CResizablePage::OnKillActive();
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :	OnCLSValCheck   
//  Description :   
//					This routine enables/disables as needed the dialog 
//                  controls for the CLS validation dialog box.
//  Returns :  
//           void  nothing.
//  Parameters : 
//           none
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
void CCLSPage::OnCLSValCheck() 
{
	// Enable the appropriate windows
	BOOL bEnable = ((CButton*)GetDlgItem(IDC_CLS_VAL_CHECK))->GetCheck();
	GetDlgItem(IDC_CLS_VAL_GROUND_RADIO)->EnableWindow(bEnable);
	GetDlgItem(IDC_CLS_VAL_ONBOARD_RADIO)->EnableWindow(bEnable);
	GetDlgItem(IDC_CLS_VAL_BOTH_RADIO)->EnableWindow(bEnable);
	m_bVal = bEnable;
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnCLSValRadio  
//  Description :   
//					This routine enables/disables the validation radio
//                  buttons on the CLS dialog box.	
//                  
//  Returns :  
//           void nothing.
//  Parameters : 
//           void nothing
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
void CCLSPage::OnCLSValRadio() 
{
	if (((CButton*)GetDlgItem(IDC_CLS_VAL_GROUND_RADIO))->GetCheck())
	{
		m_eValLoc = GROUND;
	}
	else if (((CButton*)GetDlgItem(IDC_CLS_VAL_ONBOARD_RADIO))->GetCheck())
	{
		m_eValLoc = ONBOARD;
	}
	else
	{
		m_eValLoc = BOTH;
	}
}


/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnSize  
//  Description :   
//					This routine resizes the dialog window based on the 
//                   the coordinates passed into the routine.	
//                  
//  Returns :  
//           void nothing.
//  Parameters : 
//           UINT nType; type of window resizing requested.
//           int cx; new width of window.
//           int cy; new height of window.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
void CCLSPage::OnSize(UINT nType, int cx, int cy) 
{
	CResizablePage::OnSize(nType, cx, cy);
	ResizeFilenames();
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnWizardFinish()  
//  Description :   
//				This routine is called when the okay button is checked
//              on the finish dialog box 	
//                  
//  Returns :  
//           void nothing.
//  Parameters :  BOOL
//           TRUE success.
//           FALSE fail.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
BOOL CCLSPage::OnWizardFinish() 
{
	((CGenUpdSheet*)GetParent())->ShowWindow(SW_HIDE);

	CFinishDialog dlg;
	if (dlg.DoModal() == IDOK)
		return TRUE;
	else
		return FALSE;
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : ResizeFilenames()  
//  Description :   
//				This routine is called to resize the pathname is displayed.
//              So that it fits within the dialog control. 	
//                  
//  Returns :  
//           void nothing.
//  Parameters :  
//           void none.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
void CCLSPage::ResizeFilenames()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	PathSetDlgItemPath(this->m_hWnd, IDC_CLS_LOC_OLD_STATIC, pSheet->GetFull());
	if (pSheet->GetFunc() == nPackage)
	{
		PathSetDlgItemPath(this->m_hWnd, IDC_CLS_LOC_GEN_STATIC,
			pSheet->MakeCLSGenFilepath());
		PathSetDlgItemPath(this->m_hWnd, IDC_CLS_LOC_UPD_STATIC, 
			pSheet->MakeFilepath(nFolderSTOL, nSTOL, FALSE));
	}
	else
	{
		PathSetDlgItemPath(this->m_hWnd, IDC_CLS_LOC_GEN_STATIC, _T(""));
		PathSetDlgItemPath(this->m_hWnd, IDC_CLS_LOC_UPD_STATIC, _T(""));
	}
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnDeltaPosDateDOYSpin  
//  Description :   
//				This routine is called when the day control is changed on
//              the CLSPage dialog window.  The number is valdiated.  	
//                  
//  Returns :  
//           void nothing.
//
//  Parameters :  
//					NMHDR* pNMHDR. Pointer to a notification message struct
//					LRESULT* pResult.  
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
void CCLSPage::OnDeltaPosDateDOYSpin(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
//	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	// Get the current Day position
	CString strTemp;
	GetDlgItem(IDC_CLS_DATE_DOY_EDIT)->GetWindowText(strTemp);
	pNMUpDown->iPos = _ttoi(strTemp);
	
	// If the new Day position would be within range, then allow it
	int nNewPos = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nNewPos >= 0) && (nNewPos <= 366))
	{
		CString strTemp;
		strTemp.Format(_T("%03d"), nNewPos);
		m_cDOYEdit.SetWindowText(strTemp);
		m_nDOY = nNewPos;
		*pResult = 0;
	}
	else
		*pResult = 1; //Don't allow the change
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnDeltaPosDateYearSpin  
//  Description :   
//				This routine is called when year control is changed on the 
//              on the CLS dialog 	
//                  
//  Returns :  
//           void nothing.
//
//  Parameters :  
//				  NMHDR* pNMHDR. Pointer to a notification message struct
//				  LRESULT* pResult.  
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
void CCLSPage::OnDeltaPosDateYearSpin(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
//	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	// Get the current Year position
	CString strTemp;
	GetDlgItem(IDC_CLS_DATE_YEAR_EDIT)->GetWindowText(strTemp);
	pNMUpDown->iPos = _ttoi(strTemp);
	
	// If the new Year position would be within range, then allow it
	int nNewPos = pNMUpDown->iPos + pNMUpDown->iDelta;
	if ((nNewPos >= 1980) && (nNewPos <= 2020))
	{
		CString strTemp;
		strTemp.Format(_T("%04d"), nNewPos);
		m_cYearEdit.SetWindowText(strTemp);
		m_nYear = nNewPos;
		*pResult = 0;
	}
	else
		*pResult = 1; //Don't allow the change
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnUpdateDateDOYEdit() 
//  Description :   
//				This routine is called when the day of year control
//              is chenged or edited. 	
//                  
//  Returns :  
//           void nothing.
//  Parameters :  
//				void	nothing
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
void CCLSPage::OnUpdateDateDOYEdit()
{
	if (m_bInInit)
		return;

	CString strTemp;
	GetDlgItem(IDC_CLS_DATE_DOY_EDIT)->GetWindowText(strTemp);
	((CSpinButtonCtrl*)GetDlgItem(IDC_CLS_DATE_DOY_SPIN))->SetPos(_ttoi(strTemp));
	m_nDOY = _ttoi(strTemp);
}

/****************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : OnUpdateDateYearEdit()  
//  Description :   
//				This routine is called when the year control button is 
//              changed. 	
//                  
//  Returns :  
//           void	nothing.
//  Parameters :  
//           void	nothing
//           
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
****************/
void CCLSPage::OnUpdateDateYearEdit()
{
	if (m_bInInit)
		return;

	CString strTemp;
	GetDlgItem(IDC_CLS_DATE_YEAR_EDIT)->GetWindowText(strTemp);
	((CSpinButtonCtrl*)GetDlgItem(IDC_CLS_DATE_YEAR_SPIN))->SetPos(_ttoi(strTemp));
	m_nYear = _ttoi(strTemp);
	GetAppRegMakeSched()->m_nCLSYear = m_nYear;
}
