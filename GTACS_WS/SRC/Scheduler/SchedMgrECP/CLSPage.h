#if !defined(AFX_CLSPAGE_H__B04A0C34_2F48_11D5_9A74_0003472193C8__INCLUDED_)
#define AFX_CLSPAGE_H__B04A0C34_2F48_11D5_9A74_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CLSPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCLSPage dialog

class CCLSPage : public CResizablePage
{
	DECLARE_DYNCREATE(CCLSPage)

// Construction
public:
	CCLSPage();
	~CCLSPage();
	void Init(BOOL bFirstWizPage, BOOL bLastWizPage, BOOL bVal, eValidLoc eValLoc);
	BOOL GetVal(){return m_bVal;};
	eValidLoc GetValLoc(){return m_eValLoc;};
	int GetDOY(){return m_nDOY;};
	int GetYear(){return m_nYear;};
		
// Dialog Data
	//{{AFX_DATA(CCLSPage)
	enum { IDD = IDD_CLS_DIALOG };
	COXMaskedEdit	m_cDOYEdit;
	COXMaskedEdit	m_cYearEdit;
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CCLSPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CCLSPage)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnSetActive();
	afx_msg BOOL OnKillActive();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnWizardFinish();
	virtual LRESULT OnWizardNext();
	afx_msg void OnCLSValRadio();
	afx_msg void OnCLSValCheck();
	afx_msg void OnDeltaPosDateDOYSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdateDateDOYEdit();
	afx_msg void OnDeltaPosDateYearSpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdateDateYearEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void ResizeFilenames();
	BOOL		m_bVal;			//Validate the CLS - if TRUE, then check the next members veriables
	eValidLoc	m_eValLoc;		//Location of validation
	BOOL		m_bFirstWizPage;//TRUE if this the first page of a Wizard
	BOOL		m_bLastWizPage;	//TRUE if this the last page of a Wizard
	int			m_nDOY;			//Day of year used for generation
	int			m_nYear;		//Year used for validation
	BOOL		m_bInInit;		// TRUE when in InitInstance.  This is needed so that the OnUpdate
								// routines get ignored for the day and version controls.  These
								// controls are COXMaskedEdit classes and the OnUpdate functions get
								// called whenever the mask and prompt symbol is set (in addition to
								// when the value is changed).  Because of this, we want to ignore
								// processing in these routines when we are initializing the control
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLSPAGE_H__B04A0C34_2F48_11D5_9A74_0003472193C8__INCLUDED_)
