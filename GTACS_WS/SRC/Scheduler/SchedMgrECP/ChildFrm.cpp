// ChildFrm.cpp : implementation of the CChildFrame class
//

#include "stdafx.h"
#include "SchedMgrECP.h"
#include "AfxImpl.h"
#include "ChildFrm.h"
#include "afxpriv.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame construction/destruction

CChildFrame::CChildFrame()
{
	// TODO: add member initialization code here
	
}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	return CMDIChildWnd::PreCreateWindow(cs);
}



/////////////////////////////////////////////////////////////////////////////
// CChildFrame diagnostics

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildFrame message handlers

BOOL CChildFrame::Create(LPCTSTR lpszClassName,
	LPCTSTR lpszWindowName, DWORD dwStyle,
	const RECT& rect, CMDIFrameWnd* pParentWnd,
	CCreateContext* pContext)
{
	if (pParentWnd == NULL)
	{
		CWnd* pMainWnd = AfxGetThread()->m_pMainWnd;
		ASSERT(pMainWnd != NULL);
		ASSERT_KINDOF(CMDIFrameWnd, pMainWnd);
		pParentWnd = (CMDIFrameWnd*)pMainWnd;
	}

	ASSERT(::IsWindow(pParentWnd->m_hWndMDIClient));

	// insure correct window positioning
	pParentWnd->RecalcLayout();

	// first copy into a CREATESTRUCT for PreCreate
	CREATESTRUCT cs;
	cs.dwExStyle		= 0L;
	cs.lpszClass		= lpszClassName;
	cs.lpszName			= lpszWindowName;
	cs.style			= dwStyle;
	cs.x				= rect.left;
	cs.y				= rect.top;
	cs.cx				= rect.right - rect.left;
	cs.cy				= rect.bottom - rect.top;
	cs.hwndParent		= pParentWnd->m_hWnd;
	cs.hMenu			= NULL;
	cs.hInstance		= AfxGetInstanceHandle();
	cs.lpCreateParams	= (LPVOID)pContext;

	if (!PreCreateWindow(cs))
	{
		PostNcDestroy();
		return FALSE;
	}

	// extended style must be zero for MDI Children (except under Win4)
//	ASSERT(afxData.bWin95 || cs.dwExStyle == 0);
	ASSERT(cs.hwndParent == pParentWnd->m_hWnd);    // must not change

	// restore code frame window to last maximized/restored state 
	CSchedMgrECPApp*	pApp	= (CSchedMgrECPApp*)AfxGetApp();
	BOOL				bMax	= pApp->GetProfileInt(_T("CodeMax"), _T("CODEMAXIMIZED"), FALSE);
	DWORD				chStyle = bMax ? WS_MAXIMIZE : 0;
	int					nWidth	= pApp->GetProfileInt(_T("CodeMax"), _T("CODEWINWTH"), cs.cx);
	int					nHeight = pApp->GetProfileInt(_T("CodeMax"), _T("CODEWINHGT"), cs.cy);

	// now copy into a MDICREATESTRUCT for real create
	MDICREATESTRUCT mcs;
	mcs.szClass	= cs.lpszClass;
	mcs.szTitle = cs.lpszName;
	mcs.hOwner	= cs.hInstance;
	mcs.x		= cs.x;
	mcs.y		= cs.y;
	mcs.cx		= bMax ? cs.cx : nWidth;
	mcs.cy		= bMax ? cs.cy : nHeight;
	mcs.style	= bMax ? (cs.style | chStyle) : (cs.style & ~(WS_MAXIMIZE | WS_VISIBLE));
	mcs.lParam	= (LONG)cs.lpCreateParams;

	// create the window through the MDICLIENT window
	AfxHookWindowCreate(this);
	HWND hWnd = (HWND)::SendMessage(pParentWnd->m_hWndMDIClient, WM_MDICREATE, 0, (LPARAM)&mcs);

	if (!AfxUnhookWindowCreate())
		PostNcDestroy();        // cleanup if MDICREATE fails too soon

	if (hWnd == NULL)
		return FALSE;

	// special handling of visibility (always created invisible)
	if (cs.style & WS_VISIBLE)
	{
		// place the window on top in z-order before showing it
		::BringWindowToTop(hWnd);

		// show it as specified
		if (cs.style & WS_MINIMIZE)
			ShowWindow(SW_SHOWMINIMIZED);
		else if (cs.style & WS_MAXIMIZE)
			ShowWindow(SW_SHOWMAXIMIZED);
		else
			ShowWindow(SW_SHOWNORMAL);

		// make sure it is active (visibility == activation)
		pParentWnd->MDIActivate(this);

		// refresh MDI Window menu
		::SendMessage(pParentWnd->m_hWndMDIClient, WM_MDIREFRESHMENU, 0, 0);
	}

	ASSERT(hWnd == m_hWnd);
	return TRUE;
}

void CChildFrame::OnSize(UINT nType, int cx, int cy) 
{
	CMDIChildWnd::OnSize(nType, cx, cy);
	
	CSchedMgrECPApp* pApp = (CSchedMgrECPApp*)AfxGetApp();

	if( nType == SIZE_RESTORED && IsWindowVisible() )
	{
		pApp->WriteProfileInt(_T("CodeMax"), _T("CODEWINWTH"), cx);
		pApp->WriteProfileInt(_T("CodeMax"), _T("CODEWINHGT"), cy);
		pApp->WriteProfileInt(_T("CodeMax"), _T("CODEMAXIMIZED"), FALSE);
	}
	else if(nType == SIZE_MAXIMIZED && IsWindowVisible() )
		pApp->WriteProfileInt(_T("CodeMax"), _T("CODEMAXIMIZED"), TRUE);
}

