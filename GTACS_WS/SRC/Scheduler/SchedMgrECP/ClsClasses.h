#if !defined( CLSCLASSESSSHPP)
#define CLSCLASSESSSHPP
#include "sslex.hpp"
#include "ssyacc.hpp"
#include "GoldWrapper.h"

class ClsLexClass : public SSLex
{
public:
	SSConstr ClsLexClass( const char*);
	SSConstr ClsLexClass( const char*, const char*);
	SSConstr ClsLexClass( SSLexConsumer&, SSLexTable&);

	const char* tokenToConstChar( SSUnsigned32);
};

SSInline ClsLexClass::ClsLexClass( const char* pszFile) : 
	SSLex( pszFile, ".\\Cls.dfa")
{
}

SSInline ClsLexClass::ClsLexClass( const char* pszFile, const char* pszTable) : 
	SSLex( pszFile, pszTable)
{
}

SSInline ClsLexClass::ClsLexClass( SSLexConsumer& Consumer, SSLexTable& Table) : 
	SSLex( Consumer, Table)
{
}

class ClsYaccClass : public GoldWrapper
{
public:
	SSConstr ClsYaccClass( const char*);

	SSYaccStackElement* reduce( SSUnsigned32, SSUnsigned32);

	virtual SSDestr ClsYaccClass( void);

	virtual SSBooleanValue error( SSUnsigned32, SSLexLexeme&);
	void parseError(SSLexLexeme& qLookahead);
	void filename(CString filename) { m_strFilename = filename; }
	void errorCount(int* nErrors)	{ m_pnErrorCnt = nErrors; }

	CMapStringToString	m_mapScanReqOptions;
	CMapStringToString	m_mapStarReqOptions;
	CStringList			m_listScanOptOptions;
	CStringList			m_listStarOptOptions;
	CString				m_strErrorMessage;
	CString				m_strFilename;
	int*				m_pnErrorCnt;
	int					m_nErrorNum;

protected:
};

SSInline ClsYaccClass::ClsYaccClass( const char* pszFile) : 
	GoldWrapper(pszFile)
{
	m_mapScanReqOptions[ctstrFRAME] 	= _T("");
	m_mapScanReqOptions[ctstrPRIORITY] 	= _T("");
	m_mapScanReqOptions[ctstrMODE] 		= _T("");
	m_mapScanReqOptions[ctstrSIDE] 		= _T("");
	m_mapScanReqOptions[ctstrINSTRUMENT]= _T("");
	m_mapStarReqOptions[ctstrMAX] 		= _T("");
	m_mapStarReqOptions[ctstrDUR] 		= _T("");
	m_mapStarReqOptions[ctstrINSTRUMENT]= _T("");
	m_pnErrorCnt= NULL;
	m_nErrorNum = 0;
}

SSInline ClsYaccClass::~ClsYaccClass( void)
{
	m_mapScanReqOptions.RemoveAll();
	m_mapStarReqOptions.RemoveAll();
	m_listScanOptOptions.RemoveAll();
	m_listStarOptOptions.RemoveAll();
	m_pnErrorCnt = NULL;
}

#endif
