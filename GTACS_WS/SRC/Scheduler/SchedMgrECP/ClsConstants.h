﻿enum SymbolConstants
{
   SYM_EOF                  =  0, // (EOF)
   SYM_ERROR                =  1, // (Error)
   SYM_COMMENTS             =  2, // Comments
   SYM_WHITESPACE           =  3, // Whitespace
   SYM_MINUS                =  4, // '-'
   SYM_QUOTE                =  5, // '"'
   SYM_DOLLAR               =  6, // '$'
   SYM_PERCENTERROR         =  7, // '%error'
   SYM_LPAREN               =  8, // '('
   SYM_RPAREN               =  9, // ')'
   SYM_COMMA                = 10, // ','
   SYM_DOT                  = 11, // '.'
   SYM_DIV                  = 12, // '/'
   SYM_PLUS                 = 13, // '+'
   SYM_EQ                   = 14, // '='
   SYM_BBCAL                = 15, // BBCAL
   SYM_CHARACTERCONSTANT    = 16, // CHARACTERconstant
   SYM_CHARACTERHEXCONSTANT = 17, // CHARACTERhexConstant
   SYM_CHARACTEROCTCONSTANT = 18, // CHARACTERoctConstant
   SYM_CLS                  = 19, // CLS
   SYM_DATA                 = 20, // DATA
   SYM_DURATION             = 21, // DURATION
   SYM_EXECUTE              = 22, // EXECUTE
   SYM_FILENAME             = 23, // FILENAME
   SYM_FLOATINGCONSTANT     = 24, // FLOATINGconstant
   SYM_FRAME                = 25, // FRAME
   SYM_GO                   = 26, // GO
   SYM_HEXCONSTANT          = 27, // HEXconstant
   SYM_IDENTIFIER           = 28, // IDENTIFIER
   SYM_INSTRUMENT           = 29, // INSTRUMENT
   SYM_INTEGERCONSTANT      = 30, // INTEGERconstant
   SYM_LABEL                = 31, // LABEL
   SYM_LAST_STAR_CLASS      = 32, // 'LAST_STAR_CLASS'
   SYM_LIMITS               = 33, // LIMITS
   SYM_MAX                  = 34, // MAX
   SYM_MODE                 = 35, // MODE
   SYM_MONSCHED             = 36, // MONSCHED
   SYM_NEWLINE              = 37, // NewLine
   SYM_NEXTSCHED            = 38, // NEXTSCHED
   SYM_OCTALCONSTANT        = 39, // OCTALconstant
   SYM_PATHFILEOPT          = 40, // pathFileOpt
   SYM_PREFIXNUMID          = 41, // PREFIXNUMID
   SYM_PRIORITY             = 42, // PRIORITY
   SYM_REPEAT               = 43, // REPEAT
   SYM_RTCS                 = 44, // RTCS
   SYM_SCAN                 = 45, // SCAN
   SYM_SET                  = 46, // SET
   SYM_SIDE                 = 47, // SIDE
   SYM_STAR                 = 48, // STAR
   SYM_START                = 49, // START
   SYM_STOP                 = 50, // STOP
   SYM_TIMECONSTANT         = 51, // TIMEconstant
   SYM_TIMESTRING           = 52, // TIMEstring
   SYM_CLSSTATEMENT         = 53, // <clsStatement>
   SYM_CONSTANT             = 54, // <constant>
   SYM_EOL                  = 55, // <eol>
   SYM_NEXTSCHED2           = 56, // <nextSched>
   SYM_PARAMETER            = 57, // <parameter>
   SYM_PARAMETERLIST        = 58, // <parameterList>
   SYM_PATHFILEOPT2         = 59, // <pathFileOpt>
   SYM_PATHLIST             = 60, // <pathList>
   SYM_PATHNAME             = 61, // <pathname>
   SYM_RTCSSTATEMENT        = 62, // <rtcsStatement>
   SYM_SCAN2                = 63, // <scan>
   SYM_SCANOPT              = 64, // <scanOpt>
   SYM_STAR2                = 65, // <star>
   SYM_STAROPT              = 66, // <starOpt>
   SYM_START2               = 67, // <start>
   SYM_STARTSTATEMENT       = 68, // <startStatement>
   SYM_STATEMENT            = 69, // <statement>
   SYM_STATEMENTERROR       = 70, // <statementError>
   SYM_STATEMENTLIST        = 71, // <statementList>
   SYM_UNARYOP              = 72  // <unaryOp>
};

enum ProductionConstants
{
   PROD_EOL_NEWLINE                                                                                       =  0, // <eol> ::= NewLine
   PROD_START                                                                                             =  1, // <start> ::= <statementList>
   PROD_STATEMENTLIST                                                                                     =  2, // <statementList> ::= <statementList> <statement>
   PROD_STATEMENTLIST2                                                                                    =  3, // <statementList> ::= <statement>
   PROD_STATEMENT                                                                                         =  4, // <statement> ::= <scan>
   PROD_STATEMENT2                                                                                        =  5, // <statement> ::= <star>
   PROD_STATEMENT3                                                                                        =  6, // <statement> ::= <nextSched>
   PROD_STATEMENT4                                                                                        =  7, // <statement> ::= <clsStatement>
   PROD_STATEMENT5                                                                                        =  8, // <statement> ::= <startStatement>
   PROD_STATEMENT6                                                                                        =  9, // <statement> ::= <rtcsStatement>
   PROD_STATEMENT7                                                                                        = 10, // <statement> ::= <statementError>
   PROD_STATEMENT_PERCENTERROR                                                                            = 11, // <statement> ::= '%error' <eol>
   PROD_STATEMENT8                                                                                        = 12, // <statement> ::= <eol>
   PROD_STATEMENTERROR_TIMECONSTANT_START                                                                 = 13, // <statementError> ::= TIMEconstant START
   PROD_STATEMENTERROR_TIMECONSTANT_CLS                                                                   = 14, // <statementError> ::= TIMEconstant CLS
   PROD_NEXTSCHED_TIMECONSTANT_MONSCHED_NEXTSCHED_SET                                                     = 15, // <nextSched> ::= TIMEconstant MONSCHED NEXTSCHED SET <pathFileOpt> <eol>
   PROD_NEXTSCHED_MONSCHED_NEXTSCHED_SET                                                                  = 16, // <nextSched> ::= MONSCHED NEXTSCHED SET <pathFileOpt> <eol>
   PROD_NEXTSCHED_TIMECONSTANT_MONSCHED_NEXTSCHED_SET_QUOTE_QUOTE                                         = 17, // <nextSched> ::= TIMEconstant MONSCHED NEXTSCHED SET '"' <pathFileOpt> '"' <eol>
   PROD_NEXTSCHED_MONSCHED_NEXTSCHED_SET_QUOTE_QUOTE                                                      = 18, // <nextSched> ::= MONSCHED NEXTSCHED SET '"' <pathFileOpt> '"' <eol>
   PROD_NEXTSCHED_TIMECONSTANT_MONSCHED_NEXTSCHED_SET_IDENTIFIER                                          = 19, // <nextSched> ::= TIMEconstant MONSCHED NEXTSCHED SET IDENTIFIER <eol>
   PROD_NEXTSCHED_MONSCHED_NEXTSCHED_SET_IDENTIFIER                                                       = 20, // <nextSched> ::= MONSCHED NEXTSCHED SET IDENTIFIER <eol>
   PROD_NEXTSCHED_TIMECONSTANT_MONSCHED_NEXTSCHED_GO                                                      = 21, // <nextSched> ::= TIMEconstant MONSCHED NEXTSCHED GO <eol>
   PROD_NEXTSCHED_MONSCHED_NEXTSCHED_GO                                                                   = 22, // <nextSched> ::= MONSCHED NEXTSCHED GO <eol>
   PROD_STARTSTATEMENT_TIMECONSTANT_START_IDENTIFIER                                                      = 23, // <startStatement> ::= TIMEconstant START IDENTIFIER <eol>
   PROD_STARTSTATEMENT_START_IDENTIFIER                                                                   = 24, // <startStatement> ::= START IDENTIFIER <eol>
   PROD_STARTSTATEMENT_TIMECONSTANT_START                                                                 = 25, // <startStatement> ::= TIMEconstant START <pathFileOpt> <eol>
   PROD_STARTSTATEMENT_START                                                                              = 26, // <startStatement> ::= START <pathFileOpt> <eol>
   PROD_STARTSTATEMENT_TIMECONSTANT_START_QUOTE_QUOTE                                                     = 27, // <startStatement> ::= TIMEconstant START '"' <pathFileOpt> '"' <eol>
   PROD_STARTSTATEMENT_START_QUOTE_QUOTE                                                                  = 28, // <startStatement> ::= START '"' <pathFileOpt> '"' <eol>
   PROD_STARTSTATEMENT_TIMECONSTANT_START_LPAREN_RPAREN                                                   = 29, // <startStatement> ::= TIMEconstant START <pathFileOpt> '(' <parameterList> ')' <eol>
   PROD_STARTSTATEMENT_START_PATHFILEOPT_LPAREN_RPAREN                                                    = 30, // <startStatement> ::= START pathFileOpt '(' <parameterList> ')' <eol>
   PROD_STARTSTATEMENT_TIMECONSTANT_START_IDENTIFIER_LPAREN_RPAREN                                        = 31, // <startStatement> ::= TIMEconstant START IDENTIFIER '(' <parameterList> ')' <eol>
   PROD_STARTSTATEMENT_START_IDENTIFIER_LPAREN_RPAREN                                                     = 32, // <startStatement> ::= START IDENTIFIER '(' <parameterList> ')' <eol>
   PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER                                                               = 33, // <rtcsStatement> ::= RTCS LABEL IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_RTCS_LABEL_EQ_IDENTIFIER                                                            = 34, // <rtcsStatement> ::= RTCS LABEL '=' IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_RTCS_LABEL_EQ_IDENTIFIER_IDENTIFIER_EQ_IDENTIFIER                                   = 35, // <rtcsStatement> ::= RTCS LABEL '=' IDENTIFIER IDENTIFIER '=' IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_EQ_IDENTIFIER                                      = 36, // <rtcsStatement> ::= RTCS LABEL IDENTIFIER IDENTIFIER '=' IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_EQ_IDENTIFIER_IDENTIFIER                                      = 37, // <rtcsStatement> ::= RTCS LABEL IDENTIFIER '=' IDENTIFIER IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_IDENTIFIER                                         = 38, // <rtcsStatement> ::= RTCS LABEL IDENTIFIER IDENTIFIER IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_TIMECONSTANT_RTCS_LABEL_IDENTIFIER                                                  = 39, // <rtcsStatement> ::= TIMEconstant RTCS LABEL IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_TIMECONSTANT_RTCS_LABEL_EQ_IDENTIFIER                                               = 40, // <rtcsStatement> ::= TIMEconstant RTCS LABEL '=' IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_TIMECONSTANT_RTCS_LABEL_EQ_IDENTIFIER_IDENTIFIER_EQ_IDENTIFIER                      = 41, // <rtcsStatement> ::= TIMEconstant RTCS LABEL '=' IDENTIFIER IDENTIFIER '=' IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_TIMECONSTANT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_EQ_IDENTIFIER                         = 42, // <rtcsStatement> ::= TIMEconstant RTCS LABEL IDENTIFIER IDENTIFIER '=' IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_TIMECONSTANT_RTCS_LABEL_IDENTIFIER_EQ_IDENTIFIER_IDENTIFIER                         = 43, // <rtcsStatement> ::= TIMEconstant RTCS LABEL IDENTIFIER '=' IDENTIFIER IDENTIFIER <eol>
   PROD_RTCSSTATEMENT_TIMECONSTANT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_IDENTIFIER                            = 44, // <rtcsStatement> ::= TIMEconstant RTCS LABEL IDENTIFIER IDENTIFIER IDENTIFIER <eol>
   PROD_CLSSTATEMENT_TIMECONSTANT_CLS_IDENTIFIER_LPAREN_RPAREN_START_EQ_TIMECONSTANT_STOP_EQ_TIMECONSTANT = 45, // <clsStatement> ::= TIMEconstant CLS IDENTIFIER '(' <parameterList> ')' START '=' TIMEconstant STOP '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_CLS_IDENTIFIER_LPAREN_RPAREN_START_EQ_TIMECONSTANT_STOP_EQ_TIMECONSTANT              = 46, // <clsStatement> ::= CLS IDENTIFIER '(' <parameterList> ')' START '=' TIMEconstant STOP '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_TIMECONSTANT_CLS_IDENTIFIER_LPAREN_RPAREN_START_EQ_TIMECONSTANT                      = 47, // <clsStatement> ::= TIMEconstant CLS IDENTIFIER '(' <parameterList> ')' START '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_CLS_IDENTIFIER_LPAREN_RPAREN_START_EQ_TIMECONSTANT                                   = 48, // <clsStatement> ::= CLS IDENTIFIER '(' <parameterList> ')' START '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_TIMECONSTANT_CLS_IDENTIFIER_LPAREN_RPAREN_STOP_EQ_TIMECONSTANT                       = 49, // <clsStatement> ::= TIMEconstant CLS IDENTIFIER '(' <parameterList> ')' STOP '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_CLS_IDENTIFIER_LPAREN_RPAREN_STOP_EQ_TIMECONSTANT                                    = 50, // <clsStatement> ::= CLS IDENTIFIER '(' <parameterList> ')' STOP '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_TIMECONSTANT_CLS_IDENTIFIER_LPAREN_RPAREN                                            = 51, // <clsStatement> ::= TIMEconstant CLS IDENTIFIER '(' <parameterList> ')' <eol>
   PROD_CLSSTATEMENT_CLS_IDENTIFIER_LPAREN_RPAREN                                                         = 52, // <clsStatement> ::= CLS IDENTIFIER '(' <parameterList> ')' <eol>
   PROD_CLSSTATEMENT_TIMECONSTANT_CLS_IDENTIFIER_START_EQ_TIMECONSTANT_STOP_EQ_TIMECONSTANT               = 53, // <clsStatement> ::= TIMEconstant CLS IDENTIFIER START '=' TIMEconstant STOP '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_CLS_IDENTIFIER_START_EQ_TIMECONSTANT_STOP_EQ_TIMECONSTANT                            = 54, // <clsStatement> ::= CLS IDENTIFIER START '=' TIMEconstant STOP '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_TIMECONSTANT_CLS_IDENTIFIER_START_EQ_TIMECONSTANT                                    = 55, // <clsStatement> ::= TIMEconstant CLS IDENTIFIER START '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_CLS_IDENTIFIER_START_EQ_TIMECONSTANT                                                 = 56, // <clsStatement> ::= CLS IDENTIFIER START '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_TIMECONSTANT_CLS_IDENTIFIER_STOP_EQ_TIMECONSTANT                                     = 57, // <clsStatement> ::= TIMEconstant CLS IDENTIFIER STOP '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_CLS_IDENTIFIER_STOP_EQ_TIMECONSTANT                                                  = 58, // <clsStatement> ::= CLS IDENTIFIER STOP '=' TIMEconstant <eol>
   PROD_CLSSTATEMENT_TIMECONSTANT_CLS_IDENTIFIER                                                          = 59, // <clsStatement> ::= TIMEconstant CLS IDENTIFIER <eol>
   PROD_CLSSTATEMENT_CLS_IDENTIFIER                                                                       = 60, // <clsStatement> ::= CLS IDENTIFIER <eol>
   PROD_PARAMETER_IDENTIFIER                                                                              = 61, // <parameter> ::= IDENTIFIER
   PROD_PARAMETER_QUOTE_IDENTIFIER_QUOTE                                                                  = 62, // <parameter> ::= '"' IDENTIFIER '"'
   PROD_PARAMETER_QUOTE_QUOTE                                                                             = 63, // <parameter> ::= '"' <pathFileOpt> '"'
   PROD_PARAMETER_PREFIXNUMID                                                                             = 64, // <parameter> ::= PREFIXNUMID
   PROD_PARAMETER_TIMESTRING                                                                              = 65, // <parameter> ::= TIMEstring
   PROD_PARAMETER                                                                                         = 66, // <parameter> ::= <constant>
   PROD_PARAMETER_IDENTIFIER_EQ_IDENTIFIER                                                                = 67, // <parameter> ::= IDENTIFIER '=' IDENTIFIER
   PROD_PARAMETER_IDENTIFIER_EQ_QUOTE_IDENTIFIER_QUOTE                                                    = 68, // <parameter> ::= IDENTIFIER '=' '"' IDENTIFIER '"'
   PROD_PARAMETER_IDENTIFIER_EQ_PREFIXNUMID                                                               = 69, // <parameter> ::= IDENTIFIER '=' PREFIXNUMID
   PROD_PARAMETER_IDENTIFIER_EQ_TIMESTRING                                                                = 70, // <parameter> ::= IDENTIFIER '=' TIMEstring
   PROD_PARAMETER_IDENTIFIER_EQ                                                                           = 71, // <parameter> ::= IDENTIFIER '=' <constant>
   PROD_PARAMETER2                                                                                        = 72, // <parameter> ::= 
   PROD_PARAMETERLIST_COMMA                                                                               = 73, // <parameterList> ::= <parameterList> ',' <parameter>
   PROD_PARAMETERLIST                                                                                     = 74, // <parameterList> ::= <parameter>
   PROD_UNARYOP_PLUS                                                                                      = 75, // <unaryOp> ::= '+'
   PROD_UNARYOP_MINUS                                                                                     = 76, // <unaryOp> ::= '-'
   PROD_CONSTANT_HEXCONSTANT                                                                              = 77, // <constant> ::= HEXconstant
   PROD_CONSTANT_OCTALCONSTANT                                                                            = 78, // <constant> ::= OCTALconstant
   PROD_CONSTANT_INTEGERCONSTANT                                                                          = 79, // <constant> ::= INTEGERconstant
   PROD_CONSTANT_INTEGERCONSTANT2                                                                         = 80, // <constant> ::= <unaryOp> INTEGERconstant
   PROD_CONSTANT_FLOATINGCONSTANT                                                                         = 81, // <constant> ::= FLOATINGconstant
   PROD_CONSTANT_FLOATINGCONSTANT2                                                                        = 82, // <constant> ::= <unaryOp> FLOATINGconstant
   PROD_CONSTANT_CHARACTERCONSTANT                                                                        = 83, // <constant> ::= CHARACTERconstant
   PROD_CONSTANT_CHARACTEROCTCONSTANT                                                                     = 84, // <constant> ::= CHARACTERoctConstant
   PROD_CONSTANT_CHARACTERHEXCONSTANT                                                                     = 85, // <constant> ::= CHARACTERhexConstant
   PROD_CONSTANT_TIMECONSTANT                                                                             = 86, // <constant> ::= TIMEconstant
   PROD_SCAN_SCAN                                                                                         = 87, // <scan> ::= SCAN <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <eol>
   PROD_SCAN_SCAN2                                                                                        = 88, // <scan> ::= SCAN <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <eol>
   PROD_SCAN_SCAN3                                                                                        = 89, // <scan> ::= SCAN <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <eol>
   PROD_SCAN_SCAN4                                                                                        = 90, // <scan> ::= SCAN <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <eol>
   PROD_SCAN_TIMECONSTANT_SCAN                                                                            = 91, // <scan> ::= TIMEconstant SCAN <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <eol>
   PROD_SCAN_TIMECONSTANT_SCAN2                                                                           = 92, // <scan> ::= TIMEconstant SCAN <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <eol>
   PROD_SCAN_TIMECONSTANT_SCAN3                                                                           = 93, // <scan> ::= TIMEconstant SCAN <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <eol>
   PROD_SCAN_TIMECONSTANT_SCAN4                                                                           = 94, // <scan> ::= TIMEconstant SCAN <scanOpt> <scanOpt> <scanOpt> <scanOpt> <scanOpt> <eol>
   PROD_STAR_STAR                                                                                         = 95, // <star> ::= STAR <starOpt> <starOpt> <starOpt> <starOpt> <eol>
   PROD_STAR_STAR2                                                                                        = 96, // <star> ::= STAR <starOpt> <starOpt> <starOpt> <eol>
   PROD_STAR_TIMECONSTANT_STAR                                                                            = 97, // <star> ::= TIMEconstant STAR <starOpt> <starOpt> <starOpt> <starOpt> <eol>
   PROD_STAR_TIMECONSTANT_STAR2                                                                           = 98, // <star> ::= TIMEconstant STAR <starOpt> <starOpt> <starOpt> <eol>
   PROD_SCANOPT_FRAME_EQ_IDENTIFIER                                                                       = 99, // <scanOpt> ::= FRAME '=' IDENTIFIER
   PROD_SCANOPT_PRIORITY_EQ_IDENTIFIER                                                                    = 100, // <scanOpt> ::= PRIORITY '=' IDENTIFIER
   PROD_SCANOPT_MODE_EQ_IDENTIFIER                                                                        = 101, // <scanOpt> ::= MODE '=' IDENTIFIER
   PROD_SCANOPT_EXECUTE_EQ_IDENTIFIER                                                                     = 102, // <scanOpt> ::= EXECUTE '=' IDENTIFIER
   PROD_SCANOPT_BBCAL_EQ_IDENTIFIER                                                                       = 103, // <scanOpt> ::= BBCAL '=' IDENTIFIER
   PROD_SCANOPT_SIDE_EQ_IDENTIFIER                                                                        = 104, // <scanOpt> ::= SIDE '=' IDENTIFIER
   PROD_SCANOPT_INSTRUMENT_EQ_IDENTIFIER                                                                  = 105, // <scanOpt> ::= INSTRUMENT '=' IDENTIFIER
   PROD_SCANOPT_REPEAT_EQ_INTEGERCONSTANT                                                                 = 106, // <scanOpt> ::= REPEAT '=' INTEGERconstant
   PROD_SCANOPT_REPEAT_EQ_IDENTIFIER                                                                      = 107, // <scanOpt> ::= REPEAT '=' IDENTIFIER
   PROD_STAROPT_INSTRUMENT_EQ_IDENTIFIER                                                                  = 108, // <starOpt> ::= INSTRUMENT '=' IDENTIFIER
   PROD_STAROPT_LAST_STAR_CLASS_EQ_IDENTIFIER                                                             = 109, // <starOpt> ::= 'LAST_STAR_CLASS' '=' IDENTIFIER
   PROD_STAROPT_DURATION_EQ_TIMESTRING                                                                    = 110, // <starOpt> ::= DURATION '=' TIMEstring
   PROD_STAROPT_MAX_EQ_INTEGERCONSTANT                                                                    = 111, // <starOpt> ::= MAX '=' INTEGERconstant
   PROD_PATHNAME_DIV_IDENTIFIER                                                                           = 112, // <pathname> ::= '/' IDENTIFIER
   PROD_PATHNAME_DIV_DOLLAR_IDENTIFIER                                                                    = 113, // <pathname> ::= '/' '$' IDENTIFIER
   PROD_PATHNAME_DIV_LIMITS                                                                               = 114, // <pathname> ::= '/' LIMITS
   PROD_PATHNAME_DIV_DATA                                                                                 = 115, // <pathname> ::= '/' DATA
   PROD_PATHLIST                                                                                          = 116, // <pathList> ::= <pathList> <pathname>
   PROD_PATHLIST2                                                                                         = 117, // <pathList> ::= <pathname>
   PROD_PATHFILEOPT_DIV_FILENAME                                                                          = 118, // <pathFileOpt> ::= <pathList> '/' FILENAME
   PROD_PATHFILEOPT                                                                                       = 119, // <pathFileOpt> ::= <pathList>
   PROD_PATHFILEOPT_DOT_DIV_FILENAME                                                                      = 120, // <pathFileOpt> ::= '.' <pathList> '/' FILENAME
   PROD_PATHFILEOPT_DOT                                                                                   = 121, // <pathFileOpt> ::= '.' <pathList>
   PROD_PATHFILEOPT_DOT_DOT_DIV_FILENAME                                                                  = 122, // <pathFileOpt> ::= '.' '.' <pathList> '/' FILENAME
   PROD_PATHFILEOPT_DOT_DOT                                                                               = 123, // <pathFileOpt> ::= '.' '.' <pathList>
   PROD_PATHFILEOPT_FILENAME                                                                              = 124, // <pathFileOpt> ::= FILENAME
   PROD_PATHFILEOPT_DOLLAR_IDENTIFIER                                                                     = 125  // <pathFileOpt> ::= '$' IDENTIFIER
};
