#include "stdafx.h"
#if !defined( CLSFUNCTIONSSSCPP)
#define CLSFUNCTIONSSSCPP
#include "ClsConstants.h"
#include "ClsClasses.h"

const char* ClsLexClass::tokenToConstChar( SSUnsigned32 ulToken)
{
	const char* pchToken;
	switch ( ulToken)
	{
	case SYM_TIMECONSTANT:
		pchToken = "TIMEconstant";
		break;

	case SYM_FILENAME:
		pchToken = "FILENAME";
		break;

	case SYM_EOL:
		pchToken = "eol";
		break;

	case SYM_IDENTIFIER:
		pchToken = "IDENTIFIER";
		break;

	case SYM_PREFIXNUMID:
		pchToken = "PREFIXNUMID";
		break;

	case SYM_OCTALCONSTANT:
		pchToken = "OCTALconstant";
		break;

	case SYM_INTEGERCONSTANT:
		pchToken = "INTEGERconstant";
		break;

	case SYM_HEXCONSTANT:
		pchToken = "HEXconstant";
		break;

	case SYM_CHARACTERCONSTANT:
		pchToken = "CHARACTERconstant";
		break;

	case SYM_CHARACTEROCTCONSTANT:
		pchToken = "CHARACTERoctConstant";
		break;

	case SYM_CHARACTERHEXCONSTANT:
		pchToken = "CHARACTERhexConstant";
		break;

	case SYM_FLOATINGCONSTANT:
		pchToken = "FLOATINGconstant0";
		break;
/*
	case ClsLexTokenFloat1:
		pchToken = "FLOATINGconstant1";
		break;

	case ClsLexTokenFloat2:
		pchToken = "FLOATINGconstant2";
		break;

	case ClsLexTokenQuote:
		pchToken = "\"";
		break;
*/
	case SYM_LPAREN:
		pchToken = "(";
		break;

	case SYM_RPAREN:
		pchToken = ")";
		break;

	case SYM_EQ:
		pchToken = "=";
		break;

	case SYM_COMMA:
		pchToken = ",";
		break;

	case SYM_DOLLAR:
		pchToken = "$";
		break;

//	case ClsLexTokenBackSlash:
//		pchToken = "\\";
//		break;

	case SYM_DOT:
		pchToken = ".";
		break;

	//case ClsLexTokenColon:
	//	pchToken = ":";
	//	break;

	//case ClsLexTokenSemiColon:
	//	pchToken = ";";
	//	break;

	//case ClsLexTokenAt:
	//	pchToken = "@";
	//	break;

	//case ClsLexTokenUnderScore:
	//	pchToken = "_";
	//	break;

	//case ClsLexTokenBoolNot:
	//	pchToken = "!";
	//	break;

	//case ClsLexTokenOBrack:
	//	pchToken = "[";
	//	break;

	//case ClsLexTokenCBrack:
	//	pchToken = "]";
	//	break;

	//case ClsLexTokenOBrace:
	//	pchToken = "{";
	//	break;

	//case ClsLexTokenCBrace:
	//	pchToken = "}";
	//	break;

	//case ClsLexTokenSingleQuote:
	//	pchToken = "'";
	//	break;

	case SYM_PLUS:
		pchToken = "+";
		break;

	case SYM_MINUS:
		pchToken = "-";
		break;

	//case ClsLexTokenMult:
	//	pchToken = "*";
	//	break;

	case SYM_DIV:
		pchToken = "/";
		break;

	//case ClsLexTokenLT:
	//	pchToken = "LT";
	//	break;

	//case ClsLexTokenGT:
	//	pchToken = "GT";
	//	break;

	//case ClsLexTokenBitOR:
	//	pchToken = "|";
	//	break;

	//case ClsLexTokenBitAND:
	//	pchToken = "&";
	//	break;

	case SYM_BBCAL:
		pchToken = "BBCAL";
		break;

	case SYM_CLS:
		pchToken = "CLS";
		break;

	case SYM_DURATION:
		pchToken = "DURATION";
		break;

	case SYM_EXECUTE:
		pchToken = "EXECUTE";
		break;

	case SYM_FRAME:
		pchToken = "FRAME";
		break;

	case SYM_GO:
		pchToken = "GO";
		break;

	case SYM_INSTRUMENT:
		pchToken = "INSTRUMENT";
		break;

	case SYM_LABEL:
		pchToken = "LABEL";
		break;

	case SYM_LAST_STAR_CLASS:
		pchToken = "LAST_STAR_CLASS";
		break;

	case SYM_MAX:
		pchToken = "MAX";
		break;

	case SYM_MODE:
		pchToken = "MODE";
		break;

	case SYM_MONSCHED:
		pchToken = "MONSCHED";
		break;

	case SYM_NEXTSCHED:
		pchToken = "NEXTSCHED";
		break;

	case SYM_PRIORITY:
		pchToken = "PRIORITY";
		break;

	case SYM_REPEAT:
		pchToken = "REPEAT";
		break;

	case SYM_RTCS:
		pchToken = "RTCS";
		break;

	case SYM_SCAN:
		pchToken = "SCAN";
		break;

	case SYM_SET:
		pchToken = "SET";
		break;

	case SYM_SIDE:
		pchToken = "SIDE";
		break;

	case SYM_STAR:
		pchToken = "STAR";
		break;

	case SYM_START:
		pchToken = "START";
		break;

	case SYM_STOP:
		pchToken = "STOP";
		break;

	case SSYaccErrorToken:
		pchToken = "%error";
		break;

	case SSYaccEofToken:
		pchToken = "eof";
		break;

	default:
		pchToken = SSLexTokenNotFound;
	}
	return pchToken;
}

#endif
