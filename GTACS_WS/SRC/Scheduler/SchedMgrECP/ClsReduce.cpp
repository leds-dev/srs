#include "stdafx.h"
#if !defined( CLSREDUCESSCPP)
#define CLSREDUCESSCPP
#include "SchedMgrECP.h"
#include "ClsConstants.h"
#include "ClsClasses.h"

SSBooleanValue ClsYaccClass::error(SSUnsigned32 qulState, SSLexLexeme& qLookahead)
{
	if((*m_pnErrorCnt) >= 100)
	{
		CString strMsg;

		strMsg.Format(IDS_SYNTAX_ERROR_COUNT, 100);
		GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
	
		return SSTrue;
	}

	m_nErrorNum = 0;
	parseError(qLookahead);
	return GoldWrapper::error(qulState, qLookahead);
}

void ClsYaccClass::parseError(SSLexLexeme& qLookahead)
{
	CString strMsg;
	CString	lexeme(qLookahead.asChar());
	int		line	= qLookahead.line();
	int		offset	= qLookahead.offset();

	(*m_pnErrorCnt)++;

	if((*m_pnErrorCnt) >= 100)
	{
		error(0, qLookahead);
		return;
	}

	if(offset > 0)
		offset = offset - 1;

	if(lexeme == _T("\n"))
	{
		m_nErrorNum	= IDS_SYNTAX_ERROR21;
		lexeme.Format(IDS_SYNTAX_ERROR21);
	}

	if(m_strErrorMessage.IsEmpty())
		m_strErrorMessage = _T("syntax error");

	if(m_nErrorNum == 0)
		m_nErrorNum	= (*m_pnErrorCnt);

	strMsg.Format(_T("%s(%d) : error %d: \'%s\': %s"), 
		m_strFilename, line, m_nErrorNum, lexeme, m_strErrorMessage);
	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL, line, offset);
	m_strErrorMessage.Empty();
	m_nErrorNum = 0;
}

/*
	You must keep this reduce function clean to avoid confusing
	the AutoMerge parser. By 'clean' we mean that if you are going
	to add a significant amount of code to a case, make a function
	call instead of adding the code directly to the case.

	NEVER add embedded switch statements inside this function.
*/
SSYaccStackElement* ClsYaccClass::reduce( SSUnsigned32 ulProd,
      SSUnsigned32 ulSize)
{

	//Uncomment out this line if you want to build a parse tree
	//The treeRoot() function will contain the tree root when finished
	//return addSubTree();

	switch ( ulProd)
	{

		case PROD_STATEMENTERROR_TIMECONSTANT_START: /*ClsYaccProdStateError2:*/
		// statement -> TIMEconstant START %error
		{ 
			SSLexLexeme* z_pLexeme = elementFromProduction(1)->lexeme();
			m_nErrorNum = /*IDS_SYNTAX_ERROR20*/0;
			//m_strErrorMessage.Format(IDS_SYNTAX_ERROR20);
			parseError(*z_pLexeme);
										
			break;
		}

		case PROD_STATEMENTERROR_TIMECONSTANT_CLS: /*ClsYaccProdStateError3:*/
		// statement -> TIMEconstant CLS %error 
		{ 
			SSLexLexeme* z_pLexeme = elementFromProduction(1)->lexeme();
			m_nErrorNum = /*IDS_SYNTAX_ERROR20*/0;
			//m_strErrorMessage.Format(IDS_SYNTAX_ERROR20);
			parseError(*z_pLexeme);
										
			break;
		}

		case PROD_RTCSSTATEMENT_RTCS_LABEL_EQ_IDENTIFIER_IDENTIFIER_EQ_IDENTIFIER: /*ClsYaccProdRtcsState3: */
		// rtcsStatement -> RTCS LABEL = IDENTIFIER IDENTIFIER = IDENTIFIER eol 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(4)->lexeme();
			CString			strToken(z_pLexeme->asConstChar());

			strToken.MakeUpper();

			if(strToken != _T("RT"))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
				break;
			}

			z_pLexeme	= elementFromProduction(6)->lexeme();
			strToken	= z_pLexeme->asConstChar();

			strToken.MakeUpper();

			if( strToken != _T("ACE_1")	&&
				strToken != _T("ACE_2") &&
				strToken != _T("ACE_FOR_DWELL") )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_EQ_IDENTIFIER: /*ClsYaccProdRtcsState4:*/
		// rtcsStatement -> RTCS LABEL IDENTIFIER IDENTIFIER = IDENTIFIER eol 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(3)->lexeme();
			CString			strToken(z_pLexeme->asConstChar());

			strToken.MakeUpper();

			if(strToken != _T("RT"))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
				break;
			}

			z_pLexeme	= elementFromProduction(5)->lexeme();
			strToken	= z_pLexeme->asConstChar();

			strToken.MakeUpper();

			if( strToken != _T("ACE_1")	&&
				strToken != _T("ACE_2") &&
				strToken != _T("ACE_FOR_DWELL") )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_EQ_IDENTIFIER_IDENTIFIER: /*ClsYaccProdRtcsState5:*/
		// rtcsStatement -> RTCS LABEL IDENTIFIER = IDENTIFIER IDENTIFIER eol 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(4)->lexeme();
			CString			strToken(z_pLexeme->asConstChar());

			strToken.MakeUpper();

			if(strToken != _T("RT"))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
				break;
			}

			z_pLexeme	= elementFromProduction(5)->lexeme();
			strToken	= z_pLexeme->asConstChar();

			strToken.MakeUpper();

			if( strToken != _T("ACE_1")	&&
				strToken != _T("ACE_2") &&
				strToken != _T("ACE_FOR_DWELL") )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_IDENTIFIER: /*ClsYaccProdRtcsState6:*/
		// rtcsStatement -> RTCS LABEL IDENTIFIER IDENTIFIER IDENTIFIER eol 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(3)->lexeme();
			CString			strToken(z_pLexeme->asConstChar());

			strToken.MakeUpper();

			if(strToken != _T("RT"))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
				break;
			}

			z_pLexeme	= elementFromProduction(4)->lexeme();
			strToken	= z_pLexeme->asConstChar();

			strToken.MakeUpper();

			if( strToken != _T("ACE_1")	&&
				strToken != _T("ACE_2") &&
				strToken != _T("ACE_FOR_DWELL") )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_RTCSSTATEMENT_TIMECONSTANT_RTCS_LABEL_EQ_IDENTIFIER_IDENTIFIER_EQ_IDENTIFIER: /*ClsYaccProdRtcsState10:*/
		// rtcsStatement -> TIMEconstant RTCS LABEL = IDENTIFIER IDENTIFIER = IDENTIFIER eol 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(5)->lexeme();
			CString			strToken(z_pLexeme->asConstChar());

			strToken.MakeUpper();

			if(strToken != _T("RT"))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
				break;
			}

			z_pLexeme	= elementFromProduction(7)->lexeme();
			strToken	= z_pLexeme->asConstChar();

			strToken.MakeUpper();

			if( strToken != _T("ACE_1")	&&
				strToken != _T("ACE_2") &&
				strToken != _T("ACE_FOR_DWELL") )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_RTCSSTATEMENT_TIMECONSTANT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_EQ_IDENTIFIER: /*ClsYaccProdRtcsState11:*/
		// rtcsStatement -> TIMEconstant RTCS LABEL IDENTIFIER IDENTIFIER = IDENTIFIER eol 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(4)->lexeme();
			CString			strToken(z_pLexeme->asConstChar());

			strToken.MakeUpper();

			if(strToken != _T("RT"))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
				break;
			}

			z_pLexeme	= elementFromProduction(6)->lexeme();
			strToken	= z_pLexeme->asConstChar();

			strToken.MakeUpper();

			if( strToken != _T("ACE_1")	&&
				strToken != _T("ACE_2") &&
				strToken != _T("ACE_FOR_DWELL") )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_RTCSSTATEMENT_TIMECONSTANT_RTCS_LABEL_IDENTIFIER_EQ_IDENTIFIER_IDENTIFIER: /*ClsYaccProdRtcsState12:*/
		// rtcsStatement -> TIMEconstant RTCS LABEL IDENTIFIER = IDENTIFIER IDENTIFIER eol 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(5)->lexeme();
			CString			strToken(z_pLexeme->asConstChar());

			strToken.MakeUpper();

			if(strToken != _T("RT"))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
				break;
			}

			z_pLexeme	= elementFromProduction(6)->lexeme();
			strToken	= z_pLexeme->asConstChar();

			strToken.MakeUpper();

			if( strToken != _T("ACE_1")	&&
				strToken != _T("ACE_2") &&
				strToken != _T("ACE_FOR_DWELL") )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_RTCSSTATEMENT_TIMECONSTANT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_IDENTIFIER: /*ClsYaccProdRtcsState13:*/
		// rtcsStatement -> TIMEconstant RTCS LABEL IDENTIFIER IDENTIFIER IDENTIFIER eol 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(4)->lexeme();
			CString			strToken(z_pLexeme->asConstChar());

			strToken.MakeUpper();

			if(strToken != _T("RT"))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
				break;
			}

			z_pLexeme	= elementFromProduction(5)->lexeme();
			strToken	= z_pLexeme->asConstChar();

			strToken.MakeUpper();

			if( strToken != _T("ACE_1")	&&
				strToken != _T("ACE_2") &&
				strToken != _T("ACE_FOR_DWELL") )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_SCAN_SCAN:
		// scan -> SCAN scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt eol 
		case PROD_SCAN_SCAN2:
		// scan -> SCAN scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt eol 
		case PROD_SCAN_SCAN3:
		// scan -> SCAN scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt eol 
		case PROD_SCAN_SCAN4: /*ClsYaccProdScan4:*/
		// scan -> SCAN scanOpt scanOpt scanOpt scanOpt scanOpt eol 
		{
			SSLexLexeme*	z_pLexeme	= elementFromProduction(0)->lexeme();
			POSITION		pos			= m_mapScanReqOptions.GetStartPosition();
			CString			strKey;
			CString			strItm;
			CString			strErr;

			while(pos != NULL)
			{
				m_mapScanReqOptions.GetNextAssoc(pos, strKey, strItm);
				
				if(strItm.IsEmpty())
					strErr += _T(" ") + strKey;	
			}

			if(!strErr.IsEmpty())
			{
				m_strErrorMessage  = _T("Required options for SCAN directive:");
				m_strErrorMessage += strErr; 
				parseError(*z_pLexeme);
			}

			m_mapScanReqOptions[ctstrFRAME] 	= _T("");
			m_mapScanReqOptions[ctstrPRIORITY] 	= _T("");
			m_mapScanReqOptions[ctstrMODE] 		= _T("");
			m_mapScanReqOptions[ctstrSIDE] 		= _T("");
			m_mapScanReqOptions[ctstrINSTRUMENT]= _T("");
			m_listScanOptOptions.RemoveAll();
			break;
		}

		case PROD_SCAN_TIMECONSTANT_SCAN:
		// scan -> TIMEconstant SCAN scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt eol 
		case PROD_SCAN_TIMECONSTANT_SCAN2:
		// scan -> TIMEconstant SCAN scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt eol 
		case PROD_SCAN_TIMECONSTANT_SCAN3:
		// scan -> TIMEconstant SCAN scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt eol 
		case PROD_SCAN_TIMECONSTANT_SCAN4: /*ClsYaccProdScan8:*/
		// scan -> TIMEconstant SCAN scanOpt scanOpt scanOpt scanOpt scanOpt eol 
		{
			SSLexLexeme*	z_pLexeme	= elementFromProduction(1)->lexeme();
			POSITION		pos			= m_mapScanReqOptions.GetStartPosition();
			CString			strKey;
			CString			strItm;
			CString			strErr;

			while(pos != NULL)
			{
				m_mapScanReqOptions.GetNextAssoc(pos, strKey, strItm);
				
				if(strItm.IsEmpty())
					strErr += _T(" ") + strKey;	
			}

			if(!strErr.IsEmpty())
			{
				m_strErrorMessage  = _T("Required options for SCAN directive:");
				m_strErrorMessage += strErr; 
				parseError(*z_pLexeme);
			}

			m_mapScanReqOptions[ctstrFRAME] 	= _T("");
			m_mapScanReqOptions[ctstrPRIORITY] 	= _T("");
			m_mapScanReqOptions[ctstrMODE] 		= _T("");
			m_mapScanReqOptions[ctstrSIDE] 		= _T("");
			m_mapScanReqOptions[ctstrINSTRUMENT]= _T("");
		 	m_listScanOptOptions.RemoveAll();
			break;
		}

		case PROD_STAR_STAR:
		// star -> STAR starOpt starOpt starOpt starOpt eol 
		case PROD_STAR_STAR2: /*ClsYaccProdStar2:*/
		// star -> STAR starOpt starOpt starOpt eol 
		{
			SSLexLexeme*	z_pLexeme	= elementFromProduction(0)->lexeme();
			POSITION		pos			= m_mapStarReqOptions.GetStartPosition();
			CString			strKey;
			CString			strItm;
			CString			strErr;

			while(pos != NULL)
			{
				m_mapStarReqOptions.GetNextAssoc(pos, strKey, strItm);
				
				if(strItm.IsEmpty())
					strErr += _T(" ") + strKey;	
			}

			if(!strErr.IsEmpty())
			{
				m_strErrorMessage  = _T("Required options for STAR directive:");
				m_strErrorMessage += strErr; 
				parseError(*z_pLexeme);
			}

			m_mapStarReqOptions[ctstrMAX] 		= _T("");
			m_mapStarReqOptions[ctstrDUR] 		= _T("");
			m_mapStarReqOptions[ctstrINSTRUMENT]= _T("");
		 	m_listStarOptOptions.RemoveAll();
			break;
		}

		case PROD_STAR_TIMECONSTANT_STAR:
		// star -> TIMEconstant STAR starOpt starOpt starOpt starOpt eol 
		case PROD_STAR_TIMECONSTANT_STAR2: /*ClsYaccProdStar4:*/
		// star -> TIMEconstant STAR starOpt starOpt starOpt eol 
		{
			SSLexLexeme*	z_pLexeme	= elementFromProduction(1)->lexeme();
			POSITION		pos			= m_mapStarReqOptions.GetStartPosition();
			CString			strKey;
			CString			strItm;
			CString			strErr;

			while(pos != NULL)
			{
				m_mapStarReqOptions.GetNextAssoc(pos, strKey, strItm);
				
				if(strItm.IsEmpty())
					strErr += _T(" ") + strKey;	
			}

			if(!strErr.IsEmpty())
			{
				m_strErrorMessage  = _T("Required options for STAR directive:");
				m_strErrorMessage += strErr; 
				parseError(*z_pLexeme);
			}

			m_mapStarReqOptions[ctstrMAX] 		= _T("");
			m_mapStarReqOptions[ctstrDUR] 		= _T("");
			m_mapStarReqOptions[ctstrINSTRUMENT]= _T("");
		 	m_listStarOptOptions.RemoveAll();
			break;
		}

		case PROD_SCANOPT_FRAME_EQ_IDENTIFIER: /*ClsYaccProdScanOpt1:*/
		// scanOpt -> FRAME = IDENTIFIER 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString			strItem;

			m_mapScanReqOptions.Lookup(ctstrFRAME, strItem);

			if(!strItem.IsEmpty())
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_mapScanReqOptions.SetAt(ctstrFRAME, _T("R"));

			break;
		}

		case PROD_SCANOPT_PRIORITY_EQ_IDENTIFIER: /*ClsYaccProdScanOpt2:*/
		// scanOpt -> PRIORITY = IDENTIFIER 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString			strToken;
			CString			strItem;

			m_mapScanReqOptions.Lookup(ctstrPRIORITY, strItem);

			if(!strItem.IsEmpty())
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_mapScanReqOptions.SetAt(ctstrPRIORITY, _T("R"));

			z_pLexeme	= elementFromProduction(2)->lexeme();
			strToken	= z_pLexeme->asConstChar();
			strToken.MakeUpper();

			if( strToken != ctstrNORMAL		&&
				strToken != ctstrPRIORITY_1	&&
				strToken != ctstrPRIORITY_2 )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_SCANOPT_MODE_EQ_IDENTIFIER: /*ClsYaccProdScanOpt3: */
		// scanOpt -> MODE = IDENTIFIER 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString			strToken;
			CString 		strItem;

			m_mapScanReqOptions.Lookup(ctstrMODE, strItem);

			if(!strItem.IsEmpty())
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_mapScanReqOptions.SetAt(ctstrMODE, _T("R"));

			z_pLexeme	= elementFromProduction(2)->lexeme();
			strToken	= z_pLexeme->asConstChar();
			strToken.MakeUpper();

			if( strToken != ctstrMODE_0		&& 
				strToken != ctstrMODE_1		&&
				strToken != ctstrSINGLE_0_1 &&
				strToken != ctstrSINGLE_0_2 &&
				strToken != ctstrSINGLE_0_4 &&
				strToken != ctstrDOUBLE_0_1 )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_SCANOPT_EXECUTE_EQ_IDENTIFIER: /*ClsYaccProdScanOpt4:*/
		// scanOpt -> EXECUTE = IDENTIFIER 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString			strToken;

			if(m_listScanOptOptions.Find(ctstrEXECUTE) != NULL)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_listScanOptOptions.AddTail(ctstrEXECUTE);

			z_pLexeme	= elementFromProduction(2)->lexeme();
			strToken	= z_pLexeme->asConstChar();
			strToken.MakeUpper();

			if(strToken != ctstrLOAD_AND_EXEC && strToken != ctstrLOAD_REG_ONLY)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_SCANOPT_BBCAL_EQ_IDENTIFIER: /*ClsYaccProdScanOpt5:*/
		// scanOpt -> BBCAL = IDENTIFIER 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString			strToken;

			if(m_listScanOptOptions.Find(ctstrBBCAL) != NULL)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_listScanOptOptions.AddTail(ctstrBBCAL);

			z_pLexeme	= elementFromProduction(2)->lexeme();
			strToken	= z_pLexeme->asConstChar();
			strToken.MakeUpper();

			if(strToken != ctstrDO_NOT_SUPP && strToken != ctstrSUPP)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_SCANOPT_SIDE_EQ_IDENTIFIER: /*ClsYaccProdScanOpt6:*/
		// scanOpt -> SIDE = IDENTIFIER 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString			strToken;
			CString 		strItem;

			m_mapScanReqOptions.Lookup(ctstrSIDE, strItem);

			if(!strItem.IsEmpty())
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_mapScanReqOptions.SetAt(ctstrSIDE, _T("R"));

			z_pLexeme	= elementFromProduction(2)->lexeme();
			strToken	= z_pLexeme->asConstChar();
			strToken.MakeUpper();

			if( strToken != ctstrEAST && 
				strToken != ctstrWEST &&
				strToken != ctstrOATS )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_SCANOPT_INSTRUMENT_EQ_IDENTIFIER: /*ClsYaccProdScanOpt7:*/
		// scanOpt -> INSTRUMENT = IDENTIFIER 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString			strToken;
			CString 		strItem;

			m_mapScanReqOptions.Lookup(ctstrINSTRUMENT, strItem);

			if(!strItem.IsEmpty())
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_mapScanReqOptions.SetAt(ctstrINSTRUMENT, _T("R"));

			z_pLexeme	= elementFromProduction(2)->lexeme();
			strToken	= z_pLexeme->asConstChar();
			strToken.MakeUpper();

			if(strToken != ctstrIMGR && strToken != ctstrSDR)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_SCANOPT_REPEAT_EQ_INTEGERCONSTANT:
		// scanOpt -> REPEAT = INTEGERconstant 
		case PROD_SCANOPT_REPEAT_EQ_IDENTIFIER: /*ClsYaccProdScanOpt9:*/
		// scanOpt -> REPEAT = IDENTIFIER 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();

			if(m_listScanOptOptions.Find(ctstrREPEAT) != NULL)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_listScanOptOptions.AddTail(ctstrREPEAT);

			break;
		}

		case PROD_STAROPT_INSTRUMENT_EQ_IDENTIFIER: /*ClsYaccProdStarOpt1:*/
		// starOpt -> INSTRUMENT = IDENTIFIER 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString			strToken;
			CString 		strItem;

			m_mapStarReqOptions.Lookup(ctstrINSTRUMENT, strItem);

			if(!strItem.IsEmpty())
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_mapStarReqOptions.SetAt(ctstrINSTRUMENT, _T("R"));

			z_pLexeme	= elementFromProduction(2)->lexeme();
			strToken	= z_pLexeme->asConstChar();
			strToken.MakeUpper();

			if(strToken != ctstrIMGR && strToken != ctstrSDR)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_STAROPT_LAST_STAR_CLASS_EQ_IDENTIFIER: /*ClsYaccProdStarOpt2:*/
		// starOpt -> LAST_STAR_CLASS = IDENTIFIER 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString			strToken;

			if(m_listStarOptOptions.Find(ctstrLAST_STAR_CLASS) != NULL)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_listStarOptOptions.AddTail(ctstrLAST_STAR_CLASS);

			z_pLexeme	= elementFromProduction(2)->lexeme();
			strToken	= z_pLexeme->asConstChar();
			strToken.MakeUpper();

			if(strToken != ctstrSENSE && strToken != ctstrSEQUENCE)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR16;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_STAROPT_DURATION_EQ_TIMESTRING: /*ClsYaccProdStarOpt3:*/
		// starOpt -> DURATION = " TIMEconstant " 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString 		strItem;

			m_mapStarReqOptions.Lookup(ctstrDUR, strItem);

			if(!strItem.IsEmpty())
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_mapStarReqOptions.SetAt(ctstrDUR, _T("R"));

			break;
		}

		case PROD_STAROPT_MAX_EQ_INTEGERCONSTANT: /*ClsYaccProdStarOpt4: */
		// starOpt -> MAX = INTEGERconstant 
		{
			SSLexLexeme*	z_pLexeme = elementFromProduction(0)->lexeme();
			CString			strItem;

			m_mapStarReqOptions.Lookup(ctstrMAX, strItem);

			if(!strItem.IsEmpty())
			{
				m_nErrorNum = IDS_SYNTAX_ERROR15;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR15);
				parseError(*z_pLexeme);
				break;
			}

			m_mapStarReqOptions.SetAt(ctstrMAX, _T("R"));

			break;
		}

		default:/*Reduce*/
			break;
	}

	return stackElement();
}

#endif











































