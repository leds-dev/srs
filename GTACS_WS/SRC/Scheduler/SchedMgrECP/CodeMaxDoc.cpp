/***************************
//////////////////////////////////////////////////////////////////////
// CodeMaxDoc.cpp : implementation file
//
// (c) 2001 Frederick J. Shaw  
// PDL updated                                    //
//////////////////////////////////////////////////////////////////////
***************************/


#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "CodeMaxDoc.h"
#include "NewDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCodeMaxDocEx

IMPLEMENT_DYNCREATE(CCodeMaxDocEx, CDocument)

BEGIN_MESSAGE_MAP(CCodeMaxDocEx, CDocument)
	//{{AFX_MSG_MAP(CCodeMaxDocEx)
	ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_REDO, OnUpdateEditRedo)
	ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
	ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)	
	ON_COMMAND(ID_EDIT_FIND, OnEditFind)
	ON_UPDATE_COMMAND_UI(ID_EDIT_FIND, OnUpdateEditFind)
	ON_COMMAND(ID_EDIT_REPLACE, OnEditReplace)
	ON_UPDATE_COMMAND_UI(ID_EDIT_REPLACE, OnUpdateEditReplace)
	ON_COMMAND(ID_EDIT_TOGGLEWHITESPACE, OnEditToggleWhitespace)
	ON_UPDATE_COMMAND_UI(ID_EDIT_TOGGLEWHITESPACE, OnUpdateEditToggleWhitespace)
	ON_COMMAND(ID_EDIT_MAKEUPPERCASE, OnEditMakeUppercase)
	ON_UPDATE_COMMAND_UI(ID_EDIT_MAKEUPPERCASE, OnUpdateEditMakeUppercase)
	ON_COMMAND(ID_EDIT_MAKELOWERCASE, OnEditMakeLowercase)
	ON_UPDATE_COMMAND_UI(ID_EDIT_MAKELOWERCASE, OnUpdateEditMakeLowercase)
	ON_COMMAND(ID_EDIT_CLEARHIGHTLIGHT, OnEditClearHightlight)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CLEARHIGHTLIGHT, OnUpdateEditClearHightlight)
	ON_COMMAND(ID_EDIT_PROPERTIES, OnEditProperties)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PROPERTIES, OnUpdateEditProperties)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateFileNew)
	//}}AFX_MSG_MAP
	ON_NOTIFY(CMN_PROPSCHANGE, IDC_EDIT, OnPropsChange)	
	ON_NOTIFY(CMN_MODIFIEDCHANGE, IDC_EDIT, OnFileModified)
	ON_NOTIFY(CMN_KEYPRESS, IDC_EDIT, OnFileKeyPressed)
END_MESSAGE_MAP()

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxDocEx()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CCodeMaxDocEx::CCodeMaxDocEx()
{
}


/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		OnNewDocument()
//	Description :	 
//					
//					
//	Return :		BOOL	
//                        TRUE - SUCCESS
//                        FALSE - ERROR
//                  
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
BOOL CCodeMaxDocEx::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	m_cTimeLastMod = 0;
	return TRUE;
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		~CCodeMaxDocEx()
//	Description :	Destructor 
//					
//					
//	Return :		void	
//                       nothing
//                       
//                  
//	Parameters :	
//					void 
//                       nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CCodeMaxDocEx::~CCodeMaxDocEx()
{
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		OnFileNew()
//	Description :	This routine opens a new document. 
//					The type of document openned is based on the what
//                  the user has selected.
//					
//	Return :		void	
//                       nothing
//                       
//                  
//	Parameters :	
//					void 
//                       nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CCodeMaxDocEx::OnFileNew() 
{
	CNewDialog dlg;
	if (dlg.DoModal() == IDOK)
	{
		//Open the first document subtype of the type selected
		GetAppDocTemplate(dlg.m_nTypeSelected)->OpenDocumentFile(NULL);	
	}
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		OnUpdateFileNew()
//	Description :	This routine enables the new doc menu selection. 
//					
//                  				
//	Return :		void	
//                       nothing
//                       
//                  
//	Parameters :	
//					CCmdUI * - Pointer to the dialog window.  
//                       
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CCodeMaxDocEx::OnUpdateFileNew(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

/********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/5/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxDocEx::OnOpenDocument
//	Description :	This routine will open a CodeMax derived file.	
//
//	Return :		BOOL	-	Open Status
//                       TRUE   -   success
//                       FALSE  -   failure         
//
//	Parameters :
//			LPCTSTR lpszPathName	-	Name of the file to open
//
//	Note :			This routine calls OnOpenDocument to mark the
//					document as being clean.  It then tries to open
//					the document and serialize it in.  The Serialize
//					function is just a stub so nothing really happens.
//					However, if there are any errors, OnOpenDocument
//					takes care of them for us and returns FALSE.  If
//					everything was OK, the was send the CMM_OPENFILE
//					message to the CodeMax control to really read in
//					the file.  Finally we save away the last
//					modification time.
//////////////////////////////////////////////////////////////////////
****************************/
BOOL CCodeMaxDocEx::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;
	
	SendMessage(m_wndEdit, CMM_OPENFILE, 0, (LPARAM)lpszPathName);
	
	CFileFind finder;
	if (finder.FindFile(lpszPathName))
	{
		if (finder.FindNextFile() >= 0)
		{
			finder.GetLastWriteTime(m_cTimeLastMod);
		}
	}
	
	return TRUE;
}

/**********************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/5/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxDocEx::OnSaveDocument
//	Description :	This routine is called to cave a CodeMax derived
//					file
//	Return :		BOOL	   - Status
//                       TRUE  - SUCCESS
//                       FALSE - FAIL
//
//	Parameters :
//			LPCTSTR lpszPathName	-	Name of file to save.  If
//										this is a Save operation, the
//										name is the document's name.
//										If this is a Save As
//										operation, then this is the
//										name entered into the File
//										Save dialog box.
//	Note :			This routine is called whenever the user clicks
//					on Save or Save As.  We do not call
//					CDocument::OnSaveDocument() since this will just
//					serialize nothing out to the file and leave us
//					with an empty file.  Instead we call the
//					CodeMax's SaveFile function to do the real work.
//					It takes care of error conditions for us!  If it
//					returns CME_SUCCESS, then we update the last
//					modification time of the document and clear the
//					document's dirty flag.
/////////////////////////////////////////////////////////////////////
******************************/
BOOL CCodeMaxDocEx::OnSaveDocument(LPCTSTR lpszPathName) 
{
	m_wndEdit.SetHighlightedLine(-1);

	// If the save name doesn't match the original file name then
	// a save as was done and we need to refresh the workspace bar
	BOOL bRefreshNeeded = !(GetPathName().CompareNoCase(lpszPathName) == 0);
	
	CME_CODE rtnStatus = m_wndEdit.SaveFile(lpszPathName, TRUE);
	if (rtnStatus == CME_SUCCESS)
	{
		CFileFind finder;
		if (finder.FindFile(lpszPathName))
		{
			if (finder.FindNextFile() >= 0)
			{
				finder.GetLastWriteTime(m_cTimeLastMod);
			}
		}
		SetModifiedFlag(FALSE);
		if (bRefreshNeeded)
		{
			GetAppWorkspaceBar()->m_wndMasterTreeCtrl.UpdateTree();
			GetAppWorkspaceBar()->m_wndDailyTreeCtrl.UpdateTree();
		}
		return TRUE;
	}
	else
	{
		LPVOID lpMsgBuf;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					  FORMAT_MESSAGE_FROM_SYSTEM | 
					  FORMAT_MESSAGE_IGNORE_INSERTS,
					  NULL,
					  GetLastError(),
					  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					  (LPTSTR) &lpMsgBuf,
					  0,
					  NULL);
		MessageBox(NULL, (LPCTSTR)lpMsgBuf, _T("Error"), MB_OK | MB_ICONINFORMATION);
		LocalFree(lpMsgBuf);
		return FALSE;
	}
}

/*******************************
  ======================================================================
	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnPropsChange
	Description :	OnPropsChange is called whenever the CodeMax control
					properties are changed.  The properties are then 
					stored into the registry object and written out to the
					registry.
	Return :		
		void		-	Nothing.
	Parameters :	
		NMHDR* pNMHDR		-	Not used.
		LRESULT* pResult	-	Not used.
	Note :			
======================================================================
*********************************/
void CCodeMaxDocEx::OnPropsChange(NMHDR* pNMHDR, LRESULT* pResult)
{
	((CSchedMgrECPApp*)AfxGetApp())->SaveCodeMaxProfile(m_wndEdit);
}

/********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/5/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		OnFileModified ()
//	Description :	This routine maintaines the modification status of 
//                  the file.	
//
//	Return :		Void
//                      nothing
//                                
//	Parameters :
//					NMHDR * - Not used
//					LRESULT * Routine's return status	
//
//	Note :			
//					
//////////////////////////////////////////////////////////////////////
****************************/
void CCodeMaxDocEx::OnFileModified (NMHDR* pNMHDR, LRESULT* pResult)
{
	// Send the command CMM_ISMODIFIED to the CodeMax control.  This will tell us if
	// the control has changed or has been restored back to it original state.  This
	// modified state is correctly maintained when Undo/Redo operation are invoked
	// upon the buffer.
	if (SendMessage (m_wndEdit, CMM_ISMODIFIED, 0, 0))
		SetModifiedFlag(TRUE);
	else
		SetModifiedFlag(FALSE);

	SetTitle(_T(""));

	*pResult = FALSE;
}

/********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/5/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		OnFileKeyPressed ()
//	Description :	  
//                  	
//
//	Return :		Void
//                      nothing
//                                
//	Parameters :	
//					NMHDR * - Not used
//					LRESULT * Routine's return status	
//
//	Note :			
//					
//////////////////////////////////////////////////////////////////////
****************************/
void CCodeMaxDocEx::OnFileKeyPressed (NMHDR* pNMHDR, LRESULT* pResult)
{
	m_wndEdit.SetHighlightedLine(-1);
	*pResult = FALSE;
}


/********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/5/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		OnFileKeyPressed ()
//	Description :	  
//                  	
//
//	Return :		Void
//                      nothing
//                                
//	Parameters :	
//					NMHDR * - Not used
//					LRESULT * Routine's return status	
//
//	Note :			
//					
//////////////////////////////////////////////////////////////////////
****************************/
void CCodeMaxDocEx::SetTitle(LPCTSTR lpszTitle) 
{
	static CString strTempFilename = _T("");  //used for a new file that has not yet been saved
	CString strFilename;
	COXUNC strPath = GetPathName();

	//If this is a new file then save away the MFC generated name
	if (strPath.IsEmpty())
	{
		if (strTempFilename.IsEmpty())
			strTempFilename = lpszTitle;
		strFilename = strTempFilename;
	}
	else
		strFilename = GetAppRegMisc()->m_bShowExt ? strPath.File() : strPath.Base();
	
	if (SendMessage (m_wndEdit, CMM_ISMODIFIED, 0, 0))
		strFilename += _T(" *");
		
	CDocument::SetTitle(strFilename);
}

#ifdef _DEBUG
void CCodeMaxDocEx::AssertValid() const
{
	CDocument::AssertValid();
}

void CCodeMaxDocEx::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCodeMaxDocEx serialization
/********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/5/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		OnFileKeyPressed ()
//	Description :	  
//                  	
//
//	Return :		Void
//                      nothing
//                                
//	Parameters :	
//					NMHDR * - Not used
//					LRESULT * Routine's return status	
//
//	Note :			
//					
//////////////////////////////////////////////////////////////////////
****************************/
void CCodeMaxDocEx::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/**************************
======================================================================
	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditRedo
	Description :	Called to Redo an edit.
	Return :		
		void		-	Nothing.
	Parameters :	None.
	Note :			None.
======================================================================
**************************/
void CCodeMaxDocEx::OnEditRedo() 
{
	m_wndEdit.Redo();
}
/****************************
  ======================================================================
	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditRedo
	Description :	Called by the framework to determine if the Redo
					resource should be enabled.
	Return :		
		void		-	Nothing.
	Parameters :	
		CCmdUI* pCmdUI	-	Pointer to a CmdUI Object.
	Note :			None.
======================================================================
****************************/
void CCodeMaxDocEx::OnUpdateEditRedo(CCmdUI* pCmdUI) 
{
	if (m_wndEdit.CanRedo())
		pCmdUI->Enable(true);
	else
		pCmdUI->Enable(false);	
}

/***************************
  ======================================================================
	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditUndo
	Description :	Called to Undo an edit.
	Return :		
		void		-	Nothing.
	Parameters :	None.
	Note :			None.
======================================================================
***************************/
void CCodeMaxDocEx::OnEditUndo() 
{
	m_wndEdit.Undo();
}

/**************************
  ======================================================================
	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditUndo
	Description :	Called by the framework to determine if the Undo
					resource should be enabled.
	Return :		
		void		-	Nothing.
	Parameters :	
		CCmdUI* pCmdUI	-	Pointer to a CmdUI Object.
	Note :			None.
  ======================================================================
**************************/
void CCodeMaxDocEx::OnUpdateEditUndo(CCmdUI* pCmdUI) 
{
	if (m_wndEdit.CanUndo())
		pCmdUI->Enable(true);
	else
		pCmdUI->Enable(false);
}

/*************************
  ======================================================================
	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditCut
	Description :	Called to Cut.
	Return :		
		void		-	Nothing.
	Parameters :	None.
	Note :			None.
  ======================================================================
*************************/
void CCodeMaxDocEx::OnEditCut() 
{
	m_wndEdit.Cut();
}

/**************************
  ======================================================================
	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditCut
	Description :	Called by the framework to determine if the Cut
					resource should be enabled.
	Return :		
		void		-	Nothing.
	Parameters :	
		CCmdUI* pCmdUI	-	Pointer to a CmdUI Object.
	Note :			None.
  ======================================================================
***************************/
void CCodeMaxDocEx::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	if (m_wndEdit.CanCut())
		pCmdUI->Enable(true);
	else
		pCmdUI->Enable(false);
}

/**************************
  ======================================================================
	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditPaste
	Description :	Called to Paste.
	Return :		
		void		-	Nothing.
	Parameters :	None.
	Note :			None.
  ======================================================================
***************************/
void CCodeMaxDocEx::OnEditPaste() 
{
	m_wndEdit.Paste();
}

/***************************
  ======================================================================
	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditPaste
	Description :	Called by the framework to determine if the Paste
					resource should be enabled.
	Return :		
		void		-	Nothing.
	Parameters :	
		CCmdUI* pCmdUI	-	Pointer to a CmdUI Object.
	Note :			None.
  ======================================================================
****************************/
void CCodeMaxDocEx::OnUpdateEditPaste(CCmdUI* pCmdUI) 
{
	if (m_wndEdit.CanPaste())
		pCmdUI->Enable(true);
	else
		pCmdUI->Enable(false);
}

/***************************
  ======================================================================
	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditCopy
	Description :	Called to Copy.
	Return :		
		void		-	Nothing.
	Parameters :	None.
	Note :			None.
  ======================================================================
****************************/
void CCodeMaxDocEx::OnEditCopy() 
{
	m_wndEdit.Copy();
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditCopy
	Description :	Called by the framework to determine if the Copy
					resource should be enabled.
	Return :		
		void		-	Nothing.
	Parameters :	
		CCmdUI* pCmdUI	-	Pointer to a CmdUI Object.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	if (m_wndEdit.CanCopy())
		pCmdUI->Enable(true);
	else
		pCmdUI->Enable(false);
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditFind()
	Description :	This routine executes the find functions.
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnEditFind() 
{
	m_wndEdit.ExecuteCmd(CMD_FIND);
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditFind(CCmdUI* pCmdUI)
	Description :	Called by the framework to determine if the Edit
					resource should be enabled.
					
	Return :		
		void		-	Nothing.
	Parameters :	
		CCmdUI *	-	Pointer to a CCmdUI object.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnUpdateEditFind(CCmdUI* pCmdUI) 
{
	int nLineCount = m_wndEdit.GetLineCount();

	if (nLineCount != 0)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditReplace()
	Description :	This routine executes the find and replace function.
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnEditReplace() 
{
	m_wndEdit.ExecuteCmd(CMD_FINDREPLACE);
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditReplace()
	Description :	This routine toggles the replace menu option.
					
	Return :		
		void		-	Nothing.
	Parameters :	
		CCmdUI *	-	Pointer to CCmdUI object.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnUpdateEditReplace(CCmdUI* pCmdUI) 
{
	BOOL bCanEdit = !m_wndEdit.IsReadOnly();
	int nLineCount = m_wndEdit.GetLineCount();

	if ((nLineCount != 0) && (bCanEdit))
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);			
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditToggleWhitespace()
	Description :	This routine toggles whether whitespace is displayed.	
					
	Return :		
		void		-	Nothing.

	Parameters :	
		void		-	Nothing.

	Note :				None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnEditToggleWhitespace() 
{
	m_wndEdit.EnableWhitespaceDisplay(!m_wndEdit.IsWhitespaceDisplayEnabled());
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditToggleWhitespace()
	Description :	This routine toggles the whitespace control. 
					
	Return :		
		void		-	Nothing.
	Parameters :	
		CCmdUI *	-	Pointer to a CCmdUI object.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnUpdateEditToggleWhitespace(CCmdUI* pCmdUI) 
{
	if (m_wndEdit.IsWhitespaceDisplayEnabled())
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditMakeUppercase()
	Description :	This routine converst the highlight text to uppercase.
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnEditMakeUppercase() 
{
	m_wndEdit.ExecuteCmd(CMD_UPPERCASESELECTION);
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditMakeUppercase()
	Description :	This routine toggles the enable status for 
					the make uppercase menu selection.
					
	Return :		
		void		-	Nothing.

	Parameters :	
		CCmdUI* pCmdUI	-	Pointer to a CmdUI object.
	
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnUpdateEditMakeUppercase(CCmdUI* pCmdUI) 
{
	CM_RANGE range;
	m_wndEdit.GetSel(&range);
	BOOL bEmptySel = ((range.posStart.nLine == range.posEnd.nLine) &&
					  (range.posStart.nCol == range.posEnd.nCol)) ? TRUE : FALSE;
	
	if (!bEmptySel)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);	
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditMakeLowercase()
	Description :	This routine makes the hightlighted text uppercase.
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnEditMakeLowercase() 
{
	m_wndEdit.ExecuteCmd(CMD_LOWERCASESELECTION);
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditMakeLowercase()
	Description :	This routines enables the make lowercase menu options.
					
	Return :		
		void		-	Nothing.
	Parameters :	
		CCmdUI* pCmdUI	-	Pointer to a CmdUI object.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnUpdateEditMakeLowercase(CCmdUI* pCmdUI) 
{
	CM_RANGE range;
	m_wndEdit.GetSel(&range);
	BOOL bEmptySel = ((range.posStart.nLine == range.posEnd.nLine) &&
					  (range.posStart.nCol == range.posEnd.nCol)) ? TRUE : FALSE;
	
	if (!bEmptySel)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);	
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditClearHightlight()
	Description :	This routine clears the highlight. 
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnEditClearHightlight() 
{
	m_wndEdit.SetHighlightedLine(-1);
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditClearHightlight()
	Description :	This routine toggles the enable status for
					the copy/paste menu options. 
					
					
	Return :		
		void		-	Nothing.
	Parameters :	
		CCmdUI* pCmdUI	-	Pointer to a CmdUI object.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnUpdateEditClearHightlight(CCmdUI* pCmdUI) 
{
	if (m_wndEdit.GetHighlightedLine() != -1)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnEditProperties()
	Description :	This routine allows the user to edit the properties
					options
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnEditProperties() 
{
	m_wndEdit.ExecuteCmd(CMD_PROPERTIES);
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCodeMaxDocEx::OnUpdateEditProperties()
	Description :	This routine enables the properties menu selection.
					
	Return :		
		void		-	Nothing.

	Parameters :	
		CCmdUI* pCmdUI - pointer to a CmdUI object.
	
	Note :			None.
  ======================================================================
*****************************/
void CCodeMaxDocEx::OnUpdateEditProperties(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);	
}

/////////////////////////////////////////////////////////////////////////////
// CCodeMaxDocEx commands

//////////////////////////////////////////////////////////////////////
// CSTOLDoc Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(CSTOLDoc, CCodeMaxDocEx)

BEGIN_MESSAGE_MAP(CSTOLDoc, CCodeMaxDocEx)
	//{{AFX_MSG_MAP(CSTOLDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CSTOLDoc::CSTOLDoc()
	Description :	Constructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CSTOLDoc::CSTOLDoc()
{
}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CSTOLDoc::~CSTOLDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CSTOLDoc::~CSTOLDoc()
{
}


IMPLEMENT_DYNCREATE(CRTCSCmdDoc, CCodeMaxDocEx)
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CRTCSCmdDoc::CRTCSCmdDoc()
	Description :	
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CRTCSCmdDoc::CRTCSCmdDoc()
{

}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CRTCSCmdDoc::~CRTCSCmdDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CRTCSCmdDoc::~CRTCSCmdDoc()
{

}

IMPLEMENT_DYNCREATE(CSTOLCmdDoc, CCodeMaxDocEx)
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CSTOLCmdDoc::CSTOLCmdDoc()
	Description :	
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CSTOLCmdDoc::CSTOLCmdDoc()
{

}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CSTOLCmdDoc::~CSTOLCmdDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CSTOLCmdDoc::~CSTOLCmdDoc()
{

}


IMPLEMENT_DYNCREATE(CPaceCmdDoc, CCodeMaxDocEx)
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CPaceCmdDoc::CPaceCmdDoc()
	Description :	Constructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CPaceCmdDoc::CPaceCmdDoc()
{

}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CPaceCmdDoc::~CPaceCmdDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CPaceCmdDoc::~CPaceCmdDoc()
{

}

IMPLEMENT_DYNCREATE(CRTCSSetDoc, CCodeMaxDocEx)
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CRTCSSetDoc::CRTCSSetDoc()
	Description :	Constructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CRTCSSetDoc::CRTCSSetDoc()
{

}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CRTCSSetDoc::~CRTCSSetDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CRTCSSetDoc::~CRTCSSetDoc()
{

}

IMPLEMENT_DYNCREATE(CRTCSDoc, CCodeMaxDocEx)
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CRTCSDoc::CRTCSDoc()
	Description :	Constructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CRTCSDoc::CRTCSDoc()
{

}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CRTCSDoc::~CRTCSDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CRTCSDoc::~CRTCSDoc()
{

}

IMPLEMENT_DYNCREATE(CCLSDoc, CCodeMaxDocEx)
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCLSDoc::CCLSDoc()
	Description :	Constructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CCLSDoc::CCLSDoc()
{

}

/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CCLSDoc::~CCLSDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CCLSDoc::~CCLSDoc()
{

}

IMPLEMENT_DYNCREATE(CRptDoc, CCodeMaxDocEx)
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CRptDoc::CRptDoc()
	Description :	Constructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CRptDoc::CRptDoc()
{

}
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CRptDoc::~CRptDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CRptDoc::~CRptDoc()
{

}

IMPLEMENT_DYNCREATE(CLisDoc, CCodeMaxDocEx)
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CLisDoc::CLisDoc()
	Description :	Constructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CLisDoc::CLisDoc()
{

}
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CLisDoc::~CLisDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CLisDoc::~CLisDoc()
{

}

IMPLEMENT_DYNCREATE(CIMCDoc, CCodeMaxDocEx)
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CIMCDoc::CIMCDoc()
	Description :	Constructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CIMCDoc::CIMCDoc()
{

}
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CIMCDoc::~CIMCDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CIMCDoc::~CIMCDoc()
{

}

IMPLEMENT_DYNCREATE(CBinDoc, CCodeMaxDocEx)
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CBinDoc::CBinDoc()
	Description :	Constructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CBinDoc::CBinDoc()
{

}
/*****************************
  ======================================================================
  	Author :Don Sanders			Date : 12/30/00			version 1.0
  ======================================================================
	Function :		CBinDoc::~CBinDoc()
	Description :	Destructor
					
	Return :		
		void		-	Nothing.
	Parameters :	
		void		-	Nothing.
	Note :			None.
  ======================================================================
*****************************/
CBinDoc::~CBinDoc()
{

}
