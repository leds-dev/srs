#if !defined(AFX_CODEMAXDOC_H__C4F2DA92_D680_11D4_800D_00609704053C__INCLUDED_)
#define AFX_CODEMAXDOC_H__C4F2DA92_D680_11D4_800D_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CodeMaxDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCodeMaxDocEx document

class CCodeMaxDocEx : public CDocument
{
protected:
	CCodeMaxDocEx();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCodeMaxDocEx)

// Attributes
public:

// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCodeMaxDocEx)
	public:
	virtual void Serialize(CArchive& ar);   // overridden for document i/o
	virtual void OnPropsChange(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	virtual void OnFileModified(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnFileKeyPressed(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void SetTitle(LPCTSTR lpszTitle);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL
// Implementation
public:
	virtual ~CCodeMaxDocEx();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
	CCodeMaxCtrl	m_wndEdit;
	CTime			m_cTimeLastMod;
	// Generated message map functions
protected:
	//{{AFX_MSG(CCodeMaxDocEx)
	afx_msg void OnFileNew();
	afx_msg void OnUpdateFileNew(CCmdUI* pCmdUI);	
	afx_msg void OnEditRedo();
	afx_msg void OnUpdateEditRedo(CCmdUI* pCmdUI);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);	
	afx_msg void OnEditFind();
	afx_msg void OnUpdateEditFind(CCmdUI* pCmdUI);
	afx_msg void OnEditReplace();
	afx_msg void OnUpdateEditReplace(CCmdUI* pCmdUI);
	afx_msg void OnEditToggleWhitespace();
	afx_msg void OnUpdateEditToggleWhitespace(CCmdUI* pCmdUI);
	afx_msg void OnEditMakeUppercase();
	afx_msg void OnUpdateEditMakeUppercase(CCmdUI* pCmdUI);
	afx_msg void OnEditMakeLowercase();
	afx_msg void OnUpdateEditMakeLowercase(CCmdUI* pCmdUI);
	afx_msg void OnEditClearHightlight();
	afx_msg void OnUpdateEditClearHightlight(CCmdUI* pCmdUI);
	afx_msg void OnEditProperties();
	afx_msg void OnUpdateEditProperties(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CSTOLDoc : public CCodeMaxDocEx
{
protected:
	CSTOLDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSTOLDoc)

public:
	virtual ~CSTOLDoc();
protected:
	//{{AFX_MSG(CSTOLDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CRTCSCmdDoc : public CCodeMaxDocEx  
{
public:
	CRTCSCmdDoc();
	DECLARE_DYNCREATE(CRTCSCmdDoc)

	virtual ~CRTCSCmdDoc();
};

class CSTOLCmdDoc : public CCodeMaxDocEx  
{
public:
	CSTOLCmdDoc();
	DECLARE_DYNCREATE(CSTOLCmdDoc)

	virtual ~CSTOLCmdDoc();
};


class CPaceCmdDoc : public CCodeMaxDocEx  
{
public:
	CPaceCmdDoc();
	DECLARE_DYNCREATE(CPaceCmdDoc)

	virtual ~CPaceCmdDoc();
};

class CRTCSSetDoc : public CCodeMaxDocEx  
{
public:
	CRTCSSetDoc();
	DECLARE_DYNCREATE(CRTCSSetDoc)

	virtual ~CRTCSSetDoc();
};

class CRTCSDoc : public CCodeMaxDocEx  
{
public:
	CRTCSDoc();
	DECLARE_DYNCREATE(CRTCSDoc)

	virtual ~CRTCSDoc();
};

class CCLSDoc : public CCodeMaxDocEx  
{
public:
	CCLSDoc();
	DECLARE_DYNCREATE(CCLSDoc)

	virtual ~CCLSDoc();
};

class CRptDoc : public CCodeMaxDocEx  
{
public:
	CRptDoc();
	DECLARE_DYNCREATE(CRptDoc)

	virtual ~CRptDoc();
};

class CLisDoc : public CCodeMaxDocEx  
{
public:
	CLisDoc();
	DECLARE_DYNCREATE(CLisDoc)

	virtual ~CLisDoc();
};

class CIMCDoc : public CCodeMaxDocEx  
{
public:
	CIMCDoc();
	DECLARE_DYNCREATE(CIMCDoc)

	virtual ~CIMCDoc();
};

class CBinDoc : public CCodeMaxDocEx  
{
public:
	CBinDoc();
	DECLARE_DYNCREATE(CBinDoc)

	virtual ~CBinDoc();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CODEMAXDOC_H__C4F2DA92_D680_11D4_800D_00609704053C__INCLUDED_)
