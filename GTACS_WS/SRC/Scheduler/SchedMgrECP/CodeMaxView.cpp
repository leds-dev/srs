/***************************
//////////////////////////////////////////////////////////////////////
// CodeMaxView.cpp : implementation file
//
// (c) 2001 Frederick J. Shaw  
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
***************************/


#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "CodeMaxView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCodeMaxViewEx

IMPLEMENT_DYNCREATE(CCodeMaxViewEx, CView)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxViewEx::CCodeMaxViewEx()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CCodeMaxViewEx::CCodeMaxViewEx()
{
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxViewEx::~CCodeMaxViewEx()
//	Description :	Class destructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CCodeMaxViewEx::~CCodeMaxViewEx()
{
}

BEGIN_MESSAGE_MAP(CCodeMaxViewEx, CView)
	//{{AFX_MSG_MAP(CCodeMaxViewEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SETFOCUS()
	ON_COMMAND(ID_FILE_PRINT_DIRECT, OnFilePrint)	
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_DIRECT, OnUpdateFilePrint)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_PROMPTRELOADDOC, PromptReloadDoc)
	ON_NOTIFY(NM_RCLICK, IDC_EDIT, OnRClick)	
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCodeMaxViewEx drawing
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxViewEx::OnDraw()
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CCodeMaxViewEx::OnDraw(CDC* pDC)
{
	//CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CCodeMaxViewEx diagnostics

#ifdef _DEBUG
void CCodeMaxViewEx::AssertValid() const
{
	CView::AssertValid();
}

void CCodeMaxViewEx::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		int CCodeMaxViewEx::OnCreate()
//	Description :	Creates the code max view. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			
*************************/
int CCodeMaxViewEx::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	m_pDoc = (CCodeMaxDocEx*)m_pDocument;

	if (m_pDoc->m_wndEdit.Create(WS_CHILD | WS_VISIBLE, CRect(0, 0, 100, 100), this, IDC_EDIT) == -1)
		return -1;

	((CSchedMgrECPApp*)AfxGetApp())->LoadCodeMaxProfile(m_pDoc->m_wndEdit);

	return 0;	
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		BOOL CCodeMaxViewEx::PreCreateWindow()
//	Description :	This method is called by the framework before the 
//                  creation of the Windows CE window attached to this
//                  CWnd object.
//					
//					
//	Return :		BOOL
//						Nonzero if the window creation should continue;
//						0 to indicate creation failure.
//						
//
//	Parameters :	
//					CREATESTRUCT Address of a CREATESTRUCT structure
//					CREATESTRUCT structure defines the initialization parameters passed to the window procedure of an application. 
//
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
BOOL CCodeMaxViewEx::PreCreateWindow(CREATESTRUCT& cs) 
{
	return CView::PreCreateWindow(cs);
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		void CCodeMaxViewEx::OnSize()
//	Description :	The framework calls this member function after the
//                  window�s size has changed. 
//					
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					UINT nType - Specifies the type of resizing requested.
//					int cx -	Specifies the new width of the client area.
//					int cy	-	Specifies the new height of the client area.
//	Note :	
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CCodeMaxViewEx::OnSize(UINT nType, int cx, int cy) 
{
	CView::OnSize(nType, cx, cy);

	if (m_pDoc->m_wndEdit.m_hWnd)
		m_pDoc->m_wndEdit.SetWindowPos(NULL, -1, -1, cx, cy, SWP_NOZORDER | SWP_NOMOVE);
}
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxViewEx::OnSetFocus()
//	Description :	The framework calls this member function after
//                  gaining the input focus. 
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					CWnd * pointer to CWnd object.
//							Contains the CWnd object that loses the input 
//                          focus (may be NULL). 
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CCodeMaxViewEx::OnSetFocus(CWnd* pOldWnd) 
{
	CTime			cTimeLastMod;
	CFileFind		finder;

	if (finder.FindFile(m_pDoc->GetPathName()))
	{
		if (finder.FindNextFile() >= 0)
		{
			finder.GetLastWriteTime(cTimeLastMod);
			CTime	cOpenDocTime = m_pDoc->m_cTimeLastMod;

			if (cTimeLastMod > m_pDoc->m_cTimeLastMod)
			{
				m_pDoc->m_cTimeLastMod = cTimeLastMod;
				//PostMessage(WM_PROMPTRELOADDOC);
				PromptReloadDoc(0,0);
			}
		}
	}

	CView::OnSetFocus(pOldWnd);
	if (m_pDoc->m_wndEdit.m_hWnd)
		m_pDoc->m_wndEdit.SetFocus();
}
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxViewEx::PromptReloadDoc()
//	Description :	This routine prompts the user to reload the document. 
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
LRESULT CCodeMaxViewEx::PromptReloadDoc(WPARAM wParam, LPARAM lParam)
{
	if (AfxMessageBox(IDS_FILE_MODIFIED, MB_OKCANCEL) == IDOK)
	{
		::SendMessage(m_pDoc->m_wndEdit, CMM_OPENFILE, 0,
						(LPARAM)((LPCTSTR)m_pDoc->GetPathName()));
	}

	return TRUE;
}
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxViewEx::OnRClick()
//	Description :	This routine pops up a context menu when the right mouse
//                  button is pressed.. 
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					NMHDR	*  pointer to NMHDR object
//						Contains information about a notification message. 
//					LRESULT *  pointer to LRESULT object
//						Specifies the return value of the window
//                      procedure that processed the message specified by the message value.
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CCodeMaxViewEx::OnRClick(NMHDR* pNMHDR, LRESULT* pResult)
{
	*pResult = 1;
	CPoint ptMousePos = (CPoint) GetMessagePos();
//	ScreenToClient(&ptMousePos);

	theApp.ShowPopupMenu (IDR_CONTEXT_CODEMAX_MENU, ptMousePos, AfxGetMainWnd());
}
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxViewEx::AbortDlgProc ()
//
//	Description :	. 
//					The AbortProc function is an application-defined
//                  callback function used with the SetAbortProc function.
//                  It is called when a print job is to be canceled during
//                  spooling.
//					
//	Return :		BOOL
//							TRUE	-	                 
//							FALSE	-	
//
//	Parameters :	
//					hdlg   - Handle to the Print common dialog box window for
//						     which the message is intended. 
//					uiMsg  - Identifies the message being received. 
//					wParam - Specifies additional information about the message.
//					         The exact meaning depends on the value of the uiMsg
//							 parameter. 
//					lParam - Specifies additional information about the message.
//							 The exact meaning depends on the value of the uiMsg
//							 parameter.
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
BOOL CALLBACK CCodeMaxViewEx::AbortDlgProc (HWND /* hWndDlg */, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static LPBOOL pbAbort = NULL;
	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			ASSERT (lParam);
			pbAbort = (LPBOOL) lParam;
			return TRUE;
		}

		case WM_COMMAND:
		{
		//	WORD wID = LOWORD (wParam);  

			if ( LOWORD (wParam) == IDCANCEL && HIWORD (wParam) == BN_CLICKED )
			{
				*pbAbort = TRUE;
			}
			break;
		}
	}
	return 0;
}
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxViewEx::PrintHookProc ()
//	Description :	The PrintHookProc hook procedure is an application-defined
//                  or library-defined callback function that is used with the
//                  PrintDlg function. 
//					
//					
//	Return :		
//                  UINT 
//					If the hook procedure returns zero, the default dialog box
//					procedure processes the message.
//
//					If the hook procedure returns a nonzero value, the default
//                  dialog box procedure ignores the message.
//
//	Parameters :	
//					hdlg   - Handle to the Print common dialog box window for
//						     which the message is intended. 
//					uiMsg  - Identifies the message being received. 
//					wParam - Specifies additional information about the message.
//					         The exact meaning depends on the value of the uiMsg
//							 parameter. 
//					lParam - Specifies additional information about the message.
//							 The exact meaning depends on the value of the uiMsg
//							 parameter.
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
UINT CALLBACK CCodeMaxViewEx::PrintHookProc (HWND hdlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			HWND hWnd = ::GetDlgItem(hdlg, 1058);
			::SetWindowText (hWnd, _T("Lines"));
		}
		break;
	}
	return 0;
}
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxViewEx::OnFilePrint()
//	Description :	This routine prints the document. 
//					
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CCodeMaxViewEx::OnFilePrint()
{
	MSG msg;

	CPrintDialog pd (false, PD_ALLPAGES | PD_HIDEPRINTTOFILE | PD_RETURNDC |
							PD_USEDEVMODECOPIES | PD_ENABLEPRINTHOOK, NULL);

	// The max number of pages (lines) is 65535.  This is a Microsoft limit on the GUI
	// control.  A user can print more than this, he just can't selectivly print out
	// lines above this range/
	unsigned short nMaxLine;
	if (CM_GetLineCount (m_pDoc->m_wndEdit) >= USHRT_MAX)
		nMaxLine = USHRT_MAX;
	else
		nMaxLine = (unsigned short)CM_GetLineCount (m_pDoc->m_wndEdit);

	// Setup defaults for the print dialog box
	pd.m_pd.hDevMode=((CSchedMgrECPApp*)AfxGetApp())->m_hDevMode;
	pd.m_pd.hDevNames=((CSchedMgrECPApp*)AfxGetApp())->m_hDevNames;
	pd.m_pd.nMinPage=1;
	pd.m_pd.nMaxPage=nMaxLine;
	pd.m_pd.lpfnPrintHook = PrintHookProc;

	// If the dialog box was closed with an OK
	if (pd.DoModal() == IDOK)
	{
		// Margins in inches
		TCHAR*	pStopString;
		double fTopMargin		= _tcstod(GetAppRegMisc()->m_strTopMargin, &pStopString);
		double fBottomMargin	= _tcstod(GetAppRegMisc()->m_strBottomMargin, &pStopString);
		double fLeftMargin		= _tcstod(GetAppRegMisc()->m_strLeftMargin, &pStopString);
		double fRightMargin		= _tcstod(GetAppRegMisc()->m_strRightMargin, &pStopString);

		HDC hDC = pd.CreatePrinterDC();
		if (hDC != NULL)
		{
			SetMapMode(hDC, MM_TEXT);
			
			((CSchedMgrECPApp*)AfxGetApp())->m_hDevMode = pd.m_pd.hDevMode;
			((CSchedMgrECPApp*)AfxGetApp())->m_hDevNames= pd.m_pd.hDevNames;

			CPoint cPhysical;
			CPoint cPrintable;
			CPoint cOffset;
			CPoint cLogPixels;

			cPhysical.x		= GetDeviceCaps(hDC, PHYSICALWIDTH);
			cPhysical.y		= GetDeviceCaps(hDC, PHYSICALHEIGHT);
			cPrintable.x	= GetDeviceCaps(hDC, HORZRES);
			cPrintable.y	= GetDeviceCaps(hDC, VERTRES);
			cOffset.x		= GetDeviceCaps(hDC, PHYSICALOFFSETX);
			cOffset.y		= GetDeviceCaps(hDC, PHYSICALOFFSETY);
			cLogPixels.x	= GetDeviceCaps(hDC, LOGPIXELSX);
			cLogPixels.y	= GetDeviceCaps(hDC, LOGPIXELSY);

			// Offsets from edge of page to start of printable area in pixels
			int	nTopOffset		= cOffset.y;
			int nBottomOffset	= cPhysical.y - cPrintable.y - cOffset.y;
			int	nLeftOffset		= cOffset.x;
			int nRightOffset	= cPhysical.x - cPrintable.x - cOffset.x;

			// Width of margins in pixels
			// Use the larger of the physical offset needed by the printer or 
			// the size requested by the operator
			int nTopMargin		= __max((int)(fTopMargin * cLogPixels.y), nTopOffset);
			int nBottomMargin	= __max((int)(fBottomMargin * cLogPixels.y), nBottomOffset);
			int nLeftMargin		= __max((int)(fLeftMargin * cLogPixels.x), nLeftOffset);
			int nRightMargin	= __max((int)(fRightMargin * cLogPixels.x), nRightOffset);

			// If the margins are too big then reset them back to the minimum needed
			if ((nTopMargin + nBottomMargin) > cPhysical.y)
			{
				nTopMargin = nTopOffset;
				nBottomMargin = nBottomOffset;
			}
			if ((nLeftMargin + nRightMargin) > cPhysical.x)
			{
				nLeftMargin = nLeftOffset;
				nRightMargin = nRightOffset;
			}

			// Readjust the printable area using the new margins
			cPrintable.x = cPhysical.x - nLeftMargin - nRightMargin;
			cPrintable.y = cPhysical.y - nTopMargin - nBottomMargin;
			
			int nTech		= GetDeviceCaps(hDC, TECHNOLOGY);

			HFONT hFont = CreateFont (-MulDiv (GetAppRegMisc()->m_nFontSize, GetDeviceCaps (hDC, LOGPIXELSY), 72),
									  0, 0, 0, GetAppRegMisc()->m_nFontWeight,
									  GetAppRegMisc()->m_bFontIsItalic,
									  GetAppRegMisc()->m_bFontIsUnderline,
									  GetAppRegMisc()->m_bFontIsStrikeOut,
									  0, 0, 0, 0, 0, GetAppRegMisc()->m_strFontFaceName);
			SelectObject(hDC, hFont);

			TCHAR szTitle[ _MAX_PATH + 1 ];
			_tcscpy_s (szTitle, m_pDoc->GetPathName());
			DOCINFO di = {sizeof (DOCINFO), szTitle, NULL, NULL, 0};	

			VERIFY (StartDoc (hDC, &di) > 0);
			TEXTMETRIC tm;
			VERIFY (GetTextMetrics (hDC, &tm));
			CPoint cTextSize;
			cTextSize.x = tm.tmAveCharWidth;
			cTextSize.y = tm.tmHeight;

			// Determine the number of whole lines that can fit within the printable area
			int nMaxLinesPerPage = cPrintable.y / cTextSize.y;
			int nMaxCharsPerLine = cPrintable.x / cTextSize.x;
 
			// Determine the size of a tab character in pixels
			int cxTab = (tm.tmAveCharWidth*10) * CM_GetTabSize (m_pDoc->m_wndEdit);

			int nCurLine = 0;
			int nStartLine;
			int nEndLine;

			// Should we print the whole document, the range specified, or the highlighted selection?
			if (pd.PrintAll())
			{
				nStartLine = 0;
				nEndLine = CM_GetLineCount (m_pDoc->m_wndEdit) - 1;
			}
			else if (pd.PrintRange())
			{
				nStartLine = pd.GetFromPage() - 1;
				nEndLine = pd.GetToPage() - 1;
			}
			else
			{
				CM_RANGE range;
				CM_GetSel (m_pDoc->m_wndEdit, &range, true);
				nStartLine = range.posStart.nLine;
				nEndLine = range.posEnd.nLine;
			}

			BOOL bAbort = FALSE;

			// display the cancel dialog
			HWND hWndAbort = CreateDialogParam (AfxGetApp()->m_hInstance, MAKEINTRESOURCE (IDD_ABORTPRINT), 
												NULL, (DLGPROC) AbortDlgProc, (LPARAM) &bAbort);
			::ShowWindow( hWndAbort, SW_SHOW );

			// flush the message queue for the abort dlg so it can redraw its controls
			while (PeekMessage(&msg, hWndAbort, NULL, NULL, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			BOOL bContinue = TRUE;
			int	nLinesPerPage;
			CM_LINENUMBERING cmNumbering;
			CM_GetLineNumbering(m_pDoc->m_wndEdit.m_hWnd, &cmNumbering);
			
			if(nTech == DT_CHARSTREAM)
				nLinesPerPage = 58;					 //62 lines minus the header and footer
			else
				nLinesPerPage = nMaxLinesPerPage - 4;//Subtract out 2 lines for header and 2 for footer ;

			int nCurLineOnPage;
			int nPageCount = 1;
			CString strPage;
			CString strCurLine;
			int nYOffset; // Top left corner of bounding box
			int nChars; // Number of actual characters on a line.  Line too long are truncated
			SetTextAlign(hDC, TA_BOTTOM);
			while (bContinue && !bAbort)
			{
				// Start the Page
				VERIFY (StartPage(hDC) > 0 );
				
				// Output the header
				nYOffset = nTopMargin;
				nChars = (m_pDoc->GetPathName().GetLength() > nMaxCharsPerLine) ? nMaxCharsPerLine : m_pDoc->GetPathName().GetLength();
				TabbedTextOut (hDC, nLeftMargin, nYOffset, m_pDoc->GetPathName(), nChars, 1, &cxTab, 0);

				// Output the footer
				nYOffset = nTopMargin + ((nMaxLinesPerPage-1) * cTextSize.y);
				strPage.Format (_T("Page: %d"), nPageCount);
				nChars = (strPage.GetLength() > nMaxCharsPerLine) ? nMaxCharsPerLine : strPage.GetLength();
				TabbedTextOut (hDC, nLeftMargin, nYOffset, strPage, nChars, 1, &cxTab, 0);

				// Fill up the page with text
				nCurLineOnPage = 1;
				nPageCount++;

				nYOffset = nTopMargin + (2 * cTextSize.y);
				while (nCurLineOnPage <= nLinesPerPage)
				{
					if (nCurLine > nEndLine)
					{
						// end of doc
						bContinue = FALSE;
						break;
					}
					else
					{
						// Only print lines within the range selected
						if (nCurLine >= nStartLine)
						{
							int cbLine = CM_GetLineLength (m_pDoc->m_wndEdit, nCurLine, true);
							CString strTextLine;
							m_pDoc->m_wndEdit.GetLine(nCurLine, strTextLine);

							if(cmNumbering.bEnabled)
							{
								strCurLine.Format(_T("%.5d "), nCurLine+1);
								strCurLine += strTextLine;						
								nChars = (strCurLine.GetLength() > nMaxCharsPerLine) ? nMaxCharsPerLine : strCurLine.GetLength();
							}
							else
							{
								nChars = (cbLine > nMaxCharsPerLine) ? nMaxCharsPerLine : cbLine;
								strCurLine = strTextLine;
							}

							TabbedTextOut (hDC, nLeftMargin, nYOffset, strCurLine, nChars, 1, &cxTab, 0);
							nYOffset += cTextSize.y;
							nCurLineOnPage++;
						}
						nCurLine++;
					}
				}
				VERIFY (EndPage(hDC) > 0);
				// flush the message queue for the abort dlg so it can process the Cancel button
				while (PeekMessage(&msg, hWndAbort, NULL, NULL, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
				if (nCurLine > nEndLine)
					bContinue = FALSE;
			}
		

			if ( bAbort )
			{
				VERIFY (AbortDoc(hDC) > 0);
			}
			else
			{
				VERIFY (EndDoc(hDC) > 0);
			}

			::DestroyWindow (hWndAbort);
			DeleteDC (hDC);
			DeleteObject (hFont);
		}
		else
		{
			GetAppOutputBar()->OutputToEvents(_T("Could not retrieve Device Context from printer"), COutputBar::FATAL);
		}
	}
}
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCodeMaxViewEx::OnUpdateFilePrint()
//	Description :	This routine enables/disables the print menu button. 
//					
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CCmdUI* pCmdUI pointer to a CCmdUI object.
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CCodeMaxViewEx::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);	
}

// GetTitle() calculates a window title suitable for display
//LPCTSTR CCodeMaxViewEx::GetTitle(LPTSTR pszBuff) const
//{
//	ASSERT(pszBuff);
//	if (*m_szPath)
//	{
//		_tcscpy(pszBuff, m_szPath);
//	}
//	else
//	{
//		_tcscpy(pszBuff, _T("Untitled"));
//	}
//	return pszBuff;

//LRESULT CCodeMaxViewEx::OnModifiedChanged()
//{
//	// Update the dirty flag (*) on the caption
//	UpdateWindowTitle();
//	return 0;
//}

//void CCodeMaxViewEx::UpdateWindowTitle()
//{
//	TCHAR szTitle[_MAX_PATH + 1];
//
//	CString strPath = GetDocument()->GetPathName();
//	if (CM_IsModified(GetDocument()->m_wndEdit))
//	{
//		strPath += _T("*");
//	}
//
//	SetWindowText(szTitle);
//}
/////////////////////////////////////////////////////////////////////////////
// CCodeMaxViewEx message handlers

//////////////////////////////////////////////////////////////////////
// CSTOLView Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNCREATE(CSTOLView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLView::CSTOLView()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CSTOLView::CSTOLView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLView::~CSTOLView()
//	Description :	Class destructor. 
//					
//					
//	Return :		destructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CSTOLView::~CSTOLView(){}

BEGIN_MESSAGE_MAP(CSTOLView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CSTOLView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLView::OnDraw()
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CSTOLView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CSTOLView::AssertValid() const
{
	CView::AssertValid();
}

void CSTOLView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLView::OnCreate()
//	Description :	Creates the view for STOL file types. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			//////////////////////////////////////////////////////////////////////////
*************************/
int CSTOLView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

//	CCodeMaxDocEx*	pDoc = (CCodeMaxDocEx*)GetDocument();

	m_pDoc->m_wndEdit.SetLanguage(_T("STOL"));
	return 0;
}

IMPLEMENT_DYNCREATE(CRTCSCmdView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSCmdView::CRTCSCmdView()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CRTCSCmdView::CRTCSCmdView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSCmdView::~CRTCSCmdView()
//	Description :	Class destructor. 
//					
//					
//	Return :		destructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CRTCSCmdView::~CRTCSCmdView(){}

BEGIN_MESSAGE_MAP(CRTCSCmdView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CRTCSCmdView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSCmdView::OnDraw()
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CRTCSCmdView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CRTCSCmdView::AssertValid() const
{
	CView::AssertValid();
}

void CRTCSCmdView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSCmdView::OnCreate()
//	Description :	Creates the view for RTCS file types. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			
//////////////////////////////////////////////////////////////////////////
*************************/
int CRTCSCmdView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pDoc->m_wndEdit.SetLanguage(_T("RTCS Command File"));
	return 0;
}

IMPLEMENT_DYNCREATE(CPaceCmdView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CPaceCmdView::CPaceCmdView()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CPaceCmdView::CPaceCmdView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CPaceCmdView::~CPaceCmdView()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CPaceCmdView::~CPaceCmdView(){}

BEGIN_MESSAGE_MAP(CPaceCmdView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CPaceCmdView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		void CPaceCmdView::OnDraw()
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CPaceCmdView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CPaceCmdView::AssertValid() const
{
	CView::AssertValid();
}

void CPaceCmdView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CPaceCmdView::OnCreate()
//	Description :	Creates the view for Command Pacing files. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
int CPaceCmdView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pDoc->m_wndEdit.SetLanguage(_T("Pace Command File"));
	return 0;
}


IMPLEMENT_DYNCREATE(CSTOLCmdView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLCmdView::CSTOLCmdView()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CSTOLCmdView::CSTOLCmdView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLCmdView::~CSTOLCmdView()
//	Description :	Class destructor. 
//					
//					
//	Return :		destructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CSTOLCmdView::~CSTOLCmdView(){}

BEGIN_MESSAGE_MAP(CSTOLCmdView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CSTOLCmdView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLCmdView::OnDraw()
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CSTOLCmdView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CSTOLCmdView::AssertValid() const
{
	CView::AssertValid();
}

void CSTOLCmdView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLCmdView::OnCreate()
//	Description :	Creates the view for STOL command files. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			
//////////////////////////////////////////////////////////////////////////
*************************/
int CSTOLCmdView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pDoc->m_wndEdit.SetLanguage(_T("STOL Command File"));
	return 0;
}


IMPLEMENT_DYNCREATE(CRTCSView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSView::CRTCSView()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CRTCSView::CRTCSView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSView::~CRTCSView()
//	Description :	Class destructor. 
//					
//					
//	Return :		destructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CRTCSView::~CRTCSView(){}

BEGIN_MESSAGE_MAP(CRTCSView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CRTCSView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSView::OnDraw()
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CRTCSView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CRTCSView::AssertValid() const
{
	CView::AssertValid();
}

void CRTCSView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSView::OnCreate()
//  Description :	Creates the view for RTCS files. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			
//////////////////////////////////////////////////////////////////////////
*************************/
int CRTCSView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pDoc->m_wndEdit.SetLanguage(_T("RTCS"));
	return 0;
}

IMPLEMENT_DYNCREATE(CRTCSSetView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSSetView::CRTCSSetView()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CRTCSSetView::CRTCSSetView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSSetView::~CRTCSSetView()
//	Description :	Class destructor. 
//					
//					
//	Return :		destructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CRTCSSetView::~CRTCSSetView(){}

BEGIN_MESSAGE_MAP(CRTCSSetView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CRTCSSetView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSSetView::OnDraw()
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CRTCSSetView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CRTCSSetView::AssertValid() const
{
	CView::AssertValid();
}

void CRTCSSetView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSSetView::OnCreate()
//	Description :	Creates the view for RTCS Set files. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			
//////////////////////////////////////////////////////////////////////////
*************************/
int CRTCSSetView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pDoc->m_wndEdit.SetLanguage(_T("RTCS Set"));
	return 0;
}

IMPLEMENT_DYNCREATE(CCLSView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCLSView::CCLSView()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CCLSView::CCLSView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCLSView::~CCLSView()
//	Description :	Class destructor. 
//					
//					
//	Return :		destructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CCLSView::~CCLSView(){}

BEGIN_MESSAGE_MAP(CCLSView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CCLSView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCLSView::OnDraw()
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CCLSView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CCLSView::AssertValid() const
{
	CView::AssertValid();
}

void CCLSView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CCLSView::OnCreate()
//	Description :	Creates the view for CLSs files. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			
//////////////////////////////////////////////////////////////////////////
*************************/
int CCLSView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pDoc->m_wndEdit.SetLanguage(_T("CLS"));
	return 0;
}

IMPLEMENT_DYNCREATE(CRptView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRptView::CRptView
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CRptView::CRptView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRptView::~CRptView()
//	Description :	Class destructor. 
//					
//					
//	Return :		destructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CRptView::~CRptView(){}

BEGIN_MESSAGE_MAP(CRptView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CRptView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		void CRptView::OnDraw()
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CRptView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CRptView::AssertValid() const
{
	CView::AssertValid();
}

void CRptView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRptView::OnCreate()
//	Description :	Creates the view for report files. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			
//////////////////////////////////////////////////////////////////////////
*************************/
int CRptView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pDoc->m_wndEdit.SetLanguage(_T("Report"));
	return 0;
}
IMPLEMENT_DYNCREATE(CLisView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CLisView::CLisView()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CLisView::CLisView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CLisView::~CLisView()
//	Description :	Class destructor. 
//					
//					
//	Return :		destructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CLisView::~CLisView(){}

BEGIN_MESSAGE_MAP(CLisView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CLisView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CLisView::OnDraw(CDC* pDC)
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CLisView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CLisView::AssertValid() const
{
	CView::AssertValid();
}

void CLisView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CLisView::OnCreate()
//	Description :	Creates the view for list files. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			
//////////////////////////////////////////////////////////////////////////
*************************/
int CLisView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pDoc->m_wndEdit.SetLanguage(_T("Report"));
	return 0;
}

IMPLEMENT_DYNCREATE(CIMCView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CIMCView::CIMCView
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CIMCView::CIMCView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CIMCView::~CIMCView
//	Description :	Class destructor. 
//					
//					
//	Return :		destructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CIMCView::~CIMCView(){}

BEGIN_MESSAGE_MAP(CIMCView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CIMCView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CIMCView::OnDraw()
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC pointer to a CDC object.
//					The CDC class defines a class of device-context objects
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CIMCView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CIMCView::AssertValid() const
{
	CView::AssertValid();
}

void CIMCView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CIMCView::OnCreate()
//	Description :	Creates the view for IMC file types. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :						
//					
//////////////////////////////////////////////////////////////////////////
*************************/
int CIMCView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

IMPLEMENT_DYNCREATE(CBinView, CCodeMaxViewEx)
/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CBinView::CBinView()
//	Description :	Class constructor. 
//					
//					
//	Return :		constructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CBinView::CBinView(){}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CBinView::~CBinView()
//	Description :	Class destructor. 
//					
//					
//	Return :		destructor	-
//                  void nothing
//	Parameters :	
//					void nothing
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
CBinView::~CBinView(){}

BEGIN_MESSAGE_MAP(CBinView, CCodeMaxViewEx)
	//{{AFX_MSG_MAP(CBinView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CBinView::OnDraw(CDC* pDC)
//	Description :	Called by the framework to render an image 
//                  of the document.  
//					
//	Return :		
//                  void nothing
//	Parameters :	
//					CDC* pDC
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
void CBinView::OnDraw(CDC* pDC)
{
	ASSERT_VALID(m_pDocument);
}

#ifdef _DEBUG
void CBinView::AssertValid() const
{
	CView::AssertValid();
}

void CBinView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CBinView::OnCreate()
//	Description :	Creates the view for Binary file types. 
//					
//	Return :		int		0/-1
//                  
//	Parameters :	
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application. 
//	Note :			
//					
//////////////////////////////////////////////////////////////////////////
*************************/
int CBinView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CCodeMaxViewEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_pDoc->m_wndEdit.SetLanguage(_T("STOL"));
	return 0;
}
