#if !defined(AFX_CODEMAXVIEW_H__C4F2DA93_D680_11D4_800D_00609704053C__INCLUDED_)
#define AFX_CODEMAXVIEW_H__C4F2DA93_D680_11D4_800D_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CodeMaxView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCodeMaxViewEx view

#include "CodeMaxDoc.h"

class CCodeMaxViewEx : public CView
{
protected:
	CCodeMaxViewEx();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCodeMaxViewEx)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCodeMaxViewEx)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnRClick(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_VIRTUAL
//	CCodeMaxDocEx* GetDocument()
//	{
//		return (CCodeMaxDocEx*)m_pDocument;
//	};

	CCodeMaxDocEx* m_pDoc;

// Implementation
protected:
	virtual ~CCodeMaxViewEx();
	CRect UserPage(CDC* pDC, double fTopMargin, double fBottomMargin,
				   double fLeftMargin, double fRightMargin);
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CCodeMaxViewEx)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnFilePrint();
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg LRESULT PromptReloadDoc(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
	static UINT CALLBACK PrintHookProc (HWND hdlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
	static BOOL CALLBACK AbortDlgProc (HWND /* hWndDlg */, UINT uMsg, WPARAM wParam, LPARAM lParam);
};

class CSTOLView : public CCodeMaxViewEx
{
protected:
	CSTOLView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSTOLView)
public:
	//{{AFX_VIRTUAL(CSTOLView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CSTOLView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CSTOLView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CRTCSCmdView : public CCodeMaxViewEx
{
protected:
	CRTCSCmdView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CRTCSCmdView)
public:
	//{{AFX_VIRTUAL(CRTCSCmdView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CRTCSCmdView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CRTCSCmdView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CPaceCmdView : public CCodeMaxViewEx
{
protected:
	CPaceCmdView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CPaceCmdView)
public:
	//{{AFX_VIRTUAL(CPaceCmdView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CPaceCmdView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CPaceCmdView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CSTOLCmdView : public CCodeMaxViewEx
{
protected:
	CSTOLCmdView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSTOLCmdView)
public:
	//{{AFX_VIRTUAL(CPaceCmdView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CSTOLCmdView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CPaceCmdView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CRTCSSetView : public CCodeMaxViewEx
{
protected:
	CRTCSSetView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CRTCSSetView)
public:
	//{{AFX_VIRTUAL(CRTCSSetView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CRTCSSetView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CRTCSSetView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CRTCSView : public CCodeMaxViewEx
{
protected:
	CRTCSView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CRTCSView)
public:
	//{{AFX_VIRTUAL(CRTCSView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CRTCSView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CRTCSView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CCLSView : public CCodeMaxViewEx
{
protected:
	CCLSView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCLSView)
public:
	//{{AFX_VIRTUAL(CCLSView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CCLSView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CCLSView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CRptView : public CCodeMaxViewEx
{
protected:
	CRptView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CRptView)
public:
	//{{AFX_VIRTUAL(CRptView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CRptView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CRptView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CLisView : public CCodeMaxViewEx
{
protected:
	CLisView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CLisView)
public:
	//{{AFX_VIRTUAL(CLisView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CLisView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CLisView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CIMCView : public CCodeMaxViewEx
{
protected:
	CIMCView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CIMCView)
public:
	//{{AFX_VIRTUAL(CIMCView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CIMCView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CIMCView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CBinView : public CCodeMaxViewEx
{
protected:
	CBinView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CBinView)
public:
	//{{AFX_VIRTUAL(CBinView)
	public:
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	//}}AFX_VIRTUAL
protected:
	virtual ~CBinView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CBinView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CODEMAXVIEW_H__C4F2DA93_D680_11D4_800D_00609704053C__INCLUDED_)
