/**
/////////////////////////////////////////////////////////////////////////////
// DistMessageBox.cpp : implementation of the DistMessageBox class.        //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// DistMessageBox.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class will prompt the user for a response when using the distribution
// software.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "stdafx.h"
#include "SchedMgrECP.h"
#include "DistMessageBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define IDC_CHKB	11
#define IDC_TEXT	12

/////////////////////////////////////////////////////////////////////////////
// CDistMessageBox dialog


/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistMessageBox::CDistMessageBox
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistMessageBox::CDistMessageBox(CWnd* pParent /*=NULL*/)
	: CDialog(CDistMessageBox::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDistMessageBox)
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistMessageBox::CDistMessageBox
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//		LPCTSTR lpszText1	-	text message
//		LPCTSTR lpszText2	-	text message
//		CWnd* pParent		-	pointer to the CWnd object
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistMessageBox::CDistMessageBox(LPCTSTR lpszText1, LPCTSTR lpszText2, CWnd* pParent)
	: CDialog(CDistMessageBox::IDD, pParent)
{
	m_strMessage1	= lpszText1;
	m_strMessage2	= lpszText2;
	m_bState		= NULL;

	TextLines(lpszText1);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistMessageBox::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//
//  Returns :		void	-  
//
//  Parameters : 
//           CDataExchange* pDX
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistMessageBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDistMessageBox)
	DDX_Control(pDX, IDC_YES_BUTTON, m_cYButton);
	DDX_Control(pDX, IDC_NO_BUTTON, m_cNButton);
	DDX_Control(pDX, IDC_CAUTION_STATIC, m_cCaution);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDistMessageBox, CDialog)
	//{{AFX_MSG_MAP(CDistMessageBox)
	ON_BN_CLICKED(IDC_NO_BUTTON, OnNo)
	ON_BN_CLICKED(IDC_YES_BUTTON, OnYes)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDistMessageBox message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistMessageBox::OnInitDialog
//  Description :	This routine create the MessageBox page dialog window.  
//
//	Return :		BOOL	-	TRUE if successful
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CDistMessageBox::OnInitDialog() 
{
	CDialog::OnInitDialog();

	BOOL bSuccess;
	
    // Get the system window message font for use for buttons and text area
    NONCLIENTMETRICS ncm;
    ncm.cbSize = sizeof(NONCLIENTMETRICS);
    VERIFY(SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &ncm, 0));
    m_font.CreateFontIndirect(&(ncm.lfMessageFont)); 

    // Now create the controls
    CRect TempRect(0,0,10,10);

    bSuccess = m_cText.Create(m_strMessage1, 
						WS_CHILD|WS_VISIBLE|SS_NOPREFIX|SS_LEFTNOWORDWRAP,
                        TempRect, this, IDC_TEXT);

    if (!bSuccess) 
		return FALSE;

    bSuccess = m_cCButton.Create(m_strMessage2, 
                           WS_CHILD|WS_VISIBLE|WS_TABSTOP|BS_AUTOCHECKBOX, 
                           TempRect, this, IDC_CHKB);

    if (!bSuccess) 
		return FALSE;

	m_cCButton.SetFont(&m_font, TRUE);
	m_cText.SetFont(&m_font, TRUE);

    // Resize the whole thing according to the number of text lines, desired window
    // width and current font.
	CRect WndRect;
	GetWindowRect(WndRect);
    SetWindowSize(m_nNumTextLines, (WndRect.Size()).cx);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistMessageBox::SetWindowSize
//  Description :	This routine sets the size of the dialog.  
//
//	Return :		void	-
//
//	Parameters :
//		int nNumTextLines	-	number of text lines
//		int nWindowWidth	-	width of window
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistMessageBox::SetWindowSize(int nNumTextLines, int nWindowWidth)
{
    int	nMargin = 10;
    CSize EdgeSize(::GetSystemMetrics(SM_CXEDGE), ::GetSystemMetrics(SM_CYEDGE));

    CRect TextRect;
	CRect BitmRect;
	CRect YesbRect;
	CRect NobnRect;
	CRect ChkbRect;

	m_cCaution.GetWindowRect(BitmRect);
	m_cYButton.GetWindowRect(YesbRect);
	m_cNButton.GetWindowRect(NobnRect);

	CSize ChkbSize;
	CSize TextSize;
	CSize BitmSize = BitmRect.Size();
	CSize YesbSize = YesbRect.Size();
	CSize NobnSize = NobnRect.Size();

    // Set up a default size for the text area in case things go wrong
    TextRect.SetRect(BitmSize.cx+(nMargin*3),nMargin+5, nWindowWidth-(BitmRect.Width()+(nMargin*3)), 100+2*nMargin);

    // Get DrawText to tell us how tall the text area will be (while we're at
    // it, we'll see how big the word "Yes", "No" is)
    CDC* pDC = GetDC();

    if (pDC) 
	{
        CFont* pOldFont = pDC->SelectObject(&m_font);
        CString str = _T("M");

        for (int i = 0; i < nNumTextLines-1; i++) 
			str += _T("\nM");

        pDC->DrawText(str, TextRect, DT_CALCRECT|DT_NOCLIP|DT_NOPREFIX);

		TextSize = pDC->GetTextExtent(m_strText) + CSize(EdgeSize.cx*35, EdgeSize.cy*4);

        TextRect.right = TextRect.left + TextSize.cx;

		ChkbSize = pDC->GetTextExtent(m_strMessage2) + CSize(EdgeSize.cx*4, EdgeSize.cy*3);

        pDC->SelectObject(pOldFont);
        ReleaseDC(pDC);
    }

    // Work out how big (and where) the No control should be
    ChkbRect.SetRect(TextRect.left, TextRect.bottom, 
                     nWindowWidth-nMargin, TextRect.bottom+ChkbSize.cy);

	// Resize the main window to fit the controls
    CSize ClientSize(TextRect.Width() + nMargin,
                     (nMargin+5) + TextRect.Height() + nMargin + 
					 ChkbRect.Height() + (nMargin*2) + YesbSize.cy);

    CRect WndRect, ClientRect;
    GetWindowRect(WndRect); 
	GetClientRect(ClientRect);

//	int wndWth = WndRect.Width();
//	int cliWth = ClientRect.Width();
//	int wndHgt = WndRect.Height();
//	int cliHgt = ClientRect.Height();

    WndRect.right	= WndRect.left + WndRect.Width()  - ClientRect.Width()  + ClientSize.cx;
    WndRect.bottom	= WndRect.top  + WndRect.Height() - ClientRect.Height() + ClientSize.cy;
    MoveWindow(WndRect);

	int nCenter = (int)WndRect.Width()/2;

    // Work out how big (and where) the Yes control should be
    YesbRect.SetRect(nCenter-(YesbSize.cx+5), ChkbRect.bottom+(nMargin*2), 
                     nCenter-5, ChkbRect.bottom+(nMargin*2)+YesbSize.cy);

    // Work out how big (and where) the No control should be
    NobnRect.SetRect(nCenter+5, ChkbRect.bottom+(nMargin*2), 
                     nCenter+YesbSize.cx+5, ChkbRect.bottom+(nMargin*2)+YesbSize.cy);

    // Now reposition the controls...
    m_cText.MoveWindow(TextRect);
	m_cCButton.MoveWindow(ChkbRect);
	m_cYButton.MoveWindow(YesbRect);
	m_cNButton.MoveWindow(NobnRect);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistMessageBox::TextLines
//  Description :	This routine sets the size of the text window.  
//
//	Return :		void	-
//
//	Parameters :
//		LPCTSTR lpszText	-	text line
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistMessageBox::TextLines(LPCTSTR lpszText)
{
	CString strTemp1(lpszText);
	CString strTemp2(lpszText);
	CString strSub;
	int nPos;
	int nLen = 0;

	m_nNumTextLines = strTemp1.Replace('\n','-') + 2;

	while((nPos = strTemp2.Find('\n')) != -1)
	{
		strSub = strTemp2.Left(nPos);

		if(nLen < strSub.GetLength())
		{
			nLen		= strSub.GetLength();
			m_strText	= strSub;
		}

		strTemp2.Delete(0, nPos+1);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistMessageBox::OnNo
//  Description :	This routine closes the dialog window.  
//
//	Return :		void	-
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistMessageBox::OnNo() 
{
	CDialog::OnCancel();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistMessageBox::OnYes
//  Description :	This routine closes the dialog window.  
//
//	Return :		void	-
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistMessageBox::OnYes() 
{
	int nCheck = m_cCButton.GetCheck();

	if(nCheck == 1)
		*m_bState = true;
	else
		*m_bState = false;

	CDialog::OnOK();	
}
