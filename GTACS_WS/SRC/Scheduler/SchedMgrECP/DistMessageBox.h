#if !defined(AFX_DISTMESSAGEBOX_H__78E94281_37E9_11D5_A9E7_00104BD11936__INCLUDED_)
#define AFX_DISTMESSAGEBOX_H__78E94281_37E9_11D5_A9E7_00104BD11936__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DistMessageBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDistMessageBox dialog

class CDistMessageBox : public CDialog
{
// Construction
public:
	CDistMessageBox(CWnd* pParent = NULL);				// standard constructor
	CDistMessageBox(LPCTSTR lpszText1, LPCTSTR lpszText2, CWnd* pParent = NULL);// alternate constructor

	void SetWindowSize(int nNumTextLines, int nWindowWidth = 232);
	void TextLines(LPCTSTR lpszText);
	void CheckState(bool* bState) { m_bState = bState; }
// Dialog Data
	//{{AFX_DATA(CDistMessageBox)
	enum { IDD = IDD_DIST_MSGBOX_DIALOG };
	CButton	m_cYButton;
	CButton	m_cNButton;
	CStatic	m_cCaution;
	//}}AFX_DATA

    int		m_nNumTextLines;
	bool*	m_bState;	
	CString	m_strText;
	CString m_strMessage1;
	CString m_strMessage2;

    CStatic	m_cText;
    CButton	m_cCButton;
    CFont	m_font;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDistMessageBox)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDistMessageBox)
	virtual BOOL OnInitDialog();
	afx_msg void OnNo();
	afx_msg void OnYes();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DISTMESSAGEBOX_H__78E94281_37E9_11D5_A9E7_00104BD11936__INCLUDED_)
