/**
/////////////////////////////////////////////////////////////////////////////
// Distribution.cpp : implementation of the Distribution class.            //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// Distribution.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class will prompt the user for a response when using the distribution
// software.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "stdafx.h"
#include "schedmgrecp.h"
// #include "Registry.h"
#include "ProgressWnd.h"
#include "DistMessageBox.h"
#include "Distribution.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDistributeSheet

IMPLEMENT_DYNAMIC(CDistributeSheet, CResizableSheet)

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeSheet::CDistributeSheet
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributeSheet::CDistributeSheet() : CResizableSheet()
{
	AddPage(&m_pp1);
	AddPage(&m_pp2);
	AddPage(&m_pp3);
	SetWizardMode();

	m_sourcePath	= NULL;
	m_destList		= NULL;
	m_fileList		= NULL;
	m_dWordArray	= NULL;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeSheet::CDistributeSheet
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributeSheet::CDistributeSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CResizableSheet(nIDCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_pp1);
	AddPage(&m_pp2);
	AddPage(&m_pp3);
	SetWizardMode();

	m_sourcePath	= NULL;
	m_destList		= NULL;
	m_fileList		= NULL;
	m_dWordArray	= NULL;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeSheet::CDistributeSheet
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributeSheet::CDistributeSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CResizableSheet(pszCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_pp1);
	AddPage(&m_pp2);
	AddPage(&m_pp3);
	SetWizardMode();

	m_sourcePath	= NULL;
	m_destList		= NULL;
	m_fileList		= NULL;
	m_dWordArray	= NULL;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeSheet::~CDistributeSheet()
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributeSheet::~CDistributeSheet()
{
}


BEGIN_MESSAGE_MAP(CDistributeSheet, CResizableSheet)
	//{{AFX_MSG_MAP(CDistributeSheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDistributeSheet message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeSheet::OnInitDialog
//  Description :	This routine creates the Distribution dialog window.  
//
//	Return :		BOOL	-	TRUE if successful
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CDistributeSheet::OnInitDialog()
{
	CResizableSheet::OnInitDialog();

	// set minimal size
	CRect rc;
	GetWindowRect(&rc);
	SetMinTrackSize(CSize(rc.Width(), rc.Height()));

	// enable save/restore, with active page
	EnableSaveRestore(_T("Resizable Sheets"), _T("Distribute"));

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CDistributionPage1 property page

IMPLEMENT_DYNCREATE(CDistributionPage1, CResizablePage)

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage1::CDistributionPage1
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributionPage1::CDistributionPage1() : CResizablePage(CDistributionPage1::IDD)
{
	//{{AFX_DATA_INIT(CDistributionPage1)
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage1::~CDistributionPage1
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributionPage1::~CDistributionPage1()
{
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage1::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//
//  Returns :		void	-  
//
//  Parameters : 
//           CDataExchange* pDX
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributionPage1::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDistributionPage1)
	DDX_Control(pDX, IDC_DIST_DAY_EDIT, m_cDayEdit);
	DDX_Control(pDX, IDC_DIST_MASTER_CHECK, m_cMasterCheck);
	DDX_Control(pDX, IDC_DIST_DAY_CHECK, m_cDayCheck);
	DDX_Control(pDX, IDC_DIST1_TREE, m_cTreeCtrl);
	DDX_Control(pDX, IDC_DIST_FROM_COMBO, m_cSourceSiteCombo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDistributionPage1, CResizablePage)
	//{{AFX_MSG_MAP(CDistributionPage1)
	ON_CBN_SELCHANGE(IDC_DIST_FROM_COMBO, OnUpdateControls)
	ON_BN_CLICKED(IDC_DIST_DAY_CHECK, OnDistDayCheck)
	ON_NOTIFY(UDN_DELTAPOS, IDC_DIST_DAY_SPIN, OnDeltaposDistDaySpin)
	ON_EN_UPDATE(IDC_DIST_DAY_EDIT, OnUpdateDistDayEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDistributionPage1 message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage1::OnInitDialog
//  Description :	This routine creates the Distribution dialog window.  
//
//	Return :		BOOL	-	TRUE if successful
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CDistributionPage1::OnInitDialog()
{
	CResizablePage::OnInitDialog();

	m_bInInit = TRUE;

	AddAnchor(IDC_DISTRIBUTE_GROUP,		TOP_LEFT,	BOTTOM_RIGHT);

	AddAnchor(IDC_DIST_FROM_GROUP,		TOP_LEFT,	TOP_RIGHT);
	AddAnchor(IDC_DIST_FROM_STATIC,		TOP_LEFT,	TOP_LEFT);
	AddAnchor(IDC_DIST_FROM_COMBO,		TOP_LEFT,	TOP_RIGHT);

	AddAnchor(IDC_DIST_TO_GROUP,		TOP_LEFT,	TOP_RIGHT);
	AddAnchor(IDC_DIST_TO_STATIC,		TOP_LEFT,	TOP_LEFT);
	AddAnchor(IDC_DIST1_TREE,			TOP_LEFT,	TOP_RIGHT);

	AddAnchor(IDC_DIST_DIR_GROUP,		TOP_LEFT,	TOP_RIGHT);
	AddAnchor(IDC_DIST_DAY_CHECK,		TOP_LEFT,	TOP_LEFT);
	AddAnchor(IDC_DIST_DAY_EDIT,		TOP_LEFT,	TOP_LEFT);
	AddAnchor(IDC_DIST_DAY_SPIN,		TOP_LEFT,	TOP_LEFT);
	AddAnchor(IDC_DIST_MASTER_CHECK,	TOP_LEFT,	TOP_LEFT);
	AddAnchor(IDC_DIST_MASTER_STATIC,	TOP_LEFT,	TOP_RIGHT);

	HICON hIcon[3];
	hIcon[0] = AfxGetApp()->LoadIcon(IDI_CHECKB1);
	hIcon[1] = AfxGetApp()->LoadIcon(IDI_CHECKB2);
	hIcon[2] = AfxGetApp()->LoadIcon(IDI_CHECKB3);
	m_imageList.Create(16, 16, 0, 3, 3);
	m_imageList.SetBkColor(RGB(255,255,255));
	m_imageList.Add(hIcon[0]);
	m_imageList.Add(hIcon[1]);
	m_imageList.Add(hIcon[2]);
	m_cTreeCtrl.SetImageList(&m_imageList, TVSIL_NORMAL);
	m_cTreeCtrl.TreeID(IDC_DIST1_TREE);

	TV_INSERTSTRUCT tvinsert;
	tvinsert.hParent			= NULL;
	tvinsert.hInsertAfter		= TVI_LAST;
	tvinsert.item.mask			= TVIF_IMAGE | TVIF_TEXT;
	tvinsert.item.hItem			= NULL;
	tvinsert.item.state			= 0;
	tvinsert.item.stateMask		= 0;
	tvinsert.item.cchTextMax	= 6;
	tvinsert.item.cChildren		= 0;
	tvinsert.item.lParam		= 0;
	tvinsert.item.pszText		= _T("All Sites"); // Top level
	tvinsert.item.iImage		= 1;

	HTREEITEM hTop = m_cTreeCtrl.InsertItem(&tvinsert);

	// Initialize Combo Boxes and Tree Control
	int					nIndex;
	CString				strFilepath;
	CString				strName;
	CDistributeSheet*	ps = (CDistributeSheet*)GetParent();

	for( int i=0; i<nMaxSites; i++ )
	{
		for( int j=0; j<nMaxGTACS[i]; j++ )
		{
			m_strList.AddTail(GetAppRegGTACS()->m_strAbbrev[i][j]);
			nIndex = m_cSourceSiteCombo.AddString(GetAppRegGTACS()->m_strTitle[i][j]);
			m_cSourceSiteCombo.SetItemData(nIndex, nIndex);

			// Second level
			if( nIndex != 0 )
			{
				strFilepath.Format(_T("\\\\%s\\%s\\%s\\"),
					GetAppRegGTACS()->m_strAbbrev[i][j],				// Server
					GetAppRegGTACS()->m_strProcShare[i][j],				// Share
					GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC]);// Spacecraft
				m_destList.AddTail(strFilepath);

				strName					= GetAppRegGTACS()->m_strTitle[i][j];
				tvinsert.hParent		= hTop;
				tvinsert.item.pszText	= strName.GetBuffer(strName.GetLength());
				strName.ReleaseBuffer();
				m_cTreeCtrl.InsertItem(&tvinsert);
			}
		}
	}

	m_cTreeCtrl.Expand(hTop, TVE_EXPAND);
	m_cSourceSiteCombo.SetCurSel(0);

	CString strTemp;
	// Set the mask, prompt symbol, and range of the various Day controls
	m_bUseDay	= FALSE;
	m_nDay		= GetAppRegMisc()->m_nDOY;
	m_cDayEdit.SetMask(_T("###"));
	m_cDayEdit.SetPromptSymbol(_T('0'));
	((CSpinButtonCtrl*)GetDlgItem(IDC_DIST_DAY_SPIN))->SetRange(0, 366);
	((CSpinButtonCtrl*)GetDlgItem(IDC_DIST_DAY_SPIN))->SetPos(m_nDay);
	strTemp.Format(_T("%03d"), m_nDay);
	m_cDayEdit.SetWindowText(strTemp);

	int			nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
	int			nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	POSITION	nNum			= m_strList.FindIndex(0);
	CString		strDir;

	strDir.Format(_T("\\\\%s\\%s\\%s\\"),
		m_strList.GetAt(nNum),										// Server
		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC]);		// Spacecraft

	(*(ps->m_sourcePath)) = strDir;
	strDir.Delete(strDir.GetLength()-1);
	GetDlgItem(IDC_DIST_MASTER_STATIC)->SetWindowText(strDir);

	((CButton*)GetDlgItem(IDC_DIST_DAY_CHECK))->SetCheck(0);
	((CButton*)GetDlgItem(IDC_DIST_MASTER_CHECK))->SetCheck(1);

	GetDlgItem(IDC_DIST_DAY_EDIT)->EnableWindow(FALSE);
	GetDlgItem(IDC_DIST_DAY_SPIN)->EnableWindow(FALSE);	

	m_bInInit = FALSE;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage1::OnSetActive  
//  Description :	This routine disables/enables, as appropriate, the 
//					different window controls.
//                  
//  Returns :		BOOL	-	TRUE  if successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CDistributionPage1::OnSetActive()
{
	((CDistributeSheet*)GetParent())->SetWizardButtons(PSWIZB_NEXT);

	return CPropertyPage::OnSetActive();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage1::OnUpdateControls  
//  Description :	This routine updates all window controls associated 
//					with the CDistributionPage1 class.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CDistributionPage1::OnUpdateControls()
{
	CString				strDir;
	CDistributeSheet*	ps = (CDistributeSheet*)GetParent();

	m_cTreeCtrl.DeleteAllItems();
	m_destList.RemoveAll();

	int			nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
	int			nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	int			strIndex		= m_cSourceSiteCombo.GetItemData(m_cSourceSiteCombo.GetCurSel());
	POSITION	nNum			= m_strList.FindIndex(strIndex);
	
	strDir.Format(_T("\\\\%s\\%s\\%s\\"),
		m_strList.GetAt(nNum),										// Server
		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC]);		// Spacecraft

	*(ps->m_sourcePath) = strDir;
	strDir.Delete(strDir.GetLength()-1);
	GetDlgItem(IDC_DIST_MASTER_STATIC)->SetWindowText(strDir);

	TV_INSERTSTRUCT tvinsert;
	tvinsert.hParent			= NULL;
	tvinsert.hInsertAfter		= TVI_LAST;
	tvinsert.item.mask			= TVIF_IMAGE | TVIF_TEXT;
	tvinsert.item.hItem			= NULL;
	tvinsert.item.state			= 0;
	tvinsert.item.stateMask		= 0;
	tvinsert.item.cchTextMax	= 6;
	tvinsert.item.cChildren		= 0;
	tvinsert.item.lParam		= 0;
	tvinsert.item.pszText		= _T("All Sites"); // Top level
	tvinsert.item.iImage		= 1;

	HTREEITEM	hTop			= m_cTreeCtrl.InsertItem(&tvinsert);

	// Repopulate Tree Control
	int		nCount=0;
	CString	strName;
	CString	strFilepath;

	for( int i=0; i<nMaxSites; i++ )
	{
		for( int j=0; j<nMaxGTACS[i]; j++ )
		{
			// Second level
			if( nCount != strIndex )
			{
				strFilepath.Format(_T("\\\\%s\\%s\\%s\\"),
					GetAppRegGTACS()->m_strAbbrev[i][j],				// Server
					GetAppRegGTACS()->m_strProcShare[i][j],				// Share
					GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC]);// Spacecraft
				
				m_destList.AddTail(strFilepath);

				strName					= GetAppRegGTACS()->m_strTitle[i][j];
				tvinsert.hParent		= hTop;
				tvinsert.item.pszText	= strName.GetBuffer(strName.GetLength());
				strName.ReleaseBuffer();
				m_cTreeCtrl.InsertItem(&tvinsert);
			}

			nCount++;
		}
	}

	m_cTreeCtrl.Expand(hTop, TVE_EXPAND);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage1::OnDistDayCheck  
//  Description :	This routine enables/disables as needed the dialog
//					controls for the Distribution dialog box.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CDistributionPage1::OnDistDayCheck() 
{
	if( m_cDayCheck.GetCheck() )
	{
		m_cDayCheck.SetCheck(1);
		m_cDayEdit.EnableWindow(true);
		GetDlgItem(IDC_DIST_DAY_SPIN)->EnableWindow(true);
		m_bUseDay = TRUE;
	}
	else
	{
		m_cDayCheck.SetCheck(0);
		m_cDayEdit.EnableWindow(false);
		GetDlgItem(IDC_DIST_DAY_SPIN)->EnableWindow(false);
		m_bUseDay = FALSE;
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage1::OnDeltaposDistDaySpin  
//  Description :	This routine sets the dialog control spin position.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//		NMHDR* pNMHDR		-	pointer to spin control object
//		LRESULT* pResult	-	pointer to spin control result value.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CDistributionPage1::OnDeltaposDistDaySpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	// Get the current Day position
	CString strTemp;
	GetDlgItem(IDC_DIST_DAY_EDIT)->GetWindowText(strTemp);
	pNMUpDown->iPos = _ttoi(strTemp);
	
	// If the new Day position would be within range, then allow it
	int nNewPos = pNMUpDown->iPos + pNMUpDown->iDelta;

	if( nNewPos >= 0 && nNewPos <= 999 )
	{
		CString strTemp;
		strTemp.Format(_T("%03d"), nNewPos);
		GetDlgItem(IDC_DIST_DAY_EDIT)->SetWindowText(strTemp);
		m_nDay	= nNewPos;
		*pResult= 0;
	}
	else
		*pResult= 1;
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage1::OnUpdateDistDayEdit  
//  Description :	This routine sets the dialog control text.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CDistributionPage1::OnUpdateDistDayEdit() 
{
	if( m_bInInit )
		return;

	CString strTemp;
	GetDlgItem(IDC_DIST_DAY_EDIT)->GetWindowText(strTemp);
	((CSpinButtonCtrl*)GetDlgItem(IDC_DIST_DAY_SPIN))->SetPos(_ttoi(strTemp));
	m_nDay = _ttoi(strTemp);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage1::OnWizardNext
//  Description :   This routine is called when the user selects next   
//					on the dialog window.   The specific window controls
//                  are updated as needed.
//                   
//                  
//  Returns :		LRESULT  -
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
LRESULT CDistributionPage1::OnWizardNext() 
{
	if( !m_cDayCheck.GetCheck() && !m_cMasterCheck.GetCheck() )
	{
		AfxMessageBox(_T("Select a directory to distribute files from."), MB_OK);
		return -1;
	}

	HTREEITEM	hItem = m_cTreeCtrl.GetItemByName(NULL, _T("All Sites"));
	TV_ITEM		item;
	item.hItem	= hItem;
	item.mask	= TVIF_IMAGE;
	m_cTreeCtrl.GetItem(&item);
	
	if( item.iImage == 0 )
	{
		AfxMessageBox(_T("Select a site or sites to distribute files."), MB_OK);
		return -1;
	}

	CDistributeSheet* ps = (CDistributeSheet*)GetParent();

	if( m_bUseDay )
	{
		CString strDay;
		strDay.Format(GetAppRegFolder()->m_strDayFormat, m_nDay);
		COXUNC	unc(*(ps->m_sourcePath)+strDay+_T("\\"));

		if( !unc.Exists() )
		{
			CString strMsg;
			strMsg.Format(_T("Directory %s does not exists!\n")
				_T("Select an existing directory to distribute files from."), 
				*(ps->m_sourcePath)+strDay);

			AfxMessageBox(strMsg, MB_OK);
			return -1;
		}
	}

	POSITION	pos		= m_destList.GetHeadPosition();
	CString		strNode;

	hItem = m_cTreeCtrl.GetChildItem(hItem);

	while( hItem != NULL )
	{
		strNode		= m_destList.GetNext(pos);
		item.hItem	= hItem;
		item.mask	= TVIF_IMAGE;
		m_cTreeCtrl.GetItem(&item);

		if( item.iImage == 1 )
			ps->m_destList->AddTail(strNode);

		// Go to next sibling item.
		hItem = m_cTreeCtrl.GetNextSiblingItem(hItem);
	}

	ps->m_bUseMaster=(m_cMasterCheck.GetCheck() == 1 ? TRUE : FALSE);
	ps->m_bUseDay	= m_bUseDay;
	ps->m_nDay		= m_nDay;

	return CPropertyPage::OnWizardNext();
}

/////////////////////////////////////////////////////////////////////////////
// CDistributionPage2 property page

IMPLEMENT_DYNCREATE(CDistributionPage2, CResizablePage)

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage2::CDistributionPage2
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributionPage2::CDistributionPage2() : CResizablePage(CDistributionPage2::IDD)
{
	//{{AFX_DATA_INIT(CDistributePage2)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_mstrList.AddTail(_T("cls"));
	m_mstrList.AddTail(_T("data"));
	m_mstrList.AddTail(_T("frame"));
	m_mstrList.AddTail(_T("procs"));
	m_mstrList.AddTail(_T("rtcs"));
	m_dstrList.AddTail(_T("frame"));
	m_dstrList.AddTail(_T("oats"));
	m_dstrList.AddTail(_T("procs"));
	m_dstrList.AddTail(_T("star"));
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage2::~CDistributionPage2
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributionPage2::~CDistributionPage2()
{
	m_mstrList.RemoveAll();
	m_dstrList.RemoveAll();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage2::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//
//  Returns :		void	-  
//
//  Parameters : 
//           CDataExchange* pDX
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributionPage2::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDistributePageB)
	DDX_Control(pDX, IDC_DIST2_TREE, m_cTreeCtrl);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDistributionPage2, CResizablePage)
	//{{AFX_MSG_MAP(CDistributionPage2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDistributionPage2 message handlers

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage2::OnInitDialog
//  Description :	This routine create the Distrbution2 page dialog window.  
//
//  Returns :		BOOL	-	TRUE if Successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CDistributionPage2::OnInitDialog()
{
	CResizablePage::OnInitDialog();

	m_bInInit = TRUE;

	AddAnchor(IDC_DIST_PARAMETERS_GROUP, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_DIST_FILE_TYPE_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_DIST_FILE_STATIC, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_DIST2_TREE, TOP_LEFT, TOP_RIGHT);

	HICON hIcon[3];
	hIcon[0] = AfxGetApp()->LoadIcon(IDI_CHECKB1);
	hIcon[1] = AfxGetApp()->LoadIcon(IDI_CHECKB2);
	hIcon[2] = AfxGetApp()->LoadIcon(IDI_CHECKB3);
	m_imageList.Create(16, 16, 0, 3, 3);
	m_imageList.SetBkColor(RGB(255,255,255));
	m_imageList.Add(hIcon[0]);
	m_imageList.Add(hIcon[1]);
	m_imageList.Add(hIcon[2]);
	m_cTreeCtrl.SetImageList(&m_imageList, TVSIL_NORMAL);
	m_cTreeCtrl.TreeID(IDC_DIST1_TREE);
	PopulateTree();
	
	m_bInInit = FALSE;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage2::OnSetActive  
//  Description :	This routine disables/enables, as appropriate, the 
//					different window controls.
//                  
//  Returns :		BOOL	-	TRUE  if successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CDistributionPage2::OnSetActive()
{
	((CDistributeSheet*)GetParent())->SetWizardButtons(PSWIZB_BACK | PSWIZB_NEXT);

	if( !m_bInInit )
	{
		m_cTreeCtrl.DeleteAllItems();
		PopulateTree();
	}

	return CPropertyPage::OnSetActive();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage2::OnWizardNext
//  Description :   This routine is called when the user selects next   
//					on the dialog window.   The specific window controls
//                  are updated as needed.
//                   
//                  
//  Returns :		LRESULT  -
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
LRESULT CDistributionPage2::OnWizardNext()
{
	CDistributeSheet* ps = (CDistributeSheet*)GetParent();

	CString strDailyTmp;
	CString strDailyDir;

	strDailyTmp.Format(GetAppRegFolder()->m_strDayFormat, ps->m_nDay);
	strDailyDir = (*(ps->m_sourcePath))+strDailyTmp+_T("\\");

	// Top level
	CString	strTmp;

	ps->m_dirList.RemoveAll();

	HTREEITEM hItem = m_cTreeCtrl.GetItemByName(NULL, _T("All Files"));

	if( hItem != NULL )
	{
		TV_ITEM item;
		item.hItem	= hItem;
		item.mask	= TVIF_IMAGE;
		m_cTreeCtrl.GetItem(&item);

		if( item.iImage == 1 )
		{
			if( ps->m_bUseMaster )
			{
				// Insert all file types into the list
				for( int i=0; i<nMaxFolderTypes; i++ )
				{
					strTmp = GetAppRegFolder()->m_strDir[i];
					strTmp.MakeLower();

					if( m_mstrList.Find(strTmp) != NULL )
						ps->m_dirList.AddTail((*(ps->m_sourcePath))+strTmp);
				}
			}

			if( ps->m_bUseDay )
			{
				// Insert all file types into the list
				for( int i=0; i<nMaxFolderTypes; i++ )
				{
					strTmp = GetAppRegFolder()->m_strDir[i];
					strTmp.MakeLower();

					if( m_dstrList.Find(strTmp) != NULL )
						ps->m_dirList.AddTail(strDailyDir+strTmp);
				}
			}
		}
		else
		{
			if( ps->m_bUseMaster )
			{
				hItem		= m_cTreeCtrl.GetItemByName(NULL, _T("Master Files"));
				item.hItem	= hItem;
				item.mask	= TVIF_IMAGE;
				m_cTreeCtrl.GetItem(&item);

				if( item.iImage == 1 )
				{
					// Insert only selected parent children into the list
					for( int i=0; i<nMaxFolderTypes; i++ )
					{
						strTmp = GetAppRegFolder()->m_strDir[i];
						strTmp.MakeLower();

						if( m_mstrList.Find(strTmp) != NULL )
							ps->m_dirList.AddTail((*(ps->m_sourcePath))+strTmp);
					}
				}
				else
				{
					CString strFol;

					// Insert only selected parent children into the list
					for( int i=0; i<nMaxFolderTypes; i++ )
					{
						strFol = GetAppRegFolder()->m_strDir[i];
						strFol.MakeLower();

						if( m_mstrList.Find(strFol) != NULL )
						{
							strTmp = GetAppRegFolder()->m_strTitle[i];

							HTREEITEM cItem = m_cTreeCtrl.GetItemByName(hItem,(LPCTSTR)strTmp);

							item.hItem = cItem;
							m_cTreeCtrl.GetItem(&item);

							if( item.iImage == 1 )
								ps->m_dirList.AddTail((*(ps->m_sourcePath))+strFol);
						}
					}
				}
			}

			if( ps->m_bUseDay )
			{
				hItem		= m_cTreeCtrl.GetItemByName(NULL, _T("Daily Files"));
				item.hItem	= hItem;
				item.mask	= TVIF_IMAGE;
				m_cTreeCtrl.GetItem(&item);

				if( item.iImage == 1 )
				{
					// Insert only selected parent children into the list
					for( int i=0; i<nMaxFolderTypes; i++ )
					{
						strTmp = GetAppRegFolder()->m_strDir[i];
						strTmp.MakeLower();

						if( m_dstrList.Find(strTmp) != NULL )
							ps->m_dirList.AddTail(strDailyDir+strTmp);
					}
				}
				else
				{
					CString strFol;

					// Insert only selected parent children into the list
					for( int i=0; i<nMaxFolderTypes; i++ )
					{
						strFol = GetAppRegFolder()->m_strDir[i];
						strFol.MakeLower();

						if( m_dstrList.Find(strFol) != NULL )
						{
							strTmp = GetAppRegFolder()->m_strTitle[i];

							HTREEITEM cItem = m_cTreeCtrl.GetItemByName(hItem,(LPCTSTR)strTmp);

							item.hItem = cItem;
							m_cTreeCtrl.GetItem(&item);

							if( item.iImage == 1 )
								ps->m_dirList.AddTail(strDailyDir+strFol);
						}
					}
				}
			}
		}
	}

	return CPropertyPage::OnWizardNext();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage2::PopulateTree
//  Description :   This routine is called to populate the tree control   
//					window.                  
//                  
//  Returns :		LRESULT  -
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CDistributionPage2::PopulateTree()
{
	TV_INSERTSTRUCT tvinsert;
	tvinsert.hParent			= NULL;
	tvinsert.hInsertAfter		= TVI_LAST;
	tvinsert.item.mask			= TVIF_IMAGE | TVIF_TEXT;
	tvinsert.item.hItem			= NULL;
	tvinsert.item.state			= 0;
	tvinsert.item.stateMask		= 0;
	tvinsert.item.cchTextMax	= 6;
	tvinsert.item.cChildren		= 0;
	tvinsert.item.lParam		= 0;
	tvinsert.item.pszText		= _T("All Files"); // Top level
	tvinsert.item.iImage		= 1;
	HTREEITEM hTop				= m_cTreeCtrl.InsertItem(&tvinsert);

	CDistributeSheet*	ps = (CDistributeSheet*)GetParent();
	CString				strTmp;

	// Second level
	if( ps->m_bUseMaster )
	{
		strTmp					= _T("Master Files");
		tvinsert.hParent		= hTop;
		tvinsert.item.pszText	= strTmp.GetBuffer(strTmp.GetLength());
		HTREEITEM hItem			= m_cTreeCtrl.InsertItem(&tvinsert);
		strTmp.ReleaseBuffer();

		for( int i=0; i<nMaxFolderTypes; i++ )
		{
			strTmp = GetAppRegFolder()->m_strDir[i];
			strTmp.MakeLower();

			if( m_mstrList.Find(strTmp) != NULL )
			{
				strTmp = GetAppRegFolder()->m_strTitle[i];
				tvinsert.hParent		= hItem;
				tvinsert.item.pszText	= strTmp.GetBuffer(strTmp.GetLength());
				strTmp.ReleaseBuffer();
				m_cTreeCtrl.InsertItem(&tvinsert);
			}
		}
	}
	
	if( ps->m_bUseDay )
	{
		strTmp					= _T("Daily Files");
		tvinsert.hParent		= hTop;
		tvinsert.item.pszText	= strTmp.GetBuffer(strTmp.GetLength());
		HTREEITEM hItem			= m_cTreeCtrl.InsertItem(&tvinsert);
		strTmp.ReleaseBuffer();

		for( int i=0; i<nMaxFolderTypes; i++ )
		{
			strTmp = GetAppRegFolder()->m_strDir[i];
			strTmp.MakeLower();

			if( m_dstrList.Find(strTmp) != NULL )
			{
				strTmp = GetAppRegFolder()->m_strTitle[i];
				tvinsert.hParent		= hItem;
				tvinsert.item.pszText	= strTmp.GetBuffer(strTmp.GetLength());
				strTmp.ReleaseBuffer();
				m_cTreeCtrl.InsertItem(&tvinsert);
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDistributionPage3 property page

IMPLEMENT_DYNCREATE(CDistributionPage3, CResizablePage)

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage3::CDistributionPage3
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributionPage3::CDistributionPage3() : CResizablePage(CDistributionPage3::IDD)
{
	//{{AFX_DATA_INIT(CDistributionPage3)
	m_strDateTimeEdit = _T("");
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage2::~CDistributionPage2
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributionPage3::~CDistributionPage3()
{
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage3::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//
//  Returns :		void	-  
//
//  Parameters : 
//           CDataExchange* pDX
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributionPage3::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDistributionPage3)
	DDX_Control(pDX, IDC_DIST_NAME_CHECK, m_cFilterNameCheck);
	DDX_Control(pDX, IDC_DIST_DATE_TIME_CHECK, m_cFilterDateCheck);
	DDX_Control(pDX, IDC_DIST_NAME_EDIT, m_cFileEdit);
	DDX_Control(pDX, IDC_DIST_DATE_TIME_EDIT, m_cDateTimeEdit);
	DDX_Control(pDX, IDC_DIST_ALL_FILES_CHECK, m_cAllFilesCheck);
	DDX_Text(pDX, IDC_DIST_DATE_TIME_EDIT, m_strDateTimeEdit);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDistributionPage3, CResizablePage)
	//{{AFX_MSG_MAP(CDistributionPage3)
	ON_BN_CLICKED(IDC_DIST_ALL_FILES_CHECK, OnAllFilesCheck)
	ON_BN_CLICKED(IDC_DIST_DATE_TIME_CHECK, OnFilterDateCheck)
	ON_BN_CLICKED(IDC_DIST_NAME_CHECK, OnFilterNameCheck)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDistributionPage3 message handlers

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CDistributionPage3::OnInitDialog
//  Description :	This routine create the Distrbution3 page dialog window.  
//
//  Returns :		BOOL	-	TRUE if Successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CDistributionPage3::OnInitDialog()
{
	CResizablePage::OnInitDialog();

	AddAnchor(IDC_DIST_FILTER_GROUP, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_DIST_FILTER_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_DIST_ALL_FILES_CHECK, TOP_LEFT, TOP_RIGHT);

	AddAnchor(IDC_DIST_FILE_FILTER_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_DIST_DATE_TIME_CHECK, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_DIST_DATE_TIME_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_DIST_NAME_CHECK, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_DIST_NAME_EDIT, TOP_LEFT, TOP_RIGHT);

	CString strMask(_T("[12][09][01289][0-9]-[0-3][0-9][0-9]-"));
	strMask += _T("[012][0-9]:[0-5][0-9]:[0-5][0-9].[0-9][0-9][0-9]");
	CTime t  = CTime::GetCurrentTime();

	m_cDateTimeEdit.Init(strMask);
	m_strDateTimeEdit = t.FormatGmt(_T("%Y-%j-%H:%M:%S.000"));
	UpdateData(FALSE);

	m_cAllFilesCheck.SetButtonStyle(BS_CHECKBOX);
	m_cAllFilesCheck.SetCheck(1);
	m_cFilterDateCheck.SetButtonStyle(BS_CHECKBOX);
	m_cFilterNameCheck.SetButtonStyle(BS_CHECKBOX);
	m_cFileEdit.EnableWindow(false);
	m_cDateTimeEdit.EnableWindow(false);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage3::OnSetActive  
//  Description :	This routine disables/enables, as appropriate, the 
//					different window controls.
//                  
//  Returns :		BOOL	-	TRUE  if successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CDistributionPage3::OnSetActive()
{
	((CDistributeSheet*)GetParent())->SetWizardButtons(PSWIZB_BACK | PSWIZB_FINISH);

	return CPropertyPage::OnSetActive();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage3::OnWizardFinish  
//  Description :   This routine is called when the okay button is checked
//					on the finish dialog box.
//                  
//  Returns :		BOOL	-	TRUE if successful
//
//  Parameters :  
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CDistributionPage3::OnWizardFinish()
{
	CDistributeSheet* ps = (CDistributeSheet*)GetParent();

	if( !m_cAllFilesCheck.GetCheck()	&&
		!m_cFilterDateCheck.GetCheck()	&&
		!m_cFilterNameCheck.GetCheck() )
	{
		AfxGetMainWnd()->MessageBox(_T("Please select a file constraint!"),
									_T("Error, no Checkbox selected."), MB_OK);

		return FALSE;
	}

	POSITION pos = ps->m_dirList.GetHeadPosition();

	ps->m_fileList->RemoveAll();

	while( pos != NULL )
	{
		m_strCurDir = ps->m_dirList.GetNext(pos);

		if( ::SetCurrentDirectory(m_strCurDir) )
			EnumerateFolders();
	}

	return CPropertyPage::OnWizardFinish();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage3::OnAllFilesCheck  
//  Description :	This routine enables/disables as needed the dialog
//					controls for the Distribution3 dialog box.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CDistributionPage3::OnAllFilesCheck()
{
	if( m_cAllFilesCheck.GetCheck() )
	{
		m_cAllFilesCheck.SetCheck(0);

		if( !m_cFilterDateCheck.GetCheck() && !m_cFilterNameCheck.GetCheck() )
		{
			m_cFilterDateCheck.SetCheck(1);
			m_cDateTimeEdit.EnableWindow(true);
		}
	}
	else
	{
		m_cAllFilesCheck.SetCheck(1);
		m_cFilterDateCheck.SetCheck(0);
		m_cFilterNameCheck.SetCheck(0);
		m_cFileEdit.EnableWindow(false);
		m_cDateTimeEdit.EnableWindow(false);
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage3::OnFilterDateCheck  
//  Description :	This routine enables/disables as needed the dialog
//					controls for the Distribution3 dialog box.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CDistributionPage3::OnFilterDateCheck()
{
	if( m_cFilterDateCheck.GetCheck() )
	{
		m_cFilterDateCheck.SetCheck(0);
		m_cDateTimeEdit.EnableWindow(false);

		if( !m_cFilterNameCheck.GetCheck() && !m_cAllFilesCheck.GetCheck() )
			m_cAllFilesCheck.SetCheck(1);
	}
	else
	{
		m_cFilterDateCheck.SetCheck(1);
		m_cAllFilesCheck.SetCheck(0);
		m_cDateTimeEdit.EnableWindow(true);
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage3::OnFilterNameCheck  
//  Description :	This routine enables/disables as needed the dialog
//					controls for the Distribution3 dialog box.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CDistributionPage3::OnFilterNameCheck()
{
	if( m_cFilterNameCheck.GetCheck() )
	{
		m_cFilterNameCheck.SetCheck(0);
		m_cFileEdit.EnableWindow(false);

		if( !m_cFilterDateCheck.GetCheck() && !m_cAllFilesCheck.GetCheck() )
			m_cAllFilesCheck.SetCheck(1);
	}
	else
	{
		m_cFilterNameCheck.SetCheck(1);
		m_cAllFilesCheck.SetCheck(0);
		m_cFileEdit.EnableWindow(true);
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage3::EnumerateFolders  
//  Description :	This routine enumerates thru directories to find files.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CDistributionPage3::EnumerateFolders()
{
	WIN32_FIND_DATA		fd;
	HANDLE				hFind	= ::FindFirstFile(_T("*.*"), &fd);
	CDistributeSheet*	ps		= (CDistributeSheet*)GetParent();

	if( hFind != INVALID_HANDLE_VALUE )
	{
		CString strFile;
		CString strText;
		CString strDate;
		CString strEdit;
		DWORD	nFileSize;

		do
		{
			strFile = fd.cFileName;

			CTime ft(fd.ftCreationTime);
			strDate = ft.FormatGmt(_T("%Y-%j-%H:%M:%S.000"));

			if( !(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) )
			{
				nFileSize = (fd.nFileSizeHigh*MAXDWORD)+fd.nFileSizeLow;

				if( m_cFilterDateCheck.GetCheck() &&
					m_cFilterNameCheck.GetCheck() )
				{
					m_cFileEdit.GetWindowText(strText);
					m_cDateTimeEdit.GetWindowText(strEdit);

					if( (strDate > strEdit) && Wildcmp(strText, strFile) )
					{
						ps->m_fileList->AddTail(m_strCurDir+_T("\\")+fd.cFileName);
						ps->m_dWordArray->Add(nFileSize);
					}
				}
				else if( m_cFilterDateCheck.GetCheck() )
				{
					m_cDateTimeEdit.GetWindowText(strEdit);

					if( strDate > strEdit )
					{
						ps->m_fileList->AddTail(m_strCurDir+_T("\\")+fd.cFileName);
						ps->m_dWordArray->Add(nFileSize);
					}
				}
				else if( m_cFilterNameCheck.GetCheck() )
				{
					m_cFileEdit.GetWindowText(strText);

					if( Wildcmp(strText, strFile) )
					{
						ps->m_fileList->AddTail(m_strCurDir+_T("\\")+fd.cFileName);
						ps->m_dWordArray->Add(nFileSize);
					}
				}
				else
				{
					ps->m_fileList->AddTail(m_strCurDir+_T("\\")+fd.cFileName);
					ps->m_dWordArray->Add(nFileSize);
				}
			}
		} 
		while( ::FindNextFile(hFind, &fd) );

		::FindClose(hFind);
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CDistributionPage3::Wildcmp  
//  Description :	This routine tries to match a wildcard file.
//                  
//  Returns :		int	- if greater than 0 file matched wildcard. 
//
//  Parameters : 
//			LPCTSTR wildcrd	-	wildcard token
//			LPCTSTR charstr	-	filename
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
int CDistributionPage3::Wildcmp(LPCTSTR wildcrd, LPCTSTR charstr) 
{
	LPCTSTR cp =  charstr + 1;
	LPCTSTR mp =  wildcrd;
	
	while( (*charstr) && (*wildcrd != _T('*')) ) 
	{
		if( (*wildcrd != *charstr) && (*wildcrd != _T('?')) )
			return 0;

		wildcrd++;
		charstr++;
	}
		
	while( *charstr ) 
	{
		if( *wildcrd == _T('*') ) 
		{
			if( !(*(++wildcrd)) )
				return 1;

			mp = wildcrd;
			cp = charstr+1;
		} 
		else if( (*wildcrd == *charstr) || (*wildcrd == _T('?')) ) 
		{
			wildcrd++;
			charstr++;
		} 
		else 
		{
			wildcrd	= mp;
			charstr	= cp++;
		}
	}
		
	while( *wildcrd == _T('*') )
		wildcrd++;

	return !(*wildcrd);
}

/////////////////////////////////////////////////////////////////////////////
// CDistributeTreeCtrl

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeTreeCtrl::CDistributeTreeCtrl
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributeTreeCtrl::CDistributeTreeCtrl()
{
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeTreeCtrl::~CDistributeTreeCtrl
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributeTreeCtrl::~CDistributeTreeCtrl()
{
}

BEGIN_MESSAGE_MAP(CDistributeTreeCtrl, CTreeCtrl)
	//{{AFX_MSG_MAP(CDistributeTreeCtrl)
	ON_WM_LBUTTONDOWN()
	ON_NOTIFY_REFLECT(TVN_SELCHANGING, OnSelchanging)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDistributeTreeCtrl message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeTreeCtrl::OnLButtonDown
//	Description :	This routine is called when the left button is
//					pressed.
//
//	Return :		void	-
//	Parameters :
//			UINT nFlags		-	information about the results of the hit test.
//			CPoint point	-	client coordinates of the point to test.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributeTreeCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	UINT		uFlags;
	HTREEITEM	testItem = HitTest(point, &uFlags);

	if( testItem != NULL && (TVHT_ONITEMICON & uFlags) )
	{
		TV_ITEM item;
		item.hItem	= testItem;
		item.mask	= TVIF_IMAGE | TVIF_PARAM;
		GetItem(&item);

		item.iImage = (item.iImage == 1 ? 0 : 1);
		SetItem(&item);

		if(IDC_DIST1_TREE == m_treeID)
		{
			CheckParentItem(&testItem);
			CheckChildItem(&testItem, (item.iImage == 1 ? true : false));
		}
		else
		{
			if( item.iImage == 1 )
			{
				m_nNumOfBytes+=(DWORD)item.lParam;
				m_nNumOfFiles++;
			}
			else
			{
				m_nNumOfBytes-=(DWORD)item.lParam;
				m_nNumOfFiles--;
			}

			CString strTmp;

			if( m_nNumOfBytes > 1048576 )
				strTmp.Format(_T("%.2f MB"), ((double)m_nNumOfBytes)/1048576);
			else if( m_nNumOfBytes > 1024 )
				strTmp.Format(_T("%.2f KB"), ((double)m_nNumOfBytes)/1024);
			else
				strTmp.Format(_T("%d bytes"), m_nNumOfBytes);

			m_pBytesStatic->SetWindowText(strTmp);
			strTmp.Format(_T("%d"), m_nNumOfFiles);
			m_pFilesStatic->SetWindowText(strTmp);
		}
	}
	else
		CTreeCtrl::OnLButtonDown(nFlags, point);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeTreeCtrl::CheckParentItem
//	Description :	This routine is called to check the parent of the
//					specified tree view item.
//
//	Return :		void	-
//	Parameters :
//			HTREEITEM hItem	-	handle of a tree item.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributeTreeCtrl::CheckParentItem(HTREEITEM* hItem)
{
	HTREEITEM parentItem = GetParentItem(*hItem);

	if( parentItem != NULL )
	{
		TV_ITEM pitem;
		pitem.hItem	= parentItem;
		pitem.mask	= TVIF_IMAGE;

		int numSibChecked = 0;
		int numOfSiblings = 0;
		int numOfPartials = 0;

		HTREEITEM sibItem = GetChildItem(parentItem);

		while( sibItem != NULL )
		{
			numOfSiblings++;

			TV_ITEM sitem;
			sitem.hItem	= sibItem;
			sitem.mask	= TVIF_IMAGE;
			GetItem(&sitem);

			if( sitem.iImage == 1 )
				numSibChecked++;
			else if( sitem.iImage == 2 )
				numOfPartials++;

			sibItem = GetNextSiblingItem(sibItem);
		}

		if( (numSibChecked > 0 && numSibChecked < numOfSiblings) || numOfPartials > 0 )
			pitem.iImage = 2;
		else if(numSibChecked == numOfSiblings)
			pitem.iImage = 1;
		else
			pitem.iImage = 0;

		SetItem(&pitem);
		CheckParentItem(&parentItem);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeTreeCtrl::OnSelchanging
//	Description :	This routine is called when the selection is about
//					to be changed from one item to another.
//
//	Return :		void	-
//	Parameters :
//			NMHDR* pNMHDR	-	pointer to structure that contains
//								information about this notification 
//								message
//			LRESULT* pResult-	pointer to the result of the change 
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributeTreeCtrl::OnSelchanging(NMHDR* pNMHDR, LRESULT* pResult)
{
//	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	*pResult = 1;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeTreeCtrl::OnSelchanging
//	Description :	This routine is called to check the tree view
//					item that is the child of the item specified
//					by hItem.
//
//	Return :		void	-
//	Parameters :
//			HTREEITEM hItem	-	handle of a tree item.
//			bool bState		-	state of tree item. 
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributeTreeCtrl::CheckChildItem(HTREEITEM* hItem, bool bState)
{
	if( ItemHasChildren(*hItem) )
	{
		HTREEITEM hChildItem = GetChildItem(*hItem);

		while( hChildItem != NULL )
		{
			TV_ITEM citem;
			citem.hItem	= hChildItem;
			citem.mask	= TVIF_IMAGE;
			citem.iImage= (bState ? 1 : 0);

			SetItem(&citem);
			CheckChildItem(&hChildItem, bState);
			hChildItem = GetNextSiblingItem(hChildItem);
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributeTreeCtrl::GetItemByName
//	Description :	This routine is called to retrieve the tree view
//					item specified by name.
//
//	Return :		HTREEITEM	-	handle of a tree item.
//	Parameters :
//			HTREEITEM hItem		-	handle of a tree item.
//			LPCTSTR szItemName	-	tree item name. 
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
HTREEITEM CDistributeTreeCtrl::GetItemByName(
	HTREEITEM hItem, LPCTSTR szItemName)
{
	// If hItem is NULL, start search from root item.
	if( hItem == NULL )
		hItem = GetRootItem();

	while( hItem != NULL )
	{
		_TCHAR 	szBuffer[51];
		TV_ITEM	item;

		item.hItem		= hItem;
		item.mask		= TVIF_TEXT | TVIF_CHILDREN;
		item.pszText	= szBuffer;
		item.cchTextMax = 50;
		GetItem(&item);

		// Did we find it?
		if( lstrcmp(szBuffer, szItemName) == 0 )
			return hItem;

		// Check whether we have child items.
		if( item.cChildren )
		{
			// Recursively traverse child items.
			HTREEITEM hItemChild = GetChildItem(hItem);
			HTREEITEM hItemFound = GetItemByName(hItemChild, szItemName);

			// Did we find it?
			if( hItemFound != NULL )
				return hItemFound;
		}

		// Go to next sibling item.
		hItem = GetNextSiblingItem(hItem);
	}

	// Not found.
	return NULL;
}

/////////////////////////////////////////////////////////////////////////////
// CDistributionFinish dialog

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionFinish::CDistributionFinish
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//		CWnd* pParent	-	pointer to the parent window object.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CDistributionFinish::CDistributionFinish(CWnd* pParent /*=NULL*/)
	: CDialog(CDistributionFinish::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDistributionFinish)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	m_sourcePath	= NULL;
	m_destList		= NULL;
	m_fileList		= NULL;
	m_dWordArray	= NULL;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionFinish::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//
//  Returns :		void	-  
//
//  Parameters : 
//           CDataExchange* pDX
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributionFinish::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDistributionFinish)
	DDX_Control(pDX, IDC_DIST3_TREE, m_cTreeCtrl);
	DDX_Control(pDX, IDC_FILESOURCE_TEXT_STATIC, m_cFileSource);
	DDX_Control(pDX, IDC_NUMOFFILES_TEXT_STATIC, m_cNumOfFiles);
	DDX_Control(pDX, IDC_DESTINATION_TEXT_STATIC, m_cDestination);
	DDX_Control(pDX, IDC_NUMOFBYTES_TEXT_STATIC, m_cNumOfBytes);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDistributionFinish, CDialog)
	//{{AFX_MSG_MAP(CDistributionFinish)
	ON_NOTIFY(TVN_SELCHANGING, IDC_DIST3_TREE, OnSelchangingTree)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDistributionFinish message handlers

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CDistributionFinish::OnInitDialog
//  Description :	This routine create the DistributionFinish page 
//					dialog window.  
//
//  Returns :		BOOL	-	TRUE if Successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CDistributionFinish::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_cFileSource.SetWindowText(_T(""));
	m_cDestination.SetWindowText(_T(""));
	m_cNumOfBytes.SetWindowText(_T(""));
	m_cNumOfFiles.SetWindowText(_T(""));
	m_cTreeCtrl.TreeID(IDC_DIST3_TREE);
	m_cTreeCtrl.NumOfBytesStatic(&m_cNumOfBytes);
	m_cTreeCtrl.NumOfFilesStatic(&m_cNumOfFiles);

	HICON hIcon[2];
	hIcon[0] = AfxGetApp()->LoadIcon(IDI_CHECKB1);
	hIcon[1] = AfxGetApp()->LoadIcon(IDI_CHECKB2);
	m_imageList.Create(16, 16, 0, 2, 2);
	m_imageList.SetBkColor(RGB(255,255,255));
	m_imageList.Add(hIcon[0]);
	m_imageList.Add(hIcon[1]);
	m_cTreeCtrl.SetImageList(&m_imageList, TVSIL_NORMAL);

	// Top level
	m_tvinsert.hParent			= NULL;
	m_tvinsert.hInsertAfter		= TVI_LAST;
	m_tvinsert.item.mask		= TVIF_IMAGE | TVIF_TEXT | TVIF_PARAM;
	m_tvinsert.item.hItem		= NULL;
	m_tvinsert.item.iImage		= 1;

	CString		strTmp;
	POSITION	pos = m_fileList->GetHeadPosition();
	int			i 	= 0;

	while( pos != NULL )
	{
		strTmp					= m_fileList->GetNext(pos);
		m_tvinsert.item.lParam	= (LPARAM)(*m_dWordArray)[i++];
		m_tvinsert.item.pszText = strTmp.GetBuffer(strTmp.GetLength());
		m_cTreeCtrl.InsertItem(&m_tvinsert);
		strTmp.ReleaseBuffer();
	}

	CString strFil;
	CString strPce;

	m_cTreeCtrl.NumOfBytes(GetByteCount());
	m_cTreeCtrl.NumOfFiles(m_fileList->GetCount());
	strFil.Format(_T("%d"), m_fileList->GetCount());
	m_cNumOfFiles.SetWindowText(strFil);
	strPce	= *m_sourcePath;
	strPce.Delete(strPce.GetLength()-1, 1);
	m_cFileSource.SetWindowText(strPce);
	pos		= m_destList->GetHeadPosition();
	strPce	= m_destList->GetAt(pos);
	strPce.Delete(strPce.GetLength()-1, 1);
	m_cDestination.SetWindowText(strPce);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionFinish::OnSelchangingTree
//	Description :	This routine is called when the selection is about
//					to be changed from one item to another.
//
//	Return :		void	-
//	Parameters :
//			NMHDR* pNMHDR	-	pointer to structure that contains
//								information about this notification 
//								message
//			LRESULT* pResult-	pointer to the result of the change 
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributionFinish::OnSelchangingTree(NMHDR* pNMHDR, LRESULT* pResult)
{
//	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
	*pResult = 1;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionFinish::OnOK
//  Description :	This routine performs the distribution process
//					then closes the dialog window.  
//
//	Return :		void	-
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributionFinish::OnOK()
{
	GetSelectedFiles();

	CString	strTmp;
	CString strTxt;
	CString	strSrc;
	CString	strDes;
	CString	strFil;
	CString	strCmd;
	CString strPth;
	CString strPce;
	CString strMch;
	CString strDir(GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC]);
	int		index;
	int		nFileCnt	= m_selectedFiles.GetCount();
	int		nMachCnt	= m_destList->GetCount();
	int		nCurMCnt	= 0;


	CProgressWnd progressWnd(AfxGetMainWnd(), _T("Distribution Progress"), TRUE);
	progressWnd.SetRange(0, nMachCnt,TRUE);
	progressWnd.SetPos(0, TRUE);
	progressWnd.PeekAndPump(false);

	POSITION nos= m_destList->GetHeadPosition();

	while( nos != NULL && (!progressWnd.Cancelled()) )
	{
		progressWnd.SetRange(0,nFileCnt,FALSE);
		progressWnd.SetPos(0,FALSE);

		strPth 			= m_destList->GetNext(nos);
		strPce 			= strPth;
		strPce.Delete(strPce.GetLength()-1, 1);
		m_cDestination.SetWindowText(strPce);

		strMch			= strPce;
		int nPos		= strMch.Find('\\', 3);
		strMch.Delete(nPos, strMch.GetLength()-nPos);
		
		strTxt = _T("Distributing to: ") + strMch.Mid(2,strMch.GetLength());
		progressWnd.SetText(TRUE, strTxt);

		POSITION pos	= m_selectedFiles.GetHeadPosition();
		int nCurFCnt	= 0;

		while( pos != NULL && (!progressWnd.Cancelled()) )
		{
			strCmd	= m_selectedFiles.GetNext(pos);
			strSrc	= strCmd;
			index	= strCmd.ReverseFind('\\');
			strFil	= strCmd.Right(strCmd.GetLength()-index);
			strCmd.Delete(index, strCmd.GetLength()-index);
			index	= strCmd.Find(strDir);
			strDes	= strPth;
			strDes += strCmd.Mid(strDir.GetLength()+(index+1));

			COXUNC unc(strDes+_T("\\"));
			
			if( !unc.Exists() )
			{	
				CString strMsg;
				progressWnd.Hide();
				strMsg.Format(_T("Directory does not exists!\n\nDirectory:\n")
					_T("%s\n\nWould you like to create it?"), strDes);

				if( AfxMessageBox(strMsg, MB_YESNO|MB_ICONQUESTION) == IDYES )
				{
					if( !unc.Create() )
					{
						LPVOID lpMsgBuf;
						FormatMessage(
							FORMAT_MESSAGE_ALLOCATE_BUFFER |
							FORMAT_MESSAGE_FROM_SYSTEM |
							FORMAT_MESSAGE_IGNORE_INSERTS,
							NULL, GetLastError(),
							MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
							(LPTSTR) &lpMsgBuf, 0, NULL);

						CString strErr((LPTSTR)lpMsgBuf);
						if( !strErr.IsEmpty() ) strErr.Delete(strErr.GetLength()-2,2);
						strMsg.Format(_T("Error! %s"),strErr);
						LocalFree(lpMsgBuf);
						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					}
				}

				progressWnd.Show();
				progressWnd.PeekAndPump(false);
			}

			strTmp = _T("Distributing:     ") + strFil.Right(strFil.GetLength()-1);
			strDes += strFil;

			progressWnd.SetText(FALSE,strTmp);

			if( !CopyFile(strSrc, strDes, false) )
			{
				LPVOID lpMsgBuf;
				FormatMessage(
					FORMAT_MESSAGE_ALLOCATE_BUFFER |
					FORMAT_MESSAGE_FROM_SYSTEM |
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL, GetLastError(),
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPTSTR) &lpMsgBuf, 0, NULL);

				CString strErr((LPTSTR)lpMsgBuf);
				if( !strErr.IsEmpty() ) strErr.Delete(strErr.GetLength()-2,2);
				CString strWrn;
				strWrn.Format(
					_T("Unable to distribute to %s.\n%s\n")
					_T("Would you like to continue?"), strPce, strErr);
				CString strQst;
				strQst.Format(_T("Skip %s?"), strMch.Mid(2,strMch.GetLength()));
				progressWnd.Hide();
				LocalFree(lpMsgBuf);

				bool bCheck;
				CDistMessageBox dlg(strWrn, strQst);
				dlg.CheckState(&bCheck);

				if( dlg.DoModal() == IDCANCEL )
				{
					OnCancel();
					return;
				}

				if( bCheck )
				{
					progressWnd.SetPos(nFileCnt-nCurFCnt,FALSE);
					break;
				}

				progressWnd.Show();
				progressWnd.PeekAndPump(false);
				//::Sleep(2000);
			}

			progressWnd.SetPos(++nCurFCnt,FALSE);
			progressWnd.PeekAndPump(false);
			//::Sleep(2000);
		}

		progressWnd.SetPos(++nCurMCnt,TRUE);
		progressWnd.PeekAndPump(false);
	}

	progressWnd.Hide();
	progressWnd.PeekAndPump(false);

	if( progressWnd.Cancelled() )
		OnCancel();
	else
		CDialog::OnOK();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionFinish::OnCancel
//  Description :	This routine closes the dialog window.  
//
//	Return :		void	-
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributionFinish::OnCancel()
{
	/*
	m_pPS->m_dWordArray.RemoveAll();
	m_cFileSource.SetWindowText(_T(""));
	m_cDestination.SetWindowText(_T(""));
	m_cNumOfBytes.SetWindowText(_T(""));
	m_cNumOfFiles.SetWindowText(_T(""));
	m_selectedFiles.RemoveAll();
	m_cTreeCtrl.LockWindowUpdate();
	m_cTreeCtrl.DeleteAllItems();
	m_cTreeCtrl.UnlockWindowUpdate();
	*/
	CDialog::OnCancel();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionFinish::GetSelectedFiles
//  Description :	This routine gets all selected files from an in-
//					memory list.
//
//	Return :		void	-
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CDistributionFinish::GetSelectedFiles()
{
	CString		strTxt;
	HTREEITEM	hItem	= m_cTreeCtrl.GetRootItem();
	POSITION	pos		= m_fileList->GetHeadPosition();

	while( hItem != NULL )
	{
		strTxt = m_fileList->GetNext(pos);

		TV_ITEM item;
		item.hItem	= hItem;
		item.mask	= TVIF_IMAGE;
		m_cTreeCtrl.GetItem(&item);

		if( item.iImage == 1 )
			m_selectedFiles.AddTail(strTxt);

		hItem = m_cTreeCtrl.GetNextSiblingItem(hItem);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CDistributionFinish::GetSelectedFiles
//  Description :	This routine calculates the number of bytes for
//					all selected files.
//
//	Return :		void	-
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CDistributionFinish::GetByteCount()
{
	int		nSize	= m_dWordArray->GetSize();
	int		nTotal	= 0;
	CString strTmp;

	for( int i=0; i<nSize; i++ )
		nTotal += (*m_dWordArray)[i];

	if( nTotal > 1048576 )
		strTmp.Format(_T("%.2f MB"), ((double)nTotal)/1048576);
	if( nTotal > 1024 )
		strTmp.Format(_T("%.2f KB"), ((double)nTotal)/1024);
	else
		strTmp.Format(_T("%d bytes"), nTotal);

	m_cNumOfBytes.SetWindowText(strTmp);

	return nTotal;
}




