#if !defined(AFX_DISTRIBUTION_H__6DF7D1C9_CCD0_453E_92E3_4079C1E93D40__INCLUDED_)
#define AFX_DISTRIBUTION_H__6DF7D1C9_CCD0_453E_92E3_4079C1E93D40__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Distribution.h : header file
//

#include "mEdit.h"
#include "ResizableSheet.h"
#include "ResizablePage.h"

/////////////////////////////////////////////////////////////////////////////
// CDistributeTreeCtrl window

class CDistributeTreeCtrl : public CTreeCtrl
{
// Construction
public:
	CDistributeTreeCtrl();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDistributeTreeCtrl)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDistributeTreeCtrl();
	void CheckChildItem(HTREEITEM*, bool);
	void CheckParentItem(HTREEITEM*);
	void TreeID(int id) { m_treeID = id; }
	void NumOfBytesStatic(CStatic* pStatic)	{ m_pBytesStatic = pStatic; }
	void NumOfFilesStatic(CStatic* pStatic)	{ m_pFilesStatic = pStatic; }
	void NumOfBytes(int nBytes)				{ m_nNumOfBytes = nBytes; }
	void NumOfFiles(int nFiles)				{ m_nNumOfFiles = nFiles; }
	HTREEITEM GetItemByName(HTREEITEM, LPCTSTR);

	// Generated message map functions
protected:
	//{{AFX_MSG(CDistributeTreeCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnSelchanging(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

private:
	int			m_treeID;
	int			m_nNumOfFiles;
	int			m_nNumOfBytes;
	CStatic*	m_pBytesStatic;
	CStatic*	m_pFilesStatic;
};

/////////////////////////////////////////////////////////////////////////////
// CDistributionPage1 dialog

class CDistributionPage1 : public CResizablePage
{
	DECLARE_DYNCREATE(CDistributionPage1)

// Construction
public:
	CDistributionPage1();
	~CDistributionPage1();

// Dialog Data
	//{{AFX_DATA(CDistributionPage1)
	enum { IDD = IDD_DISTRIBUTION1_DIALOG };
	COXMaskedEdit	m_cDayEdit;
	CButton	m_cMasterCheck;
	CButton	m_cDayCheck;
	CDistributeTreeCtrl	m_cTreeCtrl;
	CComboBox	m_cSourceSiteCombo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDistributionPage1)
	public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
	void UpdateFromDir();	
	void UpdateToDir();	

protected:
	// Generated message map functions
	//{{AFX_MSG(CDistributionPage1)
	virtual BOOL OnInitDialog();
	afx_msg void OnUpdateControls();
	afx_msg void OnDistDayCheck();
	afx_msg void OnDeltaposDistDaySpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnUpdateDistDayEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CStringList	m_strList;
	CStringList	m_destList;
	CImageList	m_imageList;
	BOOL		m_bInInit;
	BOOL		m_bUseDay;
	int			m_nDay;
};

/////////////////////////////////////////////////////////////////////////////
// CDistributionPage2 dialog

class CDistributionPage2 : public CResizablePage
{
	DECLARE_DYNCREATE(CDistributionPage2)

// Construction
public:
	CDistributionPage2();
	~CDistributionPage2();

// Dialog Data
	//{{AFX_DATA(CDistributionPage2)
	enum { IDD = IDD_DISTRIBUTION2_DIALOG };
	CDistributeTreeCtrl	m_cTreeCtrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDistributionPage2)
	public:
	virtual BOOL OnSetActive();
	virtual LRESULT OnWizardNext();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDistributionPage2)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void CheckChildItem(HTREEITEM*, bool);
	void PopulateTree();

	CImageList	m_imageList;
	CStringList m_mstrList;
	CStringList m_dstrList;
	BOOL		m_bInInit;
};

/////////////////////////////////////////////////////////////////////////////
// CDistributionPage3 dialog

class CDistributionPage3 : public CResizablePage
{
	DECLARE_DYNCREATE(CDistributionPage3)

// Construction
public:
	CDistributionPage3();
	~CDistributionPage3();

// Dialog Data
	//{{AFX_DATA(CDistributionPage3)
	enum { IDD = IDD_DISTRIBUTION3_DIALOG };
	CButton	m_cFilterNameCheck;
	CButton	m_cFilterDateCheck;
	CEdit	m_cFileEdit;
	CmEdit	m_cDateTimeEdit;
	CButton	m_cAllFilesCheck;
	CString	m_strDateTimeEdit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDistributionPage3)
	public:
	virtual BOOL OnSetActive();
	virtual BOOL OnWizardFinish();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDistributionPage3)
	virtual BOOL OnInitDialog();
	afx_msg void OnAllFilesCheck();
	afx_msg void OnFilterDateCheck();
	afx_msg void OnFilterNameCheck();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void	EnumerateFolders();
	int		Wildcmp(LPCTSTR, LPCTSTR);

	CString	m_strDateTime;
	CString	m_strCurDir;
};

/////////////////////////////////////////////////////////////////////////////
// CDistributeSheet

class CDistributeSheet : public CResizableSheet
{
	DECLARE_DYNAMIC(CDistributeSheet)

// Construction
public:
	CDistributeSheet();
	CDistributeSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CDistributeSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:
	CString*			m_sourcePath;
	CStringList*		m_destList;
	CStringList*		m_fileList;
	CStringList			m_dirList;
	CStringList			m_filesToDistribute;
	CDWordArray*		m_dWordArray;

	CDistributionPage1	m_pp1;
	CDistributionPage2	m_pp2;
	CDistributionPage3	m_pp3;
	BOOL				m_bUseDay;
	BOOL				m_bUseMaster;
	int					m_nDay;

// Operations
public:
	void SourcePath(CString* s)			{ m_sourcePath	= s; }
	void DestinationList(CStringList* l){ m_destList	= l; }
	void FileList(CStringList* l)		{ m_fileList	= l; }
	void FileSizeList(CDWordArray* l)	{ m_dWordArray	= l; }


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDistributeSheet)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CDistributeSheet();

	// Generated message map functions
protected:
	virtual BOOL OnInitDialog();
	//{{AFX_MSG(CDistributeSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CDistributionFinish dialog

class CDistributionFinish : public CDialog
{
// Construction
public:
	CDistributionFinish(CWnd* pParent = NULL);   // standard constructor

// Attributes
public:
	CString*		m_sourcePath;
	CStringList*	m_destList;
	CStringList*	m_fileList;
	CDWordArray*	m_dWordArray;

// Operations
public:
	void SourcePath(CString* s)			{ m_sourcePath = s; }
	void DestinationList(CStringList* l){ m_destList = l; }
	void FileList(CStringList* l)		{ m_fileList = l; }
	void FileSizeList(CDWordArray* l)	{ m_dWordArray = l; }

// Dialog Data
	//{{AFX_DATA(CDistributionFinish)
	enum { IDD = IDD_DISTRIBUTION_OP_DIALOG };
	CDistributeTreeCtrl	m_cTreeCtrl;
	CStatic	m_cFileSource;
	CStatic	m_cNumOfFiles;
	CStatic	m_cDestination;
	CStatic	m_cNumOfBytes;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDistributionFinish)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	void DistributeSheet(CDistributeSheet* ps) { m_pPS = ps; }

protected:

	// Generated message map functions
	//{{AFX_MSG(CDistributionFinish)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangingTree(NMHDR* pNMHDR, LRESULT* pResult);
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	void GetSelectedFiles();
	int GetByteCount();

	TV_INSERTSTRUCT		m_tvinsert;
	CImageList			m_imageList;
	CStringList			m_selectedFiles;
	CDistributeSheet*	m_pPS;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DISTRIBUTION_H__6DF7D1C9_CCD0_453E_92E3_4079C1E93D40__INCLUDED_)
