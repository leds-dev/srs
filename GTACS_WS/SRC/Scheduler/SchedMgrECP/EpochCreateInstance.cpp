// @(#) epochnt source EpToolbox/EpochCreateInstance.cpp 1.3 00/08/02 13:32:16

// EpochCreateInstance.cpp: implementation of the CEpochCreateInstance class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EpochCreateInstance.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEpochCreateInstance::CEpochCreateInstance(LPCTSTR a_pstrHostName)
{
	HKEY  ourKey;
	DWORD ourType;
	DWORD ourDataSize = 1024;
	
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE,_T("SOFTWARE\\ISI"),0,KEY_QUERY_VALUE,&ourKey) == 0)
	{
		if (RegQueryValueEx(ourKey,a_pstrHostName,0,&ourType,(BYTE*)m_strHostName,&ourDataSize) != 0)
			m_strHostName[0] = 0;

		::RegCloseKey(ourKey);
	}

} // CEpochCreateInstance::CEpochCreateInstance

CEpochCreateInstance::~CEpochCreateInstance()
{
}

// Create
HRESULT CEpochCreateInstance::Create(REFCLSID a_ClsID,REFIID a_IID,LPVOID* a_pI)
{
	if (!m_strHostName[0] ||
		_tcsicmp(m_strHostName,_T("localhost")) == 0)
		return CoCreateInstance(a_ClsID,NULL,CLSCTX_LOCAL_SERVER,a_IID,a_pI);
	else
	{
		COSERVERINFO serverInfo = { 0, m_strHostName, 0, 0 };
		MULTI_QI qi             = { &a_IID, NULL, 0 };
		HRESULT rc = CoCreateInstanceEx(a_ClsID,NULL,CLSCTX_REMOTE_SERVER,&serverInfo,1,&qi);
		*a_pI = qi.pItf;

		return rc;

	} // else : Remote create

} // CEpochCreateInstance::Create
