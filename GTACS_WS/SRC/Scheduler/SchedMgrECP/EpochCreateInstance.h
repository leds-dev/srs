// @(#) epochnt source EpToolbox/EpochCreateInstance.h 1.1 00/07/06 10:19:55

// EpochCreateInstance.h: interface for the CEpochCreateInstance class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EPOCHCREATEINSTANCE_H__F2AE9C5D_5107_11D4_89DF_00B0D0285FE7__INCLUDED_)
#define AFX_EPOCHCREATEINSTANCE_H__F2AE9C5D_5107_11D4_89DF_00B0D0285FE7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CEpochCreateInstance  
{
	/// Host name
	TCHAR m_strHostName[1024];

public:
	CEpochCreateInstance(LPCTSTR a_pstrHostName);
	virtual ~CEpochCreateInstance();

	/// Create
	HRESULT Create(REFCLSID,REFIID,LPVOID*);

}; // CEpochCreateInstance

#endif // !defined(AFX_EPOCHCREATEINSTANCE_H__F2AE9C5D_5107_11D4_89DF_00B0D0285FE7__INCLUDED_)
