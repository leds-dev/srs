/**
/////////////////////////////////////////////////////////////////////////////
// FinishDialog.cpp : implementation of the FinishDialog class.            //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// FinishDialog.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class shows a list of operations that the user has selected from
// various wizards.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"
#include "FinishDialog.h"
#include "AfxPriv.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		OutputSaveAs
//	Description :	A callback procedure that reads the rich edit
//					control contents menu button.
//
//	Return :
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
DWORD CALLBACK CFinishRichEditCtrl::OutputSaveAs(DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb)
{
   CFile* pFile = (CFile*) dwCookie;

   pFile->Write(pbBuff, cb);
   *pcb = cb;

   return 0;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFinishRichEditCtrl::CFinishRichEditCtrl
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CFinishRichEditCtrl::CFinishRichEditCtrl(){}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFinishRichEditCtrl::~CFinishRichEditCtrl
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CFinishRichEditCtrl::~CFinishRichEditCtrl(){}

BEGIN_MESSAGE_MAP(CFinishRichEditCtrl, CRichEditCtrl)
//{{AFX_MSG_MAP(CFinishRichEditCtrl)
// NOTE - the ClassWizard will add and remove mapping macros here.
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFinishRichEditCtrl message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFinishRichEditCtrl::SetRTF
//	Description :	This routine puts the RTF string sRTF into the 
//					rich edit control.
//
//	Return :		void	-
//	Parameters :
//		CString sRTF	-	text string from edit control.	
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CFinishRichEditCtrl::SetRTF(CString sRTF)
{
	// Put the RTF string sRTF into the rich edit control.

	// Read the text in
	EDITSTREAM es;
	es.dwError = 0;
	es.pfnCallback = CBStreamIn;
	es.dwCookie = (DWORD) &sRTF;
	StreamIn(SF_RTF | SFF_SELECTION, es);	// Do it.
}

DWORD CALLBACK CFinishRichEditCtrl::CBStreamIn(DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb)
{
/*	
	This function taken from CodeGuru.com
	http://www.codeguru.com/richedit/rtf_string_streamin.shtml
	Zafir Anjum
*/
	CString *pUnicode = (CString *) dwCookie;
	int nLen = ((CString *)dwCookie)->GetLength();

	USES_CONVERSION;
	
	LPCSTR pstr = T2CA(*pUnicode);

	// Must be less than 4096 bytes otherwise multiple callbacks will be made.
	// We don't account for that!
	if (nLen < cb)
	{
		*pcb = nLen;
		memcpy(pbBuff, pstr, *pcb);
	}
	else
	{
		ASSERT(FALSE);
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// CFinishDialog dialog

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::CFinishDialog
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//		CWnd* pWnd	-	pointer to the parent window object.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CFinishDialog::CFinishDialog(CWnd* pWnd /*NULL*/)
: CResizableDialog(CFinishDialog::IDD, pWnd)
{
	//{{AFX_DATA_INIT(CFinishDialog)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//
//  Returns :		void	-  
//
//  Parameters : 
//           CDataExchange* pDX
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFinishDialog)
	DDX_Control(pDX, IDC_FINISH_EDIT, m_cFinishRTF);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFinishDialog, CResizableDialog)
	//{{AFX_MSG_MAP(CFinishDialog)
	ON_BN_CLICKED(IDC_FINISH_SAVEAS_BUTTON, OnFinishSaveAsButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OnInitDialog
//  Description :	This routine create the FinishDialog dialog window.  
//
//  Returns :		BOOL	-	TRUE if Successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CFinishDialog::OnInitDialog() 
{
	CResizableDialog::OnInitDialog();
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	AddAnchor(IDC_FINISH_GROUP, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_FINISH_LABEL_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_FINISH_EDIT, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDOK, BOTTOM_RIGHT, BOTTOM_RIGHT);
	AddAnchor(IDCANCEL, BOTTOM_RIGHT, BOTTOM_RIGHT);
	AddAnchor(IDC_FINISH_SAVEAS_BUTTON, BOTTOM_LEFT, BOTTOM_LEFT);

	EnableSaveRestore(_T("Resizable Sheets"), _T("Update Finish"));
	
	CString strText;
	CString strRTF;
	CString strValue;
	CString strCRLF			= _T("\x0D\x0A");
	strText.Empty();
	strText += _T("The following actions will be executed.  To accept these actions select OK.  If") + strCRLF;
	strText += _T("changes need to be made, select cancel and make the necessary modifications.") + strCRLF;
	GetDlgItem(IDC_FINISH_LABEL_EDIT)->SetWindowText(strText);

	strText.Empty();
	
	switch (pSheet->GetFunc())
	{
		case nValCLS	: 
		{
			strText += strBB + strTB + _T("Component Level Schedule Validation:") +	strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputCLS();
			break;
		}
		case nValSTOL	: 
		{
			strText += strBB + strTB + _T("STOL Procedure Validation:") + strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputSTOL();
			break;
		}

		case nValRTCS	: 
		{
			strText += strBB + strTB + _T("RTCS Procedure Validation:") + strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputRTCS();
			break;
		}

		case nValRTCSSet	: 
		{
			strText += strBB + strTB + _T("RTCS Set Validation:") + strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputRTCSSet();
			break;
		}
		case nUpdSTOL	:
		{
			strText += strBB + strTB + _T("Updating a Schedule:") + strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputIMCSet();
			OutputFrame();
			OutputStar();
			OutputRTCS();
			OutputSTOL();
			break;
		}
		case nPackage	:
		{
			strText += strBB + strTB + _T("Generate a complete Daily Schedule Package:") + strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputCLS();
			OutputIMCSet();
			OutputFrame();
			OutputStar();
			OutputRTCS();
			OutputSTOL();
			break;
		}
		case nReqIMC	:
		{
			strText += strBB + strTB + _T("IMC Set Request:") + strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputOATS();
			break;
		}
		case nReqSFCS	:
		{
			strText += strBB + strTB + _T("IMC Scale Factor Calibration Schedule Request:") + strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputOATS();
			break;
		}
		case nReqEclipse:
		{
			strText += strBB + strTB + _T("Generate an Eclipse Report:") + strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputOATS();
			break;
		}
		case nReqIntrusion:
		{
			strText += strBB + strTB + _T("Generate an Intrusion Report:") + strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputOATS();
			break;
		}
		case nGenSTOL :
		{
			strText += strBB + strTB + _T("Generate a STOL Procedure:") + strBE + strTE + str2PE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			OutputCLS();
			break;
		}
		default			: ASSERT(FALSE);	break;
	}
	
	strText.Empty();

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OutputCLS
//  Description :	This routine shows Component Level Schedule Parameters.  
//
//  Returns :		void	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OutputCLS()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CString strRTF;
	CString strText;

	strText += strTab + strBB + _T("Component Level Schedule Parameters:") + strBE + strPE;
	strRTF = rtfPrefix + strText + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
	strText.Empty();
	
	CString strCLSFile = pSheet->GetFull();
	strCLSFile.Replace(_T("\\"), _T("\\\\"));

	CString strLoc;
	if (pSheet->GetCLSPage()->GetVal())
	{
		switch (pSheet->GetCLSPage()->GetValLoc())
		{
			case GROUND		:	strLoc = _T("be Syntax Checked and Validated for ") + strBB +
										 _T("Ground") + strBE + _T(" execution:"); break;
			case ONBOARD	:	strLoc = _T("be Syntax Checked and Validated for ") + strBB + 
										 _T("Onboard") + strBE + _T(" execution:"); break;
			case BOTH		:	strLoc = _T("be Syntax Checked and Validated for ") + strBB + 
										 _T("Ground") + strBE + _T(" and ") + strBB +
										 _T("Onboard") + strBE + _T(" execution:");	break;
		}
	}
	else
		strLoc = strBB + _T("not") + strBE + _T(" be validated:");
	
	strText += str2Tab + _T("The following CLS file will ") +  strLoc + strPE;
	strText += str2TabBTab + strCLSFile + str2PE;


	if (pSheet->GetFunc() == nPackage)
	{
		CString strDate;
		strDate.Format(_T("%04d/%03d"), pSheet->GetCLSPage()->GetYear(), pSheet->GetCLSPage()->GetDOY());
		strText += str2Tab + _T("The schedule will be generated from the CLS for the following date:") + strPE;
		strText += str2TabBTab + strDate + str2PE;
	}
	
	strRTF = rtfPrefix + strText + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OutputSTOL
//  Description :	This routine shows STOL Procedure Parameters.  
//
//  Returns :		void	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OutputSTOL()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CString strRTF;
	CString strText;
	
	strText += strTab + strBB + _T("STOL Procedure Parameters:") + strBE + strPE;
	strRTF = rtfPrefix + strText + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
	strText.Empty();
	
	CString strOldFilename;
	CString strUpdateFilename;
	CString strBinFilename;
	CString strLoadFilename;

	switch (pSheet->GetFunc())
	{
		case nPackage:
		case nUpdSTOL:
		{
			if( (pSheet->GetFramePage())->GetUse()	||
				(pSheet->GetStarPage())->GetUse()	||
				(pSheet->GetRTCSPage())->GetUse() )
			{
				strBinFilename		= pSheet->MakeFilepath(nFolderSTOL, nBinary,TRUE);
				strLoadFilename		= pSheet->MakeFilepath(nFolderSTOL, nSTOL,	TRUE);
				strUpdateFilename	= pSheet->MakeFilepath(nFolderSTOL, nSTOL,	FALSE);
			}
			else
			{
				CString	strExtS;
				CString strExtB;
				COXUNC	unc(pSheet->GetOrig());

				if(GetAppRegMisc()->m_bShowExt)
				{
					GetAppDocTemplate(nSTOL)->GetDocString(strExtS, CDocTemplate::filterExt);
					GetAppDocTemplate(nBinary)->GetDocString(strExtB, CDocTemplate::filterExt);
				}

				unc.File()			= unc.Base() + _T("_load") + strExtB;
				strBinFilename		= unc.Full();
				unc.File()			= unc.Base() + strExtS;
				strLoadFilename		= unc.Full();
				strUpdateFilename	= pSheet->GetOrig();
			}
				
			strOldFilename.Empty();
			break;
		}
		case nValSTOL:
		{
			strBinFilename.Empty();
			strLoadFilename.Empty();
			strUpdateFilename = pSheet->GetFull();
			strOldFilename.Empty();
			break;
		}
		default:
		{
			strBinFilename.Empty();
			strLoadFilename.Empty();
			strUpdateFilename.Empty();
			strOldFilename.Empty();
			break;
		}
	}
	
	strOldFilename.Replace(_T("\\"), _T("\\\\"));
	strUpdateFilename.Replace(_T("\\"), _T("\\\\"));
	strBinFilename.Replace(_T("\\"), _T("\\\\"));;
	strLoadFilename.Replace(_T("\\"), _T("\\\\"));

	if ((pSheet->GetFunc() == nUpdSTOL) ||
		(pSheet->GetFunc() == nPackage))
	{
		strText.Empty();

		if( !(pSheet->GetFramePage())->GetUse()	&&
			!(pSheet->GetStarPage())->GetUse()	&&
			!(pSheet->GetRTCSPage())->GetUse() )
		{
			COXUNC	unc(pSheet->GetOrig());
			strText = str2Tab + _T("There will be no Update processing for the generated STOL proc ") + unc.File() + _T(".") + strPE;
		}
		else
		{
			strText += str2Tab + _T("The following STOL Procedure file will contain the updates:") +  strPE;
			strText += str2TabBTab + strUpdateFilename + str2PE;
		}

		strRTF = rtfPrefix + strText + rtfPostfix;
		m_cFinishRTF.SetRTF (strRTF);
	}
	
	// Output the validation data if necessary
	if ((pSheet->GetFunc() == nValSTOL) ||
		(pSheet->GetFunc() == nUpdSTOL)	||
		(pSheet->GetFunc() == nPackage))
	{
		strText.Empty();
		CString strLoc;
		if (pSheet->GetSchedPage()->GetVal())
		{
			switch (pSheet->GetSchedPage()->GetValLoc())
			{
				case GROUND		:	strLoc = _T("be Syntax Checked and Validated for ") + strBB + 
											 _T("Ground") + strBE + _T(" execution:");	break;
				case ONBOARD	:	strLoc = _T("be Syntax Checked and Validated for  ") + strBB + 
											 _T("Onboard") + strBE + _T(" execution:");	break;
				case BOTH		:	strLoc = _T("be Syntax Checked and Validated for ") + strBB + 
											 _T("Ground") + strBE + _T(" and ") + strBB + 
											 _T("Onboard") + strBE + _T(" execution:");	break;
				default			:	strLoc = _T("be Syntax Checked only.");
			}
		}
		else
			strLoc = strBB + _T("not") + strBE + _T(" be validated:");
				
		strText+= str2Tab + _T("The following STOL Procedure file will ") +  strLoc + strPE;
		strText+= str2TabBTab + strUpdateFilename + str2PE;
		strRTF	= rtfPrefix + strText + rtfPostfix;
		m_cFinishRTF.SetRTF (strRTF);
	}

	// Output the generation data if necessary
	if ((pSheet->GetFunc() == nUpdSTOL) ||
		(pSheet->GetFunc() == nPackage))
	{
		strText.Empty();
		if (pSheet->GetSchedPage()->GetGen())
		{
			strText += str2Tab + _T("The following upload files will be generated:") + strPE;
			strText += str2TabBTab + strBinFilename + strPE;
			strText += str2TabBTab + strLoadFilename + str2PE;
		}
		else
		{
			strText += str2Tab + _T("Upload files will ") +  strBB + _T("not") + strBE +
				_T(" be generated.") + str2PE;
		}
		strRTF = rtfPrefix + strText + rtfPostfix + rtfPostfix;
		m_cFinishRTF.SetRTF (strRTF);
	}	
	// Output the generation data if necessary
	
	if (((pSheet->GetFunc() == nUpdSTOL)		||
		(pSheet->GetFunc() == nValSTOL)			||
		(pSheet->GetFunc() == nPackage))		&&
		(pSheet->GetSchedPage()->GetRTCSVal()	&&
		 pSheet->GetSchedPage()->GetVal()		&&
		(pSheet->GetSchedPage()->GetValLoc()	!= GROUND)) ||
		((pSheet->GetFunc() == nValCLS)			&&
		(pSheet->GetSchedPage()->GetRTCSVal())))
	{
		CString strSetFilename = pSheet->GetSchedPage()->GetRTCSSetFilename();
		strSetFilename.Replace(_T("\\"), _T("\\\\"));
		strText.Empty();
		strText += str2Tab + _T("The following RTCS Set file will be used during validation:") + strPE;
		strText += str2TabBTab + strSetFilename + strPE;
		strRTF = rtfPrefix + strText + rtfPostfix + rtfPostfix;
		m_cFinishRTF.SetRTF (strRTF);
	}	
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OutputIMCSet
//  Description :	This routine shows IMC Set Parameters.  
//
//  Returns :		void	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OutputIMCSet()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CString strRTF;
	CString strText;
	CString strImg(_T(" for IMAGER"));
	CString strSnd(_T(" for SOUNDER"));

	strText+= strTab + strBB + _T("IMC Set Parameters") + strBE + strPE;
	strRTF	= rtfPrefix + strText + rtfPostfix;

	m_cFinishRTF.SetRTF(strRTF);
	strText.Empty();

//	if( !(pSheet->GetFramePage())->GetUse() && !(pSheet->GetStarPage())->GetUse() )
	if( !(pSheet->GetFramePage())->GetUse() && !(pSheet->GetStarPage())->GetUse() || 
		((pSheet->GetFramePage())->GetUse()	&& !(pSheet->GetFramePage())->GetCreateNew()) && 
		!(pSheet->GetStarPage())->GetUse() )
		strText += str2Tab + _T("There will be no IMC set used during the update:") + strPE;
	else
	{
		strText += str2Tab + _T("IMC processing is ");

		if( pSheet->GetIMCSetPage()->GetUseImgIMC() )
			strText += strBB + _T("Enabled") + strBE + strImg + _T(".") + strPE;
		else
			strText += strBB + _T("Disabled") + strBE + strImg + _T(".") + strPE;
	
		strText += str2Tab + _T("IMC processing is ");

		if( pSheet->GetIMCSetPage()->GetUseSndIMC() )
			strText += strBB + _T("Enabled") + strBE + strSnd + _T(".") + str2PE;
		else
			strText += strBB + _T("Disabled") + strBE + strSnd + _T(".") + str2PE;
	
		strText += str2Tab + _T("The following IMC set will be used during the update:") + strPE;
		strText += str2TabBTab + _T("IMC Set ID - ") + pSheet->GetIMCSetPage()->GetIMC() + str2PE;
	}

	strRTF = rtfPrefix + strText + rtfPostfix + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OutputStar
//  Description :	This routine shows Star Parameters.  
//
//  Returns :		void	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OutputStar()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CString strRTF;
	CString strText;

	strText += strTab + strBB + _T("Star Parameters:") + strBE + strPE;
	strRTF = rtfPrefix + strText + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
	strText.Empty();
	
	if (pSheet->GetStarPage()->GetUse())
	{
		strText += str2Tab + _T("A new Star file will be created and the STOL procedure updated with it.") + strPE;
		CString strStarFilename = pSheet->MakeFilepath(nFolderStar, nStar, FALSE);
		strStarFilename.Replace(_T("\\"), _T("\\\\"));
		strText += str2TabBTab + strStarFilename + str2PE;

		if (pSheet->GetStarPage()->GetGen())
		{
			CString strBinFilename	= pSheet->MakeFilepath(nFolderStar, nBinary,TRUE);
			CString strLoadFilename	= pSheet->MakeFilepath(nFolderStar, nSTOL,	TRUE);
			strBinFilename.Replace(_T("\\"), _T("\\\\"));;
			strLoadFilename.Replace(_T("\\"), _T("\\\\"));
			strText += str2Tab + _T("The following upload files will be generated.") + strPE;
			strText += str2TabBTab + strBinFilename + strPE;
			strText += str2TabBTab + strLoadFilename + str2PE;
		}
		else
		{
			strText += str2Tab + _T("Upload files will ") +  strBB + _T("not") + strBE + 
				_T(" be generated.") + str2PE;
		}
	}
	else
	{
		strText += str2Tab + _T("The STOL procedure will ") +  strBB + _T("not") + strBE +
			_T(" be updated for Stars.") + str2PE;
	}
	strRTF = rtfPrefix + strText + rtfPostfix + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OutputFrame
//  Description :	This routine shows Frame Parameters.  
//
//  Returns :		void	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OutputFrame()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CString strRTF;
	CString strText;
	
	strText += strTab + strBB + _T("Frame Parameters:") + strBE + strPE;
	strRTF = rtfPrefix + strText + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
	
	strText.Empty();
	if ((pSheet->GetFunc() == nUpdSTOL) ||
		(pSheet->GetFunc() == nPackage))
	{
		if (pSheet->GetFramePage()->GetUse())
		{
			if (pSheet->GetFramePage()->GetCreateNew())
			{
				strText += str2Tab + _T("A new Frame file will be created and the STOL procedure updated with it.") + strPE;
				CString strSectorFilename = pSheet->GetFramePage()->GetSectorFilename();
				strSectorFilename.Replace(_T("\\"), _T("\\\\"));
				CString strFrameFilename = pSheet->MakeFilepath(nFolderFrame, nFrame, FALSE);
				strFrameFilename.Replace(_T("\\"), _T("\\\\"));
				strText += str2TabBTab + strFrameFilename + strPE;
				strText += str2TabBTab + strSectorFilename + str2PE;

				if (pSheet->GetFramePage()->GetUseSRSO())
				{
					strText += str2Tab + _T("This update will be a ") + strBB + _T("SRSO") + strBE + 
						_T(" update an use the following parameters:") + strPE;
					strText += str2TabBTab + _T("Scan Frame Label	-  ") + GetAppRegSC()->m_strSRSOFrameLabel[GetAppRegSite()->m_nDefaultSC] + strPE;
					
					if( pSheet->GetSRSOPage()->GetUsePreDef() )
						strText += str2TabBTab + _T("Pre-defined Label	-  ") + pSheet->GetSRSOPage()->GetSRSOLabel() + str2PE;
					else
					{
						strText += str2TabBTab + _T("Manually Entered Coordinates:") + strPE;
						strText += strTab + str2TabBTab + _T("Coordinate Type	-  Lat/Long ") + strPE;
						// strText += (pSheet->GetSRSOPage()->GetUseLatLong() ? _T("Lat/Long") : _T("Line/Pixel")) + strPE;
						// strText += _T("Lat/Long") + strPE;
						strText += strTab + str2TabBTab + _T("N/S Center Point	-  ") + pSheet->GetSRSOPage()->GetSRSO_NS() + strPE;
						strText += strTab + str2TabBTab + _T("E/W Center Point	-  ") + pSheet->GetSRSOPage()->GetSRSO_EW() + strPE;
						strText += strTab + str2TabBTab + _T("Duration      	-  ") + pSheet->GetSRSOPage()->GetSRSODuration() + strPE;
						strText += strTab + str2TabBTab + _T("Aspect Ratio  	-  ") + pSheet->GetSRSOPage()->GetSRSORatio() + str2PE;
					}
				}
				else
				{
					strText += str2Tab + _T("This update will be a ") + strBB + _T("Normal") + strBE +
						_T(" update.") + str2PE;
				}

				if (pSheet->GetFramePage()->GetGen())
				{
					CString strBinFilename	= pSheet->MakeFilepath(nFolderFrame, nBinary,	TRUE);
					CString strLoadFilename	= pSheet->MakeFilepath(nFolderFrame, nSTOL,		TRUE);
					strBinFilename.Replace(_T("\\"), _T("\\\\"));;
					strLoadFilename.Replace(_T("\\"), _T("\\\\"));
					strText += str2Tab + _T("The following upload files will be generated.") + strPE;
					strText += str2TabBTab + strBinFilename + strPE;
					strText += str2TabBTab + strLoadFilename + str2PE;
				}
				else
				{
					strText += str2Tab + _T("Upload files will ") +  strBB + _T("not") + strBE +
						_T(" be generated.") + str2PE;
				}
			}
			else
			{
				strText += str2Tab + _T("The STOL procedure will be updated for Frames based upon the following Frame file:") + strPE;
				CString strFrameFilename = pSheet->GetFramePage()->GetFrameFilename();
				strFrameFilename.Replace(_T("\\"), _T("\\\\"));;
				strText += str2TabBTab + strFrameFilename + str2PE;
			}
		}
		else
		{
			strText += str2Tab + _T("The STOL procedure will ") +  strBB + _T("not") + strBE +
				_T(" be updated for Frames.") + str2PE;
		}
	}
	
	strRTF = rtfPrefix + strText + rtfPostfix + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OutputRTCS
//  Description :	This routine shows RTCS Parameters.  
//
//  Returns :		void	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OutputRTCS()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CString strRTF;
	CString strText;
	
	strText += strTab + strBB + _T("RTCS Parameters:") + strBE + strPE;
	strRTF = rtfPrefix + strText + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
	
	strText.Empty();
	CString strRTCSFile = pSheet->GetFull();
	strRTCSFile.Replace(_T("\\"), _T("\\\\"));

	if( pSheet->GetFunc() == nPackage || 
		pSheet->GetFunc() == nUpdSTOL )
	{
		if (pSheet->GetRTCSPage()->GetUse())
		{
			CString strMapFilename = pSheet->GetRTCSPage()->GetMapFilename();
			strMapFilename.Replace(_T("\\"), _T("\\\\"));
			strText += str2Tab + _T("The following Map file will be used during the update:") + strPE;
			strText += str2TabBTab + strMapFilename + str2PE;
		}
		else
		{
			strText += str2Tab + _T("The STOL procedure will ") +  strBB + _T("not") + strBE +
				_T(" be updated for RTCS.") + str2PE;
		}		
	}

	if (pSheet->GetRTCSPage()->GetGen())
	{
		CString strBinFilename	= pSheet->MakeFilepath(nFolderRTCS, nBinary,TRUE, FALSE);
		CString strLoadFilename	= pSheet->MakeFilepath(nFolderRTCS, nSTOL,	TRUE, FALSE);
		strBinFilename.Replace(_T("\\"), _T("\\\\"));;
		strLoadFilename.Replace(_T("\\"), _T("\\\\"));
		strText += str2Tab + _T("The following upload files will be generated.") + strPE;
		strText += str2TabBTab + strBinFilename + strPE;
		strText += str2TabBTab + strLoadFilename + str2PE;
	}
	else
	{
		strText += str2Tab + _T("Upload files will ") +  strBB + _T("not") + strBE +
			_T(" be generated.") + str2PE;
	}

	if (pSheet->GetFunc() == nValRTCS)
	{
		CString strLoc;
		if (pSheet->GetCLSPage()->GetVal())
		{
			strLoc = _T("be Syntax Checked and Validated for ") + strBB + 
					 _T("Onboard") + strBE + _T(" execution:"); 
		} else
			strLoc = strBB + _T("not") + strBE + _T(" be validated:");
		
		strText += str2Tab + _T("The following RTCS file will ") +  strLoc + strPE;
		strText += str2TabBTab + strRTCSFile + str2PE;
	}

	strRTF = rtfPrefix + strText + rtfPostfix + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OutputRTCSSet
//  Description :	This routine shows RTCSSet Parameters.  
//
//  Returns :		void	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OutputRTCSSet()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CString strRTF;
	CString strText;

	strText += strTab + strBB + _T("RTCS Set file Parameters:") + strBE + strPE;
	strRTF = rtfPrefix + strText + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
	strText.Empty();
	
	CString strRTCSSetFile = pSheet->GetFull();
	strRTCSSetFile.Replace(_T("\\"), _T("\\\\"));

	CString strLoc = _T("be Syntax Checked and Validated for ") + strBB + 
										 _T("Onboard") + strBE + _T(" execution:");
	strText += str2Tab + _T("The following RTCS set file will ") +  strLoc + strPE;
	strText += str2TabBTab + strRTCSSetFile + str2PE;
	
	strRTF = rtfPrefix + strText + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OutputOATS
//  Description :	This routine shows OATS Parameters.  
//
//  Returns :		void	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OutputOATS()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CString strRTF;
	CString strText;

	switch(pSheet->GetFunc())
	{
		case nReqIMC:
		{
			strText += strTab + strBB + _T("IMC Set Parameters") + strBE + strPE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			strText.Empty();
			strText += str2Tab + _T("The following IMC set will be used for this request:") + strPE;
			strText += str2TabBTab + _T("IMC Set ID - ") + pSheet->GetOutputPage()->GetIMCSet() + str2PE;
			strText += str2Tab + _T("The following IMC file will be generated.") + strPE;
			break;
		}
		case nReqSFCS:
		{
			CString strDuration;
			strDuration.Format(_T("%d"), pSheet->GetOATSPage()->GetDuration());
			strText += strTab + strBB + _T("IMC Scale Factor Calibration Schedule Parameters") + strBE + strPE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			strText.Empty();
			strText += str2Tab + _T("The following OATS parameters will be used for this request:") + strPE;
			strText += str2TabBTab + _T("Instrument	- ") + pSheet->GetOATSPage()->GetInstrument() + strPE;
			strText += str2TabBTab + _T("Start Time  - ") + pSheet->GetOATSPage()->GetStartTime() + strPE;
			strText += str2TabBTab + _T("Duration    - ") + strDuration + str2PE;
			strText += str2Tab + _T("The following IMC Scale Factor Calibration Schedule file will be generated.") + strPE;
			break;
		}
		case nReqEclipse:
		{
			CString strDuration;
			strDuration.Format(_T("%d"), pSheet->GetOATSPage()->GetDuration());
			strText += strTab + strBB + _T("Eclipse Report Parameters") + strBE + strPE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			strText.Empty();
			strText += str2Tab + _T("The following OATS parameters will be used for this request:") + strPE;
			strText += str2TabBTab + _T("Start Time  - ") + pSheet->GetOATSPage()->GetStartTime() + strPE;
			strText += str2TabBTab + _T("Duration    - ") + strDuration + str2PE;
			strText += str2Tab + _T("The following Eclipse Report file will be generated.") + strPE;
			break;
		}
		case nReqIntrusion:
		{
			CString strDuration;
			strDuration.Format(_T("%d"), pSheet->GetOATSPage()->GetDuration());
			strText += strTab + strBB + _T("Intrusion Report Parameters") + strBE + strPE;
			strRTF = rtfPrefix + strText + rtfPostfix;
			m_cFinishRTF.SetRTF (strRTF);
			strText.Empty();
			strText += str2Tab + _T("The following OATS parameters will be used for this request:") + strPE;
			strText += str2TabBTab + _T("Start Time  - ") + pSheet->GetOATSPage()->GetStartTime() + strPE;
			strText += str2TabBTab + _T("Duration    - ") + strDuration + str2PE;
			strText += str2Tab + _T("The following Intrusion Report file will be generated.") + strPE;
			break;
		}
		default: break;
	}

	CString strFrameFilename = pSheet->GetOutputPage()->GetStrPath();
	strFrameFilename.Replace(_T("\\"), _T("\\\\"));;
	strText += str2TabBTab + strFrameFilename + str2PE;
		
	strRTF = rtfPrefix + strText + rtfPostfix + rtfPostfix;
	m_cFinishRTF.SetRTF (strRTF);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OnFinishSaveAsButton
//  Description :	This routine saves all choosen operations displayed 
//					to a file.  
//
//  Returns :		void	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OnFinishSaveAsButton() 
{
//	AfxMessageBox(_T("The Save As... functions has not been implemented yet."));
	// m_cFinishRTF
//	CString strText;
	CString strExt;
	CString strFilter;
	GetAppDocTemplate(nRpt)->GetDocString(strExt,	CDocTemplate::filterExt);
	GetAppDocTemplate(nRpt)->GetDocString(strFilter, CDocTemplate::filterName);
	strFilter = strFilter + _T("|*") + strExt + _T("||");	
	CFileDialog dlg(FALSE, strExt, _T("Update"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, strFilter);
	 if (dlg.DoModal()) {
	   // OutputSaveAs(GetAppOutputBar()->GetBuildList(), dlg.GetPathName());
	   CString strFileName = dlg.GetPathName(); 
 	   CFile cFile(dlg.GetPathName(), CFile::modeCreate|CFile::modeWrite);
	   EDITSTREAM es;
	   es.dwCookie = (DWORD) &cFile;
	   es.pfnCallback = m_cFinishRTF.OutputSaveAs; 
	   m_cFinishRTF.StreamOut(SF_TEXT, es);
	 }
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OnOK
//  Description :	This routine closes the dialog window.  
//
//	Return :		void	-
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OnOK() 
{
	CResizableDialog::OnOK();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFinishDialog::OnCancel
//  Description :	This routine closes the dialog window.  
//
//	Return :		void	-
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CFinishDialog::OnCancel() 
{
	((CGenUpdSheet*)GetParent())->ShowWindow(SW_SHOW);

	CResizableDialog::OnCancel();
}







  
