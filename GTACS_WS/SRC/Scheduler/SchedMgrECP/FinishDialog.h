#if !defined(AFX_FINISHDIALOG_H__3D27A6E2_0D80_11D5_9A63_0003472193C8__INCLUDED_)
#define AFX_FINISHDIALOG_H__3D27A6E2_0D80_11D5_9A63_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FinishDialog.h : header file
//


class CFinishRichEditCtrl : public CRichEditCtrl
{
public:
	CFinishRichEditCtrl();
	~CFinishRichEditCtrl();
	static DWORD CALLBACK OutputSaveAs(DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG *pcb);

public:
	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFinishRichEditCtrl)
//	virtual BOOL Create (DWORD in_dwStyle, const RECT& in_rcRect, CWnd* in_pParentWnd, UINT in_nID);
	//}}AFX_VIRTUAL
	
	// Implementation
	void SetRTF(CString sRTF);
	// Generated message map functions
protected:
	//{{AFX_MSG(CFinishRichEditCtrl)
	// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	
	DECLARE_MESSAGE_MAP()
private:
	static DWORD CALLBACK CBStreamIn(DWORD dwCookie, LPBYTE pbBuff, LONG cb, LONG* pcb);
};

/////////////////////////////////////////////////////////////////////////////
// CFinishDialog dialog

class CFinishDialog : public CResizableDialog
{
// Construction
public:
	CFinishDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFinishDialog)
	enum { IDD = IDD_FINISH_DIALOG };
	CFinishRichEditCtrl	m_cFinishRTF;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFinishDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFinishDialog)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnFinishSaveAsButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void OutputCLS();
	void OutputSTOL();
	void OutputStar();
	void OutputFrame();
	void OutputRTCS();
	void OutputRTCSSet();
	void OutputIMCSet();
	void OutputOATS();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FINISHDIALOG_H__3D27A6E2_0D80_11D5_9A63_0003472193C8__INCLUDED_)
