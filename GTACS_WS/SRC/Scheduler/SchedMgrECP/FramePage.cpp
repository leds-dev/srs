/**
/////////////////////////////////////////////////////////////////////////////
// FramePage.cpp : implementation of the FramePage class.                  //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
// MSD (06/12) - PR000446 - Default options during schedupd.			   //
// Revision History:													   //
// Manan Dalal - July-August 2014										   //
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   //
// Release in Build16.2													   //
/////////////////////////////////////////////////////////////////////////////

// FramePage.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class will allow the user to choose frame parameters for updating
// a schedule for frames.
//
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"
#include "FinishDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFramePage property page

IMPLEMENT_DYNCREATE(CFramePage, CResizablePage)

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFramePage::CFramePage
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CFramePage::CFramePage() : CResizablePage(CFramePage::IDD)
{
	//{{AFX_DATA_INIT(CFramePage)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFramePage::~CFramePage
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CFramePage::~CFramePage()
{
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFramePage::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is
//					generated and maintained by the code wizard.
//
//  Returns :		void	-
//
//  Parameters :
//           CDataExchange* pDX
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CFramePage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFramePage)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFramePage, CResizablePage)
	//{{AFX_MSG_MAP(CFramePage)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_FRAME_UPD_CHECK, OnUpdateControls)
	ON_EN_UPDATE(IDC_FRAME_OLD_EDIT, OnUpdateFrameOldEdit)
	ON_BN_CLICKED(IDC_FRAME_SECTOR_BUTTON, OnFrameSectorButton)
	ON_BN_CLICKED(IDC_FRAME_OLD_BUTTON, OnFrameOldButton)
	ON_BN_CLICKED(IDC_FRAME_RSO_CHECK, OnUpdateControls)
	ON_BN_CLICKED(IDC_FRAME_GEN_CHECK, OnUpdateControls)
	ON_BN_CLICKED(IDC_FRAME_NEW_RADIO, OnUpdateControls)
	ON_BN_CLICKED(IDC_FRAME_OLD_RADIO, OnUpdateControls)
	ON_BN_CLICKED(IDC_SRSO_FRAME_CHECK, OnUpdateControls)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::Init
//  Description :	This routine initializes the member variables based on
//					the passed parameters.
//
//  Returns :		void	-
//
//  Parameters :
//		BOOL bFirstWizPage	-	first display page.
//		BOOL bLastWizPage	-	last display page.
//		BOOL bGen			-	generate frame updates.
//		BOOL bUseDef		-   use defaults if TRUE.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFramePage::Init(BOOL bFirstWizPage, BOOL bLastWizPage, BOOL bGen, BOOL bUseDef)
{
	m_bFirstWizPage		= bFirstWizPage;
	m_bLastWizPage		= bLastWizPage;
//	m_bGen				= bGen;
//	m_bUse				= TRUE;
//	m_bUseSRSO			= FALSE;
//	m_bCreateNew		= FALSE;
	m_bSchedUpd			= bUseDef;
	//PR00446: MSD (06/12) - Default to always update for frames, IF Schedule is being updated.
	// Ignore registry values/user defaults at this point.
	// Modified so the default state of the check boxes is retrieved from the registry.
	if(bUseDef) {
		m_bGen = nRegMakeSchedFrmGenDefValue;
		m_bUse = nRegMakeSchedFrmUpdateDefValue;
	} else {
		m_bGen				= (BOOL)GetAppRegMakeSched()->m_nFrmGen;
		m_bUse				= (BOOL)GetAppRegMakeSched()->m_nFrmUpdate;
	}
	m_bUseSRSO			= (BOOL)GetAppRegMakeSched()->m_nFrmSRSO;
	m_bUseRSO			= (BOOL)GetAppRegMakeSched()->m_nFrmRSO;
	m_bCreateNew		= (BOOL)GetAppRegMakeSched()->m_nNewFrmTbl;
	m_strSRSOLabel		= _T("Label");
	m_strSRSOFrame		= _T("Frame");

//	int	nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int	nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	CString strExt;

	GetAppDocTemplate(nSector)->GetDocString(strExt, CDocTemplate::filterExt);
//	m_strSectorFilename.Format(_T("\\\\%s\\%s\\%s\\%s\\%s%s"),
//		GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
	m_strSectorFilename.Format(_T("%s\\%s\\%s\\%s\\%s\\%s%s"),
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],		// SC Specific Directory
		GetAppRegFolder()->m_strDir[nFolderFrame],					// Sector (Master Frame) directory
		GetAppRegMakeSched()->m_strSecTblFileName,          		// Sector Table name from Registry
		strExt);													// Extension

	GetAppDocTemplate(nFrame)->GetDocString(strExt, CDocTemplate::filterExt);
//	m_strFrameFilename.Format(_T("\\\\%s\\%s\\%s\\%s\\%s%s"),
//		GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
	m_strFrameFilename.Format(_T("%s\\%s\\%s\\%s\\%s\\%s%s"),
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],		// SC Specific Directory
		GetAppRegFolder()->m_strDir[nFolderFrame],					// Master Frame directory
		GetAppRegMakeSched()->m_strFrmTblFileName,                  // Frame Table name from Registry
		strExt);													// Extension
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CFramePage::OnInitDialog
//  Description :	This routine creates the FramePage dialog window.
//
//	Return :		BOOL	-	TRUE if successful
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CFramePage::OnInitDialog()
{
	CResizablePage::OnInitDialog();
//	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	m_bInInit = TRUE;

	// Set all of the necessary anchors
	AddAnchor(IDC_FRAME_UPD_GROUP, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_FRAME_UPD_CHECK, TOP_LEFT, TOP_LEFT);

	AddAnchor(IDC_FRAME_TYPE_GROUP, TOP_LEFT, TOP_RIGHT);

	AddAnchor(IDC_FRAME_OLD_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_FRAME_OLD_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_FRAME_OLD_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_FRAME_OLD_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_FRAME_OLD_BUTTON, TOP_RIGHT, TOP_RIGHT);

	AddAnchor(IDC_FRAME_NEW_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_FRAME_NEW_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_FRAME_SECTOR_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_FRAME_SECTOR_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_FRAME_SECTOR_BUTTON, TOP_RIGHT, TOP_RIGHT);

	AddAnchor(IDC_FRAME_SRSO_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_STATIC_FRAME_CHECK, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SRSO_FRAME_CHECK, TOP_LEFT, TOP_RIGHT);

	AddAnchor(IDC_FRAME_GEN_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_FRAME_GEN_CHECK, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_FRAME_GEN_BIN_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_FRAME_GEN_BIN_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_FRAME_GEN_PROC_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_FRAME_GEN_PROC_STATIC, TOP_LEFT, TOP_RIGHT);

	AddAnchor(IDC_FRAME_LOC_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_FRAME_LOC_FRAME_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_FRAME_LOC_FRAME_STATIC, TOP_LEFT, TOP_RIGHT);

	GetDlgItem(IDC_FRAME_OLD_EDIT)->SetWindowText(m_strFrameFilename);
	GetDlgItem(IDC_FRAME_SECTOR_EDIT)->SetWindowText(m_strSectorFilename);

	// Add the images to the page
	CImageList imageList;
	imageList.Create(IDR_FRAME_MENUIMAGES, 16, 0, RGB (192,192,192));
	((CStatic*)GetDlgItem(IDC_FRAME_GEN_ICON))->SetIcon(imageList.ExtractIcon(2));
	((CStatic*)GetDlgItem(IDC_FRAME_UPD_ICON))->SetIcon(imageList.ExtractIcon(3));
//	((CStatic*)GetDlgItem(IDC_FRAME_SRSO_ICON))->SetIcon(imageList.ExtractIcon(5));

	GetDlgItem(IDC_FRAME_GEN_ICON)->BringWindowToTop();
	GetDlgItem(IDC_FRAME_UPD_ICON)->BringWindowToTop();
//	GetDlgItem(IDC_FRAME_SRSO_ICON)->BringWindowToTop();

	((CButton*)GetDlgItem(IDC_STATIC_FRAME_CHECK))->SetCheck(TRUE);

	m_bInInit = FALSE;

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::OnSetActive
//  Description :	This routine disables/enables, as appropriate, the
//					different window controls.
//
//  Returns :		BOOL	-	TRUE  if successful
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CFramePage::OnSetActive()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	// Update all of the check boxes with the proper status
	((CButton*)GetDlgItem(IDC_FRAME_UPD_CHECK))->SetCheck(m_bUse);
	((CButton*)GetDlgItem(IDC_FRAME_OLD_RADIO))->SetCheck(!m_bCreateNew);
	((CButton*)GetDlgItem(IDC_FRAME_NEW_RADIO))->SetCheck(m_bCreateNew);
	((CButton*)GetDlgItem(IDC_FRAME_RSO_CHECK))->SetCheck(m_bUseRSO);
	((CButton*)GetDlgItem(IDC_SRSO_FRAME_CHECK))->SetCheck(m_bUseSRSO);
	((CButton*)GetDlgItem(IDC_FRAME_GEN_CHECK))->SetCheck(m_bGen);

	// Update the various edit fields with their values
//	GetDlgItem(IDC_FRAME_SRSO_LABEL_EDIT)->SetWindowText(m_strSRSOLabel);
//	GetDlgItem(IDC_FRAME_SRSO_FRAME_EDIT)->SetWindowText(m_strSRSOFrame);

	// Update the visibility of the controls based upon the radio button positions
	// Update the filenames
	UpdateControls();


	// Setup the proper Wizard Buttons
	DWORD dWizFlags = PSWIZB_FINISH;
	if ((m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_FINISH;
	else if ((m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_NEXT;
	else if ((!m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_FINISH;
	else if ((!m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_NEXT;

	pSheet->SetWizardButtons(dWizFlags);

	return CResizablePage::OnSetActive();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::UpdateControls
//  Description :	This routine updates all window controls associated
//					with the CFramePage class.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFramePage::UpdateControls()
{
	OnUpdateControls();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::OnUpdateControls
//  Description :	This routine updates all window controls associated
//					with the CFramePage class.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFramePage::OnUpdateControls()
{
//	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	m_bUse = ((CButton*)GetDlgItem(IDC_FRAME_UPD_CHECK))->GetCheck();

	GetDlgItem(IDC_FRAME_OLD_RADIO)->EnableWindow(m_bUse);
	GetDlgItem(IDC_FRAME_NEW_RADIO)->EnableWindow(m_bUse);

	m_bCreateNew = ((CButton*)GetDlgItem(IDC_FRAME_NEW_RADIO))->GetCheck();

	GetDlgItem(IDC_FRAME_OLD_LABEL)->EnableWindow(m_bUse && !m_bCreateNew);
	GetDlgItem(IDC_FRAME_OLD_EDIT)->EnableWindow(m_bUse && !m_bCreateNew);
	GetDlgItem(IDC_FRAME_OLD_BUTTON)->EnableWindow(m_bUse && !m_bCreateNew);

	GetDlgItem(IDC_FRAME_SECTOR_LABEL)->EnableWindow(m_bUse && m_bCreateNew);
	GetDlgItem(IDC_FRAME_SECTOR_EDIT)->EnableWindow(m_bUse && m_bCreateNew);
	GetDlgItem(IDC_FRAME_SECTOR_BUTTON)->EnableWindow(m_bUse && m_bCreateNew);

	m_bUseRSO = ((CButton*)GetDlgItem(IDC_FRAME_RSO_CHECK))->GetCheck();
	GetDlgItem(IDC_FRAME_RSO_CHECK)->EnableWindow(m_bUse && !m_bCreateNew);

	m_bUseSRSO = ((CButton*)GetDlgItem(IDC_SRSO_FRAME_CHECK))->GetCheck();
	GetDlgItem(IDC_FRAME_SRSO_GROUP)->EnableWindow(m_bUse && m_bCreateNew);
	GetDlgItem(IDC_STATIC_FRAME_CHECK)->EnableWindow(m_bUse && m_bCreateNew);
	GetDlgItem(IDC_SRSO_FRAME_CHECK)->EnableWindow(m_bUse && m_bCreateNew);
//	GetDlgItem(IDC_FRAME_SRSO_CHECK)->EnableWindow(m_bUse && m_bCreateNew);
//	GetDlgItem(IDC_FRAME_SRSO_FRAME_LABEL)->EnableWindow(m_bUse && m_bUseSRSO && m_bCreateNew);
//	GetDlgItem(IDC_FRAME_SRSO_FRAME_EDIT)->EnableWindow(m_bUse && m_bUseSRSO && m_bCreateNew);
//	GetDlgItem(IDC_FRAME_SRSO_LABEL_LABEL)->EnableWindow(m_bUse && m_bUseSRSO && m_bCreateNew);
//	GetDlgItem(IDC_FRAME_SRSO_LABEL_EDIT)->EnableWindow(m_bUse && m_bUseSRSO && m_bCreateNew);

	m_bGen = ((CButton*)GetDlgItem(IDC_FRAME_GEN_CHECK))->GetCheck();
	GetDlgItem(IDC_FRAME_GEN_GROUP)->EnableWindow(m_bUse && m_bCreateNew);
	GetDlgItem(IDC_FRAME_GEN_CHECK)->EnableWindow(m_bUse && m_bCreateNew);
	GetDlgItem(IDC_FRAME_GEN_BIN_LABEL)->EnableWindow(m_bUse && m_bGen && m_bCreateNew);
	GetDlgItem(IDC_FRAME_GEN_BIN_STATIC)->EnableWindow(m_bUse && m_bGen && m_bCreateNew);
	GetDlgItem(IDC_FRAME_GEN_PROC_LABEL)->EnableWindow(m_bUse && m_bGen && m_bCreateNew);
	GetDlgItem(IDC_FRAME_GEN_PROC_STATIC)->EnableWindow(m_bUse && m_bGen && m_bCreateNew);

	GetDlgItem(IDC_FRAME_LOC_GROUP)->EnableWindow(m_bUse && m_bCreateNew);
	GetDlgItem(IDC_FRAME_LOC_FRAME_LABEL)->EnableWindow(m_bUse && m_bCreateNew);
	GetDlgItem(IDC_FRAME_LOC_FRAME_STATIC)->EnableWindow(m_bUse && m_bCreateNew);

	// PR00446: MSD - Only save back to registry if this isn't SchedUpd.
	// Save off checked values to Registry.
	if(!m_bSchedUpd) {
		GetAppRegMakeSched()->m_nFrmRSO		= (int)m_bUseRSO;
		GetAppRegMakeSched()->m_nFrmSRSO	= (int)m_bUseSRSO;
		GetAppRegMakeSched()->m_nNewFrmTbl	= (int)m_bCreateNew;
		GetAppRegMakeSched()->m_nFrmGen		= (int)m_bGen;
		GetAppRegMakeSched()->m_nFrmUpdate	= (int)m_bUse;
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::OnSize
//  Description :   This routine resizes the dialog window based on the
//					the coordinates passed into the routine.
//
//  Returns :		void	-
//  Parameters :
//           UINT nType	-	type of window resizing requested.
//           int cx		-	new width of window.
//           int cy		-	new height of window.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFramePage::OnSize(UINT nType, int cx, int cy)
{
	CResizablePage::OnSize(nType, cx, cy);
	ResizeFilenames();
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::ResizeFilenames
//  Description :   This routine is called to resize the pathname is
//					displayed. So that it fits within the dialog control.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFramePage::ResizeFilenames()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	CString strBinFilename		= pSheet->MakeFilepath(nFolderFrame, nBinary,	TRUE);
	CString strLoadFilename		= pSheet->MakeFilepath(nFolderFrame, nSTOL,		TRUE);
	CString strUpdateFilename	= pSheet->MakeFilepath(nFolderFrame, nFrame,	FALSE);

	PathSetDlgItemPath(this->m_hWnd, IDC_FRAME_GEN_BIN_STATIC,	strBinFilename);
	PathSetDlgItemPath(this->m_hWnd, IDC_FRAME_GEN_PROC_STATIC, strLoadFilename);
	PathSetDlgItemPath(this->m_hWnd, IDC_FRAME_LOC_FRAME_STATIC,strUpdateFilename);
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::OnUpdateFrameOldEdit
//  Description :   This routine is called when the frame control
//					button is changed.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFramePage::OnUpdateFrameOldEdit()
{
	if (m_bInInit) return;
//	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	GetDlgItem(IDC_FRAME_OLD_EDIT)->GetWindowText(m_strFrameFilename);
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::OnFrameSectorButton
//  Description :   This routine is called when the sector control
//					button is pressed.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFramePage::OnFrameSectorButton()
{
//	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	COXUNC uncFilename (m_strSectorFilename);
	CString strExt;
	CString strFilter;
	GetAppDocTemplate(nSector)->GetDocString(strExt,	CDocTemplate::filterExt);
	GetAppDocTemplate(nSector)->GetDocString(strFilter, CDocTemplate::filterName);
	strFilter = strFilter + _T("|*") + strExt + _T("||");
	CFileDialog dlg(TRUE, strExt, m_strSectorFilename, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter, NULL);
//	CFileDialog dlg(TRUE, strExt, uncFilename.Base(), OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter, NULL);
//	CString	strInitialDir = uncFilename.Server() + uncFilename.Share() + uncFilename.Directory();
//	dlg.m_ofn.lpstrInitialDir = strInitialDir;
	if (dlg.DoModal())
	{
		m_strSectorFilename = dlg.GetPathName();
		GetDlgItem(IDC_FRAME_SECTOR_EDIT)->SetWindowText(m_strSectorFilename);
		COXUNC uncRetrievedSectorFilename (m_strSectorFilename);
		GetAppRegMakeSched()->m_strSecTblFileName =	uncRetrievedSectorFilename.Base();
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::OnFrameOldButton
//  Description :   This routine is called when the frame control
//					button is pressed.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CFramePage::OnFrameOldButton()
{
//	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

//	COXUNC uncFilename (m_strFrameFilename);
	CString strExt;
	CString strFilter;
	GetAppDocTemplate(nFrame)->GetDocString(strExt,		CDocTemplate::filterExt);
	GetAppDocTemplate(nFrame)->GetDocString(strFilter,	CDocTemplate::filterName);
	strFilter = strFilter + _T("|*") + strExt + _T("||");
	CFileDialog dlg(TRUE, strExt, m_strFrameFilename, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter, NULL);
//	CFileDialog dlg(TRUE, strExt, uncFilename.Base(), OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter, NULL);
//	CString	strInitialDir = uncFilename.Server() + uncFilename.Share() + uncFilename.Directory();
//	dlg.m_ofn.lpstrInitialDir = strInitialDir;
	if (dlg.DoModal())
	{
		m_strFrameFilename = dlg.GetPathName();
		GetDlgItem(IDC_FRAME_OLD_EDIT)->SetWindowText(m_strFrameFilename);
		COXUNC uncRetrievedFilename (m_strFrameFilename);
		GetAppRegMakeSched()->m_strFrmTblFileName =	uncRetrievedFilename.Base();
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::OnWizardFinish
//  Description :   This routine is called when the okay button is checked
//					on the finish dialog box
//
//  Returns :	void	-
//
//  Parameters :  BOOL	-	TRUE if successful.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CFramePage::OnWizardFinish()
{
	CFinishDialog dlg;
	if (dlg.DoModal() == IDOK)
		return TRUE;
	else
		return FALSE;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CFramePage::OnWizardNext
//  Description :   This routine is called when the user selects next
//					on the dialog window. The specific window controls
//                  are updated as needed.
//
//
//  Returns :	LRESULT	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
LRESULT CFramePage::OnWizardNext()
{
	m_bUseStaticFrm = ((CButton*)GetDlgItem(IDC_STATIC_FRAME_CHECK))->GetCheck();
	BOOL bDynm = ((CButton*)GetDlgItem(IDC_SRSO_FRAME_CHECK))->GetCheck();

	if( m_bCreateNew && (!m_bUseStaticFrm && !bDynm))
	{
		AfxMessageBox(_T("Please select frames to update!"));
		return -1;
	}

	return CResizablePage::OnWizardNext();
}
