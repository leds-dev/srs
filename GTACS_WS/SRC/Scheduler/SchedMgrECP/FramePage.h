#if !defined(AFX_FRAMEPAGE_H__3B36ADA5_08ED_11D5_8019_00609704053C__INCLUDED_)
#define AFX_FRAMEPAGE_H__3B36ADA5_08ED_11D5_8019_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FramePage.h : header file
//
// MSD (06/12) PR000446: Implemented defaults during SchedUpd.
// Revision History:													   
// Manan Dalal - July-August 2014										   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   
// Release in Build16.2													   

class CFramePage : public CResizablePage
{
	DECLARE_DYNCREATE(CFramePage)

// Construction
public:
	CFramePage();
	~CFramePage();
	// PR00446: MSD (6/2012) - Added a way to default updating frames during SchedUpd.
	void Init(BOOL bFirstWizPage, BOOL bLastWizPage, BOOL bVal, BOOL bUseDef);
	BOOL GetGen(){return m_bGen;};
	BOOL GetUse(){return m_bUse;};
	BOOL GetUseRSO(){return m_bUseRSO;};				// Returns whether RSO update checkbox is selected
	BOOL GetUseSRSO(){return m_bUseSRSO;};
	BOOL GetSchedUpd(){return m_bSchedUpd;};
	BOOL GetUseStaticFrm(){return m_bUseStaticFrm;};
	BOOL GetCreateNew(){return m_bCreateNew;};
	CString GetSRSOFrame(){return m_strSRSOFrame;};
	CString GetSRSOLabel(){return m_strSRSOLabel;};
	CString GetSectorFilename(){return m_strSectorFilename;};
	CString GetFrameFilename(){return m_strFrameFilename;};
	
protected:
	// Dialog Data
	//{{AFX_DATA(CFramePage)
	enum { IDD = IDD_FRAME_DIALOG };
	//}}AFX_DATA

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CFramePage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CFramePage)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnSetActive();
	afx_msg void OnUpdateControls();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnUpdateFrameOldEdit();
	afx_msg void OnFrameSectorButton();
	afx_msg void OnFrameOldButton();
	virtual BOOL OnWizardFinish();
	virtual LRESULT OnWizardNext();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void	UpdateControls();
	void	ResizeFilenames();
	BOOL	m_bUse;				// TRUE if frames should be used
	BOOL	m_bCreateNew;		// TRUE if a new frame file should be generated
	BOOL	m_bUseRSO;			// TRUE if special RSO should be used
	BOOL	m_bUseSRSO;			// TRUE if SRSO should be used
	BOOL	m_bUseStaticFrm;	// TRUE if Static frames should be used
	BOOL	m_bGen;				// Generate upload - if TRUE, then generate upload files
	BOOL    m_bSchedUpd;		// TRUE if doing schedule update, so we don't store values to reg.
	BOOL	m_bFirstWizPage;	// TRUE if this the first page of a Wizard
	BOOL	m_bLastWizPage;		// TRUE if this the last page of a Wizard
	CString	m_strSectorFilename;// Sector filename
	CString	m_strFrameFilename;	// Frame Filename (if reusing an existing one)
	CString	m_strSRSOFrame;		// SRSO Frame
	CString	m_strSRSOLabel;		// SRSO Label
	BOOL	m_bInInit;			// TRUE when in InitInstance.  This is needed so that the OnUpdate
								// routines get ignored for the day and version controls.  These
								// controls are COXMaskedEdit classes and the OnUpdate functions get
								// called whenever the mask and prompt symbol is set (in addition to
								// when the value is changed).  Because of this, we want to ignore
								// processing in these routines when we are initializing the control
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FRAMEPAGE_H__3B36ADA5_08ED_11D5_8019_00609704053C__INCLUDED_)
