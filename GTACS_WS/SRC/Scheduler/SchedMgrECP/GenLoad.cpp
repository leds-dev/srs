/***************************
//////////////////////////////////////////////////////////////////////
// GenLoad.cpp : implementation file
// (c) 2001 Frederick J. Shaw
//
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
***************************/



#include "stdafx.h"
#include "GenLoad.h"
#include "CmnHdr.h"
#include "SearchConstants.h"
// #include "winnetwk.h"
// #include "Registry.h"
#include "SchedMgrEcp.h"

/////////////////////////////////////////////////////////////////////////////
// GenLoad message handlers
/////////////////////////////////////////////////////////////////////////////
// CGenLoad

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// The CGenLoad class
/************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-18-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CGenLoad::CGenLoad()
//	Description :	Class constructor.
//
//
//	Return :		constructor	-
//                  void nothing
//	Parameters :
//					const CString strInputFileName		-
//                  const enum GenLdTypeEnum eLdType	-

//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
CGenLoad::CGenLoad(const CString strInputFileName, const enum GenLdTypeEnum eLdType){
	m_strInputFileName =  strInputFileName;
	m_eLdType          = eLdType;
}

/***************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-18-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : ConvertFullPathNameToUnix()
//
//	Description :	This routine converts the passed in path to a UNIX
//                  format.
//
//	Return :	BOOL
//						TRUE	- succes
//						FALSE	- failure
//
//	Parameters :
//				 const CString strFileNameIn -	Filename to be converted
//				 CString &strFileNameOut	 -	Converted filename
//
//	Note :
//////////////////////////////////////////////////////////////////////////
***************************/
bool CGenLoad::ConvertFullPathNameToUnix(const CString strFileNameIn, CString &strFileNameOut)
{
	bool bReturnStatus = true; // Set to success

	/* Get the file and path name.
	   Extract the drive name and store. */

	// Break the schedule filename up into its components.
	TCHAR fDrive[_MAX_DRIVE];
	TCHAR fDir[_MAX_DIR];
	TCHAR fFname[_MAX_FNAME];
	TCHAR fExt[_MAX_EXT];
	_wsplitpath_s((LPCTSTR)strFileNameIn, fDrive, fDir, fFname, fExt);

	CString strfDir(strFileNameIn);

	if (0 != wcslen(fDrive)){
		// Retrieve the remote name from the drive name.
		DWORD BufferSize = 1056; // size of buffer
		TCHAR szBuff[1056];
		REMOTE_NAME_INFO  *lpBuffer = (REMOTE_NAME_INFO *)&szBuff;
		DWORD res;
		if ((res = WNetGetUniversalName(fDrive,
							 REMOTE_NAME_INFO_LEVEL,
							 lpBuffer,
							 &BufferSize)) == NO_ERROR){
			strfDir = (LPTSTR)lpBuffer->lpUniversalName;
			int nIndex = 0;
			if (0 == (nIndex = strfDir.Find(_T("\\")))){
				strfDir.Delete(nIndex);
				if (0 == (nIndex = strfDir.Find(_T("\\")))){
					strfDir.Delete(nIndex);
				}
			}

			//	Convert beginning directory name to a UNIX environmental
			//	variable.
			if (-1 != (nIndex = strfDir.Find(_T("\\")))){
				strfDir.Delete(0, nIndex + 1);
				// strfDir.Insert(0, '$');
			}
			strfDir += fDir;
			strfDir += fFname;
			strfDir += fExt;
		} else {

			strfDir = fDir;
			int nIndex = 0;
			//	Convert beginning directory name to a UNIX environmental
			//	variable.
			if (0 == (nIndex = strfDir.Find(_T("\\")))){
				strfDir.Delete(nIndex, 1);
				// strfDir.Insert(nIndex, '$');
			}
			strfDir += fFname;
			strfDir += fExt;
		}
	} else{
		int nIndex = 0;
		if (-1 != (nIndex = strfDir.Find(_T("\\")))){
			strfDir.Delete(nIndex);
			if ( -1 != (nIndex = strfDir.Find(_T("\\")))){
				strfDir.Delete(nIndex);
			}
		}

		//	Convert beginning directory name to a UNIX environmental
		//	variable.
		if ( -1 != (nIndex = strfDir.Find(_T("\\")))){
			strfDir.Delete(0, nIndex + 1);
			// strfDir.Insert(0, '$');
		}
	}

//	char defPathBuffer[_MAX_PATH];
//	_makepath (defPathBuffer, "", strfDir, fFname, fExt);
//	strFileNameOut = defPathBuffer;
    strfDir.Replace(_T('\\'),_T('/'));

	strFileNameOut = strfDir;

	return bReturnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-18-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : CGenLoad::SendMsg()
//
//	Description :  This routine makes a COM connection to the EPOCH	 client.
//					It then executes a STOL directive, stored in a CString
//
//	Return : BOOL
//
//	Parameters :
//					CString strCmdLine	- a varaiable containing the directive
//					to be sent to the EPOCH client.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CGenLoad::SendMsg(CString strCmdLine)
{
	CComBSTR			bstrDirective(strCmdLine);
	CSchedMgrECPApp*	pApp			= (CSchedMgrECPApp*)AfxGetApp();
	BOOL				eReturnStatus	= TRUE;  // Set to Success.
	ULONG				ulStrmHandle	= NULL;
//	int					nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int					nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	EPSTOLRESP			srResponse;

	srResponse.bstrResponse = NULL;

	CString strStreamName;
//	strStreamName.Format(_T("%s%d_ge"), GetAppRegSite()->m_strAbbrev[nGTACSSiteDef], nGTACSDef + 1);

	strStreamName.Format(_T("%s"), pApp->m_strGrdStrmName);
	strStreamName.MakeLower();

	CWaitCursor wait;   // display wait cursor
	CComBSTR	bstrStrmName(strStreamName);
	HRESULT		hr = (pApp->m_pEpSTOL)->ConnectStream(bstrStrmName, pApp->m_dwCookieSS, &ulStrmHandle);

	if(SUCCEEDED(hr))
	{
		HRESULT	hr = (pApp->m_pEpSTOL)->ExecuteSTOLWait(ulStrmHandle, bstrDirective, &srResponse);
		
		if(SUCCEEDED(hr)) {
			CString	strResponse(srResponse.bstrResponse);
			int		nIndex = strResponse.GetLength();

			if(nIndex > 0)
				strResponse.Remove(strResponse.GetAt(nIndex-1));

			if(EPSTOLCOMPSTAT_ACCEPTED == srResponse.scsStatus)
			{
				GetAppOutputBar()->OutputToEvents(_T("The genload directive was successful."), COutputBar::INFO);
				GetAppOutputBar()->OutputToBuild(_T("The genload directive was successful."), COutputBar::INFO);
			}
			else if(EPSTOLCOMPSTAT_ERROR == srResponse.scsStatus)
			{
				eReturnStatus = FALSE;  // Set to fail.
				GetAppOutputBar()->OutputToEvents(_T("The genload directive returned an error."), COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(strResponse, COutputBar::FATAL);
				GetAppOutputBar()->OutputToBuild(_T("The genload directive returned an error."), COutputBar::FATAL);
				GetAppOutputBar()->OutputToBuild(strResponse, COutputBar::FATAL);
			}
			else if(EPSTOLCOMPSTAT_REJECTED == srResponse.scsStatus)
			{
				eReturnStatus = FALSE;  // Set to fail.
				GetAppOutputBar()->OutputToEvents(_T("The genload directive was rejected."), COutputBar::FATAL);
				GetAppOutputBar()->OutputToBuild(_T("The genload directive was rejected."), COutputBar::FATAL);
			} else{
				eReturnStatus = FALSE;  // Set to fail.
				GetAppOutputBar()->OutputToEvents(_T("ExecuteSTOLWait returned an unknown value."), COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(strResponse, COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(_T("Ensure the ground stream on the mapped GTACS is running."), COutputBar::FATAL);
				GetAppOutputBar()->OutputToBuild(_T("ExecuteSTOLWait returned an unknown value."), COutputBar::FATAL);
				GetAppOutputBar()->OutputToBuild(strResponse, COutputBar::FATAL);
				GetAppOutputBar()->OutputToBuild(_T("Ensure the ground stream on the mapped GTACS is running."), COutputBar::FATAL);
			}
		}  else {
			eReturnStatus = FALSE;  // Set to fail.
			GetAppOutputBar()->OutputToEvents(_T("ExecuteSTOLWait returned an error value."), COutputBar::FATAL);
			GetAppOutputBar()->OutputToEvents(_T("Ensure the ground stream on the mapped GTACS is running."), COutputBar::FATAL);
			GetAppOutputBar()->OutputToBuild(_T("ExecuteSTOLWait returned an error value."), COutputBar::FATAL);
			GetAppOutputBar()->OutputToBuild(_T("Ensure the ground stream on the mapped GTACS is running."), COutputBar::FATAL);
		}

		// Cleanup BSTR returned in srResponse
		if(NULL != srResponse.bstrResponse)
			::SysFreeString(srResponse.bstrResponse);
		}
	else
	{
		eReturnStatus = FALSE;  // Set to fail.
		GetAppOutputBar()->OutputToEvents(_T("Error connecting to stream."), COutputBar::FATAL);
		GetAppOutputBar()->OutputToEvents(_T("Ensure the ground stream on the mapped GTACS is running."), COutputBar::FATAL);
		GetAppOutputBar()->OutputToBuild(_T("Error connecting to stream."), COutputBar::FATAL);
		GetAppOutputBar()->OutputToBuild(_T("Ensure the ground stream on the mapped GTACS is running."), COutputBar::FATAL);
	}



	// Disconnect IEpSTOL stream and connection point.
	hr = (pApp->m_pEpSTOL)->DisconnectStream(ulStrmHandle);

	if(!SUCCEEDED(hr))
		eReturnStatus = FALSE;  // Set to fail.

	return eReturnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-18-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CGenLoad::Send_GenLD_Dir()
//
//	Description :	Creates and sends the GenLoad directive to EPOCH client
//
//	Return : BOOL
//					TRUE - SUCCESS
//					FALSE - FAIL
//
//	Parameters : Void	-	none.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CGenLoad::Send_GenLD_Dir()
{
	BOOL eReturnStatus = TRUE;

	CString strFileName = m_strInputFileName;
	CString strTemp;

	// Initialize a LdGen string to "GENLOAD scid="
	// Append the spacecraft id to LdGen string.
	CString strGenLoadCMDLine = ctstrGENLOAD;
	strGenLoadCMDLine += ctstrSCID;

	CString strSCDir = GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC];
	strGenLoadCMDLine += GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC];
	//	strGenLoadCMDLine += _T("goes13");

	/* Get the database name.
	   If the database name exists
	   append to the LdGen string.	*/

	if (!(GetAppRegSC()->m_strDatabaseName[GetAppRegSite()->m_nDefaultSC]).IsEmpty()){
//		CString strDataFile;
//		CString strLisFile(_T("\\\\Sgtacs01\\$GTACS_ROOT\\export\\home\\gtacsops\\epoch\\database\\reports\\goes13\\"));
		CString strLisFile;
		CString strUnixLisFile;

	     // Get the spacecraft configuration.
		// This makes the search path satellite specific.
		CString strSCDir = GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC];
		int	nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
		int	nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];

		// Append satellite & file folder to the search path.
		strLisFile = GetAppRegGTACS()->m_strDBShare[nGTACSSiteDef][nGTACSDef] + _T('/') + strSCDir + _T('/');

//		ConvertFullPathNameToUnix(strLisFile, strUnixLisFile);
//		strUnixLisFile += GetAppRegSC()->m_strDatabaseName[GetAppRegSite()->m_nDefaultSC];
		strLisFile += GetAppRegSC()->m_strDatabaseName[GetAppRegSite()->m_nDefaultSC];
		strGenLoadCMDLine += ctstrDBASE;

		// Get the database extension.
		CString strExt(_T(".lis"));
		(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nLis])->GetDocString(strExt, CDocTemplate::filterExt);
		strGenLoadCMDLine += strLisFile + strExt;
	}

	/* Get the RT Address.
		If the RT address exists
		append to the LdGen string. */

	strTemp = GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC];
	if (!strTemp.IsEmpty()){
		strGenLoadCMDLine += ctstrRT;
		strGenLoadCMDLine += strTemp;
	}

	/* Get the CTCU Id.
	   If the CTCU ID exists
	   append to the LdGen string. */

	int nCTCUId = GetAppRegSC()->m_nCTCUID[GetAppRegSite()->m_nDefaultSC];
	if (-1 != nCTCUId){
		CString strCTCUId;
		strCTCUId.Empty();
		strCTCUId.Format(_T("%d"), nCTCUId);
		strGenLoadCMDLine += ctstrCTCU + strCTCUId;
	}

	/* Get the file and path name.
	   Extract the drive name and store. */

	CString strUnixFileName;
	ConvertFullPathNameToUnix(strFileName, strUnixFileName);

//	CGTACSReg cGTACSReg;
//	CString strStreamName = cGTACSReg.m_strStreamName;


	strGenLoadCMDLine += ctstrFILE + strUnixFileName;
//	strGenLoadCMDLine += ctstrFILE + strFileName;

	switch (m_eLdType)
	{
		case RAM_LDTYPE :
		{
			strGenLoadCMDLine += ctstrTYPE;
			strGenLoadCMDLine += ctstrRAM;
		}
		break;

		case STO_LDTYPE :
		{
			// CSCReg cCSCReg;
			strGenLoadCMDLine += ctstrTYPE;
			strGenLoadCMDLine += ctstrSTO;
			strTemp.Empty();

			if (-1 != GetAppRegSC()->m_nSTOSize[GetAppRegSite()->m_nDefaultSC]){
				CString strSTOSize;
				strSTOSize.Format(_T("%d"), GetAppRegSC()->m_nSTOSize);
				strGenLoadCMDLine += ctstrMAXEQ + strSTOSize;
			}
		}
		break;

		case SKB_LDTYPE :
		{
//			CSCReg cCSCReg;
			strGenLoadCMDLine += ctstrTYPE;
			strGenLoadCMDLine += ctstrSKB;

			if (-1 != GetAppRegSC()->m_nSKBSize[GetAppRegSite()->m_nDefaultSC]){
				CString strSKBSize;
				strSKBSize.Format(_T("%d"), GetAppRegSC()->m_nSKBSize[GetAppRegSite()->m_nDefaultSC]);
				strGenLoadCMDLine += ctstrMAXEQ + strSKBSize;
			}

			if (-1 != GetAppRegSC()->m_nSKBSegSize[GetAppRegSite()->m_nDefaultSC]){
				CString strSKBSegSize;
				strSKBSegSize.Format(_T("%d"), GetAppRegSC()->m_nSKBSegSize[GetAppRegSite()->m_nDefaultSC]);
				strGenLoadCMDLine += " SEGSIZE=" + strSKBSegSize;
			}
		}
		break;

		case STAR_LDTYPE:
		{
//			CSCReg cCSCReg;
			strGenLoadCMDLine += ctstrTYPE;
			strGenLoadCMDLine += ctstrSTAR;
			if (-1 != GetAppRegSC()->m_nStarSize[GetAppRegSite()->m_nDefaultSC]){
				CString strStarSize;
				strStarSize.Format(_T("%d"), GetAppRegSC()->m_nStarSize[GetAppRegSite()->m_nDefaultSC]);
				strGenLoadCMDLine += ctstrMAXEQ + strStarSize;
			}
		}
		break;

		case FRAME_LDTYPE:
		{
//			CSCReg cCSCReg;
			strGenLoadCMDLine += ctstrTYPE;
			strGenLoadCMDLine += ctstrFRAME;
		//	strTemp.Format("%d",cCSCReg.m_nFrameSize);
			if (-1 != GetAppRegSC()->m_nFrameSize[GetAppRegSite()->m_nDefaultSC]){
				CString strFrameSize;
				strFrameSize.Format(_T("%d"), GetAppRegSC()->m_nFrameSize[GetAppRegSite()->m_nDefaultSC]);
				strGenLoadCMDLine += ctstrMAXEQ + strFrameSize;
			}
		}
		break;

		case IMC_LDTYPE:
		{
			strGenLoadCMDLine += ctstrTYPE;
			strGenLoadCMDLine += ctstrIMC;
		}
		break;

		case RTCS_LDTYPE:
		{
			strGenLoadCMDLine += ctstrTYPE;
			strGenLoadCMDLine += ctstrRTCS;

//			if (-1 != GetAppRegSC()->m_nRTCSSize[GetAppRegSite()->m_nDefaultSC]){
				CString strSize;
				strSize.Format(_T(" %d"), GetAppRegSC()->m_nSTORTCSSize[GetAppRegSite()->m_nDefaultSC]);
			//	strSize.Format(_T("553"));
				strGenLoadCMDLine += ctstrSTART_RTCSEQ + strSize;
//			}

//			if (-1 != GetAppRegSC()->m_nRTCS_Start[GetAppRegSite()->m_nDefaultSC]){
//				CString strSize;
				strSize.Format(_T(" %d"), GetAppRegSC()->m_nSTOSize[GetAppRegSite()->m_nDefaultSC]);
			//	strSize.Format(_T("1553"));
				strGenLoadCMDLine += ctstrMAXEQ + strSize;
//			}
		}
		break;
	}

	/*
	Removed because unneeded.  Needed to make process automatic.  No prompting.
	CString strMsg;
	strMsg.Format(_T("The load file for schedule %s\n")
					 _T("will be generated."),
					  m_strInputFileName);
	if (AfxMessageBox(strMsg, MB_OKCANCEL) == IDOK)
	{
		CWaitCursor wait;   // display wait cursor
		if ( TRUE != SendMsg(strGenLoadCMDLine)){
			eReturnStatus = FALSE;  // Set to fail.
		}
	}
	else {
		eReturnStatus = FALSE;  // Set to fail.
	}

	*/
	if ( TRUE != SendMsg(strGenLoadCMDLine)){
		eReturnStatus = FALSE;  // Set to fail.
	}

	if (eReturnStatus == FALSE){
		CString strErrorMsg;
		strErrorMsg = _T("Error generating STOL procedure.");
		GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
		GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
	} else  {
		CString strErrorMsg;
		strErrorMsg = _T("Product generation complete.");
		GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::INFO);
		GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::INFO);
	}

	return eReturnStatus;
}
