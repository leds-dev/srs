#if !defined(AFX_GenLoad_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_)
#define AFX_GenLoad_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GenLoad.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GenLoad window

class CGenLoad 
{
// Construction
public:
	
	typedef enum GenLdTypeEnum {RAM_LDTYPE, STO_LDTYPE, SKB_LDTYPE,	STAR_LDTYPE, FRAME_LDTYPE, IMC_LDTYPE, RTCS_LDTYPE, UNDEF_LDTYPE};

//	CGenLoad();
//	CGenLoad(const CString strInputFileName);
	CGenLoad(const CString strInputFileName, const enum GenLdTypeEnum eLdType=UNDEF_LDTYPE);

	virtual ~CGenLoad(void){};
	BOOL     Send_GenLD_Dir();

//	enum GenLdTypeEnum GetLoadType(void){return m_eLdType;};
//	void               SetLoadType(const enum GenLdTypeEnum eLdType){m_eLdType = eLdType;};
//	CString            GetInputFileName(void){return m_strInputFileName;};
//	void               SetInputFileName(CString &strFileName){m_strInputFileName = strFileName;};

private:

	bool ConvertFullPathNameToUnix(const CString strFileNameIn, CString &strFileNameOut);
	BOOL SendMsg(CString strCmdLine);

	CString m_strInputFileName;
	enum GenLdTypeEnum m_eLdType;

};


/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GenLoad_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_)
