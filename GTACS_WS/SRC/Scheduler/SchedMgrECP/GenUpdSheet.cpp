// GenUpdSheet.cpp : implementation file
// MSD (06/12) PR000446: Enhance the SchedMgr frame option and other defaults.
// Revision History:													   
// Manan Dalal - July-August 2014										   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   
// Release in Build16.2

#include "Stdafx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CGenUpdSheet, CResizableSheet)

CGenUpdSheet::CGenUpdSheet()
{
	CGenUpdSheet(nInvalid, _T(""));
}

CGenUpdSheet::CGenUpdSheet(int nFunc, CString strInputFilename)
	: CResizableSheet(_T(""), NULL, 0)
{
	m_nFunction			=	nFunc;
	m_cUNCInputFilePath =	strInputFilename;
	m_nDay				=	GetAppRegMisc()->m_nDOY;
	m_nVersion			=	1;
	m_strOriginalFile	=	m_cUNCInputFilePath.Server()	+
							m_cUNCInputFilePath.Share()		+
							m_cUNCInputFilePath.Directory()	+
							m_cUNCInputFilePath.Base();

	if (GetAppRegMisc()->m_bShowExt)
		m_strOriginalFile += m_cUNCInputFilePath.Extension();

	// Check to see if the filename contains a version
	CString	strBase		= m_cUNCInputFilePath.Base();
	int		nBaseLen	= strBase.GetLength();
	if (nBaseLen > 4)
	{
		if ((strBase[nBaseLen-4] == _T('_')) &&
			(strBase[nBaseLen-3] >= _T('0')) &&
			(strBase[nBaseLen-3] <= _T('9')) &&
			(strBase[nBaseLen-2] >= _T('0')) &&
			(strBase[nBaseLen-2] <= _T('9')) &&
			(strBase[nBaseLen-1] >= _T('0')) &&
			(strBase[nBaseLen-1] <= _T('9')))
		{
			CString strVersion = strBase.Right(3);
			m_nVersion = _ttoi(strBase.Right(3));
			if (m_nVersion < 999)
				m_nVersion++;
			strBase = strBase.Left(nBaseLen-4);
			COXUNC	cNewInputFilePath(m_cUNCInputFilePath.Server(),
									  m_cUNCInputFilePath.Share(),
									  m_cUNCInputFilePath.Directory(),
									  strBase + m_cUNCInputFilePath.Extension());
			m_cUNCInputFilePath = cNewInputFilePath;
		}
	}
	
	switch (m_nFunction)
	{
		case nValCLS :
		{
			m_cCLSPage.Init(TRUE, TRUE, TRUE, BOTH);
			AddPage(&m_cCLSPage);
			break;
		}
		case nValSTOL :
		{
			m_cSchedPage.Init(TRUE, TRUE, FALSE, TRUE, FALSE, BOTH, FALSE);
			AddPage(&m_cSchedPage); 
			break;
		}
		case nUpdFrame :
		{
			m_cIMCSetPage.Init(TRUE, TRUE);
			AddPage(&m_cIMCSetPage);
			break;
		}
		case nUpdSTOL :
		{
			m_cOutputPage.Init(TRUE, FALSE);
			m_cStarPage.Init(FALSE, FALSE, TRUE, TRUE);
			m_cFramePage.Init(FALSE, FALSE, TRUE, TRUE);
			m_cRSOPage.Init(FALSE, FALSE);
			m_cSRSOPage.Init(FALSE, FALSE);
			m_cRTCSPage.Init(FALSE, FALSE, FALSE, TRUE);
			m_cIMCSetPage.Init(FALSE, FALSE);
			m_cSchedPage.Init(FALSE, TRUE, TRUE, TRUE, FALSE, BOTH, TRUE);	
			AddPage(&m_cOutputPage);
			AddPage(&m_cStarPage);
			AddPage(&m_cFramePage);
			AddPage(&m_cRSOPage);
			AddPage(&m_cSRSOPage);
			AddPage(&m_cRTCSPage);
			AddPage(&m_cIMCSetPage);
			AddPage(&m_cSchedPage);
			break;
		}
		case nPackage :
		{
			m_cOutputPage.Init(TRUE, FALSE);
			m_cCLSPage.Init(FALSE, FALSE, TRUE, BOTH);
			m_cStarPage.Init(FALSE, FALSE, TRUE, FALSE);
			m_cFramePage.Init(FALSE, FALSE, TRUE, FALSE);
			m_cSRSOPage.Init(FALSE, FALSE);
			m_cRTCSPage.Init(FALSE, FALSE, FALSE, FALSE);
			m_cIMCSetPage.Init(FALSE, FALSE);
			m_cSchedPage.Init(FALSE, TRUE, TRUE, TRUE, FALSE, BOTH, TRUE);				
			AddPage(&m_cOutputPage);
			AddPage(&m_cCLSPage);
			AddPage(&m_cStarPage);
			AddPage(&m_cFramePage);
			AddPage(&m_cSRSOPage);
			AddPage(&m_cRTCSPage);
			AddPage(&m_cIMCSetPage);
			AddPage(&m_cSchedPage);
			break;
		}

		case nGenSTOL :
		{
			m_cOutputPage.Init(TRUE, FALSE);
			m_cCLSPage.Init(FALSE, TRUE, FALSE, BOTH);
			AddPage(&m_cOutputPage);
			AddPage(&m_cCLSPage);
			break;
		}

		case nReqIMC :
		{
			m_cOutputPage.Init(TRUE, TRUE);
			AddPage(&m_cOutputPage);
			break;
		}
		case nReqSFCS :
		case nReqEclipse :
		case nReqIntrusion :
		{
			m_cOutputPage.Init(TRUE, FALSE);
			m_cOATSPage.Init(FALSE, TRUE);
			AddPage(&m_cOutputPage);
			AddPage(&m_cOATSPage);
			break;
		}
	}

	SetWizardMode();
}

CGenUpdSheet::~CGenUpdSheet()
{
}

BEGIN_MESSAGE_MAP(CGenUpdSheet, CResizableSheet)
	//{{AFX_MSG_MAP(CGenUpdSheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BOOL CGenUpdSheet::OnInitDialog() 
{
	CResizableSheet::OnInitDialog();
	
	EnableSaveRestore(_T("Resizable Sheets"), _T("Gen Val Update"), FALSE);

	return TRUE;
}

CString CGenUpdSheet::MakeCLSGenFilepath()
{
	CString	strFilepath;
//	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	CString	strFilename		= m_strNameToUse;
	CString	strExt;

	if (GetAppRegMisc()->m_bShowExt)
		GetAppDocTemplate(nSTOL)->GetDocString(strExt, CDocTemplate::filterExt);
	else
		strExt.Empty();
	
	strFilepath.Format(_T("%s\\%s\\%s\\%s\\%s%s\\%s%s"),
	//	GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
	//	GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],			// Spacecraft
		m_strDirToUse,												// Either user or day dir
		GetAppRegFolder()->m_strDir[nFolderSTOL],					// Specific dir
		GetBase(),													// Filename
		strExt);													// Extension

	return strFilepath;
}

CString CGenUpdSheet::MakeFilepath(int nFolder, int nType, BOOL bLoad, BOOL bDaily /*=TRUE*/)
{
	CString	strFilepath;
//	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	CString	strFilename		= m_strNameToUse;
	CString	strExt;

	if (bLoad)
		strFilename = strFilename + _T("_load");

	if (GetAppRegMisc()->m_bShowExt)
		GetAppDocTemplate(nType)->GetDocString(strExt, CDocTemplate::filterExt);
	else
		strExt.Empty();
	
	if (bDaily)
	{
//		strFilepath.Format(_T("\\\\%s\\%s\\%s\\%s%s\\%s%s"),
//			GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//			GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share

		strFilepath.Format(_T("%s\\%s\\%s\\%s\\%s%s\\%s%s"),
			ctcProcDrive, ctcMountPoint, ctcProcsDir,
			GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],			// Spacecraft
			m_strDirToUse,												// Either user or day dir
			GetAppRegFolder()->m_strDir[nFolder],						// Specific dir
			strFilename,												// Filename
			strExt);													// Extension
	}
	else
	{
//		strFilepath.Format(_T("\\\\%s\\%s\\%s\\%s\\%s%s"),
//			GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//			GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share

		strFilepath.Format(_T("%s\\%s\\%s\\%s\\%s\\%s%s"),
			ctcProcDrive, ctcMountPoint, ctcProcsDir,
			GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],			// Spacecraft
			GetAppRegFolder()->m_strDir[nFolder],						// Specific dir
			strFilename,												// Filename
			strExt);													// Extension
	}

	return strFilepath;
}
