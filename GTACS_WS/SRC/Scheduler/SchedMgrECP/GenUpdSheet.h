#if !defined(AFX_GENUPDSHEET_H__375850E8_0B34_11D5_8019_00609704053C__INCLUDED_)
#define AFX_GENUPDSHEET_H__375850E8_0B34_11D5_8019_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GenUpdSheet.h : header file
//
// Revision History:													   
// Manan Dalal - July-August 2014										   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   
// Release in Build16.2													   

#include "SchedPage.h"
#include "FramePage.h"
#include "SRSOPage.h"
#include "RSOPage.h"
#include "StarPage.h"
#include "CLSPage.h"
#include "OutputPage.h"
#include "OATSPage.h"
#include "IMCSetPage.h"
#include "RTCSPage.h"

/////////////////////////////////////////////////////////////////////////////
// CGenUpdSheetEx

class CGenUpdSheet : public CResizableSheet
{
	DECLARE_DYNAMIC(CGenUpdSheet)
public:
	CGenUpdSheet();
	CGenUpdSheet(int nFunc, CString strInputFilename);
	virtual ~CGenUpdSheet();
	
	int				GetFunc(){return m_nFunction;};
	CString			GetBase(){return m_cUNCInputFilePath.Base();};
	CString			GetFull(){return m_cUNCInputFilePath.Full();};
	CString			GetOrig(){return m_strOriginalFile;};
	int				GetDay(){return m_nDay;};
	void			SetDay(int nDay){m_nDay = nDay;};
	int				GetVersion(){return m_nVersion;};
	void			SetVersion(int nVersion){m_nVersion = nVersion;};
	COutputPage*	GetOutputPage(){return &m_cOutputPage;};
	CCLSPage*		GetCLSPage(){return &m_cCLSPage;};
	CSchedPage*		GetSchedPage(){return &m_cSchedPage;};
	CStarPage*		GetStarPage(){return &m_cStarPage;};
	CFramePage*		GetFramePage(){return &m_cFramePage;};
	CRSOPage*		GetRSOPage(){return &m_cRSOPage;};				// New RSO Updage page.
	CSRSOPage*		GetSRSOPage(){return &m_cSRSOPage;};
	COATSPage*		GetOATSPage(){return &m_cOATSPage;};
	CIMCSetPage*	GetIMCSetPage(){return &m_cIMCSetPage;};
	CRTCSPage*		GetRTCSPage(){return &m_cRTCSPage;};
	
	//COutputPage Parameters that will be used by other sheets
	CString	m_strNameToUse;					//Filename that should be used.  Does not contain an extension.  If
											//m_bUseVersion is TRUE, this name will contain the version,
											//otherwise, this name will be the name that the user entered.
	CString m_strDirToUse;					//Directory to use.  It ends in a "\" but does not contain the
											//final portion of the directory.
											//  a) If the m_bUseDay member variable is TRUE then the
											//     "Day Format" + "Folder directory" must be added.
											//  b)
											//Otherwise, just the "Folder directory" must be added.

	CString MakeFilepath(int nFolder, int nType, BOOL bLoad, BOOL bDaily = TRUE);
	CString MakeCLSGenFilepath();	
	// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGenUpdSheet)
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation

	// Generated message map functions
protected:
	//{{AFX_MSG(CGenUpdSheet)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	int			m_nFunction;				//One of the various eFunction values
	COXUNC		m_cUNCInputFilePath;		//The complete path of the InFocus document
	int			m_nDay;						//Day of year that is built into the output path
	int			m_nVersion;					//Version that is built into the output filename
	COutputPage	m_cOutputPage;
	CCLSPage	m_cCLSPage;
	CSchedPage	m_cSchedPage;
	CStarPage	m_cStarPage;
	CFramePage	m_cFramePage;
	CRSOPage	m_cRSOPage;					//PR000951 - New RSO Page.
	CSRSOPage	m_cSRSOPage;
	COATSPage	m_cOATSPage;
	CIMCSetPage	m_cIMCSetPage;
	CRTCSPage	m_cRTCSPage;
	CString		m_strOriginalFile;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GENUPDSHEET_H__375850E8_0B34_11D5_8019_00609704053C__INCLUDED_)
