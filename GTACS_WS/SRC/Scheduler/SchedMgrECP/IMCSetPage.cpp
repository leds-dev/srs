// IMCSetPage.cpp : implementation file
//

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIMCSetage property page

IMPLEMENT_DYNCREATE(CIMCSetPage, CResizablePage)

CIMCSetPage::CIMCSetPage() : CResizablePage(CIMCSetPage::IDD)
//, m_strIMCSet(_T(""))
, m_strIMC(_T(""))
{
	//{{AFX_DATA_INIT(CIMCSetPage)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CIMCSetPage::~CIMCSetPage()
{
}

void CIMCSetPage::DoDataExchange(CDataExchange* pDX)
{
	//	DDX_Control(pDX, IDC_IMC_EDIT, m_cIMCEdit);
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIMCSetPage)
	DDX_Text(pDX, IDC_IMC_EDIT, m_strIMC);
	DDV_MaxChars(pDX, m_strIMC, 4);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CIMCSetPage, CResizablePage)
	//{{AFX_MSG_MAP(CIMCSetPage)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_IMC_CHECK, OnUpdateControls)
	ON_EN_UPDATE(IDC_IMC_EDIT, OnUpdateIMCEdit)
	ON_BN_CLICKED(IDC_IMC_IMG_CHECK, OnUpdateControls)
	ON_BN_CLICKED(IDC_IMC_SND_CHECK, OnUpdateControls)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CIMCSetPage::Init(BOOL bFirstWizPage, BOOL bLastWizPage)
{
	m_bFirstWizPage	= bFirstWizPage;
	m_bLastWizPage	= bLastWizPage;
	m_bUseSndIMC	= TRUE;
	m_bUseImgIMC	= TRUE;
	m_strIMC		= _T("A000");
}

BOOL CIMCSetPage::OnInitDialog() 
{
	CResizablePage::OnInitDialog();
//	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	m_bInInit = TRUE;
	
	AddAnchor(IDC_IMC_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_IMC_ENABLED_GROUP_STATIC, TOP_LEFT, TOP_LEFT);
	//AddAnchor(IDC_IMC_IMG_CHECK, TOP_LEFT, TOP_LEFT);
	//AddAnchor(IDC_IMC_SND_CHECK, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_IMC_LABEL, TOP_RIGHT, TOP_RIGHT);
	AddAnchor(IDC_IMC_EDIT, TOP_RIGHT, TOP_RIGHT);
	
	// Set the mask, prompt symbol, and range of the various Day controls
	((CButton*)GetDlgItem(IDC_IMC_IMG_CHECK))->SetCheck(m_bUseImgIMC);
	((CButton*)GetDlgItem(IDC_IMC_SND_CHECK))->SetCheck(m_bUseSndIMC);
//	m_cIMCEdit.SetMask(_T(">###"));
//	m_strIMCSetEdit = _T("A000");

	m_bInInit = FALSE;

	return TRUE;
}

BOOL CIMCSetPage::OnSetActive()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	GetDlgItem(IDC_IMC_EDIT)->SetWindowText(m_strIMC);
	
	// Update the visibility of the controls based upon the radio button positions
	// Update the filenames
	UpdateControls();
	
	// Setup the proper Wizard Buttons
	DWORD dWizFlags = PSWIZB_FINISH;
	if ((m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_FINISH;
	else if ((m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_NEXT;
	else if ((!m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_FINISH;
	else if ((!m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_NEXT;
	
	pSheet->SetWizardButtons(dWizFlags);

	return CResizablePage::OnSetActive();
}

void CIMCSetPage::OnUpdateIMCEdit() 
{
	if (m_bInInit) return;
//	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	GetDlgItem(IDC_IMC_EDIT)->GetWindowText(m_strIMC);
}


void CIMCSetPage::UpdateControls()
{
	OnUpdateControls();
}

void CIMCSetPage::OnUpdateControls()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	GetDlgItem(IDC_IMC_GROUP)->EnableWindow(TRUE);
	GetDlgItem(IDC_IMC_ENABLED_GROUP_STATIC)->EnableWindow(TRUE);
	GetDlgItem(IDC_IMC_IMG_CHECK)->EnableWindow(TRUE);
	GetDlgItem(IDC_IMC_SND_CHECK)->EnableWindow(TRUE);
	GetDlgItem(IDC_IMC_LABEL)->EnableWindow(TRUE);
	GetDlgItem(IDC_IMC_EDIT)->EnableWindow(TRUE);

	if( pSheet->GetFunc() == nUpdSTOL || pSheet->GetFunc() == nPackage )
	{
		if( !(pSheet->GetFramePage())->GetUse() && !(pSheet->GetStarPage())->GetUse() )
		{
			((CButton*)GetDlgItem(IDC_IMC_IMG_CHECK))->SetCheck(FALSE);
			((CButton*)GetDlgItem(IDC_IMC_SND_CHECK))->SetCheck(FALSE);
			GetDlgItem(IDC_IMC_GROUP)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_IMG_CHECK)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_SND_CHECK)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_ENABLED_GROUP_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_LABEL)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_EDIT)->EnableWindow(FALSE);
		}
		else
		{
			if( !(pSheet->GetFramePage())->GetCreateNew() )
			{
				GetDlgItem(IDC_IMC_ENABLED_GROUP_STATIC)->EnableWindow(FALSE);
				((CButton*)GetDlgItem(IDC_IMC_IMG_CHECK))->SetCheck(TRUE);
				((CButton*)GetDlgItem(IDC_IMC_SND_CHECK))->SetCheck(TRUE);
				GetDlgItem(IDC_IMC_IMG_CHECK)->EnableWindow(FALSE);
				GetDlgItem(IDC_IMC_SND_CHECK)->EnableWindow(FALSE);
			}
		}
	}

	m_bUseImgIMC = ((CButton*)GetDlgItem(IDC_IMC_IMG_CHECK))->GetCheck();
	m_bUseSndIMC = ((CButton*)GetDlgItem(IDC_IMC_SND_CHECK))->GetCheck();
}

void CIMCSetPage::OnSize(UINT nType, int cx, int cy) 
{
	CResizablePage::OnSize(nType, cx, cy);
}	

BOOL CIMCSetPage::OnWizardFinish() 
{
	return TRUE;
}
