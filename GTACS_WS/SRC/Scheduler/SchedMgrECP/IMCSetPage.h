#if !defined(AFX_IMCSETPAGE_H__3B36ADA5_08ED_11D5_8019_00609704053C__INCLUDED_)
#define AFX_IMCSETPAGE_H__3B36ADA5_08ED_11D5_8019_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FramePage.h : header file
//

class CIMCSetPage : public CResizablePage
{
	DECLARE_DYNCREATE(CIMCSetPage)

// Construction
public:
	CIMCSetPage();
	~CIMCSetPage();
	void Init(BOOL bFirstWizPage, BOOL bLastWizPage);
	BOOL GetUseImgIMC(){return m_bUseImgIMC;};
	BOOL GetUseSndIMC(){return m_bUseSndIMC;};
	CString GetIMC(){return m_strIMC;};
	
protected:
	// Dialog Data
	//{{AFX_DATA(CIMCSetPage)
	enum { IDD = IDD_IMCSET_DIALOG };
//	COXMaskedEdit	m_cIMCEdit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CIMCSetPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CIMCSetPage)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnSetActive();
	afx_msg void OnUpdateControls();
	virtual BOOL OnWizardFinish();
	afx_msg void OnUpdateIMCEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void	UpdateControls();
	BOOL	m_bUseImgIMC;		// TRUE if IMC should be used
	BOOL	m_bUseSndIMC;		// TRUE if IMC should be used
	BOOL	m_bFirstWizPage;	// TRUE if this the first page of a Wizard
	BOOL	m_bLastWizPage;		// TRUE if this the last page of a Wizard
//	CString	m_strIMC;			// IMC Set ID
	BOOL	m_bInInit;			// TRUE when in InitInstance.  This is needed so that the OnUpdate
								// routines get ignored for the day and version controls.  These
								// controls are COXMaskedEdit classes and the OnUpdate functions get
								// called whenever the mask and prompt symbol is set (in addition to
								// when the value is changed).  Because of this, we want to ignore
								// processing in these routines when we are initializing the control
private:
	CString m_strIMC;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMCSETPAGE_H__3B36ADA5_08ED_11D5_8019_00609704053C__INCLUDED_)
