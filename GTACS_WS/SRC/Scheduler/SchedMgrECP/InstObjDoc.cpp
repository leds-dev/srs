/**
/////////////////////////////////////////////////////////////////////////////
// InstObjExDoc.cpp : implementation of the InstObjExDoc class.            //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// InstObjExDoc.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class creates a new or edits an existing instrument object document.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "MainFrm.h"
#include "InstObjDoc.h"
#include "Parse.h"
#include "NewDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CInstObjExDoc, CDocument)

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExDoc::CInstObjExDoc
//	Description :	Class Constructor.
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			Initialize the last modification time to 0, and the
//					data file to NULL
//////////////////////////////////////////////////////////////////////////
CInstObjExDoc::CInstObjExDoc()
{
	m_cTimeLastMod	= 0;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExDoc::~CInstObjExDoc
//	Description :	Class Destructor.
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CInstObjExDoc::~CInstObjExDoc(){}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExDoc::OnNewDocument
//	Description :	Called by the framework to create a new document
//	Return :		
//		BOOL		-	Creation status.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CInstObjExDoc::OnNewDocument()
{
	return TRUE;
}

BEGIN_MESSAGE_MAP(CInstObjExDoc, CDocument)
	//{{AFX_MSG_MAP(CInstObjExDoc)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateFileNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExDoc::AssertValid
//	Description :	Debug AssertValid routine.
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CInstObjExDoc::AssertValid() const
{
	CDocument::AssertValid();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExDoc::Dump
//	Description :	Debug Dump routine.
//	Return :		
//		void		-	
//	Parameters :	
//		CDumpContext& dc	-	Dump context
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CInstObjExDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExDoc::OnOpenDocument
//	Description :	This routines just sets the internal file time
//	Return :		
//		BOOL		-	TRUE if ok, otherwise FALSE
//	Parameters :	
//		LPCTSTR lpszPathName	-	Name of the file to open.
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CInstObjExDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	// Update the document's last modification time
	CFileFind finder;
	if (finder.FindFile(lpszPathName))
	{
		finder.FindNextFile();
		return finder.GetLastWriteTime(m_cTimeLastMod);
	}
	else
		return FALSE;
}

void CInstObjExDoc::OnFileNew() 
{
	CNewDialog dlg;
	if (dlg.DoModal() == IDOK)
	{
		//Open the first document subtype of the type selected
		GetAppDocTemplate(dlg.m_nTypeSelected)->OpenDocumentFile(NULL);	
	}
}

void CInstObjExDoc::OnUpdateFileNew(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExDoc::OnSaveDocument
//	Description :	This routines just updates the mod time.
//	Return :		
//		BOOL		-	TRUE if ok, otherwise FALSE
//	Parameters :	
//		LPCTSTR lpszPathName	-	Name of the file to save.
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CInstObjExDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	CFileFind finder;
	finder.FindFile(lpszPathName);
	finder.FindNextFile();
	finder.GetLastWriteTime(m_cTimeLastMod);
	m_tModTime = m_cTimeLastMod.GetTime();
	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExDoc::MarkModified
//	Description :	This routine will either add or remove a "*" from the
//					title bar and then update the file's modification
//					time and modification user.
//	Return :		
//		void		-	
//	Parameters :	
//		bool bModified	-	TRUE to mark the file as modified, FALSE
//							to mark it as not modified.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CInstObjExDoc::MarkModified(bool bModified)
{
	CString strTitle = GetTitle();
	int		nTitleLength = strTitle.GetLength();
	if (bModified)
	{
		if (strTitle.Find(_T(" *")) != nTitleLength-2)
			strTitle = strTitle + _T(" *");
	}
	else
	{
		if (strTitle.Find(_T(" *")) == nTitleLength-2)
			strTitle.Delete(nTitleLength-2, 2);
	}

	SetTitle(strTitle);
	SetModifiedFlag(bModified);
	m_tModTime = (CTime::GetCurrentTime()).GetTime();
	unsigned long nUserNameSize = 100;
	TCHAR cUserName[100];
	::GetUserName (cUserName, &nUserNameSize);
	m_strModUser = cUserName;
}

IMPLEMENT_DYNCREATE(CFrameObjDoc, CInstObjExDoc)

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjDoc::CFrameObjDoc
//	Description :	Class constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CFrameObjDoc::CFrameObjDoc(){}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjDoc::~CFrameObjDoc
//	Description :	Class destructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CFrameObjDoc::~CFrameObjDoc(){}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjDoc::OnNewDocument
//	Description :	This routine will create a new Frame document.  It
//					will get the default settings from the registry and
//					fill the document with blank data.
//	Return :		
//		BOOL		-	TREU if the document was created, otherwise FALSE.
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CFrameObjDoc::OnNewDocument()
{
	if (!CInstObjExDoc::OnNewDocument())
		return FALSE;

	m_fVersion = 1.0;
	m_nNumObj = GetAppRegSC()->m_nFrameSize[GetAppRegSite()->m_nDefaultSC];
	m_nSRSOFirst = GetAppRegSC()->m_nFrameSRSOStart[GetAppRegSite()->m_nDefaultSC];
	m_tModTime = 0;
	m_strModUser.Empty();
	m_tUpdateTime = 0;
	m_strUpdateUser.Empty();
	for (int nIndex=0; nIndex<m_nNumObj; nIndex++)
	{
		CParseFrame cParseFrame;
		m_strDataRecord.Add(cParseFrame.GetDataNew(nIndex));
		m_nHasData.Add(FALSE);
	}
	
	return TRUE;
}

BEGIN_MESSAGE_MAP(CFrameObjDoc, CInstObjExDoc)
	//{{AFX_MSG_MAP(CFrameObjDoc)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjDoc::AssertValid
//	Description :	Debug AssertValid routine.
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjDoc::AssertValid() const
{
	CDocument::AssertValid();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjDoc::Dump
//	Description :	Debug Dump routine
//	Return :		
//		void		-	
//	Parameters :	
//		CDumpContext& dc	-	Dump context.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjDoc::OnOpenDocument
//	Description :	This routine will open a frame file and read in its
//					data.  A number of different of checks are made to
//					ensure that the data is from a valid frame file.
//	Return :		
//		BOOL		-	TRUE if the file opened OK and the data was
//						valid, otherwise FALSE.
//	Parameters :	
//		LPCTSTR lpszPathName	-	Name of the frame file to open.
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CFrameObjDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	CWaitCursor waitCursor;
	BOOL		bStatus = TRUE;
	CTextFile	dataFile;
	CString		strError;
	CParseFrame	cParseFrame;

	// Open the document
	if (bStatus)
	{
		CFileException cFE;
		m_strDataRecord.RemoveAll();
		if (!dataFile.Load(lpszPathName, CTextFile::RO, &cFE))
		{
			strError = CTextFile::ExceptionErrorText(&cFE);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}

	// Make sure that there are atleast 2 header records in the file and if OK
	// read in the header records
	int nNumRecords;
	if (bStatus)
	{
		nNumRecords = dataFile.GetLineCount();
		if (nNumRecords < 2)
		{
			strError.Format (IDS_OPEN_HEADER_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
			cParseFrame.SetHdr(dataFile.GetTextAt(0), dataFile.GetTextAt(1));
	}

	// Parse the header records
	if (bStatus)
	{
		if (!cParseFrame.ParseHdr())
		{
			strError.Format (IDS_INV_HEADER_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
		{
			m_fVersion = cParseFrame.m_fVersion;
			m_nSRSOFirst = cParseFrame.m_nSRSOFirst;
			m_tModTime = cParseFrame.m_tModTime;
			m_strModUser = cParseFrame.m_strModUser;
			m_tUpdateTime = cParseFrame.m_tUpdTime;
			m_strUpdateUser = cParseFrame.m_strUpdUser;
		}
	}

	// Make sure that the number of objects listed in the header is less than the number
	// of records in the file
	if (bStatus)
	{
		m_nNumObj = cParseFrame.m_nNumObj;
		if (m_nNumObj < dataFile.GetLineCount()-2)
		{
			strError.Format (IDS_BAD_FRAMEHEADER_NUM, m_nNumObj, m_strDataRecord.GetSize());
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}
	
	// Make sure that the number of frames in the file is <= the size of the onboard buffer
	if (bStatus)
	{
		int nOnBoardNumFrames = GetAppRegSC()->m_nFrameSize[GetAppRegSite()->m_nDefaultSC];
		if (nOnBoardNumFrames < m_nNumObj)
		{
			strError.Format(IDS_BAD_DATA_SIZE, m_nNumObj, nOnBoardNumFrames);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);
			strError.Format(IDS_BAD_DATA_SIZE_MSGBOX, m_nNumObj, nOnBoardNumFrames);
			if (AfxGetMainWnd()->MessageBox(strError, _T("Frame File Size Mismatch"),
											MB_YESNO | MB_ICONEXCLAMATION) == IDNO)
			{
				m_nNumObj = nOnBoardNumFrames;
				strError.Format(IDS_TRUNCATE_FRAME);
				GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);
				MarkModified(TRUE);
			}
			else
			{
				strError.Format(IDS_NOTRUNCATE_FRAME);
				GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);
			}
		}
	}

	// Read the data from the file into the document
	if (bStatus)
	{
		int nCurObj;
		int nRecIdx = 0;
		m_strDataRecord.SetSize(m_nNumObj);
		m_nHasData.SetSize(m_nNumObj);

		// Initialize all of the objects to be empty
		for (nCurObj=0; nCurObj<m_nNumObj; nCurObj++)
		{
			m_strDataRecord[nCurObj] = cParseFrame.GetDataNew(nCurObj);
			m_nHasData[nCurObj] = FALSE;
		}

		nCurObj = 2;
		while ((bStatus) && (nCurObj < dataFile.GetLineCount()))
		{
			cParseFrame.SetData(dataFile.GetTextAt(nCurObj));
			if (!cParseFrame.ParseData())
			{
				strError.Format (IDS_BAD_DATA, nCurObj+1);
				GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
				bStatus = FALSE;
			}
			else
			{
				if (cParseFrame.HasData())
				{
					if (cParseFrame.m_nNumObj > m_nNumObj)
					{
						strError.Format (IDS_DISCARD_FRAME, cParseFrame.m_nIndex);
						GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);
					}
					else
					{
					/*	m_strDataRecord[cParseFrame.m_nIndex-1] = dataFile.GetTextAt(nCurObj);
						m_nHasData[cParseFrame.m_nIndex-1] = TRUE;	*/
						m_strDataRecord[nRecIdx] = dataFile.GetTextAt(nCurObj);
						m_nHasData[nRecIdx++] = TRUE;
					}
				}
			}
			nCurObj++;
		}
	}

	// Close the file, all of the data is now local
	if (bStatus)
	{
		CFileException cFE;
		dataFile.Close(&cFE);
	}

	// Call the parent's OnOpendocument
	if (bStatus)
	{
		bStatus = CInstObjExDoc::OnOpenDocument(lpszPathName);
	}
	
	return bStatus;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjDoc::OnSaveDocument
//	Description :	OnSaveDocument is call by the framework whenever we
//					need to save a file.  Normally the documents data
//					is just serialized out, but we need to do more than
//					that.  This routine creates a CTextFile document,
//					writes the data into it, and then closes it.
//	Return :		
//		BOOL		-	
//	Parameters :	
//		LPCTSTR lpszPathName	-	Name of the file to save.
//	Note :			If the user selected FileSave, the filename passed
//					in is the current name of the file.  If FileSaveAs
//					was selected, then the filename is the name that the
//					user either entered or selected.
//////////////////////////////////////////////////////////////////////////
BOOL CFrameObjDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	CWaitCursor		waitCursor;
	BOOL		bStatus = TRUE;
	CString		strError;
	CTextFile	dataFile;
	CParseFrame	cParseFrame;
	CFileException cFE;
	
	if (bStatus)
	{
		if (!dataFile.Load(lpszPathName, CTextFile::CR, &cFE))
		{
			strError = CTextFile::ExceptionErrorText(&cFE);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}

	if (bStatus)
	{
		cParseFrame.m_fVersion = m_fVersion;
		dataFile.AppendText(cParseFrame.GetHdrRec1());
		cParseFrame.m_nNumObj = m_nNumObj;
		cParseFrame.m_nSRSOFirst = m_nSRSOFirst;
		cParseFrame.m_tModTime = m_tModTime;
		cParseFrame.m_strModUser = m_strModUser;
		cParseFrame.m_tUpdTime = m_tUpdateTime;
		cParseFrame.m_strUpdUser = m_strUpdateUser;
		dataFile.AppendText(cParseFrame.GetHdrRec2());
		for (int nIndex=0; nIndex<m_nNumObj; nIndex++)
		{
			if (m_nHasData[nIndex])
				dataFile.AppendText(m_strDataRecord[nIndex]);
		}
		if (!dataFile.Close(CTextFile::WRITE, &cFE))
		{
			strError.Format (IDS_FILESAVE_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
		{
			CInstObjExDoc::OnSaveDocument(lpszPathName); //xyz
			MarkModified(FALSE);
		}
	}

	return bStatus;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjDoc::MarkModified
//	Description :	This routine is called to update the modified status
//					of the file.
//	Return :		
//		void		-	
//	Parameters :	
//		bool bModified	-	If TRUE, the file is marked as having been
//							modified
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjDoc::MarkModified(bool bModified)
{
	if (bModified)
	{
		m_tUpdateTime = 0;
		m_strUpdateUser = "Not Updated";
	}
	CInstObjExDoc::MarkModified(bModified);
}

void CFrameObjDoc::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CFrameObjDoc::OnUpdateFileSaveAs(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

IMPLEMENT_DYNCREATE(CSectorObjDoc, CInstObjExDoc)

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSectorObjDoc::CSectorObjDoc
//	Description :	Class constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CSectorObjDoc::CSectorObjDoc(){}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSectorObjDoc::~CSectorObjDoc
//	Description :	Class destructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CSectorObjDoc::~CSectorObjDoc(){}

BOOL CSectorObjDoc::OnNewDocument()
{
	if (!CInstObjExDoc::OnNewDocument())
		return FALSE;

	m_fVersion = 1.0;
	m_nNumObj = 1;
	m_tModTime = 0;
	m_strModUser.Empty();

	CParseSector cParseSector;
	m_strDataRecord.Add(cParseSector.GetDataNew());
	m_nHasData.Add(FALSE);

	return TRUE;
}

BEGIN_MESSAGE_MAP(CSectorObjDoc, CInstObjExDoc)
	//{{AFX_MSG_MAP(CSectorObjDoc)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSectorObjDoc::AssertValid
//	Description :	Debug AssertValid routine
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CSectorObjDoc::AssertValid() const
{
	CDocument::AssertValid();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSectorObjDoc::Dump
//	Description :	Debug Dump routine
//	Return :		
//		void		-	
//	Parameters :	
//		CDumpContext& dc	-	Dump context.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CSectorObjDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSectorObjDoc::OnOpenDocument
//	Description :	This routine will open a sector file and read in its
//					data.  A number of different of checks are made to
//					ensure that the data is from a valid frame file.
//	Return :		
//		BOOL		-	TRUE if the file opened OK and the data was
//						valid, otherwise FALSE.
//	Parameters :	
//		LPCTSTR lpszPathName	-	Name of the frame file to open.
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CSectorObjDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	CWaitCursor		waitCursor;
	BOOL			bStatus = TRUE;
	CTextFile		dataFile;
	CString			strError;
	CParseSector	cParseSector;

	// Open the document
	if (bStatus)
	{
		CFileException cFE;
		m_strDataRecord.RemoveAll();
		if (!dataFile.Load(lpszPathName, CTextFile::RO, &cFE))
		{
			strError = CTextFile::ExceptionErrorText(&cFE);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}

	// Make sure that there are atleast 2 header records in the file and if OK
	// read in the header records
	int nNumRecords;
	if (bStatus)
	{
		nNumRecords = dataFile.GetLineCount();
		if (nNumRecords < 2)
		{
			strError.Format (IDS_OPEN_HEADER_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
			cParseSector.SetHdr(dataFile.GetTextAt(0), dataFile.GetTextAt(1));
	}

	// Parse the header records
	if (bStatus)
	{
		if (!cParseSector.ParseHdr())
		{
			strError.Format (IDS_INV_HEADER_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
		{
			m_fVersion = cParseSector.m_fVersion;
			m_tModTime = cParseSector.m_tModTime;
			m_strModUser = cParseSector.m_strModUser;
			m_nNumObj = cParseSector.m_nNumObj;
		}
	}

	// Read the data from the file into the document
	if (bStatus)
	{
		int nCurObj;
		m_strDataRecord.SetSize(m_nNumObj);
		m_nHasData.SetSize(m_nNumObj);

		nCurObj = 2;
		while ((bStatus) && (nCurObj < dataFile.GetLineCount()))
		{
			cParseSector.SetData(dataFile.GetTextAt(nCurObj));
			if (!cParseSector.ParseData())
			{
				strError.Format (IDS_BAD_DATA, nCurObj+1);
				GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
				bStatus = FALSE;
			}
			else
			{
				m_strDataRecord[nCurObj-2] = dataFile.GetTextAt(nCurObj);
				m_nHasData[nCurObj-2] = TRUE;
			}
			nCurObj++;
		}
	}

	// Close the file, all of the data is now local
	if (bStatus)
	{
		CFileException cFE;
		dataFile.Close(&cFE);
	}

	// Call the parent's OnOpendocument
	if (bStatus)
	{
		bStatus = CInstObjExDoc::OnOpenDocument(lpszPathName);
	}
	
	return bStatus;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSectorObjDoc::OnSaveDocument
//	Description :	OnSaveDocument is call by the framework whenever we
//					need to save a file.  Normally the documents data
//					is just serialized out, but we need to do more than
//					that.  This routine creates a CTextFile document,
//					writes the data into it, and then closes it.
//	Return :		
//		BOOL		-	
//	Parameters :	
//		LPCTSTR lpszPathName	-	Name of the file to save.
//	Note :			If the user selected FileSave, the filename passed
//					in is the current name of the file.  If FileSaveAs
//					was selected, then the filename is the name that the
//					user either entered or selected.
//////////////////////////////////////////////////////////////////////////
BOOL CSectorObjDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	CWaitCursor		waitCursor;
	BOOL			bStatus = TRUE;
	CString			strError;
	CTextFile		dataFile;
	CParseSector	cParseSector;
	CFileException	cFE;
	// int				nIndex;

	int nNumRecordsWithoutData = 0;
	for (int nIndex=0; nIndex<m_nNumObj; nIndex++)
	{
		if (!m_nHasData[nIndex])
			nNumRecordsWithoutData++;
	}
	
	if (nNumRecordsWithoutData != 0)
	{
		strError.Format (IDS_NODATA_SECTOR);
		GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
		bStatus = FALSE;
	}

	if (bStatus)
	{
		if (!dataFile.Load(lpszPathName, CTextFile::CR, &cFE))
		{
			strError = CTextFile::ExceptionErrorText(&cFE);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}
	
	if (bStatus)
	{
		cParseSector.m_fVersion = m_fVersion;
		dataFile.AppendText(cParseSector.GetHdrRec1());
		cParseSector.m_nNumObj = m_nNumObj;
		cParseSector.m_tModTime = m_tModTime;
		cParseSector.m_strModUser = m_strModUser;
		dataFile.AppendText(cParseSector.GetHdrRec2());
		for (int nIndex=0; nIndex<m_nNumObj; nIndex++)
		{
			if (m_nHasData[nIndex])
				dataFile.AppendText(m_strDataRecord[nIndex]);
		}
		if (!dataFile.Close(CTextFile::WRITE, &cFE))
		{
			strError.Format (IDS_FILESAVE_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
		{
			CInstObjExDoc::OnSaveDocument(lpszPathName);
			MarkModified(FALSE);
		}
	}

	return bStatus;
}

void CSectorObjDoc::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

void CSectorObjDoc::OnUpdateFileSaveAs(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

IMPLEMENT_DYNCREATE(CStarObjDoc, CInstObjExDoc)

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CStarObjDoc::CFrameObjDoc
//	Description :	Class constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CStarObjDoc::CStarObjDoc(){}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CStarObjDoc::~CFrameObjDoc
//	Description :	Class destructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CStarObjDoc::~CStarObjDoc(){}

BEGIN_MESSAGE_MAP(CStarObjDoc, CInstObjExDoc)
	//{{AFX_MSG_MAP(CStarObjDoc)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE, OnUpdateFileSave)
	ON_UPDATE_COMMAND_UI(ID_FILE_SAVE_AS, OnUpdateFileSaveAs)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateFileNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CStarObjDoc::AssertValid
//	Description :	Debug AssertValid routine.
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CStarObjDoc::AssertValid() const
{
	CDocument::AssertValid();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CStarObjDoc::Dump
//	Description :	Debug Dump routine
//	Return :		
//		void		-	
//	Parameters :	
//		CDumpContext& dc	-	Dump context
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CStarObjDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CStarObjDoc::OnOpenDocument
//	Description :	This routine will open a star file and read in its
//					data.  A number of different of checks are made to
//					ensure that the data is from a valid frame file.
//	Return :		
//		BOOL		-	TRUE if the file opened OK and the data was
//						valid, otherwise FALSE.
//	Parameters :	
//		LPCTSTR lpszPathName	-	Name of the frame file to open.
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CStarObjDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	CWaitCursor	waitCursor;
	BOOL		bStatus = TRUE;
	CTextFile	dataFile;
	CString		strError;
	CParseStar	cParseStar;

	// Open the document
	if (bStatus)
	{
		CFileException cFE;
		m_strDataRecord.RemoveAll();
		if (!dataFile.Load(lpszPathName, CTextFile::RO, &cFE))
		{
			strError = CTextFile::ExceptionErrorText(&cFE);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}

	// Make sure that there are atleast 2 header records in the file and if OK
	// read in the header records
	int nNumRecords;
	if (bStatus)
	{
		nNumRecords = dataFile.GetLineCount();
		if (nNumRecords < 2)
		{
			strError.Format (IDS_OPEN_HEADER_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
			cParseStar.SetHdr(dataFile.GetTextAt(0), dataFile.GetTextAt(1));
	}

	// Parse the header records
	if (bStatus)
	{
		if (!cParseStar.ParseHdr())
		{
			strError.Format (IDS_INV_HEADER_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
		{
			m_fVersion = cParseStar.m_fVersion;
			m_tModTime = cParseStar.m_tModTime;
			m_strModUser = cParseStar.m_strModUser;
			m_nNumObj = cParseStar.m_nNumObj;
		}
	}

	if (bStatus)
	{
		m_nNumObj = cParseStar.m_nNumObj;
		if (m_nNumObj < dataFile.GetLineCount()-2)
		{
			strError.Format(
			_T("The Header Record in the file is invalid.  ")
			_T("It specifies that %d stars exist however, %d were found.  ")
			_T("Refer to the Scheduling User's Guide for more information."),
			m_nNumObj, m_strDataRecord.GetSize());
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}
	
	if (bStatus)
	{
		int nOnBoardNumStars = GetAppRegSC()->m_nStarSize[GetAppRegSite()->m_nDefaultSC];
		if (nOnBoardNumStars < m_nNumObj)
		{
			strError.Format(IDS_BAD_DATA_SIZE, m_nNumObj, nOnBoardNumStars);
			GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);
			strError.Format(IDS_BAD_DATA_SIZE_MSGBOX, m_nNumObj, nOnBoardNumStars);
			if (AfxGetMainWnd()->MessageBox(strError, _T("Star File Size Mismatch"),
											MB_YESNO | MB_ICONEXCLAMATION) == IDNO)
			{
				m_nNumObj = nOnBoardNumStars;
				strError.Format(
				_T("The star file size was not modified and does not match the onboard star table size.  ")
				_T("Do not upload this star table to the spacecraft."));
				GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);
				MarkModified(TRUE);
			}
			else
			{
				strError.Format(
				_T("The star file size was not modified and does not match the onboard star table size.  ")
				_T("Do not upload this star table to the spacecraft."));
				GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);
			}
		}
	}
	
	// Read the data from the file into the document
	if (bStatus)
	{
		int nCurObj;
		m_strDataRecord.SetSize(m_nNumObj);
		m_nHasData.SetSize(m_nNumObj);

		for (nCurObj=0; nCurObj<m_nNumObj; nCurObj++)
		{
			m_strDataRecord[nCurObj] = cParseStar.GetDataNew(nCurObj);
			m_nHasData[nCurObj] = FALSE;
		}

		nCurObj = 2;
		while ((bStatus) && (nCurObj < dataFile.GetLineCount()))
		{
			cParseStar.SetData(dataFile.GetTextAt(nCurObj));
			if (!cParseStar.ParseData())
			{
				strError.Format (IDS_BAD_DATA, nCurObj+1);
				GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
				bStatus = FALSE;
			}
			else
			{
				if (cParseStar.HasData())
				{
					if (cParseStar.m_nNumObj > m_nNumObj)
					{
						strError.Format(
						_T("Star index for object %d is outside of the star table size and  has been discarded."),
						cParseStar.m_nIndex);
						GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);
					}
					else
					{
						m_strDataRecord[nCurObj-2] = dataFile.GetTextAt(nCurObj);
						m_nHasData[nCurObj-2] = TRUE;
					}
				}
			}
			nCurObj++;
		}
	}

	// Close the file, all of the data is now local
	if (bStatus)
	{
		CFileException cFE;
		dataFile.Close(&cFE);
	}

	// Call the parent's OnOpendocument
	if (bStatus)
	{
		bStatus = CInstObjExDoc::OnOpenDocument(lpszPathName);
	}
	
	return bStatus;
}

void CStarObjDoc::OnUpdateFileSave(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(FALSE);
}
void CStarObjDoc::OnUpdateFileSaveAs(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(FALSE);
}
void CStarObjDoc::OnUpdateFileNew(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(FALSE);
}
