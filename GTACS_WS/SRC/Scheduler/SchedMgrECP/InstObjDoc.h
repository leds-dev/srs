/////////////////////////////////////////////////////////////////////////////
#if !defined(AFX_INSTOBJDOC_H__CC1CD226_F7F7_11D3_8634_0020EA0406A1__INCLUDED_)
#define AFX_INSTOBJDOC_H__CC1CD226_F7F7_11D3_8634_0020EA0406A1__INCLUDED_

#include "TextFile.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/////////////////////////////////////////////////////////////////////////////
// CInstObjExDoc document
class CInstObjExDoc : public CDocument
{
protected:
	CInstObjExDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CInstObjExDoc)
public:
	//{{AFX_VIRTUAL(CInstObjExDoc)
	public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL
public:
	virtual ~CInstObjExDoc();
	static BOOL Generate(CString *pstrFileName = NULL, short nFileType = -1);
	void MarkModified(bool bModified);

	CStringArray	m_strDataRecord;
	CWordArray		m_nHasData;
	float			m_fVersion;
	int				m_nNumObj;
	time_t			m_tModTime;
	CString			m_strModUser;
	CTime			m_cTimeLastMod;  // Used during file saves only

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CInstObjExDoc)
	afx_msg void OnFileNew();
	afx_msg void OnUpdateFileNew(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CFrameObjDoc document
class CFrameObjDoc : public CInstObjExDoc
{
protected:
	CFrameObjDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CFrameObjDoc)
public:
	//{{AFX_VIRTUAL(CFrameObjDoc)
	public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL
public:
	virtual ~CFrameObjDoc();
//	BOOL UpdateFrames(SOCKET* pSocket, CString* pstrIMC, int nOATSSite, int nOATS);
//	BOOL IsImager(int nIndex);
	void MarkModified(bool bModified);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CFrameObjDoc)
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	int				m_nSRSOFirst;
	time_t			m_tUpdateTime;
	CString			m_strUpdateUser;
};
/////////////////////////////////////////////////////////////////////////////
// CSectorObjDoc document
class CSectorObjDoc : public CInstObjExDoc
{
protected:
	CSectorObjDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSectorObjDoc)
public:
	//{{AFX_VIRTUAL(CSectorObjDoc)
	public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	protected:
	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL
public:
	virtual ~CSectorObjDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CSectorObjDoc)
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CStarObjDoc document
class CStarObjDoc : public CInstObjExDoc
{
protected:
	CStarObjDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CStarObjDoc)
public:
	//{{AFX_VIRTUAL(CStarObjDoc)
	public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL
public:
	virtual ~CStarObjDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CStarObjDoc)
	afx_msg void OnUpdateFileSave(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileNew(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INSTOBJDOC_H__CC1CD226_F7F7_11D3_8634_0020EA0406A1__INCLUDED_)
