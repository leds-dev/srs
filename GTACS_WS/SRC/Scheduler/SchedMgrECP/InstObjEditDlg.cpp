/**
/////////////////////////////////////////////////////////////////////////////
// InstObjEditDlg.cpp : implementation of the InstObjEditDlg class.        //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// InstObjEditDlg.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class will allow the user to edit instrument object parameters for
// the various instrument object files.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "Parse.h"
#include "InstObjEditDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInstObjEditDlg dialog

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInstObjEditDlg::CInstObjEditDlg
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//		CWnd* pParentWnd	-	Specifies the dialog�s parent window, 
//								usually a CDialog. It must not be NULL.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CInstObjEditDlg::CInstObjEditDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CInstObjEditDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInstObjEditDlg)
	m_strLabel = _T("");
	m_strDescEdit = _T("");
	m_strIndex = _T("");
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInstObjEditDlg::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//
//  Returns :		void	-  
//
//  Parameters : 
//           CDataExchange* pDX
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CInstObjEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInstObjEditDlg)
	DDX_Control(pDX, IDC_DESC_EDIT, m_cDescEdit);
	DDX_Control(pDX, IDC_FRINDEX_EDIT, m_cFrameIndexEdit);
	DDX_Control(pDX, IDC_INDEX_STATIC, m_cIndexStatic);
	DDX_Control(pDX, IDC_FRLABEL_EDIT, m_cFrameLabelEdit);
	DDX_Control(pDX, IDC_FRAMETIME_EDIT, m_cFrameTimeEdit);
	DDX_Control(pDX, IDOK, m_cIDOK);
	DDX_Control(pDX, IDC_SLMODE_STATIC, m_cSLModeStatic);
	DDX_Control(pDX, IDC_SNDRSTEP_STATIC, m_cSNDRStepStatic);
	DDX_Control(pDX, IDC_STOPNSLABEL_STATIC, m_cStopNSLabelStatic);
	DDX_Control(pDX, IDC_STOPEWLABEL_STATIC, m_cStopEWLabelStatic);
	DDX_Control(pDX, IDC_STARTNSLABEL_STATIC, m_cStartNSLabelStatic);
	DDX_Control(pDX, IDC_STARTEWLABEL_STATIC, m_cStartEWLabelStatic);
	DDX_Control(pDX, IDC_STOPNS_STATIC, m_cStopNSStatic);
	DDX_Control(pDX, IDC_STOPEW_STATIC, m_cStopEWStatic);
	DDX_Control(pDX, IDC_STARTNS_STATIC, m_cStartNSStatic);
	DDX_Control(pDX, IDC_STARTEW_STATIC, m_cStartEWStatic);
	DDX_Control(pDX, IDC_STARTEW_EDIT, m_cStartEWEdit);
	DDX_Control(pDX, IDC_STOPNS_EDIT, m_cStopNSEdit);
	DDX_Control(pDX, IDC_STOPEW_EDIT, m_cStopEWEdit);
	DDX_Control(pDX, IDC_STARTNS_EDIT, m_cStartNSEdit);
	DDX_Control(pDX, IDC_COORDINATE_COMBO, m_cCoordinateCombo);
	DDX_Control(pDX, IDC_SNDRSTEP_COMBO, m_cSNDRStepCombo);
	DDX_Control(pDX, IDC_SLSIDE_COMBO, m_cSLSideCombo);
	DDX_Control(pDX, IDC_SLMODE_COMBO, m_cSLModeCombo);
	DDX_Control(pDX, IDC_INSTR_COMBO, m_cInstrCombo);
	DDX_Text(pDX, IDC_FRLABEL_EDIT, m_strLabel);
	DDX_Text(pDX, IDC_DESC_EDIT, m_strDescEdit);
	DDV_MaxChars(pDX, m_strDescEdit, 255);
	DDX_Text(pDX, IDC_FRINDEX_EDIT, m_strIndex);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInstObjEditDlg, CDialog)
	//{{AFX_MSG_MAP(CInstObjEditDlg)
	ON_CBN_SELCHANGE(IDC_COORDINATE_COMBO, OnUpdateActiveFields)
	ON_CBN_SELCHANGE(IDC_INSTR_COMBO, OnUpdateActiveFields)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInstObjEditDlg message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CInstObjEditDlg::OnInitDialog
//  Description :	This routine creates the InstObjEditDlg dialog 
//					window.  
//
//	Return :		BOOL	-	TRUE if successful
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CInstObjEditDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetWindowText(m_strTitle);
	m_cInstrCombo.AddString (strInstrument[1]);
	m_cInstrCombo.AddString (strInstrument[2]);
	m_cInstrCombo.SelectString (-1, m_strInstrument);

	m_cSLModeCombo.AddString (strSpacelookMode[0]);
	m_cSLModeCombo.AddString (strSpacelookMode[1]);
	m_cSLModeCombo.AddString (strSpacelookMode[2]);
	m_cSLModeCombo.AddString (strSpacelookMode[3]);
	m_cSLModeCombo.SelectString (-1, m_strSpacelookMode);

	if( m_strTitle.Find(_T("Sector")) != -1 )
		m_cSLSideCombo.AddString (strSpacelookSide[0]);
	m_cSLSideCombo.AddString (strSpacelookSide[1]);
	m_cSLSideCombo.AddString (strSpacelookSide[2]);
	m_cSLSideCombo.SelectString (-1, m_strSpacelookSide);

	m_cSNDRStepCombo.AddString (strSndrStepMode[4]);
	m_cSNDRStepCombo.AddString (strSndrStepMode[0]);
	m_cSNDRStepCombo.AddString (strSndrStepMode[1]);
	m_cSNDRStepCombo.AddString (strSndrStepMode[2]);
	m_cSNDRStepCombo.AddString (strSndrStepMode[3]);
	m_cSNDRStepCombo.SelectString (-1, m_strSndrStepMode);

	m_cCoordinateCombo.AddString (strCoordType[1]);
	m_cCoordinateCombo.AddString (strCoordType[2]);
	m_cCoordinateCombo.AddString (strCoordType[3]);
//	m_cCoordinateCombo.AddString (strCoordType[12]);
	m_cCoordinateCombo.SelectString (-1, m_strCoordType);

	CString	strFloat;
	strFloat.Format(_T("%.3f"), m_fStartNSEdit);
	m_cStartNSEdit.SetWindowText (strFloat);
	strFloat.Format(_T("%.3f"), m_fStartEWEdit);
	m_cStartEWEdit.SetWindowText (strFloat);
	strFloat.Format(_T("%.3f"), m_fStopNSEdit);
	m_cStopNSEdit.SetWindowText (strFloat);
	strFloat.Format(_T("%.3f"), m_fStopEWEdit);
	m_cStopEWEdit.SetWindowText (strFloat);

	m_cFrameTimeEdit.SetMask(_T("####/###/##:##:##.###"));
	m_cFrameTimeEdit.SetPromptSymbol(_T('0'));

	OnUpdateActiveFields();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInstObjEditDlg::OnUpdateActiveFields  
//  Description :	This routine updates all active fields controls 
//					associated with the CInstObjEditDlg class.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CInstObjEditDlg::OnUpdateActiveFields() 
{
	CString strSelCoordType;
	m_cCoordinateCombo.GetLBText(m_cCoordinateCombo.GetCurSel(), strSelCoordType);
	if (strSelCoordType.CompareNoCase (strCoordType[1]) == 0)
	{
		m_cStartNSLabelStatic.SetWindowText (_T("Start Latitude:"));
		m_cStartEWLabelStatic.SetWindowText (_T("Start Longitude:"));
		m_cStopNSLabelStatic.SetWindowText (_T("Stop Latitdue:"));
		m_cStopEWLabelStatic.SetWindowText (_T("Stop Longitude:"));
		m_cStartNSStatic.SetWindowText (_T("(degrees)"));
		m_cStartEWStatic.SetWindowText (_T("(degrees west)"));
		m_cStopNSStatic.SetWindowText (_T("(degrees)"));
		m_cStopEWStatic.SetWindowText (_T("(degrees west)"));
		GetDlgItem(IDC_FRAMETIME_STATIC)->EnableWindow(m_bReadOnly);
		GetDlgItem(IDC_FRAMETIME_EDIT)->EnableWindow(m_bReadOnly);
		m_cFrameTimeEdit.SetInputData(m_strFrameTimeEdit);
	}
	else if (strSelCoordType.CompareNoCase (strCoordType[2]) == 0)
	{
		m_cStartNSLabelStatic.SetWindowText (_T("Start Line:"));
		m_cStartEWLabelStatic.SetWindowText (_T("Start Pixel:"));
		m_cStopNSLabelStatic.SetWindowText (_T("Stop Line:"));
		m_cStopEWLabelStatic.SetWindowText (_T("Stop Pixel:"));
		m_cStartNSStatic.SetWindowText (_T("(lines)"));
		m_cStartEWStatic.SetWindowText (_T("(pixels)"));
		m_cStopNSStatic.SetWindowText (_T("(lines)"));
		m_cStopEWStatic.SetWindowText (_T("(pixels)"));
		GetDlgItem(IDC_FRAMETIME_STATIC)->EnableWindow(m_bReadOnly);
		GetDlgItem(IDC_FRAMETIME_EDIT)->EnableWindow(m_bReadOnly);
		//if (m_bReadOnly)
		//m_cFrameTimeEdit.SetInputData(_T("2000/001/00:00:00.000"));
		m_cFrameTimeEdit.SetInputData(m_strFrameTimeEdit);
	}
	else if (strSelCoordType.CompareNoCase (strCoordType[3]) == 0)
	{
		m_cStartNSLabelStatic.SetWindowText (_T("SRSO Center Point Latitude:"));
		m_cStartEWLabelStatic.SetWindowText (_T("SRSO Center Point Longitude:"));
		m_cStopNSLabelStatic.SetWindowText (_T("SRSO Aspect Ratio:"));
		m_cStopEWLabelStatic.SetWindowText (_T("SRSO Duration:"));
		m_cStartNSStatic.SetWindowText (_T("(degrees)"));
		m_cStartEWStatic.SetWindowText (_T("(degree west)"));
		m_cStopNSStatic.SetWindowText (_T(""));
		m_cStopEWStatic.SetWindowText (_T("(minutes)"));
		GetDlgItem(IDC_FRAMETIME_STATIC)->EnableWindow(m_bReadOnly);
		GetDlgItem(IDC_FRAMETIME_EDIT)->EnableWindow(m_bReadOnly);
		m_cFrameTimeEdit.SetInputData(m_strFrameTimeEdit);
	}/*
	else if (strSelCoordType.CompareNoCase (strCoordType[12]) == 0)
	{
		m_cStartNSLabelStatic.SetWindowText (_T("SRSO Center Point Line:"));
		m_cStartEWLabelStatic.SetWindowText (_T("SRSO Center Point Pixel:"));
		m_cStopNSLabelStatic.SetWindowText (_T("SRSO Aspect Ratio:"));
		m_cStopEWLabelStatic.SetWindowText (_T("SRSO Duration:"));
		m_cStartNSStatic.SetWindowText (_T("(lines)"));
		m_cStartEWStatic.SetWindowText (_T("(pixels)"));
		m_cStopNSStatic.SetWindowText (_T(""));
		m_cStopEWStatic.SetWindowText (_T("(minutes)"));
		GetDlgItem(IDC_FRAMETIME_STATIC)->EnableWindow(TRUE);
		GetDlgItem(IDC_FRAMETIME_EDIT)->EnableWindow(TRUE);
		m_cFrameTimeEdit.SetInputData(m_strFrameTimeEdit);
	}*/

	CString strInstr;
	m_cInstrCombo.GetLBText(m_cInstrCombo.GetCurSel(), strInstr);
	if (strInstr.CompareNoCase (strInstrument[1]) == 0)
	{
		m_cSLModeCombo.EnableWindow(true);
		m_cSLModeCombo.ResetContent();
		m_cSLModeCombo.AddString (strSpacelookMode[1]);
		m_cSLModeCombo.AddString (strSpacelookMode[2]);
		m_cSLModeCombo.AddString (strSpacelookMode[3]);
		if (m_strSpacelookMode.CompareNoCase(strSpacelookMode[0]) == 0)
			m_cSLModeCombo.SetCurSel(0);
		else
			m_cSLModeCombo.SelectString(-1, m_strSpacelookMode);
		m_cSLModeStatic.EnableWindow(true);
		m_cSNDRStepCombo.EnableWindow(false);
		m_cSNDRStepCombo.ResetContent();
		m_cSNDRStepCombo.AddString (strSndrStepMode[4]);
		m_cSNDRStepCombo.SetCurSel (0);
		m_cSNDRStepStatic.EnableWindow(false);
	}
	else if (strInstr.CompareNoCase (strInstrument[2]) == 0)
	{
		m_cSLModeCombo.EnableWindow(false);
		m_cSLModeCombo.ResetContent();
		m_cSLModeCombo.AddString (strSpacelookMode[0]);
		m_cSLModeCombo.SetCurSel(0);
		m_cSLModeStatic.EnableWindow(false);
		m_cSNDRStepCombo.EnableWindow(true);
		m_cSNDRStepCombo.ResetContent();
		m_cSNDRStepCombo.AddString (strSndrStepMode[0]);
		m_cSNDRStepCombo.AddString (strSndrStepMode[1]);
		m_cSNDRStepCombo.AddString (strSndrStepMode[2]);
		m_cSNDRStepCombo.AddString (strSndrStepMode[3]);
		if (m_strSndrStepMode.CompareNoCase(strSndrStepMode[4]) == 0)
			m_cSNDRStepCombo.SetCurSel(0);
		else
			m_cSNDRStepCombo.SelectString (-1, m_strSndrStepMode);
		m_cSNDRStepStatic.EnableWindow(true);
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInstObjEditDlg::ValidateInputRange  
//  Description :   This routine is called to valid input range. 
//                  
//  Returns :		bool	-	true if value is within range.
//
//  Parameters :  
//		const float fMin		-	range min value	
//		const float fMax		-	range max value
//		const CEdit* pEditCtrl	-	pointer to edit control
//		CString* strValue		-	string value to check
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
bool CInstObjEditDlg::ValidateInputRange (
	const float fMin, const float fMax, const CEdit* pEditCtrl, CString* strValue)
{
	float fValue;

	pEditCtrl->GetWindowText(*strValue);

	if (_stscanf_s ((LPCTSTR)*strValue, _T("%f"), &fValue) == 0)
		return false;

	if ((fValue<fMin) || (fValue>fMax))
		return false;
	else
		return true;
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInstObjEditDlg::ValidateInputOrder  
//  Description :   This routine is called to check if the start value is
//					not greater than the stop value. 
//                  
//  Returns :		bool	-	true if start value is less than stop value.
//
//  Parameters :  
//		const CEdit* pStartEditCtrl	-	pointer that holds the start value	
//		const CEdit* pStopEditCtrl	-	pointer that holds the stop value
//		CString* strStartValue		-	pointer to string start value
//		CString* strStopValue		-	pointer to string stop value
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
bool CInstObjEditDlg::ValidateInputOrder (
	const CEdit* pStartEditCtrl,CString* strStartValue, 
	const CEdit* pStopEditCtrl,	CString* strStopValue)
{
	float fStartValue = 0.0;
	float fStopValue  = 0.0;

	pStartEditCtrl->GetWindowText(*strStartValue);
	pStopEditCtrl->GetWindowText(*strStopValue);

	if (_stscanf_s ((LPCTSTR)*strStartValue, _T("%f"), &fStartValue) == 0)
		return false;
	if (_stscanf_s ((LPCTSTR)*strStopValue, _T("%f"), &fStopValue) == 0)
		return false;

	if (fStartValue < fStopValue)
		return false;
	else
		return true;
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CInstObjEditDlg::OnOK  
//  Description :	This routine validates all window control values 
//					associated with the InstObjEditDlg class then closes 
//					the dialog.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CInstObjEditDlg::OnOK() 
{
	bool bNoError= true;
	CString strErrorText;

	CString strValue;
	CString strStartValue;
	CString strStopValue;

	m_cSLModeCombo.GetLBText(m_cSLModeCombo.GetCurSel(), m_strSpacelookMode);
	m_cSLSideCombo.GetLBText(m_cSLSideCombo.GetCurSel(), m_strSpacelookSide);
	m_cSNDRStepCombo.GetLBText(m_cSNDRStepCombo.GetCurSel(), m_strSndrStepMode);
	m_cInstrCombo.GetLBText(m_cInstrCombo.GetCurSel(), m_strInstrument);
	m_cCoordinateCombo.GetLBText(m_cCoordinateCombo.GetCurSel(), m_strCoordType);

	if (m_strInstrument.CompareNoCase (strInstrument[1]) == 0)
	{
		if (m_strCoordType.CompareNoCase (strCoordType[1]) == 0)
		{
			if (!ValidateInputRange (-90.0, +90.0, &m_cStartNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Latitude value of %s is invalid.\n")
									 _T("It must be in the range of -90.000 to +90.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 360.0, &m_cStartEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Longitude value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 360.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (-90.0, +90.0, &m_cStopNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Latitude value of %s is invalid.\n")
									 _T("It must be in the range of -90.000 to +90.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 360.0, &m_cStopEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Longitude value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 360.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputOrder (&m_cStartNSEdit, &strStartValue, &m_cStopNSEdit, &strStopValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Latitude must be greater than the Stop Latitude.\n")
									 _T("The values entered are %s and %s."), strStartValue, strStopValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputOrder (&m_cStartEWEdit, &strStartValue, &m_cStopEWEdit, &strStopValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Longitude must be greater than the Stop Longitude.\n")
									 _T("The values entered are %s and %s."), strStartValue, strStopValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
		}
		else if (m_strCoordType.CompareNoCase (strCoordType[2]) == 0)
		{
			if (!ValidateInputRange (1.0, 15787.0, &m_cStartNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Line value of %s is invalid.\n")
									 _T("It must be in the range of 1.000 to 15787.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 30680.0, &m_cStartEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Pixel value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 30680.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (1.0, 15787.0, &m_cStopNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Line value of %s is invalid.\n")
									 _T("It must be in the range of 1.000 to 15787.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 30680.0, &m_cStopEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Pixel value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 30680.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputOrder (&m_cStopNSEdit, &strStopValue, &m_cStartNSEdit, &strStartValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Line must be greater than the Start Line.\n")
									 _T("The values entered are %s and %s."), strStartValue, strStopValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputOrder (&m_cStopEWEdit, &strStopValue, &m_cStartEWEdit, &strStartValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Pixel must be greater than the Start Pixel.\n")
									 _T("The values entered are %s and %s."), strStartValue, strStopValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
		}
		else if (m_strCoordType.CompareNoCase (strCoordType[3]) == 0)
		{
			if (!ValidateInputRange (-90.0, +90.0, &m_cStartNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Center Point Latitude value of %s is invalid.\n")
									 _T("It must be in the range of -90.000 to +90.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 360.0, &m_cStartEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Center Point Longitude value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 360.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange ((float)0.1, 10.0, &m_cStopNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Aspect Ratio value of %s is invalid.\n")
									 _T("It must be in the range of 0.100 to 10.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 60.0, &m_cStopEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Duration value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 60.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
		}/*
		else if (m_strCoordType.CompareNoCase (strCoordType[12]) == 0)
		{
			if (!ValidateInputRange (1.0, 15787.0, &m_cStartNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Center Point Line value of %s is invalid.\n")
									 _T("It must be in the range of 1.000 to 15787.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 30680.0, &m_cStartEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Center Point Pixel value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 30680.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange ((float)0.1, 10.0, &m_cStopNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Aspect Ratio value of %s is invalid.\n")
									 _T("It must be in the range of 0.100 to 10.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 60.0, &m_cStopEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Duration value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 60.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
		}*/
	}
	else if (m_strInstrument.CompareNoCase (strInstrument[2]) == 0)
	{
		if (m_strCoordType.CompareNoCase (strCoordType[1]) == 0)
		{
			if (!ValidateInputRange (-90.0, +90.0, &m_cStartNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Latitude value of %s is invalid.\n")
									 _T("It must be in the range of -90.000 to +90.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 360.0, &m_cStartEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Longitude value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 360.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (-90.0, +90.0, &m_cStopNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Latitude value of %s is invalid.\n")
									 _T("It must be in the range of -90.000 to +90.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 360.0, &m_cStopEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Longitude value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 360.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputOrder (&m_cStartNSEdit, &strStartValue, &m_cStopNSEdit, &strStopValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Latitude must be greater than the Stop Latitude.\n")
									 _T("The values entered are %s and %s."), strStartValue, strStopValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputOrder (&m_cStartEWEdit, &strStartValue, &m_cStopEWEdit, &strStopValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Longitude must be greater than the Stop Longitude.\n")
									 _T("The values entered are %s and %s."), strStartValue, strStopValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
		}
		else if (m_strCoordType.CompareNoCase (strCoordType[2]) == 0)
		{
			if (!ValidateInputRange (1.0, 1582.0, &m_cStartNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Line value of %s is invalid.\n")
									 _T("It must be in the range of 1.000 to 1582.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 1758.0, &m_cStartEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Start Pixel value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 1758.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (1.0, 1582.0, &m_cStopNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Line value of %s is invalid.\n")
									 _T("It must be in the range of 1.000 to 1582.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 1758.0, &m_cStopEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Pixel value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 1758.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputOrder (&m_cStopNSEdit, &strStopValue, &m_cStartNSEdit, &strStartValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Line must be greater than the Start Line.\n")
									 _T("The values entered are %s and %s."), strStartValue, strStopValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputOrder (&m_cStopEWEdit, &strStopValue, &m_cStartEWEdit, &strStartValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The Stop Pixel must be greater than the Start Pixel.\n")
									 _T("The values entered are %s and %s."), strStartValue, strStopValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
		}
		else if (m_strCoordType.CompareNoCase (strCoordType[3]) == 0)
		{
			if (!ValidateInputRange (-90.0, +90.0, &m_cStartNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Center Point Latitude value of %s is invalid.\n")
									 _T("It must be in the range of -90.000 to +90.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 360.0, &m_cStartEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Center Point Longitude value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 360.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange ((float)0.1, 10.0, &m_cStopNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Aspect Ratio value of %s is invalid.\n")
									 _T("It must be in the range of 0.100 to 10.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 60.0, &m_cStopEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Duration value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 60.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
		}/*
		else if (m_strCoordType.CompareNoCase (strCoordType[12]) == 0)
		{
			if (!ValidateInputRange (1.0, 1582.0, &m_cStartNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Center Point Line value of %s is invalid.\n")
									 _T("It must be in the range of 1.000 to 1582.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 1758.0, &m_cStartEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Center Point Pixel value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 1758.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange ((float)0.1, 10.0, &m_cStopNSEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Aspect Ratio value of %s is invalid.\n")
									 _T("It must be in the range of 0.100 to 10.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
			if (!ValidateInputRange (0.0, 60.0, &m_cStopEWEdit, &strValue) && bNoError)
			{
				CString strErrorText;
				strErrorText.Format (_T("The SRSO Duration value of %s is invalid.\n")
									 _T("It must be in the range of 0.000 to 60.000"), strValue);
				AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
				bNoError = false;
			}
		}*/
	}

	if (bNoError)
	{
		m_cFrameLabelEdit.GetWindowText(strValue);
		strStartValue = strValue;
		strStartValue.TrimLeft();
		strStartValue.TrimRight();
		if (strStartValue.Find(_T(" ")) != -1)
		{
			CString strErrorText;
			strErrorText.Format (_T("The Label \"%s\" is invalid.\n")
								 _T("It must be a single word with no leading, trailing, or embedded blanks."), strValue);
			AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
			bNoError = false;
		}
	}
	
	if (bNoError)
	{
		m_cDescEdit.GetWindowText(strValue);
		if (strValue.Replace (_T(','), _T(' ')) != 0)
		{
			CString strErrorText;
			strErrorText.Format (_T("The description is invalid.\n")
								 _T("It must not contain any commas."));
			AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
			bNoError = false;
		}
	}

	if (bNoError)
	{
		CString strInstrCombo;
		CString strSNDRStepCombo;
		m_cInstrCombo.GetLBText(m_cInstrCombo.GetCurSel(), strInstrCombo);
		m_cSNDRStepCombo.GetLBText(m_cSNDRStepCombo.GetCurSel(), strSNDRStepCombo);
		if (((strInstrCombo.CompareNoCase (strInstrument[2])) == 0) &
			((strSNDRStepCombo.CompareNoCase (strSndrStepMode[4])) == 0))
		{
			CString strErrorText;
			strErrorText.Format (_T("Invalid Sounder Step Mode selected for the Sounder."));
			AfxGetMainWnd()->MessageBox(strErrorText, _T("Data Validation Error"), MB_OK | MB_ICONEXCLAMATION);
			bNoError = false;
		}
	}

	m_cStartNSEdit.GetWindowText(strValue);
	_stscanf_s (strValue, _T("%f"), &m_fStartNSEdit);
	m_cStartEWEdit.GetWindowText(strValue);
	_stscanf_s (strValue, _T("%f"), &m_fStartEWEdit);
	m_cStopNSEdit.GetWindowText(strValue);
	_stscanf_s (strValue, _T("%f"), &m_fStopNSEdit);
	m_cStopEWEdit.GetWindowText(strValue);
	_stscanf_s (strValue, _T("%f"), &m_fStopEWEdit);
	m_cFrameTimeEdit.GetWindowText(m_strFrameTimeEdit);

	if (bNoError)
		CDialog::OnOK();
}
