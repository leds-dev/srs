/////////////////////////////////////////////////////////////////////////////
#if !defined(AFX_INSTOBJEDITDLG_H__7450D844_F84A_11D3_8634_0020EA0406A1__INCLUDED_)
#define AFX_INSTOBJEDITDLG_H__7450D844_F84A_11D3_8634_0020EA0406A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InstObjEditDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInstObjEditDlg dialog

class CInstObjEditDlg : public CDialog
{
// Construction
public:
	CInstObjEditDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CInstObjEditDlg)
	enum { IDD = IDD_INSTOBJEDIT_DIALOG };
	CEdit	m_cDescEdit;
	CEdit	m_cFrameIndexEdit;
	CStatic	m_cIndexStatic;
	CEdit	m_cFrameLabelEdit;
	CButton	m_cIDOK;
	CStatic	m_cSLModeStatic;
	CStatic	m_cSNDRStepStatic;
	CStatic	m_cStopNSLabelStatic;
	CStatic	m_cStopEWLabelStatic;
	CStatic	m_cStartNSLabelStatic;
	CStatic	m_cStartEWLabelStatic;
	CStatic	m_cStopNSStatic;
	CStatic	m_cStopEWStatic;
	CStatic	m_cStartNSStatic;
	CStatic	m_cStartEWStatic;
	CEdit	m_cSRSONSEdit;
	CEdit	m_cSRSOEWEdit;
	CEdit	m_cSRSODurEdit;
	CEdit	m_cSRSOAspectEdit;
	CEdit	m_cStartEWEdit;
	CEdit	m_cStopNSEdit;
	CEdit	m_cStopEWEdit;
	CEdit	m_cStartNSEdit;
	CComboBox	m_cCoordinateCombo;
	CComboBox	m_cSNDRStepCombo;
	CComboBox	m_cSLSideCombo;
	CComboBox	m_cSLModeCombo;
	CComboBox	m_cInstrCombo;
	CString	m_strLabel;
	CString	m_strDescEdit;
	CString	m_strIndex;
	COXMaskedEdit	m_cFrameTimeEdit;
	CString m_strFrameTimeEdit;
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	CString	m_strTitle;
	CString m_strInstrument;
	CString m_strSpacelookSide;
	CString m_strSpacelookMode;
	CString m_strSndrStepMode;
	CString m_strCoordType;
	float	m_fStartEWEdit;
	float	m_fStartNSEdit;
	float	m_fStopEWEdit;
	float	m_fStopNSEdit;
	bool	m_bReadOnly;
	bool	ValidateInputRange (const float fMin, const float fMax, const CEdit* pEditCtrl, CString* strValue);
	bool	ValidateInputOrder (const CEdit* pStartEditCtrl, CString* strStartValue, 
								const CEdit* pStopEditCtrl, CString* strStopValue);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInstObjEditDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CInstObjEditDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnUpdateActiveFields();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INSTOBJEDITDLG_H__7450D844_F84A_11D3_8634_0020EA0406A1__INCLUDED_)
