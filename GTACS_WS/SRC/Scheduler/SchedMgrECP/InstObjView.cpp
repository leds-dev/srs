/**
/////////////////////////////////////////////////////////////////////////////
// InstObjExView.cpp : implementation of the InstObjExView class.          //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// InstObjExView.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class allows users to view, edit, print instrument object documents.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "Resource.h"
#include "MainFrm.h"
#include "SchedMgrECP.h"
#include "Parse.h"
#include "InstObjDoc.h"
#include "InstObjView.h"
#include "InstObjEditDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		FramePrintHookProc
//	Description :	The PrintHookProc is called from when a CPrintDialog
//					is created from within OnFilePrint.  It becomes active
//					after the print dialog has been created but before
//					it is displayed.  It is used so that we can change the
//					static text string that says "Print Pages from x to y"
//					to "Print Frames from x to y".
//	Return :		
//		UINT CALLBACK		-	Always 0
//	Parameters :	
//		HWND hdlg			-	HWND of the dialog box
//		UINT uMsg			-	Should always be WM_INITDIALOG
//		WPARAM wParam		-	Not used
//		LPARAM lParam		-	Not used
//	Note :			
//////////////////////////////////////////////////////////////////////////
UINT CALLBACK FramePrintHookProc (HWND hdlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_INITDIALOG)
	{
			HWND hWnd = GetDlgItem(hdlg, 1058);
			SetWindowText (hWnd, _T("Frames"));
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		SectorPrintHookProc
//	Description :	The PrintHookProc is called from when a CPrintDialog
//					is created from within OnFilePrint.  It becomes active
//					after the print dialog has been created but before
//					it is displayed.  It is used so that we can change the
//					static text string that says "Print Pages from x to y"
//					to "Print Sectors from x to y".
//	Return :		
//		UINT CALLBACK		-	Always 0
//	Parameters :	
//		HWND hdlg			-	HWND of the dialog box
//		UINT uMsg			-	Should always be WM_INITDIALOG
//		WPARAM wParam		-	Not used
//		LPARAM lParam		-	Not used
//	Note :			
//////////////////////////////////////////////////////////////////////////
UINT CALLBACK SectorPrintHookProc (HWND hdlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_INITDIALOG)
	{
			HWND hWnd = GetDlgItem(hdlg, 1058);
			SetWindowText (hWnd, _T("Sectors"));
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		StarPrintHookProc
//	Description :	The PrintHookProc is called from when a CPrintDialog
//					is created from within OnFilePrint.  It becomes active
//					after the print dialog has been created but before
//					it is displayed.  It is used so that we can change the
//					static text string that says "Print Pages from x to y"
//					to "Print Start from x to y".
//	Return :		
//		UINT CALLBACK		-	Always 0
//	Parameters :	
//		HWND hdlg			-	HWND of the dialog box
//		UINT uMsg			-	Should always be WM_INITDIALOG
//		WPARAM wParam		-	Not used
//		LPARAM lParam		-	Not used
//	Note :			
//////////////////////////////////////////////////////////////////////////
UINT CALLBACK StarPrintHookProc (HWND hdlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_INITDIALOG)
	{
			HWND hWnd = GetDlgItem(hdlg, 1058);
			SetWindowText (hWnd, _T("Stars"));
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		AbortPrintDlgProc
//	Description :	This function is a callback function registered
//					whenever the Print Abort dialog box is displayed.  It
//					is created from within OnFilePrint.  It handles two
//					windows messages.  When the WM_INITDIALOG message
//					is received, it stores a pointer the abort flag that
//					it receives from the lParam parameter.  Then, if
//					it receives a WM_COMMAND with a wParam value of
//					IDCANCEL and BN_CLICKED, it sets this about flag to
//					TRUE;
//	Return :		
//		BOOL CALLBACK		-	TRUE to keep the abort dialog displayed,
//								FALSE to remove it.
//	Parameters :	
//		HWND hWndDlg		-	Not used.
//		UINT uMsg			-	Either WM_INITDIALOG or WM_COMMAND
//		WPARAM wParam		-	Info if uMsg is WM_COMMAND
//		LPARAM lParam		-	Info ir uMsg is WM)INITDIALOG
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CALLBACK AbortPrintDlgProc (HWND hWndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	static LPBOOL pbAbort = NULL;
	switch (uMsg)
	{
		case WM_INITDIALOG:
		{
			ASSERT (lParam);
			pbAbort = (LPBOOL) lParam;
			return TRUE;
		}

		case WM_COMMAND:
		{
			// WORD wID = LOWORD (wParam);  

			if ( LOWORD (wParam) == IDCANCEL && HIWORD (wParam) == BN_CLICKED )
			{
				*pbAbort = TRUE;
			}
			break;
		}
	}
	return 0;
}

IMPLEMENT_DYNCREATE(CInstObjExView, CListView)

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExView::CInstObjExView
//	Description :	Class Constructor
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CInstObjExView::CInstObjExView(){}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExView::~CInstObjExView
//	Description :	Class Desctructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CInstObjExView::~CInstObjExView(){}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExView::OnCreate
//	Description :	Creates the CListView Object when the view is created.
//	Return :		
//		int		-	
//	Parameters :	
//		LPCREATESTRUCT lpCreateStruct	-	
//	Note :			
//////////////////////////////////////////////////////////////////////////
int CInstObjExView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CListView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	return 0;
}

BEGIN_MESSAGE_MAP(CInstObjExView, CListView)
	//{{AFX_MSG_MAP(CInstObjExView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExView::AssertValid
//	Description :	AssertValid Debug function.
//	Return :		
//		void		-	
//	Parameters :	None
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CInstObjExView::AssertValid() const
{
	CListView::AssertValid();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExView::Dump
//	Description :	Dump debug function.
//	Return :		
//		void		-	
//	Parameters :	
//		CDumpContext& dc	-	Dump context
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CInstObjExView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif //_DEBUG

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExView::PreCreateWindow
//	Description :	This function is used to set the list control's
//					style bits to "Report View", "Single Select".
//	Return :		
//		BOOL		-	Status	
//	Parameters :	
//		CREATESTRUCT& cs	-	CREATESTRUCT of the object
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CInstObjExView::PreCreateWindow(CREATESTRUCT& cs) 
{
	cs.style &= ~LVS_TYPEMASK;
	cs.style |= LVS_REPORT | LVS_SINGLESEL;
	return CListView::PreCreateWindow(cs);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExView::OnInitialUpdate
//	Description :	This routine creates all of the columns in the view.
//					The child classes of this class must call this routine
//					after setting up the data.
//	Return :		
//		void		-	
//	Parameters :	None.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CInstObjExView::OnInitialUpdate() 
{
	CListView::OnInitialUpdate();

	HDC hDC = CreateDC (_T("Display"), NULL, NULL, NULL);
	TEXTMETRIC tm;
	GetTextMetrics (hDC, &tm);
	DeleteDC(hDC);
	tm.tmAveCharWidth;

	CListCtrl&	theCtrl = GetListCtrl();
	theCtrl.SetExtendedStyle(theCtrl.GetExtendedStyle()|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	theCtrl.DeleteAllItems();

	for (int nColumn=0; nColumn<m_nNumParams; nColumn++)
	{
		int nJustify = LVCFMT_LEFT;
		switch (GetAppRegInstObj()->m_nDisplayJustify[m_nObjType][nColumn])
		{
			case 0 : nJustify = LVCFMT_LEFT;	break;
			case 1 : nJustify = LVCFMT_CENTER;	break;
			case 2 : nJustify = LVCFMT_RIGHT;	break;
		}

		int nScreenWidth;
		if (GetAppRegInstObj()->m_nDisplay[m_nObjType][nColumn])
			nScreenWidth = GetAppRegInstObj()->m_nDisplayWidth[m_nObjType][nColumn] * tm.tmAveCharWidth;
		else
			nScreenWidth = 0;

		theCtrl.InsertColumn(nColumn, GetAppRegInstObj()->m_strPropTitle[m_nObjType][nColumn], nJustify, nScreenWidth, nColumn);
	}
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CInstObjExView::OnFilePrint
//	Description :	Called to print the view.
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CInstObjExView::OnFilePrint()
{
	MSG msg;
	CInstObjExDoc* pDoc = (CInstObjExDoc*)GetDocument();

	CPrintDialog pd (false, PD_ALLPAGES | PD_HIDEPRINTTOFILE | PD_RETURNDC |
							PD_USEDEVMODECOPIES | PD_ENABLEPRINTHOOK | PD_NOSELECTION, NULL);

	// The max number of pages (lines) is 65535.  This is a Microsoft limit on the GUI
	// control.  A user can print more than this, he just can't selectivly print out
	// lines above this range/
	unsigned short nMaxObj;
	if (pDoc->m_strDataRecord.GetSize() >= USHRT_MAX)
		nMaxObj = USHRT_MAX;
	else
		nMaxObj = (unsigned short)pDoc->m_strDataRecord.GetSize();

	// Setup defaults for the print dialog box
	pd.m_pd.hDevMode=((CSchedMgrECPApp*)AfxGetApp())->m_hDevMode;
	pd.m_pd.hDevNames=((CSchedMgrECPApp*)AfxGetApp())->m_hDevNames;
	pd.m_pd.nMinPage=1;
	pd.m_pd.nMaxPage=nMaxObj;
	pd.m_pd.lpfnPrintHook = m_pPrintHookProc;

	// If the dialog box was closed with an OK
	if (pd.DoModal() == IDOK)
	{
		// Margins in inches
		HDC hDC = pd.CreatePrinterDC();
		if (hDC != NULL)
		{
			SetMapMode(hDC, MM_TEXT);
			
			((CSchedMgrECPApp*)AfxGetApp())->m_hDevMode = pd.m_pd.hDevMode;
			((CSchedMgrECPApp*)AfxGetApp())->m_hDevNames= pd.m_pd.hDevNames;

			CPoint cPhysical;
			CPoint cPrintable;
			CPoint cOffset;
			CPoint cLogPixels;

			cPhysical.x		= GetDeviceCaps(hDC, PHYSICALWIDTH);
			cPhysical.y		= GetDeviceCaps(hDC, PHYSICALHEIGHT);
			cPrintable.x	= GetDeviceCaps(hDC, HORZRES);
			cPrintable.y	= GetDeviceCaps(hDC, VERTRES);
			cOffset.x		= GetDeviceCaps(hDC, PHYSICALOFFSETX);
			cOffset.y		= GetDeviceCaps(hDC, PHYSICALOFFSETY);
			cLogPixels.x	= GetDeviceCaps(hDC, LOGPIXELSX);
			cLogPixels.y	= GetDeviceCaps(hDC, LOGPIXELSY);

			// Offsets from edge of page to start of printable area in pixels
			int	nTopOffset		= cOffset.y;
			int nBottomOffset	= cPhysical.y - cPrintable.y - cOffset.y;
			int	nLeftOffset		= cOffset.x;
			int nRightOffset	= cPhysical.x - cPrintable.x - cOffset.x;

			// Width of margins in pixels
			// Use the larger of the physical offset needed by the printer or 
			// the size requested by the operator
			int nTopMargin		= __max((int)(m_fPrintTopMargin * cLogPixels.y), nTopOffset);
			int nBottomMargin	= __max((int)(m_fPrintBottomMargin * cLogPixels.y), nBottomOffset);
			int nLeftMargin		= __max((int)(m_fPrintLeftMargin * cLogPixels.x), nLeftOffset);
			int nRightMargin	= __max((int)(m_fPrintRightMargin * cLogPixels.x), nRightOffset);

			// If the margins are too big then reset them back to the minimum needed
			if ((nTopMargin + nBottomMargin) > cPhysical.y)
			{
				nTopMargin = nTopOffset;
				nBottomMargin = nBottomOffset;
			}
			if ((nLeftMargin + nRightMargin) > cPhysical.x)
			{
				nLeftMargin = nLeftOffset;
				nRightMargin = nRightOffset;
			}

			// Readjust the printable area using the new margins
			cPrintable.x = cPhysical.x - nLeftMargin - nRightMargin;
			cPrintable.y = cPhysical.y - nTopMargin - nBottomMargin;
			
			int nTech		= GetDeviceCaps(hDC, TECHNOLOGY);

			HFONT hFont = CreateFont (-MulDiv (m_nPrintPoint, GetDeviceCaps (hDC, LOGPIXELSY), 72),
									  0, 0, 0, m_nPrintWeight,
									  m_nPrintItalic,
									  m_nPrintUnderline,
									  m_nPrintStrikeOut,
									  0, 0, 0, 0, 0, m_strPrintFontName);
			SelectObject(hDC, hFont);

			TCHAR szTitle[ _MAX_PATH + 1 ];
			_tcscpy_s (szTitle, pDoc->GetPathName());
			DOCINFO di;
			memset (&di, 0, sizeof(DOCINFO));
			di.cbSize = sizeof(DOCINFO); 
			di.lpszDocName = szTitle; 
			di.lpszOutput = (LPTSTR) NULL; 
			di.lpszDatatype = (LPTSTR) NULL; 
			di.fwType = 0; 

			TEXTMETRIC tm;
			VERIFY (GetTextMetrics (hDC, &tm));
			CPoint cTextSize;
			cTextSize.x = tm.tmAveCharWidth;
			cTextSize.y = tm.tmHeight;

			// Determine the number of whole lines that can fit within the printable area
			int nMaxLinesPerPage = cPrintable.y / cTextSize.y;
			int nMaxCharsPerLine = cPrintable.x / cTextSize.x;
 
			// Determine the size of a tab character in pixels
		// 	int cxTab = (tm.tmAveCharWidth*10) * 4;

			int nCurObj = 0;
			int nStartObj = 0;
			int nEndObj = 0;

			// Should we print the whole document, the range specified, or the highlighted selection?
			if (pd.PrintAll())
			{
				nStartObj = 0;
				nEndObj =  nMaxObj - 1;
			}
			else 
			{
				nStartObj = pd.GetFromPage() - 1;
				nEndObj = pd.GetToPage() - 1;
			}

	//		BOOL bAbort = FALSE;

			BOOL bContinue = TRUE;
			int	nInstObjPerPage;						 //Number of Inst Objs that can be printed per page
			
			if(nTech == DT_CHARSTREAM)
				nInstObjPerPage = 58;					 //62 lines minus the header and footer
			else
				nInstObjPerPage = nMaxLinesPerPage - 6;//Minus 2 lines for hdr, 2 for footer, 2 for col hdr ;

			int nPageCount = 1;
			CString strPage;
			CString strCurLine;

			SetTextAlign(hDC, TA_BOTTOM);

			int nIndex;
			int nCharCount = 0;
			
			for (nIndex=0; nIndex<m_nNumParams; nIndex++)
			{
				if (GetAppRegInstObj()->m_nPrint[m_nObjType][nIndex])
				{
					nCharCount = nCharCount + GetAppRegInstObj()->m_nPrintWidth[m_nObjType][nIndex] + 1;
				}
			}
			// Subtract off the last character since it is just a spacer between columns
			if (nCharCount > 0) nCharCount = nCharCount - 1;

			// If total width of the columns to print is 0, then no columns were selected
			if (nCharCount == 0)
			{
				CString strPrompt;
				strPrompt.Format (_T("No columns were selected to print.  From within\n")
								  _T("Workspace Options..., choose the columns you want to\n")
								  _T("print along with the width of each column."));
				// int retval = AfxGetMainWnd()->MessageBox(strPrompt, _T("No columns selected"), MB_OK | MB_ICONEXCLAMATION);
				AfxGetMainWnd()->MessageBox(strPrompt, _T("No columns selected"), MB_OK | MB_ICONEXCLAMATION);
				bContinue = false;
			}

			// If we are still printing then...
			// If total width of the columns to print is greate then the number of chars per line
			// then ask the user if he wants the lines truncated or the print job canceled
			if (bContinue)
			{
				if (nCharCount >= nMaxCharsPerLine)
				{
					CString strPrompt;
					strPrompt.Format (_T("The paper size, orientation, and font selected allow\n")
									  _T("for a maximum of %d characters per line.  The total size\n")
									  _T("of all of the columns selected to print is %d.  Select\n")
									  _T("OK to truncate all lines to fit within the page margins, or\n")
									  _T("select Cancel to cancel the print request."), nMaxCharsPerLine,
									  nCharCount);
					int retval = AfxGetMainWnd()->MessageBox(strPrompt, _T("Lines to long"), MB_OKCANCEL | MB_ICONEXCLAMATION);
					if (retval == IDCANCEL)
						bContinue = false;
				}
			}

			if (bContinue)
			{
				// Call StartDoc to begin the printing of a document
				// int an = StartDoc(hDC, &di);
				StartDoc(hDC, &di);
				//VERIFY (StartDoc (hDC, &di) > 0);

				// Create the Printer Abort dialog box
				// This dialog box will be displayed while the printing is taking place and allow the user
				// the option of canceling the print job
				// If canceled, the bAbort flag will set to true from within the AbortDlgProc procedure
				bool bAbort = false;
				HWND hWndAbort = CreateDialogParam (AfxGetApp()->m_hInstance, MAKEINTRESOURCE (IDD_ABORTPRINT), 
													NULL, (DLGPROC) AbortPrintDlgProc, (LPARAM) &bAbort);

				// Display the Printer Abort dialog box
				// Search through message queue looking for a click on the Cancel button
				::ShowWindow(hWndAbort, SW_SHOW);
				while (PeekMessage(&msg, hWndAbort, NULL, NULL, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}

				while (bContinue && !bAbort)
				{
					// Begin to print a page
					// int a = StartPage(hDC);
					StartPage(hDC);
					// int b = GetLastError();
					//VERIFY (StartPage(hDC) > 0 );

					// Output the header
					CRect cBounds;
					cBounds.bottom	= nTopMargin;
					cBounds.top		= cBounds.bottom - cTextSize.y;
					cBounds.left	= nLeftMargin;
					cBounds.right	= nLeftMargin + cPrintable.x;
					SetTextAlign (hDC, TA_LEFT|TA_BOTTOM);
					ExtTextOut (hDC, cBounds.left, cBounds.bottom, ETO_CLIPPED, cBounds,
								pDoc->GetPathName(), pDoc->GetPathName().GetLength(), NULL);

					// Output the footer
					cBounds.bottom	= nTopMargin + cPrintable.y;
					cBounds.top		= cBounds.top - cTextSize.y;
					cBounds.left	= nLeftMargin;
					cBounds.right	= nLeftMargin + cPrintable.x;
					CString strPage;
					strPage.Format(_T("Page: %d"), nPageCount);
					ExtTextOut (hDC, cBounds.left, cBounds.bottom, ETO_CLIPPED, cBounds,
								strPage, strPage.GetLength(), NULL);
					
					// Fill up the page with text
					nPageCount++;

					// Position ourselves back up to the first line that we will print data on
					// Creating a bounding box for this row
					// The left side is against the margin, while width is equal to 0
					cBounds.bottom = nTopMargin + (cTextSize.y*2);
					cBounds.top = cBounds.bottom - cTextSize.y;
					cBounds.left = nLeftMargin;
					cBounds.right = 0;

					// We now need to print out column headers for each column that will be printer
					int nXPos;
					for (nIndex=0; nIndex<m_nNumParams; nIndex++)
					{
						if (GetAppRegInstObj()->m_nPrint[m_nObjType][nIndex])
						{
							// Adjust the width of the bounding box to include the proper number of characters
							// Set the text alignment according the justification specified (Left, Centered, or Right)
							// Load the string used for the column header from the string table
							// Output the string
							// Readjust the left and right sides of the bounding box
							cBounds.right = cBounds.left +
								(GetAppRegInstObj()->m_nPrintWidth[m_nObjType][nIndex]*cTextSize.x);
							
							switch (GetAppRegInstObj()->m_nPrintJustify[m_nObjType][nIndex])
							{
								case 0 :
								{
									SetTextAlign (hDC, TA_LEFT|TA_BOTTOM);
									nXPos = cBounds.left;
									break;
								}
								case 1 : 
								{
									SetTextAlign (hDC, TA_CENTER|TA_BOTTOM);
									nXPos = (cBounds.left+cBounds.right)/2;
									break;
								}
								case 2 :
								{
									SetTextAlign (hDC, TA_RIGHT|TA_BOTTOM);
									nXPos = cBounds.right;
									break;
								}
							}

							CString strHeader = GetAppRegInstObj()->m_strPropTitle[m_nObjType][nIndex];

							// If the right edge goes past the right margin, then truncate the right edge
							if (cBounds.right > (nLeftMargin + cPrintable.x))
								cBounds.right = nLeftMargin + cPrintable.x;

							// If the left edge is within the printable area, then print the line
							if (cBounds.left < (nLeftMargin + cPrintable.x))
							{
								ExtTextOut (hDC, nXPos, cBounds.bottom, ETO_CLIPPED, cBounds, 
											strHeader, strHeader.GetLength(), NULL);
							}
							cBounds.right = cBounds.right + cTextSize.x;
							cBounds.left = cBounds.right;
						}
					}

					// Increment the current line number that we are on
					// Reset the bounding box back to the left margin
					cBounds.bottom = nTopMargin + (cTextSize.y*3);
					cBounds.top = cBounds.bottom - cTextSize.y;
					cBounds.left = nLeftMargin;
					cBounds.right = 0;
					
					// We now need to print out a row of dashes under each column heading
					for (nIndex=0; nIndex<m_nNumParams; nIndex++)
					{
						if (GetAppRegInstObj()->m_nPrint[m_nObjType][nIndex])
						{
							// Adjust the width of the bounding box to include the proper number of characters
							// Set the text alignment according the justification specified (Left, Centered, or Right)
							// Create a string of dashes
							// Output the string
							// Readjust the left and right sides of the bounding box
							cBounds.right = cBounds.left +
								(GetAppRegInstObj()->m_nPrintWidth[m_nObjType][nIndex]*cTextSize.x);
							
							switch (GetAppRegInstObj()->m_nPrintJustify[m_nObjType][nIndex])
							{
								case 0 :
								{
									SetTextAlign (hDC, TA_LEFT|TA_BOTTOM);
									nXPos = cBounds.left;
									break;
								}
								case 1 : 
								{
									SetTextAlign (hDC, TA_CENTER|TA_BOTTOM);
									nXPos = (cBounds.left+cBounds.right)/2;
									break;
								}
								case 2 :
								{
									SetTextAlign (hDC, TA_RIGHT|TA_BOTTOM);
									nXPos = cBounds.right;
									break;
								}
							}

							// If the right edge goes past the right margin, then truncate the right edge
							if (cBounds.right > (nLeftMargin + cPrintable.x))
								cBounds.right = nLeftMargin + cPrintable.x;

							// If the left edge is within the printable area, then print the line
							if (cBounds.left < (nLeftMargin + cPrintable.x))
							{
								CString strDash('-', GetAppRegInstObj()->m_nPrintWidth[m_nObjType][nIndex]);
								ExtTextOut (hDC, nXPos, cBounds.bottom, ETO_CLIPPED, cBounds, 
											strDash, strDash.GetLength(), NULL);
							}
							cBounds.right = cBounds.right + cTextSize.x;
							cBounds.left = cBounds.right;
						}
					}


					// Increment the current line number that we are on
					// Reset the bounding box back to the left margin
					int nCurObjCntOnPage = 0;
					while (nCurObjCntOnPage <= nInstObjPerPage)
					{
						cBounds.bottom = nTopMargin + (cTextSize.y*(4 + nCurObjCntOnPage));
						cBounds.top = cBounds.bottom - cTextSize.y;
						cBounds.left = nLeftMargin;
						cBounds.right = 0;
						// If we have already printed to the last selected frame then we are done
						if (nCurObj > nEndObj)
						{
							bContinue = FALSE;
							break;
						}
						else
						{
							// Only print Frames within the range selected
							CParseSector	cParseSector;
							CParseFrame		cParseFrame;
							CParseStar		cParseStar;
							if ((nCurObj >= nStartObj) && (pDoc->m_nHasData[nCurObj]))
							{
								switch (m_nObjType)
								{
									case nInstObjSector : 
									{
										cParseSector.SetData(((CSectorObjDoc*)GetDocument())->m_strDataRecord[nCurObj]);
										cParseSector.ParseData();
										break;
									}
									case nInstObjFrame : 
									{
										cParseFrame.SetData(((CFrameObjDoc*)GetDocument())->m_strDataRecord[nCurObj]);
										cParseFrame.ParseData();
										break;
									}
									case nInstObjStar : 
									{
										cParseStar.SetData(((CStarObjDoc*)GetDocument())->m_strDataRecord[nCurObj]);
										cParseStar.ParseData();
										break;
									}
									default	:
									{
										ASSERT (FALSE);
										break;
									}
								}

								for (nIndex=0; nIndex<m_nNumParams; nIndex++)
								{
									if (GetAppRegInstObj()->m_nPrint[m_nObjType][nIndex])
									{
										cBounds.right = cBounds.left +
											(GetAppRegInstObj()->m_nPrintWidth[m_nObjType][nIndex]*cTextSize.x);
						
										switch (GetAppRegInstObj()->m_nPrintJustify[m_nObjType][nIndex])
										{
											case 0 :
											{
												SetTextAlign (hDC, TA_LEFT|TA_BOTTOM);
												nXPos = cBounds.left;
												break;
											}
											case 1 : 
											{
												SetTextAlign (hDC, TA_CENTER|TA_BOTTOM);
												nXPos = (cBounds.left+cBounds.right)/2;
												break;
											}
											case 2 :
											{
												SetTextAlign (hDC, TA_RIGHT|TA_BOTTOM);
												nXPos = cBounds.right;
												break;
											}
										}
										if (cBounds.right > (nLeftMargin + cPrintable.x))
											cBounds.right = nLeftMargin + cPrintable.x;

										// If the left edge is within the printable area, then print the line
										if (cBounds.left < (nLeftMargin + cPrintable.x))
										{
											CString strTextField;
											switch (m_nObjType)
											{
												case nInstObjSector	:
												{
													strTextField = cParseSector.GetData(nIndex);
													break;
												};
												case nInstObjFrame	: 
												{
													strTextField = cParseFrame.GetData(nIndex);
													break;
												};
												case nInstObjStar	:
												{
													strTextField = cParseStar.GetData(nIndex);
													break;
												};
												default				: ASSERT(FALSE);	break;
											}
											ExtTextOut (hDC, nXPos, cBounds.bottom, ETO_CLIPPED, cBounds, 
														strTextField, strTextField.GetLength(), NULL);
										}
										cBounds.right = cBounds.right + cTextSize.x;
										cBounds.left = cBounds.right;
									}
								}
								nCurObjCntOnPage++;

							}
							nCurObj++;
						}
					}

					// At this point we have just printed one page
					// Call EndPage so the printer will eject the paper
					// Flush the message queue for the abort dlg so it can process the Cancel button
					VERIFY (EndPage(hDC) > 0);
					while (PeekMessage(&msg, hWndAbort, NULL, NULL, PM_REMOVE))
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
					if (nCurObj > nEndObj)
						bContinue = FALSE;
				}

				// At this point, the print job is either done, or it was aborted
				// If it was aborted call AbortDoc otherwise call EndDoc
				if (bAbort)
				{
					VERIFY (AbortDoc(hDC) > 0);
				}
				else
				{
					VERIFY (EndDoc(hDC) > 0);
				}
		
				// Delete the Print Abort dialog from the display
				// Delete the Device Context and Font resources used for the print job
				::DestroyWindow (hWndAbort);
				DeleteDC (hDC);
				DeleteObject (hFont);
			}
		}
		else
		{
			GetAppOutputBar()->OutputToEvents(_T("Could not retrieve Device Context from printer"), COutputBar::FATAL);
		}
	}
}

IMPLEMENT_DYNCREATE(CFrameObjView, CInstObjExView)

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::CFrameObjView
//	Description :	Class Constructor.
//	Return :		
//		constructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CFrameObjView::CFrameObjView(){}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::~CFrameObjView
//	Description :	Class Destructor
//	Return :		
//		destructor		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
CFrameObjView::~CFrameObjView(){}

BEGIN_MESSAGE_MAP(CFrameObjView, CInstObjExView)
	//{{AFX_MSG_MAP(CFrameObjView)
	ON_COMMAND(ID_TOOLS_EDITFRAME, OnToolsEditFrame)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_EDITFRAME, OnUpdateToolsEditFrame)
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_WM_SETFOCUS()
	ON_COMMAND(ID_TOOLS_DELETEFRAME, OnToolsDeleteFrame)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_DELETEFRAME, OnUpdateToolsDeleteFrame)
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::AssertValid
//	Description :	Debug AssertValid routine.
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::AssertValid() const
{
	CInstObjExView::AssertValid();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::Dump
//	Description :	Debug Dump routine.
//	Return :		
//		void		-	
//	Parameters :	
//		CDumpContext& dc	-	Dump context
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::Dump(CDumpContext& dc) const
{
	CInstObjExView::Dump(dc);
}
#endif //_DEBUG

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnInitialUpdate
//	Description :	This routine will setup its parents member variables
//					so that it knows what type of instrument object we
//					have.  The parent OnInitialUpdate is then called
//					to create all of the columns of the proper size and
//					justification.  Then, the data is read from the 
//					document.  OnUpdate is called for each row so that
//					the data can be displayed.
//	Return :		
//		void		-	
//	Parameters :	None.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnInitialUpdate()
{
	m_nObjType = nInstObjFrame;
	m_nNumParams = nMaxInstObjProps[m_nObjType];
	
	CInstObjExView::OnInitialUpdate();
	CListCtrl&	theCtrl = GetListCtrl();
	CUpdateSingle	single;

	for (int nRow=0; nRow<((CFrameObjDoc*)GetDocument())->m_strDataRecord.GetSize(); nRow++)
	{
		CString strIndex;
		strIndex.Format (_T("%d"), nRow+1);
		theCtrl.InsertItem(nRow, strIndex);
		theCtrl.SetItemData (nRow, 0);
		OnUpdate(this, nRow, &single);
	}
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnUpdate
//	Description :	This routine is responsible for updating a row on the
//					list control.  It should be called whenever the
//					documents data is modified.
//	Return :		
//		void		-	
//	Parameters :	
//		CView* pSender	-	Not used.
//		LPARAM lHint	-	The row that should be updated.
//		CObject* pHint	-	This is a pointer to an object of type
//							RUNTIME_CLASS(CUpdateSingle).  The object is
//							not used for anything other then checking
//							its type.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	if (pHint != NULL)
	{
		if (pHint->IsKindOf(RUNTIME_CLASS(CUpdateSingle)))
		{
			CParseFrame cParseFrame;
			CStringArray	strArrayData;
			CListCtrl&		listCtrl = GetListCtrl();
			if (lHint>=((CFrameObjDoc*)GetDocument())->m_strDataRecord.GetSize())
			{
				((CFrameObjDoc*)GetDocument())->m_strDataRecord.SetAtGrow(lHint, cParseFrame.GetDataNew(lHint));
				((CFrameObjDoc*)GetDocument())->m_nHasData.SetAtGrow(lHint, FALSE);
			}
			cParseFrame.SetData(((CFrameObjDoc*)GetDocument())->m_strDataRecord[lHint]);
			if ((cParseFrame.ParseData() && ((CFrameObjDoc*)GetDocument())->m_nHasData[lHint]))
			{
				listCtrl.SetItemText(lHint, eFrameIndex, cParseFrame.GetDataIndex());
				listCtrl.SetItemText(lHint, eFrameTime, cParseFrame.GetDataTime());
				listCtrl.SetItemText(lHint, eFrameLabel, cParseFrame.GetDataLabel());
				listCtrl.SetItemText(lHint, eFrameInstrument, cParseFrame.GetDataInstrument());
				listCtrl.SetItemText(lHint, eFrameSpacelookMode, cParseFrame.GetDataSpacelookMode());
				listCtrl.SetItemText(lHint, eFrameSpacelookSide, cParseFrame.GetDataSpacelookSide());
				listCtrl.SetItemText(lHint, eFrameSndrStepMode, cParseFrame.GetDataSndrStepMode());
				listCtrl.SetItemText(lHint, eFrameCoordType, cParseFrame.GetDataCoordType());
				listCtrl.SetItemText(lHint, eFrameStartNS, cParseFrame.GetDataStartNS());
				listCtrl.SetItemText(lHint, eFrameStartEW, cParseFrame.GetDataStartEW());
				listCtrl.SetItemText(lHint, eFrameStopNS, cParseFrame.GetDataStopNS());
				listCtrl.SetItemText(lHint, eFrameStopEW, cParseFrame.GetDataStopEW());
				listCtrl.SetItemText(lHint, eFrameStartCycNS, cParseFrame.GetDataStartCycNS());
				listCtrl.SetItemText(lHint, eFrameStartIncrNS, cParseFrame.GetDataStartIncNS());
				listCtrl.SetItemText(lHint, eFrameStartCycEW, cParseFrame.GetDataStartCycEW());
				listCtrl.SetItemText(lHint, eFrameStartIncrEW, cParseFrame.GetDataStartIncEW());
				listCtrl.SetItemText(lHint, eFrameStopCycNS, cParseFrame.GetDataStopCycNS());
				listCtrl.SetItemText(lHint, eFrameStopIncrNS, cParseFrame.GetDataStopIncNS());
				listCtrl.SetItemText(lHint, eFrameStopCycEW, cParseFrame.GetDataStopCycEW());
				listCtrl.SetItemText(lHint, eFrameStopIncrEW, cParseFrame.GetDataStopIncEW());
				listCtrl.SetItemText(lHint, eFrameDur, cParseFrame.GetDataDuration());
				listCtrl.SetItemText(lHint, eFrameDesc, cParseFrame.GetDataDesc());
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnLButtonDblClk
//	Description :	OnLButtonDblClk is called whenever a user double
//					clicks on the list control.  When this happens, a
//					dialog ox is displayed to allow the user to edit
//					the data on the line that was clicked.
//	Return :		
//		void		-	
//	Parameters :	
//		UINT nFlags		-	Flags associated with the click
//		CPoint point	-	Point where the user clicked
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CInstObjEditDlg	dlg;

	dlg.m_strTitle = _T("Edit Frame Parameters");
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		// Get all of the document data for the selected line
		CParseFrame cParseFrame;
		cParseFrame.SetData(((CFrameObjDoc*)GetDocument())->m_strDataRecord[nPos]);
		cParseFrame.ParseData();

		dlg.m_bReadOnly			= true;
		dlg.m_strIndex			= cParseFrame.GetDataIndex();			// Index
		dlg.m_strLabel			= cParseFrame.GetDataLabel();			// Label
		dlg.m_strDescEdit		= cParseFrame.GetDataDesc();			// Description
		dlg.m_strFrameTimeEdit	= cParseFrame.GetDataTime();			// Frame Time
		dlg.m_strInstrument		= cParseFrame.GetDataInstrument();		// Instrument
		dlg.m_strSpacelookMode	= cParseFrame.GetDataSpacelookMode();	// Spacelook Mode
		dlg.m_strSpacelookSide	= cParseFrame.GetDataSpacelookSide();	// Spacelook Side
		dlg.m_strSndrStepMode	= cParseFrame.GetDataSndrStepMode();	// Sounder Step Mode
		dlg.m_strCoordType		= cParseFrame.GetDataCoordType();		// Coordinate Type
		float fStartNS			= cParseFrame.m_fStartNS;
		float fStartEW			= cParseFrame.m_fStartEW;
		float fStopNS			= cParseFrame.m_fStopNS;
		float fStopEW			= cParseFrame.m_fStopEW;

		// If the frame is a new frame assign some defaults
		if (!((CFrameObjDoc*)GetDocument())->m_nHasData[nPos])
		{
			dlg.m_strInstrument = strInstrument[1];
			dlg.m_strSpacelookMode = strSpacelookMode[1];
			dlg.m_strSpacelookSide = strSpacelookSide[1];
			dlg.m_strSndrStepMode = strSndrStepMode[4];
			dlg.m_strCoordType = strCoordType[1];
			fStartNS = 0.0;
			fStartEW = 0.0;
			fStopNS  = 0.0;
			fStopEW  = 0.0;
		}

		// Convert the floating point numbers
		dlg.m_fStartNSEdit = fStartNS;		
		dlg.m_fStartEWEdit = fStartEW;	
		dlg.m_fStopNSEdit = fStopNS;
		dlg.m_fStopEWEdit = fStopEW;

		if (dlg.DoModal() == IDOK)
		{
			cParseFrame.SetDataIndex(dlg.m_strIndex);
			cParseFrame.m_strLabel = dlg.m_strLabel;
			cParseFrame.SetDataTime(dlg.m_strFrameTimeEdit);
			cParseFrame.SetDataInstrument(dlg.m_strInstrument);
			cParseFrame.SetDataSpacelookMode(dlg.m_strSpacelookMode);
			cParseFrame.SetDataSpacelookSide(dlg.m_strSpacelookSide);
			cParseFrame.SetDataSndrStepMode(dlg.m_strSndrStepMode);
			cParseFrame.SetDataCoordType(dlg.m_strCoordType);
			cParseFrame.m_fStartNS = dlg.m_fStartNSEdit;
			cParseFrame.m_fStartEW = dlg.m_fStartEWEdit;
			cParseFrame.m_fStopNS = dlg.m_fStopNSEdit;
			cParseFrame.m_fStopEW = dlg.m_fStopEWEdit;
			cParseFrame.m_strDesc = dlg.m_strDescEdit;

			CUpdateSingle	single;
			((CFrameObjDoc*)GetDocument())->m_strDataRecord[nPos] = cParseFrame.GetData();
			((CFrameObjDoc*)GetDocument())->m_nHasData[nPos] = TRUE;
			((CFrameObjDoc*)GetDocument())->MarkModified(true);
			((CFrameObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &single);
		}
	}
	CInstObjExView::OnLButtonDblClk(nFlags, point);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnToolsEditFrame
//	Description :	The routine is called whenever a user selects Edit
//					frame from a toolbar or menu.  It simply calls
//					OnLButtonDblClk to process the request.
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnToolsEditFrame() 
{
	UINT nFlags = 0;
	CPoint point = 0;
	OnLButtonDblClk(nFlags, point);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnUpdateToolsEditFrame
//	Description :	This routine is called by the framework to either
//					enable or disable the Edit Frame toolbar button/menu
//					item.
//	Return :		
//		void		-	
//	Parameters :	
//		CCmdUI* pCmdUI	-	Command UI object.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnUpdateToolsEditFrame(CCmdUI* pCmdUI) 
{
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
		pCmdUI->Enable(true);
	else
		pCmdUI->Enable(false);
}


void CFrameObjView::OnFilePrint() 
{
	TCHAR*	pStopString;
	m_fPrintTopMargin		= _tcstod(GetAppRegInstObj()->m_strTopMargin[nInstObjFrame], &pStopString);
	m_fPrintBottomMargin	= _tcstod(GetAppRegInstObj()->m_strBottomMargin[nInstObjFrame], &pStopString);
	m_fPrintLeftMargin		= _tcstod(GetAppRegInstObj()->m_strLeftMargin[nInstObjFrame], &pStopString);
	m_fPrintRightMargin		= _tcstod(GetAppRegInstObj()->m_strRightMargin[nInstObjFrame], &pStopString);
	m_nPrintPoint			= GetAppRegInstObj()->m_nFontSize[nInstObjFrame];
	m_nPrintWeight			= GetAppRegInstObj()->m_nFontWeight[nInstObjFrame];
	m_nPrintItalic			= GetAppRegInstObj()->m_bFontIsItalic[nInstObjFrame];
	m_nPrintUnderline		= GetAppRegInstObj()->m_bFontIsUnderline[nInstObjFrame];
	m_nPrintStrikeOut		= GetAppRegInstObj()->m_bFontIsStrikeOut[nInstObjFrame];
	m_strPrintFontName		= GetAppRegInstObj()->m_strFontFaceName[nInstObjFrame];
	m_pPrintHookProc		= FramePrintHookProc;
	m_nObjType				= nInstObjFrame;

	CInstObjExView::OnFilePrint();
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnUpdateFilePrint
//	Description :	This routine is called by the framework to either
//					enable or disable the print toolbar button/menu
//					item.  In this case, the item is always enabled.
//	Return :		
//		void		-	
//	Parameters :	
//		CCmdUI* pCmdUI	-	Command UI object.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);	
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnEditCut
//	Description :	This routine inplements the cut operation.  First, the
//					selected line is copied into the clipboard by calling
//					OnEditCopy.  Then, the document is updated with a
//					blank record for the selected line.  Finally, 
//					UpdateAllViews is called to update all of the views
//					with the new document data.
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnEditCut() 
{
	CListCtrl&	theCtrl = GetListCtrl();

	int nPos = (int) theCtrl.GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		CUpdateSingle single;
		OnEditCopy();
		CParseFrame cParseFrame;
		((CFrameObjDoc*)GetDocument())->m_strDataRecord[nPos] = cParseFrame.GetDataNew(nPos);
		((CFrameObjDoc*)GetDocument())->MarkModified(true);
		((CFrameObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &single);
		((CFrameObjDoc*)GetDocument())->m_nHasData[nPos] = FALSE;
	}
	else
		MessageBeep(0xFFFFFFFF);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnUpdateEditCut
//	Description :	This routine is called by the frameworks to either
//					enable or disable the Cut toolbar button/menu item.
//					It will only be enabled if the list control has
//					focus and a row that has data is currently selected.
//	Return :		
//		void		-	
//	Parameters :	
//		CCmdUI* pCmdUI	-	Command UI object.	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnUpdateEditCut(CCmdUI* pCmdUI)
{
	CListCtrl&	theCtrl = GetListCtrl();
	int nPos = (int) theCtrl.GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		if (((CFrameObjDoc*)GetDocument())->m_nHasData[nPos])
			pCmdUI->Enable (true);
		else
			pCmdUI->Enable (false);
	}
	else
		pCmdUI->Enable (false);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnEditCopy
//	Description :	This routine will copy the selected row into the
//					clipboard
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnEditCopy() 
{
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		COleDataSource*	pSource = new COleDataSource();
		CSharedFile		sf(GMEM_MOVEABLE|GMEM_DDESHARE|GMEM_ZEROINIT);
		// UINT			format = RegisterClipboardFormat(_T("FrameDef"));
		CLIPFORMAT		format = (CLIPFORMAT)RegisterClipboardFormat(_T("FrameDef"));
		CArchive		ar (&sf, CArchive::store);
		ar << ((CFrameObjDoc*)GetDocument())->m_strDataRecord[nPos];
		ar.Close();

		HGLOBAL hMem = sf.Detach();
		if (!hMem)return;
		pSource->CacheGlobalData(format, hMem);
		pSource->SetClipboard();
	}
	else
		MessageBeep(0xFFFFFFFF);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnUpdateEditCopy
//	Description :	This routine is called by the frameworks to either
//					enable or disable the Copy toolbar button/menu item.
//					It will only be enabled if the list control has
//					focus and a row that has data is currently selected.
//	Return :		
//		void		-	
//	Parameters :	
//		CCmdUI* pCmdUI	-	Command UI object.	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	CListCtrl&	theCtrl = GetListCtrl();
	int nPos = (int) theCtrl.GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		if (((CFrameObjDoc*)GetDocument())->m_nHasData[nPos])
			pCmdUI->Enable (true);
		else
			pCmdUI->Enable (false);
	}
	else
		pCmdUI->Enable (false);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnEditPaste
//	Description :	This routine will paste contents of the clipboard
//					into the selected row.  The clipboard contents must
//					be foramtted as either "FrameDef" or "SectorDef".
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnEditPaste() 
{
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if (nPos == -1)
	{
		nPos = 0;
		GetListCtrl().SetItemState (nPos, 1, LVIS_FOCUSED);
		GetListCtrl().SetItemState (nPos, 2, LVIS_SELECTED);
	}
	if (nPos > -1)
	{
		COleDataObject	obj;
		// int nFormatFrame	= RegisterClipboardFormat(_T("FrameDef"));
		CLIPFORMAT	nFormatFrame	= (CLIPFORMAT)RegisterClipboardFormat(_T("FrameDef"));
		// int nFormatSector	= RegisterClipboardFormat(_T("SectorDef"));
		CLIPFORMAT  nFormatSector	= (CLIPFORMAT)RegisterClipboardFormat(_T("SectorDef"));

		if (obj.AttachClipboard())
		{
			CUpdateSingle single;
			if (obj.IsDataAvailable(nFormatSector))
			{
				HGLOBAL hmem;
				hmem = obj.GetGlobalData (nFormatSector);
				CMemFile sf((BYTE*)::GlobalLock(hmem), ::GlobalSize(hmem));	
				CString strLine;
				CArchive ar(&sf, CArchive::load);
				ar >> strLine;
				ar.Close();
				::GlobalUnlock(hmem);
				CParseFrame cParseFrame;
				CParseSector cParseSector;
				cParseSector.SetData(strLine);
				cParseSector.ParseData();
				cParseFrame.m_nIndex = nPos+1;
				cParseFrame.m_strLabel = cParseSector.m_strLabel;
				cParseFrame.m_nInstrument = cParseSector.m_nInstrument;
				cParseFrame.m_nSpacelookSide = cParseSector.m_nSpacelookSide;
				cParseFrame.m_nSpacelookMode = cParseSector.m_nSpacelookMode;
				cParseFrame.m_nSndrStepMode = cParseSector.m_nSndrStepMode;
				cParseFrame.m_nCoordType = cParseSector.m_nCoordType;
				cParseFrame.m_fStartNS = cParseSector.m_fStartNS;
				cParseFrame.m_fStartEW = cParseSector.m_fStartEW;
				cParseFrame.m_fStopNS = cParseSector.m_fStopNS;
				cParseFrame.m_fStopEW = cParseSector.m_fStopEW;
				cParseFrame.m_strDesc = cParseSector.m_strDesc;
				cParseFrame.m_strTime = _T("2000/001/00:00:00.000");
				((CFrameObjDoc*)GetDocument())->m_strDataRecord[nPos] = cParseFrame.GetData();
				((CFrameObjDoc*)GetDocument())->m_nHasData[nPos] = TRUE;
				((CFrameObjDoc*)GetDocument())->MarkModified(TRUE);
				((CFrameObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &single);
			}
			else if (obj.IsDataAvailable(nFormatFrame))
			{
				HGLOBAL hmem;
				hmem = obj.GetGlobalData (nFormatFrame);
				CMemFile sf((BYTE*)::GlobalLock(hmem), ::GlobalSize(hmem));	
				CString strLine;
				CArchive ar(&sf, CArchive::load);
				ar >> strLine;
				ar.Close();
				::GlobalUnlock(hmem);
				CParseFrame cParseFrame;
				cParseFrame.SetData(strLine);
				cParseFrame.ParseData();
				cParseFrame.m_nIndex = nPos+1;
				((CFrameObjDoc*)GetDocument())->m_strDataRecord[nPos] = cParseFrame.GetData();
				((CFrameObjDoc*)GetDocument())->m_nHasData[nPos] = TRUE;
				((CFrameObjDoc*)GetDocument())->MarkModified(TRUE);
				((CFrameObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &single);
			}
		}
		else
			MessageBeep(0xFFFFFFFF);
	}
	else
		MessageBeep(0xFFFFFFFF);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnUpdateEditPaste
//	Description :	This routine is called by the framework to either
//					enable or disable the Paste toolbar button/menu item.
//					It will only be enabled if the clipboard contains data
//					formatted as either "FrameDef" or "SectorDef"
//	Return :		
//		void		-	
//	Parameters :	
//		CCmdUI* pCmdUI	-	Command UI object.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnUpdateEditPaste(CCmdUI* pCmdUI) 
{
	CListCtrl&	theCtrl = GetListCtrl();
	int nPos = (int) theCtrl.GetFirstSelectedItemPosition() - 1;

	if (nPos > -1)
	{
		// UINT formatFrame	= RegisterClipboardFormat(_T("FrameDef"));
		CLIPFORMAT formatFrame	= (CLIPFORMAT)RegisterClipboardFormat(_T("FrameDef"));
		// UINT formatSector	= RegisterClipboardFormat(_T("SectorDef"));
		CLIPFORMAT formatSector	= (CLIPFORMAT)RegisterClipboardFormat(_T("SectorDef"));

		COleDataObject	obj;
		if (obj.AttachClipboard())
		{
			if (obj.IsDataAvailable(formatFrame) || obj.IsDataAvailable(formatSector))
				pCmdUI->Enable (true);
			else
				pCmdUI->Enable (false);
		}
	}
	else
		pCmdUI->Enable (false);
}

void CFrameObjView::OnSetFocus(CWnd* pOldWnd) 
{
	CInstObjExView::OnSetFocus(pOldWnd);
	
	CFrameObjDoc* pDoc = (CFrameObjDoc*)GetDocument();
	BOOL	bNewerFile = FALSE;

	if (!pDoc->GetPathName().IsEmpty())
	{
		CTime cTimeNew;
		CFileFind finder;
		finder.FindFile(pDoc->GetPathName());
		finder.FindNextFile();
		finder.GetLastWriteTime(cTimeNew);
		if (pDoc->m_cTimeLastMod < cTimeNew)
			bNewerFile = TRUE;
		pDoc->m_cTimeLastMod = cTimeNew;
		if (bNewerFile)
		{
			CString strMessage = _T("The file has been modified outside of the\n")
								 _T("Schedule Manager program.  Do you want to\n")
								 _T("reload it before saving the file?");
			int nResponse = AfxGetMainWnd()->MessageBox(strMessage, _T("Newer file exists"), MB_YESNO | MB_ICONEXCLAMATION);
			if (nResponse == IDYES)
			{
				pDoc->OnOpenDocument(pDoc->GetPathName());
				POSITION posView = pDoc->GetFirstViewPosition();
				while (posView != NULL)
				{
					CFrameObjView* pView = (CFrameObjView*)pDoc->GetNextView(posView);
					CListCtrl&	theCtrl = GetListCtrl();
					int nColumnCount = theCtrl.GetHeaderCtrl()->GetItemCount();
					// Delete all of the columns.
					for (int i=0;i < nColumnCount;i++)
					{
						theCtrl.DeleteColumn(0);
					}
					pView->OnInitialUpdate();
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnToolsDeleteFrame
//	Description :	This routine is called to delete the frame.
//	Return :		
//		void		-	
//	Parameters :	
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnToolsDeleteFrame() 
{
	CListCtrl&	theCtrl = GetListCtrl();

	int nPos = (int) theCtrl.GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		CUpdateSingle single;
		CParseFrame cParseFrame;
		((CFrameObjDoc*)GetDocument())->m_strDataRecord[nPos] = cParseFrame.GetDataNew(nPos);
		((CFrameObjDoc*)GetDocument())->MarkModified(true);
		((CFrameObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &single);
		((CFrameObjDoc*)GetDocument())->m_nHasData[nPos] = FALSE;
	}
	else
		MessageBeep(0xFFFFFFFF);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnUpdateToolsDeleteFrame
//	Description :	This routine is called by the frameworks to either
//					enable or disable the Delete Frame toolbar button/menu item.
//					It will only be enabled if the list control has
//					focus and a row that has data is currently selected.
//	Return :		
//		void		-	
//	Parameters :	
//		CCmdUI* pCmdUI	-	Command UI object.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnUpdateToolsDeleteFrame(CCmdUI* pCmdUI)
{
	CListCtrl&	theCtrl = GetListCtrl();
	int nPos = (int) theCtrl.GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		if (((CFrameObjDoc*)GetDocument())->m_nHasData[nPos])
			pCmdUI->Enable (true);
		else
			pCmdUI->Enable (false);
	}
	else
		pCmdUI->Enable (false);
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CFrameObjView::OnContextMenu
//	Description :	Display the Frame Context Menu.
//	Return :		
//		void		-	
//	Parameters :	
//		CWnd* pWnd		-	Not used
//		CPoint point	-	Point in screen coordinates where the menu
//							should be displayed
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CFrameObjView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	GetParentFrame()->ActivateFrame();

	CPoint local = point;
	ScreenToClient(&local);
	theApp.ShowPopupMenu (IDR_CONTEXT_FRAME_MENU, point, AfxGetMainWnd());
}

BOOL CFrameObjView::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_DELETE)
		{
			OnToolsDeleteFrame();
			return TRUE;
		}
	}
	
	return CInstObjExView::PreTranslateMessage(pMsg);
}

/////////////////////////////////////////////////////////////////////////////
// CSectorObjView
IMPLEMENT_DYNCREATE(CSectorObjView, CInstObjExView)

CSectorObjView::CSectorObjView()
{
}

CSectorObjView::~CSectorObjView()
{
}

BEGIN_MESSAGE_MAP(CSectorObjView, CInstObjExView)
	//{{AFX_MSG_MAP(CSectorObjView)
	ON_COMMAND(ID_TOOLS_EDITSECTOR, OnToolsEditSector)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_EDITSECTOR, OnUpdateToolsEditSector)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateEditCut)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_COMMAND(ID_TOOLS_INSERTSECTOR, OnToolsInsertSector)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_INSERTSECTOR, OnUpdateToolsInsertSector)
	ON_COMMAND(ID_TOOLS_DELETESECTOR, OnToolsDeleteSector)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_DELETESECTOR, OnUpdateToolsDeleteSector)
	ON_WM_CONTEXTMENU()
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CSectorObjView::OnDraw(CDC* pDC)
{
//	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

#ifdef _DEBUG
void CSectorObjView::AssertValid() const
{
	CInstObjExView::AssertValid();
}

void CSectorObjView::Dump(CDumpContext& dc) const
{
	CInstObjExView::Dump(dc);
}
#endif //_DEBUG

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSectorObjView::OnInitialUpdate
//	Description :	This routine will setup its parents member variables
//					so that it knows what type of instrument object we
//					have.  The parent OnInitialUpdate is then called
//					to create all of the columns of the proper size and
//					justification.  Then, the data is read from the 
//					document.  OnUpdate is called for each row so that
//					the data can be displayed.
//	Return :		
//		void		-	
//	Parameters :	None.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CSectorObjView::OnInitialUpdate() 
{
	m_nObjType = nInstObjSector;
	m_nNumParams = nMaxInstObjProps[m_nObjType];

	CInstObjExView::OnInitialUpdate();
	CListCtrl&	theCtrl = GetListCtrl();
	CUpdateSingle	single;	

	for (int nRow=0; nRow<((CSectorObjDoc*)GetDocument())->m_strDataRecord.GetSize(); nRow++)
	{
		//CString strIndex;
		//strIndex.Format (_T("%d"), nRow+1);
		//theCtrl.InsertItem(nRow, strIndex);
		CString strIndex(_T(""));
		theCtrl.InsertItem(nRow, strIndex);
		theCtrl.SetItemData (nRow, 0);
		OnUpdate(this, nRow, &single);
	}
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSectorObjView::OnUpdate
//	Description :	This routine is responsible for updating a row on the
//					list control.  It should be called whenever the
//					documents data is modified.
//	Return :		
//		void		-	
//	Parameters :	
//		CView* pSender	-	Not used.
//		LPARAM lHint	-	The row that should be updated.
//		CObject* pHint	-	This is a pointer to an object of type
//							RUNTIME_CLASS(CUpdateSingle).  The object is
//							not used for anything other then checking
//							its type.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CSectorObjView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	if (pHint != NULL)
	{
		if (pHint->IsKindOf(RUNTIME_CLASS(CUpdateSingle)))
		{
			CParseSector cParseSector;
			CStringArray	strArrayData;
			CListCtrl&		listCtrl = GetListCtrl();
			if (lHint>=((CSectorObjDoc*)GetDocument())->m_strDataRecord.GetSize())
			{
				((CSectorObjDoc*)GetDocument())->m_strDataRecord.SetAtGrow(lHint, cParseSector.GetDataNew());
				((CSectorObjDoc*)GetDocument())->m_nHasData.SetAtGrow(lHint, FALSE);
			}
			cParseSector.SetData(((CSectorObjDoc*)GetDocument())->m_strDataRecord[lHint]);
			if (cParseSector.ParseData() && ((CSectorObjDoc*)GetDocument())->m_nHasData[lHint])
			{
				listCtrl.SetItemText(lHint, eSectorLabel, cParseSector.GetDataLabel());
				listCtrl.SetItemText(lHint, eSectorInstrument, cParseSector.GetDataInstrument());
				listCtrl.SetItemText(lHint, eSectorSpacelookMode, cParseSector.GetDataSpacelookMode());
				listCtrl.SetItemText(lHint, eSectorSpacelookSide, cParseSector.GetDataSpacelookSide());
				listCtrl.SetItemText(lHint, eSectorSndrStepMode, cParseSector.GetDataSndrStepMode());
				listCtrl.SetItemText(lHint, eSectorCoordType, cParseSector.GetDataCoordType());
				listCtrl.SetItemText(lHint, eSectorStartNS, cParseSector.GetDataStartNS());
				listCtrl.SetItemText(lHint, eSectorStartEW, cParseSector.GetDataStartEW());
				listCtrl.SetItemText(lHint, eSectorStopNS, cParseSector.GetDataStopNS());
				listCtrl.SetItemText(lHint, eSectorStopEW, cParseSector.GetDataStopEW());
				listCtrl.SetItemText(lHint, eSectorDesc, cParseSector.GetDataDesc());
			}
		}
		else if (pHint->IsKindOf(RUNTIME_CLASS(CUpdateDelete)))
		{
			GetListCtrl().DeleteItem(lHint);
		}
		else if (pHint->IsKindOf(RUNTIME_CLASS(CUpdateInsert)))
		{
			GetListCtrl().InsertItem(lHint, _T(""));
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSectorObjView::OnLButtonDblClk
//	Description :	OnLButtonDblClk is called whenever a user double
//					clicks on the list control.  When this happens, a
//					dialog ox is displayed to allow the user to edit
//					the data on the line that was clicked.
//	Return :		
//		void		-	
//	Parameters :	
//		UINT nFlags		-	Flags associated with the click
//		CPoint point	-	Point where the user clicked
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CSectorObjView::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	CInstObjEditDlg	dlg;

	dlg.m_strTitle = _T("Edit Sector Parameters");
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		// Get all of the document data for the selected line
		CParseSector cParseSector;
		cParseSector.SetData(((CSectorObjDoc*)GetDocument())->m_strDataRecord[nPos]);
		cParseSector.ParseData();

		dlg.m_bReadOnly			= false;
		dlg.m_strIndex			= _T("N/A");
		dlg.m_strLabel			= cParseSector.GetDataLabel();			// Label
		dlg.m_strDescEdit		= cParseSector.GetDataDesc();			// Description
		dlg.m_strInstrument		= cParseSector.GetDataInstrument();		// Instrument
		dlg.m_strSpacelookMode	= cParseSector.GetDataSpacelookMode();	// Spacelook Mode
		dlg.m_strSpacelookSide	= cParseSector.GetDataSpacelookSide();	// Spacelook Side
		dlg.m_strSndrStepMode	= cParseSector.GetDataSndrStepMode();	// Sounder Step Mode
		dlg.m_strCoordType		= cParseSector.GetDataCoordType();		// Coordinate Type
		float fStartNS			= cParseSector.m_fStartNS;
		float fStartEW			= cParseSector.m_fStartEW;
		float fStopNS			= cParseSector.m_fStopNS;
		float fStopEW			= cParseSector.m_fStopEW;

		// If the sector is a new sector assign some defaults
		if (!((CSectorObjDoc*)GetDocument())->m_nHasData[nPos])
		{
			dlg.m_strInstrument		= strInstrument[1];
			dlg.m_strSpacelookMode	= strSpacelookMode[1];
			dlg.m_strSpacelookSide	= strSpacelookSide[0];
			dlg.m_strSndrStepMode	= strSndrStepMode[4];
			dlg.m_strCoordType		= strCoordType[1];
			fStartNS = 0.0;
			fStartEW = 0.0;
			fStopNS  = 0.0;
			fStopEW  = 0.0;
		}

		// Convert the floating point numbers
		dlg.m_fStartNSEdit = fStartNS;		
		dlg.m_fStartEWEdit = fStartEW;	
		dlg.m_fStopNSEdit = fStopNS;
		dlg.m_fStopEWEdit = fStopEW;

		if (dlg.DoModal() == IDOK)
		{
			cParseSector.m_strLabel = dlg.m_strLabel;
			cParseSector.SetDataInstrument(dlg.m_strInstrument);
			cParseSector.SetDataSpacelookMode(dlg.m_strSpacelookMode);
			cParseSector.SetDataSpacelookSide(dlg.m_strSpacelookSide);
			cParseSector.SetDataSndrStepMode(dlg.m_strSndrStepMode);
			cParseSector.SetDataCoordType(dlg.m_strCoordType);
			cParseSector.m_fStartNS = dlg.m_fStartNSEdit;
			cParseSector.m_fStartEW = dlg.m_fStartEWEdit;
			cParseSector.m_fStopNS = dlg.m_fStopNSEdit;
			cParseSector.m_fStopEW = dlg.m_fStopEWEdit;
			cParseSector.m_strDesc = dlg.m_strDescEdit;

			CUpdateSingle	single;
			((CSectorObjDoc*)GetDocument())->m_strDataRecord[nPos] = cParseSector.GetData();
			((CSectorObjDoc*)GetDocument())->m_nHasData[nPos] = TRUE;
			((CSectorObjDoc*)GetDocument())->MarkModified(true);
			((CSectorObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &single);
		}
	}
	CInstObjExView::OnLButtonDblClk(nFlags, point);
}

void CSectorObjView::OnToolsEditSector() 
{
	UINT nFlags = 0;
	CPoint point = 0;
	OnLButtonDblClk(nFlags, point);
}

void CSectorObjView::OnUpdateToolsEditSector(CCmdUI* pCmdUI) 
{
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
		pCmdUI->Enable(true);
	else
		pCmdUI->Enable(false);
}

void CSectorObjView::OnFilePrint() 
{
	TCHAR*	pStopString;
	m_fPrintTopMargin		= _tcstod(GetAppRegInstObj()->m_strTopMargin[nInstObjSector], &pStopString);
	m_fPrintBottomMargin	= _tcstod(GetAppRegInstObj()->m_strBottomMargin[nInstObjSector], &pStopString);
	m_fPrintLeftMargin		= _tcstod(GetAppRegInstObj()->m_strLeftMargin[nInstObjSector], &pStopString);
	m_fPrintRightMargin		= _tcstod(GetAppRegInstObj()->m_strRightMargin[nInstObjSector], &pStopString);
	m_nPrintPoint			= GetAppRegInstObj()->m_nFontSize[nInstObjSector];
	m_nPrintWeight			= GetAppRegInstObj()->m_nFontWeight[nInstObjSector];
	m_nPrintItalic			= GetAppRegInstObj()->m_bFontIsItalic[nInstObjSector];
	m_nPrintUnderline		= GetAppRegInstObj()->m_bFontIsUnderline[nInstObjSector];
	m_nPrintStrikeOut		= GetAppRegInstObj()->m_bFontIsStrikeOut[nInstObjSector];
	m_strPrintFontName		= GetAppRegInstObj()->m_strFontFaceName[nInstObjSector];
	m_pPrintHookProc		= SectorPrintHookProc;
	m_nObjType				= nInstObjSector;

	CInstObjExView::OnFilePrint();
}

void CSectorObjView::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);	
}

void CSectorObjView::OnEditCut() 
{
	CListCtrl&	theCtrl = GetListCtrl();

	int nPos = (int) theCtrl.GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		CUpdateSingle single;
		OnEditCopy();
		OnToolsDeleteSector();
	}
	else
		MessageBeep(0xFFFFFFFF);
}

void CSectorObjView::OnUpdateEditCut(CCmdUI* pCmdUI) 
{
	CListCtrl&	theCtrl = GetListCtrl();
	int nPos = (int) theCtrl.GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
		pCmdUI->Enable (true);
	else
		pCmdUI->Enable (false);
}

void CSectorObjView::OnEditCopy() 
{
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		COleDataSource*	pSource = new COleDataSource();
		CSharedFile		sf(GMEM_MOVEABLE|GMEM_DDESHARE|GMEM_ZEROINIT);
		// UINT			format = RegisterClipboardFormat(_T("SectorDef"));
		CLIPFORMAT		format = (CLIPFORMAT)RegisterClipboardFormat(_T("SectorDef"));
		CArchive		ar (&sf, CArchive::store);
		ar << ((CSectorObjDoc*)GetDocument())->m_strDataRecord[nPos];
		ar.Close();

		HGLOBAL hMem = sf.Detach();
		if (!hMem)return;
		pSource->CacheGlobalData(format, hMem);
		pSource->SetClipboard();
	}
	else
			MessageBeep(0xFFFFFFFF);
}

void CSectorObjView::OnUpdateEditCopy(CCmdUI* pCmdUI) 
{
	CListCtrl&	theCtrl = GetListCtrl();
	int nPos = (int) theCtrl.GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
		pCmdUI->Enable (true);
	else
		pCmdUI->Enable (false);
}

void CSectorObjView::OnEditPaste() 
{
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if (nPos == -1)
	{
		nPos = 0;
		GetListCtrl().SetItemState (nPos, 1, LVIS_FOCUSED);
		GetListCtrl().SetItemState (nPos, 2, LVIS_SELECTED);
	}
	if (nPos > -1)
	{
		COleDataObject	obj;

		// int nFormatSector	= RegisterClipboardFormat(_T("SectorDef"));
		CLIPFORMAT  nFormatSector	= (CLIPFORMAT)RegisterClipboardFormat(_T("SectorDef"));

		if (obj.AttachClipboard())
		{
			CUpdateSingle single;
			CUpdateInsert insert;
			if (obj.IsDataAvailable(nFormatSector))
			{
				HGLOBAL hmem = obj.GetGlobalData(nFormatSector);
				CMemFile sf((BYTE*)::GlobalLock(hmem), ::GlobalSize(hmem));
				CString strLine;
				CArchive ar(&sf, CArchive::load);
				ar >> strLine;
				ar.Close();
				::GlobalUnlock(hmem);
				((CSectorObjDoc*)GetDocument())->m_strDataRecord.InsertAt(nPos, strLine);
				((CSectorObjDoc*)GetDocument())->m_nHasData.InsertAt(nPos, TRUE);				/* Update CWordArray */
				((CSectorObjDoc*)GetDocument())->m_nNumObj++;
				((CSectorObjDoc*)GetDocument())->MarkModified(true);
				((CSectorObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &insert);
				((CSectorObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &single);
				GetListCtrl().SetItemState (nPos, 1, LVIS_FOCUSED);
				GetListCtrl().SetItemState (nPos, 2, LVIS_SELECTED);
			}
			else
				MessageBeep(0xFFFFFFFF);
		}
		else
			MessageBeep(0xFFFFFFFF);
	}
	else
		MessageBeep(0xFFFFFFFF);
}

void CSectorObjView::OnUpdateEditPaste(CCmdUI* pCmdUI) 
{
	CListCtrl&	theCtrl = GetListCtrl();
	int nPos = (int) theCtrl.GetFirstSelectedItemPosition() - 1;

	if (nPos > -1)
	{
		// UINT formatSector	= RegisterClipboardFormat(_T("SectorDef"));
		CLIPFORMAT formatSector	= (CLIPFORMAT)RegisterClipboardFormat(_T("SectorDef"));
		COleDataObject	obj;
		if (obj.AttachClipboard())
		{
			if (obj.IsDataAvailable(formatSector))
				pCmdUI->Enable (true);
			else
				pCmdUI->Enable (false);
		}
	}
	else
		pCmdUI->Enable (false);
}

void CSectorObjView::OnToolsInsertSector() 
{
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if ((nPos == -1) && (((CSectorObjDoc*)GetDocument())->m_nNumObj == 0))
		nPos = 0;
	if (nPos > -1)
	{
		CParseSector cParseSector;
		((CSectorObjDoc*)GetDocument())->m_strDataRecord.InsertAt(nPos, cParseSector.GetDataNew());
		((CSectorObjDoc*)GetDocument())->m_nHasData.InsertAt(nPos, FALSE, 1);
		((CSectorObjDoc*)GetDocument())->m_nNumObj++;
		CUpdateInsert insert;
		CUpdateSingle single;
		((CSectorObjDoc*)GetDocument())->MarkModified(true);
		((CSectorObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &insert);
		((CSectorObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &single);
		GetListCtrl().SetItemState (nPos, 1, LVIS_FOCUSED);
		GetListCtrl().SetItemState (nPos, 2, LVIS_SELECTED);
	}
}

void CSectorObjView::OnUpdateToolsInsertSector(CCmdUI* pCmdUI) 
{
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if ((nPos > -1) || (((CSectorObjDoc*)GetDocument())->m_nNumObj == 0))
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

void CSectorObjView::OnToolsDeleteSector() 
{
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
	{
		((CSectorObjDoc*)GetDocument())->m_strDataRecord.RemoveAt(nPos);
		((CSectorObjDoc*)GetDocument())->m_nNumObj--;
		CUpdateDelete del;
		((CSectorObjDoc*)GetDocument())->MarkModified(true);
		((CSectorObjDoc*)GetDocument())->UpdateAllViews(NULL, nPos, &del);		
		if (!((nPos == 0) && (((CSectorObjDoc*)GetDocument())->m_nNumObj == 0)))
		{
			if (nPos == ((CSectorObjDoc*)GetDocument())->m_nNumObj)
				nPos--;
			GetListCtrl().SetItemState (nPos, 1, LVIS_FOCUSED);
			GetListCtrl().SetItemState (nPos, 2, LVIS_SELECTED);
		}
	}
}

void CSectorObjView::OnUpdateToolsDeleteSector(CCmdUI* pCmdUI) 
{
	int nPos = (int) GetListCtrl().GetFirstSelectedItemPosition() - 1;
	if (nPos > -1)
		pCmdUI->Enable(true);
	else
		pCmdUI->Enable(false);
}

void CSectorObjView::OnSetFocus(CWnd* pOldWnd) 
{
	CInstObjExView::OnSetFocus(pOldWnd);

	CSectorObjDoc* pDoc = (CSectorObjDoc*)GetDocument();
	BOOL	bNewerFile = FALSE;

	if (!pDoc->GetPathName().IsEmpty())
	{
		CTime cTimeNew;
		CFileFind finder;
		finder.FindFile(pDoc->GetPathName());
		finder.FindNextFile();
		finder.GetLastWriteTime(cTimeNew);
		if (pDoc->m_cTimeLastMod < cTimeNew)
			bNewerFile = TRUE;
		pDoc->m_cTimeLastMod = cTimeNew;
		if (bNewerFile)
		{
			CString strMessage = _T("The file has been modified outside of the\n")
								 _T("Schedule Manager program.  Do you want to\n")
								 _T("reload it before saving the file?");
			int nResponse = AfxGetMainWnd()->MessageBox(strMessage, _T("Newer file exists"), MB_YESNO | MB_ICONEXCLAMATION);
			if (nResponse == IDYES)
			{
				pDoc->OnOpenDocument(pDoc->GetPathName());
				POSITION posView = pDoc->GetFirstViewPosition();
				while (posView != NULL)
				{
					CSectorObjView* pView = (CSectorObjView*)pDoc->GetNextView(posView);
					CListCtrl&	theCtrl = GetListCtrl();
					int nColumnCount = theCtrl.GetHeaderCtrl()->GetItemCount();
					// Delete all of the columns.
					for (int i=0;i < nColumnCount;i++)
					{
						theCtrl.DeleteColumn(0);
					}
					pView->OnInitialUpdate();				}
			}
		}
	}
}

BOOL CSectorObjView::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_INSERT)
		{
			OnToolsInsertSector();
			return TRUE;
		}
		else if (pMsg->wParam == VK_DELETE)
		{
			OnToolsDeleteSector();
			return TRUE;
		}
	}

	return CInstObjExView::PreTranslateMessage(pMsg);
}

void CSectorObjView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	GetParentFrame()->ActivateFrame();

	CPoint local = point;
	ScreenToClient(&local);
	theApp.ShowPopupMenu (IDR_CONTEXT_SECTOR_MENU, point, AfxGetMainWnd());
}

/////////////////////////////////////////////////////////////////////////////
// CStarObjView
IMPLEMENT_DYNCREATE(CStarObjView, CInstObjExView)

CStarObjView::CStarObjView(){}

CStarObjView::~CStarObjView(){}

BEGIN_MESSAGE_MAP(CStarObjView, CInstObjExView)
	//{{AFX_MSG_MAP(CStarObjView)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_WM_CONTEXTMENU()
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
void CStarObjView::AssertValid() const
{
	CInstObjExView::AssertValid();
}

void CStarObjView::Dump(CDumpContext& dc) const
{
	CInstObjExView::Dump(dc);
}
#endif //_DEBUG

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CStarObjView::OnInitialUpdate
//	Description :	This routine will setup its parents member variables
//					so that it knows what type of instrument object we
//					have.  The parent OnInitialUpdate is then called
//					to create all of the columns of the proper size and
//					justification.  Then, the data is read from the 
//					document.  OnUpdate is called for each row so that
//					the data can be displayed.
//	Return :		
//		void		-	
//	Parameters :	None.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CStarObjView::OnInitialUpdate()
{
	m_nObjType = nInstObjStar;
	m_nNumParams = nMaxInstObjProps[m_nObjType];
	
	CInstObjExView::OnInitialUpdate();
	CListCtrl&	theCtrl = GetListCtrl();
	CUpdateSingle	single;

	for (int nRow=0; nRow<((CStarObjDoc*)GetDocument())->m_strDataRecord.GetSize(); nRow++)
	{
		CString strIndex;
		strIndex.Format (_T("%d"), nRow+1);
		theCtrl.InsertItem(nRow, strIndex);
		theCtrl.SetItemData (nRow, 0);
		OnUpdate(this, nRow, &single);
	}
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CStarObjView::OnUpdate
//	Description :	This routine is responsible for updating a row on the
//					list control.  It should be called whenever the
//					documents data is modified.
//	Return :		
//		void		-	
//	Parameters :	
//		CView* pSender	-	Not used.
//		LPARAM lHint	-	The row that should be updated.
//		CObject* pHint	-	This is a pointer to an object of type
//							RUNTIME_CLASS(CUpdateSingle).  The object is
//							not used for anything other then checking
//							its type.
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CStarObjView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	if (pHint != NULL)
	{
		if (pHint->IsKindOf(RUNTIME_CLASS(CUpdateSingle)))
		{
			CParseStar		cParseStar;
			CStringArray	strArrayData;
			CListCtrl&		listCtrl = GetListCtrl();

			cParseStar.SetData(((CStarObjDoc*)GetDocument())->m_strDataRecord[lHint]);
			if (cParseStar.ParseData())
			{
				listCtrl.SetItemText(lHint, eStarTime, cParseStar.GetDataTime());
				listCtrl.SetItemText(lHint, eStarDuration, cParseStar.GetDataDuration());
				listCtrl.SetItemText(lHint, eStarWindowNum, cParseStar.GetDataWindowNum());
				listCtrl.SetItemText(lHint, eStarInstrument, cParseStar.GetDataInstrument());
				listCtrl.SetItemText(lHint, eStarIMCSet, cParseStar.GetDataIMCSet());
				listCtrl.SetItemText(lHint, eStarIndex, cParseStar.GetDataIndex());
				listCtrl.SetItemText(lHint, eStarID, cParseStar.GetDataID());
				listCtrl.SetItemText(lHint, eStarNumLooks, cParseStar.GetDataNumLooks());
				listCtrl.SetItemText(lHint, eStarLookNum, cParseStar.GetDataLookNum());
				listCtrl.SetItemText(lHint, eStarRepeats, cParseStar.GetDataRepeats());
				listCtrl.SetItemText(lHint, eStarCycNS, cParseStar.GetDataCycNS());
				listCtrl.SetItemText(lHint, eStarIncNS, cParseStar.GetDataIncNS());
				listCtrl.SetItemText(lHint, eStarCycEW, cParseStar.GetDataCycEW());
				listCtrl.SetItemText(lHint, eStarIncEW, cParseStar.GetDataIncEW());
			}
		}
	}
}

void CStarObjView::OnFilePrint() 
{
	TCHAR*	pStopString;
	m_fPrintTopMargin		= _tcstod(GetAppRegInstObj()->m_strTopMargin[nInstObjStar], &pStopString);
	m_fPrintBottomMargin	= _tcstod(GetAppRegInstObj()->m_strBottomMargin[nInstObjStar], &pStopString);
	m_fPrintLeftMargin		= _tcstod(GetAppRegInstObj()->m_strLeftMargin[nInstObjStar], &pStopString);
	m_fPrintRightMargin		= _tcstod(GetAppRegInstObj()->m_strRightMargin[nInstObjStar], &pStopString);
	m_nPrintPoint			= GetAppRegInstObj()->m_nFontSize[nInstObjStar];
	m_nPrintWeight			= GetAppRegInstObj()->m_nFontWeight[nInstObjStar];
	m_nPrintItalic			= GetAppRegInstObj()->m_bFontIsItalic[nInstObjStar];
	m_nPrintUnderline		= GetAppRegInstObj()->m_bFontIsUnderline[nInstObjStar];
	m_nPrintStrikeOut		= GetAppRegInstObj()->m_bFontIsStrikeOut[nInstObjStar];
	m_strPrintFontName		= GetAppRegInstObj()->m_strFontFaceName[nInstObjStar];
	m_pPrintHookProc		= StarPrintHookProc;
	m_nObjType				= nInstObjStar;

	CInstObjExView::OnFilePrint();
}

void CStarObjView::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);	
}

//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CStarObjView::OnContextMenu
//	Description :	Display the Frame Context Menu.
//	Return :		
//		void		-	
//	Parameters :	
//		CWnd* pWnd		-	Not used
//		CPoint point	-	Point in screen coordinates where the menu
//							should be displayed
//	Note :			
//////////////////////////////////////////////////////////////////////////
void CStarObjView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	GetParentFrame()->ActivateFrame();

	CPoint local = point;
	ScreenToClient(&local);
	theApp.ShowPopupMenu (IDR_CONTEXT_STAR_MENU, point, AfxGetMainWnd());
}

void CStarObjView::OnSetFocus(CWnd* pOldWnd) 
{
	CInstObjExView::OnSetFocus(pOldWnd);

	CStarObjDoc* pDoc = (CStarObjDoc*)GetDocument();
	BOOL	bNewerFile = FALSE;

	if (!pDoc->GetPathName().IsEmpty())
	{
		CTime cTimeNew;
		CFileFind finder;
		finder.FindFile(pDoc->GetPathName());
		finder.FindNextFile();
		finder.GetLastWriteTime(cTimeNew);
		if (pDoc->m_cTimeLastMod < cTimeNew)
			bNewerFile = TRUE;
		pDoc->m_cTimeLastMod = cTimeNew;
		if (bNewerFile)
		{
			CString strMessage = _T("The file has been modified outside of the\n")
								 _T("Schedule Manager program.  Do you want to\n")
								 _T("reload it before saving the file?");
			int nResponse = AfxGetMainWnd()->MessageBox(strMessage, _T("Newer file exists"), MB_YESNO | MB_ICONEXCLAMATION);
			if (nResponse == IDYES)
			{
				pDoc->OnOpenDocument(pDoc->GetPathName());
				POSITION posView = pDoc->GetFirstViewPosition();
				while (posView != NULL)
				{
					CStarObjView* pView = (CStarObjView*)pDoc->GetNextView(posView);
					CListCtrl&	theCtrl = GetListCtrl();
					int nColumnCount = theCtrl.GetHeaderCtrl()->GetItemCount();
					// Delete all of the columns.
					for (int i=0;i < nColumnCount;i++)
					{
						theCtrl.DeleteColumn(0);
					}
					pView->OnInitialUpdate();
				}
			}
		}
	}
}
//////////////////////////////////////////////////////////////////////
// CUpdateSingle Class
//////////////////////////////////////////////////////////////////////
IMPLEMENT_DYNCREATE(CUpdateSingle, CObject)
CUpdateSingle::CUpdateSingle(){}
CUpdateSingle::~CUpdateSingle(){}
IMPLEMENT_DYNCREATE(CUpdateDelete, CObject)
CUpdateDelete::CUpdateDelete(){}
CUpdateDelete::~CUpdateDelete(){}
IMPLEMENT_DYNCREATE(CUpdateInsert, CObject)
CUpdateInsert::CUpdateInsert(){}
CUpdateInsert::~CUpdateInsert(){}

