/////////////////////////////////////////////////////////////////////////////
#if !defined(AFX_INSTOBJVIEW_H__8616B369_F72D_11D3_8634_0020EA0406A1__INCLUDED_)
#define AFX_INSTOBJVIEW_H__8616B369_F72D_11D3_8634_0020EA0406A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/////////////////////////////////////////////////////////////////////////////
// CInstObjExView view
class CInstObjExView : public CListView
{
protected:
	CInstObjExView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CInstObjExView)
public:
	//{{AFX_VIRTUAL(CInstObjExView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL
protected:
	virtual ~CInstObjExView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CInstObjExView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	int				m_nNumParams;
	int				m_nObjType;
	int				m_nPrintPoint;
	int				m_nPrintWeight;
	int				m_nPrintItalic;
	int				m_nPrintUnderline;
	int				m_nPrintStrikeOut;
	CString			m_strPrintFontName;
	double			m_fPrintLeftMargin;
	double			m_fPrintRightMargin;
	double			m_fPrintTopMargin;
	double			m_fPrintBottomMargin;
	LPPRINTHOOKPROC	m_pPrintHookProc;
	void			OnFilePrint();
};

/////////////////////////////////////////////////////////////////////////////
// CFrameObjView view
class CFrameObjView : public CInstObjExView
{
protected:
	CFrameObjView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CFrameObjView)
public:
	//{{AFX_VIRTUAL(CFrameObjView)
	public:
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL
protected:
	virtual ~CFrameObjView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CFrameObjView)
	afx_msg void OnToolsEditFrame();
	afx_msg void OnUpdateToolsEditFrame(CCmdUI* pCmdUI);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnFilePrint();
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnToolsDeleteFrame();
	afx_msg void OnUpdateToolsDeleteFrame(CCmdUI* pCmdUI);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CSectorObjView view
class CSectorObjView : public CInstObjExView
{
protected:
	CSectorObjView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSectorObjView)
public:
	//{{AFX_VIRTUAL(CSectorObjView)
	public:
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL
protected:
	virtual ~CSectorObjView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CSectorObjView)
	afx_msg void OnEditCut();
	afx_msg void OnUpdateEditCut(CCmdUI* pCmdUI);
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg void OnToolsInsertSector();
	afx_msg void OnUpdateToolsInsertSector(CCmdUI* pCmdUI);
	afx_msg void OnToolsDeleteSector();
	afx_msg void OnUpdateToolsDeleteSector(CCmdUI* pCmdUI);
	afx_msg void OnToolsEditSector();
	afx_msg void OnUpdateToolsEditSector(CCmdUI* pCmdUI);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnFilePrint();
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnSetFocus(CWnd* pOldWnd);	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// CStarObjView view
class CStarObjView : public CInstObjExView
{
protected:
	CStarObjView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CStarObjView)
public:
	//{{AFX_VIRTUAL(CStarObjView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL
protected:
	virtual ~CStarObjView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CStarObjView)
	afx_msg void OnFilePrint();
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////
// The following classes are used by the OnUpdate routines.
/////////////////////////////////////////////////////////////////////////////
// CUpdateSingle
class CUpdateSingle : public CObject  
{
	DECLARE_DYNCREATE(CUpdateSingle)
public:
	CUpdateSingle();
	virtual ~CUpdateSingle();
};
/////////////////////////////////////////////////////////////////////////////
// CUpdateDelete
class CUpdateDelete : public CObject  
{
	DECLARE_DYNCREATE(CUpdateDelete)
public:
	CUpdateDelete();
	virtual ~CUpdateDelete();
};
/////////////////////////////////////////////////////////////////////////////
// CUpdateInsert
class CUpdateInsert : public CObject  
{
	DECLARE_DYNCREATE(CUpdateInsert)
public:
	CUpdateInsert();
	virtual ~CUpdateInsert();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INSTOBJVIEW_H__8616B369_F72D_11D3_8634_0020EA0406A1__INCLUDED_)
