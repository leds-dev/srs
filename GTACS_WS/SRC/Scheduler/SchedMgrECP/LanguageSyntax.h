/////////////////////////////////////////////////////////////////////////////
#if !defined(LANGUAGE_SYNTAX_INCLUDED_)
#define LANGUAGE_SYNTAX_INCLUDED_

// The CMLANG_STOL structure will contain all of the STOL keywords.  At runtime
// command mnemonic will be appended to the keyword list and critical command
// mnemonics will be appended tot he operators list.
static CM_LANGUAGE CMLANG_STOL = {
/* Language style            */ CMLS_PROCEDURAL,
/* Case-senstitve            */ false,
/* Keywords                  */ 
/* Epoch keywords			 */	_T("abs\naccepted\naccess\nacos\nactivate\nadd\naddress\nalarm\n")
								_T("archive\nargs\nascii\nasin\natan\nautoexec\nba\nbackup\n")
								_T("bbcal\nbell\nc0\nc1\nc2\nc3\nc4\nc5\nc6\nc7\ncancel\nchain\n")
								_T("clean\nclear\nclock\nclose\ncmd\ncmd_code\ncmd_name\n")
								_T("cmd_queue\ncmd_status\ncmd_system\ncmdsim\ncommand\n")
								_T("connect\ncontinue\ncos\ncurstream\ncv\ndbase\ndec\ndelay\n")
								_T("delete\ndirerr\ndisconnect\ndisplay\ndo\ndwell\nduration\nelse\n")
								_T("elseif\nend\nenddo\nendif\nenv\nerror\neu\neu_contexth\n")
								_T("event\nex_contextl\nexec\nexecute\nexit\n")
								_T("fail\nfile\nfile_edit\n")
								_T("fkey\nfkeys\nframe\nframe_file\nfreeze\nglobal\ngmt\n")
								_T("goto\ngrant\nground\nground_equipment\ngv_limit\ngv_name\n")
								_T("gv_system\ngv_type\nhelp\nhex\nif\ninhibit\ninit\ninput\n")
								_T("int\nis_error\nis_file\nis_normal\nis_valid\nis_warn\n")
								_T("instrument\nlandscript\nlast_star_class\n")
								_T("label\nlimit_contexth\nlimit_contextl\nlimit_count\n")
								_T("limits\nlinear\nload_reg\nload_words\nloc10\nlocal\nlock\nloge\n")
								_T("logoff\nlook\nlooknum\nlower\nmax\nmcid\nmet\nmode\n")
								_T("mod\nmodify\nnargs\nnowait\nnumlooks\n")
								_T("objid\noff\non\nopen\npass\npause\npct\nplayback\n")
								_T("point\npoly\npopdown\npopup\nportrait\npostscript\nprint\n")
								_T("priority\nproc\nprocedure\nprocess\nprompt\npseudo\nptv\npv\nraw\n")
								_T("raw\nreal\nrealtime\nreconnect\nrecord\nredh\nredl\n")
								_T("rejected\nrepeat\nreset\nreturn\nrexmit\n")
								_T("")
								_T("save\nsc\nscandata\nschedule\nscid\n")
								_T("setup\nsim\nsimulation\nscan\n")
								_T("select\nset\nset\nsetenv\nside\nsin\nsleep\nsnap\nsqrt\nstar\n")
								_T("start\nstate\nstep\nstop\nstream\n")
								_T("string\nstrstr\nsubstr\nswap\nsync\nsystem\ntan\nterm\n")
								_T("")
								_T("then\ntime\ntimedcmd\ntlm\ntlm_limit\ntlm_name\n")
								_T("tlm_system\ntlm_type\ntriggers\ntv\nunlock\nupper\nutc\n")
								_T("value\nview\nviewer\nvolts\nwait\nwaiton\nwarn\nwhile\n")
								_T("write\nyellow\nyellowh\ngo\nset\n")
/* NOPQ keywords             */ _T("avcmd\navctrl_mon\ntakecmd\navtlm\ncem\ncemmon\ncfgcmd\n")
								_T("cfgtlm\ncload\ncgi\nchkpt\nclim\nclk\ncmdwarn\ncxctrl\ncxmon\n")
								_T("cxrng\ncxtc\ncxtlm\ncxtms\ndcr_gen\nimage\nimcreq\n")
								_T("encrypt\ngenload\n")
								_T("maneuver\nmomentum\n")
								_T("monsched\nmrss\n")
								_T("nextsched\noats\npacing\npm\npss\npste\resmon\nschedx\nstardata\nstarid\nstarinfo\n")
								_T("sps\ntgi\ntlmwait\noffset\n")
								_T("")
								_T("rtcs\nrtcsdata\n"),
/* Operators                 */ NULL, // Critical command mnemonics appended at runtime
/* Single line comments      */ _T("#"),
/* Multiline comments start  */ NULL,
/* Multiline comments stop   */ NULL,
/* Scope words start         */ NULL,
/* Scope words stop          */ NULL,
/* String literal char       */ _T("\"\n"),
/* Escape char               */ NULL,
/* Statement terminator char */ NULL,
/* Tag element names         */ _T("**\n-\n+\n*\n/\n<<\n>>\n==\n!=\n<>\n<\n>\n<=\n>=\n=\n")
                                _T(".and.\n&\n.or.\n|\n.xor.\n.true.\n.t.\n.false.\n.f.\n")
								_T(".eq.\n.ne.\n.lt.\n.gt.\n.le.\n.ge.\n"),
/* Tag attribute names       */ NULL,
/* Tag entries               */ NULL};

// The CMLAND_CMD structure will contain just command mnemonics.  Regular command
// mnemonics will be assigned as keywords, while critical commands will be assigned
// as operators.
									
// The CMLAND_CLS structure will allow operators to highlight all of the CLS directives.
static CM_LANGUAGE CMLANG_CLS = {
/* Language style            */ CMLS_PROCEDURAL,
/* Case-senstitve            */ false,
/* Keywords                  */ _T("repeart\nstart\nscan\nstop\n")
								_T("side\nstar\n")
								_T("cls\n")
								_T("priority\ntime\nduration\n")
								_T("execute\nbbcal\nmode_0\n")
								_T("rtcs\n")
								_T("dwell\npriority\nmode\nframe\ninstrument\n")
								_T("monsched\nschedx\nstardata\nstarid\nstarinfo\n")
								_T("instrument\nmax\n"), 
/* Operators                 */ _T("sounder\nimager\nload_reg\nsense\nsequence\n")
								_T("mode\nrepeat\nside\noats\neast\nwest\n")
								_T("mode_1\nnormal\npriority_1\npriority_2")
								_T("single_ns_0_1\nsingle_ns_0_2\nsingle_ns_0_4\ndouble_ns_0_1\n")
								_T("load_and_exec\nload_reg_only\ndo_not_suppress\nsuppress_bbcal\n")
								_T("plus_x\nminus_x\n")
								_T("")
								_T("numlooks\nlooknum\nscid\n")
								_T("load_reg\nsense\nsequence\nmax\n"), 
/* Single line comments      */ _T("#"),
/* Multiline comments start  */ NULL,
/* Multiline comments stop   */ NULL,
/* Scope words start         */ NULL,
/* Scope words stop          */ NULL,
/* String literal char       */ NULL,
/* Escape char               */ NULL,
/* Statement terminator char */ NULL,
/* Tag element names         */ NULL,
/* Tag attribute names       */ NULL,
/* Tag entries               */ NULL};

// The CMLAND_FSP structure will allow operators to highlight all of the FSP directives.
// The CMLANG_RPT structure is used for the various reports.  It does nothing
// but highlight text strings and numbers
static CM_LANGUAGE CMLANG_RPT = {
/* Language style            */ CMLS_PROCEDURAL,
/* Case-senstitve            */ false,
/* Keywords                  */ _T("informational\nwarning\nfatal\n"),
/* Operators                 */ NULL,
/* Single line comments      */ _T("#"),
/* Multiline comments start  */ NULL,
/* Multiline comments stop   */ NULL,
/* Scope words start         */ NULL,
/* Scope words stop          */ NULL,
/* String literal char       */ _T("\"\n'"),
/* Escape char               */ _T('\\'),
/* Statement terminator char */ NULL,
/* Tag element names         */ NULL,
/* Tag attribute names       */ NULL,
/* Tag entries               */ NULL};

static CM_LANGUAGE CMLANG_RTCS = {
/* Language style            */ CMLS_PROCEDURAL,
/* Case-senstitve            */ false,
/* Keywords                  */ _T("wait\ncmd\nreturn\n"),
/* Operators                 */ NULL,
/* Single line comments      */ _T("#"),
/* Multiline comments start  */ NULL,
/* Multiline comments stop   */ NULL,
/* Scope words start         */ NULL,
/* Scope words stop          */ NULL,
/* String literal char       */ _T("\"\n'"),
/* Escape char               */ _T('\\'),
/* Statement terminator char */ NULL,
/* Tag element names         */ NULL,
/* Tag attribute names       */ NULL,
/* Tag entries               */ NULL};

static CM_LANGUAGE CMLANG_RTCSSET = {
/* Language style            */ CMLS_PROCEDURAL,
/* Case-senstitve            */ false,
/* Keywords                  */ _T("informational\nwarning\nfatal\ninclude\n")
	                            _T("include\nlabel\nstart\n"),
/* Operators                 */ NULL,
/* Single line comments      */ _T("#"),
/* Multiline comments start  */ NULL,
/* Multiline comments stop   */ NULL,
/* Scope words start         */ NULL,
/* Scope words stop          */ NULL,
/* String literal char       */ _T("\"\n'"),
/* Escape char               */ _T('\\'),
/* Statement terminator char */ NULL,
/* Tag element names         */ NULL,
/* Tag attribute names       */ NULL,
/* Tag entries               */ NULL};

static CM_LANGUAGE CMLANG_RTCSCMD = {
/* Language style            */ CMLS_PROCEDURAL,
/* Case-senstitve            */ false,
/* Keywords                  */ _T("informational\nwarning\nfatal\n"),
/* Operators                 */ NULL,
/* Single line comments      */ _T("#"),
/* Multiline comments start  */ NULL,
/* Multiline comments stop   */ NULL,
/* Scope words start         */ NULL,
/* Scope words stop          */ NULL,
/* String literal char       */ _T("\"\n'"),
/* Escape char               */ _T('\\'),
/* Statement terminator char */ NULL,
/* Tag element names         */ NULL,
/* Tag attribute names       */ NULL,
/* Tag entries               */ NULL};

static CM_LANGUAGE CMLANG_PACECMD = {
/* Language style            */ CMLS_PROCEDURAL,
/* Case-senstitve            */ false,
/* Keywords                  */ _T("informational\nwarning\nfatal\n"),
/* Operators                 */ NULL,
/* Single line comments      */ _T("#"),
/* Multiline comments start  */ NULL,
/* Multiline comments stop   */ NULL,
/* Scope words start         */ NULL,
/* Scope words stop          */ NULL,
/* String literal char       */ _T("\"\n'"),
/* Escape char               */ _T('\\'),
/* Statement terminator char */ NULL,
/* Tag element names         */ NULL,
/* Tag attribute names       */ NULL,
/* Tag entries               */ NULL};

static CM_LANGUAGE CMLANG_STOLCMD = {
/* Language style            */ CMLS_PROCEDURAL,
/* Case-senstitve            */ false,
/* Keywords                  */ _T("informational\nwarning\nfatal\n"),
/* Operators                 */ NULL,
/* Single line comments      */ _T("#"),
/* Multiline comments start  */ NULL,
/* Multiline comments stop   */ NULL,
/* Scope words start         */ NULL,
/* Scope words stop          */ NULL,
/* String literal char       */ _T("\"\n'"),
/* Escape char               */ _T('\\'),
/* Statement terminator char */ NULL,
/* Tag element names         */ NULL,
/* Tag attribute names       */ NULL,
/* Tag entries               */ NULL};


#endif
