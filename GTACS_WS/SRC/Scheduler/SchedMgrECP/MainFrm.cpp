/***************************
//////////////////////////////////////////////////////////////////////
// MainFrm.cpp : implementation of the CMainFrame class             //
//                                                                  //
// (c) 2001 Frederick J. Shaw                                       //
// Prologs updated						                            //
// Revision History:												//	   
// Manan Dalal - July-August 2014									//	   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors					//	   
// Release in Build16.2												//
//////////////////////////////////////////////////////////////////////
***************************/
#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "MainFrm.h"
#include "RulesClasses.h"
#include "RtcsClasses.h"
#include "RtcsSetClasses.h"
#include "ClsClasses.h"	
#include "RcmdClasses.h"
#include "PcmdClasses.h"
#include "WorkspaceOptions.h"
#include "WorkspaceProperties.h"
#include "InstObjDoc.h"
#include "CodeMaxDoc.h"
#include "MapDoc.h"
#include "NewDialog.h"
#include "SchedGen.h"
#include "SchedVal.h"
#include "NewSchedFile.h"
#include "SchedUpdUtil.h"
#include "RTCS.h"
#include "Distribution.h"
#include "GenLoad.h"
#include "MapSearchDlg.h"
#include "Parse.h"
#include "TextFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CMDIFrameWnd)

const int  iMaxUserToolbars		= 10;
const UINT uiFirstUserToolBarId	= AFX_IDW_CONTROLBAR_FIRST + 40;
const UINT uiLastUserToolBarId	= uiFirstUserToolBarId + iMaxUserToolbars - 1;

BEGIN_MESSAGE_MAP(CMainFrame, CMDIFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_TOOLS_LOADDATABASE, OnToolsLoadDB)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_LOADDATABASE, OnUpdateToolsLoadDB)
	ON_COMMAND(ID_TOOLS_ADMINISTRATIONMODE, OnToolsAdministrationMode)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_ADMINISTRATIONMODE, OnUpdateToolsAdministrationMode)
	ON_COMMAND(ID_TOOLS_WORKSPACEOPTIONS, OnToolsWorkspaceOptions)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_WORKSPACEOPTIONS, OnUpdateToolsWorkspaceOptions)
	ON_COMMAND(ID_TOOLS_PROPERTIES, OnToolsProperties)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_PROPERTIES, OnUpdateToolsProperties)
	ON_COMMAND(ID_TOOLS_GENERATELOADFRAME, OnToolsGenerateLoadFrame)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_GENERATELOADFRAME, OnUpdateToolsGenerateLoadFrame)
	ON_COMMAND(ID_TOOLS_GENERATELOADSTAR, OnToolsGenerateLoadStar)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_GENERATELOADSTAR, OnUpdateToolsGenerateLoadStar)
	ON_COMMAND(ID_TOOLS_SCHEDVAL, OnToolsSTOLVal)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_SCHEDVAL, OnUpdateToolsSTOLVal)
	ON_COMMAND(ID_TOOLS_SCHEDGEN, OnToolsSTOLGenLoad)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_SCHEDGEN, OnUpdateToolsSTOLGenLoad)
	ON_COMMAND(ID_TOOLS_SCHEDMAKE, OnToolsSchedMake)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_SCHEDMAKE, OnUpdateToolsSchedMake)
	ON_COMMAND(ID_MDITABS_ICONS, OnMditabsIcons)
	ON_UPDATE_COMMAND_UI(ID_MDITABS_ICONS, OnUpdateMditabsIcons)
	ON_COMMAND(ID_MDITABS_TOP, OnMditabsTop)
	ON_UPDATE_COMMAND_UI(ID_MDITABS_TOP, OnUpdateMditabsTop)
	ON_COMMAND(ID_MDITABS_BOTTOM, OnMditabsBottom)
	ON_UPDATE_COMMAND_UI(ID_MDITABS_BOTTOM, OnUpdateMditabsBottom)
	ON_COMMAND(ID_VIEW_MDI_TABS, OnViewMdiTabs)
	ON_UPDATE_COMMAND_UI(ID_VIEW_MDI_TABS, OnUpdateViewMdiTabs)
	ON_COMMAND(ID_MDITABS_CLOSE, OnMditabsClose)
	ON_UPDATE_COMMAND_UI(ID_MDITABS_CLOSE, OnUpdateMditabsClose)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateFileNew)
	ON_COMMAND(ID_VIEW_REFRESH, OnViewRefresh)
	ON_UPDATE_COMMAND_UI(ID_VIEW_REFRESH, OnUpdateViewRefresh)
	ON_COMMAND(ID_VIEW_SHOWEXT, OnViewShowExt)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SHOWEXT, OnUpdateViewShowExt)
	ON_COMMAND(ID_VIEW_SORTBYFILENAME, OnViewSortByFilename)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SORTBYFILENAME, OnUpdateViewSortByFilename)
	ON_COMMAND(ID_VIEW_SORTBYEXTENSION, OnViewSortByExtension)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SORTBYEXTENSION, OnUpdateViewSortByExtension)
	ON_COMMAND(ID_TOOLS_CLSVAL, OnToolsCLSVal)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_CLSVAL, OnUpdateToolsCLSVal)
	ON_COMMAND(ID_TOOLS_OATS_IMC_REQUESTIMCSET, OnToolsOATSRequestIMCSet)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OATS_IMC_REQUESTIMCSET, OnUpdateToolsOATSRequestIMCSet)
	ON_COMMAND(ID_TOOLS_OUTPUTEVENT_COPY, OnToolsOutputEventCopy)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OUTPUTEVENT_COPY, OnUpdateToolsOutputEventCopy)
	ON_COMMAND(ID_TOOLS_OUTPUTEVENT_SAVEAS, OnToolsOutputEventSaveAs)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OUTPUTEVENT_SAVEAS, OnUpdateToolsOutputEventSaveAs)
	ON_COMMAND(ID_TOOLS_OUTPUTBUILD_COPY, OnToolsOutputBuildCopy)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OUTPUTBUILD_COPY, OnUpdateToolsOutputBuildCopy)
	ON_COMMAND(ID_TOOLS_OUTPUTBUILD_SAVEAS, OnToolsOutputBuildSaveAs)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OUTPUTBUILD_SAVEAS, OnUpdateToolsOutputBuildSaveAs)
	ON_COMMAND(ID_TOOLS_OUTPUTBUILD_GOTOERROR, OnToolsOutputBuildGotoError)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OUTPUTBUILD_GOTOERROR, OnUpdateToolsOutputBuildGotoError)
	ON_WM_DROPFILES()
	ON_COMMAND(ID_TOOLS_OUTPUTBUILD_CLEAR, OnToolsOutputBuildClear)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OUTPUTBUILD_CLEAR, OnUpdateToolsOutputBuildClear)
	ON_COMMAND(ID_TOOLS_OUTPUTEVENT_CLEAR, OnToolsOutputEventClear)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OUTPUTEVENT_CLEAR, OnUpdateToolsOutputEventClear)
	ON_COMMAND(ID_TOOLS_SCHEDSYNTAXCHECK, OnToolsSchedSyntaxCheck)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_SCHEDSYNTAXCHECK, OnUpdateToolsSchedSyntaxCheck)
	ON_COMMAND(ID_VIEW_MUTE, OnViewMute)
	ON_UPDATE_COMMAND_UI(ID_VIEW_MUTE, OnUpdateViewMute)
	ON_COMMAND(ID_TOOLS_OATS_GENERATE_ECLIPSEREPORT, OnToolsOATSGenerateEclipseReport)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OATS_GENERATE_ECLIPSEREPORT, OnUpdateToolsOATSGenerateEclipseReport)
	ON_COMMAND(ID_TOOLS_OATS_IMC_REQUESTIMCSFCS, OnToolsOATSIMCRequestIMCSFCS)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OATS_IMC_REQUESTIMCSFCS, OnUpdateToolsOATSIMCRequestIMCSFCS)
	ON_COMMAND(ID_TOOLS_OUTPUTEVENT_PRINT, OnToolsOutputEventPrint)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OUTPUTEVENT_PRINT, OnUpdateToolsOutputEventPrint)
	ON_COMMAND(ID_TOOLS_OUTPUTBUILD_PRINT, OnToolsOutputBuildPrint)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OUTPUTBUILD_PRINT, OnUpdateToolsOutputBuildPrint)
	ON_COMMAND(ID_TOOLS_GENERATELOADRTCS, OnToolsGenerateLoadRTCS)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_GENERATELOADRTCS, OnUpdateToolsGenerateLoadRTCS)
	ON_COMMAND(ID_TOOLS_RTCSVAL, OnToolsRTCSVal)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_RTCSVAL, OnUpdateToolsRTCSVal)
	ON_COMMAND(ID_TOOLS_RTCSSETVAL, OnToolsRTCSSetVal)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_RTCSSETVAL, OnUpdateToolsRTCSSetVal)
	ON_COMMAND(ID_TOOLS_RTCSSYNTAXCHECK, OnToolsRTCSSyntaxCheck)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_RTCSSYNTAXCHECK, OnUpdateToolsRTCSSyntaxCheck)
	ON_COMMAND(ID_TOOLS_RTCSSETSYNTAXCHECK, OnToolsRTCSSetSyntaxCheck)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_RTCSSETSYNTAXCHECK, OnUpdateToolsRTCSSetSyntaxCheck)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_CLSSYNTAXCHECK, OnUpdateToolsCLSSyntaxCheck)
	ON_COMMAND(ID_TOOLS_UPDATEFRAME, OnToolsUpdateFrame)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_UPDATEFRAME, OnUpdateToolsUpdateFrame)
	ON_COMMAND(ID_TOOLS_SCHEDUPD, OnToolsSchedUpd)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_SCHEDUPD, OnUpdateToolsSchedUpd)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_GENERATELOADIMC, OnUpdateToolsGenerateLoadIMC)
	ON_COMMAND(ID_TOOLS_GENERATELOADIMC, OnToolsGenerateLoadIMC)
	ON_COMMAND(ID_TOOLS_GENERATESTOLPROCEDURE, OnToolsGenerateSTOLProc)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_GENERATESTOLPROCEDURE, OnUpdateToolsGenerateSTOLProc)
	ON_COMMAND(ID_TOOLS_FILEDISTRIBUTIONWIZARD, OnToolsFileDistributionWizard)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_FILEDISTRIBUTIONWIZARD, OnUpdateToolsFileDistributionWizard)
	ON_COMMAND(ID_TOOLS_SEARCHFORTIME, OnToolsSearchForTime)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_SEARCHFORTIME, OnUpdateToolsSearchForTime)
	ON_COMMAND(ID_TOOLS_CLSSYNTAXCHECK, OnToolsCLSSyntaxCheck)
	ON_COMMAND(ID_TOOLS_PACECMDSYNTAXCHECK, OnToolsPaceCmdSyntaxCheck)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_PACECMDSYNTAXCHECK, OnUpdateToolsPaceCmdSyntaxCheck)
	ON_COMMAND(ID_TOOLS_RTCSCMDSYNTAXCHECK, OnToolsRTCSCmdSyntaxCheck)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_RTCSCMDSYNTAXCHECK, OnUpdateToolsRTCSCmdSyntaxCheck)
	ON_COMMAND(ID_TOOLS_OATS_GENERATE_FRAME_INTRUSIONREPORT, OnToolsOatsGenerateFrameIntrusionReport)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OATS_GENERATE_FRAME_INTRUSIONREPORT, OnUpdateToolsOatsGenerateFrameIntrusionReport)
	ON_COMMAND(ID_TOOLS_OATS_GENERATE_SENSOR_INTRUSIONREPORT, OnToolsOatsGenerateSensorIntrusionReport)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_OATS_GENERATE_SENSOR_INTRUSIONREPORT, OnUpdateToolsOatsGenerateSensorIntrusionReport)
	ON_WM_CLOSE()
	ON_COMMAND(ID_TOOLS_STOLCMDSYNTAXCHECK, OnToolsStolCmdSyntaxCheck)
	ON_UPDATE_COMMAND_UI(ID_TOOLS_STOLCMDSYNTAXCHECK, OnUpdateToolsStolCmdSyntaxCheck)
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_VIEW_CUSTOMIZE, OnViewCustomize)
	ON_REGISTERED_MESSAGE(BCGM_RESETTOOLBAR, OnToolbarReset)
	ON_REGISTERED_MESSAGE(BCGM_TOOLBARMENU, OnToolbarContextMenu)
	ON_COMMAND_EX_RANGE(ID_VIEW_USER_TOOLBAR1, ID_VIEW_USER_TOOLBAR10, OnToolsViewUserToolbar)
	ON_UPDATE_COMMAND_UI_RANGE(ID_VIEW_USER_TOOLBAR1, ID_VIEW_USER_TOOLBAR10, OnUpdateToolsViewUserToolbar)
	ON_COMMAND(ID_VIEW_WORKSPACE, OnViewWorkspace)
	ON_UPDATE_COMMAND_UI(ID_VIEW_WORKSPACE, OnUpdateViewWorkspace)
	ON_COMMAND(ID_VIEW_OUTPUT, OnViewOutput)
	ON_UPDATE_COMMAND_UI(ID_VIEW_OUTPUT, OnUpdateViewOutput)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::CMainFrame
//	Description :	Class Constructor
//	Return :		constructor	-	none
//	Parameters :	void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
CMainFrame::CMainFrame()
{
	m_hwndClickedTabWnd = NULL;
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::~CMainFrame
//	Description :	Class Destructor
//	Return :		destructor	-	none
//	Parameters :	void -	none.
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
CMainFrame::~CMainFrame()
{
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnCreate()
//	Description :	Creates the main frame view.
//
//	Return :		int		0/-1
//
//	Parameters :
//					LPCREATESTRUCT lpCreateStruct
//                  long pointer to CreateStruct structure
//					The CREATESTRUCT structure defines the initialization
//                  parameters passed to the window procedure of an application.
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMDIFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// Load toolbar user images:
	if (!m_UserImages.Load (IDB_USER_IMAGES))
	{
		TRACE(_T("Failed to load user images\n"));
		ASSERT (FALSE);
	}
	else
	{
		CBCGPToolBar::SetUserImages (&m_UserImages);
	}

	CBCGPToolBar::EnableQuickCustomization ();
	EnableMDITabs (theApp.m_bMDITabs, theApp.m_bMDITabIcons, theApp.m_MDITabLocation);
	CBCGPVisualManager::SetDefaultManager(RUNTIME_CLASS(CBCGPWinXPVisualManager));

	// TODO: Define your own basic commands. Be sure, that each pulldown
	// menu have at least one basic command.

	CList<UINT, UINT>	lstBasicCommands;

	lstBasicCommands.AddTail (ID_VIEW_TOOLBARS);
	lstBasicCommands.AddTail (ID_FILE_NEW);
	lstBasicCommands.AddTail (ID_FILE_OPEN);
	lstBasicCommands.AddTail (ID_FILE_SAVE);
	lstBasicCommands.AddTail (ID_FILE_PRINT);
	lstBasicCommands.AddTail (ID_APP_EXIT);
	lstBasicCommands.AddTail (ID_EDIT_CUT);
	lstBasicCommands.AddTail (ID_EDIT_PASTE);
	lstBasicCommands.AddTail (ID_EDIT_UNDO);
	lstBasicCommands.AddTail (ID_APP_ABOUT);
	lstBasicCommands.AddTail (ID_VIEW_REFRESH);
	lstBasicCommands.AddTail (ID_VIEW_TOOLBAR);
	lstBasicCommands.AddTail (ID_VIEW_CUSTOMIZE);
	lstBasicCommands.AddTail (ID_WINDOW_TILE_HORZ);
	lstBasicCommands.AddTail (ID_TOOLS_OATS_GENERATE_INTRUSIONREPORT);
	lstBasicCommands.AddTail (ID_TOOLS_OATS_GENERATE_ECLIPSEREPORT);
	lstBasicCommands.AddTail (ID_TOOLS_OATS_IMC_REQUESTIMCSET);

	CBCGPToolBar::SetBasicCommands (lstBasicCommands);

	if (!m_wndMenuBar.Create (this))
	{
		TRACE0("Failed to create menubar\n");
		return -1;      // fail to create
	}

	m_wndMenuBar.SetBarStyle(m_wndMenuBar.GetBarStyle() | CBRS_SIZE_DYNAMIC);

	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	if (!GetAppOutputBar()->Create (_T("Output"), this, CSize (150, 150),
		TRUE /* Has gripper */, ID_VIEW_OUTPUT, WS_CHILD | WS_VISIBLE | CBRS_BOTTOM))
	{
		TRACE0("Failed to create output bar\n");
		return -1;      // fail to create
	}

	if (!GetAppWorkspaceBar()->Create (_T("Workspace"), this, CSize (200, 200),
		TRUE /* Has gripper */, ID_VIEW_WORKSPACE, WS_CHILD | WS_VISIBLE | CBRS_LEFT))
	{
		TRACE0("Failed to create workspace bar\n");
		return -1;      // fail to create
	}
	
	CString strMainToolbarTitle;
	strMainToolbarTitle.LoadString (IDS_MAIN_TOOLBAR);
	m_wndToolBar.SetWindowText (strMainToolbarTitle);
	m_wndMenuBar.EnableDocking(CBRS_ALIGN_ANY);
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	GetAppWorkspaceBar()->EnableDocking(CBRS_ALIGN_ANY);
	GetAppOutputBar()->EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	EnableAutoHideBars(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndMenuBar);
	DockControlBar(&m_wndToolBar);

	// OutputBar must dock first, so docked againest left bar. 
	DockControlBar(GetAppOutputBar());
	DockControlBar(GetAppWorkspaceBar());

	// Add images for menu items that aren't on a toolbar
	CBCGPToolBar::AddToolBarForImageCollection (IDR_STOL_MENUIMAGES);
	CBCGPToolBar::AddToolBarForImageCollection (IDR_CLS_MENUIMAGES);
	CBCGPToolBar::AddToolBarForImageCollection (IDR_SECTOR_MENUIMAGES);
	CBCGPToolBar::AddToolBarForImageCollection (IDR_FRAME_MENUIMAGES);
	CBCGPToolBar::AddToolBarForImageCollection (IDR_STAR_MENUIMAGES);
	CBCGPToolBar::AddToolBarForImageCollection (IDR_CODEMAX_MENUIMAGES);
	CBCGPToolBar::AddToolBarForImageCollection (IDR_RTCS_MENUIMAGES);
	CBCGPToolBar::AddToolBarForImageCollection (IDR_RTCSSET_MENUIMAGES);
	CBCGPToolBar::AddToolBarForImageCollection (IDR_RTCSCMD_MENUIMAGES);
	CBCGPToolBar::AddToolBarForImageCollection (IDR_PACECMD_MENUIMAGES);
	CBCGPToolBar::AddToolBarForImageCollection (IDR_STOLCMD_MENUIMAGES);

	// Allow user-defined toolbars operations:
	InitUserToobars (NULL,
					uiFirstUserToolBarId,
					uiLastUserToolBarId);
	return 0;
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::PreCreateWindow()
//	Description :	This method is called by the framework before the
//                  creation of the Windows CE window attached to this
//                  CWnd object.
//
//
//	Return :		BOOL
//						Nonzero if the window creation should continue;
//						0 to indicate creation failure.
//
//
//	Parameters :
//					CREATESTRUCT Address of a CREATESTRUCT structure
//					CREATESTRUCT structure defines the initialization parameters passed to the window procedure of an application.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if(!CMDIFrameWnd::PreCreateWindow(cs))
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CMDIFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CMDIFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnViewCustomize()
//
//	Description :	Customizes the toollbar.
//
//	Return :		Void - nothing.
//
//	Parameters :	Void - nothing.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnViewCustomize()
{
	//------------------------------------
	// Create a customize toolbars dialog:
	//------------------------------------
//	The option pages can be added to the customize dialog as shown below.
//	Maybe in the future I'll move them there and get rid of the options
//  property sheet.
//
//	CList<CRuntimeClass*,CRuntimeClass*> listCustomPages;
//	listCustomPages.AddTail(RUNTIME_CLASS(CSCPropPage));
//	listCustomPages.AddTail(RUNTIME_CLASS(CGTACSPropPage));
//	listCustomPages.AddTail(RUNTIME_CLASS(COATSPropPage));
//	listCustomPages.AddTail(RUNTIME_CLASS(CFolderPropPage));
//	listCustomPages.AddTail(RUNTIME_CLASS(CMiscPropPage));
//	listCustomPages.AddTail(RUNTIME_CLASS(CInstObjPropPage));

//	CBCGPToolbarCustomize* pDlgCust = new CBCGPToolbarCustomize (this,
//			TRUE /* Automatic menus scaning */, 
//			BCGCUSTOMIZE_MENU_SHADOWS | BCGCUSTOMIZE_TEXT_LABELS |
//			BCGCUSTOMIZE_LOOK_2000 | BCGCUSTOMIZE_MENU_ANIMATIONS |
//			BCGCUSTOMIZE_SELECT_SKINS | BCGCUSTOMIZE_NOHELP/*,&listCustomPages*/);

	CBCGPToolbarCustomize* pDlgCust = new CBCGPToolbarCustomize (this, TRUE);

	pDlgCust->EnableUserDefinedToolbars ();

	pDlgCust->Create ();
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolbarContextMenu()

//	Description :
//
//
//
//
//	Return :
//                  LRESULT -	always 0
//
//	Parameters :
//					wParam - Specifies additional information about the message.
//					         The exact meaning depends on the value of the uiMsg
//							 parameter.
//					lParam - Specifies additional information about the message.
//							 The exact meaning depends on the value of the uiMsg
//							 parameter.
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
LRESULT CMainFrame::OnToolbarContextMenu(WPARAM,LPARAM lp)
{
	CPoint point (BCG_GET_X_LPARAM (lp), BCG_GET_Y_LPARAM(lp));

	CMenu menu;
	VERIFY(menu.LoadMenu (IDR_POPUP_TOOLBAR));

	CMenu* pPopup = menu.GetSubMenu(0);
	ASSERT(pPopup != NULL);

	SetupToolbarMenu (*pPopup, ID_VIEW_USER_TOOLBAR1, ID_VIEW_USER_TOOLBAR10);

	CBCGPPopupMenu* pPopupMenu = new CBCGPPopupMenu;
	pPopupMenu->SetAutoDestroy (FALSE);
	pPopupMenu->Create (this, point.x, point.y, pPopup->Detach ());

	return 0;
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsViewUserToolbar ()

//	Description :
//
//
//
//
//	Return :
//                  Void	-	nothing
//
//	Parameters :
//					UINT	-	uiId
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
BOOL CMainFrame::OnToolsViewUserToolbar (UINT uiId)
{
	CBCGPToolBar* pUserToolBar = GetUserBarByIndex (uiId - ID_VIEW_USER_TOOLBAR1);
	if (pUserToolBar == NULL)
	{
		ASSERT (FALSE);
		return FALSE;
	}

	ShowControlBar (pUserToolBar, !(pUserToolBar->GetStyle () & WS_VISIBLE), FALSE, TRUE);
	RecalcLayout ();

	return TRUE;
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsViewUserToolbar()
//
//
//	Description :
//
//	Return :
//                  void -	nothing.
//
//	Parameters :
//					CCmdUI* pCmdUI pointer to a CCmdUI object.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnUpdateToolsViewUserToolbar (CCmdUI* pCmdUI)
{
	CBCGPToolBar* pUserToolBar = GetUserBarByIndex (pCmdUI->m_nID - ID_VIEW_USER_TOOLBAR1);
	if (pUserToolBar == NULL)
	{
		pCmdUI->Enable (FALSE);
		return;
	}

	pCmdUI->Enable ();
	pCmdUI->SetCheck (pUserToolBar->GetStyle () & WS_VISIBLE);
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnToolbarReset(WPARAM wp, LPARAM)
//
//
//	Description :
//
//	Return :		LRESULT
//						Always returns 0.
//
//
//	Parameters :
//					wParam - Specifies additional information about the message.
//					         The exact meaning depends on the value of the uiMsg
//							 parameter.
//					lParam - Specifies additional information about the message.
//							 The exact meaning depends on the value of the uiMsg
//							 parameter.
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
afx_msg LRESULT CMainFrame::OnToolbarReset(WPARAM wp, LPARAM)
{
	return 0;
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 BOOL CMainFrame::OnShowPopupMenu ()
//
//
//	Description :
//
//	Return :		BOOL
//						TRUE	Succes.
//                      FALSE	Failure.
//
//	Parameters :
//				CBCGPPopupMenu* Pointer to CBCGPopupMenu.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
BOOL CMainFrame::OnShowPopupMenu (CBCGPPopupMenu* pMenuPopup)
{
 	//---------------------------------------------------------
	// Replace ID_VIEW_TOOLBARS menu item to the toolbars list:
	//---------------------------------------------------------
    CMDIFrameWnd::OnShowPopupMenu (pMenuPopup);

    if (pMenuPopup != NULL &&
		pMenuPopup->GetMenuBar ()->CommandToIndex (ID_VIEW_TOOLBARS) >= 0)
    {
		if (CBCGPToolBar::IsCustomizeMode ())
		{
			//----------------------------------------------------
			// Don't show toolbars list in the cuztomization mode!
			//----------------------------------------------------
			return FALSE;
		}

		pMenuPopup->RemoveAllItems ();

		CMenu menu;
		VERIFY(menu.LoadMenu (IDR_POPUP_TOOLBAR));

		CMenu* pPopup = menu.GetSubMenu(0);
		ASSERT(pPopup != NULL);

		SetupToolbarMenu (*pPopup, ID_VIEW_USER_TOOLBAR1, ID_VIEW_USER_TOOLBAR10);
		pMenuPopup->GetMenuBar ()->ImportFromMenu (*pPopup, TRUE);
    }

	return TRUE;

	//
	//
	//    CBCGPMDIFrameWnd::OnShowPopupMenu (pMenuPopup);
	//
	//    if (pMenuPopup == NULL)
	//	{
	//		m_hwndClickedTabWnd = NULL;
	//		return TRUE;
	//	}
	//
	//	if (pMenuPopup != NULL &&
	//		pMenuPopup->GetMenuBar ()->CommandToIndex (ID_VIEW_TOOLBARS) >= 0)
	//    {
	//		if (CBCGPToolBar::IsCustomizeMode ())
	//		{
	//			//----------------------------------------------------
	//			// Don't show toolbars list in the cuztomization mode!
	//			//----------------------------------------------------
	//			return FALSE;
	//		}
	//
	//		pMenuPopup->RemoveAllItems ();
	//
	//		CMenu menu;
	//		VERIFY(menu.LoadMenu (IDR_POPUP_TOOLBAR));
	//
	//		CMenu* pPopup = menu.GetSubMenu(0);
	//		ASSERT(pPopup != NULL);
	//
	//		SetupToolbarMenu (*pPopup, ID_VIEW_USER_TOOLBAR1, ID_VIEW_USER_TOOLBAR10);
	//		pMenuPopup->GetMenuBar ()->ImportFromMenu (*pPopup, TRUE);
	//    }
	//
	//	return TRUE;

}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnViewWorkspace()
//
//	Description :
//
//	Return :	Void nothing
//
//	Parameters :
//				Void nothing
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnViewWorkspace()
{
	ShowControlBar (GetAppWorkspaceBar(),
					!(GetAppWorkspaceBar()->GetStyle () & WS_VISIBLE),
					FALSE, TRUE);
	RecalcLayout ();
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnUpdateViewWorkspace()
//
//	Description :
//
//	Return :	Void nothing
//
//	Parameters :
//				CCmdUI	*	Pointer to CCmdUI object.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnUpdateViewWorkspace(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck (GetAppWorkspaceBar()->GetStyle () & WS_VISIBLE);
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnViewOutput()
//
//	Description :
//
//	Return :	Void nothing
//
//	Parameters :
//				Void nothing
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnViewOutput()
{
	ShowControlBar (GetAppOutputBar(),
					!(GetAppOutputBar()->GetStyle () & WS_VISIBLE),
					FALSE, TRUE);
	RecalcLayout ();
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnUpdateViewOutput()
//
//	Description :
//
//	Return :	Void nothing
//
//	Parameters :
//				CCmdUI *	- Pointer to CCmdUI object.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnUpdateViewOutput(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck (GetAppOutputBar()->GetStyle () & WS_VISIBLE);
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnToolsLoadDB()
//
//	Description :  This routine loads the EPOCH database.
//
//	Return :	Void nothing
//
//	Parameters :
//				Void nothing
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnToolsLoadDB()
{
	((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB =  ((CSchedMgrECPApp*)AfxGetApp())->LoadDatabase();
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnUpdateToolsLoadDB()
//
//	Description :	This routine toggles the enable/disable the
//					load database menu option.
//
//	Return :	Void nothing
//
//	Parameters :
//				CCmdUI * - Pointer to CCmdUI object.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnUpdateToolsLoadDB(CCmdUI* pCmdUI)
{
	// BOOL bTest = ((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB;
	pCmdUI->Enable(!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB);
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnToolsAdministrationMode()
//
//	Description : Enables/Disables the administrator option.
//
//	Return :	Void nothing
//
//	Parameters :
//				Void nothing
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnToolsAdministrationMode()
{
	CString strFmt = _T("Administrator mode allows an operator to modify low level\n")
					 _T("Spacecraft and Database Settings.  Generally, operators\n")
					 _T("should not enter Administrator Mode unless there is a\n")
					 _T("specific need to do so.  Low level Spacecraft and Database\n")
					 _T("settings are under configuration control and should be\n")
					 _T("modified only by configuration management staff.\n\n")
					 _T("Currently Administrator Mode is %s.\n\n")
					 _T("Do you want to toggle this setting?");
	CString strMsg;
	if (*GetAppAdminMode())
		strMsg.Format(strFmt, _T("Enabled"));
	else
		strMsg.Format(strFmt, _T("Disabled"));


	if (AfxMessageBox(strMsg, MB_YESNO) == IDYES)
	{
		*GetAppAdminMode() = !*GetAppAdminMode();
		strMsg.Format(IDS_ADMINPRIV_TOGGLE, *GetAppAdminMode() ? _T("Enabled") : _T("Disabled"));
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	}

}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnUpdateToolsAdministrationMode()
//
//	Description : Enables/Disables the administrator menu option.
//
//	Return :	Void nothing
//
//	Parameters :
//				CCmdUI * - Pointer to a CCmdUI object.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnUpdateToolsAdministrationMode(CCmdUI* pCmdUI)
{
	if (*GetAppAdminPriv())
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);

	if (*GetAppAdminMode())
		pCmdUI->SetCheck(TRUE);
	else
		pCmdUI->SetCheck(FALSE);
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnToolsWorkspaceOptions()
//
//	Description :	Manages the workspace options page.
//
//	Return :	Void nothing
//
//	Parameters :
//				Void nothing
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnToolsWorkspaceOptions()
{
	CWorkspaceOptionsSheet	sheet(_T("Workspace Options"));

	if (GetActiveFrame() != this)
	{
		CString strMessage;
		strMessage.LoadString(IDS_READONLY_WKSOPTIONS);
		GetAppOutputBar()->OutputToEvents(strMessage, COutputBar::INFO);
		sheet.SetReadOnly();
	}

	if (sheet.DoModal() == IDOK)
	{
		if (*GetAppLoadDB()){
			
			if( ((CSchedMgrECPApp*)AfxGetApp())->m_ulDBHandle != NULL ){
				CSchedMgrECPApp* pApp = (CSchedMgrECPApp*)AfxGetApp();
				(pApp->m_pEpDB)->CloseDatabase(((CSchedMgrECPApp*)AfxGetApp())->m_ulDBHandle);
			}

			((CSchedMgrECPApp*)AfxGetApp())->m_ulDBHandle = NULL;

			// Check if GTACS server has changed, if so then disconnect services
//			if (((CSchedMgrECPApp*)AfxGetApp())->m_NewGTACS == TRUE){




				// if (((CSchedMgrECPApp*)AfxGetApp())->Connect2EpochServices()){
					// Load database
					// ((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB =((CSchedMgrECPApp*)AfxGetApp())->LoadDatabase();
				// }
//			} else {  
				// No need to stop services if GTACS server has not changed.
				((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB =((CSchedMgrECPApp*)AfxGetApp())->LoadDatabase();
//			}
		}
		// Check to see if we need to refresh the workspacebar
		if (sheet.m_bRefresh)
			GetAppWorkspaceBar()->Refresh();  // Don't call OnViewRefresh - we need to re-init due to a new DB.
	}
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnUpdateToolsWorkspaceOptions()
//
//	Description :	Enables the workspace menu option.
//
//	Return :	Void nothing
//
//	Parameters :
//				CCmdUI	*	- pointer to CCmdUI object.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnUpdateToolsWorkspaceOptions(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnToolsProperties()
//
//	Description :	Updates and displays the property sheet.
//
//	Return :	Void nothing
//
//	Parameters :
//				Void nothing
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnToolsProperties()
{
	GetAppWorkspaceBar()->UpdatePropertySheet();
	GetAppWorkspaceBar()->ShowPropertySheet();
}

/************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 08/20/2001		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	 CMainFrame::OnUpdateToolsProperties()
//
//	Description :	Enables/Disables the properties menu option.
//
//	Return :	Void nothing
//
//	Parameters :
//				CCmdUI	* -	pointer to a CCmdUI object.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************/
void CMainFrame::OnUpdateToolsProperties(CCmdUI* pCmdUI)
{
	if ((GetFocus() == &(GetAppWorkspaceBar()->m_wndMasterTreeCtrl)) ||
		(GetFocus() == &(GetAppWorkspaceBar()->m_wndDailyTreeCtrl)))
	{
		GetAppWorkspaceBar()->m_pTreeCtrlInFocus = (CTreeCtrl*)GetFocus();
		pCmdUI->Enable(TRUE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnContextMenu
//	Description :	This routine is called whenever a user right
//					clicks.  If the click was in the general area,
//					then OnToolbarContextMenu is called.  If the click
//					is on one of the MDI tabs, then the Tab popup
//					menu is shown
//	Return :		void	-
//	Parameters :
//			CWnd* pWnd		-	Window where the mouse was clicked.
//			CPoint point	-	Coordinates of the mouse click.
//	Note :
//////////////////////////////////////////////////////////////////////
**********************************/
void CMainFrame::OnContextMenu(CWnd* pWnd, CPoint point)
{
	if (pWnd->GetSafeHwnd () == m_wndClientArea.GetSafeHwnd ())
	{
		OnToolbarContextMenu (0, MAKELPARAM(point.x, point.y));
	}
	else if (pWnd->GetSafeHwnd () == m_wndClientArea.GetMDITabs ().GetSafeHwnd ())
	{
		const CBCGPTabWnd& wndTab = m_wndClientArea.GetMDITabs ();

		CRect rectTabs;
		wndTab.GetTabsRect (rectTabs);

		CPoint ptTab = point;
		wndTab.ScreenToClient (&ptTab);

		int iTab = wndTab.GetTabFromPoint (ptTab);
		if (iTab >= 0)
		{
			m_hwndClickedTabWnd = wndTab.GetTabWnd (iTab)->GetSafeHwnd ();
		}

		theApp.ShowPopupMenu (IDR_POPUP_MDI_TABS, point, pWnd);
	}
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateMditabsIcons
//	Description :	Update the appearance of the Show MDI Tab Icons
//					menu button
//	Return :		void	-
//	Parameters :
//			CCmdUI* pCmdUI	-
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateMditabsIcons(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (theApp.m_bMDITabs);
	pCmdUI->SetCheck (theApp.m_bMDITabIcons);
}

/************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnMditabsIcons
//	Description :	This routine will toggle the appearence of icons
//					on the MDI tabs
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnMditabsIcons()
{
	theApp.m_bMDITabIcons = !theApp.m_bMDITabIcons;
	EnableMDITabs (theApp.m_bMDITabs, theApp.m_bMDITabIcons, theApp.m_MDITabLocation);
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateMditabsTop
//	Description :	Update the appearence of the MDI Top
//					menu button
//	Return :		void	-	nothing
//	Parameters :
//			CCmdUI* pCmdUI	-
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateMditabsTop(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (theApp.m_bMDITabs);
	pCmdUI->SetRadio (theApp.m_MDITabLocation == CBCGPTabWnd::LOCATION_TOP);
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnMditabsTop
//	Description :	This routine will place the MDI Tabs along the
//					top of the MDI client area.
//	Return :		void	-	nothing
//	Parameters :
//	Note :
//////////////////////////////////////////////////////////////////////
**********************************/
void CMainFrame::OnMditabsTop()
{
	theApp.m_MDITabLocation = CBCGPTabWnd::LOCATION_TOP;
	EnableMDITabs (theApp.m_bMDITabs, theApp.m_bMDITabIcons, theApp.m_MDITabLocation);
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateMditabsBottom
//	Description :	Update the appearence of the MDI Bottom
//					menu button
//	Return :		void	-	nothing
//	Parameters :
//			CCmdUI* pCmdUI	-
//	Note :
//////////////////////////////////////////////////////////////////////
**********************************/
void CMainFrame::OnUpdateMditabsBottom(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (theApp.m_bMDITabs);
	pCmdUI->SetRadio (theApp.m_MDITabLocation == CBCGPTabWnd::LOCATION_BOTTOM);
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnMditabsBottom
//	Description :	This function will place the MDI tabs along the
//					bottom of the MDI client area.
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnMditabsBottom()
{
	theApp.m_MDITabLocation = CBCGPTabWnd::LOCATION_BOTTOM;
	EnableMDITabs (theApp.m_bMDITabs, theApp.m_bMDITabIcons, theApp.m_MDITabLocation);
}

//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateViewMdiTabs
//	Description :	Update the appearence of the View MDI Buttons
//					menu button
//	Return :		void	-	nothing
//	Parameters :
//			CCmdUI* pCmdUI	-
//	Note :
//////////////////////////////////////////////////////////////////////
void CMainFrame::OnUpdateViewMdiTabs(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck (theApp.m_bMDITabs);
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnViewMdiTabs
//	Description :	This routine will toggle the appearence of the MDI
//					tab buttons.  These are the buttons that appear
//					along the bottom of the MDI client area to allow
//					easy access to the MDI windows.
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
**********************************/
void CMainFrame::OnViewMdiTabs()
{
	theApp.m_bMDITabs = !theApp.m_bMDITabs;
	EnableMDITabs (theApp.m_bMDITabs, theApp.m_bMDITabIcons, theApp.m_MDITabLocation);
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateMditabsClose
//	Description :	Update the appearence of the Close MDI Tab
//					menu button
//	Return :		void	-	nothing
//	Parameters :
//			CCmdUI* pCmdUI	-
//	Note :
//////////////////////////////////////////////////////////////////////
**********************************/
void CMainFrame::OnUpdateMditabsClose(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (m_hwndClickedTabWnd != NULL);
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 5/2/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnMditabsClose
//	Description :	Close the MDI Tabs
//	Return :		void	-	nothing
//	Parameters :
//	Note :
//////////////////////////////////////////////////////////////////////
**********************************/
void CMainFrame::OnMditabsClose()
{
	ASSERT (m_hwndClickedTabWnd != NULL);
	::PostMessage (m_hwndClickedTabWnd, WM_CLOSE, 0, 0);
}

/**********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateFileNew
//	Description :	Update the appearence of the File New
//					menu button
//	Return :		void	-	nothing
//	Parameters :
//			CCmdUI* pCmdUI	-
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateFileNew(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

void CMainFrame::OnFileNew()
{
	CNewDialog dlg;
	if (dlg.DoModal() == IDOK)
	{
		//Open the first document subtype of the type selected
		GetAppDocTemplate(dlg.m_nTypeSelected)->OpenDocumentFile(NULL);
	}
}

/**************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateViewRefresh
//	Description :	Update the appearence of the Refresh
//					menu button
//	Return :		void	-	nothing
//	Parameters :
//			CCmdUI* pCmdUI	-
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateViewRefresh(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (GetAppWorkspaceBar()->GetStyle () & WS_VISIBLE);
}

/************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnViewRefresh
//	Description :	This routine refreshes all of the directory trees.
//	Return :		void	-
//	Parameters :
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnViewRefresh()
{
	GetAppWorkspaceBar()->m_wndMasterTreeCtrl.UpdateTree();
	GetAppWorkspaceBar()->m_wndDailyTreeCtrl.UpdateTree();
}

/************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateViewShowExt
//	Description :	Update the appearence of the ShowExtension
//					menu button
//	Return :		void	- nothing
//	Parameters :
//			CCmdUI* pCmdUI	-
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateViewShowExt(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (GetAppWorkspaceBar()->GetStyle () & WS_VISIBLE);
	pCmdUI->SetCheck (GetAppRegMisc()->m_bShowExt);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnViewShowExt
//	Description :	The routine toggles the Show Extension flag.  It
//					update the view.
//	Return :		void - nothing
//	Parameters :	void - nothing
//	Note :
//////////////////////////////////////////////////////////////////////
**************************************/
void CMainFrame::OnViewShowExt()
{
	GetAppRegMisc()->m_bShowExt = !GetAppRegMisc()->m_bShowExt;
	OnViewRefresh();
	GetAppRegMisc()->SetValues(); // Save value when changed.

	LRESULT dwResult;
	// Go through all of the open documents and call OnFileModified.  This will
	// update the document's banner so that the extension is either shown or hidden
	POSITION posTemplate = AfxGetApp()->GetFirstDocTemplatePosition();
	while (posTemplate != NULL)
	{
		CDocTemplate* pDocTemplate = ((CSchedMgrECPApp*)AfxGetApp())->GetNextDocTemplate(posTemplate);
		BOOL bFound = FALSE;
		if (pDocTemplate != NULL)
		{
			POSITION posDoc = pDocTemplate->GetFirstDocPosition();
			while ((posDoc != NULL) && !bFound)
			{
				CDocument* pDoc = pDocTemplate->GetNextDoc(posDoc);
				if (pDoc->IsKindOf(RUNTIME_CLASS(CCodeMaxDocEx)))
					((CCodeMaxDocEx*)pDoc)->OnFileModified(NULL, &dwResult);
			}
		}
	}
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateViewSortByFilename
//	Description :	Update the appearence of the SortByFilename
//					menu button
//	Return :		void	-	nothing
//	Parameters :
//			CCmdUI* pCmdUI	-
//	Note :
//////////////////////////////////////////////////////////////////////
*****************************************/
void CMainFrame::OnUpdateViewSortByFilename(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (GetAppWorkspaceBar()->GetStyle () & WS_VISIBLE);
	pCmdUI->SetRadio(!GetAppRegMisc()->m_bSortByExt);
}

/**************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnViewSortByFilename
//	Description :	Clears the Sort By Extension flag to TRUE and
//					refreshs the view.
//	Return :		void	-
//	Parameters :
//	Note :
//////////////////////////////////////////////////////////////////////
****************************************/
void CMainFrame::OnViewSortByFilename()
{
	GetAppRegMisc()->m_bSortByExt = FALSE;
	OnViewRefresh();
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateViewSortByExtension
//	Description :	Update the appearence of the SortByExtension
//					menu button
//	Return :		void	-	nothing
//	Parameters :
//			CCmdUI* pCmdUI	-
//	Note :
//////////////////////////////////////////////////////////////////////
****************************************/
void CMainFrame::OnUpdateViewSortByExtension(CCmdUI* pCmdUI)
{
	pCmdUI->Enable (GetAppWorkspaceBar()->GetStyle () & WS_VISIBLE);
	pCmdUI->SetRadio(GetAppRegMisc()->m_bSortByExt);
}

/*************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnViewSortByExtension
//	Description :	Sets the Sort By Extension flag to TRUE and
//					refreshs the view.
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnViewSortByExtension()
{
	GetAppRegMisc()->m_bSortByExt = TRUE;
	OnViewRefresh();
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::GetInFocusDocInfo
//	Description :	This routine returns information on the file
//					that is in focus.  This means that if the user
//					is pointing to a file on the workspace bar, info
//					on the selected file will be returned.  Otherwise
//					information on the currently active MDI window
//					will be returned.
//	Return :		void	-	Pointer to document if document is opened.
//	Parameters :
//			CString*	pstrFilePath	- Path of files
//			short int*	pnType			- File type,see Constants.h
//										  If this value is
//										  nInvalidType, then all
//										  other data should be
//										  ignored.
//			BOOL*		pbFolder		- TRUE - folder, FALSE - file
//	Note :
//////////////////////////////////////////////////////////////////////
****************************************/
CDocument* CMainFrame::GetInFocusDocInfo(CString* pstrFilePath, short int* pnType, BOOL* pbFolder)
{
	CDocument*	pDoc = NULL;
	CWnd* pFocusWnd = GetFocus();
	CTreeCtrl*	pTreeCtrl = GetAppWorkspaceBar()->m_pTreeCtrlInFocus;
	if ((pFocusWnd == pTreeCtrl) && (pTreeCtrl != NULL))
	{
		CWorkspaceTreeCtrlData* pData = (CWorkspaceTreeCtrlData*)pTreeCtrl->GetItemData(pTreeCtrl->GetSelectedItem());
		*pstrFilePath	= pData->m_strFilePath;
		*pnType			= pData->m_nType;
		*pbFolder		= pData->m_bFolder;
	}
	else
	{
		*pbFolder		= FALSE;
		*pnType			= nInvalidType;
		pstrFilePath->Empty();

		CMDIChildWnd	*pMDIActive = ((CMainFrame*)AfxGetMainWnd())->MDIGetActive();
		if (pMDIActive)
		{
			CDocument* pActiveDoc = pMDIActive->GetActiveDocument();
			if (pActiveDoc)
			{
				pDoc = pActiveDoc;
				*pstrFilePath = pActiveDoc->GetPathName();
				if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CSTOLDoc)))
					*pnType = nSTOL;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CCLSDoc)))
					*pnType = nCLS;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CRptDoc)))
					*pnType = nRpt;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CSTOLMapDoc)))
					*pnType = nSTOLMap;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CRTCSMapDoc)))
					*pnType = nRTCSMap;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CSectorObjDoc)))
					*pnType = nSector;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CFrameObjDoc)))
					*pnType = nFrame;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CStarObjDoc)))
					*pnType = nStar;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CBinDoc)))
					*pnType = nBinary;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CLisDoc)))
					*pnType = nLis;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CRTCSSetDoc)))
					*pnType = nRTCSSet;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CRTCSDoc)))
					*pnType = nRTCS;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CIMCDoc)))
					*pnType = nIMC;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CRTCSCmdDoc)))
					*pnType = nRTCSCmd;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CPaceCmdDoc)))
					*pnType = nPaceCmd;
				else if (pActiveDoc->IsKindOf(RUNTIME_CLASS(CSTOLCmdDoc)))
					*pnType = nSTOLCmd;
				else
					ASSERT(FALSE);
			}
		}
	}
	return pDoc;
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::UpdateOpenDocuments()
//
//	Description :	Updates all open documents.
//
//	Returns :		void	-	nothing
//	Parameters :	void	-	nothing
//
//	Note :
//////////////////////////////////////////////////////////////////////
****************************************/
void CMainFrame::UpdateOpenDocuments()
{
	POSITION posTemplate = AfxGetApp()->GetFirstDocTemplatePosition();
	while (posTemplate != NULL)
	{
		CDocTemplate* pDocTemplate = ((CSchedMgrECPApp*)AfxGetApp())->GetNextDocTemplate(posTemplate);
		BOOL bFound = FALSE;
		if (pDocTemplate != NULL)
		{
			POSITION posDoc = pDocTemplate->GetFirstDocPosition();
			while ((posDoc != NULL) && !bFound)
			{
				CDocument* pDoc = pDocTemplate->GetNextDoc(posDoc);
				if (pDoc->IsKindOf(RUNTIME_CLASS(CCodeMaxDocEx)))
				{
					CTime	cTimeLastMod;
					CFileFind finder;
					if (finder.FindFile(pDoc->GetPathName()))
					{
						if (finder.FindNextFile() >= 0)
						{
							finder.GetLastWriteTime(cTimeLastMod);
							CTime	cOpenDocTime = ((CCodeMaxDocEx*)pDoc)->m_cTimeLastMod;

							if (cTimeLastMod > ((CCodeMaxDocEx*)pDoc)->m_cTimeLastMod)
							{
								((CCodeMaxDocEx*)pDoc)->m_cTimeLastMod = cTimeLastMod;
								::SendMessage(((CCodeMaxDocEx*)pDoc)->m_wndEdit, CMM_OPENFILE, 0,
									(LPARAM)(LPCTSTR)pDoc->GetPathName());
							}
						}
					}
				}
			}
		}
	}
}

/********************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::IsDocModified
//	Description :	This routine will return TRUE if the filepath
//					passed in is currently opened in one of the MDI
//					windows and the file's dirty bit has been set.
//	Return :		BOOL	-	TRUE if opened and modified.
//	Parameters :
//			CString		strFilePath	-	Filepath
//			short int	nType		-	Type of file
//	Note :
//////////////////////////////////////////////////////////////////////
********************************************/
BOOL CMainFrame::IsDocModified(CString strFilePath, short int nType)
{
	BOOL bRtnStatus = FALSE;
	CDocTemplate* pDocTemplate = ((CSchedMgrECPApp*)AfxGetApp())->m_pDocTemplate[nType];
	BOOL bFound = FALSE;
	if (pDocTemplate != NULL)
	{
		POSITION posDoc = pDocTemplate->GetFirstDocPosition();
		while ((posDoc != NULL) && !bFound)
		{
			CDocument* pDoc = pDocTemplate->GetNextDoc(posDoc);
			POSITION posView = pDoc->GetFirstViewPosition();
			while ((posView != NULL) && !bFound)
			{
			//	CView* pView = pDoc->GetNextView(posView);
				pDoc->GetNextView(posView);
				if (pDoc->GetPathName().CompareNoCase(strFilePath) == 0)
				{
					bFound = TRUE;
					if (pDoc->IsModified())
						bRtnStatus = TRUE;
				}
			}
		}
	}
	return bRtnStatus;
}

//////////////////////////////////////////////////////////////////////
//	The following blocks of code handle the various menu buttons and
//  OnUpdate calls associated with the various document types
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//	Menu buttons for Star files
//////////////////////////////////////////////////////////////////////

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsGenerateLoadStar
//	Description :	Disable the menu button if the the selected file
//					is open and has been modified.
//	Return :		void	-	Nothing
//	Parameters :
//			CCmdUI* pCmdUI	-	UI Command pointer
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateToolsGenerateLoadStar(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	if (!bFolder)
	{
		if (nType == nStar)
		{
			if (IsDocModified(strFilePath, nType))
				pCmdUI->Enable(FALSE);
			else
				pCmdUI->Enable(TRUE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsGenerateLoadStar
//	Description :	This function is called when Generate Star upload
//					button is pressed.
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnToolsGenerateLoadStar()
{
	CString			strFilePath;
	short int		nType = nInvalidType;
	BOOL			bFolder = FALSE;
	CString			strMsg;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nStar);

	CGenLoad Star(strFilePath, CGenLoad::STAR_LDTYPE);
    Star.Send_GenLD_Dir();
	OnViewRefresh();
	TRACE(_T("Generate upload files was checked!\n"));
}


//////////////////////////////////////////////////////////////////////
//	Menu buttons for Frame files
//////////////////////////////////////////////////////////////////////

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsGenerateLoadFrame
//	Description :	Disable the menu button if the the selected file
//					is open and has been modified.
//	Return :		void	-	Nothing
//	Parameters :
//			CCmdUI* pCmdUI	-	UI Command pointer
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateToolsGenerateLoadFrame(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	if (!bFolder)
	{
		if (nType == nFrame)
		{
			if (IsDocModified(strFilePath, nType))
				pCmdUI->Enable(FALSE);
			else
				pCmdUI->Enable(TRUE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsGenerateLoadFrame
//	Description :	This function is called when Generate Frame upload
//					button is pressed.
//	Return :		void	-
//	Parameters :
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnToolsGenerateLoadFrame()
{
	CString			strFilePath;
	short int		nType = nInvalidType;
	BOOL			bFolder = FALSE;
	CString			strMsg;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nFrame);

	CGenLoad Frame(strFilePath, CGenLoad::FRAME_LDTYPE);
    Frame.Send_GenLD_Dir();
	OnViewRefresh();
}

//////////////////////////////////////////////////////////////////////
//	Menu buttons for STOL files
//////////////////////////////////////////////////////////////////////

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsSTOLVal
//	Description :	Disable the menu button if the the selected file
//					is open and has been modified.
//	Return :		void	-	Nothing
//	Parameters :
//			CCmdUI* pCmdUI	-	UI Command pointer
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateToolsSTOLVal(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;


	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nSTOL)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsSTOLVal
//	Description :	This function is called when STOL validate button
//					is pressed.
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Notes :
//
//		Master view:
//			Check if need to perform syntax checking.
//          Check if validating STOL proc.
//          Check if generate upload file; currently grayed out is this correct.
//
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnToolsSTOLVal()
{
	CString		strFilePath;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[5]= {"STOL"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nSTOL);

	CGenUpdSheet sheet(nValSTOL, strFilePath);

	TRACE(_T("In OnToolsSTOLVal()"));

	if (sheet.DoModal() == ID_WIZFINISH)
	{
		if ((sheet.GetSchedPage())->GetVal())
		{
			if (SyntaxCheck(nType, rType, strFilePath))
			{
				CSchedVal STOL(strFilePath);
				STOL.SetValLoc((sheet.GetSchedPage())->GetValLoc());
				STOL.SetRTCSSetFileName((sheet.GetSchedPage())->GetRTCSSetFilename());
				STOL.SetValRTCSProcsFlag((sheet.GetSchedPage())->GetRTCSVal());
				STOL.Validate(CSchedVal::STOL_VAL);
				TRACE(_T("Validate STOL proc was checked!\n"));
			} else {
				CFileException Exception;
				CTextFile STOL;
				if (TRUE == STOL.Load(strFilePath, CTextFile::RW, &Exception)){
					STOL.ExtractProlog();
					STOL.WriteValidationStatus(FAIL, (sheet.GetSchedPage())->GetValLoc());
					if (TRUE != STOL.Close(CTextFile::WRITE, &Exception)){
						CString strMsg;
						strMsg.Format(_T("Error closing file  %s"), strFilePath);
						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
						GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
						CString strException = STOL.ExceptionErrorText(&Exception);
						GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
						GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
					}
				} else {
					CString strMsg;
					strMsg.Format(_T("Error loading file: %s"), strFilePath);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
					CString strException = STOL.ExceptionErrorText(&Exception);
					GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
				}
			}
		}
	}
	UpdateOpenDocuments();
}

/******************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsSTOLGenLoad
//	Description :	Disable the menu button if the the selected file
//					is open and has been modified.
//	Return :		void	-	Nothing
//	Parameters :
//			CCmdUI* pCmdUI	-	UI Command pointer
//	Note :
//////////////////////////////////////////////////////////////////////
*******************************/
void CMainFrame::OnUpdateToolsSTOLGenLoad(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	if (!bFolder)
	{
		if (nType == nSTOL)
		{
			if (IsDocModified(strFilePath, nType))
				pCmdUI->Enable(FALSE);
			else
				pCmdUI->Enable(TRUE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsSTOLGenLoad
//	Description :	This function is called when the Generate Load
//					file for a STOL procedures button has been
//					pressed.
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
****************************************/
void CMainFrame::OnToolsSTOLGenLoad()
{
	CString			strFilePath;
	short int		nType	= nInvalidType;
	BOOL			bFolder = FALSE;
	CString			strMsg;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nSTOL);
//	CGenUpdSheet	sheet(nType, strFilePath);

	CGenUpdSheet	sheet(nGenSTOL, strFilePath);

	if ((sheet.GetSchedPage())->GetGen())
	{
		COXUNC	uncFilePath(strFilePath);
		CString strMapPath;
		CString strLisPath;
		CString strExt;
//		BOOL	bUseDay	= (sheet.GetOutputPage())->GetUseDay();

		GetAppDocTemplate(nSTOLMap)->GetDocString(strExt, CDocTemplate::filterExt);
		strMapPath	= uncFilePath.Server()+uncFilePath.Share()+uncFilePath.Directory()+uncFilePath.Base()+strExt;
		GetAppDocTemplate(nLis)->GetDocString(strExt, CDocTemplate::filterExt);
		strLisPath	= uncFilePath.Server()+uncFilePath.Share()+uncFilePath.Directory()+uncFilePath.Base()+strExt;

		CSchedGen STOL(strFilePath, CNewSchedFile::STOL);
		STOL.SetStolFileName(strFilePath);
		STOL.SetLisFileName(strLisPath);
		STOL.SetMapFileName(strMapPath);
		STOL.Gen_ProdFile();
		OnViewRefresh();
		TRACE(_T("Generate upload files was checked!\n"));
	}
}


//////////////////////////////////////////////////////////////////////
//	Menu buttons for CLS files
//////////////////////////////////////////////////////////////////////

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsCLSVal
//	Description :	This routine will enable the Validate CLS
//					function.
//	Return :		void	-
//	Parameters :
//			CCmdUI* pCmdUI	-	Pointer the CommandUI object
//	Note :
//////////////////////////////////////////////////////////////////////
*****************************************/
void CMainFrame::OnUpdateToolsCLSVal(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nCLS)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}

/************************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsCLSVal
//	Description :	This routine is called to validate a CLS file.
//	Return :		void	-
//	Parameters :
//	Note :
//////////////////////////////////////////////////////////////////////
*********************************************/
void CMainFrame::OnToolsCLSVal()
{
	CString		strFilePath;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[4]= {"CLS"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nCLS);
	TRACE(_T("Validate CLS proc was checked!\n"));

	CGenUpdSheet sheet(nValCLS, strFilePath);

	if (sheet.DoModal() == ID_WIZFINISH)
	{
		if ((sheet.GetCLSPage())->GetVal())
		{
			if (SyntaxCheck(nType, rType, strFilePath))
			{
				CSchedVal CLS(strFilePath);
				CLS.SetValLoc((sheet.GetCLSPage())->GetValLoc());
				CLS.SetRTCSSetFileName((sheet.GetSchedPage())->GetRTCSSetFilename());
				CLS.SetValRTCSProcsFlag((sheet.GetSchedPage())->GetRTCSVal());
				CLS.Validate(CSchedVal::CLS_VAL);
			} else {
				CFileException Exception;
				CTextFile CLS;
				if (TRUE == CLS.Load(strFilePath, CTextFile::RW, &Exception)){
					CLS.ExtractProlog();
					CLS.WriteValidationStatus(FAIL, (sheet.GetCLSPage())->GetValLoc());
					if (TRUE != CLS.Close(CTextFile::WRITE, &Exception)){
						CString strMsg;
						strMsg.Format(_T("Error closing file  %s"), strFilePath);
						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
						GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
						CString strException = CLS.ExceptionErrorText(&Exception);
						GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
						GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
					}
				} else {
					CString strMsg;
					strMsg.Format(_T("Error loading file: %s"), strFilePath);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
					CString strException = CLS.ExceptionErrorText(&Exception);
					GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
				}
			}
		}
	}
	UpdateOpenDocuments();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsCLSSyntaxCheck()
//	Description :	This routine is called to validate a CLS file.
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsCLSSyntaxCheck()
{
	CString		strFilePath;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[4]= {"CLS"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nCLS);

	SyntaxCheck(nType, rType, strFilePath);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsCLSSyntaxCheck()
//	Description :	This routine is called to validate a CLS file.
//	Return :		void	-	nothing
//	Parameters :	CCmdUI *	- Pointer to CCmdUI object.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsCLSSyntaxCheck(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nCLS)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}

/****************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsSchedMake
//	Description :	This routine will enable the Make Schedule
//					function.
//	Return :		void	-	none
//	Parameters :
//			CCmdUI* pCmdUI	-	Pointer the CommandUI object
//	Note :
//////////////////////////////////////////////////////////////////////
******************************************/
void CMainFrame::OnUpdateToolsSchedMake(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nCLS)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsSchedMake
//	Description :	This routine is called to create a complete
//					schedule package
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsSchedMake()
{
	CString			strFilePath;
	CString			strStolPath;
	short int		nType = nInvalidType;
	BOOL			bFolder = FALSE;

	TRACE(_T("Schedule make was checked!\n"));

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nCLS);
	CGenUpdSheet	sheet(nPackage, strFilePath);

	if(sheet.DoModal() == ID_WIZFINISH)
	{
		CWaitCursor	wait;
		CString		strMsg;

		if((sheet.GetCLSPage())->GetVal())
		{
			char rType[4] = {"CLS"};
			// Validate CLS
			if(TRUE != SyntaxCheck(nType, rType, strFilePath))
			{
				CFileException	Exception;
				CTextFile		CLS;
				if (TRUE == CLS.Load(strFilePath, CTextFile::RW, &Exception)){
					CLS.ExtractProlog();
					CLS.WriteValidationStatus(FAIL, (sheet.GetCLSPage())->GetValLoc());
					if (TRUE != CLS.Close(CTextFile::WRITE, &Exception)){
						CString strMsg;
						strMsg.Format(_T("Error closing file  %s"), strFilePath);
						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
						GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
						CString strException = CLS.ExceptionErrorText(&Exception);
						GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
						GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
					}
				} else {
					CString strMsg;
					strMsg.Format(_T("Error loading file: %s"), strFilePath);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
					CString strException = CLS.ExceptionErrorText(&Exception);
					GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
				}
				UpdateOpenDocuments();
				OnViewRefresh();
				return;
			}

			CSchedVal CLS(strFilePath);
			CLS.SetRTCSSetFileName((sheet.GetSchedPage())->GetRTCSSetFilename());
			CLS.SetValLoc((sheet.GetCLSPage())->GetValLoc());
			CLS.SetValRTCSProcsFlag((sheet.GetSchedPage())->GetRTCSVal());

			CLS.Validate(CSchedVal::CLS_VAL);
		}

		// Always generate a master schedule.
		strStolPath = sheet.MakeCLSGenFilepath();

		COXUNC	uncStolPath(strStolPath);
		CString strExt;

		if((uncStolPath.Extension()).IsEmpty())
		{
			GetAppDocTemplate(nSTOL)->GetDocString(strExt, CDocTemplate::filterExt);
			strStolPath += strExt;
		}

		CSchedGen SchedGen(strFilePath, CNewSchedFile::CLS);
		SchedGen.SetDayOfYear((sheet.GetCLSPage())->GetDOY());
		SchedGen.SetYear((sheet.GetCLSPage())->GetYear());
		SchedGen.SetStolFileName(strStolPath);

		if(TRUE != SchedGen.Gen_ProdFile()){
			UpdateOpenDocuments();
			OnViewRefresh();
			return;
		}

		TRACE(_T("Generate upload files was checked!"));

		if((sheet.GetSchedPage())->GetVal())
		{
			char rType[5] = {"STOL"};

			// Validate CLS generated master schedule
			if(TRUE != SyntaxCheck(nSTOL, rType, strStolPath))
			{
				CFileException	Exception;
				CTextFile		STOL;
				if (TRUE == STOL.Load(strFilePath, CTextFile::RW, &Exception)){
					STOL.ExtractProlog();
					STOL.WriteValidationStatus(FAIL, (sheet.GetSchedPage())->GetValLoc());
					if (TRUE != STOL.Close(CTextFile::WRITE, &Exception)){
						CString strMsg;
						strMsg.Format(_T("Error closing file  %s"), strFilePath);
						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
						GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
						CString strException = STOL.ExceptionErrorText(&Exception);
						GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
						GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
					}
				} else {
					CString strMsg;
					strMsg.Format(_T("Error loading file: %s"), strFilePath);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
					CString strException = STOL.ExceptionErrorText(&Exception);
					GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
				}
				UpdateOpenDocuments();
				OnViewRefresh();
				return;
			}

			CSchedVal Sched(strStolPath);
			Sched.SetRTCSSetFileName((sheet.GetSchedPage())->GetRTCSSetFilename());
			Sched.SetValRTCSProcsFlag((sheet.GetSchedPage())->GetRTCSVal());
			Sched.SetValLoc((sheet.GetSchedPage())->GetValLoc());

			Sched.Validate(CSchedVal::STOL_VAL);
		}

		// Update CLS generated master schedule
		if( !SchedUpd(&sheet, strStolPath)){
			UpdateOpenDocuments();
			OnViewRefresh();
			//SSGS-1156: Needs to skip the next return even if we couldn't update the schedule.
			//return; 
		}

		strMsg = _T("Creation of Schedule Package complete.");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
		GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::INFO);
		UpdateOpenDocuments();
		OnViewRefresh();

		//SSGS-1156: Add Schedule Distribution prompt after schedule generation/Update
		strMsg = _T("Would you like to distribute the new Schedule Package?");
		if (AfxMessageBox(strMsg, MB_YESNO) == IDYES)
		{	
			OnToolsFileDistributionWizard();
		}
	}
}

//////////////////////////////////////////////////////////////////////
//	Menu buttons for IMC files
//////////////////////////////////////////////////////////////////////

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOATSRequestIMCSet()
//	Description :	This routine toggles the enable/disable
//					the IMC request menu option.
//	Return :		void	-	nothing
//	Parameters :	CCmdUI *	pointer to CCmdUI object.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsOATSRequestIMCSet(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOATSRequestIMCSet()
//	Description :	This routine retrieves an IMC set from
//					OATS.
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsOATSRequestIMCSet()
{
	CGenUpdSheet	sheet(nReqIMC, _T("A000"));  // Filename is also default

	if (sheet.DoModal() == ID_WIZFINISH)
	{
		CString			strMsg;
		CString			strFilePath((sheet.GetOutputPage())->GetStrPath());
		CWaitCursor		cWait;
		CSchedUpdUtil	updUtil;

		updUtil.RptFile(strFilePath);
		updUtil.IMCSet((sheet.GetOutputPage())->GetIMCSet());

		// 1. Get a socket connection to OATS
		if (!updUtil.Connect())
			return;

		// 2. Create IMC Set report
		if (updUtil.IMCRpt())
			OnViewRefresh();

		updUtil.Disconnect();
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function:		CMainFrame::OnUpdateToolsOatsGenerateFrameIntrusionReport
//	Description :	This routine will enable the generate a frame intrusion
//					report menu option.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI * -	Pointer to a CCmdUI object.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsOatsGenerateFrameIntrusionReport(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOatsGenerateFrameIntrusionReport()
//	Description :	This routine when called will create a frame intrusion
//                  report.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsOatsGenerateFrameIntrusionReport()
{
	CGenUpdSheet sheet(nReqIntrusion, _T("Frame_Intrusion"));  // Filename is also default

	if (sheet.DoModal() == ID_WIZFINISH)
	{
		CString			strMsg;
		CString			strFilePath((sheet.GetOutputPage())->GetStrPath());
		CWaitCursor		cWait;
		CSchedUpdUtil	updUtil;

		updUtil.RptFile(strFilePath);
		updUtil.StartTime((sheet.GetOATSPage())->GetStartTime());
		updUtil.Duration((sheet.GetOATSPage())->GetDuration());

		// 1. Get a socket connection to OATS
		if (!updUtil.Connect())
			return;

		// 2. Create OATS Intrusion report
		if(updUtil.GenerateIntrusion(2))
			OnViewRefresh();

		updUtil.Disconnect();
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :	CMainFrame::OnUpdateToolsOatsGenerateSensorIntrusionReport
//	Description :	This routine enables the Generate Sensor Intrusion Report.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI * -	Pointer to a Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsOatsGenerateSensorIntrusionReport(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOatsGenerateSensorIntrusionReport
//	Description :	This routine creates a sensor intrusion report.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsOatsGenerateSensorIntrusionReport()
{
	CGenUpdSheet sheet(nReqIntrusion, _T("Sensor_Intrusion"));  // Filename is also default

	if (sheet.DoModal() == ID_WIZFINISH)
	{
		CString			strMsg;
		CString			strFilePath((sheet.GetOutputPage())->GetStrPath());
		CWaitCursor		cWait;
		CSchedUpdUtil	updUtil;

		updUtil.RptFile(strFilePath);
		updUtil.StartTime((sheet.GetOATSPage())->GetStartTime());
		updUtil.Duration((sheet.GetOATSPage())->GetDuration());

		// 1. Get a socket connection to OATS
		if (!updUtil.Connect())
			return;

		// 2. Create OATS Intrusion report
		if (updUtil.GenerateIntrusion(1))
			OnViewRefresh();

		updUtil.Disconnect();
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOATSGenerateEclipseReport
//	Description :	This routine enables the generate eclipse report
//					menu option.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	pointer to a Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsOATSGenerateEclipseReport(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOATSGenerateEclipseReport
//	Description :	This routine generates a eclipse report.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsOATSGenerateEclipseReport()
{
	CGenUpdSheet sheet(nReqEclipse, _T("Eclipse"));  // Filename is also default

	if (sheet.DoModal() == ID_WIZFINISH)
	{
		CString			strMsg;
		CString			strFilePath((sheet.GetOutputPage())->GetStrPath());
		CWaitCursor		cWait;
		CSchedUpdUtil	updUtil;

		updUtil.RptFile(strFilePath);
		updUtil.StartTime((sheet.GetOATSPage())->GetStartTime());
		updUtil.Duration((sheet.GetOATSPage())->GetDuration());

		// 1. Get a socket connection to OATS
		if (!updUtil.Connect())
			return;

		// 2. Create OATS Eclipse Schedule report
		if (updUtil.EclipseRpt())
			OnViewRefresh();

		updUtil.Disconnect();
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOATSIMCRequestIMCSFCS
//	Description :	This routine enables the request for a star
//					calibration minischedule menu option.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	pointer to a Command UI object
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsOATSIMCRequestIMCSFCS(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOATSIMCRequestIMCSFCS()
//	Description :	This routine requests from OATS a Scale Factor
//                  Calibration Schedule minischedule.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsOATSIMCRequestIMCSFCS()
{
	CGenUpdSheet sheet(nReqSFCS, _T("SFCS"));  // Filename is also default

	if (sheet.DoModal() == ID_WIZFINISH)
	{
		CString			strMsg;
		CString			strFilePath((sheet.GetOutputPage())->GetStrPath());

		CWaitCursor		cWait;
		CSchedUpdUtil	updUtil;

		updUtil.RptFile(strFilePath);
		updUtil.Instrument((sheet.GetOATSPage())->GetInstrument());
		updUtil.StartTime((sheet.GetOATSPage())->GetStartTime());
		updUtil.Duration((sheet.GetOATSPage())->GetDuration());

		// 1. Get a socket connection to OATS
		if (!updUtil.Connect())
			return;

		// 2. Create Scale Factor Calibration Schedule report
		if (updUtil.IMCScaleFactorCalSched())
			OnViewRefresh();

		updUtil.Disconnect();
	}
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOutputEventCopy
//	Description :	This routine enables the event copy menu option.
//                  This option is only enabled if there are events
//                  in the list.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	Pointer to the Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsOutputEventCopy(CCmdUI* pCmdUI)
{
	if (GetAppOutputBar()->GetEventList()->GetItemCount() == 0)
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOutputEventCopy()
//	Description :	This routine copies event objects to the clipboard
//					pasting.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsOutputEventCopy()
{
	OutputCopy(GetAppOutputBar()->GetEventList());
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOutputBuildCopy
//	Description :	This routine enables the build copy menu option.
//                  This option is only enabled if there are events
//                  in the list.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	Pointer to a Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsOutputBuildCopy(CCmdUI* pCmdUI)
{
	if (GetAppOutputBar()->GetBuildList()->GetItemCount() == 0)
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOatsGenerateFrameIntrusionReport()
//	Description :	This routine copies build objects to the clipboard
//					pasting.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsOutputBuildCopy()
{
	OutputCopy(GetAppOutputBar()->GetBuildList());
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOutputEventSaveAs
//	Description :	This routine enables the SaveAs option for the
//					event list.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* Pointer to a Commnand UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsOutputEventSaveAs(CCmdUI* pCmdUI)
{
	if (GetAppOutputBar()->GetEventList()->GetItemCount() == 0)
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOutputEventSaveAs()
//	Description :	This routine save the events log to a user specified
//                  file.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsOutputEventSaveAs()
{
	CFileDialog dlg(FALSE, _T(".rpt"), _T("Events"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
					_T("Report Files (*.rpt)|*.rpt|All Files (*.*)|*.*||"));
	if (dlg.DoModal())
		OutputSaveAs(GetAppOutputBar()->GetEventList(), dlg.GetPathName());
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOutputBuildSaveAs
//	Description :	This routine enables the build save as menu option.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	Pointer to a commnad UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsOutputBuildSaveAs(CCmdUI* pCmdUI)
{
	if (GetAppOutputBar()->GetBuildList()->GetItemCount() == 0)
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOutputBuildSaveAs()
//	Description :	This routine saves the build list to a user specified
//                  file.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsOutputBuildSaveAs()
{
	CFileDialog dlg(FALSE, _T(".rpt"), _T("Build"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
					_T("Report Files (*.rpt)|*.rpt|All Files (*.*)|*.*||"));
	if (dlg.DoModal())
		OutputSaveAs(GetAppOutputBar()->GetBuildList(), dlg.GetPathName());
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OutputGetText
//	Description :	This routine retrieves the info a control list and
//					formats it for coping, saving, or printing.
//
//	Return :		void	-	nothing
//	Parameters :
//					CListCtrl*  pointer to a CListCtrl object.
//					CString*	pointer to a CString.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OutputGetText(CListCtrl* pList, CString* pstrText)
{
	int nCnt = pList->GetItemCount();
	for (int nIndex=0; nIndex<nCnt; nIndex++)
	{
		CString strListText;
		CString strSeverity;
		LVITEM	lvItem;
		lvItem.mask = LVIF_IMAGE;
		lvItem.iItem = nIndex;
		lvItem.iSubItem = 0;
		pList->GetItem(&lvItem);
		switch (lvItem.iImage)
		{
			case COutputBar::INFO		: strSeverity = _T("Informational"); break;
			case COutputBar::WARNING	: strSeverity = _T("Warning      "); break;
			case COutputBar::FATAL		: strSeverity = _T("Fatal        "); break;
		}

		strListText.Format (_T("%s %s %s"), strSeverity,
											 pList->GetItemText(nIndex, 0),
										     pList->GetItemText(nIndex, 1));
		*pstrText = *pstrText + strListText + _T("\n");
	}
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OutputCopy
//	Description :	This routine formats and copies the list contents
//                  to the clipboard.
//
//	Return :		void	   - nothing
//	Parameters :	CListCtrl* - pointer to a list control object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OutputCopy(CListCtrl* pList)
{
	CString strText;

	OutputGetText(pList, &strText);

	if(OpenClipboard())
	{
		HGLOBAL clipbuffer;
		TCHAR * buffer;
		EmptyClipboard();
		clipbuffer = GlobalAlloc(GMEM_DDESHARE, (strText.GetLength()+1) * sizeof (TCHAR));
		buffer = (TCHAR*)GlobalLock(clipbuffer);
		_tcscpy_s(buffer, ((strText.GetLength()+1)), LPCTSTR(strText));
		GlobalUnlock(clipbuffer);
		SetClipboardData(CF_UNICODETEXT,clipbuffer);
		CloseClipboard();
	}
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OutputSaveAs
//	Description :	This routine formats and saves the list contents to
//					a file.
//
//	Return :		void	-	nothing
//	Parameters :
//					CListCtrl* Pointer to a list control object.
//					CString	   Name of file to save list contents to.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OutputSaveAs(CListCtrl* pList, CString strFilename)
{
	CString strText;

	OutputGetText(pList, &strText);

	CStdioFile stdFile;
	if (!stdFile.Open(strFilename, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		AfxMessageBox(_T("Could not open the file.  Verify that the disk is\n")
					  _T("not full and that a previous version is not locked\n")
					  _T("or write protected"));
	}
	else
	{
		stdFile.WriteString(strText);
		stdFile.Close();
	}
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOutputBuildGotoError()
//	Description :	This routine highlights the text line in the file
//                  that generated the error.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsOutputBuildGotoError()
{
	GetAppOutputBar()->GetBuildList()->GotoBuildError();
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOutputBuildGotoError
//	Description :	This routine enables the option to highlight the
//                  text line that generated the error.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI*	-	Pointer to a Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsOutputBuildGotoError(CCmdUI* pCmdUI)
{
	if (CWnd::GetFocus() == GetAppOutputBar()->GetBuildList())
	{
		if (GetAppOutputBar()->GetBuildList()->GetItemCount() != 0)
		{
			POSITION pos = GetAppOutputBar()->GetBuildList()->GetFirstSelectedItemPosition();
			if (pos != NULL)
			{
				int nIndex = GetAppOutputBar()->GetBuildList()->GetNextSelectedItem(pos);
				struct sErrorInfo* psEI = (struct sErrorInfo*)(GetAppOutputBar()->GetBuildList()->GetItemData(nIndex));
				if (psEI->m_nLineNumber != -1)
					pCmdUI->Enable(TRUE);
				else
					pCmdUI->Enable(FALSE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnDropFiles
//	Description :	The framework calls this member function when the
//					user releases the left mouse button over a window
//					that has registered itself as the recipient of
//					dropped files.
//
//	Return :		void	-	nothing
//	Parameters :	HDROP	-	A pointer to an internal data structure
//								that describes the dropped files.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnDropFiles(HDROP hDropInfo)
{
	CMDIFrameWnd::OnDropFiles(hDropInfo);
	DragFinish(hDropInfo);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOutputBuildClear
//	Description :	This routine enables the build clear menu option.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	Pointer to a Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsOutputBuildClear(CCmdUI* pCmdUI)
{
	if (GetAppOutputBar()->GetBuildList()->GetItemCount() == 0)
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOutputBuildClear()
//	Description :	TYhis routine clears the build list.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsOutputBuildClear()
{
	GetAppOutputBar()->ClearBuild();
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOutputEventClear
//	Description :	This routine enables the event clear list.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	Pointer to a Commnad UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsOutputEventClear(CCmdUI* pCmdUI)
{
	if (GetAppOutputBar()->GetEventList()->GetItemCount() == 0)
		pCmdUI->Enable(FALSE);
	else
		pCmdUI->Enable(TRUE);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOutputEventClear()
//	Description :	This routine clears the events log.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsOutputEventClear()
{
	GetAppOutputBar()->ClearEvents();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsSchedSyntaxCheck()
//	Description :	This routine syntax checks schedule files.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsSchedSyntaxCheck()
{
	CString		strFilePath;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[5]= {"STOL"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nSTOL);

	SyntaxCheck(nType, rType, strFilePath);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsSchedSyntaxCheck
//	Description :	This routine enables the syntax check menu option.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	Pointer to a Command UI object file.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsSchedSyntaxCheck(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nSTOL)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnViewMute()
//	Description :	This routine mutes the sound.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnViewMute()
{
	GetAppRegMisc()->m_bMuteSound = !GetAppRegMisc()->m_bMuteSound;
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateViewMute
//	Description :	This routine enables the mute menu option.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	Pointer to a Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateViewMute(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck (GetAppRegMisc()->m_bMuteSound);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOutputEventPrint
//	Description :	This routine prints the event log.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsOutputEventPrint()
{
	GetAppOutputBar()->PrintData((GetAppOutputBar()->GetEventList()));
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOutputEventPrint
//	Description :	This routine enables the print option on the menu option.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	Pointer to Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsOutputEventPrint(CCmdUI* pCmdUI)
{
	pCmdUI->Enable((GetAppOutputBar()->GetEventList()->GetItemCount() != 0 ? TRUE : FALSE));
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOutputBuildPrint()
//	Description :	This routine prints the contents of the build list.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsOutputBuildPrint()
{
	GetAppOutputBar()->PrintData((GetAppOutputBar()->GetBuildList()));
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsOutputBuildPrint
//	Description :	This routine enables the menu option for printing
//                  the build list contents.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI*	-	Pointer to Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsOutputBuildPrint(CCmdUI* pCmdUI)
{
	pCmdUI->Enable((GetAppOutputBar()->GetBuildList()->GetItemCount() != 0 ? TRUE : FALSE));
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsGenerateLoadRTCS()
//	Description :	This routine generates the necessary upload files
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsGenerateLoadRTCS()
{
	CString		strFilePath;
	CString		strMsg;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nRTCSSet);
	COXUNC		uncFile(strFilePath);

	strMsg.Format(_T("Generating the RTCS Upload Files for %s"),uncFile.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	CWaitCursor	wait;
	CRTCS		rtcs(strFilePath);

	if(!rtcs.OpenProductFiles())
		return;

	if(!rtcs.LoadProductFiles())
		return;

	CGenLoad Gen_RTCS(rtcs.GetMapFilename(), CGenLoad::RTCS_LDTYPE);

	if(!Gen_RTCS.Send_GenLD_Dir())
	{
		rtcs.DeleteProductFiles();
		return;
	}

	strMsg = _T("Generation of RTCS upload files complete.");
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	OnViewRefresh();
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsGenerateLoadRTCS
//	Description :	This routine enables the menu option for generating
//                  the upload files for RTCSs.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	pointer to a Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsGenerateLoadRTCS(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	if (!bFolder)
	{
		if (nType == nRTCSSet)
		{
			if (IsDocModified(strFilePath, nType))
				pCmdUI->Enable(FALSE);
			else
				pCmdUI->Enable(TRUE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsRTCSVal()
//	Description :	This routine syntax checks and validates a RTCS.
//
//	Return :		void	-	nothing
//	Parameters :	void	-	nothing
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnToolsRTCSVal()
{
	TRACE(_T("Validate RTCS proc was checked!\n"));
	CString		strFilePath;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[5]= {"RTCS"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nRTCS);

	if (TRUE == SyntaxCheck(nType, rType, strFilePath))
	{
		CSchedVal RTCS_Val(strFilePath);
		RTCS_Val.SetValLoc(RTCS);
		RTCS_Val.Validate(CSchedVal::RTCS_PRC_VAL);
	} else {
		CFileException Exception;
		CTextFile RTCS_Val;
		if (TRUE == RTCS_Val.Load(strFilePath, CTextFile::RW, &Exception)){
			RTCS_Val.ExtractProlog();
	        RTCS_Val.WriteValidationStatus(FAIL);
			if (TRUE != RTCS_Val.Close(CTextFile::WRITE, &Exception)){
				CString strMsg;
				strMsg.Format(_T("Error closing file  %s"), strFilePath);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				CString strException = RTCS_Val.ExceptionErrorText(&Exception);
				GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
				GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
			}
		} else {
			CString strMsg;
			strMsg.Format(_T("Error loading file: %s"), strFilePath);
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			CString strException = RTCS_Val.ExceptionErrorText(&Exception);
			GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
			GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
		}
	}
	UpdateOpenDocuments();
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsOatsGenerateFrameIntrusionReport()
//	Description :	This routine enables the menu option for validating RTCSs.
//
//	Return :		void	-	nothing
//	Parameters :	CCmdUI* -	Pointer to a command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
*************************************/
void CMainFrame::OnUpdateToolsRTCSVal(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nRTCS)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsRTCSSyntaxCheck()
//	Description :	This routine syntax checks RTCSs files.
//	Return :		void	-	none
//	Parameters :	void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsRTCSSyntaxCheck()
{
	CString		strFilePath;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[5]= {"RTCS"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nRTCS);

	SyntaxCheck(nType, rType, strFilePath);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsRTCSSyntaxCheck
//	Description :	This routine enables the menu option for syntax
//                  checking RTCS files.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	pointer to Commnad UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsRTCSSyntaxCheck(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nRTCS)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsRTCSSetVal()
//	Description :	This routine validates RTCS set files.
//	Return :		void	-	none
//	Parameters :	void	-	none
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnToolsRTCSSetVal()
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[8]= {"RTCSSET"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nRTCSSet);

	if (TRUE == SyntaxCheck(nType, rType, strFilePath))
	{
		CSchedVal RTCS_Set(strFilePath);
		RTCS_Set.SetValLoc(RTCS);
		RTCS_Set.SetRTCSSetFileName(strFilePath);
		RTCS_Set.Validate(CSchedVal::RTCS_SET_VAL);
		TRACE(_T("Validate RTCS Set file was checked!\n"));
	} else {
		CFileException Exception;
		CTextFile RTCS_Set;
		if (TRUE == RTCS_Set.Load(strFilePath, CTextFile::RW, &Exception)){
			RTCS_Set.ExtractProlog();
	        RTCS_Set.WriteValidationStatus(FAIL, RTCS);
			if (TRUE != RTCS_Set.Close(CTextFile::WRITE, &Exception)){
				CString strMsg;
				strMsg.Format(_T("Error closing file  %s"), strFilePath);
				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				CString strException = RTCS_Set.ExceptionErrorText(&Exception);
				GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
				GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
			}
		} else {
			CString strMsg;
			strMsg.Format(_T("Error loading file: %s"), strFilePath);
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			CString strException = RTCS_Set.ExceptionErrorText(&Exception);
			GetAppOutputBar()->OutputToEvents(strException, COutputBar::FATAL);
			GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
		}
	}
	UpdateOpenDocuments();
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsRTCSSetVal
//	Description :	This routine enables the menu option for validating
//                  RTCS set files.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	Pointer to Command UI object
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateToolsRTCSSetVal(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	if (!bFolder)
	{
		if (nType == nRTCSSet)
		{
			if (IsDocModified(strFilePath, nType))
				pCmdUI->Enable(FALSE);
			else
				pCmdUI->Enable(TRUE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsRTCSSetSyntaxCheck()
//	Description :	This routine syntax checks RTCS set files.
//	Return :		void -	none
//	Parameters :	void -	none
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsRTCSSetSyntaxCheck()
{
	CString		strFilePath;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[8]= {"RTCSSET"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nRTCSSet);

	SyntaxCheck(nType, rType, strFilePath);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsRTCSSetSyntaxCheck
//	Description :	This routine enables the menu option for syntax
//                  checking RTCS set files.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	pointer to Command UI object
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsRTCSSetSyntaxCheck(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	if (!bFolder)
	{
		if (nType == nRTCSSet)
		{
			if (IsDocModified(strFilePath, nType))
				pCmdUI->Enable(FALSE);
			else
				pCmdUI->Enable(TRUE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsUpdateFrame()
//	Description :	This routine updates a frame file.
//	Return :		void -	none
//	Parameters :	void -	none
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsUpdateFrame()
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;
	CString		strMsg;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nFrame);
	CGenUpdSheet	sheet(nUpdFrame, strFilePath);

	if (sheet.DoModal() == ID_WIZFINISH)
	{
		CSchedUpdUtil	updUtil;
		CWaitCursor		wait;

		updUtil.ScanFile(strFilePath);
		updUtil.ImgIMCEnabled((sheet.GetIMCSetPage())->GetUseImgIMC());
		updUtil.SndIMCEnabled((sheet.GetIMCSetPage())->GetUseSndIMC());
		updUtil.IMCSet((sheet.GetIMCSetPage())->GetIMC());
		updUtil.UpdateFrame(TRUE);

		// 1. Get a socket connection to OATS
		if (!updUtil.Connect())
			return;

		// 2. Update frame file
		if (updUtil.UpdateFrameTable())
			OnViewRefresh();

		updUtil.Disconnect();
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsUpdateFrame
//	Description :	This routine enables the menu option for updating
//                  schedules for frames.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	Pointer to a Command UI object
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsUpdateFrame(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;


	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	if (!bFolder)
	{
		if (nType == nFrame)
		{
			if (IsDocModified(strFilePath, nType))
				pCmdUI->Enable(FALSE);
			else
				pCmdUI->Enable(TRUE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsSchedUpd
//	Description :	This routine updates a schedule for frames
//                  and stars.
//	Return :		void -	none
//	Parameters :	void -	none
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsSchedUpd()
{
	CString			strFilePath;
	CString			strMsg;
	short int		nType = nInvalidType;
	BOOL			bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nSTOL);
	CGenUpdSheet	sheet(nUpdSTOL, strFilePath);

	if (sheet.DoModal() == ID_WIZFINISH)
	{
		CWaitCursor	wait;

		if( SchedUpd(&sheet, strFilePath) )
		{
			UpdateOpenDocuments();
			OnViewRefresh();
		}
		//SSGS-1156: Prompt for Schedule Distribution after Schedule generation/update.
		strMsg = _T("Would you like to distribute the new Schedule Package?");
		if (AfxMessageBox(strMsg, MB_YESNO) == IDYES)
		{	
			OnToolsFileDistributionWizard();	
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsSchedUpd
//	Description :	This routine enables the menu option for updating
//                  schedules for frames and stars.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	Pointer to a Command UI object
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsSchedUpd(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nSTOL)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::SchedUpd
//	Description :	This routine updates a schedule for frames
//                  and stars.
//	Return :		BOOL
//					TRUE	-	SUCCESS
//					FALSE	-	FAIL
//	Parameters :
//					CGenUpdSheet*	pointer to a CGenUpdSheet
//					CString			Name of the schedule file to be updated.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CMainFrame::SchedUpd(CGenUpdSheet*	sheet, CString strFilePath)
{
	CString			strExt;
	CString			strMsg;
	CSchedUpdUtil	updUtil;
	COXUNC			uncFile;
//	BOOL			bUseDay		= (sheet->GetOutputPage())->GetUseDay();
	BOOL			bStarUpdate	= (sheet->GetStarPage())->GetUse();
	BOOL			bFrameUpdate= (sheet->GetFramePage())->GetUse();
	BOOL			bRtcsUpdate	= (sheet->GetRTCSPage())->GetUse();
//	CString			strSchedFile= sheet->MakeFilepath(nFolderSTOL,nSTOL,FALSE,bUseDay);
	CString			strSchedFile= sheet->MakeFilepath(nFolderSTOL,nSTOL,FALSE);

	// try
	// {
		BOOL bGenStrLoad = FALSE;
		BOOL bGenFrmLoad = FALSE;

		if( bStarUpdate || bFrameUpdate || bRtcsUpdate )
		{
			uncFile = strFilePath;
			strMsg.Format(_T("Updating the Schedule File %s"), uncFile.File());
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

			updUtil.SchedOldFile(strFilePath);
			updUtil.SchedNewFile(strSchedFile);
			updUtil.IMCSet((sheet->GetIMCSetPage())->GetIMC());
			updUtil.ImgIMCEnabled((sheet->GetIMCSetPage())->GetUseImgIMC());
			updUtil.SndIMCEnabled((sheet->GetIMCSetPage())->GetUseSndIMC());

			// Make sure that the schedule is valid and
			// Open all of the necessary Schedule files
			if( !updUtil.CheckValid() || !updUtil.OpenSchedFiles() )
				return FALSE;

			if( bStarUpdate || bFrameUpdate )
			{
				// Get a socket connection to OATS
				if( !updUtil.Connect() )
					return FALSE;
			}

			if( bFrameUpdate )
			{
				updUtil.NewScan((sheet->GetFramePage())->GetCreateNew());
				updUtil.DoScan(TRUE);
				updUtil.SRSOFrame((sheet->GetSRSOPage())->GetSRSOFrame());

				if( (sheet->GetFramePage())->GetCreateNew() )
				{
					updUtil.ScanFile(sheet->MakeFilepath(nFolderFrame,nFrame,FALSE));
					updUtil.SectorFile((sheet->GetFramePage())->GetSectorFilename());
					updUtil.StaticFrame((sheet->GetFramePage())->GetUseStaticFrm());

					if( (sheet->GetFramePage())->GetUseSRSO() )
					{
						updUtil.DoSRSO(TRUE);
						updUtil.SRSOLabel((sheet->GetSRSOPage())->GetSRSOLabel());
						updUtil.SRSOPreDefined((sheet->GetSRSOPage())->GetUsePreDef());
						// updUtil.SRSOCoordType((sheet->GetSRSOPage())->GetUseLatLong() ? _T("SRSO Lat/Long") : _T("Line/Pixel"));
						updUtil.SRSOCoordType(_T("SRSO Lat/Long"));
						updUtil.SRSOCoordNS((sheet->GetSRSOPage())->GetSRSO_NS());
						updUtil.SRSOCoordEW((sheet->GetSRSOPage())->GetSRSO_EW());
						updUtil.SRSODuration((sheet->GetSRSOPage())->GetSRSODuration());
//						CString strTemp = (sheet->GetSRSOPage())->GetSRSODuration();
						updUtil.SRSORatio((sheet->GetSRSOPage())->GetSRSORatio());
					}

					if( (sheet->GetFramePage())->GetGen() )
						bGenFrmLoad = TRUE;
				}
				else
				{
					updUtil.StaticFrame(TRUE);
					updUtil.DoSRSO(FALSE);
					updUtil.ScanFile((sheet->GetFramePage())->GetFrameFilename());
				
					// Check if Special RSO update needs to be done
					if( (sheet->GetFramePage())->GetUseRSO())
					{
						CString m_strRSOFrameTable;
						updUtil.DoRSO(TRUE);
						//Check if RSO Frame in Registry is same as Frame Table supplied by user.
						m_strRSOFrameTable = (sheet->GetRSOPage())->GetRSOFrmName();
						if(m_strRSOFrameTable.Compare((sheet->GetFramePage())->GetFrameFilename())) 
						{
							strMsg.Format(_T("Frame File %s differs from RSO Frame file %s"), sheet->GetFramePage()->GetFrameFilename(), m_strRSOFrameTable);
							GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
							return FALSE;
						}
						updUtil.RSOCurLabel(sheet->GetRSOPage()->GetRSOCurLabel());
						updUtil.RSONewLabel(sheet->GetRSOPage()->GetRSONewLabel());
					}
				}

				// Open all of the necessary Frame files
				if( !updUtil.OpenFrameFiles() )
					return FALSE;

				// Scan through the schedule file and build up an
				// object list for each instrument.
				updUtil.CreateUpdateLists();

				// Process Scan Request(s)
				if( !updUtil.ProcessScanInfo() )
				{
					updUtil.CloseFiles();
					updUtil.DeleteFiles();
					updUtil.Disconnect();
					return FALSE;
				}
			}

			if( bStarUpdate )
			{
				updUtil.StarFile(sheet->MakeFilepath(nFolderStar,nStar,FALSE));
				updUtil.DoStar(TRUE);

				// Open all of the necessary star files
				if( !updUtil.OpenStarFiles() )
				{
					updUtil.Disconnect();
					return FALSE;
				}

				// Scan through the schedule file and build up an
				// object lists for each instrument.
				updUtil.CreateUpdateLists();

				// Process Star Request(s)
				if( !updUtil.ProcessStarInfo() )
				{
					updUtil.CloseFiles();
					updUtil.DeleteFiles();
					updUtil.Disconnect();
					return FALSE;
				}

				if( (sheet->GetStarPage())->GetGen() )
					bGenStrLoad = TRUE;
			}

			if( bStarUpdate || bFrameUpdate )
				updUtil.Disconnect();

			if( bRtcsUpdate )
			{
				updUtil.MapFile((sheet->GetRTCSPage())->GetMapFilename());
				updUtil.DoRTCS(TRUE);

				// Update RTCSs in Schedule
				if( !updUtil.UpdateScheduleRTCSs() )
				{
					updUtil.CloseFiles();
					updUtil.DeleteFiles();
					return FALSE;
				}
			}

			updUtil.CloseFiles();
			uncFile = strFilePath;
			strMsg.Format(_T("Updating completed for Schedule File %s"), uncFile.File());
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
		}
		else // Use original schedule if not updating
			strSchedFile = strFilePath;

		if( bGenFrmLoad )
		{
			// Generate Frame Load file after ALL UPDATING is completed
			CGenLoad Frame(updUtil.GetScanFilename(), CGenLoad::FRAME_LDTYPE);

			if( !Frame.Send_GenLD_Dir() )
			{
				updUtil.CloseFiles();
				updUtil.DeleteFiles();
				return FALSE;
			}
		}

		if( bGenStrLoad )
		{
			// Generate Star Load file after ALL UPDATING is completed
			CGenLoad Star(updUtil.GetStarFilename(), CGenLoad::STAR_LDTYPE);

			if( !Star.Send_GenLD_Dir() )
			{
				updUtil.CloseFiles();
				updUtil.DeleteFiles();
				return FALSE;
			}
		}

		if( (sheet->GetSchedPage())->GetVal() )
		{
			uncFile = strSchedFile;

			if( (uncFile.Extension()).IsEmpty() )
			{
				GetAppDocTemplate(nSTOL)->GetDocString(strExt, CDocTemplate::filterExt);
				strSchedFile += strExt;
			}

			char rType[5] = {"STOL"};

			// Validate updated schedule
			if( !SyntaxCheck(nSTOL, rType, strSchedFile) )
			{
				CFileException* pException = NULL;
				CTextFile		STOL;

				if( STOL.Load(strFilePath, CTextFile::RW, pException) )
				{
					STOL.ExtractProlog();
					STOL.WriteValidationStatus(FAIL, (sheet->GetSchedPage())->GetValLoc());

					if( !STOL.Close(CTextFile::WRITE, pException) )
					{
						strMsg.Format(_T("Error closing file %s :"), strFilePath);
						strMsg += STOL.ExceptionErrorText(pException);
						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					}
				}
				else
				{
					strMsg.Format(_T("Error loading file: %s :"), strFilePath);
					strMsg += STOL.ExceptionErrorText(pException);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				}

				UpdateOpenDocuments();
				return FALSE;
			}

			CSchedVal Sched(strSchedFile);
			Sched.SetRTCSSetFileName((sheet->GetSchedPage())->GetRTCSSetFilename());
			Sched.SetValRTCSProcsFlag((sheet->GetSchedPage())->GetRTCSVal());
			Sched.SetValLoc((sheet->GetSchedPage())->GetValLoc());
			Sched.Validate(CSchedVal::STOL_VAL);
			UpdateOpenDocuments();
		}

		// CMD level schedule
		if( (sheet->GetSchedPage())->GetGen() )
		{
			// File paths
			COXUNC  UNCMapPath(strSchedFile);
			COXUNC  UNCLisPath(strSchedFile);

			uncFile = strSchedFile;

			// Get the  extension.
			GetAppDocTemplate(nSTOLMap)->GetDocString(strExt, CDocTemplate::filterExt);
			UNCMapPath.File() = UNCMapPath.Base() + strExt;
			GetAppDocTemplate(nLis)->GetDocString(strExt, CDocTemplate::filterExt);
			UNCLisPath.File() = UNCLisPath.Base() + strExt;

			CSchedGen STOL(strSchedFile, CNewSchedFile::STOL);
			STOL.SetStolFileName(strSchedFile);
			STOL.SetLisFileName(UNCLisPath.Full());
			STOL.SetMapFileName(UNCMapPath.Full());

			if( !STOL.Gen_ProdFile() )
			{
				UpdateOpenDocuments();
				return FALSE;
			}
		}
	// }

	// ::TODO Need to fix this exception issue.

	/*

	catch( CException e )
	{
		TCHAR   szCause[255];
		CString strFormatted(_T("Error Updating Schedule!!!: "));

		e.GetErrorMessage(szCause, 255);
		strFormatted += szCause;
		GetAppOutputBar()->OutputToEvents(strFormatted, COutputBar::FATAL);
		return FALSE;
	}

	*/

	

	return TRUE;
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsGenerateLoadIMC
//	Description :	This routine enables the menu option for generating
//                  the upload files for an IMC set.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	pointer to Commnad UI object
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateToolsGenerateLoadIMC(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	if (!bFolder)
	{
		if (nType == nIMC)
		{
			if (IsDocModified(strFilePath, nType))
				pCmdUI->Enable(FALSE);
			else
				pCmdUI->Enable(TRUE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsGenerateLoadIMC()
//	Description :	This routine generates the upload files for IMC sets.
//	Return :		void -	none
//	Parameters :	void -	none
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnToolsGenerateLoadIMC()
{
	CString			strFilePath;
	short int		nType = nInvalidType;
	BOOL			bFolder = FALSE;
	CString			strMsg;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nIMC);

	strMsg.Format(_T("Generating the IMC Upload Files for %s"),strFilePath);
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	CGenLoad IMC(strFilePath, CGenLoad::IMC_LDTYPE);
	IMC.Send_GenLD_Dir();
	OnViewRefresh();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsFileDistributionWizard()
//	Description :	This routine distributes the files to the different
//                  GTACS servers.
//	Return :		void	-	none
//	Parameters :	void	-	none
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsFileDistributionWizard()
{
	CString strMsg;

	// Check if we have open files in the MDI window.
	if( GetActiveFrame() != this )
	{
		// Check if only CLS files are open. If not, display an error. Otherwise, let it continue.
		POSITION template_pos = AfxGetApp()->GetFirstDocTemplatePosition();
		while(NULL != template_pos) 
		{
			CDocTemplate* pDocTemplate = AfxGetApp()->GetNextDocTemplate(template_pos);
			if(NULL != pDocTemplate) 
			{
				POSITION document_pos = pDocTemplate->GetFirstDocPosition();
				while(NULL != document_pos) 
				{
					CDocument* pDoc_to_check = pDocTemplate->GetNextDoc(document_pos);
					if(!pDoc_to_check->IsKindOf(RUNTIME_CLASS(CCLSDoc))) 
					{
						strMsg.LoadString(IDS_DIST_ACTIVEDOC_ERROR);
						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
						return;
					}
				}
			}
		}
	}

	strMsg.LoadString(IDS_DIST_START);
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	CDistributeSheet	distDlg;
	CDWordArray			dWordArray;
	CStringList			listDest;
	CStringList			listFile;
	CString				strPath;

	distDlg.FileSizeList(&dWordArray);
	distDlg.DestinationList(&listDest);
	distDlg.FileList(&listFile);
	distDlg.SourcePath(&strPath);

	int result = distDlg.DoModal();

	if( result == ID_WIZFINISH )
	{
		CDistributionFinish dlg;
		dlg.FileSizeList(&dWordArray);
		dlg.DestinationList(&listDest);
		dlg.FileList(&listFile);
		dlg.SourcePath(&strPath);
		dlg.DoModal();
		dWordArray.RemoveAll();
		listDest.RemoveAll();
		listFile.RemoveAll();
		strMsg.LoadString(IDS_DIST_END);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	}
	else if( result != IDCANCEL )
	{
		strMsg.LoadString(IDS_DIST_END_ERROR);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	}
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsFileDistributionWizard
//	Description :	This routine enables the menu option for the
//                  file distribution wizard.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	Pointer to Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateToolsFileDistributionWizard(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsGenerateSTOLProc()
//	Description :	This routine generates a MASTEWr schedule from
//                  a CLS file.
//	Return :		void	-	none
//	Parameters :	void	-	none
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnToolsGenerateSTOLProc()
{
	CString			strFilePath;
	short int		nType = nInvalidType;
	BOOL			bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nCLS);

	CGenUpdSheet	sheet(nGenSTOL, strFilePath);

	if (sheet.DoModal() == ID_WIZFINISH)
	{
		CString strExt;
		CString strStolPath(sheet.MakeFilepath(nFolderSTOL,nSTOL,FALSE,(sheet.GetOutputPage())->GetUseDay()));
		COXUNC	uncStolPath(strStolPath);

		if((uncStolPath.Extension()).IsEmpty())
		{
			GetAppDocTemplate(nSTOL)->GetDocString(strExt, CDocTemplate::filterExt);
			strStolPath += strExt;
		}

		CSchedGen SchedGen(strFilePath, CNewSchedFile::CLS);
		SchedGen.SetStolFileName(strStolPath);
		SchedGen.SetDayOfYear((sheet.GetCLSPage())->GetDOY());
		SchedGen.SetYear((sheet.GetCLSPage())->GetYear());
		SchedGen.Gen_ProdFile();
		TRACE(_T("Generate STOL procedure was checked!"));
		OnViewRefresh();
   }
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsGenerateSTOLProc
//	Description :	This routine enables the menu option to generate
//                  STOL procedures from a CLS file.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	pointer to Command UI object
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateToolsGenerateSTOLProc(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	if (!bFolder)
	{
		if (nType == nCLS)
		{
			if (IsDocModified(strFilePath, nType))
				pCmdUI->Enable(FALSE);
			else
				pCmdUI->Enable(TRUE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
	else
		pCmdUI->Enable(FALSE);
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsSearchForTime()
//	Description :	This routine searches the STOL map file for a specific
//                  time tag.
//	Return :		void	-	none
//	Parameters :	void	-	none
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnToolsSearchForTime()
{
	CMapSearchDlg	dlg;
	dlg.m_eSearchType = CMapSearchDlg::SEARCH_PAUSE;
	if (dlg.DoModal())
	{
		CString		strFilePath;
		short int	nType = nInvalidType;
		BOOL		bFolder = FALSE;
		CSTOLMapDoc* pDoc = (CSTOLMapDoc*)GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (nType == nSTOLMap)
		{
			int		nLineToSelect;
			if (dlg.m_eSearchType == CMapSearchDlg::SEARCH_EXACT)
				nLineToSelect = -1;
			else
				nLineToSelect = 0;

			BOOL	bDone = FALSE;
			int		nSize = pDoc->m_strDataRecord.GetSize();
			int		nCurLine = 0;
			while ((nCurLine<nSize) && !bDone)
			{
				CParseSTOLMap cParseSTOLMap;
				cParseSTOLMap.SetData(pDoc->m_strDataRecord[nCurLine]);
				cParseSTOLMap.ParseData();
				switch (dlg.m_eSearchType)
				{
					case CMapSearchDlg::SEARCH_PAUSE :
					{
						if (dlg.m_strSearchTimeEdit.CompareNoCase(cParseSTOLMap.m_strExecTime) >= 0)
						{
							if (pDoc->IsBreak(nCurLine))
								nLineToSelect = nCurLine;
							nCurLine++;
						}
						else
							bDone = TRUE;
						break;
					}
					case CMapSearchDlg::SEARCH_CLOSEST :
					{
						if (dlg.m_strSearchTimeEdit.CompareNoCase(cParseSTOLMap.m_strExecTime) >= 0)
						{
							nLineToSelect = nCurLine;
							nCurLine++;
						}
						else
							bDone = TRUE;
						break;
					}
					case CMapSearchDlg::SEARCH_EXACT :
					{
						if (dlg.m_strSearchTimeEdit.CompareNoCase(cParseSTOLMap.GetDataExecTime()) == 0)
						{
							nLineToSelect = nCurLine;
							bDone = TRUE;
						}
						else
							nCurLine++;
						break;
					}
				}
			}
			if (nLineToSelect == -1)
				AfxMessageBox(_T("Could not find the specified time."));
			else
			{
				POSITION pos = pDoc->GetFirstViewPosition();
				while (pos != NULL)
				{
					CListView* pView = (CListView*) pDoc->GetNextView(pos);
					CListCtrl&	theCtrl = pView->GetListCtrl();

					theCtrl.EnsureVisible(nLineToSelect, FALSE);
					theCtrl.SetItemState(nLineToSelect, LVIS_SELECTED, LVIS_SELECTED);
				}
			}
		}
	}
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsSearchForTime
//	Description :	This routine enbales the menu option to search for a
//                  specific time tag.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	Pointer to a Commnad UI object
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMainFrame::OnUpdateToolsSearchForTime(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	if (nType == nSTOLMap)
		pCmdUI->Enable(TRUE);
	else
		pCmdUI->Enable(FALSE);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::SyntaxCheck()
//	Description :	This routine syntax checks the specified file.
//	Return :		BOOL
//					TRUE	-	SUCCESS
//					FALSE	-	FAIL
//	Parameters :
//					int 	-	type of file to be syntax checked
//					char*	-	resource type to be used
//					CString	-	File name
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CMainFrame::SyntaxCheck(int nType, char* rType, CString strFilePath)
{
	if( strFilePath.IsEmpty() )
		return FALSE;

	CFileException	ex;
	CTextFile		tf;
	CString			strMsg;
	COXUNC			uncFile(strFilePath);

	if( !tf.Load(strFilePath, CTextFile::RO, &ex) )
	{
		strMsg.Format(_T("Syntax Check: %s %s"), tf.ExceptionErrorText(&ex), uncFile.File());
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	tf.Close(CTextFile::NOWRITE, &ex);

	strMsg.Format(_T("Syntax Checking File %s"), uncFile.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	USES_CONVERSION;
	CWaitCursor			wait;
	// unsigned long	rh			= (unsigned long)::AfxGetResourceHandle();
	HINSTANCE			rh			= ::AfxGetResourceHandle();
	BOOL				bPass		= FALSE;
	int					nErrCnt		= 0;
	LPSTR				lpszAscii	= T2A((LPTSTR)((LPCTSTR)strFilePath));

	// SSLexTable			lexTable(rh,	"SSLexTable",	rType);
	// SSYaccTable			yaccTable(rh,	"SSYaccTable",	rType);
//	SSLexTable			lexTable(rh,	"SSLexTable",	rType);
//	SSYaccTable			yaccTable(rh,	"SSYaccTable",	rType);

//	SSLexFileConsumer	consumer(lpszAscii);

	GetAppOutputBar()->InitBuild(_T("--------------------Syntax Check--------------------"), strFilePath);

	try
	{
		switch(nType)
		{
			case nPaceCmd:
			{
				PcmdYaccClass parser("res\\Pcmd.cgt");
				parser.loadSource(lpszAscii);
				parser.filename(strFilePath);
				parser.errorCount(&nErrCnt);
				parser.parse();
				break;
			}

			case nRTCSCmd:
			case nSTOLCmd:
			{
				RcmdYaccClass		parser("res\\Rcmd.cgt");
				parser.loadSource(lpszAscii);
				parser.filename(strFilePath);
				parser.errorCount(&nErrCnt);
				parser.parse();
				break;
			}

			case nRTCSSet:
			{
				RtcsSetYaccClass	parser("res\\RtcsSet.cgt");
				parser.loadSource(lpszAscii);
				parser.filename(strFilePath);
				parser.errorCount(&nErrCnt);
				parser.parse();
				break;
			}

			case nRTCS:
			{
				RtcsYaccClass parser("res\\Rtcs.cgt");
				parser.loadSource(lpszAscii);
				parser.filename(strFilePath);
				parser.errorCount(&nErrCnt);
				parser.parse();
				break;
			}

			case nSTOL:
			{
				RulesYaccClass		parser("res\\Rules.cgt");
				parser.loadSource(lpszAscii);
				parser.filename(strFilePath);
				parser.errorCount(&nErrCnt);
				parser.parse();
				break;
			}

			case nCLS:
			{
				ClsYaccClass parser("res\\Cls.cgt");
				parser.loadSource(lpszAscii);
				parser.filename(strFilePath);
				parser.errorCount(&nErrCnt);
				parser.parse();
				break;
			}

			default:
				break;
		}
	}

	catch(SSException z_except)
	{
//		GetAppOutputBar()->OutputToBuild(z_except.text(), COutputBar::FATAL);
	}

	strMsg.Format(_T("%s - %d error(s)"), uncFile.File(), nErrCnt);
	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::INFO);

	if( nErrCnt == 0 )
	{
		strMsg.Format(_T("Syntax Check passed for File %s"), uncFile.File());
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
		bPass = TRUE;
	}
	else
	{
		strMsg.Format(_T("Syntax Check failed for file %s"), uncFile.File());
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
	}

	return bPass;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsPaceCmdSyntaxCheck()
//	Description :	This routine syntax checks the command pacing file.
//	Return :		void	-	none
//	Parameters :	void	-	none
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsPaceCmdSyntaxCheck()
{
	CString		strFilePath;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[5]= {"PACE"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nPaceCmd);

	SyntaxCheck(nType, rType, strFilePath);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsPaceCmdSyntaxCheck
//	Description :	This routine enables the menu option to syntax check
//                  a command pacing file.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	pointer to a CCmdUI object
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsPaceCmdSyntaxCheck(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nPaceCmd)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsRTCSCmdSyntaxCheck()
//	Description :	This routine syntax checks the RTCS command
//                  restriction file.
//	Return :		void	-	none
//	Parameters :	void	-	none
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsRTCSCmdSyntaxCheck()
{
	CString		strFilePath;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[9]= {"RESTRICT"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nRTCSCmd);

	SyntaxCheck(nType, rType, strFilePath);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsRTCSCmdSyntaxCheck
//	Description :	This routine enables the menu options for syntax
//                  checking a RTCS command restriction file.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	pointer to CCmdUI object
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsRTCSCmdSyntaxCheck(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nRTCSCmd)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnToolsStolCmdSyntaxCheck()
//	Description :	This routine syntax checks a STOL command restriction
//                  file.
//	Return :		void	-	none
//	Parameters :	void	-	none
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnToolsStolCmdSyntaxCheck()
{
	CString		strFilePath;
	short int	nType	= nInvalidType;
	BOOL		bFolder = FALSE;
	char		rType[9]= {"RESTRICT"};

	GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
	ASSERT(nType == nSTOLCmd);

	SyntaxCheck(nType, rType, strFilePath);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/12/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMainFrame::OnUpdateToolsStolCmdSyntaxCheck
//	Description :	This routine enables the menu option to syntax
//                  check the STOL command restriction file.
//	Return :		void	-	none
//	Parameters :	CCmdUI*	-	pointer to CCmdUI object
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CMainFrame::OnUpdateToolsStolCmdSyntaxCheck(CCmdUI* pCmdUI)
{
	CString		strFilePath;
	short int	nType = nInvalidType;
	BOOL		bFolder = FALSE;

	if (!((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)
		pCmdUI->Enable(FALSE);
	else
	{
		GetInFocusDocInfo(&strFilePath, &nType, &bFolder);
		if (!bFolder)
		{
			if (nType == nSTOLCmd)
			{
				if (IsDocModified(strFilePath, nType))
					pCmdUI->Enable(FALSE);
				else
					pCmdUI->Enable(TRUE);
			}
			else
				pCmdUI->Enable(FALSE);
		}
		else
			pCmdUI->Enable(FALSE);
	}
}
