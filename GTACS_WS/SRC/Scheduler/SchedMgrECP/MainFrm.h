// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__A464C532_D502_11D4_800D_00609704053C__INCLUDED_)
#define AFX_MAINFRM_H__A464C532_D502_11D4_800D_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "WorkspaceBar.h"
#include "OutputBar.h"
#include "GenUpdSheet.h"

#define CMDIFrameWnd CBCGPMDIFrameWnd

class CMainFrame : public CMDIFrameWnd
{
	DECLARE_DYNAMIC(CMainFrame)
public:
	CMainFrame();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	BOOL SyntaxCheck(int, char*, CString);
	BOOL SchedUpd(CGenUpdSheet*, CString);
protected:  // control bar embedded members
	CBCGPMenuBar	m_wndMenuBar;
	CStatusBar		m_wndStatusBar;
	CBCGPToolBar	m_wndToolBar;

	CBCGPToolBarImages	m_UserImages;
	HWND				m_hwndClickedTabWnd;
// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnToolsLoadDB();
	afx_msg void OnUpdateToolsLoadDB(CCmdUI* pCmdUI);
	afx_msg void OnToolsAdministrationMode();
	afx_msg void OnUpdateToolsAdministrationMode(CCmdUI* pCmdUI);
	afx_msg void OnToolsWorkspaceOptions();
	afx_msg void OnUpdateToolsWorkspaceOptions(CCmdUI* pCmdUI);
	afx_msg void OnToolsProperties();
	afx_msg void OnUpdateToolsProperties(CCmdUI* pCmdUI);
	afx_msg void OnToolsGenerateLoadFrame();
	afx_msg void OnUpdateToolsGenerateLoadFrame(CCmdUI* pCmdUI);
	afx_msg void OnToolsGenerateLoadStar();
	afx_msg void OnUpdateToolsGenerateLoadStar(CCmdUI* pCmdUI);
	afx_msg void OnToolsSTOLVal();
	afx_msg void OnUpdateToolsSTOLVal(CCmdUI* pCmdUI);
	afx_msg void OnToolsSTOLGenLoad();
	afx_msg void OnUpdateToolsSTOLGenLoad(CCmdUI* pCmdUI);
	afx_msg void OnToolsSchedMake();
	afx_msg void OnUpdateToolsSchedMake(CCmdUI* pCmdUI);
	afx_msg void OnMditabsIcons();
	afx_msg void OnUpdateMditabsIcons(CCmdUI* pCmdUI);
	afx_msg void OnMditabsTop();
	afx_msg void OnUpdateMditabsTop(CCmdUI* pCmdUI);
	afx_msg void OnMditabsBottom();
	afx_msg void OnUpdateMditabsBottom(CCmdUI* pCmdUI);
	afx_msg void OnViewMdiTabs();
	afx_msg void OnUpdateViewMdiTabs(CCmdUI* pCmdUI);
	afx_msg void OnMditabsClose();
	afx_msg void OnUpdateMditabsClose(CCmdUI* pCmdUI);
	afx_msg void OnFileNew();
	afx_msg void OnUpdateFileNew(CCmdUI* pCmdUI);
	afx_msg void OnViewRefresh();
	afx_msg void OnUpdateViewRefresh(CCmdUI* pCmdUI);
	afx_msg void OnViewShowExt();
	afx_msg void OnUpdateViewShowExt(CCmdUI* pCmdUI);
	afx_msg void OnViewSortByFilename();
	afx_msg void OnUpdateViewSortByFilename(CCmdUI* pCmdUI);
	afx_msg void OnViewSortByExtension();
	afx_msg void OnUpdateViewSortByExtension(CCmdUI* pCmdUI);
	afx_msg void OnToolsCLSVal();
	afx_msg void OnUpdateToolsCLSVal(CCmdUI* pCmdUI);
	afx_msg void OnToolsOATSRequestIMCSet();
	afx_msg void OnUpdateToolsOATSRequestIMCSet(CCmdUI* pCmdUI);
	afx_msg void OnToolsOutputEventCopy();
	afx_msg void OnUpdateToolsOutputEventCopy(CCmdUI* pCmdUI);
	afx_msg void OnToolsOutputEventSaveAs();
	afx_msg void OnUpdateToolsOutputEventSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnToolsOutputBuildCopy();
	afx_msg void OnUpdateToolsOutputBuildCopy(CCmdUI* pCmdUI);
	afx_msg void OnToolsOutputBuildSaveAs();
	afx_msg void OnUpdateToolsOutputBuildSaveAs(CCmdUI* pCmdUI);
	afx_msg void OnToolsOutputBuildGotoError();
	afx_msg void OnUpdateToolsOutputBuildGotoError(CCmdUI* pCmdUI);
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnToolsOutputBuildClear();
	afx_msg void OnUpdateToolsOutputBuildClear(CCmdUI* pCmdUI);
	afx_msg void OnToolsOutputEventClear();
	afx_msg void OnUpdateToolsOutputEventClear(CCmdUI* pCmdUI);
	afx_msg void OnToolsSchedSyntaxCheck();
	afx_msg void OnUpdateToolsSchedSyntaxCheck(CCmdUI* pCmdUI);
	afx_msg void OnViewMute();
	afx_msg void OnUpdateViewMute(CCmdUI* pCmdUI);
	afx_msg void OnToolsOATSGenerateEclipseReport();
	afx_msg void OnUpdateToolsOATSGenerateEclipseReport(CCmdUI* pCmdUI);
	afx_msg void OnToolsOATSIMCRequestIMCSFCS();
	afx_msg void OnUpdateToolsOATSIMCRequestIMCSFCS(CCmdUI* pCmdUI);
	afx_msg void OnToolsOutputEventPrint();
	afx_msg void OnUpdateToolsOutputEventPrint(CCmdUI* pCmdUI);
	afx_msg void OnToolsOutputBuildPrint();
	afx_msg void OnUpdateToolsOutputBuildPrint(CCmdUI* pCmdUI);
	afx_msg void OnToolsGenerateLoadRTCS();
	afx_msg void OnUpdateToolsGenerateLoadRTCS(CCmdUI* pCmdUI);
	afx_msg void OnToolsRTCSVal();
	afx_msg void OnUpdateToolsRTCSVal(CCmdUI* pCmdUI);
	afx_msg void OnToolsRTCSSetVal();
	afx_msg void OnUpdateToolsRTCSSetVal(CCmdUI* pCmdUI);
	afx_msg void OnToolsRTCSSyntaxCheck();
	afx_msg void OnUpdateToolsRTCSSyntaxCheck(CCmdUI* pCmdUI);
	afx_msg void OnToolsRTCSSetSyntaxCheck();
	afx_msg void OnUpdateToolsRTCSSetSyntaxCheck(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToolsCLSSyntaxCheck(CCmdUI* pCmdUI);
	afx_msg void OnToolsUpdateFrame();
	afx_msg void OnUpdateToolsUpdateFrame(CCmdUI* pCmdUI);
	afx_msg void OnToolsSchedUpd();
	afx_msg void OnUpdateToolsSchedUpd(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToolsGenerateLoadIMC(CCmdUI* pCmdUI);
	afx_msg void OnToolsGenerateLoadIMC();
	afx_msg void OnToolsGenerateSTOLProc();
	afx_msg void OnUpdateToolsGenerateSTOLProc(CCmdUI* pCmdUI);
	afx_msg void OnToolsFileDistributionWizard();
	afx_msg void OnUpdateToolsFileDistributionWizard(CCmdUI* pCmdUI);
	afx_msg void OnToolsSearchForTime();
	afx_msg void OnUpdateToolsSearchForTime(CCmdUI* pCmdUI);
	afx_msg void OnToolsCLSSyntaxCheck();
	afx_msg void OnToolsPaceCmdSyntaxCheck();
	afx_msg void OnUpdateToolsPaceCmdSyntaxCheck(CCmdUI* pCmdUI);
	afx_msg void OnToolsRTCSCmdSyntaxCheck();
	afx_msg void OnUpdateToolsRTCSCmdSyntaxCheck(CCmdUI* pCmdUI);
	afx_msg void OnToolsOatsGenerateFrameIntrusionReport();
	afx_msg void OnUpdateToolsOatsGenerateFrameIntrusionReport(CCmdUI* pCmdUI);
	afx_msg void OnToolsOatsGenerateSensorIntrusionReport();
	afx_msg void OnUpdateToolsOatsGenerateSensorIntrusionReport(CCmdUI* pCmdUI);
	afx_msg void OnToolsStolCmdSyntaxCheck();
	afx_msg void OnUpdateToolsStolCmdSyntaxCheck(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg void OnViewCustomize();
	afx_msg LRESULT OnToolbarReset(WPARAM,LPARAM);
	BOOL OnToolsViewUserToolbar (UINT id);
	void OnUpdateToolsViewUserToolbar (CCmdUI* pCmdUI);
	afx_msg LRESULT OnToolbarContextMenu(WPARAM,LPARAM);
	afx_msg void OnViewWorkspace();
	afx_msg void OnUpdateViewWorkspace(CCmdUI* pCmdUI);
	afx_msg void OnViewOutput();
	afx_msg void OnUpdateViewOutput(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnShowPopupMenu (CBCGPPopupMenu* pMenuPopup);
	CDocument* GetInFocusDocInfo(CString* pstrFilePath, short int* pnType, BOOL* pbFolder);	BOOL IsDocModified(CString strFilePath, short int nType);
	void OutputCopy(CListCtrl* pList);
	void OutputGetText(CListCtrl* pList, CString* pstrText);
	void OutputSaveAs(CListCtrl* pList, CString strFilename);
	void UpdateOpenDocuments();
private:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__A464C532_D502_11D4_800D_00609704053C__INCLUDED_)
