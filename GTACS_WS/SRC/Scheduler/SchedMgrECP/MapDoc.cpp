/***************************
//////////////////////////////////////////////////////////////////////
// MapDoc.cpp : implementation of the CMapExDoc class             //
//                                                                  //
// (c) 2001 Frederick J. Shaw                                       // 
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
***************************/
/////////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "MainFrm.h"
#include "MapDoc.h"
#include "Parse.h"
#include "MapView.h"
#include "CmnHdr.h"
#include "NewDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNCREATE(CMapExDoc, CDocument)

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw	Date : 3/16/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMapExDoc::CMapExDoc()
//	Description :	Class Constructor
//	Return :		constructor	-	none
//	Parameters :	void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
CMapExDoc::CMapExDoc()
{
	m_cTimeLastMod	= 0;
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw	Date : 3/16/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMapExDoc::~CMapExDoc()
//	Description :	Class Destructor
//	Return :		destructor	-	none
//	Parameters :	void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
CMapExDoc::~CMapExDoc(){}


BEGIN_MESSAGE_MAP(CMapExDoc, CDocument)
	//{{AFX_MSG_MAP(CMapExDoc)
	ON_COMMAND(ID_FILE_NEW, OnFileNew)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEW, OnUpdateFileNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
void CMapExDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMapExDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw			Date : 3/16/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMapExDoc::OnOpenDocument
//	Description :	This routines just sets the internal file time
//	Return :
//		BOOL		-	TRUE if ok, otherwise FALSE
//	Parameters :
//		LPCTSTR lpszPathName	-	Name of the file to open.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
BOOL CMapExDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	// Update the document's last modification time
	CFileFind finder;
	if (finder.FindFile(lpszPathName))
	{
		finder.FindNextFile();
		return finder.GetLastWriteTime(m_cTimeLastMod);
	}
	else
		return FALSE;
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw			Date : 3/16/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMapExDoc::OnFileNew()
//	Description :	This routines creates a new map file.
//	Return :
//					void	-	none. 
//	Parameters :
//					void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CMapExDoc::OnFileNew() 
{
	CNewDialog dlg;
	if (dlg.DoModal() == IDOK)
	{
		//Open the first document subtype of the type selected
		GetAppDocTemplate(dlg.m_nTypeSelected)->OpenDocumentFile(NULL);	
	}
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw			Date : 3/16/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMapExDoc::OnFileNew()
//	Description :	This routines enables a the file new menu option.
//	Return :
//					void	-	none. 
//	Parameters :
//					CCmdUI* -	Pointer to a Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CMapExDoc::OnUpdateFileNew(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);
}

IMPLEMENT_DYNCREATE(CSTOLMapDoc, CMapExDoc)

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLMapDoc::CSTOLMapDoc()
//	Description :	Class constructor
//	Return :		
//		constructor		-	none.
//	Parameters :	void	-	 none.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***********************************/
CSTOLMapDoc::CSTOLMapDoc(){}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 2/1/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLMapDoc::~CSTOLMapDoc()
//	Description :	Class destructor
//	Return :		
//		destructor		-	none	
//	Parameters :	void	-	none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***********************************/
CSTOLMapDoc::~CSTOLMapDoc(){}

BEGIN_MESSAGE_MAP(CSTOLMapDoc, CMapExDoc)
	//{{AFX_MSG_MAP(CSTOLMapDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG

void CSTOLMapDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSTOLMapDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw			Date : 3/16/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLMapDoc::OnOpenDocument
//	Description :	This routines enables a the file new menu option.
//	Return :		BOOL
//					TRUE	-	SUCCESS
//					FALSE	-	FAIL
//					 
//	Parameters :
//					LPCTSTR -	pathname of the file to be openned.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
BOOL CSTOLMapDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	CWaitCursor		waitCursor;
	BOOL			bStatus = TRUE;
	CTextFile		dataFile;
	CString			strError;
	CParseSTOLMap	cParseSTOLMap;

	CString strTitle (_T("Reading in a STOL Map File..."));
	GetAppOutputBar()->InitBuild(strTitle, lpszPathName);

	// Open the document
	if (bStatus)
	{
		CFileException cFE;
		m_strDataRecord.RemoveAll();
		if (!dataFile.Load(lpszPathName, CTextFile::RO, &cFE))
		{
			strError = CTextFile::ExceptionErrorText(&cFE);
			GetAppOutputBar()->OutputToBuild(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}
	
	// Make sure that there are at least 2 header records in the file and if OK
	// read in the header records
	int nNumRecords;
	if (bStatus)
	{
		nNumRecords = dataFile.GetLineCount();
		cParseSTOLMap.m_nNumObj = nNumRecords;
		if (nNumRecords < 2)
		{
			strError.Format (IDS_OPEN_HEADER_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToBuild(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
			cParseSTOLMap.SetHdr(dataFile.GetTextAt(0), dataFile.GetTextAt(1));
	}
	
	// Parse the header records
	if (bStatus)
	{
		if (!cParseSTOLMap.ParseHdr())
		{
			strError.Format (IDS_INV_HEADER_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToBuild(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
		{
			m_fVersion      = cParseSTOLMap.m_fVersion;
			m_tModTime      = cParseSTOLMap.m_tModTime;
			m_strModUser    = cParseSTOLMap.m_strModUser;
		}
	}

	// Make sure that the number of objects listed in the header is less than the number
	// of records in the file
	if (bStatus)
	{
		m_nNumObj = cParseSTOLMap.m_nNumObj;
		if (m_nNumObj > dataFile.GetLineCount()-2)
		{
			strError.Format (IDS_BAD_MAPHEADER_NUM, m_nNumObj, m_strDataRecord.GetSize());
			GetAppOutputBar()->OutputToBuild(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}
	
	// Read the data from the file into the document
	if (bStatus)
	{
		int nCurObj = 0;
		m_strDataRecord.SetSize(m_nNumObj);
		m_nHasData.SetSize(m_nNumObj);
//		int nTemp = dataFile.GetLineCount();

		while ((bStatus) && (nCurObj < m_nNumObj))
		{
			cParseSTOLMap.SetData(dataFile.GetTextAt(nCurObj+2));
			if (!cParseSTOLMap.ParseData())
			{
				strError.Format (IDS_BAD_DATA, nCurObj+1);
				GetAppOutputBar()->OutputToBuild(strError, COutputBar::FATAL);
				bStatus = FALSE;
			}
			else
			{
				m_strDataRecord[nCurObj] = dataFile.GetTextAt(nCurObj+2);
				m_nHasData[nCurObj] = TRUE;
			}
			nCurObj++;
		}
	}
 
	// Close the file, all of the data is now local
	if (bStatus)
	{
		CFileException cFE;
		dataFile.Close(&cFE);
	}

	// Call the parent's OnOpendocument
	if (bStatus)
	{
		bStatus = CMapExDoc::OnOpenDocument(lpszPathName);
	}
	
	return bStatus;
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw			Date : 3/16/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLMapDoc::IsBreak
//	Description :	This routines flags lines that are timetagged commands.
//	Return :		BOOL
//					TRUE	-	Yes
//					FALSE	-	No
//					 
//	Parameters :	
//					int		-	Line in map file to be checked.	
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
BOOL CSTOLMapDoc::IsBreak(int nIndex)
{
	BOOL	bRtn = FALSE;
	CParseSTOLMap	cParseSTOLMap;
	cParseSTOLMap.SetData(m_strDataRecord[nIndex]);
	cParseSTOLMap.ParseData();

	// The line must contain one of the various "time" or "pause" commands and
	// The line must point to a line number in the schedule that is positive.
	// Positive line numbers mean that the line is part of the top level proc and
	// not part of a nested proc.
	CString strCmdLine = cParseSTOLMap.GetDataSchedCmd();
	if ((strCmdLine.Find(ctstrCMDWAITDOY) != -1) ||
		(strCmdLine.Find(ctstrCMDWAITTOD) != -1) ||
		(strCmdLine.Find(ctstrCMDPAUSE) != -1))
	{
		if (atoi((const char *)(LPCTSTR)cParseSTOLMap.GetDataSchedLine()) > 0)
		{
			bRtn = TRUE;
		}
	}

	return bRtn;
}

IMPLEMENT_DYNCREATE(CRTCSMapDoc, CMapExDoc)

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw			Date : 3/16/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSMapDoc::CRTCSMapDoc()
//	Description :	CRTCSMapDoc Constructor
//	Return :		void none
//					
//	Parameters :	void none	
//						
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
CRTCSMapDoc::CRTCSMapDoc(){}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw			Date : 3/16/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSMapDoc::~CRTCSMapDoc()
//	Description :	Destructor
//	Return :		void none
//					
//	Parameters :	void none
//						
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
CRTCSMapDoc::~CRTCSMapDoc(){}

BEGIN_MESSAGE_MAP(CRTCSMapDoc, CMapExDoc)
	//{{AFX_MSG_MAP(CRTCSMapDoc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG

void CRTCSMapDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CRTCSMapDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw			Date : 3/16/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSMapDoc::OnOpenDocument
//	Description :	This routine opens the file.
//	Return :		BOOL
//					TRUE	-	SUCCESS
//					FALSE	-	FAIL
//					
//	Parameters :	LPCTSTR -	Pathname of file to open.
//						
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
BOOL CRTCSMapDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	CWaitCursor		waitCursor;
	BOOL			bStatus = TRUE;
	CTextFile		dataFile;
	CString			strError;
	CParseRTCSMap	cParseRTCSMap;

	CString strTitle (_T("Reading in a RTCS Map File..."));
	GetAppOutputBar()->InitBuild(strTitle, lpszPathName);

	// Open the document
	if (bStatus)
	{
		CFileException cFE;
		m_strDataRecord.RemoveAll();
		if (!dataFile.Load(lpszPathName, CTextFile::RO, &cFE))
		{
			strError = CTextFile::ExceptionErrorText(&cFE);
			GetAppOutputBar()->OutputToBuild(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}
	
	// Make sure that there are at least 2 header records in the file and if OK
	// read in the header records
	int nNumRecords;
	if (bStatus)
	{
		nNumRecords = dataFile.GetLineCount();
		cParseRTCSMap.m_nNumObj = nNumRecords;
		if (nNumRecords < 2)
		{
			strError.Format (IDS_OPEN_HEADER_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToBuild(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
			cParseRTCSMap.SetHdr(dataFile.GetTextAt(0), dataFile.GetTextAt(1));
	}
	
	// Parse the header records
	if (bStatus)
	{
		if (!cParseRTCSMap.ParseHdr())
		{
			strError.Format (IDS_INV_HEADER_ERROR, lpszPathName);
			GetAppOutputBar()->OutputToBuild(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
		else
		{
			m_fVersion      = cParseRTCSMap.m_fVersion;
			m_tModTime      = cParseRTCSMap.m_tModTime;
			m_strModUser    = cParseRTCSMap.m_strModUser;
		}
	}

	// Make sure that the number of objects listed in the header is less than the number
	// of records in the file
	if (bStatus)
	{
		m_nNumObj = cParseRTCSMap.m_nNumObj;
		if (m_nNumObj > dataFile.GetLineCount()-2)
		{
			strError.Format (IDS_BAD_MAPHEADER_NUM, m_nNumObj, m_strDataRecord.GetSize());
			GetAppOutputBar()->OutputToBuild(strError, COutputBar::FATAL);
			bStatus = FALSE;
		}
	}
	
	// Read the data from the file into the document
	if (bStatus)
	{
		int nCurObj = 0;
		m_strDataRecord.SetSize(m_nNumObj);
		m_nHasData.SetSize(m_nNumObj);
//		int nTemp = dataFile.GetLineCount();

		while ((bStatus) && (nCurObj < m_nNumObj))
		{
			cParseRTCSMap.SetData(dataFile.GetTextAt(nCurObj+2));
			if (!cParseRTCSMap.ParseData())
			{
				strError.Format (IDS_BAD_DATA, nCurObj+1);
				GetAppOutputBar()->OutputToBuild(strError, COutputBar::FATAL);
				bStatus = FALSE;
			}
			else
			{
				m_strDataRecord[nCurObj] = dataFile.GetTextAt(nCurObj+2);
				m_nHasData[nCurObj] = TRUE;
			}
			nCurObj++;
		}
	}
 
	// Close the file, all of the data is now local
	if (bStatus)
	{
		CFileException cFE;
		dataFile.Close(&cFE);
	}

	// Call the parent's OnOpendocument
	if (bStatus)
	{
		bStatus = CMapExDoc::OnOpenDocument(lpszPathName);
	}
	
	return bStatus;
}

