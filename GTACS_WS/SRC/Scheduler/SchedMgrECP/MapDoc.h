/////////////////////////////////////////////////////////////////////////////
#if !defined(AFX_MAPDOC_H__CC1CD226_F7F7_11D3_8634_0020EA0406A1__INCLUDED_)
#define AFX_MAPDOC_H__CC1CD226_F7F7_11D3_8634_0020EA0406A1__INCLUDED_

#include "TextFile.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
/////////////////////////////////////////////////////////////////////////////
// CMapExDoc document
class CMapExDoc : public CDocument
{
protected:
	CMapExDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CMapExDoc)
public:
	//{{AFX_VIRTUAL(CMapExDoc)
	public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	protected:
	//}}AFX_VIRTUAL
public:
	virtual ~CMapExDoc();

	CStringArray	m_strDataRecord;
	CWordArray		m_nHasData;
	float			m_fVersion;
	int				m_nNumObj;
	time_t			m_tModTime;
	CString			m_strModUser;
	CTime			m_cTimeLastMod;

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CMapExDoc)
	afx_msg void OnFileNew();
	afx_msg void OnUpdateFileNew(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CSTOLMapDoc : public CMapExDoc
{
protected:
	CSTOLMapDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSTOLMapDoc)
public:
	//{{AFX_VIRTUAL(CSTOLMapDoc)
	public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL
public:
	virtual ~CSTOLMapDoc();
	BOOL IsBreak(int nIndex);
	
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CSTOLMapDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CRTCSMapDoc : public CMapExDoc
{
protected:
	CRTCSMapDoc();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CRTCSMapDoc)
public:
	//{{AFX_VIRTUAL(CRTCSMapDoc)
	public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL
public:
	virtual ~CRTCSMapDoc();
	
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CRTCSMapDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPDOC_H__CC1CD226_F7F7_11D3_8634_0020EA0406A1__INCLUDED_)
