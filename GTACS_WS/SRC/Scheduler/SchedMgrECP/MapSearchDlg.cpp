/***************************
//////////////////////////////////////////////////////////////////////
// MapSearchDlg.cpp : implementation file
//
// (c) 2001 Frederick J. Shaw                                       // 
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
***************************/


#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "MapSearchDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMapSearchDlg dialog

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw	Date : 3/16/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMapSearchDlg::CMapSearchDlg
//	Description :	Class Constructor
//	Return :		constructor	-	none
//	Parameters :	CWnd* -	Pointer to CWnd object; not used.
//	Note 
//////////////////////////////////////////////////////////////////////
***********************************/
CMapSearchDlg::CMapSearchDlg(CWnd* pParent /*=NULL*/)
	: CResizableDialog(CMapSearchDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMapSearchDlg)
	m_strSearchTimeEdit = _T("");
	//}}AFX_DATA_INIT
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw	Date : 3/16/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		void CMapSearchDlg::DoDataExchange
//	Description :	Called by the framework to exchange and validate 
//					dialog data.
//	Return :		void	-	none
//	Parameters :	CDataExchange* pointer to a DataExchange object.
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMapSearchDlg::DoDataExchange(CDataExchange* pDX)
{
	CResizableDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMapSearchDlg)
	DDX_Control(pDX, IDC_SEARCH_TIME_EDIT, m_cSearchTimeEdit);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_SEARCH_TIME_EDIT, m_strSearchTimeEdit);
}

BEGIN_MESSAGE_MAP(CMapSearchDlg, CResizableDialog)
	//{{AFX_MSG_MAP(CMapSearchDlg)
	ON_BN_CLICKED(IDC_SEARCH_PAUSE_RADIO, OnSearchPauseRadio)
	ON_BN_CLICKED(IDC_SEARCH_EXACT_RADIO, OnSearchExactRadio)
	ON_BN_CLICKED(IDC_SEARCH_CLOSEST_RADIO, OnSearchClosestRadio)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMapSearchDlg message handlers

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw	Date : 3/16/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMapSearchDlg::OnInitDialog()		
//	Description :	Initialize the dialog window. 
//					
//	Return :		BOOL
//					TRUE	-	SUCCESS
//					FALSE	-	FAIL
//	Parameters :	void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
BOOL CMapSearchDlg::OnInitDialog() 
{
	CResizableDialog::OnInitDialog();

	m_cSearchTimeEdit.SetMask(_T("####/###/##:##:##.###"));
	m_cSearchTimeEdit.SetPromptSymbol(_T('0'));

	((CButton*)GetDlgItem(IDC_SEARCH_PAUSE_RADIO))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_SEARCH_CLOSEST_RADIO))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_SEARCH_EXACT_RADIO))->SetCheck(FALSE);
	switch (m_eSearchType)
	{
		case SEARCH_PAUSE	:	((CButton*)GetDlgItem(IDC_SEARCH_PAUSE_RADIO))->SetCheck(TRUE);		break;
		case SEARCH_CLOSEST	:	((CButton*)GetDlgItem(IDC_SEARCH_CLOSEST_RADIO))->SetCheck(TRUE);	break;
		case SEARCH_EXACT	:	((CButton*)GetDlgItem(IDC_SEARCH_EXACT_RADIO))->SetCheck(TRUE);		break;
	}
	
	ShowSizeGrip(FALSE);
	EnableSaveRestore(_T("Resizable Sheets"), _T("Map Search"));	

	return TRUE;
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw	Date : 3/16/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMapSearchDlg::OnSearchPauseRadio()		
//	Description :	This routine sets the member variable based on the
//					user selected dialog option 
//					
//	Return :		void	-	none
//					
//	Parameters :	void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMapSearchDlg::OnSearchPauseRadio() 
{
	m_eSearchType = SEARCH_PAUSE;
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw	Date : 3/16/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMapSearchDlg::OnInitDialog()		
//	Description :	This routine sets the member variable based on the
//					user selected dialog option  
//					
//	Return :		void	-	none.
//					
//	Parameters :	void	-	none.
//
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMapSearchDlg::OnSearchExactRadio() 
{
	m_eSearchType = SEARCH_EXACT;
}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw	Date : 3/16/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMapSearchDlg::OnSearchClosestRadio()		
//	Description :	This routine sets the member variable based on the
//					user selected dialog option  
//					
//	Return :		void	-	none
//					
//	Parameters :	void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////
***********************************/
void CMapSearchDlg::OnSearchClosestRadio() 
{
	m_eSearchType = SEARCH_CLOSEST;
}
