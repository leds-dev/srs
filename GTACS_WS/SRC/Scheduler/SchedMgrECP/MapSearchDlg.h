#if !defined(AFX_MAPSEARCHDLG_H__C4AB9D14_D884_11D5_9ACF_0003472193C8__INCLUDED_)
#define AFX_MAPSEARCHDLG_H__C4AB9D14_D884_11D5_9ACF_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MapSearchDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMapSearchDlg dialog

class CMapSearchDlg : public CResizableDialog
{
// Construction
public:

	typedef enum eMapSearchType {SEARCH_PAUSE, SEARCH_CLOSEST, SEARCH_EXACT};

	CMapSearchDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMapSearchDlg)
	enum { IDD = IDD_MAPSEARCH_DIALOG };
	CString	m_strSearchTimeEdit;
	//}}AFX_DATA
	COXMaskedEdit	m_cSearchTimeEdit;
	eMapSearchType	m_eSearchType;
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMapSearchDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMapSearchDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSearchPauseRadio();
	afx_msg void OnSearchExactRadio();
	afx_msg void OnSearchClosestRadio();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPSEARCHDLG_H__C4AB9D14_D884_11D5_9ACF_0003472193C8__INCLUDED_)
