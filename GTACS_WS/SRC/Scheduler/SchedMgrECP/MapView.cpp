/***************************
//////////////////////////////////////////////////////////////////////
// MapView.cpp : implementation file
//
// (c) 2001 Frederick J. Shaw                                       // 
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
***************************/
/////////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "Resource.h"

#include "MapView.h"
#include "MainFrm.h"
#include "SchedMgrECP.h"
#include "Parse.h"
#include "MapDoc.h"
#include "MapView.h"
#include "SchedTime.h"
#include "SearchConstants.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		MapPrintHookProc
//	Description :	The PrintHookProc is called from when a CPrintDialog
//					is created from within OnFilePrint.  It becomes active
//					after the print dialog has been created but before
//					it is displayed.  It is used so that we can change the
//					static text string that says "Print Pages from x to y"
//					to "Print Frames from x to y".
//	Return :		
//		UINT CALLBACK		-	Always 0
//	Parameters :	
//		HWND hdlg			-	HWND of the dialog box
//		UINT uMsg			-	Should always be WM_INITDIALOG
//		WPARAM wParam		-	Not used
//		LPARAM lParam		-	Not used
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
UINT CALLBACK MapPrintHookProc (HWND hdlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_INITDIALOG)
	{
			HWND hWnd = GetDlgItem(hdlg, 1058);
			SetWindowText (hWnd, _T("Lines"));
	}
	return 0;
}

IMPLEMENT_DYNCREATE(CMapExView, CListView)
/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw	Date : 3/16/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMapExView::CMapExView()
//	Description :	Class Constructor
//	Return :
//					void	-	none
//	Parameters :
//					void	-	none
//	Note 
//////////////////////////////////////////////////////////////////////
***********************************/
CMapExView::CMapExView(){}

/***********************************
//////////////////////////////////////////////////////////////////////
//	Author : Frederick J. Shaw	Date : 3/16/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CMapExView::~CMapExView()
//	Description :	Class Destructor
//	Return :
//					void	-	none.
//	Parameters :
//					void	-	none.
//	Note 
//////////////////////////////////////////////////////////////////////
***********************************/
CMapExView::~CMapExView(){}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMapExView::OnCreate
//	Description :	Creates the CListView Object when the view is created.
//	Return :
//					int		
//							0	-	success
//							-1	-	fail		
//	Parameters :
//					LPCREATESTRUCT lpCreateStruct	-
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
int CMapExView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListView::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

BEGIN_MESSAGE_MAP(CMapExView, CListView)
	//{{AFX_MSG_MAP(CMapExView)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMapExView::AssertValid
//	Description :	AssertValid Debug function.
//	Return :
//		void		-
//	Parameters :	None
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CMapExView::AssertValid() const
{
	CListView::AssertValid();
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMapExView::Dump
//	Description :	Dump debug function.
//	Return :
//		void		-
//	Parameters :
//		CDumpContext& dc	-	Dump context
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CMapExView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif //_DEBUG

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMapExView::PreCreateWindow
//	Description :	This function is used to set the list control's
//					style bits to "Report View", "Single Select".
//	Return :
//		BOOL		-	Status
//	Parameters :
//		CREATESTRUCT& cs	-	CREATESTRUCT of the object
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
BOOL CMapExView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~LVS_TYPEMASK;
	cs.style |= LVS_REPORT | LVS_SINGLESEL;
	return CListView::PreCreateWindow(cs);
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMapExView::OnInitialUpdate
//	Description :	This is the base class routine that initializes the
//					list view window.
//					
//	Return :
//					void	-	none.
//	Parameters :	
//					void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CMapExView::OnInitialUpdate()
{
	CListView::OnInitialUpdate();

	HDC hDC = CreateDC (_T("Display"), NULL, NULL, NULL);
	TEXTMETRIC tm;
	GetTextMetrics (hDC, &tm);
	DeleteDC(hDC);
	tm.tmAveCharWidth;

	CListCtrl&	theCtrl = GetListCtrl();
	theCtrl.SetExtendedStyle(theCtrl.GetExtendedStyle()|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
	theCtrl.DeleteAllItems();
//	CMapExDoc* pDoc = (CMapExDoc*)GetDocument();

	for (int nColumn=0; nColumn < m_nNumParams; nColumn++)
	{
		int nJustify = 0;
		switch (GetAppRegMapObj()->m_nDisplayJustify[m_nObjType][nColumn])
		{
			case 0: nJustify = LVCFMT_LEFT;		break;
			case 1: nJustify = LVCFMT_CENTER;	break;
			case 2: nJustify = LVCFMT_RIGHT;	break;
		}

		int nScreenWidth;
		if (GetAppRegMapObj()->m_nDisplay[m_nObjType][nColumn])
			nScreenWidth = GetAppRegMapObj()->m_nDisplayWidth[m_nObjType][nColumn] * tm.tmAveCharWidth;
		else
			nScreenWidth = 0;
		theCtrl.InsertColumn(nColumn, GetAppRegMapObj()->m_strPropTitle[m_nObjType][nColumn], nJustify, nScreenWidth, nColumn);
	}		
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CMapExView::OnFilePrint()
//	Description :	This routine is the base class file print routine..
//					
//	Return :
//					void	-	none.
//	Parameters :
//					void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CMapExView::OnFilePrint()
{
	MSG msg;
	CMapExDoc* pDoc = (CMapExDoc*)GetDocument();

	CPrintDialog pd (false, PD_ALLPAGES | PD_HIDEPRINTTOFILE | PD_RETURNDC |
							PD_USEDEVMODECOPIES | PD_ENABLEPRINTHOOK | PD_NOSELECTION, NULL);

	// The max number of pages (lines) is 65535.  This is a Microsoft limit on the GUI
	// control.  A user can print more than this, he just can't selectivly print out
	// lines above this range/
	unsigned short nMaxObj;
	if (pDoc->m_strDataRecord.GetSize() >= USHRT_MAX)
		nMaxObj = USHRT_MAX;
	else
		nMaxObj = (unsigned short)pDoc->m_strDataRecord.GetSize();

	// Setup defaults for the print dialog box
	pd.m_pd.hDevMode=((CSchedMgrECPApp*)AfxGetApp())->m_hDevMode;
	pd.m_pd.hDevNames=((CSchedMgrECPApp*)AfxGetApp())->m_hDevNames;
	pd.m_pd.nMinPage=1;
	pd.m_pd.nMaxPage=nMaxObj;
	pd.m_pd.lpfnPrintHook = m_pPrintHookProc;

	// If the dialog box was closed with an OK
	if (pd.DoModal() == IDOK)
	{
		// Margins in inches
		HDC hDC = pd.CreatePrinterDC();
		if (hDC != NULL)
		{
			SetMapMode(hDC, MM_TEXT);
			
			((CSchedMgrECPApp*)AfxGetApp())->m_hDevMode = pd.m_pd.hDevMode;
			((CSchedMgrECPApp*)AfxGetApp())->m_hDevNames= pd.m_pd.hDevNames;

			CPoint cPhysical;
			CPoint cPrintable;
			CPoint cOffset;
			CPoint cLogPixels;

			cPhysical.x		= GetDeviceCaps(hDC, PHYSICALWIDTH);
			cPhysical.y		= GetDeviceCaps(hDC, PHYSICALHEIGHT);
			cPrintable.x	= GetDeviceCaps(hDC, HORZRES);
			cPrintable.y	= GetDeviceCaps(hDC, VERTRES);
			cOffset.x		= GetDeviceCaps(hDC, PHYSICALOFFSETX);
			cOffset.y		= GetDeviceCaps(hDC, PHYSICALOFFSETY);
			cLogPixels.x	= GetDeviceCaps(hDC, LOGPIXELSX);
			cLogPixels.y	= GetDeviceCaps(hDC, LOGPIXELSY);

			// Offsets from edge of page to start of printable area in pixels
			int	nTopOffset		= cOffset.y;
			int nBottomOffset	= cPhysical.y - cPrintable.y - cOffset.y;
			int	nLeftOffset		= cOffset.x;
			int nRightOffset	= cPhysical.x - cPrintable.x - cOffset.x;

			// Width of margins in pixels
			// Use the larger of the physical offset needed by the printer or 
			// the size requested by the operator
			int nTopMargin		= __max((int)(m_fPrintTopMargin * cLogPixels.y), nTopOffset);
			int nBottomMargin	= __max((int)(m_fPrintBottomMargin * cLogPixels.y), nBottomOffset);
			int nLeftMargin		= __max((int)(m_fPrintLeftMargin * cLogPixels.x), nLeftOffset);
			int nRightMargin	= __max((int)(m_fPrintRightMargin * cLogPixels.x), nRightOffset);

			// If the margins are too big then reset them back to the minimum needed
			if ((nTopMargin + nBottomMargin) > cPhysical.y)
			{
				nTopMargin = nTopOffset;
				nBottomMargin = nBottomOffset;
			}
			if ((nLeftMargin + nRightMargin) > cPhysical.x)
			{
				nLeftMargin = nLeftOffset;
				nRightMargin = nRightOffset;
			}

			// Readjust the printable area using the new margins
			cPrintable.x = cPhysical.x - nLeftMargin - nRightMargin;
			cPrintable.y = cPhysical.y - nTopMargin - nBottomMargin;
			
			int nTech		= GetDeviceCaps(hDC, TECHNOLOGY);

			HFONT hFont = CreateFont (-MulDiv (m_nPrintPoint, GetDeviceCaps (hDC, LOGPIXELSY), 72),
									  0, 0, 0, m_nPrintWeight,
									  m_nPrintItalic,
									  m_nPrintUnderline,
									  m_nPrintStrikeOut,
									  0, 0, 0, 0, 0, m_strPrintFontName);
			SelectObject(hDC, hFont);

			TCHAR szTitle[ _MAX_PATH + 1 ];
			_tcscpy_s (szTitle, pDoc->GetPathName());
			DOCINFO di;
			memset (&di, 0, sizeof(DOCINFO));
			di.cbSize = sizeof(DOCINFO); 
			di.lpszDocName = szTitle; 
			di.lpszOutput = (LPTSTR) NULL; 
			di.lpszDatatype = (LPTSTR) NULL; 
			di.fwType = 0; 

			TEXTMETRIC tm;
			VERIFY (GetTextMetrics (hDC, &tm));
			CPoint cTextSize;
			cTextSize.x = tm.tmAveCharWidth;
			cTextSize.y = tm.tmHeight;

			// Determine the number of whole lines that can fit within the printable area
			int nMaxLinesPerPage = cPrintable.y / cTextSize.y;
			int nMaxCharsPerLine = cPrintable.x / cTextSize.x;
 
			// Determine the size of a tab character in pixels
//			int cxTab = (tm.tmAveCharWidth*10) * 4;

			int nCurObj = 0;
			int nStartObj = 0;
			int nEndObj = 0;

			// Should we print the whole document, the range specified, or the highlighted selection?
			if (pd.PrintAll())
			{
				nStartObj = 0;
				nEndObj =  nMaxObj - 1;
			}
			else 
			{
				nStartObj = pd.GetFromPage() - 1;
				nEndObj = pd.GetToPage() - 1;
			}

//			BOOL bAbort = FALSE;

			BOOL bContinue = TRUE;
			int	nMapObjPerPage = 0;	// Number of Inst Objs that can be printed per page
			
			if(nTech == DT_CHARSTREAM)
				nMapObjPerPage = 58; 	// 62 lines minus the header and footer
			else
				nMapObjPerPage = nMaxLinesPerPage - 6;// Minus 2 lines for hdr, 2 for footer, 2 for col hdr ;

			int nPageCount = 1;
			CString strPage;
			CString strCurLine;

			SetTextAlign(hDC, TA_BOTTOM);

//			int nIndex;
			int nCharCount = 0;
			
			for (int nIndex=0; nIndex<m_nNumParams; nIndex++)
			{
				if (GetAppRegMapObj()->m_nPrint[m_nObjType][nIndex])
				{
					nCharCount = nCharCount + GetAppRegMapObj()->m_nPrintWidth[m_nObjType][nIndex] + 1;
				}
			}
			// Subtract off the last character since it is just a spacer between columns
			if (nCharCount > 0) nCharCount = nCharCount - 1;

			// If total width of the columns to print is 0, then no columns were selected
			if (nCharCount == 0)
			{
				CString strPrompt;
				strPrompt.Format (_T("No columns were selected to print.  From within\n")
								  _T("Workspace Options..., choose the columns you want to\n")
								  _T("print along with the width of each column."));
//				int retval = AfxGetMainWnd()->MessageBox(strPrompt, _T("No columns selected"), MB_OK | MB_ICONEXCLAMATION);
				bContinue = false;
			}

			// If we are still printing then...
			// If total width of the columns to print is greate then the number of chars per line
			// then ask the user if he wants the lines truncated or the print job canceled
			if (bContinue)
			{
				if (nCharCount >= nMaxCharsPerLine)
				{
					CString strPrompt;
					strPrompt.Format (_T("The paper size, orientation, and font selected allow\n")
									  _T("for a maximum of %d characters per line.  The total size\n")
									  _T("of all of the columns selected to print is %d.  Select\n")
									  _T("OK to truncate all lines to fit within the page margins, or\n")
									  _T("select Cancel to cancel the print request."), nMaxCharsPerLine,
									  nCharCount);
					int retval = AfxGetMainWnd()->MessageBox(strPrompt, _T("Lines to long"), MB_OKCANCEL | MB_ICONEXCLAMATION);
					if (retval == IDCANCEL)
						bContinue = false;
				}
			}

			if (bContinue)
			{
				// Call StartDoc to begin the printing of a document
//				int an = StartDoc(hDC, &di);
				//VERIFY (StartDoc (hDC, &di) > 0);
				StartDoc(hDC, &di);

				// Create the Printer Abort dialog box
				// This dialog box will be displayed while the printing is taking place and allow the user
				// the option of canceling the print job
				// If canceled, the bAbort flag will set to true from within the AbortDlgProc procedure
				bool bAbort = false;
				HWND hWndAbort = CreateDialogParam (AfxGetApp()->m_hInstance, MAKEINTRESOURCE (IDD_ABORTPRINT), 
													NULL, (DLGPROC) AbortPrintDlgProc, (LPARAM) &bAbort);

				// Display the Printer Abort dialog box
				// Search through message queue looking for a click on the Cancel button
				::ShowWindow(hWndAbort, SW_SHOW);
				while (PeekMessage(&msg, hWndAbort, NULL, NULL, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}

				while (bContinue && !bAbort)
				{
					// Begin to print a page
//					int a = StartPage(hDC);
//					int b = GetLastError();
					//VERIFY (StartPage(hDC) > 0 );
					StartPage(hDC);
					GetLastError();

					// Output the header
					CRect cBounds;
					cBounds.bottom	= nTopMargin;
					cBounds.top		= cBounds.bottom - cTextSize.y;
					cBounds.left	= nLeftMargin;
					cBounds.right	= nLeftMargin + cPrintable.x;
					SetTextAlign (hDC, TA_LEFT|TA_BOTTOM);
					ExtTextOut (hDC, cBounds.left, cBounds.bottom, ETO_CLIPPED, cBounds,
								pDoc->GetPathName(), pDoc->GetPathName().GetLength(), NULL);

					// Output the footer
					cBounds.bottom	= nTopMargin + cPrintable.y;
					cBounds.top		= cBounds.top - cTextSize.y;
					cBounds.left	= nLeftMargin;
					cBounds.right	= nLeftMargin + cPrintable.x;
					CString strPage;
					strPage.Format(_T("Page: %d"), nPageCount);
					ExtTextOut (hDC, cBounds.left, cBounds.bottom, ETO_CLIPPED, cBounds,
								strPage, strPage.GetLength(), NULL);
					
					// Fill up the page with text
					nPageCount++;

					// Position ourselves back up to the first line that we will print data on
					// Creating a bounding box for this row
					// The left side is against the margin, while width is equal to 0
					cBounds.bottom = nTopMargin + (cTextSize.y*2);
					cBounds.top = cBounds.bottom - cTextSize.y;
					cBounds.left = nLeftMargin;
					cBounds.right = 0;

					// We now need to print out column headers for each column that will be printer
					int nXPos = 0;
					for (int nIndex=0; nIndex<m_nNumParams; nIndex++)
					{
						if (GetAppRegMapObj()->m_nPrint[m_nObjType][nIndex])
						{
							// Adjust the width of the bounding box to include the proper number of characters
							// Set the text alignment according the justification specified (Left, Centered, or Right)
							// Load the string used for the column header from the string table
							// Output the string
							// Readjust the left and right sides of the bounding box
							cBounds.right = cBounds.left +
								(GetAppRegMapObj()->m_nPrintWidth[m_nObjType][nIndex]*cTextSize.x);
							
							switch (GetAppRegMapObj()->m_nPrintJustify[m_nObjType][nIndex])
							{
								case 0 :
								{
									SetTextAlign (hDC, TA_LEFT|TA_BOTTOM);
									nXPos = cBounds.left;
									break;
								}
								case 1 : 
								{
									SetTextAlign (hDC, TA_CENTER|TA_BOTTOM);
									nXPos = (cBounds.left+cBounds.right)/2;
									break;
								}
								case 2 :
								{
									SetTextAlign (hDC, TA_RIGHT|TA_BOTTOM);
									nXPos = cBounds.right;
									break;
								}
							}

							CString strHeader = GetAppRegMapObj()->m_strPropTitle[m_nObjType][nIndex];

							// If the right edge goes past the right margin, then truncate the right edge
							if (cBounds.right > (nLeftMargin + cPrintable.x))
								cBounds.right = nLeftMargin + cPrintable.x;

							// If the left edge is within the printable area, then print the line
							if (cBounds.left < (nLeftMargin + cPrintable.x))
							{
								ExtTextOut (hDC, nXPos, cBounds.bottom, ETO_CLIPPED, cBounds, 
											strHeader, strHeader.GetLength(), NULL);
							}
							cBounds.right = cBounds.right + cTextSize.x;
							cBounds.left = cBounds.right;
						}
					}

					// Increment the current line number that we are on
					// Reset the bounding box back to the left margin
					cBounds.bottom = nTopMargin + (cTextSize.y*3);
					cBounds.top = cBounds.bottom - cTextSize.y;
					cBounds.left = nLeftMargin;
					cBounds.right = 0;
					
					// We now need to print out a row of dashes under each column heading
					for (int nIndex=0; nIndex<m_nNumParams; nIndex++)
					{
						if (GetAppRegMapObj()->m_nPrint[m_nObjType][nIndex])
						{
							// Adjust the width of the bounding box to include the proper number of characters
							// Set the text alignment according the justification specified (Left, Centered, or Right)
							// Create a string of dashes
							// Output the string
							// Readjust the left and right sides of the bounding box
							cBounds.right = cBounds.left +
								(GetAppRegMapObj()->m_nPrintWidth[m_nObjType][nIndex]*cTextSize.x);
							
							switch (GetAppRegMapObj()->m_nPrintJustify[m_nObjType][nIndex])
							{
								case 0 :
								{
									SetTextAlign (hDC, TA_LEFT|TA_BOTTOM);
									nXPos = cBounds.left;
									break;
								}
								case 1 : 
								{
									SetTextAlign (hDC, TA_CENTER|TA_BOTTOM);
									nXPos = (cBounds.left+cBounds.right)/2;
									break;
								}
								case 2 :
								{
									SetTextAlign (hDC, TA_RIGHT|TA_BOTTOM);
									nXPos = cBounds.right;
									break;
								}
							}

							// If the right edge goes past the right margin, then truncate the right edge
							if (cBounds.right > (nLeftMargin + cPrintable.x))
								cBounds.right = nLeftMargin + cPrintable.x;

							// If the left edge is within the printable area, then print the line
							if (cBounds.left < (nLeftMargin + cPrintable.x))
							{
								CString strDash('-', GetAppRegMapObj()->m_nPrintWidth[m_nObjType][nIndex]);
								ExtTextOut (hDC, nXPos, cBounds.bottom, ETO_CLIPPED, cBounds, 
											strDash, strDash.GetLength(), NULL);
							}
							cBounds.right = cBounds.right + cTextSize.x;
							cBounds.left = cBounds.right;
						}
					}


					// Increment the current line number that we are on
					// Reset the bounding box back to the left margin
					int nCurObjCntOnPage = 0;
					while (nCurObjCntOnPage <= nMapObjPerPage)
					{
						cBounds.bottom = nTopMargin + (cTextSize.y*(4 + nCurObjCntOnPage));
						cBounds.top = cBounds.bottom - cTextSize.y;
						cBounds.left = nLeftMargin;
						cBounds.right = 0;
						// If we have already printed to the last selected frame then we are done
						if (nCurObj > nEndObj)
						{
							bContinue = FALSE;
							break;
						}
						else
						{
							CParseSTOLMap	cParseSTOLMap;
							CParseRTCSMap	cParseRTCSMap;
							if ((nCurObj >= nStartObj) && (pDoc->m_nHasData[nCurObj]))
							{
								switch (m_nObjType)
								{
									case nMapObjSTOL : 
									{
										cParseSTOLMap.SetData(((CSTOLMapDoc*)GetDocument())->m_strDataRecord[nCurObj]);
										cParseSTOLMap.ParseData();
										break;
									}
									case nMapObjRTCS : 
									{
										cParseRTCSMap.SetData(((CRTCSMapDoc*)GetDocument())->m_strDataRecord[nCurObj]);
										cParseRTCSMap.ParseData();
										break;
									}
									default	:
									{
										ASSERT (FALSE);
										break;
									}
								}

								for (int nIndex=0; nIndex<m_nNumParams; nIndex++)
								{
									if (GetAppRegMapObj()->m_nPrint[m_nObjType][nIndex])
									{
										cBounds.right = cBounds.left +
											(GetAppRegMapObj()->m_nPrintWidth[m_nObjType][nIndex]*cTextSize.x);
						
										switch (GetAppRegMapObj()->m_nPrintJustify[m_nObjType][nIndex])
										{
											case 0 :
											{
												SetTextAlign (hDC, TA_LEFT|TA_BOTTOM);
												nXPos = cBounds.left;
												break;
											}
											case 1 : 
											{
												SetTextAlign (hDC, TA_CENTER|TA_BOTTOM);
												nXPos = (cBounds.left+cBounds.right)/2;
												break;
											}
											case 2 :
											{
												SetTextAlign (hDC, TA_RIGHT|TA_BOTTOM);
												nXPos = cBounds.right;
												break;
											}
										}
										if (cBounds.right > (nLeftMargin + cPrintable.x))
											cBounds.right = nLeftMargin + cPrintable.x;

										// If the left edge is within the printable area, then print the line
										if (cBounds.left < (nLeftMargin + cPrintable.x))
										{
											CString strTextField;
											switch (m_nObjType)
											{
												case nMapObjSTOL	:
												{
													int	nReorder[nMaxMapObjPropsPerObj] = {5, 8, 4, 2, 3, 0, 7, 1, 6};
													strTextField = cParseSTOLMap.GetData(nReorder[nIndex]);
													break;
												};
												case nMapObjRTCS	: 
												{
													int	nReorder[nMaxMapObjPropsPerObj] = {8, 5, 0, 1, 9, 2, 3, 4, 6, 7, 10};
													strTextField = cParseRTCSMap.GetData(nReorder[nIndex]);
													break;
												};
												default				: ASSERT(FALSE);	break;
											}
											ExtTextOut (hDC, nXPos, cBounds.bottom, ETO_CLIPPED, cBounds, 
														strTextField, strTextField.GetLength(), NULL);
										}
										cBounds.right = cBounds.right + cTextSize.x;
										cBounds.left = cBounds.right;
									}
								}
								nCurObjCntOnPage++;

							}
							nCurObj++;
						}
					}

					// At this point we have just printed one page
					// Call EndPage so the printer will eject the paper
					// Flush the message queue for the abort dlg so it can process the Cancel button
					VERIFY (EndPage(hDC) > 0);
					while (PeekMessage(&msg, hWndAbort, NULL, NULL, PM_REMOVE))
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
					if (nCurObj > nEndObj)
						bContinue = FALSE;
				}

				// At this point, the print job is either done, or it was aborted
				// If it was aborted call AbortDoc otherwise call EndDoc
				if (bAbort)
				{
					VERIFY (AbortDoc(hDC) > 0);
				}
				else
				{
					VERIFY (EndDoc(hDC) > 0);
				}
		
				// Delete the Print Abort dialog from the display
				// Delete the Device Context and Font resources used for the print job
				::DestroyWindow (hWndAbort);
				DeleteDC (hDC);
				DeleteObject (hFont);
			}
		}
		else
		{
			GetAppOutputBar()->OutputToEvents(_T("Could not retrieve Device Context from printer"), COutputBar::FATAL);
		}
	}
}

IMPLEMENT_DYNCREATE(CSTOLMapView, CMapExView)
/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLMapView::CSTOLMapView()
//	Description :	Constructor
//					
//	Return :
//					void	-	none
//	Parameters :
//					void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
CSTOLMapView::CSTOLMapView() {}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLMapView::~CSTOLMapView()
//	Description :	Destructor.
//					
//	Return :
//					void	-	none
//	Parameters :
//					void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
CSTOLMapView::~CSTOLMapView() {}

BEGIN_MESSAGE_MAP(CSTOLMapView, CMapExView)
	//{{AFX_MSG_MAP(CSTOLMapView)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
void CSTOLMapView::AssertValid() const
{
	CListView::AssertValid();
}

void CSTOLMapView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif //_DEBUG

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLMapView::OnInitialUpdate
//	Description :	This routine creates all of the columns in the view.
//					
//	Return :
//					void	-	none.
//	Parameters :
//					void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CSTOLMapView::OnInitialUpdate()
{
	CWaitCursor		waitCursor;
	m_nObjType = nMapObjSTOL;
	m_nNumParams = nMaxMapObjProps[m_nObjType];
	CMapExView::OnInitialUpdate();
	CListCtrl& theCtrl = GetListCtrl();
	m_cImageList.Create(IDB_MAP, 16, 0, RGB(255,0,255));
	theCtrl.SetImageList(&m_cImageList, LVSIL_SMALL);

	for (int nRow=0; nRow<((CSTOLMapDoc*)GetDocument())->m_strDataRecord.GetSize(); nRow++)
	{
		//Note that the view columns are not in the same order that they appear in the map file!
		CParseSTOLMap	cParseSTOLMap;
		cParseSTOLMap.SetData(((CSTOLMapDoc*)GetDocument())->m_strDataRecord[nRow]);
		cParseSTOLMap.ParseData();
		int nImage = ((CSTOLMapDoc*)GetDocument())->IsBreak(nRow) ? 1 : 0;
		theCtrl.InsertItem(nRow, cParseSTOLMap.GetDataExecTime(), nImage);
		theCtrl.SetItemText(nRow, 1, cParseSTOLMap.GetDataSchedLine());
		theCtrl.SetItemText(nRow, 2, cParseSTOLMap.GetDataSchedCmd());
		theCtrl.SetItemText(nRow, 3, cParseSTOLMap.GetDataOBStartIndex());
		theCtrl.SetItemText(nRow, 4, cParseSTOLMap.GetDataOBEndIndex());
		theCtrl.SetItemText(nRow, 5, cParseSTOLMap.GetDataLisLine());
		theCtrl.SetItemText(nRow, 6, cParseSTOLMap.GetDataPrevLisLine());
		theCtrl.SetItemText(nRow, 7, cParseSTOLMap.GetDataNextLisLine());
		theCtrl.SetItemText(nRow, 8, cParseSTOLMap.GetDataCmdExecDuration());
	}
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLMapView::OnFilePrint()
//	Description :	This routine prints the STOL Map file..
//					
//	Return :
//					void	-	none.
//	Parameters :	void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CSTOLMapView::OnFilePrint() 
{
	TCHAR*	pStopString = NULL;
	m_fPrintTopMargin		= _tcstod(GetAppRegInstObj()->m_strTopMargin[nMapObjSTOL], &pStopString);
	m_fPrintBottomMargin	= _tcstod(GetAppRegInstObj()->m_strBottomMargin[nMapObjSTOL], &pStopString);
	m_fPrintLeftMargin		= _tcstod(GetAppRegInstObj()->m_strLeftMargin[nMapObjSTOL], &pStopString);
	m_fPrintRightMargin		= _tcstod(GetAppRegInstObj()->m_strRightMargin[nMapObjSTOL], &pStopString);
	m_nPrintPoint			= GetAppRegInstObj()->m_nFontSize[nMapObjSTOL];
	m_nPrintWeight			= GetAppRegInstObj()->m_nFontWeight[nMapObjSTOL];
	m_nPrintItalic			= GetAppRegInstObj()->m_bFontIsItalic[nMapObjSTOL];
	m_nPrintUnderline		= GetAppRegInstObj()->m_bFontIsUnderline[nMapObjSTOL];
	m_nPrintStrikeOut		= GetAppRegInstObj()->m_bFontIsStrikeOut[nMapObjSTOL];
	m_strPrintFontName		= GetAppRegInstObj()->m_strFontFaceName[nMapObjSTOL];
	m_pPrintHookProc		= MapPrintHookProc;
	m_nObjType				= nMapObjSTOL;

	CMapExView::OnFilePrint();
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSTOLMapView::OnUpdateFilePrint 
//	Description :	This routine enables the file printing menu option.
//					
//	Return :
//					void	-	none.
//	Parameters :	
//					CCmdUI* pointer to command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CSTOLMapView::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);	
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		void CSTOLMapView::OnContextMenu 
//	Description :	Called by the framework when the user has clicked the
//					right mouse button (right clicked) in the window.
//					
//	Return :
//					void	-	none
//	Parameters :	
//					CWnd	- Handle to the window in which the user right
//                            clicked the mouse
//					CPoint	- Position of the cursor, in screen coordinates,
//                            at the time of the mouse click.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CSTOLMapView::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	GetParentFrame()->ActivateFrame();

	CPoint local = point;
	ScreenToClient(&local);
	theApp.ShowPopupMenu (IDR_CONTEXT_MAP_MENU, point, AfxGetMainWnd());
}

IMPLEMENT_DYNCREATE(CRTCSMapView, CMapExView)
/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSMapView::CRTCSMapView()
//	Description :	Constructor.
//					
//	Return :
//					void	-	none.
//	Parameters :	
//					void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
CRTCSMapView::CRTCSMapView() {}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSMapView::~CRTCSMapView()
//	Description :	Destructor.
//					
//	Return :
//					void	-	none.
//	Parameters :	
//					void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
CRTCSMapView::~CRTCSMapView() {}

BEGIN_MESSAGE_MAP(CRTCSMapView, CMapExView)
	//{{AFX_MSG_MAP(CRTCSMapView)
	ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT, OnUpdateFilePrint)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

#ifdef _DEBUG
void CRTCSMapView::AssertValid() const
{
	CListView::AssertValid();
}

void CRTCSMapView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif //_DEBUG

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSMapView::OnInitialUpdate()
//	Description :	This routine initializes the RTCS Map file view window..
//					
//	Return :
//					void	-	none.
//	Parameters :
					void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CRTCSMapView::OnInitialUpdate()
{
	CWaitCursor		waitCursor;
	m_nObjType = nMapObjRTCS;
	m_nNumParams = nMaxMapObjProps[m_nObjType];
	CMapExView::OnInitialUpdate();
	CListCtrl& theCtrl = GetListCtrl();

	for (int nRow=0; nRow<((CRTCSMapDoc*)GetDocument())->m_strDataRecord.GetSize(); nRow++)
	{
		//Note that the view columns are not in the same order that they appear in the map file!
		CParseRTCSMap	cParseRTCSMap;
		cParseRTCSMap.SetData(((CRTCSMapDoc*)GetDocument())->m_strDataRecord[nRow]);
		cParseRTCSMap.ParseData();
		theCtrl.InsertItem(nRow, cParseRTCSMap.GetDataNumber(), 0);
		theCtrl.SetItemText(nRow, 1, cParseRTCSMap.GetDataLabel());
		theCtrl.SetItemText(nRow, 2, cParseRTCSMap.GetDataLisLine());
		theCtrl.SetItemText(nRow, 3, cParseRTCSMap.GetDataNextLisLine());
		theCtrl.SetItemText(nRow, 4, cParseRTCSMap.GetDataRamAddress());
		theCtrl.SetItemText(nRow, 5, cParseRTCSMap.GetDataOBStartIndex());
		theCtrl.SetItemText(nRow, 6, cParseRTCSMap.GetDataOBEndIndex());
		theCtrl.SetItemText(nRow, 7, cParseRTCSMap.GetDataCmd());
		theCtrl.SetItemText(nRow, 8, cParseRTCSMap.GetDataHexCmdValue());
		theCtrl.SetItemText(nRow, 9, cParseRTCSMap.GetDataCmdExecDuration());
		theCtrl.SetItemText(nRow, 10, cParseRTCSMap.GetDataRtcsExecDuration());
	}
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSMapView::OnFilePrint() 
//	Description :	This routine prints the RTCS map file.
//					
//	Return :
//					void	-	none.
//	Parameters :	
//					void	-	none.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CRTCSMapView::OnFilePrint() 
{
	TCHAR*	pStopString = NULL;
	m_fPrintTopMargin		= _tcstod(GetAppRegInstObj()->m_strTopMargin[nMapObjRTCS], &pStopString);
	m_fPrintBottomMargin	= _tcstod(GetAppRegInstObj()->m_strBottomMargin[nMapObjRTCS], &pStopString);
	m_fPrintLeftMargin		= _tcstod(GetAppRegInstObj()->m_strLeftMargin[nMapObjRTCS], &pStopString);
	m_fPrintRightMargin		= _tcstod(GetAppRegInstObj()->m_strRightMargin[nMapObjRTCS], &pStopString);
	m_nPrintPoint			= GetAppRegInstObj()->m_nFontSize[nMapObjRTCS];
	m_nPrintWeight			= GetAppRegInstObj()->m_nFontWeight[nMapObjRTCS];
	m_nPrintItalic			= GetAppRegInstObj()->m_bFontIsItalic[nMapObjRTCS];
	m_nPrintUnderline		= GetAppRegInstObj()->m_bFontIsUnderline[nMapObjRTCS];
	m_nPrintStrikeOut		= GetAppRegInstObj()->m_bFontIsStrikeOut[nMapObjRTCS];
	m_strPrintFontName		= GetAppRegInstObj()->m_strFontFaceName[nMapObjRTCS];
	m_pPrintHookProc		= MapPrintHookProc;
	m_nObjType				= nMapObjRTCS;

	CMapExView::OnFilePrint();
}

/***********************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/30/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CRTCSMapView::OnUpdateFilePrint
//	Description :	This routine enables the File print menu option.
//					
//					
//	Return :
//					void	-	None.
//	Parameters :
//					CCmdUI* pointer to Command UI object.
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
void CRTCSMapView::OnUpdateFilePrint(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable(TRUE);	
}
