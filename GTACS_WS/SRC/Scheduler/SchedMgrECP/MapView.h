/////////////////////////////////////////////////////////////////////////////
#if !defined(AFX_MAPVIEW_H__8616B369_F72D_11D3_8634_0020EA0406A1__INCLUDED_)
#define AFX_MAPVIEW_H__8616B369_F72D_11D3_8634_0020EA0406A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMapExView view

class CMapExView : public CListView
{
protected:
	CMapExView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CMapExView)
public:
	//{{AFX_VIRTUAL(CMapExView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL
protected:
	virtual ~CMapExView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CMapExView)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	int				m_nNumParams;
	int				m_nObjType;
	int				m_nPrintPoint;
	int				m_nPrintWeight;
	int				m_nPrintItalic;
	int				m_nPrintUnderline;
	int				m_nPrintStrikeOut;
	CString			m_strPrintFontName;
	double			m_fPrintLeftMargin;
	double			m_fPrintRightMargin;
	double			m_fPrintTopMargin;
	double			m_fPrintBottomMargin;
	LPPRINTHOOKPROC	m_pPrintHookProc;
	void			OnFilePrint();
};

/////////////////////////////////////////////////////////////////////////////
// CMapView view
class CSTOLMapView : public CMapExView
{
protected:
	CSTOLMapView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CSTOLMapView)
public:
	//{{AFX_VIRTUAL(CSTOLMapView)
	public:
	virtual void OnInitialUpdate();
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//}}AFX_VIRTUAL
protected:
	virtual ~CSTOLMapView();
	CImageList	m_cImageList;
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CSTOLMapView)
	afx_msg void OnFilePrint();
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CMapView view
class CRTCSMapView : public CMapExView
{
protected:
	CRTCSMapView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CRTCSMapView)
public:
	//{{AFX_VIRTUAL(CRTCSMapView)
	public:
	virtual void OnInitialUpdate();
	//}}AFX_VIRTUAL
protected:
	virtual ~CRTCSMapView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
protected:
	//{{AFX_MSG(CRTCSMapView)
	afx_msg void OnFilePrint();
	afx_msg void OnUpdateFilePrint(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPVIEW_H__8616B369_F72D_11D3_8634_0020EA0406A1__INCLUDED_)

