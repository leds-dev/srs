// Monitor.cpp: implementation of the CMonitor class.
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "Monitor.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CProcessSnapshot::CProcessSnapshot(){}
CProcessSnapshot::~CProcessSnapshot(){}

void CProcessSnapshot::MakeSnapshot()
{
	HANDLE			hSnapShot	= CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);
	PROCESSENTRY32* processInfo	= new PROCESSENTRY32;

	processInfo->dwSize = sizeof(PROCESSENTRY32);

	m_strActiveProcessList.RemoveAll();
	m_dwActiveProcessList.RemoveAll();

	CString strImageName;

	while( Process32Next(hSnapShot,processInfo) != FALSE )
	{
		COXUNC cFilename(processInfo->szExeFile);
		m_strActiveProcessList.Add(cFilename.Base());
		m_dwActiveProcessList.Add(processInfo->th32ProcessID);
	}

    delete processInfo;
	CloseHandle(hSnapShot);
}

CString	CProcessSnapshot::GetProcess(int nIndex)
{
	ASSERT (nIndex<m_strActiveProcessList.GetSize());
	return m_strActiveProcessList[nIndex];
}

DWORD CProcessSnapshot::GetID(int nIndex)
{
	ASSERT (nIndex<m_dwActiveProcessList.GetSize());
	return m_dwActiveProcessList[nIndex];
}

CMonitor::CMonitor(){}

CMonitor::CMonitor(CString strTitle, CString strName, TYPE eType)
{
	m_strTitle = strTitle;
	m_strName = strName;
	m_eType = eType;
	m_bActive = FALSE;
	m_dwProcessID = 0;
}

CMonitor::~CMonitor(){}

void CMonitor::Update(CProcessSnapshot* pSnapShot)
{

	int		nIndex=0;
	BOOL	bContinue;

	if	(nIndex>=pSnapShot->GetSize())
		bContinue = FALSE;
	else
		bContinue = TRUE;

	m_bActive = FALSE;
	m_dwProcessID = 0;

	while (bContinue)
	{
		if (pSnapShot->GetProcess(nIndex).CompareNoCase(m_strName) == 0)
		{
			m_bActive = TRUE;
			m_dwProcessID = pSnapShot->GetID(nIndex);
			bContinue = FALSE;
		}
		else
			nIndex++;

		if (nIndex>=pSnapShot->GetSize())
			bContinue = FALSE;
	}
}

DWORD CMonitor::Stop(SC_HANDLE hSCM)
{
	if (m_eType == SERVICE)
	{
		SC_HANDLE hService;
		if (!(hService = OpenService(hSCM, m_strName, SERVICE_ALL_ACCESS)))
			return GetLastError();
		else
		{
			DWORD dwStatus = StopService (hSCM, hService);
			CloseServiceHandle(hService);
			return dwStatus;
		}
	}
	else
		return StopProcess();
}


DWORD CMonitor::StopProcess()
{
	HANDLE hProcess;

	if( (hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, m_dwProcessID)) == NULL )
	{
        if( GetLastError() != ERROR_ACCESS_DENIED )
            return GetLastError();

        OSVERSIONINFO osvi;

        // determine operating system version
        osvi.dwOSVersionInfoSize = sizeof(osvi);
        GetVersionEx(&osvi);

        // nothing else to do if this is not Windows NT
        if( osvi.dwPlatformId != VER_PLATFORM_WIN32_NT )
            return ERROR_ACCESS_DENIED;

        // enable SE_DEBUG_NAME and try again
        TOKEN_PRIVILEGES	priv; 
		TOKEN_PRIVILEGES	privOld;
        DWORD				cbPriv = sizeof(privOld);
        HANDLE				hToken;

        // get current thread token 
        if( !OpenThreadToken(GetCurrentThread(),TOKEN_QUERY|TOKEN_ADJUST_PRIVILEGES,FALSE,&hToken) )
        {
            if( GetLastError() != ERROR_NO_TOKEN )
                return GetLastError();

            // revert to the process token, if not impersonating
            if( !OpenProcessToken(GetCurrentProcess(),TOKEN_QUERY|TOKEN_ADJUST_PRIVILEGES,&hToken) )
                return GetLastError();
        }

        _ASSERTE(ANYSIZE_ARRAY > 0);

        priv.PrivilegeCount				= 1;
        priv.Privileges[0].Attributes	= SE_PRIVILEGE_ENABLED;

        LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &priv.Privileges[0].Luid);

        // try to enable the privilege
        if( !AdjustTokenPrivileges(hToken, FALSE, &priv, sizeof(priv), &privOld, &cbPriv) )
        {
            CloseHandle(hToken);
            return GetLastError();
        }

        if( GetLastError() == ERROR_NOT_ALL_ASSIGNED )
        {
            // the SE_DEBUG_NAME privilege is not in the caller's token
            CloseHandle(hToken);
            return GetLastError();
        }

        // try to open process handle again
        hProcess= OpenProcess(PROCESS_TERMINATE, FALSE, m_dwProcessID);
        
        // restore original privilege state
        AdjustTokenPrivileges(hToken, FALSE, &privOld, sizeof(privOld), NULL, NULL);
        CloseHandle(hToken);

        if( hProcess == NULL )
            return GetLastError();
    }

    // try to terminate the process
	DWORD dwReturn;
	
	if( TerminateProcess(hProcess, 0) )
		dwReturn = ERROR_SUCCESS;
	else
		dwReturn = GetLastError();
		
	CloseHandle(hProcess);
	return dwReturn;
}

DWORD CMonitor::StopService(SC_HANDLE hSCM, SC_HANDLE hService)
{
	SERVICE_STATUS ss;
	BOOL bStopDependencies = TRUE;
	DWORD dwTimeout = 45000;

	DWORD dwStartTime = GetTickCount();

	// Make sure the service is not already stopped
	if (!QueryServiceStatus(hService, &ss))
		return GetLastError();

	// If the service is stopped, just return
	if (ss.dwCurrentState == SERVICE_STOPPED)
		return ERROR_SUCCESS;

	// If a stop is pending, just wait for it
	while (ss.dwCurrentState == SERVICE_STOP_PENDING)
	{
		Sleep (ss.dwWaitHint);
		if (!QueryServiceStatus (hService, &ss))
			return GetLastError ();

		if (ss.dwCurrentState == SERVICE_STOPPED)
			return ERROR_SUCCESS;

		if (GetTickCount() - dwStartTime > dwTimeout)
			return ERROR_TIMEOUT;
	}

	// If the service is running, dependencies must be stopped first
	if (bStopDependencies)
	{
		DWORD i;
		DWORD dwBytesNeeded;
		DWORD dwCount;

		LPENUM_SERVICE_STATUS   lpDependencies = NULL;
		ENUM_SERVICE_STATUS     ess;
		SC_HANDLE               hDepService;

		// Pass a zero-length buffer to get the required buffer size
		if (EnumDependentServices(hService, SERVICE_ACTIVE, lpDependencies, 0, &dwBytesNeeded, &dwCount))
		{
			// If the Enum call succeeds, then there are no dependent
			// services so do nothing
		}
		else
		{
			if (GetLastError() != ERROR_MORE_DATA)
				return GetLastError(); // Unexpected error

			// Allocate a buffer for the dependencies
			lpDependencies = (LPENUM_SERVICE_STATUS) HeapAlloc (GetProcessHeap(), HEAP_ZERO_MEMORY, dwBytesNeeded);

			if (!lpDependencies)
				return GetLastError();

			__try
			{
				// Enumerate the dependencies
				if (!EnumDependentServices(hService, SERVICE_ACTIVE, lpDependencies,
										   dwBytesNeeded, &dwBytesNeeded, &dwCount))
					return GetLastError();

				for (i=0; i<dwCount; i++)
				{
					ess = *(lpDependencies+i);
					// Open the service
					hDepService = OpenService (hSCM, ess.lpServiceName, SERVICE_ALL_ACCESS);
					if (!hDepService)
						return GetLastError();

					__try
					{
						// Send a stop code
						if (!ControlService(hDepService, SERVICE_CONTROL_STOP, &ss))
							return GetLastError();

						// Wait for the service to stop
						while (ss.dwCurrentState != SERVICE_STOPPED)
						{
							Sleep(ss.dwWaitHint);
							if (!QueryServiceStatus(hDepService, &ss))
								return GetLastError();

							if (ss.dwCurrentState == SERVICE_STOPPED)
								break;

							if (GetTickCount() - dwStartTime > dwTimeout)
								return ERROR_TIMEOUT;
						}
					}
					__finally
					{
						// Always release the service handle
						CloseServiceHandle( hDepService );
					}
				}
			}
			__finally
			{
				// Always free the enumeration buffer
				HeapFree(GetProcessHeap(), 0, lpDependencies);
			}
		}
	}

	// Send a stop code to the main service
	if (!ControlService(hService, SERVICE_CONTROL_STOP, &ss))
		return GetLastError();

	// Wait for the service to stop
	while (ss.dwCurrentState != SERVICE_STOPPED)
	{
		Sleep(ss.dwWaitHint);
		if (!QueryServiceStatus(hService, &ss))
			return GetLastError();

		if (ss.dwCurrentState == SERVICE_STOPPED)
			break;

		if (GetTickCount() - dwStartTime > dwTimeout)
			return ERROR_TIMEOUT;
	}

	// Return success
	return ERROR_SUCCESS;
}
