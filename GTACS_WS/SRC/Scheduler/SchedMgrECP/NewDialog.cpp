/***************************
//////////////////////////////////////////////////////////////////////
// NewDialog.cpp : implementation file
//
// (c) 2001 Frederick J. Shaw                                       // 
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
***************************/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "NewDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewDialog dialog

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewDialog::CNewDialog
//	Description :	Constructor.
//					
//	Return :		
//			void	-	none.
//
//	Parameters :	
//					CWnd* pointer to parent window.
//	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CNewDialog::CNewDialog(CWnd* pParent /*=NULL*/)
	: CResizableDialog(CNewDialog::IDD)
{
	//{{AFX_DATA_INIT(CNewDialog)
	//}}AFX_DATA_INIT
	m_nTypeSelected = nSTOL;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		void CNewDialog::DoDataExchange
//	Description :	Called by the framework to exchange and validate dialog
//					data.
//					
//	Return :		
//				void	-	none
//	Parameters :	
//				CDataExchange*	-	A pointer to a CDataExchange object.	
//		
//		
//		
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewDialog::DoDataExchange(CDataExchange* pDX)
{
	CResizableDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNewDialog)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CNewDialog, CResizableDialog)
	//{{AFX_MSG_MAP(CNewDialog)
	ON_BN_CLICKED(IDC_SECTOR_RADIO, OnSectorRadio)
	ON_BN_CLICKED(IDC_FRAME_RADIO, OnFrameRadio)
	ON_BN_CLICKED(IDC_STOL_RADIO, OnSTOLRadio)
	ON_BN_CLICKED(IDC_CLS_RADIO, OnClsRadio)
	ON_BN_CLICKED(IDC_RTCS_RADIO, OnRTCSRadio)
	ON_BN_CLICKED(IDC_RTCSSET_RADIO, OnRTCSSetRadio)
	ON_BN_CLICKED(IDC_RTCSCMD_RADIO, OnRTCSCmdRadio)
	ON_BN_CLICKED(IDC_PACECMD_RADIO, OnPaceCmdRadio)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNewDialog message handlers
/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewDialog::OnSectorRadio() 
//	Description :	Sets member variable to Sector file. 
//					
//	Return :		
//					void	-	none
//	Parameters :	
//					void	-	none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewDialog::OnSectorRadio() 
{
	m_nTypeSelected = nSector;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewDialog::OnFrameRadio() 
//	Description :	Sets member variable to Frame file. 
//					
//	Return :		
//					void	-	none
//	Parameters :	
//					void	-	none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewDialog::OnFrameRadio() 
{
	m_nTypeSelected = nFrame;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewDialog::OnSTOLRadio() 
//	Description :	Sets member variable to STOL Procedure file. 
//					
//	Return :		
//					void	-	none
//	Parameters :	
//					void	-	none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewDialog::OnSTOLRadio() 
{
	m_nTypeSelected = nSTOL;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewDialog::OnClsRadio() 
//	Description :	Sets member variable to CLS file. 
//					
//	Return :		
//					void	-	none
//	Parameters :	
//					void	-	none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewDialog::OnClsRadio() 
{
	m_nTypeSelected = nCLS;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewDialog::OnRTCSRadio() 
//	Description :	Sets member variable to RTCS procedure file. 
//					
//	Return :		
//					void	-	none
//	Parameters :	
//					void	-	none//	Note :			
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewDialog::OnRTCSRadio() 
{
	m_nTypeSelected = nRTCS;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewDialog::OnRTCSSetRadio() 
//	Description :	Sets member variable to RTCS Set file. 
//					
//	Return :		
//					void	-	none
//	Parameters :	
//					void	-	none//	Note :			
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewDialog::OnRTCSSetRadio() 
{
	m_nTypeSelected = nRTCSSet;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
/	Function :		CNewDialog::OnRTCSCmdRadio() 
//	Description :	Sets member variable to RTCS restricted command file. 
//					
//	Return :		
//					void	-	none
//	Parameters :	
//					void	-	none//	Note :			
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewDialog::OnRTCSCmdRadio() 
{
	m_nTypeSelected = nRTCSCmd;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewDialog::OnPaceCmdRadio() 
//	Description :	Sets member variable to STOL Pace Command file. 
//					
//	Return :		
//					void	-	none
//	Parameters :	
//					void	-	none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewDialog::OnPaceCmdRadio() 
{
	m_nTypeSelected = nPaceCmd;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Don Sanders			Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		BOOL CNewDialog::OnInitDialog()
//	Description :	Initializes the dialog.
//					
//	Return :		
//					BOOL	-	Always TRUE
//	Parameters :	
//					VOID	-	none
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CNewDialog::OnInitDialog() 
{
	CResizableDialog::OnInitDialog();
	
	CImageList imageList;

	EnableSaveRestore(_T("Resizable Sheets"), _T("New Dialog"));
	ShowSizeGrip(FALSE);
	imageList.Create(IDB_FILETYPES, 16, 0, RGB (255,0,255));
	
	((CStatic*)GetDlgItem(IDC_STOL_ICON))->SetIcon(imageList.ExtractIcon(4));
	((CStatic*)GetDlgItem(IDC_CLS_ICON))->SetIcon(imageList.ExtractIcon(5));
	((CStatic*)GetDlgItem(IDC_FRAME_ICON))->SetIcon(imageList.ExtractIcon(8));
	((CStatic*)GetDlgItem(IDC_SECTOR_ICON))->SetIcon(imageList.ExtractIcon(7));
	((CStatic*)GetDlgItem(IDC_RTCS_ICON))->SetIcon(imageList.ExtractIcon(15));
	((CStatic*)GetDlgItem(IDC_RTCSSET_ICON))->SetIcon(imageList.ExtractIcon(14));
	((CStatic*)GetDlgItem(IDC_RTCSCMD_ICON))->SetIcon(imageList.ExtractIcon(16));
	((CStatic*)GetDlgItem(IDC_PACECMD_ICON))->SetIcon(imageList.ExtractIcon(18));
	
	((CButton*)GetDlgItem(IDC_STOL_RADIO))->SetCheck(TRUE);
	((CButton*)GetDlgItem(IDC_CLS_RADIO))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_FRAME_RADIO))->SetCheck(FALSE);
	((CButton*)GetDlgItem(IDC_SECTOR_RADIO))->SetCheck(FALSE);	
	((CButton*)GetDlgItem(IDC_RTCSSET_RADIO))->SetCheck(FALSE);	
	((CButton*)GetDlgItem(IDC_RTCS_RADIO))->SetCheck(FALSE);	
	((CButton*)GetDlgItem(IDC_RTCSCMD_RADIO))->SetCheck(FALSE);	
	((CButton*)GetDlgItem(IDC_PACECMD_RADIO))->SetCheck(FALSE);	
	return TRUE;
}
