#if !defined(AFX_NEWDIALOG_H__B2F9B8C1_1E4A_11D5_9A6F_0003472193C8__INCLUDED_)
#define AFX_NEWDIALOG_H__B2F9B8C1_1E4A_11D5_9A6F_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CNewDialog dialog

class CNewDialog : public CResizableDialog
{
// Construction
public:
	CNewDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CNewDialog)
	enum { IDD = IDD_NEW_FILE_DIALOG };
	//}}AFX_DATA
	int m_nTypeSelected;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	
	// Generated message map functions
	//{{AFX_MSG(CNewDialog)
	afx_msg void OnSectorRadio();
	afx_msg void OnFrameRadio();
	afx_msg void OnSTOLRadio();
	afx_msg void OnClsRadio();
	afx_msg void OnRTCSRadio();
	afx_msg void OnRTCSSetRadio();
	afx_msg void OnRTCSCmdRadio();
	afx_msg void OnPaceCmdRadio();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWDIALOG_H__B2F9B8C1_1E4A_11D5_9A6F_0003472193C8__INCLUDED_)
