/***************************
//////////////////////////////////////////////////////////////////////
// NewDialog.cpp : implementation file
//
// (c) 2001 Frederick J. Shaw                                       // 
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
//
// @(#) goes_n-q sched Software/SchedValLib/NewGblValData.cpp 1.13 99/11/03 15:56:00
//
// NewGblValData.cpp: implementation of the CNewGblValData class.
//
// Revision history:
//
// Frederick Shaw 1999
// Initial release.
//
// Fixed Issues #76 & 77
// Dec 9, 1999
// Added new error messages for syntax checking of the 
// the scan and star directives. 
//
//
//////////////////////////////////////////////////////////////////////
***************************/

#include "stdafx.h"
#include "SearchConstants.h"
#include "SchedMgrECP.h"
#include "NewGblValData.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewGblValData::CNewGblValData 
//	Description :	Default Constructor.
//					
//	Return :		
//			void	-	none.
//
//	Parameters :	
//			void	-	none.
//	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CNewGblValData::CNewGblValData(){
	m_nValidStatus = 0;
	m_nNumWarnings = 0;
	m_nNumErrors = 0;
	m_NumPrologLines = 0;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewGblValData::CNewGblValData 
//	Description :	Constructor.
//					
//	Return :		
//			void	-	none.
//
//	Parameters :	
//			const CString	-	 Input file name.
//	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CNewGblValData::CNewGblValData(const CString strInputFileName){
	m_nValidStatus = 0;
	m_nNumWarnings = 0;
	m_nNumErrors = 0;
	m_NumPrologLines = 0;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewGblValData::~CNewGblValData 
//	Description :	Destructor.
//					
//	Return :		
//			void	-	none.
//
//	Parameters :	
//			void	-	none..
//	
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CNewGblValData::~CNewGblValData(){}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewGblValData::Init() 
//	Description :	This routine initializes the validation message array.
//									
//
//	Return :		
//			void	-	none.
//
//	Parameters :	
//			void	-	none.
//	
//	Note :	
//			Currently, the maximum number of validation messages is 99.		
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewGblValData::Init()
{
	m_structValidMsgs[0].nStatus = 2; // error
	m_structValidMsgs[0].strName = _T("##ground begin directive found without a matching end.(0)");

	m_structValidMsgs[1].nStatus = 1; // warning
	m_structValidMsgs[1].strName = _T("File does not contain a valid prolog block. (1)");
	
	m_structValidMsgs[2].nStatus = 2; // error
	m_structValidMsgs[2].strName = _T("File extension is invalid. (%s) (2)");

	m_structValidMsgs[3].nStatus = 1; // warning
	m_structValidMsgs[3].strName = _T("Too much time elapsed without a command being sent. (%s). (3)");

	m_structValidMsgs[4].nStatus = 2; // error
	m_structValidMsgs[4].strName = _T("The time associated with the directive is not valid (%s). (4)");
	// The time tag associated with directive is invalid

	m_structValidMsgs[5].nStatus = 1; // warning
	m_structValidMsgs[5].strName = _T("The selected instrument does not match (%s).(5)");
	
	m_structValidMsgs[6].nStatus = 1; // warning
	m_structValidMsgs[6].strName = _T("File does not contain a prolog end directive. (6)");
	// 

	m_structValidMsgs[7].nStatus = 2; // error
	m_structValidMsgs[7].strName = _T("Timetag is an invalid structure (%s). (7)");
	// 

	m_structValidMsgs[8].nStatus = 2; // error
	m_structValidMsgs[8].strName = _T("Local variable are not allowed in onboard commanding (%s). (8)");
	// 

	m_structValidMsgs[9].nStatus = 2; // error
	m_structValidMsgs[9].strName = _T("Timetag must be greater than previous time-tag. (9)");
	// 

	m_structValidMsgs[10].nStatus = 2; // error
	m_structValidMsgs[10].strName = _T("File does not exist or can not be opened (%s). (10)");
	// 

	m_structValidMsgs[11].nStatus = 2; // error
	m_structValidMsgs[11].strName = _T("File has failed validation. file:%s. (11)");
	// 
	
	m_structValidMsgs[12].nStatus = 2; // error
	m_structValidMsgs[12].strName = _T("Number of passed parameters does not match. (12)");
	// 
	
	m_structValidMsgs[13].nStatus = 2; // error
	m_structValidMsgs[13].strName = _T("Duplicate passed parameter names (%s). (13)");
	// 
	
	m_structValidMsgs[14].nStatus = 1; // warning
	m_structValidMsgs[14].strName = _T("Housekeeping directive too close to preHousekeeping directive. (14)");
	// 

	m_structValidMsgs[15].nStatus = 2; // error
	m_structValidMsgs[15].strName = _T("Invalid format for CMD directive (%s). (15)");
	// 

	m_structValidMsgs[16].nStatus = 2; // error
	m_structValidMsgs[16].strName = _T("##ground end directive found without a matching begin. (16)");
	// 

	m_structValidMsgs[17].nStatus = 2; // error
	m_structValidMsgs[17].strName = _T("Invalid format for SCAN directive. (17)");
	// 

	m_structValidMsgs[18].nStatus = 2; // error
	m_structValidMsgs[18].strName = _T("Invalid format for STAR directive. (18)");
	// 

	m_structValidMsgs[19].nStatus = 1; // warning
	m_structValidMsgs[19].strName = _T("Not enough time allocated for the activity (%s). (19)") ;
	// 

	m_structValidMsgs[20].nStatus = 2; // error
	m_structValidMsgs[20].strName = _T("Missing a closing parentheses. (20)");
	// 

	m_structValidMsgs[21].nStatus = 2; // error
	m_structValidMsgs[21].strName = _T("Missing a matching quote. (21)");
	// 

	m_structValidMsgs[22].nStatus = 2; // error
	m_structValidMsgs[22].strName = _T("Wait directive missing a time tag. (22)");
	// 

	m_structValidMsgs[23].nStatus = 1; // warning
	m_structValidMsgs[23].strName = _T("BB cal parameter must be set to do not suppress for sounder.(23).");
	// 

	m_structValidMsgs[24].nStatus = 2; // error
	m_structValidMsgs[24].strName = _T("Not enough time allowed for previous RTCS to execute. (24)");
	//  

	m_structValidMsgs[25].nStatus = 2; // error  
	m_structValidMsgs[25].strName = _T("Mismatch in the file's validation status. (25)");
	// onboard != ground

	m_structValidMsgs[26].nStatus = 1; // warning
	m_structValidMsgs[26].strName = _T("Not enough time allowed for the previous command to execute. (26)");
	// 

	m_structValidMsgs[27].nStatus = 1; // warning
	m_structValidMsgs[27].strName = _T("File contains an invalid exectime directive. (27)");
	// 

	m_structValidMsgs[28].nStatus = 2; // error
	m_structValidMsgs[28].strName = _T("Star directive requires a timetag. (28)");
	// 

	m_structValidMsgs[29].nStatus = 2; // error
	m_structValidMsgs[29].strName = _T("Invalid syntax for STOL directive. (29)");
	// 

	m_structValidMsgs[30].nStatus = 1; // warning
	m_structValidMsgs[30].strName = _T("Not enough time allotted for procedure to execute. (30)");
	// 

	m_structValidMsgs[31].nStatus = 2; // error
	m_structValidMsgs[31].strName = _T("Passed parameters are invalid (%s). (31)");
	// 

	m_structValidMsgs[32].nStatus = 1; // warning
	m_structValidMsgs[32].strName = _T("Scan times overlap. (32)");
	// Not enough time is allocated between scans. 

	m_structValidMsgs[33].nStatus = 2; // error
	m_structValidMsgs[33].strName = _T("Invalid instrument parameter value (%s). (33)");
	// Instrument parameter can only contain imager or sounder.

	m_structValidMsgs[34].nStatus = 1; // warning
	m_structValidMsgs[34].strName = _T("No commanding during a star window. (34)");
	// 

	m_structValidMsgs[35].nStatus = 2; // error
	m_structValidMsgs[35].strName = _T("Directive not valid in this type of file (%s). (35)");
	// 

	m_structValidMsgs[36].nStatus = 2; // error
	m_structValidMsgs[36].strName = _T("Error parsing the passed parameters (%s). (36)");
	
	m_structValidMsgs[37].nStatus = 1; // warning
	m_structValidMsgs[37].strName = _T("File validation status is warning (%s). (37)");
	
	m_structValidMsgs[38].nStatus = 1; // warning
	m_structValidMsgs[38].strName = _T("This star window is empty. (38)");
	
	m_structValidMsgs[39].nStatus = 2; // error
	m_structValidMsgs[39].strName = _T("Instrument submnemonic value outside acceptable range: %s. (39)");
	
	m_structValidMsgs[40].nStatus = 2; // error
	m_structValidMsgs[40].strName = _T("PRC_START must be set to \"gmt()\".  (40)");
	
	m_structValidMsgs[41].nStatus = 1; // warning
	m_structValidMsgs[41].strName = _T("No passed parameter variable name was specified. (41)");
	
	m_structValidMsgs[42].nStatus = 2; // error
	m_structValidMsgs[42].strName = _T("The passed parameter(s) declared in the prolog do(es) not match what STOL is expecting.(42)");
	
	m_structValidMsgs[43].nStatus = 2; // error
	m_structValidMsgs[43].strName = _T("NARGS() directive precedes the ARGS() directive. (43)");
	
	m_structValidMsgs[44].nStatus = 2; // error
	m_structValidMsgs[44].strName = _T("Imager scan frame definition exceeds FOV limits for the East: %s. (44)");
	
	m_structValidMsgs[45].nStatus = 2; // error
	m_structValidMsgs[45].strName = _T("Imager scan frame definition exceeds FOV limits for the West: %s. (45)");
	
	m_structValidMsgs[46].nStatus = 2; // error
	m_structValidMsgs[46].strName = _T("Imager scan frame definition exceeds FOV limits for the North: %s. (46)");
    
	m_structValidMsgs[47].nStatus = 1; // warning
	m_structValidMsgs[47].strName = _T("Imager scan frame definition exceeds FOV limits for the South: %s. (47)");

	m_structValidMsgs[48].nStatus = 1; // warning
	m_structValidMsgs[48].strName = _T("(48)");

	m_structValidMsgs[49].nStatus = 2; // error
	m_structValidMsgs[49].strName = _T("The start address is not an integral multiple from west space look address. (49)");

	m_structValidMsgs[50].nStatus = 2; // error
	m_structValidMsgs[50].strName = _T("Star look is not a star sequence. (50)");

	m_structValidMsgs[51].nStatus = 2; // error
	m_structValidMsgs[51].strName = _T("Last star look is not a star sense. (51)");

	m_structValidMsgs[52].nStatus = 1; // warning
	m_structValidMsgs[52].strName = _T("Sounder frame mode is not \"single step/single dwell\": %s. (52)");

	m_structValidMsgs[53].nStatus = 2; // error
	m_structValidMsgs[53].strName = _T("Invalid space look side is selected: %s. (53)");

	m_structValidMsgs[54].nStatus = 0; // info
	m_structValidMsgs[54].strName = _T("Intrusions occur on both space look sides: %s. (54)");

	m_structValidMsgs[55].nStatus = 0; // info
	m_structValidMsgs[55].strName = _T("Intruding body not recognized: %s. (55)");

	m_structValidMsgs[56].nStatus = 2; // error
	m_structValidMsgs[56].strName = _T("Sounder scan frame definition exceeds FOV limits for the East: %s. (56)");

	m_structValidMsgs[57].nStatus = 2; // error
	m_structValidMsgs[57].strName = _T("Sounder scan frame definition exceeds FOV limits for the West: %s. (57)");

	m_structValidMsgs[58].nStatus = 1; // warning
	m_structValidMsgs[58].strName = _T("Sounder scan frame definition exceeds FOV limits for the North: %s. (58)");

	m_structValidMsgs[59].nStatus = 2; // error
	m_structValidMsgs[59].strName = _T("Sounder scan frame definition exceeds FOV limits for the South: %s. (59)");

	m_structValidMsgs[60].nStatus = 0; // informational
	m_structValidMsgs[60].strName = _T("NextSched go directive was not found. (60)");

	m_structValidMsgs[61].nStatus = 0; // informational
	m_structValidMsgs[61].strName = _T("NextSched go directive found before set directive. (61)");

	m_structValidMsgs[62].nStatus = 1; // warning
	m_structValidMsgs[62].strName = _T("NextSched set was not found but NextSched go was present. (62)");

	m_structValidMsgs[63].nStatus = 1; // warning
	m_structValidMsgs[63].strName = _T("The execute time directive has been replaced: %s. (63)");

	m_structValidMsgs[64].nStatus = 2; // error
	m_structValidMsgs[64].strName = _T("Missing a openning parentheses. (64)");

	m_structValidMsgs[65].nStatus = 2; // error
	m_structValidMsgs[65].strName = _T("Activity directive not found: %s. (65)");

	m_structValidMsgs[66].nStatus = 2; // error
	m_structValidMsgs[66].strName = _T("Activity already defined; can't nest activity directives: %s (66)");

	m_structValidMsgs[67].nStatus = 2; // error
	m_structValidMsgs[67].strName = _T("No matching activity begin directive: %s. (67)");

	m_structValidMsgs[68].nStatus = 2; // warning
	m_structValidMsgs[68].strName = _T("No matching activity end directive: %s. (68)");

	m_structValidMsgs[69].nStatus = 2; // error
	m_structValidMsgs[69].strName = _T("Activity directive must contain either a begin or end: %s. (69)");

	m_structValidMsgs[70].nStatus = 2; // error
	m_structValidMsgs[70].strName = _T("Duplicate RTCS starting address. (70)");

	m_structValidMsgs[71].nStatus = 2; // error
	m_structValidMsgs[71].strName = _T("This command is restricted: %s.  (71)");

	m_structValidMsgs[72].nStatus = 2; // error
	m_structValidMsgs[72].strName = _T("RTCS procedures must start with either an NO_OP or MOV_TOP command. (72)");

	m_structValidMsgs[73].nStatus = 2; // error
	m_structValidMsgs[73].strName = _T("RTCS procedures must end with a ACE_STO_MOVE_TOP_08 STO_TOP_INDEX=0 command. (73)");

	m_structValidMsgs[74].nStatus = 2; // fail
	m_structValidMsgs[74].strName = _T("HEXCMD directives are not valid in onboard schedules. (74)");

	m_structValidMsgs[75].nStatus = 2; // error
	m_structValidMsgs[75].strName = _T("Exceeded the maximum number of RTCS procedures that can be declared: %s. (75)");

	m_structValidMsgs[76].nStatus = 2; // error
	m_structValidMsgs[76].strName = _T("Invalid RTCS label value: %s (76)");

	m_structValidMsgs[77].nStatus = 2; // error
	m_structValidMsgs[77].strName = _T("Error parsing line in set file. %s (77)");

	m_structValidMsgs[78].nStatus = 2; // error
	m_structValidMsgs[78].strName = _T("Not enough time has elapsed before the current mnemonic can be commanded: %s (78)");

	m_structValidMsgs[79].nStatus = 2; // error
	m_structValidMsgs[79].strName = _T("Command not allowed in RTCSs procedures: %s (79)");

	m_structValidMsgs[80].nStatus = 2; // error
	m_structValidMsgs[80].strName = _T("The stop address is not an integral multiple from east space look address. (80)");

	m_structValidMsgs[81].nStatus = 2; // error
	m_structValidMsgs[81].strName = _T("Difference between N/S start and stop is not a integral multiple of 28. (81)");

	m_structValidMsgs[82].nStatus = 1; // warning
	m_structValidMsgs[82].strName = _T("Difference between E/W start and stop is less than 4. (82)");

	m_structValidMsgs[83].nStatus = 2; // error
	m_structValidMsgs[83].strName = _T("Imager star sense definition exceeds FOV limits for the East: %s. (83)");

	m_structValidMsgs[84].nStatus = 2; // error
	m_structValidMsgs[84].strName = _T("Imager star sense definition exceeds FOV limits for the West: %s. (84)");

	m_structValidMsgs[85].nStatus = 2; // error
	m_structValidMsgs[85].strName = _T("Imager star sense definition exceeds FOV limits for the North: %s. (85)");

	m_structValidMsgs[86].nStatus = 1; // warning
	m_structValidMsgs[86].strName = _T("Imager star sense definition exceeds FOV limits for the South: %s. (86)");

	m_structValidMsgs[87].nStatus = 2; // error
	m_structValidMsgs[87].strName = _T("Sounder star sense definition exceeds FOV limits for the East: %s. (87)");
	
	m_structValidMsgs[88].nStatus = 2; // error
	m_structValidMsgs[88].strName = _T("Sounder star sense definition exceeds FOV limits for the West: %s. (88)");
	
	m_structValidMsgs[89].nStatus = 1; // warning
	m_structValidMsgs[89].strName = _T("Sounder star sense definition exceeds FOV limits for the North: %s. (89)");
	
	m_structValidMsgs[90].nStatus = 2; // error
	m_structValidMsgs[90].strName = _T("Sounder star sense definition exceeds FOV limits for the South: %s. (90)");

	m_structValidMsgs[91].nStatus = 1; // error
	m_structValidMsgs[91].strName = _T("The E/W Frame boundary is less than 1600 incr from the space look address: %s. (91)");
	
	m_structValidMsgs[92].nStatus = 1; // warning
	m_structValidMsgs[92].strName = _T("The keyword \"prc_start\" must be set equal to \"gmt()\": %s. (92)");

	m_structValidMsgs[93].nStatus = 1; // warning
	m_structValidMsgs[93].strName = _T("##ground directive found without valid parameter: %s. (93)");

	m_structValidMsgs[94].nStatus = 1; // warning
	m_structValidMsgs[94].strName = _T("This command is not allowed: %s (94)");

	m_structValidMsgs[95].nStatus = 1; // warning
	m_structValidMsgs[95].strName = _T("The memory dump start address is greater than the memory dump stop address: %s. (95)");

	m_structValidMsgs[96].nStatus = 2; // error
	m_structValidMsgs[96].strName = _T("Time tag commands are spaced too close: %s. (96)");

	m_structValidMsgs[97].nStatus = 1; // warning
	m_structValidMsgs[97].strName = _T("The memory address states do not match: %s. (97)");

	m_structValidMsgs[98].nStatus = 1; // warning
	m_structValidMsgs[98].strName = _T("Not enough time is allowed to uplink commands: %s secs. (98)");

	m_structValidMsgs[99].nStatus = 1; // warning
	m_structValidMsgs[99].strName = _T("Mnemonic not found in command frame report file: %s. (99)");

	m_structValidMsgs[100].nStatus = 2; // error
	m_structValidMsgs[100].strName = _T("Not enough time allowed for the previous CLS to execute. (100)");

	// max number of elements is 101.
} 

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewGblValData::GetValStatus 
//	Description :	This routine retrieves the message severity level.   
//					
//	Return :		
//			int	-	Severity status.
//					-1	-	Index out of range
//					0:2	-	Validation severity
//	Parameters :	
//			int -	message index. 
//	
//	Note :	
//			Currently, the maximum number of validation messages is 99.		
//////////////////////////////////////////////////////////////////////////
***************************/
int CNewGblValData::GetValStatus(int nIndex){
	
	int nRetStatus;

	if ( nIndex >= MAX_NUM_MEMBERS){
		nRetStatus = -1;
	} else {
		nRetStatus = m_structValidMsgs[nIndex].nStatus;
	}

	return nRetStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewGblValData::GetValMessage
//	Description :	This routine retrieves the validation message.
//									
//
//	Return :		
//			CString	-	Validation message.
//					
//	Parameters :	
//			int -	message index. 
//	
//	Note :	
//			Currently, the maximum number of validation messages is 99.		
//////////////////////////////////////////////////////////////////////////
***************************/	
CString CNewGblValData::GetValMessage(const int nIndex){
	
	CString strRetMsg;

	if ( nIndex >= MAX_NUM_MEMBERS){
		strRetMsg = _T("Validation Error: Message number out of range");
	} else{
		strRetMsg = m_structValidMsgs[nIndex].strName;
	}

	return strRetMsg;
}

/*****************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		BOOL CNewGblValData::WriteValidationMsg
//
//	Description: This routine writes the messages to the events window.
//				 The error message is referenced by number.  Which is
//               stored in the global CNewGblValData class.  Associated 
//               with each message is also the status (info, warning, 
//				 or an error).
//
//	Return :		
//			BOOL	-	Always TRUE.
//					
//	Parameters :	
//			int nCurrLineNum	-	Line number in file,
//			int nMsgIndx		-	Message number
//	
//	Note :	
//			Currently, the maximum number of validation messages is 99.		
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CNewGblValData::WriteValidationMsg(int nCurrLineNum, const int nMsgIndx){
	CString strMsg;
	int nValidStatus = GetValStatus(nMsgIndx);
	BOOL eReturnStatus = TRUE;

	nCurrLineNum += m_NumPrologLines;

	// Check what is the current validation status.
	// Set value to current if the current value is lower than the value passed in. 

	if (1 == nValidStatus){
		if (m_nValidStatus < 1){
			m_nValidStatus = 1;
		}
		m_nNumWarnings++;
		// strMsg.Format(_T("%s"),	GetValMessage(nMsgIndx));
		GetAppOutputBar()->OutputToBuild(GetValMessage(nMsgIndx),
			COutputBar::WARNING, nCurrLineNum);
		strMsg.Format(_T("%s%d%s"),ctstrWARNINGMSG, nCurrLineNum,
			GetValMessage(nMsgIndx));

	} else if (2 == nValidStatus){
		if (m_nValidStatus < 2){
			m_nValidStatus = 2;
		}
		m_nNumErrors++;
		GetAppOutputBar()->OutputToBuild(GetValMessage(nMsgIndx),
			COutputBar::FATAL, nCurrLineNum);
		strMsg.Format(_T("%s%d%s"), ctstrERRORMSG, nCurrLineNum,
			GetValMessage(nMsgIndx));
	} else if (0 == nValidStatus) {
		GetAppOutputBar()->OutputToBuild(GetValMessage(nMsgIndx),
			COutputBar::INFO, nCurrLineNum);
		strMsg.Format(_T("%s%d%s"), ctstrERRORMSG, nCurrLineNum,
			GetValMessage(nMsgIndx));
	} else {
		// Invalid message # 
		// Put out an error message to outputbar.
		strMsg = _T("Validation Error: Message number out of range");
		GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
	}

//	AppendTextToProdFile(strMsg);
	return eReturnStatus;
}

/*****************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CNewGblValData::WriteValidationMsg
//
//	Description: This routine outputs the messages to the events window.
//				 The error message is referenced by number.  Which is
//               stored in the global CNewGblValData class.  Associated 
//               with each message is also the severity status.   
//               
//
//	Return :		
//			BOOL	-	Always TRUE.
//					
//	Parameters :	
//			int nCurrLineNum 	-	Line number in file,
//			int nMsgIndx		-	Message number
//			CString strSubMsg	-	Message to be added to validation message	
//
//	Note :	
//			Currently, the maximum number of validation messages is 99.		
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL  CNewGblValData::WriteValidationMsg(int nCurrLineNum, const int nMsgIndx,
										 CString strSubMsg){

	CString strMsg;
	int nValidStatus = GetValStatus(nMsgIndx);
	bool bReturnStatus = true;
	nCurrLineNum += m_NumPrologLines;

	if (1 == nValidStatus){
		if (m_nValidStatus < 1){
			m_nValidStatus = 1;
		}
		m_nNumWarnings++;
		CString strTxtMsg;
		CString strErrMsg = GetValMessage(nMsgIndx);
		strTxtMsg.Format(strErrMsg,strSubMsg);
		GetAppOutputBar()->OutputToBuild(strTxtMsg, COutputBar::WARNING, nCurrLineNum);
		strMsg.Format(_T("%s%d%s"),ctstrWARNINGMSG, nCurrLineNum, strTxtMsg);
	} else if (2 == nValidStatus){
		if (m_nValidStatus < 2){
			m_nValidStatus = 2;
		}
		m_nNumErrors++;
		CString strTxtMsg;
		CString strErrMsg = GetValMessage(nMsgIndx);
		strTxtMsg.Format(strErrMsg,strSubMsg);
		GetAppOutputBar()->OutputToBuild(strTxtMsg, COutputBar::FATAL, nCurrLineNum);
		strMsg.Format(_T("%s%d%s"), ctstrERRORMSG, nCurrLineNum, strTxtMsg);
	} else if (0 == nValidStatus){
		CString strTxtMsg;
		CString strErrMsg = GetValMessage(nMsgIndx);
		strTxtMsg.Format(strErrMsg,strSubMsg);
		GetAppOutputBar()->OutputToBuild(strTxtMsg, COutputBar::INFO, nCurrLineNum);
		strMsg.Format(_T("%s%d%s"), ctstrERRORMSG, nCurrLineNum, strTxtMsg);
	} else {
		// Invalid message # 
		// Put out an error message to outputbar.
		strMsg = _T("Validation Error: Message number out of range");
		GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
	}


//	AppendTextToProdFile(strMsg);
	return bReturnStatus;
}

/*****************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CNewGblValData::WriteRptHeader()
//
//	Description: This routine writes the validation header to the events 
//				 window.
//				 
//	Return :		
//			BOOL	-	Always TRUE.
//					
//	Parameters :	
//			void	-	none.
//
//	Note :	
//			Currently, the maximum number of validation messages is 99.		
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CNewGblValData::WriteRptHeader(){
	BOOL eReturn_Status =  TRUE;
	CString strMsg;
	enum eValidStatus eStatus = FAIL;

	const CString strDesc(_T("Validation Report File"));
//	AppendDescProlog2ProdFile(strDesc);
	GetAppOutputBar()->OutputToBuild(strDesc, COutputBar::INFO);
	
	switch (m_nValidStatus){
		case 0 : eStatus = PASSED;  break;
		case 1 :
		{
			eStatus = WARNING;
			eReturn_Status = FALSE;
		}
		break;
		
		case 2 :
		{
			eStatus = FAIL;
			eReturn_Status = FALSE;
		}
		break;
	}
	
	strMsg.Format(_T("Number of Warnings: %d"), m_nNumWarnings);
//	AppendDescProlog2ProdFile(strDesc);
	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::INFO);

	strMsg.Format(_T("Number of Errors: %d"), m_nNumErrors);
//	AppendDescProlog2ProdFile(strDesc);
	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::INFO);

//	enum eValidLoc eLoc = ONBOARD;
//	WriteInputValidationStatus(eStatus, eLoc);
//	WriteProdValidationStatus(eStatus, eLoc);

	return eReturn_Status;
}

/*****************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	BOOL CNewGblValData::WriteRptHeader
//
//	Description: This routine writes the validation header to the events 
//				 window.  Inaddition, the severity status of the file is 
//				 returned.
//				 
//	Return :		
//			BOOL	-	Always TRUE.
//					
//	Parameters :	
//			enum eValidStatus &eStatus	-	Returns severity status.
//
//	Note :	
//			Currently, the maximum number of validation messages is 99.		
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CNewGblValData::WriteRptHeader(enum eValidStatus &eStatus){
	BOOL eReturn_Status =  TRUE;
	CString strMsg;

	const CString strDesc(_T("Validation Report File"));
	GetAppOutputBar()->OutputToBuild(strDesc, COutputBar::INFO);
	GetAppOutputBar()->OutputToEvents(strDesc, COutputBar::INFO);

	
	switch (m_nValidStatus){
		case 0 : eStatus = PASSED;  break;
		case 1 : eStatus = WARNING; break;
		case 2 : eStatus = FAIL;    break;
	}
	
	strMsg.Format(_T("Number of Warnings: %d"), m_nNumWarnings);
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::INFO);

	strMsg.Format(_T("Number of Errors: %d"), m_nNumErrors);
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::INFO);
	return eReturn_Status;
}

/*****************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CNewGblValData::SetNumPrologLines
//
//	Description: This routine stores the number of prolog lines in a member 
//				  variable.
//				 
//	Return :		
//			void	-	none.
//					
//	Parameters :	
//			const int nNumLines	-	number of prolog lines 
//
//	Note :	
//			Currently, the maximum number of validation messages is 99.		
//////////////////////////////////////////////////////////////////////////
***************************/
void CNewGblValData::SetNumPrologLines(const int nNumLines)
{
	m_NumPrologLines = nNumLines;
}

/*****************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 1/26/01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	BOOL   CNewGblValData::WriteValidationMsg
//
//	Description: This routine outputs the validaiton message to the events 
//				 window.  The line number, the validation message, and 
//               severity are passed into the routine. 
//				 				 
//	Return :		
//			BOOL	-	Always TRUE.
//					
//	Parameters :	
//			 int nCurrLineNum		-	The line number of the text file
//			 const CString strMsg	-	The validation message
//			 const int nStatus		-	The severity
//
//	Note :	
//			Currently, the maximum number of validation messages is 99.		
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL   CNewGblValData::WriteValidationMsg(int nCurrLineNum,
										  const CString strMsg,
										  const int nStatus){
	BOOL eReturnStatus = TRUE;
	nCurrLineNum += m_NumPrologLines;
	CString strErrMsg(strMsg);

	if (1 == nStatus){
		if (m_nValidStatus < 1){
			m_nValidStatus = 1;
		}
		m_nNumWarnings++;
		GetAppOutputBar()->OutputToBuild(strErrMsg, COutputBar::WARNING, nCurrLineNum);
	} else if (2 == nStatus){
		if (m_nValidStatus < 2){
			m_nValidStatus = 2;
		}
		m_nNumErrors++;
		GetAppOutputBar()->OutputToBuild(strErrMsg, COutputBar::FATAL, nCurrLineNum);
	} else if (0 == nStatus) {
		GetAppOutputBar()->OutputToBuild(strErrMsg, COutputBar::INFO, nCurrLineNum);
	} else {
		strErrMsg = _T("Invalid error status number."); 
		GetAppOutputBar()->OutputToBuild(strErrMsg, COutputBar::FATAL, nCurrLineNum);
	}
	return eReturnStatus;
}

