// 
// NewGblValData.h: interface for the CNewGblValData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NEWGBLVALDATA_H__6D30DF81_517B_11D3_9776_00104B8CF942__INCLUDED_)
#define AFX_NEWGBLVALDATA_H__6D30DF81_517B_11D3_9776_00104B8CF942__INCLUDED_

#include "NewSchedFile.h"
//#include "TextFile.h"

#define MAX_NUM_MEMBERS 101

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef struct tagValidMsgs{
	int nStatus;
	CString strName;
}VALID_MSGS;

class CNewGblValData 
{

public:
	void SetNumPrologLines(const int nNumLines);

	typedef enum ValOpEnum {VALOP_INV=-1, 
		VALOP_DIR=0,
		VALOP_TT=1, VALOP_DOY=1,
		VALOP_PROLOG=2, 
		VALOP_CMD=3, VALOP_PARAM=3,  
	};

	void    Init(void);
	        CNewGblValData();
			CNewGblValData(const CString strInputFileName);
//			CNewGblValData(const CNewSchedFile &Other);
//			CNewGblValData(const CNewGblValData &Other);
	virtual ~CNewGblValData();
//	CString GetValOptString(int nFT, int i);
	int	    GetValStatus(int nIndex);
	CString GetValMessage(const int nIndex);
	BOOL    WriteValidationMsg(int nCurrLineNum, const int nMsgIndx);
	BOOL    WriteValidationMsg(int nCurrLineNum, const int nMsgIndx, CString strSubMsg);
	BOOL    WriteValidationMsg(int nCurrLineNum, const CString strMsg, const int nStatus);
	BOOL    WriteValidationMsg(const int nMsgIndx);
	BOOL    WriteRptHeader(); 
	BOOL    WriteRptHeader(enum eValidStatus &eStatus);

private:
	int        m_NumPrologLines;
	CString    m_strFileName;
	int        m_nValidStatus;
	int        m_nNumWarnings;
	int        m_nNumErrors;
	VALID_MSGS m_structValidMsgs[MAX_NUM_MEMBERS];
};

#endif // !defined(AFX_NEWGBLVALDATA_H__6D30DF81_517B_11D3_9776_00104B8CF942__INCLUDED_)
