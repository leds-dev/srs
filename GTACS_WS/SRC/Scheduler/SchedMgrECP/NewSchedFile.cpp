/***************************
//////////////////////////////////////////////////////////////////////
// NewSchedFile.cpp : implementation file
// (c) 2001 Frederick J. Shaw                                       // 
// Prologs updated						                            //
//////////////////////////////////////////////////////////////////////
***************************/

#include "stdafx.h"
#include "schedmgrecp.h"
#include "NewSchedFile.h"
#include "CmnHdr.h"
// #include "SplitPath.h"
#include "SchedTime.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
/***************************
/////////////////////////////////////////////////////////////////////////////
// CNewSchedFile message handlers
/////////////////////////////////////////////////////////////////////////////
// CNewSchedFile

// enum eFileType {CLS, STOL, FRAME, STAR, IMC, RTCS-CLS, RTCS-STO, RPT, INV, UNDEF};
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// The CNewSchedFile class contains a member variable that points to the input text file
// and the product text file.  It also contains member variables for the input and 
// product type.  The valid choices are {CLS, STOL, FRAME, STAR, IMC, RTCS-CLS, 
// RTCS-STO, RPT, INV, UNDEF}.

//////////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 04-18-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewSchedFile::CNewSchedFile(CString strFileName)	
//	Description :	Sets the input path and file name.  Based on the file extension, 
//					the input file type is initialized.  The product file name and path
//                  are left empty.  The product file type is undefined.
//					
//	Return :		constructor		-	
//	Parameter :		CString  full path name.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
CNewSchedFile::CNewSchedFile(CString strFileName){
	m_strFileName = strFileName;
	m_nCurrentFileLineNum = 0;
	m_eFileType   = UNDEF;

	COXUNC	FilePath(strFileName);
	CString strTemp = FilePath.Directory();
	CString strFullPath(FilePath.Full());
	CString strDir(FilePath.Directory());
	CString strExt(FilePath.Extension());
	CString strCMDExt(_T(""));
	CString strCLSExt(_T(""));
	CString strSTOLExt(_T(""));

	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nCLS])->GetDocString(strCLSExt, CDocTemplate::filterExt);
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nSTOL])->GetDocString(strSTOLExt, CDocTemplate::filterExt);
	if (strExt.CompareNoCase(strCLSExt)){
		m_eFileType   = CLS;
	} else if (strExt.CompareNoCase(strCMDExt)){
		m_eFileType   = STOL;
	} else if (strExt.CompareNoCase(strSTOLExt)){
		m_eFileType   = STOL;
	}
};

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 04-18-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewSchedFile::Open	
//	Description :	 This routine opens and loads the text file.
//					
//	Return :		BOOL		
//						TRUE	-	success
//						FALSE	-	fail
//	Parameter :		CTextFile::eOpenFT eOpenMode	
					Open status	-	 Readonly, ReadWrite, Create	.
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL CNewSchedFile::Open(CTextFile::eOpenFT eOpenMode){
	CFileException	cFileException;
	BOOL            bDataOK = FALSE;
	CString			strErrorMsg;

	if (!m_strFileName.IsEmpty()){
		bDataOK = Load(m_strFileName, eOpenMode, &cFileException);
		if (!bDataOK){	
		// Output error message from loading file.
			strErrorMsg = ExceptionErrorText(&cFileException);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, m_strFileName);
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
		} else {
			ExtractProlog();
		}
	} else {
		strErrorMsg = _T("Text file name not set.");
		GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
		GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
	}

	return bDataOK;
};

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 04-18-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewSchedFile::ConvertFullPathNameToUnix
//	Description :	This routine converts a WinX pathname to a Unix format. .
//					
//	Return :	bool		
//					true	-	success
//					false	-	fail
//	Parameter :		
//				const CString strFileNameIn	- WinX pathname.	
//				CString &strFileNameOut		- Unix formated pathname.		
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
bool CNewSchedFile::ConvertFullPathNameToUnix(const CString strFileNameIn, CString &strFileNameOut)
{
	bool bReturnStatus = true; // Set to success

	/* Get the file and path name.
	   Extract the drive name and store. */

	// Break the schedule filename up into its components.
	TCHAR fDrive[_MAX_DRIVE];
	TCHAR fDir[_MAX_DIR];
	TCHAR fFname[_MAX_FNAME];
	TCHAR fExt[_MAX_EXT];
	_tsplitpath_s(strFileNameIn, fDrive, fDir, fFname, fExt);

	CString strfDir(strFileNameIn);

	if (0 != wcslen(fDrive)){
		// Retrieve the remote name from the drive name.
		DWORD BufferSize = _MAX_PATH; // size of buffer
		TCHAR szBuff[_MAX_PATH];
		REMOTE_NAME_INFO  *lpBuffer = (REMOTE_NAME_INFO *)&szBuff;
		DWORD res;
		if ((res = WNetGetUniversalName(fDrive,
							 REMOTE_NAME_INFO_LEVEL,
							 lpBuffer,
							 &BufferSize)) == NO_ERROR){
			strfDir = (LPTSTR)lpBuffer->lpUniversalName;
			int nIndex = 0;
			if (0 == (nIndex = strfDir.Find(_T("\\")))){
				strfDir.Delete(nIndex);
				if (0 == (nIndex = strfDir.Find(_T("\\")))){
					strfDir.Delete(nIndex);
				}
			}

			//	Convert beginning directory name to a UNIX environmental
			//	variable.
			if (-1 != (nIndex = strfDir.Find(_T("\\")))){
				strfDir.Delete(0, nIndex + 1);
				// strfDir.Insert(0, '$');
			}
			strfDir += fDir;
		} else {

			strfDir = fDir;
			int nIndex = 0;
			//	Convert beginning directory name to a UNIX environmental
			//	variable.
			if (0 == (nIndex = strfDir.Find(_T("\\")))){
				strfDir.Delete(nIndex, 1);
				// strfDir.Insert(nIndex, '$');
			}
		}
	} else{
		int nIndex = 0;
		if (-1 != (nIndex = strfDir.Find(_T("\\")))){
			strfDir.Delete(nIndex);
			if ( -1 != (nIndex = strfDir.Find(_T("\\")))){
				strfDir.Delete(nIndex);
			}
		}

		//	Convert beginning directory name to a UNIX environmental
		//	variable.
		if ( -1 != (nIndex = strfDir.Find(_T("\\")))){
			strfDir.Delete(0, nIndex + 1);
			// strfDir.Insert(0, '$');
		}
	}

	strFileNameOut = strfDir;
	return bReturnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 04-18-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewSchedFile::FileEOF
//	Description :	This routine checks if at end of file.
//					
//	Return :	BOOL		
//					TRUE	-	At end of file.
//					FALSE	-	Not at end
//	Parameter :		
//				void	- none.	
//		
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL  CNewSchedFile::FileEOF(){

	if (m_nCurrentFileLineNum >= GetLineCount())
		return TRUE;
	else
		return FALSE;
};

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Frederick J. Shaw		Date : 04/19/2001			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		NewSchedFile::GetFileDirNext
//	Description :	Returns a directive line.  Checks for a continuation line.
//					Concates the lines.
//					Searches input line number for continuation character.  If one is
//					found, gets next line and appends to strBuffer.  Repeats until a
//					line is retrieved which does not have a continuation char.
//	Return :		TRUE; if not at end of file
//	                FALSE; if end of file.
//	Parameters :	CString; concantenated  STOL line.
//	Note :			
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
***************************/
BOOL  CNewSchedFile::GetFileDirNext(CString & strBuffer){

	BOOL bReturn_Status = FALSE;
	strBuffer.Empty();
//		int nFileLineCount = GetFileLineCount();

	if (m_nCurrentFileLineNum >= GetLineCount()){
		bReturn_Status = FALSE;
	}else
	{
		strBuffer = GetTextAt(m_nCurrentFileLineNum);
		m_nCurrentFileLineNum++;
		CString strTrimmedLine(strBuffer);

		strTrimmedLine.TrimRight();
		strTrimmedLine.TrimLeft();
		if (!strTrimmedLine.IsEmpty() && (_T('#') != strTrimmedLine[0])){
			// Remove trailing comment.
			int nPos = 0;
			if ( -1 != (nPos = (strBuffer.Find(_T('#'), 0)))){
				strBuffer.Delete(nPos, strBuffer.GetLength() - nPos);
			}
		}

		strBuffer.TrimRight();
		// Don't forget the newline.  Subtract one character from end.
		int nEndOfLine = strBuffer.GetLength() - 1;
		if (nEndOfLine < 0) nEndOfLine = 0;
		
		int nTemp = 0;
		while ( !FileEOF() && ((nTemp = strBuffer.Find(_T('\\'))) == nEndOfLine))
		{
			strBuffer.Remove(_T('\\'));
			CString strTemp = GetTextAt(m_nCurrentFileLineNum);
			int nPos = 0;
			if ( -1 != (nPos = (strTemp.Find(_T('#'), 0)))){
				strTemp.Delete(nPos, strTemp.GetLength() - nPos);
			}
			m_nCurrentFileLineNum++;
			strTemp.TrimRight();
			strTemp.TrimLeft();
			strBuffer += _T(" ") + strTemp;
			strBuffer.TrimRight();
			nEndOfLine = strBuffer.GetLength() - 1;
			if (nEndOfLine < 0) nEndOfLine = 0;
		}

		if (m_nCurrentFileLineNum <= GetLineCount())
		{
			bReturn_Status = TRUE;
		} 
	}
	return bReturn_Status;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 04-18-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CNewSchedFile::ConvertFullPathNameToUnix
//	Description :	This routine converts a WinX pathname to a Unix format. .
//					
//	Return :	bool		
//					true	-	success
//					false	-	fail
//	Parameter :		
//				const CString strFileNameIn	- WinX pathname.	
//				CString &strFileNameOut		- Unix formated pathname.		
//	Note :			
//////////////////////////////////////////////////////////////////////////
***************************/
	//////////////////////////////////////////////////////////////////////////
	//	Author :Frederick J. Shaw		Date : 		version 1.0
	//////////////////////////////////////////////////////////////////////////
	//	Function : 
	//
	//	Description :	
	//	  
	//	  
	//	  
	//    
	//    
	//    
	//    
	//    
	//    
 	//					
	//	Return : BOOL		
	//	               
	//	Parameters : 
	//
	//	Note :			
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////
/*
BOOL  CNewSchedFile::AppendDescProlog2File(CString strDesc)
{
	CString strText;
	CString strErrorText;

	BOOL eReturn_Status =
		((GetProlog())->GetDescription())->AddDir(strDesc, &strErrorText);
	return eReturn_Status;
}
*/

