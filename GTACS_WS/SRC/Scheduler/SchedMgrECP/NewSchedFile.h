#if !defined(AFX_NEWSCHEDFILE_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_)
#define AFX_NEWSCHEDFILE_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// NewSchedFile.h : header file
//

#include "TextFile.h"
/////////////////////////////////////////////////////////////////////////////
// CNewSchedFile 

class CNewSchedFile : public CTextFile 
{
// Construction
public:

	typedef enum eFileType {CLS, STOL, STO, FRAME, STAR, IMC, RTCS_SET, RPT, INV, UNDEF};


	CNewSchedFile(CString strFileName);
	CString &       GetFileName(void){ return m_strFileName;};
	bool            SetFileName(const CString strFileName){ m_strFileName = strFileName;};
	bool            ConvertFullPathNameToUnix(const CString strFileNameIn, CString &strFileNameOut);
	int             GetCurrentFileLineNum(void){return m_nCurrentFileLineNum;};
	void            SetCurrentFileLineNum(int nLineNum){m_nCurrentFileLineNum = nLineNum;};
	BOOL            FileEOF(void);
	BOOL            GetFileDirNext(CString & strDirLine);
//	BOOL            AppendDescProlog2File(CString strDesc);
	BOOL			WasExectimeFoundInFile(){return GetProlog()->WasExectimeFound();};
	BOOL			WasBeginFoundInFile(){return GetProlog()->WasBeginFound();};
	BOOL			WasEndFoundInFile(){return GetProlog()->WasEndFound();};
	BOOL			WasUpdateFoundInFile(){return GetProlog()->WasUpdateFound();};
	BOOL            AddPrologStarttime2File(CString strStartTime){return AddPrologStarttime(strStartTime);};
	BOOL            AddPrologStoptime2File(CString strStopTime){return AddPrologStoptime(strStopTime);};
	CStringArray *	GetUpdateTextArray(){return ((GetProlog()->GetUpdate())->GetTextArray());};
	
protected:

	BOOL Open(CTextFile::eOpenFT eOpenMode);
	
private:

	// member variables
	CString         m_strFileName;
	enum eFileType	m_eFileType;
	int             m_nCurrentFileLineNum;
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWSCHEDFILE_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_)
