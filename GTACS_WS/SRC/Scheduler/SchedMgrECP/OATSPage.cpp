/**
/////////////////////////////////////////////////////////////////////////////
// OATSPage.cpp : implementation of the OATSPage class.                    //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// OATSPage.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class will allow the user to choose OATS parameters for various
// OATS request.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"
#include "OATSPage.h"
#include "FinishDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COATSPage property page

IMPLEMENT_DYNCREATE(COATSPage, CResizablePage)

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COATSPage::COATSPage
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
COATSPage::COATSPage() : CResizablePage(COATSPage::IDD)
{
	//{{AFX_DATA_INIT(COATSPage)
	m_nOATSDur = 0;
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COATSPage::~COATSPage
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
COATSPage::~COATSPage()
{
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COATSPage::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//
//  Returns :		void	-  
//
//  Parameters : 
//           CDataExchange* pDX
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void COATSPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COATSPage)
	DDX_Control(pDX, IDC_OATSSTARTTIME_EDIT, m_cStartTimeEdit);
	DDX_Text(pDX, IDC_OATSDURATION_EDIT, m_nOATSDur);
	DDV_MinMaxInt(pDX, m_nOATSDur, 0, 14400);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COATSPage, CResizablePage)
	//{{AFX_MSG_MAP(COATSPage)
	ON_BN_CLICKED(IDC_OATSSOCC_RADIO, OnOatsRadio)
	ON_BN_CLICKED(IDC_OATSWALLOPS_RADIO, OnOatsRadio)
	ON_BN_CLICKED(IDC_OATSBACKUP_RADIO, OnOatsRadio)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COATSPage message handlers

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COATSPage::Init
//  Description :	This routine initializes the member variables based on 
//					the passed parameters.
//
//  Returns :		void	-
//
//  Parameters :
//		BOOL bFirstWizPage	-	first display page.
//		BOOL bLastWizPage	-	last display page.
//		BOOL bVal			-	valid file.
//		eValidLoc eValLoc	-	exectuion type.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COATSPage::Init(BOOL bFirstWizPage, BOOL bLastWizPage)
{
	m_bFirstWizPage	= bFirstWizPage;
	m_bLastWizPage	= bLastWizPage;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COATSPage::OnInitDialog
//  Description :	This routine creates the OATSPage dialog window.  
//
//	Return :		BOOL	-	TRUE if successful
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL COATSPage::OnInitDialog() 
{
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	CResizablePage::OnInitDialog();
	
	AddAnchor(IDC_OATS_GROUP, TOP_LEFT, BOTTOM_RIGHT);

	AddAnchor(IDC_OATSMACHINE_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_OATSSOCC_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSSOCC_COMBO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSWALLOPS_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSWALLOPS_COMBO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSBACKUP_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSBACKUP_COMBO, TOP_LEFT, TOP_LEFT);

	AddAnchor(IDC_OATSDATA_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_OATSINST_STATIC, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSINST_COMBO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSSTARTTIME_STATIC, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSSTARTTIME_EDIT, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSDURATION_STATIC, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSDURATION_EDIT, TOP_LEFT, TOP_LEFT);

	AddAnchor(IDC_OATSLOC_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_OATSLOC_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OATSLOC_STATIC, TOP_LEFT, TOP_RIGHT);

//	int	nIndex;

	for(int  nIndex=0; nIndex<nMaxOATS[0]; nIndex++ )
		((CComboBox*)GetDlgItem(IDC_OATSSOCC_COMBO))->AddString(GetAppRegOATS()->m_strTitle[0][nIndex]);

	((CComboBox*)GetDlgItem(IDC_OATSSOCC_COMBO))->SetCurSel(0);

	for(int nIndex=0; nIndex<nMaxOATS[1]; nIndex++ )
		((CComboBox*)GetDlgItem(IDC_OATSWALLOPS_COMBO))->AddString(GetAppRegOATS()->m_strTitle[1][nIndex]);

	((CComboBox*)GetDlgItem(IDC_OATSWALLOPS_COMBO))->SetCurSel(0);

	for(int nIndex=0; nIndex<nMaxOATS[2]; nIndex++ )
		((CComboBox*)GetDlgItem(IDC_OATSBACKUP_COMBO))->AddString(GetAppRegOATS()->m_strTitle[2][nIndex]);

	((CComboBox*)GetDlgItem(IDC_OATSBACKUP_COMBO))->SetCurSel(0);

	((CButton *)GetDlgItem(IDC_OATSSOCC_RADIO))->SetWindowText(GetAppRegSite()->m_strTitle[0]);
	((CButton *)GetDlgItem(IDC_OATSWALLOPS_RADIO))->SetWindowText(GetAppRegSite()->m_strTitle[1]);
	((CButton *)GetDlgItem(IDC_OATSBACKUP_RADIO))->SetWindowText(GetAppRegSite()->m_strTitle[2]);	

	m_cStartTimeEdit.SetMask(_T("####/###/##:##:##.###"));
	m_cStartTimeEdit.SetPromptSymbol(_T('0'));

	CString strTime;
	CTime curTime(CTime::GetCurrentTime());
	strTime = curTime.FormatGmt(_T("%Y-%j/12:00:00.000"));
	m_cStartTimeEdit.SetWindowText(strTime);
	// m_cStartTimeEdit.SetWindowText(_T("2007/050/12:00:00.000"));
	// m_cStartTimeEdit.SetWindowText(GetAppRegMakeSched()->m_strOATSStartTime);

	m_nOATSDur = 10;
	CString strOATSDur;
	strOATSDur.Format(_T("%d"), m_nOATSDur);
	GetDlgItem(IDC_OATSDURATION_EDIT)->GetWindowText(strOATSDur);
	GetOATSDefaultLocation();
	OnOatsRadio();

	((CComboBox*)GetDlgItem(IDC_OATSINST_COMBO))->AddString(_T("Imager"));
	((CComboBox*)GetDlgItem(IDC_OATSINST_COMBO))->AddString(_T("Sounder"));
	((CComboBox*)GetDlgItem(IDC_OATSINST_COMBO))->SetCurSel(0);
	switch( pSheet->GetFunc() )
	{
		case nReqIMC:
		{
			GetDlgItem(IDC_OATSDATA_GROUP)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSINST_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSINST_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSSTARTTIME_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSSTARTTIME_EDIT)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSDURATION_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSDURATION_EDIT)->EnableWindow(FALSE);
			break;
		}
		case nReqSFCS:
		{
			GetDlgItem(IDC_OATSMACHINE_GROUP)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSSOCC_RADIO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSSOCC_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSWALLOPS_RADIO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSWALLOPS_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSBACKUP_RADIO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSBACKUP_COMBO)->EnableWindow(FALSE);
			break;
		}
		case nReqEclipse:
		case nReqIntrusion:
		{
			GetDlgItem(IDC_OATSINST_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSINST_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSMACHINE_GROUP)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSSOCC_RADIO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSSOCC_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSWALLOPS_RADIO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSWALLOPS_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSBACKUP_RADIO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSBACKUP_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSDURATION_STATIC)->SetWindowText(_T("Duration (Days):"));
			break;
		}
		default : ASSERT (FALSE); break;
	}

	ResizeFilenames();
	UpdateData(FALSE);
	
	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COATSPage::OnSetActive  
//  Description :	This routine disables/enables, as appropriate, the 
//					different window controls.
//                  
//  Returns :		BOOL	-	TRUE  if successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL COATSPage::OnSetActive()
{
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	// Setup the proper Wizard Buttons
	DWORD dWizFlags = PSWIZB_FINISH;
	if( (m_bFirstWizPage) && (m_bLastWizPage) )
		dWizFlags = PSWIZB_FINISH;
	else if( (m_bFirstWizPage) && (!m_bLastWizPage) )
		dWizFlags = PSWIZB_NEXT;
	else if( (!m_bFirstWizPage) && (m_bLastWizPage) )
		dWizFlags = PSWIZB_BACK | PSWIZB_FINISH;
	else if( (!m_bFirstWizPage) && (!m_bLastWizPage) )
		dWizFlags = PSWIZB_BACK | PSWIZB_NEXT;

	pSheet->SetWizardButtons(dWizFlags);			
	ResizeFilenames();
	
	return CResizablePage::OnSetActive();
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COATSPage::OnKillActive  
//  Description :	This routine calls the CResizablePage::OnKillActive 
//					routine.
//
//  Returns :		BOOL	-	 TRUE  if successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL COATSPage::OnKillActive()
{
	return CResizablePage::OnKillActive();
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COATSPage::OnSize  
//  Description :   This routine resizes the dialog window based on the 
//					the coordinates passed into the routine.	
//                  
//  Returns :		void	-
//  Parameters : 
//           UINT nType	-	type of window resizing requested.
//           int cx		-	new width of window.
//           int cy		-	new height of window.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COATSPage::OnSize(UINT nType, int cx, int cy) 
{
	CResizablePage::OnSize(nType, cx, cy);
	ResizeFilenames();
}	

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COATSPage::ResizeFilenames  
//  Description :   This routine is called to resize the pathname is 
//					displayed. So that it fits within the dialog control. 	
//                  
//  Returns :		void	-
//
//  Parameters :  
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COATSPage::ResizeFilenames()
{
	CGenUpdSheet*	pSheet = (CGenUpdSheet*)GetParent();
	CString			strIMCFilename;

	switch( pSheet->GetFunc() )
	{
		case nReqSFCS		:
		case nReqEclipse	:
		case nReqIntrusion	:	strIMCFilename = (pSheet->GetOutputPage())->GetStrPath();		break;
		default:				strIMCFilename = pSheet->MakeFilepath(nFolderOATS,nIMC,FALSE);	break;
	}

	PathSetDlgItemPath(this->m_hWnd, IDC_OATSLOC_STATIC, strIMCFilename);
//	TRACE0("In Resize\n");
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COATSPage::OnWizardFinish  
//  Description :   This routine is called when the okay button is checked
//					on the finish dialog box 	
//                  
//  Returns :		BOOL	-	TRUE if successful.
//
//  Parameters :  
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL COATSPage::OnWizardFinish()
{
	CString			strDur;
	CString			strDurPrompt	=	_T("Invalid Duration specified.");
	CString			strTimePrompt	=	_T("The Start Time entered is invalid. All fields\n")
										_T("must be supplied and within valid ranges.");
	CComboBox*		pCB				=	(CComboBox*)GetDlgItem(IDC_OATSINST_COMBO);
	CGenUpdSheet*	pSheet			=	(CGenUpdSheet*)GetParent();

	GetDlgItem(IDC_OATSDURATION_EDIT)->GetWindowText(strDur);

	m_nOATSDur = _ttoi(strDur);
	
	if( pSheet->GetFunc() == nReqIntrusion && pSheet->GetOrig() == _T("Frame_Intrusion") )
	{
		if( m_nOATSDur < 1 || m_nOATSDur > 90 )
		{
			AfxGetMainWnd()->MessageBox (strDurPrompt, _T("Invalid Duration"), MB_OK|MB_ICONEXCLAMATION);
			return FALSE;
		}
	}
	else if( pSheet->GetFunc() == nReqEclipse || pSheet->GetFunc() == nReqIntrusion )
	{
		/* changed from 10 to 90 by dmurphy on 11 AUG 2008
		 * at the request of Kevin Ludlum
		 */
		if( m_nOATSDur < 1 || m_nOATSDur > 90 )
		{
			AfxGetMainWnd()->MessageBox (strDurPrompt, _T("Invalid Duration"), MB_OK|MB_ICONEXCLAMATION);
			return FALSE;
		}
	}
	else if( pSheet->GetFunc() == nReqSFCS )
	{
		if( m_nOATSDur < 1 || m_nOATSDur > 180 )
		{
			AfxGetMainWnd()->MessageBox (strDurPrompt, _T("Invalid Duration"), MB_OK|MB_ICONEXCLAMATION);
			return FALSE;
		}
	}

	m_cStartTimeEdit.GetWindowText(m_strStartTime);

	int nYear = _ttoi((LPCTSTR)m_strStartTime.Mid(0,  4));
	int nDay  = _ttoi((LPCTSTR)m_strStartTime.Mid(5,  3));
	int nHour = _ttoi((LPCTSTR)m_strStartTime.Mid(9,  2));
	int nMin  = _ttoi((LPCTSTR)m_strStartTime.Mid(12, 2));
	int nSec  = _ttoi((LPCTSTR)m_strStartTime.Mid(15, 2));
	int nMili = _ttoi((LPCTSTR)m_strStartTime.Mid(18, 3));

	if( nYear < 1989|| nYear > 2025	|| nDay  < 1|| nDay  > 366	||
		nHour < 0	|| nHour > 23	|| nMin  < 0|| nMin  > 59	||
		nSec  < 0	|| nSec  > 59	|| nMili < 0|| nMili > 999 )
	{
		AfxGetMainWnd()->MessageBox (strTimePrompt, _T("Invalid Start Time"), MB_OK | MB_ICONEXCLAMATION);
		return FALSE;
	}

	pCB->GetLBText(pCB->GetCurSel(), m_strInstrument);
	pSheet->ShowWindow(SW_HIDE);

	CFinishDialog dlg;

//	GetAppRegMakeSched()->m_strOATSStartTime = m_strStartTime;
	return (dlg.DoModal()==IDOK ? TRUE : FALSE);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COATSPage::OnOatsRadio   
//  Description :   This routine is called when radio control
//					is changed on the OATS Page dialog.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COATSPage::OnOatsRadio() 
{
	if( ((CButton*)GetDlgItem(IDC_OATSSOCC_RADIO))->GetCheck() )
	{
		m_nOATSSite = nSOCC;
		GetDlgItem(IDC_OATSSOCC_COMBO)->EnableWindow(TRUE);
		GetDlgItem(IDC_OATSWALLOPS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_OATSBACKUP_COMBO)->EnableWindow(FALSE);
	}

	if( ((CButton*)GetDlgItem(IDC_OATSWALLOPS_RADIO))->GetCheck() )
	{
		m_nOATSSite = nWallops;
		GetDlgItem(IDC_OATSSOCC_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_OATSWALLOPS_COMBO)->EnableWindow(TRUE);
		GetDlgItem(IDC_OATSBACKUP_COMBO)->EnableWindow(FALSE);
	}
	
	if( ((CButton*)GetDlgItem(IDC_OATSBACKUP_RADIO))->GetCheck() )
	{
		m_nOATSSite = nGoddard;
		GetDlgItem(IDC_OATSSOCC_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_OATSWALLOPS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_OATSBACKUP_COMBO)->EnableWindow(TRUE);
	}	
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COATSPage::OnSetActive  
//  Description :	This routine disables/enables, as appropriate, the 
//					different window controls.
//                  
//  Returns :		BOOL	-	TRUE  if successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COATSPage::GetOATSDefaultLocation()
{
	int nDefOATSSite = GetAppRegSite()->m_nDefaultOATS;
	int nDefOATSMachine = GetAppRegOATS()->m_nDefault[nDefOATSSite];

	switch( nDefOATSSite )
	{
		case nSOCC:
		{
			GetDlgItem(IDC_OATSSOCC_COMBO)->EnableWindow(TRUE);
			GetDlgItem(IDC_OATSWALLOPS_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSBACKUP_COMBO)->EnableWindow(FALSE);
			((CComboBox*)GetDlgItem(IDC_OATSSOCC_COMBO))->SetCurSel(nDefOATSMachine);
			((CButton *)GetDlgItem(IDC_OATSSOCC_RADIO))->SetCheck(TRUE);
			break;
		}
		case nWallops:
		{
			GetDlgItem(IDC_OATSSOCC_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSWALLOPS_COMBO)->EnableWindow(TRUE);
			GetDlgItem(IDC_OATSBACKUP_COMBO)->EnableWindow(FALSE);
			((CComboBox*)GetDlgItem(IDC_OATSWALLOPS_COMBO))->SetCurSel(nDefOATSMachine);
			((CButton *)GetDlgItem(IDC_OATSWALLOPS_RADIO))->SetCheck(TRUE);
			break;
		}
		case nGoddard:
		{
			GetDlgItem(IDC_OATSSOCC_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSWALLOPS_COMBO)->EnableWindow(FALSE);
			GetDlgItem(IDC_OATSBACKUP_COMBO)->EnableWindow(TRUE);
			((CComboBox*)GetDlgItem(IDC_OATSBACKUP_COMBO))->SetCurSel(nDefOATSMachine);
			((CButton *)GetDlgItem(IDC_OATSBACKUP_RADIO))->SetCheck(TRUE);
			break;
		}
	}
}
