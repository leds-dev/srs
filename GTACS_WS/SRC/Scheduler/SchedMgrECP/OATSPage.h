#if !defined(AFX_OATSPAGE_H__8012F2B1_408D_11D5_9A7B_0003472193C8__INCLUDED_)
#define AFX_OATSPAGE_H__8012F2B1_408D_11D5_9A7B_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OATSPage.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// COATSPage dialog

class COATSPage : public CResizablePage
{
	DECLARE_DYNCREATE(COATSPage)

// Construction
public:
	COATSPage();
	~COATSPage();
	void	Init(BOOL bFirstWizPage, BOOL bLastWizPage);
	CString	GetInstrument() { return m_strInstrument; }
	CString	GetStartTime()	{ return m_strStartTime; }
	int		GetDuration()	{ return m_nOATSDur; }

// Dialog Data
	//{{AFX_DATA(COATSPage)
	enum { IDD = IDD_OATS_DIALOG };
	COXMaskedEdit	m_cStartTimeEdit;
	int		m_nOATSDur;
	//}}AFX_DATA

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(COATSPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(COATSPage)
	virtual BOOL OnInitDialog();
	virtual BOOL OnWizardFinish();
	afx_msg BOOL OnSetActive();
	afx_msg BOOL OnKillActive();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnOatsRadio();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void	ResizeFilenames();
	BOOL	m_bFirstWizPage;//TRUE if this the first page of a Wizard
	BOOL	m_bLastWizPage;	//TRUE if this the last page of a Wizard
	int		m_nOATSSite;
	int		m_nOATSMachine;
	CString	m_strInstrument;
	CString m_strStartTime;
	void	GetOATSDefaultLocation();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OATSPAGE_H__8012F2B1_408D_11D5_9A7B_0003472193C8__INCLUDED_)
