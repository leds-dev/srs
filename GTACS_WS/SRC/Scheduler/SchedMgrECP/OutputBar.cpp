// OutputBar.cpp : implementation of the COutputBar class
//

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "OutputBar.h"
#include "OXSound.h"
#include "MainFrm.h"
#include "CodeMaxDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



CBuildListCtrl::CBuildListCtrl()
{
}

CBuildListCtrl::~CBuildListCtrl()
{
}


BEGIN_MESSAGE_MAP(CBuildListCtrl, CListCtrl)
	//{{AFX_MSG_MAP(CBuildListCtrl)
	ON_WM_LBUTTONDBLCLK()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CBuildListCtrl::OnLButtonDblClk(UINT nFlags, CPoint point) 
{
	GotoBuildError();
}

void CBuildListCtrl::GotoBuildError()
{
	POSITION pos = GetAppOutputBar()->GetBuildList()->GetFirstSelectedItemPosition();
	if (pos != NULL)
	{
		int nIndex = GetAppOutputBar()->GetBuildList()->GetNextSelectedItem(pos);
		CString strFilename = GetAppOutputBar()->GetBuildFilename();
		struct sErrorInfo* psEI = (struct sErrorInfo*)(GetAppOutputBar()->GetBuildList()->GetItemData(nIndex));
		

		BOOL bMatchFound = FALSE;
		POSITION posDocTemplate = AfxGetApp()->GetFirstDocTemplatePosition();
		CDocument* pDocument = NULL;
		CSchedMgrMultiDocTemplate* pTemplate = NULL;
		while (posDocTemplate && !bMatchFound)
		{
			pTemplate = (CSchedMgrMultiDocTemplate*)(AfxGetApp()->GetNextDocTemplate(posDocTemplate));
			POSITION posDoc = pTemplate->GetFirstDocPosition();
			while (posDoc && !bMatchFound)
			{
				if ((pDocument=pTemplate->GetNextDoc(posDoc)) != NULL)
				{
					if (strFilename.CompareNoCase(pDocument->GetPathName()) == 0)
						bMatchFound = TRUE;
				}
			}
		}
		
		if (bMatchFound)
		{
			POSITION posView = pDocument->GetFirstViewPosition();
			CView* pView = pDocument->GetNextView(posView);
			if (pView != NULL)
				((CMDIChildWnd*)(pView->GetParent()))->MDIActivate();
		}
		else
		{
			posDocTemplate = AfxGetApp()->GetFirstDocTemplatePosition();
			CDocTemplate::Confidence confidence = CDocTemplate::noAttempt;
			while (posDocTemplate && (confidence == CDocTemplate::noAttempt))
			{
				CDocument* pDoc;
				pTemplate = (CSchedMgrMultiDocTemplate*)(AfxGetApp()->GetNextDocTemplate(posDocTemplate));
				confidence = pTemplate->MatchDocType(strFilename, pDoc);
				if (confidence == CDocTemplate::yesAttemptNative)
					pDocument = pTemplate->OpenDocumentFile (strFilename);
			}
		}
		if (pDocument != NULL)
		{
			int nLine;
			nLine = ((CCodeMaxDocEx*)pDocument)->m_wndEdit.GetHighlightedLine();
			((CCodeMaxDocEx*)pDocument)->m_wndEdit.SetHighlightedLine(-1);
			nLine = ((CCodeMaxDocEx*)pDocument)->m_wndEdit.GetHighlightedLine();
			((CCodeMaxDocEx*)pDocument)->m_wndEdit.SetHighlightedLine(psEI->m_nLineNumber-1);
			nLine = ((CCodeMaxDocEx*)pDocument)->m_wndEdit.GetHighlightedLine();
			((CCodeMaxDocEx*)pDocument)->m_wndEdit.SetCaretPos(psEI->m_nLineNumber-1, psEI->m_nOffset);
			if (psEI->m_nLineNumber != -1)
				((CCodeMaxDocEx*)pDocument)->m_wndEdit.SetFocus();
			else
				MessageBeep(0xFFFFFFFF);
		}
		else
			MessageBeep(0xFFFFFFFF);
	}
}

/////////////////////////////////////////////////////////////////////////////
// COutputBar

BEGIN_MESSAGE_MAP(COutputBar, CBCGPDockingControlBar)
	//{{AFX_MSG_MAP(COutputBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COutputBar construction/destruction

COutputBar::COutputBar()
{
	// TODO: add one-time construction code here

}

COutputBar::~COutputBar()
{
}

/////////////////////////////////////////////////////////////////////////////
// COutputBar message handlers

int COutputBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CBCGPDockingControlBar::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect rectDummy;
	rectDummy.SetRectEmpty ();

	// Create tabs window:
	if (!m_wndTabs.Create (CBCGPTabWnd::STYLE_3D, rectDummy, this, 1))
	{
		TRACE0("Failed to create output tab window\n");
		return -1;      // fail to create
	}

	// Create tree windows.
	const DWORD dwViewStyle = LBS_NOINTEGRALHEIGHT | LVS_REPORT | WS_CHILD | WS_VISIBLE | WS_VSCROLL;

	m_wndTabs.SetImageList (IDB_OUTPUT, 16, RGB (255, 0, 255));
	m_ilEvent.Create(IDB_EVENT, 16, 8, RGB(255, 0, 255));
	m_ilBuild.Create(IDB_EVENT, 16, 8, RGB(255, 0, 255));

	if (!m_wndEventList.Create (dwViewStyle, rectDummy, &m_wndTabs, 2) ||
		!m_wndBuildList.Create (dwViewStyle, rectDummy, &m_wndTabs, 3))
	{
		TRACE0("Failed to create output view\n");
		return -1;      // fail to create
	}

	m_wndEventList.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	m_wndBuildList.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	// Attach list windows to tab:
	m_wndTabs.AddTab (&m_wndEventList, _T("Events"), 0);
	m_wndTabs.AddTab (&m_wndBuildList, _T("Gen/Val"), 1);

	m_wndEventList.SetImageList(&m_ilEvent, LVSIL_SMALL);
	m_wndBuildList.SetImageList(&m_ilEvent, LVSIL_SMALL);

	m_wndEventList.InsertColumn(0, _T("Time"), LVCFMT_LEFT, 150, 0);
	m_wndEventList.InsertColumn(1, _T("Message"), LVCFMT_LEFT, 1000, 0);
	m_wndBuildList.InsertColumn(0, _T("Time"), LVCFMT_LEFT, 150, 0);
	m_wndBuildList.InsertColumn(1, _T("Message"), LVCFMT_LEFT, 1000, 0);

	m_hBuildAccelerator = ::LoadAccelerators(AfxGetInstanceHandle(),
											 MAKEINTRESOURCE(IDR_CONTEXT_OUTPUT_BUILD_MENU));
	ASSERT(m_hBuildAccelerator);

	m_hEventAccelerator = ::LoadAccelerators(AfxGetInstanceHandle(),
											 MAKEINTRESOURCE(IDR_CONTEXT_OUTPUT_EVENT_MENU));
	ASSERT(m_hEventAccelerator);

	return 0;
}

void COutputBar::OnSize(UINT nType, int cx, int cy) 
{
	CBCGPDockingControlBar::OnSize(nType, cx, cy);

	// Tab control should cover a whole client area:
	m_wndTabs.SetWindowPos (NULL, -1, -1, cx, cy,
		SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);
}

void COutputBar::OutputToEvents(CString strText, eSeverity nSeverity)
{
	if (!IsWindow(m_wndEventList.m_hWnd))
		return;
//	static COXSound	oxWarningSound;
//	static COXSound	oxFatalSound;
	static BOOL bFirstTime = TRUE;
	if (bFirstTime)
	{
//		oxWarningSound.Open(IDW_WHOOSH);
//		oxFatalSound.Open(IDW_LASER);
		bFirstTime = FALSE;
	}

	int	nPos;
	CString strTime;
	CTime curTime(CTime::GetCurrentTime());
	strTime = curTime.FormatGmt(_T("%Y-%j-%H:%M:%S"));
	nPos = m_wndEventList.InsertItem(LVIF_IMAGE | LVIF_TEXT,
		m_wndEventList.GetItemCount(), strTime, 
		0, 0, nSeverity, 0);
	m_wndEventList.SetItemText(nPos, 1, strText);
	m_wndEventList.EnsureVisible(nPos, FALSE);
	m_wndEventList.UpdateWindow();
//	switch (nSeverity)
//	{
//		case(INFO) :	break;
//		case(WARNING) :	if (!GetAppRegMisc()->m_bMuteSound) oxWarningSound.Play(FALSE, FALSE);	break;
//		case(FATAL) :	if (!GetAppRegMisc()->m_bMuteSound) oxFatalSound.Play(FALSE, FALSE);	break;
//	}
}

void COutputBar::OutputToBuild(CString strText, eSeverity nSeverity,
							   int nLineNumber /*=-1*/, int nOffset /*=-1*/)
{
	if (!IsWindow(m_wndBuildList.m_hWnd))
		return;

//	static COXSound	oxWarningSound;
//	static COXSound	oxFatalSound;
	static BOOL bFirstTime = TRUE;
	if (bFirstTime)
	{
//		oxWarningSound.Open(IDW_WHOOSH);
//		oxFatalSound.Open(IDW_LASER);
		bFirstTime = FALSE;
	}

	int	nPos;
	CString strTime;
	CTime curTime(CTime::GetCurrentTime());
	strTime = curTime.FormatGmt(_T("%Y-%j-%H:%M:%S"));
	struct sErrorInfo* psEI = new sErrorInfo;
	psEI->m_nLineNumber = nLineNumber;
	psEI->m_nOffset = nOffset;
	long nData = (long)psEI;
	nPos = m_wndBuildList.InsertItem(LVIF_IMAGE | LVIF_TEXT,
									 m_wndBuildList.GetItemCount(),
									 strTime, 0, 0, nSeverity, 0);
	m_wndBuildList.SetItemText(nPos, 1, strText);
	m_wndBuildList.SetItemData(nPos, nData);

	m_wndBuildList.EnsureVisible(nPos, FALSE);
	m_wndBuildList.UpdateWindow();
//	switch (nSeverity)
//	{
//		case(INFO) :	break;
//		case(WARNING) :	if (!GetAppRegMisc()->m_bMuteSound) oxWarningSound.Play(FALSE, FALSE);	break;
//		case(FATAL) :	if (!GetAppRegMisc()->m_bMuteSound) oxFatalSound.Play(FALSE, FALSE);	break;
//	}
}

void COutputBar::InitBuild(CString strTitle, CString strBuildFilename)
{
	m_strBuildFilename = strBuildFilename;
	ClearBuild();
//	m_wndTabs.SetActiveTab(1);
	OutputToBuild(strTitle, INFO);
}

void COutputBar::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	UINT uiMenuResId = 0;
	int iActiveTab = m_wndTabs.GetActiveTab ();
	if (iActiveTab < 0)
	{
		return;
	}
	
	switch (iActiveTab)
	{
		case 0: uiMenuResId = IDR_CONTEXT_OUTPUT_EVENT_MENU;	break;
		case 1: uiMenuResId = IDR_CONTEXT_OUTPUT_BUILD_MENU;	break;
	}
		
	if (uiMenuResId != 0)
	{
		int n = theApp.ShowPopupMenu (uiMenuResId, point, AfxGetMainWnd());
	}	
}

BOOL COutputBar::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message >= WM_KEYFIRST && pMsg->message <= WM_KEYLAST)
	{
		int iActiveTab = m_wndTabs.GetActiveTab ();
		if (iActiveTab >= 0)
		{
			switch (iActiveTab)
			{
				case 0: return ::TranslateAccelerator(m_hWnd, m_hEventAccelerator, pMsg);	break;
				case 1: return ::TranslateAccelerator(m_hWnd, m_hBuildAccelerator, pMsg);	break;
			}
		}
	}

	return CBCGPDockingControlBar::PreTranslateMessage(pMsg);
}

void COutputBar::ClearEvents()
{
	m_wndEventList.DeleteAllItems();
}

void COutputBar::ClearBuild()
{
	int nCount = m_wndBuildList.GetItemCount();
	for (int nIndex=0; nIndex<nCount; nIndex++)
	{
		delete ((sErrorInfo*)m_wndBuildList.GetItemData(nIndex));
	}
	m_wndBuildList.DeleteAllItems();
}

void COutputBar::OnDestroy() 
{
	ClearBuild();

	CBCGPDockingControlBar::OnDestroy();
}

UINT CALLBACK OutputBarPrintHookProc (HWND hdlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_INITDIALOG)
	{
			HWND hWnd = GetDlgItem(hdlg, 1058);
			SetWindowText (hWnd, _T("Lines"));
	}
	return 0;
}

void COutputBar::PrintData(CListCtrl* pListCtrl)
{
	MSG msg;

	CPrintDialog pd (false, PD_ALLPAGES | PD_HIDEPRINTTOFILE | PD_RETURNDC |
							PD_USEDEVMODECOPIES | PD_ENABLEPRINTHOOK | PD_NOSELECTION, NULL);

	// The max number of pages (lines) is 65535.  This is a Microsoft limit on the GUI
	// control.  A user can print more than this, he just can't selectivly print out
	// lines above this range/
	unsigned short nMaxObj = 0;
	if (pListCtrl->GetItemCount() >= USHRT_MAX)
		nMaxObj = USHRT_MAX;
	else
		nMaxObj = (unsigned short)pListCtrl->GetItemCount();

	// Setup defaults for the print dialog box
	pd.m_pd.hDevMode  = ((CSchedMgrECPApp*)AfxGetApp())->m_hDevMode;
	pd.m_pd.hDevNames = ((CSchedMgrECPApp*)AfxGetApp())->m_hDevNames;
	pd.m_pd.nMinPage  = 1;
	pd.m_pd.nMaxPage  = nMaxObj;
	pd.m_pd.lpfnPrintHook = OutputBarPrintHookProc;

	// If the dialog box was closed with an OK
	if (pd.DoModal() == IDOK)
	{
		// Margins in inches
		HDC hDC = pd.CreatePrinterDC();
		if (hDC != NULL)
		{
			SetMapMode(hDC, MM_TEXT);
			
			((CSchedMgrECPApp*)AfxGetApp())->m_hDevMode = pd.m_pd.hDevMode;
			((CSchedMgrECPApp*)AfxGetApp())->m_hDevNames= pd.m_pd.hDevNames;

			CPoint cPhysical;
			CPoint cPrintable;
			CPoint cOffset;
			CPoint cLogPixels;

			cPhysical.x		= GetDeviceCaps(hDC, PHYSICALWIDTH);
			cPhysical.y		= GetDeviceCaps(hDC, PHYSICALHEIGHT);
			cPrintable.x	= GetDeviceCaps(hDC, HORZRES);
			cPrintable.y	= GetDeviceCaps(hDC, VERTRES);
			cOffset.x		= GetDeviceCaps(hDC, PHYSICALOFFSETX);
			cOffset.y		= GetDeviceCaps(hDC, PHYSICALOFFSETY);
			cLogPixels.x	= GetDeviceCaps(hDC, LOGPIXELSX);
			cLogPixels.y	= GetDeviceCaps(hDC, LOGPIXELSY);

			// Offsets from edge of page to start of printable area in pixels
			int	nTopOffset		= cOffset.y;
			int nBottomOffset	= cPhysical.y - cPrintable.y - cOffset.y;
			int	nLeftOffset		= cOffset.x;
			int nRightOffset	= cPhysical.x - cPrintable.x - cOffset.x;

			TCHAR*	pStopString;
			double	fPrintTopMargin		= _tcstod(GetAppRegMisc()->m_strTopMargin, &pStopString);
			double	fPrintBottomMargin	= _tcstod(GetAppRegMisc()->m_strBottomMargin, &pStopString);
			double	fPrintLeftMargin	= _tcstod(GetAppRegMisc()->m_strLeftMargin, &pStopString);
			double	fPrintRightMargin	= _tcstod(GetAppRegMisc()->m_strRightMargin, &pStopString);
			int		nPrintPoint			= GetAppRegMisc()->m_nFontSize;
			int		nPrintWeight		= GetAppRegMisc()->m_nFontWeight;
			int		nPrintItalic		= GetAppRegMisc()->m_bFontIsItalic;
			int		nPrintUnderline		= GetAppRegMisc()->m_bFontIsUnderline;
			int		nPrintStrikeOut		= GetAppRegMisc()->m_bFontIsStrikeOut;
			CString	strPrintFontName	= GetAppRegMisc()->m_strFontFaceName;


			// Width of margins in pixels
			// Use the larger of the physical offset needed by the printer or 
			// the size requested by the operator
			int nTopMargin		= __max((int)(fPrintTopMargin * cLogPixels.y), nTopOffset);
			int nBottomMargin	= __max((int)(fPrintBottomMargin * cLogPixels.y), nBottomOffset);
			int nLeftMargin		= __max((int)(fPrintLeftMargin * cLogPixels.x), nLeftOffset);
			int nRightMargin	= __max((int)(fPrintRightMargin * cLogPixels.x), nRightOffset);

			// If the margins are too big then reset them back to the minimum needed
			if ((nTopMargin + nBottomMargin) > cPhysical.y)
			{
				nTopMargin = nTopOffset;
				nBottomMargin = nBottomOffset;
			}
			if ((nLeftMargin + nRightMargin) > cPhysical.x)
			{
				nLeftMargin = nLeftOffset;
				nRightMargin = nRightOffset;
			}

			// Readjust the printable area using the new margins
			cPrintable.x = cPhysical.x - nLeftMargin - nRightMargin;
			cPrintable.y = cPhysical.y - nTopMargin - nBottomMargin;
			
			int nTech		= GetDeviceCaps(hDC, TECHNOLOGY);

			HFONT hFont = CreateFont (-MulDiv (nPrintPoint, GetDeviceCaps (hDC, LOGPIXELSY), 72),
									  0, 0, 0, nPrintWeight,
									  nPrintItalic,
									  nPrintUnderline,
									  nPrintStrikeOut,
									  0, 0, 0, 0, 0, strPrintFontName);
			SelectObject(hDC, hFont);

			CString	strTitle = _T("OutputBar");

			TCHAR szTitle[ _MAX_PATH + 1 ];
			_tcscpy_s (szTitle, strTitle);
			DOCINFO di;
			memset (&di, 0, sizeof(DOCINFO));
			di.cbSize = sizeof(DOCINFO); 
			di.lpszDocName = szTitle; 
			di.lpszOutput = (LPTSTR) NULL; 
			di.lpszDatatype = (LPTSTR) NULL; 
			di.fwType = 0; 

			TEXTMETRIC tm;
			VERIFY (GetTextMetrics (hDC, &tm));
			CPoint cTextSize;
			cTextSize.x = tm.tmAveCharWidth;
			cTextSize.y = tm.tmHeight;

			// Determine the number of whole lines that can fit within the printable area
			int nMaxLinesPerPage = cPrintable.y / cTextSize.y;
			int nMaxCharsPerLine = cPrintable.x / cTextSize.x;
 
			// Determine the size of a tab character in pixels
			int cxTab = (tm.tmAveCharWidth*10) * 4;

			int nCurObj = 0;
			int nStartObj;
			int nEndObj;

			// Should we print the whole document, the range specified, or the highlighted selection?
			if (pd.PrintAll())
			{
				nStartObj = 0;
				nEndObj =  nMaxObj - 1;
			}
			else 
			{
				nStartObj = pd.GetFromPage() - 1;
				nEndObj = pd.GetToPage() - 1;
			}

			BOOL bAbort = FALSE;

			BOOL bContinue = TRUE;
			int	nLinesPerPage;						 //Number of Inst Objs that can be printed per page
			
			if(nTech == DT_CHARSTREAM)
				nLinesPerPage = 58;					 //62 lines minus the header and footer
			else
				nLinesPerPage = nMaxLinesPerPage - 6;//Minus 2 lines for hdr, 2 for footer, 2 for col hdr ;

			int nPageCount = 1;
			CString strPage;
			CString strCurLine;

			SetTextAlign(hDC, TA_BOTTOM);

			int nCharCount = 0;
			
			if (bContinue)
			{
				// Call StartDoc to begin the printing of a document
				int an = StartDoc(hDC, &di);
				//VERIFY (StartDoc (hDC, &di) > 0);

				// Create the Printer Abort dialog box
				// This dialog box will be displayed while the printing is taking place and allow the user
				// the option of canceling the print job
				// If canceled, the bAbort flag will set to true from within the AbortDlgProc procedure
				bool bAbort = false;
				HWND hWndAbort = CreateDialogParam (AfxGetApp()->m_hInstance, MAKEINTRESOURCE (IDD_ABORTPRINT), 
													NULL, (DLGPROC) AbortPrintDlgProc, (LPARAM) &bAbort);

				// Display the Printer Abort dialog box
				// Search through message queue looking for a click on the Cancel button
				::ShowWindow(hWndAbort, SW_SHOW);
				while (PeekMessage(&msg, hWndAbort, NULL, NULL, PM_REMOVE))
				{
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}

				while (bContinue && !bAbort)
				{
					// Begin to print a page
					int a = StartPage(hDC);
					int b = GetLastError();
					//VERIFY (StartPage(hDC) > 0 );

					// Output the header
					CRect cBounds;
					cBounds.bottom	= nTopMargin;
					cBounds.top		= cBounds.bottom - cTextSize.y;
					cBounds.left	= nLeftMargin;
					cBounds.right	= nLeftMargin + cPrintable.x;
					SetTextAlign (hDC, TA_LEFT|TA_BOTTOM);
					ExtTextOut (hDC, cBounds.left, cBounds.bottom, ETO_CLIPPED, cBounds,
								strTitle, strTitle.GetLength(), NULL);

					// Output the footer
					cBounds.bottom	= nTopMargin + cPrintable.y;
					cBounds.top		= cBounds.top - cTextSize.y;
					cBounds.left	= nLeftMargin;
					cBounds.right	= nLeftMargin + cPrintable.x;
					CString strPage;
					strPage.Format(_T("Page: %d"), nPageCount);
					ExtTextOut (hDC, cBounds.left, cBounds.bottom, ETO_CLIPPED, cBounds,
								strPage, strPage.GetLength(), NULL);
					
					// Fill up the page with text
					nPageCount++;

					// Increment the current line number that we are on
					// Reset the bounding box back to the left margin
					int nCurLineOnPage = 0;
					while (nCurLineOnPage <= nLinesPerPage)
					{
						cBounds.bottom = nTopMargin + (cTextSize.y*(2 + nCurLineOnPage));
						cBounds.top = cBounds.bottom - cTextSize.y;
						// If we have already printed to the last selected frame then we are done
						if (nCurObj > nEndObj)
						{
							bContinue = FALSE;
							break;
						}
						else
						{
							SetTextAlign (hDC, TA_LEFT|TA_BOTTOM);
							int nXPos = cBounds.left;
							if (cBounds.right > (nLeftMargin + cPrintable.x))
								cBounds.right = nLeftMargin + cPrintable.x;

							// If the left edge is within the printable area, then print the line
							if (cBounds.left < (nLeftMargin + cPrintable.x))
							{
								CString strTextField;
								CString strSeverity;
								LVITEM	lvItem;
								lvItem.mask = LVIF_IMAGE;
								lvItem.iItem = nCurObj;
								lvItem.iSubItem = 0;
								pListCtrl->GetItem(&lvItem);
								switch (lvItem.iImage)
								{
									case COutputBar::INFO		: strSeverity = _T("Informational"); break;
									case COutputBar::WARNING	: strSeverity = _T("Warning      "); break;
									case COutputBar::FATAL		: strSeverity = _T("Fatal        "); break;
								}
								strTextField.Format (_T("%s %s %s"), strSeverity,
																	 pListCtrl->GetItemText(nCurObj, 0),
																	 pListCtrl->GetItemText(nCurObj, 1));							
								strTextField.Replace(_T("\\"), _T("\\\\"));
								int nTextFieldLen = strTextField.GetLength();
								// Check if text is greater than can fit on a printed line 
								if (nTextFieldLen > nMaxCharsPerLine){
									// Text size must be too large.
									// Wrap text line.
									CString strPartialTextField(strTextField);
									CString strTemp(strTextField);
									strPartialTextField.Delete(nMaxCharsPerLine, nTextFieldLen - nMaxCharsPerLine);
									ExtTextOut (hDC, nXPos, cBounds.bottom, ETO_CLIPPED, cBounds, 
											strPartialTextField, strPartialTextField.GetLength(), NULL);
									nCurLineOnPage++;
									strTextField.Delete(0, nMaxCharsPerLine);
									cBounds.bottom = nTopMargin + (cTextSize.y*(2 + nCurLineOnPage));
									cBounds.top = cBounds.bottom - cTextSize.y;
								} 
								ExtTextOut (hDC, nXPos, cBounds.bottom, ETO_CLIPPED, cBounds, 
									strTextField, strTextField.GetLength(), NULL);
							}
							nCurLineOnPage++;
						}
						nCurObj++;
					}

					// At this point we have just printed one page
					// Call EndPage so the printer will eject the paper
					// Flush the message queue for the abort dlg so it can process the Cancel button
					VERIFY (EndPage(hDC) > 0);
					while (PeekMessage(&msg, hWndAbort, NULL, NULL, PM_REMOVE))
					{
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
					if (nCurObj > nEndObj)
						bContinue = FALSE;
				}

				// At this point, the print job is either done, or it was aborted
				// If it was aborted call AbortDoc otherwise call EndDoc
				if (bAbort)
				{
					VERIFY (AbortDoc(hDC) > 0);
				}
				else
				{
					VERIFY (EndDoc(hDC) > 0);
				}
		
				// Delete the Print Abort dialog from the display
				// Delete the Device Context and Font resources used for the print job
				::DestroyWindow (hWndAbort);
				DeleteDC (hDC);
				DeleteObject (hFont);
			}
		}
		else
		{
			OutputToEvents(_T("Could not retrieve Device Context from printer"), FATAL);
		}
	}
}
