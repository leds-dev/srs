// OutputBar.h : interface of the COutputBar class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_OUTPUTBAR_H__A464C53D_D502_11D4_800D_00609704053C__INCLUDED_)
#define AFX_OUTPUTBAR_H__A464C53D_D502_11D4_800D_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CBuildListCtrl : public CListCtrl
{
public:
	CBuildListCtrl();
// Attributes
public:
// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBuildListCtrl)
	//}}AFX_VIRTUAL
// Implementation
public:
	virtual ~CBuildListCtrl();
	void	GotoBuildError();
	// Generated message map functions
protected:
	//{{AFX_MSG(CBuildListCtrl)
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class COutputBar : public CBCGPDockingControlBar
{
public:
	COutputBar();
	typedef enum eSeverity {INFO=0, WARNING=1, FATAL=2};
	CListCtrl*		GetEventList(){return &m_wndEventList;};
	CBuildListCtrl*	GetBuildList(){return &m_wndBuildList;};
// Attributes
protected:
	CBCGPTabWnd	m_wndTabs;
	CImageList	m_ilEvent;
	CImageList	m_ilBuild;

	CListCtrl		m_wndEventList;
	CBuildListCtrl	m_wndBuildList;

	HACCEL		m_hEventAccelerator;
	HACCEL		m_hBuildAccelerator;

	CString		m_strBuildFilename;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COutputBar)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~COutputBar();
	void	OutputToEvents(CString strText, eSeverity nSeverity);
	void	OutputToBuild(CString strText, eSeverity nSeverity, int nLineNumber=-1, int nOffset=-1);
	void	InitBuild(CString strTitle, CString strBuildFilename);
	void	ClearEvents();
	void	ClearBuild();
	CString GetBuildFilename(){return m_strBuildFilename;};
	void	PrintData(CListCtrl* pListCtrl);
	
// Generated message map functions
protected:
	//{{AFX_MSG(COutputBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

struct sErrorInfo
{
	int m_nLineNumber;	// 0 based line number
	int m_nOffset;		// 0 based offset
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OUTPUTBAR_H__A464C53D_D502_11D4_800D_00609704053C__INCLUDED_)
