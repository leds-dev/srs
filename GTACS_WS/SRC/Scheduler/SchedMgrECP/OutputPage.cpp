/**
/////////////////////////////////////////////////////////////////////////////
// OutputPage.cpp : implementation of the OutputPage class.                //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// OutputPage.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class will allow the user to choose output parameters for various
// scheduler actions.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"
#include "FinishDialog.h"
#include "OutputPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COutputPage property page

IMPLEMENT_DYNCREATE(COutputPage, CResizablePage)

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COutputPage::COutputPage
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
COutputPage::COutputPage() : CResizablePage(COutputPage::IDD)
, m_strIMC(_T(""))
{
	//{{AFX_DATA_INIT(COutputPage)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COutputPage::~COutputPage
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
COutputPage::~COutputPage()
{
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COutputPage::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//
//  Returns :		void	-  
//
//  Parameters : 
//           CDataExchange* pDX
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void COutputPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COutputPage)
	//	DDX_Control(pDX, IDC_OUTPUT_FILE_IMC_EDIT, m_cIMCEdit);
	DDX_Control(pDX, IDC_OUTPUT_FILE_VERSION_EDIT, m_cVerEdit);
	DDX_Control(pDX, IDC_OUTPUT_DIR_DAY_EDIT, m_cDayEdit);
	//}}AFX_DATA_MAP
	DDX_Text(pDX, IDC_IMC_SET_EDIT, m_strIMC);
	DDV_MaxChars(pDX, m_strIMC, 4);
}


BEGIN_MESSAGE_MAP(COutputPage, CResizablePage)
	//{{AFX_MSG_MAP(COutputPage)
	ON_NOTIFY(UDN_DELTAPOS, IDC_OUTPUT_DIR_DAY_SPIN, OnDeltaPosOutputUpdDaySpin)
	ON_NOTIFY(UDN_DELTAPOS, IDC_OUTPUT_FILE_VERSION_SPIN, OnDeltaPosOutputUpdVerSpin)
	ON_BN_CLICKED(IDC_OUTPUT_DIR_DAY_RADIO, OnOutputDirRadio)
	ON_BN_CLICKED(IDC_OUTPUT_FILE_VERSION_RADIO, OnOutputFileRadio)
	ON_EN_UPDATE(IDC_OUTPUT_DIR_DAY_EDIT, OnUpdateOutputDirDayEdit)
	ON_EN_UPDATE(IDC_OUTPUT_FILE_VERSION_EDIT, OnUpdateOutputFileVersionEdit)
	ON_WM_SIZE()
	ON_EN_UPDATE(IDC_OUTPUT_FILE_USER_EDIT, OnChangeOutputFileUserEdit)
	ON_EN_UPDATE(IDC_OUTPUT_FILE_IMC_EDIT, OnChangeOutputFileIMCEdit)
	ON_EN_UPDATE(IDC_OUTPUT_DIR_USER_EDIT, OnUpdateOutputDirUserEdit)
	ON_BN_CLICKED(IDC_OUTPUT_DIR_USER_RADIO, OnOutputDirRadio)
	ON_BN_CLICKED(IDC_OUTPUT_FILE_USER_RADIO, OnOutputFileRadio)
	//}}AFX_MSG_MAP
//	ON_EN_CHANGE(IDC_IMC_SET_EDIT, &COutputPage::OnEnChangeImcSetEdit)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COutputPage message handlers

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::Init
//  Description :	This routine initializes the member variables based on 
//					the passed parameters.
//
//  Returns :		void	-
//
//  Parameters :
//		BOOL bFirstWizPage	-	first display page.
//		BOOL bLastWizPage	-	last display page.
//		BOOL bVal			-	valid file.
//		eValidLoc eValLoc	-	exectuion type.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::Init(BOOL bFirstWizPage, BOOL bLastWizPage)
{
	m_bFirstWizPage	= bFirstWizPage;
	m_bLastWizPage	= bLastWizPage;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		COutputPage::OnInitDialog
//  Description :	This routine creates the OutputPage dialog window.  
//
//	Return :		BOOL	-	TRUE if successful
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL COutputPage::OnInitDialog() 
{
	m_bInInit = TRUE;
	CString strTemp;
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	CResizablePage::OnInitDialog();
	
	// Set the anchors to allow the page to grow

	AddAnchor(IDC_OUTPUT_GROUP, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_OUTPUT_DIR_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_OUTPUT_DIR_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_DIR_DAY_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_DIR_DAY_EDIT, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_DIR_DAY_SPIN, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_DIR_USER_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_DIR_USER_EDIT, TOP_LEFT, TOP_RIGHT);
	
	AddAnchor(IDC_OUTPUT_FILE_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_OUTPUT_FILE_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_FILE_VERSION_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_FILE_VERSION_EDIT, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_FILE_VERSION_SPIN, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_FILE_USER_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_FILE_USER_EDIT, TOP_LEFT, TOP_RIGHT);

	AddAnchor(IDC_IMC_SET_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_IMC_SET_STATIC, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_IMC_SET_EDIT, TOP_LEFT, TOP_LEFT);

	AddAnchor(IDC_OUTPUT_DEFAULTFILENAME_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_OUTPUT_DEFAULTFILENAME_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_OUTPUT_DEFAULTFILENAME_STATIC, TOP_LEFT, TOP_RIGHT);
	
	// Set the mask, prompt symbol, and range of the various Day controls
	m_cDayEdit.SetMask(_T("###"));
	m_cDayEdit.SetPromptSymbol(_T('0'));
	((CSpinButtonCtrl*)GetDlgItem(IDC_OUTPUT_DIR_DAY_SPIN))->SetRange(0, 366);
	((CSpinButtonCtrl*)GetDlgItem(IDC_OUTPUT_DIR_DAY_SPIN))->SetPos(pSheet->GetDay());
	strTemp.Format(_T("%03d"), pSheet->GetDay());
	m_cDayEdit.SetWindowText(strTemp);
	
	// Set the mask, prompt symbol, and range of the various Version controls
	m_cVerEdit.SetMask(_T("###"));
	m_cVerEdit.SetPromptSymbol(_T('0'));
	((CSpinButtonCtrl*)GetDlgItem(IDC_OUTPUT_FILE_VERSION_SPIN))->SetRange(0, 999);
	((CSpinButtonCtrl*)GetDlgItem(IDC_OUTPUT_FILE_VERSION_SPIN))->SetPos(pSheet->GetVersion());
	strTemp.Format(_T("%03d"), pSheet->GetVersion());
	m_cVerEdit.SetWindowText(strTemp);
	
//	m_cIMCEdit.SetMask(_T(">###"));
//	m_cIMCEdit.SetPromptSymbol(_T('-'));
	
	m_bUseDay = TRUE;
	
	switch( pSheet->GetFunc() )
	{
		case (nReqIMC)		:
		{
			m_bUseVersion	= FALSE;
			m_bUseUser		= TRUE;
			//m_bUseIMC		= TRUE;
			//GetDlgItem(IDC_OUTPUT_FILE_IMC_RADIO)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_SET_EDIT)->SetWindowText(pSheet->GetBase());
			break;
		}
		case (nReqSFCS)		:
		case (nReqEclipse)	:
		case (nReqIntrusion):
		{
			m_bUseVersion	= FALSE;
			m_bUseUser		= TRUE;
			//m_bUseIMC		= FALSE;
			GetDlgItem(IDC_IMC_SET_GROUP)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_SET_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_SET_EDIT)->EnableWindow(FALSE);
			break;
		}
		default				:
		{
			m_bUseVersion	= TRUE;
			m_bUseUser		= FALSE;
			//m_bUseIMC		= FALSE;
			GetDlgItem(IDC_IMC_SET_GROUP)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_SET_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_SET_EDIT)->EnableWindow(FALSE);
			break;
		}
	}

	// Update all of the check boxes with the proper status
	((CButton*)GetDlgItem(IDC_OUTPUT_DIR_DAY_RADIO))->SetCheck(m_bUseDay);
	((CButton*)GetDlgItem(IDC_OUTPUT_DIR_USER_RADIO))->SetCheck(!m_bUseDay);
	((CButton*)GetDlgItem(IDC_OUTPUT_FILE_VERSION_RADIO))->SetCheck(m_bUseVersion);
	((CButton*)GetDlgItem(IDC_OUTPUT_FILE_USER_RADIO))->SetCheck(m_bUseUser);
	//((CButton*)GetDlgItem(IDC_OUTPUT_FILE_IMC_RADIO))->SetCheck(m_bUseIMC);

	OnOutputDirRadio();
	OnOutputFileRadio();

	// Determine the initial User directory.  This will be the default directory Server +
	// Share + Spacecraft directory.
	// Update the user control with this value.
//	int	nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int	nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];

//	m_strUserDir.Format(_T("\\\\%s\\%s\\%s\\"),
//		GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
	m_strUserDir.Format(_T("%s\\%s\\%s\\%s\\"),
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC]);		// SC Specific Directory

	GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT)->SetWindowText(m_strUserDir);
	((CEdit*)GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT))->SetSel(0,0);

	//switch (pSheet->GetFunc())
	//{
	//	case (nReqIMC)		:
	//	case (nReqSFCS)		:
	//	case (nReqEclipse)	:
	//	case (nReqIntrusion):
	//	case (nPackage)		:
	//		GetDlgItem(IDC_OUTPUT_FILE_USER_EDIT)->SetWindowText(pSheet->GetBase());
	//		break;
	//	default:
	//		GetDlgItem(IDC_OUTPUT_FILE_USER_EDIT)->SetWindowText(m_strUserDir);
	//}

	GetDlgItem(IDC_OUTPUT_FILE_USER_EDIT)->SetWindowText(pSheet->GetBase());

	UpdateFilenames();
	
	m_bInInit = FALSE;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnOutputDirRadio   
//  Description :   This routine enables/disables as needed the dialog 
//                  controls for the OutputPage dialog box.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::OnOutputDirRadio()
{
	// Determine the state we are now in
	m_bUseDay = ((CButton*)GetDlgItem(IDC_OUTPUT_DIR_DAY_RADIO))->GetCheck();

	// Enable/Disable the controls as appropriate
	GetDlgItem(IDC_OUTPUT_DIR_DAY_EDIT)->EnableWindow(m_bUseDay);	
	GetDlgItem(IDC_OUTPUT_DIR_DAY_SPIN)->EnableWindow(m_bUseDay);	
	GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT)->EnableWindow(!m_bUseDay);

	if( !m_bInInit )
		UpdateFilenames();	
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnOutputFileRadio   
//  Description :   This routine enables/disables as needed the dialog 
//                  controls for the OutputPage dialog box.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::OnOutputFileRadio()
{
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	// Determine the state we are now in
	m_bUseVersion	= ((CButton*)GetDlgItem(IDC_OUTPUT_FILE_VERSION_RADIO))->GetCheck();
	m_bUseUser		= ((CButton*)GetDlgItem(IDC_OUTPUT_FILE_USER_RADIO))->GetCheck();
	
	// Enable/Disable the controls as appropriate
	
	switch( pSheet->GetFunc() )
	{
		case (nReqIMC)		:
		{
			GetDlgItem(IDC_OUTPUT_FILE_VERSION_EDIT)->EnableWindow(m_bUseVersion);	
			GetDlgItem(IDC_OUTPUT_FILE_USER_EDIT)->EnableWindow(m_bUseUser);	
			GetDlgItem(IDC_IMC_SET_EDIT)->EnableWindow(TRUE);
			break;
		}
		case (nReqSFCS)		:
		case (nReqEclipse)	:
		case (nReqIntrusion):
		{
			GetDlgItem(IDC_OUTPUT_FILE_VERSION_EDIT)->EnableWindow(m_bUseVersion);	
			GetDlgItem(IDC_OUTPUT_FILE_USER_EDIT)->EnableWindow(m_bUseUser);	
			GetDlgItem(IDC_IMC_SET_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_SET_EDIT)->EnableWindow(FALSE);
			break;
		}
		default:
		{
			GetDlgItem(IDC_OUTPUT_FILE_VERSION_EDIT)->EnableWindow(m_bUseVersion);	
			GetDlgItem(IDC_OUTPUT_FILE_VERSION_SPIN)->EnableWindow(m_bUseVersion);	
			GetDlgItem(IDC_OUTPUT_FILE_USER_EDIT)->EnableWindow(m_bUseUser);	
			GetDlgItem(IDC_IMC_SET_STATIC)->EnableWindow(FALSE);
			GetDlgItem(IDC_IMC_SET_EDIT)->EnableWindow(FALSE);
		}
	}

	if( !m_bInInit )
		UpdateFilenames();	
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnUpdateOutputDirDayEdit   
//  Description :   This routine is called when day control is changed 
//					on the OutputPage dialog.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::OnUpdateOutputDirDayEdit()
{
	if( m_bInInit )
		return;

	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	CString strTemp;
	GetDlgItem(IDC_OUTPUT_DIR_DAY_EDIT)->GetWindowText(strTemp);
	((CSpinButtonCtrl*)GetDlgItem(IDC_OUTPUT_DIR_DAY_SPIN))->SetPos(_ttoi(strTemp));
	pSheet->SetDay(_ttoi(strTemp));
	UpdateFilenames();	
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnUpdateOutputDirUserEdit   
//  Description :   This routine is called when user control is updated 
//					on the OutputPage dialog.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::OnUpdateOutputDirUserEdit()
{
	if( m_bInInit )
		return;
	
	// Determine the directory prefix
//	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	CString	strPrefix;
	
//	strPrefix.Format(_T("\\\\%s\\%s\\%s\\"),
//		GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
	
	strPrefix.Format(_T("%s\\%s\\%s\\%s\\"),
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC]);		// Spacecraft
	
	//Get the start and stop position of the curent selection
	int nStart;
	int nEnd;			
	
	//A start or end position within the prefix area is not allowed!
	//The directory must alwasy start with the default server and share.
	//Restore what was originally there
	((CEdit*)GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT))->GetSel(nStart, nEnd);

	if( (nStart == 0) && (nEnd == 0) )
	{
		((CEdit*)GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT))->SetSel(strPrefix.GetLength(), strPrefix.GetLength());
		((CEdit*)GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT))->GetSel(nStart, nEnd);
	}
	
	if( (nStart < strPrefix.GetLength()) || (nEnd < strPrefix.GetLength()) )
	{
		GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT)->SetWindowText(m_strUserDir);
		MessageBeep(0xFFFFFFFF);
	}
	else
	{
		//Read what is currently being displayed
		CString			strTemp;
		GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT)->GetWindowText(strTemp);
		
		//If the last character is not a \ or /, then add one onto the end
		int	nLen = strTemp.GetLength();

		if( (strTemp[nLen-1] != _T('\\')) && (strTemp[nLen-1] != _T('/')) )
		{
			m_strUserDir = strTemp + _T("\\");
			GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT)->SetWindowText(m_strUserDir);
			((CEdit*)GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT))->SetSel(nStart, nStart);
		}
		else
		{
			if( nLen>1 )
			{
				//If the last two characters are the same (and are both \ or /) delete the last one
				if( strTemp[nLen-1] == strTemp[nLen-2] )
				{
					m_strUserDir = strTemp.Left(nLen-1);
					GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT)->SetWindowText(m_strUserDir);
					((CEdit*)GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT))->SetSel(nStart-1, nStart-1);
				}
				else
					m_strUserDir = strTemp;
			}
			else
				m_strUserDir = strTemp;
		}
	}

	UpdateFilenames();	
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnUpdateOutputFileVersionEdit   
//  Description :   This routine is called when file version control is 
//					updated on the OutputPage dialog.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::OnUpdateOutputFileVersionEdit()
{
	if( m_bInInit )
		return;
	
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	CString strTemp;
	GetDlgItem(IDC_OUTPUT_FILE_VERSION_EDIT)->GetWindowText(strTemp);
	((CSpinButtonCtrl*)GetDlgItem(IDC_OUTPUT_FILE_VERSION_SPIN))->SetPos(_ttoi(strTemp));
	pSheet->SetVersion(_ttoi(strTemp));
	UpdateFilenames();	
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnChangeOutputFileUserEdit   
//  Description :   This routine is called when output file user control
//					is changed on the OutputPage dialog.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::OnChangeOutputFileUserEdit()
{
	UpdateFilenames();	
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnChangeOutputFileIMCEdit   
//  Description :   This routine is called when file IMC control
//					is changed on the OutputPage dialog.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::OnChangeOutputFileIMCEdit()
{
	UpdateFilenames();	
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnDeltaPosOutputUpdDaySpin   
//  Description :   This routine is called when day spin control
//					is changed on the OutputPage dialog.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::OnDeltaPosOutputUpdDaySpin(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_UPDOWN*		pNMUpDown	= (NM_UPDOWN*)pNMHDR;
	CGenUpdSheet*	pSheet		= (CGenUpdSheet*)GetParent();

	// Get the current Day position
	CString strTemp;
	GetDlgItem(IDC_OUTPUT_DIR_DAY_EDIT)->GetWindowText(strTemp);
	pNMUpDown->iPos = _ttoi(strTemp);
	
	// If the new Day position would be within range, then allow it
	int nNewPos = pNMUpDown->iPos + pNMUpDown->iDelta;

	if( (nNewPos >= 0) && (nNewPos <= 366) )
	{
		CString strTemp;
		strTemp.Format(_T("%03d"), nNewPos);
		m_cDayEdit.SetWindowText(strTemp);
		pSheet->SetDay(nNewPos);
		*pResult = 0;
	}
	else
		*pResult = 1; //Don't allow the change
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnDeltaPosOutputUpdVerSpin   
//  Description :   This routine is called when version spin control
//					is changed on the OutputPage dialog.
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::OnDeltaPosOutputUpdVerSpin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN*		pNMUpDown	= (NM_UPDOWN*)pNMHDR;
	CGenUpdSheet*	pSheet		= (CGenUpdSheet*)GetParent();

	// Get the current Version position
	CString strTemp;
	GetDlgItem(IDC_OUTPUT_FILE_VERSION_EDIT)->GetWindowText(strTemp);
	pNMUpDown->iPos = _ttoi(strTemp);
	
	// If the new Version position would be within range, then allow it
	int nNewPos = pNMUpDown->iPos + pNMUpDown->iDelta;

	if( (nNewPos >= 0) && (nNewPos <= 366) )
	{
		CString strTemp;
		strTemp.Format(_T("%03d"), nNewPos);
		m_cVerEdit.SetWindowText(strTemp);
		pSheet->SetVersion(nNewPos);
		*pResult = 0;
	}
	else
		*pResult = 1; //Don't allow the change
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnSize  
//  Description :   This routine resizes the dialog window based on the 
//					the coordinates passed into the routine.	
//                  
//  Returns :		void	-
//  Parameters : 
//           UINT nType	-	type of window resizing requested.
//           int cx		-	new width of window.
//           int cy		-	new height of window.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::OnSize(UINT nType, int cx, int cy) 
{
	CResizablePage::OnSize(nType, cx, cy);
	ResizeFilenames();
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnWizardFinish  
//  Description :   This routine is called when the okay button is checked
//					on the finish dialog box 	
//                  
//  Returns :		BOOL	-	TRUE if successful.
//
//  Parameters :  
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL COutputPage::OnWizardFinish() 
{
	((CGenUpdSheet*)GetParent())->ShowWindow(SW_HIDE);

	CFinishDialog dlg;

	if( dlg.DoModal() == IDOK )
		return TRUE;
	else
		return FALSE;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnKillActive  
//  Description :	This routine calls the CResizablePage::OnKillActive 
//					routine.
//
//  Returns :		BOOL	-	 TRUE  if successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL COutputPage::OnKillActive()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	if( m_bUseDay )
	{
		CString strDay;
		CString strPrefix;
//		int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//		int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
//		strPrefix.Format(_T("\\\\%s\\%s\\%s\\"),
//			GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//			GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share

		strPrefix.Format(_T("%s\\%s\\%s\\%s\\"),
			ctcProcDrive, ctcMountPoint, ctcProcsDir,
			GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC]);		// Spacecraft

		strDay.Format(GetAppRegFolder()->m_strDayFormat + _T("\\"), pSheet->GetDay());

		COXUNC unc1(strPrefix+strDay);
		COXUNC unc2(strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderSTOL]	+ _T("\\"));
		COXUNC unc3(strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderFrame]	+ _T("\\"));
		COXUNC unc4(strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderStar]	+ _T("\\"));
		COXUNC unc5(strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderOATS]	+ _T("\\"));
			
		if( !unc1.Exists() )
		{	
			CString strMsg;
			CString strFmt(strPrefix+strDay);
			strFmt.Delete(strFmt.GetLength()-1);
			strMsg.Format(_T("Directory does not exists!\n\nDirectory and Subdirectories:\n")
				_T("%s\n%s\n%s\n%s\n%s\n\nWould you like to create them?"), strFmt,
				strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderSTOL],
				strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderFrame],
				strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderStar],
				strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderOATS]);

			if( AfxMessageBox(strMsg, MB_YESNO|MB_ICONQUESTION) == IDNO )
				return FALSE;

			if( !unc2.Create()||!unc3.Create()||!unc4.Create()||!unc5.Create() )
			{
				LPVOID lpMsgBuf;
				FormatMessage(
					FORMAT_MESSAGE_ALLOCATE_BUFFER |
					FORMAT_MESSAGE_FROM_SYSTEM |
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL, GetLastError(),
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPTSTR) &lpMsgBuf, 0, NULL);

				strMsg.Format(_T("Error! %s"),(LPTSTR)lpMsgBuf);
				LocalFree(lpMsgBuf);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			}
		}
	}

	if( ((CButton*)GetDlgItem(IDC_OUTPUT_FILE_USER_RADIO))->GetCheck() )
	{
		CString strName;
		GetDlgItem(IDC_OUTPUT_FILE_USER_EDIT)->GetWindowText(strName);

		if( strName.IsEmpty() )
		{
			AfxMessageBox(_T("The user specified filename must not be blank."), MB_OK | MB_ICONSTOP);
			return FALSE;
		}
	}

	if( pSheet->GetFunc() == nReqIMC )
	{
		CString strIMC;
		GetDlgItem(IDC_IMC_SET_EDIT)->GetWindowText(strIMC);

		if( strIMC.Find(_T('-')) != -1 )
		{
			AfxMessageBox(
				_T("The IMC Set ID is improperly specified.\n")
				_T("It must be in the format <letter><number><number><number> (i.e. A000)."),
				MB_OK | MB_ICONSTOP);
			return FALSE;
		}
	}
	
	return CResizablePage::OnKillActive();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::OnSetActive  
//  Description :	This routine disables/enables, as appropriate, the 
//					different window controls.
//                  
//  Returns :		BOOL	-	TRUE  if successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL COutputPage::OnSetActive()
{
	CString strTemp;
	
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	// Setup the proper Wizard Buttons
	DWORD dWizFlags = PSWIZB_FINISH;

	if( (m_bFirstWizPage) && (m_bLastWizPage) )
		dWizFlags = PSWIZB_FINISH;
	else if( (m_bFirstWizPage) && (!m_bLastWizPage) )
		dWizFlags = PSWIZB_NEXT;
	else if( (!m_bFirstWizPage) && (m_bLastWizPage) )
		dWizFlags = PSWIZB_BACK | PSWIZB_FINISH;
	else if( (!m_bFirstWizPage) && (!m_bLastWizPage) )
		dWizFlags = PSWIZB_BACK | PSWIZB_NEXT;
	
	pSheet->SetWizardButtons(dWizFlags);		
	
	return CResizablePage::OnSetActive();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::UpdateFilenames   
//  Description :   This routine is called to get the pathnames that is 
//					displayed in the dialog. 	
//
//  Returns :		void	-
//
//  Parameters :
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::UpdateFilenames()
{
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CString strExt;

	if( pSheet->GetFunc() == nReqIMC	||
		pSheet->GetFunc() == nReqSFCS	||
		pSheet->GetFunc() == nReqEclipse|| 
		pSheet->GetFunc() == nReqIntrusion )
	{
		int nType;

		switch( pSheet->GetFunc() )
		{
			case nReqIMC:	nType = nIMC;	break;
			case nReqSFCS:	nType = nSTOL;	break;
			default:		nType = nRpt;
		}
			
		GetAppDocTemplate(nType)->GetDocString(strExt, CDocTemplate::filterExt);

		if( m_bUseVersion )
		{
			CString strVersion;
			GetDlgItem(IDC_OUTPUT_FILE_VERSION_EDIT)->GetWindowText(strVersion);
			pSheet->m_strNameToUse = pSheet->GetBase() + _T("_") + strVersion + strExt;
		}
		else if( m_bUseUser )
		{
			GetDlgItem(IDC_OUTPUT_FILE_USER_EDIT)->GetWindowText(pSheet->m_strNameToUse);
			pSheet->m_strNameToUse += strExt;
		}
		else
			pSheet->m_strNameToUse = pSheet->GetBase() + strExt;

		GetDlgItem(IDC_IMC_SET_EDIT)->GetWindowText(m_strIMC);
	}
	else if( m_bUseVersion )
	{
		CString strVersion;
		GetDlgItem(IDC_OUTPUT_FILE_VERSION_EDIT)->GetWindowText(strVersion);
		pSheet->m_strNameToUse = pSheet->GetBase() + _T("_") + strVersion;
	}
	else if( m_bUseUser )
		GetDlgItem(IDC_OUTPUT_FILE_USER_EDIT)->GetWindowText(pSheet->m_strNameToUse);
	else // m_bUseIMC 
	{
		GetDlgItem(IDC_IMC_SET_EDIT)->GetWindowText(m_strIMC);
		pSheet->m_strNameToUse = m_strIMC;
	}

	// Determine the directory prefix
//	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	CString	strPrefix;
	
//	strPrefix.Format(_T("\\\\%s\\%s\\%s\\"),
//		GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share

	strPrefix.Format(_T("%s\\%s\\%s\\%s\\"),
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC]);		// Spacecraft
	
	if( m_bUseDay )
		pSheet->m_strDirToUse.Format(GetAppRegFolder()->m_strDayFormat + _T("\\"), pSheet->GetDay());
	else
	{
		CString strDir;
		GetDlgItem(IDC_OUTPUT_DIR_USER_EDIT)->GetWindowText(strDir);
		strDir.Delete(0, strPrefix.GetLength());
		pSheet->m_strDirToUse = strDir;
	}

	CString strFolder;

	switch( pSheet->GetFunc() )
	{
		case nReqSFCS:		strFolder = GetAppRegFolder()->m_strDir[nFolderSTOL] + _T("\\"); break;
		case nReqIMC:		
		case nReqEclipse:	
		case nReqIntrusion:	strFolder = GetAppRegFolder()->m_strDir[nFolderOATS] + _T("\\"); break;
		default:			strFolder = _T("<product>\\");
	}

	m_strPath = strPrefix + pSheet->m_strDirToUse + strFolder + pSheet->m_strNameToUse;

	GetDlgItem(IDC_OUTPUT_DEFAULTFILENAME_STATIC)->SetWindowText(m_strPath);
	ResizeFilenames();
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		COutputPage::ResizeFilenames  
//  Description :   This routine is called to resize the pathname is 
//					displayed. So that it fits within the dialog control. 	
//                  
//  Returns :		void	-
//
//  Parameters :  
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void COutputPage::ResizeFilenames()
{
	// CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	PathSetDlgItemPath(this->m_hWnd, IDC_OUTPUT_DEFAULTFILENAME_STATIC, m_strPath);
}

