#if !defined(AFX_OUTPUTPAGE_H__B04A0C35_2F48_11D5_9A74_0003472193C8__INCLUDED_)
#define AFX_OUTPUTPAGE_H__B04A0C35_2F48_11D5_9A74_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OutputPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COutputPage dialog

class COutputPage : public CResizablePage
{
	DECLARE_DYNCREATE(COutputPage)

// Construction
public:
	COutputPage();
	void	Init(BOOL bFirstWizPage, BOOL bLastWizPage);
	~COutputPage();
	BOOL	GetUseDay()	{ return m_bUseDay; }
	CString GetStrPath(){ return m_strPath; }
	CString GetIMCSet() { return m_strIMC; }
// Dialog Data
	//{{AFX_DATA(COutputPage)
	enum { IDD = IDD_OUTPUT_DIALOG };
	COXMaskedEdit	m_cVerEdit;
	COXMaskedEdit	m_cDayEdit;
//	COXMaskedEdit	m_cIMCEdit;
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(COutputPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(COutputPage)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnSetActive();
	afx_msg BOOL OnKillActive();
	afx_msg void OnDeltaPosOutputUpdDaySpin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaPosOutputUpdVerSpin(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnWizardFinish();
	afx_msg void OnOutputDirRadio();
	afx_msg void OnOutputFileRadio();
	afx_msg void OnUpdateOutputDirDayEdit();
	afx_msg void OnUpdateOutputFileVersionEdit();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnChangeOutputFileUserEdit();
	afx_msg void OnChangeOutputFileIMCEdit();
	afx_msg void OnUpdateOutputDirUserEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void	UpdateFilenames();
	void	ResizeFilenames();
	BOOL	m_bFirstWizPage;			//TRUE if this the first page of a Wizard
	BOOL	m_bLastWizPage;				//TRUE if this the last page of a Wizard
//	CString m_strIMCSet;
	CString m_strPath;
	CString m_strUserFilename;			// The name that the user has entered - Initially the name
										// portion of m_strSchedFilename
	CString m_strUserDir;				// The directory that the user has entered - Initially this will
										// be the default server + share + spacecraft directory
	BOOL	m_bInInit;					// TRUE when in InitInstance.  This is needed so that the OnUpdate
										// routines get ignored for the day and version controls.  These
										// controls are COXMaskedEdit classes and the OnUpdate functions get
										// called whenever the mask and prompt symbol is set (in addition to
										// when the value is changed).  Because of this, we want to ignore
										// processing in these routines when we are initializing the control
	BOOL	m_bUseDay;					//If TRUE all output files are placed into the directories
										//specified within the registry. The directory is derived from
										//GetAppRegSC()->m_strDir + GetAppRegFolder()->m_strDir[DailyXXX].
										//keys.  If FALSE, all output files are placed into the directories
										//m_strUserDir + GetAppRegFolder()->m_strDir[XXX].
	BOOL	m_bUseVersion;				//If TRUE the output filename will be have the version specified
										//appended to it.
	BOOL	m_bSameLoc;					//If TRUE, the output filename will be based upon the inputfiles,
										//and placed in the same directory.
	BOOL	m_bUseUser;					//If TRUE the output filename will be based upon what the user entered.
	BOOL	m_bUseIMC;					//If TRUE the output filename will be based upon the IMC set entered.
public:
	CString m_strIMC;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OUTPUTPAGE_H__B04A0C35_2F48_11D5_9A74_0003472193C8__INCLUDED_)
