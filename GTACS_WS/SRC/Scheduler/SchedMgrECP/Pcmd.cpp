﻿enum SymbolConstants
{
   SYM_EOF              =  0, // (EOF)
   SYM_ERROR            =  1, // (Error)
   SYM_COMMENTS         =  2, // Comments
   SYM_WHITESPACE       =  3, // Whitespace
   SYM_COMMA            =  4, // ','
   SYM_FLOATINGCONSTANT =  5, // FLOATINGconstant
   SYM_IDENTIFIER       =  6, // IDENTIFIER
   SYM_INTEGERCONSTANT  =  7, // INTEGERconstant
   SYM_NEWLINE          =  8, // NewLine
   SYM_CMD              =  9, // <cmd>
   SYM_CONSTANT         = 10, // <constant>
   SYM_EOL              = 11, // <eol>
   SYM_NL               = 12, // <nl>
   SYM_PCMD             = 13, // <pcmd>
   SYM_START            = 14, // <start>
   SYM_STATEMENT        = 15, // <statement>
   SYM_STATEMENTLIST    = 16  // <statementList>
};

enum ProductionConstants
{
   PROD_EOL                       =  0, // <eol> ::= <nl>
   PROD_NL_NEWLINE                =  1, // <nl> ::= NewLine
   PROD_START                     =  2, // <start> ::= <statementList>
   PROD_STATEMENTLIST             =  3, // <statementList> ::= <statementList> <statement>
   PROD_STATEMENTLIST2            =  4, // <statementList> ::= <statement>
   PROD_STATEMENT                 =  5, // <statement> ::= <pcmd> <eol>
   PROD_STATEMENT2                =  6, // <statement> ::= <eol>
   PROD_CMD_IDENTIFIER            =  7, // <cmd> ::= IDENTIFIER
   PROD_PCMD_COMMA_COMMA          =  8, // <pcmd> ::= <cmd> ',' <constant> ',' <cmd>
   PROD_PCMD_COMMA                =  9, // <pcmd> ::= <cmd> ',' <constant>
   PROD_CONSTANT_INTEGERCONSTANT  = 10, // <constant> ::= INTEGERconstant
   PROD_CONSTANT_FLOATINGCONSTANT = 11  // <constant> ::= FLOATINGconstant
};
