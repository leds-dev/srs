﻿#if !defined( PCMDCLASSESSSHPP)
#define PCMDCLASSESSSHPP
#include "sslex.hpp"
#include "ssyacc.hpp"
#include "GoldWrapper.h"

class PcmdLexClass : public SSLex
{
public:
	SSConstr PcmdLexClass( const char*);
	SSConstr PcmdLexClass( const char*, const char*);
	SSConstr PcmdLexClass( SSLexConsumer&, SSLexTable&);

	const char* tokenToConstChar( SSUnsigned32);
};

SSInline PcmdLexClass::PcmdLexClass( const char* pszFile) : 
	SSLex( pszFile, ".\\Pcmd.dfa")
{
}

SSInline PcmdLexClass::PcmdLexClass( const char* pszFile, const char* pszTable) : 
	SSLex( pszFile, pszTable)
{
}

SSInline PcmdLexClass::PcmdLexClass( SSLexConsumer& Consumer, SSLexTable& Table) : 
	SSLex( Consumer, Table)
{
}

class PcmdYaccClass : public GoldWrapper
{
public:
	SSConstr PcmdYaccClass( const char*);
	SSYaccStackElement* reduce( SSUnsigned32, SSUnsigned32);

	virtual SSDestr PcmdYaccClass( void);

	virtual SSBooleanValue error( SSUnsigned32, SSLexLexeme&);
	void parseError(SSLexLexeme& qLookahead);
	void filename(CString filename) { m_strFilename = filename; }
	void errorCount(int* nErrors)	{ m_pnErrorCnt = nErrors; }

	IEpEnumCommands*	m_pEnumCmds;
	CString				m_strErrorMessage;
	CString				m_strFilename;
	int*				m_pnErrorCnt;
	int					m_nErrorNum;

};

SSInline PcmdYaccClass::PcmdYaccClass( const char* pszFile) : 
	GoldWrapper(pszFile), m_pEnumCmds(NULL), m_pnErrorCnt(NULL), m_nErrorNum(0)
{
}

SSInline PcmdYaccClass::~PcmdYaccClass( void)
{
	if( m_pEnumCmds != NULL)
	{
		m_pEnumCmds->Release();
		m_pEnumCmds = NULL;
	}

	m_pnErrorCnt = NULL;
}

#endif
