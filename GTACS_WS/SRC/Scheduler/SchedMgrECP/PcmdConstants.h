﻿enum SymbolConstants
{
   SYM_EOF              =  0, // (EOF)
   SYM_ERROR            =  1, // (Error)
   SYM_COMMENTS         =  2, // Comments
   SYM_WHITESPACE       =  3, // Whitespace
   SYM_PERCENTERROR     =  4, // '%error'
   SYM_COMMA            =  5, // ','
   SYM_FLOATINGCONSTANT =  6, // FLOATINGconstant
   SYM_IDENTIFIER       =  7, // IDENTIFIER
   SYM_INTEGERCONSTANT  =  8, // INTEGERconstant
   SYM_NEWLINE          =  9, // NewLine
   SYM_CMD              = 10, // <cmd>
   SYM_CONSTANT         = 11, // <constant>
   SYM_EOL              = 12, // <eol>
   SYM_NL               = 13, // <nl>
   SYM_PCMD             = 14, // <pcmd>
   SYM_START            = 15, // <start>
   SYM_STATEMENT        = 16, // <statement>
   SYM_STATEMENTLIST    = 17  // <statementList>
};

enum ProductionConstants
{
   PROD_EOL                       =  0, // <eol> ::= <nl>
   PROD_NL_NEWLINE                =  1, // <nl> ::= NewLine
   PROD_START                     =  2, // <start> ::= <statementList>
   PROD_STATEMENTLIST             =  3, // <statementList> ::= <statementList> <statement>
   PROD_STATEMENTLIST2            =  4, // <statementList> ::= <statement>
   PROD_STATEMENT                 =  5, // <statement> ::= <pcmd> <eol>
   PROD_STATEMENT_PERCENTERROR    =  6, // <statement> ::= '%error' <eol>
   PROD_STATEMENT2                =  7, // <statement> ::= <eol>
   PROD_CMD_IDENTIFIER            =  8, // <cmd> ::= IDENTIFIER
   PROD_PCMD_COMMA_COMMA          =  9, // <pcmd> ::= <cmd> ',' <constant> ',' <cmd>
   PROD_PCMD_COMMA                = 10, // <pcmd> ::= <cmd> ',' <constant>
   PROD_CONSTANT_INTEGERCONSTANT  = 11, // <constant> ::= INTEGERconstant
   PROD_CONSTANT_FLOATINGCONSTANT = 12  // <constant> ::= FLOATINGconstant
};
