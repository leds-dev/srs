#include "stdafx.h"
#if !defined( PCMDFUNCTIONSSSCPP)
#define PCMDFUNCTIONSSSCPP
#include "PcmdConstants.h"
#include "PcmdClasses.h"

const char* PcmdLexClass::tokenToConstChar( SSUnsigned32 ulToken)
{
	const char* pchToken;
	switch ( ulToken)
	{
	case SYM_EOL:
		pchToken = "eol";
		break;

	case SYM_IDENTIFIER:
		pchToken = "IDENTIFIER";
		break;

	case SYM_INTEGERCONSTANT:
		pchToken = "INTEGERconstant";
		break;

	case SYM_FLOATINGCONSTANT:
		pchToken = "FLOATINGconstant0";
		break;

		/*
	case PcmdLexTokenOParen:
		pchToken = "(";
		break;

	case PcmdLexTokenCParen:
		pchToken = ")";
		break;
*/
	case SYM_COMMA:
		pchToken = ",";
		break;

/*
case PcmdLexTokenColon:
		pchToken = ":";
		break;

	case PcmdLexTokenSemiColon:
		pchToken = ";";
		break;

	case PcmdLexTokenAssign:
		pchToken = "=";
		break;

	case PcmdLexTokenAt:
		pchToken = "@";
		break;

	case PcmdLexTokenUnderScore:
		pchToken = "_";
		break;

	case PcmdLexTokenOBrack:
		pchToken = "[";
		break;

	case PcmdLexTokenCBrack:
		pchToken = "]";
		break;

	case PcmdLexTokenOBrace:
		pchToken = "{";
		break;

	case PcmdLexTokenCBrace:
		pchToken = "}";
		break;

	case PcmdLexTokenDollar:
		pchToken = "$";
		break;

	case PcmdLexTokenBackSlash:
		pchToken = "\\";
		break;

	case PcmdLexTokenForwardSlash:
		pchToken = "/";
		break;

	case PcmdLexTokenDot:
		pchToken = ".";
		break;

	case PcmdLexTokenPlus:
		pchToken = "+";
		break;

	case PcmdLexTokenMinus:
		pchToken = "-";
		break;

	case PcmdLexTokenStar:
		pchToken = "*";
		break;

	case PcmdLexTokenExPoint:
		pchToken = "!";
		break;

	case PcmdLexTokenMod:
		pchToken = "%";
		break;

	case PcmdLexTokenHat:
		pchToken = "^";
		break;

	case PcmdLexTokenAmpersand:
		pchToken = "&";
		break;

	case PcmdLexTokenSquiggle:
		pchToken = "~";
		break;
*/

	case SYM_ERROR:
		pchToken = "%error";
		break;

	case SYM_EOF:
		pchToken = "eof";
		break;

	default:
		pchToken = SSLexTokenNotFound;
	}
	return pchToken;
}

#endif
