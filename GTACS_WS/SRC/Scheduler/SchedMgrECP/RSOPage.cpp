// CRSOPage.cpp : implementation file
//
// Revision History:													   
// Manan Dalal - July-August 2014										   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   
// Release in Build16.2

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"
#include "RSOPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const int nMaxRSOLbls = 10;
///////////////////////////////////////////////////////////////////////////////////////////
// CRSOPage propery page

IMPLEMENT_DYNCREATE(CRSOPage, CResizablePage)

CRSOPage::CRSOPage() : CResizablePage(CRSOPage::IDD)
{
	//{{AFX_DATA_INIT(CRSOPage)
	//}}AFX_DATA_INIT
}

CRSOPage::~CRSOPage() {}

void CRSOPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRSOPage)
	DDX_Control(pDX, IDC_RSO_FRMLBL_EDIT, m_cRSOCurLabel);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CRSOPage, CResizablePage)
	//{{AFX_MSG_MAP(CRSOPage)
	ON_WM_SIZE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRSOPage message handlers

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Manan Dalal		Date : 8/20/14		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CRSOPage::Init
//  Description :	This routine initializes the member variables based on 
//					the passed parameters.
//
//  Returns :		void	-
//
//  Parameters :
//		BOOL bFirstWizPage	-	first display page.
//		BOOL bLastWizPage	-	last display page.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CRSOPage::Init(BOOL bFirstWizPage, BOOL bLastWizPage)
{
	m_bFirstWizPage	= bFirstWizPage;
	m_bLastWizPage	= bLastWizPage;
}

BOOL CRSOPage::OnInitDialog()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CResizablePage::OnInitDialog();

	//Add all of the necessary anchors
	AddAnchor(IDC_RSO_UPD_GROUP, TOP_LEFT, BOTTOM_RIGHT);

	AddAnchor(IDC_RSO_SCAN_FRMLBL_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_RSO_FRMLBL_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_RSO_FRMLBL_EDIT, TOP_LEFT, TOP_RIGHT);

	AddAnchor(IDC_RSO_FRMLBL_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_RSO_NEWFRM_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_RSO_NEWRSOLBL_COMBO, TOP_LEFT, TOP_RIGHT);

	int nDefSCID=GetAppRegSite()->m_nDefaultSC;
	CString strExt;
	GetAppDocTemplate(nFrame)->GetDocString(strExt, CDocTemplate::filterExt);
	m_strFrameFile.Format(_T("%s\\%s\\%s\\%s\\%s\\%s%s"),
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],		// SC Specific Directory
		GetAppRegFolder()->m_strDir[nFolderFrame],					// Master Frame directory
		GetAppRegRSO()->m_strFrmTable[nDefSCID],                    // Frame Table name from Registry
		strExt);													// Extension

	CString m_strRSOLabel[nMaxRSOLbls];
	m_strRSOLabel[0]=GetAppRegRSO()->m_strNewRSOName1[nDefSCID];
	m_strRSOLabel[1]=GetAppRegRSO()->m_strNewRSOName2[nDefSCID];
	m_strRSOLabel[2]=GetAppRegRSO()->m_strNewRSOName3[nDefSCID];
	m_strRSOLabel[3]=GetAppRegRSO()->m_strNewRSOName4[nDefSCID];
	m_strRSOLabel[4]=GetAppRegRSO()->m_strNewRSOName5[nDefSCID];
	m_strRSOLabel[5]=GetAppRegRSO()->m_strNewRSOName6[nDefSCID];
	m_strRSOLabel[6]=GetAppRegRSO()->m_strNewRSOName7[nDefSCID];
	m_strRSOLabel[7]=GetAppRegRSO()->m_strNewRSOName8[nDefSCID];
	m_strRSOLabel[8]=GetAppRegRSO()->m_strNewRSOName9[nDefSCID];
	m_strRSOLabel[9]=GetAppRegRSO()->m_strNewRSOName10[nDefSCID];

	GetDlgItem(IDC_RSO_FRMLBL_EDIT)->SetWindowText(GetAppRegRSO()->m_strCurRSOName[nDefSCID]);
	for(int nCount=0; nCount<nMaxRSOLbls; nCount++)
	{
		if(!m_strRSOLabel[nCount].IsEmpty())
			((CComboBox*)GetDlgItem(IDC_RSO_NEWRSOLBL_COMBO))->InsertString(0, m_strRSOLabel[nCount]);
	}

	if( pSheet->GetFramePage()->GetCreateNew()
		|| !pSheet->GetFramePage()->GetUseRSO() 
		|| !pSheet->GetFramePage()->GetUse())
	{
		GetDlgItem(IDC_RSO_UPD_GROUP)->EnableWindow(FALSE);

		GetDlgItem(IDC_RSO_SCAN_FRMLBL_GROUP)->EnableWindow(FALSE);
		GetDlgItem(IDC_RSO_FRMLBL_LABEL)->EnableWindow(FALSE);
		GetDlgItem(IDC_RSO_FRMLBL_EDIT)->EnableWindow(FALSE);

		GetDlgItem(IDC_RSO_FRMLBL_GROUP)->EnableWindow(FALSE);
		GetDlgItem(IDC_RSO_NEWFRM_LABEL)->EnableWindow(FALSE);
		GetDlgItem(IDC_RSO_NEWRSOLBL_COMBO)->EnableWindow(FALSE);
	}
	return TRUE;	//Return TRUE unless you set the focus to a control
					// EXCEPTION: OCX Property Pages should return FALSE
}

void CRSOPage::OnSize(UINT nType, int cx, int cy)
{
	CResizablePage::OnSize(nType, cx, cy);
}

BOOL CRSOPage::OnSetActive()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	//Update the visibility of the controls based upon the radio buttons positions
	UpdateControls();

	//Setup the proper wizard buttons
	DWORD dWizFlags;
	if( (m_bFirstWizPage) && (m_bLastWizPage) )
		dWizFlags = PSWIZB_FINISH;
	else if( (m_bFirstWizPage) && (!m_bLastWizPage) )
		dWizFlags = PSWIZB_NEXT;
	else if( (!m_bFirstWizPage) && (m_bLastWizPage) )
		dWizFlags = PSWIZB_BACK | PSWIZB_FINISH;
	else if( (!m_bFirstWizPage) && (!m_bLastWizPage) )
		dWizFlags = PSWIZB_BACK | PSWIZB_NEXT;

	pSheet->SetWizardButtons(dWizFlags);

	return CResizablePage::OnSetActive();
}
	
/**
//////////////////////////////////////////////////////////////////////////
//  Author :Manan Dalal      Date : 8/25/14       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CRSOPage::OnWizardNext  
//  Description :   This routine is called when the user selects next   
//					on the dialog window. The specific window controls
//                  are updated as needed.
//                   
//                  
//  Returns :	LRESULT	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
LRESULT CRSOPage::OnWizardNext()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	GetDlgItem(IDC_RSO_FRMLBL_EDIT)->GetWindowText(m_strCurRSOLabel);
	GetDlgItem(IDC_RSO_NEWRSOLBL_COMBO)->GetWindowText(m_strNewRSOLabel);

	return CResizablePage::OnWizardNext();

}

void CRSOPage::UpdateControls()
{
	CGenUpdSheet*	pSheet	= (CGenUpdSheet*)GetParent();
	BOOL			bUseRSO	= true;

	// If update for frames, or create a new frametable, or not doing schedule update, or update FOR RSO frames is
	// NOT checked, then don't enable the RSO dialog page.

	if( pSheet->GetFramePage()->GetCreateNew()
		|| !pSheet->GetFramePage()->GetUseRSO() 
		|| !pSheet->GetFramePage()->GetUse())
		bUseRSO = false;

	GetDlgItem(IDC_RSO_UPD_GROUP)->EnableWindow(bUseRSO);

	GetDlgItem(IDC_RSO_SCAN_FRMLBL_GROUP)->EnableWindow(bUseRSO);
	GetDlgItem(IDC_RSO_FRMLBL_LABEL)->EnableWindow(bUseRSO);
	GetDlgItem(IDC_RSO_FRMLBL_EDIT)->EnableWindow(bUseRSO);

	GetDlgItem(IDC_RSO_FRMLBL_GROUP)->EnableWindow(bUseRSO);
	GetDlgItem(IDC_RSO_NEWFRM_LABEL)->EnableWindow(bUseRSO);
	GetDlgItem(IDC_RSO_NEWRSOLBL_COMBO)->EnableWindow(bUseRSO);

}