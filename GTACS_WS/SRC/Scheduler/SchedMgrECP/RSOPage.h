#if !defined(AFX_RSOPAGE_H__8B4B1AB9_585D_463C_A099_76883D832E21__INCLUDED_)
#define AFX_RSOPAGE_H__8B4B1AB9_585D_463C_A099_76883D832E21__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RSOPage.h : header file
// Initial Creation:
// Manan Dalal - July-August 2014										   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   
// Release in Build16.2

/////////////////////////////////////////////////////////////////////////////
// CRSOPage dialog
/////////////////////////////////////////////////////////////////////////////

class CRSOPage : public CResizablePage
{
	DECLARE_DYNCREATE(CRSOPage)

	// Construction
public:
	CRSOPage();
	~CRSOPage();
	void Init(BOOL bFirstWizPage, BOOL bLastWizPage);
	CString GetRSOFrmName()  { return m_strFrameFile; };
	CString GetRSOCurLabel() { return m_strCurRSOLabel; };
	CString GetRSONewLabel() { return m_strNewRSOLabel; };

	//Dialog Data
	//{{AFX_DATA(CRSOPage)
	enum { IDD = IDD_RSO_DIALOG };
	COXEdit	m_cRSOCurLabel;
	//}}AFX_DATA

	//Overrides
	//ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CRSOPage)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV Support
	//}}AFX_VIRTUAL

	//Implementation
protected:
	//Generated message map functions
	//{{AFX_MSG(CRSOPage)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnSetActive();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual LRESULT OnWizardNext();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void	UpdateControls();
	BOOL	m_bFirstWizPage; //True if this is the first page of a wizard
	BOOL	m_bLastWizPage;	 //True if this is the last page of a wizard
	BOOL	m_bInInit;		 //True when in InitInstance.
	CString m_strFrameFile;	 //Frame Name from Registry.
	CString	m_strCurRSOLabel;//Current Special RSO label name
	CString m_strNewRSOLabel;//New Special RSO label name
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RSOPAGE_H__8B4B1AB9_585D_463C_A099_76883D832E21__INCLUDED_)
