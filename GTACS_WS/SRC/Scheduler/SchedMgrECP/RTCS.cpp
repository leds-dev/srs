/**
/////////////////////////////////////////////////////////////////////////////
// RTCS.cpp : implementation of the RTCS class.                            //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// RTCS.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class will create RTCS list and map files from RTCS set files.
// 
/////////////////////////////////////////////////////////////////////////////
**/
#include "stdafx.h"
#include "SchedMgrECP.h"
#include "SearchConstants.h"
#include "SchedTime.h"
#include "RTCS.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRTCS::CRTCS
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//		CString strRTCSFilename	-	RTCS set file name.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CRTCS::CRTCS(CString strRTCSFilename)
{
	COXUNC	uncFilepath(strRTCSFilename);
	DWORD	nUserNameSize = UNLEN + 1;
	TCHAR	cUserName[UNLEN + 1];
	
	::GetUserName(cUserName, &nUserNameSize);

	m_strUserName	= cUserName;
	m_strRTCSName	= strRTCSFilename;
	m_strLisName	= MakeFilepath(uncFilepath.Base(), nLis);
	m_strMapName	= MakeFilepath(uncFilepath.Base(), nSTOLMap);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRTCS::CheckValid
//	Description :	CheckValid opens the existing RTCS file reads
//					in the prolog and return true if the file contains
//					a valid validation record.
//
//	Return :		BOOL	-	TRUE if file was successfully 
//								validated.
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CRTCS::CheckValid(CString strFile)
{
	CTextFile	rtcsFile;
	CString		strErr;
	COXUNC		uncFile(strFile);

	if( !rtcsFile.Load(strFile, CTextFile::RO, &m_fileException) )
	{
		strErr = _T("Error opening file ") + strFile;
		GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
		return FALSE;
	}

	eValidStatus eVStatus;
	rtcsFile.IsFileValid(eVStatus);

	if( eVStatus != PASSED && eVStatus != WARNING )
	{
		if( rtcsFile.IsOverRideSet() )
		{
			strErr = _T("The validation status has been overridden for rtcs ") + uncFile.File();
			strErr+= _T(". Proceeding with generation.");
			GetAppOutputBar()->OutputToEvents(strErr, COutputBar::WARNING);
		}
		else
		{
			strErr.Format(_T("The rtcs file %s is not stamped as being valid."), uncFile.File());
			strErr+= _T(" Only validated rtcss can be used to generate loads.");
			GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
			rtcsFile.Close(CTextFile::NOWRITE, &m_fileException);
			return FALSE;
		}
	}

	rtcsFile.Close(CTextFile::NOWRITE, &m_fileException);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRTCS::OpenProductFiles
//	Description :	This routine is used to open all files needed to
//					to produce the RTCS list and map files.
//
//	Return :		BOOL	-	TRUE if file was successfully opened.
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CRTCS::OpenProductFiles()
{
	CString strMsg;

	if( !CheckValid(m_strRTCSName) )
		return FALSE;

	if( !m_textSetFile.Load(m_strRTCSName, CTextFile::RO, &m_fileException) )
	{
		strMsg = m_textSetFile.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	if( !m_textLisFile.Load(m_strLisName, CTextFile::CR, &m_fileException) )
	{
		strMsg = m_textLisFile.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	if( !m_textMapFile.Load(m_strMapName, CTextFile::CR, &m_fileException) )
	{
		strMsg = m_textMapFile.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		DeleteFile(m_strLisName);
		return FALSE;
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRTCS::LoadProductFiles
//	Description :	This routine is used to create the RTCS list and 
//					map files.
//
//	Return :		BOOL	-	TRUE if files were successfully 
//								created.
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CRTCS::LoadProductFiles()
{
	int		nCurLine		= 0;
	int		nRtnLine		= 0;
//	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	BOOL	bProlog			= FALSE;

	CString strLine;
	CString strFilepath;
	CString strInclude;
	CString strRTCSName;
	CString strLabel;
	CString strStart;
	CString strMap;
	CString strRtcsExeTime;
	CString strRtAddr(GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC]);

	CString strMsg(_T("Creating RTCS product files..."));
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	int nMapIndex = CopyProlog();

	while( nCurLine < m_textSetFile.GetLineCount() )
	{
		strLine = m_textSetFile.GetDirLine(nCurLine++, nRtnLine);
		strLine.TrimLeft();

		// Process RTCS line.
		if( !strLine.IsEmpty() && ctchCOMMENT != strLine[0] )
		{
			if( strLine.Find(ctstrInclude) != -1 || strLine.Find(_T("INCLUDE")) != -1 )
			{
				m_textLisFile.AppendText(_T("#") + strLine);
				nMapIndex++;

				strInclude	= RemoveFirstToken(&strLine);
				strRTCSName	= RemoveFirstToken(&strLine);
				strLabel 	= ExtractValue(strLine, _T("LABEL="));
				strStart 	= ExtractValue(strLine, _T("START="));
				strFilepath = MakeFilepath(strRTCSName, nRTCS);

				CTextFile tf;

				if( !CheckValid(strFilepath) )
				{
					DeleteProductFiles();
					return FALSE;
				}

				if( !tf.Load(strFilepath, CTextFile::RO, &m_fileException) )
				{
					strMsg = tf.ExceptionErrorText(&m_fileException);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					DeleteProductFiles();
					return FALSE;
				}

				int		nIndex	= 0;
				int		nFound	= 0;
				int		nStart	= 0;
				float	fWait	= 0;
				strRtcsExeTime	= tf.GetProlog()->GetExectime()->GetTextString();

				while( nIndex < tf.GetLineCount() )
				{
					strLine = tf.GetDirLine(nIndex++, nRtnLine);
					strLine.TrimLeft();

					if( !strLine.IsEmpty() && ctchCOMMENT != strLine[0] )
					{
						if( tf.ScanLine(strLine, ctstrWAIT) )
						{
							CString strTmp(strLine);
							strTmp.MakeUpper();
							nFound = strTmp.Find(_T('+'));

							if( nFound != -1 )
								strTmp.Delete(nFound, 1);

							nFound = strTmp.Find(ctstrWAIT);

							if( nStart > 0 )
							{
								strStart		= _T("0");
								strLabel		= _T("");
								strRtcsExeTime	= _T("");
							}

							strTmp.Delete(nFound, 4);
							strTmp.TrimLeft();
							strTmp.TrimRight();
							_stscanf_s(strTmp, _T("%f"), &fWait);
							strMap.Format(_T("%d, %d, %s, 0, CMD %s %s%s%s%s, %s, 0, 0, 0, %.3f, %s"),
									nMapIndex,nMapIndex,strStart,ctstrCMDPAUSE,ctstrPAUSE_TIMEEQ,
									strTmp,ctstrRT,strRtAddr,strLabel,fWait,strRtcsExeTime);
							m_textLisFile.AppendText(strLine);
							m_textMapFile.AppendText(strMap);
							nStart++; nMapIndex++;
						}

						if( tf.ScanLine(strLine, ctstrCMD) )
						{
							if( nStart > 0 )
							{
								strStart		= _T("0");
								strLabel		= _T("");
								strRtcsExeTime	= _T("");
							}

							// Replace $QBCTCU_SEL with CTCU and $QBACE_SEL with RT values in registry.	
							CString strCTCUID(_T(" "));
							int		nCTCUID = GetAppRegSC()->m_nCTCUID[GetAppRegSite()->m_nDefaultSC];

							if( nCTCUID != -1 )
								strCTCUID.Format(_T("%d"), nCTCUID);

							strLine.Replace(ctstr$QBACE_SEL,	strRtAddr); 
							strLine.Replace(ctstr$QBCTCU_SEL,	strCTCUID); 						
							strLine.TrimLeft();
							strLine.TrimRight();
							strMap.Format(_T("%d, %d, %s, 0, %s, %s, 0, 0, 0, %f, %s"),
									nMapIndex,nMapIndex,strStart,strLine,strLabel,
									GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC],
									strRtcsExeTime);
							m_textLisFile.AppendText(strLine);
							m_textMapFile.AppendText(strMap);
							nStart++; nMapIndex++;
						}
					}
				}

				tf.Close(CTextFile::NOWRITE, &m_fileException);
			}
		}
	}

	CString	strUpdateText;
	strUpdateText.Format(_T("%d, %d, %s"),
	m_textMapFile.GetLineCount(),CTime::GetCurrentTime(),m_strUserName);
	m_textMapFile.InsertAt(0,_T("1.0, RTCSMap"));
	m_textMapFile.InsertAt(1,strUpdateText);

	if( !m_textMapFile.Close(CTextFile::WRITE, &m_fileException) )
	{
		COXUNC	unc(m_strMapName);
		CString strMsg(_T("Error closing file ") + unc.File() + _T(" :"));
		strMsg+=m_textMapFile.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	if( !m_textLisFile.Close(CTextFile::WRITE, &m_fileException) )
	{
		COXUNC	unc(m_strLisName);
		CString strMsg(_T("Error closing file ") + unc.File() + _T(" :"));
		strMsg+=m_textLisFile.ExceptionErrorText(&m_fileException); 
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRTCS::CopyProlog
//	Description :	This routine is used to copy the RTCS set file 
//					prolog to the RTCS list file.
//
//	Return :		int	-	number of lines in prolog.
//	
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CRTCS::CopyProlog()
{
	int		nCurLine= 0;
	int		nRtnLine= 0;
	BOOL	bProlog = FALSE;
	BOOL	bContin	= TRUE;
	CString strLine;

	while( nCurLine < m_textSetFile.GetLineCount() && bContin )
	{
		strLine = m_textSetFile.GetDirLine(nCurLine, nRtnLine);

		if( strLine.Find(ctstrPrlgBeg) != -1 )
			bProlog = TRUE;

		if( strLine.Find(ctstrPrlgEnd) != -1 )
		{
			bProlog = FALSE;
			bContin = FALSE;
		}
			
		if( bProlog )
			m_textLisFile.AppendText(strLine);

		nCurLine++;
	}

	m_textLisFile.AppendText(strLine);
	m_textLisFile.AppendText(_T(""));

	return (nCurLine+2);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRTCS::ExtractValue
//	Description :	ExtractValue will return what is on the right hand
//					side of a qualifier.
//
//	Return :		CString		-	value of the string qualifier.
//
//	Parameters :
//			const CString	strLine		-	CString that contains
//											qualifier 	
//			const CString	strQualifier-	qualifier to find value
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CRTCS::ExtractValue(const CString strLine, const CString strQualifier)
{
	CString strValue		= _T("");
	CString strTempLine		= strLine;
	CString strTempQualifier= strQualifier;

	strTempLine.MakeUpper();
	strTempLine.TrimLeft();
	strTempLine.TrimRight();
	strTempQualifier.MakeUpper();
	int nQualifierStart = strTempLine.Find(strTempQualifier);

	if( nQualifierStart != -1 )
	{
		strTempLine = strTempLine.Mid(nQualifierStart);

		if( strTempLine.GetLength() > strTempQualifier.GetLength() )
		{
			strTempLine.Replace(strTempQualifier, NULL);
			strTempLine.TrimLeft();

			int nSpaceStart = strTempLine.Find(_T(" "));
			strValue		= (nSpaceStart == -1 ? strTempLine : strTempLine.Left(nSpaceStart));
		}
	}

	return strValue;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRTCS::MakeFilepath
//	Description :	This routine creates the directory path based on
//					file type.
//
//	Return :		CString		-	filename and directory path.
//
//	Parameters :
//			CString strNameToUse-	name of file. 	
//			int	nType			-	file type.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CRTCS::MakeFilepath(CString strNameToUse, int nType)
{
	CString	strFilepath;
	CString	strExt;
//	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];

	GetAppDocTemplate(nType)->GetDocString(strExt, CDocTemplate::filterExt);

//	strFilepath.Format(_T("\\\\%s\\%s\\%s\\%s\\%s%s"),
//		GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
	strFilepath.Format(_T("%s\\%s\\%s\\%s\\%s\\%s%s"),
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],			// Spacecraft
		GetAppRegFolder()->m_strDir[nFolderRTCS],					// Specific dir
		strNameToUse,												// Filename
		strExt);													// Extension

	return strFilepath;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CRTCS::RemoveFirstToken
//	Description :	This routine removes the first token off of a 
//					list of comma delimited tokens. To cleans things 
//					up, the original string will be left trimmed and
//					any double quotes will be removed.
//
//	Return :		CString	-	first token off list.
//
//	Parameters :
//		CString *pstrInput	-	pointer to list of tokens.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CRTCS::RemoveFirstToken(CString *pstrInput)
{
	CString strToken;

	pstrInput->TrimLeft();
	pstrInput->Remove('"');

	int nPos = pstrInput->Find(' ');

	if( nPos == -1 )
	{
		strToken	= *pstrInput;
		*pstrInput	= "";
	}
	else
	{
		strToken	= pstrInput->Left(nPos);
		*pstrInput	= pstrInput->Right(pstrInput->GetLength() - nPos - 1);
	}

	return strToken;
}
