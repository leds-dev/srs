//////////////////////////////////////////////////////////////////////
// RTCS.h : interface for the RTCS class.
// (c) 2001
// modifications:
//
//////////////////////////////////////////////////////////////////////

#ifndef __RTCS_H__
#define __RTCS_H__

#include "TextFile.h"

#if _MSC_VER
#pragma once
#endif // _MSC_VER

class CRTCS
{

// Construction
public:

	CRTCS(CString strRTCSFileName);

// Operations
	BOOL	OpenProductFiles();
	BOOL	LoadProductFiles();
	CString GetMapFilename(){ return m_strMapName; }
	CString GetLisFilename(){ return m_strLisName; }
	void	DeleteProductFiles()
	{
		DeleteFile(m_strLisName);
		DeleteFile(m_strMapName);
	}

private:

	int		CopyProlog();
	BOOL	CheckValid(CString);
	CString	ExtractValue(const CString, const CString);
	CString	MakeFilepath(CString, int);
	CString RemoveFirstToken(CString*);

// Attributes
	CFileException	m_fileException;
	CTextFile 		m_textSetFile;
	CTextFile 		m_textLisFile;
	CTextFile 		m_textMapFile;
	CString			m_strRTCSName;
	CString			m_strUserName;
	CString			m_strMapName;
	CString			m_strLisName;
};

#endif  // __RTCS_H__
