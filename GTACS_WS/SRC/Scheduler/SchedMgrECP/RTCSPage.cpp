// RTCSPage.cpp : implementation file
// 
// MSD (06/12) : Implemented default options during SchedUpd.

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"
#include "FinishDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRTCSPage property page

IMPLEMENT_DYNCREATE(CRTCSPage, CResizablePage)

CRTCSPage::CRTCSPage() : CResizablePage(CRTCSPage::IDD)
{
	//{{AFX_DATA_INIT(CRTCSPage)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CRTCSPage::~CRTCSPage()
{
}

void CRTCSPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRTCSPage)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CRTCSPage, CResizablePage)
	//{{AFX_MSG_MAP(CRTCSPage)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_RTCS_UPD_CHECK, OnUpdateControls)
	ON_BN_CLICKED(IDC_RTCS_GEN_CHECK, OnUpdateControls)
	ON_EN_CHANGE(IDC_RTCS_MAP_EDIT, OnChangeMapEdit)
	ON_BN_CLICKED(IDC_RTCS_MAP_BUTTON, OnMapButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CRTCSPage::Init(BOOL bFirstWizPage, BOOL bLastWizPage, BOOL bGen, BOOL bSchedUpd)
{
	m_bFirstWizPage		= bFirstWizPage;
	m_bLastWizPage		= bLastWizPage;
	m_bGen				= bGen;	
	//PR00446: MSD (06/12) - Only use registry values when not doing SchedUpd.
//	m_bGen				= GetAppRegMakeSched()->m_nRTCSGen;
	if(!bSchedUpd) 
		m_bUse				= GetAppRegMakeSched()->m_nRTCSUpdate;
	else
		m_bUse				= nRegMakeSchedRTCSUpdateDefValue;
	m_bSchedUpd			= bSchedUpd;
}

BOOL CRTCSPage::OnInitDialog() 
{
	CResizablePage::OnInitDialog();
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	m_bInInit	= TRUE;
	
	if ((pSheet->GetFunc() == nPackage) ||
		(pSheet->GetFunc() == nUpdSTOL))
	{
//		int	nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//		int	nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
		
		CString strExt;
		GetAppDocTemplate(nRTCSMap)->GetDocString(strExt, CDocTemplate::filterExt);

		CString strTemp = GetAppRegMakeSched()->m_strRTCSMapFileName;
//		m_strMapFilename.Format(_T("\\\\%s\\%s\\%s\\%s\\%s%s"),
//			GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
//			GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
		m_strMapFilename.Format(_T("%s\\%s\\%s\\%s\\%s\\%s%s"),

			ctcProcDrive, ctcMountPoint, ctcProcsDir,
			GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],		// SC Specific Directory
			GetAppRegFolder()->m_strDir[nFolderRTCS],					// RTCS directory
			GetAppRegMakeSched()->m_strRTCSMapFileName,
			strExt);				// Map Filename excluding extension from Registry
	}
	else
		m_strMapFilename.Empty();

	// Set all of the necessary anchors
	AddAnchor(IDC_RTCS_UPD_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_RTCS_UPD_CHECK, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_RTCS_MAP_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_RTCS_MAP_BUTTON, TOP_RIGHT, TOP_RIGHT);
	
	AddAnchor(IDC_RTCS_GEN_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_RTCS_GEN_CHECK, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_RTCS_GEN_BIN_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_RTCS_GEN_BIN_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_RTCS_GEN_PROC_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_RTCS_GEN_PROC_STATIC, TOP_LEFT, TOP_RIGHT);

	GetDlgItem(IDC_RTCS_MAP_EDIT)->SetWindowText(m_strMapFilename);
	
	// Add the images to the page
	CImageList imageListRTCS;
	CImageList imageListSTOL;
	imageListRTCS.Create(IDR_RTCS_MENUIMAGES, 16, 0, RGB (192,192,192));
	imageListSTOL.Create(IDR_STOL_MENUIMAGES, 16, 0, RGB (192,192,192));
	((CStatic*)GetDlgItem(IDC_RTCS_GEN_ICON))->SetIcon(imageListRTCS.ExtractIcon(0));
	((CStatic*)GetDlgItem(IDC_RTCS_UPD_ICON))->SetIcon(imageListSTOL.ExtractIcon(3));
	
	GetDlgItem(IDC_RTCS_GEN_ICON)->BringWindowToTop();
	GetDlgItem(IDC_RTCS_UPD_ICON)->BringWindowToTop();

	BOOL bGenEnable = FALSE;
	BOOL bUseEnable = FALSE;
	
	switch (pSheet->GetFunc())
	{
		case nUpdSTOL:	bUseEnable = TRUE;	bGenEnable = FALSE;	break;
		case nPackage:	bUseEnable = TRUE;	bGenEnable = FALSE;	break;
		case nValRTCS:	bUseEnable = FALSE;	bGenEnable = FALSE;	break;	
		default:		bUseEnable = FALSE;	bGenEnable = FALSE;	break;
	}
	
	GetDlgItem(IDC_RTCS_UPD_GROUP)->EnableWindow(bUseEnable);
	GetDlgItem(IDC_RTCS_UPD_CHECK)->EnableWindow(bUseEnable);
	GetDlgItem(IDC_RTCS_UPD_ICON)->EnableWindow(bUseEnable);
	GetDlgItem(IDC_RTCS_MAP_LABEL)->EnableWindow(bUseEnable);
	GetDlgItem(IDC_RTCS_MAP_EDIT)->EnableWindow(bUseEnable);
	GetDlgItem(IDC_RTCS_MAP_BUTTON)->EnableWindow(bUseEnable);

	GetDlgItem(IDC_RTCS_GEN_GROUP)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_RTCS_GEN_CHECK)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_RTCS_GEN_ICON)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_RTCS_GEN_BIN_LABEL)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_RTCS_GEN_BIN_STATIC)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_RTCS_GEN_PROC_LABEL)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_RTCS_GEN_PROC_STATIC)->EnableWindow(bGenEnable);
		
	m_bInInit = FALSE;

	return TRUE;
}

BOOL CRTCSPage::OnSetActive()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	// Update all of the check boxes with the proper status
	((CButton*)GetDlgItem(IDC_RTCS_UPD_CHECK))->SetCheck(m_bUse);
	((CButton*)GetDlgItem(IDC_RTCS_GEN_CHECK))->SetCheck(m_bGen);
	
	// Update the visibility of the controls based upon the radio button positions
	// Update the filenames
	UpdateControls();

	// Setup the proper Wizard Buttons
	DWORD dWizFlags = 0;
	if ((m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_FINISH;
	else if ((m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_NEXT;
	else if ((!m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_FINISH;
	else if ((!m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_NEXT;
	
	pSheet->SetWizardButtons(dWizFlags);

	return CResizablePage::OnSetActive();
}

void CRTCSPage::UpdateControls()
{
	OnUpdateControls();
}

void CRTCSPage::OnUpdateControls()
{
	m_bUse = ((CButton*)GetDlgItem(IDC_RTCS_UPD_CHECK))->GetCheck();
	m_bGen = ((CButton*)GetDlgItem(IDC_RTCS_GEN_CHECK))->GetCheck();
	if(!m_bSchedUpd) {
		GetAppRegMakeSched()->m_nRTCSUpdate  = m_bUse;
		GetAppRegMakeSched()->m_nRTCSGen = m_bGen;
	}

	GetDlgItem(IDC_RTCS_MAP_LABEL)->EnableWindow(m_bUse);
	GetDlgItem(IDC_RTCS_MAP_EDIT)->EnableWindow(m_bUse);
	GetDlgItem(IDC_RTCS_MAP_BUTTON)->EnableWindow(m_bUse);

	GetDlgItem(IDC_RTCS_GEN_BIN_LABEL)->EnableWindow(m_bGen);
	GetDlgItem(IDC_RTCS_GEN_BIN_STATIC)->EnableWindow(m_bGen);
	GetDlgItem(IDC_RTCS_GEN_PROC_LABEL)->EnableWindow(m_bGen);
	GetDlgItem(IDC_RTCS_GEN_PROC_STATIC)->EnableWindow(m_bGen);
}

void CRTCSPage::OnSize(UINT nType, int cx, int cy) 
{
	CResizablePage::OnSize(nType, cx, cy);
	ResizeFilenames();
}	

void CRTCSPage::ResizeFilenames()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	CString strBfilename(pSheet->MakeFilepath(nFolderRTCS, nBinary,	TRUE, FALSE));
	CString strLfilename(pSheet->MakeFilepath(nFolderRTCS, nSTOL,	TRUE, FALSE));

	PathSetDlgItemPath(this->m_hWnd, IDC_RTCS_GEN_BIN_STATIC,	strBfilename);
	PathSetDlgItemPath(this->m_hWnd, IDC_RTCS_GEN_PROC_STATIC,	strLfilename);
}

void CRTCSPage::OnChangeMapEdit() 
{
	GetDlgItem(IDC_RTCS_MAP_EDIT)->GetWindowText(m_strMapFilename);
}

void CRTCSPage::OnMapButton() 
{
	COXUNC	uncFilename(m_strMapFilename);
	CString strExt;
	CString strFilter;

	GetAppDocTemplate(nSTOLMap)->GetDocString(strExt,	CDocTemplate::filterExt);
	GetAppDocTemplate(nSTOLMap)->GetDocString(strFilter,CDocTemplate::filterName);
	strFilter = strFilter + _T("|*") + strExt + _T("||");
	CFileDialog dlg(TRUE, strExt, m_strMapFilename, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter, NULL);
//	CFileDialog dlg(TRUE, strExt, uncFilename.Base(), OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter, NULL);
//	CString	strInitialDir = uncFilename.Server() + uncFilename.Share() + uncFilename.Directory();
//	dlg.m_ofn.lpstrInitialDir = strInitialDir;

	if (dlg.DoModal())
	{
		m_strMapFilename = dlg.GetPathName();
		GetDlgItem(IDC_RTCS_MAP_EDIT)->SetWindowText(m_strMapFilename);
		COXUNC	uncRetrievedFilename (m_strMapFilename);
		uncRetrievedFilename.Full();
		GetAppRegMakeSched()->m_strRTCSMapFileName =	uncRetrievedFilename.Base();
	}	
}

BOOL CRTCSPage::OnWizardFinish() 
{
	CFinishDialog dlg;
	if (dlg.DoModal() == IDOK)
		return TRUE;
	else
		return FALSE;
}