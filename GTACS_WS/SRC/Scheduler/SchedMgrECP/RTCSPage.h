#if !defined(AFX_RTCSPAGE_H__3B36ADA5_08ED_11D5_8019_00609704053C__INCLUDED_)
#define AFX_RTCSPAGE_H__3B36ADA5_08ED_11D5_8019_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RTCSPage.h : header file
//
// MSD (06/12) PR000446: Implemented default options during SchedUpd

class CRTCSPage : public CResizablePage
{
	DECLARE_DYNCREATE(CRTCSPage)

// Construction
public:
	CRTCSPage();
	~CRTCSPage();
	void Init(BOOL bFirstWizPage, BOOL bLastWizPage, BOOL bVal, BOOL bSchedUpd);
	BOOL GetGen(){return m_bGen;};
	BOOL GetUse(){return m_bUse;};
	CString GetMapFilename(){return m_strMapFilename;};
	
protected:
	// Dialog Data
	//{{AFX_DATA(CRTCSPage)
	enum { IDD = IDD_RTCS_DIALOG };
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CRTCSPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CRTCSPage)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnSetActive();
	afx_msg void OnUpdateControls();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnChangeMapEdit();
	afx_msg void OnMapButton();
	virtual BOOL OnWizardFinish();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void	UpdateControls();
	void	ResizeFilenames();
	BOOL	m_bUse;				// TRUE if RTCSs should be used
	BOOL	m_bGen;				// Generate upload - if TRUE, then generate upload files
	BOOL    m_bSchedUpd;		// TRUE if doing schedule update, so we don't store values to reg.
	BOOL	m_bFirstWizPage;	// TRUE if this the first page of a Wizard
	BOOL	m_bLastWizPage;		// TRUE if this the last page of a Wizard
	CString	m_strMapFilename;	// Sector filename
	BOOL	m_bInInit;			// TRUE when in InitInstance.  This is needed so that the OnUpdate
								// routines get ignored for the day and version controls.  These
								// controls are COXMaskedEdit classes and the OnUpdate functions get
								// called whenever the mask and prompt symbol is set (in addition to
								// when the value is changed).  Because of this, we want to ignore
								// processing in these routines when we are initializing the control
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RTCSPAGE_H__3B36ADA5_08ED_11D5_8019_00609704053C__INCLUDED_)
