#if !defined( RCMDCLASSESSSHPP)
#define RCMDCLASSESSSHPP
#include "sslex.hpp"
#include "ssyacc.hpp"
#include "GoldWrapper.h"

class RcmdLexClass : public SSLex
{
public:
	SSConstr RcmdLexClass( const char*);
	SSConstr RcmdLexClass( const char*, const char*);
	SSConstr RcmdLexClass( SSLexConsumer&, SSLexTable&);

	const char* tokenToConstChar( SSUnsigned32);
};

SSInline RcmdLexClass::RcmdLexClass( const char* pszFile) : 
	SSLex( pszFile, ".\\Rcmd.dfa")
{
}

SSInline RcmdLexClass::RcmdLexClass( const char* pszFile, const char* pszTable) : 
	SSLex( pszFile, pszTable)
{
}

SSInline RcmdLexClass::RcmdLexClass( SSLexConsumer& Consumer, SSLexTable& Table) : 
	SSLex( Consumer, Table)
{
}

class RcmdYaccClass : public GoldWrapper
{
public:
	SSConstr RcmdYaccClass( const char*);

	SSYaccStackElement* reduce( SSUnsigned32, SSUnsigned32);

	virtual SSDestr RcmdYaccClass( void);

	virtual SSBooleanValue error( SSUnsigned32, SSLexLexeme&);
	void parseError(SSLexLexeme& qLookahead);
	void filename(CString filename) { m_strFilename = filename; }
	void errorCount(int* nErrors)	{ m_pnErrorCnt = nErrors; }

	IEpEnumCommands*	m_pEnumCmds;
	CString				m_strFilename;
	CString				m_strErrorMessage;
	int*				m_pnErrorCnt;
	int					m_nErrorNum;

};

SSInline RcmdYaccClass::RcmdYaccClass( const char* pszFile) : 
	GoldWrapper(pszFile)
{
	m_pEnumCmds	= NULL;
	m_pnErrorCnt= NULL;
	m_nErrorNum	= 0;
}

SSInline RcmdYaccClass::~RcmdYaccClass( void)
{

	if( m_pEnumCmds != NULL )
	{
		m_pEnumCmds->Release();
		m_pEnumCmds = NULL;
	}
	
	m_pnErrorCnt = NULL;
}

#endif
