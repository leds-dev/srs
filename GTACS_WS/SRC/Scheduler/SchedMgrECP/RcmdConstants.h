﻿enum SymbolConstants
{
   SYM_EOF           =  0, // (EOF)
   SYM_ERROR         =  1, // (Error)
   SYM_COMMENTS      =  2, // Comments
   SYM_WHITESPACE    =  3, // Whitespace
   SYM_PERCENTERROR  =  4, // '%error'
   SYM_TIMES         =  5, // '*'
   SYM_CMDALLOW      =  6, // CMDALLOW
   SYM_IDENTIFIER    =  7, // IDENTIFIER
   SYM_NEWLINE       =  8, // NewLine
   SYM_EOL           =  9, // <eol>
   SYM_NL            = 10, // <nl>
   SYM_RCMD          = 11, // <rcmd>
   SYM_START         = 12, // <start>
   SYM_STATEMENT     = 13, // <statement>
   SYM_STATEMENTLIST = 14, // <statementList>
   SYM_WILDCARD      = 15  // <wildCard>
};

enum ProductionConstants
{
   PROD_EOL                             =  0, // <eol> ::= <nl>
   PROD_NL_NEWLINE                      =  1, // <nl> ::= NewLine
   PROD_START                           =  2, // <start> ::= <statementList>
   PROD_STATEMENTLIST                   =  3, // <statementList> ::= <statementList> <statement>
   PROD_STATEMENTLIST2                  =  4, // <statementList> ::= <statement>
   PROD_STATEMENT                       =  5, // <statement> ::= <rcmd>
   PROD_STATEMENT_PERCENTERROR          =  6, // <statement> ::= '%error' <eol>
   PROD_STATEMENT2                      =  7, // <statement> ::= <eol>
   PROD_RCMD_IDENTIFIER                 =  8, // <rcmd> ::= IDENTIFIER <eol>
   PROD_RCMD_CMDALLOW_IDENTIFIER        =  9, // <rcmd> ::= CMDALLOW IDENTIFIER <eol>
   PROD_RCMD                            = 10, // <rcmd> ::= <wildCard> <eol>
   PROD_RCMD_CMDALLOW                   = 11, // <rcmd> ::= CMDALLOW <wildCard> <eol>
   PROD_WILDCARD_TIMES_IDENTIFIER_TIMES = 12, // <wildCard> ::= '*' IDENTIFIER '*'
   PROD_WILDCARD_TIMES_IDENTIFIER       = 13, // <wildCard> ::= '*' IDENTIFIER
   PROD_WILDCARD_IDENTIFIER_TIMES       = 14  // <wildCard> ::= IDENTIFIER '*'
};
