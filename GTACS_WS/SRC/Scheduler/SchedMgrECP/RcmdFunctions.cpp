#include "stdafx.h"
#if !defined( RCMDFUNCTIONSSSCPP)
#define RCMDFUNCTIONSSSCPP
#include "RcmdConstants.h"
#include "RcmdClasses.h"

const char* RcmdLexClass::tokenToConstChar( SSUnsigned32 ulToken)
{
	const char* pchToken;
	switch ( ulToken)
	{
	case SYM_EOL:
		pchToken = "eol";
		break;

	case SYM_IDENTIFIER:
		pchToken = "IDENTIFIER";
		break;

	//case RcmdLexTokenDec:
	//	pchToken = "INTEGERconstant";
	//	break;

	//case RcmdLexTokenFloat0:
	//	pchToken = "FLOATINGconstant0";
	//	break;

	//case RcmdLexTokenOParen:
	//	pchToken = "(";
	//	break;

	//case RcmdLexTokenCParen:
	//	pchToken = ")";
	//	break;

	//case RcmdLexTokenComma:
	//	pchToken = ",";
	//	break;

	//case RcmdLexTokenColon:
	//	pchToken = ":";
	//	break;

	//case RcmdLexTokenSemiColon:
	//	pchToken = ";";
	//	break;

	//case RcmdLexTokenAssign:
	//	pchToken = "=";
	//	break;

	//case RcmdLexTokenAt:
	//	pchToken = "@";
	//	break;

	// case RcmdLexTokenUnderScore:
	//	pchToken = "_";
	//	break;

	//case RcmdLexTokenOBrack:
	//	pchToken = "[";
	//	break;

	//case RcmdLexTokenCBrack:
	//	pchToken = "]";
	//	break;

	//case RcmdLexTokenOBrace:
	//	pchToken = "{";
	//	break;

	//case RcmdLexTokenCBrace:
	//	pchToken = "}";
	//	break;

	//case RcmdLexTokenDollar:
	//	pchToken = "$";
	//	break;

	//case RcmdLexTokenBackSlash:
	//	pchToken = "\\";
	//	break;

	//case RcmdLexTokenForwardSlash:
	//	pchToken = "/";
	//	break;

	//case RcmdLexTokenDot:
	//	pchToken = ".";
	//	break;

	//case RcmdLexTokenPlus:
	//	pchToken = "+";
	//	break;

	//case RcmdLexTokenMinus:
	//	pchToken = "-";
	//	break;

	//case RcmdLexTokenStar:
	//	pchToken = "*";
	//	break;

	//case RcmdLexTokenExPoint:
	//	pchToken = "!";
	//	break;

	//case RcmdLexTokenMod:
	//	pchToken = "%";
	//	break;

	//case RcmdLexTokenHat:
	//	pchToken = "^";
	//	break;

	//case RcmdLexTokenAmpersand:
	//	pchToken = "&";
	//	break;

	//case RcmdLexTokenSquiggle:
	//	pchToken = "~";
	//	break;

	case SYM_CMDALLOW:
		pchToken = "##CMDALLOW";
		break;

	case SSYaccErrorToken:
		pchToken = "%error";
		break;

	case SSYaccEofToken:
		pchToken = "eof";
		break;

	default:
		pchToken = SSLexTokenNotFound;
	}
	return pchToken;
}

#endif
