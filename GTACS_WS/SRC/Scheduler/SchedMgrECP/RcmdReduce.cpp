/**
/////////////////////////////////////////////////////////////////////////////
// RcmdReduce.cpp : implementation of the RcmdYaccClass class.             //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// RcmdReduce.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is the main driver for the Rcmd parser class.
// 
/////////////////////////////////////////////////////////////////////////////
**/
#include "stdafx.h"
#if !defined( RCMDREDUCESSCPP)
#define RCMDREDUCESSCPP
#include "SchedMgrECP.h"
#include "RcmdClasses.h"
#include "RcmdConstants.h"

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RcmdYaccClass::error
//	Description :	This routine handles all cls parsing errors.
//
//	Return :		SSBooleanValue	-	SSTrue if error count is less 
//										than 100. (continue to parse)
//	Parameters :
//		SSUnsigned32 qulState	-	state of the lookahead symbol.
//		SSLexLexeme& qLookahead	-	the token that caused the error.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
SSBooleanValue RcmdYaccClass::error(SSUnsigned32 qulState, SSLexLexeme& qLookahead)
{
	if((*m_pnErrorCnt) >= 100)
	{
		CString strMsg;

		strMsg.Format(IDS_SYNTAX_ERROR_COUNT, 100);
		GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);

		return SSTrue;
	}

	m_nErrorNum = 0;
	parseError(qLookahead);
	return GoldWrapper::error(qulState, qLookahead);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RcmdYaccClass::parseError
//	Description :	This routine outputs an event message that 
//					contains the token that caused the error, the line
//					number, and the offset of where the error occurred.
//
//	Return :		void	-
//
//	Parameters :
//		SSLexLexeme& qLookahead	-	the token that caused the error.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RcmdYaccClass::parseError(SSLexLexeme& qLookahead)
{
	CString strMsg;
	CString	lexeme(qLookahead.asChar());
	int		line	= qLookahead.line();
	int		offset	= qLookahead.offset();

	(*m_pnErrorCnt)++;

	if((*m_pnErrorCnt) >= 100)
	{
		error(0, qLookahead);
		return;
	}

	if(offset > 0)
		offset = offset - 1;

	if(lexeme == _T("\n"))
	{
		m_nErrorNum	= IDS_SYNTAX_ERROR21;
		lexeme.Format(IDS_SYNTAX_ERROR21);
	}

	if(m_strErrorMessage.IsEmpty())
		m_strErrorMessage = _T("syntax error");

	if(m_nErrorNum == 0)
		m_nErrorNum	= (*m_pnErrorCnt);

	strMsg.Format(_T("%s(%d) : error %d: \'%s\': %s"),
		m_strFilename, line, m_nErrorNum, lexeme, m_strErrorMessage);
	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL, line, offset);
	m_strErrorMessage.Empty();
	m_nErrorNum = 0;
}

/*
	You must keep this reduce function clean to avoid confusing
	the AutoMerge parser. By 'clean' we mean that if you are going
	to add a significant amount of code to a case, make a function
	call instead of adding the code directly to the case.

	NEVER add embedded switch statements inside this function.
*/
/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RcmdYaccClass::reduce
//	Description :	This routine is called each time  a grammer rule,
//					or production, is recognized.
//
//	Return :		SSYaccStackElement*	-	element that is associated
//											with the token.
//
//	Parameters :
//		SSUnsigned32 ulProd	-	production element number.
//		SSUnsigned32 ulSize	-	
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
SSYaccStackElement* RcmdYaccClass::reduce( SSUnsigned32 ulProd,
      SSUnsigned32 ulSize)
{

	//Uncomment out this line if you want to build a parse tree
	//The treeRoot() function will contain the tree root when finished
	//return addSubTree();

	CSchedMgrECPApp*	pApp = (CSchedMgrECPApp*)AfxGetApp();
	HRESULT				hr;

	switch ( ulProd)
	{
		case PROD_STATEMENT: /* RcmdYaccProdStateRcmd: */
		// statement -> rcmd 
		{ 
			if(m_pEnumCmds != NULL)
			{
				m_pEnumCmds->Release();
				m_pEnumCmds = NULL;
			}

			break;
		}

		case PROD_RCMD_IDENTIFIER: /*RcmdYaccProdRcmd:*/
		// rcmd -> IDENTIFIER eol 
		{ 
			//database call for lexeme 0
			SSLexLexeme*		z_pLexeme = elementFromProduction(0)->lexeme();
			CComBSTR			bstrMnemonic(z_pLexeme->asConstChar());

			hr = (pApp->m_pEpDB)->GetCommandEnum(pApp->m_ulDBHandle, bstrMnemonic, &m_pEnumCmds);

			if (FAILED(hr))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR4;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR4);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_RCMD_CMDALLOW_IDENTIFIER: /*RcmdYaccProdAllowCmd:*/
		// rcmd -> ##CMDALLOW IDENTIFIER eol 
		{ 
			//database call for lexeme 1
			SSLexLexeme*		z_pLexeme = elementFromProduction(1)->lexeme();
			CComBSTR			bstrMnemonic(z_pLexeme->asConstChar());

			hr = (pApp->m_pEpDB)->GetCommandEnum(pApp->m_ulDBHandle, bstrMnemonic, &m_pEnumCmds);

			if (FAILED(hr))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR4;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR4);
				parseError(*z_pLexeme);
			}

			break;
		}

		case PROD_WILDCARD_TIMES_IDENTIFIER:
		// wildCard -> * IDENTIFIER * 
		case PROD_WILDCARD_TIMES_IDENTIFIER_TIMES: /*RcmdYaccProdWildCardOpt2:*/
		// wildCard -> * IDENTIFIER 
		{ 
			//database call for lexeme 1
			SSLexLexeme*		z_pLexeme	= elementFromProduction(1)->lexeme();
			BOOL				bFound		= FALSE;
			ULONG				ulFetched;
			CString				strMnemonic;
			CString				strToken(z_pLexeme->asConstChar());
			EpDbCmdDescription 	cCmd[5000];

			hr = (pApp->m_pEpDB)->GetCommandListEnum(pApp->m_ulDBHandle, &m_pEnumCmds);

			if (FAILED(hr))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR4;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR4);
				parseError(*z_pLexeme);
				break;
			}

			while (!bFound)
			{
				hr = m_pEnumCmds->Next(1, cCmd, &ulFetched);
	
				if (ulFetched == 0)  break;

				for (unsigned int nIndex=0; nIndex<ulFetched; nIndex++)
				{
					strMnemonic = cCmd[nIndex].bstrMnemonic;

					if(strMnemonic.Find(strToken) > -1)
					{
						SysFreeString(cCmd[nIndex].bstrMnemonic);
						SysFreeString(cCmd[nIndex].bstrDescription);
						SysFreeString(cCmd[nIndex].bstrAlias);
						SysFreeString(cCmd[nIndex].bstrExecMnemonic);
						SysFreeString(cCmd[nIndex].bstrCmdFormat);
						SysFreeString(cCmd[nIndex].bstrSpacecraftAddr);
						SysFreeString(cCmd[nIndex].bstrPrivilegeGroup);

						bFound = TRUE;
						break;
					}

					SysFreeString(cCmd[nIndex].bstrMnemonic);
					SysFreeString(cCmd[nIndex].bstrDescription);
					SysFreeString(cCmd[nIndex].bstrAlias);
					SysFreeString(cCmd[nIndex].bstrExecMnemonic);
					SysFreeString(cCmd[nIndex].bstrCmdFormat);
					SysFreeString(cCmd[nIndex].bstrSpacecraftAddr);
					SysFreeString(cCmd[nIndex].bstrPrivilegeGroup);
				}
			}

			if (!bFound)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR4;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR4);
				parseError(*z_pLexeme);
			}

			break;
		}

//		case RcmdYaccProdWildCardOpt3:
//		// wildCard -> IDENTIFIER * IDENTIFIER 
//		{ 
//			//database call for lexeme 0
//			SSLexLexeme*		z_pLexeme0	= elementFromProduction(0)->lexeme();
//			SSLexLexeme*		z_pLexeme2	= elementFromProduction(2)->lexeme();
//			BOOL				bFound		= FALSE;
//			ULONG				ulFetched;
//			CString				strMnemonic;
//			CString				strToken0(z_pLexeme0->asConstChar());
//			CString				strToken2(z_pLexeme2->asConstChar());
//			EpDbCmdDescription 	cCmd[5000];
//
//			hr = (pApp->m_pEpDB)->GetCommandListEnum(pApp->m_ulDBHandle, &m_pEnumCmds);
//
//			if (FAILED(hr))
//			{
//				m_nErrorNum = IDS_SYNTAX_ERROR4;
//				m_strErrorMessage.Format(IDS_SYNTAX_ERROR4);
//				parseError(*z_pLexeme0);
//				break;
//			}
//
//			while (!bFound)
//			{
//				hr = m_pEnumCmds->Next(1, cCmd, &ulFetched);
//	
//				if (ulFetched == 0)  break;
//
//				for (unsigned int nIndex=0; nIndex<ulFetched; nIndex++)
//				{
//					strMnemonic = cCmd[nIndex].bstrMnemonic;
//
//					if(strMnemonic.Find(strToken0) > -1 && strMnemonic.Find(strToken2) > -1)
//					{
//						SysFreeString(cCmd[nIndex].bstrMnemonic);
//						SysFreeString(cCmd[nIndex].bstrDescription);
//						SysFreeString(cCmd[nIndex].bstrAlias);
//						SysFreeString(cCmd[nIndex].bstrExecMnemonic);
//						SysFreeString(cCmd[nIndex].bstrCmdFormat);
//						SysFreeString(cCmd[nIndex].bstrSpacecraftAddr);
//						SysFreeString(cCmd[nIndex].bstrPrivilegeGroup);
//
//						bFound = TRUE;
//						break;
//					}
//
//					SysFreeString(cCmd[nIndex].bstrMnemonic);
//					SysFreeString(cCmd[nIndex].bstrDescription);
//					SysFreeString(cCmd[nIndex].bstrAlias);
//					SysFreeString(cCmd[nIndex].bstrExecMnemonic);
//					SysFreeString(cCmd[nIndex].bstrCmdFormat);
//					SysFreeString(cCmd[nIndex].bstrSpacecraftAddr);
//					SysFreeString(cCmd[nIndex].bstrPrivilegeGroup);
//				}
//			}
//
//			if (!bFound)
//			{
//				m_nErrorNum = IDS_SYNTAX_ERROR4;
//				m_strErrorMessage.Format(IDS_SYNTAX_ERROR4);
//				parseError(*z_pLexeme0);
//			}
//
//			break;
//		}
//
		case PROD_WILDCARD_IDENTIFIER_TIMES: /*RcmdYaccProdWildCardOpt4:*/
		// wildCard -> IDENTIFIER * 
		{ 
			//database call for lexeme 0
			SSLexLexeme*		z_pLexeme	= elementFromProduction(0)->lexeme();
			BOOL				bFound		= FALSE;
			ULONG				ulFetched;
			CString				strMnemonic;
			CString				strToken(z_pLexeme->asConstChar());
			EpDbCmdDescription 	cCmd[5000];

			hr = (pApp->m_pEpDB)->GetCommandListEnum(pApp->m_ulDBHandle, &m_pEnumCmds);

			if (FAILED(hr))
			{
				m_nErrorNum = IDS_SYNTAX_ERROR4;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR4);
				parseError(*z_pLexeme);
				break;
			}

			while (!bFound)
			{
				hr = m_pEnumCmds->Next(1, cCmd, &ulFetched);
	
				if (ulFetched == 0)  break;

				for (unsigned int nIndex=0; nIndex<ulFetched; nIndex++)
				{
					strMnemonic = cCmd[nIndex].bstrMnemonic;

					if(strMnemonic.Find(strToken) > -1)
					{
						SysFreeString(cCmd[nIndex].bstrMnemonic);
						SysFreeString(cCmd[nIndex].bstrDescription);
						SysFreeString(cCmd[nIndex].bstrAlias);
						SysFreeString(cCmd[nIndex].bstrExecMnemonic);
						SysFreeString(cCmd[nIndex].bstrCmdFormat);
						SysFreeString(cCmd[nIndex].bstrSpacecraftAddr);
						SysFreeString(cCmd[nIndex].bstrPrivilegeGroup);

						bFound = TRUE;
						break;
					}

					SysFreeString(cCmd[nIndex].bstrMnemonic);
					SysFreeString(cCmd[nIndex].bstrDescription);
					SysFreeString(cCmd[nIndex].bstrAlias);
					SysFreeString(cCmd[nIndex].bstrExecMnemonic);
					SysFreeString(cCmd[nIndex].bstrCmdFormat);
					SysFreeString(cCmd[nIndex].bstrSpacecraftAddr);
					SysFreeString(cCmd[nIndex].bstrPrivilegeGroup);
				}
			}

			if (!bFound)
			{
				m_nErrorNum = IDS_SYNTAX_ERROR4;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR4);
				parseError(*z_pLexeme);
			}

			break;
		}

		default:/*Reduce*/
			break;
	}

	return stackElement();
}

#endif










