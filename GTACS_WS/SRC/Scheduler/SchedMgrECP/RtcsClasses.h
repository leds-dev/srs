#if !defined( RTCSCLASSESSSHPP)
#define RTCSCLASSESSSHPP
#include "sslex.hpp"
#include "ssyacc.hpp"
#include "GoldWrapper.h"

class RtcsLexClass : public SSLex
{
public:
	SSConstr RtcsLexClass( const char*);
	SSConstr RtcsLexClass( const char*, const char*);
	SSConstr RtcsLexClass( SSLexConsumer&, SSLexTable&);

	const char* tokenToConstChar( SSUnsigned32);
};

SSInline RtcsLexClass::RtcsLexClass( const char* pszFile) : 
	SSLex( pszFile, ".\\Rtcs.dfa")
{
}

SSInline RtcsLexClass::RtcsLexClass( const char* pszFile, const char* pszTable) : 
	SSLex( pszFile, pszTable)
{
}

SSInline RtcsLexClass::RtcsLexClass( SSLexConsumer& Consumer, SSLexTable& Table) : 
	SSLex( Consumer, Table)
{
}

class RtcsYaccClass : public GoldWrapper
{
public:
	SSConstr RtcsYaccClass( const char*);

	SSYaccStackElement* reduce( SSUnsigned32, SSUnsigned32);

	virtual SSDestr RtcsYaccClass( void);
	virtual SSBooleanValue error( SSUnsigned32, SSLexLexeme&);
	void parseError(SSLexLexeme& qLookahead);
	void filename(CString filename) { m_strFilename = filename; }
	void errorCount(int* nErrors)	{ m_pnErrorCnt = nErrors; }

	IEpEnumCommands*	m_pEnumCmds;
	CMapStringToOb		m_mapTlmType;
	CString				m_strErrorMessage;
	CString				m_strFilename;
	int*				m_pnErrorCnt;
	int					m_nErrorNum;

};

SSInline RtcsYaccClass::RtcsYaccClass( const char* pszFile) : 
	GoldWrapper(pszFile)
{
	m_pEnumCmds	= NULL;
	m_nErrorNum = 0;
}

SSInline RtcsYaccClass::~RtcsYaccClass( void)
{			
	if( m_pEnumCmds != NULL )
	{
		m_pEnumCmds->Release();
		m_pEnumCmds = NULL;
	}
}

#endif
