﻿enum SymbolConstants
{
   SYM_EOF                      =  0, // (EOF)
   SYM_ERROR                    =  1, // (Error)
   SYM_COMMENTS                 =  2, // Comments
   SYM_WHITESPACE               =  3, // Whitespace
   SYM_MINUS                    =  4, // '-'
   SYM_DOLLAR                   =  5, // '$'
   SYM_PERCENTERROR             =  6, // '%error'
   SYM_LPAREN                   =  7, // '('
   SYM_RPAREN                   =  8, // ')'
   SYM_TIMES                    =  9, // '*'
   SYM_TIMESTIMES               = 10, // '**'
   SYM_DIV                      = 11, // '/'
   SYM_SEMI                     = 12, // ';'
   SYM_AT                       = 13, // '@'
   SYM_BACKSLASH                = 14, // '\'
   SYM__                        = 15, // '_'
   SYM_PLUS                     = 16, // '+'
   SYM_EQ                       = 17, // '='
   SYM_ARGS                     = 18, // ARGS
   SYM_AUTO_EXEC                = 19, // 'AUTO_EXEC'
   SYM_CLEAR                    = 20, // CLEAR
   SYM_CMD                      = 21, // CMD
   SYM_CV                       = 22, // CV
   SYM_DEL                      = 23, // DEL
   SYM_EXEC_AND_STORE           = 24, // 'EXEC_AND_STORE'
   SYM_FILE                     = 25, // FILE
   SYM_FLOATINGCONSTANT         = 26, // FLOATINGconstant
   SYM_FLOATINGCONSTANT2        = 27, // 'FLOATINGconstant2'
   SYM_HEXCONSTANT              = 28, // HEXconstant
   SYM_IDENTIFIER               = 29, // IDENTIFIER
   SYM_IMM                      = 30, // IMM
   SYM_INTEGERCONSTANT          = 31, // INTEGERconstant
   SYM_LIFO                     = 32, // LIFO
   SYM_LITERALSTRING            = 33, // LiteralString
   SYM_LOAD_WORDS               = 34, // 'LOAD_WORDS'
   SYM_NEWLINE                  = 35, // NewLine
   SYM_NOWAIT                   = 36, // NOWAIT
   SYM_OCTALCONSTANT            = 37, // OCTALconstant
   SYM_OFF                      = 38, // OFF
   SYM_ON                       = 39, // ON
   SYM_OVERRIDE                 = 40, // OVERRIDE
   SYM_PV                       = 41, // PV
   SYM_RETURN                   = 42, // RETURN
   SYM_STORE_AND_EXECUTE        = 43, // 'STORE_AND_EXECUTE'
   SYM_STORE_ONLY               = 44, // 'STORE_ONLY'
   SYM_TIMECONSTANT             = 45, // TIMEconstant
   SYM_TV                       = 46, // TV
   SYM_WAIT                     = 47, // WAIT
   SYM_ADDITIVEEXPRESSION       = 48, // <additiveExpression>
   SYM_CMDMNEMONIC              = 49, // <cmdMnemonic>
   SYM_CMDOPTIONS               = 50, // <cmdOptions>
   SYM_CMDSTATEMENT             = 51, // <cmdStatement>
   SYM_CONSTANT                 = 52, // <constant>
   SYM_DATAWORDARGS             = 53, // <datawordArgs>
   SYM_DECODERADDRESS           = 54, // <decoderAddress>
   SYM_EOL                      = 55, // <eol>
   SYM_EXPRESSION               = 56, // <expression>
   SYM_HEXWORDLIST              = 57, // <hexwordList>
   SYM_JUMPSTATEMENT            = 58, // <jumpStatement>
   SYM_KEYWORDVALUE             = 59, // <keywordValue>
   SYM_KEYWORDVALUELIST         = 60, // <keywordValueList>
   SYM_MULTIPLICATIVEEXPRESSION = 61, // <multiplicativeExpression>
   SYM_NL                       = 62, // <nl>
   SYM_POSITIONAL               = 63, // <positional>
   SYM_PRIMARYEXPR              = 64, // <primaryExpr>
   SYM_PRIMARYEXPRESSION        = 65, // <primaryExpression>
   SYM_PRIMARYEXPRLIST          = 66, // <primaryExprList>
   SYM_PTOPTION                 = 67, // <ptOption>
   SYM_START                    = 68, // <start>
   SYM_STATEMENT                = 69, // <statement>
   SYM_STATEMENTLIST            = 70, // <statementList>
   SYM_STRINGLITERAL            = 71, // <stringLiteral>
   SYM_TOGGLE                   = 72, // <toggle>
   SYM_UNARYEXPRESSION          = 73, // <unaryExpression>
   SYM_UNARYOPERATOR            = 74, // <unaryOperator>
   SYM_VARIABLE                 = 75, // <variable>
   SYM_WAITSTATEMENT            = 76  // <waitStatement>
};

enum ProductionConstants
{
   PROD_EOL                                      =  0, // <eol> ::= <nl>
   PROD_NL_NEWLINE                               =  1, // <nl> ::= NewLine
   PROD_START                                    =  2, // <start> ::= <statementList>
   PROD_STATEMENTLIST                            =  3, // <statementList> ::= <statementList> <statement>
   PROD_STATEMENTLIST2                           =  4, // <statementList> ::= <statement>
   PROD_STATEMENT                                =  5, // <statement> ::= <waitStatement>
   PROD_STATEMENT2                               =  6, // <statement> ::= <jumpStatement>
   PROD_STATEMENT3                               =  7, // <statement> ::= <cmdStatement>
   PROD_STATEMENT_ARGS_LPAREN_RPAREN             =  8, // <statement> ::= ARGS '(' ')' <eol>
   PROD_STATEMENT_PERCENTERROR                   =  9, // <statement> ::= '%error' <eol>
   PROD_STATEMENT4                               = 10, // <statement> ::= <eol>
   PROD_WAITSTATEMENT_WAIT_PLUS_INTEGERCONSTANT  = 11, // <waitStatement> ::= WAIT '+' INTEGERconstant <eol>
   PROD_WAITSTATEMENT_WAIT_INTEGERCONSTANT       = 12, // <waitStatement> ::= WAIT INTEGERconstant <eol>
   PROD_WAITSTATEMENT_WAIT_PLUS_FLOATINGCONSTANT = 13, // <waitStatement> ::= WAIT '+' FLOATINGconstant <eol>
   PROD_WAITSTATEMENT_WAIT_FLOATINGCONSTANT      = 14, // <waitStatement> ::= WAIT FLOATINGconstant <eol>
   PROD_JUMPSTATEMENT_RETURN                     = 15, // <jumpStatement> ::= RETURN <eol>
   PROD_JUMPSTATEMENT_RETURN_SEMI                = 16, // <jumpStatement> ::= RETURN ';' <eol>
   PROD_JUMPSTATEMENT___RETURN                   = 17, // <jumpStatement> ::= '_' RETURN <eol>
   PROD_CMDSTATEMENT_CMD                         = 18, // <cmdStatement> ::= CMD <decoderAddress> <cmdMnemonic> <datawordArgs> <cmdOptions> <eol>
   PROD_STRINGLITERAL_LITERALSTRING              = 19, // <stringLiteral> ::= LiteralString
   PROD_CONSTANT_HEXCONSTANT                     = 20, // <constant> ::= HEXconstant
   PROD_CONSTANT_OCTALCONSTANT                   = 21, // <constant> ::= OCTALconstant
   PROD_CONSTANT_INTEGERCONSTANT                 = 22, // <constant> ::= INTEGERconstant
   PROD_CONSTANT_FLOATINGCONSTANT                = 23, // <constant> ::= FLOATINGconstant
   PROD_CONSTANT_FLOATINGCONSTANT2               = 24, // <constant> ::= 'FLOATINGconstant2'
   PROD_PRIMARYEXPRESSION                        = 25, // <primaryExpression> ::= <constant>
   PROD_PRIMARYEXPRESSION_LPAREN_RPAREN          = 26, // <primaryExpression> ::= '(' <expression> ')'
   PROD_UNARYEXPRESSION                          = 27, // <unaryExpression> ::= <primaryExpression>
   PROD_UNARYEXPRESSION2                         = 28, // <unaryExpression> ::= <unaryOperator> <constant>
   PROD_UNARYOPERATOR_PLUS                       = 29, // <unaryOperator> ::= '+'
   PROD_UNARYOPERATOR_MINUS                      = 30, // <unaryOperator> ::= '-'
   PROD_MULTIPLICATIVEEXPRESSION_TIMESTIMES      = 31, // <multiplicativeExpression> ::= <multiplicativeExpression> '**' <unaryExpression>
   PROD_MULTIPLICATIVEEXPRESSION_DIV             = 32, // <multiplicativeExpression> ::= <multiplicativeExpression> '/' <unaryExpression>
   PROD_MULTIPLICATIVEEXPRESSION_TIMES           = 33, // <multiplicativeExpression> ::= <multiplicativeExpression> '*' <unaryExpression>
   PROD_MULTIPLICATIVEEXPRESSION                 = 34, // <multiplicativeExpression> ::= <unaryExpression>
   PROD_ADDITIVEEXPRESSION_PLUS                  = 35, // <additiveExpression> ::= <additiveExpression> '+' <multiplicativeExpression>
   PROD_ADDITIVEEXPRESSION_MINUS                 = 36, // <additiveExpression> ::= <additiveExpression> '-' <multiplicativeExpression>
   PROD_ADDITIVEEXPRESSION                       = 37, // <additiveExpression> ::= <multiplicativeExpression>
   PROD_EXPRESSION                               = 38, // <expression> ::= <additiveExpression>
   PROD_VARIABLE_DOLLAR_IDENTIFIER               = 39, // <variable> ::= '$' IDENTIFIER
   PROD_DECODERADDRESS                           = 40, // <decoderAddress> ::= <constant>
   PROD_DECODERADDRESS2                          = 41, // <decoderAddress> ::= <variable>
   PROD_DECODERADDRESS3                          = 42, // <decoderAddress> ::= 
   PROD_CMDMNEMONIC_IDENTIFIER                   = 43, // <cmdMnemonic> ::= IDENTIFIER
   PROD_KEYWORDVALUE_IDENTIFIER                  = 44, // <keywordValue> ::= IDENTIFIER <expression>
   PROD_KEYWORDVALUE_IDENTIFIER_IDENTIFIER       = 45, // <keywordValue> ::= IDENTIFIER IDENTIFIER
   PROD_KEYWORDVALUE_IDENTIFIER2                 = 46, // <keywordValue> ::= IDENTIFIER <variable>
   PROD_KEYWORDVALUE_IDENTIFIER3                 = 47, // <keywordValue> ::= IDENTIFIER <toggle>
   PROD_KEYWORDVALUE_IDENTIFIER_EQ               = 48, // <keywordValue> ::= IDENTIFIER '=' <expression>
   PROD_KEYWORDVALUE_IDENTIFIER_EQ_IDENTIFIER    = 49, // <keywordValue> ::= IDENTIFIER '=' IDENTIFIER
   PROD_KEYWORDVALUE_IDENTIFIER_EQ2              = 50, // <keywordValue> ::= IDENTIFIER '=' <variable>
   PROD_KEYWORDVALUE_IDENTIFIER_EQ3              = 51, // <keywordValue> ::= IDENTIFIER '=' <toggle>
   PROD_KEYWORDVALUELIST                         = 52, // <keywordValueList> ::= <keywordValueList> <keywordValue>
   PROD_KEYWORDVALUELIST2                        = 53, // <keywordValueList> ::= <keywordValue>
   PROD_PRIMARYEXPR_LPAREN_RPAREN                = 54, // <primaryExpr> ::= '(' <expression> ')'
   PROD_PRIMARYEXPRLIST                          = 55, // <primaryExprList> ::= <primaryExprList> <primaryExpr>
   PROD_PRIMARYEXPRLIST2                         = 56, // <primaryExprList> ::= <primaryExpr>
   PROD_HEXWORDLIST_HEXCONSTANT                  = 57, // <hexwordList> ::= <hexwordList> HEXconstant
   PROD_HEXWORDLIST_HEXCONSTANT2                 = 58, // <hexwordList> ::= HEXconstant
   PROD_POSITIONAL                               = 59, // <positional> ::= <primaryExprList>
   PROD_POSITIONAL2                              = 60, // <positional> ::= <constant>
   PROD_DATAWORDARGS                             = 61, // <datawordArgs> ::= <positional> <keywordValueList>
   PROD_DATAWORDARGS2                            = 62, // <datawordArgs> ::= <positional>
   PROD_DATAWORDARGS3                            = 63, // <datawordArgs> ::= <keywordValueList>
   PROD_DATAWORDARGS_LOAD_WORDS_EQ               = 64, // <datawordArgs> ::= 'LOAD_WORDS' '=' <hexwordList>
   PROD_DATAWORDARGS_FILE_EQ                     = 65, // <datawordArgs> ::= FILE '=' <stringLiteral>
   PROD_DATAWORDARGS4                            = 66, // <datawordArgs> ::= 
   PROD_CMDOPTIONS_BACKSLASH_PV_EQ               = 67, // <cmdOptions> ::= '\' PV '=' <ptOption>
   PROD_CMDOPTIONS_BACKSLASH_CV_EQ               = 68, // <cmdOptions> ::= '\' CV '=' <toggle>
   PROD_CMDOPTIONS_BACKSLASH_TV_EQ               = 69, // <cmdOptions> ::= '\' TV '=' <ptOption>
   PROD_CMDOPTIONS_BACKSLASH_AUTO_EXEC_EQ        = 70, // <cmdOptions> ::= '\' 'AUTO_EXEC' '=' <toggle>
   PROD_CMDOPTIONS_BACKSLASH_IMM                 = 71, // <cmdOptions> ::= '\' IMM
   PROD_CMDOPTIONS_BACKSLASH_STORE_AND_EXECUTE   = 72, // <cmdOptions> ::= '\' 'STORE_AND_EXECUTE'
   PROD_CMDOPTIONS_BACKSLASH_DEL                 = 73, // <cmdOptions> ::= '\' DEL
   PROD_CMDOPTIONS_BACKSLASH_CLEAR               = 74, // <cmdOptions> ::= '\' CLEAR
   PROD_CMDOPTIONS_BACKSLASH_STORE_ONLY          = 75, // <cmdOptions> ::= '\' 'STORE_ONLY'
   PROD_CMDOPTIONS_BACKSLASH_EXEC_AND_STORE      = 76, // <cmdOptions> ::= '\' 'EXEC_AND_STORE'
   PROD_CMDOPTIONS_BACKSLASH_LIFO                = 77, // <cmdOptions> ::= '\' LIFO
   PROD_CMDOPTIONS_BACKSLASH_OVERRIDE            = 78, // <cmdOptions> ::= '\' OVERRIDE
   PROD_CMDOPTIONS_AT_LPAREN_TIMECONSTANT_RPAREN = 79, // <cmdOptions> ::= '@' '(' TIMEconstant ')'
   PROD_CMDOPTIONS                               = 80, // <cmdOptions> ::= 
   PROD_TOGGLE_ON                                = 81, // <toggle> ::= ON
   PROD_TOGGLE_OFF                               = 82, // <toggle> ::= OFF
   PROD_PTOPTION                                 = 83, // <ptOption> ::= <toggle>
   PROD_PTOPTION_WAIT                            = 84, // <ptOption> ::= WAIT
   PROD_PTOPTION_NOWAIT                          = 85  // <ptOption> ::= NOWAIT
};
