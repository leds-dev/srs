#include "stdafx.h"
#if !defined( RTCSFUNCTIONSSSCPP)
#define RTCSFUNCTIONSSSCPP
#include "RtcsConstants.h"
#include "RtcsClasses.h"

const char* RtcsLexClass::tokenToConstChar( SSUnsigned32 ulToken)
{
	const char* pchToken;
	switch ( ulToken)
	{
	case SYM_TIMECONSTANT: /*RtcsLexTokenTimeTag:*/
		pchToken = "TIMEconstant";
		break;

	case SYM_EOL: /*RtcsLexTokenEol:*/
		pchToken = "eol";
		break;

	case SYM_IDENTIFIER: /*RtcsLexTokenId:*/
		pchToken = "IDENTIFIER";
		break;

	case SYM_OCTALCONSTANT: /*RtcsLexTokenOct:*/
		pchToken = "OCTALconstant";
		break;

	case SYM_INTEGERCONSTANT: /*RtcsLexTokenDec:*/
		pchToken = "INTEGERconstant";
		break;

	case SYM_HEXCONSTANT: /*RtcsLexTokenHex:*/
		pchToken = "HEXconstant";
		break;

	case SYM_FLOATINGCONSTANT: /*RtcsLexTokenFloat0:*/
		pchToken = "FLOATINGconstant0";
		break;

	case SYM_FLOATINGCONSTANT2: /*RtcsLexTokenFloat1:*/
		pchToken = "FLOATINGconstant1";
		break;

	//case RtcsLexTokenFloat2:
	//	pchToken = "FLOATINGconstant2";
	//	break;

	//case RtcsLexTokenStringSt:
	//	pchToken = "STRINGstart";
	//	break;

	case SYM_EQ: /*RtcsLexTokenAssign:*/
		pchToken = "=";
		break;

	case SYM_LPAREN: /*RtcsLexTokenOParen:*/
		pchToken = "(";
		break;

	case SYM_RPAREN: /*RtcsLexTokenCParen:*/
		pchToken = ")";
		break;

	case SYM_AT: /*RtcsLexTokenAt:*/
		pchToken = "@";
		break;

	case SYM_BACKSLASH: /*RtcsLexTokenBackSlash:*/
		pchToken = "\\";
		break;

	case SYM__: /*RtcsLexTokenUnderScore:*/
		pchToken = "_";
		break;

	case SYM_SEMI: /*RtcsLexTokenSemiColon:*/
		pchToken = ";";
		break;

	case SYM_DOLLAR: /*RtcsLexTokenDollar:*/
		pchToken = "$";
		break;

	case SYM_PLUS: /*RtcsLexTokenPlus:*/
		pchToken = "+";
		break;

	case SYM_MINUS: /*RtcsLexTokenMinus:*/
		pchToken = "-";
		break;

	case SYM_TIMES: /*RtcsLexTokenStar:*/
		pchToken = "*";
		break;

	case SYM_DIV: /*RtcsLexTokenDiv:*/
		pchToken = "/";
		break;

	case SYM_TIMESTIMES: /*RtcsLexTokenStarStar:*/
		pchToken = "**";
		break;

	case SYM_ARGS: /*RtcsLexTokenArgs:*/
		pchToken = "ARGS";
		break;

	case SYM_AUTO_EXEC: /*RtcsLexTokenAutoExe:*/
		pchToken = "AUTO_EXEC";
		break;

	case SYM_CLEAR: /*RtcsLexTokenClear:*/
		pchToken = "CLEAR";
		break;

	case SYM_CMD: /*RtcsLexTokenCmd:*/
		pchToken = "CMD";
		break;

	case SYM_CV: /*RtcsLexTokenCv:*/
		pchToken = "CV";
		break;

	case SYM_DEL: /*RtcsLexTokenDel:*/
		pchToken = "DEL";
		break;

	case SYM_EXEC_AND_STORE: /*RtcsLexTokenExecAndStore:*/
		pchToken = "EXEC_AND_STORE";
		break;

	case SYM_FILE: /*RtcsLexTokenFile:*/
		pchToken = "FILE";
		break;

	case SYM_IMM: /*RtcsLexTokenImm:*/
		pchToken = "IMM";
		break;

	case SYM_LIFO: /*RtcsLexTokenLifo:*/
		pchToken = "LIFO";
		break;

	case SYM_LOAD_WORDS: /*RtcsLexTokenLoadWords:*/
		pchToken = "LOAD_WORDS";
		break;

	case SYM_NOWAIT: /*RtcsLexTokenNoWait:*/
		pchToken = "NOWAIT";
		break;

	case SYM_OFF: /*RtcsLexTokenOff:*/
		pchToken = "OFF";
		break;

	case SYM_ON: /*RtcsLexTokenOn:*/
		pchToken = "ON";
		break;

	case SYM_OVERRIDE: /*RtcsLexTokenOverride:*/
		pchToken = "OVERRIDE";
		break;

	case SYM_PV: /*RtcsLexTokenPv:*/
		pchToken = "PV";
		break;

	case SYM_RETURN: /*RtcsLexTokenReturn:*/
		pchToken = "RETURN";
		break;

	case SYM_STORE_AND_EXECUTE: /*RtcsLexTokenStoreAndExecute:*/
		pchToken = "STORE_AND_EXECUTE";
		break;

	case SYM_STORE_ONLY: /*RtcsLexTokenStoreOnly:*/
		pchToken = "STORE_ONLY";
		break;

	case SYM_TV: /*RtcsLexTokenTv:*/
		pchToken = "TV";
		break;

	case SYM_WAIT: /*RtcsLexTokenWait:*/
		pchToken = "WAIT";
		break;

	case SYM_LITERALSTRING: /*RtcsLexTokenString:*/
		pchToken = "STRINGliteral";
		break;

	//case RtcsLexTokenStringError:
	//	pchToken = "STRINGerror";
	//	break;

	//case RtcsLexTokenStringEnd:
	//	pchToken = "STRINGend";
	//	break;

	case SSYaccErrorToken:
		pchToken = "%error";
		break;

	case SSYaccEofToken:
		pchToken = "eof";
		break;

	default:
		pchToken = SSLexTokenNotFound;
	}
	return pchToken;
}

#endif
