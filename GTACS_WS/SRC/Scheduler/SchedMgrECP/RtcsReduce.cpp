/**
/////////////////////////////////////////////////////////////////////////////
// RtcsReduce.cpp : implementation of the RtcsYaccClass class.             //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// RtcsReduce.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is the main driver for the Rtcs parser class.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "stdafx.h"
#if !defined( RTCSREDUCESSCPP)
#define RTCSREDUCESSCPP
#include "SchedMgrECP.h"
#include "RtcsClasses.h"
#include "RtcsConstants.h"

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RtcsYaccClass::error
//	Description :	This routine handles all cls parsing errors.
//
//	Return :		SSBooleanValue	-	SSTrue if error count is less 
//										than 100. (continue to parse)
//	Parameters :
//		SSUnsigned32 qulState	-	state of the lookahead symbol.
//		SSLexLexeme& qLookahead	-	the token that caused the error.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
SSBooleanValue RtcsYaccClass::error(SSUnsigned32 qulState, SSLexLexeme& qLookahead)
{
	if((*m_pnErrorCnt) >= 100)
	{
		CString strMsg;

		strMsg.Format(IDS_SYNTAX_ERROR_COUNT, 100);
		GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
	
		return SSTrue;
	}

	m_nErrorNum = 0;
	parseError(qLookahead);
	return GoldWrapper::error(qulState, qLookahead);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RtcsYaccClass::parseError
//	Description :	This routine outputs an event message that 
//					contains the token that caused the error, the line
//					number, and the offset of where the error occurred.
//
//	Return :		void	-
//
//	Parameters :
//		SSLexLexeme& qLookahead	-	the token that caused the error.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RtcsYaccClass::parseError(SSLexLexeme& qLookahead)
{
	CString strMsg;
	CString	lexeme(qLookahead.asChar());
	int		line	= qLookahead.line();
	int		offset	= qLookahead.offset();

	(*m_pnErrorCnt)++;

	if((*m_pnErrorCnt) >= 100)
	{
		error(0, qLookahead);
		return;
	}

	if(offset > 0)
		offset = offset - 1;

	if(lexeme == _T("\n"))
	{
		m_nErrorNum	= IDS_SYNTAX_ERROR21;
		lexeme.Format(IDS_SYNTAX_ERROR21);
	}

	if(m_strErrorMessage.IsEmpty())
		m_strErrorMessage = _T("syntax error");

	if(m_nErrorNum == 0)
		m_nErrorNum	= (*m_pnErrorCnt);

	strMsg.Format(_T("%s(%d) : error %d: \'%s\': %s"), 
		m_strFilename, line, m_nErrorNum, lexeme, m_strErrorMessage);
	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL, line, offset);
	m_strErrorMessage.Empty();
	m_nErrorNum = 0;
}

/*
	You must keep this reduce function clean to avoid confusing
	the AutoMerge parser. By 'clean' we mean that if you are going
	to add a significant amount of code to a case, make a function
	call instead of adding the code directly to the case.

	NEVER add embedded switch statements inside this function.
*/
/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RtcsYaccClass::reduce
//	Description :	This routine is called each time  a grammer rule,
//					or production, is recognized.
//
//	Return :		SSYaccStackElement*	-	element that is associated
//											with the token.
//
//	Parameters :
//		SSUnsigned32 ulProd	-	production element number.
//		SSUnsigned32 ulSize	-	
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
SSYaccStackElement* RtcsYaccClass::reduce( SSUnsigned32 ulProd,
      SSUnsigned32 ulSize)
{
	//Uncomment out this line if you want to build a parse tree
	//The treeRoot() function will contain the tree root when finished
	//return addSubTree();

	CSchedMgrECPApp*	pApp = (CSchedMgrECPApp*)AfxGetApp();
	HRESULT				hr;

	switch ( ulProd)
	{

		case PROD_CMDSTATEMENT_CMD: /*RtcsYaccProdCommand:*/
		// cmdStatement -> CMD decoderAddress cmdMnemonic datawordArgs cmdOptions eol 
		{
			if( m_pEnumCmds != NULL )
			{
				m_pEnumCmds->Release();
				m_pEnumCmds = NULL;
			}

			break;
		}

		case PROD_VARIABLE_DOLLAR_IDENTIFIER: /*RtcsYaccProdVariable:*/
		// variable -> $ IDENTIFIER 
		{
			//database call for lexeme 1
			SSLexLexeme*	z_pLexeme = elementFromProduction(1)->lexeme();
			IEpEnumPoints*	pEnumPoints;
			CComBSTR		bstrMnemonic(z_pLexeme->asConstChar());

			hr = (pApp->m_pEpDB)->GetPointEnum(pApp->m_ulDBHandle, bstrMnemonic, &pEnumPoints);

			if( FAILED(hr) )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR3;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR3);
				parseError(*z_pLexeme);
			}

			if( pEnumPoints != NULL )
			{
				pEnumPoints->Release();
				pEnumPoints = NULL;
			}

			break;
		}

		case PROD_CMDMNEMONIC_IDENTIFIER: /*RtcsYaccProdCmdMnemonicId:*/
		// cmdMnemonic -> IDENTIFIER 
		{
			//database call for lexeme 0
			SSLexLexeme*		z_pLexeme = elementFromProduction(0)->lexeme();
			CComBSTR			bstrMnemonic(z_pLexeme->asConstChar());
			EpDbCmdDescription	cmdDesc;
			ULONG				ulFetched;

			hr = (pApp->m_pEpDB)->GetCommandEnum(pApp->m_ulDBHandle, bstrMnemonic, &m_pEnumCmds);

			if( FAILED(hr) )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR4;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR4);
				parseError(*z_pLexeme);
				break;
			}

			hr = m_pEnumCmds->Next(1, &cmdDesc, &ulFetched);

			if( ulFetched == 0 )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR9;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR9);
				parseError(*z_pLexeme);
				break;
			}

			SysFreeString(cmdDesc.bstrMnemonic);
			SysFreeString(cmdDesc.bstrDescription);
			SysFreeString(cmdDesc.bstrAlias);
			SysFreeString(cmdDesc.bstrExecMnemonic);
			SysFreeString(cmdDesc.bstrCmdFormat);
			SysFreeString(cmdDesc.bstrSpacecraftAddr);
			SysFreeString(cmdDesc.bstrPrivilegeGroup);

			break;
		}

		case PROD_KEYWORDVALUE_IDENTIFIER: /*RtcsYaccProdKeywordConst:*/
		// keywordValue -> IDENTIFIER expression 
		{
			SSLexLexeme*			z_pLexeme = elementFromProduction(0)->lexeme();
			CString					strToken(z_pLexeme->asConstChar());
			IEpEnumDatawordArgs*	pEnumDwArgs;
			ULONG					ulFetched;

			if( m_pEnumCmds == NULL ) break;

			hr = m_pEnumCmds->GetDatawordArgEnum(&pEnumDwArgs);

			if( SUCCEEDED(hr) )
			{
				EpDbCmdDatawordArgDescription cdwaDesc;
				bool bFound = false;

				while( !bFound )
				{
					pEnumDwArgs->Next(1, &cdwaDesc, &ulFetched);	
			
					if( ulFetched == 0 ) break;

					if( strToken == cdwaDesc.bstrKeyword )
						bFound = true;

					SysFreeString(cdwaDesc.bstrKeyword);
					SysFreeString(cdwaDesc.bstrDescription);
				}

				if( !bFound )
				{
					m_nErrorNum = IDS_SYNTAX_ERROR10;
					m_strErrorMessage.Format(IDS_SYNTAX_ERROR10);
					parseError(*z_pLexeme);
				}
			}
			else
			{
				CString	strMsg;
				LPVOID	lpMsgBuf;

				FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

				strMsg = (LPTSTR)lpMsgBuf;
				strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF

				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				LocalFree(lpMsgBuf);
			}

			if( pEnumDwArgs != NULL )
			{
				pEnumDwArgs->Release();
				pEnumDwArgs = NULL;
			}

			break;
		}

		case PROD_KEYWORDVALUE_IDENTIFIER_IDENTIFIER: /*RtcsYaccProdKeywordId:*/
		// keywordValue -> IDENTIFIER IDENTIFIER 
		{
			SSLexLexeme*			z_pLexeme = elementFromProduction(0)->lexeme();
			CString					strToken(z_pLexeme->asConstChar());
			IEpEnumDatawordArgs*	pEnumDwArgs;
			ULONG					ulFetched;

			if( m_pEnumCmds == NULL ) break;

			hr = m_pEnumCmds->GetDatawordArgEnum(&pEnumDwArgs);

			if( SUCCEEDED(hr) )
			{
				EpDbCmdDatawordArgDescription cdwaDesc;
				bool bFound = false;

				while( !bFound )
				{
					pEnumDwArgs->Next(1, &cdwaDesc, &ulFetched);	
			
					if(ulFetched == 0)  break;

					if(strToken == cdwaDesc.bstrKeyword)
						bFound = true;

					SysFreeString(cdwaDesc.bstrKeyword);
					SysFreeString(cdwaDesc.bstrDescription);
				}

				if( bFound )
				{
					z_pLexeme	= elementFromProduction(1)->lexeme();

					if( z_pLexeme == NULL ) break;

					strToken	= z_pLexeme->asConstChar();

					IEpEnumDatawordArgValues* pEnum;
					hr = pEnumDwArgs->GetDatawordArgValueEnum(&pEnum);

					if( SUCCEEDED(hr) )
					{
						EpDbCmdDatawordArgValueDescription cdwavDesc;
						bFound = false;

						while( !bFound )
						{
							pEnum->Next(1, &cdwavDesc, &ulFetched);

							if( ulFetched == 0 )  break;

							if( strToken == cdwavDesc.bstrMnemonic )
								bFound = true;

							SysFreeString(cdwavDesc.bstrMnemonic);
							SysFreeString(cdwavDesc.bstrDescription);
						}
				
						if( !bFound )
						{
							m_nErrorNum = IDS_SYNTAX_ERROR11;
							m_strErrorMessage.Format(IDS_SYNTAX_ERROR11);
							parseError(*z_pLexeme);
						}
					}
					else
					{
						CString	strMsg;
						LPVOID	lpMsgBuf;

						FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
							NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

						strMsg = (LPTSTR)lpMsgBuf;
						strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF

						GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
						LocalFree(lpMsgBuf);
					}
				}
				else
				{
					m_nErrorNum = IDS_SYNTAX_ERROR10;
					m_strErrorMessage.Format(IDS_SYNTAX_ERROR10);
					parseError(*z_pLexeme);
				}
			}
			else
			{
				CString	strMsg;
				LPVOID	lpMsgBuf;

				FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

				strMsg = (LPTSTR)lpMsgBuf;
				strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF

				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				LocalFree(lpMsgBuf);
			}

			if( pEnumDwArgs != NULL )
			{
				pEnumDwArgs->Release();
				pEnumDwArgs = NULL;
			}

			break;
		}

		case PROD_KEYWORDVALUE_IDENTIFIER2: /*RtcsYaccProdKeywordVar:*/
		// keywordValue -> IDENTIFIER variable 
		case PROD_KEYWORDVALUE_IDENTIFIER3: /*RtcsYaccProdKeywordOnOff:*/
		// keywordValue -> IDENTIFIER toggle 
		{
			SSLexLexeme*			z_pLexeme = elementFromProduction(0)->lexeme();
			CString					strToken(z_pLexeme->asConstChar());
			IEpEnumDatawordArgs*	pEnumDwArgs;
			ULONG					ulFetched;

			if( m_pEnumCmds == NULL ) break;

			hr = m_pEnumCmds->GetDatawordArgEnum(&pEnumDwArgs);

			if( SUCCEEDED(hr) )
			{
				EpDbCmdDatawordArgDescription cdwaDesc;
				bool bFound = false;

				while( !bFound )
				{
					pEnumDwArgs->Next(1, &cdwaDesc, &ulFetched);	
			
					if( ulFetched == 0 )  break;

					if( strToken == cdwaDesc.bstrKeyword )
						bFound = true;

					SysFreeString(cdwaDesc.bstrKeyword);
					SysFreeString(cdwaDesc.bstrDescription);
				}

				if( !bFound )
				{
					m_nErrorNum = IDS_SYNTAX_ERROR11;
					m_strErrorMessage.Format(IDS_SYNTAX_ERROR11);
					parseError(*z_pLexeme);
					break;
				}
			}
			else
			{
				CString	strMsg;
				LPVOID	lpMsgBuf;

				FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

				strMsg = (LPTSTR)lpMsgBuf;
				strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF

				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				LocalFree(lpMsgBuf);
			}

			break;
		}

		case PROD_KEYWORDVALUE_IDENTIFIER_EQ: /*RtcsYaccProdKeywordEqConst:*/
		// keywordValue -> IDENTIFIER = expression 
		{
			SSLexLexeme*			z_pLexeme = elementFromProduction(0)->lexeme();
			CString					strToken(z_pLexeme->asConstChar());
			IEpEnumDatawordArgs*	pEnumDwArgs;
			ULONG					ulFetched;

			if( m_pEnumCmds == NULL ) break;

			hr = m_pEnumCmds->GetDatawordArgEnum(&pEnumDwArgs);

			if( SUCCEEDED(hr) )
			{
				EpDbCmdDatawordArgDescription cdwaDesc;
				bool bFound = false;

				while( !bFound )
				{
					pEnumDwArgs->Next(1, &cdwaDesc, &ulFetched);	
			
					if( ulFetched == 0 )  break;

					if( strToken == cdwaDesc.bstrKeyword )
						bFound = true;

					SysFreeString(cdwaDesc.bstrKeyword);
					SysFreeString(cdwaDesc.bstrDescription);
				}

				if( !bFound )
				{
					m_nErrorNum = IDS_SYNTAX_ERROR11;
					m_strErrorMessage.Format(IDS_SYNTAX_ERROR11);
					parseError(*z_pLexeme);
				}
			}
			else
			{
				CString	strMsg;
				LPVOID	lpMsgBuf;

				FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

				strMsg = (LPTSTR)lpMsgBuf;
				strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF

				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				LocalFree(lpMsgBuf);
			}

			if( pEnumDwArgs != NULL )
			{
				pEnumDwArgs->Release();
				pEnumDwArgs = NULL;
			}

			break;
		}

		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_IDENTIFIER: /*RtcsYaccProdKeywordEqId:*/
		// keywordValue -> IDENTIFIER = IDENTIFIER 
		{
			SSLexLexeme*			z_pLexeme = elementFromProduction(0)->lexeme();
			CString					strToken(z_pLexeme->asConstChar());
			IEpEnumDatawordArgs*	pEnumDwArgs;
			ULONG					ulFetched;

			if( m_pEnumCmds == NULL ) break;

			hr = m_pEnumCmds->GetDatawordArgEnum(&pEnumDwArgs);

			if( SUCCEEDED(hr) )
			{
				EpDbCmdDatawordArgDescription cdwaDesc;
				bool bFound = false;

				while( !bFound )
				{
					pEnumDwArgs->Next(1, &cdwaDesc, &ulFetched);	
			
					if( ulFetched == 0 )  break;

					if( strToken == cdwaDesc.bstrKeyword )
						bFound = true;

					SysFreeString(cdwaDesc.bstrKeyword);
					SysFreeString(cdwaDesc.bstrDescription);
				}

				if( bFound )
				{
					z_pLexeme	= elementFromProduction(2)->lexeme();

					if( z_pLexeme == NULL ) break;

					strToken	= z_pLexeme->asConstChar();

					IEpEnumDatawordArgValues* pEnum;
					hr = pEnumDwArgs->GetDatawordArgValueEnum(&pEnum);

					if( SUCCEEDED(hr) )
					{
						EpDbCmdDatawordArgValueDescription cdwavDesc;
						bFound = false;

						while( !bFound )
						{
							pEnum->Next(1, &cdwavDesc, &ulFetched);

							if( ulFetched == 0 )  break;

							if( strToken == cdwavDesc.bstrMnemonic )
								bFound = true;

							SysFreeString(cdwavDesc.bstrMnemonic);
							SysFreeString(cdwavDesc.bstrDescription);
						}
				
						if( !bFound )
						{
							m_nErrorNum = IDS_SYNTAX_ERROR11;
							m_strErrorMessage.Format(IDS_SYNTAX_ERROR11);
							parseError(*z_pLexeme);
						}
					}
					else
					{
						CString	strMsg;
						LPVOID	lpMsgBuf;

						FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
							NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

						strMsg = (LPTSTR)lpMsgBuf;
						strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF

						GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
						LocalFree(lpMsgBuf);
					}
				}
				else
				{
					m_nErrorNum = IDS_SYNTAX_ERROR10;
					m_strErrorMessage.Format(IDS_SYNTAX_ERROR10);
					parseError(*z_pLexeme);
				}
			}
			else
			{
				CString	strMsg;
				LPVOID	lpMsgBuf;

				FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

				strMsg = (LPTSTR)lpMsgBuf;
				strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF

				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				LocalFree(lpMsgBuf);
			}

			if( pEnumDwArgs != NULL )
			{
				pEnumDwArgs->Release();
				pEnumDwArgs = NULL;
			}

			break;
		}

		case PROD_KEYWORDVALUE_IDENTIFIER_EQ2: /*RtcsYaccProdKeywordEqVar:*/
		// keywordValue -> IDENTIFIER = variable 
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ3: /*RtcsYaccProdKeywordIdOnOff:*/
		// keywordValue -> IDENTIFIER = toggle 
		{
			SSLexLexeme*			z_pLexeme = elementFromProduction(0)->lexeme();
			CString					strToken(z_pLexeme->asConstChar());
			IEpEnumDatawordArgs*	pEnumDwArgs;
			ULONG					ulFetched;

			if( m_pEnumCmds == NULL ) break;

			hr = m_pEnumCmds->GetDatawordArgEnum(&pEnumDwArgs);

			if( SUCCEEDED(hr) )
			{
				EpDbCmdDatawordArgDescription cdwaDesc;
				bool bFound = false;

				while( !bFound )
				{
					pEnumDwArgs->Next(1, &cdwaDesc, &ulFetched);	
			
					if( ulFetched == 0 )  break;

					if( strToken == cdwaDesc.bstrKeyword )
						bFound = true;

					SysFreeString(cdwaDesc.bstrKeyword);
					SysFreeString(cdwaDesc.bstrDescription);
				}

				if( !bFound )
				{
					m_nErrorNum = IDS_SYNTAX_ERROR11;
					m_strErrorMessage.Format(IDS_SYNTAX_ERROR11);
					parseError(*z_pLexeme);
					break;
				}
			}
			else
			{
				CString	strMsg;
				LPVOID	lpMsgBuf;

				FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);

				strMsg = (LPTSTR)lpMsgBuf;
				strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF

//				GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
//				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				LocalFree(lpMsgBuf);
			}

			if( pEnumDwArgs != NULL )
			{
				pEnumDwArgs->Release();
				pEnumDwArgs = NULL;
			}

			break;
		}

		default:/*Reduce*/
			break;
	}

	return stackElement();
}

#endif











