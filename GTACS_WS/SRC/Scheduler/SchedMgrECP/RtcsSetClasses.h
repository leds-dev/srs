#if !defined( RTCSSETCLASSESSSHPP)
#define RTCSSETCLASSESSSHPP
#include "sslex.hpp"
#include "ssyacc.hpp"
#include "GoldWrapper.h"

class RtcsSetLexClass : public SSLex
{
public:
	SSConstr RtcsSetLexClass( const char*);
	SSConstr RtcsSetLexClass( const char*, const char*);
	SSConstr RtcsSetLexClass( SSLexConsumer&, SSLexTable&);

	const char* tokenToConstChar( SSUnsigned32);
};

SSInline RtcsSetLexClass::RtcsSetLexClass( const char* pszFile) : 
	SSLex( pszFile, ".\\RtcsSet.dfa")
{
}

SSInline RtcsSetLexClass::RtcsSetLexClass( const char* pszFile, const char* pszTable) : 
	SSLex( pszFile, pszTable)
{
}

SSInline RtcsSetLexClass::RtcsSetLexClass( SSLexConsumer& Consumer, SSLexTable& Table) : 
	SSLex( Consumer, Table)
{
}

class RtcsSetYaccClass : public GoldWrapper
{
public:
	SSConstr RtcsSetYaccClass( const char*);

	SSYaccStackElement* reduce( SSUnsigned32, SSUnsigned32);

	virtual SSDestr RtcsSetYaccClass( void);

	virtual SSBooleanValue error( SSUnsigned32, SSLexLexeme&);
	void parseError(SSLexLexeme& qLookahead);
	void filename(CString filename) { m_strFilename = filename; }
	void errorCount(int* nErrors)	{ m_pnErrorCnt = nErrors; }

	CString	m_strErrorMessage;
	CString	m_strFilename;
	int*	m_pnErrorCnt;
	int		m_nErrorNum;

protected:
};

SSInline RtcsSetYaccClass::RtcsSetYaccClass( const char* pszFile) : 
	GoldWrapper(pszFile)
{
	m_pnErrorCnt= NULL;
	m_nErrorNum	= 0;
}

SSInline RtcsSetYaccClass::~RtcsSetYaccClass( void)
{
}

#endif
