﻿enum SymbolConstants
{
   SYM_EOF             =  0, // (EOF)
   SYM_ERROR           =  1, // (Error)
   SYM_COMMENTS        =  2, // Comments
   SYM_WHITESPACE      =  3, // Whitespace
   SYM_PERCENTERROR    =  4, // '%error'
   SYM_EQ              =  5, // '='
   SYM_HEXCONSTANT     =  6, // HEXconstant
   SYM_IDENTIFIER      =  7, // IDENTIFIER
   SYM_INCLUDE         =  8, // INCLUDE
   SYM_INTEGERCONSTANT =  9, // INTEGERconstant
   SYM_LABEL           = 10, // LABEL
   SYM_NEWLINE         = 11, // NewLine
   SYM_OCTALCONSTANT   = 12, // OCTALconstant
   SYM_START           = 13, // START
   SYM_CONSTANT        = 14, // <constant>
   SYM_EOL             = 15, // <eol>
   SYM_NL              = 16, // <nl>
   SYM_RTCSSTATEMENT   = 17, // <rtcsStatement>
   SYM_START2          = 18, // <start>
   SYM_STATEMENT       = 19, // <statement>
   SYM_STATEMENTLIST   = 20  // <statementList>
};

enum ProductionConstants
{
   PROD_EOL                                                           =  0, // <eol> ::= <nl>
   PROD_NL_NEWLINE                                                    =  1, // <nl> ::= NewLine
   PROD_START                                                         =  2, // <start> ::= <statementList>
   PROD_STATEMENTLIST                                                 =  3, // <statementList> ::= <statementList> <statement>
   PROD_STATEMENTLIST2                                                =  4, // <statementList> ::= <statement>
   PROD_STATEMENT                                                     =  5, // <statement> ::= <rtcsStatement>
   PROD_STATEMENT_PERCENTERROR                                        =  6, // <statement> ::= '%error' <eol>
   PROD_STATEMENT2                                                    =  7, // <statement> ::= <eol>
   PROD_RTCSSTATEMENT_INCLUDE_IDENTIFIER_LABEL_EQ_IDENTIFIER_START_EQ =  8, // <rtcsStatement> ::= INCLUDE IDENTIFIER LABEL '=' IDENTIFIER START '=' <constant> <eol>
   PROD_RTCSSTATEMENT_INCLUDE_IDENTIFIER_LABEL_EQ_IDENTIFIER          =  9, // <rtcsStatement> ::= INCLUDE IDENTIFIER LABEL '=' IDENTIFIER <eol>
   PROD_CONSTANT_HEXCONSTANT                                          = 10, // <constant> ::= HEXconstant
   PROD_CONSTANT_OCTALCONSTANT                                        = 11, // <constant> ::= OCTALconstant
   PROD_CONSTANT_INTEGERCONSTANT                                      = 12  // <constant> ::= INTEGERconstant
};
