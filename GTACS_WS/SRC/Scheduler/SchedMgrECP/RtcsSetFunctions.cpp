#include "stdafx.h"
#if !defined( RTCSSETFUNCTIONSSSCPP)
#define RTCSSETFUNCTIONSSSCPP
#include "RtcsSetConstants.h"
#include "RtcsSetClasses.h"

const char* RtcsSetLexClass::tokenToConstChar( SSUnsigned32 ulToken)
{
	const char* pchToken;
	switch ( ulToken)
	{
	case SYM_EOL:
		pchToken = "eol";
		break;

	case SYM_IDENTIFIER:
		pchToken = "IDENTIFIER";
		break;

	case SYM_OCTALCONSTANT:
		pchToken = "OCTALconstant";
		break;

	case SYM_INTEGERCONSTANT:
		pchToken = "INTEGERconstant";
		break;

	case SYM_HEXCONSTANT:
		pchToken = "HEXconstant";
		break;

	case SYM_EQ:
		pchToken = "=";
		break;

//	case RtcsSetLexTokenQuote:
//		pchToken = "\"";
//		break;

//	case RtcsSetLexTokenOParen:
//		pchToken = "(";
//		break;

//	case RtcsSetLexTokenCParen:
//		pchToken = ")";
//		break;

//	case RtcsSetLexTokenComma:
//		pchToken = ",";
//		break;

//	case RtcsSetLexTokenDollar:
//		pchToken = "$";
//		break;

//	case RtcsSetLexTokenForwardSlash:
//		pchToken = "/";
//		break;

//	case RtcsSetLexTokenBackSlash:
//		pchToken = "\\";
//		break;

//	case RtcsSetLexTokenDot:
//		pchToken = ".";
//		break;

	case SYM_INCLUDE:
		pchToken = "INCLUDE";
		break;

	case SYM_LABEL:
		pchToken = "LABEL";
		break;

	case SYM_START:
		pchToken = "START";
		break;

	case SSYaccErrorToken:
		pchToken = "%error";
		break;

	case SSYaccEofToken:
		pchToken = "eof";
		break;

	default:
		pchToken = SSLexTokenNotFound;
	}
	return pchToken;
}

#endif
