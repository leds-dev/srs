/**
/////////////////////////////////////////////////////////////////////////////
// RtcsSetReduce.cpp : implementation of the RtcsSetYaccClass class.       //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// RtcsSetReduce.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is the main driver for the RtcsSet parser class.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "stdafx.h"
#if !defined( RTCSSETREDUCESSCPP)
#define RTCSSETREDUCESSCPP
#include "SchedMgrECP.h"
#include "RtcsSetClasses.h"
#include "RtcsSetConstants.h"

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RtcsSetYaccClass::error
//	Description :	This routine handles all RtcsSet parsing errors.
//
//	Return :		SSBooleanValue	-	SSTrue if error count is less 
//										than 100. (continue to parse)
//	Parameters :
//		SSUnsigned32 qulState	-	state of the lookahead symbol.
//		SSLexLexeme& qLookahead	-	the token that caused the error.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
SSBooleanValue RtcsSetYaccClass::error(SSUnsigned32 qulState, SSLexLexeme& qLookahead)
{
	if((*m_pnErrorCnt) >= 100)
	{
		CString strMsg;

		strMsg.Format(IDS_SYNTAX_ERROR_COUNT, 100);
		GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
	
		return SSTrue;
	}

	m_nErrorNum = 0;
	parseError(qLookahead);
	return GoldWrapper::error(qulState, qLookahead);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RtcsSetYaccClass::parseError
//	Description :	This routine outputs an event message that 
//					contains the token that caused the error, the line
//					number, and the offset of where the error occurred.
//
//	Return :		void	-
//
//	Parameters :
//		SSLexLexeme& qLookahead	-	the token that caused the error.
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RtcsSetYaccClass::parseError(SSLexLexeme& qLookahead)
{
	CString strMsg;
	CString	lexeme(qLookahead.asChar());
	int		line	= qLookahead.line();
	int		offset	= qLookahead.offset();

	(*m_pnErrorCnt)++;

	if((*m_pnErrorCnt) >= 100)
	{
		error(0, qLookahead);
		return;
	}

	if(offset > 0)
		offset = offset - 1;

	if(lexeme == _T("\n"))
	{
		m_nErrorNum	= IDS_SYNTAX_ERROR21;
		lexeme.Format(IDS_SYNTAX_ERROR21);
	}

	if(m_strErrorMessage.IsEmpty())
		m_strErrorMessage = _T("syntax error");

	if(m_nErrorNum == 0)
		m_nErrorNum	= (*m_pnErrorCnt);

	strMsg.Format(_T("%s(%d) : error %d: \'%s\': %s"), 
		m_strFilename, line, m_nErrorNum, lexeme, m_strErrorMessage);
	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL, line, offset);
	m_strErrorMessage.Empty();
	m_nErrorNum = 0;
}

/*
	You must keep this reduce function clean to avoid confusing
	the AutoMerge parser. By 'clean' we mean that if you are going
	to add a significant amount of code to a case, make a function
	call instead of adding the code directly to the case.

	NEVER add embedded switch statements inside this function.
*/
/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RtcsSetYaccClass::reduce
//	Description :	This routine is called each time  a grammer rule,
//					or production, is recognized.
//
//	Return :		SSYaccStackElement*	-	element that is associated
//											with the token.
//
//	Parameters :
//		SSUnsigned32 ulProd	-	production element number.
//		SSUnsigned32 ulSize	-	
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
SSYaccStackElement* RtcsSetYaccClass::reduce( SSUnsigned32 ulProd,
      SSUnsigned32 ulSize)
{

	//Uncomment out this line if you want to build a parse tree
	//The treeRoot() function will contain the tree root when finished
	//return addSubTree();

//	switch ( ulProd)
//	{
//		default:/*Reduce*/
//			break;
//	}

	return stackElement();
}

#endif





