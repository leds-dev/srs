#if !defined( RULESCLASSESSSHPP)
#define RULESCLASSESSSHPP
#include "sslex.hpp"
#include "ssyacc.hpp"
#include "SearchConstants.h"
#include "TextFile.h"
#include "GoldWrapper.h"

class CLexObject : public CObject
{
public:
	CLexObject()					{ m_bPaired = false;   }
	~CLexObject()					{}

	void	Line(int nLine)			{ m_nLine	= nLine;   }
	void	Offset(int nOffset)		{ m_nOffset = nOffset; }
	void	Paired(bool bPaired)	{ m_bPaired = bPaired; }
	void	Token(CString sTok)		{ m_strToken= sTok;    }
	void	Directive(CString sDir)	{ m_strDir	= sDir;    }
	int		Line()					{ return m_nLine;      }
	int		Offset()				{ return m_nOffset;    }
	bool	Paired()				{ return m_bPaired;    }
	CString Token()					{ return m_strToken;   }
	CString	Directive()				{ return m_strDir;     }

private:
	int		m_nLine;
	int		m_nOffset;
	bool	m_bPaired;
	CString	m_strToken;
	CString m_strDir;
};

class RulesLexClass : public SSLex
{
public:
	SSConstr RulesLexClass( const char*);
	SSConstr RulesLexClass( const char*, const char*);
	SSConstr RulesLexClass( SSLexConsumer&, SSLexTable&);

	const char* tokenToConstChar( SSUnsigned32);
};

SSInline RulesLexClass::RulesLexClass( const char* pszFile) :
	SSLex( pszFile, ".\\Rules.dfa")
{
}

SSInline RulesLexClass::RulesLexClass( const char* pszFile, const char* pszTable) :
	SSLex( pszFile, pszTable)
{
}

SSInline RulesLexClass::RulesLexClass( SSLexConsumer& Consumer, SSLexTable& Table) :
	SSLex( Consumer, Table)
{
}

class RulesYaccClass : public GoldWrapper
{
public:
	typedef enum eOptType {eSCAN, eSCANDATA, eSTAR, eSTARDATA, eSTARINFO};

	SSConstr RulesYaccClass( const char*);

	SSYaccStackElement* reduce( SSUnsigned32, SSUnsigned32);

	virtual SSDestr RulesYaccClass( void);

virtual SSBooleanValue error( SSUnsigned32, SSLexLexeme&);
	void parseError(SSLexLexeme& qLookahead);
	void filename(CString filename)			{ m_strFilename = filename; }
	void errorCount(int* nErrors)			{ m_pnErrorCnt	= nErrors;  }
	void CheckGotoLabel();

private:
	void CheckVarType(CString);
	void GotoLabel(int, CString);
	void CheckCmdMnemonic(int, BOOL);
	void CheckKeywordValue(int, int);
	void CheckCfgTlm(int, int, BOOL);
	void LoadIdListElements(int);
	void CheckArgExpLst(int);
	void FindHostName(int);
	void TravIdList(int);
	void CheckCmdStmt(int);
	void CheckCommaExp(int);
	void CheckDatawordArgs(int);
	void CheckAssignExp(int, int, int);
	void GetSearchPaths(CString);
	void CheckRange(int, int, int, CString);
	void Cleanup();
	void LoadTlmTypeCfgFile();
	void LoadCmdDestCfgFile();
	void LoadDbModTypes();
	void DeleteIdListElements();
	bool CheckDatawordArgValue(IEpEnumDatawordArgValues*, CString);

	BOOL FindStream(int, BOOL, CLexObject*);
	BOOL CheckRTCS(int,int);
	BOOL CheckVarDef(int);
	BOOL FindVariable(CString);
	BOOL FindTlmPoint(int,BOOL, UINT);
	BOOL CheckList(int, CStringList*, UINT, BOOL);
	BOOL OptionLookup(int, CMapStringToString*, UINT);
	BOOL OptionMapLookup(int, CMapStringToString*, eOptType);
	BOOL CheckToken(int,CString,CString,CString,CString,CString,CString,CString,CString);

	SSYaccStackElement* GetStackElement(int);
	SSYaccStackElement* AddVarToList(int, BOOL);

	IEpEnumCommands*	m_pEnumCmds;
	CMapStringToOb		m_mapTlmType;
	CStringArray		m_SearchPathArray;
	CString				m_strErrorMessage;
	CString				m_strFilename;
	int*				m_pnErrorCnt;
	int					m_nErrorNum;

	CMapStringToString	m_mapScanReqOptions;
	CMapStringToString	m_mapScandataReqOptions;
	CMapStringToString	m_mapStarReqOptions;
	CMapStringToString	m_mapStardataReqOptions;
	CMapStringToString	m_mapStarinfoReqOptions;
	CStringList			m_listScanOptOptions;
	CStringList			m_listScandataOptOptions;
	CStringList			m_listStarOptOptions;
	CStringList			m_listOptions;
	CStringList			m_listDatawords;
	CStringList			m_listLocalArg;
	CStringList			m_listGlobalArg;
	CStringList			m_listArgsParam;
	CStringList			m_listLabel;
	CStringList			m_listStream;
	CStringList			m_listTemp;
	CStringList			m_listDbModType;
	CStringList			m_listCmdDest;
	CObList				m_listObjectsIds;

	CMapStringToPtr		m_LexemeMap;

protected:

};

SSInline RulesYaccClass::RulesYaccClass( const char* pszFile) :
	GoldWrapper(pszFile)
{
		LoadTlmTypeCfgFile();
			LoadCmdDestCfgFile();
			LoadDbModTypes();

			m_mapScanReqOptions[ctstrFRAME] 				= _T("");
			m_mapScanReqOptions[ctstrPRIORITY] 				= _T("");
			m_mapScanReqOptions[ctstrMODE] 					= _T("");
			m_mapScanReqOptions[ctstrSIDE] 					= _T("");
			m_mapScanReqOptions[ctstrINSTRUMENT]			= _T("");

			m_mapScandataReqOptions[ctstrFRAME] 			= _T("");
			m_mapScandataReqOptions[ctstrPRIORITY] 			= _T("");
			m_mapScandataReqOptions[ctstrMODE] 				= _T("");
			m_mapScandataReqOptions[ctstrSIDE] 				= _T("");
			m_mapScandataReqOptions[ctstrINSTRUMENT]		= _T("");
			m_mapScandataReqOptions[ctstrREPEAT] 			= _T("");
			m_mapScandataReqOptions[ctstrOBJID] 			= _T("");
			m_mapScandataReqOptions[ctstrDUR] 				= _T("");
			m_mapScandataReqOptions[ctstrEXECUTE] 			= _T("");

			m_mapStarReqOptions[ctstrMAX] 					= _T("");
			m_mapStarReqOptions[ctstrDUR] 					= _T("");
			m_mapStarReqOptions[ctstrINSTRUMENT]			= _T("");

			m_mapStardataReqOptions[ctstrMAX] 				= _T("");
			m_mapStardataReqOptions[ctstrDUR] 				= _T("");
			m_mapStardataReqOptions[ctstrINSTRUMENT]		= _T("");
			m_mapStardataReqOptions[ctstrOBJID] 			= _T("");
			m_mapStardataReqOptions[ctstrLAST_STAR_CLASS]	= _T("");
			m_mapStardataReqOptions[ctstrLOAD_REG]			= _T("");

			m_mapStarinfoReqOptions[ctstrSTARID] 			= _T("");
			m_mapStarinfoReqOptions[ctstrTIME] 				= _T("");
			m_mapStarinfoReqOptions[ctstrINSTRUMENT]		= _T("");
			m_mapStarinfoReqOptions[ctstrOBJID] 			= _T("");
			m_mapStarinfoReqOptions[ctstrDWELL] 			= _T("");
			m_mapStarinfoReqOptions[ctstrNUMLOOKS]			= _T("");
			m_mapStarinfoReqOptions[ctstrLOOKNUM] 			= _T("");
			m_mapStarinfoReqOptions[ctstrSC]				= _T("");

			m_pEnumCmds										= NULL;
			m_pnErrorCnt									= NULL;
			m_nErrorNum										= 0;
}

SSInline RulesYaccClass::~RulesYaccClass( void)
{
	CheckGotoLabel();
	Cleanup();
}

#endif
