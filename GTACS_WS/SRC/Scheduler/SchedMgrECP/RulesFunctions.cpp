#include "stdafx.h"
#if !defined( RULESFUNCTIONSSSCPP)
#define RULESFUNCTIONSSSCPP
#include "RulesClasses.h"
#include "RulesConstants.h"

const char* RulesLexClass::tokenToConstChar( SSUnsigned32 ulToken)
{
	const char* pchToken;
	switch ( ulToken)
	{
	case SYM_TIMECONSTANT:
		pchToken = "TIMEconstant";
		break;

	case SYM_TIMESTRING:
		pchToken = "TIMEstring";
		break;

	case SYM_EOL:
		pchToken = "eol";
		break;

	case SYM_IDENTIFIER:
		pchToken = "IDENTIFIER";
		break;

	case SYM_FILENAME:
		pchToken = "FILENAME";
		break;

	//case SYM_CXTLMALPHANUM:
	//	pchToken = "ALPHANUMERIC";
	//	break;

	case SYM_OCTALCONSTANT:
		pchToken = "OCTALconstant";
		break;

	case SYM_INTEGERCONSTANT:
		pchToken = "INTEGERconstant";
		break;

	case SYM_HEXCONSTANT:
		pchToken = "HEXconstant";
		break;

	case SYM_CHARACTERCONSTANT:
		pchToken = "CHARACTERconstant";
		break;

	case SYM_CHARACTEROCTCONSTANT:
		pchToken = "CHARACTERoctConstant";
		break;

	case SYM_CHARACTERHEXCONSTANT:
		pchToken = "CHARACTERhexConstant";
		break;

	case SYM_FLOATINGCONSTANT:
		pchToken = "FLOATINGconstant0";
		break;

//	case RulesLexTokenFloat1:
//		pchToken = "FLOATINGconstant1";
//		break;

//	case RulesLexTokenFloat2:
//		pchToken = "FLOATINGconstant2";
//		break;

	case SYM_TRUECONSTANT:
		pchToken = "TRUEconstant";
		break;

	case SYM_FALSECONSTANT:
		pchToken = "FALSEconstant";
		break;

	case SYM_STRINGSTART:
		pchToken = "STRINGstart";
		break;

	case SYM_LPAREN:
		pchToken = "(";
		break;

	case SYM_RPAREN:
		pchToken = ")";
		break;

	case SYM_COMMA:
		pchToken = ",";
		break;

	case SYM_COLON:
		pchToken = ":";
		break;

	case SYM_SEMI:
		pchToken = ";";
		break;

	case SYM_EQ:
		pchToken = "=";
		break;

	case SYM_AT:
		pchToken = "@";
		break;

	case SYM__:
		pchToken = "_";
		break;

	case SYM_EXCLAM:
		pchToken = "!";
		break;

	//case RulesLexTokenOBrack:
	//	pchToken = "[";
	//	break;

	//case RulesLexTokenCBrack:
	//	pchToken = "]";
	//	break;

	//case RulesLexTokenOBrace:
	//	pchToken = "{";
	//	break;

	//case RulesLexTokenCBrace:
	//	pchToken = "}";
	//	break;

	case SYM_DOLLAR:
		pchToken = "$";
		break;

//	case SYM_BACKSLASH:
//		pchToken = "\\";
//		break;

	case SYM_DOT:
		pchToken = ".";
		break;

	//case RulesLexTokenSingleQuote:
	//	pchToken = "'";
	//	break;

	case SYM_PLUS:
		pchToken = "+";
		break;

	case SYM_MINUS:
		pchToken = "-";
		break;

	case SYM_TIMES:
		pchToken = "*";
		break;

	case SYM_DIV:
		pchToken = "/";
		break;

	case SYM_TIMESTIMES:
		pchToken = "**";
		break;

	case SYM_EQ2:
		pchToken = "EQ";
		break;

	case SYM_LT:
		pchToken = "LT";
		break;

	case SYM_GT:
		pchToken = "GT";
		break;

	case SYM_LE:
		pchToken = "LE";
		break;

	case SYM_GE:
		pchToken = "GE";
		break;

	case SYM_OR:
		pchToken = "OR";
		break;

	case SYM_NE:
		pchToken = "NE";
		break;

	case SYM_AND:
		pchToken = "AND";
		break;

	case SYM_NOT:
		pchToken = "NOT";
		break;

	case SYM_XOR:
		pchToken = "XOR";
		break;

	case SYM_PIPE:
		pchToken = "|";
		break;

	case SYM_AMP:
		pchToken = "&";
		break;

	case SYM_LS:
		pchToken = "LS";
		break;

	case SYM_RS:
		pchToken = "RS";
		break;

	case SYM_ABORT:
		pchToken = "ABORT";
		break;

	case SYM_ACK:
		pchToken = "ACK";
		break;

	case SYM_ACQUIRE:
		pchToken = "ACQUIRE";
		break;

	case SYM_ADS:
		pchToken = "ADS";
		break;

	case SYM_ARCH_MGR:
		pchToken = "ARCH_MGR";
		break;

	case SYM_AV:
		pchToken = "AV";
		break;

	case SYM_AVCMD:
		pchToken = "AVCMD";
		break;

	case SYM_AVCTRL_MON:
		pchToken = "AVCTRL_MON";
		break;

	case SYM_AVS:
		pchToken = "AVS";
		break;

	case SYM_AVTLM:
		pchToken = "AVTLM";
		break;

	case SYM_ACCESS:
		pchToken = "ACCESS";
		break;

	case SYM_ACTIVATE:
		pchToken = "ACTIVATE";
		break;

	case SYM_ADDRESS:
		pchToken = "ADDRESS";
		break;

	case SYM_ALARM:
		pchToken = "ALARM";
		break;

	case SYM_ALL:
		pchToken = "ALL";
		break;

	case SYM_ARCHIVE:
		pchToken = "ARCHIVE";
		break;

	case SYM_ASCII:
		pchToken = "ASCII";
		break;

	case SYM_CMDOPTIONAUTO_EXEC:
	pchToken = "AUTO_EXEC";
		break;

	//case RulesLexTokenAutoExec:
	//	pchToken = "AUTOEXEC";
	//	break;

	case SYM_BBCAL:
		pchToken = "BBCAL";
		break;

	case SYM_BIAS:
		pchToken = "BIAS";
		break;

	case SYM_BOTH:
		pchToken = "BOTH";
		break;

	case SYM_BYPASS:
		pchToken = "BYPASS";
		break;

	case SYM_BACKUP:
		pchToken = "BACKUP";
		break;

	case SYM_BELL:
		pchToken = "BELL";
		break;

	case SYM_CAL:
		pchToken = "CAL";
		break;

	case SYM_CALIBRATE:
		pchToken = "CALIBRATE";
		break;

	case SYM_CARRIER:
		pchToken = "CARRIER";
		break;

	case SYM_CEM:
		pchToken = "CEM";
		break;

	case SYM_CEMMON:
		pchToken = "CEMMON";
		break;

	case SYM_CFG:
		pchToken = "CFG";
		break;

	case SYM_CFGCMD:
		pchToken = "CFGCMD";
		break;

	case SYM_CGI:
		pchToken = "CGI";
		break;

	case SYM_CHECK:
		pchToken = "CHECK";
		break;

	case SYM_CHKPT:
		pchToken = "CHKPT";
		break;

	case SYM_CLK:
		pchToken = "CLK";
		break;

	case SYM_CLOAD:
		pchToken = "CLOAD";
		break;

	case SYM_CMDDEST:
		pchToken = "CMDDEST";
		break;

	case SYM_CMDSRC:
		pchToken = "CMDSRC";
		break;

	case SYM_CMDWARN:
		pchToken = "CMDWARN";
		break;

	case SYM_COLLECT:
		pchToken = "COLLECT";
		break;

	case SYM_COMP:
		pchToken = "COMP";
		break;

	case SYM_COMPARE:
		pchToken = "COMPARE";
		break;

	case SYM_CONFIG:
		pchToken = "CONFIG";
		break;

	case SYM_COPY:
		pchToken = "COPY";
		break;

	case SYM_CORT:
		pchToken = "CORT";
		break;

	case SYM_CXCTRL:
		pchToken = "CXCTRL";
		break;

	case SYM_CXMON:
		pchToken = "CXMON";
		break;

	case SYM_CXRNG:
		pchToken = "CXRNG";
		break;

	case SYM_CXTC:
		pchToken = "CXTC";
		break;

	case SYM_CXTLMALPHANUM:
		pchToken = "CXTLM";
		break;

	case SYM_CXTMS:
		pchToken = "CXTMS";
		break;

	case SYM_CYC:
		pchToken = "CYC";
		break;

	case SYM_CANCEL:
		pchToken = "CANCEL";
		break;

	case SYM_CFGTLM:
		pchToken = "CFGTLM";
		break;

	case SYM_CLEAR:
		pchToken = "CLEAR";
		break;

	case SYM_CLEAN:
		pchToken = "CLEAN";
		break;

	case SYM_CLIM:
		pchToken = "CLIM";
		break;

	case SYM_CLOSE:
		pchToken = "CLOSE";
		break;

	case SYM_CMD:
		pchToken = "CMD";
		break;

	case SYM_COMMAND:
		pchToken = "COMMAND";
		break;

	case SYM_CONNECT:
		pchToken = "CONNECT";
		break;

	case SYM_CONTINUE:
		pchToken = "CONTINUE";
		break;

	case SYM_CV:
		pchToken = "CV";
		break;

	case SYM_DCR_GEN:
		pchToken = "DCR_GEN";
		break;

	case SYM_DEBUG:
		pchToken = "DEBUG";
		break;

	case SYM_DERTLM:
		pchToken = "DERTLM";
		break;

	case SYM_DESKTOP:
		pchToken = "DESKTOP";
		break;

	case SYM_DIST:
		pchToken = "DIST";
		break;

	case SYM_DUMP:
		pchToken = "DUMP";
		break;

	case SYM_DURATION:
		pchToken = "DURATION";
		break;

	case SYM_DWELL:
		pchToken = "DWELL";
		break;

	case SYM_DATA:
		pchToken = "DATA";
		break;

	case SYM_DBASE:
		pchToken = "DBASE";
		break;

	case SYM_DEL:
		pchToken = "DEL";
		break;

	case SYM_DELETE:
		pchToken = "DELETE";
		break;

	case SYM_DISCONNECT:
		pchToken = "DISCONNECT";
		break;

	case SYM_DISPLAY:
		pchToken = "DISPLAY";
		break;

	case SYM_DO:
		pchToken = "DO";
		break;

	case SYM_EARTH:
		pchToken = "EARTH";
		break;

	case SYM_ECHO:
		pchToken = "ECHO";
		break;

	case SYM_ENCRYPT:
		pchToken = "ENCRYPT";
		break;

	case SYM_EQUALITY:
		pchToken = "EQUALITY";
		break;

	case SYM_ERR_MSG:
		pchToken = "ERR_MSG";
		break;

	case SYM_EVENTS:
		pchToken = "EVENTS";
		break;

	case SYM_EXECUTE:
		pchToken = "EXECUTE";
		break;

	case SYM_ELSE:
		pchToken = "ELSE";
		break;

	case SYM_ELSEIF:
		pchToken = "ELSEIF";
		break;

	case SYM_EMODE:
		pchToken = "EMODE";
		break;

	case SYM_ENDDO:
		pchToken = "ENDDO";
		break;

	case SYM_ENDIF:
		pchToken = "ENDIF";
		break;

	//case RulesLexTokenEquipment:
	//	pchToken = "EQUIPMENT";
	//	break;

	case SYM_EVENT:
		pchToken = "EVENT";
		break;

	case SYM_EXEC_AND_STORE:
		pchToken = "EXEC_AND_STORE";
		break;

	case SYM_FIX:
		pchToken = "FIX";
		break;

	case SYM_FLIP:
		pchToken = "FLIP";
		break;

	case SYM_FROM:
		pchToken = "FROM";
		break;

	case SYM_FTP:
		pchToken = "FTP";
		break;

	case SYM_FKEY:
		pchToken = "FKEY";
		break;

	case SYM_FILE:
		pchToken = "FILE";
		break;

	case SYM_FRAME:
		pchToken = "FRAME";
		break;

	case SYM_FREEZE:
		pchToken = "FREEZE";
		break;

	case SYM_GENCOM:
		pchToken = "GENCOM";
		break;

	case SYM_GENERATE:
		pchToken = "GENERATE";
		break;

	case SYM_GENLOAD:
		pchToken = "GENLOAD";
		break;

	case SYM_GETSTAT:
		pchToken = "GETSTAT";
		break;

	case SYM_GO:
		pchToken = "GO";
		break;

	case SYM_GPRIME:
		pchToken = "GPRIME";
		break;

	case SYM_GROUP:
		pchToken = "GROUP";
		break;

	case SYM_GLOBAL:
		pchToken = "GLOBAL";
		break;

	case SYM_GOTO:
		pchToken = "GOTO";
		break;

	case SYM_GOTOTIME:
		pchToken = "GOTOTIME";
		break;

	case SYM_GRANT:
		pchToken = "GRANT";
		break;

	case SYM_GROUND:
		pchToken = "GROUND";
		break;

	case SYM_GV:
		pchToken = "GV";
		break;

	case SYM_HDR:
		pchToken = "HDR";
		break;

	case SYM_HK:
		pchToken = "HK";
		break;

	case SYM_HOST:
		pchToken = "HOST";
		break;

	case SYM_HELP:
		pchToken = "HELP";
		break;

	case SYM_HEXCMD:
		pchToken = "HEXCMD";
		break;

	case SYM_IDLING:
		pchToken = "IDLING";
		break;

	case SYM_IHOST:
		pchToken = "IHOST";
		break;

	case SYM_IMAGE:
		pchToken = "IMAGE";
		break;

	case SYM_IMCENB:
		pchToken = "IMCENB";
		break;

	case SYM_IMCREQ:
		pchToken = "IMCREQ";
		break;

	case SYM_IMCSTAT:
		pchToken = "IMCSTAT";
		break;

	case SYM_INLUT:
		pchToken = "INLUT";
		break;

	case SYM_INST:
		pchToken = "INST";
		break;

	//case RulesLexTokenInstance:
	//	pchToken = "INSTANCE";
	//	break;

	case SYM_INSTRUMENT:
		pchToken = "INSTRUMENT";
		break;

	case SYM_I_OFF:
		pchToken = "I_OFF";
		break;

	case SYM_I_ON:
		pchToken = "I_ON";
		break;

	case SYM_IF:
		pchToken = "IF";
		break;

	case SYM_IMM:
		pchToken = "IMM";
		break;

	case SYM_INIT:
		pchToken = "INIT";
		break;

	case SYM_INPUT:
		pchToken = "INPUT";
		break;

	case SYM_INSIDE:
		pchToken = "INSIDE";
		break;

	case SYM_KEY:
		pchToken = "KEY";
		break;

	case SYM_KEYOFFSET:
		pchToken = "KEYOFFSET";
		break;

	case SYM_KEYSELECT:
		pchToken = "KEYSELECT";
		break;

	case SYM_LAST_STAR_CLASS:
		pchToken = "LAST_STAR_CLASS";
		break;

	case SYM_LAYOUT:
		pchToken = "LAYOUT";
		break;

	case SYM_LEAP:
		pchToken = "LEAP";
		break;

	case SYM_LOAD_REG:
		pchToken = "LOAD_REG";
		break;

	case SYM_LOG:
		pchToken = "LOG";
		break;

	case SYM_LOGGING:
		pchToken = "LOGGING";
		break;

	case SYM_LOOKNUM:
		pchToken = "LOOKNUM";
		break;

	case SYM_LABEL:
		pchToken = "LABEL";
		break;

	case SYM_LANDSCAPE:
		pchToken = "LANDSCAPE";
		break;

	case  SYM_CMDOPTIONLIFO:
		pchToken = "LIFO";
		break;

	case SYM_LIMITS:
		pchToken = "LIMITS";
		break;

	case SYM_LOAD:
		pchToken = "LOAD";
		break;

	case SYM_LOAD_WORDS:
		pchToken = "LOAD_WORDS";
		break;

	case SYM_LOCAL:
		pchToken = "LOCAL";
		break;

	case SYM_LOCK:
		pchToken = "LOCK";
		break;

	case SYM_LOGOFF:
		pchToken = "LOGOFF";
		break;

	case SYM_LOOK:
		pchToken = "LOOK";
		break;

	case SYM_MANEUVER:
		pchToken = "MANEUVER";
		break;

	case SYM_MAP:
		pchToken = "MAP";
		break;

	case SYM_MAX:
		pchToken = "MAX";
		break;

	case SYM_MODE:
		pchToken = "MODE";
		break;

	case SYM_MOMENTUM:
		pchToken = "MOMENTUM";
		break;

	case SYM_MONITOR:
		pchToken = "MONITOR";
		break;

	case SYM_MRSS:
		pchToken = "MRSS";
		break;

	case SYM_MUX:
		pchToken = "MUX";
		break;

	case SYM_MONSCHED:
		pchToken = "MONSCHED";
		break;

	case SYM_MODIFY:
		pchToken = "MODIFY";
		break;

	case SYM_NEXTSCHED:
		pchToken = "NEXTSCHED";
		break;

	case SYM_NONE:
		pchToken = "NONE";
		break;

	case SYM_NOP:
		pchToken = "NOP";
		break;

	case SYM_NORM:
		pchToken = "NORM";
		break;

	case SYM_NUM:
		pchToken = "NUM";
		break;

	case SYM_NUMLOOKS:
		pchToken = "NUMLOOKS";
		break;

	case SYM_NOOP:
		pchToken = "NOOP";
		break;

	case SYM_NOWAIT:
		pchToken = "NOWAIT";
		break;

	case SYM_OATS:
		pchToken = "OATS";
		break;

	case SYM_OBJID:
		pchToken = "OBJID";
		break;

	case SYM_OBS:
		pchToken = "OBS";
		break;

	//case RulesLexTokenOffset:
	//	pchToken = "OFFSET";
	//	break;

	case SYM_OPER:
		pchToken = "OPER";
		break;

	case SYM_OFF:
		pchToken = "OFF";
		break;

	case SYM_ON:
		pchToken = "ON";
		break;

	case SYM_OPEN:
		pchToken = "OPEN";
		break;

	case SYM_OUTSIDE:
		pchToken = "OUTSIDE";
		break;

	case SYM_CMDOPTIONOVERRIDE:
		pchToken = "OVERRIDE";
		break;

	case SYM_PACING:
		pchToken = "PACING";
		break;

	case SYM_PDI:
		pchToken = "PDI";
		break;

	case SYM_PENDING:
		pchToken = "PENDING";
		break;

	case SYM_PKT:
		pchToken = "PKT";
		break;

	case SYM_PM:
		pchToken = "PM";
		break;

	case SYM_PSS:
		pchToken = "PSS";
		break;

	case SYM_PSTE:
		pchToken = "PSTE";
		break;

	case SYM_PAUSE:
		pchToken = "PAUSE";
		break;

	case SYM_PLAYBACK:
		pchToken = "PLAYBACK";
		break;

	case SYM_POINT:
		pchToken = "POINT";
		break;

	case SYM_POPDOWN:
		pchToken = "POPDOWN";
		break;

	case SYM_POPUP:
		pchToken = "POPUP";
		break;

	case SYM_PORTRAIT:
		pchToken = "PORTRAIT";
		break;

	case SYM_POSTSCRIPT:
		pchToken = "POSTSCRIPT";
		break;

	case SYM_PRINT:
		pchToken = "PRINT";
		break;

	case SYM_PROC:
		pchToken = "PROC";
		break;

	case SYM_PROCEDURE:
		pchToken = "PROCEDURE";
		break;

	case SYM_PROMPT:
		pchToken = "PROMPT";
		break;

	case SYM_PSEUDO:
		pchToken = "PSEUDO";
		break;

	//case RulesLexTokenPtv:
	//	pchToken = "PTV";
	//	break;

	case SYM_CMDOPTIONPV:
		pchToken = "PV";
		break;

	case SYM_RANGE:
		pchToken = "RANGE";
		break;

	case SYM_RATE:
		pchToken = "RATE";
		break;

	case SYM_RECOVER:
		pchToken = "RECOVER";
		break;

	case SYM_REMOTE:
		pchToken = "REMOTE";
		break;

	case SYM_REOPEN:
		pchToken = "REOPEN";
		break;

	case SYM_REPORT:
		pchToken = "REPORT";
		break;

	case SYM_RESMON:
		pchToken = "RESMON";
		break;

	case SYM_REXMITS:
		pchToken = "REXMITS";
		break;

	case SYM_RPRIME:
		pchToken = "RPRIME";
		break;

	case SYM_RTCS:
		pchToken = "RTCS";
		break;

	case SYM_RTCSDATA:
		pchToken = "RTCSDATA";
		break;

	case SYM_REALTIME:
		pchToken = "REALTIME";
		break;

	case SYM_RECONNECT:
		pchToken = "RECONNECT";
		break;

	case SYM_RECORD:
		pchToken = "RECORD";
		break;

	case SYM_RELOAD:
		pchToken = "RELOAD";
		break;

	case SYM_RESET:
		pchToken = "RESET";
		break;

	case SYM_RESTORE:
		pchToken = "RESTORE";
		break;

	case SYM_RETURN:
		pchToken = "RETURN";
		break;

	case SYM_SCAN:
		pchToken = "SCAN";
		break;

	case SYM_SCANDATA:
		pchToken = "SCANDATA";
		break;

	case SYM_SCHEDX:
		pchToken = "SCHEDX";
		break;

	case SYM_SCID:
		pchToken = "SCID";
		break;

	case SYM_SEGSIZE:
		pchToken = "SEGSIZE";
		break;

	case SYM_SELF:
		pchToken = "SELF";
		break;

	case SYM_SENSOR:
		pchToken = "SENSOR";
		break;

	case SYM_SHADOW:
		pchToken = "SHADOW";
		break;

	case SYM_SIDE:
		pchToken = "SIDE";
		break;

	case SYM_SNLUT:
		pchToken = "SNLUT";
		break;

	case SYM_S_OFF:
		pchToken = "S_OFF";
		break;

	case SYM_S_ON:
		pchToken = "S_ON";
		break;

	case SYM_SPS:
		pchToken = "SPS";
		break;

	case SYM_STAR:
		pchToken = "STAR";
		break;

	case SYM_STARDATA:
		pchToken = "STARDATA";
		break;

	case SYM_STARID:
		pchToken = "STARID";
		break;

	case SYM_STARINFO:
		pchToken = "STARINFO";
		break;

	case SYM_STATIONID:
		pchToken = "STATIONID";
		break;

	case SYM_SWEEP:
		pchToken = "SWEEP";
		break;

	case SYM_SWW:
		pchToken = "SWW";
		break;

	case SYM_SYNC:
		pchToken = "SYNC";
		break;

	case SYM_SAVE:
		pchToken = "SAVE";
		break;

	case SYM_SAVESETS:
		pchToken = "SAVESETS";
		break;

	case SYM_SCHEDULE:
		pchToken = "SCHEDULE";
		break;

	case SYM_SCMD:
		pchToken = "SCMD";
		break;

	case SYM_SCREEN:
		pchToken = "SCREEN";
		break;

	case SYM_SELECT:
		pchToken = "SELECT";
		break;

	case SYM_SEND:
		pchToken = "SEND";
		break;

	case SYM_SET:
		pchToken = "SET";
		break;

	case SYM_SETENV:
		pchToken = "SETENV";
		break;

	case SYM_SETUP:
		pchToken = "SETUP";
		break;

	case SYM_SIM:
		pchToken = "SIM";
		break;

	case SYM_SIMULATION:
		pchToken = "SIMULATION";
		break;

	case SYM_SLEEP:
		pchToken = "SLEEP";
		break;

	case SYM_SNAP:
		pchToken = "SNAP";
		break;

	case SYM_START:
		pchToken = "START";
		break;

	case SYM_STEP:
		pchToken = "STEP";
		break;

	case SYM_STOP:
		pchToken = "STOP";
		break;

	case SYM_STREAM:
		pchToken = "STREAM";
		break;

	case SYM_STORE_AND_EXECUTE:
		pchToken = "STORE_AND_EXECUTE";
		break;

	case SYM_STORE_ONLY:
		pchToken = "STORE_ONLY";
		break;

	case SYM_SWAP:
		pchToken = "SWAP";
		break;

	case SYM_SYSTEM:
		pchToken = "SYSTEM";
		break;

	case SYM_TAKECMD:
		pchToken = "TAKECMD";
		break;

	case SYM_TC:
		pchToken = "TC";
		break;

	case SYM_TCU:
		pchToken = "TCU";
		break;

	case SYM_TEST:
		pchToken = "TEST";
		break;

	case SYM_TEXT:
		pchToken = "TEXT";
		break;

	case SYM_TGI:
		pchToken = "TGI";
		break;

	case SYM_TIMEOUT:
		pchToken = "TIMEOUT";
		break;

	case SYM_TLMDEST:
		pchToken = "TLMDEST";
		break;

	case SYM_TLMSRC:
		pchToken = "TLMSRC";
		break;

	case SYM_TLMWAIT:
		pchToken = "TLMWAIT";
		break;

	case SYM_TYPE:
		pchToken = "TYPE";
		break;

	case SYM_TERM:
		pchToken = "TERM";
		break;

	case SYM_THEN:
		pchToken = "THEN";
		break;

	case SYM_TIMEDCMD:
		pchToken = "TIMEDCMD";
		break;

	case SYM_TLM:
		pchToken = "TLM";
		break;

	case SYM_TRIGGER:
		pchToken = "TRIGGER";
		break;

	case SYM_TV:
		pchToken = "TV";
		break;

	case SYM_UNAV:
		pchToken = "UNAV";
		break;

	case SYM_USC:
		pchToken = "USC";
		break;

	case SYM_UPCOM:
		pchToken = "UPCOM";
		break;

	case SYM_UNLOCK:
		pchToken = "UNLOCK";
		break;

	case SYM_USE_DEFAULT:
		pchToken = "USE_DEFAULT";
		break;

	case SYM_USE_RAW:
		pchToken = "USE_RAW";
		break;

	case SYM_VCC:
		pchToken = "VCC";
		break;

	case SYM_VCCFILL:
		pchToken = "VCCFILL";
		break;

	case SYM_VS:
		pchToken = "VS";
		break;

	case SYM_VALUE:
		pchToken = "VALUE";
		break;

	case SYM_VIEW:
		pchToken = "VIEW";
		break;

	case SYM_VIEWER:
		pchToken = "VIEWER";
		break;

	case SYM_WAIT:
		pchToken = "WAIT";
		break;

	case SYM_WAITON:
		pchToken = "WAITON";
		break;

	case SYM_WHILE:
		pchToken = "WHILE";
		break;

	case SYM_WINDOW:
		pchToken = "WINDOW";
		break;

	case SYM_WRITE:
		pchToken = "WRITE";
		break;

	case SYM_YAW:
		pchToken = "YAW";
		break;

	case SYM_ZERO:
		pchToken = "ZERO";
		break;

	case SYM_STRINGLITERAL:
		pchToken = "STRINGliteral";
		break;

	case SYM_STRINGERROR:
		pchToken = "STRINGerror";
		break;

	//case RulesLexTokenStringEnd:
	//	pchToken = "STRINGend";
	//	break;

	case SSYaccErrorToken:
		pchToken = "%error";
		break;

	case SSYaccEofToken:
		pchToken = "eof";
		break;

	default:
		pchToken = SSLexTokenNotFound;
	}
	return pchToken;
}

#endif