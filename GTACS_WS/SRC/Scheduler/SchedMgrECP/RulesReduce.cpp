/**
/////////////////////////////////////////////////////////////////////////////
// RulesReduce.cpp : implementation of the RulesYaccClass class.           //
// (c) 2001 Ray Mosley
//     2006 Fred Shaw
//
//  PDL Updated                                                            //
/////////////////////////////////////////////////////////////////////////////

// RulesReduce.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is the main driver for the Rules parser class.
//
/////////////////////////////////////////////////////////////////////////////
**/


#include "stdafx.h"
#if !defined( RULESREDUCESSCPP)
#define RULESREDUCESSCPP
#include "RegistryUsr.h"
#include "RegistryGbl.h"
#include "SchedMgrECP.h"
#include "RulesClasses.h"
#include "RulesConstants.h"

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::error
//	Description :	This routine handles all Rules parsing errors.
//
//	Return :		SSBooleanValue	-	SSTrue if error count is less
//										than 100. (continue to parse)
//	Parameters :
//		SSUnsigned32 qulState	-	state of the lookahead symbol.
//		SSLexLexeme& qLookahead	-	the token that caused the error.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
SSBooleanValue RulesYaccClass::error(SSUnsigned32 qulState, SSLexLexeme& qLookahead)
{
	if( (*m_pnErrorCnt) >= 100 )
	{
		CString strMsg;

		strMsg.Format(IDS_SYNTAX_ERROR_COUNT, 100);
		GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);

		return SSTrue;
	}

	m_nErrorNum = 0;
	parseError(qLookahead);
	return GoldWrapper::error(qulState, qLookahead);

}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::parseError
//	Description :	This routine outputs an event message that
//					contains the token that caused the error, the line
//					number, and the offset of where the error occurred.
//
//	Return :		void	-
//
//	Parameters :
//		SSLexLexeme& qLookahead	-	the token that caused the error.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::parseError(SSLexLexeme& qLookahead)
{
	CString strMsg;
	CString	lexeme(qLookahead.asChar());
	int		line	= qLookahead.line();
	int		offset	= qLookahead.offset();

	(*m_pnErrorCnt)++;

	if( (*m_pnErrorCnt) >= 100 )
	{
		error(0, qLookahead);
		return;
	}

	if( offset > 0 )
		offset = offset - 1;

	lexeme.TrimLeft();
	lexeme.TrimRight();

	if( lexeme == _T("\n") || lexeme.IsEmpty() )
	{
		m_nErrorNum	= IDS_SYNTAX_ERROR21;
		offset		= 0;
		lexeme.Format(IDS_SYNTAX_ERROR21);
	}

	if( m_strErrorMessage.IsEmpty() )
		m_strErrorMessage = _T("syntax error");

	if( m_nErrorNum == 0 )
		m_nErrorNum	= (*m_pnErrorCnt);

	strMsg.Format(_T("%s(%d) : error %d: \'%s\': %s"),
		m_strFilename, line, m_nErrorNum, lexeme, m_strErrorMessage);
	GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL, line, offset);
	m_strErrorMessage.Empty();
	m_nErrorNum = 0;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::Cleanup
//	Description :	This routine
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::Cleanup()
{
	if( m_pEnumCmds != NULL )
	{
		m_pEnumCmds->Release();
		m_pEnumCmds = NULL;
	}

	m_pnErrorCnt = NULL;

	CString			strKey;
	CStringList*	l	= NULL;
	POSITION		pos	= m_mapTlmType.GetStartPosition();

	while( pos != NULL )
	{
		m_mapTlmType.GetNextAssoc(pos, strKey, (CObject*&)l);
		m_mapTlmType.RemoveKey(strKey);
		l->RemoveAll();
		delete l;
	}

	m_mapTlmType.RemoveAll();
	m_mapScanReqOptions.RemoveAll();
	m_mapScandataReqOptions.RemoveAll();
	m_mapStarReqOptions.RemoveAll();
	m_mapStardataReqOptions.RemoveAll();
	m_mapStarinfoReqOptions.RemoveAll();
	m_listScanOptOptions.RemoveAll();
	m_listScandataOptOptions.RemoveAll();
	m_listStarOptOptions.RemoveAll();
	m_listOptions.RemoveAll();
	m_listDatawords.RemoveAll();
	m_listLocalArg.RemoveAll();
	m_listGlobalArg.RemoveAll();
	m_listArgsParam.RemoveAll();
	m_listLabel.RemoveAll();
	m_listTemp.RemoveAll();
	m_listDbModType.RemoveAll();
	m_listCmdDest.RemoveAll();
	DeleteIdListElements();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::GotoLabel
//	Description :	This routine
//
//	Return :		void	-
//
//	Parameters :
//		int nTok			-	token number.
//		CString strDir		-	directive.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::GotoLabel(int nTok, CString strDir)
{
	CLexObject*		lexObj		= NULL;
	SSLexLexeme*	z_pLexeme	= elementFromProduction(nTok)->lexeme();
	CString			strKey(z_pLexeme->asChar());

	if( m_LexemeMap.Lookup(strKey, (void*&)lexObj) )
	{
		lexObj->Paired(true);
		return;
	}

	CLexObject* Obj = new CLexObject;

	Obj->Line(z_pLexeme->line());
	Obj->Offset(z_pLexeme->offset());
	Obj->Token(strKey);
	Obj->Directive(strDir);

	m_LexemeMap.SetAt(strKey, Obj);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckGotoLabel
//	Description :	This routine
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckGotoLabel()
{
	USES_CONVERSION;
	CString		strKey;
	CLexObject*	lexObj	= NULL;
	POSITION	pos		= m_LexemeMap.GetStartPosition();

	while( pos != NULL )
	{
		m_LexemeMap.GetNextAssoc(pos, strKey, (void*&)lexObj);

		if( !lexObj->Paired() && !(lexObj->Directive()).IsEmpty() )
		{
			m_nErrorNum			= 0;
			m_strErrorMessage	= _T(" can not find ") + strKey;
			LPSTR lpszAscii		= T2A((LPTSTR)((LPCTSTR)lexObj->Directive()));

			SSLexLexeme	z_Lexeme;
			z_Lexeme.setLine((SSUnsigned32)lexObj->Line());
			z_Lexeme.setOffset((SSUnsigned32)lexObj->Offset());
			z_Lexeme.setBuffer(lpszAscii, strlen(lpszAscii));
			parseError(z_Lexeme);
		}

		m_LexemeMap.RemoveKey(strKey);
		delete lexObj;
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::DeleteIdListElements
//	Description :	This routine
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::DeleteIdListElements()
{
	POSITION pos = m_listObjectsIds.GetHeadPosition();

	while( pos != NULL )
		delete m_listObjectsIds.GetNext(pos);

	m_listObjectsIds.RemoveAll();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::LoadIdListElements
//	Description :	This routine
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::LoadIdListElements(int nTokNum)
{
	SSLexLexeme*	z_pLexeme	= elementFromProduction(nTokNum)->lexeme();
	CLexObject*		obj			= new CLexObject;
	CString			strToken(z_pLexeme->asConstChar());

	obj->Line(z_pLexeme->line());
	obj->Offset(z_pLexeme->offset());
	obj->Token(strToken);
	m_listObjectsIds.AddTail(obj);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckVarType
//	Description :	This routine places the local or global variables
//					into a list.
//
//	Return :		void	-
//
//	Parameters :
//		CString strType		-	variable type.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckVarType(CString strType)
{
	POSITION	pos = m_listTemp.GetHeadPosition();
	CString		strTemp;

	while( pos != NULL )
	{
		strTemp = m_listTemp.GetNext(pos);

		if( strType == _T("LOCAL") )
			m_listLocalArg.AddTail(strTemp);
		else if( strType == _T("GLOBAL") )
			m_listGlobalArg.AddTail(strTemp);
	}

	m_listTemp.RemoveAll();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::FindStream
//	Description :	This routine searches a list to find a stream name.
//
//	Return :		BOOL	-	TRUE if successful.
//
//	Parameters :
//		int nTok			-	token number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL RulesYaccClass::FindStream(int nTok, BOOL bList, CLexObject* lexObj)
{
	EPSTRMINFO			epStrmInfo;
	BOOL				bRtnVal		= TRUE;
	SSLexLexeme*		z_pLexeme	= NULL;
	CSchedMgrECPApp*	pApp		= (CSchedMgrECPApp*)AfxGetApp();
	CComBSTR			bStrStrmName;

	if( bList )
		bStrStrmName= lexObj->Token();
	else
	{
		z_pLexeme	= elementFromProduction(nTok)->lexeme();
		bStrStrmName= z_pLexeme->asChar();
	}

	HRESULT	hr = (pApp->m_pEpSMon)->GetStrmInfo(bStrStrmName, &epStrmInfo);

	if( FAILED(hr) )
	{
		m_nErrorNum	= IDS_SYNTAX_ERROR14;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR14);

		if( bList )
		{
			USES_CONVERSION;
			SSLexLexeme	z_Lexeme;
			LPSTR		lpszAscii = T2A((LPTSTR)((LPCTSTR)lexObj->Token()));

			z_Lexeme.setLine((SSUnsigned32)lexObj->Line());
			z_Lexeme.setOffset((SSUnsigned32)lexObj->Offset());
			z_Lexeme.setBuffer(lpszAscii, strlen(lpszAscii));
			parseError(z_Lexeme);
		}
		else
			parseError(*z_pLexeme);

		bRtnVal		= FALSE;
	}
	else
	{
		SysFreeString(epStrmInfo.bstrStrmName);
		SysFreeString(epStrmInfo.bstrHost);
		SysFreeString(epStrmInfo.bstrDBName);
	}

	return bRtnVal;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckRTCS
//	Description :	This routine checks rtcs syntax.
//
//	Return :		BOOL	-	TRUE if successful.
//
//	Parameters :
//		int tok1-2			-	token number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL RulesYaccClass::CheckRTCS(int tok1, int tok2)
{
	SSLexLexeme*	z_pLexeme = elementFromProduction(tok1)->lexeme();
	CString			strToken(z_pLexeme->asConstChar());

	strToken.MakeUpper();

	if( strToken != _T("RT") )
	{
		m_nErrorNum = IDS_SYNTAX_ERROR16;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR16);
		parseError(*z_pLexeme);
		return FALSE;
	}

	if( tok2 > tok1 )
	{
		if( !CheckToken(tok2,_T("ACE_1"),_T("ACE_2"),_T("ACE_FOR_DWELL"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" ")) )
			return FALSE;
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::FindTlmPoint
//	Description :	This routine searches the database for a given
//					telemetry point.
//
//	Return :		BOOL	-	TRUE if successful.
//	Parameters :
//		int tok				-	token number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL RulesYaccClass::FindTlmPoint(int tok, BOOL bChkStmId, UINT nErr)
{
	//database call for lexeme tok
	CSchedMgrECPApp*	pApp		= (CSchedMgrECPApp*)AfxGetApp();
	SSLexLexeme*		z_pLexeme	= elementFromProduction(tok)->lexeme();
	IEpEnumPoints*		pEnumPoints;
	CComBSTR			bstrMnemonic(z_pLexeme->asConstChar());
	CString				strToken(z_pLexeme->asConstChar());
	BOOL				bVal		= TRUE;
	HRESULT				hr			= (pApp->m_pEpDB)->GetPointEnum(pApp->m_ulDBHandle, bstrMnemonic, &pEnumPoints);

	if( bChkStmId )
		bVal = FindStream(tok, FALSE, NULL);

	if( FAILED(hr) && !bVal )
	{
		m_nErrorNum	= nErr;
		m_strErrorMessage.Format(nErr);
		parseError(*z_pLexeme);
		bVal		= FALSE;
	}

	if( pEnumPoints != NULL )
	{
		pEnumPoints->Release();
		pEnumPoints = NULL;
	}

	return bVal;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckStrToken2
//	Description :	This routine compares strings against token.
//
//	Return :		BOOL	-	TRUE if successful.
//	Parameters :
//		int tokNum			-	token number.
//		CString str1-8		-	directive options.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL RulesYaccClass::CheckToken(
	int tokNum,	CString str1, CString str2, CString str3, CString str4,
				CString str5, CString str6, CString str7, CString str8)
{
	SSLexLexeme*	z_pLexeme = elementFromProduction(tokNum)->lexeme();
	CString			strToken(z_pLexeme->asConstChar());

	strToken.MakeUpper();

	if( strToken != str1 && strToken != str2 && strToken != str3 && strToken != str4 &&
		strToken != str5 && strToken != str6 && strToken != str7 && strToken != str8 )
	{
		m_nErrorNum = IDS_SYNTAX_ERROR18;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR18);
		parseError(*z_pLexeme);
		return FALSE;
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckList
//	Description :	This routine compares a token against a list of
//					directive options.
//
//	Return :		BOOL	-	TRUE if successful.
//	Parameters :
//		int tokNum			-	token number.
//		CStringList* pList	-	pointer to a list of directive options.
//		UINT nErr			-	error number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL RulesYaccClass::CheckList(int tokNum, CStringList* pList, UINT nErr, BOOL bIsOpt)
{
	SSLexLexeme*	z_pLexeme	= elementFromProduction(tokNum)->lexeme();
	CString			strToken(z_pLexeme->asConstChar());
	BOOL			bError		= FALSE;

	strToken.MakeUpper();

	if( bIsOpt )
	{
		if( pList->Find(strToken) != NULL )
			bError = TRUE;
	}
	else
	{
		if( pList->Find(strToken) == NULL )
			bError = TRUE;
	}

	if( bError )
	{
		m_nErrorNum = nErr;
		m_strErrorMessage.Format(nErr);
		parseError(*z_pLexeme);
		return FALSE;
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::OptionLookup
//	Description :	This routine compares a token against a map of
//					directive option objects.
//
//	Return :		BOOL			-	TRUE if successful.
//	Parameters :
//		int tokNum					-	token number.
//		CMapStringToString* pStrMap	-	pointer to a list of directive
//										options.
//		UINT nErr					-	error number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL RulesYaccClass::OptionLookup(int tokNum, CMapStringToString* pStrMap, UINT nErr)
{
	SSLexLexeme*	z_pLexeme = elementFromProduction(tokNum)->lexeme();
	CString			strToken(z_pLexeme->asConstChar());
	CString			strItem;

	strToken.MakeUpper();
	pStrMap->Lookup(strToken, strItem);

	if( !strItem.IsEmpty() )
	{
		m_nErrorNum = nErr;
		m_strErrorMessage.Format(nErr);
		parseError(*z_pLexeme);
		return FALSE;
	}

	pStrMap->SetAt(strToken, _T("R"));
	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::OptionMapLookup
//	Description :	This routine compares a token against a map of
//					directive option objects.
//
//	Return :		BOOL			-	TRUE if successful.
//	Parameters :
//		int tokNum					-	token number.
//		CMapStringToString* pStrMap	-	pointer to a list of directive
//										options.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL RulesYaccClass::OptionMapLookup(int tokNum, CMapStringToString* pStrMap, eOptType eOpt)
{
	SSLexLexeme*	z_pLexeme	= elementFromProduction(tokNum)->lexeme();
	CString			strToken(z_pLexeme->asConstChar());
	POSITION		pos			= pStrMap->GetStartPosition();
	BOOL			bRtnVal		= TRUE;
	CString			strKey;
	CString			strItm;
	CString			strErr;

	while( pos != NULL )
	{
		pStrMap->GetNextAssoc(pos, strKey, strItm);

		if( strItm.IsEmpty() )
			strErr += _T(" ") + strKey;
	}

	if( !strErr.IsEmpty() )
	{
		m_strErrorMessage  = _T("Required options for ") + strToken + _T(" directive:");
		m_strErrorMessage += strErr;
		parseError(*z_pLexeme);
		bRtnVal	= FALSE;
	}

	switch( eOpt )
	{
		case eSCAN:
			pStrMap->SetAt(ctstrFRAME,				_T(""));
			pStrMap->SetAt(ctstrPRIORITY,			_T(""));
			pStrMap->SetAt(ctstrMODE,				_T(""));
			pStrMap->SetAt(ctstrSIDE,				_T(""));
			pStrMap->SetAt(ctstrINSTRUMENT,			_T(""));
			m_listScanOptOptions.RemoveAll();
			break;
		case eSCANDATA:
			pStrMap->SetAt(ctstrFRAME,				_T(""));
			pStrMap->SetAt(ctstrPRIORITY,			_T(""));
			pStrMap->SetAt(ctstrMODE,				_T(""));
			pStrMap->SetAt(ctstrSIDE,				_T(""));
			pStrMap->SetAt(ctstrINSTRUMENT,			_T(""));
			pStrMap->SetAt(ctstrREPEAT,				_T(""));
			pStrMap->SetAt(ctstrDUR,				_T(""));
			pStrMap->SetAt(ctstrOBJID,				_T(""));
			pStrMap->SetAt(ctstrEXECUTE,			_T(""));
			m_listScandataOptOptions.RemoveAll();
			break;
		case eSTAR:
			pStrMap->SetAt(ctstrDUR,				_T(""));
			pStrMap->SetAt(ctstrINSTRUMENT,			_T(""));
			pStrMap->SetAt(ctstrMAX,				_T(""));
			m_listStarOptOptions.RemoveAll();
			break;
		case eSTARDATA:
			pStrMap->SetAt(ctstrDUR,				_T(""));
			pStrMap->SetAt(ctstrOBJID,				_T(""));
			pStrMap->SetAt(ctstrINSTRUMENT,			_T(""));
			pStrMap->SetAt(ctstrMAX,				_T(""));
			pStrMap->SetAt(ctstrLAST_STAR_CLASS,	_T(""));
			pStrMap->SetAt(ctstrLOAD_REG,			_T(""));
			break;
		case eSTARINFO:
			pStrMap->SetAt(ctstrOBJID,				_T(""));
			pStrMap->SetAt(ctstrINSTRUMENT,			_T(""));
			pStrMap->SetAt(_T("STARID"),			_T(""));
			pStrMap->SetAt(ctstrTIME,				_T(""));
			pStrMap->SetAt(ctstrDWELL,				_T(""));
			pStrMap->SetAt(ctstrNUMLOOKS,			_T(""));
			pStrMap->SetAt(ctstrLOOKNUM,			_T(""));
			pStrMap->SetAt(ctstrSC,					_T(""));
//			pStrMap->SetAt(_T("OFFSET"),			_T(""));
			break;
		default:
			bRtnVal	= FALSE;
	}

	return bRtnVal;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckDatawordArgs
//	Description :	This routine validates command submnemonics
//
//	Return :		void			-
//	Parameters :
//		int nTokNum					-	token number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckDatawordArgs(int nTokNum)
{
	USES_CONVERSION;
	CLexObject*				lexObj;
	IEpEnumDatawordArgs*	pEnumDwArgs = NULL;
	ULONG					ulFetched = 0;
	HRESULT					hr = 0;
	POSITION				pos			= m_listObjectsIds.GetHeadPosition();
	LPSTR					lpszAscii	= NULL;
	SSLexLexeme				z_Lexeme;

	if( m_pEnumCmds == NULL )
	{
		DeleteIdListElements();
		return;
	}

	while( pos != NULL )
	{
		bool bFound	= false;

		lexObj	= (CLexObject*)m_listObjectsIds.GetNext(pos);
		hr		= m_pEnumCmds->GetDatawordArgEnum(&pEnumDwArgs);

		if( FAILED(hr) )
		{
			m_nErrorNum = IDS_SYNTAX_ERROR10;
			m_strErrorMessage.Format(IDS_SYNTAX_ERROR10);
			lpszAscii	= T2A((LPTSTR)((LPCTSTR)lexObj->Token()));
			z_Lexeme.setLine((SSUnsigned32)lexObj->Line());
			z_Lexeme.setOffset((SSUnsigned32)lexObj->Offset());
			z_Lexeme.setBuffer(lpszAscii, strlen(lpszAscii));
			parseError(z_Lexeme);
			DeleteIdListElements();
			return;
		}

		EpDbCmdDatawordArgDescription cdwaDesc;

		while( !bFound )
		{
			pEnumDwArgs->Next(1, &cdwaDesc, &ulFetched);

			if( ulFetched == 0 )  break;

			switch( cdwaDesc.nType )
			{
				case EPDATAWORDARGTYPE_POSITION:
				{
					IEpEnumDatawordArgValues* pEnum;

					hr = pEnumDwArgs->GetDatawordArgValueEnum(&pEnum);

					if( FAILED(hr) )
					{
						m_nErrorNum = IDS_SYNTAX_ERROR11;
						m_strErrorMessage.Format(IDS_SYNTAX_ERROR11);
						lpszAscii	= T2A((LPTSTR)((LPCTSTR)lexObj->Token()));
						z_Lexeme.setLine((SSUnsigned32)lexObj->Line());
						z_Lexeme.setOffset((SSUnsigned32)lexObj->Offset());
						z_Lexeme.setBuffer(lpszAscii, strlen(lpszAscii));
						parseError(z_Lexeme);
						pEnumDwArgs->Release();
						pEnumDwArgs = NULL;
						DeleteIdListElements();
						return;
					}

					bFound = CheckDatawordArgValue(pEnum, lexObj->Token()/*strTemp*/);

					if( pEnum != NULL )
					{
						pEnum->Release();
						pEnum = NULL;
					}

					break;
				}
				case EPDATAWORDARGTYPE_KEYWORD:
				case EPDATAWORDARGTYPE_TLM_OR_KEYWORD:
				{
					if( lexObj->Token() == cdwaDesc.bstrKeyword )
					{
						IEpEnumDatawordArgValues* pEnum;

						lexObj	= (CLexObject*)m_listObjectsIds.GetNext(pos);
						hr		= pEnumDwArgs->GetDatawordArgValueEnum(&pEnum);

						if( FAILED(hr) )
						{
							m_nErrorNum = IDS_SYNTAX_ERROR11;
							m_strErrorMessage.Format(IDS_SYNTAX_ERROR11);
							lpszAscii	= T2A((LPTSTR)((LPCTSTR)lexObj->Token()));
							z_Lexeme.setLine((SSUnsigned32)lexObj->Line());
							z_Lexeme.setOffset((SSUnsigned32)lexObj->Offset());
							z_Lexeme.setBuffer(lpszAscii, strlen(lpszAscii));
							parseError(z_Lexeme);
							pEnumDwArgs->Release();
							pEnumDwArgs = NULL;
							DeleteIdListElements();
							return;
						}

						bFound	= CheckDatawordArgValue(pEnum, lexObj->Token());

						if( pEnum != NULL )
						{
							pEnum->Release();
							pEnum = NULL;
						}
					}

					break;
				}
				case EPDATAWORDARGTYPE_TELEM:
				case EPDATAWORDARGTYPE_LOAD_FILE:
				case EPDATAWORDARGTYPE_PROCEDURE_FILE:
				case EPDATAWORDARGTYPE_FRAME_SEQ_NUM:
				case EPDATAWORDARGTYPE_MAX_DATAWORD_ARG_TYPE:
				default:
					break;
			}

			POSITION p = m_listDatawords.Find(lexObj->Token());

			if( p != NULL )
				m_listDatawords.RemoveAt(p);

			SysFreeString(cdwaDesc.bstrKeyword);
			SysFreeString(cdwaDesc.bstrMnemonic);
			SysFreeString(cdwaDesc.bstrDescription);
		}
	}

	DeleteIdListElements();
	pEnumDwArgs->Release();
	pEnumDwArgs = NULL;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckDatawordArgValue
//	Description :	This routine validates a command submnemonic value
//
//	Return :		bool	-
//	Parameters :
//		IEpEnumDatawordArgValues* pEnum		-	pointer to structure.
//		CString strToken					-	value to be found.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
bool RulesYaccClass::CheckDatawordArgValue(IEpEnumDatawordArgValues* pEnum, CString strToken)
{
	EpDbCmdDatawordArgValueDescription	cdwavDesc;
	ULONG								ulFetched;
	bool								bFound	= false;

	while( !bFound )
	{
		pEnum->Next(1, &cdwavDesc, &ulFetched);

		if( ulFetched == 0 )  break;

		if( strToken == cdwavDesc.bstrMnemonic )
			bFound = true;

		SysFreeString(cdwavDesc.bstrMnemonic);
		SysFreeString(cdwavDesc.bstrDescription);
	}

	return bFound;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckKeywordValue
//	Description :	This routine validates command submnemonics
//
//	Return :		void			-
//	Parameters :
//		int tok1-2					-	token number.
//		BOOL bPos					-	positional subnmemonic
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckKeywordValue(int tok1, int tok2)
{
	SSLexLexeme*			z_pLexeme = elementFromProduction(tok1)->lexeme();
	CString					strToken(z_pLexeme->asConstChar());
	IEpEnumDatawordArgs*	pEnumDwArgs;
	ULONG					ulFetched;
	HRESULT					hr;

	strToken.MakeUpper();

	if( m_pEnumCmds == NULL ) return;

	hr = m_pEnumCmds->GetDatawordArgEnum(&pEnumDwArgs);

	if( FAILED(hr) )
	{
		m_nErrorNum = IDS_SYNTAX_ERROR10;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR10);
		parseError(*z_pLexeme);
		return;
	}

	EpDbCmdDatawordArgDescription cdwaDesc;
	bool bFound = false;

	while( !bFound )
	{
		pEnumDwArgs->Next(1, &cdwaDesc, &ulFetched);

		if( ulFetched == 0 )  break;

		if( strToken == cdwaDesc.bstrKeyword )
		{
			bFound = true;

			POSITION p = m_listDatawords.Find(strToken);

			if( p != NULL )
				m_listDatawords.RemoveAt(p);
		}

		SysFreeString(cdwaDesc.bstrKeyword);
		SysFreeString(cdwaDesc.bstrDescription);
	}

	if( !bFound )
	{
		m_nErrorNum = IDS_SYNTAX_ERROR10;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR10);
		parseError(*z_pLexeme);
		pEnumDwArgs->Release();
		pEnumDwArgs = NULL;
		return;
	}

	z_pLexeme = elementFromProduction(tok2)->lexeme();

	if( z_pLexeme != NULL )
	{
		strToken= z_pLexeme->asConstChar();
		strToken.MakeUpper();

		IEpEnumDatawordArgValues* pEnum;
		hr = pEnumDwArgs->GetDatawordArgValueEnum(&pEnum);

		if( FAILED(hr) || !(CheckDatawordArgValue(pEnum, strToken)) )
		{
			m_nErrorNum = IDS_SYNTAX_ERROR11;
			m_strErrorMessage.Format(IDS_SYNTAX_ERROR11);
			parseError(*z_pLexeme);
			pEnumDwArgs->Release();
			pEnumDwArgs = NULL;
			return;
		}

		pEnum->Release();
		pEnum = NULL;
	}

	pEnumDwArgs->Release();
	pEnumDwArgs = NULL;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckCmdMnemonic
//	Description :	This routine validates command mnemonics.
//
//	Return :		void	-
//	Parameters :
//		int tokNum			-	token number.
//		BOOL bCkReqArgs		-	check for required arguments.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckCmdMnemonic(int tokNum, BOOL bCkReqArgs)
{
	//database call for lexeme 0
	CSchedMgrECPApp*		pApp		= (CSchedMgrECPApp*)AfxGetApp();
	SSLexLexeme*			z_pLexeme	= elementFromProduction(tokNum)->lexeme();
	CComBSTR				bstrMnemonic(z_pLexeme->asConstChar());
	EpDbCmdDescription		cmdDesc;
	IEpEnumDatawordArgs*	pEnumDwArgs;
	ULONG					ulFetched;
	HRESULT					hr			= (pApp->m_pEpDB)->GetCommandEnum(pApp->m_ulDBHandle, bstrMnemonic, &m_pEnumCmds);

	if( FAILED(hr) )
	{
		m_nErrorNum = IDS_SYNTAX_ERROR4;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR4);
		parseError(*z_pLexeme);
		return;
	}

	if( bCkReqArgs )
	{
		hr = m_pEnumCmds->Next(1, &cmdDesc, &ulFetched);

		if( ulFetched == 0 )
		{
			m_nErrorNum = IDS_SYNTAX_ERROR9;
			m_strErrorMessage.Format(IDS_SYNTAX_ERROR9);
			parseError(*z_pLexeme);
			return;
		}

		hr = m_pEnumCmds->GetDatawordArgEnum(&pEnumDwArgs);

		if( SUCCEEDED(hr) )
		{
			EpDbCmdDatawordArgDescription cdwaDesc;

			while( TRUE )
			{
				pEnumDwArgs->Next(1, &cdwaDesc, &ulFetched);

				if( ulFetched == 0 ) break;

				if( cdwaDesc.bRequired )
					m_listDatawords.AddTail(cdwaDesc.bstrKeyword);

				SysFreeString(cdwaDesc.bstrKeyword);
				SysFreeString(cdwaDesc.bstrDescription);
			}
		}

		SysFreeString(cmdDesc.bstrMnemonic);
		SysFreeString(cmdDesc.bstrDescription);
		SysFreeString(cmdDesc.bstrAlias);
		SysFreeString(cmdDesc.bstrExecMnemonic);
		SysFreeString(cmdDesc.bstrCmdFormat);
		SysFreeString(cmdDesc.bstrSpacecraftAddr);
		SysFreeString(cmdDesc.bstrPrivilegeGroup);

		if( pEnumDwArgs != NULL )
		{
			pEnumDwArgs->Release();
			pEnumDwArgs = NULL;
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::GetStackElement
//	Description :	This routine returns a stack element for a given
//					production.
//
//	Return :		SSYaccStackElement*	-	pointer to a stack element.
//	Parameters :
//		int nTokNum						-	token number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
SSYaccStackElement* RulesYaccClass::GetStackElement(int nTokNum)
{
	SSLexLexeme* z_pLexeme = elementFromProduction(nTokNum)->lexeme();

	if( z_pLexeme != NULL )
	{
		SSYaccStackElement* z_pRet = stackElement();
		z_pRet->setLexeme(*z_pLexeme);
		return z_pRet;
	}

	return NULL;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckVarDef
//	Description :	This routine checks for variable declaration.
//
//	Return :		BOOL	-	TRUE if defined.
//	Parameters :
//		int nTokNum			-	token number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL RulesYaccClass::CheckVarDef(int nTokNum)
{
	CSchedMgrECPApp*	pApp		= (CSchedMgrECPApp*)AfxGetApp();
	SSLexLexeme*		z_pLexeme	= elementFromProduction(nTokNum)->lexeme();
	bool				bFound		= false;

	if( z_pLexeme == NULL )
		return TRUE;

	CString	strToken(z_pLexeme->asConstChar());

	if( FindVariable(strToken) )
		return TRUE;

	IEpEnumPoints*	pEnumPoints = NULL;
	CComBSTR		bstrMnemonic(z_pLexeme->asConstChar());
	HRESULT			hr = (pApp->m_pEpDB)->GetPointEnum(pApp->m_ulDBHandle, bstrMnemonic, &pEnumPoints);

	if( SUCCEEDED(hr) )
	{
		pEnumPoints->Release();
		pEnumPoints = NULL;
		return TRUE;
	}

	IEpEnumDatawordArgs*	pEnumDwArgs;
	ULONG					ulFetched;

	if( m_pEnumCmds != NULL )
	{
		hr = m_pEnumCmds->GetDatawordArgEnum(&pEnumDwArgs);

		if( SUCCEEDED(hr) )
		{
			EpDbCmdDatawordArgDescription cdwaDesc;

			strToken.MakeUpper();

			while( !bFound )
			{
				pEnumDwArgs->Next(1, &cdwaDesc, &ulFetched);

				if( ulFetched == 0 )  break;

				IEpEnumDatawordArgValues* pEnum;
				hr = pEnumDwArgs->GetDatawordArgValueEnum(&pEnum);

				if( SUCCEEDED(hr) )
				{
					bFound	= CheckDatawordArgValue(pEnum, strToken);
					pEnum->Release();
					pEnum	= NULL;
				}

				SysFreeString(cdwaDesc.bstrKeyword);
				SysFreeString(cdwaDesc.bstrDescription);
			}

			pEnumDwArgs->Release();
			pEnumDwArgs = NULL;
		}
	}

	if( !bFound )
	{
		m_nErrorNum = IDS_SYNTAX_ERROR19;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR19);
		parseError(*z_pLexeme);
		return FALSE;
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::AddVarToList
//	Description :	This routine adds a variable to a list.
//
//	Return :		BOOL	-	TRUE if added successfully.
//	Parameters :
//		int nTokNum			-	token number.
//		BOOL bRtnEle		-	return stack element.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
SSYaccStackElement* RulesYaccClass::AddVarToList(int nTokNum, BOOL bRtnEle)
{
	CSchedMgrECPApp*	pApp		= (CSchedMgrECPApp*)AfxGetApp();
	SSLexLexeme*		z_pLexeme	= elementFromProduction(nTokNum)->lexeme();
	IEpEnumPoints*		pEnumPoints;

	if( z_pLexeme != NULL )
	{
		CComBSTR	bstrMnemonic(z_pLexeme->asConstChar());
		HRESULT		hr = (pApp->m_pEpDB)->GetPointEnum(pApp->m_ulDBHandle, bstrMnemonic, &pEnumPoints);

		if( FAILED(hr) )
		{
			CString	strToken(z_pLexeme->asConstChar());

			m_listTemp.AddTail(strToken);
		}

		if( pEnumPoints != NULL )
		{
			pEnumPoints->Release();
			pEnumPoints = NULL;
		}

		if( bRtnEle )
		{
			SSYaccStackElement* z_pRet = stackElement();
			z_pRet->setLexeme(*z_pLexeme);

			return z_pRet;
		}
	}

	return NULL;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckCfgTlm
//	Description :	This routine validates the CFGTLM directive.
//
//	Return :		void	-
//	Parameters :
//		int nTok1-2			-	token number.
//		BOOL bChkSrc		-	Check source type.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckCfgTlm(int nTok1, int nTok2, BOOL bChkSrc)
{
	SSLexLexeme*	z_pLexeme	= elementFromProduction(nTok1)->lexeme();
	CString			strKey(z_pLexeme->asConstChar());
	CStringList*	itemList	= NULL;

	strKey.MakeUpper();

	if( !m_mapTlmType.Lookup(strKey, (CObject*&)itemList) )
	{
		m_nErrorNum = IDS_SYNTAX_ERROR7;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR7);
		parseError(*z_pLexeme);
	}
	else
	{
		if( bChkSrc )
		{
			z_pLexeme	= elementFromProduction(nTok2)->lexeme();
			strKey		= z_pLexeme->asConstChar();

			strKey.MakeUpper();

			if( itemList->Find(strKey) == NULL )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR8;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR8);
				parseError(*z_pLexeme);
			}
		}
	}

	itemList = NULL;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::FindVariable
//	Description :	This routine seraches for a variable name.
//
//	Return :		void	-
//	Parameters :
//		CString strVar		-	name of variable.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL RulesYaccClass::FindVariable(CString strVar)
{
	if( m_listArgsParam.Find(strVar)== NULL &&
		m_listLocalArg.Find(strVar) == NULL &&
		m_listGlobalArg.Find(strVar)== NULL )
		return FALSE;

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::GetHostName
//	Description :	This routine finds the name of the GTACS host.
//
//	Return :		void	-
//	Parameters :
//		CString strVar		-	name of variable.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::FindHostName(int nTokNum)
{
	SSLexLexeme*	z_pLexeme = elementFromProduction(nTokNum)->lexeme();
	CString			strToken(z_pLexeme->asConstChar());

	//get host name from registry
	CRegGTACS		regGTACS;
	bool			bFnd = false;

	for( int i=0; i<nMaxSites; i++ )
	{
		for( int j=0; j<nMaxGTACSperSite; j++ )
		{
			if( strToken == regGTACS.m_strAbbrev[i][j] )
			{
				bFnd = true;
				break;
			}
		}
	}

	if( !bFnd )
	{
		m_nErrorNum = IDS_SYNTAX_ERROR17;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR17);
		parseError(*z_pLexeme);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckArgExpLst
//	Description :	This routine checks the argument expression list
//					for validity.
//
//	Return :		void	-
//	Parameters :
//		int nTokNum			-	token number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckArgExpLst(int nTokNum)
{
	SSLexLexeme*	z_pLexeme	= elementFromProduction(nTokNum)->lexeme();

	if( z_pLexeme == NULL ) return;

	CString			strToken(z_pLexeme->asConstChar());
	POSITION		pos			= m_listTemp.GetHeadPosition();
	CString			strTemp;

	strToken.MakeUpper();

	if( strToken == _T("ARGS") )
	{
		if( m_listTemp.GetCount() == 1 )
		{
			LPCTSTR	strBuf = (LPCTSTR)m_listTemp.GetAt(pos);
			int		nNumber= atoi((const char*)strBuf);

			if( nNumber > 0 )
			{
				for( int j=0; j<nNumber; j++ )
				{
					CString strArg;
					strArg.Format(_T("$%d"), j+1);
					m_listArgsParam.AddTail(strArg);
				}

				m_listTemp.RemoveAll();
				return;
			}
		}

		while( pos != NULL )
		{
			strTemp = m_listTemp.GetNext(pos);
			m_listArgsParam.AddTail(strTemp);
		}
	}
	else
	{
		USES_CONVERSION;
		CString strTemp;

		while( pos != NULL )
		{
			strTemp = m_listTemp.GetNext(pos);

			if( !FindVariable(strTemp) )
			{
				LPSTR lpszAscii	= T2A((LPTSTR)((LPCTSTR)strTemp));
				m_nErrorNum		= IDS_SYNTAX_ERROR19;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR19);
				z_pLexeme->setBuffer(lpszAscii, strlen(lpszAscii));
				parseError(*z_pLexeme);
				break;
			}
		}
	}

	m_listTemp.RemoveAll();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::TravIdList
//	Description :	This routine iterates thru a list of stream tokens.
//
//	Return :		void	-
//	Parameters :
//		int nTokNum			-	token number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::TravIdList(int nTokNum)
{
	CLexObject*		lexObj;
	SSLexLexeme		z_Lexeme;
	POSITION		pos	= m_listObjectsIds.GetHeadPosition();

	while( pos != NULL )
	{
		lexObj	= (CLexObject*)m_listObjectsIds.GetNext(pos);

		if( !FindStream(nTokNum, TRUE, lexObj) )
			pos = NULL;
	}

	DeleteIdListElements();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckCmdStmt
//	Description :	This routine checks the CMD statement for required
//					options.
//
//	Return :		void	-
//	Parameters :
//		int nTokNum			-	token number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckCmdStmt(int nTokNum)
{
	DeleteIdListElements();

	if( m_pEnumCmds != NULL )
	{
		m_pEnumCmds->Release();
		m_pEnumCmds = NULL;
	}

	if( !m_listDatawords.IsEmpty() )
	{
		SSLexLexeme*	z_pLexeme	= elementFromProduction(nTokNum)->lexeme();
		CString			strTemp(_T("Command Mnemonic Required Dataword(s): "));
		POSITION		pos			= m_listDatawords.GetHeadPosition();

		while( pos != NULL )
		{
			strTemp += m_listDatawords.GetNext(pos);
			strTemp += _T(" ");
		}

		m_listDatawords.RemoveAll();
		m_strErrorMessage = strTemp;
		parseError(*z_pLexeme);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckCmdStmt
//	Description :	This routine checks the validity of the
//					comma expression.
//
//	Return :		void	-
//	Parameters :
//		int nTokNum			-	token number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckCommaExp(int nTokNum)
{
	SSLexLexeme* z_pLexeme = elementFromProduction(nTokNum)->lexeme();

	if( z_pLexeme != NULL )
	{
		POSITION	pos = m_listTemp.GetHeadPosition();
		CString		strTemp(z_pLexeme->asConstChar());

		if( pos == NULL )
		{
			m_nErrorNum = IDS_SYNTAX_ERROR19;
			m_strErrorMessage.Format(IDS_SYNTAX_ERROR19);
			parseError(*z_pLexeme);
			return;
		}

		while( pos != NULL )
		{
			strTemp = m_listTemp.GetNext(pos);

			if( !FindVariable(strTemp) )
			{
				m_nErrorNum = IDS_SYNTAX_ERROR19;
				m_strErrorMessage.Format(IDS_SYNTAX_ERROR19);
				parseError(*z_pLexeme);
				break;
			}
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckAssignExp
//	Description :	This routine checks the validity of the
//					assignment '=' expression.
//
//	Return :		void	-
//	Parameters :
//		int nTokNum1		-	token number 1.
//		int nTokNum2		-	token number 2.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckAssignExp(int nTokNum1, int nTokNum2, int nTokNum3)
{
	SSLexLexeme* z_pLexemeP = elementFromProduction(nTokNum1)->lexeme();

	if( z_pLexemeP == NULL )
	{
		SSLexLexeme	z_LexemeO;

		z_pLexemeP			= elementFromProduction(nTokNum2)->lexeme();
		m_nErrorNum			= 0;
		m_strErrorMessage	= _T("Syntax Error!: Invalid assignment expression");
		z_LexemeO.setLine((SSUnsigned32)z_pLexemeP->line());
		z_LexemeO.setOffset((SSUnsigned32)z_pLexemeP->offset());
		z_LexemeO.setBuffer("$", 1);
		parseError(z_LexemeO);
		return;
	}

	CString strToken(z_pLexemeP->asChar());

	if( !FindVariable(strToken) )
	{
		m_nErrorNum = IDS_SYNTAX_ERROR19;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR19);
		parseError(*z_pLexemeP);
	}

	CheckVarDef(nTokNum3);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::LoadTlmTypeCfgFile
//	Description :	This routine
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::LoadTlmTypeCfgFile()
{
	int				nCurLine= 0;
	int				nPathNum= 0;
	BOOL			bFound	= FALSE;
	CString			strLine;
	CString			tlmType;
	CString			tlmSrc;
	CString			strPath;
	CFileException	ex;
	CTextFile		tf;


//  ctcEPOCHREPORTS;
	GetSearchPaths(ctstrEPOCHREPORTS);

	while( !bFound && (nPathNum < m_SearchPathArray.GetSize()) )
	{
		// ctcTLM_SRCCFG
		strPath = m_SearchPathArray[nPathNum++] + _T("TLM_SRCS.cfg");

		if( tf.Load(strPath, CTextFile::RO, &ex) )
		{
			bFound = TRUE;
			tf.Clear();
			tf.Close(CTextFile::NOWRITE, &ex);
		}
	}

	if( !bFound )
	{
		CString strMsg(_T("Syntax Check: Unable to locate file TLM_SRCS.cfg."));
		GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
		(*m_pnErrorCnt)++;
		return;
	}

	int i = 0;

	tf.Load(strPath, CTextFile::RO, &ex);

	while( nCurLine < tf.GetLineCount() )
	{
		strLine = tf.GetTextAt(nCurLine++);
		strLine.TrimLeft();

		if( !strLine.IsEmpty() && ctchCOMMENT != strLine[0] )
		{
			if( strLine.Find(_T("TLM_TYPE"), 0) != -1 )
			{
				strLine.Delete(0, 9);
				strLine.TrimLeft();
				strLine.TrimRight();
				tlmType = strLine;

				strLine = tf.GetTextAt(nCurLine++);
				CStringList* l = new CStringList;

				while( nCurLine < tf.GetLineCount() && strLine.Find(_T("TLM_SRC"), 0) != -1 )
				{
					strLine.Delete(0, 8);
					strLine.TrimLeft();

					i = strLine.FindOneOf(_T("\t "));

					tlmSrc= strLine.Left(i);
					l->AddTail(tlmSrc);
					strLine = tf.GetTextAt(nCurLine++);
				}

				m_mapTlmType[tlmType] = l;
				nCurLine--;
			}
		}
	}

	tf.Close(CTextFile::NOWRITE, &ex);
	m_SearchPathArray.RemoveAll();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::LoadCmdDestCfgFile
//	Description :	This routine
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::LoadCmdDestCfgFile()
{
	int				nCurLine= 0;
	int				nPathNum= 0;
	BOOL			bFound	= FALSE;
	CString			strLine;
	CString			cmdDest;
	CString			strPath;
	CFileException	ex;
	CTextFile		tf;

	GetSearchPaths(ctstrEPOCHREPORTS);

	while( !bFound && (nPathNum < m_SearchPathArray.GetSize()) )
	{
		strPath = m_SearchPathArray[nPathNum++] + _T("CMD_DESTS.cfg");

		if( tf.Load(strPath, CTextFile::RO, &ex) )
		{
			bFound = TRUE;
			tf.Clear();
			tf.Close(CTextFile::NOWRITE, &ex);
		}
	}

	if( !bFound )
	{
		CString strMsg(_T("Syntax Check: Unable to locate file CMD_DESTS.cfg."));
		GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
		(*m_pnErrorCnt)++;
		return;
	}

	int i = 0;

	tf.Load(strPath, CTextFile::RO, &ex);

	while( nCurLine < tf.GetLineCount() )
	{
		strLine = tf.GetTextAt(nCurLine++);
		strLine.TrimLeft();

		if( !strLine.IsEmpty() && ctchCOMMENT != strLine[0] )
		{
			if( strLine.Find(_T("CMD_DEST"), 0) != -1 )
			{
				strLine.Delete(0, 9);
				strLine.TrimLeft();
				strLine.TrimRight();

				i = strLine.FindOneOf(_T("\t "));

				cmdDest = strLine.Left(i);
				m_listCmdDest.AddTail(cmdDest);
			}
		}
	}

	tf.Close(CTextFile::NOWRITE, &ex);
	m_SearchPathArray.RemoveAll();
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::LoadDbModTypes
//	Description :	This routine
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::LoadDbModTypes()
{
	CString	strTemp;

	for( UINT i=IDS_DBMODTYPE1; i <= IDS_DBMODTYPE58; i++ )
	{
		strTemp.LoadString(i);
		m_listDbModType.AddTail(strTemp);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::GetSearchPaths
//	Description :	This routine
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::GetSearchPaths(CString strEnvVar)
{
   // Get the search path(s) from the NT environmental variable.
   // CString	strEPOCH_VAR(_wgetenv_s(strEnvVar));

   TCHAR* cEPOCH_VAR;
   size_t requiredSize;
   bool	  bDone = false;

   const size_t newsize = 100;
   TCHAR nstring[newsize];
   _tcscpy_s(nstring, strEnvVar);

   _tgetenv_s( &requiredSize, NULL, 0, nstring);

   cEPOCH_VAR = (TCHAR*) malloc(requiredSize * sizeof(TCHAR));
   if (!cEPOCH_VAR)
   {
		// output messge error with search path in environmental variable.
		CString strErrorMsg = _T("Unable to allocate mmemory for the enviromental variable.");
		GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
		bDone = true;
   }

   // Get the value of the LIB environment variable.
   _tgetenv_s( &requiredSize, cEPOCH_VAR, requiredSize, nstring);

   CString	strEPOCH_VAR(cEPOCH_VAR);
   CString	strDrive;
   CString	strPath;

   free(cEPOCH_VAR);

   // Now extract all search paths and prepend drive letter to path.
   while( !bDone && !strEPOCH_VAR.IsEmpty() )
   {
		int nPos = strEPOCH_VAR.Find(_T(':'));

		if( nPos != -1 )
		{
			strPath		= strEPOCH_VAR.Left(nPos);
			strEPOCH_VAR= strEPOCH_VAR.Right(strEPOCH_VAR.GetLength() - nPos - 1);
			strEPOCH_VAR.TrimLeft();
			strEPOCH_VAR.TrimRight();
			strPath.TrimLeft();
			strPath.TrimRight();
		}
		else
		{
			strPath	= strEPOCH_VAR;
			bDone	= true;
		}

		if( (nPos = strPath.Find(_T('='))) != -1 )
		{
			// Remove the drive letter.
			strDrive = strPath.Left(nPos + 1);
			strDrive.Replace(_T("/"), _T(""));
			strDrive.Replace(_T("="), _T(":"));

			strPath.Delete(0, nPos + 1);
			strPath.TrimLeft();
			strPath.TrimRight();
		}
		else
		{
			// output messge error with search path in environmental variable.
			CString strErrorMsg = _T("Error interpreting environmental variable.");
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			(*m_pnErrorCnt)++;
			bDone = true;
		}

		m_SearchPathArray.Add(strDrive + strPath + _T('/'));
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::CheckRange
//	Description :	This routine
//
//	Return :		void	-
//
//	Parameters :
	int nTokNum				-	token to check range.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void RulesYaccClass::CheckRange(int nTok, int nStrt, int nEnd, CString strMsg)
{
	SSLexLexeme*	z_pLexeme	= elementFromProduction(nTok)->lexeme();
	CString			strKey(z_pLexeme->asConstChar());
	int				nRate		= _ttoi(strKey);

	if( nRate < nStrt || nRate > nEnd )
	{
		m_strErrorMessage = strMsg;
		parseError(*z_pLexeme);
	}
}

/*
	You must keep this reduce function clean to avoid confusing
	the AutoMerge parser. By 'clean' we mean that if you are going
	to add a significant amount of code to a case, make a function
	call instead of adding the code directly to the case.

	NEVER add embedded switch statements inside this function.
*/

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		RulesYaccClass::reduce
//	Description :	This routine is called each time  a grammer rule,
//					or production, is recognized.
//
//	Return :		SSYaccStackElement*	-	element that is associated
//											with the token.
//
//	Parameters :
//		SSUnsigned32 ulProd	-	production element number.
//		SSUnsigned32 ulSize	-
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
SSYaccStackElement* RulesYaccClass::reduce( SSUnsigned32 ulProd,
      SSUnsigned32 ulSize)
{

	//Uncomment out this line if you want to build a parse tree
	//The treeRoot() function will contain the tree root when finished
	//return addSubTree();

	switch ( ulProd)
	{

/******************************/
/**** Procedure Directives ****/
/*    --------------------    */
/******************************/

		case PROD_STATEMENT_GLOBAL:
		// statement -> GLOBAL argumentExprList eol
		CheckVarType(_T("GLOBAL"));
		break;

		case PROD_STATEMENT_LOCAL:
		// statement -> LOCAL argumentExprList eol
		CheckVarType(_T("LOCAL"));
			break;

/**********************************/
/**** Stream Viewer Directives ****/
/*    ------------------------    */
/**********************************/

		case PROD_STATEMENT_AT_IDENTIFIER_TERM:
		// statement -> @ IDENTIFIER TERM eol
			FindStream(1, FALSE, NULL);
			break;

//************************************************************************
// GOES NOPQ Production section
//************************************************************************

		case PROD_STRINGERROR_OATS_ALL_TEXT:
		// stringError -> OATS ALL TEXT strError
		case PROD_STRINGERROR_OATS_INTEGERCONSTANT_TEXT:
		// stringError -> OATS INTEGERconstant TEXT strError
		case PROD_STRINGERROR_OATS_TEXT:
		// stringError -> OATS variable TEXT strError
		case PROD_STRINGERROR_SPS_ALL_TEXT:
		// stringError -> SPS ALL TEXT strError
		case PROD_STRINGERROR_SPS_INTEGERCONSTANT_TEXT:
		// stringError -> SPS INTEGERconstant TEXT strError
		case PROD_STRINGERROR_SPS_TEXT:
		// stringError -> SPS variable TEXT strError
		case PROD_STRINGERROR_SPS_ALL_FILE:
		// stringError -> SPS ALL FILE strError
		case PROD_STRINGERROR_SPS_FILE:
		// stringError -> SPS variable FILE strError
		case PROD_STRINGERROR_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_EQUALITY:
		// stringError -> TLMWAIT INTEGERconstant IDENTIFIER EQUALITY strError
		case PROD_STRINGERROR_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_EQUALITY2:
		// stringError -> TLMWAIT INTEGERconstant IDENTIFIER EQUALITY stringLiteral strError
		case PROD_STRINGERROR_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_EQUALITY3:
		// stringError -> TLMWAIT INTEGERconstant IDENTIFIER EQUALITY constant strError
		case PROD_STRINGERROR_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_EQUALITY4:
		// stringError -> TLMWAIT INTEGERconstant IDENTIFIER EQUALITY variable strError
		case PROD_STRINGERROR_TLMWAIT_IDENTIFIER_EQUALITY:
		// stringError -> TLMWAIT variable IDENTIFIER EQUALITY strError
		case PROD_STRINGERROR_TLMWAIT_IDENTIFIER_EQUALITY2:
		// stringError -> TLMWAIT variable IDENTIFIER EQUALITY stringLiteral strError
		case PROD_STRINGERROR_TLMWAIT_IDENTIFIER_EQUALITY3:
		// stringError -> TLMWAIT variable IDENTIFIER EQUALITY constant strError
		case PROD_STRINGERROR_TLMWAIT_IDENTIFIER_EQUALITY4:
		// stringError -> TLMWAIT variable IDENTIFIER EQUALITY variable strError
		case PROD_STRINGERROR_TLMWAIT_TIMECONSTANT_IDENTIFIER_EQUALITY:
		// stringError -> TLMWAIT TIMEconstant IDENTIFIER EQUALITY strError
		case PROD_STRINGERROR_TLMWAIT_TIMECONSTANT_IDENTIFIER_EQUALITY2:
		// stringError -> TLMWAIT TIMEconstant IDENTIFIER EQUALITY stringLiteral strError
		case PROD_STRINGERROR_TLMWAIT_TIMECONSTANT_IDENTIFIER_EQUALITY3:
		// stringError -> TLMWAIT TIMEconstant IDENTIFIER EQUALITY constant strError
		case PROD_STRINGERROR_TLMWAIT_TIMECONSTANT_IDENTIFIER_EQUALITY4:
		// stringError -> TLMWAIT TIMEconstant IDENTIFIER EQUALITY variable strError
		case PROD_STRINGERROR_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY:
		// stringError -> TLMWAIT INTEGERconstant IDENTIFIER ( IDENTIFIER ) EQUALITY strError
		case PROD_STRINGERROR_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY2:
		// stringError -> TLMWAIT INTEGERconstant IDENTIFIER ( IDENTIFIER ) EQUALITY stringLiteral strError
		case PROD_STRINGERROR_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY3:
		// stringError -> TLMWAIT INTEGERconstant IDENTIFIER ( IDENTIFIER ) EQUALITY constant strError
		case PROD_STRINGERROR_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY4:
		// stringError -> TLMWAIT INTEGERconstant IDENTIFIER ( IDENTIFIER ) EQUALITY variable strError
		case PROD_STRINGERROR_TLMWAIT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY:
		// stringError -> TLMWAIT variable IDENTIFIER ( IDENTIFIER ) EQUALITY strError
		case PROD_STRINGERROR_TLMWAIT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY2:
		// stringError -> TLMWAIT variable IDENTIFIER ( IDENTIFIER ) EQUALITY stringLiteral strError
		case PROD_STRINGERROR_TLMWAIT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY3:
		// stringError -> TLMWAIT variable IDENTIFIER ( IDENTIFIER ) EQUALITY constant strError
		case PROD_STRINGERROR_TLMWAIT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY4:
		// stringError -> TLMWAIT variable IDENTIFIER ( IDENTIFIER ) EQUALITY variable strError
		case PROD_STRINGERROR_TLMWAIT_TIMECONSTANT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY:
		// stringError -> TLMWAIT TIMEconstant IDENTIFIER ( IDENTIFIER ) EQUALITY strError
		case PROD_STRINGERROR_TLMWAIT_TIMECONSTANT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY2:
		// stringError -> TLMWAIT TIMEconstant IDENTIFIER ( IDENTIFIER ) EQUALITY stringLiteral strError
		case PROD_STRINGERROR_TLMWAIT_TIMECONSTANT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY3:
		// stringError -> TLMWAIT TIMEconstant IDENTIFIER ( IDENTIFIER ) EQUALITY constant strError
		case PROD_STRINGERROR_TLMWAIT_TIMECONSTANT_IDENTIFIER_LPAREN_IDENTIFIER_RPAREN_EQUALITY4:
		// stringError -> TLMWAIT TIMEconstant IDENTIFIER ( IDENTIFIER ) EQUALITY variable strError
		m_nErrorNum = IDS_SYNTAX_ERROR20;
		m_strErrorMessage.Format(IDS_SYNTAX_ERROR20);
		parseError(*(elementFromProduction(0)->lexeme()));
		break;

		case PROD_EXPRESSIONSTATEMENT:
		// expressionStatement -> commaExpressionOpt eol
			CheckCommaExp(0);
			break;

		case PROD_COMMAEXPRESSIONOPT:
		// commaExpressionOpt -> commaExpression
		case PROD_COMMAEXPRESSION:
		// commaExpression -> assignmentExpression
		{
					SSYaccStackElement* z_pRet = GetStackElement(0);
					if( z_pRet != NULL ) return z_pRet;
					break;
		}

		case PROD_ASSIGNMENTEXPRESSION:
		// assignmentExpression -> conditionalExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
				break;
		}

		case PROD_ASSIGNMENTEXPRESSION_EQ:
		// assignmentExpression -> unaryExpression = assignmentExpression
			CheckAssignExp(0, 1, 2);
			break;

		case PROD_CONDITIONALEXPRESSION:
		// conditionalExpression -> logicalOrExpression
		case PROD_LOGICALOREXPRESSION:
		// logicalOrExpression -> logicalAndExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_LOGICALOREXPRESSION_OR:
		// logicalOrExpression -> logicalOrExpression OR logicalAndExpression
			CheckVarDef(0);
			CheckVarDef(2);
			break;

		case PROD_LOGICALANDEXPRESSION:
		// logicalAndExpression -> inclusiveOrExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_LOGICALANDEXPRESSION_AND:
		// logicalAndExpression -> logicalAndExpression AND inclusiveOrExpression
			CheckVarDef(0);
			CheckVarDef(2);
			break;

		case PROD_INCLUSIVEOREXPRESSION:
		// inclusiveOrExpression -> exclusiveOrExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_INCLUSIVEOREXPRESSION_PIPE:
		// inclusiveOrExpression -> inclusiveOrExpression | exclusiveOrExpression
			CheckVarDef(0);
			CheckVarDef(2);
			break;

		case PROD_EXCLUSIVEOREXPRESSION:
		// exclusiveOrExpression -> AndExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_EXCLUSIVEOREXPRESSION_XOR:
		// exclusiveOrExpression -> exclusiveOrExpression XOR AndExpression
			CheckVarDef(0);
			CheckVarDef(2);
			break;

		case PROD_ANDEXPRESSION:
		// AndExpression -> equalityExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_ANDEXPRESSION_AMP:
		// AndExpression -> AndExpression & equalityExpression
			CheckVarDef(0);
			CheckVarDef(2);
			break;

		case PROD_EQUALITYEXPRESSION:
		// equalityExpression -> relationalExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_EQUALITYEXPRESSION_EQ:
		// equalityExpression -> equalityExpression EQ relationalExpression
		case PROD_EQUALITYEXPRESSION_NE:
		// equalityExpression -> equalityExpression NE relationalExpression
			CheckVarDef(0);
			CheckVarDef(2);
		break;

		case PROD_RELATIONALEXPRESSION:
		// relationalExpression -> shiftExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_RELATIONALEXPRESSION_LT:
		// relationalExpression -> relationalExpression LT shiftExpression
		case PROD_RELATIONALEXPRESSION_GT:
		// relationalExpression -> relationalExpression GT shiftExpression
		case PROD_RELATIONALEXPRESSION_LE:
		// relationalExpression -> relationalExpression LE shiftExpression
		case PROD_RELATIONALEXPRESSION_GE:
		// relationalExpression -> relationalExpression GE shiftExpression
			CheckVarDef(0);
			CheckVarDef(2);
		break;

		case PROD_SHIFTEXPRESSION:
		// shiftExpression -> additiveExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_SHIFTEXPRESSION_LS:
		// shiftExpression -> shiftExpression LS additiveExpression
		case PROD_SHIFTEXPRESSION_RS:
		// shiftExpression -> shiftExpression RS additiveExpression
			CheckVarDef(0);
			CheckVarDef(2);
		break;

		case PROD_ADDITIVEEXPRESSION:
		// additiveExpression -> multiplicativeExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_ADDITIVEEXPRESSION_PLUS:
		// additiveExpression -> additiveExpression + multiplicativeExpression
		case PROD_ADDITIVEEXPRESSION_MINUS:
		// additiveExpression -> additiveExpression - multiplicativeExpression
			CheckVarDef(0);
			CheckVarDef(2);
		break;

		case PROD_MULTIPLICATIVEEXPRESSION:
		// multiplicativeExpression -> castExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_MULTIPLICATIVEEXPRESSION_TIMES:
		// multiplicativeExpression -> multiplicativeExpression * castExpression
		case PROD_MULTIPLICATIVEEXPRESSION_DIV:
		// multiplicativeExpression -> multiplicativeExpression / castExpression
		case PROD_MULTIPLICATIVEEXPRESSION_TIMESTIMES:
		// multiplicativeExpression -> multiplicativeExpression ** castExpression
			CheckVarDef(0);
			CheckVarDef(2);
		break;

		case PROD_CASTEXPRESSION:
		// castExpression -> unaryExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_UNARYEXPRESSION:
		// unaryExpression -> postfixExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_ARGUMENTEXPRLIST:
		// argumentExprList -> assignmentExpression
		{
			SSYaccStackElement* z_pRet = AddVarToList(0, TRUE);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_ARGUMENTEXPRLIST_COMMA:
		// argumentExprList -> argumentExprList , assignmentExpression
			AddVarToList(2, FALSE);
			break;

		case PROD_POSTFIXEXPRESSION:
		// postfixExpression -> primaryExpression
		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
			break;
		}

		case PROD_POSTFIXEXPRESSION_LPAREN_RPAREN2:
		// postfixExpression -> postfixExpression ( argumentExprList )
			CheckArgExpLst(0);
			break;

		case PROD_PRIMARYEXPRESSION_IDENTIFIER:
		// primaryExpression -> IDENTIFIER
		{
			SSYaccStackElement* z_pRet		= stackElement();
			SSLexLexeme*		z_pLexeme	= elementFromProduction(0)->lexeme();
			z_pRet->setLexeme(*z_pLexeme);
			return z_pRet;
		}

		case PROD_IDENTIFIERLIST_IDENTIFIER2:
		// identifierList -> IDENTIFIER
			LoadIdListElements(0);
			break;

		case PROD_IDENTIFIERLIST_IDENTIFIER:
		// identifierList -> identifierList IDENTIFIER
			LoadIdListElements(1);
			break;

		case PROD_VARIABLE_DOLLAR_IDENTIFIER:
		// variable -> $ IDENTIFIER
			CheckVarDef(1);
			break;

		case PROD_LABELSTATEMENT_COLON:
		//  <labelName> ':' <eol>
			GotoLabel(0, _T(""));
			break;

		case PROD_LABELNAME_SETUP:
		case PROD_LABELNAME_IDENTIFIER:
		case PROD_LABELNAME_LOAD:
		{
			// <labelName> ':' <eol>
//			GotoLabel(0, _T(""));
//			break;
//		}
//		case PROD_LABELNAME_IDENTIFIER:
		// <labelName> ::= IDENTIFIER
//		{
			SSYaccStackElement* z_pRet = GetStackElement(0);
			if( z_pRet != NULL ) return z_pRet;
				break;
		}

		case PROD_JUMPSTATEMENT_GOTO:
		// jumpStatement -> GOTO labelname eol
			GotoLabel(1, _T("GOTO"));
			break;

		case PROD_ADDRESSSTATEMENT_ADDRESS_IDENTIFIER_INTEGERCONSTANT:
		// addressStatement -> ADDRESS IDENTIFIER INTEGERconstant eol
		case PROD_ADDRESSSTATEMENT_ADDRESS_IDENTIFIER:
		// addressStatement -> ADDRESS IDENTIFIER variable eol
			CheckToken(1,_T("CTCU"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_ARCHMGRSTATEMENT_ARCH_MGR_FTP_IDENTIFIER:
		// archMgrStatement -> ARCH_MGR FTP IDENTIFIER eol
			CheckToken(2,_T("ENABLE"),_T("DISABLE"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_ARCHMGRSTATEMENT_ARCH_MGR_FTP_RATE_INTEGERCONSTANT:
		// archMgrStatement -> ARCH_MGR FTP RATE INTEGERconstant eol
			CheckRange(3, 1, 24, _T("FTP rate is outside of valid range.") );
			break;

		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_IDENTIFIER:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant IDENTIFIER eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_IDENTIFIER:
		// avctrlStatement -> AVCTRL_MON variable IDENTIFIER eol
			CheckToken(2,_T("ENABLEALLSTREAMS"),_T("DISABLEALLSTREAMS"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_IDENTIFIER_EQ:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant IDENTIFIER = toggle1 eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_IDENTIFIER_EQ:
		// avctrlStatement -> AVCTRL_MON variable IDENTIFIER = toggle1 eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_IDENTIFIER_EQ_IDENTIFIER:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant IDENTIFIER = IDENTIFIER eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_IDENTIFIER_EQ_DOLLAR_IDENTIFIER:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant IDENTIFIER = $ IDENTIFIER eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_IDENTIFIER_EQ_IDENTIFIER:
		// avctrlStatement -> AVCTRL_MON variable IDENTIFIER = IDENTIFIER eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_IDENTIFIER_EQ_DOLLAR_IDENTIFIER:
		// avctrlStatement -> AVCTRL_MON variable IDENTIFIER = $ IDENTIFIER eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_IDENTIFIER_EQ2:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant IDENTIFIER = pathFileOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_IDENTIFIER_EQ2:
		// avctrlStatement -> AVCTRL_MON variable IDENTIFIER = pathFileOpt eol
			CheckToken(2,_T("RANGE"),_T("DSNMON"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_CONFIG:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant CONFIG avctrlOpt avctrlOpt avctrlOpt avctrlOpt avctrlOpt avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_CONFIG2:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant CONFIG avctrlOpt avctrlOpt avctrlOpt avctrlOpt avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_CONFIG3:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant CONFIG avctrlOpt avctrlOpt avctrlOpt avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_CONFIG4:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant CONFIG avctrlOpt avctrlOpt avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_CONFIG5:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant CONFIG avctrlOpt avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_INTEGERCONSTANT_CONFIG6:
		// avctrlStatement -> AVCTRL_MON INTEGERconstant CONFIG avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_CONFIG:
		// avctrlStatement -> AVCTRL_MON variable CONFIG avctrlOpt avctrlOpt avctrlOpt avctrlOpt avctrlOpt avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_CONFIG2:
		// avctrlStatement -> AVCTRL_MON variable CONFIG avctrlOpt avctrlOpt avctrlOpt avctrlOpt avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_CONFIG3:
		// avctrlStatement -> AVCTRL_MON variable CONFIG avctrlOpt avctrlOpt avctrlOpt avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_CONFIG4:
		// avctrlStatement -> AVCTRL_MON variable CONFIG avctrlOpt avctrlOpt avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_CONFIG5:
		// avctrlStatement -> AVCTRL_MON variable CONFIG avctrlOpt avctrlOpt eol
		case PROD_AVCTRLSTATEMENT_AVCTRL_MON_CONFIG6:
		// avctrlStatement -> AVCTRL_MON variable CONFIG avctrlOpt eol
			m_listOptions.RemoveAll();
			break;

		case PROD_AVCTRLOPT_SCID_EQ_INTEGERCONSTANT:
		// avctrlOpt -> SCID = INTEGERconstant
		case PROD_AVCTRLOPT_SCID_EQ:
		// avctrlOpt -> SCID = variable
			if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE) )break;
			m_listOptions.AddTail(_T("SCID"));
			break;

		case PROD_AVCTRLOPT_CMDSRC_EQ_IDENTIFIER:
		// avctrlOpt -> CMDSRC = IDENTIFIER
			if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE) )break;
			m_listOptions.AddTail(_T("CMDSRC"));
			break;

		case PROD_AVCTRLOPT_TLMSRC_EQ_IDENTIFIER:
		// avctrlOpt -> TLMSRC = IDENTIFIER
			if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE) )break;
			m_listOptions.AddTail(_T("TLMSRC"));
			break;

		case PROD_AVCTRLOPT_CMDDEST_EQ_IDENTIFIER:
		// avctrlOpt -> CMDDEST = IDENTIFIER
			if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE) )break;
			m_listOptions.AddTail(_T("CMDDEST"));
			break;

		case PROD_AVCTRLOPT_TLMDEST_EQ_IDENTIFIER:
		// avctrlOpt -> TLMDEST = IDENTIFIER
			if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE) )break;
			m_listOptions.AddTail(_T("TLMDEST"));
			break;

		case PROD_AVCTRLOPT_STATIONID_EQ_IDENTIFIER:
		// avctrlOpt -> STATIONID = IDENTIFIER
			if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE) )break;
			m_listOptions.AddTail(_T("STATIONID"));
			break;

		case PROD_CANCELSTATEMENT_CANCEL_IDENTIFIER:
		// cancelStatement -> CANCEL IDENTIFIER eol
		case PROD_CANCELSTATEMENT_CANCEL_IDENTIFIER_AT_LPAREN_RPAREN:
		// cancelStatement -> CANCEL IDENTIFIER @ ( commaExpression ) eol
			CheckCmdMnemonic(1, FALSE);
			break;

		case PROD_CMDSTATEMENT_CMD:
		// cmdStatement -> CMD decoderAddress cmdMnemonic datawordArgs cmdOptions eol
			CheckCmdStmt(0);
			break;

		case PROD_CMDMNEMONIC_IDENTIFIER:
		// cmdMnemonic -> IDENTIFIER
			CheckCmdMnemonic(0, TRUE);
			break;

		case PROD_KEYWORDVALUE_IDENTIFIER_EQ:
		// keywordValue -> IDENTIFIER = commaExpression
			CheckKeywordValue(0, 2);
			break;

		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_STAR:
		// keywordValue -> IDENTIFIER = STAR
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_SCAN:
		// keywordValue -> IDENTIFIER = SCAN
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_ABORT:
		// keywordValue -> IDENTIFIER = ABORT
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_GROUND:
		// keywordValue -> IDENTIFIER = GROUND
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_RESET:
		// keywordValue -> IDENTIFIER = RESET
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_SET:
		// keywordValue -> IDENTIFIER = SET
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_CANCEL:
		// keywordValue -> IDENTIFIER = CANCEL
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_PENDING:
		// keywordValue -> IDENTIFIER = PENDING
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_ALL:
		// keywordValue -> IDENTIFIER = ALL
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_CLEAR:
		// keywordValue -> IDENTIFIER = CLEAR
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_EARTH:
		// keywordValue -> IDENTIFIER = EARTH
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_EXECUTE:
		// keywordValue -> IDENTIFIER = EXECUTE
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_INIT:
		// keywordValue -> IDENTIFIER = INIT
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_NONE:
		// keywordValue -> IDENTIFIER = NONE
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ_WRITE:
		// keywordValue -> IDENTIFIER = WRITE
		case PROD_KEYWORDVALUE_IDENTIFIER_EQ2:
		// keywordValue -> IDENTIFIER = toggle1
			CheckKeywordValue(0, 2);
			break;

		case PROD_KEYWORDVALUE_IDENTIFIER2:
		// keywordValue -> IDENTIFIER variable
			CheckKeywordValue(0, 1);
			break;

		case PROD_KEYWORDVALUE_IDENTIFIER_STAR:
		// keywordValue -> IDENTIFIER STAR
		case PROD_KEYWORDVALUE_IDENTIFIER_SCAN:
		// keywordValue -> IDENTIFIER SCAN
		case PROD_KEYWORDVALUE_IDENTIFIER_ABORT:
		// keywordValue -> IDENTIFIER ABORT
		case PROD_KEYWORDVALUE_IDENTIFIER_GROUND:
		// keywordValue -> IDENTIFIER GROUND
		case PROD_KEYWORDVALUE_IDENTIFIER_RESET:
		// keywordValue -> IDENTIFIER RESET
		case PROD_KEYWORDVALUE_IDENTIFIER_SET:
		// keywordValue -> IDENTIFIER SET
		case PROD_KEYWORDVALUE_IDENTIFIER_CANCEL:
		// keywordValue -> IDENTIFIER CANCEL
		case PROD_KEYWORDVALUE_IDENTIFIER_PENDING:
		// keywordValue -> IDENTIFIER PENDING
		case PROD_KEYWORDVALUE_IDENTIFIER_ALL:
		// keywordValue -> IDENTIFIER ALL
		case PROD_KEYWORDVALUE_IDENTIFIER_CLEAR:
		// keywordValue -> IDENTIFIER CLEAR
		case PROD_KEYWORDVALUE_IDENTIFIER_EARTH:
		// keywordValue -> IDENTIFIER EARTH
		case PROD_KEYWORDVALUE_IDENTIFIER_EXECUTE:
		// keywordValue -> IDENTIFIER EXECUTE
		case PROD_KEYWORDVALUE_IDENTIFIER_INIT:
		// keywordValue -> IDENTIFIER INIT
		case PROD_KEYWORDVALUE_IDENTIFIER_NONE:
		// keywordValue -> IDENTIFIER NONE
		case PROD_KEYWORDVALUE_IDENTIFIER_WRITE:
		// keywordValue -> IDENTIFIER WRITE
		case PROD_KEYWORDVALUE_IDENTIFIER3:
		// keywordValue -> IDENTIFIER toggle1
			CheckKeywordValue(0, 1);
			break;

		case PROD_DATAWORDARGS24:
		// datawordArgs -> identifierList
			CheckDatawordArgs(0);
			break;

		case PROD_DATAWORDARGS2:
		// datawordArgs -> positional identifierList
			CheckDatawordArgs(1);
			break;

		case PROD_DATAWORDARGS23:
		// datawordArgs -> identifierList keywordValueList
		case PROD_DATAWORDARGS22:
		// datawordArgs -> identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS21:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS20:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS19:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS18:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS17:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS16:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS15:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS14:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS13:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS12:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS11:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS10:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS9:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS8:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS7:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS6:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS5:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS4:
		// datawordArgs -> identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
			CheckDatawordArgs(0);
			break;

		case PROD_DATAWORDARGS44:
		// datawordArgs -> keywordValueList identifierList
		case PROD_DATAWORDARGS43:
		// datawordArgs -> keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS42:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS41:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS40:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS39:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS38:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS37:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS36:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS35:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS34:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS33:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS32:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS31:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS30:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS29:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS28:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS27:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
		case PROD_DATAWORDARGS26:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList
		case PROD_DATAWORDARGS25:
		// datawordArgs -> keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList identifierList keywordValueList
			CheckDatawordArgs(1);
			break;

		case PROD_CVSTATEMENT_CV_IDENTIFIER:
		// cvStatement -> CV IDENTIFIER eol
			CheckToken(1,_T("ENABLE"),_T("DISABLE"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_DBASESTATEMENT_DBASE_MODIFY_LOCAL_IDENTIFIER_IDENTIFIER:
		// dbaseStatement -> DBASE MODIFY LOCAL IDENTIFIER IDENTIFIER constantList eol
		case PROD_DBASESTATEMENT_DBASE_MODIFY_LOCAL_IDENTIFIER_IDENTIFIER2:
		// dbaseStatement -> DBASE MODIFY LOCAL IDENTIFIER IDENTIFIER variableList eol
			FindTlmPoint(3, FALSE, IDS_SYNTAX_ERROR6);
			CheckList(4, &m_listDbModType, IDS_SYNTAX_ERROR18, FALSE);
			break;

		case PROD_DBASESTATEMENT_DBASE_MODIFY_LOCAL_IDENTIFIER_RECORD_INTEGERCONSTANT_IDENTIFIER:
		// dbaseStatement -> DBASE MODIFY LOCAL IDENTIFIER RECORD INTEGERconstant IDENTIFIER constantList eol
		case PROD_DBASESTATEMENT_DBASE_MODIFY_LOCAL_IDENTIFIER_RECORD_INTEGERCONSTANT_IDENTIFIER2:
		// dbaseStatement -> DBASE MODIFY LOCAL IDENTIFIER RECORD INTEGERconstant IDENTIFIER variableList eol
			FindTlmPoint(3, FALSE, IDS_SYNTAX_ERROR6);
			CheckList(6, &m_listDbModType, IDS_SYNTAX_ERROR18, FALSE);
			break;

		case PROD_DBASESTATEMENT_DBASE_MODIFY_GLOBAL_IDENTIFIER_IDENTIFIER:
		// dbaseStatement -> DBASE MODIFY GLOBAL IDENTIFIER IDENTIFIER constantList eol
		case PROD_DBASESTATEMENT_DBASE_MODIFY_GLOBAL_IDENTIFIER_IDENTIFIER2:
		// dbaseStatement -> DBASE MODIFY GLOBAL IDENTIFIER IDENTIFIER variableList eol
			FindTlmPoint(3, FALSE, IDS_SYNTAX_ERROR6);
			CheckList(4, &m_listDbModType, IDS_SYNTAX_ERROR18, FALSE);
			break;

		case PROD_DBASESTATEMENT_DBASE_MODIFY_GLOBAL_IDENTIFIER_RECORD_INTEGERCONSTANT_IDENTIFIER:
		// dbaseStatement -> DBASE MODIFY GLOBAL IDENTIFIER RECORD INTEGERconstant IDENTIFIER constantList eol
		case PROD_DBASESTATEMENT_DBASE_MODIFY_GLOBAL_IDENTIFIER_RECORD_INTEGERCONSTANT_IDENTIFIER2:
		// dbaseStatement -> DBASE MODIFY GLOBAL IDENTIFIER RECORD INTEGERconstant IDENTIFIER variableList eol
			FindTlmPoint(3, FALSE, IDS_SYNTAX_ERROR6);
			CheckList(6, &m_listDbModType, IDS_SYNTAX_ERROR18, FALSE);
			break;

		case PROD_DBASESTATEMENT_DBASE_MODIFY_RESET_IDENTIFIER_IDENTIFIER:
		// dbaseStatement -> DBASE MODIFY RESET IDENTIFIER IDENTIFIER constantList eol
		case PROD_DBASESTATEMENT_DBASE_MODIFY_RESET_IDENTIFIER_IDENTIFIER2:
		// dbaseStatement -> DBASE MODIFY RESET IDENTIFIER IDENTIFIER variableList eol
			FindTlmPoint(3, FALSE, IDS_SYNTAX_ERROR6);
			CheckList(4, &m_listDbModType, IDS_SYNTAX_ERROR18, FALSE);
			break;

		case PROD_DBASESTATEMENT_DBASE_MODIFY_RESET_IDENTIFIER_RECORD_INTEGERCONSTANT_IDENTIFIER:
		// dbaseStatement -> DBASE MODIFY RESET IDENTIFIER RECORD INTEGERconstant IDENTIFIER constantList eol
		case PROD_DBASESTATEMENT_DBASE_MODIFY_RESET_IDENTIFIER_RECORD_INTEGERCONSTANT_IDENTIFIER2:
		// dbaseStatement -> DBASE MODIFY RESET IDENTIFIER RECORD INTEGERconstant IDENTIFIER variableList eol
			FindTlmPoint(3, FALSE, IDS_SYNTAX_ERROR6);
			CheckList(6, &m_listDbModType, IDS_SYNTAX_ERROR18, FALSE);
			break;

		case PROD_DISPLAYSTATEMENT_DISPLAY_POINT_IDENTIFIER:
		// displayStatement -> DISPLAY POINT IDENTIFIER eol
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR3);
			break;


		case PROD_EVENTSTATEMENT_EVENT_ALARM_IDENTIFIER_INTEGERCONSTANT:
		// eventStatement -> EVENT ALARM IDENTIFIER INTEGERconstant eol
		case PROD_EVENTSTATEMENT_EVENT_ALARM_IDENTIFIER:
		// eventStatement -> EVENT ALARM IDENTIFIER variable eol
			CheckToken(2,_T("COUNT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_EVENTSTATEMENT_EVENT_IDENTIFIER:
		// eventStatement -> EVENT toggle1 IDENTIFIER eol
			FindTlmPoint(2, TRUE, IDS_SYNTAX_ERROR12);
			break;

		case PROD_GVSTATEMENT_GV_SET_IDENTIFIER:
		// gvStatement -> GV SET IDENTIFIER commaExpression eol
		case PROD_GVSTATEMENT_GV_VALUE_IDENTIFIER:
		// gvStatement -> GV VALUE IDENTIFIER commaExpression eol
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR22);
			break;

		case PROD_GVSTATEMENT_GV_IDENTIFIER_IDENTIFIER:
		// gvStatement -> GV IDENTIFIER IDENTIFIER commaExpression eol
			CheckToken(1,_T("STATE"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR22);
			break;

		case PROD_HELPSTATEMENT_HELP_CMD_IDENTIFIER:
		// helpStatement -> HELP CMD IDENTIFIER eol
			CheckCmdMnemonic(2, FALSE);
			break;

		case PROD_HELPSTATEMENT_HELP_TLM_IDENTIFIER:
		// helpStatement -> HELP TLM IDENTIFIER eol
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR3);
			break;

		case PROD_INITSTATEMENT_INIT_PLAYBACK_IDENTIFIER:
		// initStatement -> INIT PLAYBACK IDENTIFIER pathFileOpt eol
		case PROD_INITSTATEMENT_INIT_PLAYBACK_IDENTIFIER_IDENTIFIER:
		// initStatement -> INIT PLAYBACK IDENTIFIER IDENTIFIER eol
		case PROD_INITSTATEMENT_INIT_PLAYBACK_IDENTIFIER_DOLLAR_IDENTIFIER:
		// initStatement -> INIT PLAYBACK IDENTIFIER $ IDENTIFIER eol
		case PROD_INITSTATEMENT_INIT_SIMULATION_IDENTIFIER_DBASE:
		// initStatement -> INIT SIMULATION IDENTIFIER DBASE pathFileOpt eol
		case PROD_INITSTATEMENT_INIT_SIMULATION_IDENTIFIER_DBASE_IDENTIFIER:
		// initStatement -> INIT SIMULATION IDENTIFIER DBASE IDENTIFIER eol
		case PROD_INITSTATEMENT_INIT_SIMULATION_IDENTIFIER_DBASE_DOLLAR_IDENTIFIER:
		// initStatement -> INIT SIMULATION IDENTIFIER DBASE $ IDENTIFIER eol
		case PROD_INITSTATEMENT_INIT_REALTIME_IDENTIFIER_DBASE:
		// initStatement -> INIT REALTIME IDENTIFIER DBASE pathFileOpt eol
		case PROD_INITSTATEMENT_INIT_REALTIME_IDENTIFIER_DBASE_IDENTIFIER:
		// initStatement -> INIT REALTIME IDENTIFIER DBASE IDENTIFIER eol
		case PROD_INITSTATEMENT_INIT_REALTIME_IDENTIFIER_DBASE_DOLLAR_IDENTIFIER:
		// initStatement -> INIT REALTIME IDENTIFIER DBASE $ IDENTIFIER eol
			FindStream(2, FALSE, NULL);
			break;

		case PROD_INITSTATEMENT_INIT_REALTIME_IDENTIFIER_DBASE_BACKUP_IDENTIFIER:
		// initStatement -> INIT REALTIME IDENTIFIER DBASE pathFileOpt BACKUP IDENTIFIER eol
		case PROD_INITSTATEMENT_INIT_REALTIME_IDENTIFIER_DBASE_IDENTIFIER_BACKUP_IDENTIFIER:
		// initStatement -> INIT REALTIME IDENTIFIER DBASE IDENTIFIER BACKUP IDENTIFIER eol
			FindStream(2, FALSE, NULL);
			FindHostName(6);
			break;

		case PROD_INITSTATEMENT_INIT_REALTIME_IDENTIFIER_DBASE_DOLLAR_IDENTIFIER_BACKUP_IDENTIFIER:
		// initStatement -> INIT REALTIME IDENTIFIER DBASE $ IDENTIFIER BACKUP IDENTIFIER eol
			FindStream(2, FALSE, NULL);
			FindHostName(7);
			break;

		case PROD_INITSTATEMENT_INIT_VIEWER:
		// initStatement -> INIT VIEWER identifierList eol
			TravIdList(2);
			break;

		case PROD_LIMITSSTATEMENT_LIMITS_IDENTIFIER:
		// limitsStatement -> LIMITS IDENTIFIER toggle1 eol
			FindTlmPoint(1, FALSE, IDS_SYNTAX_ERROR3);
			break;



		case PROD_PLAYBACKSTATEMENT_PLAYBACK_IDENTIFIER:
		// playbackStatement -> PLAYBACK IDENTIFIER eol
			CheckToken(1,_T("STATUS"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_PSEUDOSTATEMENT_PSEUDO_SET_IDENTIFIER:
		// pseudoStatement -> PSEUDO SET IDENTIFIER constant eol
		case PROD_PSEUDOSTATEMENT_PSEUDO_SET_IDENTIFIER2:
		// pseudoStatement -> PSEUDO SET IDENTIFIER variable eol
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR3);
			break;

		case PROD_SCHEDULESTATEMENT_AT_IDENTIFIER_SCHEDULE_GOTO_IDENTIFIER:
		// scheduleStatement -> @ IDENTIFIER SCHEDULE GOTO IDENTIFIER eol
			FindStream(1, FALSE, NULL);
			break;

		case PROD_SCHEDULESTATEMENT_AT_IDENTIFIER_SCHEDULE_CONTINUE:
		// scheduleStatement -> @ IDENTIFIER SCHEDULE CONTINUE eol
			FindStream(1, FALSE, NULL);
			break;

		case PROD_SIMSTATEMENT_SIM_IDENTIFIER_IDENTIFIER:
		// simStatement -> SIM IDENTIFIER IDENTIFIER eol
			CheckToken(1,_T("ENABLE"),_T("INHIBIT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR3);
			break;

		case PROD_SIMSTATEMENT_SIM_IDENTIFIER_TIMES:
		// simStatement -> SIM IDENTIFIER * eol
			CheckToken(1,_T("ENABLE"),_T("INHIBIT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_SINGLETONID_CONNECT_IDENTIFIER:
		// singletonId -> CONNECT IDENTIFIER eol
		case PROD_SINGLETONID_DISCONNECT_IDENTIFIER:
		// singletonId -> DISCONNECT IDENTIFIER eol
			FindHostName(2);
			break;

		case PROD_SINGLETONID_LOOK_IDENTIFIER:
		// singletonId -> LOOK IDENTIFIER eol
			FindTlmPoint(1, FALSE, IDS_SYNTAX_ERROR3);
			break;

		case PROD_SINGLETONID_TERM_IDENTIFIER:
		// singletonId -> TERM IDENTIFIER eol
		case PROD_SINGLETONID_WAITON_IDENTIFIER:
		// singletonId -> WAITON IDENTIFIER eol
		case PROD_SINGLETONID_STREAM_IDENTIFIER:
		// singletonId -> STREAM IDENTIFIER eol
			FindStream(1, FALSE, NULL);
			break;

		case PROD_SINGLETONID_TIMEDCMD_IDENTIFIER:
		// singletonId -> TIMEDCMD IDENTIFIER eol
			CheckToken(1,_T("ENABLE"),_T("INHIBIT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_TLMSTATEMENT_TLM_SET_IDENTIFIER_IDENTIFIER:
		// tlmStatement -> TLM SET IDENTIFIER IDENTIFIER constant eol
		case PROD_TLMSTATEMENT_TLM_SET_IDENTIFIER_IDENTIFIER2:
		// tlmStatement -> TLM SET IDENTIFIER IDENTIFIER variable eol
			CheckToken(2,_T("EU"),_T("SC"),_T("BA"),_T("RAW"),_T("POLY"),_T("LIMIT"),_T("STATE"),_T("LINEAR"));
			FindTlmPoint(3, FALSE, IDS_SYNTAX_ERROR3);
			break;

		case PROD_TLMSTATEMENT_TLM_IDENTIFIER_IDENTIFIER:
		// tlmStatement -> TLM IDENTIFIER IDENTIFIER eol
			CheckToken(1,_T("ENABLE"),_T("INHIBIT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR3);
			break;

		case PROD_TLMSTATEMENT_TLM_IDENTIFIER_TIMES:
		// tlmStatement -> TLM IDENTIFIER * eol
			CheckToken(1,_T("ENABLE"),_T("INHIBIT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_TRIGGERSTATEMENT_TRIGGER_IDENTIFIER:
		// triggerStatement -> TRIGGER IDENTIFIER eol
			CheckToken(1,_T("ENABLE"),_T("DISABLE"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		// case RulesYaccProdTriggerDeleteAll:
		// triggerStatement -> TRIGGER DELETE ALL eol
		//	break;

		case PROD_TRIGGERSTATEMENT_TRIGGER_DELETE_ALL:
		// triggerStatement -> TRIGGER DELETE POINT IDENTIFIER eol
			FindTlmPoint(3, FALSE, IDS_SYNTAX_ERROR5);
			break;

		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_IDENTIFIER_IDENTIFIER_PROC:
		// triggerStatement -> TRIGGER POINT IDENTIFIER IDENTIFIER IDENTIFIER PROC commaExpression eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_IDENTIFIER_IDENTIFIER_PROC2:
		// triggerStatement -> TRIGGER POINT IDENTIFIER IDENTIFIER IDENTIFIER PROC pathFileOpt eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_IDENTIFIER_IDENTIFIER_PROC_LPAREN_RPAREN2:
		// triggerStatement -> TRIGGER POINT IDENTIFIER IDENTIFIER IDENTIFIER PROC pathFileOpt ( argumentExprList ) eol
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR5);
			CheckToken(3,_T("LIMIT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			CheckToken(4,_T("REDH"),_T("REDL"),_T("WARN"),_T("ERROR"),_T("YELLOWH"),_T("YELLOWL"),_T(" "),_T(" "));
			break;

		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_INSIDE_PROC:
		// triggerStatement -> TRIGGER POINT IDENTIFIER INSIDE constant constant PROC commaExpression eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_INSIDE_PROC2:
		// triggerStatement -> TRIGGER POINT IDENTIFIER INSIDE constant constant PROC pathFileOpt eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_IDENTIFIER_IDENTIFIER_PROC_LPAREN_RPAREN:
		// triggerStatement -> TRIGGER POINT IDENTIFIER INSIDE constant constant PROC pathFileOpt ( argumentExprList ) eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_INSIDE_PROC3:
		// triggerStatement -> TRIGGER POINT IDENTIFIER INSIDE variable variable PROC commaExpression eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_INSIDE_PROC4:
		// triggerStatement -> TRIGGER POINT IDENTIFIER INSIDE variable variable PROC pathFileOpt eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_INSIDE_PROC_LPAREN_RPAREN3:
		// triggerStatement -> TRIGGER POINT IDENTIFIER INSIDE variable variable PROC pathFileOpt ( argumentExprList ) eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_OUTSIDE_PROC:
		// triggerStatement -> TRIGGER POINT IDENTIFIER OUTSIDE constant constant PROC commaExpression eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_OUTSIDE_PROC2:
		// triggerStatement -> TRIGGER POINT IDENTIFIER OUTSIDE constant constant PROC pathFileOpt eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_OUTSIDE_PROC_LPAREN_RPAREN:
		// triggerStatement -> TRIGGER POINT IDENTIFIER OUTSIDE constant constant PROC pathFileOpt ( argumentExprList ) eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_OUTSIDE_PROC3:
		// triggerStatement -> TRIGGER POINT IDENTIFIER OUTSIDE variable variable PROC commaExpression eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_OUTSIDE_PROC4:
		// triggerStatement -> TRIGGER POINT IDENTIFIER OUTSIDE variable variable PROC pathFileOpt eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_POINT_IDENTIFIER_OUTSIDE_PROC_LPAREN_RPAREN3:
		// triggerStatement -> TRIGGER POINT IDENTIFIER OUTSIDE variable variable PROC pathFileOpt ( argumentExprList ) eol
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR5);
			break;

		case PROD_TRIGGERSTATEMENT_TRIGGER_RELOAD_POINT_IDENTIFIER_IDENTIFIER_IDENTIFIER_PROC:
		// triggerStatement -> TRIGGER RELOAD POINT IDENTIFIER IDENTIFIER IDENTIFIER PROC commaExpression eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_RELOAD_POINT_IDENTIFIER_IDENTIFIER_IDENTIFIER_PROC2:
		// triggerStatement -> TRIGGER RELOAD POINT IDENTIFIER IDENTIFIER IDENTIFIER PROC pathFileOpt eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_RELOAD_POINT_IDENTIFIER_IDENTIFIER_IDENTIFIER_PROC_LPAREN_RPAREN:
		// triggerStatement -> TRIGGER RELOAD POINT IDENTIFIER IDENTIFIER IDENTIFIER PROC pathFileOpt ( argumentExprList ) eol
			FindTlmPoint(3, FALSE, IDS_SYNTAX_ERROR5);
			CheckToken(4,_T("LIMIT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			CheckToken(5,_T("REDH"),_T("REDL"),_T("WARN"),_T("ERROR"),_T("YELLOWH"),_T("YELLOWL"),_T(" "),_T(" "));
			break;

		case PROD_TRIGGERSTATEMENT_TRIGGER_RELOAD_POINT_IDENTIFIER_OUTSIDE_PROC3:
		// triggerStatement -> TRIGGER RELOAD POINT IDENTIFIER OUTSIDE variable variable PROC commaExpression eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_RELOAD_POINT_IDENTIFIER_OUTSIDE_PROC4:
		// triggerStatement -> TRIGGER RELOAD POINT IDENTIFIER OUTSIDE variable variable PROC pathFileOpt eol
		case PROD_TRIGGERSTATEMENT_TRIGGER_RELOAD_POINT_IDENTIFIER_OUTSIDE_PROC_LPAREN_RPAREN3:
		// triggerStatement -> TRIGGER RELOAD POINT IDENTIFIER OUTSIDE variable variable PROC pathFileOpt ( argumentExprList ) eol
			FindTlmPoint(3, FALSE, IDS_SYNTAX_ERROR5);
			break;

		case PROD_TVSTATEMENT_TV_ACTIVATE_IDENTIFIER:
		// tvStatement -> TV ACTIVATE IDENTIFIER eol
		case PROD_TVSTATEMENT_TV_CLEAR_IDENTIFIER:
		// tvStatement -> TV CLEAR IDENTIFIER eol
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR3);
			break;

		case PROD_TVSTATEMENT_TV_IDENTIFIER:
		// tvStatement -> TV IDENTIFIER eol
			CheckToken(1,_T("ENABLE"),_T("DISABLE"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_CEMSTATEMENT_CEM_INTEGERCONSTANT_IDENTIFIER:
		// cemStatement -> CEM INTEGERconstant IDENTIFIER eol
			CheckToken(2,ctstrNORMAL,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_CEMSTATEMENT_CEM_IDENTIFIER:
		// cemStatement -> CEM variable IDENTIFIER eol
			CheckToken(2,ctstrNORMAL,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_CEMMONSTATEMENT_CEMMON_INTEGERCONSTANT_IDENTIFIER:
		// cemmonStatement -> CEMMON INTEGERconstant IDENTIFIER eol
			CheckToken(2,ctstrNORMAL,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_CEMMONSTATEMENT_CEMMON_IDENTIFIER:
		// cemmonStatement -> CEMMON variable IDENTIFIER eol
			CheckToken(2,ctstrNORMAL,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_CFGCMDSTATEMENT_CFGCMD_IDENTIFIER:
		// cfgcmdStatement -> CFGCMD IDENTIFIER eol
			CheckList(1, &m_listCmdDest, IDS_SYNTAX_ERROR23, FALSE);
			break;

		case PROD_CFGTLMSTATEMENT_CFGTLM_IDENTIFIER_IDENTIFIER:
		// cfgTlmStatement -> CFGTLM IDENTIFIER IDENTIFIER eol
			CheckCfgTlm(1, 2, TRUE);
			break;

		case PROD_CFGTLMSTATEMENT_CFGTLM_IDENTIFIER:
		// cfgTlmStatement -> CFGTLM IDENTIFIER eol
		case PROD_CFGTLMSTATEMENT_CFGTLM_IDENTIFIER_CLEAR:
		// cfgTlmStatement -> CFGTLM IDENTIFIER CLEAR eol
			CheckCfgTlm(1, 0, FALSE);
			break;

		case PROD_CGISTATEMENT_CGI_IDENTIFIER:
		// cgiStatement -> CGI memOpt IDENTIFIER eol
		case PROD_CGISTATEMENT_CGI_IDENTIFIER2:
		// cgiStatement -> CGI memOpt IDENTIFIER fromOpt eol
			CheckToken(2,_T("ACE1"),_T("ACE2"),_T("SXI"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_CHKPTSTATEMENT_CHKPT_IMAGE_IDENTIFIER:
		// chkptStatement -> CHKPT IMAGE IDENTIFIER toggle1 eol
			CheckToken(2,_T("ACE1"),_T("ACE2"),_T("SXI"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_CLOADSTATEMENT_CLOAD_IDENTIFIER_IDENTIFIER:
		// cloadStatement -> CLOAD IDENTIFIER IDENTIFIER eol
		case PROD_CLOADSTATEMENT_CLOAD_IDENTIFIER_IDENTIFIER_INTEGERCONSTANT:
		// cloadStatement -> CLOAD IDENTIFIER IDENTIFIER INTEGERconstant eol
		case PROD_CLOADSTATEMENT_CLOAD_IDENTIFIER_IDENTIFIER2:
		// cloadStatement -> CLOAD IDENTIFIER IDENTIFIER variable eol
			CheckToken(1,_T("GLOBALS"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			CheckToken(2,_T("UPDATE"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_CLOADSTATEMENT_CLOAD_IDENTIFIER_CLEAR_IDENTIFIER_INTEGERCONSTANT:
		// cloadStatement -> CLOAD IDENTIFIER CLEAR IDENTIFIER INTEGERconstant eol
		case PROD_CLOADSTATEMENT_CLOAD_IDENTIFIER_CLEAR_IDENTIFIER:
		// cloadStatement -> CLOAD IDENTIFIER CLEAR IDENTIFIER variable eol
		case PROD_CLOADSTATEMENT_CLOAD_IDENTIFIER_CLEAR_IDENTIFIER_INTEGERCONSTANT_INTEGERCONSTANT:
		// cloadStatement -> CLOAD IDENTIFIER CLEAR IDENTIFIER INTEGERconstant INTEGERconstant eol
		case PROD_CLOADSTATEMENT_CLOAD_IDENTIFIER_CLEAR_IDENTIFIER_INTEGERCONSTANT2:
		// cloadStatement -> CLOAD IDENTIFIER CLEAR IDENTIFIER variable INTEGERconstant eol
		case PROD_CLOADSTATEMENT_CLOAD_IDENTIFIER_CLEAR_IDENTIFIER_INTEGERCONSTANT3:
		// cloadStatement -> CLOAD IDENTIFIER CLEAR IDENTIFIER INTEGERconstant variable eol
		case PROD_CLOADSTATEMENT_CLOAD_IDENTIFIER_CLEAR_IDENTIFIER2:
		// cloadStatement -> CLOAD IDENTIFIER CLEAR IDENTIFIER variable variable eol
			CheckToken(1,_T("GLOBALS"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			CheckToken(3,_T("SKB"),_T("IMC"),_T("RAM"),_T("STO"),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_CXRNGSTATEMENT_CXRNG_INTEGERCONSTANT_IDENTIFIER_IDENTIFIER:
		// cxrngStatement -> CXRNG INTEGERconstant IDENTIFIER IDENTIFIER eol
			CheckToken(2,ctstrNORMAL,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_CXRNGSTATEMENT_CXRNG_IDENTIFIER_IDENTIFIER:
		// cxrngStatement -> CXRNG variable IDENTIFIER IDENTIFIER eol
			CheckToken(2,ctstrNORMAL,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_DERTLMSTATEMENT_DERTLM_POINT_IDENTIFIER_IDENTIFIER:
		// derTlmStatement -> DERTLM POINT IDENTIFIER IDENTIFIER eol
			CheckToken(2,_T("ENABLE"),_T("DISABLE"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_DERTLMSTATEMENT_DERTLM_POINT_IDENTIFIER_DOLLAR_IDENTIFIER:
		// derTlmStatement -> DERTLM POINT IDENTIFIER $ IDENTIFIER eol
			CheckToken(2,_T("ENABLE"),_T("DISABLE"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			FindTlmPoint(4, FALSE, IDS_SYNTAX_ERROR3);
			break;

		case PROD_DERTLMSTATEMENT_DERTLM_GROUP_IDENTIFIER_IDENTIFIER:
		// derTlmStatement -> DERTLM GROUP IDENTIFIER IDENTIFIER eol
			CheckToken(2,_T("ENABLE"),_T("DISABLE"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_ENCRYPTSTATEMENT_ENCRYPT_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT toggle1 IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_IDENTIFIER:
		// encryptStatement -> ENCRYPT toggle1 IDENTIFIER variable eol
			CheckToken(2,_T("CTCU"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SYNC_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT SYNC IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SYNC_IDENTIFIER:
		// encryptStatement -> ENCRYPT SYNC IDENTIFIER variable eol
			CheckToken(2,_T("CTCU"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_VCC_INTEGERCONSTANT_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT SET VCC INTEGERconstant IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_VCC_INTEGERCONSTANT_IDENTIFIER:
		// encryptStatement -> ENCRYPT SET VCC INTEGERconstant IDENTIFIER variable eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_VCC_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT SET VCC variable IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_VCC_IDENTIFIER:
		// encryptStatement -> ENCRYPT SET VCC variable IDENTIFIER variable eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_KEY_INTEGERCONSTANT_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT SET KEY INTEGERconstant IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_KEY_INTEGERCONSTANT_IDENTIFIER:
		// encryptStatement -> ENCRYPT SET KEY INTEGERconstant IDENTIFIER variable eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_KEY_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT SET KEY variable IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_KEY_IDENTIFIER:
		// encryptStatement -> ENCRYPT SET KEY variable IDENTIFIER variable eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_KEYOFFSET_INTEGERCONSTANT_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT SET KEYOFFSET INTEGERconstant IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_KEYOFFSET_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT SET KEYOFFSET variable IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_KEYOFFSET_INTEGERCONSTANT_IDENTIFIER:
		// encryptStatement -> ENCRYPT SET KEYOFFSET INTEGERconstant IDENTIFIER variable eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_SET_KEYOFFSET_IDENTIFIER:
		// encryptStatement -> ENCRYPT SET KEYOFFSET variable IDENTIFIER variable eol
			CheckToken(4,_T("CTCU"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_ENCRYPTSTATEMENT_ENCRYPT_VCCFILL_INTEGERCONSTANT_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT VCCFILL INTEGERconstant IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_VCCFILL_INTEGERCONSTANT_IDENTIFIER:
		// encryptStatement -> ENCRYPT VCCFILL INTEGERconstant IDENTIFIER variable eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_VCCFILL_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT VCCFILL variable IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_VCCFILL_IDENTIFIER:
		// encryptStatement -> ENCRYPT VCCFILL variable IDENTIFIER variable eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_KEYSELECT_INTEGERCONSTANT_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT KEYSELECT INTEGERconstant IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_KEYSELECT_INTEGERCONSTANT_IDENTIFIER:
		// encryptStatement -> ENCRYPT KEYSELECT INTEGERconstant IDENTIFIER variable eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_KEYSELECT_IDENTIFIER_INTEGERCONSTANT:
		// encryptStatement -> ENCRYPT KEYSELECT variable IDENTIFIER INTEGERconstant eol
		case PROD_ENCRYPTSTATEMENT_ENCRYPT_KEYSELECT_IDENTIFIER:
		// encryptStatement -> ENCRYPT KEYSELECT variable IDENTIFIER variable eol
			CheckToken(3,_T("CTCU"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE pathFileOpt gopt gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE_IDENTIFIER:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE IDENTIFIER gopt gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE_DOLLAR_IDENTIFIER:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE $ IDENTIFIER gopt gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE2:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE pathFileOpt gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE_IDENTIFIER2:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE IDENTIFIER gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE_DOLLAR_IDENTIFIER2:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE $ IDENTIFIER gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE3:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE pathFileOpt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE_IDENTIFIER3:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE IDENTIFIER gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE_DOLLAR_IDENTIFIER3:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE $ IDENTIFIER gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE4:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE pathFileOpt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE_IDENTIFIER4:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE IDENTIFIER gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE_DOLLAR_IDENTIFIER4:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE $ IDENTIFIER gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE5:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE pathFileOpt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE_IDENTIFIER5:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE IDENTIFIER gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_IDENTIFIER_FILE_DOLLAR_IDENTIFIER5:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE IDENTIFIER FILE $ IDENTIFIER gopt eol
		 	m_listOptions.RemoveAll();
		 	break;
			 
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE pathFileOpt gopt gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE_IDENTIFIER:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE IDENTIFIER gopt gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE_DOLLAR_IDENTIFIER:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE $ IDENTIFIER gopt gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE2:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE pathFileOpt gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE_IDENTIFIER2:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE IDENTIFIER gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE_DOLLAR_IDENTIFIER2:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE $ IDENTIFIER gopt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE3:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE pathFileOpt gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE_IDENTIFIER3:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE IDENTIFIER gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE_DOLLAR_IDENTIFIER3:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE $ IDENTIFIER gopt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE4:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE pathFileOpt gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE_IDENTIFIER4:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE IDENTIFIER gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE_DOLLAR_IDENTIFIER4:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE $ IDENTIFIER gopt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE5:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE pathFileOpt gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE_IDENTIFIER5:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE IDENTIFIER gopt eol
		case PROD_GENLOADSTATEMENT_GENLOAD_SCID_IDENTIFIER_TYPE_FILE_DOLLAR_IDENTIFIER5:
		// genloadStatement -> GENLOAD SCID IDENTIFIER TYPE variable FILE $ IDENTIFIER gopt eol
			m_listOptions.RemoveAll();
			break;

		case PROD_GOPT_DBASE:
		// gopt -> DBASE pathFileOpt
			if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE) )break;
			m_listOptions.AddTail(_T("DBASE"));
			break;

		case PROD_GOPT_SEGSIZE_INTEGERCONSTANT:
		// gopt -> SEGSIZE INTEGERconstant
		case PROD_GOPT_SEGSIZE:
		// gopt -> SEGSIZE variable
			if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE) )break;
			m_listOptions.AddTail(_T("SEGSIZE"));
			break;

		case PROD_GOPT_IDENTIFIER_INTEGERCONSTANT:
		// gopt -> IDENTIFIER INTEGERconstant
			if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE) )break;
			m_listOptions.AddTail(_T("CTCU"));
			break;

		case PROD_GOPT_IDENTIFIER_IDENTIFIER:
		// gopt -> IDENTIFIER IDENTIFIER
			if( !CheckToken(0,_T("RT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" ")) )
				break;
			if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE))
				break;
			m_listOptions.AddTail(_T("RT"));
			CheckToken(1,_T("ACE_1"),_T("ACE_2"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
				break;

		case PROD_GOPT_IDENTIFIER:
		// gopt -> IDENTIFIER variable
		if( !CheckToken(0,_T("RT"),_T("CTCU"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" ")))
			break;
		if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE))
			break;
		m_listOptions.AddTail((elementFromProduction(0)->lexeme())->asConstChar());
		break;

		case PROD_GOPT_MAX_INTEGERCONSTANT:
		// gopt -> MAX INTEGERconstant
		case PROD_GOPT_MAX:
		// gopt -> MAX variable
		if( !CheckList(0, &m_listOptions, IDS_SYNTAX_ERROR15, TRUE))
			break;
		m_listOptions.AddTail(ctstrMAX);
			break;

		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER:
		// imageStatement -> IMAGE memOpt IDENTIFIER eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER2:
		// imageStatement -> IMAGE memOpt IDENTIFIER fromOpt eol
		case PROD_IMAGESTATEMENT_IMAGE_ALL:
		// imageStatement -> IMAGE memOpt ALL eol
			CheckToken(2,_T("ACE1"),_T("ACE2"),_T("SXI"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_IMAGESTATEMENT_IMAGE_DUMP_IDENTIFIER:
		// imageStatement -> IMAGE DUMP IDENTIFIER operInst constant constant constant eol
		case PROD_IMAGESTATEMENT_IMAGE_DUMP_IDENTIFIER2:
		// imageStatement -> IMAGE DUMP IDENTIFIER constant constant constant eol
			CheckToken(2,_T("ACE1"),_T("ACE2"),_T("SXI"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER3:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt operInst constant constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER4:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt operInst constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER5:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER operInst constant constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER2:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER operInst constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER3:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER6:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt constant constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER7:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER8:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER4:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER constant constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER5:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER6:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER operInst constant constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER2:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER operInst constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER3:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER4:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER constant constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER5:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER constant eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER6:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_HDR:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt HDR eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER_HDR:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER HDR eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER_HDR:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER HDR eol
			CheckToken(1,_T("EXPORT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER9:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt pathFileOpt operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER_IDENTIFIER:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER IDENTIFIER operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER_DOLLAR_IDENTIFIER:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER $ IDENTIFIER operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER7:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt IDENTIFIER operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER8:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER pathFileOpt operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER7:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt $ IDENTIFIER operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER8:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER pathFileOpt operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER_DOLLAR_IDENTIFIER:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER $ IDENTIFIER operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER_IDENTIFIER:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER IDENTIFIER operInst eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER10:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt pathFileOpt eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER_IDENTIFIER2:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER IDENTIFIER eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER_DOLLAR_IDENTIFIER2:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER $ IDENTIFIER eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER9:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt IDENTIFIER eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER10:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER pathFileOpt eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER9:
		// imageStatement -> IMAGE IDENTIFIER pathFileOpt $ IDENTIFIER eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER10:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER pathFileOpt eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_IDENTIFIER_DOLLAR_IDENTIFIER2:
		// imageStatement -> IMAGE IDENTIFIER IDENTIFIER $ IDENTIFIER eol
		case PROD_IMAGESTATEMENT_IMAGE_IDENTIFIER_DOLLAR_IDENTIFIER_IDENTIFIER2:
		// imageStatement -> IMAGE IDENTIFIER $ IDENTIFIER IDENTIFIER eol
			CheckToken(1,_T("IMPORT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_IMAGESTATEMENT_IMAGE_LAYOUT_IDENTIFIER:
		// imageStatement -> IMAGE LAYOUT IDENTIFIER eol
			CheckToken(2,_T("ACE1"),_T("ACE2"),_T("SXI"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_IMAGEOPT_IDENTIFIER:
		// imageOpt -> IDENTIFIER
			CheckToken(0,_T("IMPORT"),_T("EXPORT"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_IMCREQSTATEMENT_IMCREQ_SCID_EQ_INTEGERCONSTANT_TYPE_EQ_IDENTIFIER_IDENTIFIER_EQ_TIMESTRING:
		// imcreqStatement -> IMCREQ SCID = INTEGERconstant TYPE = IDENTIFIER IDENTIFIER = TIMEstring eol
		case PROD_IMCREQSTATEMENT_IMCREQ_SCID_EQ_INTEGERCONSTANT_TYPE_EQ_IDENTIFIER_IDENTIFIER_EQ_TIMESTRING_DURATION_EQ_TIMESTRING:
		// imcreqStatement -> IMCREQ SCID = INTEGERconstant TYPE = IDENTIFIER IDENTIFIER = TIMEstring DURATION = TIMEstring eol
		case PROD_IMCREQSTATEMENT_IMCREQ_SCID_EQ_TYPE_EQ_IDENTIFIER_IDENTIFIER_EQ_TIMESTRING:
		// imcreqStatement -> IMCREQ SCID = variable TYPE = IDENTIFIER IDENTIFIER = TIMEstring eol
		case PROD_IMCREQSTATEMENT_IMCREQ_SCID_EQ_TYPE_EQ_IDENTIFIER_IDENTIFIER_EQ_TIMESTRING_DURATION_EQ_TIMESTRING:
		// imcreqStatement -> IMCREQ SCID = variable TYPE = IDENTIFIER IDENTIFIER = TIMEstring DURATION = TIMEstring eol
			CheckToken(6,ctstrNORMAL,_T("POSTHK"),_T("ECLIPSE"),_T("POSTMAN"),_T("POSTYAW"),_T("POSTTF"),_T("POSTECL"),_T(" "));
			CheckToken(7,ctstrTIME,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_PSSSTATEMENT_PSS_COLLECT_START_IDENTIFIER:
		// pssStatement -> PSS COLLECT START IDENTIFIER eol
			CheckToken(3,_T("AZIMUTH"),_T("ELEVATION"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_PSSSTATEMENT_PSS_CALIBRATE_IDENTIFIER_TIMECONSTANT:
		// pssStatement -> PSS CALIBRATE IDENTIFIER TIMEconstant eol
		case PROD_PSSSTATEMENT_PSS_CALIBRATE_IDENTIFIER_TIMESTRING:
		// pssStatement -> PSS CALIBRATE IDENTIFIER TIMEstring eol
		case PROD_PSSSTATEMENT_PSS_CALIBRATE_IDENTIFIER:
		// pssStatement -> PSS CALIBRATE IDENTIFIER eol
			CheckToken(2,_T("AZIMUTH"),_T("ELEVATION"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_PSSSTATEMENT_PSS_INIT_FILENAME:
		// pssStatement -> PSS INIT FILENAME eol
			CheckToken(2,_T("AZ.CAL"),_T("EL.CAL"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_RTCSSTATEMENT_RTCS_LABEL_EQ_IDENTIFIER_IDENTIFIER_EQ_IDENTIFIER:
		// rtcsStatement -> RTCS LABEL = IDENTIFIER IDENTIFIER = IDENTIFIER eol
			CheckRTCS(4,6);
			break;

		case PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_EQ_IDENTIFIER:
		// rtcsStatement -> RTCS LABEL IDENTIFIER IDENTIFIER = IDENTIFIER eol
			CheckRTCS(3,5);
			break;

		case PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_EQ_IDENTIFIER_IDENTIFIER:
		// rtcsStatement -> RTCS LABEL IDENTIFIER = IDENTIFIER IDENTIFIER eol
			CheckRTCS(4,5);
			break;

		case PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_IDENTIFIER:
		// rtcsStatement -> RTCS LABEL IDENTIFIER IDENTIFIER IDENTIFIER eol
			CheckRTCS(3,4);
			break;

		case PROD_RTCSSTATEMENT_RTCS_LABEL_EQ_IDENTIFIER_IDENTIFIER_EQ:
		// rtcsStatement -> RTCS LABEL = IDENTIFIER IDENTIFIER = variable eol
			CheckRTCS(4,0);
			break;

		case PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_IDENTIFIER_EQ:
		// rtcsStatement -> RTCS LABEL IDENTIFIER IDENTIFIER = variable eol
			CheckRTCS(3,0);
			break;

		case PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_EQ_IDENTIFIER:
		// rtcsStatement -> RTCS LABEL IDENTIFIER = IDENTIFIER variable eol
			CheckRTCS(4,0);
			break;

		case PROD_RTCSSTATEMENT_RTCS_LABEL_IDENTIFIER_IDENTIFIER:
		// rtcsStatement -> RTCS LABEL IDENTIFIER IDENTIFIER variable eol
			CheckRTCS(3,0);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_IDENTIFIER_ADDRESS_INTEGERCONSTANT_IDENTIFIER_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL IDENTIFIER ADDRESS INTEGERconstant IDENTIFIER IDENTIFIER eol
			CheckRTCS(5,6);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_EQ_IDENTIFIER_ADDRESS_INTEGERCONSTANT_IDENTIFIER_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL = IDENTIFIER ADDRESS INTEGERconstant IDENTIFIER IDENTIFIER eol
		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_IDENTIFIER_ADDRESS_EQ_INTEGERCONSTANT_IDENTIFIER_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL IDENTIFIER ADDRESS = INTEGERconstant IDENTIFIER IDENTIFIER eol
			CheckRTCS(6,7);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_IDENTIFIER_ADDRESS_INTEGERCONSTANT_IDENTIFIER_EQ_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL IDENTIFIER ADDRESS INTEGERconstant IDENTIFIER = IDENTIFIER eol
			CheckRTCS(5,7);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_EQ_IDENTIFIER_ADDRESS_EQ_INTEGERCONSTANT_IDENTIFIER_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL = IDENTIFIER ADDRESS = INTEGERconstant IDENTIFIER IDENTIFIER eol
			CheckRTCS(7,8);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_EQ_IDENTIFIER_ADDRESS_INTEGERCONSTANT_IDENTIFIER_EQ_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL = IDENTIFIER ADDRESS INTEGERconstant IDENTIFIER = IDENTIFIER eol
		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_IDENTIFIER_ADDRESS_EQ_INTEGERCONSTANT_IDENTIFIER_EQ_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL IDENTIFIER ADDRESS = INTEGERconstant IDENTIFIER = IDENTIFIER eol
			CheckRTCS(6,8);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_EQ_IDENTIFIER_ADDRESS_EQ_INTEGERCONSTANT_IDENTIFIER_EQ_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL = IDENTIFIER ADDRESS = INTEGERconstant IDENTIFIER = IDENTIFIER eol
			CheckRTCS(7,9);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_IDENTIFIER_ADDRESS_INTEGERCONSTANT_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL IDENTIFIER ADDRESS INTEGERconstant IDENTIFIER variable eol
			CheckRTCS(5,0);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_EQ_IDENTIFIER_ADDRESS_INTEGERCONSTANT_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL = IDENTIFIER ADDRESS INTEGERconstant IDENTIFIER variable eol
		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_IDENTIFIER_ADDRESS_EQ_INTEGERCONSTANT_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL IDENTIFIER ADDRESS = INTEGERconstant IDENTIFIER variable eol
			CheckRTCS(6,0);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_IDENTIFIER_ADDRESS_INTEGERCONSTANT_IDENTIFIER_EQ:
		// rtcsdataStatement -> RTCSDATA LABEL IDENTIFIER ADDRESS INTEGERconstant IDENTIFIER = variable eol
			CheckRTCS(5,0);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_EQ_IDENTIFIER_ADDRESS_EQ_INTEGERCONSTANT_IDENTIFIER:
		// rtcsdataStatement -> RTCSDATA LABEL = IDENTIFIER ADDRESS = INTEGERconstant IDENTIFIER variable eol
			CheckRTCS(7,0);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_EQ_IDENTIFIER_ADDRESS_INTEGERCONSTANT_IDENTIFIER_EQ:
		// rtcsdataStatement -> RTCSDATA LABEL = IDENTIFIER ADDRESS INTEGERconstant IDENTIFIER = variable eol
		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_IDENTIFIER_ADDRESS_EQ_INTEGERCONSTANT_IDENTIFIER_EQ:
		// rtcsdataStatement -> RTCSDATA LABEL IDENTIFIER ADDRESS = INTEGERconstant IDENTIFIER = variable eol
			CheckRTCS(6,0);
			break;

		case PROD_RTCSDATASTATEMENT_RTCSDATA_LABEL_EQ_IDENTIFIER_ADDRESS_EQ_INTEGERCONSTANT_IDENTIFIER_EQ:
		// rtcsdataStatement -> RTCSDATA LABEL = IDENTIFIER ADDRESS = INTEGERconstant IDENTIFIER = variable eol
			CheckRTCS(7,0);
			break;

		case PROD_SCAN_SCAN:
		// scan -> SCAN scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt eol
		case PROD_SCAN_SCAN2:
		// scan -> SCAN scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt eol
		case PROD_SCAN_SCAN3:
		// scan -> SCAN scanOpt scanOpt scanOpt scanOpt scanOpt scanOpt eol
		case PROD_SCAN_SCAN4:
		// scan -> SCAN scanOpt scanOpt scanOpt scanOpt scanOpt eol
			OptionMapLookup(0, &m_mapScanReqOptions, eSCAN);
			break;

		case PROD_SCANOPT_FRAME_EQ_IDENTIFIER:
		// scanOpt -> FRAME = IDENTIFIER
			OptionLookup(0, &m_mapScanReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_SCANOPT_IDENTIFIER_EQ_IDENTIFIER:
		// scanOpt -> IDENTIFIER = IDENTIFIER
			if(!CheckToken(0,ctstrPRIORITY,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" ")))
				break;
			if(!OptionLookup(0, &m_mapScanReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrPRIORITY_1,ctstrPRIORITY_2,ctstrNORMAL,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
				break;

		case PROD_SCANOPT_MODE_EQ_IDENTIFIER:
		// scanOpt -> MODE = IDENTIFIER
			if(!OptionLookup(0, &m_mapScanReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrMODE_0,ctstrMODE_1,ctstrSINGLE_0_1,ctstrSINGLE_0_2,ctstrSINGLE_0_4,ctstrDOUBLE_0_1,_T(" "),_T(" "));
				break;

		case PROD_SCANOPT_EXECUTE_EQ_IDENTIFIER:
		// scanOpt -> EXECUTE = IDENTIFIER
			if( !CheckList(0, &m_listScanOptOptions, IDS_SYNTAX_ERROR15, TRUE))
				break;
			m_listScanOptOptions.AddTail(ctstrEXECUTE);
			CheckToken(2,ctstrLOAD_AND_EXEC,ctstrLOAD_REG_ONLY,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_SCANOPT_BBCAL_EQ_IDENTIFIER:
		// scanOpt -> BBCAL = IDENTIFIER
			if( !CheckList(0, &m_listScanOptOptions, IDS_SYNTAX_ERROR15, TRUE))
				break;
			m_listScanOptOptions.AddTail(ctstrBBCAL);
			CheckToken(2,ctstrDO_NOT_SUPP,ctstrSUPP,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_SCANOPT_SIDE_EQ_OATS:
		// scanOpt -> SIDE = OATS
			OptionLookup(0, &m_mapScanReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_SCANOPT_SIDE_EQ_IDENTIFIER:
		// scanOpt -> SIDE = IDENTIFIER
			if( !OptionLookup(0, &m_mapScanReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrEAST,ctstrWEST,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_SCANOPT_INSTRUMENT_EQ_IDENTIFIER:
		// scanOpt -> INSTRUMENT = IDENTIFIER
			if( !OptionLookup(0, &m_mapScanReqOptions, IDS_SYNTAX_ERROR15) )
				break;
			CheckToken(2,ctstrIMGR,ctstrSDR,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_SCANOPT_IDENTIFIER_EQ_INTEGERCONSTANT:
		// scanOpt -> IDENTIFIER = INTEGERconstant
		case PROD_SCANOPT_IDENTIFIER_EQ:
		// scanOpt -> IDENTIFIER = variable
			if( !CheckToken(0,ctstrREPEAT,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" ")))
				break;
			if( !CheckList(0, &m_listScanOptOptions, IDS_SYNTAX_ERROR15, TRUE))
				break;
			m_listScanOptOptions.AddTail(ctstrREPEAT);
			break;

		case PROD_SCANDATA_SCANDATA:
		// scandata -> SCANDATA scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt eol
		case PROD_SCANDATA_SCANDATA2:
		// scandata -> SCANDATA scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt scanDOpt eol
			OptionMapLookup(0, &m_mapScandataReqOptions, eSCANDATA);
			break;

		case PROD_SCANDOPT_FRAME_EQ_IDENTIFIER:
		// scanDOpt -> FRAME = IDENTIFIER
			OptionLookup(0, &m_mapScandataReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_SCANDOPT_IDENTIFIER_EQ_IDENTIFIER:
		// scanDOpt -> IDENTIFIER = IDENTIFIER
			if( !CheckToken(0,ctstrPRIORITY,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" ")))
				break;
			if( !OptionLookup(0, &m_mapScandataReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrPRIORITY_1,ctstrPRIORITY_2,ctstrNORMAL,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_SCANDOPT_MODE_EQ_IDENTIFIER:
		// scanDOpt -> MODE = IDENTIFIER
			if( !OptionLookup(0, &m_mapScandataReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrMODE_0,ctstrMODE_1,ctstrSINGLE_0_1,ctstrSINGLE_0_2,ctstrSINGLE_0_4,ctstrDOUBLE_0_1,_T(" "),_T(" "));
			break;

		case PROD_SCANDOPT_EXECUTE_EQ_IDENTIFIER:
		// scanDOpt -> EXECUTE = IDENTIFIER
			if( !OptionLookup(0, &m_mapScandataReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrLOAD_AND_EXEC,ctstrLOAD_REG_ONLY,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_SCANDOPT_BBCAL_EQ_IDENTIFIER:
		// scanDOpt -> BBCAL = IDENTIFIER
			if( !CheckList(0, &m_listScandataOptOptions, IDS_SYNTAX_ERROR15, TRUE))
				break;
			m_listScandataOptOptions.AddTail(ctstrBBCAL);
			CheckToken(2,ctstrDO_NOT_SUPP,ctstrSUPP,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_SCANDOPT_SIDE_EQ_OATS:
		// scanDOpt -> SIDE = OATS
			OptionLookup(0, &m_mapScandataReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_SCANDOPT_SIDE_EQ_IDENTIFIER:
		// scanDOpt -> SIDE = IDENTIFIER
			if (!OptionLookup(0, &m_mapScandataReqOptions, IDS_SYNTAX_ERROR15) )
				break;
			CheckToken(2,ctstrPLUS_X,ctstrMINUS_X,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_SCANDOPT_INSTRUMENT_EQ_IDENTIFIER:
		// scanDOpt -> INSTRUMENT = IDENTIFIER
			if (!OptionLookup(0, &m_mapScandataReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrIMGR,ctstrSDR,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_SCANDOPT_IDENTIFIER_EQ_INTEGERCONSTANT:
		// scanDOpt -> IDENTIFIER = INTEGERconstant
		case PROD_SCANDOPT_IDENTIFIER_EQ:
		// scanDOpt -> IDENTIFIER = variable
			if (!CheckToken(0,ctstrREPEAT,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" ")))
				break;
			OptionLookup(0, &m_mapScandataReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_SCANDOPT_OBJID_EQ_INTEGERCONSTANT:
		// scanDOpt -> OBJID = INTEGERconstant
		case PROD_SCANDOPT_OBJID_EQ:
		// scanDOpt -> OBJID = variable
			OptionLookup(0, &m_mapScandataReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_SCANDOPT_DURATION_EQ_TIMESTRING:
		// scanDOpt -> DURATION = TIMEstring
			OptionLookup(0, &m_mapScandataReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_SCMDSTATEMENT_SCMD:
		// scmdStatement -> SCMD decoderAddress cmdMnemonic datawordArgs eol
			CheckCmdStmt(0);
			break;

		case PROD_STAR_STAR:
		// star -> STAR starOpt starOpt starOpt starOpt eol
		case PROD_STAR_STAR2:
		// star -> STAR starOpt starOpt starOpt eol
			OptionMapLookup(0, &m_mapStarReqOptions, eSTAR);
			break;

		case PROD_STAROPT_INSTRUMENT_EQ_IDENTIFIER:
		// starOpt -> INSTRUMENT = IDENTIFIER
			if (!OptionLookup(0, &m_mapStarReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrIMGR,ctstrSDR,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_STAROPT_LAST_STAR_CLASS_EQ_IDENTIFIER:
		// starOpt -> LAST_STAR_CLASS = IDENTIFIER
			if( !CheckList(0, &m_listStarOptOptions, IDS_SYNTAX_ERROR15, TRUE))
				break;
			m_listStarOptOptions.AddTail(ctstrLAST_STAR_CLASS);
			CheckToken(2,ctstrSENSE,ctstrSEQUENCE,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_STAROPT_DURATION_EQ_TIMESTRING:
		// starOpt -> DURATION = TIMEstring
			OptionLookup(0, &m_mapStarReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_STAROPT_MAX_EQ_INTEGERCONSTANT:
		// starOpt -> MAX = INTEGERconstant
		case PROD_STAROPT_MAX_EQ:
		// starOpt -> MAX = variable
			OptionLookup(0, &m_mapStarReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_STARDATA_STARDATA:
		// stardata -> STARDATA starDOpt starDOpt starDOpt starDOpt starDOpt starDOpt eol
			OptionMapLookup(0, &m_mapStardataReqOptions, eSTARDATA);
			break;

		case PROD_STARDOPT_INSTRUMENT_EQ_IDENTIFIER:
		// starDOpt -> INSTRUMENT = IDENTIFIER
			if(! OptionLookup(0, &m_mapStardataReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrIMGR,ctstrSDR,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_STARDOPT_LAST_STAR_CLASS_EQ_IDENTIFIER:
		// starDOpt -> LAST_STAR_CLASS = IDENTIFIER
			if( !OptionLookup(0, &m_mapStardataReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrSENSE,ctstrSEQUENCE,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_STARDOPT_LOAD_REG_EQ_IDENTIFIER:
		// starDOpt -> LOAD_REG = IDENTIFIER
			if( !OptionLookup(0, &m_mapStardataReqOptions, IDS_SYNTAX_ERROR15) )
					break;
			CheckToken(2,ctstrLOAD_AND_EXEC,ctstrLOAD_REG_ONLY,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_STARDOPT_DURATION_EQ_TIMESTRING:
		// starDOpt -> DURATION = TIMEstring
		case PROD_STARDOPT_MAX_EQ_INTEGERCONSTANT:
		// starDOpt -> MAX = INTEGERconstant
		case PROD_STARDOPT_MAX_EQ:
		// starDOpt -> MAX = variable
		case PROD_STARDOPT_OBJID_EQ_INTEGERCONSTANT:
		// starDOpt -> OBJID = INTEGERconstant
		case PROD_STARDOPT_OBJID_EQ:
		// starDOpt -> OBJID = variable
			OptionLookup(0, &m_mapStardataReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_STARINFO_STARINFO:
		// starinfo -> STARINFO starIOpt starIOpt starIOpt starIOpt starIOpt starIOpt starIOpt starIOpt eol
			OptionMapLookup(0, &m_mapStarinfoReqOptions, eSTARINFO);
			break;
			
		case PROD_STARIOPT_INSTRUMENT_EQ_IDENTIFIER:
		// starIOpt -> INSTRUMENT = IDENTIFIER
			if( !OptionLookup(0, &m_mapStarinfoReqOptions, IDS_SYNTAX_ERROR15))
				break;
			CheckToken(2,ctstrIMGR,ctstrSDR,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_STARIOPT_IDENTIFIER_EQ_TIMESTRING:
		// starIOpt -> IDENTIFIER = TIMEstring
			if ( !CheckToken(0,ctstrTIME,_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" ")))
				break;
			OptionLookup(0, &m_mapStarinfoReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_STARIOPT_SCID_EQ_INTEGERCONSTANT:
		// starIOpt -> SCID = INTEGERconstant
		case PROD_STARIOPT_SCID_EQ:
		// starIOpt -> SCID = variable
		case PROD_STARIOPT_OBJID_EQ_INTEGERCONSTANT:
		// starIOpt -> OBJID = INTEGERconstant
		case PROD_STARIOPT_OBJID_EQ:
		// starIOpt -> OBJID = variable
		case PROD_STARIOPT_STARID_EQ_INTEGERCONSTANT:
		// starIOpt -> STARID = INTEGERconstant
		case PROD_STARIOPT_STARID_EQ:
		// starIOpt -> STARID = variable
		case PROD_STARIOPT_DWELL_EQ_INTEGERCONSTANT:
		// starIOpt -> DWELL = INTEGERconstant
		case PROD_STARIOPT_DWELL_EQ_MINUS_INTEGERCONSTANT:
		// starIOpt -> DWELL = - INTEGERconstant
		case PROD_STARIOPT_DWELL_EQ:
		// starIOpt -> DWELL = variable
		case PROD_STARIOPT_NUMLOOKS_EQ_INTEGERCONSTANT:
		// starIOpt -> NUMLOOKS = INTEGERconstant
		case PROD_STARIOPT_NUMLOOKS_EQ:
		// starIOpt -> NUMLOOKS = variable
		case PROD_STARIOPT_LOOKNUM_EQ_INTEGERCONSTANT:
		// starIOpt -> LOOKNUM = INTEGERconstant
			OptionLookup(0, &m_mapStarinfoReqOptions, IDS_SYNTAX_ERROR15);
			break;

		case PROD_TGISTATEMENT_TGI_IDENTIFIER:
		// tgiStatement -> TGI memOpt IDENTIFIER eol
			CheckToken(2,_T("ACE1"),_T("ACE2"),_T("SXI"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			break;

		case PROD_TLMWAITSTATEMENT_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_EQUALITY:
		// tlmwaitStatement -> TLMWAIT INTEGERconstant IDENTIFIER EQUALITY stringLiteral stringLiteral eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_EQUALITY2:
		// tlmwaitStatement -> TLMWAIT INTEGERconstant IDENTIFIER EQUALITY stringLiteral variable eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_EQUALITY3:
		// tlmwaitStatement -> TLMWAIT INTEGERconstant IDENTIFIER EQUALITY constant stringLiteral eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_EQUALITY4:
		// tlmwaitStatement -> TLMWAIT INTEGERconstant IDENTIFIER EQUALITY constant variable eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_EQUALITY5:
		// tlmwaitStatement -> TLMWAIT INTEGERconstant IDENTIFIER EQUALITY variable stringLiteral eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_INTEGERCONSTANT_IDENTIFIER_EQUALITY6:
		// tlmwaitStatement -> TLMWAIT INTEGERconstant IDENTIFIER EQUALITY variable variable eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_IDENTIFIER_EQUALITY:
		// tlmwaitStatement -> TLMWAIT variable IDENTIFIER EQUALITY stringLiteral stringLiteral eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_IDENTIFIER_EQUALITY2:
		// tlmwaitStatement -> TLMWAIT variable IDENTIFIER EQUALITY stringLiteral variable eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_IDENTIFIER_EQUALITY3:
		// tlmwaitStatement -> TLMWAIT variable IDENTIFIER EQUALITY constant stringLiteral eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_IDENTIFIER_EQUALITY4:
		// tlmwaitStatement -> TLMWAIT variable IDENTIFIER EQUALITY constant variable eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_IDENTIFIER_EQUALITY5:
		// tlmwaitStatement -> TLMWAIT variable IDENTIFIER EQUALITY variable stringLiteral eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_IDENTIFIER_EQUALITY6:
		// tlmwaitStatement -> TLMWAIT variable IDENTIFIER EQUALITY variable variable eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_TIMECONSTANT_IDENTIFIER_EQUALITY:
		// tlmwaitStatement -> TLMWAIT TIMEconstant IDENTIFIER EQUALITY stringLiteral stringLiteral eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_TIMECONSTANT_IDENTIFIER_EQUALITY2:
		// tlmwaitStatement -> TLMWAIT TIMEconstant IDENTIFIER EQUALITY stringLiteral variable eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_TIMECONSTANT_IDENTIFIER_EQUALITY3:
		// tlmwaitStatement -> TLMWAIT TIMEconstant IDENTIFIER EQUALITY constant stringLiteral eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_TIMECONSTANT_IDENTIFIER_EQUALITY4:
		// tlmwaitStatement -> TLMWAIT TIMEconstant IDENTIFIER EQUALITY constant variable eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_TIMECONSTANT_IDENTIFIER_EQUALITY5:
		// tlmwaitStatement -> TLMWAIT TIMEconstant IDENTIFIER EQUALITY variable stringLiteral eol
		case PROD_TLMWAITSTATEMENT_TLMWAIT_TIMECONSTANT_IDENTIFIER_EQUALITY6:
		// tlmwaitStatement -> TLMWAIT TIMEconstant IDENTIFIER EQUALITY variable variable eol
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR3);
			break;

		case PROD_USCSTATEMENT_USC_IDENTIFIER_IDENTIFIER:
		// uscStatement -> USC IDENTIFIER IDENTIFIER eol
			CheckToken(1,_T("ENABLE"),_T("DISABLE"),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "),_T(" "));
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR3);
			break;

		case PROD_USCSTATEMENT_USC_ACK_IDENTIFIER_TIMECONSTANT:
		// uscStatement -> USC ACK IDENTIFIER TIMEconstant eol
		case PROD_USCSTATEMENT_USC_ACK_IDENTIFIER_TIMESTRING:
		// uscStatement -> USC ACK IDENTIFIER TIMEstring eol
			FindTlmPoint(2, FALSE, IDS_SYNTAX_ERROR3);
			break;

		default:/*Reduce*/
			break;
	}

	return stackElement();
}

#endif
