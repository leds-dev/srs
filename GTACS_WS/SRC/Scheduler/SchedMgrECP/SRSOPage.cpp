// SRSOPage.cpp : implementation file
//

#include "stdafx.h"
#include "schedmgrecp.h"
#include "GenUpdSheet.h"
#include "SRSOPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSRSOPage property page

IMPLEMENT_DYNCREATE(CSRSOPage, CResizablePage)

CSRSOPage::CSRSOPage() : CResizablePage(CSRSOPage::IDD)
{
	//{{AFX_DATA_INIT(CSRSOPage)
	//}}AFX_DATA_INIT
}

CSRSOPage::~CSRSOPage(){}

void CSRSOPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSRSOPage)
	DDX_Control(pDX, IDC_SRSO_COORD_NS_EDIT, m_cSRSO_NS);
	DDX_Control(pDX, IDC_SRSO_COORD_RATIO_EDIT, m_cSRSO_Ratio);
	DDX_Control(pDX, IDC_SRSO_COORD_DUR_EDIT, m_cSRSO_Dur);
	DDX_Control(pDX, IDC_SRSO_COORD_EW_EDIT, m_cSRSO_EW);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSRSOPage, CResizablePage)
	//{{AFX_MSG_MAP(CSRSOPage)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_SRSO_PREDEF_RADIO, OnSrsoPredefRadio)
	ON_BN_CLICKED(IDC_SRSO_MAN_COORD_RADIO, OnSrsoManCoordRadio)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSRSOPage message handlers

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Ray Mosley		Date : 9/04/03		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CSRSOPage::Init
//  Description :	This routine initializes the member variables based on 
//					the passed parameters.
//
//  Returns :		void	-
//
//  Parameters :
//		BOOL bFirstWizPage	-	first display page.
//		BOOL bLastWizPage	-	last display page.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CSRSOPage::Init(BOOL bFirstWizPage, BOOL bLastWizPage)
{
	m_bFirstWizPage	= bFirstWizPage;
	m_bLastWizPage	= bLastWizPage;
	m_bUseManCrd	= !(BOOL)GetAppRegMakeSched()->m_nUsePreDef;
}

BOOL CSRSOPage::OnInitDialog() 
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	CResizablePage::OnInitDialog();

	// Add all of the necessary anchors
	AddAnchor(IDC_SRSO_UPD_GROUP, TOP_LEFT, BOTTOM_RIGHT);

	AddAnchor(IDC_SRSO_SCAN_FRMLBL_GROUP, TOP_LEFT, TOP_RIGHT);	
	AddAnchor(IDC_SRSO_FRMLBL_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SRSO_FRMLBL_EDIT, TOP_LEFT, TOP_RIGHT);

	AddAnchor(IDC_SRSO_COORD_SEL_GROUP, TOP_LEFT, TOP_RIGHT);

	AddAnchor(IDC_SRSO_PREDEF_GROUP, TOP_LEFT, TOP_RIGHT);	
	AddAnchor(IDC_SRSO_PREDEF_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SRSO_SECTOR_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SRSO_SECTOR_EDIT, TOP_LEFT, TOP_RIGHT);

	AddAnchor(IDC_SRSO_MAN_COORD_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SRSO_MAN_COORD_RADIO, TOP_LEFT, TOP_LEFT);

	AddAnchor(IDC_SRSO_COORD_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SRSO_COORD_NS_STATIC, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SRSO_COORD_NS_EDIT, TOP_LEFT, TOP_CENTER);
	AddAnchor(IDC_SRSO_COORD_DUR_STATIC, TOP_CENTER, TOP_CENTER);
	AddAnchor(IDC_SRSO_COORD_DUR_EDIT, TOP_CENTER, TOP_RIGHT);
	AddAnchor(IDC_SRSO_COORD_EW_STATIC, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SRSO_COORD_EW_EDIT, TOP_LEFT, TOP_CENTER);
	AddAnchor(IDC_SRSO_COORD_RATIO_STATIC, TOP_CENTER, TOP_CENTER);
	AddAnchor(IDC_SRSO_COORD_RATIO_EDIT, TOP_CENTER, TOP_RIGHT);

	((CButton*)GetDlgItem(IDC_SRSO_PREDEF_RADIO))->SetCheck(!m_bUseManCrd);
	((CButton*)GetDlgItem(IDC_SRSO_MAN_COORD_RADIO))->SetCheck(m_bUseManCrd);

	GetDlgItem(IDC_SRSO_FRMLBL_EDIT)->SetWindowText(GetAppRegSC()->m_strSRSOFrameLabel[GetAppRegSite()->m_nDefaultSC]);
	m_cSRSO_Dur.SetWindowText(_T("1.00"));
	m_cSRSO_Ratio.SetWindowText(_T("1.00"));
	m_cSRSO_NS.SetWindowText(_T("1.00"));
	m_cSRSO_EW.SetWindowText(_T("1.00"));


	if( !pSheet->GetFramePage()->GetCreateNew()
		|| !pSheet->GetFramePage()->GetUseSRSO() 
		|| !pSheet->GetFramePage()->GetUse()		
		)
	{
		GetDlgItem(IDC_SRSO_UPD_GROUP)->EnableWindow(FALSE);	

		GetDlgItem(IDC_SRSO_SCAN_FRMLBL_GROUP)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_FRMLBL_LABEL)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_FRMLBL_EDIT)->EnableWindow(FALSE);	

		GetDlgItem(IDC_SRSO_COORD_SEL_GROUP)->EnableWindow(FALSE);

		GetDlgItem(IDC_SRSO_PREDEF_GROUP)->EnableWindow(FALSE);
		GetDlgItem(IDC_SRSO_PREDEF_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_SRSO_SECTOR_LABEL)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_SECTOR_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_SRSO_MAN_COORD_GROUP)->EnableWindow(FALSE);
		GetDlgItem(IDC_SRSO_MAN_COORD_RADIO)->EnableWindow(FALSE);

		GetDlgItem(IDC_SRSO_COORD_GROUP)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_COORD_NS_STATIC)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_COORD_NS_EDIT)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_COORD_EW_STATIC)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_COORD_EW_EDIT)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_COORD_DUR_STATIC)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_COORD_DUR_EDIT)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_COORD_RATIO_STATIC)->EnableWindow(FALSE);	
		GetDlgItem(IDC_SRSO_COORD_RATIO_EDIT)->EnableWindow(FALSE);	
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSRSOPage::OnSize(UINT nType, int cx, int cy) 
{
	CResizablePage::OnSize(nType, cx, cy);
}

void CSRSOPage::UpdateControls()
//void CSRSOPage::OnUpdateControls() 
{
	CGenUpdSheet*	pSheet	 = (CGenUpdSheet*)GetParent();
	BOOL			bUseSRSO = true;

	// If update for frames, or create a new frametable, or update for SRSO frames is not checked,
	//		then don't enable the SRSO dialog page. 


	if( !pSheet->GetFramePage()->GetCreateNew()
		|| !pSheet->GetFramePage()->GetUseSRSO() 
		|| !pSheet->GetFramePage()->GetUse()){
			bUseSRSO = false;
	}

	//	BOOL			bUseSRSO = pSheet->GetFramePage()->GetUseSRSO();

	m_bUseManCrd	= ((CButton*)GetDlgItem(IDC_SRSO_MAN_COORD_RADIO))->GetCheck();
	GetDlgItem(IDC_SRSO_UPD_GROUP)->EnableWindow(bUseSRSO);	

	GetDlgItem(IDC_SRSO_SCAN_FRMLBL_GROUP)->EnableWindow(bUseSRSO);	
	GetDlgItem(IDC_SRSO_FRMLBL_LABEL)->EnableWindow(bUseSRSO);	
	GetDlgItem(IDC_SRSO_FRMLBL_EDIT)->EnableWindow(bUseSRSO);	

	GetDlgItem(IDC_SRSO_COORD_SEL_GROUP)->EnableWindow(bUseSRSO);

	GetDlgItem(IDC_SRSO_PREDEF_GROUP)->EnableWindow(bUseSRSO);
	GetDlgItem(IDC_SRSO_PREDEF_RADIO)->EnableWindow(bUseSRSO);
	GetDlgItem(IDC_SRSO_SECTOR_LABEL)->EnableWindow(!m_bUseManCrd && bUseSRSO);	
	GetDlgItem(IDC_SRSO_SECTOR_EDIT)->EnableWindow(!m_bUseManCrd && bUseSRSO);

	GetDlgItem(IDC_SRSO_MAN_COORD_GROUP)->EnableWindow(bUseSRSO);
	GetDlgItem(IDC_SRSO_MAN_COORD_RADIO)->EnableWindow(bUseSRSO);

	GetDlgItem(IDC_SRSO_COORD_GROUP)->EnableWindow(m_bUseManCrd && bUseSRSO);	
	GetDlgItem(IDC_SRSO_COORD_NS_STATIC)->EnableWindow(m_bUseManCrd && bUseSRSO);	
	GetDlgItem(IDC_SRSO_COORD_NS_EDIT)->EnableWindow(m_bUseManCrd && bUseSRSO);	
	GetDlgItem(IDC_SRSO_COORD_EW_STATIC)->EnableWindow(m_bUseManCrd && bUseSRSO);	
	GetDlgItem(IDC_SRSO_COORD_EW_EDIT)->EnableWindow(m_bUseManCrd && bUseSRSO);	
	GetDlgItem(IDC_SRSO_COORD_DUR_STATIC)->EnableWindow(m_bUseManCrd && bUseSRSO);	
	GetDlgItem(IDC_SRSO_COORD_DUR_EDIT)->EnableWindow(m_bUseManCrd && bUseSRSO);	
	GetDlgItem(IDC_SRSO_COORD_RATIO_STATIC)->EnableWindow(m_bUseManCrd && bUseSRSO);	
	GetDlgItem(IDC_SRSO_COORD_RATIO_EDIT)->EnableWindow(m_bUseManCrd && bUseSRSO);	
}

BOOL CSRSOPage::OnSetActive() 
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	// Update the visibility of the controls based upon the radio button positions
	// Update the filenames
	UpdateControls();
	
	// Setup the proper Wizard Buttons
	DWORD dWizFlags;
	if( (m_bFirstWizPage) && (m_bLastWizPage) )
		dWizFlags = PSWIZB_FINISH;
	else if( (m_bFirstWizPage) && (!m_bLastWizPage) )
		dWizFlags = PSWIZB_NEXT;
	else if( (!m_bFirstWizPage) && (m_bLastWizPage) )
		dWizFlags = PSWIZB_BACK | PSWIZB_FINISH;
	else if( (!m_bFirstWizPage) && (!m_bLastWizPage) )
		dWizFlags = PSWIZB_BACK | PSWIZB_NEXT;
	
	pSheet->SetWizardButtons(dWizFlags);

	return CResizablePage::OnSetActive();
}

void CSRSOPage::OnSrsoPredefRadio() 
{	
	m_bUsePreDef = ((CButton*)GetDlgItem(IDC_SRSO_PREDEF_RADIO))->GetCheck();

	((CButton*)GetDlgItem(IDC_SRSO_PREDEF_RADIO))->SetCheck(!m_bUsePreDef);
	((CButton*)GetDlgItem(IDC_SRSO_MAN_COORD_RADIO))->SetCheck(m_bUsePreDef);

	UpdateControls();
}

void CSRSOPage::OnSrsoManCoordRadio() 
{
	m_bUseManCrd = ((CButton*)GetDlgItem(IDC_SRSO_MAN_COORD_RADIO))->GetCheck();

	((CButton*)GetDlgItem(IDC_SRSO_MAN_COORD_RADIO))->SetCheck(!m_bUseManCrd);
	((CButton*)GetDlgItem(IDC_SRSO_PREDEF_RADIO))->SetCheck(m_bUseManCrd);

	UpdateControls();
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Ray Mosley       Date : 9/04/03       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CSRSOPage::OnWizardNext  
//  Description :   This routine is called when the user selects next   
//					on the dialog window. The specific window controls
//                  are updated as needed.
//                   
//                  
//  Returns :	LRESULT	-
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
LRESULT CSRSOPage::OnWizardNext()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	m_bUsePreDef = ((CButton*)GetDlgItem(IDC_SRSO_PREDEF_RADIO))->GetCheck();
	m_bUseManCrd = !m_bUsePreDef;
	GetAppRegMakeSched()->m_nUsePreDef = (int)m_bUsePreDef;

	m_strSRSODur.Format(_T("%f"), m_cSRSO_Dur.GetValue());
	m_strSRSO_EW.Format(_T("%f"), m_cSRSO_EW.GetValue());
	m_strSRSO_NS.Format(_T("%f"), m_cSRSO_NS.GetValue());
	m_strSRSORatio.Format(_T("%f"), m_cSRSO_Ratio.GetValue());
	GetDlgItem(IDC_SRSO_FRMLBL_EDIT)->GetWindowText(m_strSRSOFrame);
	GetDlgItem(IDC_SRSO_SECTOR_EDIT)->GetWindowText(m_strSRSOLabel);

	if( m_bUsePreDef )
	{
		if( pSheet->GetFramePage()->GetUseSRSO() && m_strSRSOLabel.IsEmpty() )
		{
			AfxMessageBox(_T("Please enter Sector Label!"));
			return -1;
		}
	}

	return CResizablePage::OnWizardNext();
}

