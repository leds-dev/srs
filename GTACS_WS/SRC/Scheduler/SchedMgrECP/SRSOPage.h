#if !defined(AFX_SRSOPAGE_H__8B4B1AB9_585D_463C_A099_76883D832E21__INCLUDED_)
#define AFX_SRSOPAGE_H__8B4B1AB9_585D_463C_A099_76883D832E21__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SRSOPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSRSOPage dialog

class CSRSOPage : public CResizablePage
{
	DECLARE_DYNCREATE(CSRSOPage)

// Construction
public:
	CSRSOPage();
	~CSRSOPage();
	void Init(BOOL bFirstWizPage, BOOL bLastWizPage);
	CString GetSRSOFrame(){return m_strSRSOFrame;};
	CString GetSRSOLabel(){return m_strSRSOLabel;};
	CString GetSRSODuration(){return m_strSRSODur;};
	CString GetSRSORatio(){return m_strSRSORatio;};
	CString GetSRSO_NS(){return m_strSRSO_NS;};
	CString GetSRSO_EW(){return m_strSRSO_EW;};
	BOOL GetUsePreDef(){return m_bUsePreDef;};

// Dialog Data
	//{{AFX_DATA(CSRSOPage)
	enum { IDD = IDD_SRSO_DIALOG };
	COXNumericEdit	m_cSRSO_NS;
	COXNumericEdit	m_cSRSO_Ratio;
	COXNumericEdit	m_cSRSO_Dur;
	COXNumericEdit	m_cSRSO_EW;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSRSOPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSRSOPage)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnSetActive();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSrsoPredefRadio();
	afx_msg void OnSrsoManCoordRadio();
	virtual LRESULT OnWizardNext();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void	UpdateControls();
	BOOL	m_bFirstWizPage;// TRUE if this the first page of a Wizard
	BOOL	m_bLastWizPage;	// TRUE if this the last page of a Wizard
	BOOL	m_bInInit;		// TRUE when in InitInstance.  This is needed so that the OnUpdate
	BOOL	m_bUseManCrd;	// TRUE if maually entered coordinates should be used
	BOOL	m_bUsePreDef;	// TRUE if Pre-defined sector label should be used
	CString m_strSRSOFrame; // SRSO Frame name
	CString	m_strSRSOLabel;	// SRSO Pre-defined sector label
	CString	m_strSRSODur;	// SRSO Duration
	CString	m_strSRSORatio;	// SRSO Aspect Ratio
	CString	m_strSRSO_NS;	// SRSO North-south coordinate center point
	CString	m_strSRSO_EW;	// SRSO East-west coordinate center point
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SRSOPAGE_H__8B4B1AB9_585D_463C_A099_76883D832E21__INCLUDED_)
