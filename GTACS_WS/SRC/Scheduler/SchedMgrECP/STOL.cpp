/******************
//////////////////////////////////////////////////////////////////////
// STOL.cpp : implementation of the STOL class.                       //
// (c) 20001 Frederick J. Shaw  
//	Prolog Updated                                    //
//////////////////////////////////////////////////////////////////////
******************/
#include "stdafx.h"
#include "STOL.h"
#include "SchedMgrECP.h"
#include "ProgressWnd.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :      CSTOL::CSTOL
//  Description :   Class constructor. 
//                  
//  Return :        constructor 
//					void	-	none.
//  Parameters : 
//				  CString strSTOLFileName	- Name of the file name.	
//				  CTextFile *ListFile		- Pointer to a textfile object	
//				  CTextFile *MapFile		-  Pointer to a textfile object
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CSTOL::CSTOL(CString strSTOLFileName, CTextFile *LisFile, CTextFile *MapFile)
	: CNewSchedFile(strSTOLFileName){
   m_pMapFile  = MapFile;
   m_pLisFile = LisFile;
   m_nRtnLine  = 0;
   m_nCurrentMasterSchedLine = 0;
   m_nCurrentLisLine = 0;
   m_nNextLisLine = 0;
   m_nPrevLisLine = 0;
   m_nPrologLines = 0;
   m_nMasterSchedLineNumFlag = 1;
 
//   m_STOLTimeTag.Create(1971,1,0,0,0,0);
   m_cCurrSchedTime.Create(1971,1,0,0,0,0);
   m_cPrevSchedTime.Create(1971,1,0,0,0,0);

   // Get the search path from the NT environmental variable.
   // CString strEPOCH_PROCS = _wgetenv(ctstrEPOCHPROCS);

   // CString strEPOCH_PROCS = _wgetenv(ctstrEPOCHPROCS);
   TCHAR* cEPOCH_PROCS;
   size_t requiredSize;
   bool	  bDone = false;
  
   _tgetenv_s( &requiredSize, NULL, 0, ctcEPOCHPROCS);

   cEPOCH_PROCS = (TCHAR*) malloc(requiredSize * sizeof(TCHAR));
   if (!cEPOCH_PROCS)
   {
		// output messge error with search path in environmental variable.
		CString strErrorMsg = _T("Unable to allocate mmemory for the enviromental variable.");
		GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
		bDone = true;
   }

   // Get the value of the LIB environment variable.
   _tgetenv_s( &requiredSize, cEPOCH_PROCS, requiredSize, ctcEPOCHPROCS);

   CString strEPOCH_PROCS(cEPOCH_PROCS);
   CString strDrive;
   CString strPath;
   
   free(cEPOCH_PROCS);
   // Now extract all search paths and prepend drive letter to path.
   while (!bDone && (!strEPOCH_PROCS.IsEmpty())){
	   int nPos = 0;
		if ( -1 != (nPos = strEPOCH_PROCS.Find(_T(':')))){
			strPath = strEPOCH_PROCS.Left(nPos);
			strEPOCH_PROCS = strEPOCH_PROCS.Right(strEPOCH_PROCS.GetLength() - nPos - 1);
			strEPOCH_PROCS.TrimLeft();
			strEPOCH_PROCS.TrimRight();
			strPath.TrimLeft();
			strPath.TrimRight();
		} else { 
			strPath = strEPOCH_PROCS;
			bDone = true;
		}

		if ( -1 != (nPos = strPath.Find(_T('=')))){
			// Remove the drive letter.
			strDrive = strPath.Left(nPos + 1);
			strDrive.Replace(_T("/"), _T(""));
			strDrive.Replace(_T("="), _T(":"));

			strPath.Delete(0, nPos + 1);
			strPath.TrimLeft();
			strPath.TrimRight();
		} else {
			// output messge error with search path in environmental variable.
			CString strErrorMsg = _T("Error interpreting environmental variable.");
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			bDone = true;
		}
		m_SearchPathArray.Add(strDrive + strPath + _T('/'));
   }
   
   m_UNCSTOLPath.File()         = strSTOLFileName;
   m_UNCSTOLPath.Full();
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 03/05/2002       version 2.0
//////////////////////////////////////////////////////////////////////////
//  Function : ~STOL()     
//  Description :   Destructor
//                  
//  Return :  None
//                       
//  Parameters :  None  
//  Note :          
//                  
//////////////////////////////////////////////////////////////////////////
******************/
CSTOL::~CSTOL(){
		
	// Free up memory allocated
	int nMaxSize = m_objParam.GetSize();
	int nObjIdx = 0;
	while ( nObjIdx < nMaxSize){
		delete ((PassedValueList *) m_objParam[nObjIdx]);
		nObjIdx++;
	}

	// Then remove elements from array.
	m_objParam.RemoveAll();
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : CSTOL::Expand()     
//  Description :  Expands out the stol procedure. 
//                 
//                  
//  Return :  BOOL:    TRUE -	SUCCESS
//                     FALSE -  FAIL
//  Parameters : 
//				void -	none   
//  Note :          
//                  
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CSTOL::Expand(){

    BOOL eReturnStatus  = TRUE;
	bool bGrdLine       = false;
	m_nPrologLines      = GetPrologLineCount(); 
    int nLineNum		= m_nCurrentMasterSchedLine + m_nPrologLines + 1;
    int nTotalNumLines  = GetLineCount();
	m_nCurrentLisLine   = m_pLisFile->GetLineCount() + 1; 

    CString strFormat   = _T("On-board schedule generation in progress.\n")
                          _T("Currently on line %d of %d\n");

#ifndef _NO_PROGRESS
    CProgressWnd wndProgress(AfxGetMainWnd(), _T("Schedule Generation"), TRUE);
    wndProgress.SetRange(0, nTotalNumLines, FALSE);
#endif
 
    while ((m_nCurrentMasterSchedLine < nTotalNumLines) && eReturnStatus ){

		// int nLineCount  = GetLineCount() + m_nPrologLines; 
        CString strLine = GetDirLine(m_nCurrentMasterSchedLine, m_nRtnLine);
        strLine.TrimLeft ();
		strLine.TrimRight();
		nLineNum		= m_nCurrentMasterSchedLine + m_nPrologLines + 1;
        
        CString strText;
        strText.Format(strFormat, nLineNum, nTotalNumLines);
        #ifndef _NO_PROGRESS
          wndProgress.SetText (FALSE, strText);
          wndProgress.SetPos (nLineNum, FALSE);
        #endif
            
        // Process STOL line.
		if (!strLine.IsEmpty() && '#' == strLine[0]){
			// Scheduler directive
			// Test for ##ground begin & ##ground end directives.
			CString strGroundBegin(ctstrGROUND);
			strGroundBegin += _T(' ');
			strGroundBegin += ctstrBEGIN;
			CString strGroundEnd(ctstrGROUND);
			strGroundEnd += _T(' ');
			strGroundEnd += ctstrEND;
			if (ScanLine(strLine, strGroundBegin) == TRUE){
				bGrdLine = true;
			} else if (ScanLine(strLine, strGroundEnd) == TRUE){
				if (!bGrdLine){
					eReturnStatus = FALSE;
					// Need to insert correct error message number.
					// Output an error "##ground end found with out a begin.
					CString strErrorMsg = _T("##ground end directive found without a matching begin. (16)");
					// GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
					// GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 16, strLine);
					GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
				} else {
					bGrdLine = false;
				}
			} else if (!bGrdLine) {
				m_pLisFile->AppendText(strLine);
				// Write line(s) to the list file
				for (int i = 1; i < m_nRtnLine; i++){
					CString strLine = GetTextAt(m_nCurrentMasterSchedLine + i);
					m_pLisFile->AppendText(strLine);
				}
				m_nCurrentLisLine += m_nRtnLine;
			}
		} else if (!bGrdLine) { 
			if (strLine.IsEmpty()){
				// m_pLisFile->AppendText(strLine);
				// m_nCurrentLisLine++;
			} else if (ScanLine(strLine, ctstrSTART) == TRUE){
					
				// Write out the name of the procedure being expanded.
				CString strComment;
				strComment.Format(_T("# %s"), strLine);
				m_pLisFile->AppendText(strComment);
				// Pass in the STOL line with a continuation character removed. 
				int nRtnLine;
				strLine = GetDirLine(m_nCurrentMasterSchedLine, nRtnLine);
				// Search and replace line with passed parameters.
				int nNumParam       = m_objParam.GetSize();
				for (int i = 0; i < nNumParam; i++){
					CString strName  = ((strctPassedValueList *)m_objParam[i])->strName;
					CString strValue = ((strctPassedValueList *)m_objParam[i])->strValue;      
					strLine.Replace(strName, strValue);
				}

				// Need to check for & and $ that are not resolved.
				if (TRUE != Process_StartLine(strLine)){
					eReturnStatus = FALSE;	
				}
				m_nCurrentLisLine++;

			} else{ 
				strLine = GetTextAt(m_nCurrentMasterSchedLine);
				// Write line to the list
				m_pLisFile->AppendText(strLine);
				// m_nCurrentLisLine++;

				// Write line(s) to the list file
				for (int i = 1; i < m_nRtnLine; i++){
					CString strLine = GetTextAt(m_nCurrentMasterSchedLine + i);
					m_pLisFile->AppendText(strLine);
					// m_nCurrentLisLine++;
				}

				int nRtnLine = 0;
				strLine = GetDirLine(m_nCurrentMasterSchedLine, nRtnLine);
				// Search and replace line with passed parameters.
				int nNumParam       = m_objParam.GetSize();
				for (int i = 0; i < nNumParam; i++){
					CString strName  = ((strctPassedValueList *)m_objParam[i])->strName;
					CString strValue = ((strctPassedValueList *)m_objParam[i])->strValue;      
					strName.Insert(0, _T('$'));
					strValue.Remove(_T('\"'));
					strLine.Replace(strName, strValue);
				}
				// Need to check for & and $ that are not resolved.
				AppendMapFile(strLine);
				m_nCurrentLisLine += m_nRtnLine;
			} 
		}

		#ifndef _NO_PROGRESS
			wndProgress.PeekAndPump(false);
			if (wndProgress.Cancelled()){
				break;
			}
		#endif
		m_nCurrentMasterSchedLine += m_nRtnLine;
	}
    return eReturnStatus;
}

/***************************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 05/30/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : Process_STOLLine(CString strSTOLLine)
//  Description :   Processes a STOL start directive
//                  
//  Return :  BOOL:    TRUE - Successful
//                     FALSE - Fail 
//  Parameters :  
//					CString strSTOLLine	- line containing a START directive.
//  
//  Note :          
//                  
//////////////////////////////////////////////////////////////////////////
***************************/
BOOL  CSTOL::Process_StartLine(CString strDirLine){

    // Remove time tag.
    CString strLineTrimmed(strDirLine);
    strLineTrimmed.TrimLeft ();
	strDirLine.TrimLeft();
    CString strSTOLName;
	CString strVal;
    BOOL eReturn_Status = TRUE;
	int nPos = 0;

	if ( -1 != (nPos = WhereIsToken(strLineTrimmed, ctchCOMMENT))){
		// Remove trailing comment.
		strLineTrimmed.Delete(nPos, strLineTrimmed.GetLength() - nPos);
		strDirLine.Delete(nPos, strLineTrimmed.GetLength() - nPos);
	}

	strLineTrimmed.MakeUpper();

    // Find the position of the STOL directive.
    nPos = WhereIsToken(strLineTrimmed, ctstrSTART);
    if (nPos < 0){
		// CString strErrorMsg = _T("Invalid syntax for STOL directive. (29)");
		// GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
		// GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
	    m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 29);
        eReturn_Status = FALSE;
    } else {
		int nLen = _tcsclen(ctstrSTART);
		strDirLine.Delete(0, nPos + nLen);
		strDirLine.TrimLeft();
		strDirLine.TrimRight();
        int nSTOLNameIndex = -1;
        strSTOLName = strDirLine;

        // Find where the STOL name ends.  
		// First look for a "(".
		if (-1 == (nSTOLNameIndex = strDirLine.Find(_T('('), 0))){
			// If a "(" is not found, then look for space.
			nSTOLNameIndex = strDirLine.Find(_T(' '), 0);	
		}  

        // Extract out the name
        if (nSTOLNameIndex > 0){
            int nLength = strSTOLName.GetLength();
            strSTOLName.Delete(nSTOLNameIndex, nLength - nSTOLNameIndex);
            // Remove the name, then parse the passed parameters.
            strDirLine.Delete(0, nSTOLNameIndex);
            strDirLine.TrimLeft();
            strDirLine.TrimRight();
            strSTOLName.TrimLeft();
            strSTOLName.TrimRight();
        } 
    }

    // Get the STOL extension.
    CString strExt(_T(".prc"));
    (((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nSTOL])->GetDocString(strExt, CDocTemplate::filterExt);

    // File paths 
    COXUNC  UNCSTOLPath (m_UNCSTOLPath);  

	UNCSTOLPath.Full();
    UNCSTOLPath.File () = strSTOLName + strExt;
	CString strFullPath = UNCSTOLPath.Full();

    CSTOL STOL(strFullPath, m_pLisFile, m_pMapFile);
	int nCurrentMasterSchedLine = 0;
	STOL.SetValues(nCurrentMasterSchedLine,
				   m_nCurrentLisLine,
				   m_nPrevLisLine,
                   m_nRtnLine,
				   m_cCurrSchedTime,
				   m_cPrevSchedTime);

    STOL.SetSTOLPath(UNCSTOLPath.Full());
    STOL.SetSearchPath(m_SearchPathArray);
	SetMasterSchedLineNumFlag((-1 * nCurrentMasterSchedLine)); // Set flag to negative since in a nested proc.

	// Set the current and previous timetag.
	// Set the current line number.

//    BOOL eSuccessful = TRUE;
    CFileException Exception;
	strFullPath = UNCSTOLPath.Full();
    if (TRUE != STOL.Load(strFullPath, CTextFile::RO, &Exception)){
		COXUNC  UNCSearchPath;
		int nMaxNumSearchPaths = m_SearchPathArray.GetSize();
		int nNumPaths = 0;
		bool bDone = false; 
		while (!bDone && (nNumPaths < nMaxNumSearchPaths)){
			// UNCSearchPath.Directory() = m_SearchPathArray[nNumPaths];
			UNCSearchPath.File () = m_SearchPathArray[nNumPaths] + strSTOLName + strExt;
			UNCSearchPath.Full();

			if (TRUE !=	STOL.Load(UNCSearchPath.Full(), CTextFile::RO, &Exception)){
				eReturn_Status = FALSE;
			} else {
				eReturn_Status = TRUE;
				bDone          = true;
			}
			nNumPaths++;
		}

		// Save the last filepath for printing out error messges. 
		strFullPath = UNCSearchPath.Full();
		if (TRUE != eReturn_Status){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			// GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			// GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			m_NewGblValData.WriteValidationMsg(m_nCurrentMasterSchedLine, strMsg, 2);
		}
    } 

	STOL.ParsePassedParams(strDirLine);
    if (TRUE == eReturn_Status){
		eValidLoc     eVLoc    = INVALID_TYPE;
		eValidStatus  eVStatus = INVALID_STATUS;
        if (TRUE != STOL.IsFileValid(eVStatus, eVLoc)){
			eReturn_Status = FALSE;
			CString strErrorMsg = _T("The STOL file has a invalid prolog.");
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			strErrorMsg = _T("Schedule generation is aborting.");
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::INFO);
        } else {
			// BOOL eTemp = STOL.IsOverRideSet();
			if (((PASSED != eVStatus) && (WARNING != eVStatus))
				 && (TRUE != STOL.IsOverRideSet())){ 
					// Not overridden and proc failed validation.	
					eReturn_Status = FALSE;
					CString strErrorMsg = _T("The STOL file failed validation or is unvalidated.");
					CString strMsg;
					strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
					strErrorMsg = _T("Schedule generation is aborting.");
					GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			} else {
	 			if (TRUE == STOL.IsOverRideSet()){
					// Override directive in file is set.
					CString strMsg(_T("Validation status was overridden!"));
					CString strTitle;
					strTitle.Format(_T("%s %s"), strMsg, strFullPath);
					GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
					GetAppOutputBar()->OutputToEvents(strTitle, COutputBar::INFO);
				} else if (WARNING == eVStatus){
					CString strErrorMsg = _T("The STOL file's validation status is warning.");
					CString strMsg;
					strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::WARNING);
					strMsg = _T("Schedule generation will proceed.");
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::INFO);
				} 
		
				if ((eVLoc != ONBOARD) && (eVLoc != BOTH)){
					eReturn_Status = FALSE;
					CString strErrorMsg = _T("The STOL file's execution locations are incompatiable.");
					CString strMsg;
					strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
					strMsg = _T("Schedule generation is aborting.");
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);

				} else 	if (TRUE !=   STOL.Expand()){
					eReturn_Status = FALSE;
					CString strErrorMsg = _T("Problems expanding the STOL proc.");
					CString strMsg;
					strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
					strMsg = _T("Schedule generation is aborting.");
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				} 
			}
	    }
		if (TRUE != STOL.Close(&Exception)){
			eReturn_Status = FALSE;
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strSTOLName);
			// GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			// GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
			m_NewGblValData.WriteValidationMsg(m_nCurrentMasterSchedLine, strMsg, 2);
		}
	}

	STOL.GetValues( &nCurrentMasterSchedLine,
					&m_nCurrentLisLine,
					&m_nPrevLisLine,
                    &m_nRtnLine,
					&m_cCurrSchedTime,
					&m_cPrevSchedTime);

    return eReturn_Status;
}

/********************************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 09/18/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function : CSTOL::AppendMapFile
//  Description :  Appends the lines after conversion to the map file 
//                  
//  Return :  
//				BOOL:    TRUE - Successful
//					     FALSE - Fail 
//  Parameters : 
//				CString strLine	- STOL line to be appended to
//				 				  the map file.
//  
//  Note :          
//                  
//////////////////////////////////////////////////////////////////////////
**********************************/
BOOL CSTOL::AppendMapFile(CString strLine){

	BOOL eError_Status = TRUE; // Set to successful.
//	CString strRTCSName;
	CString strSave(strLine);
	int nNextLisLine = m_nCurrentLisLine + m_nRtnLine - 1;	

	strLine.TrimLeft ();
	strLine.TrimRight();

	// Remove trailing comment.
	int nPos = 0;
	if ( -1 != (nPos = (strLine.Find(_T('#'), 0)))){
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	if ((0 == WhereIsToken(strLine, ctstrWAIT))){

		// Remove extraneous info.
		// Leaving only the time tag.
		// strLine.MakeUpper ();

		// Remove "WAIT" directive
		if (-1 != (nPos = WhereIsToken(strLine, ctstrWAIT))){
			int nLen = _tcsclen(ctstrWAIT);
			strLine.Delete(nPos, nLen);
		}

		// Remove semicolon if present.
		if ( -1 != (nPos = strLine.Find(_T(';'), 0))){
			strLine.Delete(nPos, 1);
		}

		// Remove "schedx_bias" from wait timetag.
		CString strToken(_T("$"));
		strToken += ctstrSCHEDX_BIAS;
		if ( -1 != (nPos = strLine.Find(strToken, 0))){
			// Remove SCHEDX_BIAS variable.
			strLine.Delete(nPos, strToken.GetLength());
			if ( -1 != (nPos = strLine.Find(_T('+'), 0))){
				// remove "+"
				strLine.Delete(nPos, 1);
			}
		}

		// Determine if relative or absolute time and remove operator.
		bool bValidTime = false;
		bool bRelTime   = true;
		if ( -1 != (nPos = strLine.Find(_T('@'), 0))){
			bRelTime= false;
			bValidTime = true;
		} else if ( -1 != (nPos = strLine.Find(_T('+'), 0))){
			bRelTime= true;
			bValidTime = true;
		} 

		strLine.Delete(nPos, 1);

		// Remove "time" function
		if (-1 != (nPos = WhereIsToken(strLine, ctstrTIME))){
			int nLen = _tcslen(ctstrTIME);
			// ctstrTIME.GetLength ()
			strLine.Delete(nPos, nLen);
		}

		// Remove rest of operators from timetag.
		bool bDone = false;
		while (!bDone){
			if (-1 != (nPos = strLine.Find(_T('('), 0))){
				strLine.Delete(nPos, 1);
			} else if ( -1 != (nPos = strLine.Find(_T('\"'), 0))){
				strLine.Delete(nPos, 1);
			} else if ( -1 != (nPos = strLine.Find(_T(')'), 0))){
				strLine.Delete(nPos, 1);
			} else {
				bDone = true;
			}
		}
	
			
		// If timetag exists, validate.
		strLine.TrimLeft();
		strLine.TrimRight();
		CSchedTimeSpan cSchedTimeSpanTemp;
		
		if (!strLine.IsEmpty()){
			bValidTime = true;
			if (!bRelTime){
				CSchedTime cSchedTimeTemp;
				if (TRUE == cSchedTimeTemp.Create(strLine)){
					m_cPrevSchedTime = m_cCurrSchedTime;
					m_cCurrSchedTime = cSchedTimeTemp;
				} else {
					eError_Status = FALSE;
					CString	strErrorMsg = cSchedTimeTemp.GetLastErrorText();
				//	GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
				//	GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
					m_NewGblValData.WriteValidationMsg(m_nCurrentMasterSchedLine, 4, strErrorMsg);
				}
			}  else {
			
				if (TRUE == cSchedTimeSpanTemp.Create(strLine)){
					m_cPrevSchedTime = m_cCurrSchedTime;
					m_cCurrSchedTime = m_cCurrSchedTime + cSchedTimeSpanTemp;
				} else {
					eError_Status = FALSE;
					CString	strErrorMsg = cSchedTimeSpanTemp.GetLastErrorText();
					// GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
					// GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
					m_NewGblValData.WriteValidationMsg(m_nCurrentMasterSchedLine, 4, strErrorMsg);
				} 
			}
		} else {
			eError_Status = FALSE;
			m_NewGblValData.WriteValidationMsg(m_nCurrentMasterSchedLine, 4, strLine);
		}

		if (!bRelTime && bValidTime){
			// Absolute time tag.
			CString strCMDLine;
			CString strTemp;
			strCMDLine.Format(_T("%s %s %s%0.2f%s%s"),ctstrCMD, ctstrCMDWAITTOD,
				ctstrTIMEEQ, m_cCurrSchedTime.GetSchedTimeFromNoonInSecs(),
				ctstrRT, GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC]);
			strTemp.Format(_T("%d, %d, , , %s, %s, %f, %d, %d"), m_nCurrentLisLine,
				nNextLisLine, strCMDLine, m_cPrevSchedTime.GetTimeString(TRUE),
				GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC], m_nPrevLisLine,
				(m_nCurrentMasterSchedLine + m_nPrologLines) * m_nMasterSchedLineNumFlag);
			m_pMapFile->AppendText(strTemp);
			m_cPrevSchedTime = m_cCurrSchedTime;
			m_nPrevLisLine  = m_nCurrentLisLine;
		} else if ( bRelTime && bValidTime){
		
			CString strCMDLine;
			strCMDLine.Format(_T("%s %s %s%0.1f%s%s"), ctstrCMD, ctstrCMDPAUSE,
				ctstrPAUSE_TIMEEQ, cSchedTimeSpanTemp.GetSchedTimeInSeconds(), ctstrRT,
				GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC]);
			CString strTemp;
			strTemp.Format(_T("%d, %d, , , %s, %s, %f, %d, %d"), m_nCurrentLisLine,
				nNextLisLine, strCMDLine, m_cPrevSchedTime.GetTimeString(TRUE), 
				GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC], m_nPrevLisLine,
				(m_nCurrentMasterSchedLine + m_nPrologLines) * m_nMasterSchedLineNumFlag);
			m_pMapFile->AppendText(strTemp);
			m_cPrevSchedTime = m_cCurrSchedTime;
			m_nPrevLisLine = m_nCurrentLisLine;
		}

	} else if (0 == WhereIsToken(strLine, ctstrSCANDIR)){

			// Remove directive
			int nLen = _tcsclen(ctstrSCANDIR);
			strLine.Delete(0, nLen);
			strLine.TrimLeft();
			strLine.TrimRight();

			CPassedParam PP_Data;
			PP_Data.Process_PP(strLine);

			CString strCMDLine;
			CString strObjIdVal;
			CString strRepeatVal;
			CString strSel_InstrVal;
			CString strSpaceLkReqVal;
			CString strFrame_Priority;
			CString strLoad_Reg;
			CString strSnd_BBCal_Suppress;
		//	CString strSnd_Dwell_Mode;
			CString strImg_Scan_Mode;
			CString strBBCal;
			CString strExecute;

			for ( int i = 0; i < PP_Data.GetParamSize(); i++){

				CString strBuffer = PP_Data.GetParamName(i);
				strBuffer.MakeUpper();
				if (0 == strBuffer.CompareNoCase(ctstrOBJID)){
					strObjIdVal = ctstrFRAME_IDXEQ;
					strObjIdVal += PP_Data.GetParamValue(i);
				} else if (0 == strBuffer.CompareNoCase(ctstrREPEAT)){
					strRepeatVal = ctstrREPSEQ;
					strRepeatVal += PP_Data.GetParamValue(i);
				} else if (0 == strBuffer.CompareNoCase(ctstrINSTRUMENT)){
					strSel_InstrVal = ctstrSELECTED_INSTREQ;
					strSel_InstrVal += PP_Data.GetParamValue(i);
				} else if (0 == strBuffer.CompareNoCase(ctstrSIDE)){
					strSpaceLkReqVal = ctstrSPACE_LOOK_REQEQ;
					strSpaceLkReqVal += PP_Data.GetParamValue(i);
				} else if (0 == strBuffer.CompareNoCase(ctstrPRIORITY) ){
					strFrame_Priority = ctstrFRAME_PRIORITYEQ;
					strFrame_Priority += PP_Data.GetParamValue(i);
				} else if (0 == strBuffer.CompareNoCase(ctstrMODE)){
					strImg_Scan_Mode += PP_Data.GetParamValue(i);
				} else if (0 == strBuffer.CompareNoCase(ctstrLOAD_REG)){
					strExecute  = ctstrLOAD_REGEQ;
					strExecute += PP_Data.GetParamValue(i);
				} else if (0 == strBuffer.CompareNoCase(ctstrBBCAL) ){
					strBBCal  = ctstrSND_BBCAL_SUPPRESSEQ;
					strBBCal += PP_Data.GetParamValue(i);
				} 
			}

			strCMDLine.Format(_T("%s %s %s %s %s %s %s %s %s"), ctstrCMD, ctstrSCANEXE,
				strObjIdVal, strFrame_Priority, strSel_InstrVal, strLoad_Reg, strRepeatVal,
				strSpaceLkReqVal, strExecute);

			if (!strBBCal.IsEmpty()){
				strCMDLine += strBBCal + _T(" ");					
			}
			
			if (-1 != WhereIsToken(strSel_InstrVal, ctstrIMGR)){
				strCMDLine += ctstrIMG_SCAN_MODEEQ;
				strCMDLine += strImg_Scan_Mode;
			} else if (-1 != WhereIsToken(strSel_InstrVal, ctstrSDR)){
				strCMDLine += ctstrSND_DWELL_MODEEQ;
				strCMDLine += strImg_Scan_Mode;
			}

			strCMDLine += ctstrRT + GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC];
			CString strTemp;
			strTemp.Format(_T("%d, %d, , , %s, %s, %f, %d, %d"), m_nCurrentLisLine,
				nNextLisLine, strCMDLine, m_cCurrSchedTime.GetTimeString(TRUE),
				GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC], m_nPrevLisLine,
				(m_nCurrentMasterSchedLine + m_nPrologLines) * m_nMasterSchedLineNumFlag);
			m_pMapFile->AppendText(strTemp);
			float fCmdDurTime = 
				GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC] * (float)GetAppRegSC()->m_nRetryCnt[GetAppRegSite()->m_nDefaultSC];
			m_cCurrSchedTime = m_cCurrSchedTime + fCmdDurTime;
			m_cPrevSchedTime = m_cCurrSchedTime;
			m_nPrevLisLine = m_nCurrentLisLine;

	} else if (0 == WhereIsToken(strLine, ctstrCMD)){

		// Replace $QBACE_SEL with RT address in registry.		
		strLine.Replace(ctstr$QBACE_SEL, GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC]); 

		int nCTCUID = 0;
		if ((nCTCUID = GetAppRegSC()->m_nCTCUID[GetAppRegSite()->m_nDefaultSC]) != -1){
			CString strCTCUID;
			strCTCUID.Format(_T("%d"), nCTCUID); // Replace with value from the registry.
			strLine.Replace(ctstr$QBCTCU_SEL, strCTCUID); 
		} else {
			strLine.Replace(ctstr$QBCTCU_SEL, _T(" ")); // CTCU ID not specified replace with space. 
		}

		CString strTemp;
		strTemp.Format(_T("%d, %d, , , %s, %s, %f, %d, %d"), m_nCurrentLisLine, 
			nNextLisLine, strLine, m_cCurrSchedTime.GetTimeString(TRUE),
			GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC], m_nPrevLisLine,
			(m_nCurrentMasterSchedLine + m_nPrologLines) * m_nMasterSchedLineNumFlag);
		m_pMapFile->AppendText(strTemp);
		float fCmdDurTime = GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC] * (float)GetAppRegSC()->m_nRetryCnt[GetAppRegSite()->m_nDefaultSC];
		m_cCurrSchedTime  = m_cCurrSchedTime + fCmdDurTime;
		m_cPrevSchedTime  = m_cCurrSchedTime;
		m_nPrevLisLine    = m_nCurrentLisLine;
		
	} else if (0 == WhereIsToken(strLine, ctstrSTARDIR)){

		CString strTemp;
		// Remove directive
		int nLen = _tcsclen(ctstrSTARDIR);
		strLine.Delete(0, nLen);
		strLine.TrimLeft();
		strLine.TrimRight();

		CPassedParam PP_Data;
		PP_Data.Process_PP(strLine);

		CString strCMDLine;
		CString strObjIdVal;
		CString strSel_InstrVal;
		CString strLoadReg;

		for ( int i = 0; i < PP_Data.GetParamSize(); i++){
			CString strBuffer = PP_Data.GetParamName(i);
			strBuffer.MakeUpper();

			if (0 == strBuffer.CompareNoCase(ctstrOBJID)){
				strObjIdVal += ctstrFIRST_STAR_IDXEQ; 
				strObjIdVal += PP_Data.GetParamValue(i);
			} else if (0 == strBuffer.CompareNoCase(ctstrINSTRUMENT)){
				strSel_InstrVal += ctstrINSTRUMENTEQ;
				strSel_InstrVal += PP_Data.GetParamValue(i);
			} else if (0 == strBuffer.CompareNoCase(ctstrLOAD_REG) ){
				strLoadReg += ctstrLOAD_REGEQ;
				strLoadReg += PP_Data.GetParamValue(i);
			}
		}

		strCMDLine.Format(_T("%s %s %s %s %s %s%s"), ctstrCMD, ctstrSTAREXE,
			strObjIdVal, strSel_InstrVal, strLoadReg, ctstrRT, 
			GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC]);

		strTemp.Format(_T("%d, %d, , , %s, %s, %f, %d, %d"), m_nCurrentLisLine,
			nNextLisLine, strCMDLine, m_cCurrSchedTime.GetTimeString(TRUE),
			GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC], m_nPrevLisLine,
			(m_nCurrentMasterSchedLine + m_nPrologLines) * m_nMasterSchedLineNumFlag);

		m_pMapFile->AppendText(strTemp);
		float fCmdDurTime = GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC]
			* (float)GetAppRegSC()->m_nRetryCnt[GetAppRegSite()->m_nDefaultSC];
		m_cCurrSchedTime = m_cCurrSchedTime + fCmdDurTime;
		m_cPrevSchedTime = m_cCurrSchedTime;
		m_nPrevLisLine   = m_nCurrentLisLine;
	} else if (0 == WhereIsToken(strLine, ctstrRTCSDATA)){

		CString strTemp;
		// Remove directive
		int nLen = _tcsclen(ctstrRTCSDATA);
		strLine.Delete(0, nLen);
		strLine.TrimLeft();
		strLine.TrimRight();

		CPassedParam PP_Data;
		PP_Data.Process_PP(strLine);

		CString strCMDLine;
		CString strMoveTopCmd;

		bool bFound = false;
		for ( int i = 0; i < PP_Data.GetParamSize() && !bFound; i++){
			CString strBuffer = PP_Data.GetParamName(i);
			strBuffer.MakeUpper();

			if (0 == strBuffer.CompareNoCase(ctstrADDRESS)){
//				int nTemp = StrToInt((LPCTSTR)PP_Data.GetParamValue(i));
				// CString strCMDFormat = GetAppRegSC()->m_strSTOGotoTopCmd[GetAppRegSite()->m_nDefaultSC];
				strMoveTopCmd.Format(_T("%s %s%s"),ctstrMovTopPtr, ctstrStoTopIdx, PP_Data.GetParamValue(i));
				bFound = true;
			} 
		}

		strCMDLine.Format(_T("%s %s %s%s"), ctstrCMD, strMoveTopCmd, ctstrRT,
			GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC]);

		strTemp.Format(_T("%d, %d, , , %s, %s, %f, %d, %d"), m_nCurrentLisLine,
			nNextLisLine, strCMDLine, m_cCurrSchedTime.GetTimeString(TRUE),
			GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC], m_nPrevLisLine,
			(m_nCurrentMasterSchedLine + m_nPrologLines) * m_nMasterSchedLineNumFlag);

		m_pMapFile->AppendText(strTemp);
		float fCmdDurTime = GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC]
			* (float)GetAppRegSC()->m_nRetryCnt[GetAppRegSite()->m_nDefaultSC];
		m_cCurrSchedTime = m_cCurrSchedTime + fCmdDurTime;
		m_cPrevSchedTime = m_cCurrSchedTime;
		m_nPrevLisLine  = m_nCurrentLisLine;
	}
 	return eError_Status;
}

/***************************** 
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 09/18/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :	GetValues
// 
//  Description : Get routine that retrieves member variables.  
//                  
//  Return :	   void	
//                      
//  Parameters :  int		 *nCurrentMasterSchedLine,
//				  int		 *nCurrentLisLine,
//				  int		 *nNextLisLine,
//				  int		 *nPrevLisLine,
//                int		 *nRtnLine,
//				  CSchedTime *cCurrSchedTime,
//				  CSchedTime *cPrevSchedTime,
//                bool       bGrdLine;
//  
//  Note :          
//                  
//////////////////////////////////////////////////////////////////////////
*****************************/
void CSTOL::GetValues(int *nCurrentMasterSchedLine,
					  int *nCurrentLisLine,
					  int *nPrevLisLine,
                      int *nRtnLine,
					  CSchedTime *cCurrSchedTime,
					  CSchedTime *cPrevSchedTime){

	*nCurrentMasterSchedLine = m_nCurrentMasterSchedLine;
	*nCurrentLisLine         = m_nCurrentLisLine;
	*nPrevLisLine            = m_nPrevLisLine;
	*nRtnLine                = m_nRtnLine; 
	*cCurrSchedTime          = m_cCurrSchedTime;
	*cPrevSchedTime          = m_cPrevSchedTime;
};

/***********************************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 09/18/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :	   SetValues()
//
//  Description :   Set function that sets the memeber variables.
//                  
//  Return :      void
//                 
//  Parameters :  int nCurrentMasterSchedLine
//				  int nCurrentLisLine
//				  int nNextLisLine
//				  int nPrevLisLine
//                int nRtnLine
//				  CSchedTime cCurrSchedTime
//				  CSchedTime cPrevSchedTime
//  
//  Note :          
//                  
//////////////////////////////////////////////////////////////////////////
*************************************/
void CSTOL::SetValues(int nCurrentMasterSchedLine,
					  int nCurrentLisLine,
					  int nPrevLisLine,
					  int nRtnLine,
					  CSchedTime cCurrSchedTime,
					  CSchedTime cPrevSchedTime){

	m_nCurrentMasterSchedLine = nCurrentMasterSchedLine;
	m_nCurrentLisLine         = nCurrentLisLine;
	m_nPrevLisLine            = nPrevLisLine;
	m_nRtnLine                = nRtnLine ;
	m_cCurrSchedTime          = cCurrSchedTime;
	m_cPrevSchedTime          = cPrevSchedTime;
};


/***************************************
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 09/18/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :	   CSTOL::ParsePassedParams()
//
//  Description :	This routine parses the passed paramters and associates
//					the value with the parameter variable. 
//                  
//  Return :      BOOL
//						TRUE	-	SUCCESS
//						FALSE	-	FAIL
//                 
//  Parameters :  
//					CString strVal - Passed parameters.  
//				  
//  Note :          
//                  
//////////////////////////////////////////////////////////////////////////
*******************************/
BOOL CSTOL::ParsePassedParams(CString strVal){

	BOOL eReturn_Status = TRUE;
//	bool bDone = false;
	int  nPos  = 0;
	CStringArray  strParamValue;
	CString strTemp;

	strVal.Replace(_T("("), _T(""));
	strVal.TrimLeft();
	strVal.TrimRight();
	nPos = strVal.Find(_T(','));
	while( nPos != -1){
		strParamValue.Add(strVal.Left(nPos));
		strVal = strVal.Right(strVal.GetLength() - nPos - 1);
		strVal.TrimLeft();
		strVal.TrimRight();
		nPos = strVal.Find(_T(','));
	}

	if (-1 != (nPos = strVal.Find(_T(')')))){
		strParamValue.Add(strVal.Left(nPos));
	} 

	/*
		Get number of passed parameters in file
		Compare to values passed in
	 */
	int nNumValuesFound = strParamValue.GetSize();
	CObArray  ObjPassedParam;
	ObjPassedParam.Copy(GetPassedParamObj());
	int nNumParam       = ObjPassedParam.GetSize();

	if ( nNumValuesFound != nNumParam){
		// output an error
		eReturn_Status = FALSE;
	} else {
		for (int i = 0; i < nNumValuesFound; i++){
			strctPassedValueList *pObjParam = new strctPassedValueList; 
			pObjParam->strValue = strParamValue[i];
//			CString strName = ((strctPassedParamList *)ObjPassedParam[i])->strName;
			pObjParam->strName = ((strctPassedParamList *)ObjPassedParam[i])->strName;
//			pObjParam->strName =  strName;      
			m_objParam.Add((CObject *)pObjParam); 
		}
	}
	
	return eReturn_Status;
}

