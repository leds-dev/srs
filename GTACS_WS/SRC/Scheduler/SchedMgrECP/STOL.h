//////////////////////////////////////////////////////////////////////
// STOL.h : interface for the STOL class.               
// (c) 2001 Frederick J. Shaw   
// modifications:
//                                         
//////////////////////////////////////////////////////////////////////

#ifndef __STOL_H__
#define __STOL_H__

#include "NewGblValData.h"
#include "PassedParam.h"
#include "SchedTime.h"

#define _NO_PROGRESS 1

#if _MSC_VER 
#pragma once
#endif // _MSC_VER 

typedef	struct PassedValueList{
	       CString strName;      // Passed parameter name from ##param
		   CString strValue;     // Value listed on the proc name.
	}strctPassedValueList;

class CSTOL : public CNewSchedFile
{ 

// Construction
public:

	CSTOL(CString strSTOLFileName, CTextFile *pLisFile, CTextFile *pMapFile);
	~CSTOL();
	BOOL Expand(); // Create list and map file.
	BOOL ParsePassedParams(CString strVal);



	void SetSTOLPath(CString strSTOLPath){m_UNCSTOLPath.Full() = strSTOLPath;};
	COXUNC GetSTOLPath(void){return m_UNCSTOLPath;};
	void SetSearchPath(CStringArray &strSearchPathArray){m_SearchPathArray.Copy(strSearchPathArray);};
	CStringArray & GetSearchPath(void){return m_SearchPathArray;};

	void SetTimeTag(CSchedTime CTimeTag){m_cCurrSchedTime = CTimeTag;};
	CSchedTime GetTimeTag(void){return m_cCurrSchedTime;};
	
	void SetMapFilePtr(CTextFile * pMapFile){m_pMapFile = pMapFile;};
	CTextFile *GetMapFilePtr(void){return m_pMapFile;};
	void SetLisFilePtr(CTextFile * pLisFile){m_pLisFile = pLisFile;};
	CTextFile *GetLisFilePtr(void){return m_pLisFile;};
	int GetMasterSchedLineNumFlag(){return m_nMasterSchedLineNumFlag;};
	void SetMasterSchedLineNumFlag(int nFlag){m_nMasterSchedLineNumFlag = nFlag;};

	void GetValues(int *nCurrentMasterSchedLine,
					  int *nCurrentLisLine,
					  int *nPrevListLine,
                      int *nRtnLine,
					  CSchedTime *cCurrSchedTime,
					  CSchedTime *cPrevSchedTime);

	void SetValues(int nCurrentMasterSchedLine,
					  int nCurrentLisLine,
					  int nPrevListLine,
                      int nRtnLine,
					  CSchedTime cCurrSchedTime,
					  CSchedTime cPrevSchedTime);

// Operations

// Attributes
protected:
	BOOL Process_StartLine(CString strSTOLLine);
	BOOL AppendMapFile(CString strLine);

private:
	
	CNewGblValData	m_NewGblValData;
	CTextFile *		m_pMapFile;
	CTextFile *		m_pLisFile;
	COXUNC			m_UNCSTOLPath;
	COXUNC			m_UNCSearchPath;
	int				m_nCurrentMasterSchedLine;
	int             m_nCurrentLisLine;
	int             m_nNextLisLine;
	int             m_nPrevLisLine;
	int             m_nRtnLine;
	int             m_nPrologLines;
	int             m_nMasterSchedLineNumFlag;  // If negative one then in a nested proc.
	CSchedTime		m_cCurrSchedTime, m_cPrevSchedTime;
	CStringArray    m_SearchPathArray;
	CObArray        m_objParam;
}; 

#endif  // __STOL_H__
