/******************
//////////////////////////////////////////////////////////////////////
// SchedGen.cpp : implementation file
// (c) 20001 Frederick J. Shaw 
//	Prolog updated                                     //
//////////////////////////////////////////////////////////////////////
******************/

#include "stdafx.h"
#include "schedmgrecp.h"
#include "SchedGen.h"
#include "CmnHdr.h"
#include "SearchConstants.h"
#include "SchedTime.h"
#include "PassedParam.h"
#include "RegistryGbl.h"
#include "RegistryUsr.h"
#include "ProgressWnd.h"
#include "cls.h"
#include "stol.h"
#include "NewSchedFile.h"
#include "GenLoad.h"

/***************************
/////////////////////////////////////////////////////////////////////////////
// SchedGen message handlers
/////////////////////////////////////////////////////////////////////////////
// CSchedGen

// enum eFileType {CLS, STOL, FRAME, STAR, IMC, STO, RTCS-CLS, RTCS-STO, RPT, INV, UNDEF};
// enum eOpenFT   {INV, RW, CR, RO, PR};
// enum eCloseFT  {WRITE, NOWRITE};

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// The CSchedGen class 

//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 04-25-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		SchedGen::SchedGen
//					 	
//	Description :	Constructor	
//		
//	Return :		
//				void	-	none	
//	Parameters :	
//				const CString strInputFileName	-	Name of input file
//				const CNewSchedFile::eFileType eFileType -	File type
//
//	Note :			
//////////////////////////////////////////////////////////////////////////
******************************/
CSchedGen::CSchedGen(const CString strInputFileName,
					 const CNewSchedFile::eFileType eFileType /* = UNDEF */){
	m_strInputFileName =  strInputFileName;
	m_eFileType        =  eFileType;
	m_nDayOfYear = 1;
	m_nYear = 1971;
}

//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 04-25-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : CSchedGen::Gen_ProdFile	
//					 
//	Description :	This top level routine calls the appropriate classes
//                  to generate the product(s) files.	
//		
//	Return : BOOL
//					TRUE	-	SUCCESS
//					FALSE	-	FAIL		
//					
//	Parameters : 
//					void	-	none	
//
//	Note :			
//////////////////////////////////////////////////////////////////////////
BOOL CSchedGen::Gen_ProdFile()
{
	BOOL eReturnStatus = TRUE;
	// enum eFileType {CLS, STOL, FRAME, STAR, IMC, RTCS-SET,  RPT, INV, UNDEF}
	
	// generate based on file type
	switch (m_eFileType)
	{
		case CNewSchedFile::CLS :
		{
			// CLS: Product => CMD proc. 

			CString strTitle (_T("Generating a STOL procedure from a CLS."));
//			GetAppOutputBar()->InitBuild(strTitle, _T(""));
			GetAppOutputBar()->OutputToEvents(strTitle, COutputBar::INFO);
			CFileException Exception;
			CString strProdFileName, strSTOLName;

			// Verify initial values are valid. 
			if (m_strInputFileName.IsEmpty()){
				eReturnStatus = FALSE;
				CString strErrorMsg = _T("CLS file name is empty.");
				GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			} else if (m_strStolFileName.IsEmpty()){
				eReturnStatus = FALSE;
				CString strErrorMsg = _T("STOL file name is empty.");
				GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			} else {
				CTextFile     STOL;
				eValidLoc     eVLoc    = INVALID_TYPE;
				eValidStatus  eVStatus = INVALID_STATUS;
				if (!STOL.Load(m_strStolFileName, CTextFile::CR, &Exception)){
					eReturnStatus = FALSE;
					CString strErrorMsg = STOL.ExceptionErrorText(&Exception);
					CString strMsg;
					strMsg.Format(_T("%s. file: %s"), strErrorMsg, m_strStolFileName);
					GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
				} else {
					CCLS  CLS(m_strInputFileName, &STOL);
					if (!CLS.Load(m_strInputFileName, CTextFile::RO, &Exception)){
						eReturnStatus = FALSE;
						CString strErrorMsg = CLS.ExceptionErrorText(&Exception);
						CString strMsg;
						strMsg.Format(_T("%s. file: %s"), strErrorMsg, m_strInputFileName);
						GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
					} else {
						if (TRUE != CLS.IsFileValid(eVStatus, eVLoc)){
							eReturnStatus = FALSE;
							CString strErrorMsg = _T("The CLS file has a invalid prolog.");
							CString strMsg;
							strMsg.Format(_T("%s. file: %s"), strErrorMsg, m_strInputFileName);
							GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
						} else if ((PASSED != eVStatus && WARNING != eVStatus)
								&& (TRUE != CLS.IsOverRideSet())) {  
							eReturnStatus = FALSE;
							CString strErrorMsg = _T("The CLS file failed validation or is unvalidated.");
							CString strMsg;
							strMsg.Format(_T("%s. file: %s"), strErrorMsg, m_strInputFileName);
							GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::FATAL);
						} else {

							if ((WARNING == eVStatus) && (TRUE != CLS.IsOverRideSet())){
								CString strErrorMsg = _T("The CLS's validation status is warning.");
								CString strMsg;
								strMsg.Format(_T("%s. file: %s"), strErrorMsg, m_strInputFileName);
								GetAppOutputBar()->OutputToBuild(strMsg, COutputBar::WARNING);
								strErrorMsg = _T("Schedule generation will proceed.");
								GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::INFO);
							}

							CProlog *pCLSProlog = CLS.GetProlog();
							STOL = *pCLSProlog;	
							CLS.SetCLSLocation(eVLoc);
																			
							CPassedParam PPData;
							CLS.SetPassedParamData(&PPData);

							CSchedTime CTimeTag;
							if (TRUE != CTimeTag.Create(m_nYear, m_nDayOfYear, 0, 0, 0, 0)){
								eReturnStatus = FALSE;
								CString strErrorMsg=CTimeTag.GetLastErrorText();
								GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
							} else {
								CLS.SetTimeTag(CTimeTag);
								// Start expanding the CLS and generate the master schedule
								if (TRUE !=	CLS.Expand()){
									eReturnStatus = FALSE;
								}
							}
							if (TRUE != STOL.WriteValidationStatus(UNVALIDATED, eVLoc)){
								CString strErrorMsg;
								eReturnStatus = FALSE;
								strErrorMsg.Format(_T("Error writing validation status to file. %s"), m_strInputFileName);
								GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::WARNING);
							}
						}
					
						if (!CLS.Close(&Exception)){
							eReturnStatus = FALSE;
							CString strErrorMsg;
							strErrorMsg.Format(_T("Closing File: %s returned an error, %s"), m_strInputFileName, CLS.ExceptionErrorText(&Exception));
							GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
						}
					}
				}
				
				if (!STOL.Close(CTextFile::WRITE, &Exception)){
					eReturnStatus = FALSE;
					CString strErrorMsg;
					strErrorMsg.Format(_T("Closing File: %s returned an error, %s"), m_strStolFileName, STOL.ExceptionErrorText(&Exception));
					GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
				}
			}
			
			if (eReturnStatus == FALSE){
				CString strErrorMsg;
				strErrorMsg.Format(_T("Errors generating master schedule:%s"), m_strInputFileName);
				GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
				// Remove the product file since errors occured.
				CFile::Remove(m_strStolFileName);
			}
		}
		break;

		case CNewSchedFile::STOL :
		{
			/*****************
			STOL:   Product => lis, map, upload, bin
					create a list file.
					need to open and extract all STOL procs recursively.
					from the list file create a map file.
					extract all mnemonics. 
					convert directives (wait, scan, star).
					send genload directive to GTACS.
			********************/

			CFileException Exception;
			CSchedTime CTimeTag;
			// Verify initial values are valid. 
			if (m_strInputFileName.IsEmpty()){
				eReturnStatus = FALSE;
				CString strErrorMsg = _T("STOL file name is empty.");
				GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			} else {
				CTextFile     Lis;
				CTextFile     Map;
				eValidLoc     eVLoc = INVALID_TYPE;
				eValidStatus  eVStatus = INVALID_STATUS;
				if (!Lis.Load(m_strLisFileName, CTextFile::CR, &Exception)){
					eReturnStatus = FALSE;
					CString strErrorMsg;
					strErrorMsg.Format(_T("Loading file: %s returned an error, %s"),
						m_strLisFileName, Lis.ExceptionErrorText(&Exception));
					GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
				} else if (!Map.Load(m_strMapFileName, CTextFile::CR, &Exception)){
					eReturnStatus = FALSE;
					CString strErrorMsg;
					strErrorMsg.Format(_T("Loading file: %s returned an error, %s"),
						m_strMapFileName, Map.ExceptionErrorText(&Exception));
					GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
				} else {
					CSTOL  Prc(m_strInputFileName, &Lis, &Map);
					if (TRUE != CTimeTag.Create(m_nYear, m_nDayOfYear, 0, 0, 0, 0)){
							eReturnStatus = FALSE;
							CString strErrorMsg=CTimeTag.GetLastErrorText();
							GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
					} else {

						Prc.SetSTOLPath(m_strStolFileName);
						
						Prc.SetTimeTag(CTimeTag);
						CString strHdr(_T("2.0, STOLMAP"));
						Map.AppendText(strHdr);

						strHdr.Format(_T(""));
						Map.AppendText(strHdr);
						
						strHdr = _T("2.0, LIST");
						Lis.AppendText(strHdr);

						strHdr.Format(_T(""));
						Lis.AppendText(strHdr);

						if (!Prc.Load(m_strInputFileName, CTextFile::RO, &Exception)){
							eReturnStatus = FALSE;
							CString strErrorMsg;
							strErrorMsg.Format(_T("Loading File: %s returned an error, %s"),
								m_strInputFileName, Prc.ExceptionErrorText(&Exception));
							GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
						} else if (TRUE != Prc.IsFileValid(eVStatus, eVLoc)){
							eReturnStatus = FALSE;
							CString strErrorMsg= _T("The STOL file is invalid.");
							strErrorMsg.Format(_T("The STOL file: %s is invalid."), m_strInputFileName);
							GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
						} else if ((eVLoc != ONBOARD && eVLoc != BOTH)){
							eReturnStatus = FALSE;
							CString strErrorMsg;
							strErrorMsg.Format(_T("The STOL file: %s is not validated for on-board execution."),
								m_strStolFileName);
							GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
						} else if ((PASSED != eVStatus && WARNING != eVStatus) 
								&& (TRUE != Prc.IsOverRideSet())){  
							eReturnStatus = FALSE;
							CString strErrorMsg;
								strErrorMsg.Format(_T("The STOL file: %s failed validation or is unvalidated."),
									m_strStolFileName);
							GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
						} else {
							if ((WARNING == eVStatus) && (TRUE != Prc.IsOverRideSet())){
								CString strErrorMsg;
								strErrorMsg.Format(_T("The STOL file: %s validation is warning."),
									m_strStolFileName);
								GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::WARNING);
								strErrorMsg = _T("Schedule generation will proceed.");
								GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::INFO);
							}

							CWaitCursor wait;   // display wait cursor
							if (TRUE !=	Prc.Expand()){
								// Prc.WriteValidationMsg(Prc.GetCurrentFileLineNum (), 72);
								eReturnStatus = FALSE;
							} 
												
							time_t ltime;
							time (&ltime);
							DWORD nUserNameSize = UNLEN + 1;
							TCHAR cUserName[UNLEN + 1];
							::GetUserName (cUserName, &nUserNameSize);
							strHdr.Format(_T("%d, %ld, %s"), Map.GetLineCount() - 2, ltime, cUserName);
							Map.SetAt(1, strHdr);
							
							strHdr.Format(_T("%d, %ld, %s"), Lis.GetLineCount(), ltime, cUserName);
							Lis.SetAt(1, strHdr);
						}
			
						if (!Prc.Close(&Exception)){
							eReturnStatus = FALSE;
							CString strErrorMsg;
							strErrorMsg.Format(_T("Closing File: %s returned an error, %s"),
								m_strStolFileName, Prc.ExceptionErrorText(&Exception));
							GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
						}
				
						if (!Lis.Close(CTextFile::WRITE, &Exception)){
							eReturnStatus = FALSE;
							CString strErrorMsg;
							strErrorMsg.Format(_T("Closing File: %s returned an error, %s"),
								m_strLisFileName, Lis.ExceptionErrorText(&Exception));
							GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
						}

						if (!Map.Close(CTextFile::WRITE, &Exception)){
							eReturnStatus = FALSE;
							CString strErrorMsg = Map.ExceptionErrorText(&Exception);
							strErrorMsg.Format(_T("Closing File: %s returned an error, %s"),
								m_strMapFileName, Map.ExceptionErrorText(&Exception));
							GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
						}
					}
					if (TRUE == eReturnStatus){
						CGenLoad GenLoadMap(m_strMapFileName, CGenLoad::SKB_LDTYPE);
						if (!GenLoadMap.Send_GenLD_Dir()) {
							eReturnStatus = FALSE;
						}
					}
				}
			}

			if (eReturnStatus == FALSE){
				CString strErrorMsg;
				strErrorMsg.Format(_T("Errors generating on board schedule:%s"), m_strStolFileName);
				GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
				GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
				// Remove the product files since errors occured.
				CFile::Remove(m_strMapFileName);
				CFile::Remove(m_strLisFileName);
			}
		}
		break;
		
		case CNewSchedFile::STO :
		{
			/********************
			STO:    Product => upload, bin
					create a map file.
					convert directives (wait). 
					send genload directive to GTACS.
			*********************/
		}
		break;

		default:
			// Ouput an error; invalid generate file type.
			eReturnStatus = FALSE;
			CString strErrorMsg;
			strErrorMsg.Format(_T("Invalid generate file type."));
			GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
		break;
	}
	return eReturnStatus;
}
