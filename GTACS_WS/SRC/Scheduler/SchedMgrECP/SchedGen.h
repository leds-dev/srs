#if !defined(AFX_SCHEDGEN_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_)
#define AFX_SCHEDGEN_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SchedGen.h : header file
//

#define _NO_PROGRESS 1
#include "NewSchedFile.h"

/////////////////////////////////////////////////////////////////////////////
// SchedGen window

// 	typedef enum eFileType {CLS, STOL, FRAME, STAR, IMC, RTCS_CLS, RTCS_STO, RPT, INV, UNDEF};

class CSchedGen 
{
// Construction
public:
	
	CSchedGen(const CString strInputFileName, const CNewSchedFile::eFileType eFileType = CNewSchedFile::UNDEF);

	virtual ~CSchedGen(void){};
	BOOL Gen_ProdFile();
	int  GetDayOfYear(void){return m_nDayOfYear;};
	int  GetYear(void){return m_nYear;};
	void SetDayOfYear(const int nDOY){m_nDayOfYear = nDOY;};
	void SetYear(const int nYear){m_nYear = nYear;};
	CString    GetInputFileName(void){return m_strInputFileName;};
	void       SetInputFileName(CString &strFileName){m_strInputFileName = strFileName;};
	CString    GetStolFileName(void){return m_strStolFileName;};
	void       SetStolFileName(CString &strFileName){m_strStolFileName = strFileName;};
	CString    GetLisFileName(void){return m_strLisFileName;};
	void       SetLisFileName(CString &strFileName){m_strLisFileName = strFileName;};
	CString    GetMapFileName(void){return m_strMapFileName;};
	void       SetMapFileName(CString &strFileName){m_strMapFileName = strFileName;};

private:

	int     m_nDayOfYear;
	int     m_nYear;
	CString m_strInputFileName;
	CString m_strStolFileName;
	CString m_strLisFileName;
	CString m_strMapFileName;
	CNewSchedFile::eFileType m_eFileType;
};


/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCHEDGEN_H__ED47D101_3259_11D5_8AF0_00104B8CF942__INCLUDED_)
