/**
//////////////////////////////////////////////////////////////////////////
// SchedMgrECP.cpp : Defines the class behaviors for the application.
// (c) 2001 Frederick J. Shaw
//	Prolog Updated
//
// Modified: DRobinson / Elarson   Jan 2009 
// Corrected so will work with Service Pack 3
// Moved the CoInitializeSecurity call to beginning of process
//
// Revision History:													   
// Manan Dalal - July-August 2014										   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   
// Release in Build16.2
//////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "MainFrm.h"
#include "ChildFrm.h"
#include "CodeMaxDoc.h"
#include "CodeMaxView.h"
#include "LanguageSyntax.h"
#include "EpCommon_i.c"
#include "EpochCreateInstance.h"
#include "TextFile.h"
#include "InstObjDoc.h"
#include "InstObjView.h"
#include "MapDoc.h"
#include "MapView.h"
#include "CommUtil.h"

#include "OXRegistryItem.h"
#include "OXIteratorRegistryItem.h"
#include "OXCmdOpt.h"
#include "OXSplashWnd.h"
#include "Credits.h"
#include "SchedMgrECP.h"

#include <process.h>
#include <Adshlp.h>
#include <security.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

const IID IID_IADs					=
{0xFD8256D0, 0xFD15, 0x11CE, {0xAB,0xC4,0x02,0x60,0x8C,0x9E,0x75,0x53}};
const IID IID_IADsUser				=
{0x3E37E320, 0x17E2, 0x11CF, {0xAB,0xC4,0x02,0x60,0x8C,0x9E,0x75,0x53}};
const IID IID_IADsADSystemInfo		=
{0x5BB11929, 0xAFD1, 0x11D2, {0x9C,0xB9,0x00,0x00,0xF8,0x7A,0x36,0x9E}};

//////////////////////////////////////////////////////////////////////////
// CSchedMgrECPApp

BEGIN_MESSAGE_MAP(CSchedMgrECPApp, CWinApp)
	//{{AFX_MSG_MAP(CSchedMgrECPApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

//The following helps create an XEpochAliveCB and XEpStrmMonCB object that
//implements the _IEpochAliveCB and _IEpStrmMonCB interfaces.
BEGIN_INTERFACE_MAP(CSchedMgrECPApp, CWinApp)
	INTERFACE_PART(CSchedMgrECPApp, IID__IEpochAliveCB, EpochAliveCB)
	INTERFACE_PART(CSchedMgrECPApp, IID__IEpStrmMonCB,	EpStrmMonCB)
	INTERFACE_PART(CSchedMgrECPApp, IID__IEpochCB,		EpochCB)
END_INTERFACE_MAP()

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedMgrECPApp
//
//  Description :   Class constructor.
//                  
//  Return :
//					void	-	none.
//  Parameters :
//				  	void	-	none.
//  Notes :
//			
//////////////////////////////////////////////////////////////////////////
**/
CSchedMgrECPApp::CSchedMgrECPApp() : CBCGPWorkspace (TRUE /* bResourceSmartUpdate */),
	m_bRegServer(FALSE),
	m_bUnRegServer(FALSE)
{
	//Initialize member variables
	m_pUnkDB					= NULL;
	m_pUnkSM					= NULL;
	m_pEpDB						= NULL;
	m_pEpSMon					= NULL;
	m_pEpSTOL					= NULL;
	m_pCPCdb					= NULL;
	m_pCPCsm					= NULL;
	m_pCPCss					= NULL;
	m_pCPdb						= NULL;
	m_pCPsm						= NULL;
	m_pCPss						= NULL;
	m_hDevMode					= NULL;
	m_hDevNames					= NULL;
	m_ulDBHandle				= NULL;
	m_strDatabasePath.Empty();
	m_strDatabaseName.Empty();

	//Tell the object implementing _IEpochAliveCB and _IEpStrmMonCB that we are its parent
	m_xEpochAliveCB.m_pParent	= this;
	m_xEpStrmMonCB.m_pParent	= this;
	m_xEpochCB.m_pParent		= this;
	m_bTabFlatBorders			= FALSE;
	m_bMDITabs					= TRUE;
	m_bMDITabIcons				= TRUE;
	m_MDITabLocation			= CBCGPTabWnd::LOCATION_BOTTOM;
	*GetAppAdminMode()			= FALSE;
	*GetAppAdminPriv()			= FALSE;
	*GetAppSchedPriv()			= FALSE;
	m_bLoadDB					= true;
	m_NewGTACS					= TRUE; // First time through need to stop all services and map drive.
}
//////////////////////////////////////////////////////////////////////////
// The one and only CSchedMgrECPApp object

CSchedMgrECPApp theApp;
WSADATA			wsaData;

// This identifier was generated to be statistically unique for your app.
// You may change it if you prefer to choose a specific identifier.

// {A464C528-D502-11D4-800D-00609704053C}
static const CLSID clsid =
{ 0xa464c528, 0xd502, 0x11d4, { 0x80, 0xd, 0x0, 0x60, 0x97, 0x4, 0x5, 0x3c } };


/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::InitInstance()
//
//  Description :   Override InitInstance to initialize each new instance
//					of your application running under Windows.. 
//                  
//  Return :        BOOL 
//						Nonzero if initialization is successful; otherwise 0.
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
BOOL CSchedMgrECPApp::InitInstance()
{

	HRESULT			hr;
	CString			strMsg;

	// Initialize OLE libraries
	if( !AfxOleInit() )
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

//	Normally would need to call CoInitializeEx here, but it is redundant 
//	because AfxOleInit calls it.

	hr = CoInitializeSecurity(NULL,							// Points to security descriptor
							-1,								// Count of entries in asAuthSvc
							NULL,
							NULL,
							RPC_C_AUTHN_LEVEL_CONNECT,		// The default authentication
															// level for proxies
							RPC_C_IMP_LEVEL_IMPERSONATE,	// The default impersonation
															// level for proxies
							NULL,				// Reserved; must be set to NULL
							EOAC_NONE,			// Additional client or server-side capabilities
							NULL				// Reserved for future use
							);
	if( FAILED(hr) )
		AfxMessageBox(IDP_COINITSEC_FAILED);


	// Set the server timeout to a reasonable number for various servers.  If this
	// is exceeded we will get a "Server Busy" dialog box
	AfxOleGetMessageFilter()->SetMessagePendingDelay(600000);
	AfxOleGetMessageFilter()->SetRetryReply(1000);
	AfxOleGetMessageFilter()->EnableBusyDialog( FALSE);
	AfxOleGetMessageFilter()->EnableNotRespondingDialog(FALSE);

	AfxEnableControlContainer();
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

// #ifdef _AFXDLL
// 	Enable3dControls();			// Call this when using MFC in a shared DLL
// #else
//	Enable3dControlsStatic();	// Call this when linking to MFC statically
// #endif
	AfxInitRichEdit();
	//The registry settings are stored in the following locations:
	//HKEY_CURRENT_USER\Software\<company name>\<app>\<section name>\<value name>.
	//The following line sets the company name that is used by all registry
	//routines (this includes CWinApp, CRegistry, and internal BCG registry classes).
	SetRegistryKey(strRegCompanyKey);
	GetAppRegFolder()->Init();
	GetAppRegSC()->Init();
	GetAppRegSite()->Init();
	GetAppRegGTACS()->Init();
	GetAppRegRSO()->Init();
	GetAppRegOATS()->Init();
	GetAppRegMisc()->Init();
	GetAppRegInstObj()->Init();
	GetAppRegMapObj()->Init();
	GetAppRegMakeSched()->Init();
	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	InitSkinManager();
	// TODO:: Need to correct this
//	GetSkinManager()->EnableSkinsDownload (_T("http://www.bcgsoft.com/Skins"));

	if( !InitAndStartSockets(&wsaData) )
	{
		LPVOID lpMsgBuf;
		FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL, GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&lpMsgBuf, 0, NULL);

		CString strMsg(_T("Could not start socket library. "));
		strMsg += (LPCTSTR)lpMsgBuf;
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
		LocalFree(lpMsgBuf);
	}

	//The following line is used by hte BCG Registry functions only - it is not
	//used by CWinApp or any of the CRegistry derived classes
	SetRegistryBase(_T("Settings"));

	// Initialize all Managers for usage. They are automatically constructed
	// if not yet present
	InitMouseManager();
	InitContextMenuManager();
	InitKeyboardManager();

	// Enable user-defined tools. If you want allow more than 10 tools,
	// add tools entry to resources (ID_USER_TOOL11, ID_USER_TOOL12,...)
	EnableUserTools(ID_TOOLS_ENTRY, ID_USER_TOOL1, ID_USER_TOOL10, RUNTIME_CLASS (CSchedMgrECPUserTool));

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CMRegisterControl();
	LoadCodeMaxProfile(NULL);
	m_pDocTemplate[nSTOL] = new CSchedMgrMultiDocTemplate(
		IDR_STOLTYPE,
		RUNTIME_CLASS(CSTOLDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CSTOLView));
	AddDocTemplate(m_pDocTemplate[nSTOL]);

	m_pDocTemplate[nCLS] = new CSchedMgrMultiDocTemplate(
		IDR_CLSTYPE,
		RUNTIME_CLASS(CCLSDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CCLSView));
	AddDocTemplate(m_pDocTemplate[nCLS]);

	m_pDocTemplate[nRpt] = new CSchedMgrMultiDocTemplate(
		IDR_RPTTYPE,
		RUNTIME_CLASS(CRptDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CRptView));
	AddDocTemplate(m_pDocTemplate[nRpt]);

	m_pDocTemplate[nRTCSMap] = new CSchedMgrMultiDocTemplate(
		IDR_MAPTYPE,
		RUNTIME_CLASS(CRTCSMapDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CRTCSMapView));
	AddDocTemplate(m_pDocTemplate[nRTCSMap]);

	m_pDocTemplate[nSTOLMap] = new CSchedMgrMultiDocTemplate(
		IDR_MAPTYPE,
		RUNTIME_CLASS(CSTOLMapDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CSTOLMapView));
	AddDocTemplate(m_pDocTemplate[nSTOLMap]);

	m_pDocTemplate[nSector] = new CSchedMgrMultiDocTemplate(
		IDR_SECTORTYPE,
		RUNTIME_CLASS(CSectorObjDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CSectorObjView));
	AddDocTemplate(m_pDocTemplate[nSector]);

	m_pDocTemplate[nFrame] = new CSchedMgrMultiDocTemplate(
		IDR_FRAMETYPE,
		RUNTIME_CLASS(CFrameObjDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CFrameObjView));
	AddDocTemplate(m_pDocTemplate[nFrame]);

	m_pDocTemplate[nStar] = new CSchedMgrMultiDocTemplate(
		IDR_STARTYPE,
		RUNTIME_CLASS(CStarObjDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CStarObjView));
	AddDocTemplate(m_pDocTemplate[nStar]);

	m_pDocTemplate[nBinary] = new CSchedMgrMultiDocTemplate(
		IDR_BINTYPE,
		RUNTIME_CLASS(CBinDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CBinView));
	AddDocTemplate(m_pDocTemplate[nBinary]);

	m_pDocTemplate[nIMC] = new CSchedMgrMultiDocTemplate(
		IDR_IMCTYPE,
		RUNTIME_CLASS(CIMCDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CIMCView));
	AddDocTemplate(m_pDocTemplate[nIMC]);

	m_pDocTemplate[nRTCSSet] = new CSchedMgrMultiDocTemplate(
		IDR_RTCSSETTYPE,
		RUNTIME_CLASS(CRTCSSetDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CRTCSSetView));
	AddDocTemplate(m_pDocTemplate[nRTCSSet]);

	m_pDocTemplate[nRTCS] = new CSchedMgrMultiDocTemplate(
		IDR_RTCSTYPE,
		RUNTIME_CLASS(CRTCSDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CRTCSView));
	AddDocTemplate(m_pDocTemplate[nRTCS]);

	m_pDocTemplate[nLis] = new CSchedMgrMultiDocTemplate(
		IDR_LISTYPE,
		RUNTIME_CLASS(CLisDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CLisView));
	AddDocTemplate(m_pDocTemplate[nLis]);

	m_pDocTemplate[nRTCSCmd] = new CSchedMgrMultiDocTemplate(
		IDR_RTCSCMDTYPE,
		RUNTIME_CLASS(CRTCSCmdDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CRTCSCmdView));
	AddDocTemplate(m_pDocTemplate[nRTCSCmd]);

	m_pDocTemplate[nPaceCmd] = new CSchedMgrMultiDocTemplate(
		IDR_PACECMDTYPE,
		RUNTIME_CLASS(CPaceCmdDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CPaceCmdView));
	AddDocTemplate(m_pDocTemplate[nPaceCmd]);

	m_pDocTemplate[nSTOLCmd] = new CSchedMgrMultiDocTemplate(
		IDR_STOLCMDTYPE,
		RUNTIME_CLASS(CSTOLCmdDoc),
		RUNTIME_CLASS(CChildFrame), // custom MDI child frame
		RUNTIME_CLASS(CSTOLCmdView));
	AddDocTemplate(m_pDocTemplate[nSTOLCmd]);

	m_bBackupPriv = AddBackupPrivilege() ? TRUE : FALSE;

	// Connect the COleTemplateServer to the document template.
	//  The COleTemplateServer creates new documents on behalf
	//  of requesting OLE containers by using information
	//  specified in the document template.
	m_server.ConnectTemplate(clsid, m_pDocTemplate[nSTOL], FALSE);

	// Register all OLE server factories as running.  This enables the
	//  OLE libraries to create objects from other applications.
	COleTemplateServer::RegisterAll();
	// Note: MDI applications register all server objects without regard
	//  to the /Embedding or /Automation on the command line.

	// Parse command line for standard shell commands, DDE, file open

	/*
	// TODO:: - uncomment out this code
	COXCommandLine		cCmdLine(m_lpCmdLine);
	COXCommandOptions	cCmdOptions;
	cCmdOptions.Add(_T("nodb"),		COD_REP_LAST, _T("Do not connect to database service"));
	cCmdOptions.Add(_T("nosplash"), COD_REP_LAST, _T("Do not show the splash screen"));
#ifdef _DEBUG
	cCmdOptions.Add(_T("admin"),	COD_REP_LAST, _T("Force administrator privilege "));
	cCmdOptions.Add(_T("sched"),	COD_REP_LAST, _T("Force scheduler privilege "));
#endif
	cCmdOptions.ParseCmdLine(cCmdLine);
	m_bLoadDB = !cCmdOptions.IsEnabled(_T("nodb"));

	if( !cCmdOptions.IsEnabled(_T("nosplash")) )
	{
		COXSplashWnd::EnableSplashScreen(TRUE);
		COXSplashWnd::LoadGlobalBitmap(IDB_SPLASH, CLR_NONE);
		COXSplashWnd::ShowGlobal(1000);
	}

	*/

	// create main MDI Frame window
	CMainFrame* pMainFrame = new CMainFrame;
	if( !pMainFrame->LoadFrame(IDR_MAINFRAME) )
		return FALSE;
	m_pMainWnd = pMainFrame;

	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();

	//The following two lines need to be debugged.  They should allow a user to double click
	//on a file to open it but it doesn't seem to work right now!
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	//Prevent blank view appearing at startup, as described in red brick
	//	if (cmdInfo.m_nShellCommand == CCommandLineInfo::FileNew)
	//		cmdInfo.m_nShellCommand = CCommandLineInfo::FileNothing;

	// Update registry for Epoch services

//	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
//	CString	strSite			= GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef];
//	CString	strShare		= GetAppRegGTACS()->m_strDBShare[nGTACSSiteDef][nGTACSDef];
//	CString strDBPath;
//	strDBPath.Format(_T("\\\\%s\\%s"), strSite, strShare);

	CString	strDBName 	= GetAppRegMisc()->m_strDBDrive +
						  _T("\\") + GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC] +
						  _T("\\") + GetAppRegSC()->m_strDatabaseName[GetAppRegSite()->m_nDefaultSC] +
						  _T(".lis");

	// When a server application is launched stand-alone, it is a good idea
	//  to update the system registry in case it has been damaged.
	// m_server.UpdateRegistry(OAT_DISPATCH_OBJECT);
	COleObjectFactory::UpdateRegistryAll();

	// Dispatch commands specified on the command line
	//	if (!ProcessShellCommand(cmdInfo))
	//		return FALSE;

	LoadCustomState();

	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	// Get the current user's ADS object
	TCHAR szDeviceName[80];
	DWORD dwResult, cchBuff = sizeof(szDeviceName);

	
	// Verify the R: is mapped 
	// Call the WNetGetConnection function.
	//
	dwResult = WNetGetConnection(_T("R:"), (LPWSTR) szDeviceName, &cchBuff);
	CString strMsg2;

	switch (dwResult)
	{
    // Print the connection name or process errors.
    case NO_ERROR:
//		strMsg2.Format(_T("No errors"));
//        TextOut(hdc, 10, 10, (LPSTR) szDeviceName,
//            lstrlen((LPSTR) szDeviceName));
//		GetAppOutputBar()->OutputToEvents(strMsg2, COutputBar::INFO);
        break;
 
    // The device is not a redirected device.
    case ERROR_NOT_CONNECTED:
		strMsg2.Format(_T("The R: drive is not connected."));
		GetAppOutputBar()->OutputToEvents(strMsg2, COutputBar::FATAL);
//        TextOut(hdc, 10, 10, "Device z: not connected.", 24);
		break;
    
    // The device is not currently connected,
    //  but it is a persistent connection.
    case ERROR_CONNECTION_UNAVAIL:
		strMsg2.Format(_T("The R: drive is unavailable."));
		GetAppOutputBar()->OutputToEvents(strMsg2, COutputBar::FATAL);
//        TextOut(hdc, 10, 10, "Connection unavailable.", 23);
		break;
    //
    // Call an application-defined error handler.
    //
    default:
		strMsg2.Format(_T("WNetGetConnection returned error # %d."), dwResult);
		GetAppOutputBar()->OutputToEvents(strMsg2, COutputBar::FATAL);
//      NetErrorHandler(hwnd, dwResult, (LPSTR)"WNetGetConnection");
		break;
	}

	if ( NO_ERROR != dwResult){
		AfxMessageBox(_T("Error accessing the R: drive.\nPlease close this application and remap the drive."));
		return FALSE;
	}

// Verify the S: drive is mapped and retrieve the server name.

	// Get the current user's ADS object
//	CString				strMsg;
//	TCHAR szDeviceName[80];
//	DWORD dwResult, cchBuff = sizeof(szDeviceName);

	// Call the WNetGetConnection function.
	//
	dwResult = WNetGetConnection(_T("S:"), (LPWSTR)szDeviceName, &cchBuff);
//	CString strMsg2;

	switch (dwResult)
	{
    case NO_ERROR:
//		strMsg2.Format(_T("No errors"));
//        TextOut(hdc, 10, 10, (LPSTR) szDeviceName,
//            lstrlen((LPSTR) szDeviceName));
//		GetAppOutputBar()->OutputToEvents(strMsg2, COutputBar::INFO);
        break;
    //
    // The device is not a redirected device.
    //
    case ERROR_NOT_CONNECTED:
		strMsg2.Format(_T("The S: drive is not connected."));
		GetAppOutputBar()->OutputToEvents(strMsg2, COutputBar::FATAL);
//        TextOut(hdc, 10, 10, "Device z: not connected.", 24);
		break;
    //
    // The device is not currently connected,
    //  but it is a persistent connection.
    //
    case ERROR_CONNECTION_UNAVAIL:
		strMsg2.Format(_T("The S: drive is unavailable."));
		GetAppOutputBar()->OutputToEvents(strMsg2, COutputBar::FATAL);
//        TextOut(hdc, 10, 10, "Connection unavailable.", 23);
		break;
    //
    // Call an application-defined error handler.
    //
    default:
		strMsg2.Format(_T("WNetGetConnection returned error # %d."), dwResult);
		GetAppOutputBar()->OutputToEvents(strMsg2, COutputBar::FATAL);
//      NetErrorHandler(hwnd, dwResult, (LPSTR)"WNetGetConnection");
		break;
	}

	if ( NO_ERROR != dwResult){
		AfxMessageBox(_T("Error accessing the S: drive.\nPlease close this application and remap the drive."));
		return FALSE;
	}

	// Create the stream name from the device name.
	m_strGrdStrmName.Format(_T("%c%c_ge"), szDeviceName[2], szDeviceName[9]);
//	szDeviceName


	CString				sUser(_T("LDAP://"));

//	Removed the CoInitialize and CoInitializeSecurity calls (which were here)
//	to the top so they get correctly set before the RegisterAll call sets the 
//	security to the default values (which won't work with SSAPI and Service Pack 3)
  		
	IADsUser*			pUser	= NULL;
	IADsADSystemInfo*	pSys	= NULL;
	CLSID				clsid;
	BSTR				bstr;

	CLSIDFromProgID(_T("ADSystemInfo"), &clsid);
	hr = CoCreateInstance(clsid,NULL,CLSCTX_INPROC_SERVER,IID_IADsADSystemInfo,(void**)&pSys);

	if( FAILED(hr) )
	{
		strMsg = _T("CoCreateInstance Returned an error!");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
	}

	hr = pSys->get_UserName(&bstr);
	if( FAILED(hr) )
	{
		strMsg = _T("get_UserName Returned an error!");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
	} 

	_bstr_t bstrStart(bstr);
	

    CString s;
    s.Format(_T("%s"), (LPCTSTR)bstrStart);

//	sUser += (LPCTSTR)bstr;
    sUser += s;

	USES_CONVERSION;

	hr = ADsGetObject(T2W((LPTSTR)((LPCTSTR)sUser)),IID_IADsUser,(void**)&pUser);
	
	if( SUCCEEDED(hr) )
	{
		// What groups do the user belong to...
		pUser->AddRef();
		hr = IsUserInGroups(pUser);
	}

	if( FAILED(hr) )
	{
		strMsg = _T("Unknown ADS user object!");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
	}

	SysFreeString(bstr);

	if( pUser ) 
		pUser->Release();
	
	//Commented out the following because it is done in ExitInstance
	//CoUninitialize();

	// TODO::  

	//Connect to the database server, if not already done in a DDE-activated
	//document.  We can't do this any sooner, because the framework seems to
	//hate COM calls while things are still initializing!?
	if( m_pEpSMon == NULL )
	{
		hr = ConnectToServerSM();

		if( FAILED(hr) )
		{
			strMsg = _T("Could not connect to the stream monitor server. ");
			strMsg+= _T("Verify that the server is running and has been properly licensed.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
			strMsg =_T("No connection to the Stream Monitor Server is being made, ");
			strMsg+=_T("stream monitor functions are disabled.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
		}
	}

	if( m_pEpSTOL == NULL )
	{
		hr = ConnectToServerSS();

		if( FAILED(hr) )
		{
			strMsg = _T("Could not connect to the stream services server. ");
			strMsg+= _T("Verify that the server is running and has been properly licensed.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
			strMsg =_T("No connection to the Stream Services Server is being made, ");
			strMsg+=_T("stream monitor functions are disabled.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
		}
	}

	if( m_bLoadDB )
	{
		if( m_pEpDB == NULL )
		{
			hr = ConnectToServerDB();

			if( FAILED(hr) )
			{
				strMsg.Format(IDS_EPCONNECTFAIL);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
				strMsg.Format(IDS_EPDBNODBSERVICE);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
				m_bLoadDB = FALSE;
			}
			else
			{
				CWaitCursor cWaitCursor;
				COXSplashWnd::HideGlobal();
				m_bLoadDB = LoadDatabase();
			}
		}
		else
			m_bLoadDB = TRUE;
	}
	else
	{
		strMsg.Format(IDS_EPDBNODBSERVICE);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	}

	// Override checking of the user's group!  (Kind of a back door for maintenance)

	/*
	TODO:: - Remove this comment
	if( cCmdOptions.IsEnabled(_T("admin")) )
		*GetAppAdminPriv() = TRUE;
	if( cCmdOptions.IsEnabled(_T("sched")) )
		*GetAppSchedPriv() = TRUE;
	*/

	CMUnregisterAllLanguages();
	CMLANG_STOL.pszOperators = (LPCTSTR)m_strCmdList;
	CMRegisterLanguage(_T("STOL"),				&CMLANG_STOL);
	CMRegisterLanguage(_T("CLS"),				&CMLANG_CLS);
	CMRegisterLanguage(_T("Report"),			&CMLANG_RPT);
	CMRegisterLanguage(_T("RTCS"),				&CMLANG_RTCS);
	CMRegisterLanguage(_T("RTCS Set"),			&CMLANG_RTCSSET);
	CMRegisterLanguage(_T("Pace Command File"),	&CMLANG_RTCSCMD);
	CMRegisterLanguage(_T("RTCS Command File"),	&CMLANG_PACECMD);
	CMRegisterLanguage(_T("STOL Command File"),	&CMLANG_STOLCMD);

	if( !m_bBackupPriv )
	{
		strMsg.Format(IDS_NOBACKUPPRIV);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
		m_bLoadDB = FALSE;
	}

	DWORD nUserNameSize = UNLEN + 1;
	TCHAR cUserName[UNLEN + 1];
	::GetUserName(cUserName, &nUserNameSize);

	strMsg.Format(_T("The user [%s] %s Admin privileges."),
		cUserName, *GetAppAdminPriv() ? _T("has") : _T("does not have"));
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	strMsg.Format(_T("The user [%s] %s Scheduler privileges."),
		cUserName, *GetAppSchedPriv() ? _T("has") : _T("does not have"));
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

HRESULT CSchedMgrECPApp::IsUserInGroups(IADsUser* pUser)
{
	IADsMembers*	pGroups;
	HRESULT			hr = S_OK;

	hr = pUser->Groups(&pGroups);
	pUser->Release();
	
	if( FAILED(hr) ) 
		return hr;

	IUnknown* pUnk;
	
	hr = pGroups->get__NewEnum(&pUnk);

	if( FAILED(hr) ) 
		return hr;

	pGroups->Release();

	IEnumVARIANT* pEnum;
	
	hr = pUnk->QueryInterface(IID_IEnumVARIANT,(void**)&pEnum);
	
	if( FAILED(hr) ) 
		return hr;

	pUnk->Release();

	// Now Enumerate
	BSTR		bstr;
	VARIANT		var;
	ULONG		lFetch;
	IADs*		pADs;
	IDispatch*	pDisp;
	CString		strAdminGroup;
	CString		strSchedGroup;
	CString		strGroup;
	int			index = -1;

	strAdminGroup.LoadString(IDS_ADMIN_GROUP);
	strSchedGroup.LoadString(IDS_SCHED_GROUP);

	VariantInit(&var);

	hr = pEnum->Next(1, &var, &lFetch);

	while( hr == S_OK )
	{
		if( lFetch == 1 )
		{
			pDisp	= V_DISPATCH(&var);
			pDisp->QueryInterface(IID_IADs, (void**)&pADs);
			pADs->get_Name(&bstr);
			strGroup= bstr;

			if( (index = strGroup.Find(_T("="), 0)) > -1 )
				strGroup.Delete(0, index+1);

			if( strGroup.CompareNoCase(strAdminGroup) == 0 )
				*GetAppAdminPriv() = TRUE;

			if( strGroup.CompareNoCase(strSchedGroup) == 0 )
				*GetAppSchedPriv() = TRUE;

			SysFreeString(bstr);
			pADs->Release();
		}

		VariantClear(&var);
		pDisp	= NULL;
		hr		= pEnum->Next(1, &var, &lFetch);
	}

	hr = pEnum->Release();
	
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders     Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::InitAndStartSockets
//
//  Description :   Call this function to initialize Windows Sockets.
//					 
//                  
//  Return :        BOOL 
//						TRUE	SUCCESS
//						FALSE	FAIL
//  Parameters : 
//				  	WSADATA* pwsaData	-	A pointer to a WSADATA structure.
//					If lpwsaData is not equal to NULL, then the address of 
//					the WSADATA structure is filled by the call to 
//					::WSAStartup. 
//  Notes : 
//	This function also ensures that ::WSACleanup is called for you before 
//	the application terminates.
//			
//////////////////////////////////////////////////////////////////////////
**/
BOOL CSchedMgrECPApp::InitAndStartSockets(WSADATA* pwsaData)
{
	if( !AfxSocketInit() )
		return false;

	if( WSAStartup(0x101, pwsaData) != 0 )
		return false;

	return true;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::LoadDatabase()
//
//  Description :   Loads GTACS database.
//					 
//                  
//  Return :        BOOL 
//						TRUE	-	success
//						FALSE	-	fail
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
BOOL CSchedMgrECPApp::LoadDatabase()
{
	BOOL	bRtnStatus = TRUE;

//	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
//	CString	strSite			= GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef];
//	CString	strShare		= GetAppRegGTACS()->m_strDBShare[nGTACSSiteDef][nGTACSDef];
//	CString strDBPath;
//	strDBPath.Format(_T("\\\\%s\\%s"), strSite, strShare);

	// If a different gtacs has been requested, need to unmap and stop all services.
	// Update registry and start up services.

	CString	strDBName 	= GetAppRegMisc()->m_strDBDrive +
						  _T("\\") + GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC] +
						  _T("\\") + GetAppRegSC()->m_strDatabaseName[GetAppRegSite()->m_nDefaultSC] +
						  _T(".lis");

	// These variables are only used to compare old values with the new!
	m_strDatabaseName.Replace(_T('/'), _T('\\'));
//	m_strDatabasePath.Replace(_T('/'), _T('\\'));
	// If the database name doesn't change, then don't reload it.  There is a potential
	// problem here
//	if( (!m_strDatabasePath.IsEmpty()) &&
//		(!m_strDatabaseName.IsEmpty()) &&
//		(m_strDatabaseName.CompareNoCase(strDBName) == 0) &&
//		(m_strDatabasePath.CompareNoCase(strDBPath) == 0)){
//		return TRUE;
//	}

	m_strDatabaseName.Empty();
//	m_strDatabasePath.Empty();
	CWaitCursor	cWaitCursor;
	HRESULT	hr = 0;

	IEpEnumCommands* pEnumCmds;		// Database cmd enumerator

	CSchedMgrECPApp* pApp = (CSchedMgrECPApp*)AfxGetApp();
	CString strMsg;

	if (m_ulDBHandle != NULL){
		(pApp->m_pEpDB)->CloseDatabase(m_ulDBHandle);
	}

	// If we are invoked vie DDE, then the server connection is not here yet
	if( (bRtnStatus) && (pApp->m_pEpDB == NULL) )
	{
		hr = pApp->ConnectToServerDB();

		if( FAILED(hr) )
		{
			strMsg.Format(IDS_EPCONNECTFAIL);
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			bRtnStatus = FALSE;
		}
	}

	if( bRtnStatus )
	{
		// Make sure that the pathname does not contain any spaces (NuTCRACKER doesn't like them)
		if( strDBName.Find(_T(" ")) != -1 )
		{
			strMsg.Format(IDS_EPINVALIDPATH);
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			bRtnStatus = FALSE;
		}
	}

	if( bRtnStatus )
	{
		// Replace all backslashes with forward slashes
		strDBName.Replace(_T('\\'), _T('/'));

		BSTR bstrDBName = SysAllocString(strDBName);

		strMsg.Format(IDS_EPOPENDB, strDBName);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

		// Attempt to open the database
		hr = (pApp->m_pEpDB)->OpenDatabase(bstrDBName, pApp->m_dwCookieDB, &m_ulDBHandle);
		SysFreeString(bstrDBName);
	}

	if( (bRtnStatus) && (FAILED(hr)) )
	{
		strMsg.Format(IDS_EPOPENFAIL);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		m_ulDBHandle = NULL;
		LPVOID lpMsgBuf;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);
		strMsg = (LPTSTR)lpMsgBuf;
		strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		LocalFree(lpMsgBuf);
		bRtnStatus = FALSE;
	}

	if( (bRtnStatus) && (SUCCEEDED(hr)) )
	{
		m_strDatabaseName = strDBName;
//		m_strDatabasePath = strDBPath;

		ULONG ulFetched;

		int nCmdCnt=0;
		int nCritCmdCnt=0;

		strMsg.Format(IDS_EPDBREADCMD);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

		GetAppCmdList()->Empty();
		GetAppCritCmdList()->Empty();

		EpDbCmdDescription cCmd[5000];
		hr = (pApp->m_pEpDB)->GetCommandListEnum(m_ulDBHandle, &pEnumCmds);

		while( TRUE )
		{
			hr = pEnumCmds->Next(1, cCmd, &ulFetched);
			if( ulFetched == 0 )  break;

			for( unsigned int nIndex=0; nIndex<ulFetched; nIndex++ )
			{
				if( cCmd[nIndex].bCritical )
				{
					nCritCmdCnt++;
					*GetAppCmdList() = *GetAppCmdList() + cCmd[nIndex].bstrMnemonic + _T("\n");
					*GetAppCritCmdList() = *GetAppCritCmdList() + cCmd[nIndex].bstrMnemonic + _T("\n");
				}
				else
				{
					nCmdCnt++;
					*GetAppCmdList() = *GetAppCmdList() + cCmd[nIndex].bstrMnemonic + _T("\n");
				}

				SysFreeString(cCmd[nIndex].bstrMnemonic);
				SysFreeString(cCmd[nIndex].bstrDescription);
				SysFreeString(cCmd[nIndex].bstrAlias);
				SysFreeString(cCmd[nIndex].bstrExecMnemonic);
				SysFreeString(cCmd[nIndex].bstrCmdFormat);
				SysFreeString(cCmd[nIndex].bstrSpacecraftAddr);
				SysFreeString(cCmd[nIndex].bstrPrivilegeGroup);
			}
		}

		pEnumCmds->Release();

		strMsg.Format(IDS_EPDBCMDTOTAL, nCmdCnt + nCritCmdCnt, nCritCmdCnt);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
		strMsg.Format(IDS_EPDBREADCOMPLETE, nCmdCnt);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	}

	if( bRtnStatus )
	{
		CMUnregisterLanguage(_T("STOL"));
		CMUnregisterLanguage(_T("RTCS"));
		CMLANG_STOL.pszOperators = (LPCTSTR) m_strCmdList;
		CMLANG_RTCS.pszOperators = (LPCTSTR) m_strCmdList;
		CMRegisterLanguage(_T("STOL"), &CMLANG_STOL);
		CMRegisterLanguage(_T("RTCS"), &CMLANG_RTCS);
	}

	return bRtnStatus;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Fred Shaw		Date : 05/11/07		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedMgrECPApp::GetLastErrorText
//  Description :
//	Return :		CString	-
//	Parameters :	None.
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CSchedMgrECPApp::GetLastErrorText()
{
	LPVOID lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				  FORMAT_MESSAGE_FROM_SYSTEM |
				  FORMAT_MESSAGE_IGNORE_INSERTS,
				  NULL,
				  GetLastError(),
				  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				  (LPTSTR) &lpMsgBuf,
				  0,
				  NULL);
	CString strMsg((LPTSTR)lpMsgBuf);
	strMsg.Remove(_T('\x0D'));
	strMsg.Remove(_T('\x0A'));
	LocalFree(lpMsgBuf);
	return strMsg;
}



/**

//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedMgrECPApp::ExitInstance
//
//  Return :        int
//						0 indicates no errors,
//
//				  	void	-	none.
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// CSchedMgrECPApp message handlers
**/
int CSchedMgrECPApp::ExitInstance()
{
	::BCGCBProCleanUp();

	if( m_ulDBHandle != NULL )
	{
		CSchedMgrECPApp* pApp = (CSchedMgrECPApp*)AfxGetApp();
		(pApp->m_pEpDB)->CloseDatabase(m_ulDBHandle);
	}

	m_ulDBHandle = NULL;
	if( !m_bUnRegServer )
		DisconnectFromServerDB();

	DisconnectFromServerSM();
	DisconnectFromServerSS();
	CoUninitialize();
	SaveCodeMaxProfile(NULL);

	/*
	// Remove drive mapping for Epoch services in the registry
	COXRegistryItem	cRegMappedDrives;
	cRegMappedDrives.SetFullRegistryItem(cRegMappedDrives.m_pszLocalMachine);
	cRegMappedDrives.SetKeyNames(ctcEPOCHMappedDrive);
	// cRegMappedDrives.SetKeyNames(_T("\\SOFTWARE\\ISI\\EpochParams\\MappedDrives"));

	// int nNumValues = cRegMappedDrives.GetNumberOfSubkeys();
	int nNumValues = cRegMappedDrives.GetNumberOfValues();
	// int nNumValues = 5;
//	cRegMappedDrives.SetStringValue(_T(""), GetAppRegMisc()->m_strDBDrive, false);
	// cRegMappedDrives.
	// cRegMappedDrives.SetValueName(GetAppRegMisc()->m_strDBDrive);
	// cRegMappedDrives.Delete();
	CString strRegName;
//	strRegName = cRegMappedDrives.EnumerateSubkey(0);
		strRegName = cRegMappedDrives.EnumerateSubkey(i);
		if (strRegName.CompareNoCase(_T("S:")) != 0){
			cRegMappedDrives.Delete();
		}
	}

	*/

//		TRACE1("Reg value : %s\n", (LPCTSTR)iterItem.GetValueName());
		// EnumerateValue(DWORD nIndex)


	// Save off values from the MakeSched dialogue windows
	GetAppRegMakeSched()->SetValues();

	return CWinApp::ExitInstance();
}

//////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();
// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	afx_msg void OnCredits();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders     Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CAboutDlg::CAboutDlg() : CDialog
//
//  Description :   Constructor
//					 
//                  
//  Return :        void	-	none
//						
//  Parameters : 
//				  	CAboutDlg::IDD	-	not used.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders    Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      void CAboutDlg::DoDataExchange
//
//  Description :   Called by the framework to 
//					exchange and validate dialog data.
//                  
//  Return :        void	-	none
//						
//  Parameters : 
//				  	CDataExchange* pDX	-	A pointer to a CDataExchange 
//					object.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
	ON_BN_CLICKED(ID_CREDIT, OnCredits)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      GetFileVersion 
//
//  Description :   This routine retrieves the software version number. 
//					 
//                  
//  Return :        
//				CString -	Software version number
//
//  Parameters : 
//				  	CString strFilePath	-	Path to	program 
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/

#if defined(DO_POINTLESS_WORK)

CString GetFileVersion (CString strFilePath)
{
	CString strVersion = _T("Unavailable");
	LPTSTR 	pcFilepath = strFilePath.GetBuffer(strFilePath.GetLength());
	DWORD 	nVersionInfoSize = GetFileVersionInfoSize(pcFilepath, 0);
	LPVOID	pData = malloc(nVersionInfoSize);

	if( GetFileVersionInfo(pcFilepath, 0, nVersionInfoSize, pData) )
	{
		VS_FIXEDFILEINFO* pcVersion;
		UINT	pnLen;
		VerQueryValue(pData, TEXT("\\"), (LPVOID*)&pcVersion, &pnLen);
		WORD nMajor		= HIWORD(pcVersion->dwFileVersionMS);
		WORD nMinor		= LOWORD(pcVersion->dwFileVersionMS);
		WORD nSpecific	= HIWORD(pcVersion->dwFileVersionLS);
		WORD nCustom	= LOWORD(pcVersion->dwFileVersionLS);
		strVersion.Format (_T("Version %d.%d.%d.%d"), nMajor, nMinor, nSpecific, nCustom);
	}

	free (pData);
	return strVersion;
}

#endif

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick J. Shaw       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CAboutDlg::OnInitDialog()
//
//  Description :   This member function is called in response to the
//					WM_INITDIALOG message. This message is sent to the
//					dialog box during the Create, CreateIndirect, or 
//					DoModal calls, which occur immediately before the
//					dialog box is displayed. 
//                  
//  Return :        BOOL
//						Specifies whether the application has set the
//						input focus to one of the controls in the dialog
//						box. If OnInitDialog returns nonzero, Windows sets
//						the input focus to the first control in the dialog
//						box. The application can return 0 only if it has
//						explicitly set the input focus to one of the
//						controls in the dialog box.
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// This is stupid.  The version string is already hardcoded in the .RC file
	// but for some bizarre reason we *open* the hardcoded filename and read it!
	// If someone really wants to see the binary values, he can bloody well
	// go into Windows Explorer and do a right-click-Properties-Version on the .EXE
#if defined(DO_POINTLESS_WORK)
	CString strVersion;
	strVersion.Format(_T("SchedMgrECP %s"), GetFileVersion(_T("SchedMgrECP.exe")));
	GetDlgItem(IDC_VERSION_STATIC)->SetWindowText(strVersion);
#endif

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/**
// App command to run the dialog
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders	Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      void CSchedMgrECPApp::OnAppAbout()
//
//  Description :   
//                  
//  Return :        void	-	none
//						
//  Parameters :	void	-	none.
//				  	
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
void CSchedMgrECPApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders      Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::PreLoadState
//
//  Description :    
//					 
//                  
//  Return :        void -	none
//						
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
void CSchedMgrECPApp::PreLoadState ()
{
	GetMouseManager()->AddView(IDR_STOLTYPE,	_T("STOL View"),				IDR_STOLTYPE);
	GetMouseManager()->AddView(IDR_FRAMETYPE,	_T("Frame View"),				IDR_FRAMETYPE);
	GetMouseManager()->AddView(IDR_SECTORTYPE,	_T("Sector View"),				IDR_SECTORTYPE);
	GetMouseManager()->AddView(IDR_STARTYPE,	_T("Star View"),				IDR_STARTYPE);
	GetMouseManager()->AddView(IDR_MAPTYPE,		_T("Map View"),					IDR_MAPTYPE);
	GetMouseManager()->AddView(IDR_RPTTYPE,		_T("Report View"),				IDR_RPTTYPE);
	GetMouseManager()->AddView(IDR_LISTYPE,		_T("Listing View"),				IDR_LISTYPE);
	GetMouseManager()->AddView(IDR_BINTYPE,		_T("Binary View"),				IDR_BINTYPE);
	GetMouseManager()->AddView(IDR_RTCSSETTYPE, _T("RTCS Set View"),			IDR_RTCSSETTYPE);
	GetMouseManager()->AddView(IDR_RTCSTYPE,	_T("RTCS View"),				IDR_RTCSTYPE);
	GetMouseManager()->AddView(IDR_RTCSCMDTYPE, _T("RTCS Cmd Restriction View"),IDR_RTCSCMDTYPE);
	GetMouseManager()->AddView(IDR_PACECMDTYPE, _T("Command Pacing View"),		IDR_PACECMDTYPE);
	GetMouseManager()->AddView(IDR_STOLCMDTYPE, _T("STOL Cmd Restriction View"),IDR_STOLCMDTYPE);


	// TODO: add another views and windows were mouse double click
	// customization is required

	// TODO: add another context menus here
	GetContextMenuManager()->AddMenu(_T("Workspace menu"),						IDR_CONTEXT_WORKSPACE_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace STOL menu"),					IDR_CONTEXT_WORKSPACE_STOL_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace CLS menu"),					IDR_CONTEXT_WORKSPACE_CLS_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace Report menu"),				IDR_CONTEXT_WORKSPACE_RPT_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace Sector menu"),				IDR_CONTEXT_WORKSPACE_SECTOR_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace Frame menu"),				IDR_CONTEXT_WORKSPACE_FRAME_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace Star menu"),					IDR_CONTEXT_WORKSPACE_STAR_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace Binary menu"),				IDR_CONTEXT_WORKSPACE_BIN_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace Map menu"),					IDR_CONTEXT_WORKSPACE_MAP_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace IMC menu"),					IDR_CONTEXT_WORKSPACE_IMC_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace RTCS Set menu"),				IDR_CONTEXT_WORKSPACE_RTCSSET_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace RTCS Cmd Restriction menu"), IDR_CONTEXT_WORKSPACE_RTCSCMD_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace Command Pacing menu"),		IDR_CONTEXT_WORKSPACE_PACECMD_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace Listing menu"),				IDR_CONTEXT_WORKSPACE_LIS_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace RTCS menu"),					IDR_CONTEXT_WORKSPACE_RTCS_MENU);
	GetContextMenuManager()->AddMenu(_T("Frame menu"),							IDR_CONTEXT_FRAME_MENU);
	GetContextMenuManager()->AddMenu(_T("Map menu"),							IDR_CONTEXT_MAP_MENU);
	GetContextMenuManager()->AddMenu(_T("Star menu"),							IDR_CONTEXT_STAR_MENU);
	GetContextMenuManager()->AddMenu(_T("Sector menu"),							IDR_CONTEXT_SECTOR_MENU);
	GetContextMenuManager()->AddMenu(_T("Map View Menu"),						IDR_CONTEXT_WORKSPACE_MAP_MENU);
	GetContextMenuManager()->AddMenu(_T("MDI Tabs menu"),						IDR_POPUP_MDI_TABS);
	GetContextMenuManager()->AddMenu(_T("Output Event menu"),					IDR_CONTEXT_OUTPUT_EVENT_MENU);
	GetContextMenuManager()->AddMenu(_T("Output Build menu"),					IDR_CONTEXT_OUTPUT_BUILD_MENU);
	GetContextMenuManager()->AddMenu(_T("CodeMax menu"),						IDR_CONTEXT_CODEMAX_MENU);
	GetContextMenuManager()->AddMenu(_T("Workspace STOL Cmd Restriction menu"), IDR_CONTEXT_WORKSPACE_STOLCMD_MENU);	
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders    Date : 08/20/2001       version 1.0
/////////////////////////////////////////////////////////////////////////////
// 
//  Function :      BOOL CSchedMgrECPApp::PreTranslateMessage
//
//  Description :   Override this function to filter window messages before 
//					they are dispatched to the Windows functions TranslateMessage
//					and DispatchMessage.   The default implementation performs
//		            accelerator-key translation, so you must call the
//                  CWinApp::PreTranslateMessage member function in your
//                  overridden version.
//
//  Return :        BOOL
//						Nonzero if the message was fully processed in
//						PreTranslateMessage and should not be processed further.
//						Zero if the message should be processed in the normal
//						way.
//
//  Parameters : 
//				  	MSG* pMsg	-	A pointer to a MSG structure that contains
//									the message to process.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
BOOL CSchedMgrECPApp::PreTranslateMessage(MSG* pMsg)
{
	return CWinApp::PreTranslateMessage(pMsg);
}

//////////////////////////////////////////////////////////////////////////
// CSchedMgrECPApp message handlers
/******************************************************************************
			XEpochAliveCB class methods:

  These methods implement IUnknown and _IEpochAliveCB.  No actual reference
  counting is performed because the lifetime of the object is controlled by
  this application, not COM or the server making calls.
******************************************************************************/
/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochAliveCB::AddRef()
//
//  Description :   EPOCH call back function 
//					 
//                  
//  Return :        STDMETHODIMP_(ULONG)
//						Always returns 2.
//						 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP_(ULONG) CSchedMgrECPApp::XEpochAliveCB::AddRef()
{
	return 2;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders     Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochAliveCB::Release()
//
//  Description :   EPOCH call back function. 
//                  
//  Return :        STDMETHODIMP_(ULONG) 
//						Always returns 2.
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP_(ULONG) CSchedMgrECPApp::XEpochAliveCB::Release()
{
	return 2;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochAliveCB::QueryInterface
//
//  Description :   EPOCH call back routine
//					 
//  Return :        STDMETHODIMP
//						S_OK	-	Success			
//						E_NOINTERFACE	-	FAIL						
//
//  Parameters : 
//				  	REFIID iid
//					void** ppvObj
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochAliveCB::QueryInterface(REFIID iid, void** ppvObj)
{
	if( iid == IID_IUnknown || iid == IID__IEpochAliveCB )
	{
		*ppvObj = &(m_pParent->m_xEpochAliveCB);
		AddRef();
		return S_OK;
	}

	return E_NOINTERFACE;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochAliveCB::AreYouAliveCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochAliveCB::AreYouAliveCB()
{
	return S_OK;
}

/******************************************************************************
			XEpStrmMonCB class methods:

  These methods implement IUnknown and _IEpStrmMonCB.  No actual reference
  counting is performed because the lifetime of the object is controlled by
  this application, not COM or the server making calls.
******************************************************************************/
/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpStrmMonCB::AddRef()
//
//  Description :   EPOCH call back function 
//					 
//                  
//  Return :        STDMETHODIMP_(ULONG)
//						Always returns 2.
//						 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP_(ULONG) CSchedMgrECPApp::XEpStrmMonCB::AddRef()
{
	return 2;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders     Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpStrmMonCB::Release()
//
//  Description :   EPOCH call back function. 
//                  
//  Return :        STDMETHODIMP_(ULONG) 
//						Always returns 2.
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP_(ULONG) CSchedMgrECPApp::XEpStrmMonCB::Release()
{
	return 2;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpStrmMonCB::QueryInterface
//
//  Description :   EPOCH call back routine
//					 
//  Return :        STDMETHODIMP
//						S_OK	-	Success			
//						E_NOINTERFACE	-	FAIL						
//
//  Parameters : 
//				  	REFIID iid
//					void** ppvObj
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpStrmMonCB::QueryInterface(REFIID iid, void** ppvObj)
{
	if( iid == IID_IUnknown || iid == IID__IEpStrmMonCB )
	{
		*ppvObj = &(m_pParent->m_xEpStrmMonCB);
		AddRef();
		return S_OK;
	}

	return E_NOINTERFACE;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpStrmMonCB::StrmInfoSrvcCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpStrmMonCB::StrmInfoSrvcCB(ULONG ulArrySize, EPSTRMINFO* siData)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpStrmMonCB::StrmMessageSrvcCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpStrmMonCB::StrmMessageSrvcCB(ULONG ulUserData,ULONG ulArrySize,BSTR* bstrData)
{
	return S_OK;
}

/******************************************************************************
			XEpochCB class methods:

  These methods implement IUnknown and _IEpochCB.  No actual reference
  counting is performed because the lifetime of the object is controlled by
  this application, not COM or the server making calls.
******************************************************************************/
/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::AddRef()
//
//  Description :   EPOCH call back function 
//					 
//                  
//  Return :        STDMETHODIMP_(ULONG)
//						Always returns 2.
//						 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP_(ULONG) CSchedMgrECPApp::XEpochCB::AddRef()
{
	return 2;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders     Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::Release()
//
//  Description :   EPOCH call back function. 
//                  
//  Return :        STDMETHODIMP_(ULONG) 
//						Always returns 2.
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP_(ULONG) CSchedMgrECPApp::XEpochCB::Release()
{
	return 2;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::QueryInterface
//
//  Description :   EPOCH call back routine
//					 
//  Return :        STDMETHODIMP
//						S_OK	-	Success			
//						E_NOINTERFACE	-	FAIL						
//
//  Parameters : 
//				  	REFIID iid
//					void** ppvObj
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::QueryInterface(REFIID iid, void** ppvObj)
{
	if( iid == IID_IUnknown || iid == IID__IEpochCB )
	{
		*ppvObj = &(m_pParent->m_xEpochCB);
		AddRef();
		return S_OK;
	}

	return E_NOINTERFACE;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::AreYouAliveCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::AreYouAliveCB()
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::ConStatServiceCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::ConStatServiceCB(EPSTATUSDATA conStatus)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::PointServiceCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::PointServiceCB(ULONG ulArrySize, EPPTDATA* ptData)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::FrameServiceCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::FrameServiceCB(EPFRMDATA frmData)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::EventServiceCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::EventServiceCB(ULONG ulArrySize, EPEVTDATA* evtData)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::STOLCompStatCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::STOLCompStatCB(EPSTOLRESP scStatus)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::CLIMStatusServiceCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::CLIMStatusServiceCB(ULONG ulUserData, EpCLIMStatusRec clsData)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::CmdStatusServiceCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::CmdStatusServiceCB(ULONG ulUserData, ULONG ulArrySize, EpCmdStatusRec* csData)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::LimitServiceCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::LimitServiceCB(ULONG ulUserData, ULONG ulArrySize, EPLIMITINFO* liData)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::DBModServiceCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::DBModServiceCB(ULONG ulUserData, ULONG ulArrySize, EPDBMODINFO* dbmData)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :Frederick Shaw    Date : 05/14/2003       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::XEpochCB::LimitOffServiceCB()
//
//  Description :   EPOCH Call back function 
//					 
//                  
//  Return :        int
//						0 indicates no errors,
//						values greater than 0 indicate an error. 
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
STDMETHODIMP CSchedMgrECPApp::XEpochCB::LimitOffServiceCB(ULONG ulUserData, EPLIMITOFFINFO loiData)
{
	return S_OK;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::GetNextToken
//
//  Description :    This function is extracts the next token from 
//                   the character string passed in and returns the
//		             token.
//                  
//  Return :        CString
//						returns the next token.  If there is no next token,
//						strNextToken.Length() will equal zero.
//
//  Parameters :	CString 
//				  			is a CString variable that is parsed for
//							the next token.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
CString CSchedMgrECPApp::GetNextToken(CString strInput)
{
	// Note: strInput gets hacked by this function
	strInput.TrimLeft();
	strInput.TrimRight();

	// Function was passed an empty CString object or
	// a string containing only whitespace
	if(strInput.GetLength() == 0)
		return strInput;

	//Find next tab or space charater
	int iWhiteSpace = strInput.FindOneOf(_T("\t "));

	if( iWhiteSpace == -1 ) //This is the only token in string
		return strInput;
	else
		return strInput.Left(iWhiteSpace);
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      ConnectToServerDB()
//
//  Description :   This function connects to the database server.  It also
//                  determines what the default drive letter should be, for
//                  database pathnames that don't include one.
//                  
//  Return :        HRESULT
//						returns the status of the connection,
//						S_OK if successful.
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
HRESULT CSchedMgrECPApp::ConnectToServerDB()
{
	HRESULT hr = S_OK;
	DWORD dwNameLen;			//Length of environment variable value
	TCHAR szValue[_MAX_PATH];	//Environment variable value

	CString	strMsg;
	strMsg.Format(IDS_EPDBCONNECTING);
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	CEpochCreateInstance creator(_T("EpDBSrvcHost"));
	CLSID	classDBID;
	CLSIDFromProgID(_T("EpDBSrvc.EpDB"), &classDBID);
	hr = creator.Create(classDBID,IID_IUnknown,(void**)&m_pUnkDB);

	if( FAILED(hr) )  return hr;
	hr = m_pUnkDB->QueryInterface(IID_IEpDB, (void **)&m_pEpDB);

	if( SUCCEEDED(hr) )
		hr = m_pUnkDB->QueryInterface(IID_IConnectionPointContainer, (void **)&m_pCPCdb);
	if( SUCCEEDED(hr) )
		hr = m_pCPCdb->FindConnectionPoint(IID__IEpochAliveCB, &m_pCPdb);
	if( SUCCEEDED(hr) )
	{
		//Call Advise for our internal _IEpochAliveCB object
		m_pCPdb->Advise((IUnknown *)&m_xEpochAliveCB, &m_dwCookieDB);
	}

	// The following variable is Not referenced 
	// COAUTHIDENTITY IdentInfo; 
	// Removed next line because failing under Win 7 - FJS 08-24-12
	//if( SUCCEEDED(hr)){
	// Don't believe the following call is necessary because this is now correctly
	// done by CoInitializeSecurity (but will leave it)
	//	hr = CoSetProxyBlanket(m_pEpDB, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL, 
    //      RPC_C_AUTHN_LEVEL_CONNECT, RPC_C_IMP_LEVEL_IMPERSONATE, &IdentInfo, EOAC_NONE); 
	// }
	// Clean up on failure
	if( FAILED(hr) )
	{
		strMsg.Format(IDS_EPDBCONNECTIONFAILED);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		LPVOID lpMsgBuf;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);
		strMsg = (LPTSTR)lpMsgBuf;
		strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		MessageBox( NULL, (LPCTSTR)lpMsgBuf, _T("Error"), MB_OK | MB_ICONINFORMATION );
		LocalFree(lpMsgBuf);
		DisconnectFromServerDB();
		strMsg.Format(IDS_EPDBDISCONNECT);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
	}
	else
	{
		strMsg.Format(IDS_EPDBCONNECTED);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	}

	//Get the default drive letter from environment variable EPOCH_DATABASE
	dwNameLen = GetEnvironmentVariable(_T("EPOCH_DATABASE"), szValue, _MAX_PATH);
	m_strDefaultDrive = _T("");

	//Figure drive letter out
	if( (_tcslen(szValue) >= 2) && (szValue[1] == ':') )
	{
		//Prefix is "X:"
		szValue[2] = '\0';
		m_strDefaultDrive = szValue;
	}
	else if( (_tcslen(szValue) >= 3) && (szValue[0] == '/') && (szValue[2] == '=') )
	{
		//Prefix is "/X=", convert to "X:"
		szValue[0] = szValue[1];
		szValue[1] = ':';
		szValue[2] = '\0';
		m_strDefaultDrive = szValue;
	}

	return hr;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      ConnectToServerSM()
//
//  Description :   This function connects to the stream monitor server.
//                  
//  Return :        HRESULT
//						returns the status of the connection,
//						S_OK if successful.
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
HRESULT CSchedMgrECPApp::ConnectToServerSM()
{
	CString					strMsg;
	CEpochCreateInstance	creator(_T("EpSMonHost"));
	CLSID					clsid;
	CLSIDFromProgID(_T("EpSMon.EpStrmMon"), &clsid);

	strMsg=_T("Connecting to the Stream Monitor Server...");
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	HRESULT	hr = creator.Create(clsid,IID_IUnknown,(void**)&m_pUnkSM);

	if( FAILED(hr) ) return hr;

	hr = m_pUnkSM->QueryInterface(IID_IEpStrmMon, (void**)&m_pEpSMon);

	if( SUCCEEDED(hr) )
		hr = m_pUnkSM->QueryInterface(IID_IConnectionPointContainer, (void**)&m_pCPCsm);

	if( SUCCEEDED(hr) )
		hr = m_pCPCsm->FindConnectionPoint(IID__IEpStrmMonCB, &m_pCPsm);

	if( SUCCEEDED(hr) )
		m_pCPsm->Advise((IUnknown *)&m_xEpStrmMonCB, &m_dwCookieSM);

	// The following variable is Not referenced 
	// COAUTHIDENTITY IdentInfo; 
	// Removed following call since is now failing on Win 7.  FJS 08/24/12
//	if( SUCCEEDED(hr)){
	//This works without the following call now because the security is
	//successfully set using the CoInitializeSecurity call now.  but will
	//leave in....
//		hr = CoSetProxyBlanket(m_pEpSMon, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL, 
//           RPC_C_AUTHN_LEVEL_CONNECT, RPC_C_IMP_LEVEL_IMPERSONATE, &IdentInfo, EOAC_NONE); 
//	}

	//Clean up on failure
	if( FAILED(hr) )
	{
		strMsg = _T("Stream Monitor connection failed");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		LPVOID lpMsgBuf;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);
		strMsg = (LPTSTR)lpMsgBuf;
		strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		MessageBox( NULL, (LPCTSTR)lpMsgBuf, _T("Error"), MB_OK | MB_ICONINFORMATION );
		LocalFree(lpMsgBuf);
		DisconnectFromServerSM();
		strMsg = _T("Stream Monitor disconnect complete.");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
	}
	else
	{
		strMsg = _T("Stream Monitor server connection complete");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	}

	return hr;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      ConnectToServerSS()
//
//  Description :   This function connects to the stream services server.
//                  
//  Return :        HRESULT
//						returns the status of the connection,
//						S_OK if successful.
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
HRESULT CSchedMgrECPApp::ConnectToServerSS()
{
	CString					strMsg;
	//CEpochCreateInstance	creator(_T("SSAPISrvcHost"));
	CLSID					clsid;
	CLSIDFromProgID(_T("EPSS.EpSTOL"), &clsid);

	strMsg=_T("Connecting to the Stream Sevices Server...");
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	//HRESULT	hr = creator.Create(clsid,IID_IUnknown,(void**)&m_pUnkSS);
	HRESULT hr = ::CoCreateInstance(clsid, NULL, CLSCTX_INPROC_SERVER, IID_IUnknown, (void**)&m_pUnkSS);

	if( FAILED(hr) ) return hr;

	hr = m_pUnkSS->QueryInterface(IID_IEpSTOL, (void**)&m_pEpSTOL);

//	COAUTHIDENTITY IdentInfo; 
//	if( SUCCEEDED(hr)){
//		hr = CoSetProxyBlanket(m_pEpSTOL, RPC_C_AUTHN_WINNT, RPC_C_AUTHZ_NONE, NULL, 
//             RPC_C_AUTHN_LEVEL_CONNECT, RPC_C_IMP_LEVEL_IMPERSONATE, &IdentInfo, EOAC_NONE); 
//	}

	if( SUCCEEDED(hr) )
		hr = m_pUnkSS->QueryInterface(IID_IConnectionPointContainer, (void**)&m_pCPCss);

	if( SUCCEEDED(hr) )
		hr = m_pCPCss->FindConnectionPoint(IID__IEpochCB, &m_pCPss);

	if( SUCCEEDED(hr) )
		m_pCPss->Advise((IUnknown *)&m_xEpochCB, &m_dwCookieSS);

	//Clean up on failure
	if( FAILED(hr) )
	{
		strMsg = _T("Stream Services connection failed");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		LPVOID lpMsgBuf;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					  NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);
		strMsg = (LPTSTR)lpMsgBuf;
		strMsg = strMsg.Left(strMsg.GetLength()-2); //Trim off trailing CR/LF
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		MessageBox( NULL, (LPCTSTR)lpMsgBuf, _T("Error"), MB_OK | MB_ICONINFORMATION );
		LocalFree(lpMsgBuf);
		DisconnectFromServerSS();
		strMsg = _T("Stream Monitor disconnect complete.");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
	}
	else
	{
		strMsg = _T("Stream Services server connection complete");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	}

	return hr;
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :     CSchedMgrECPApp::DisconnectFromServerDB()
//
//  Description :  This function disconnects from the database server.
//				   If the connection was only partially successful, this 
//				   function can still be used to clean up. Note however, 
//				   that no databases are closed. This should be done by
//				   the document classes before DisconnectFromServerDB()
//                 is called.
//                  
//  Return :        void	-	none
//						
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
void CSchedMgrECPApp::DisconnectFromServerDB()
{
	if( m_pEpDB )
	{
		m_pEpDB->Release();
		m_pEpDB = NULL;
	}

	if( m_pCPdb )
	{
		m_pCPdb->Unadvise(m_dwCookieDB);
		m_pCPdb->Release();
		m_pCPdb = NULL;
	}

	if( m_pCPCdb )
	{
		m_pCPCdb->Release();
		m_pCPCdb = NULL;
	}

	if( m_pUnkDB )
	{
		m_pUnkDB->Release();
		m_pUnkDB = NULL;
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :     CSchedMgrECPApp::DisconnectFromServerSM()
//
//  Description :  This function disconnects from the stream monitor server.
//				   If the connection was only partially successful, this 
//				   function can still be used to clean up. Note however, 
//				   that no databases are closed. This should be done by
//				   the document classes before DisconnectFromServerSM() is
//                 called.
//                  
//  Return :        void	-	none
//						
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
void CSchedMgrECPApp::DisconnectFromServerSM()
{
	if( m_pEpSMon )
	{
		m_pEpSMon->Release();
		m_pEpSMon = NULL;
	}

	if( m_pCPsm )
	{
		m_pCPsm->Unadvise(m_dwCookieSM);
		m_pCPsm->Release();
		m_pCPsm = NULL;
	}

	if( m_pCPCsm )
	{
		m_pCPCsm->Release();
		m_pCPCsm = NULL;
	}

	if( m_pUnkSM )
	{
		m_pUnkSM->Release();
		m_pUnkSM = NULL;
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :     CSchedMgrECPApp::DisconnectFromServerSS()
//
//  Description :  This function disconnects from the stream services server.
//				   If the connection was only partially successful, this 
//				   function can still be used to clean up. Note however, 
//				   that no services are closed. This should be done by
//				   the document classes before DisconnectFromServerSS() is
//                 called.
//                  
//  Return :        void	-	none
//						
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
void CSchedMgrECPApp::DisconnectFromServerSS()
{
	if( m_pEpSTOL )
	{
		m_pEpSTOL->Release();
		m_pEpSTOL = NULL;
	}

	if( m_pCPss )
	{
		m_pCPss->Unadvise(m_dwCookieSS);
		m_pCPss->Release();
		m_pCPss = NULL;
	}

	if( m_pCPCss )
	{
		m_pCPCss->Release();
		m_pCPCss = NULL;
	}

	if( m_pUnkSS )
	{
		m_pUnkSS->Release();
		m_pUnkSS = NULL;
	}
}

//	}
// }

// Codemax Registry keys
#define CODEMAX_CODE_SECTION	_T("CodeMax")
#define KEY_COLORSYNTAX			_T("ColorSyntax")
#define KEY_COLORS				_T("Colors")
#define KEY_WHITESPACEDISPLAY	_T("WhiteSpaceDisplayed")
#define KEY_TABEXPAND			_T("TabExpand")
#define KEY_SMOOTHSCROLLING		_T("SmoothScrolling")
#define KEY_TABSIZE				_T("TabSize")
#define KEY_LINETOOLTIPS		_T("LineToolTips")
#define KEY_LEFTMARGIN			_T("LeftMargin")
#define KEY_COLUMNSEL			_T("ColumnSel")
#define KEY_DRAGDROP			_T("DragDrop")
#define KEY_CASESENSITIVE		_T("CaseSensitive")
#define KEY_PRESERVECASE		_T("PreserveCase")
#define KEY_WHOLEWORD			_T("WholeWord")
#define KEY_AUTOINDENTMODE		_T("AutoIndentMode")
#define KEY_HSCROLLBAR			_T("HScrollBar")
#define KEY_VSCROLLBAR			_T("VScrollBar")
#define KEY_HSPLITTER			_T("HSplitter")
#define KEY_VSPLITTER			_T("VSplitter")
#define KEY_HOTKEYS				_T("HotKeys")
#define KEY_FINDMRULIST			_T("FindMRUList")
#define KEY_REPLMRULIST			_T("ReplaceMRUList")
#define KEY_FONT				_T("Font")
#define KEY_LINENUMBERING		_T("LineNumbering")
#define KEY_FONTSTYLES			_T("FontStyles")
#define KEY_NORMALIZECASE		_T("NormalizeCase")
#define KEY_SELBOUNDS			_T("SelBounds")

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::SaveCodeMaxProfile
//
//  Description :   SaveCodeMaxProfile() will save the current settings of
//					a window (hWnd != NULL) or the global settings
//					(hWnd == NULL) to the registry.
//                  
//  Return :        
//					HWND hWnd	-	This property returns a handle
//									to a form or control.
//						
//
//  Parameters :   	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
void CSchedMgrECPApp::SaveCodeMaxProfile( HWND hWnd )
{
	if( hWnd )
	{
		// misc props
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_COLORSYNTAX,		CM_IsColorSyntaxEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_HSPLITTER,		CM_IsSplitterEnabled(hWnd, TRUE));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_VSPLITTER,		CM_IsSplitterEnabled(hWnd, FALSE));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_HSCROLLBAR,		CM_HasScrollBar(hWnd, TRUE));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_VSCROLLBAR,		CM_HasScrollBar(hWnd, FALSE));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_WHOLEWORD,		CM_IsWholeWordEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_AUTOINDENTMODE,	CM_GetAutoIndentMode(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_SMOOTHSCROLLING,	CM_IsSmoothScrollingEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_LINETOOLTIPS,		CM_IsLineToolTipsEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_LEFTMARGIN,		CM_IsLeftMarginEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_COLUMNSEL,		CM_IsColumnSelEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_DRAGDROP,			CM_IsDragDropEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_CASESENSITIVE,	CM_IsCaseSensitiveEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_PRESERVECASE,		CM_IsPreserveCaseEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_TABEXPAND,		CM_IsTabExpandEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_TABSIZE,			CM_GetTabSize(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_NORMALIZECASE,	CM_IsNormalizeCaseEnabled(hWnd));
		WriteProfileInt(CODEMAX_CODE_SECTION, KEY_SELBOUNDS,		CM_IsSelBoundsEnabled(hWnd));

		// color info
		CM_COLORS colors;
		CM_GetColors(hWnd, &colors);
		WriteProfileBinary(CODEMAX_CODE_SECTION, KEY_COLORS, (LPBYTE)&colors, sizeof(colors));

		// font info
		CM_FONTSTYLES fs;
		CM_GetFontStyles(hWnd, &fs);
		WriteProfileBinary(CODEMAX_CODE_SECTION, KEY_FONTSTYLES, (LPBYTE)&fs, sizeof(fs));

		// font
		LOGFONT lf;
		HFONT hFont = (HFONT)SendMessage(hWnd, WM_GETFONT, 0, 0);
		int cbSize = ::GetObject(hFont, sizeof(lf), NULL);
		VERIFY(::GetObject(hFont, cbSize, &lf));
		WriteProfileBinary(CODEMAX_CODE_SECTION, KEY_FONT, (LPBYTE) &lf, sizeof(lf));

		CM_LINENUMBERING cmLineNum;
		CM_GetLineNumbering(hWnd, &cmLineNum);
		WriteProfileBinary(CODEMAX_CODE_SECTION, KEY_LINENUMBERING, (LPBYTE)&cmLineNum, sizeof(cmLineNum));
	}
	else
	{
		// save global settings
		// macros
		for( int i = 0; i < CM_MAX_MACROS; i++ )
		{
			int cbMacro = CMGetMacro(i, NULL);
			LPBYTE pMacro = cbMacro ? new BYTE[cbMacro] : 0;

			if( pMacro )
				CMGetMacro(i, pMacro);

			TCHAR szKey[ 10 ];
			wsprintf(szKey, _T("Macro%d"), i);
			WriteProfileBinary(CODEMAX_CODE_SECTION, szKey, pMacro, cbMacro);

			if( pMacro )
				delete pMacro;
		}

		// hotkeys
		int cbHotKeys = CMGetHotKeys( NULL );
		LPBYTE pHotKeys = cbHotKeys ? new BYTE[ cbHotKeys ] : 0;

		if( pHotKeys )
			CMGetHotKeys(pHotKeys);

		WriteProfileBinary(CODEMAX_CODE_SECTION, KEY_HOTKEYS, pHotKeys, cbHotKeys);

		if( pHotKeys )
			delete pHotKeys;

		// Find/Replace MRU lists
		TCHAR szMRUList[CM_FIND_REPLACE_MRU_BUFF_SIZE];
		CMGetFindReplaceMRUList(szMRUList, TRUE);
		WriteProfileString(CODEMAX_CODE_SECTION, KEY_FINDMRULIST, szMRUList);
		CMGetFindReplaceMRUList(szMRUList, FALSE);
		WriteProfileString(CODEMAX_CODE_SECTION, KEY_REPLMRULIST, szMRUList);
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      void CSchedMgrECPApp::LoadCodeMaxProfile
//
//  Description :	This routine loads the different code max profiles.    
//					
//                  
//  Return :        void	- none
//						
//  Parameters : 
//				  	HWND hWnd	-	This property returns a handle
//									to a form or control.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
void CSchedMgrECPApp::LoadCodeMaxProfile(HWND hWnd)
{
	if( hWnd )
	{
		// load window settings
		CM_EnableColorSyntax(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_COLORSYNTAX, TRUE));
		CM_ShowScrollBar(hWnd, TRUE, GetProfileInt(CODEMAX_CODE_SECTION, KEY_HSCROLLBAR, TRUE));
		CM_ShowScrollBar(hWnd, FALSE, GetProfileInt(CODEMAX_CODE_SECTION, KEY_VSCROLLBAR, TRUE));
		CM_EnableSplitter(hWnd, TRUE, GetProfileInt(CODEMAX_CODE_SECTION, KEY_HSPLITTER, TRUE));
		CM_EnableSplitter(hWnd, FALSE, GetProfileInt(CODEMAX_CODE_SECTION, KEY_VSPLITTER, TRUE));
		CM_EnableWholeWord(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_WHOLEWORD, FALSE));
		CM_SetAutoIndentMode(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_AUTOINDENTMODE, CM_INDENT_PREVLINE));
		CM_EnableSmoothScrolling(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_SMOOTHSCROLLING, FALSE));
		CM_EnableLineToolTips(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_LINETOOLTIPS, TRUE));
		CM_EnableLeftMargin(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_LEFTMARGIN, TRUE));
		CM_EnableColumnSel(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_COLUMNSEL, TRUE));
		CM_EnableDragDrop(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_DRAGDROP, TRUE));
		CM_EnableCaseSensitive(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_CASESENSITIVE, FALSE));
		CM_EnablePreserveCase(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_PRESERVECASE, FALSE));
		CM_EnableTabExpand( hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_TABEXPAND, TRUE));
		CM_SetTabSize(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_TABSIZE, 4));
		CM_EnableNormalizeCase(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_NORMALIZECASE, FALSE));
		CM_EnableSelBounds(hWnd, GetProfileInt(CODEMAX_CODE_SECTION, KEY_SELBOUNDS, FALSE));

		// colors
		LPBYTE pColors;
		UINT unSize;

		if( GetProfileBinary(CODEMAX_CODE_SECTION, KEY_COLORS, &pColors, &unSize) )
		{
			if( unSize == sizeof(CM_COLORS) )
				CM_SetColors(hWnd, pColors);

			delete pColors;
		}

		// font info
		LPBYTE pFontStyles;

		if( GetProfileBinary(CODEMAX_CODE_SECTION, KEY_FONTSTYLES, &pFontStyles, &unSize) )
		{
			if (unSize == sizeof(CM_FONTSTYLES) )
				CM_SetFontStyles(hWnd, pFontStyles);

			delete pFontStyles;
		}

		// font
		{
			HFONT hFont;
			LOGFONT *plf;

			if( GetProfileBinary( CODEMAX_CODE_SECTION, KEY_FONT, (LPBYTE*)&plf, &unSize) )
			{
				hFont = CreateFontIndirect(plf);
				delete plf;
				SendMessage(hWnd, WM_SETFONT, (WPARAM)hFont, 0);
			}
			else
			{
				HDC hDC = GetDC (NULL);
				hFont = CreateFont(-MulDiv (10, GetDeviceCaps (hDC, LOGPIXELSY), 72),
									0, 0, 0, FW_NORMAL, 0, 0, 0, 0, 0, 0, 0, 0, _T("Courier New"));
				ReleaseDC(NULL, hDC);
				SendMessage(hWnd, WM_SETFONT, (WPARAM)hFont, 0 );
			}
		}

		// colors
		LPBYTE pLineNum;

		if( GetProfileBinary( CODEMAX_CODE_SECTION, KEY_LINENUMBERING, &pLineNum, &unSize) )
		{
			if( unSize == sizeof(CM_LINENUMBERING) )
				CM_SetLineNumbering(hWnd, pLineNum);

			delete pLineNum;
		}
		else
		{
			CM_LINENUMBERING	sLineNumberingDef	= {TRUE, 1, CM_DECIMAL};
			CM_SetLineNumbering(hWnd, &sLineNumberingDef);
		}
	}
	else
	{
		// load global settings
		// macros
		UINT unSize;

		for( int i = 0; i < CM_MAX_MACROS; i++ )
		{
			TCHAR szKey[10];
			wsprintf(szKey, _T("Macro%d"), i);
			LPBYTE pMacro;

			if( GetProfileBinary( CODEMAX_CODE_SECTION, szKey, &pMacro, &unSize) )
			{
				if( unSize )
					CMSetMacro(i, pMacro);

				delete pMacro;
			}
		}

		// hotkeys
		LPBYTE pHotKeys;
		if (GetProfileBinary( CODEMAX_CODE_SECTION, KEY_HOTKEYS, &pHotKeys, &unSize ))
		{
			if( unSize )
				CMSetHotKeys(pHotKeys);

			delete pHotKeys;
		}

		// Find/Replace MRU lists

		CMSetFindReplaceMRUList(GetProfileString(CODEMAX_CODE_SECTION, KEY_FINDMRULIST, _T("")), TRUE );
		CMSetFindReplaceMRUList(GetProfileString(CODEMAX_CODE_SECTION, KEY_REPLMRULIST, _T("")), FALSE );
	}
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPApp::AddBackupPrivilege()
//
//  Description :    
//					 
//                  
//  Return :        BOOL
//						TRUE	-	SUCCESS
//						FALSE	-	FAIL
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
BOOL CSchedMgrECPApp::AddBackupPrivilege()
{
	TOKEN_PRIVILEGES NewState;
	LUID             luid;
	HANDLE	hToken    = NULL;
	BOOL	bRtnStatus= TRUE;

    // Open the process token for this process.
	if( OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken) )
    {
	    // Get the local unique id for the privilege.
		if( LookupPrivilegeValue(NULL, _T("SeBackupPrivilege"), &luid) )
		{
			// Assign values to the TOKEN_PRIVILEGE structure.
			NewState.PrivilegeCount				= 1;
			NewState.Privileges[0].Luid			= luid;
			NewState.Privileges[0].Attributes	= SE_PRIVILEGE_ENABLED;
					  //(fEnable ? SE_PRIVILEGE_ENABLED : 0);

			// Adjust the token privilege.
			if( !AdjustTokenPrivileges(hToken, FALSE, &NewState, sizeof(NewState), NULL, NULL) )
				bRtnStatus = FALSE;
		}
		else
			bRtnStatus = FALSE;
    }
	else
		bRtnStatus = FALSE;

	if( hToken )
		CloseHandle(hToken);

	return bRtnStatus;
}





/**
//////////////////////////////////////////////////////////////////////////
//  Author :Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      void CAboutDlg::OnCredits()
//
//  Description :   Displays the credits for the software 
//					 
//                  
//  Return :        void	-	none.
//
//  Parameters :	void	-	none.
//				  	
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
void CAboutDlg::OnCredits()
{
	CCredits dlgCredits;
	TCHAR* pcArrCredit[] = 
	{	
		_T("BITMAP_CREDITS2\b"),    // MYBITMAP is a quoted bitmap resource
		_T(""),
		_T("Schedule Manager ECP\n"),
		_T(""),
		_T("Copyright (c) 2012 \f"),
		_T("OSPO/NESDIS/NOAA/DOC \f"),
		_T("NSOF Bldg. \f"),
		_T("4213 Suitland Road \f"),
		_T("Suitland, Maryland \f"),
		_T(""),
		_T("301-817-4133 \f"),
		_T("www.ospo.noaa.gov \f"),
		_T(""),
		_T("All Rights Reserved \f"),
		_T(""),
		_T(""),
		_T(""),
		_T("Project Lead \t"),
		_T(""),
		_T("Diane Robinson \f"),
		_T(""),
		_T(""),
		_T("Team Programmer \t"),
		_T(""),
		_T("Manan Dalal \f"),
		_T(""),
		_T(""),
		_T("Build15.1 - June, 2012"),
		_T("PR00446, Defaults for Schedule Updates added."),
		_T("PR00507 - Sched Validation moves lines within CP."),
		_T(""),
		_T("BITMAP_CREDITS2\b"),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
		_T(""),
	};

	dlgCredits.m_nArrCnt	= 39;
	dlgCredits.m_pcArrCredit= pcArrCredit;
	dlgCredits.DoModal();
}

//////////////////////////////////////////////////////////////////////////
// CSchedMgrECPUserTool Class
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////////

IMPLEMENT_SERIAL(CSchedMgrECPUserTool, CBCGPUserTool, VERSIONABLE_SCHEMA | 1)

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPUserTool::CSchedMgrECPUserTool()
//
//  Description :    Constructor
//                  
//  Return :        void	-	none
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
CSchedMgrECPUserTool::CSchedMgrECPUserTool(){}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPUserTool::~CSchedMgrECPUserTool()
//
//  Description :   Destructor 
//					 
//                  
//  Return :        void	-	none.
//						
//  Parameters :   	void	-	none.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
CSchedMgrECPUserTool::~CSchedMgrECPUserTool(){}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
// 
//  Function :      CSchedMgrECPUserTool::Invoke ()
//
//  Description :    
//					 
//                  
//  Return :        BOOL
//						TRUE	-	SUCCESS	
//						FALSE	-	FAIL
//
//  Parameters : 
//				  	void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
**/
BOOL CSchedMgrECPUserTool::Invoke()
{
	// Setup m_strArguments with the "actual" arguments,
	// e.g., you can replace "$(ActiveDoc)" by the active document path
	CString		strFilePath;
	strFilePath.Empty();
	CBCGPMDIChildWnd	*pMDIActive = (CBCGPMDIChildWnd*)((CMainFrame*)AfxGetMainWnd())->MDIGetActive();

	if( pMDIActive )
	{
		CDocument	*pActiveDoc = pMDIActive->GetActiveDocument();

		if( pActiveDoc )
			strFilePath = pActiveDoc->GetPathName();
	}

	CString strArguments = m_strArguments;

	if( !strFilePath.IsEmpty() )
		strFilePath = _T("\"") + strFilePath + _T("\"");

	CString strActiveDoc = _T("$(ActiveDoc)");
	int nActiveDoc = strArguments.Find(strActiveDoc);

	if( nActiveDoc != -1 )
	{
		strArguments.Delete(nActiveDoc, strActiveDoc.GetLength());
		strArguments.Insert(nActiveDoc, strFilePath);
	}

	// Invoke user tool:
	if( ::ShellExecute(AfxGetMainWnd()->GetSafeHwnd(), NULL, m_strCommand,
						strArguments, m_strInitialDirectory,
						SW_SHOWNORMAL) < (HINSTANCE)32 )
	{
		TRACE(_T("Can't invoke command: %s\n"), m_strCommand);
		return FALSE;
	}

	return TRUE;
}
