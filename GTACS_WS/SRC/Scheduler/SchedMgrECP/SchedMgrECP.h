// SchedMgrECP.h : main header file for the SCHEDMGRECP application
//
// Revision History:													   
// Manan Dalal - July-August 2014										   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   
// Release in Build16.2

#if !defined(AFX_SCHEDMGRECP_H__A464C52D_D502_11D4_800D_00609704053C__INCLUDED_)
#define AFX_SCHEDMGRECP_H__A464C52D_D502_11D4_800D_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <IAds.h>

#include "Resource.h"
#include "OutputBar.h"
#include "WorkspaceBar.h"
#include "RegistryUsr.h"
#include "RegistryGbl.h"
#include "Monitor.h"

/////////////////////////////////////////////////////////////////////////////
// CSchedMgrECPApp:
// See SchedMgrECP.cpp for the implementation of this class
//

#define GetAppDocTemplate(nDocType)	((CSchedMgrECPApp*)AfxGetApp())->m_pDocTemplate[nDocType]
#define GetAppAdminPriv()			(&((CSchedMgrECPApp*)AfxGetApp())->m_bAdminPriv)
#define GetAppAdminMode()			(&((CSchedMgrECPApp*)AfxGetApp())->m_bAdminMode)
#define GetAppSchedPriv()			(&((CSchedMgrECPApp*)AfxGetApp())->m_bSchedPriv)
#define GetAppOutputBar()			(&((CSchedMgrECPApp*)AfxGetApp())->m_wndOutput)
#define GetAppWorkspaceBar()		(&((CSchedMgrECPApp*)AfxGetApp())->m_wndWorkspace)
#define GetAppCmdList()				(&((CSchedMgrECPApp*)AfxGetApp())->m_strCmdList)
#define GetAppCritCmdList()			(&((CSchedMgrECPApp*)AfxGetApp())->m_strCritCmdList)
#define GetAppLoadDB()				(&((CSchedMgrECPApp*)AfxGetApp())->m_bLoadDB)

#define GetAppRegFolder()			(&((CSchedMgrECPApp*)AfxGetApp())->m_regFolderType)
#define GetAppRegSC()				(&((CSchedMgrECPApp*)AfxGetApp())->m_regSC)
#define GetAppRegSite()				(&((CSchedMgrECPApp*)AfxGetApp())->m_regSite)
#define GetAppRegGTACS()			(&((CSchedMgrECPApp*)AfxGetApp())->m_regGTACS)
#define GetAppRegOATS()				(&((CSchedMgrECPApp*)AfxGetApp())->m_regOATS)
#define GetAppRegRSO()				(&((CSchedMgrECPApp*)AfxGetApp())->m_regRSO)
#define GetAppRegMisc()				(&((CSchedMgrECPApp*)AfxGetApp())->m_regMisc)
#define GetAppRegInstObj()			(&((CSchedMgrECPApp*)AfxGetApp())->m_regInstObj)
#define GetAppRegMapObj()			(&((CSchedMgrECPApp*)AfxGetApp())->m_regMapObj)
#define GetAppRegMakeSched()		(&((CSchedMgrECPApp*)AfxGetApp())->m_regMakeSched)

class CSchedMgrECPApp : public CWinApp,
						public CBCGPWorkspace
{
public:
	CSchedMgrECPApp();
	//The following allows us to implement _IEpochAliveCB in an internal
	//class called XEpochAliveCB.  The class object ends up in member
	//variable m_xEpochAliveCB.  The IUnknown and _IEpochAliveCB methods
	//are in file EpMnemBrwsrApp.cpp.
	BEGIN_INTERFACE_PART(EpochAliveCB, _IEpochAliveCB)
		STDMETHOD(AreYouAliveCB)();
		CSchedMgrECPApp* m_pParent;
	END_INTERFACE_PART(EpochAliveCB)

	BEGIN_INTERFACE_PART(EpStrmMonCB, _IEpStrmMonCB)
		STDMETHOD(StrmInfoSrvcCB)(ULONG, EPSTRMINFO*);
		STDMETHOD(StrmMessageSrvcCB)(ULONG, ULONG, BSTR*);
		CSchedMgrECPApp* m_pParent;
	END_INTERFACE_PART(EpStrmMonCB)

	BEGIN_INTERFACE_PART(EpochCB, _IEpochCB)
		STDMETHOD(AreYouAliveCB)();
		STDMETHOD(PointServiceCB)(ULONG, EPPTDATA*);
		STDMETHOD(FrameServiceCB)(EPFRMDATA);
		STDMETHOD(EventServiceCB)(ULONG, EPEVTDATA*);
		STDMETHOD(STOLCompStatCB)(EPSTOLRESP);
		STDMETHOD(ConStatServiceCB)(EPSTATUSDATA);
		STDMETHOD(CLIMStatusServiceCB)(ULONG, EpCLIMStatusRec);
		STDMETHOD(CmdStatusServiceCB)(ULONG, ULONG, EpCmdStatusRec*);
		STDMETHOD(LimitServiceCB)(ULONG, ULONG, EPLIMITINFO*);
		STDMETHOD(DBModServiceCB)(ULONG, ULONG, EPDBMODINFO*);
		STDMETHOD(LimitOffServiceCB)(ULONG, EPLIMITOFFINFO);
		CSchedMgrECPApp* m_pParent;
	END_INTERFACE_PART(EpochCB)

// Override from CBCGPWorkspace
	virtual void PreLoadState ();
	BOOL	m_bTabFlatBorders;
	BOOL	AddBackupPrivilege();
	BOOL	LoadDatabase();
	BOOL	InitAndStartSockets(WSADATA*);
	BOOL	m_bMDITabs;
	BOOL	m_bMDITabIcons;
	CBCGPTabWnd::Location	m_MDITabLocation;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSchedMgrECPApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL

// Implementation
	COleTemplateServer m_server;
		// Server object for document creation
	//{{AFX_MSG(CSchedMgrECPApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	//The following helps us implement _IEpochAliveCB
	DECLARE_INTERFACE_MAP()

public:
	HRESULT ConnectToServerDB();
	HRESULT ConnectToServerSM();
	HRESULT ConnectToServerSS();
	void	DisconnectFromServerDB();
	void	DisconnectFromServerSM();
	void	DisconnectFromServerSS();
	void	SaveCodeMaxProfile(HWND hWnd);
	void	LoadCodeMaxProfile(HWND hWnd);
	CString GetNextToken(CString strInput);
	CString GetLastErrorText();
	void	UpdateProcessList();

	BOOL						m_bRegServer;
	BOOL						m_bUnRegServer;
	IEpDB*						m_pEpDB;
	IEpStrmMon*					m_pEpSMon;
	IEpSTOL*					m_pEpSTOL;
	DWORD						m_dwCookieDB;
	DWORD						m_dwCookieSM;
	DWORD						m_dwCookieSS;
	ULONG						m_ulDBHandle;	// Database handle
	CString						m_strDefaultDrive;
	CSchedMgrMultiDocTemplate*	m_pDocTemplate[nMaxDocTypes];
	COutputBar					m_wndOutput;
	CWorkspaceBar				m_wndWorkspace;
	BOOL						m_bBackupPriv;	// True if user has NT Backup Priv
	BOOL						m_bAdminPriv;	// True if user is in NT "Domain Admin" group
	BOOL						m_bSchedPriv;	// True if user is in NT "Scheduler" group
	BOOL						m_bAdminMode;	// True if user has toggled into Admin Mode
												//   Only available if m_bAdminPriv is TRUE;
	BOOL						m_bLoadDB;
	BOOL                        m_NewGTACS;
	CRegFolderType				m_regFolderType;
	CRegSC						m_regSC;
	CRegSite					m_regSite;
	CRegGTACS					m_regGTACS;
	CRegOATS					m_regOATS;
	CRegRSO						m_regRSO;
	CRegMisc					m_regMisc;
	CRegInstObj					m_regInstObj;
	CRegMapObj					m_regMapObj;
	CRegMakeSched				m_regMakeSched;
	CString						m_strCmdList;
	CString						m_strCritCmdList;
	HGLOBAL						m_hDevMode;
	HGLOBAL						m_hDevNames;
	CString                     m_strGrdStrmName;
	CArray						<CMonitor, CMonitor&> m_cMonitorArray;


private:
	HRESULT	IsUserInGroups(IADsUser*);

	IUnknown*					m_pUnkDB;
	IUnknown*					m_pUnkSM;
	IUnknown*					m_pUnkSS;
	IConnectionPointContainer*	m_pCPCdb;
	IConnectionPointContainer*	m_pCPCsm;
	IConnectionPointContainer*	m_pCPCss;
	IConnectionPoint*			m_pCPdb;
	IConnectionPoint*			m_pCPsm;
	IConnectionPoint*			m_pCPss;
	CString						m_strDatabasePath;
	CString						m_strDatabaseName;
};


extern CSchedMgrECPApp theApp;

/////////////////////////////////////////////////////////////////////////////


class CSchedMgrECPUserTool : public CBCGPUserTool
{
	DECLARE_SERIAL(CSchedMgrECPUserTool)
public:
	CSchedMgrECPUserTool();
	virtual ~CSchedMgrECPUserTool();
	virtual BOOL Invoke ();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCHEDMGRECP_H__A464C52D_D502_11D4_800D_00609704053C__INCLUDED_)
