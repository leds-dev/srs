/******************
//////////////////////////////////////////////////////////////////////
// SchedMgrMultiDocTemplate.cpp: implementation of the 
//		CSchedMgrMultiDocTemplate class.
// (c) 2001 Frederick J. Shaw  
//	Prolog Updated                                    //
//////////////////////////////////////////////////////////////////////
******************/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "SchedMgrMultiDocTemplate.h"
#include "MapDoc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

/******************
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedMgrMultiDocTemplate::CSchedMgrMultiDocTemplate
//
//  Description :   Class constructor. 
//                  
//  Return :         
//					void	-	none.
//  Parameters : 
//				  	UINT nIDResource	-	Specifies the ID of the 
//						resources used with the document type. This may 
//						include menu, icon, accelerator table, and string
//						resources. 
//
//					CRuntimeClass* pDocClass	-	Points to the 
//						CRuntimeClass object of the document class. This 
//						class is a CDocument-derived class you define to 
//						represent your documents.
//
//					CRuntimeClass* pFrameClass	-	Points to the 
//						CRuntimeClass object of the frame-window class.
//						This class can be a CMDIChildWnd-derived class, or
//						it can be CMDIChildWnd itself if you want default 
//						behavior for your document frame windows.
//
//					CRuntimeClass* pViewClass -	Points to the CRuntimeClass
//						object of the view class. This class is a 
//						CView-derived class you define to display your 
//						documents.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CSchedMgrMultiDocTemplate::CSchedMgrMultiDocTemplate(
		UINT nIDResource, CRuntimeClass* pDocClass,
		CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass ) :
		CMultiDocTemplate(nIDResource, pDocClass, pFrameClass, pViewClass)
{
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
//  Function :      CSchedMgrMultiDocTemplate::~CSchedMgrMultiDocTemplate
//
//  Description :   Class destructor. 
//                  
//  Return :         
//					void	-	none.
//  Parameters : 
//				  	void	-	none
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CSchedMgrMultiDocTemplate::~CSchedMgrMultiDocTemplate()
{
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedMgrMultiDocTemplate::MatchDocType
//
//  Description :   Use this function to determine the type of document
//					template to use for opening a file. 
//                  
//  Return :         
//					CDocTemplate::Confidence	-	A value from the
//						 Confidence enumeration
//  Parameters : 
//				  	const TCHAR* pszPathName	-	Pathname of the file
//						 whose type is to be determined.
//					CDocument*& rpDocMatch	-	Pointer to a document that
//						 is assigned the matching document, if the file 
//						 specified by lpszPathName is already open.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CDocTemplate::Confidence CSchedMgrMultiDocTemplate::MatchDocType(const TCHAR* pszPathName, CDocument*& rpDocMatch)
{
	ASSERT(pszPathName != NULL);
	rpDocMatch = NULL;

	// Go through all documents and see if document is already opened.
	POSITION pos = GetFirstDocPosition();
	while (pos != NULL)
	{
		CDocument* pDoc = GetNextDoc(pos);
		if (pDoc->GetPathName() == pszPathName)
		{
			// already open
			rpDocMatch = pDoc;
			return yesAlreadyOpen;
		}
	}  // end while

	// STOL Map files and RTCS Map files share the same suffix
	// If we find one we need to open the file and check the header
	// to determine the proper type
	CString strMAPFilterExt = _T("ASDF");
	if ((m_pDocClass == RUNTIME_CLASS(CSTOLMapDoc)) ||
		(m_pDocClass == RUNTIME_CLASS(CRTCSMapDoc)))
	{
		CStdioFile cTextMapFile;
		if (cTextMapFile.Open(pszPathName, CFile::modeRead | CFile::shareDenyNone))
		{
			CString strHdrLine;
			cTextMapFile.ReadString(strHdrLine);
			strHdrLine.MakeUpper();
			cTextMapFile.Close();
			if ((m_pDocClass == RUNTIME_CLASS(CSTOLMapDoc)) &&
				(strHdrLine.Find(_T("STOLMAP")) != -1))
				return yesAttemptNative;
			else if ((m_pDocClass == RUNTIME_CLASS(CRTCSMapDoc)) &&
				     (strHdrLine.Find(_T("RTCSMAP")) != -1))
				return yesAttemptNative;
			else
				return noAttempt; //unknown document type - Don't try to open it!
		}
		else
			return noAttempt; //unknown document type - Don't try to open it!
	}
	else	
	{
		// see if it matches either suffix
		CString strFilterExt;
		if (GetDocString(strFilterExt, CDocTemplate::filterExt) && !strFilterExt.IsEmpty())
		{
			// see if extension matches
			ASSERT(strFilterExt[0] == _T('.'));
			int nDot = CString(pszPathName).ReverseFind('.');
			if (nDot >= 0 && (lstrcmpi(pszPathName+nDot,strFilterExt)==0))
				return yesAttemptNative;  // If it does, then allow the open
		}
	}
	return noAttempt; //unknown document type - Don't try to open it!
}
