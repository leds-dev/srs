// SchedMgrMultiDocTemplate.h: interface for the CSchedMgrMultiDocTemplate class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCHEDMGRMULTIDOCTEMPLATE_H__708B4B77_5A80_11D5_9A81_0003472193C8__INCLUDED_)
#define AFX_SCHEDMGRMULTIDOCTEMPLATE_H__708B4B77_5A80_11D5_9A81_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CSchedMgrMultiDocTemplate : public CMultiDocTemplate  
{
public:
	CSchedMgrMultiDocTemplate::CSchedMgrMultiDocTemplate(
		UINT nIDResource, CRuntimeClass* pDocClass,
		CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass );
	virtual ~CSchedMgrMultiDocTemplate();
	CDocTemplate::Confidence CSchedMgrMultiDocTemplate::MatchDocType(const TCHAR* pszPathName, CDocument*& rpDocMatch);
};

#endif // !defined(AFX_SCHEDMGRMULTIDOCTEMPLATE_H__708B4B77_5A80_11D5_9A81_0003472193C8__INCLUDED_)
