/******************
////////////////////////////////////////////////////////
// SchedPage.cpp : implementation of the              // 
//		CSchedPage class.                             // 
// (c) 2001 Frederick J. Shaw                         //
//	Prolog Updated                                    //
//													  //
// MSD (06/12) PR000446: Implemented defaults during  //
// Schedule Update.									  //
////////////////////////////////////////////////////////
******************/
#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"
#include "FinishDialog.h"
#include "SchedPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSchedPage property page

IMPLEMENT_DYNCREATE(CSchedPage, CResizablePage)

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::CSchedPage() 
//
//  Description :   Class constructor. 
//                  
//  Return :         
//					void	-	none.
//  Parameters : 
//				  	void	-	none. 
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CSchedPage::CSchedPage() : CResizablePage(CSchedPage::IDD)
{
	//{{AFX_DATA_INIT(CSchedPage)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::~CSchedPage() 
//
//  Description :   Class destructor. 
//                  
//  Return :         
//					void	-	none.
//  Parameters : 
//				  	void	-	none. 
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CSchedPage::~CSchedPage()
{
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      void CSchedPage::DoDataExchange(CDataExchange* pDX) 
//
//  Description :   Called by the framework to exchange and validate
//					 dialog data.
//                  
//  Return :         
//					void	-	none.
//  Parameters : 
//				  	CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CSchedPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSchedPage)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSchedPage, CResizablePage)
	//{{AFX_MSG_MAP(CSchedPage)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_SCHED_VAL_CHECK, OnSchedValCheck)
	ON_BN_CLICKED(IDC_SCHED_GEN_CHECK, OnSchedGenCheck)
	ON_BN_CLICKED(IDC_SCHED_VAL_GROUND_RADIO, OnSchedValRadio)
	ON_BN_CLICKED(IDC_SCHED_VAL_ONBOARD_RADIO, OnSchedValRadio)
	ON_BN_CLICKED(IDC_SCHED_VAL_BOTH_RADIO, OnSchedValRadio)
	ON_BN_CLICKED(IDC_SCHED_RTCS_CHECK, OnSchedRTCSValCheck)
	ON_EN_CHANGE(IDC_SCHED_RTCS_SET_EDIT, OnChangeSchedRTCSSetEdit)
	ON_BN_CLICKED(IDC_SCHED_RTCS_SET_BUTTON, OnSchedRTCSSetButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
/////////////////////////////////////////////////////////////////////////////
// CSchedPage message handlers
////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      void CSchedPage::Init
//
//  Description :   This routine initializes the member variables to the
//					passed parameters.
//                  
//  Return :         
//					void	-	none.
//  Parameters : 
//				  	BOOL bFirstWizPage	-	TRUE if this the first page
//											 of a Wizard
//					BOOL bLastWizPage	-	TRUE if this the last page 
//											 of a Wizard
//					BOOL bGen			-	Generate upload - if TRUE, then
//											 generate upload files
//					BOOL bVal			-	Validate the Sched - if TRUE,
//											 then check the next members veriables
//					BOOL bRTCSVal		-	Validate the for RTCS - if TRUE,
//											 then check the next members veriables
//					eValidLoc eValLoc	-	Location of procedure execution
//					
//					BOOL bSchedUpd		-   TRUE if doing schedule update.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CSchedPage::Init(BOOL bFirstWizPage, BOOL bLastWizPage, 
					  BOOL bGen, BOOL bVal, BOOL bRTCSVal, eValidLoc eValLoc, BOOL bSchedUpd)
{
	m_bFirstWizPage	= bFirstWizPage;
	m_bLastWizPage	= bLastWizPage;
//	m_bGen			= bGen;
	//m_bGen			= GetAppRegMakeSched()->m_nOBSchedGen;
	m_bVal		= GetAppRegMakeSched()->m_nValSched;
		
	if(!bSchedUpd) {
		m_bRTCSVal  = (BOOL)GetAppRegMakeSched()->m_nRTCSVal;
		m_bGen		= GetAppRegMakeSched()->m_nOBSchedGen;
	}
	else {
		m_bRTCSVal	= nRegMakeSchedRTCSValDefValue;
		m_bGen		= nRegMakeSchedOBSchedGenDefValue;
	}
	//m_bRTCSVal		= (BOOL)GetAppRegMakeSched()->m_nRTCSVal;
	m_eValLoc		= (eValidLoc)GetAppRegMakeSched()->m_nValSchedType;
	m_bSchedUpd		= bSchedUpd;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnInitDialog() 
//
//  Description :   This member function is called in response to the 
//					WM_INITDIALOG message.
//					.
//                  
//  Return :         
//					BOOL	-	always TRUE.	
//							
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CSchedPage::OnInitDialog() 
{
	CResizablePage::OnInitDialog();
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	if ((pSheet->GetFunc() == nPackage) ||
		(pSheet->GetFunc() == nUpdSTOL) ||
		(pSheet->GetFunc() == nValCLS) ||
		(pSheet->GetFunc() == nValSTOL))
	{
//		int	nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//		int	nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
		
		CString strExt;
		GetAppDocTemplate(nRTCSSet)->GetDocString(strExt, CDocTemplate::filterExt);

		CString strTemp = GetAppRegMakeSched()->m_strRTCSSetFileName;
	//	m_strRTCSSet.Format(_T("\\\\%s\\%s\\%s\\%s\\%s%s"),
		//	GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
		//	GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
		m_strRTCSSet.Format(_T("%s\\%s\\%s\\%s\\%s\\%s%s"),
			ctcProcDrive, ctcMountPoint, ctcProcsDir,
			GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],		// SC Specific Directory
			GetAppRegFolder()->m_strDir[nFolderRTCS],					// RTCS Directory
 			GetAppRegMakeSched()->m_strRTCSSetFileName,                 // Retrieve default filename from registry
			strExt);													// RTCS extension
	}
	else
	{
		m_strRTCSSet.Empty();
	}
	
	GetDlgItem(IDC_SCHED_RTCS_SET_EDIT)->SetWindowText(m_strRTCSSet);

	// Set all of the necessary anchors
	AddAnchor(IDC_SCHED_GROUP, TOP_LEFT, BOTTOM_RIGHT);

	AddAnchor(IDC_SCHED_VAL_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SCHED_VAL_CHECK, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SCHED_VAL_GROUND_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SCHED_VAL_ONBOARD_RADIO, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SCHED_VAL_BOTH_RADIO, TOP_LEFT, TOP_LEFT);
	
	AddAnchor(IDC_SCHED_GEN_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SCHED_GEN_CHECK, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SCHED_GEN_BIN_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SCHED_GEN_PROC_STATIC, TOP_LEFT, TOP_RIGHT);
	
	AddAnchor(IDC_SCHED_RTCS_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SCHED_RTCS_SET_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_SCHED_RTCS_SET_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SCHED_RTCS_SET_BUTTON, TOP_RIGHT, TOP_RIGHT);

	AddAnchor(IDC_SCHED_LOC_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SCHED_LOC_OLD_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SCHED_LOC_NEW_STATIC, TOP_LEFT, TOP_RIGHT);
	
	// Add the images to the page
	CImageList imageList;
	imageList.Create(IDR_STOL_MENUIMAGES, 16, 0, RGB (192,192,192));
	((CStatic*)GetDlgItem(IDC_SCHED_VAL_ICON))->SetIcon(imageList.ExtractIcon(2));
	((CStatic*)GetDlgItem(IDC_SCHED_GEN_ICON))->SetIcon(imageList.ExtractIcon(1));

	GetDlgItem(IDC_SCHED_VAL_ICON)->BringWindowToTop();
	GetDlgItem(IDC_SCHED_GEN_ICON)->BringWindowToTop();

	BOOL bValRadioEnable = FALSE;
	BOOL bGenEnable = FALSE;
	BOOL bRTCSEnable = m_bRTCSVal;
	BOOL bValChkEnable = TRUE;

	switch (pSheet->GetFunc())
	{
		case nUpdSTOL:	bValChkEnable = TRUE;	bValRadioEnable = TRUE;	/* bRTCSEnable = m_bRTCSVal ;*/ bGenEnable = TRUE;	break;
		case nPackage:	bValChkEnable = TRUE;	bValRadioEnable = TRUE;	/* bRTCSEnable = m_bRTCSVal ;*/ bGenEnable = TRUE;	break;
		case nValSTOL:	bValChkEnable = TRUE;	bValRadioEnable = TRUE;	/* bRTCSEnable = m_bRTCSVal ;*/ bGenEnable = FALSE;	
						GetDlgItem(IDC_SCHED_LOC_NEW_LABEL)->EnableWindow(FALSE);
						GetDlgItem(IDC_SCHED_LOC_NEW_STATIC)->EnableWindow(FALSE);									break;
		case nValCLS:	bValChkEnable = TRUE;	bValRadioEnable = TRUE; /* bRTCSEnable = m_bRTCSVal ;*/ bGenEnable = FALSE;
						GetDlgItem(IDC_SCHED_LOC_GROUP)->EnableWindow(FALSE);
						GetDlgItem(IDC_SCHED_LOC_OLD_LABEL)->EnableWindow(FALSE);
						GetDlgItem(IDC_SCHED_LOC_OLD_STATIC)->EnableWindow(FALSE);
						GetDlgItem(IDC_SCHED_LOC_NEW_LABEL)->EnableWindow(FALSE);
						GetDlgItem(IDC_SCHED_LOC_NEW_STATIC)->EnableWindow(FALSE);									break;
		default:		bValChkEnable = TRUE;	bValRadioEnable = TRUE; /* bRTCSEnable = m_bRTCSVal;*/ bGenEnable = FALSE;	break;
	}

	GetDlgItem(IDC_SCHED_VAL_GROUP)->EnableWindow(bValChkEnable);
	GetDlgItem(IDC_SCHED_VAL_ICON)->EnableWindow(bValChkEnable);
	GetDlgItem(IDC_SCHED_VAL_CHECK)->EnableWindow(bValChkEnable);
	GetDlgItem(IDC_SCHED_VAL_GROUND_RADIO)->EnableWindow(bValRadioEnable);
	GetDlgItem(IDC_SCHED_VAL_ONBOARD_RADIO)->EnableWindow(bValRadioEnable);
	GetDlgItem(IDC_SCHED_VAL_BOTH_RADIO)->EnableWindow(bValRadioEnable);

	GetDlgItem(IDC_SCHED_GEN_GROUP)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_SCHED_GEN_ICON)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_SCHED_GEN_CHECK)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_SCHED_GEN_BIN_LABEL)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_SCHED_GEN_BIN_STATIC)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_SCHED_GEN_PROC_LABEL)->EnableWindow(bGenEnable);
	GetDlgItem(IDC_SCHED_GEN_PROC_STATIC)->EnableWindow(bGenEnable);
	
	GetDlgItem(IDC_SCHED_RTCS_GROUP)->EnableWindow(bRTCSEnable);
	GetDlgItem(IDC_SCHED_RTCS_CHECK)->EnableWindow(bRTCSEnable);
	GetDlgItem(IDC_SCHED_RTCS_SET_LABEL)->EnableWindow(bRTCSEnable);
	GetDlgItem(IDC_SCHED_RTCS_SET_EDIT)->EnableWindow(bRTCSEnable);
	GetDlgItem(IDC_SCHED_RTCS_SET_BUTTON)->EnableWindow(bRTCSEnable);
	
	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnSetActive() 
//
//  Description :   This member function is called by the framework when
//					the page is chosen by the user and becomes the active
//					page. 
//                  
//  Return :        BOOL -
//					Nonzero if the page was successfully set active; 
//					otherwise 0.
//							
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CSchedPage::OnSetActive()
{
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	if (pSheet->GetFunc() == nPackage)
	{
		if (pSheet->GetCLSPage()->GetVal())
			m_eValLoc = (eValidLoc)pSheet->GetCLSPage()->GetValLoc();
	}

	if (pSheet->GetFunc() == nUpdSTOL)
		m_eValLoc = BOTH;
	
	if( pSheet->GetFunc() == nPackage || pSheet->GetFunc() == nUpdSTOL )
	{
		BOOL bUpdate =	(pSheet->GetFramePage())->GetUse()	||
						(pSheet->GetStarPage())->GetUse()	||
						(pSheet->GetRTCSPage())->GetUse();

		GetDlgItem(IDC_SCHED_LOC_NEW_LABEL)->EnableWindow(bUpdate);
		GetDlgItem(IDC_SCHED_LOC_NEW_STATIC)->EnableWindow(bUpdate);
	}

	((CButton*)GetDlgItem(IDC_SCHED_VAL_CHECK))->SetCheck(m_bVal);
	((CButton*)GetDlgItem(IDC_SCHED_VAL_GROUND_RADIO))->SetCheck(m_eValLoc == GROUND ? TRUE : FALSE);
	((CButton*)GetDlgItem(IDC_SCHED_VAL_ONBOARD_RADIO))->SetCheck(m_eValLoc == ONBOARD ? TRUE : FALSE);
 	((CButton*)GetDlgItem(IDC_SCHED_VAL_BOTH_RADIO))->SetCheck(m_eValLoc == BOTH ? TRUE : FALSE);
	((CButton*)GetDlgItem(IDC_SCHED_GEN_CHECK))->SetCheck(m_bGen);
	((CButton*)GetDlgItem(IDC_SCHED_RTCS_CHECK))->SetCheck(m_bRTCSVal);
	
	OnSchedValCheck();
	OnSchedGenCheck();
	OnSchedRTCSValCheck();
	ResizeFilenames();
	
	// Setup the proper Wizard Buttons
	DWORD dWizFlags = PSWIZB_FINISH;
	if ((m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_FINISH;
	else if ((m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_NEXT;
	else if ((!m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_FINISH;
	else if ((!m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_NEXT;

	pSheet->SetWizardButtons(dWizFlags);		
	GetAppRegMakeSched()->m_nValSchedType = (int)m_eValLoc;
	
	return CResizablePage::OnSetActive();
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnKillActive() 
//
//  Description :   This member function is called by the framework when 
//					the page is no longer the active page. 
//                  
//  Return :        BOOL -
//					Nonzero if the page was successfully set active; 
//					otherwise 0.
//							
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CSchedPage::OnKillActive()
{
	return CResizablePage::OnKillActive();
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnSchedValCheck() 
//
//  Description :   This member function is called by the framework when 
//					validate schedule is checked.. 
//                  
//  Return :        
//					void	-	none.					 
//
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/	
void CSchedPage::OnSchedValCheck() 
{
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	BOOL bEnable = ((CButton*)GetDlgItem(IDC_SCHED_VAL_CHECK))->GetCheck();
	m_bVal = bEnable;

	GetAppRegMakeSched()->m_nValSched = bEnable;
	// If we are doing a complete package and we selected to validate the
	// CLS file, then don't allow the user to change the validation location
	if ((pSheet->GetFunc() == nPackage) ||
		(pSheet->GetFunc() == nValCLS))
	{
		if (pSheet->GetCLSPage()->GetVal())
		{
			bEnable = FALSE;
		}
	}

	GetDlgItem(IDC_SCHED_VAL_GROUND_RADIO)->EnableWindow(bEnable);
	GetDlgItem(IDC_SCHED_VAL_ONBOARD_RADIO)->EnableWindow(bEnable);
	GetDlgItem(IDC_SCHED_VAL_BOTH_RADIO)->EnableWindow(bEnable);
	
	OnSchedRTCSValCheck();
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnSchedValCheck() 
//
//  Description :   This member function is called by the framework when 
//					validate schedule is checked.. 
//                  
//  Return :        
//					void	-	none.					 
//
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/	
void CSchedPage::OnSchedRTCSValCheck()
{
	// Get a pointer to the parent sheet
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();

	BOOL bVal		= ((CButton*)GetDlgItem(IDC_SCHED_VAL_CHECK))->GetCheck();
	GetAppRegMakeSched()->m_nValSched = bVal;
	
//	m_bRTCSVal = bRTCSVal;

	// Only update the registry and the member variable if validation is enabled.
	if (bVal){
		m_bRTCSVal	= ((CButton*)GetDlgItem(IDC_SCHED_RTCS_CHECK))->GetCheck();
		if(!m_bSchedUpd)
			GetAppRegMakeSched()->m_nRTCSVal  = m_bRTCSVal;
	}

	GetDlgItem(IDC_SCHED_RTCS_SET_LABEL)->EnableWindow(bVal && m_bRTCSVal);
	GetDlgItem(IDC_SCHED_RTCS_SET_EDIT)->EnableWindow(bVal && m_bRTCSVal);
	GetDlgItem(IDC_SCHED_RTCS_SET_BUTTON)->EnableWindow(bVal && m_bRTCSVal);
	GetDlgItem(IDC_SCHED_RTCS_CHECK)->EnableWindow(bVal);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnSchedValCheck() 
//
//  Description :   This member function is called by the framework when 
//					validate schedule is checked.. 
//                  
//  Return :        
//					void	-	none.					 
//
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/	
void CSchedPage::OnSchedGenCheck() 
{
	bool bEnable = FALSE;
	if (GetDlgItem(IDC_SCHED_GEN_CHECK)->IsWindowEnabled()){
		// Generate onboard schedules is enabled.
		// Enable the static text.
		m_bGen = ((CButton*)GetDlgItem(IDC_SCHED_GEN_CHECK))->GetCheck();
		bEnable = TRUE;
	} 
	GetDlgItem(IDC_SCHED_GEN_BIN_LABEL)->EnableWindow(bEnable);
	GetDlgItem(IDC_SCHED_GEN_BIN_STATIC)->EnableWindow(bEnable);
	GetDlgItem(IDC_SCHED_GEN_PROC_LABEL)->EnableWindow(bEnable);
	GetDlgItem(IDC_SCHED_GEN_PROC_STATIC)->EnableWindow(bEnable);
	if (bEnable){
		// Generate onboard schedules is enabled.
		// Save off the check box status, Only if its not SchedUpd
		if(!m_bSchedUpd) GetAppRegMakeSched()->m_nOBSchedGen = m_bGen;
	}
//	m_bGen = bEnable;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnSchedValCheck() 
//
//  Description :   This member function is called by the framework when 
//					validate schedule is checked.. 
//                  
//  Return :        
//					void	-	none.					 
//
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/	
void CSchedPage::OnSchedValRadio() 
{
	if (((CButton*)GetDlgItem(IDC_SCHED_VAL_GROUND_RADIO))->GetCheck())
	{
		m_eValLoc = GROUND;
	}
	else if (((CButton*)GetDlgItem(IDC_SCHED_VAL_ONBOARD_RADIO))->GetCheck())
	{
		m_eValLoc = ONBOARD;
	}
	else
	{
		m_eValLoc = BOTH;
	}
	OnSchedRTCSValCheck();
	GetAppRegMakeSched()->m_nValSchedType = (int)m_eValLoc;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnSchedValCheck() 
//
//  Description :   This member function is called by the framework when 
//					validate schedule is checked.. 
//                  
//  Return :        
//					void	-	none.					 
//
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/	
void CSchedPage::OnSize(UINT nType, int cx, int cy) 
{
	CResizablePage::OnSize(nType, cx, cy);
	ResizeFilenames();
}	

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::ResizeFilenames()
//
//  Description :   This member function reformats the file pathnames
//                  entered in the dialog window. 
//					. 
//                  
//  Return :        
//					void	-	none.					 
//
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/	
void CSchedPage::ResizeFilenames()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	CString strBinFilename;
	CString strLoadFilename;
	CString strUpdateFilename;
	CString strOldFilename;

	switch (pSheet->GetFunc())
	{
		case nUpdSTOL:
		case nPackage:
		{
			if( (pSheet->GetFramePage())->GetUse()	||
				(pSheet->GetStarPage())->GetUse()	||
				(pSheet->GetRTCSPage())->GetUse() )
			{
				strBinFilename		= pSheet->MakeFilepath(nFolderSTOL, nBinary,TRUE);
				strLoadFilename		= pSheet->MakeFilepath(nFolderSTOL, nSTOL,	TRUE);
				strUpdateFilename	= pSheet->MakeFilepath(nFolderSTOL, nSTOL,	FALSE);
			}
			else
			{
				CString	strExtS;
				CString strExtB;
				COXUNC	unc(pSheet->GetOrig());

				if(GetAppRegMisc()->m_bShowExt)
				{
					GetAppDocTemplate(nSTOL)->GetDocString(strExtS, CDocTemplate::filterExt);
					GetAppDocTemplate(nBinary)->GetDocString(strExtB, CDocTemplate::filterExt);
				}

				unc.File()			= unc.Base() + _T("_load") + strExtB;
				strBinFilename		= unc.Full();
				unc.File()			= unc.Base() + strExtS;
				strLoadFilename		= unc.Full();
				strUpdateFilename.Empty();
			}
				
			strOldFilename = pSheet->GetOrig();
			break;
		}
		case nValSTOL:
		{
			strBinFilename.Empty();
			strLoadFilename.Empty();
			strUpdateFilename.Empty();
			strOldFilename = pSheet->GetFull();
			break;
		}
		default:
		{
			strBinFilename.Empty();
			strLoadFilename.Empty();
			strUpdateFilename.Empty();
			strOldFilename.Empty();
			break;
		}
	}
	
	PathSetDlgItemPath(this->m_hWnd, IDC_SCHED_LOC_OLD_STATIC,	strOldFilename);
	PathSetDlgItemPath(this->m_hWnd, IDC_SCHED_LOC_NEW_STATIC,	strUpdateFilename);
	PathSetDlgItemPath(this->m_hWnd, IDC_SCHED_GEN_BIN_STATIC,	strBinFilename);
	PathSetDlgItemPath(this->m_hWnd, IDC_SCHED_GEN_PROC_STATIC, strLoadFilename);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnWizardFinish() 
//
//  Description :   This member function is called by the framework when 
//					the finish dialog window is displayed. 
//                  
//  Return :        BOOL	
//						TRUE	-	okay.
//						FALSE	-	cancel				 
//
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/	
BOOL CSchedPage::OnWizardFinish() 
{
	((CGenUpdSheet*)GetParent())->ShowWindow(SW_HIDE);

	CFinishDialog dlg;
	if (dlg.DoModal() == IDOK)
		return TRUE;
	else
		return FALSE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnChangeSchedRTCSSetEdit()
//
//  Description :   This member function is called by the framework when 
//					a RTCS set file name is entered in the RTCS set edit
//					control.  
//                  
//  Return :        
//					void	-	none.					 
//
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/	
void CSchedPage::OnChangeSchedRTCSSetEdit() 
{
	GetDlgItem(IDC_SCHED_RTCS_SET_EDIT)->GetWindowText(m_strRTCSSet);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CSchedPage::OnSchedRTCSSetButton() 
//
//  Description :   This member function is called by the framework when 
//					the RTCS set button is checked. 
//                  
//  Return :        
//					void	-	none.					 
//
//  Parameters : 
//				  	void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/	
void CSchedPage::OnSchedRTCSSetButton() 
{
	COXUNC uncFilename (m_strRTCSSet);
	CString strExt;
	CString strFilter;
	GetAppDocTemplate(nRTCSSet)->GetDocString(strExt, CDocTemplate::filterExt);
	GetAppDocTemplate(nRTCSSet)->GetDocString(strFilter, CDocTemplate::filterName);
	strFilter = strFilter + _T("|*") + strExt + _T("||");
	CFileDialog dlg(TRUE, strExt, m_strRTCSSet, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter, NULL);
//	CFileDialog dlg(TRUE, strExt, uncFilename.Base(), OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, strFilter, NULL);
//	CString	strInitialDir = uncFilename.Server() + uncFilename.Share() + uncFilename.Directory();
//	dlg.m_ofn.lpstrInitialDir = strInitialDir;
	if (dlg.DoModal())
	{
		m_strRTCSSet = dlg.GetPathName();
		GetDlgItem(IDC_SCHED_RTCS_SET_EDIT)->SetWindowText(m_strRTCSSet);
		COXUNC	uncRetrievedFilename (m_strRTCSSet);
		uncRetrievedFilename.Full();
		CString strTemp = uncRetrievedFilename.Base();
		GetAppRegMakeSched()->m_strRTCSSetFileName = uncRetrievedFilename.Base();
	}	
}
