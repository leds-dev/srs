#if !defined(AFX_SCHEDPAGE_H__375850E9_0B34_11D5_8019_00609704053C__INCLUDED_)
#define AFX_SCHEDPAGE_H__375850E9_0B34_11D5_8019_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SchedPage.h : header file
//
// MSD (06/12) PR000446: Implemented defaults during SchedUpd.
/////////////////////////////////////////////////////////////////////////////
// CSchedPage dialog

#include "TextFile.h"

class CSchedPage : public CResizablePage
{
	DECLARE_DYNCREATE(CSchedPage)

// Construction
public:
	CSchedPage();
	~CSchedPage();
	void Init(BOOL bFirstWizPage, BOOL bLastWizPage, BOOL bGen, BOOL bVal, BOOL bRTCSVal, eValidLoc eValLoc, BOOL bSchedUpd);

	BOOL GetGen(){return m_bGen;};
	BOOL GetVal(){return m_bVal;};
	BOOL GetRTCSVal(){return m_bRTCSVal;};
	eValidLoc GetValLoc(){return m_eValLoc;};
	CString GetRTCSSetFilename(){return m_strRTCSSet;};
	
// Dialog Data
protected:
	//{{AFX_DATA(CSchedPage)
	enum { IDD = IDD_SCHED_DIALOG };
	//}}AFX_DATA

// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSchedPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSchedPage)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnSetActive();
	afx_msg BOOL OnKillActive();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSchedValCheck();
	afx_msg void OnSchedRTCSValCheck();
	afx_msg void OnSchedGenCheck();
	afx_msg void OnSchedValRadio();
	virtual BOOL OnWizardFinish();
	afx_msg void OnChangeSchedRTCSSetEdit();
	afx_msg void OnSchedRTCSSetButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void	ResizeFilenames();
	BOOL		m_bGen;			//Generate upload - if TRUE, then generate upload files
	BOOL		m_bVal;			//Validate the Sched - if TRUE, then check the next members veriables
	BOOL		m_bRTCSVal;		//Validate the for RTCS - if TRUE, then check the next members veriables
	BOOL		m_bSchedUpd;	// TRUE if doing schedule update, so we don't store values to reg.
	eValidLoc	m_eValLoc;		//Location of procedure execution
	CString		m_strRTCSSet;	//RTCS Set File
	BOOL		m_bFirstWizPage;//TRUE if this the first page of a Wizard
	BOOL		m_bLastWizPage;	//TRUE if this the last page of a Wizard
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCHEDPAGE_H__375850E9_0B34_11D5_8019_00609704053C__INCLUDED_)
