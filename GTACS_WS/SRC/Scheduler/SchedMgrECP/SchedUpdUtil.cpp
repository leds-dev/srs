/**
/////////////////////////////////////////////////////////////////////////////
// SchedUpdUtil.cpp : implementation of the SchedUpdUtil class.            //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
// Revision History:													   //
// Manan Dalal - July-August 2014										   //
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   //
// Release in Build16.2													   //
/////////////////////////////////////////////////////////////////////////////

// SchedUpdUtil.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class is used for updating schedules and creating various oats
// reports.
//
/////////////////////////////////////////////////////////////////////////////
**/

#include "stdafx.h"
#include "schedMgrECP.h"
#include "SchedTime.h"
#include "SearchConstants.h"
#include "SchedUpdUtil.h"

#include <math.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSchedUpdUtil

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::CSchedUpdUtil
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
IMPLEMENT_DYNCREATE(CSchedUpdUtil, CCmdTarget)

CSchedUpdUtil::CSchedUpdUtil()
{
	DWORD nUserNameSize = UNLEN + 1;
	TCHAR cUserName[UNLEN + 1];
	::GetUserName (cUserName, &nUserNameSize);

	m_strUserName	= cUserName;
	m_bImgIMCEnabled= FALSE;
	m_bSndIMCEnabled= FALSE;
	m_bDoScan		= FALSE;
	m_bDoStar		= FALSE;
	m_bDoRTCS		= FALSE;
	m_bScanNew		= FALSE;
	m_bDoRSO		= FALSE;
	m_bDoSRSO		= FALSE;
	m_bUpdFrm		= FALSE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::~CSchedUpdUtil
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CSchedUpdUtil::~CSchedUpdUtil()
{
}


BEGIN_MESSAGE_MAP(CSchedUpdUtil, CCmdTarget)
	//{{AFX_MSG_MAP(CSchedUpdUtil)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSchedUpdUtil message handlers

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::Connect
//	Description :	Creates a socket connection with a remote host
//
//	Return :		BOOL	-	TRUE if connection is successful
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::Connect()
{
	int				nOATSSiteDef	= GetAppRegSite()->m_nDefaultOATS;
	int				nOATSDef		= GetAppRegOATS()->m_nDefault[nOATSSiteDef];
	UINT			nPort			= (UINT)GetAppRegOATS()->m_nPort;
					m_strOATSHost	= GetAppRegOATS()->m_strAbbrev[nOATSSiteDef][nOATSDef];
	CString			strMsg;

	strMsg.Format(_T("Getting connection with OATS %s."), m_strOATSHost);
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
	GetAppOutputBar()->ClearBuild();

	if( !m_CommUtil.CreateAndConnect(m_strOATSHost,nPort) )
	{
		CString strErr;

		strErr = GetLastError()==ERROR_SUCCESS?_T("Unknown Socket Error."):GetLastErrorText(); 
		strMsg.Format(IDS_UPDATE_COMM_ERROR,m_strOATSHost,strErr);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	strMsg.Format(IDS_UPDATE_CONN_OPEN_OK, m_strOATSHost);
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::Disconnect
//	Description :	Disconnects from host and closes socket connection.
//
//	Return :		void
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::Disconnect()
{
	CString strMsg;

	if( !m_CommUtil.ShutdownAndClose() )
	{
		strMsg.Format(IDS_UPDATE_CONN_SHUTDOWN_ERROR, m_strOATSHost, GetLastError());
		strMsg.TrimRight();
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return;
	}

	strMsg.Format(IDS_UPDATE_CONN_CLOSE_OK, m_strOATSHost);
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::RemoveGroundBlock
//	Description :	Remove Ground Block removes all lines between the
//					##ground begin and ##ground end directives. The
//					ground block must start on the first line read in
//					otherwise it is assumed that no ground block
//					exists.
//
//	Return :		int		-	The number of lines removed.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CSchedUpdUtil::RemoveGroundBlock(const CString strBlockType)
{
	int		nNumRemoved	= 0;
	int		nCurPos		= 0;
	int		nTotalLines	= m_txtNewSched.GetLineCount();
	CString strBegin	= _T("##ground begin ")	+ strBlockType;
	CString strEnd		= _T("##ground end ")	+ strBlockType;
	CString strLine;

	// If we are not at the end of the file
	while( nCurPos < nTotalLines )
	{
		// Read in the next line.  Check to see if it is a ##ground
		// begin directive
		strLine = m_txtNewSched.GetTextAt(nCurPos);

		if( m_txtNewSched.ScanLine(strLine, strBegin) )
		{
			m_txtNewSched.RemoveAt(nCurPos);
			nNumRemoved++;
			nTotalLines--;
			bool bEndNotFound = true;

			// Now that a ##ground begin directive has been found keep
			// searching for a ##ground end until either one is found,
			// of the end of the file is reached.
			while( bEndNotFound && (nCurPos < nTotalLines) )
			{
				strLine = m_txtNewSched.GetTextAt(nCurPos);

				if( m_txtNewSched.ScanLine(strLine, strEnd) )
					bEndNotFound = false;

				m_txtNewSched.RemoveAt(nCurPos);
				nNumRemoved++;
				nTotalLines--;
			}
		}
		else
			nCurPos++;
	}

	// Return the number of lines removed
	return nNumRemoved;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::OpenSchedFiles
//	Description :	OpenSchedFiles is used to open the original
//					schedule file then copy it to a new schedule file.
//
//	Return :		BOOL	-	TRUE if file was successfully opened.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::OpenSchedFiles()
{
	// If there is an error then we can't do anything except return false
	CString strText;
	COXUNC	uncSchedfile(m_strNewSched);

	if( (uncSchedfile.Extension()).IsEmpty() )
	{
		GetAppDocTemplate(nSTOL)->GetDocString(strText, CDocTemplate::filterExt);
		m_strNewSched += strText;
	}

	// Copy the original command schedule to the new command schedule
	// First check if filenames are the same
	// If so issue an error message.
	if (0 == m_strOldSched.CompareNoCase(m_strNewSched)){
		// The filenames are same.  Can't overwrite file
		// Output an error message to the report file
		// Close the report file and return false
		strText = _T("Error: Can not update schedule files with same name. existing: ") + m_strOldSched + _T(" updated: ") + m_strNewSched;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;

	} else if( !CopyFile(m_strOldSched, m_strNewSched, false)){
		// If we make it here, the copy failed
		// Output an error message to the report file
		// Close the report file and return false
		strText = _T("Error copying file ") + m_strOldSched + _T(" to ") + m_strNewSched;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Open the new command schedule for Read/Write
	if( !m_txtNewSched.Load(m_strNewSched, CTextFile::RW, &m_fileException) )
	{
		// If we make it here, we could not open the new command schedule.
		// Output an error message to the report file
		// Delete the new command schedule (if possible)
		// Close the report file and return false
		strText = _T("Error opening file ") + m_strNewSched;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		DeleteFile(m_strNewSched);
		return FALSE;
	}

	// Invalidate the validation record within the prolog
	if( !m_txtNewSched.WriteValidationStatus(UNVALIDATED, BOTH) )
	{
		strText = _T("Error: Unable to change validation status of ") + uncSchedfile.File();
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		DeleteFile(m_strNewSched);
		return FALSE;
	}

	// Remove OverRide Directive if present
	if (m_txtNewSched.IsOverRideSet()){
		m_txtNewSched.GetProlog()->GetOverRide()->RemoveOverRide();
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::CloseFiles
//	Description :	CloseFiles will close all of the files the
//					OpenFiles opened.
//
//	Return :		BOOL	-	TRUE if file was successfully closed.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::CloseFiles()
{
	CString strMsg;

	// Write out and Close the new schedule file
	if( !m_txtNewSched.Close(CTextFile::WRITE, &m_fileException) )
	{
		strMsg = m_txtNewSched.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	m_txtNewSched.Clear();

	// If the scan already existed, the just close the file
	// Otherwise write out and close the file
	if( m_bDoScan )
	{
		m_txtScan.Close(CTextFile::NOWRITE, &m_fileException);
		m_txtScan.Clear();

		if( m_bScanNew )
		{
			m_txtSector.Close(CTextFile::NOWRITE, &m_fileException);
			m_txtSector.Clear();
		}
	}

	// If a new star file was created, then write out and close the file
	if( m_bDoStar )
	{
		if( !m_txtStar.Close(CTextFile::WRITE, &m_fileException) )
		{
			strMsg = m_txtStar.ExceptionErrorText(&m_fileException);
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		m_txtStar.Clear();
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::DeleteFiles
//	Description :	DeleteFiles will close all of the files the
//					OpenFiles opened if unable to update schedule due
//					to error.
//
//	Return :		BOOL	-	TRUE if file was successfully deleted.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::DeleteFiles()
{
	DeleteFile(m_strNewSched);

	if( m_bDoScan && m_bScanNew )
		DeleteFile(m_strScan);

	if( m_bDoStar )
		DeleteFile(m_strStar);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::CheckValid
//	Description :	CheckValid opens the existing schedule file reads
//					in the prolog and return true if the file contains
//					a valid validation record.
//
//	Return :		BOOL	-	TRUE if file was successfully
//								validated.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::CheckValid()
{
	CTextFile	oldFile;
	CString		strErr;
	COXUNC		uncFile(m_strOldSched);

	if( !oldFile.Load(m_strOldSched, CTextFile::RO, &m_fileException) )
	{
		strErr = _T("Error opening file ") + m_strOldSched;
		GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
		return FALSE;
	}

	eValidStatus eVStatus;
	oldFile.IsFileValid(eVStatus);

	if( eVStatus != PASSED && eVStatus != WARNING )
	{
		if( oldFile.IsOverRideSet() )
		{
			strErr = _T("The validation status has been overridden for schedule ") + uncFile.File();
			strErr+= _T(". Proceeding with update.");
			GetAppOutputBar()->OutputToEvents(strErr, COutputBar::WARNING);
		}
		else
		{
			strErr = _T("The schedule file is not stamped as being valid.");
			strErr+= _T(" Only validated schedules can be updated.");
			GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
			oldFile.Close(CTextFile::NOWRITE, &m_fileException);
			return FALSE;
		}
	}

	oldFile.Close(CTextFile::NOWRITE, &m_fileException);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::FindTime
//	Description :	FindTime returns the time value found on a stol
//					WAIT directive. The directive must be in the
//					format of "WAIT @(time("233-00:16:30.0"))". The
//					time between the quotes is returned.
//
//	Return :		BOOL	-	TRUE if time in wait directive was
//								found successfully.
//
//	Parameters :
//			CString*	pstrDirLine	-	pointer to the STOL WAIT
//										directive
//			CString*	pstrTime	-	pointer to the time value
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::FindTime(const CString* pstrDirLine, CString* pstrTime)
{
	int	nOpenDelm	= -1;
	int	nCloseDelm	= -1;

	nOpenDelm = pstrDirLine->Find(_T("\""), 0);

	if( (nOpenDelm != -1) && (nOpenDelm < pstrDirLine->GetLength()-1) )
	{
		nCloseDelm = pstrDirLine->Find(_T("\""), nOpenDelm+1);

		if( nCloseDelm == -1 )
		{
			*pstrTime = _T("");
			return FALSE;
		}

		*pstrTime = pstrDirLine->Mid(nOpenDelm+1, (nCloseDelm-nOpenDelm)-1);
		return TRUE;
	}

	nOpenDelm = pstrDirLine->Find(_T("("), 0);

	if( (nOpenDelm != -1) && (nOpenDelm < pstrDirLine->GetLength()-1) )
	{
		nCloseDelm = pstrDirLine->Find(_T(")"), nOpenDelm+1);

		if( nCloseDelm == -1 )
		{
			*pstrTime = _T("");
			return FALSE;
		}

		*pstrTime = pstrDirLine->Mid(nOpenDelm+1, (nCloseDelm-nOpenDelm)-1);
		return TRUE;
	}

	nOpenDelm = pstrDirLine->Find(_T("@"), 0);

	if( (nOpenDelm != -1) && (nOpenDelm < pstrDirLine->GetLength()-1) )
	{
		nCloseDelm = pstrDirLine->Find(_T(";"), nOpenDelm+1);

		if( nCloseDelm != -1 )
		{
			*pstrTime = pstrDirLine->Mid(nOpenDelm+1, (nCloseDelm-nOpenDelm)-1);
			return TRUE;
		}

		nCloseDelm = pstrDirLine->Find(_T("#"), nOpenDelm+1);

		if( nCloseDelm != -1 )
		{
			*pstrTime = pstrDirLine->Mid(nOpenDelm+1, (nCloseDelm-nOpenDelm)-1);
			return TRUE;
		}

		nCloseDelm = pstrDirLine->Find(_T("\\"), nOpenDelm+1);

		if( nCloseDelm != -1 )
			*pstrTime = pstrDirLine->Mid(nOpenDelm+1, (nCloseDelm-nOpenDelm)-1);
		else
			*pstrTime = pstrDirLine->Mid(nOpenDelm+1, pstrDirLine->GetLength());

		return TRUE;
	}

	*pstrTime = _T("");
	return FALSE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::ExtractValue
//	Description :	ExtractValue will return what is on the right hand
//					side of a qualifier.
//
//	Return :		CString		-	value of the string qualifier.
//
//	Parameters :
//			const CString	strLine		-	CString that contains
//											qualifier
//			const CString	strQualifier-	qualifier to find value
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CSchedUpdUtil::ExtractValue(const CString strLine, const CString strQualifier)
{
	CString strValue		= _T("");
	CString strTempLine		= strLine;
	CString strTempQualifier= strQualifier;

	strTempLine.MakeUpper();
	strTempLine.TrimLeft();
	strTempLine.TrimRight();
	strTempQualifier.MakeUpper();

	int nQualifierStart = strTempLine.Find(strTempQualifier);

	if( nQualifierStart != -1 )
	{
		strTempLine = strTempLine.Mid(nQualifierStart);

		if( strTempLine.GetLength() > strTempQualifier.GetLength() )
		{
			strTempLine.Replace(strTempQualifier, NULL);
			strTempLine.TrimLeft();
			int nSpaceStart	= strTempLine.Find(_T(" "));
				strValue	= (nSpaceStart == -1 ? strTempLine : strTempLine.Left(nSpaceStart));
		}
	}

	return strValue;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::CreateUpdateLists
//	Description :	CreateUpdateLists scans through the schedule file
//					and build up an object lists for each instrument.
//
//	Return :		void	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::CreateUpdateLists()
{
	CString strLastTime;
	int		nIndex = 0;
	int		nNewIx = 0;

	while( nIndex < m_txtNewSched.GetLineCount() )
	{
		CString strLine = m_txtNewSched.GetDirLine(nIndex,nNewIx);
		strLine.MakeUpper();
		strLine.TrimLeft();

		nIndex = nIndex + nNewIx;
		nNewIx = 0;

		if( (!strLine.IsEmpty()) && strLine[0] != ctchCOMMENT ) //NEW CHECK BEFORE USE
		{
			if( m_txtNewSched.ScanLine(strLine, ctstrSCAN) )
			{
				if( m_bDoScan )
				{
					CString strFrame	= ExtractValue(strLine, _T("FRAME="));
					CString strSide		= ExtractValue(strLine, _T("SIDE="));
					CString strInst		= ExtractValue(strLine, _T("INSTRUMENT="));
					CString strRepeat	= ExtractValue(strLine, _T("REPEAT="));
					CString strCP		= ExtractValue(strLine, _T("CP="));
					CString strPriority	= ExtractValue(strLine, _T("PRIORITY="));
					CString strMode		= ExtractValue(strLine, _T("MODE="));
					CString strBbcal	= ExtractValue(strLine, _T("BBCAL="));
					CString strExecute	= ExtractValue(strLine, _T("EXECUTE="));

					if( m_bDoSRSO && (strFrame != m_strSRSOFrame) && !m_bDoStaticFrm )
						continue;
					else if( m_bDoStaticFrm && strFrame == m_strSRSOFrame && !m_bDoSRSO )
						continue;

					if( m_bDoSRSO && m_bDoSRSOPreDef && strFrame == m_strSRSOFrame )
						strFrame = m_strSRSOLabel;
					else if( m_bDoRSO && !m_bDoSRSO && strFrame == m_strRSOCurLabel )
						strFrame = m_strRSONewLabel;

					if( (!strFrame.IsEmpty())	&& (!strSide.IsEmpty())		&&
						(!strInst.IsEmpty())	&& (!strLastTime.IsEmpty()) &&
						(!strPriority.IsEmpty())&& (!strMode.IsEmpty()) )
					{
						CString strNewLine;

						if( strInst.CompareNoCase(strInstrument[1]) == 0 )
						{
							strNewLine.Format(_T("%s, %s, %s, %s, %s, %s, %s, %s, %s"),
							strLastTime, strFrame, strRepeat, strCP, strSide,
							strInst, strPriority, strMode, strExecute);
							m_ScanImgList.Add(strNewLine);
						}
						else
						{
							strNewLine.Format(_T("%s, %s, %s, %s, %s, %s, %s, %s, %s, %s"),
							strLastTime, strFrame, strRepeat, strCP, strSide, strInst,
							strPriority, strMode, strBbcal, strExecute);
							m_ScanSndList.Add(strNewLine);
						}
					}
				}
			}
			else if( m_txtNewSched.ScanLine(strLine, ctstrSTAR) )
			{
				if( m_bDoStar )
				{
					CString strInst		= ExtractValue(strLine, _T("INSTRUMENT="));
					CString strDuration = ExtractValue(strLine, _T("DURATION="));
					CString strMax		= ExtractValue(strLine, _T("MAX="));

					if( (!strInst.IsEmpty())&& (!strDuration.IsEmpty()) &&
						(!strMax.IsEmpty())	&& (!strLastTime.IsEmpty()) )
					{
						CString strNewLine;
						strNewLine.Format(_T("%s, %s, %s"),strLastTime,strDuration,strMax);

						if( strInst.CompareNoCase(strInstrument[1]) == 0 )
							m_StarImgList.Add(strNewLine);
						else
							m_StarSndList.Add(strNewLine);
					}
				}
			}
			else if( m_txtNewSched.ScanLine(strLine, ctstrWAIT) )
				FindTime(&strLine, &strLastTime);
		}
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::OpenStarFiles
//	Description :	OpenStarFiles is used to remove all star blocks
//					from the schedule file then creates a new star
//					table file.
//
//	Return :		BOOL	-	TRUE if file was successfully opened.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::OpenStarFiles()
{
	COXUNC	uncStarfile(m_strStar);
	CString strText;

	RemoveStarBlock();

	if( (uncStarfile.Extension()).IsEmpty() )
	{
		GetAppDocTemplate(nStar)->GetDocString(strText, CDocTemplate::filterExt);
		m_strStar += strText;
	}

	// Create the new file
	if( !m_txtStar.Load(m_strStar, CTextFile::CR, &m_fileException) )
	{
		// If we make it here then we could not open the sector file.
		// Close and Delete the new command schedule
		// Close the scan file and if it was a new one,
		// delete it - don't worry about errors
		// Close the sector - don't worry about errors and return false
		strText = _T("Error opening file ") + m_strStar;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		m_txtNewSched.Close(CTextFile::NOWRITE, &m_fileException);
		DeleteFile(m_strNewSched);
		return FALSE;
	}

	// First remove all previous star update records
	if ( TRUE != m_txtNewSched.DelStarUpdateDir()){
		CString strStatusMsg = _T("Error removing old star update records");
		// GetAppOutputBar()->OutputToEvents(strText, COutputBar::WARNING);
	}

	// Insert an update record into the new command
	// schedule stating what star file was used
	strText = _T("Using star file ") + uncStarfile.File();
	GetAppOutputBar()->OutputToEvents(strText, COutputBar::INFO);
	CSchedTime schedTime;
	CString strUpdateLine(ctstrSTAR + _T(" ") + m_strUserName + _T(" ") +
			schedTime.GetCurrSchedTime() + _T(" ") + m_strStar);
	m_txtNewSched.AddUpdateDir(strUpdateLine);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::ReadStarFile
//	Description :	ReadStarFile will read all of the data contained
//					in a star file into global memory.
//
//	Return :		BOOL	-	TRUE if file was successfully read.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::ReadStarFile()
{
	int		nNumStars;
	CString	strText;
	CString strStarLine;
	CString strVersion;
	CString strType;
	CString strNumStars;

	// Delete all of the star data that may have been previously used
	for( int nColumn=0; nColumn < eMaxStarColumns; nColumn++ )
		m_strStarData[nColumn].RemoveAll(); //Index 0 is not used

	// Read first line and strip off its components
	strStarLine	= m_txtStar.GetTextAt(0);
	strVersion	= RemoveFirstToken(&strStarLine);
	strType		= RemoveFirstToken(&strStarLine);
	strType.TrimRight();

	// Make sure it contains a version that is supported
	if( strVersion.CompareNoCase(_T("1.0")) != 0 )
	{
		strText = _T("Star file must be at version 1.0:  ") + strVersion;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Make sure the file is a star file
	if( strType.CompareNoCase(ctstrSTAR) != 0 )
	{
		strText = _T("Star file identification record must indicate ");
		strText+= _T("that the file is not a star file:  ") + strType;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Read in the second line and strip off all of its components
	strStarLine = m_txtStar.GetTextAt(1);
	strNumStars	= RemoveFirstToken(&strStarLine);
	nNumStars	= _ttoi(strNumStars);

	// Make sure that the file contains at least one frame
	if( nNumStars < 1 )
	{
		strText = _T("Invalid number of stars specified in the star file:  ") + strNumStars;
		strText+= _T(". The schedule file was not updated.");
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Set the size of the data structures large enough to hold all of the data
	// Determine how many data lines are within the file
	// Read in each line
	// Split up the line into its compontents and save the data into the
	// global structures
	for(int nColumn=0; nColumn < eMaxStarColumns; nColumn++ )
		m_strStarData[nColumn].SetSize(nNumStars+1); //Index 0 is not used

	int nNumLines = m_txtStar.GetLineCount() - 2;

	for( int nCurrentLine=0; nCurrentLine<nNumLines; nCurrentLine++ )
	{
		strStarLine = m_txtStar.GetTextAt(nCurrentLine+2);

		m_strStarData[eStarTime].SetAt(nCurrentLine,		RemoveFirstToken(&strStarLine));
		m_strStarData[eStarDuration].SetAt(nCurrentLine,	RemoveFirstToken(&strStarLine));
		m_strStarData[eStarWindowNum].SetAt(nCurrentLine,	RemoveFirstToken(&strStarLine));
		m_strStarData[eStarInstrument].SetAt(nCurrentLine,	RemoveFirstToken(&strStarLine));
		m_strStarData[eStarIMCSet].SetAt(nCurrentLine,		RemoveFirstToken(&strStarLine));
		m_strStarData[eStarIndex].SetAt(nCurrentLine,		RemoveFirstToken(&strStarLine));
		m_strStarData[eStarID].SetAt(nCurrentLine,			RemoveFirstToken(&strStarLine));
		m_strStarData[eStarNumLooks].SetAt(nCurrentLine,	RemoveFirstToken(&strStarLine));
		m_strStarData[eStarLookNum].SetAt(nCurrentLine,		RemoveFirstToken(&strStarLine));
		m_strStarData[eStarRepeats].SetAt(nCurrentLine,		RemoveFirstToken(&strStarLine));
		m_strStarData[eStarCycNS].SetAt(nCurrentLine,		RemoveFirstToken(&strStarLine));
		m_strStarData[eStarIncNS].SetAt(nCurrentLine,		RemoveFirstToken(&strStarLine));
		m_strStarData[eStarCycEW].SetAt(nCurrentLine,		RemoveFirstToken(&strStarLine));
		m_strStarData[eStarIncEW].SetAt(nCurrentLine,		RemoveFirstToken(&strStarLine));
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::RemoveStarBlock
//	Description :	RemoveScanBlock removes all of the star lines that
//					were automatically inserted by a previous update.
//					It will remove the stardata and starinfo lines
//					then call RemoveGroundBlock.
//
//	Return :		int		-	The number of lines removed.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CSchedUpdUtil::RemoveStarBlock()
{
	int		nNumRemoved = 0;
	int		nCurPos		= 0;
	int		nTotalLines	= m_txtNewSched.GetLineCount();
	CString strLine;

	// If we are not at the end of the file
	while( nCurPos < nTotalLines )
	{
		// Read in the next line.  Check to see if it is a scandata directive
		strLine = m_txtNewSched.GetTextAt(nCurPos);

		if(	m_txtNewSched.ScanLine(strLine, ctstrSTARDATA) ||
			m_txtNewSched.ScanLine(strLine, ctstrSTARINFO) )
		{
			m_txtNewSched.RemoveAt(nCurPos);
			nNumRemoved++;
			nTotalLines--;

			while( strLine.ReverseFind(_T('\\')) != -1 )
			{
				strLine = m_txtNewSched.GetTextAt(nCurPos);
				m_txtNewSched.RemoveAt(nCurPos);
				nNumRemoved++;
				nTotalLines--;
			}
		}
		else
			nCurPos++;
	}

	nNumRemoved = nNumRemoved + RemoveGroundBlock(ctstrSTAR);

	return nNumRemoved;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::CreateStarList
//	Description :	CreateStarList creates a star table based on the
//					values returned from OATS.
//
//	Return :		BOOL	-	TRUE if table was successfully created.
//
//	Parameters :
//			CStringArray*	pStarList		-	pointer to the in memory
//												star values obtained from
//												OATS
//			MSG_151* 		pMsg151			-	pointer to the 151 message
//			CWordArray*		pMax			-	pointer to the array of
//												star max values
//			CStringArray*	pDur			-	pointer to the array of
//												star durations
//			int*			pnTotalWindows	-	pointer to the number of
//												windows updated
//			int*			nObjIdCnt		-	pointer to the number of
//												object ids in the table
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::CreateStarList(
	CStringArray*	pStarList,
	CPtrList*		pList,
	CWordArray*		pMax,
	CStringArray*	pDur,
	int*			pnTotalWindows,
	int*			nObjIdCnt)
{
	CString			strPrompt;
	CString			strQuote(_T("\""));
	CString			strObjId;
	CString			strMsg;
	CPtrList		list;
	MSG_151*		pMsg151 = NULL;
	int				nAryNum = 0;
	POSITION		pos1	= pList->GetHeadPosition();

	while( pos1 != NULL )
	{
		pMsg151 = (MSG_151*)pList->GetNext(pos1);

		int	nWindowsInReq = pMsg151->m_151Hdr.m_nNumWindows;

		m_CommUtil.WinCnt(pMsg151->m_151Hdr.m_nNumWindows);

		if( !m_CommUtil.SendMsg(pMsg151, 51, &list) )
		{
			CString strErr;
			
			strErr = GetLastError()==ERROR_SUCCESS?_T(""):GetLastErrorText();
			
			if( strErr.IsEmpty() )
				strErr = _T("The Schedule file was not updated.");
			else
				strErr+= _T(" The Schedule file was not updated.");
			
			strMsg.Format(IDS_UPDATE_WRITE_ERROR, strErr);				
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			DeleteListElements(&list);
			return FALSE;
		}

		if( list.IsEmpty() )
		{
			strMsg.Format(_T("No star data returned from OATS."));
			strMsg += _T(" The Schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		if( nWindowsInReq != m_CommUtil.WinCnt() )
		{
			strMsg.Format(
				_T("The number of windows in message 151 sent to OATS ")
				_T("does not match the number of windows in message 51 recieved from OATS."));
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			strMsg = _T("The Schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			DeleteListElements(&list);
			return FALSE;
		}

		POSITION	pos2 = list.GetHeadPosition();
		MSG_51*		p51Msg;
		int			nInstF;

		while( pos2 != NULL )
		{
			p51Msg = (MSG_51*)list.GetNext(pos2);
			nInstF = (p51Msg->m_hdr.m_nMsgSubType == 1 ? -1 : -4);

			if( p51Msg->m_51Hdr.m_nNumWindows < 0 || p51Msg->m_51Hdr.m_nNumWindows > MAX_51_WINDOW_DATA )
			{
				strMsg.Format(IDS_UPDATE_STARS_FRAMES,_T("'the number of windows in message'"));
				strMsg += _T(" The schedule file was not updated.");
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				DeleteListElements(&list);
				return FALSE;
			}

			for( int nCurWindow=0; nCurWindow<(int)p51Msg->m_51Hdr.m_nNumWindows; nCurWindow++ )
			{
				(*pnTotalWindows)++;

				CSchedTime	t1;
				CSchedTime	wd(pDur->GetAt(nAryNum));

				int nCurMax			= pMax->GetAt(nAryNum++);
				int nCurTotal		= 0;
				int nLooksInWindow	= 0;
				int nStarsInWindow	= p51Msg->m_51WindowData[nCurWindow].m_data.m_nNumStarsInWindow;
				BOOL endWindow		= FALSE;

				if( nStarsInWindow < 1 || nCurMax < 1 )
				{
					CString strTemp;
					strObjId.Format(_T("%d"), ++(*nObjIdCnt));
					strTemp.Format(_T("** No Stars for window #%d **,,%d,%s,,%s,,,,,,,,"),
						*pnTotalWindows,*pnTotalWindows,strInstrument[p51Msg->m_hdr.m_nMsgSubType],strObjId);
					pStarList->Add(strTemp);

					if( nStarsInWindow < 1 )
						strMsg.Format(_T("OATS returned no stars for STAR WINDOW %d."), *pnTotalWindows);
					else
						strMsg.Format(_T("MAX=0 for STAR WINDOW %d. ")
						_T("All remaining items for this window are omitted."), *pnTotalWindows);

					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
					continue;
				}

				if( nStarsInWindow > MAX_51_STAR_DATA )
				{
					strMsg.LoadString(IDS_MSG51_STATUS2);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
				}

				for( int nCurStar=0; nCurStar<nStarsInWindow; nCurStar++ )
				{
					int nStarLooks = p51Msg->m_51WindowData[nCurWindow].m_51StarData[nCurStar].m_data.m_nNumLooksForStar;

					if( nCurStar == 0 )
						nLooksInWindow = nStarsInWindow*nStarLooks;

					if( nCurStar == (nStarsInWindow-1) )
						endWindow = TRUE;

					for( int nCurLook=0; nCurLook<nStarLooks; nCurLook++ )
					{
						nCurTotal++;

						if( nCurTotal <= nCurMax )
						{
							CString strLookTime;
							ConvertBCDToStringTime(p51Msg->m_51WindowData[nCurWindow].
							m_51StarData[nCurStar].m_51LookData[nCurLook].
							m_data.m_nBCDTime,&strLookTime);

							CSchedTime	t2(strLookTime);
							double		t4;
							int			nDwellRepeats = p51Msg->m_51WindowData[nCurWindow].m_51StarData[nCurStar].
																m_51LookData[nCurLook].m_data.m_nDwellRepeats;

							if( nCurTotal == 1 )
								t1 = t2;

							if( (nLooksInWindow == nCurTotal && endWindow) || nCurTotal == nCurMax )
								t4 = (nDwellRepeats + 1) * nInstF;
							else
							{
								CString strTmpTime;

								if( nCurLook < (nStarLooks-1) )
									ConvertBCDToStringTime(p51Msg->m_51WindowData[nCurWindow].
									m_51StarData[nCurStar].m_51LookData[nCurLook+1].m_data.m_nBCDTime, &strTmpTime);
								else
									ConvertBCDToStringTime(p51Msg->m_51WindowData[nCurWindow].
									m_51StarData[nCurStar+1].m_51LookData[0].m_data.m_nBCDTime, &strTmpTime);

								CSchedTime t3(strTmpTime);

								t4 = t3.GetSchedTimeInSeconds() - t2.GetSchedTimeInSeconds();
								t4 = (t4 <= ctnMaxStarDwell ? t4 : ctnMaxStarDwell);
							}

							CString strIMC(p51Msg->m_51Hdr.m_cIMCSetID);
							CString strLine;
							strObjId.Format(_T("%d"), ++(*nObjIdCnt));
							strLine.Format(
								_T("%s, %d, %d, %s, %s, %s, %d, %d, %d, %d, %d, %d, %d, %d"),
								strLookTime, (int)t4, *pnTotalWindows,
								strInstrument[p51Msg->m_hdr.m_nMsgSubType], strIMC, strObjId,
								p51Msg->m_51WindowData[nCurWindow].m_51StarData[nCurStar].m_data.m_nStarID,
								p51Msg->m_51WindowData[nCurWindow].m_51StarData[nCurStar].m_data.m_nNumLooksForStar,
								p51Msg->m_51WindowData[nCurWindow].m_51StarData[nCurStar].m_51LookData[nCurLook].m_data.m_nLookNum,
								p51Msg->m_51WindowData[nCurWindow].m_51StarData[nCurStar].m_51LookData[nCurLook].m_data.m_nDwellRepeats,
								p51Msg->m_51WindowData[nCurWindow].m_51StarData[nCurStar].m_51LookData[nCurLook].m_data.m_nNSCyc,
								p51Msg->m_51WindowData[nCurWindow].m_51StarData[nCurStar].m_51LookData[nCurLook].m_data.m_nNSInc,
								p51Msg->m_51WindowData[nCurWindow].m_51StarData[nCurStar].m_51LookData[nCurLook].m_data.m_nEWCyc,
								p51Msg->m_51WindowData[nCurWindow].m_51StarData[nCurStar].m_51LookData[nCurLook].m_data.m_nEWInc);
							pStarList->Add(strLine);
						}
						else
						{
							strMsg.Format(IDS_UPDATE_TOO_MANY_STARS);
							GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
						}
					}
				}

				nCurTotal++;

				while( nCurTotal <= nCurMax )
				{
					CString strLine;
					strLine.Format(_T("** Not used #%d **,,%d,,,%d,,,,,,,,"),nCurTotal,*pnTotalWindows,++(*nObjIdCnt));
					nCurTotal++;
					pStarList->Add(strLine);
				}
			}
		}

		DeleteListElements(&list);
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::ProcessStarInfo
//	Description :	ProcessStarInfo processes star request then builds
//					the star table.
//
//	Return :		BOOL	-	TRUE if request was successfully
//								processed.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::ProcessStarInfo()
{
	// An in memory "string" copy of the new star table
	// Does not include any header records
	CStringArray	starList;
	CStringArray	durList;
	CWordArray		nMax;
	CPtrList		msgList;
	CString			strMsg;
	COXUNC			unc(m_strOldSched);
	int				nTotalWindows	= -1;
//	int				nMsgs			= 0;
	int				nObjCnt			= 0;

	strMsg.Format(_T("Updating the Schedule File %s for Stars"), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	if( m_StarImgList.GetSize() == 0 && m_StarSndList.GetSize() == 0 )
	{
		strMsg.Format(IDS_UPDATE_NO_DATA_ERROR,ctstrINSTRUMENT,ctstrSTAR);
		strMsg += _T(" The schedule file was not updated.");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		DeleteListElements(&msgList);
		return FALSE;
	}

	// Pull the data out of the star imager table that is required for OATS.
	// Put the data into a 151 msg. If at least one request exists send the
	// request to OATS and build the in memory copy of the star table
	if( m_StarImgList.GetSize() > 0 )
		BuildStarMessage(nMax,m_StarImgList,durList,msgList,1,m_bImgIMCEnabled);

	// Repeat the steps above with the star sounder table. Data will be
	// appended to the "in memory" copy of the star table
	if( m_StarSndList.GetSize() > 0 )
		BuildStarMessage(nMax,m_StarSndList,durList,msgList,2,m_bSndIMCEnabled);

	if( !CreateStarList(&starList,&msgList,&nMax,&durList,&nTotalWindows,&nObjCnt) )
	{
		DeleteListElements(&msgList);
		return FALSE;
	}

	DeleteListElements(&msgList);

	// Write the "in memory" copy of the star table out to the star
	// file buffer. Write the star file buffer out to disk and clear
	// out the buffer
	m_txtStar.AppendText(_T("1.0, Star"));
	CString	strUpdateText;
	CTime t = CTime::GetCurrentTime();
	time_t osBinaryTime = t.GetTime();  // time_t defined in <time.h>
	strUpdateText.Format(_T("%d, %ld, %s"), starList.GetSize(), (long)osBinaryTime, m_strUserName);
	m_txtStar.AppendText(strUpdateText);

	// index k = Star Count
	for( int k=0; k<starList.GetSize(); k++ )
		m_txtStar.AppendText(starList.GetAt(k));

	if( !m_txtStar.Close(CTextFile::WRITE, &m_fileException) )
	{
		strMsg = m_txtStar.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		DeleteListElements(&msgList);
		return FALSE;
	}

	m_txtStar.Clear();
	m_txtStar.Load(m_strStar, CTextFile::RO, &m_fileException);

	if( !UpdateScheduleStars()){
		DeleteListElements(&msgList);
		return FALSE;
	}

	strMsg.Format(_T("Schedule file %s has been updated for Stars successfully"), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::BuildStarMessage
//	Description :	BuildStarMessage builds a 151 message for OATS
//					processing.
//
//	Return :		void	-
//
//	Parameters :
//			MSG_151* 		pMsg151			-	pointer to the 151 message
//			CWordArray&		pMax			-	list of star max values
//			CStringArray&	pDur			-	list of star durations
//			CStringArray&	starInstrList	-	list of star directives
//												for specified instrument
//			int*			nInstrument		-	instrument value
//			BOOL			bIMCEnabled		-	is IMC set inabled
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::BuildStarMessage(
	CWordArray&		nMax,
	CStringArray&	starInstrList,
	CStringArray&	durList,
	CPtrList&		list,
	int				nInstrument,
	BOOL			bIMCEnabled)
{
	int				nMessage= 0;
	CString			strTmp;
	MSG_151*		pMsg151 = new MSG_151;

	// index i = Star Imager Count
	for( int i=0; i<starInstrList.GetSize(); i++ )
	{
		if( nMessage < MAX_151_WINDOWS )
		{
			CStringArray	strData;
			strData.Add(RemoveFirstToken(&starInstrList[i]));
			CString			strDur(RemoveFirstToken(&starInstrList[i]));
			CSchedTime		td(strDur);
			CString			strDurInMin;

			strDurInMin.Format(_T("%f"), td.GetSchedTimeInMinutes());
			strData.Add(strDurInMin);

			strTmp = RemoveFirstToken(&starInstrList[i]);
			nMax.Add((WORD)_ttoi(strTmp));
			durList.Add(strDur);

			Set151FromString(pMsg151, &strData,
				GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC],
				bIMCEnabled, &m_strIMCSet, nInstrument, nMessage);

			nMessage++;
		}
		else
		{
			list.AddTail(pMsg151);
			nMessage= 0;
			pMsg151	= new MSG_151;
			i--;
		}
	}

	list.AddTail(pMsg151);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::UpdateScheduleStars
//	Description :	UpdateScheduleStars updates a schedule for stars.
//
//	Return :		BOOL	-	TRUE if schedule was updated successfully
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::UpdateScheduleStars()
{
	CString strTxt(_T("Updating Schedule Stars..."));
	GetAppOutputBar()->OutputToEvents(strTxt, COutputBar::INFO);

	if( !ReadStarFile() )
		return FALSE;

	int		nCurImgWindow		= 0;
	int		nCurSndWindow		= 0;
	int		nCurStarImgIndex	= FindInstrIndex(ctstrIMGR,	&nCurImgWindow);
	int		nCurStarSndIndex	= FindInstrIndex(ctstrSDR,	&nCurSndWindow);
	int*	nCurrentStarIndex	= NULL;
	int*	nCurrentStarWindow	= NULL;
//	int		nPreviousStarWindow	= -1;
	int		nIndex				= 0;
	int		nNewIx				= 0;
	int		nSCNum				= GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC];
	BOOL	bStarIOMiniSched	= GetAppRegSC()->m_strStarIOMiniSched[GetAppRegSite()->m_nDefaultSC].IsEmpty();
	BOOL	bStarMiniSched		= GetAppRegSC()->m_strStarMiniSched[GetAppRegSite()->m_nDefaultSC].IsEmpty();
	CString strLastTime;


	if( bStarMiniSched && bStarIOMiniSched )
	{
		strTxt = _T("No Star Mini Schedule name! \"See Workspace options.\" The schedule file was not updated.");
		GetAppOutputBar()->OutputToEvents(strTxt, COutputBar::FATAL);
		return FALSE;
	}

	if( !bStarMiniSched && !bStarIOMiniSched )
	{
		strTxt = _T("Only one Star Mini Schedule required for ground execution!!!");
		GetAppOutputBar()->OutputToEvents(strTxt, COutputBar::WARNING);
		strTxt = _T("Remove Star or StarIO mini schedule from ground blocks before executing Schedule from the ground.");
		GetAppOutputBar()->OutputToEvents(strTxt, COutputBar::WARNING);
	}

	if( nCurStarImgIndex == -1 && nCurStarSndIndex == -1 )
	{
		strTxt = _T("No Instrument listing in the star table! The schedule file was not updated.");
		GetAppOutputBar()->OutputToEvents(strTxt, COutputBar::FATAL);
		return FALSE;
	}

	while( nIndex < m_txtNewSched.GetLineCount() )
	{
		CString strLine = m_txtNewSched.GetDirLine(nIndex, nNewIx);
		strLine.MakeUpper();
		strLine.TrimLeft();

		nIndex = nIndex + nNewIx;
		nNewIx = 0;

		if( (!strLine.IsEmpty()) && strLine[0] != ctchCOMMENT ) //NEW CHECK BEFORE USE
		{
			if( m_txtNewSched.ScanLine(strLine, ctstrSTAR) )
			{
				BOOL	bMoreStars		= TRUE;
				int		nMaxUsed		= 0;
				int		nMax			= _ttoi(ExtractValue(strLine, _T("MAX=")));
				int		nCurrentStar	= 0;
				CString strInsertLine;
				CString strDuration		= ExtractValue(strLine, _T("DURATION="));
				CString strInstrument	= ExtractValue(strLine, _T("INSTRUMENT="));
				CString strLastStarClass= ExtractValue(strLine, _T("LAST_STAR_CLASS="));
				CString strLoadReg		= ExtractValue(strLine, _T("LOAD_REG="));
				CStringArray strStarParam;

				strInstrument.MakeUpper();

				nCurrentStarIndex	= (strInstrument == ctstrIMGR) ? &nCurStarImgIndex	: &nCurStarSndIndex;
				nCurrentStarWindow	= (strInstrument == ctstrIMGR) ? &nCurImgWindow		: &nCurSndWindow;

				if( strLastStarClass.IsEmpty() )
					strLastStarClass= ctstrSENSE;

				if( strLoadReg.IsEmpty() )
					strLoadReg		= ctstrLOAD_AND_EXEC;

				CString strTmp;
				int		nStartIndex = _ttoi(m_strStarData[eStarIndex].GetAt(*nCurrentStarIndex));

				if( m_strStarData[eStarTime].GetAt(*nCurrentStarIndex).Find(_T("No Stars for window")) > -1 )
				{
					(strInstrument == ctstrIMGR) ? nCurStarImgIndex++	: nCurStarSndIndex++;
					(strInstrument == ctstrIMGR) ? nCurImgWindow++		: nCurSndWindow++;

					strInsertLine = _T("    #OATS returned no stars for this window.");
					//strInsertLine.Format(
					//	_T("    STARDATA INSTRUMENT=%s DURATION=%s MAX=%d OBJID=0 ")
					//	_T("LAST_STAR_CLASS=%s LOAD_REG=%s"),
					//	strInstrument,strDuration,nMax,strLastStarClass,strLoadReg);
					m_txtNewSched.InsertAt(nIndex++,strInsertLine);
					continue;
				}

				strInsertLine.Format(
					_T("    STARDATA INSTRUMENT=%s DURATION=%s MAX=%d OBJID=%d ")
					_T("LAST_STAR_CLASS=%s LOAD_REG=%s"),
					strInstrument, strDuration, nMax, nStartIndex,
					strLastStarClass, strLoadReg);
				m_txtNewSched.InsertAt(nIndex++,strInsertLine);
				strStarParam.RemoveAll();

				while( bMoreStars )
				{
					CString strInstr(m_strStarData[eStarInstrument].GetAt(*nCurrentStarIndex));
					nCurrentStar++;

					if( m_strStarData[eStarLookNum].GetSize()-1 > (*nCurrentStarIndex) )
					{
						if( _ttoi(m_strStarData[eStarWindowNum].GetAt(*nCurrentStarIndex)) == *nCurrentStarWindow )
						{
							if( m_strStarData[eStarTime].GetAt(*nCurrentStarIndex)[0] != '*' )
							{
								CString strStarBit(ctstrON);
								CString strRepeat(_T("0"));
								int		nDwell = _ttoi(m_strStarData[eStarDuration].GetAt(*nCurrentStarIndex));

								if( nDwell < 0 )
								{
									if( strLastStarClass == ctstrSENSE )
									{
										int	nInstF	= (strInstr.CompareNoCase(_T("IMAGER")) == 0) ? 1 : 4;
										strStarBit	= ctstrOFF;
										strRepeat	= m_strStarData[eStarRepeats].GetAt(*nCurrentStarIndex);
										// StarInfo dwell for last star per Dave Herndon
										nDwell		= ((_ttoi(strRepeat)+1)*nInstF)+15;
									}
								}

								strInsertLine.Format(
									_T("    STARINFO STARID=%s OBJID=%s SCID=%d INSTRUMENT=%s ")
									_T("NUMLOOKS=%s LOOKNUM=%s TIME=\"%s\" DWELL=%d"),
									m_strStarData[eStarID].GetAt(*nCurrentStarIndex),
									m_strStarData[eStarIndex].GetAt(*nCurrentStarIndex), nSCNum,
									m_strStarData[eStarInstrument].GetAt(*nCurrentStarIndex),
									m_strStarData[eStarNumLooks].GetAt(*nCurrentStarIndex),
									m_strStarData[eStarLookNum].GetAt(*nCurrentStarIndex),
									m_strStarData[eStarTime].GetAt(*nCurrentStarIndex),
									nDwell);
								m_txtNewSched.InsertAt(nIndex++,strInsertLine);

								if( !bStarMiniSched )
								{
									CString strInst(m_strStarData[eStarInstrument].GetAt(*nCurrentStarIndex));

									strInst.MakeUpper();
									strStarBit.MakeUpper();
									strInsertLine.Format(_T("START ./%s/procs/%s(\"%s\", \"%s\", %s, \"%s\", %s, %s, %s, %s)"),
										GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],
										GetAppRegSC()->m_strStarMiniSched[GetAppRegSite()->m_nDefaultSC], strInst,
										m_strStarData[eStarTime].GetAt(*nCurrentStarIndex), strRepeat, strStarBit,
										m_strStarData[eStarCycEW].GetAt(*nCurrentStarIndex),
										m_strStarData[eStarIncEW].GetAt(*nCurrentStarIndex),
										m_strStarData[eStarCycNS].GetAt(*nCurrentStarIndex),
										m_strStarData[eStarIncNS].GetAt(*nCurrentStarIndex));
								}

								strStarParam.SetAtGrow(nCurrentStar, strInsertLine);
								nMaxUsed++;
							}
							else
							{
								strInsertLine.Format(_T("    STARINFO STARID=0"));
								m_txtNewSched.InsertAt(nIndex++,strInsertLine);
							}

							(strInstrument == ctstrIMGR) ? nCurStarImgIndex++	: nCurStarSndIndex++;
						}
						else
						{
							(strInstrument == ctstrIMGR) ? nCurImgWindow++		: nCurSndWindow++;
							bMoreStars = FALSE;
						}
					}
					else
						bMoreStars = FALSE;
				}

				m_txtNewSched.InsertAt(nIndex++,_T("##ground begin star"));

				if( !bStarIOMiniSched )
				{
					strInstrument.MakeUpper();
					strLoadReg.MakeUpper();
					strLastStarClass.MakeUpper();
					strInsertLine.Format(_T("START ./%s/procs/%s(%d, \"%s\", \"%s\", \"%s\", \"%s\")"),
						GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],
						GetAppRegSC()->m_strStarIOMiniSched[GetAppRegSite()->m_nDefaultSC], /* InstObj Mini Schedule */
						nStartIndex, strInstrument, strLastStarClass, strLoadReg,
						GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC]);
					m_txtNewSched.InsertAt(nIndex++,strInsertLine);
				}

				if( !bStarMiniSched )
				{
					for( int nStarCmdIndex=1; nStarCmdIndex <= nMaxUsed; nStarCmdIndex++ )
						m_txtNewSched.InsertAt(nIndex++, strStarParam.GetAt(nStarCmdIndex));
				}

				m_txtNewSched.InsertAt(nIndex++,_T("##ground end star"));
			}
			else if( m_txtNewSched.ScanLine (strLine, ctstrWAIT) )
				FindTime(&strLine, &strLastTime);
		}
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::OpenFrameFiles
//	Description :	OpenFrameFiles is used to remove all scan blocks
//					from the schedule file then if frame table exists
//					open.
//
//	Return :		BOOL	-	TRUE if file was successfully opened.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::OpenFrameFiles()
{
	CString strText;

	m_txtNewSched.DelScanUpdateDir();
	m_txtNewSched.DelSectorUpdateDir();

	RemoveScanBlock();

	if( m_bScanNew )
	{
		COXUNC uncScanfile(m_strScan);

		if( (uncScanfile.Extension()).IsEmpty() )
		{
			GetAppDocTemplate(nFrame)->GetDocString(strText, CDocTemplate::filterExt);
			m_strScan += strText;
		}

		// Create the new file
		if( !m_txtScan.Load(m_strScan, CTextFile::CR, &m_fileException) )
		{
			// If we make it here then we could not open the scan file.
			// Output an error message to the report file, Close and
			// Delete the new command schedule.
			strText = _T("Error opening file ") + m_strScan;
			GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
			m_txtNewSched.Close(CTextFile::NOWRITE, &m_fileException);
			DeleteFile(m_strNewSched);
			return FALSE;
		}

		// Insert an update record into the new command schedule
		// stating what frame file was used.
		strText = _T("Creating frame file ") + uncScanfile.File();
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::INFO);
		CSchedTime schedTime;
		CString strUpdateLine(ctstrSCAN + _T(" ") + m_strUserName + _T(" ") +
				schedTime.GetCurrSchedTime() + _T(" ") + m_strScan);
		m_txtNewSched.AddUpdateDir(strUpdateLine);

		// Delete all of the sector data that may have
		// been previously used; Index 0 is not used
		for( int nColumn=0; nColumn < eMaxSectorColumns; nColumn++ )
			m_strSectorData[nColumn].RemoveAll();

		if( !ReadSectorFile() )
		{
			m_txtNewSched.Close(CTextFile::NOWRITE, &m_fileException);
			m_txtScan.Close(CTextFile::NOWRITE, &m_fileException);
			DeleteFile(m_strNewSched);
			return FALSE;
		}

		// Insert a description record into the report file prolog
		// stating what sector file was used. Insert an update record
		// into the new command schedule stating what sector file was
		// used. Read the data in from the scan file.
		uncScanfile = m_strSector;
		strText		= _T("Using sector file ") + uncScanfile.File();
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::INFO);
		strUpdateLine = _T("SECTOR ") + m_strUserName + _T(" ") +
				schedTime.GetCurrSchedTime() + _T(" ") + m_strSector;
		m_txtNewSched.AddUpdateDir(strUpdateLine);
	}
	else
	{
		// Open the existing file up as readonly
		if( !m_txtScan.Load(m_strScan, CTextFile::RO, &m_fileException) )
		{
			// If we make it here then we could not open the scan
			// file Output an error message to the report file
			// Close and Delete the new command schedule Close
			strText = _T("Error opening file ") + m_strScan;
			GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
			m_txtNewSched.Close(CTextFile::NOWRITE, &m_fileException);
			DeleteFile(m_strNewSched);
			return FALSE;
		}

		// Insert an update record into the new command schedule
		// stating what frame file was used.
		// Read the data in from the scan file - if there are errors,
		// return false
		// strText = _T("Using frame file ") + m_strScan;
		CSchedTime schedTime;
		CString strUpdateLine(ctstrSCAN + _T(" ") + m_strUserName + _T(" ") +
				schedTime.GetCurrSchedTime() + _T(" ") + m_strScan);
		m_txtNewSched.AddUpdateDir(strUpdateLine);

		if( !ReadScanFile() )
		{
			m_txtNewSched.Close(CTextFile::NOWRITE, &m_fileException);
			m_txtScan.Close(CTextFile::NOWRITE, &m_fileException);
			DeleteFile(m_strNewSched);
			return FALSE;
		}
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::ReadScanFile
//	Description :	ReadScanFile will read all of the data contained
//					in a scan file into global memory. Any error will
//					be written to the status window.
//
//	Return :		BOOL	-	TRUE if file was successfully read.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::ReadScanFile()
{
	int		nNumFrames;
	CString strText;
	CString strScanLine;
	CString strVersion;
	CString strType;
	CString strNumFrames;
	CString strSRSOStart;
	CString strModTime;
	CString strModUser;
	CString strUpdateTime;
	CString strUpdateUser;

	// Delete all of the scan data that may have been previously used
	for( int nColumn=0; nColumn < eMaxFrameColumns; nColumn++ )
		m_strScanData[nColumn].RemoveAll(); //Index 0 is not used

	// Read first line and strip off its components
	strScanLine	= m_txtScan.GetTextAt(0);
	strVersion	= RemoveFirstToken(&strScanLine);
	strType		= RemoveFirstToken(&strScanLine);
	strType.TrimRight();

	// Make sure it contains a version that is supported
	if( strVersion.CompareNoCase(_T("1.0")) != 0 )
	{
		strText = _T("Frame file must be at version 1.0:  ") + strVersion;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Make sure the file is a frame file
	if( strType.CompareNoCase(ctstrFRAME) != 0 )
	{
		strText = _T("Frame file identification record must indicate that ");
		strText+= _T("the file is not a frame file:  ") + strType;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Read in the second line and strip off all of its components
	strScanLine		= m_txtScan.GetTextAt(1);
	strNumFrames	= RemoveFirstToken(&strScanLine);
	strSRSOStart	= RemoveFirstToken(&strScanLine);
	strModTime		= RemoveFirstToken(&strScanLine);
	strModUser		= RemoveFirstToken(&strScanLine);
	strUpdateTime	= RemoveFirstToken(&strScanLine);
	strUpdateUser	= RemoveFirstToken(&strScanLine);
	nNumFrames		= _ttoi(strNumFrames);

	// Make sure that the file contains at least one frame
	if( nNumFrames < 1 )
	{
		strText = _T("Invalid number of frames specified in the frame file:  ") + strNumFrames;
		strText+= _T(". The schedule file was not updated.");
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Make sure that the file has been previously updated (removed by REM 11-30-2001)
	if( !m_bUpdFrm )
	{
		if( strUpdateTime.CompareNoCase(_T("0")) == 0 )
		{
			strText = _T("The frame file specified has not yet been updated by OATS");
			GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
			return FALSE;
		}
	}

	// Make sure that the number of frames specified match the onboard
	// allocation. This is not an error -
	if( nNumFrames != GetAppRegSC()->m_nFrameSize[GetAppRegSite()->m_nDefaultSC] )
	{
		strText.Format(
			_T("The number of frames defined in the frame file (%d),")
			_T("does not match the on board allocation (%d)."),
			nNumFrames, GetAppRegSC()->m_nFrameSize[GetAppRegSite()->m_nDefaultSC]);
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::WARNING);
	}

	// Set the size of the data structures large enough to hold all of the data
	// Determine how many data lines are within the file
	// Read in each line
	// Split up the line into its compontents and save the data into the
	// global structures
	for(int nColumn=0; nColumn < eMaxFrameColumns; nColumn++ )
		m_strScanData[nColumn].SetSize(nNumFrames+1); //Index 0 is not used

	int nNumLines = m_txtScan.GetLineCount() - 2;

	for( int nCurrentLine=0; nCurrentLine<nNumLines; nCurrentLine++ )
	{
				strScanLine	= m_txtScan.GetTextAt(nCurrentLine+2);
		CString strObjID	= RemoveFirstToken(&strScanLine);

		m_strScanData[eFrameIndex].SetAt(nCurrentLine,			strObjID);
		m_strScanData[eFrameTime].SetAt(nCurrentLine,			RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameLabel].SetAt(nCurrentLine,			RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameInstrument].SetAt(nCurrentLine,		RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameSpacelookMode].SetAt(nCurrentLine,	RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameSpacelookSide].SetAt(nCurrentLine,	RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameSndrStepMode].SetAt(nCurrentLine,	RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameCoordType].SetAt(nCurrentLine,		RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStartNS].SetAt(nCurrentLine,		RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStartEW].SetAt(nCurrentLine,		RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStopNS].SetAt(nCurrentLine,			RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStopEW].SetAt(nCurrentLine,			RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStartCycNS].SetAt(nCurrentLine,		RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStartIncrNS].SetAt(nCurrentLine,	RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStartCycEW].SetAt(nCurrentLine,		RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStartIncrEW].SetAt(nCurrentLine,	RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStopCycNS].SetAt(nCurrentLine,		RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStopIncrNS].SetAt(nCurrentLine,		RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStopCycEW].SetAt(nCurrentLine,		RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameStopIncrEW].SetAt(nCurrentLine,		RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameDur].SetAt(nCurrentLine,			RemoveFirstToken(&strScanLine));
		m_strScanData[eFrameDesc].SetAt(nCurrentLine,			RemoveFirstToken(&strScanLine));
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::ReadSectorFile
//	Description :	ReadSectorFile will read all of the data contained
//					in a Sector file into global memory.
//
//	Return :		BOOL	-	TRUE if file was successfully read.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::ReadSectorFile()
{
	int		nNumFrames;
	CString strText;
	CString strSectorLine;
	CString strVersion;
	CString strType;
	CString strNumFrames;

	// Read all if the data into memory
	if( !m_txtSector.Load(m_strSector, CTextFile::RO, &m_fileException) )
	{
		strText = _T("Error reading sector file") + m_strSector;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Read first line and strip off its components
	strSectorLine	= m_txtSector.GetTextAt(0);
	strVersion		= RemoveFirstToken(&strSectorLine);
	strType			= RemoveFirstToken(&strSectorLine);
	strType.TrimRight();

	// Make sure it contains a version that is supported
	if( strVersion.CompareNoCase(_T("1.0")) != 0 )
	{
		strText = _T("Sector file must be at version 1.0:  ") + strVersion;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Make sure the file is a sector file
	if( strType.CompareNoCase(_T("sector")) != 0 )
	{
		strText = _T("Sector file identification record must indicate ");
		strText+= _T("that the file is not a frame file:  ") + strType;
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Read in the second line and strip off all of its components
	strSectorLine	= m_txtSector.GetTextAt(1);
	strNumFrames	= RemoveFirstToken(&strSectorLine);
	nNumFrames		= _ttoi(strNumFrames);

	// Make sure that the file contains at least one frame
	if( nNumFrames < 1 )
	{
		strText = _T("Invalid number of sectors specified in the ");
		strText+= _T("sector file:  ") + strNumFrames;
		strText+= _T(". The schedule file was not updated.");
		GetAppOutputBar()->OutputToEvents(strText, COutputBar::FATAL);
		return FALSE;
	}

	// Set the size of the data structures large enough to hold all of the data
	// Determine how many data lines are within the file
	// Read in each line
	// Split up the line into its compontents and save the data into the
	// global structures
	for( int nColumn=0; nColumn < eMaxSectorColumns; nColumn++ )
		m_strSectorData[nColumn].SetSize(nNumFrames+1); //Index 0 is not used

	int nNumLines = m_txtSector.GetLineCount() - 2;

	for( int nCurrentLine=0; nCurrentLine<nNumLines; nCurrentLine++ )
	{
		strSectorLine = m_txtSector.GetTextAt(nCurrentLine+2);

		m_strSectorData[eSectorLabel].SetAt(nCurrentLine,			RemoveFirstToken(&strSectorLine));
		m_strSectorData[eSectorInstrument].SetAt(nCurrentLine,		RemoveFirstToken(&strSectorLine));
		m_strSectorData[eSectorSpacelookMode].SetAt(nCurrentLine,	RemoveFirstToken(&strSectorLine));
		m_strSectorData[eSectorSpacelookSide].SetAt(nCurrentLine,	RemoveFirstToken(&strSectorLine));
		m_strSectorData[eSectorSndrStepMode].SetAt(nCurrentLine,	RemoveFirstToken(&strSectorLine));
		m_strSectorData[eSectorCoordType].SetAt(nCurrentLine,		RemoveFirstToken(&strSectorLine));
		m_strSectorData[eSectorStartNS].SetAt(nCurrentLine,			RemoveFirstToken(&strSectorLine));
		m_strSectorData[eSectorStartEW].SetAt(nCurrentLine,			RemoveFirstToken(&strSectorLine));
		m_strSectorData[eSectorStopNS].SetAt(nCurrentLine,			RemoveFirstToken(&strSectorLine));
		m_strSectorData[eSectorStopEW].SetAt(nCurrentLine,			RemoveFirstToken(&strSectorLine));
		m_strSectorData[eSectorDesc].SetAt(nCurrentLine,			RemoveFirstToken(&strSectorLine));
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::RemoveScanBlock
//	Description :	RemoveScanBlock removes all of the scan lines that
//					were automatically inserted by a previous update.
//					It will remove the scandata lines then call
//					RemoveGroundBlock.
//
//	Return :		int		-	The number of lines removed.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CSchedUpdUtil::RemoveScanBlock()
{
	int		nNumRemoved = 0;
	int		nCurPos		= 0;
	int		nTotalLines	= m_txtNewSched.GetLineCount();
	CString strLine;

	// If we are not at the end of the file
	while( nCurPos < nTotalLines )
	{
		// Read in the next line. Check to see if it is a scandata directive
		strLine = m_txtNewSched.GetTextAt(nCurPos);

		if( m_txtNewSched.ScanLine(strLine, ctstrSCANDATA) )
		{
			m_txtNewSched.RemoveAt(nCurPos);
			nNumRemoved++;
			nTotalLines--;

			while( strLine.ReverseFind(_T('\\')) != -1 )
			{
				strLine = m_txtNewSched.GetTextAt(nCurPos);
				m_txtNewSched.RemoveAt(nCurPos);
				nNumRemoved++;
				nTotalLines--;
			}
		}
		else
			nCurPos++;
	}

	nNumRemoved = nNumRemoved + RemoveGroundBlock(ctstrSCAN);

	return nNumRemoved;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::FindScanObj
//	Description :	FindScanObj will return with the index that
//					matches the label and instrument passed in. It
//					will search either the scan table or sector table.
//
//	Return :		int		-	The index of the scan object.
//
//	Parameters :
//			const CString*	pstrLabel	-	pointer to the frame label
//			const CString*	pstrInst	-	pointer to the inst label
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CSchedUpdUtil::FindScanObj(const CString* pstrLabel, const CString* pstrInst)
{
	BOOL	bContinue	= TRUE;
	BOOL	bFound		= FALSE;
	int		nObjID		= -1;
	int		nCurObjID	= 0;
	int		nMaxObjID;

	// If we are creating a new scan file, then search through the sector file
	// Otherwise search through the scan file
	nMaxObjID = m_strSectorData[0].GetSize();

	while( bContinue )
	{
		if( nCurObjID != nMaxObjID )
		{
			if( (m_strSectorData[eSectorLabel].GetAt(nCurObjID).CompareNoCase(*pstrLabel) == 0) &&
				(m_strSectorData[eSectorInstrument].GetAt(nCurObjID).CompareNoCase(*pstrInst) == 0) )
			{
				bContinue	= FALSE;
				bFound		= TRUE;
				nObjID		= nCurObjID;
			}
			else
				nCurObjID++;
		}
		else
			bContinue = FALSE;
	}

	return nObjID;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::CreateScanList
//	Description :	CreateScanList creates a star table based on the
//					values returned from OATS.
//
//	Return :		BOOL	-	TRUE if table was successfully created.
//
//	Parameters :
//			CStringArray*	pScanList	-	pointer to the in memory
//											frame values obtained
//											from OATS
//			MSG_151* 		pMsg155		-	pointer to the 155 message
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::CreateScanList(CStringArray* pScanList, CPtrList* pList)
{
	MSG_55	msg55;
	CString strMsg;
	int		nStepMode;

	MSG_155* pMsg155 = NULL;

	POSITION pos = pList->GetHeadPosition();

	while( pos != NULL )
	{
		pMsg155 = (MSG_155*)pList->GetNext(pos);

		if( !m_CommUtil.SendMsg(pMsg155, 55, &msg55) )
		{			
			CString strErr;

			strErr = GetLastError()==ERROR_SUCCESS?_T(""):GetLastErrorText();
			
			if( strErr.IsEmpty() )
				strErr = _T("The Schedule file was not updated.");
			else
				strErr+= _T(" The Schedule file was not updated.");
			
			strMsg.Format(IDS_UPDATE_WRITE_ERROR, strErr);				
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		for( int nCurScan=0; nCurScan < msg55.m_55Hdr.m_nNumFrames; nCurScan++ )
		{
			CString strTime;
			ConvertBCDToStringTime(msg55.m_55Data[nCurScan].m_nBCDTime, &strTime);
			CString strLine;
			CString strTemp;

			if( !OutputStatus(strTime,
				strInstrument[msg55.m_hdr.m_nMsgSubType],
				strSpacelookSide[msg55.m_55Data[nCurScan].m_nSpacelookSide],
				msg55.m_55Data[nCurScan].m_nOutputStatus) )
				return FALSE;

			nStepMode = msg55.m_55Data[nCurScan].m_nSounderStepMode;

			if( nStepMode == -1 )
			{
				if( !CriticalLimit(strTime,
						strInstrument[msg55.m_hdr.m_nMsgSubType],
						strSpacelookSide[msg55.m_55Data[nCurScan].m_nSpacelookSide],
						msg55.m_55Data[nCurScan].m_nCriticalLimit) )
					return FALSE;

				nStepMode = 4;
			}
			else if( msg55.m_hdr.m_nMsgSubType == 1 )
			{
				if( !CriticalLimit(strTime,
						strInstrument[msg55.m_hdr.m_nMsgSubType],
						strSpacelookSide[msg55.m_55Data[nCurScan].m_nSpacelookSide],
						msg55.m_55Data[nCurScan].m_nCriticalLimit) )
					return FALSE;
			}

			strLine.Format(_T("%s, %f, %f, %s, %s, %s, %d, %s, %d, "), strTime,
					msg55.m_55Data[nCurScan].m_fDuration,
					msg55.m_55Data[nCurScan].m_fFrequency,
					strCoordType[msg55.m_55Data[nCurScan].m_nCoordinateType],
					strSpacelookSide[msg55.m_55Data[nCurScan].m_nSpacelookSide],
					strSpacelookMode[msg55.m_55Data[nCurScan].m_nSpacelookMode],
					msg55.m_55Data[nCurScan].m_nDefaultFrameFlag,
					strSndrStepMode[nStepMode],
					msg55.m_55Data[nCurScan].m_nOutputStatus);

			strTemp.Format(_T("%d, %d, %d, %d, %d, %d, %d, %d, %d, %s"),
					msg55.m_55Data[nCurScan].m_nCriticalLimit,
					msg55.m_55Data[nCurScan].m_nStartEWCyc,
					msg55.m_55Data[nCurScan].m_nStartEWInc,
					msg55.m_55Data[nCurScan].m_nStartNSCyc,
					msg55.m_55Data[nCurScan].m_nStartNSInc,
					msg55.m_55Data[nCurScan].m_nStopEWCyc,
					msg55.m_55Data[nCurScan].m_nStopEWInc,
					msg55.m_55Data[nCurScan].m_nStopNSCyc,
					msg55.m_55Data[nCurScan].m_nStopNSInc,
					strInstrument[msg55.m_hdr.m_nMsgSubType]);

			strLine += strTemp;

			pScanList->Add(strLine);
		}
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::ProcessScanInfo
//	Description :	ProcessScanInfo processes frame request then
//					builds the frame table.
//
//	Return :		BOOL	-	TRUE if request was successfully
//								processed.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::ProcessScanInfo()
{
	CStringArray	scanList1;
	CStringArray	scanList2;
	CPtrList		msgList;
	CString			strMsg;
	COXUNC			unc(m_strOldSched);

	strMsg.Format(_T("Updating the Schedule File %s for Frames"), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	if( m_ScanImgList.GetSize() == 0 && m_ScanSndList.GetSize() == 0 )
	{
		strMsg.Format(IDS_UPDATE_NO_DATA_ERROR,ctstrINSTRUMENT,ctstrFRAME);
		strMsg += _T(" The schedule file was not updated.");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		DeleteListElements(&msgList);
		return FALSE;
	}

	if( m_ScanImgList.GetSize() > 0 )
	{
		if( !BuildScanMessage(m_ScanImgList,msgList,1,m_bImgIMCEnabled) )
		{
			DeleteListElements(&msgList);
			return FALSE;
		}
	}

	if( m_ScanSndList.GetSize() > 0 )
	{
		if( !BuildScanMessage(m_ScanSndList,msgList,2,m_bSndIMCEnabled) )
		{
			DeleteListElements(&msgList);
			return FALSE;
		}
	}

	if( !CreateScanList(&scanList1,&msgList) )
	{
		DeleteListElements(&msgList);
		return FALSE;
	}

	DeleteListElements(&msgList);

	// If we are creating a new file or one based upon
	// the sched name create it here
	if( m_bScanNew )
	{
		for( int n=0; n < scanList1.GetSize(); n++){
			scanList2.Add(CString(scanList1.GetAt(n)));
		}

		if( !CreateScanFile(&scanList2)){
			return FALSE;
		}

		if( !ReadScanFile()){
			return FALSE;
		}

		if( !UpdateScheduleScans(&scanList1)){
			return FALSE;
		}
	} else {
		UpdateScansWithFrmTable(&scanList1);
	}

	strMsg.Format(_T("Schedule file %s has been updated for Frames successfully"), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::BuildScanMessage
//	Description :	BuildScanMessage builds a 151 message for OATS
//					processing.
//
//	Return :		BOOL	-	TRUE if message was built successfully
//
//	Parameters :
//			CStringArray&	starInstrList	-	list of frame directives
//												for specified instrument
//			int*			nInstrument		-	instrument value
//			BOOL			bIMCEnabled		-	is IMC set inabled
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::BuildScanMessage(
	CStringArray&	scanInstrList,
	CPtrList&		list,
	int				nInstrument,
	BOOL			bIMCEnabled)
{
	int				nMessage= 0;
	MSG_155*		pMsg155 = new MSG_155;

	// index i = Scan Instrument Count
	for( int i=0; i<scanInstrList.GetSize(); i++ )
	{
		if( nMessage < MAX_55_DATA )
		{
			CStringArray strData;
			CString strTime(RemoveFirstToken(&scanInstrList[i]));
			CString strLabel(RemoveFirstToken(&scanInstrList[i]));
			CString strRepeat(RemoveFirstToken(&scanInstrList[i]));
			CString strCP(RemoveFirstToken(&scanInstrList[i]));
			CString strSide(RemoveFirstToken(&scanInstrList[i]));
			CString strInst(RemoveFirstToken(&scanInstrList[i]));
			CString strPriority(RemoveFirstToken(&scanInstrList[i]));
			CString strMode(RemoveFirstToken(&scanInstrList[i]));
			CString strBbcal;

			if( nInstrument == 2 )
				strBbcal = RemoveFirstToken(&scanInstrList[i]);

			CString strExecute(RemoveFirstToken(&scanInstrList[i]));

			int nScanInstrIndex;

			if( m_bScanNew )
			{
				if( ((m_bDoSRSO && !m_bDoSRSOPreDef && (strLabel != m_strSRSOFrame)) && m_bDoStaticFrm) ||
					((m_bDoSRSO && m_bDoSRSOPreDef) && m_bDoStaticFrm)	||
					((m_bDoSRSO && m_bDoSRSOPreDef) && !m_bDoStaticFrm)	||
					(!m_bDoSRSO && m_bDoStaticFrm) )
				{
					if( (nScanInstrIndex = LookupScanInSectorTable(&strLabel,&strSide,&strInst)) == -1 )
						return FALSE;

					SectorStrData(&strData,strTime,strLabel,strInst,nScanInstrIndex);
				}
				else
					SrsoStrData(&strData,strTime,strLabel,strInst,strSide);
			}
			else
			{
				/**** Only Schedules that have been previously updated can be updated****/
				/**** using existing frame tables****/
				if( strSide.CompareNoCase(strSpacelookSide[0]) == 0 )
				{
					CString strErr;
					strErr.LoadString(IDS_UPDATE_SCAN_ENTRY_ERROR);
					GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
					return FALSE;
			}

			if( (nScanInstrIndex = LookupScanInFrameTable(&strLabel,&strSide,&strInst)) == -1 )
				return FALSE;

				ScanStrData(&strData,strTime,strLabel,strInst,nScanInstrIndex);
		}

			Set155FromString(pMsg155, &strData,
				GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC],
				_ttoi(strRepeat),
				bIMCEnabled, &m_strIMCSet, nInstrument, nMessage);

			nMessage++;
		}
		else
		{
			list.AddTail(pMsg155);
			nMessage= 0;
			i--;
			pMsg155	= new MSG_155;
		}
	}

	list.AddTail(pMsg155);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::CreateScanFile
//	Description :	CreateScanFile creates a frame file.
//
//	Return :		BOOL	-	TRUE if frame file successfully created.
//
//	Parameters :
//		const CStringArray*	pScanList	-	pointer to a list of
//											frame directives.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::CreateScanFile(const CStringArray* pScanList)
{
	CStringArray	srsoArray;
	CStringArray	scanArray;
	CStringList		strList;
	CString			strLastTime;
	CString			strMsg;
	int				objId = 1;
	int				nIndex= 0;
	int				nNewIx= 0;

	strMsg = _T("Creating Frame Table...");
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	while( nIndex < m_txtNewSched.GetLineCount() )
	{
		CString strLine = m_txtNewSched.GetDirLine(nIndex,nNewIx);
		strLine.MakeUpper();
		strLine.TrimLeft();

		nIndex = nIndex + nNewIx;
		nNewIx = 0;

		if( (!strLine.IsEmpty()) && strLine[0] != ctchCOMMENT ) //NEW CHECK BEFORE USE
		{
			if( m_txtNewSched.ScanLine(strLine, ctstrSCAN) )
			{
				CString strInst		= ExtractValue(strLine, _T("INSTRUMENT="));
				CString strLabel	= ExtractValue(strLine, _T("FRAME="));
				CString strSide		= ExtractValue(strLine, _T("SIDE="));
				CString strRepeat	= ExtractValue(strLine, _T("REPEAT="));
				CString strCP		= ExtractValue(strLine, _T("CP="));
				int		nScanIndex	= LookupScanInData(&strLastTime,&strInst,pScanList);

				if( nScanIndex != -1 )
				{
					CString strUpdateLine		= pScanList->GetAt(nScanIndex);
					CString strTime				= RemoveFirstToken(&strUpdateLine);
					CString strDuration			= RemoveFirstToken(&strUpdateLine);
					CString strFrequency		= RemoveFirstToken(&strUpdateLine);
					CString strCoordType		= RemoveFirstToken(&strUpdateLine);
					CString strFramelookSide	= RemoveFirstToken(&strUpdateLine);
					CString strSpacelookMode	= RemoveFirstToken(&strUpdateLine);
					CString strDefaultFrameFlag	= RemoveFirstToken(&strUpdateLine);
					CString strSndrStepMode		= RemoveFirstToken(&strUpdateLine);
					CString strOutputStatus		= RemoveFirstToken(&strUpdateLine);
					CString strCriticalLimit	= RemoveFirstToken(&strUpdateLine);
					CString strStartEWCyc		= RemoveFirstToken(&strUpdateLine);
					CString strStartEWInc		= RemoveFirstToken(&strUpdateLine);
					CString strStartNSCyc		= RemoveFirstToken(&strUpdateLine);
					CString strStartNSInc		= RemoveFirstToken(&strUpdateLine);
					CString strStopEWCyc		= RemoveFirstToken(&strUpdateLine);
					CString strStopEWInc		= RemoveFirstToken(&strUpdateLine);
					CString strStopNSCyc		= RemoveFirstToken(&strUpdateLine);
					CString strStopNSInc		= RemoveFirstToken(&strUpdateLine);

					if( m_bDoSRSO && m_bDoSRSOPreDef && strLabel == m_strSRSOFrame )
						strLabel = m_strSRSOLabel;

					int nScanObjId=-1;

					if( ((m_bDoSRSO && !m_bDoSRSOPreDef && (strLabel != m_strSRSOFrame)) && m_bDoStaticFrm) ||
						((m_bDoSRSO && m_bDoSRSOPreDef) && m_bDoStaticFrm)	||
						((m_bDoSRSO && m_bDoSRSOPreDef) && !m_bDoStaticFrm)	||
						(!m_bDoSRSO && m_bDoStaticFrm) )
					{
						nScanObjId	= LookupScanInSectorTable(&strLabel,&strFramelookSide,&strInst);

						if( nScanObjId == -1 )
							return FALSE;
					}

					POSITION	pos			= strList.Find(strLabel+_T(",")+strFramelookSide+_T(",")+strInst);
					BOOL		bIMCEnabled	= (strInst.CompareNoCase(ctstrIMGR) == 0 ? m_bImgIMCEnabled : m_bSndIMCEnabled);

					if( (pos != NULL && !bIMCEnabled) || (pos == NULL && bIMCEnabled) || pos == NULL && !bIMCEnabled)
					{
						CString strLine;

						if( m_bDoSRSO && !m_bDoSRSOPreDef && strLabel == m_strSRSOFrame )
						{
							CString strTemp;

							// Place correct instrument tags in frame file for GenLoad
							strTemp  = (strInst.CompareNoCase(ctstrIMGR) == 0 ? strInstrument[1] : strInstrument[2]); 
							strLine  = _T(", ");
							strLine += strTime				+ _T(", ");
							strLine += strLabel				+ _T(", ");
							strLine += strTemp				+ _T(", ");
							strLine += GetAppRegSC()->m_strSRSOSpacelookMode[GetAppRegSite()->m_nDefaultSC];
							strLine += _T(", ");
							strLine += strFramelookSide		+ _T(", N/A-Imager, ");
							strLine += m_strSRSOCoordType	+ _T(", ");
							strLine += m_strSRSOCoordNS		+ _T(", ");
							strLine += m_strSRSOCoordEW		+ _T(", ");
							strLine += m_strSRSORatio		+ _T(", ");
							strLine += m_strSRSODuration	+ _T(", ");
							strLine += strStartNSCyc		+ _T(", ");
							strLine += strStartNSInc		+ _T(", ");
							strLine += strStartEWCyc		+ _T(", ");
							strLine += strStartEWInc		+ _T(", ");
							strLine += strStopNSCyc			+ _T(", ");
							strLine += strStopNSInc			+ _T(", ");
							strLine += strStopEWCyc			+ _T(", ");
							strLine += strStopEWInc			+ _T(", ");
							strLine += strDuration			+ _T(", ");
							strLine += _T("SRSO ");
							strLine += GetAppRegSC()->m_strSRSOSpacelookMode[GetAppRegSite()->m_nDefaultSC];
							strLine += _T(" Scan");
							srsoArray.Add(strLine);
						}
						else
						{
							strLine.Format(_T("%d, "), objId++);
							strLine += strTime		+ _T(", ");
							strLine += m_strSectorData[eSectorLabel].GetAt(nScanObjId)			+ _T(", ");
							strLine += m_strSectorData[eSectorInstrument].GetAt(nScanObjId)		+ _T(", ");
							strLine += m_strSectorData[eSectorSpacelookMode].GetAt(nScanObjId)	+ _T(", ");
							strLine += m_strSectorData[eSectorSpacelookSide].GetAt(nScanObjId)	+ _T(", ");
							strLine += m_strSectorData[eSectorSndrStepMode].GetAt(nScanObjId)	+ _T(", ");
							strLine += m_strSectorData[eSectorCoordType].GetAt(nScanObjId)		+ _T(", ");
							strLine += m_strSectorData[eSectorStartNS].GetAt(nScanObjId)		+ _T(", ");
							strLine += m_strSectorData[eSectorStartEW].GetAt(nScanObjId)		+ _T(", ");
							strLine += m_strSectorData[eSectorStopNS].GetAt(nScanObjId)			+ _T(", ");
							strLine += m_strSectorData[eSectorStopEW].GetAt(nScanObjId)			+ _T(", ");
							strLine += strStartNSCyc+ _T(", ");
							strLine += strStartNSInc+ _T(", ");
							strLine += strStartEWCyc+ _T(", ");
							strLine += strStartEWInc+ _T(", ");
							strLine += strStopNSCyc + _T(", ");
							strLine += strStopNSInc + _T(", ");
							strLine += strStopEWCyc + _T(", ");
							strLine += strStopEWInc + _T(", ");
							strLine += strDuration  + _T(", ");
							strLine += m_strSectorData[eSectorDesc].GetAt(nScanObjId);

							if( m_bDoSRSO && m_bDoSRSOPreDef && strLabel == m_strSRSOLabel )
								srsoArray.Add(strLine);
							else
								scanArray.Add(strLine);
						}

						strList.AddTail(strLabel+_T(",")+strFramelookSide+_T(",")+strInst);
					}
				}
			}
			else if( m_txtNewSched.ScanLine(strLine, ctstrWAIT) )
				FindTime(&strLine, &strLastTime);
		}
	}

	strList.RemoveAll();

	CString strTmp;
	CString strObj;
	BOOL	bSRSO 		= FALSE;
	int 	nObjId		= 1;
	int 	nSRSOIdx	= GetAppRegSC()->m_nFrameSRSOStart[GetAppRegSite()->m_nDefaultSC];
	int		nTblSize	= scanArray.GetSize() + srsoArray.GetSize();
	int		nScanIdx	= 0;

	for( int i = 0; i<nTblSize; i++ )
	{
		strObj.Format(_T("%d"), nObjId);

		if( (i < (nSRSOIdx-1) || bSRSO || (srsoArray.GetSize() == 0)) && (nScanIdx < scanArray.GetSize()) )
		{
			strTmp = scanArray.GetAt(nScanIdx++);
			nIndex = strTmp.Find(_T(","), 0);
			strTmp.Delete(0, nIndex);
			strTmp.Insert(0, strObj);
			m_txtScan.AppendText(strTmp);
			nObjId++;
		}
		else
		{
			bSRSO = TRUE;
			nObjId= nSRSOIdx;

			for( int j = 0; j<srsoArray.GetSize(); j++, i++ )
			{
				strObj.Format(_T("%d"), nObjId++);
				strTmp = srsoArray.GetAt(j);
				nIndex = strTmp.Find(_T(","), 0);
				strTmp.Delete(0, nIndex);
				strTmp.Insert(0, strObj);
				m_txtScan.AppendText(strTmp);
			}
		}		
	}

	// Write the "in memory" copy of the scan table out to the scan
	// file buffer. Write the scan file buffer out to disk and clear
	// out the buffer

	CTime t = CTime::GetCurrentTime();
	time_t osBinaryTime = t.GetTime();  // time_t defined in <time.h>

	m_txtScan.InsertAt(0, _T("1.0, Frame"));
	CString	strUpdateText;
	strUpdateText.Format(_T("%d, %d, %ld, %s, %ld, %s"),
		m_txtScan.GetLineCount()-1,
		GetAppRegSC()->m_nFrameSize[GetAppRegSite()->m_nDefaultSC],
		(long)osBinaryTime, m_strUserName,
		(long)osBinaryTime, m_strUserName);
	m_txtScan.InsertAt(1, strUpdateText);

	if( !m_txtScan.Close(CTextFile::WRITE, &m_fileException) )
	{
		strMsg = m_txtScan.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	m_txtScan.Clear();
	m_txtScan.Load(m_strScan, CTextFile::RO, &m_fileException);

	strMsg = _T("Frame Table created successfully.");
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::UpdateScheduleScans
//	Description :	UpdateScheduleScans updates a schedule for frames.
//
//	Return :		BOOL	-	TRUE if schedule was updated successfully
//
//	Parameters :
//		const CStringArray*	pScanList	-	pointer to a list of
//											frame directives.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::UpdateScheduleScans(const CStringArray*	pScanList)
{
	CString strLastTime;
	CString	strMsg;
//	MSG_153	msg153;
	MSG_53	msg53;

	BOOL	bFrameIOMiniSched	= GetAppRegSC()->m_strFrameIOMiniSched[GetAppRegSite()->m_nDefaultSC].IsEmpty();
	BOOL	bFrameMiniSched		= GetAppRegSC()->m_strFrameMiniSched[GetAppRegSite()->m_nDefaultSC].IsEmpty();

	if( bFrameMiniSched && bFrameIOMiniSched )
	{
		strMsg = _T("No Frame Mini Schedule name! \"See Workspace options...\" The schedule file was not updated.");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	if( !bFrameMiniSched && !bFrameIOMiniSched )
	{
		strMsg = _T("Only one Frame Mini Schedule required for ground execution!!!");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
		strMsg = _T("Remove Frame or FrameIO mini schedule from ground blocks before executing Schedule from the ground.");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
	}

	/*
	msg153.m_hdr.m_nSequence		= 0;
	msg153.m_hdr.m_nSatelliteID		= (unsigned __int8)GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC];
	msg153.m_hdr.m_nSource			= 10;
	msg153.m_hdr.m_nDestination		= 32;
	msg153.m_hdr.m_nNumHalfWords	= MSG_153_DATA_HW_SIZE;
	msg153.m_hdr.m_nMsgType			= 153;
	msg153.m_hdr.m_nMsgSubType		= 1;
	strncpy (msg153.m_hdr.m_cTracer, "         ", 10);
	msg153.m_hdr.m_cTracer[9] = ' ';
	msg153.m_hdr.m_nMsgCnt			= 1;
	msg153.m_hdr.m_nMsgEndFlag		= 0xFF;
	msg153.m_hdr.m_nSpare			= 0;
	msg153.m_hdr.m_nRoutingID		= 0;
	msg153.m_153Data.m_cIMCSet[0]	= (char)m_strIMCSet.GetAt(0);
	msg153.m_153Data.m_cIMCSet[1]	= (char)m_strIMCSet.GetAt(1);
	msg153.m_153Data.m_cIMCSet[2]	= (char)m_strIMCSet.GetAt(2);
	msg153.m_153Data.m_cIMCSet[3]	= (char)m_strIMCSet.GetAt(3);

	if( !m_CommUtil.SendMsg(&msg153, 53, &msg53) )
	{
		CString strErr;
		
		strErr = GetLastError()==ERROR_SUCCESS?_T(""):GetLastErrorText();
			
		if( strErr.IsEmpty() )
			strErr = _T("The Schedule file was not updated.");
		else
			strErr+= _T(" The Schedule file was not updated.");
			
		strMsg.Format(IDS_UPDATE_WRITE_ERROR, strErr);				
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}
	*/

	if( !SpacecraftOrientation(&msg53) )
		return FALSE;

	strMsg = _T("Updating Schedule Frames...");
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	int	nIndex = 0;
	int	nNewIx = 0;
	int nRepIx = 0;

	while( nIndex < m_txtNewSched.GetLineCount() )
	{
		CString strLine = m_txtNewSched.GetDirLine(nIndex,nNewIx);
		strLine.TrimLeft();

		nIndex = nIndex + nNewIx;
		nRepIx = nNewIx;
		nNewIx = 0;

		if( (!strLine.IsEmpty()) && strLine[0] != ctchCOMMENT ) //NEW CHECK BEFORE USE
		{
			if( m_txtNewSched.ScanLine(strLine, ctstrSCAN) )
			{
				CString strInsertLine;
				CString strInst		= ExtractValue(strLine, _T("INSTRUMENT="));
				CString strLabel	= ExtractValue(strLine, _T("FRAME="));
				CString strSide		= ExtractValue(strLine, _T("SIDE="));
				CString strRepeat	= ExtractValue(strLine, _T("REPEAT="));
				CString strPriority = ExtractValue(strLine, _T("PRIORITY="));
				CString strMode		= ExtractValue(strLine, _T("MODE="));
				CString strBbcal;

				if( strInst == ctstrSDR )
				{
					strBbcal = ExtractValue (strLine, _T("BBCAL="));

					if( strBbcal.IsEmpty() )
						strBbcal = ctstrDO_NOT_SUPP;
				}

				CString strExecute = ExtractValue (strLine, _T("EXECUTE="));

				if( strExecute.IsEmpty()){
					strExecute = ctstrLOAD_AND_EXEC;
				}

				if( strRepeat.IsEmpty()){
					strRepeat = _T("1");
				}

				int nScanIndex = LookupScanInData(&strLastTime,&strInst,pScanList);

				if( nScanIndex != -1 )
				{
					CString strUpdateLine		= pScanList->GetAt(nScanIndex);
					CString strTime				= RemoveFirstToken(&strUpdateLine);
					CString strDuration			= RemoveFirstToken(&strUpdateLine);
					CString strFrequency		= RemoveFirstToken(&strUpdateLine);
					CString strCoordType		= RemoveFirstToken(&strUpdateLine);
					CString strFramelookSide	= RemoveFirstToken(&strUpdateLine);
					CString strSpacelookMode	= RemoveFirstToken(&strUpdateLine);
					CString strDefaultFrameFlag	= RemoveFirstToken(&strUpdateLine);
					CString strSndrStepMode		= RemoveFirstToken(&strUpdateLine);
					CString strOutputStatus		= RemoveFirstToken(&strUpdateLine);
					CString strCriticalLimit	= RemoveFirstToken(&strUpdateLine);
					CString strStartEWCyc		= RemoveFirstToken(&strUpdateLine);
					CString strStartEWInc		= RemoveFirstToken(&strUpdateLine);
					CString strStartNSCyc		= RemoveFirstToken(&strUpdateLine);
					CString strStartNSInc		= RemoveFirstToken(&strUpdateLine);
					CString strStopEWCyc		= RemoveFirstToken(&strUpdateLine);
					CString strStopEWInc		= RemoveFirstToken(&strUpdateLine);
					CString strStopNSCyc		= RemoveFirstToken(&strUpdateLine);
					CString strStopNSInc		= RemoveFirstToken(&strUpdateLine);
					CString strLabelSide;
					CString strSLSCMD;
					CString strOATSside(strSpacelookSide[0]);
					int		nScanObjId;

					if( m_bDoSRSO && m_bDoSRSOPreDef && strLabel == m_strSRSOFrame )
						strLabel = m_strSRSOLabel;

					// Replace all OATS selected sides in the updated schedule
					// with space look side retreived from OATS
					if( strSide.CompareNoCase(strOATSside) == 0 )
					{
						CString strTxt;
						CString strSLS(strFramelookSide);
						int		i = -1;

						strSLS.MakeUpper();

						while( i < nRepIx )
						{
							int j = 0;
							strTxt= m_txtNewSched.GetTextAt(nIndex-(nRepIx-(i+1)));

							if( (j= strTxt.Find(strSide)) == -1 )
								i++;
							else
							{
								strTxt.Replace(strSide, strSLS);
								m_txtNewSched.SetAt(nIndex-(nRepIx-(i+1)), strTxt);
								i = nRepIx;
							}
						}
					}

					if( (m_bDoSRSO && m_bDoSRSOPreDef	&& strLabel == m_strSRSOLabel) ||
						(m_bDoSRSO && !m_bDoSRSOPreDef	&& strLabel == m_strSRSOFrame) )
						strLabelSide	= strLabel;
					else
						strLabelSide	= (strSide.CompareNoCase(strOATSside) != 0 ?
										strLabel + strSide : strLabel + strFramelookSide);

					BOOL bIMCEnabled= (strInst.CompareNoCase(ctstrIMGR) == 0 ? m_bImgIMCEnabled : m_bSndIMCEnabled);

					if( !bIMCEnabled )
						nScanObjId	= LookupScanInFrameTable(&strLabel,&strFramelookSide,&strInst,&strTime);
					else
						nScanObjId	= LookupScanInFrameTable(&strLabel,&strFramelookSide,&strInst);

					if( nScanObjId == -1 )
						return FALSE;

					/*
					switch( msg53.m_53Data.m_nYawFlipFlag )
					{
						case -1:
							if( strFramelookSide.CompareNoCase(ctstrEAST) == 0 )
							{
								strSLSCMD		= ctstrMINUS_X;
								strFramelookSide= ctstrWEST;
							}
							else
							{
								strSLSCMD		= ctstrPLUS_X;
								strFramelookSide= ctstrEAST;
							}
							break;

						case  1:
							strSLSCMD= (strFramelookSide.CompareNoCase(ctstrEAST) == 0 ? ctstrPLUS_X : ctstrMINUS_X);
							break;

						default:
						{
							strMsg.Format(IDS_UPDATE_STARS_FRAMES,_T("yaw flip flag"));
							GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
							return FALSE;
						}
					}
					*/

					strSLSCMD = SpacelookRequest(msg53.m_53Data.m_nYawFlipFlag, strFramelookSide.CompareNoCase(ctstrEAST));

					CString strTmp1;

					ConvertDurationToMSm(&strDuration);
					strInsertLine.Format(
						_T("    SCANDATA FRAME=%s REPEAT=%s SIDE=%s INSTRUMENT=%s OBJID=%d DURATION=\"%s\" \\\n"),
						strLabelSide, strRepeat, strSLSCMD, strInst, nScanObjId, strDuration);

					if( strInst == ctstrSDR )
						strTmp1.Format(_T("        PRIORITY=%s MODE=%s BBCAL=%s EXECUTE=%s"),
							strPriority, strMode, strBbcal, strExecute);
					else
						strTmp1.Format(_T("        PRIORITY=%s MODE=%s EXECUTE=%s"),
							strPriority, strMode, strExecute);

					strInsertLine += strTmp1;
					strInst.MakeUpper();
					strMode.MakeUpper();
					strPriority.MakeUpper();
					strFramelookSide.MakeUpper();

					m_txtNewSched.InsertAt(nIndex++,strInsertLine);
					strInsertLine.Empty();
					m_txtNewSched.InsertAt(nIndex++,_T("##ground begin scan"));

					if( !bFrameIOMiniSched )
					{
						strInsertLine.Format(_T("START ./%s/procs/%s(%d,\"%s\",\"%s\",\"%s\",\"%s\",%s,\"%s\",\"%s\",\"%s\")"),
							GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],
							GetAppRegSC()->m_strFrameIOMiniSched[GetAppRegSite()->m_nDefaultSC], /* InstObj Mini Schedule */
							nScanObjId, strPriority, strMode, strInst, strExecute, strRepeat, strBbcal, strSLSCMD,
							GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC]);
						m_txtNewSched.InsertAt(nIndex++,strInsertLine);
						strInsertLine.Empty();
					}

					if( !bFrameMiniSched )
					{
						strInsertLine.Format(_T("START ./%s/procs/%s(\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",%s,%s,%s,%s,%s,%s,%s,%s,%s)"),
							GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],
							GetAppRegSC()->m_strFrameMiniSched[GetAppRegSite()->m_nDefaultSC],
							strInst, strFramelookSide, strBbcal, strMode, strPriority, strRepeat,
							strStartEWCyc, strStartEWInc, strStopEWCyc, strStopEWInc,
							strStartNSCyc, strStartNSInc, strStopNSCyc, strStopNSInc);
						m_txtNewSched.InsertAt(nIndex++,strInsertLine);
						strInsertLine.Empty();
					}

					m_txtNewSched.InsertAt(nIndex++,_T("##ground end scan"));
				}
			}
			else if( m_txtNewSched.ScanLine(strLine, ctstrWAIT) )
				FindTime(&strLine, &strLastTime);
		}
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::UpdateScansWithFrmTable
//	Description :	UpdateScheduleScans updates a schedule for frames.
//
//	Return :		BOOL	-	TRUE if schedule was updated successfully
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::UpdateScansWithFrmTable(const CStringArray*	pScanList)
{
	CString strLastTime;
	CString strLabelSide;
	CString strSLSCMD;
	CString	strMsg;
	COXUNC	unc(m_strOldSched);
	MSG_53	msg53;

	BOOL	bFrameIOMiniSched	= GetAppRegSC()->m_strFrameIOMiniSched[GetAppRegSite()->m_nDefaultSC].IsEmpty();
	BOOL	bFrameMiniSched		= GetAppRegSC()->m_strFrameMiniSched[GetAppRegSite()->m_nDefaultSC].IsEmpty();

	if( bFrameMiniSched && bFrameIOMiniSched )
	{
		strMsg = _T("No Frame Mini Schedule name! \"See Workspace options...\" The schedule file was not updated.");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	if( !bFrameMiniSched && !bFrameIOMiniSched )
	{
		strMsg = _T("Only one Frame Mini Schedule required for ground execution!!!");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
		strMsg = _T("Remove Frame or FrameIO mini schedule from ground blocks before executing Schedule from the ground.");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
	}

	if( !SpacecraftOrientation(&msg53) )
		return FALSE;

	strMsg = _T("Updating Schedule Frames...");
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	int	nIndex = 0;
	int	nNewIx = 0;
	int nRepIx = 0;

	while( nIndex < m_txtNewSched.GetLineCount() )
	{
		CString strLine = m_txtNewSched.GetDirLine(nIndex,nNewIx);
		strLine.TrimLeft();

		nIndex = nIndex + nNewIx;
		nRepIx = nNewIx;
		nNewIx = 0;

		if( (!strLine.IsEmpty()) && strLine[0] != ctchCOMMENT ) //NEW CHECK BEFORE USE
		{
			if( m_txtNewSched.ScanLine(strLine, ctstrSCAN) )
			{
				CString strInsertLine;
				CString strInst		= ExtractValue(strLine, _T("INSTRUMENT="));
				CString strLabel	= ExtractValue(strLine, _T("FRAME="));
				CString strSide		= ExtractValue(strLine, _T("SIDE="));
				CString strRepeat	= ExtractValue(strLine, _T("REPEAT="));
				CString strPriority = ExtractValue(strLine, _T("PRIORITY="));
				CString strMode		= ExtractValue(strLine, _T("MODE="));
				CString strBbcal;

				if( strInst == ctstrSDR )
				{
					strBbcal = ExtractValue (strLine, _T("BBCAL="));

					if( strBbcal.IsEmpty() )
						strBbcal = ctstrSUPP;
				}

				CString strExecute = ExtractValue (strLine, _T("EXECUTE="));

/**/			if( m_bDoStaticFrm && strLabel == m_strSRSOFrame && !m_bDoSRSO )
/**/				continue;
				else if (m_bDoRSO && strLabel == m_strRSOCurLabel) //Replace with Special RSO if Frame matches
					strLabel = m_strRSONewLabel;

				if( strSide.CompareNoCase(strSpacelookSide[0]) == 0 )
				{
					strMsg = _T("No OATS selectable sides allowed in schedule when ");
					strMsg+= _T("updating with an existing frame table!");
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					return FALSE;
				}

				if( strExecute.IsEmpty() )
					strExecute = ctstrLOAD_AND_EXEC;

				if( strRepeat.IsEmpty() )
					strRepeat = _T("1");

				strLabelSide = strLabel + strSide;

/**/			int nScanIndex = LookupScanInData(&strLastTime,&strInst,pScanList);

/**/			if( nScanIndex != -1 )
/**/			{
/**/				CString strUpdateLine			= pScanList->GetAt(nScanIndex);
/**/				CString strTimeOATS				= RemoveFirstToken(&strUpdateLine);
/**/				CString strDurationOATS			= RemoveFirstToken(&strUpdateLine);
/**/				CString strFrequencyOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strCoordTypeOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strFramelookSideOATS	= RemoveFirstToken(&strUpdateLine);
/**/				CString strSpacelookModeOATS	= RemoveFirstToken(&strUpdateLine);
/**/				CString strDefaultFrameFlagOATS	= RemoveFirstToken(&strUpdateLine);
/**/				CString strSndrStepModeOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strOutputStatusOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strCriticalLimitOATS	= RemoveFirstToken(&strUpdateLine);
/**/				CString strStartEWCycOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strStartEWIncOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strStartNSCycOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strStartNSIncOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strStopEWCycOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strStopEWIncOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strStopNSCycOATS		= RemoveFirstToken(&strUpdateLine);
/**/				CString strStopNSIncOATS		= RemoveFirstToken(&strUpdateLine);

					// Make sure the cycles and increments match whats comming from OATS...
					// int nScanObjId = LookupScanInFrameTable(&strLabel,&strSide,&strInst);
/**/				int nScanObjId = LookupScanInFrameTable(&strLabel,&strFramelookSideOATS,&strInst,NULL,
/**/									&strStartEWCycOATS,&strStartEWIncOATS,&strStartNSCycOATS,&strStartNSIncOATS,
/**/									&strStopEWCycOATS,&strStopEWIncOATS,&strStopNSCycOATS,&strStopNSIncOATS);

					if( nScanObjId == -1 )
						return FALSE;

					// strSLSCMD = strSide.CompareNoCase(ctstrEAST) == 0 ? ctstrPLUS_X : ctstrMINUS_X;
					// strSLSCMD = SpacelookRequest(msg53.m_53Data.m_nYawFlipFlag, strSide.CompareNoCase(ctstrEAST));
				
					CString strTime				= m_strScanData[eFrameTime].GetAt(nScanObjId-1);
					CString strDuration			= m_strScanData[eFrameDur].GetAt(nScanObjId-1);
					CString strCoordType		= m_strScanData[eFrameCoordType].GetAt(nScanObjId-1);
					CString strFramelookSide	= m_strScanData[eFrameSpacelookSide].GetAt(nScanObjId-1);
					CString strSpacelookMode	= m_strScanData[eFrameSpacelookMode].GetAt(nScanObjId-1);
					CString strSndrStepMode		= m_strScanData[eFrameSndrStepMode].GetAt(nScanObjId-1);
					CString strStartEWCyc		= m_strScanData[eFrameStartCycEW].GetAt(nScanObjId-1);
					CString strStartEWInc		= m_strScanData[eFrameStartIncrEW].GetAt(nScanObjId-1);
					CString strStartNSCyc		= m_strScanData[eFrameStartCycNS].GetAt(nScanObjId-1);
					CString strStartNSInc		= m_strScanData[eFrameStartIncrNS].GetAt(nScanObjId-1);
					CString strStopEWCyc		= m_strScanData[eFrameStopCycEW].GetAt(nScanObjId-1);
					CString strStopEWInc		= m_strScanData[eFrameStopIncrEW].GetAt(nScanObjId-1);
					CString strStopNSCyc		= m_strScanData[eFrameStopCycNS].GetAt(nScanObjId-1);
					CString strStopNSInc		= m_strScanData[eFrameStopIncrNS].GetAt(nScanObjId-1);
					CString strTmp1;

					// strSLSCMD = strSide.CompareNoCase(ctstrEAST) == 0 ? ctstrPLUS_X : ctstrMINUS_X;
					strSLSCMD = SpacelookRequest(msg53.m_53Data.m_nYawFlipFlag, strFramelookSide.CompareNoCase(ctstrEAST));

					ConvertDurationToMSm(&strDuration);
					strInsertLine.Format(
						_T("    SCANDATA FRAME=%s REPEAT=%s SIDE=%s INSTRUMENT=%s OBJID=%d DURATION=\"%s\" \\\n"),
						strLabelSide, strRepeat, strSLSCMD, strInst, nScanObjId, strDuration);

					if( strInst == ctstrSDR )
						strTmp1.Format(_T("        PRIORITY=%s MODE=%s BBCAL=%s EXECUTE=%s"),
							strPriority, strMode, strBbcal, strExecute);
					else
						strTmp1.Format(_T("        PRIORITY=%s MODE=%s EXECUTE=%s"),
							strPriority, strMode, strExecute);

					strInsertLine += strTmp1;
					strInst.MakeUpper();
					strMode.MakeUpper();
					strPriority.MakeUpper();

					m_txtNewSched.InsertAt(nIndex++,strInsertLine);
					strInsertLine.Empty();
					m_txtNewSched.InsertAt(nIndex++,_T("##ground begin scan"));

					if( !bFrameIOMiniSched )
					{
						strInsertLine.Format(_T("START ./%s/procs/%s(%d,\"%s\",\"%s\",\"%s\",\"%s\",%s,\"%s\",\"%s\",\"%s\")"),
							GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],
							GetAppRegSC()->m_strFrameIOMiniSched[GetAppRegSite()->m_nDefaultSC], /* InstObj Mini Schedule */
							nScanObjId, strPriority, strMode, strInst, strExecute, strRepeat, strBbcal, strSLSCMD,
							GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC]);
						m_txtNewSched.InsertAt(nIndex++,strInsertLine);
						strInsertLine.Empty();
					}

					if( !bFrameMiniSched )
					{
						strInsertLine.Format(_T("START ./%s/procs/%s(\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",%s,%s,%s,%s,%s,%s,%s,%s,%s)"),
							GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],
							GetAppRegSC()->m_strFrameMiniSched[GetAppRegSite()->m_nDefaultSC],
							strInst, strSide, strBbcal, strMode, strPriority, strRepeat,
							strStartEWCyc, strStartEWInc, strStopEWCyc, strStopEWInc,
							strStartNSCyc, strStartNSInc, strStopNSCyc, strStopNSInc);
						m_txtNewSched.InsertAt(nIndex++,strInsertLine);
						strInsertLine.Empty();
					}

					m_txtNewSched.InsertAt(nIndex++,_T("##ground end scan"));
				}
			}
			else if( m_txtNewSched.ScanLine(strLine, ctstrWAIT) )
				FindTime(&strLine, &strLastTime);
		}
	}

	// strMsg.Format(_T("Schedule file %s has been updated for Frames successfully"), unc.File());
	// GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::SpacecraftOrientation
//	Description :	Queries OATS for orientation of S/C. 
//
//	Return :		BOOL	-	TRUE if communication with OATS is
//								successfully.
//
//	Parameters :
//					MSG_53* pMsg53 - 50402 message 53
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::SpacecraftOrientation(MSG_53* pMsg53)
{
	CString strMsg;
	MSG_153	msg153;

	GetAppOutputBar()->OutputToEvents(_T("Querying OATS for spacecraft orientation..."), COutputBar::INFO);

	msg153.m_hdr.m_nSequence		= 0;
	msg153.m_hdr.m_nSatelliteID		= (unsigned __int8)GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC];
	msg153.m_hdr.m_nSource			= 10;
	msg153.m_hdr.m_nDestination		= 32;
	msg153.m_hdr.m_nNumHalfWords	= MSG_153_DATA_HW_SIZE;
	msg153.m_hdr.m_nMsgType			= 153;
	msg153.m_hdr.m_nMsgSubType		= 1;
	strncpy_s (msg153.m_hdr.m_cTracer, "         ", 10);
	msg153.m_hdr.m_cTracer[9] = ' ';
	msg153.m_hdr.m_nMsgCnt			= 1;
	msg153.m_hdr.m_nMsgEndFlag		= 0xFF;
	msg153.m_hdr.m_nSpare			= 0;
	msg153.m_hdr.m_nRoutingID		= 0;
	msg153.m_153Data.m_cIMCSet[0]	= (char)m_strIMCSet.GetAt(0);
	msg153.m_153Data.m_cIMCSet[1]	= (char)m_strIMCSet.GetAt(1);
	msg153.m_153Data.m_cIMCSet[2]	= (char)m_strIMCSet.GetAt(2);
	msg153.m_153Data.m_cIMCSet[3]	= (char)m_strIMCSet.GetAt(3);

	if( !m_CommUtil.SendMsg(&msg153, 53, pMsg53) )
	{
		CString strErr;
		
		strErr = GetLastError()==ERROR_SUCCESS?_T(""):GetLastErrorText();
			
		if( strErr.IsEmpty() )
			strErr = _T("The Schedule file was not updated.");
		else
			strErr+= _T(" The Schedule file was not updated.");
			
		strMsg.Format(IDS_UPDATE_WRITE_ERROR, strErr);				
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	CString strOrien;

	if( pMsg53->m_53Data.m_nYawFlipFlag == 1 )
		strOrien = _T("upright");
	else if( pMsg53->m_53Data.m_nYawFlipFlag == -1 )
		strOrien = _T("inverted");

	strMsg.Format(_T("OATS states that spacecraft is in an %s position."), strOrien);

	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::SpacelookRequest
//	Description :	Sets the spacelook side for the frame IO command. 
//
//	Return :		void	-	
//
//	Parameters :
//					int nYawFlip - Orientation of S/C
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CSchedUpdUtil::SpacelookRequest(int nYawFlip, int nSide)
{
	CString strSLSReq;
	CString strMsg;

	switch( nYawFlip )
	{
		case -1:
			strSLSReq = nSide == 0 ? ctstrMINUS_X : ctstrPLUS_X;
			break;

		case  1:
			strSLSReq = nSide == 0 ? ctstrPLUS_X : ctstrMINUS_X;
			break;

		default:
		{
			strMsg.Format(IDS_UPDATE_STARS_FRAMES,_T("yaw flip flag"));
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return _T("");
		}
	}

	return strSLSReq;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::ConvertDurationToMS
//	Description :	ConvertDurationToMS converts duration decimal 
//					string to minutes-seconds string.
//
//	Return :		BOOL	-	TRUE if the frame file was updated
//								successfully.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::ConvertDurationToMSm(CString* strDur)
{
	CString strSec;
	CString strMil;
	TCHAR*	pEndp;
	int		index1	= strDur->ReverseFind('.');
			
			strSec	= strDur->Right(strDur->GetLength()-index1);
	strSec.Insert(0, '0');
	double	s		= _tcstod(strSec, &pEndp) * 60;
	int		index2	= strSec.ReverseFind('.');
			strMil	= strSec.Right(strSec.GetLength()-(index2+1));
			strMil	= strMil.Left(3);
	int		ms		= _ttoi(strMil);
			strSec	= strDur->Left(index1);
	int		m		= _ttoi(strSec);

	if( m < 60 )
		strDur->Format(_T("%.2d:%.2d.%.3d"),m ,(int)s, ms);
	else
	{
		int h = (int)(m / 60);
			m = (int)(m % 60);
		strDur->Format(_T("%.2d:%.2d:%.2d.%.3d"),h ,m, (int)s, ms);
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::UpdateFrameTable
//	Description :	UpdateFrameTable updates a frame file.
//
//	Return :		BOOL	-	TRUE if the frame file was updated
//								successfully.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::UpdateFrameTable()
{
	CStringArray	scanList;
	CPtrList		list;
	CString			strMsg;
//	int				nNumMsgs= 0;
	int				nIndex	= 2;
	int				nNewIx	= 0;
	COXUNC			unc(m_strScan);

	strMsg.Format(_T("Updating the Frame File %s"), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	if( !m_txtScan.Load(m_strScan, CTextFile::RO, &m_fileException) )
	{
		strMsg = _T("Error opening file ") + m_strScan;
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	if( !ReadScanFile())
		return FALSE;

	// Create imager and sounder frame lists
	while( nIndex < m_txtScan.GetLineCount() )
	{
		CString strInst;
		CString strTemp = m_txtScan.GetDirLine(nIndex,nNewIx);
		CString strRowe(strTemp);

		nIndex = nIndex + nNewIx;
		nNewIx = 0;

		for( int nCnt=0; nCnt<4; nCnt++ )
			strInst = RemoveFirstToken(&strTemp);

		if( strInst.CompareNoCase(ctstrIMGR) == 0 )
			m_ScanImgList.Add(strRowe);
		else if( strInst.CompareNoCase(ctstrSDR) == 0 )
			m_ScanSndList.Add(strRowe);
	}

	m_txtScan.Close(CTextFile::NOWRITE, &m_fileException);
	m_txtScan.Clear();

	if( m_ScanImgList.GetSize() == 0 && m_ScanSndList.GetSize() == 0 )
	{
		strMsg.Format(IDS_UPDATE_NO_DATA_ERROR,ctstrINSTRUMENT,ctstrFRAME);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	CString			strLine;
	CStringArray	strData;
	MSG_155*		pMsg155= NULL;
	int				nFrame = 0;

	// Add imager frames to message list
	if( m_ScanImgList.GetSize() > 0 )
	{
		pMsg155	= new MSG_155;

		// Build 155 message for Imager
		for( int i=0; i<m_ScanImgList.GetSize(); i++ )
		{
			if( nFrame < MAX_55_DATA )
			{
				strLine = m_ScanImgList[i];

				for( int nColIndex=0; nColIndex<nMaxInstObjPropsPerObj; nColIndex++ )
					strData.SetAtGrow(nColIndex, RemoveFirstToken(&strLine));

				Set155FromString(
					pMsg155, &strData,
					GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC],
					0, m_bImgIMCEnabled, &m_strIMCSet, 1, nFrame);

				nFrame++;
			}
			else
			{
				list.AddTail(pMsg155);
				nFrame	= 0;
				pMsg155	= new MSG_155;
				i--;
			}
		}

		list.AddTail(pMsg155);
	}

	// Add sounder frames to message list
	if( m_ScanSndList.GetSize() > 0 )
	{
		pMsg155	= new MSG_155;
		nFrame	= 0;

		// Build 155 message for Sounder
		for( int i=0; i<m_ScanSndList.GetSize(); i++ )
		{
			if( nFrame < MAX_55_DATA )
			{
				strLine = m_ScanSndList[i];

				for( int nColIndex=0; nColIndex<nMaxInstObjPropsPerObj; nColIndex++ )
					strData.SetAtGrow(nColIndex, RemoveFirstToken(&strLine));

				Set155FromString(
					pMsg155, &strData,
					GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC],
					0, m_bSndIMCEnabled, &m_strIMCSet, 2, nFrame);

				nFrame++;
			}
			else
			{
				list.AddTail(pMsg155);
				nFrame	= 0;
				i--;
				pMsg155	= new MSG_155;
			}
		}

		list.AddTail(pMsg155);
	}

	// Send frames messages to OATS
	if( !CreateScanList(&scanList,/*&(m_pMsg155)[i]*/&list) )
	{
		DeleteListElements(&list);
		return FALSE;
	}

	DeleteListElements(&list);

	CString strDummy;
	CString strDuration;
	CString strStartEWCyc;
	CString strStartEWInc;
	CString strStartNSCyc;
	CString strStartNSInc;
	CString strStopEWCyc;
	CString strStopEWInc;
	CString strStopNSCyc;
	CString strStopNSInc;

	DeleteFile(m_strScan);

	if( !m_txtScan.Load(m_strScan, CTextFile::CR, &m_fileException) )
	{
		strMsg = _T("Error opening file ") + m_strScan;
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	m_txtScan.AppendText (_T("1.0, Frame"));
	CString	strUpdateText;
	CTime t = CTime::GetCurrentTime();
	time_t osBinaryTime = t.GetTime();  // time_t defined in <time.h>
	strUpdateText.Format(_T("%d, %d, %ld, %s, %ld, Updated"),
		scanList.GetSize(),
		GetAppRegSC()->m_nFrameSize[GetAppRegSite()->m_nDefaultSC],
		osBinaryTime, m_strUserName, osBinaryTime);
	m_txtScan.AppendText(strUpdateText);

	for( int i=0;i<scanList.GetSize();i++ )
	{
		strLine 		= scanList.GetAt(i);
		strDummy		= RemoveFirstToken(&strLine);
		strDuration		= RemoveFirstToken(&strLine);
		strDummy		= RemoveFirstToken(&strLine);
		strDummy		= RemoveFirstToken(&strLine);
		strDummy		= RemoveFirstToken(&strLine);
		strDummy		= RemoveFirstToken(&strLine);
		strDummy		= RemoveFirstToken(&strLine);
		strDummy		= RemoveFirstToken(&strLine);
		strDummy		= RemoveFirstToken(&strLine);
		strDummy		= RemoveFirstToken(&strLine);
		strStartEWCyc	= RemoveFirstToken(&strLine);
		strStartEWInc	= RemoveFirstToken(&strLine);
		strStartNSCyc	= RemoveFirstToken(&strLine);
		strStartNSInc	= RemoveFirstToken(&strLine);
		strStopEWCyc	= RemoveFirstToken(&strLine);
		strStopEWInc	= RemoveFirstToken(&strLine);
		strStopNSCyc	= RemoveFirstToken(&strLine);
		strStopNSInc	= RemoveFirstToken(&strLine);

		strLine  = m_strScanData[eFrameIndex].GetAt(i)			+ _T(", ");
		strLine += m_strScanData[eFrameTime].GetAt(i)			+ _T(", ");
		strLine += m_strScanData[eFrameLabel].GetAt(i)			+ _T(", ");
		strLine += m_strScanData[eFrameInstrument].GetAt(i)		+ _T(", ");
		strLine += m_strScanData[eFrameSpacelookMode].GetAt(i)	+ _T(", ");
		strLine += m_strScanData[eFrameSpacelookSide].GetAt(i)	+ _T(", ");
		strLine += m_strScanData[eFrameSndrStepMode].GetAt(i)	+ _T(", ");
		strLine += m_strScanData[eFrameCoordType].GetAt(i)		+ _T(", ");
		strLine += m_strScanData[eFrameStartNS].GetAt(i)		+ _T(", ");
		strLine += m_strScanData[eFrameStartEW].GetAt(i)		+ _T(", ");
		strLine += m_strScanData[eFrameStopNS].GetAt(i)			+ _T(", ");
		strLine += m_strScanData[eFrameStopEW].GetAt(i)			+ _T(", ");
		strLine += strStartNSCyc+	_T(", ")	+ strStartNSInc + _T(", ");
		strLine += strStartEWCyc+	_T(", ")	+ strStartEWInc + _T(", ");
		strLine += strStopNSCyc	+	_T(", ")	+ strStopNSInc	+ _T(", ");
		strLine += strStopEWCyc +	_T(", ")	+ strStopEWInc	+ _T(", ");
		strLine += strDuration	+	_T(", ");
		strLine += m_strScanData[eFrameDesc].GetAt(i);

		m_txtScan.AppendText(strLine);
	}

	strMsg.Format(_T("Frame file %s has been updated successfully"), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	scanList.RemoveAll();
	m_ScanImgList.RemoveAll();
	m_ScanSndList.RemoveAll();

	if( !m_txtScan.Close(CTextFile::WRITE, &m_fileException) )
	{
		strMsg = m_txtScan.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::LookupScanInFrameTable
//	Description :	LookupScanInFrameTable searches in the frame file
//					for a scan frame from a schedule.
//
//	Return :		int	-	The index of the scan object.
//
//	Parameters :
//		const CString* pstrLabel	-	pointer to the label of a frame.
//		const CString* pstrSide		-	pointer to the space look side of a frame.
//		const CString* pstrInst		-	pointer to the inst name of a frame.
//		const CString* pstrTime		-	pointer to the time of the scan.
//		const CString* pstrStrtXCyc -	pointer to the start X cyc value.
//		const CString* pstrStrtXInc -	pointer to the start X inc value.
//		const CString* pstrStrtYCyc -	pointer to the start Y cyc value.
//		const CString* pstrStrtYInc -	pointer to the start Y inc value.
//		const CString* pstrStopXCyc -	pointer to the stop X cyc value.
//		const CString* pstrStopXInc -	pointer to the stop X inc value.
//		const CString* pstrStopYCyc -	pointer to the stop Y cyc value.
//		const CString* pstrStopYInc -	pointer to the stop Y inc value.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CSchedUpdUtil::LookupScanInFrameTable(
	const CString* pstrLabel,	const CString* pstrSide,
	const CString* pstrInst,	const CString* pstrTime,
/**/const CString* pstrStrtXCyc,const CString* pstrStrtXInc,
/**/const CString* pstrStrtYCyc,const CString* pstrStrtYInc,
/**/const CString* pstrStopXCyc,const CString* pstrStopXInc,
/**/const CString* pstrStopYCyc,const CString* pstrStopYInc)
{
	int		nObjIndex   = 0;
	int		nCurFrame	= 0;
	bool	bFound		= false;
	CString strLabel	= *pstrLabel;
	CString strSide		= *pstrSide;
	CString strInst		= *pstrInst;

	if( m_bScanNew )
	{
		if( strSide.CompareNoCase(strSpacelookSide[0]) != 0 ) // != Oats
		{
			if( (m_bDoSRSO && m_bDoSRSOPreDef	&& strLabel != m_strSRSOLabel && m_bDoStaticFrm)||
				(m_bDoSRSO && !m_bDoSRSOPreDef	&& strLabel != m_strSRSOFrame && m_bDoStaticFrm)||
				(!m_bDoSRSO&& m_bDoStaticFrm) )
				strLabel = strLabel + strSide;
		}
	}
	else
		strLabel = strLabel + strSide;

	while( (nCurFrame < m_strScanData[eFrameIndex].GetSize()) && (!bFound) )
	{
		if( (m_strScanData[eFrameLabel].GetAt(nCurFrame).CompareNoCase(strLabel)	== 0) &&
			(m_strScanData[eFrameInstrument].GetAt(nCurFrame).CompareNoCase(strInst)== 0) )

			if( pstrTime == NULL )
			{
				nObjIndex	= _ttoi(m_strScanData[eFrameIndex].GetAt(nCurFrame));
				bFound		= true;

				// If using existing frame table make sure the cycles and increments match...
/**/			if( !m_bScanNew )
/**/			{
/**/				if( pstrStrtXCyc != NULL )
/**/				{
/**/					if( (m_strScanData[eFrameStartCycEW].GetAt(nCurFrame).Compare(*pstrStrtXCyc)	!= 0)	||
/**/						(m_strScanData[eFrameStartIncrEW].GetAt(nCurFrame).Compare(*pstrStrtXInc)	!= 0)	||
/**/						(m_strScanData[eFrameStartCycNS].GetAt(nCurFrame).Compare(*pstrStrtYCyc)	!= 0)	||
/**/						(m_strScanData[eFrameStartIncrNS].GetAt(nCurFrame).Compare(*pstrStrtYInc)	!= 0)	||
/**/						(m_strScanData[eFrameStopCycEW].GetAt(nCurFrame).Compare(*pstrStopXCyc)		!= 0)	||
/**/						(m_strScanData[eFrameStopIncrEW].GetAt(nCurFrame).Compare(*pstrStopXInc)	!= 0)	||
/**/						(m_strScanData[eFrameStopCycNS].GetAt(nCurFrame).Compare(*pstrStopYCyc)		!= 0)	||
/**/						(m_strScanData[eFrameStopIncrNS].GetAt(nCurFrame).Compare(*pstrStopYInc)	!= 0) )
/**/					{
/**/						CString strMsg(_T("Frame data from OATS does not match Frame table data!"));
/**/						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
/**/						strMsg.Format(_T("Frame from OATS : %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s"),
/**/							strLabel, strSide, strInst, *pstrStrtXCyc, *pstrStrtXInc, *pstrStrtYCyc,
/**/							*pstrStrtYInc, *pstrStopXCyc, *pstrStopXInc, *pstrStopYCyc, *pstrStopYInc);
/**/						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
/**/						strMsg.Format(_T("Frame from TABLE: %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s"),
/**/							strLabel, strSide, strInst,
/**/							m_strScanData[eFrameStartCycEW].GetAt(nCurFrame),
/**/							m_strScanData[eFrameStartIncrEW].GetAt(nCurFrame),
/**/							m_strScanData[eFrameStartCycNS].GetAt(nCurFrame),
/**/							m_strScanData[eFrameStartIncrNS].GetAt(nCurFrame),
/**/							m_strScanData[eFrameStopCycEW].GetAt(nCurFrame),
/**/							m_strScanData[eFrameStopIncrEW].GetAt(nCurFrame),
/**/							m_strScanData[eFrameStopCycNS].GetAt(nCurFrame),
/**/							m_strScanData[eFrameStopIncrNS].GetAt(nCurFrame));
/**/						GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
/**/					}
/**/				}
/**/			}
/**/		}
			else
			{
				if( m_strScanData[eFrameTime].GetAt(nCurFrame).CompareNoCase(*pstrTime) == 0 )
				{
					nObjIndex	= _ttoi(m_strScanData[eFrameIndex].GetAt(nCurFrame));
					bFound		= true;
				}
				else
					nCurFrame++;
			}
		else
			nCurFrame++;
	}

	if( bFound )
		return nObjIndex/*nCurFrame*/;
	else
	{
		CString strErr;
		strErr.Format(IDS_UPDATE_SCAN_ENTRY_NOT_FOUND, strLabel, strSide, strInst, ctstrFRAME);
		GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
		return -1;
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::LookupScanInSectorTable
//	Description :	LookupScanInSectorTable searches in the sector file
//					for a scan frame from a schedule.
//
//	Return :		int	-	The index of the scan object.
//
//	Parameters :
//		const CString* pstrLabel	-	pointer to the label of a frame.
//		const CString* pstrSide		-	pointer to the space look side of a frame.
//		const CString* pstrInst		-	pointer to the inst name of a frame.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CSchedUpdUtil::LookupScanInSectorTable(
	const CString* pstrLabel,
	const CString* pstrSide,
	const CString* pstrInst)
{
	int		nCurSector	= 0;
	bool	bFound		= false;
	CString strLabel(*pstrLabel);

	if( pstrSide->CompareNoCase(strSpacelookSide[0]) != 0 ) // != Oats
	{
		if( (m_bDoSRSO && m_bDoSRSOPreDef	&& strLabel != m_strSRSOLabel && m_bDoStaticFrm)||
			(m_bDoSRSO && !m_bDoSRSOPreDef	&& strLabel != m_strSRSOFrame && m_bDoStaticFrm)||
			(!m_bDoSRSO&& m_bDoStaticFrm) )
			strLabel = strLabel + *pstrSide;
	}

	while( (nCurSector < m_strSectorData[eSectorLabel].GetSize()) && (!bFound) )
	{
		if( (m_strSectorData[eSectorLabel].GetAt(nCurSector).CompareNoCase(strLabel)			== 0)&&
			(m_strSectorData[eSectorInstrument].GetAt(nCurSector).CompareNoCase(*pstrInst)		== 0)&&
			(m_strSectorData[eSectorSpacelookSide].GetAt(nCurSector).CompareNoCase(*pstrSide)	== 0) )
			bFound = true;
		else
			nCurSector++;
	}

	if( bFound )
		return nCurSector;
	else
	{
		CString strErr;
		strErr.Format(IDS_UPDATE_SCAN_ENTRY_NOT_FOUND, *pstrLabel, *pstrSide, *pstrInst, _T("SECTOR"));
		GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
		return -1;
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::LookupScanInData
//	Description :	LookupScanInData searches the in-memory scan data
//					for a scan frame from a schedule.
//
//	Return :		int	-	The index of the scan object.
//
//	Parameters :
//		const CString* pstrTime			-	pointer to the time of the scan.
//		const CString* pstrInst			-	pointer to the inst name of a frame.
//		const CStringArray*	pScanList	-	pointer to a list of
//											frame directives.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CSchedUpdUtil::LookupScanInData(
	const CString*		pstrTime,
	const CString*		pstrInst,
	const CStringArray*	pScanList)
{
	int		nCurScan	= 0;
	bool	bFound		= false;
	CString strInst		= ' ' + *pstrInst;

	strInst.MakeUpper();

	while( (nCurScan < pScanList->GetSize()) && (!bFound) )
	{
		CString strTemp(pScanList->GetAt(nCurScan));
		strTemp.MakeUpper();

		if( strTemp.Find(*pstrTime) != -1 && strTemp.Find(strInst) != -1 )
			bFound = true;
		else
			nCurScan++;
	}

	return (bFound ? nCurScan : -1);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::ScanStrData
//	Description :	ScanStrData is used to help construct a 155 message
//					for OATS processing.
//
//	Return :		void		-
//
//	Parameters :
//		CStringArray* strData	-	pointer to a list of frame directives.
//		CString strTime			-	time of the scan.
//		CString strLabel		-	label name of a frame.
//		CString strInst			-	inst name of a frame.
//		int nScanInstrIndex		-	index of frame.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::ScanStrData(
	CStringArray*	strData,
	CString			strTime,
	CString			strLabel,
	CString			strInst,
	int				nScanInstrIndex)
{
	CString strIndex;
	strIndex.Format(_T("%d"), nScanInstrIndex);
	strData->SetAtGrow(eFrameIndex, 		strIndex);
	strData->SetAtGrow(eFrameTime, 			strTime);
	strData->SetAtGrow(eFrameLabel, 		strLabel);
	strData->SetAtGrow(eFrameInstrument, 	strInst);
	strData->SetAtGrow(eFrameSpacelookMode,	m_strScanData[eFrameSpacelookMode].GetAt(nScanInstrIndex-1));
	strData->SetAtGrow(eFrameSpacelookSide,	m_strScanData[eFrameSpacelookSide].GetAt(nScanInstrIndex-1));
	strData->SetAtGrow(eFrameSndrStepMode,	m_strScanData[eFrameSndrStepMode].GetAt(nScanInstrIndex-1));
	strData->SetAtGrow(eFrameCoordType,		m_strScanData[eFrameCoordType].GetAt(nScanInstrIndex-1));
	strData->SetAtGrow(eFrameStartNS,		m_strScanData[eFrameStartNS].GetAt(nScanInstrIndex-1));
	strData->SetAtGrow(eFrameStartEW,		m_strScanData[eFrameStartEW].GetAt(nScanInstrIndex-1));
	strData->SetAtGrow(eFrameStopNS,		m_strScanData[eFrameStopNS].GetAt(nScanInstrIndex-1));
	strData->SetAtGrow(eFrameStopEW,		m_strScanData[eFrameStopEW].GetAt(nScanInstrIndex-1));
	strData->SetAtGrow(eFrameStartCycNS, 	_T("0.0"));
	strData->SetAtGrow(eFrameStartIncrNS, 	_T("0.0"));
	strData->SetAtGrow(eFrameStartCycEW, 	_T("0.0"));
	strData->SetAtGrow(eFrameStartIncrEW, 	_T("0.0"));
	strData->SetAtGrow(eFrameStopCycNS, 	_T("0.0"));
	strData->SetAtGrow(eFrameStopIncrNS, 	_T("0.0"));
	strData->SetAtGrow(eFrameStopCycEW, 	_T("0.0"));
	strData->SetAtGrow(eFrameStopIncrEW, 	_T("0.0"));
	strData->SetAtGrow(eFrameDur, 			_T("0.0"));
	strData->SetAtGrow(eFrameDesc, 			m_strScanData[eFrameDesc].GetAt(nScanInstrIndex-1));
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::SectorStrData
//	Description :	SectorStrData is used to help construct a 155 message
//					for OATS processing.
//
//	Return :		void		-
//
//	Parameters :
//		CStringArray* strData	-	pointer to a list of frame directives.
//		CString strTime			-	time of the scan.
//		CString strLabel		-	label name of a frame.
//		CString strInst			-	inst name of a frame.
//		int nScanInstrIndex		-	index of frame.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::SectorStrData(
	CStringArray*	strData,
	CString			strTime,
	CString			strLabel,
	CString			strInst,
	int				nScanInstrIndex)
{
	CString strIndex;
	strIndex.Format(_T("%d"), nScanInstrIndex);
	strData->SetAtGrow(0, 						strIndex);
	strData->SetAtGrow(1, 						strTime);
	strData->SetAtGrow(eSectorLabel+2, 			strLabel);
	strData->SetAtGrow(eSectorInstrument+2, 	strInst);
	strData->SetAtGrow(eSectorSpacelookMode+2,	m_strSectorData[eSectorSpacelookMode].GetAt(nScanInstrIndex));
	strData->SetAtGrow(eSectorSpacelookSide+2,	m_strSectorData[eSectorSpacelookSide].GetAt(nScanInstrIndex));
	strData->SetAtGrow(eSectorSndrStepMode+2,	m_strSectorData[eSectorSndrStepMode].GetAt(nScanInstrIndex));
	strData->SetAtGrow(eSectorCoordType+2,		m_strSectorData[eSectorCoordType].GetAt(nScanInstrIndex));
	strData->SetAtGrow(eSectorStartNS+2,		m_strSectorData[eSectorStartNS].GetAt(nScanInstrIndex));
	strData->SetAtGrow(eSectorStartEW+2,		m_strSectorData[eSectorStartEW].GetAt(nScanInstrIndex));
	strData->SetAtGrow(eSectorStopNS+2,			m_strSectorData[eSectorStopNS].GetAt(nScanInstrIndex));
	strData->SetAtGrow(eSectorStopEW+2,			m_strSectorData[eSectorStopEW].GetAt(nScanInstrIndex));
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::SrsoStrData
//	Description :	SrsoStrData is used to help construct a 155 message
//					for OATS processing.
//
//	Return :		void		-
//
//	Parameters :
//		CStringArray* strData	-	pointer to a list of frame directives.
//		CString strTime			-	time of the scan.
//		CString strLabel		-	label name of a frame.
//		CString strInst			-	inst name of a frame.
//		int nScanInstrIndex		-	index of frame.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::SrsoStrData(
	CStringArray*	strData,
	CString			strTime,
	CString			strLabel,
	CString			strInst,
	CString			strSide)
{
	CString strIndex;
	strIndex.Format(_T("%d"), 61);
	strData->SetAtGrow(eFrameIndex, 		strIndex);
	strData->SetAtGrow(eFrameTime, 			strTime);
	strData->SetAtGrow(eFrameLabel, 		strLabel);
	strData->SetAtGrow(eFrameInstrument, 	strInst);
	strData->SetAtGrow(eFrameSpacelookMode,	GetAppRegSC()->m_strSRSOSpacelookMode[GetAppRegSite()->m_nDefaultSC]);
	strData->SetAtGrow(eFrameSpacelookSide,	strSide);
	strData->SetAtGrow(eFrameSndrStepMode,	_T("N/A-Imager"));
	strData->SetAtGrow(eFrameCoordType,		m_strSRSOCoordType);
	strData->SetAtGrow(eFrameStartNS,		m_strSRSOCoordNS);
	strData->SetAtGrow(eFrameStartEW,		m_strSRSOCoordEW);
	strData->SetAtGrow(eFrameStopNS,		m_strSRSODuration);
	strData->SetAtGrow(eFrameStopEW,		m_strSRSORatio);
	strData->SetAtGrow(eFrameStartCycNS, 	_T("0.0"));
	strData->SetAtGrow(eFrameStartIncrNS, 	_T("0.0"));
	strData->SetAtGrow(eFrameStartCycEW, 	_T("0.0"));
	strData->SetAtGrow(eFrameStartIncrEW, 	_T("0.0"));
	strData->SetAtGrow(eFrameStopCycNS, 	_T("0.0"));
	strData->SetAtGrow(eFrameStopIncrNS, 	_T("0.0"));
	strData->SetAtGrow(eFrameStopCycEW, 	_T("0.0"));
	strData->SetAtGrow(eFrameStopIncrEW, 	_T("0.0"));
	strData->SetAtGrow(eFrameDur, 			_T("0.0"));
	strData->SetAtGrow(eFrameDesc, 			_T("SRSO Slow Space Scan"));
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::RemoveRtcsBlock
//	Description :	RemoveStarBlock removes all of the rtcs lines that
//					were automatically inserted by a previous update.
//					It will remove the rtcsdata lines then call
//					RemoveGroundBlock.
//
//	Return :		int		-	The number of lines removed.
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CSchedUpdUtil::RemoveRtcsBlock()
{
	int		nNumRemoved = 0;
	int		nCurPos		= 0;
	int		nTotalLines	= m_txtNewSched.GetLineCount();
	CString strLine;

	// If we are not at the end of the file
	while( nCurPos < nTotalLines )
	{
		// Read in the next line.  Check to see if it is a scandata directive
		strLine = m_txtNewSched.GetTextAt(nCurPos);

		if(	m_txtNewSched.ScanLine(strLine, ctstrRTCSDATA) )
		{
			m_txtNewSched.RemoveAt(nCurPos);
			nNumRemoved++;
			nTotalLines--;

			while( strLine.ReverseFind(_T('\\')) != -1 )
			{
				strLine = m_txtNewSched.GetTextAt(nCurPos);
				m_txtNewSched.RemoveAt(nCurPos);
				nNumRemoved++;
				nTotalLines--;
			}
		}
		else
			nCurPos++;
	}

	nNumRemoved = nNumRemoved + RemoveGroundBlock(_T("rtcs"));

	return nNumRemoved;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::UpdateScheduleRTCSs
//	Description :	UpdateScheduleRTCSs updates a schedule for RTCSs.
//
//	Return :		BOOL	-	TRUE if schedule was updated successfully
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::UpdateScheduleRTCSs()
{
	CString strTxt(_T("Updating Schedule RTCSs..."));
	GetAppOutputBar()->OutputToEvents(strTxt, COutputBar::INFO);

	RemoveRtcsBlock();

	m_txtNewSched.DelRTCSUpdateDir();

	CSchedTime schedTime;
	CString strUpdateLine(ctstrRTCS + m_strUserName + _T(" ") +
			schedTime.GetCurrSchedTime() + _T(" ") + m_strMap);
	m_txtNewSched.AddUpdateDir(strUpdateLine);

	CTextFile tf;

	if( !tf.Load(m_strMap, CTextFile::RO, &m_fileException) )
	{
//		CString strMsg(tf.ExceptionErrorText(&m_fileException));
		CString strError;
		strError.Format(_T("%s file: %s"), tf.ExceptionErrorText(&m_fileException), tf.GetFileName());
//		strError.Format(_T("%s generated an exception: %s."), tf.GetFileName(), strMsg); 
//		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		GetAppOutputBar()->OutputToEvents(strError, COutputBar::FATAL);
		return FALSE;
	}

	CString strSubst;
	CString strLabel;
	CString strLineS;
	CString strLineM;
	CString strRTCSdata;
	CString strFilepath;

	int	nIndex = 0;
	int	nRtnIx = 0;

	while( nIndex < m_txtNewSched.GetLineCount() )
	{
		strLineS = m_txtNewSched.GetDirLine(nIndex++,nRtnIx);
		strLineS.MakeUpper();
		strLineS.TrimLeft();

		if( (!strLineS.IsEmpty()) && strLineS[0] != ctchCOMMENT ) //NEW CHECK BEFORE USE
		{
			if( m_txtNewSched.ScanLine(strLineS, _T("RTCS")) )
			{
				int		nCount	= 0;
				BOOL	nFound	= FALSE;
						strLabel= ExtractValue(strLineS,_T("LABEL="));

				if( strLabel.IsEmpty() )
					strLabel = ExtractValue(strLineS,_T("LABEL ="));

				if( strLabel.IsEmpty() )
					strLabel = ExtractValue(strLineS,_T("LABEL "));

				// find on-board index using label in map file
				while( nCount < tf.GetLineCount() && (!nFound) )
				{
					strLineM = tf.GetDirLine(nCount++, nRtnIx);
					strLineM.MakeUpper();
					strSubst = strLineM;
					strSubst.Replace(',', ' ');

					if( tf.ScanLine(strSubst, strLabel) )
					{
						strSubst= RemoveFirstToken(&strLineM);
						strSubst= RemoveFirstToken(&strLineM);
						strSubst= RemoveFirstToken(&strLineM);
						nFound	= TRUE;
					}
				}

				if( !nFound )
				{
					CString strMsg;
					strMsg.Format(_T("No label reference %s in map file. The schedule file was not updated."),strLabel);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					return FALSE;
				}

				int nAddrIndx = _ttoi(strSubst);

				strFilepath.Format(_T("START ./%s/RTCS_exec(\" %s \")"),
					GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC], strLabel);

				strRTCSdata.Format(_T("    RTCSDATA LABEL=%s ADDRESS=%d"), strLabel, nAddrIndx+1);
				m_txtNewSched.InsertAt(nIndex++,strRTCSdata);
				m_txtNewSched.InsertAt(nIndex++,_T("##ground begin rtcs"));
				m_txtNewSched.InsertAt(nIndex++,strFilepath);
				m_txtNewSched.InsertAt(nIndex++,_T("##ground end rtcs"));
			}
		}
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::IMCScaleFactorCalSched
//	Description :	IMCScaleFactorCalSched request from OATS the data
//					to construct a Scale Factor Calibration Schedule.
//
//	Return :		BOOL	-	TRUE if the schedule file was created
//								successfully.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::IMCScaleFactorCalSched()
{
	MSG_175		msg175;
	int			nInstF;
	CString		strMsg;
	CString		strInstrTyp;
	CPtrList	list;
	COXUNC		unc(m_strRptFile);

	if( m_strInst.CompareNoCase(ctstrIMGR) == 0 )
	{
		strInstrTyp = ctstrIMGR;
		msg175.m_hdr.m_nMsgSubType = 1;
		nInstF		= 1;
	}
	else if( m_strInst.CompareNoCase(ctstrSDR) == 0 )
	{
		strInstrTyp = ctstrSDR;
		msg175.m_hdr.m_nMsgSubType = 2;
		nInstF 		= 4;
	}
	else
	{
		strMsg = _T("Invalid instrument ");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	// Write out the message
	msg175.m_hdr.m_nSequence		= 0;
	msg175.m_hdr.m_nSatelliteID		= (unsigned __int8)GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC];
	msg175.m_hdr.m_nSource			= 10;
	msg175.m_hdr.m_nDestination		= 32;
	msg175.m_hdr.m_nNumHalfWords	= MSG_175_DATA_HW_SIZE;
	msg175.m_hdr.m_nMsgType			= 175;
	strncpy_s (msg175.m_hdr.m_cTracer, "         ", 10);
	msg175.m_hdr.m_cTracer[9] = ' ';
	msg175.m_hdr.m_nMsgCnt			= 1;
	msg175.m_hdr.m_nMsgEndFlag		= 0xFF;
	msg175.m_hdr.m_nSpare			= 0;
	msg175.m_hdr.m_nRoutingID		= 0;

	unsigned __int32 timeBCD[2];
	ConvertStringTimeToBCD (m_strStartTime, timeBCD);
	msg175.m_175Data.m_nBCDTime[0]	= timeBCD[0];
	msg175.m_175Data.m_nBCDTime[1]	= timeBCD[1];
	msg175.m_175Data.m_fDuration	= (float)m_nDuration;

	if( !m_CommUtil.SendMsg(&msg175, 75, &list) )
	{
		CString strErr;
		
		strErr = GetLastError()==ERROR_SUCCESS?_T(""):GetLastErrorText();
			
		if( strErr.IsEmpty() )
			strErr = _T("The IMC SFCS file was not generated.");
		else
			strErr+= _T(" The IMC SFCS file was not generated.");
			
		strMsg.Format(IDS_UPDATE_WRITE_ERROR, strErr);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		DeleteListElements(&list);
		return FALSE;
	}

	if( list.IsEmpty() )
	{
		strMsg.Format(_T("No star data returned from OATS."));
		strMsg += _T(" The IMC SFCS file was not generated.");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	CTextFile dataFile;

	// Open the filename
	if( !dataFile.Load(m_strRptFile, CTextFile::CR, &m_fileException) )
	{
		strMsg = _T("Error opening file ") + m_strRptFile;
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		DeleteListElements(&list);
		return FALSE;
	}

	CTime	curTime(CTime::GetCurrentTime());
	CString strGenTime = curTime.FormatGmt(_T("%Y-%j-%H:%M:%S.000"));
	CString strTmp;

	// Generate the Procedure
	dataFile.AppendText(ctstrPrlgBeg);
	strTmp.Format(_T("##valid unvalidated ground %s %s"), m_strUserName, strGenTime);
	dataFile.AppendText(strTmp);
	strTmp.Format(_T("##desc %s"), m_strRptFile);
	dataFile.AppendText(strTmp);
	dataFile.AppendText(ctstrPrlgEnd);

	CStringArray	strStarParam;
	CStringArray	strProcParam;
	CString			strCmdExeTime;
	MSG_75*			p75Msg;
	POSITION		pos				= list.GetHeadPosition();
	BOOL			bGTFNMsg		= FALSE; // Get Time From Next Message
	int				nObjId			= 1;
	int				nCurTotal		= 0;
	int				nStarId			= 0;
	int				nNumLooks		= 0;
	int				nLookNum		= 0;
	int				nXCyc			= 0;
	int				nXInc			= 0;
	int				nYCyc			= 0;
	int				nYInc			= 0;
	float			fImcCalOffset	= 0.0;

	while( pos != NULL )
	{
			p75Msg		= (MSG_75*)list.GetNext(pos);
		int nNumOffsets	= p75Msg->m_75Hdr.m_nNumOffsets;

		if( bGTFNMsg )
		{
			CString		tmpTime;
			CSchedTime	t1a(strCmdExeTime);

			ConvertBCDToStringTime(
				p75Msg->m_75OffsetData[0].m_75StarData[0].m_75LookData[0].m_data.m_nBCDTime, &tmpTime);

			CSchedTime	t2a(tmpTime);
			double		t3a	= t2a.GetSchedTimeInSeconds() - t1a.GetSchedTimeInSeconds();

			CString strLineStar;
			strLineStar.Format(
				_T("STARINFO STARID=%d OBJID=%d SCID=%d INSTRUMENT=%s NUMLOOKS=%d LOOKNUM=%d TIME=\"%s\" DWELL=%d OFFSET=%f"),
				nStarId, nObjId, GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC],
				strInstrTyp, nNumLooks, nLookNum, strCmdExeTime, (int)t3a, fImcCalOffset);
			strStarParam.SetAtGrow(nCurTotal, strLineStar);

			CString strLineProc;
			strLineProc.Format(
				_T("    START ./%s/procs/%s(\"%s\", \"%s\", %d, %d, %d, %d, \"%s\", %d, \"%s\", %f)"),
				GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],
				GetAppRegSC()->m_strSFCSMiniSched[GetAppRegSite()->m_nDefaultSC],
				GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC],
				strCmdExeTime, nXCyc, nXInc, nYCyc, nYInc, strInstrTyp, 0, ctstrON, fImcCalOffset);
			strProcParam.SetAtGrow(nCurTotal++, strLineProc);

			nObjId++;
			bGTFNMsg = FALSE;
		}

		if( nNumOffsets < 1 || nNumOffsets > MAX_75_OFFSET_DATA )
		{
			strMsg.Format(IDS_UPDATE_STARS_FRAMES,_T("'the number of offset changes'."));
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			DeleteFile(m_strRptFile);
			DeleteListElements(&list);
			return FALSE;
		}

		CString strStartTime;
		ConvertBCDToStringTime(p75Msg->m_75Hdr.m_nBCDTime, &strStartTime);

		for( int nCurOffset=0; nCurOffset<nNumOffsets; nCurOffset++ )
		{
			int	nStarsInOffset	= p75Msg->m_75OffsetData[nCurOffset].m_data.m_nNumStarsInOffset;
				fImcCalOffset	= p75Msg->m_75OffsetData[nCurOffset].m_data.m_fImcCalOffset;

			if( nStarsInOffset < 0 || nStarsInOffset > MAX_75_STAR_DATA )
			{
				strMsg.Format(IDS_UPDATE_STARS_FRAMES,_T("'the number of stars for IMC offset'."));
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				DeleteFile(m_strRptFile);
				DeleteListElements(&list);
				return FALSE;
			}

			if( fImcCalOffset < ctnMinImcCalOffset || fImcCalOffset > ctnMaxImcCalOffset )
			{
				strMsg.Format(IDS_UPDATE_STARS_FRAMES,_T("'the IMC calibration offset'."));
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
				DeleteFile(m_strRptFile);
				DeleteListElements(&list);
				return FALSE;
			}

			for( int nCurStar=0; nCurStar<nStarsInOffset; nCurStar++ )
			{
				int nStarLooks	= p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].m_data.m_nNumLooksForStar;
					nStarId		= p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].m_data.m_nStarID;

				if( nStarLooks < 1 && nStarLooks > MAX_75_LOOK_DATA )
				{
					strMsg.Format(IDS_UPDATE_STARS_FRAMES,_T("'the number of looks for star'."));
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					DeleteFile(m_strRptFile);
					DeleteListElements(&list);
					return FALSE;
				}

				if( nStarId < -1 || nStarId == 0 || nStarId > ctnMaxStarId )
				{
					strMsg.Format(IDS_UPDATE_STARS_FRAMES,_T("'the star Id'."));
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
					DeleteFile(m_strRptFile);
					DeleteListElements(&list);
					return FALSE;
				}

				for( int nCurLook=0; nCurLook<nStarLooks; nCurLook++ )
				{
					if( !ValidateSfMsg(&(p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].
							m_75LookData[nCurLook].m_data),p75Msg->m_hdr.m_nMsgSubType) )
					{
						DeleteFile(m_strRptFile);
						DeleteListElements(&list);
						return FALSE;
					}

					nNumLooks	= p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].m_data.m_nNumLooksForStar;
					nLookNum	= p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].m_75LookData[nCurLook].m_data.m_nLookNum;
					nXCyc		= p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].m_75LookData[nCurLook].m_data.m_nEWCyc;
					nXInc		= p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].m_75LookData[nCurLook].m_data.m_nEWInc;
					nYCyc		= p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].m_75LookData[nCurLook].m_data.m_nNSCyc;
					nYInc		= p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].m_75LookData[nCurLook].m_data.m_nNSInc;

					ConvertBCDToStringTime(
						p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].m_75LookData[nCurLook].m_data.m_nBCDTime,
						&strCmdExeTime);

					CString		strStarBit;
					CSchedTime	t1(strCmdExeTime);
					double		t3;
					int			nDwellRepeats = p75Msg->m_75OffsetData[nCurOffset].m_75StarData[nCurStar].m_75LookData[nCurLook].m_data.m_nDwellRepeats;

					if( (p75Msg->m_hdr.m_nMsgEndFlag == 255)&& (nCurOffset == (nNumOffsets-1)) &&
						(nCurStar == (nStarsInOffset-1))	&& (nCurLook == (nStarLooks-1)) )
					{
						t3			= ((nDwellRepeats+1) * nInstF) + 15;
						strStarBit	= ctstrOFF;
					}
					else
					{
						CString strTmpTime;

						if( nCurLook < (nStarLooks-1) )
							ConvertBCDToStringTime(p75Msg->m_75OffsetData[nCurOffset].
							m_75StarData[nCurStar].m_75LookData[nCurLook+1].m_data.m_nBCDTime, &strTmpTime);
						else if( nCurStar < (nStarsInOffset-1) )
							ConvertBCDToStringTime(p75Msg->m_75OffsetData[nCurOffset].
							m_75StarData[nCurStar+1].m_75LookData[0].m_data.m_nBCDTime, &strTmpTime);
						else if( nCurOffset < (nNumOffsets-1) )
							ConvertBCDToStringTime(p75Msg->m_75OffsetData[nCurOffset+1].
							m_75StarData[0].m_75LookData[0].m_data.m_nBCDTime, &strTmpTime);
						else // Get star look time from next 50402-75 message
						{
							bGTFNMsg = TRUE;
							break;
						}

						CSchedTime t2(strTmpTime);

						t3				= t2.GetSchedTimeInSeconds() - t1.GetSchedTimeInSeconds();
						strStarBit		= ctstrON;
						nDwellRepeats	= 0;
					}

					CString strLineStar;
					strLineStar.Format(
						_T("STARINFO STARID=%d OBJID=%d SCID=%d INSTRUMENT=%s NUMLOOKS=%d LOOKNUM=%d TIME=\"%s\" DWELL=%d OFFSET=%f"),
						nStarId, nObjId, GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC],
						strInstrTyp, nNumLooks, nLookNum, strCmdExeTime, (int)t3, fImcCalOffset );
					strStarParam.SetAtGrow(nCurTotal, strLineStar);

					CString strLineProc;
					strLineProc.Format(
						_T("    START ./%s/procs/%s(\"%s\", \"%s\", %d, %d, %d, %d, \"%s\", %d, \"%s\", %f)"),
						GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],
						GetAppRegSC()->m_strSFCSMiniSched[GetAppRegSite()->m_nDefaultSC],
						GetAppRegSC()->m_strRTAddress[GetAppRegSite()->m_nDefaultSC],
						strCmdExeTime, nXCyc, nXInc, nYCyc, nYInc, strInstrTyp,
						nDwellRepeats, strStarBit, fImcCalOffset);
					strProcParam.SetAtGrow(nCurTotal++, strLineProc);

					nObjId++;
				}
			}
		}
	}

	for( int nStarIndex=0; nStarIndex < nCurTotal; nStarIndex++ )
		dataFile.AppendText(strStarParam.GetAt(nStarIndex));

	dataFile.AppendText(_T("##ground begin star"));

	for( int nProcIndex=0; nProcIndex < nCurTotal; nProcIndex++ )
		dataFile.AppendText(strProcParam.GetAt(nProcIndex));

	dataFile.AppendText(_T("##ground end star"));

	if( !dataFile.Close(CTextFile::WRITE, &m_fileException) )
	{
		strMsg = dataFile.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		DeleteListElements(&list);
		return FALSE;
	}

	strStarParam.RemoveAll();
	strProcParam.RemoveAll();
	DeleteListElements(&list);

	strMsg.Format(
	_T("IMC Scale Factor Calibration Schedule file Generation successful - File %s created."), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::GenerateIntrusion
//	Description :	GenerateIntrusion request from OATS the data
//					to construct an Intrusion report.
//
//	Return :		BOOL	-	TRUE if the report file was created
//								successfully.
//
//	Parameters :	int	nType	-	type of intrusion report.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::GenerateIntrusion(int nType)
{
	CPtrList	list;
	CString		strMsg;
	MSG_160		msg160;
	MSG_60		msg60;

	// Write out the message
	msg160.m_hdr.m_nSequence		= 0;
	msg160.m_hdr.m_nSatelliteID		= (unsigned __int8)GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC];
	msg160.m_hdr.m_nSource			= 10;
	msg160.m_hdr.m_nDestination		= 32;
	msg160.m_hdr.m_nNumHalfWords	= MSG_160_DATA_HW_SIZE;
	msg160.m_hdr.m_nMsgType			= 160;
	msg160.m_hdr.m_nMsgSubType		= (unsigned __int8)nType;
	strncpy_s (msg160.m_hdr.m_cTracer, "         ", 10);
	msg160.m_hdr.m_cTracer[9] = ' ';
	msg160.m_hdr.m_nMsgCnt			= 1;
	msg160.m_hdr.m_nMsgEndFlag		= 0xFF;
	msg160.m_hdr.m_nSpare			= 0;
	msg160.m_hdr.m_nRoutingID		= 0;
	msg160.m_160Data.m_fDuration	= (float)(m_nDuration*1440);
	ConvertStringTimeToBCD (m_strStartTime, &msg160.m_160Data.m_nStartTime[0]);

	if( nType == 1 )
	{
		if( !m_CommUtil.SendMsg(&msg160, 60, &msg60)){
			strMsg = _T("ERROR");
			DeleteListElements(&list);
		}
	}
	else
	{
		if( !m_CommUtil.SendMsg(&msg160, 61, &list)){
			strMsg = _T("ERROR");
			DeleteListElements(&list);
		}
	}

	if( !strMsg.IsEmpty() )
	{
		CString strErr;

		strErr = GetLastError()==ERROR_SUCCESS?_T(""):GetLastErrorText();
			
		if( strErr.IsEmpty() )
			strErr = _T("The Intrusion Report was not generated.");
		else
			strErr+= _T(" The Intrusion Report was not generated.");
			
		strMsg.Format(IDS_UPDATE_WRITE_ERROR, strErr);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	switch( nType )
	{
	case 1: if( !SensorIntrusionRpt(&msg60)){
				return FALSE;
			}
			break;
	case 2: if( !FrameIntrusionRpt(&list) )
			{
				DeleteListElements(&list);
				return FALSE;
			}

			DeleteListElements(&list);
			break;
	default:
			strMsg = _T("Unknown Intrusion Report Request!");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::SensorIntrusionRpt
//	Description :	SensorIntrusionRpt creates a report from the data
//					supplied from OATS.
//
//	Return :		BOOL	-	TRUE if the report file was created
//								successfully.
//
//	Parameters :	MSG_60* pMsg	-	50402 message.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::SensorIntrusionRpt(MSG_60* pMsg)
{
	CString		strMsg;
	CTextFile	dataFile;
	COXUNC		unc(m_strRptFile);

	strMsg.Format(_T("Generating OATS Sensor Intrusion report file %s"), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	// Open the filename
	if( !dataFile.Load(m_strRptFile, CTextFile::CR, &m_fileException) )
	{
		strMsg = _T("Error opening file ") + m_strRptFile;
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	// Generate the report
	CTime curTime(CTime::GetCurrentTime());
	CString strGenTime = curTime.FormatGmt(_T("%Y-%j-%H:%M:%S.000"));
	CString strStatus0(_T("Request within range of operational support. All events in\n"));
	CString strStatus1(_T("Request extends beyond range of operational support. All\n"));
	CString strStatus2(_T("Unable to respond to a request outside the bounds or \n"));
	strStatus0 += _T("                                    requested range are given.");
	strStatus1 += _T("                                    events in requested range and in range of operational support\n");
	strStatus1 += _T("                                    are listed below.");
	strStatus2 += _T("                                    operational support.");

	CString strImagerStatus;
	CString strSounderStatus;
	CString strAntennaStatus;

	switch( pMsg->m_60ImagerHdr.m_nStatus )
	{
		case -2:	strImagerStatus = strStatus2;		break;
		case -1:	strImagerStatus = strStatus1;		break;
		case  0:	strImagerStatus = strStatus0;		break;
		default:	strImagerStatus = _T("Unknown");	break;
	}

	switch( pMsg->m_60SounderHdr.m_nStatus )
	{
		case -2:	strSounderStatus = strStatus2;		break;
		case -1:	strSounderStatus = strStatus1;		break;
		case  0:	strSounderStatus = strStatus0;		break;
		default:	strSounderStatus = _T("Unknown");	break;
	}

	switch( pMsg->m_60AntennaHdr.m_nStatus )
	{
		case -2:	strAntennaStatus = strStatus2;		break;
		case -1:	strAntennaStatus = strStatus1;		break;
		case  0:	strAntennaStatus = strStatus0;		break;
		default:	strAntennaStatus = _T("Unknown");	break;
	}

	int		nOATSSiteDef= GetAppRegSite()->m_nDefaultOATS;
	int		nOATSDef	= GetAppRegOATS()->m_nDefault[nOATSSiteDef];
	CString strText;
	CString	strNode(GetAppRegOATS()->m_strAbbrev[nOATSSiteDef][nOATSDef]);
	CString	strName(GetAppRegOATS()->m_strTitle[nOATSSiteDef][nOATSDef]);
	dataFile.AppendText(_T("Sensor Intrusion Report"));
	dataFile.AppendText(_T(""));
	strText.Format(_T("Generation Date:                    %s"), strGenTime);
	dataFile.AppendText(strText);
	strText.Format(_T("Requested Start Time of Prediction: %s"), m_strStartTime);
	dataFile.AppendText(strText);
	strText.Format(_T("Requested Prediction Duration:      %d(days)"), m_nDuration);
	dataFile.AppendText(strText);
	strText.Format(_T("Spacecraft:                         %s"),
		GetAppRegSC()->m_strTitle[GetAppRegSite()->m_nDefaultSC]);
	dataFile.AppendText(strText);
	strText.Format(_T("OATS:                               %s (%s)"), strName, strNode);
	dataFile.AppendText(strText);
	strText.Format(_T("Imager Status:                      %s"), strImagerStatus);
	dataFile.AppendText(strText);
	strText.Format(_T("Sounder Status:                     %s"), strSounderStatus);
	dataFile.AppendText(strText);
	strText.Format(_T("Antenna Status:                     %s"), strAntennaStatus);
	dataFile.AppendText(strText);
	dataFile.AppendText(_T(""));
	strText.Format(_T("%-12.12s  %-14.14s  %-4.4s  %-21.21s  %-21.21s"),
		_T("Sensor"), _T("Intruding Body"), _T("Edge"), _T("Start Time"), _T("Stop Time"));
	dataFile.AppendText(strText);
	strText.Format(_T("%-12.12s  %-14.14s  %-4.4s  %-21.21s  %-21.21s"),
		_T("------------"), _T("--------------"), _T("----"),
		_T("---------------------"), _T("---------------------"));
	dataFile.AppendText(strText);

	CString strIntrudingBody;
	CString strIntrudingID;
	CString strStartTime;
	CString strStopTime;

	if( pMsg->m_60ImagerHdr.m_nNumIntrusions == 0 &&
		pMsg->m_60SounderHdr.m_nNumIntrusions== 0 &&
		pMsg->m_60AntennaHdr.m_nNumIntrusions== 0 )
	{
		strMsg = _T("OATS returned no Sensor Intrusion data!");
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		dataFile.Close(CTextFile::NOWRITE, &m_fileException);
		DeleteFile(m_strRptFile);
		return FALSE;
	}

	for( int nIndex=0; nIndex<pMsg->m_60ImagerHdr.m_nNumIntrusions; nIndex++ )
	{
		switch( pMsg->m_60ImagerData[nIndex].m_nIntrudingBody )
		{
			case 1:		strIntrudingBody = _T("Sun");		break;
			case 2:		strIntrudingBody = _T("Moon");		break;
			default:	strIntrudingBody = _T("Unknown");	break;
		}

		switch( pMsg->m_60ImagerData[nIndex].m_nIntrudingID )
		{
			case 1:		strIntrudingID = _T("East");		break;
			case 2:		strIntrudingID = _T("West");		break;
			default:	strIntrudingID = _T("Unknown");		break;
		}

		ConvertBCDToStringTime(pMsg->m_60ImagerData[nIndex].m_nStartTime, &strStartTime);
		ConvertBCDToStringTime(pMsg->m_60ImagerData[nIndex].m_nStopTime, &strStopTime);
		strText.Format(_T("%-12.12s  %-14.14s  %-4.4s  %-21.21s  %-21.21s"),
			_T("Imager"), strIntrudingBody, strIntrudingID, strStartTime, strStopTime);
		dataFile.AppendText(strText);
	}

	for(int nIndex=0; nIndex<pMsg->m_60SounderHdr.m_nNumIntrusions; nIndex++ )
	{
		switch( pMsg->m_60SounderData[nIndex].m_nIntrudingBody )
		{
			case 1:		strIntrudingBody = _T("Sun");		break;
			case 2:		strIntrudingBody = _T("Moon");		break;
			default:	strIntrudingBody = _T("Unknown");	break;
		}

		switch( pMsg->m_60SounderData[nIndex].m_nIntrudingID )
		{
			case 1:		strIntrudingID = _T("East");		break;
			case 2:		strIntrudingID = _T("West");		break;
			default:	strIntrudingID = _T("Unknown");		break;
		}

		ConvertBCDToStringTime(pMsg->m_60SounderData[nIndex].m_nStartTime, &strStartTime);
		ConvertBCDToStringTime(pMsg->m_60SounderData[nIndex].m_nStopTime, &strStopTime);
		strText.Format(_T("%-12.12s  %-14.14s  %-4.4s  %-21.21s  %-21.21s"),
			_T("Sounder"), strIntrudingBody, strIntrudingID, strStartTime, strStopTime);
		dataFile.AppendText(strText);
	}

	for(int nIndex=0; nIndex<pMsg->m_60AntennaHdr.m_nNumIntrusions; nIndex++ )
	{
		CString strAntenna;
		strAntenna.Format(_T("Antenna-%2d"), pMsg->m_60AntennaData[nIndex].m_nIntrudingBody);
		ConvertBCDToStringTime(pMsg->m_60AntennaData[nIndex].m_nStartTime, &strStartTime);
		ConvertBCDToStringTime(pMsg->m_60AntennaData[nIndex].m_nStopTime, &strStopTime);
		strText.Format(_T("%-12.12s  %-14.14s  %-4.4s  %-21.21s  %-21.21s"),
			strAntenna, _T(""), _T(""), strStartTime, strStopTime);
		dataFile.AppendText(strText);
	}

	// Close the file
	if( !dataFile.Close(CTextFile::WRITE, &m_fileException) )
	{
		strMsg = _T("Error closing file ") + m_strRptFile;
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	strMsg.Format(_T("OATS Sensor Intrusion report file Generation successful - File %s created."), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::FrameIntrusionRpt
//	Description :	FrameIntrusionRpt creates a report from the data
//					supplied from OATS.
//
//	Return :		BOOL	-	TRUE if the report file was created
//								successfully.
//
//	Parameters :	CPtrList* pList	-	list of 50402 messages.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::FrameIntrusionRpt(CPtrList* pList)
{
	CString		strMsg;
	CTextFile	dataFile;
	COXUNC		unc(m_strRptFile);

	strMsg.Format(_T("Generating OATS Frame Intrusion report file %s"), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	if( pList->IsEmpty() )
	{
		strMsg.Format(_T("No Frame Intrusion report data returned from OATS."));
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	POSITION	pos = pList->GetHeadPosition();
	MSG_61*		p61Msg;

	// Open the filename
	if( !dataFile.Load(m_strRptFile, CTextFile::CR, &m_fileException) )
	{
		strMsg = _T("Error opening file ") + m_strRptFile;
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	// Generate the report
	CTime curTime(CTime::GetCurrentTime());
	CString strGenTime = curTime.FormatGmt(_T("%Y-%j-%H:%M:%S.000"));
	CString strStatus0(_T("All intrusions lie within the requested time span.\n"));
	CString strStatus1(_T("Instrusion exist outside of requested time span.\n"));
	CString strStatus2(_T("No intrusions in requested time span."));
	strStatus0 += _T("        Any intrusion in requested range is given.");
	strStatus1 += _T("        Any intrusion in requested range is given.");

	int		nOATSSiteDef= GetAppRegSite()->m_nDefaultOATS;
	int		nOATSDef	= GetAppRegOATS()->m_nDefault[nOATSSiteDef];
	CString strText;
	CString	strNode(GetAppRegOATS()->m_strAbbrev[nOATSSiteDef][nOATSDef]);
	CString	strName(GetAppRegOATS()->m_strTitle[nOATSSiteDef][nOATSDef]);
	dataFile.AppendText(_T("Frame Intrusion Report"));
	dataFile.AppendText(_T(""));
	strText.Format(_T("Generation Date:                    %s"), strGenTime);
	dataFile.AppendText(strText);
	strText.Format(_T("Requested Start Time of Prediction: %s"), m_strStartTime);
	dataFile.AppendText(strText);
	strText.Format(_T("Requested Prediction Duration:      %d(days)"), m_nDuration);
	dataFile.AppendText(strText);
	strText.Format(_T("Spacecraft:                         %s"),
		GetAppRegSC()->m_strTitle[GetAppRegSite()->m_nDefaultSC]);
	dataFile.AppendText(strText);
	strText.Format(_T("OATS:                               %s (%s)"), strName, strNode);
	dataFile.AppendText(strText);
	dataFile.AppendText(_T(""));
	strText.Format(_T("(%s) Frame Status"), _T("*"));
	dataFile.AppendText(strText);
	strText.Format(_T("    -2: %s"), strStatus2);
	dataFile.AppendText(strText);
	strText.Format(_T("    -1: %s"), strStatus1);
	dataFile.AppendText(strText);
	strText.Format(_T("     0: %s"), strStatus0);
	dataFile.AppendText(strText);
	dataFile.AppendText(_T(""));
	strText.Format(_T("%-30.30s  %-16.16s  %-21.21s  %-21.21s"),
		_T("Frame"), _T("Frame Status (*)"), _T("Start Time"), _T("Stop Time"));
	dataFile.AppendText(strText);
	strText.Format(_T("%-30.30s  %-16.16s  %-21.21s  %-21.21s"),
		_T("------------------------------"), _T("----------------"),
		_T("---------------------"), _T("---------------------"));
	dataFile.AppendText(strText);

	CString strFrameStatus;
	CString strStartTime;
	CString strStopTime;

	while( pos != NULL )
	{
		p61Msg = (MSG_61*)pList->GetNext(pos);

		for( int i=0; i<(int)p61Msg->m_61Hdr.m_nNumFrames; i++ )
		{
			CString strFrame	= p61Msg->m_61FrameData[i].m_data.m_cFrameName;
			int		nIntrusions = p61Msg->m_61FrameData[i].m_data.m_nNumIntrusions;
			int		nStatus		= p61Msg->m_61FrameData[i].m_data.m_nStatus;
			strText.Format(_T("%-30.30s  %9d"), strFrame, nStatus);
			dataFile.AppendText(strText);

			for( int j=0; j<nIntrusions; j++ )
			{
				ConvertBCDToStringTime (
					p61Msg->m_61FrameData[i].m_61IntrusionData[j].m_data.m_nStartTime, &strStartTime);
				ConvertBCDToStringTime (
					p61Msg->m_61FrameData[i].m_61IntrusionData[j].m_data.m_nStopTime, &strStopTime);
				strText.Format(_T("%-48.48s  %-21.21s  %-21.21s"),
					_T("                                                "),
					strStartTime, strStopTime);
				dataFile.AppendText(strText);
			}

			dataFile.AppendText(_T(""));
		}
	}

	// Close the file
	if( !dataFile.Close(CTextFile::WRITE, &m_fileException) )
	{
		strMsg = _T("Error closing file ") + m_strRptFile;
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	strMsg.Format(_T("OATS Frame Intrusion report file Generation successful - File %s created."), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::EclipseRpt
//	Description :	EclipseRpt creates a eclipse report from the data
//					supplied from OATS.
//
//	Return :		BOOL	-	TRUE if the report file was created
//								successfully.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::EclipseRpt()
{
	CString		strMsg;
	CTextFile	dataFile;
	COXUNC		unc(m_strRptFile);
	MSG_59		msg59;
	MSG_159		msg159;

	strMsg.Format(_T("Generating OATS Eclipse report file %s"), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	msg159.m_hdr.m_nSequence		= 0;
	msg159.m_hdr.m_nSatelliteID		= (unsigned __int8)GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC];
	msg159.m_hdr.m_nSource			= 10;
	msg159.m_hdr.m_nDestination		= 32;
	msg159.m_hdr.m_nNumHalfWords	= MSG_159_DATA_HW_SIZE;
	msg159.m_hdr.m_nMsgType			= 159;
	msg159.m_hdr.m_nMsgSubType		= 0;
	strncpy_s (msg159.m_hdr.m_cTracer, "         ", 10);
	msg159.m_hdr.m_cTracer[9] = ' ';
	msg159.m_hdr.m_nMsgCnt			= 1;
	msg159.m_hdr.m_nMsgEndFlag		= 0xFF;
	msg159.m_hdr.m_nSpare			= 0;
	msg159.m_hdr.m_nRoutingID		= 0;
	msg159.m_159Data.m_fDuration	= (float)(m_nDuration*1440);
	ConvertStringTimeToBCD (m_strStartTime, &msg159.m_159Data.m_nStartTime[0]);

	if( !m_CommUtil.SendMsg(&msg159, 59, &msg59) )
	{
		CString strErr;
		
		strErr = GetLastError()==ERROR_SUCCESS?_T(""):GetLastErrorText();
			
		if( strErr.IsEmpty() )
			strErr = _T("The Eclipse Report was not generated.");
		else
			strErr+= _T(" The Eclipse Report was not generated.");
			
		strMsg.Format(IDS_UPDATE_WRITE_ERROR, strErr);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	if( !dataFile.Load(m_strRptFile, CTextFile::CR, &m_fileException) )
	{
		strMsg = dataFile.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	// Generate the report
	CTime curTime(CTime::GetCurrentTime());
	CString strGenTime = curTime.FormatGmt(_T("%Y-%j-%H:%M:%S.000"));
	CString strStatus0(_T("Request within range of operational support. All events in\n"));
	CString strStatus1(_T("Request extends beyond range of operational support. All\n"));
	CString strStatus2(_T("Unable to respond to a request outside the bounds or \n"));
	strStatus0 += _T("                                    requested range are given.");
	strStatus1 += _T("                                    events in requested range and in range of operational support\n");
	strStatus1 += _T("                                    are listed below.");
	strStatus2 += _T("                                    operational support.");

	CString strImagerStatus;
	CString strSounderStatus;
	CString strPredictionStatus;

	switch( msg59.m_59Hdr.m_nStatus )
	{
		case -2:	strPredictionStatus = strStatus2;		break;
		case -1:	strPredictionStatus = strStatus1;		break;
		case  0:	strPredictionStatus = strStatus0;		break;
		default:	strPredictionStatus = _T("Unknown");	break;
	}

	int		nOATSSiteDef= GetAppRegSite()->m_nDefaultOATS;
	int		nOATSDef	= GetAppRegOATS()->m_nDefault[nOATSSiteDef];
	CString strText;
	CString	strNode(GetAppRegOATS()->m_strAbbrev[nOATSSiteDef][nOATSDef]);
	CString	strName(GetAppRegOATS()->m_strTitle[nOATSSiteDef][nOATSDef]);
	dataFile.AppendText(_T("Eclipse Intrusion Report"));
	dataFile.AppendText(_T(""));
	strText.Format(_T("Generation Date:                    %s"), strGenTime);
	dataFile.AppendText(strText);
	strText.Format(_T("Requested Start Time of Prediction: %s"), m_strStartTime);
	dataFile.AppendText(strText);
	strText.Format(_T("Requested Prediction Duration:      %d(days)"), m_nDuration);
	dataFile.AppendText(strText);
	strText.Format(_T("Spacecraft:                         %s"),
		GetAppRegSC()->m_strTitle[GetAppRegSite()->m_nDefaultSC]);
	dataFile.AppendText(strText);
	strText.Format(_T("OATS:                               %s (%s)"), strName, strNode);
	dataFile.AppendText(strText);
	strText.Format(_T("Number of Events:                   %d"), msg59.m_59Hdr.m_nNumEvents);
	dataFile.AppendText(strText);
	strText.Format(_T("Status:                             %s"), strPredictionStatus);
	dataFile.AppendText(strText);
	dataFile.AppendText(_T(""));
	strText.Format(_T("%-15.15s  %-21.21s  %-21.21s  %-21.21s  %-21.21s"),
		_T("Intruding Body"), _T("Penumbral Entrance"), _T("Umbral Entrance"), _T("Umbral Exit"), _T("Penumbral Exit"));
	dataFile.AppendText(strText);
	strText.Format(_T("%-15.15s  %-21.21s  %-21.21s  %-21.21s  %-21.21s"),
		_T("---------------"), _T("---------------------"), _T("---------------------"),_T("---------------------"),_T("---------------------"));
	dataFile.AppendText(strText);

	CString strIntrudingBody;
	CString strPrenumbralEntrance;
	CString strUmbralEntrance;
	CString strUmbralExit;
	CString strPrenumbralExit;
	CString strStopTime;

	for( int nIndex=0; nIndex<msg59.m_59Hdr.m_nNumEvents; nIndex++ )
	{
		switch( msg59.m_59Data[nIndex].m_nIntrudingBody )
		{
			case 1:		strIntrudingBody = _T("Earth");		break;
			case 2:		strIntrudingBody = _T("Moon");		break;
			default:	strIntrudingBody = _T("Unknown");	break;
		}

		ConvertBCDToStringTime (msg59.m_59Data[nIndex].m_nPenumbralEntranceTime, &strPrenumbralEntrance);
		ConvertBCDToStringTime (msg59.m_59Data[nIndex].m_nUmbralEntranceTime, &strUmbralEntrance);
		ConvertBCDToStringTime (msg59.m_59Data[nIndex].m_nUmbralExitTime, &strUmbralExit);
		ConvertBCDToStringTime (msg59.m_59Data[nIndex].m_nPenumbralExitTime, &strPrenumbralExit);
		strText.Format(_T("%-15.15s  %-21.21s  %-21.21s  %-21.21s  %-21.21s"),
		strIntrudingBody, strPrenumbralEntrance, strUmbralEntrance, strUmbralExit, strPrenumbralExit);
		dataFile.AppendText(strText);
	}

	// Close the file
	if( !dataFile.Close(CTextFile::WRITE, &m_fileException) )
	{
		strMsg = dataFile.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	strMsg.Format(_T("OATS Eclipse report file Generation successful - File %s created."), unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::IMCRpt
//	Description :	IMCRpt creates a IMC report from the data
//					supplied from OATS.
//
//	Return :		BOOL	-	TRUE if the report file was created
//								successfully.
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::IMCRpt()
{
	CString 	strMsg;
	CTextFile	dataFile;
	COXUNC		unc(m_strRptFile);
	MSG_153		msg153;
	MSG_53		msg53;

	strMsg.Format(_T("Generating OATS IMC report file %s"),unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	msg153.m_hdr.m_nSequence		= 0;
	msg153.m_hdr.m_nSatelliteID		= (unsigned __int8)GetAppRegSC()->m_nID[GetAppRegSite()->m_nDefaultSC];
	msg153.m_hdr.m_nSource			= 10;
	msg153.m_hdr.m_nDestination		= 32;
	msg153.m_hdr.m_nNumHalfWords	= MSG_153_DATA_HW_SIZE;
	msg153.m_hdr.m_nMsgType			= 153;
	msg153.m_hdr.m_nMsgSubType		= 1;
	strncpy_s (msg153.m_hdr.m_cTracer, "         ", 10);
	msg153.m_hdr.m_cTracer[9] = ' ';
	msg153.m_hdr.m_nMsgCnt			= 1;
	msg153.m_hdr.m_nMsgEndFlag		= 0xFF;
	msg153.m_hdr.m_nSpare			= 0;
	msg153.m_hdr.m_nRoutingID		= 0;
	msg153.m_153Data.m_cIMCSet[0]	= (char)m_strIMCSet.GetAt(0);
	msg153.m_153Data.m_cIMCSet[1]	= (char)m_strIMCSet.GetAt(1);
	msg153.m_153Data.m_cIMCSet[2]	= (char)m_strIMCSet.GetAt(2);
	msg153.m_153Data.m_cIMCSet[3]	= (char)m_strIMCSet.GetAt(3);

	if( !m_CommUtil.SendMsg(&msg153, 53, &msg53) )
	{
		CString strErr;
		
		strErr = GetLastError()==ERROR_SUCCESS?_T(""):GetLastErrorText();

		if( strErr.IsEmpty() )
			strErr = _T("The IMC Report was not generated.");
		else
			strErr+= _T(" The IMC Report was not generated.");
			
		strMsg.Format(IDS_UPDATE_WRITE_ERROR, strErr);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	// Open the filename
	if( !dataFile.Load(m_strRptFile, CTextFile::CR, &m_fileException) )
	{
		strMsg = dataFile.ExceptionErrorText(&m_fileException);
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	// Generate the report
	double			dTemp			= 0.0;
	unsigned short 	pnFL1750[2]		= {0,0};
	unsigned short 	pnEFL1750[3]	= {0,0,0};
	int				nIndex			= 0;
	int				nSetID			= 0;
	CString			strSetName[10]	=
					{
						_T("Imager Roll"),
						_T("Imager Pitch"),
						_T("Imager Yaw"),
						_T("Imager Misalignment (psi)"),	// (psi)
						_T("Imager Misalignment (theta)"),	// (theta)
						_T("Sounder Roll"),
						_T("Sounder Pitch"),
						_T("Sounder Yaw"),
						_T("Sounder Misalignment (psi)"),	// (psi)
						_T("Sounder Misalignment (theta)")	// (theta)
					};

	int		nOATSSiteDef= GetAppRegSite()->m_nDefaultOATS;
	int		nOATSDef	= GetAppRegOATS()->m_nDefault[nOATSSiteDef];
	CString	strNode(GetAppRegOATS()->m_strAbbrev[nOATSSiteDef][nOATSDef]);
	CString	strName(GetAppRegOATS()->m_strTitle[nOATSSiteDef][nOATSDef]);
	CString strText;
	CTime	curTime(CTime::GetCurrentTime());
	CString strGenTime(curTime.FormatGmt(_T("%Y-%j-%H:%M:%S.000")));
	CString strIMCSet(msg53.m_53Data.m_cIMCSet, 4);
	dataFile.AppendText(_T("C CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"));
	dataFile.AppendText(_T("C"));
	dataFile.AppendText(_T("C IMC Requested Data"));
	dataFile.AppendText(_T("C"));
	strText.Format(_T("C Generation Date:  %s"), strGenTime);
	dataFile.AppendText(strText);
	strText.Format(_T("C Spacecraft:       %s"), GetAppRegSC()->m_strTitle[GetAppRegSite()->m_nDefaultSC]);
	dataFile.AppendText(strText);
	strText.Format(_T("C OATS:             %s (%s)"), strName, strNode);
	dataFile.AppendText(strText);
	strText.Format(_T("C IMC Set ID:       %s"), (LPCTSTR)strIMCSet);
	dataFile.AppendText(strText);
	dataFile.AppendText(_T("C"));
	dataFile.AppendText(_T("C CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"));
	unsigned short int nIMCNum	= (unsigned __int8)_ttoi(strIMCSet.Right(3));
	unsigned short int nIMCAlpha= (strIMCSet[0] - 'A') + 1;
	unsigned short int nIMCSetId= 0;
	nIMCSetId	= nIMCAlpha << 10;
	nIMCSetId  |= nIMCNum;
	dataFile.AppendText(_T("C"));
	dataFile.AppendText(_T("BLOCK_NUM 1"));
	// This is a change for build 12.  This changes the IMC type to a message 62 IMC-subtype. 
	strText.Format(_T("IMC_TYPE %d"), (LPCTSTR)(msg53.m_53Data.m_nIMCType));
	dataFile.AppendText(strText);
	// Child type is the old IMC type.  
	strText.Format(_T("CHILD_TYPE %d"), (LPCTSTR)(msg53.m_hdr.m_nMsgSubType));
	dataFile.AppendText(strText);
	strText.Format(_T("IMC_DYNRNG %d"), msg53.m_53Data.m_nDynamicRangeFlag);
	dataFile.AppendText(strText);
	// Subtract off first six half words not used by spacecraft.
	// Ninety-two values are long ints, so need to also subtract 92.
	int nLength = msg53.m_hdr.m_nNumHalfWords - 98;
	strText.Format(_T("LENGTH %d"), nLength);
	dataFile.AppendText(strText);
	strText.Format(_T("DATA %04.4hX IMC set identifier (%s)"), nIMCSetId, (LPCTSTR)strIMCSet);
	dataFile.AppendText(strText);
	short int nYawFlipFlag = 1;

	nYawFlipFlag = (msg53.m_53Data.m_nYawFlipFlag >= 0 ? 0 : 1);

	strText.Format(_T("DATA %04.4hX Yaw flip flag (%d)"), nYawFlipFlag, nYawFlipFlag);
	dataFile.AppendText(strText);
	strText.Format(_T("DATA %04.4hX Epoch day as referenced in JD2000 (%d)"),
		msg53.m_53Data.m_nEpochDay, msg53.m_53Data.m_nEpochDay);
	dataFile.AppendText(strText);
	dTemp = msg53.m_53Data.m_fTimeOfDay;
	DoubleToEFL1750(dTemp, pnEFL1750);
	strText.Format(_T("DATA %04.4hX Epoch time-of-day (%.6e)"), pnEFL1750[0], msg53.m_53Data.m_fTimeOfDay);
	dataFile.AppendText(strText);
	strText.Format(_T("DATA %04.4hX"), pnEFL1750[1]);
	dataFile.AppendText(strText);
	strText.Format(_T("DATA %04.4hX"), pnEFL1750[2]);
	dataFile.AppendText(strText);
	dTemp = msg53.m_53Data.m_fFundFourierFreq;
	DoubleToFL1750(dTemp, pnFL1750);
	strText.Format(_T("DATA %04.4hX Corrected on-board daily fundamental frequency (%.6e)"),
		pnFL1750[0], msg53.m_53Data.m_fFundFourierFreq);
	dataFile.AppendText(strText);
	strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
	dataFile.AppendText(strText);
	dTemp = msg53.m_53Data.m_fNominalEarthRate;
	DoubleToFL1750(dTemp, pnFL1750);
	strText.Format(_T("DATA %04.4hX Corrected nominal earth sidereal rate (%.6e)"),
		pnFL1750[0], msg53.m_53Data.m_fNominalEarthRate);
	dataFile.AppendText(strText);
	strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
	dataFile.AppendText(strText);

	for( nIndex = 0; nIndex<24; nIndex++ )
	{
		dTemp = msg53.m_53Data.m_fOrbitalCoefficients[nIndex];
		DoubleToFL1750(dTemp, pnFL1750);
		strText.Format(_T("DATA %04.4hX Orbital computation coefficient %d (%.6e)"),
			pnFL1750[0], nIndex + 1, msg53.m_53Data.m_fOrbitalCoefficients[nIndex]);
		dataFile.AppendText(strText);
		strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
		dataFile.AppendText(strText);
	}

	dTemp = msg53.m_53Data.m_fLongitudeReference;
	DoubleToFL1750(dTemp, pnFL1750);
	strText.Format(_T("DATA %04.4hX Spacecraft longitude reference (%.6e)"),
		pnFL1750[0], msg53.m_53Data.m_fLongitudeReference);
	dataFile.AppendText(strText);
	strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
	dataFile.AppendText(strText);
	dTemp = msg53.m_53Data.m_fGreenwichHourAngle;
	DoubleToFL1750(dTemp, pnFL1750);
	strText.Format(_T("DATA %04.4hX True Greenwich hour angle (%.6e)"),
		pnFL1750[0], msg53.m_53Data.m_fGreenwichHourAngle);
	dataFile.AppendText(strText);
	strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
	dataFile.AppendText(strText);
	dTemp = msg53.m_53Data.m_fNominalGeosynchRadius;
	DoubleToFL1750(dTemp, pnFL1750);
	strText.Format(_T("DATA %04.4hX Nominal orbit radius (%.6e)"), pnFL1750[0], msg53.m_53Data.m_fNominalGeosynchRadius);
	dataFile.AppendText(strText);
	strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
	dataFile.AppendText(strText);

	for( nIndex=0; nIndex<4; nIndex++ )
	{
		dTemp = msg53.m_53Data.m_fPresessionNutation[nIndex];
		DoubleToFL1750(dTemp, pnFL1750);
		strText.Format(_T("DATA %04.4hX True ECI to ECI quaternions %d (%.6e)"),
			pnFL1750[0], nIndex + 1, msg53.m_53Data.m_fPresessionNutation[nIndex]);
		dataFile.AppendText(strText);
		strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
		dataFile.AppendText(strText);
	}

	dTemp = msg53.m_53Data.m_fExpStartTime;
	DoubleToFL1750(dTemp, pnFL1750);
	strText.Format(_T("DATA %04.4hX Exponential start time from IMC epoch (%.6e)"), pnFL1750[0],  msg53.m_53Data.m_fExpStartTime);
	dataFile.AppendText(strText);
	strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
	dataFile.AppendText(strText);

	for( nSetID=0; nSetID<10; nSetID++ )
	{
		dTemp = msg53.m_53IMCData[nSetID].m_fExpMag;
		DoubleToFL1750(dTemp, pnFL1750);
		strText.Format(_T("DATA %04.4hX %s exponential magnitude (%.6e)"),
			pnFL1750[0], strSetName[nSetID], msg53.m_53IMCData[nSetID].m_fExpMag);
		dataFile.AppendText(strText);
		strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
		dataFile.AppendText(strText);
		dTemp = msg53.m_53IMCData[nSetID].m_fExpTimeConstant;
		DoubleToFL1750(dTemp, pnFL1750);
		strText.Format(_T("DATA %04.4hX %s exponential time constant (%.6e)"),
			pnFL1750[0], strSetName[nSetID], msg53.m_53IMCData[nSetID].m_fExpTimeConstant);
		dataFile.AppendText(strText);
		strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
		dataFile.AppendText(strText);
		dTemp = msg53.m_53IMCData[nSetID].m_fMagZeroOrder;
		DoubleToFL1750(dTemp, pnFL1750);
		strText.Format(_T("DATA %04.4hX %s attitude 0-th order coefficient (%.6e)"),
			pnFL1750[0], strSetName[nSetID], msg53.m_53IMCData[nSetID].m_fMagZeroOrder);
		dataFile.AppendText(strText);
		strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
		dataFile.AppendText(strText);

		for( nIndex=0; nIndex<15; nIndex++ )
		{
			dTemp = msg53.m_53IMCData[nSetID].m_fMagSine[nIndex];
			DoubleToFL1750(dTemp, pnFL1750);
			strText.Format(_T("DATA %04.4hX %s attitude sine coefficients %d (%.6e)"),
				pnFL1750[0], strSetName[nSetID], nIndex + 1 ,msg53.m_53IMCData[nSetID].m_fMagSine[nIndex]);
			dataFile.AppendText(strText);
			strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
			dataFile.AppendText(strText);
		}

		for( nIndex=0; nIndex<15; nIndex++ )
		{
			dTemp = msg53.m_53IMCData[nSetID].m_fMagCosine[nIndex];
			DoubleToFL1750(dTemp, pnFL1750);
			strText.Format(_T("DATA %04.4hX %s attitude cosine coefficients %d (%.6e)"),
				pnFL1750[0], strSetName[nSetID], nIndex + 1, msg53.m_53IMCData[nSetID].m_fMagCosine[nIndex]);
			dataFile.AppendText(strText);
			strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
			dataFile.AppendText(strText);
		}

		strText.Format(_T("DATA %04.4hX %s number of monomial sinusoids (%d)"),
			msg53.m_53IMCData[nSetID].m_nNumMonomialSinusoids, strSetName[nSetID], msg53.m_53IMCData[nSetID].m_nNumMonomialSinusoids);
		dataFile.AppendText(strText);

		for( nIndex=0; nIndex<4; nIndex++ )
		{
			strText.Format(_T("DATA %04.4hX %s order of sinusoid (%d)"),
				msg53.m_53IMCData[nSetID].m_nOrderSinusoid[nIndex], strSetName[nSetID], nIndex + 1, msg53.m_53IMCData[nSetID].m_nOrderSinusoid[nIndex]);
			dataFile.AppendText(strText);
		}

		for( nIndex=0; nIndex<4; nIndex++ )
		{
			strText.Format(_T("DATA %04.4hX %s order of monomial (%d)"),
				msg53.m_53IMCData[nSetID].m_nOrderMonomial[nIndex], strSetName[nSetID], nIndex + 1, msg53.m_53IMCData[nSetID].m_nOrderMonomial[nIndex]);
			dataFile.AppendText(strText);
		}

		for( nIndex=0; nIndex<4; nIndex++ )
		{
			dTemp = msg53.m_53IMCData[nSetID].m_fMagMonomialSinusoid[nIndex];
			DoubleToFL1750(dTemp, pnFL1750);
			strText.Format(_T("DATA %04.4hX %s magnitude of monomial sinusoid (%.6e)"),
				pnFL1750[0], strSetName[nSetID], nIndex + 1, msg53.m_53IMCData[nSetID].m_fMagMonomialSinusoid[nIndex]);
			dataFile.AppendText(strText);
			strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
			dataFile.AppendText(strText);
		}

		for( nIndex=0; nIndex<4; nIndex++ )
		{
			dTemp = msg53.m_53IMCData[nSetID].m_fPhaseAngleSinusoid[nIndex];
			DoubleToFL1750(dTemp, pnFL1750);
			strText.Format(_T("DATA %04.4hX %s phase angle of sinusoid (%.6e)"),
				pnFL1750[0], strSetName[nSetID], nIndex + 1, msg53.m_53IMCData[nSetID].m_fPhaseAngleSinusoid[nIndex]);
			dataFile.AppendText(strText);
			strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
			dataFile.AppendText(strText);
		}

		for( int nIndex=0; nIndex<4; nIndex++ )
		{
			dTemp = msg53.m_53IMCData[nSetID].m_fAngleFromZero[nIndex];
			DoubleToFL1750(dTemp, pnFL1750);
			strText.Format(_T("DATA %04.4hX %s angle from epoch where monomial is zero (%.6e)"),
				pnFL1750[0], strSetName[nSetID], nIndex + 1, msg53.m_53IMCData[nSetID].m_fAngleFromZero[nIndex]);
			dataFile.AppendText(strText);
			strText.Format(_T("DATA %04.4hX"), pnFL1750[1]);
			dataFile.AppendText(strText);
		}

		dataFile.AppendText(_T("C"));
		dataFile.AppendText(_T("C End of file"));
	}

	// Close the file
	if( !dataFile.Close(CTextFile::WRITE, &m_fileException) )
	{
		CString strMsg(dataFile.ExceptionErrorText(&m_fileException));
		GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
		return FALSE;
	}

	strMsg.Format(_T("OATS IMC report file Generation successful - File %s created."),unc.File());
	GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::INFO);

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::OutputStatus
//	Description :	OutputStatus generates a status message from the
//					50402-55 message output status value supplied
//					from OATS.
//
//	Return :		BOOL	-	TRUE if the output status value is 0.
//
//	Parameters :
//		CString strTime		-	time of the scan.
//		CString strLabel	-	label name of a frame.
//		CString strSide		-	space look side of a frame.
//		int nStatus			-	output status value.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::OutputStatus(CString strTime, CString strInst, CString strSide, int nStatus)
{
	CString strMsg;
	CString strScan;

	strScan.Format(_T("SCAN FRAME: %s %s %s :"), strTime, strInst, strSide);

	switch( nStatus )
	{
		case 0:
			break;

		case 1:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS1);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::WARNING);
			break;
		}

		case 2:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS2);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::WARNING);
			break;
		}

		case 3:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS3);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::FATAL);
			strMsg = _T("The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		case 4:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS4);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::FATAL);
			strMsg = _T("The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		case 5:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS5);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::FATAL);
			strMsg = _T("The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		case 6:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS6);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::FATAL);
			strMsg = _T("The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		case 7:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS7);
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::FATAL);
			strScan += strMsg;
			strMsg = _T("The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		case 8:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS8);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::FATAL);
			strMsg = _T("The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		case 9:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS9);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::FATAL);
			strMsg = _T("The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		case 10:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS10);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::FATAL);
			strMsg = _T("The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		case 11:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS11);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::FATAL);
			strMsg = _T("The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}

		case 12:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS12);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::WARNING);
			break;
		}

		case 13:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS13);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::WARNING);
			break;
		}

		case 14:
		{
			strMsg.Format(IDS_MSG55_OUTPUT_STATUS14);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::WARNING);
			break;
		}

		case 15:
		{
			//strMsg.Format(IDS_MSG55_OUTPUT_STATUS15);
			strMsg = _T("Sun Intrusion into frame");
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::WARNING);
			break;
		}

		case 16:
		{
			//strMsg.Format(IDS_MSG55_OUTPUT_STATUS16);
			strMsg = _T("Frame scan period entirely within eclipse umbra");
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::WARNING);
			break;
		}

		default:
		{
			strMsg.Format(IDS_UPDATE_STARS_FRAMES,_T("output status"));
			strMsg += _T(" The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::CriticalLimit
//	Description :	CriticalLimit generates a status message from the
//					50402-55 message critical limit value supplied
//					from OATS.
//
//	Return :		BOOL	-	TRUE if the critical limit value is 0.
//
//	Parameters :
//		CString strTime		-	time of the scan.
//		CString strInst		-	instrument type.
//		CString strSide		-	space look side of a frame.
//		int nCriticalLimit	-	critical limit value.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::CriticalLimit(CString strTime,CString strInst,CString strSide,int nCriticalLimit)
{
	CString strMsg;
	CString strScan;

	strScan.Format(_T("SCAN FRAME: %s %s %s :"), strTime, strInst, strSide);

	switch( nCriticalLimit )
	{
		case 0:
			break;
		case 1:
		case 2:
		case 3:
		case 4:
		{
			strMsg.Format(IDS_MSG55_CRITICAL_LIMITS1);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::WARNING);
			break;
		}

		case 5:
		{
			strMsg.Format(IDS_MSG55_CRITICAL_LIMITS2);
			strScan += strMsg;
			GetAppOutputBar()->OutputToEvents(strScan, COutputBar::WARNING);
			break;
		}

		default:
		{
			strMsg.Format(IDS_UPDATE_STARS_FRAMES,_T("critical limit indication"));
			strMsg += _T(" The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::FATAL);
			return FALSE;
		}
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::ValidateSfMsg
//	Description :	ValidateSfMsg checks some of the engineering
//					constriants before creating SFCS minischedule.
//
//	Return :		BOOL	-	TRUE if no constraint is violated.
//
//	Parameters :
//		MSG_75_LOOK_DATA* pData	-	pointer to 50402-75 message.
//		int nInstr				-	instrument type.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CSchedUpdUtil::ValidateSfMsg(MSG_75_LOOK_DATA* pData, int nInstr)
{
	CString strErr;

	if( pData->m_nNSCyc < 0 || pData->m_nNSCyc > ctnMaxNSCycles )
	{
		strErr.Format(IDS_UPDATE_STARS_FRAMES,_T("star ns cycles"));
		GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
		return FALSE;
	}

	if( pData->m_nEWCyc < 0 || pData->m_nEWCyc > ctnMaxEWCycles )
	{
		strErr.Format(IDS_UPDATE_STARS_FRAMES,_T("star ew cycles"));
		GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
		return FALSE;
	}

	if( nInstr == 1 )
	{
		if( pData->m_nDwellRepeats < 0 || pData->m_nDwellRepeats > ctnMaxImgDwellRepeats )
		{
			strErr.Format(IDS_UPDATE_STARS_FRAMES,_T("look dwell repeats"));
			strErr += _T(" The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
			return FALSE;
		}

		if( pData->m_nNSInc < 0 || pData->m_nNSInc > ctnMaxImgIncrements )
		{
			strErr.Format(IDS_UPDATE_STARS_FRAMES,_T("star ns increments"));
			strErr += _T(" The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
			return FALSE;
		}

		if( pData->m_nEWInc < 0 || pData->m_nEWInc > ctnMaxImgIncrements )
		{
			strErr.Format(IDS_UPDATE_STARS_FRAMES,_T("star ew increments"));
			strErr += _T(" The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
			return FALSE;
		}
	}
	else if( nInstr == 2 )
	{
		if( pData->m_nDwellRepeats < 0 || pData->m_nDwellRepeats > ctnMaxSndDwellRepeats )
		{
			strErr.Format(IDS_UPDATE_STARS_FRAMES,_T("look dwell repeats"));
			strErr += _T(" The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
			return FALSE;
		}

		if( pData->m_nNSInc < 0 || pData->m_nNSInc > ctnMaxSndIncrements )
		{
			strErr.Format(IDS_UPDATE_STARS_FRAMES,_T("star ns increments"));
			strErr += _T(" The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
			return FALSE;
		}

		if( pData->m_nEWInc < 0 || pData->m_nEWInc > ctnMaxSndIncrements )
		{
			strErr.Format(IDS_UPDATE_STARS_FRAMES,_T("star ew increments"));
			strErr += _T(" The schedule file was not updated.");
			GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
			return FALSE;
		}
	}
	else
	{
		strErr.Format(IDS_UPDATE_STARS_FRAMES,_T("message subtype"));
		strErr += _T(" The schedule file was not updated.");
		GetAppOutputBar()->OutputToEvents(strErr, COutputBar::FATAL);
		return FALSE;
	}

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::FindInstrIndex
//	Description :	FindInstrIndex finds the first index of an
//					instrument in the in-memory star data.
//
//	Return :		BOOL	-	index of instrument.
//
//	Parameters :
//		CString strTok		-	name of instrument.
//		int* nWinNum		-	pointer to star window number.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
int CSchedUpdUtil::FindInstrIndex(CString strTok, int* nWinNum)
{
	CString tok;
	CString strWin;

	int nCount = m_strStarData[eStarLookNum].GetSize();

	for( int i=0; i<nCount; i++ )
	{
		tok		= m_strStarData[eStarInstrument].GetAt(i);
		strWin	= m_strStarData[eStarWindowNum].GetAt(i);

		if( tok.CompareNoCase(strTok) == 0 )
		{
			*nWinNum = _ttoi(strWin);
			return i;
		}
	}

	return -1;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::RemoveFirstToken
//	Description :	RemoveFirstToken removes the first token off of a
//					list of comma delimited tokens. To cleans things
//					up, the original string will be left trimmed and
//					any double quotes will be removed.
//
//	Return :		CString	-	first token off list.
//
//	Parameters :
//		CString *pstrInput	-	pointer to list of tokens.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CSchedUpdUtil::RemoveFirstToken(CString *pstrInput)
{
	CString strToken;

	pstrInput->TrimLeft();
	pstrInput->Remove('"');

	int nPos = pstrInput->Find(',');

	if( nPos == -1 )
	{
		strToken = *pstrInput;
		*pstrInput = "";
	}
	else
	{
		strToken = pstrInput->Left(nPos);
		*pstrInput = pstrInput->Right(pstrInput->GetLength() - nPos - 1);
		strToken.TrimRight();
	}

	return strToken;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::DeleteListElements
//	Description :	DeleteListElements is a clean up routine to delete
//					unused memory space.
//
//	Return :		void	-
//
//	Parameters :
//		CPtrList* list		-	pointer to a list of objects.
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::DeleteListElements(CPtrList* list)
{
	if( list->IsEmpty()){
		// TRACE0 ("DeleteListElements:List empty returning.\n");
		return;
	}

	POSITION pos = list->GetHeadPosition();

	while( pos != NULL )
		delete list->GetNext(pos);

	list->RemoveAll();

	// TRACE0 ("DeleteListElements:executed module to completion.\n");
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::GetLastErrorText
//	Description :	GetLastErrorText
//
//	Return :		CString	-
//
//	Parameters :
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CString CSchedUpdUtil::GetLastErrorText()
{
	LPVOID lpMsgBuf;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				  FORMAT_MESSAGE_FROM_SYSTEM |
				  FORMAT_MESSAGE_IGNORE_INSERTS,
				  NULL,
				  GetLastError(),
				  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				  (LPTSTR) &lpMsgBuf,
				  0,
				  NULL);
	CString strMsg((LPTSTR)lpMsgBuf);
	strMsg.Remove(_T('\x0D'));
	strMsg.Remove(_T('\x0A'));
	LocalFree(lpMsgBuf);
	return strMsg;
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::DoubleToFL1750
//	Description :	DoubleToFL1750
//
//	Return :		void	-
//
//	Parameters :
//		double source				-	original value
//		unsigned short *destination	-	converted value
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::DoubleToFL1750(double source, unsigned short *destination)
{
	double			d			= 0.0;
	int				exp			= 0;
	int				sign		= 0;
	unsigned long	mantissa	= 0;
	unsigned long	lmantissa	= 0;
	short			aexp		= 0;

	if( source != 0.0 )
	{
		sign = (source < 0.0);

		if( sign )
			source = -source;

		source		= frexp(source, &exp);
		mantissa	= ((unsigned long) ldexp (source, 31));
		source		= ldexp(source, 23);
		source		= modf(source, &d);
		lmantissa	= ((unsigned long) ldexp (source, 16));

		if( sign )
		{
			mantissa	= ~mantissa;
			lmantissa	= (~lmantissa & 0xFFFF) + 1;

			if( lmantissa & 0X10000 )
				mantissa++;
		}

		if( (mantissa & (unsigned long)0xC0000000) == (unsigned long)0xC0000000 )
		{
			mantissa = mantissa >> 7;
			exp--;
		}
		else
			mantissa = mantissa >> 8;

		aexp = (short)exp;
	}

	*destination++ = (unsigned short)((mantissa & 0x00ffff00) >> 8);
	*destination   = (unsigned short)((mantissa & 0x000000ff) << 8);
	*destination  |= (unsigned short)(aexp & 0x000000ff);
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Ray Mosley		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CSchedUpdUtil::DoubleToEFL1750
//	Description :	DoubleToEFL1750
//
//	Return :		void	-
//
//	Parameters :
//		double source				-	original value
//		unsigned short *destination	-	converted value
//
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CSchedUpdUtil::DoubleToEFL1750(double source, unsigned short *destination)
{
	double			d;
	short 			aexp;
	int 			sign;
	int				exp			= 0;
	unsigned long	lmantissa	= 0;
	unsigned long	mantissa	= 0;

	if( source != 0.0 )
	{
		sign = (source < 0.0);

		if( sign )
			source = -source;

		source		= frexp(source, &exp);
		mantissa	= ((unsigned long) ldexp (source, 31));
		source		= ldexp(source, 23);
		source		= modf(source, &d);
		lmantissa	= ((unsigned long) ldexp (source, 16));

		if( sign )
		{
			mantissa	= ~mantissa;
			lmantissa	= (~lmantissa & 0xFFFF) + 1;

			if( lmantissa & 0X10000 )
				mantissa++;
		}

		if( (mantissa & (unsigned long)0xC0000000) == (unsigned long)0xC0000000 )
		{
			mantissa = mantissa >> 7;
			exp--;
		}
		else
			mantissa = mantissa >> 8;
	}

	aexp			= (short)exp;
	*destination++	= (unsigned short)((mantissa & 0x00ffff00) >> 8);
	*destination	= (unsigned short)((mantissa & 0x000000ff) << 8);
	*destination++ |= (unsigned short)(aexp & 0x000000ff);
	*destination	= (unsigned short)(lmantissa & 0x0000ffff);
}
