#if !defined(AFX_SCHEDUPDUTIL_H__FF748431_17BE_11D4_A998_00104BD11936__INCLUDED_)
#define AFX_SCHEDUPDUTIL_H__FF748431_17BE_11D4_A998_00104BD11936__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SchedUpdUtil.h : header file
//
// Revision History:													   
// Manan Dalal - July-August 2014										   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   
// Release in Build16.2
#include "TextFile.h"
#include "Parse.h"
#include "InstObj.h"
#include "CommUtil.h"

/////////////////////////////////////////////////////////////////////////////
// CSchedUpdUtil command target

class CSchedUpdUtil : public CCmdTarget
{
	DECLARE_DYNCREATE(CSchedUpdUtil)

	CSchedUpdUtil();           // protected constructor used by dynamic creation
	virtual ~CSchedUpdUtil();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSchedUpdUtil)
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSchedUpdUtil)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

public:
	int		ProcessStarInfo();
	int		ProcessScanInfo();
	BOOL	Connect();
	BOOL	OpenSchedFiles();
	BOOL	OpenStarFiles();
	BOOL	OpenFrameFiles();
	BOOL	CloseFiles();
	BOOL	CheckValid();
	BOOL	UpdateScheduleRTCSs();
	BOOL	UpdateFrameTable();
	BOOL	IMCScaleFactorCalSched();
	BOOL	GenerateIntrusion(int);
	BOOL	SensorIntrusionRpt(MSG_60*);
	BOOL	FrameIntrusionRpt(CPtrList*);
	BOOL	EclipseRpt();
	BOOL	IMCRpt();
	void	Disconnect();
	void	DeleteFiles();
	void	CreateUpdateLists();
	void	SchedOldFile(CString strFile)		{ m_strOldSched		= strFile; }
	void	SchedNewFile(CString strFile)		{ m_strNewSched		= strFile; }
	void	StarFile(CString strFile)			{ m_strStar			= strFile; }
	void	ScanFile(CString strFile)			{ m_strScan			= strFile; }
	void	SectorFile(CString strFile)			{ m_strSector		= strFile; }
	void	MapFile(CString strFile)			{ m_strMap			= strFile; }
	void	RptFile(CString strFile)			{ m_strRptFile		= strFile; }
	void	ImgIMCEnabled(BOOL bEnable)			{ m_bImgIMCEnabled	= bEnable; }
	void	SndIMCEnabled(BOOL bEnable)			{ m_bSndIMCEnabled	= bEnable; }
	void	IMCSet(CString strISet)				{ m_strIMCSet		= strISet; }
	void	StartTime(CString strStTm)			{ m_strStartTime	= strStTm; }
	void	Duration(int nDura)					{ m_nDuration		= nDura; }
	void	DoStar(BOOL bDoStar)				{ m_bDoStar			= bDoStar; }
	void	DoScan(BOOL bDoScan)				{ m_bDoScan			= bDoScan; }
	void	DoRTCS(BOOL bDoRTCS)				{ m_bDoRTCS			= bDoRTCS; }
	void	DoRSO(BOOL  bDoRSO)					{ m_bDoRSO			= bDoRSO; }	
	void	DoSRSO(BOOL bDoSRSO)				{ m_bDoSRSO			= bDoSRSO; }
	void	Instrument(CString strInst)			{ m_strInst			= strInst; }
	void	RSOCurLabel(CString strRSOCurLbl)	{ m_strRSOCurLabel	= strRSOCurLbl; }
	void	RSONewLabel(CString strRSONewLbl)	{ m_strRSONewLabel	= strRSONewLbl; }
	void	SRSOLabel(CString strSRSOlbl)		{ m_strSRSOLabel	= strSRSOlbl; }
	void	SRSOFrame(CString strSRSOfrm)		{ m_strSRSOFrame	= strSRSOfrm; }
	void	UpdateFrame(BOOL bUpdFrm)			{ m_bUpdFrm			= bUpdFrm; }
	void	NewScan(BOOL bScan)					{ m_bScanNew		= bScan; }
	void	StaticFrame(BOOL bStaticFrm)		{ m_bDoStaticFrm	= bStaticFrm; }
	void	SRSOCoordType(CString strCoordType)	{ m_strSRSOCoordType= strCoordType; }
	void	SRSOCoordNS(CString strCoordNS)		{ m_strSRSOCoordNS	= strCoordNS; }
	void	SRSOCoordEW(CString strCoordEW)		{ m_strSRSOCoordEW	= strCoordEW; }
	void	SRSODuration(CString strDuration)	{ m_strSRSODuration	= strDuration; }
	void	SRSOPreDefined(BOOL bPreDef)		{ m_bDoSRSOPreDef	= bPreDef; }
	void	SRSORatio(CString strRatio)			{ m_strSRSORatio	= strRatio; }
	CString GetStarFilename()					{ return m_strStar; }
	CString GetScanFilename()					{ return m_strScan; }

private:
	int		LookupScanInFrameTable(
				const CString*,const CString*,const CString*,
/**/			const CString* pstr1=NULL,const CString* pstr2=NULL,
/**/			const CString* pstr3=NULL,const CString* pstr4=NULL,
/**/			const CString* pstr5=NULL,const CString* pstr6=NULL,
/**/			const CString* pstr7=NULL,const CString* pstr8=NULL,
/**/			const CString* pstr9=NULL);
	int		CreateScanList(CStringArray*,CPtrList*);
	int		RemoveScanBlock();
	int		RemoveStarBlock();
	int		RemoveRtcsBlock();
	int		RemoveGroundBlock(const CString);
	int		LookupScanInSectorTable(const CString*,const CString*,const CString*);
	int		LookupScanInData(const CString*,const CString*,const CStringArray*);
	int		FindScanObj(const CString*,const CString*);
	int		FindInstrIndex(CString,int*);
	void	SectorStrData(CStringArray*,CString,CString,CString,int);
	void	SrsoStrData(CStringArray*,CString,CString,CString,CString);
	void	ScanStrData(CStringArray*,CString,CString,CString,int);
	void	BuildStarMessage(CWordArray&,CStringArray&,CStringArray&,CPtrList&,int,BOOL);
	void	DoubleToEFL1750(double, unsigned short*);
	void	DoubleToFL1750(double, unsigned short*);
	void	DeleteListElements(CPtrList*);
	void	ConvertDurationToMSm(CString*);
	BOOL	CheckForMiniSchedName();
	BOOL	ReadScanFile();
	BOOL	ReadSectorFile();
	BOOL	ReadStarFile();
	BOOL	UpdateScheduleStars();
	BOOL	SpacecraftOrientation(MSG_53*);
	BOOL	CreateStarList(CStringArray*,CPtrList*,CWordArray*,CStringArray*,int*,int*);
	BOOL	CreateScanFile(const CStringArray*);
	BOOL	BuildScanMessage(CStringArray&,CPtrList&,int,BOOL);
	BOOL	ValidateSfMsg(MSG_75_LOOK_DATA*, int);
	BOOL	FindTime(const CString*,CString*);
	BOOL	OutputStatus(CString,CString,CString,int);
	BOOL	CriticalLimit(CString,CString,CString,int);
	BOOL	UpdateScheduleScans(const CStringArray*);
	BOOL	UpdateScansWithFrmTable(const CStringArray*);
	CString	GetLastErrorText();
	CString RemoveFirstToken(CString*);
	CString SpacelookRequest(int, int);
	CString ExtractValue(const CString,const CString);

// Attributes
	CCommUtil		m_CommUtil;
	CFileException	m_fileException;
	CTextFile		m_txtOldSched;
	CTextFile		m_txtNewSched;
	CTextFile		m_txtScan;
	CTextFile		m_txtSector;
	CTextFile		m_txtStar;
	CStringArray	m_strScanData[eMaxFrameColumns];
	CStringArray	m_strSectorData[eMaxSectorColumns];
	CStringArray	m_strStarData[eMaxStarColumns];
	CStringArray	m_ScanImgList;
	CStringArray	m_ScanSndList;
	CStringArray	m_StarImgList;
	CStringArray	m_StarSndList;
	CString			m_strUserName;
	CString			m_strOldSched;
	CString			m_strNewSched;
	CString			m_strSector;
	CString			m_strScan;
	CString			m_strStar;
	CString			m_strMap;
	CString			m_strIMCSet;
	CString			m_strStartTime;
	CString			m_strRptFile;
	CString			m_strInst;
	CString			m_strRSOCurLabel;	//Frame Label to look for to replace
	CString			m_strRSONewLabel;	//Frame Label to replace with
	CString			m_strSRSOLabel;
	CString			m_strSRSOFrame;
	CString			m_strOATSHost;
	CString			m_strSRSOCoordType;
	CString			m_strSRSOCoordNS;
	CString			m_strSRSOCoordEW;
	CString			m_strSRSODuration;
	CString			m_strSRSORatio;
	BOOL			m_bImgIMCEnabled;
	BOOL			m_bSndIMCEnabled;
	BOOL			m_bDoScan;
	BOOL			m_bDoStar;
	BOOL			m_bDoRTCS;
	BOOL			m_bScanNew;
	BOOL			m_bUpdFrm;
	BOOL			m_bDoRSO;			// True if special RSO is to be updated
	BOOL			m_bDoSRSO;
	BOOL			m_bDoStaticFrm;
	BOOL			m_bDoSRSOPreDef;
	int				m_nDuration;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCHEDUPDUTIL_H__FF748431_17BE_11D4_A998_00104BD11936__INCLUDED_)
