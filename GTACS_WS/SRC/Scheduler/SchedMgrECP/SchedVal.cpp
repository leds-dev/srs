/******************
////////////////////////////////////////////////////////
// SchedVal.cpp : implementation file                 //
// (c) 2001 Frederick J. Shaw                         //
//	Prolog Updated                                    //
////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////
//
// Routine name: CSchedVal
//
// Routine description:	SchedVal.cpp: implementation
//   of the CSchedVal class.
//
// Revision history:
//
// Frederick Shaw 1999
// Initial release.
//
// Fixed Issues #76 & 77
// Dec 9, 1999
// When a time tag which allows enough time for the star window
// is found.  The star window time tag is reset to empty and is
// is not checked again.
// Added code to validate the syntax of the scan and star directives.
// Also added code to validate the duration time tags for the
// directives.
//
// Fixed issue # 77
// Modified Validate_FLS_File routine.
// Error in the routine, Searched for "Scandata" and "Stardata".
// The routines should have been "Scan" and "Star".
//
// Ported code to SchedMgrECP.
// 06/2001
//
//
////////////////////////////////////////////////////////
******************/

#include "stdafx.h"
#include "SchedVal.h"
#include "CmnHdr.h"
#include "SchedTime.h"
#include "SearchConstants.h"
#include "SchedMgrECP.h"
#include "PassedParam.h"
#include "Parse.h"
#include "ProgressWnd.h"
#include "RulesClasses.h"
#include "RcmdClasses.h"
#include "PcmdClasses.h"
#include "MainFrm.h"
#include "STOL.h"
/******************
/////////////////////////////////////////////////////////////////////////////
// SchedVal message handlers
/////////////////////////////////////////////////////////////////////////////
// CSchedVal

// enum eFileType {CLS, STOL, FRAME, STAR, IMC, STO, RTCS-CLS, RTCS-STO, RPT, INV, UNDEF};
// enum eOpenFT   {INV, RW, CR, RO, PR};
// enum eCloseFT  {WRITE, NOWRITE};

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// The CSchedVal class

//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CSchedVal()
//
//	Description : This is one of the constructors for the SchedVal class.
//                Sets the path for the file to be validated.  Also initializes
//                the variables.
//
//	Return :		N/A constructor
//
//	Parameters :
//               CString strInputFileName; Name of the file to be validated.
//               enum eFileType eValFileType; Type of file to be validated.
//
//	Note :
//			This class inherits from CGblValData.
//
//////////////////////////////////////////////////////////////////////////
******************/
CSchedVal::CSchedVal(CString strInputFileName)
:CNewSchedFile(strInputFileName)
{
   // Get the search path from the NT environmental variable. */
   // CString strEPOCH_PROCS = _wgetenv(ctstrEPOCHPROCS);
   bool bDone = false;

   TCHAR* cEPOCH_PROCS;
   size_t requiredSize;

   _tgetenv_s( &requiredSize, NULL, 0, ctcEPOCHPROCS);

   cEPOCH_PROCS = (TCHAR*) malloc(requiredSize * sizeof(TCHAR));

   // Get the value of the LIB environment variable.
   _tgetenv_s( &requiredSize, cEPOCH_PROCS, requiredSize, ctcEPOCHPROCS);

   if (!cEPOCH_PROCS){
		CString strErrorMsg = _T("Error allocating memory for string to hold environmental variable path. Exiting...");
		GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
		bDone = true;
	}

   CString strEPOCH_PROCS(cEPOCH_PROCS);
   CString strDrive;
   CString strPath;

   free(cEPOCH_PROCS);
   if (strEPOCH_PROCS.IsEmpty()){
		bDone = true;
		// output error message with search path in environmental variable. */
		CString strErrorMsg = _T("Error interpreting environmental variable.");
		GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
   }

   // Now extract all search paths and prepend drive letter to path.  */
   while (!bDone && (!strEPOCH_PROCS.IsEmpty())){
	   int nPos = 0;
		if ( -1 != (nPos = strEPOCH_PROCS.Find(_T(':')))){
			strPath = strEPOCH_PROCS.Left(nPos);
			strEPOCH_PROCS = strEPOCH_PROCS.Right(strEPOCH_PROCS.GetLength() - nPos - 1);
			strEPOCH_PROCS.TrimLeft();
			strEPOCH_PROCS.TrimRight();
			strPath.TrimLeft();
			strPath.TrimRight();
		} else {
			strPath = strEPOCH_PROCS;
			bDone = true;
		}

		if ( -1 != (nPos = strPath.Find(_T('=')))){
			// Remove the drive letter.  */
			strDrive = strPath.Left(nPos + 1);
			strDrive.Replace(_T("/"), _T(""));
			strDrive.Replace(_T("="), _T(":"));

			strPath.Delete(0, nPos + 1);
			strPath.TrimLeft();
			strPath.TrimRight();
		} else {
			// output message error with search path in environmental variable. */
			CString strErrorMsg = _T("Error interpreting environmental variable.");
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			bDone = true;
		}
		m_SearchPathArray.Add(strDrive + strPath + _T('/'));
   }

	// Set the member variables to the path that was passed into the class. **/
	m_UNCInputFileName.Full() = strInputFileName;
	m_UNCInputFileName.Full();

	/*
	CString	strFilepath;
	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];

	strFilepath.Format(_T("\\\\%s\\%s\\%s\\%s\\"),
			GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
			GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
			GetAppRegSC()->m_strDir[GetAppRegSC()->m_nDefault],			// Spacecraft
			GetAppRegFolder()->m_strDir[nFolderSTOL]);					// Specific dir

	*/
	m_UNCPrcPath.Full() = strInputFileName;
	m_UNCPrcPath.Full();

	m_UNCCLSPath.Full() = strInputFileName;
	m_UNCCLSPath.Full();

	m_eValLoc = INVALID_TYPE;
	m_NewGblValData.SetNumPrologLines(GetPrologLineCount());
	m_eValidStatus = INVALID_STATUS;

//	m_bDOYFound             = false;
	m_bBeginFound           = false;
	m_bCurrTimeFound        = false;
	m_bPrevTimeFound        = false;
	m_bCLSStopTimeFound     = false;
	m_bCLSStartTimeFound    = false;
	m_bSTARWinTimeFound     = false;
	m_bCurrCMDGapCmdFound   = false;
	m_bPrcWinTimeFound      = false;
	m_bRTCSWinTimeFound     = false;
	m_bCMDPaceFileLoaded    = false;
	m_bPaceCMDTimeFound     = false;
	m_bSensorIntrFileLoaded = false;
	m_bNextSchedSet         = false;
	m_bNextSchedGo          = false;
	m_nTotalNumCmdFrames    = 0;
	m_nGrdBeginCnt			= 0;


	m_NewGblValData.Init();  // Initalize global validation messages

	// Setup time before a HK maneuver - hh:mm:ss.t
	/*
	if (TRUE != m_CSTPreHseKpgGap.Create(GetAppRegSC()->m_strPreHKGap[GetAppRegSC()->m_nDefault])){
		CString strSubMsg = m_CSTPreHseKpgGap.GetLastErrorText();
		CString strErrorMsg;
		strErrorMsg.Format(_T("The housekeeping maneuver time is invalid; %s"), strSubMsg);
		WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
	} */

	// Spans greater than this without cmds will generate warnings - hh:mm:ss.t
	if (TRUE != m_CSTCMDGap.Create(GetAppRegSC()->m_strMaxCmdGap[GetAppRegSite()->m_nDefaultSC])){
		CString strSubMsg = m_CSTCMDGap.GetLastErrorText();
		CString strErrorMsg;
		strErrorMsg.Format(_T("The commanding gap time is invalid; %s"), strSubMsg);
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
	}

	/* Retrieve from the registry the user supplied constants.
	// How many times does the NTACTS retry to send the commands on failure?  */
	m_nRetryCnt = GetAppRegSC()->m_nRetryCnt[GetAppRegSite()->m_nDefaultSC];

	// What is the default time for each command to execute on the ground?
	m_fCmdTime = GetAppRegSC()->m_fCmdExeTime[GetAppRegSite()->m_nDefaultSC];

	// Set the command running total to zero to start.
	if (TRUE != m_CSTTotalCMDTime.Create(ctstrZeroTimeTag)){
		CString strSubMsg = m_CSTTotalCMDTime.GetLastErrorText();
		CString strErrorMsg;
		strErrorMsg.Format(_T("Error initializing the command running time; %s"), strSubMsg);
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
	}

	// Set the RTCS address buffer to some real high number.
	m_nPrevStartValue= 10000000;

	/* Clean up object arrays when starting.
	// First free up memory allocated  */
	int nMaxSize = m_objActivityEndTime.GetSize();
	int nObjIdx = 0;
	while ( nObjIdx < nMaxSize){
		delete ((CSchedTime *) m_objActivityEndTime[nObjIdx]);
		nObjIdx++;
	}

	/* Then remove elements from array.  */
	m_objActivityEndTime.RemoveAll();

	nMaxSize = m_objCMDPaceMnemonic.GetSize();

	/* Clean up object arrays when starting.
	// First free up memory allocated */
	nObjIdx = 0;
	while ( nObjIdx < nMaxSize){
		delete ((strctCMDPaceMnemonicList *) m_objCMDPaceMnemonic[nObjIdx]);
		nObjIdx++;
	}

	/* Then remove elements from array.  */
	m_objCMDPaceMnemonic.RemoveAll();

	/* Clean up object arrays when starting.
	// First free up memory allocated  */
	nMaxSize = m_objCMDPaceRestriction.GetSize();
	nObjIdx = 0;

	while ( nObjIdx < nMaxSize){
		delete ((strctCMDPaceRestrictList *) m_objCMDPaceRestriction[nObjIdx]);
		nObjIdx++;
	}

	/* Then remove elements from array. */
   	m_objCMDPaceRestriction.RemoveAll();

	LoadCmdPacingFile();
	LoadRTCSCmdRestrictFile();
	LoadSTOLCmdRestrictFile();
//	LoadNumCmdFrameFile();

	/* Clear the build window of the output bar.
	// Put out an informational message that the
	// validation process has started. */
	CString strTitle;
	strTitle.Format(_T("Validating File %s"), strInputFileName);
	GetAppOutputBar()->InitBuild(strTitle, strInputFileName);
	GetAppOutputBar()->OutputToEvents(strTitle, COutputBar::INFO);

	if (!m_bRTCSCMDRestrictedFileLoaded){
		CString strMsg(_T("Unable to load RTCS Command Restriction File: "));
		CString strTitle;
		strTitle.Format(_T("%s %s"), strMsg, GetAppRegSC()->m_strRTCSCmd[GetAppRegSite()->m_nDefaultSC]);
		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
	} else {
		CString strMsg(_T("Successfully loaded the RTCS Command Restriction File:"));
		CString strTitle;
		strTitle.Format(_T("%s %s"), strMsg, GetAppRegSC()->m_strRTCSCmd[GetAppRegSite()->m_nDefaultSC]);
		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
	}

	if (!m_bSTOLCMDRestrictedFileLoaded){
		CString strMsg(_T("Unable to load STOL Restriction file: "));
		CString strTitle;
		strTitle.Format(_T("%s %s"), strMsg, GetAppRegSC()->m_strSTOLCmd[GetAppRegSite()->m_nDefaultSC]);
		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
	} else {
		CString strMsg(_T("Successfully loaded the STOL Restriction File:"));
		CString strTitle;
		strTitle.Format(_T("%s %s"), strMsg, GetAppRegSC()->m_strSTOLCmd[GetAppRegSite()->m_nDefaultSC]);
		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
	}

	if (!m_bCMDPaceFileLoaded){
		CString strMsg(_T("Unable to load the Command Pacing File: "));
		CString strTitle;
		strTitle.Format(_T("%s %s"), strMsg, GetAppRegSC()->m_strPaceCmd[GetAppRegSite()->m_nDefaultSC]);
		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
	} else {
		CString strMsg(_T("Successfully loaded the Command Pacing File:"));
		CString strTitle;
		strTitle.Format(_T("%s %s"), strMsg, GetAppRegSC()->m_strPaceCmd[GetAppRegSite()->m_nDefaultSC]);
		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
	}

	m_eValRTCSProcs = FALSE;
	m_bReadRTCSSetFile = false;

	/* Clean up object arrays when starting.
	// First free up memory allocated */
	nMaxSize = m_objRTCSSetProc.GetSize();
	nObjIdx = 0;

	while ( nObjIdx < nMaxSize){
		delete ((strctRTCSSetProcList *) m_objRTCSSetProc[nObjIdx]);
		nObjIdx++;
	}

	/* Then remove elements from array.  */
	m_objRTCSSetProc.RemoveAll();

	/* Clean up object arrays when starting.
	// First free up memory allocated.  */
	nMaxSize = m_objSensorIntrusions.GetSize();
	nObjIdx = 0;

	while ( nObjIdx < nMaxSize){
		delete ((strctIntrusionRptList *) m_objSensorIntrusions[nObjIdx]);
		nObjIdx++;
	}

	/* Then remove elements from array.  */
	m_objSensorIntrusions.RemoveAll();

};
/********************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-10-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSchedVal::~CSchedVal()
//
//	Description :	SchedVal destructor.
//
//	Return :		none
//
//	Parameters :	none
//
//	Note :
//////////////////////////////////////////////////////////////////////////
********************************/
CSchedVal::~CSchedVal(){

	/* Clean up object arrays when exiting.
	// First free up memory allocated.  */
	int nMaxSize = m_objActivityEndTime.GetSize();
	int nObjIdx = 0;
	while ( nObjIdx < nMaxSize){
		delete ((CSchedTime *) m_objActivityEndTime[nObjIdx]);
		nObjIdx++;
	}

	/* Then remove elements from array.  */
	m_objActivityEndTime.RemoveAll();

	//
	nMaxSize = m_objCMDPaceMnemonic.GetSize();

	/* Clean up object arrays when exiting.
	// First free up memory allocated  */
	nObjIdx = 0;
	while ( nObjIdx < nMaxSize){
		delete ((strctCMDPaceMnemonicList *) m_objCMDPaceMnemonic[nObjIdx]);
		nObjIdx++;
	}

	/* Then remove elements from array.  */
	m_objCMDPaceMnemonic.RemoveAll();

	/* Clean up object arrays when exiting.
	// First free up memory allocated.  */
	nMaxSize = m_objCMDPaceRestriction.GetSize();
	nObjIdx = 0;

	while ( nObjIdx < nMaxSize){
		delete ((strctCMDPaceRestrictList *) m_objCMDPaceRestriction[nObjIdx]);
		nObjIdx++;
	}

	/* Then remove elements from array.  */
	m_objCMDPaceRestriction.RemoveAll();

	/* Clean up object arrays when exiting.
	// First free memory allocated.  */
	nMaxSize = m_objRTCSSetProc.GetSize();
	nObjIdx = 0;
	while ( nObjIdx < nMaxSize){
		delete ((strctRTCSSetProcList *) m_objRTCSSetProc[nObjIdx]);
		nObjIdx++;
	}

	/* Then remove elements from array.  */
	m_objRTCSSetProc.RemoveAll();

	/* Clean up object arrays when exiting.
	// First free memory allocated.  */
	nMaxSize = m_objSensorIntrusions.GetSize();
	nObjIdx = 0;

	while ( nObjIdx < nMaxSize){
		delete ((strctIntrusionRptList *) m_objSensorIntrusions[nObjIdx]);
		nObjIdx++;
	}

	/* Then remove elements from array.  */
	m_objSensorIntrusions.RemoveAll();

	// Clean up object arrays when exiting.
	// First free memory allocated
	/*
	POSITION pos = m_mapNumCmdFrame.GetStartPosition();
	while ( pos != NULL){
		CString *pNum = NULL;
		CString key;
		m_mapNumCmdFrame.GetNextAssoc(pos, key, *pNum);
		delete (pNum);
	}
	*/
	/* Then remove elements from array.  */
	m_mapNumCmdFrame.RemoveAll();
}

/********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 04-25-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSchedVal::Validate(const enum eValType eFileValType)
//
//	Description :	Top level function.  The file type is passed in.  The
//                  file type drives which validition function is called.
//
//	Return :		BOOL
//                      TRUE if no errors occured and file passed validation.
//                      FALSE if errors occured or file failed validation.
//
//	Parameters :	const enum eValType eFileValType
//
//	Note :
//////////////////////////////////////////////////////////////////////////
**********************************************/
BOOL CSchedVal::Validate(const enum eValType eFileValType)
{
	BOOL eReturnStatus = TRUE;
	bool bSuccessful = true;
	m_eValType = eFileValType;

	if (TRUE != Open(RW)){
		eReturnStatus = FALSE;
		bSuccessful   = false;
		CString strErrorMsg;
		strErrorMsg.Format(_T("Error openning input file. %s"), m_UNCInputFileName.File());
		GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
	} else {
		m_NewGblValData.SetNumPrologLines(GetPrologLineCount());
		/* validate based on file type  */
		Validate_Prolog(eFileValType);

		switch (eFileValType)
		{
			case CLS_VAL :
			{
				if (!Validate_CLS_File()){
					eReturnStatus = FALSE;
				}
			}
			break;

			case SKB_VAL:
			case STOL_VAL :
			{
				if (!Validate_STOL_File()){
					eReturnStatus = FALSE;
				}
				// Validate instrument objects
				if (!Check_Star_InstrObjFile()){
					eReturnStatus = FALSE;
				}

				if (!Check_Scan_InstrObjFile()){
					eReturnStatus = FALSE;
				}
			}
			break;

			case RTCS_SET_VAL :
			{
				if (!Validate_RTCS_SET_File()){
					eReturnStatus = FALSE;
				}
			}
			break;

			case RTCS_PRC_VAL :
			{
				if (!Validate_RTCS_PRC_File()){
					eReturnStatus = FALSE;
				}
			}
			break;

			/*
			case STORED_VAL :
			{
				if (!Validate_STORED_File ()){
					eReturnStatus = FALSE;
				}
			}
			break;
			*/

			default:
				// Ouput an error; invalid validation file type.
				eReturnStatus = FALSE;
				CString strErrorMsg;
				strErrorMsg.Format(_T("Invalid validation file type."));
				GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			break;
		}
	}

	if (bSuccessful){
		if (TRUE != m_NewGblValData.WriteRptHeader(m_eValidStatus)){
			CString strErrorMsg;
			eReturnStatus = FALSE;
			strErrorMsg.Format(_T("Error creating validation report header."));
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
		}

		if (TRUE != WriteValidationStatus(m_eValidStatus, m_eValLoc)){
			CString strErrorMsg;
			eReturnStatus = FALSE;
			strErrorMsg.Format(_T("Error writing validation status to file."));
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
		}

		CFileException pException;
		if (TRUE != Close(CTextFile::WRITE, &pException)){
			CString strErrorMsg;
			eReturnStatus = FALSE;
			strErrorMsg.Format(_T("Error closing file."));
			GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			CString strException = ExceptionErrorText(&pException);
			GetAppOutputBar()->OutputToBuild(strException, COutputBar::FATAL);
		}
	}

	CString strErrorMsg = _T("File validation done.");
	GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::INFO);
	GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::INFO);
	return eReturnStatus;
}
/************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-26-01		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function:	 Validate_STOL_File ()
//
//	Description: Validates STOL files.
//
//	Return:		 BOOL
//					TRUE  File passed validation.
//                  FALSE File failed validation.
//
//	Parameters:	none.
//
//	Note:
//////////////////////////////////////////////////////////////////////////
*********************************************/
BOOL CSchedVal::Validate_STOL_File ()
{
	bool eReturn_Status = TRUE;
//	int nNumPrologLines = GetPrologLineCount();

	CString strInputFileExt  = m_UNCInputFileName.Extension(TRUE);
	CString strInputFileName = m_UNCInputFileName.File();
	CString strInputFileBase = m_UNCInputFileName.Base();


	CString strExt(_T(".prc"));
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nSTOL])->GetDocString(strExt, CDocTemplate::filterExt);

	if (strExt.Compare(strInputFileExt)){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 2, m_UNCInputFileName.File());
	}

	LoadNumCmdFrameFile();

	if (TRUE == m_eValRTCSProcs){
		ReadRTCSSetFile();
	}

	/* Override the procedure path since validating a STOL proc.
	// Need to follow the same search paths as STOL.  */
	m_UNCPrcPath.Full() = m_UNCInputFileName.Full();

	CWaitCursor wait;   // display wait cursor
	CString strLine;
	bool bArgsDirFound =  false;
//	m_nGrdBeginCnt = 0;
//	m_bBeginFound = false;


	while (GetFileDirNext(strLine)){

		#ifndef _NO_PROGRESS
			int nLineNum = GetCurrentFileLineNum () + nNumPrologLines + 1;
			CString strText;
			strText.Format (strFormat, nLineNum, nLineCount);
			wndProgress.SetText (FALSE, strText);
			wndProgress.SetPos (nLineNum, FALSE);
		#endif

		CString strLineTrimmed(strLine);
		bool bWaitDirFound = false;
		strLineTrimmed.TrimLeft ();
		strLineTrimmed.TrimRight();
		strLineTrimmed.MakeUpper();
		strLine.TrimLeft();
		strLine.TrimRight();

		if (strLineTrimmed.IsEmpty()){
			// Empty line
		} else if (ctchCOMMENT == strLineTrimmed[0]){
			/* Scheduler directive
			// Test for ##ground begin & ##ground end directives.
			// Test for ##activity directive.  */
			CString strTokenBegin(ctstrGROUND);
			strTokenBegin += _T(' ');
			strTokenBegin += ctstrBEGIN;
			CString strTokenEnd(ctstrGROUND);
			strTokenEnd += _T(' ');
			strTokenEnd += ctstrEND;
//			if ((-1 != strLineTrimmed.Find(ctstrGROUND)) && 
//				(-1 != strLineTrimmed.Find(ctstrBEGIN))){
			if (-1 != strLineTrimmed.Find(strTokenBegin)){
				// if (m_bBeginFound){
				//	m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 0);
				//	eReturn_Status = FALSE;
			//  } {
				m_bBeginFound = true;
				m_nGrdBeginCnt++;
			//	}
			} else if (-1 != strLineTrimmed.Find(strTokenEnd)){
				if ( m_nGrdBeginCnt <=0){ // No begin found before end.  
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 16);
					eReturn_Status = FALSE;
				}else {
					m_nGrdBeginCnt--;
					m_bBeginFound = false;
				}
			} else if (0 == strLineTrimmed.Find(ctstrGROUND)){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 93, strLine);
				eReturn_Status = FALSE;
			} else if (0 == strLineTrimmed.Find(ctstrACTIVITY)){
				if (!Validate_Activity_Dir(strLine)){
					eReturn_Status = FALSE;
				}
			}
		} else if ((m_eValLoc == ONBOARD) && m_bBeginFound){
			/* Inside ground begin section and validation location is onboard.
			// skip lines  */
		} else {
			/*
			// If a label is present, need to remove before processing line.
			// Extract the first token from the line.
			// Use this token to check for label.
			// Remove label if present.  */
			int nPos = strLine.Find(_T(' '));
			CString str1stToken = strLine.Left(nPos + 1);
			if (-1 != str1stToken.Find(_T(':'))){
				strLine.Delete(0, nPos);
			}

			CString strLineTrimmedUpper(strLineTrimmed);
//			strLineTrimmedUpper.MakeUpper ();
			if ((0 == strLineTrimmedUpper.Find(ctstrWRITE)) && ScanLine(strLineTrimmedUpper, ctstrWRITE)){
				// Ignore line write directive
			}  else if ((0 == strLineTrimmedUpper.Find(ctstrRTCSDATA)) && ScanLine(strLineTrimmedUpper, ctstrRTCSDATA)){
				// Scan line
			} else if ((0 == strLineTrimmedUpper.Find(ctstrSCHEDMON)) && ScanLine(strLineTrimmedUpper, ctstrSCHEDMON)){
				Validate_NEXT_Dir(strLine);
			} else if ((m_eValLoc == ONBOARD || m_eValLoc == BOTH) && (0 == strLineTrimmedUpper.Find(ctstrHEXCMD)) && ScanLine(strLine, ctstrHEXCMD)){
				// Hexcmd directive are not valid in onboard schedules.
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 74, strLine);
			} else {

				CString strPrevTimeTemp = m_CSTPrevTime.GetTimeString(TRUE);
				CString strCurrTimeTemp = m_CSTCurrTime.GetTimeString(TRUE);
				m_bCurrTimeFound = false;
				if ((0 == strLineTrimmedUpper.Find(ctstrWAIT)) && ScanLine(strLineTrimmedUpper, ctstrWAIT)){
					if (!Validate_Wait_Dir(strLine)){
						eReturn_Status = FALSE;
					}
					bWaitDirFound = true;
				} else {
					bWaitDirFound = false;
				}

				strPrevTimeTemp = m_CSTPrevTime.GetTimeString(TRUE);
				strCurrTimeTemp = m_CSTCurrTime.GetTimeString(TRUE);
				/**
				// Check if commanding gaps have been exceeded.
				// The m_bCurrentCMDGapCmd is set to false when a command is found.
				// This resets the commanding gap.
				// When a timetag is found, m_bCurrentCMDGapCmd is set to the current
				// time plus the user specified commanding gap.  Any timetags found are
				// compared to the maximum commanding gap time.  If it is exceeded a warning
				// message is posted.  **/
				if (m_bCurrTimeFound && m_bCurrCMDGapCmdFound){
					CString strCurrTimeTemp = m_CSTCurrTime.GetTimeString(TRUE);
					CString strCurrCMDGap   = m_CSTCurrCMDGapCmd.GetTimeString(TRUE);
					if (m_CSTCurrTime > m_CSTCurrCMDGapCmd) {
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 3, strLine);
						eReturn_Status = FALSE;
					} else {
					   m_CSTCurrCMDGapCmd = m_CSTCurrTime + m_CSTCMDGap;
					   CString strCurrCMDGap = m_CSTCurrCMDGapCmd.GetTimeString(TRUE);
					   m_bCurrCMDGapCmdFound =  true;
					}
				} else if (m_bCurrTimeFound){
					m_CSTCurrCMDGapCmd    = m_CSTCurrTime + m_CSTCMDGap;
					m_bCurrCMDGapCmdFound = true;
				}

				/**
				// Check if number of command frames exceeds the delta between timetags
				// m_nNumCmdFrames / reg value (13) = number of seconds
				// If this value greater than the timetag delta,
				// output an warning message **/
				if ((m_CSTCurrTime >= m_CSTPrevTime) && bWaitDirFound
						&& m_bCurrTimeFound	&& m_bPrevTimeFound){
					CSchedTime CSTUplinkTime;
//					int nTemp = GetAppRegSC()->m_nCmdFrameUplinkRate[GetAppRegSC()->m_nDefault];
					float fNumSecsReq = ((float)m_nTotalNumCmdFrames/(float)GetAppRegSC()->m_nCmdFrameUplinkRate[GetAppRegSite()->m_nDefaultSC]);
					CSTUplinkTime = m_CSTPrevTime + fNumSecsReq;
					CString strUplinkTime   = CSTUplinkTime.GetTimeString(TRUE);
					CString strPrevTimeTemp = m_CSTPrevTime.GetTimeString(TRUE);
					CString strCurrTimeTemp = m_CSTCurrTime.GetTimeString(TRUE);
					CString strUplinkTimeTemp = CSTUplinkTime.GetTimeString(TRUE);
					if ((CSTUplinkTime >=  m_CSTCurrTime) && (m_eValLoc == GROUND || m_eValLoc == BOTH)) {

						CString strTemp;
						strTemp.Format(_T("%.3f"), fNumSecsReq);
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 98, strTemp);
					}

					m_nTotalNumCmdFrames = 0;
				}

				if (m_bTotalCMDTimeFound){
					if (m_bCurrTimeFound){
						if (m_CSTCurrTime < m_CSTTotalCMDTime){
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 26, strLine);
							eReturn_Status = FALSE;
							// Reset command timing counter to empty.
							m_bTotalCMDTimeFound =  false;
						}
					}
					/*
					else if (m_bPrevTimeFound) {
						if ((m_CSTPrevTime < m_CSTTotalCMDTime)){
							WriteValidationMsg(GetCurrentFileLineNum (), 26, strLine);
							eReturn_Status = FALSE;
							// Reset command timing counter to empty.
							m_bTotalCMDTimeFound =  false;
						}
					}  */
					else {
						// Reset command timing counter to empty.
						m_bTotalCMDTimeFound =  false;
					}
				}


				// Check if current timetag is greater than previous timetag.
				// If true then copy current timetag to the previous timetag.
				if (m_bCurrTimeFound && (m_CSTCurrTime >= m_CSTPrevTime)){
					m_CSTPrevTime = m_CSTCurrTime;
					m_bPrevTimeFound = true;
				} else if ((m_bCurrTimeFound) && (m_bPrevTimeFound)
					&& (m_CSTCurrTime >= m_CSTPrevTime)) {
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 9, strLine);
						m_bCurrTimeFound = false;
						eReturn_Status = FALSE;
				}

//				int nTemp = GetCurrentFileLineNum ();
				if (!bWaitDirFound){
					if (!m_bBeginFound){
						CString strCurrTime = m_CSTCurrTime.GetTimeString(TRUE);
						CString strStarTime = m_CSTSTARWinTime.GetTimeString(TRUE);
						CString strPrevTime = m_CSTPrevTime.GetTimeString(TRUE);
						if (m_bSTARWinTimeFound){
							if (m_bCurrTimeFound){
								if ((m_CSTCurrTime < m_CSTSTARWinTime)){
									m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 34);
									eReturn_Status = FALSE;
								}
							} else if (m_bPrevTimeFound) {
								if ((m_CSTPrevTime < m_CSTSTARWinTime)){
									m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 34);
									eReturn_Status = FALSE;
								}
							} else {
								m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 34);
								eReturn_Status = FALSE;
							}
						} else {
							m_bSTARWinTimeFound = false;
						}
					}

					// Modified this line to correct for PSS START directive.  Flagging START inside of directives.
					if ((0 == (strLineTrimmedUpper.Find(ctstrSTART))) && ScanLine(strLineTrimmedUpper, ctstrSTART)){
					// if ((-1 != strLineTrimmedUpper.Find(ctstrSTART)) && ScanLine(strLineTrimmedUpper, ctstrSTART)){
						if(!Validate_START_Dir(strLine)){
							eReturn_Status = FALSE ;
						}

					// && ScanLine(strLineTrimmedUpper, ctstrSTAR)
					} else if ((-1 != strLineTrimmedUpper.Find(ctstrSTAR)) && ScanLine(strLineTrimmedUpper, ctstrSTAR)){
						if(!Validate_Star_Dir(strLine)){
							eReturn_Status = FALSE;
						}
					} else if ((-1 != strLineTrimmedUpper.Find(ctstrSCAN)) && ScanLine(strLineTrimmedUpper, ctstrSCAN)){
						if(!Validate_Scan_Dir(strLine)){
							eReturn_Status = FALSE;
						}
					} else if ((-1 != strLineTrimmedUpper.Find(ctstrCMD)) && ScanLine(strLineTrimmedUpper, ctstrCMD)){
						Validate_CMD_Dir(strLine);
						// Only check for local variables if validation status is onboard or both
						// and not in a ground block.
						if ((m_eValLoc == ONBOARD || m_eValLoc == BOTH) && !m_bBeginFound){
							CheckForLocalVariables(strLine);
						}
					}  else if ((-1 != strLineTrimmedUpper.Find(ctstrPREHSEKPG)) && ScanLine(strLineTrimmedUpper, ctstrPREHSEKPG)){
						if ( !m_bCurrTimeFound && !m_bPrevTimeFound){
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 4, strLine);
							eReturn_Status = FALSE;
						} else {
							if (m_bCurrTimeFound){
								m_CSTPreHseKpgTime = m_CSTCurrTime;
							} else if (m_bPrevTimeFound){
								m_CSTPreHseKpgTime = m_CSTPrevTime;
							}
						}
					} else if ((-1 != strLineTrimmedUpper.Find(ctstrHSEKPG)) && ScanLine(strLineTrimmedUpper, ctstrHSEKPG)){
						if (!m_bCurrTimeFound && !m_bPrevTimeFound){
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 4, strLine);
							eReturn_Status = FALSE;
						} else {
							CSchedTime CurrTime;
							if (m_bCurrTimeFound){
								CurrTime = m_CSTCurrTime;
							} else if (m_bPrevTimeFound){
								CurrTime = m_CSTPrevTime;
							}

							CSchedTimeSpan cTempTime = CurrTime - m_CSTPreHseKpgTime;
							if ((CurrTime - m_CSTPreHseKpgTime) < m_CSTPreHseKpgGap){
								m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 14, strLine);
								eReturn_Status = FALSE;
							}
						}
					} else if ((-1 != strLineTrimmedUpper.Find(ctstrRTCS)) && ScanLine(strLineTrimmedUpper, ctstrRTCS)
						&& TRUE == m_eValRTCSProcs){
						if (Validate_RTCS_Dir(strLine) != TRUE){
							eReturn_Status = FALSE;
						}
					} else if ((-1 != strLineTrimmedUpper.Find(ctstrNARGS))){
						if (!bArgsDirFound){
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 43, strLine);
							eReturn_Status = FALSE;
						}
					} else if (-1 != strLineTrimmedUpper.Find(ctstrARGS)){
						bArgsDirFound = true;
						if (Validate_ARGS_Dir(strLine) != TRUE){
							eReturn_Status = FALSE;
						}
					}
				}
			}
			#ifndef _NO_PROGRESS
				wndProgress.PeekAndPump(false);
				if (wndProgress.Cancelled()){
					break;
				}
			#endif
		}
	}

	// Check if ##param is present and args() directive is not.
	int nPrologPPSize = GetNumberPassedParams ();
	if ((!bArgsDirFound) && (nPrologPPSize > 0)){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 42);
		eReturn_Status = FALSE;
	}

	#ifndef _NO_PROGRESS
	//	::Sleep(nctSLPTIME);
	#endif

	Check_Activity_Log();

	// If NextSched GO is not present, an informational message will be issued.
	if (!m_bNextSchedGo){
		// Set is not present before NextSched GO.
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 60);
	}

	if (m_bNextSchedGo && !m_bNextSchedSet){
		// Set is not present before NextSched GO.
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 62);
		eReturn_Status = FALSE;
	}

	if (m_nGrdBeginCnt > 0){
		// ##ground begin was found but ground end was not found.
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 0);
		eReturn_Status = FALSE;
	} else if (m_nGrdBeginCnt < 0){
		// ##ground end was found but ground begin was not found.
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 16);
		eReturn_Status = FALSE;
	}

	return eReturn_Status;
}

/********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		Validate_CLS_File ()
//
//	Description :	Validates a CLS file.
//
//	Return :		BOOL
//						TRUE, if file passed validation.
//                      FALSE, if file failed validation.
//
//	Parameters :	none.
//
//	Note :	wait directives are not allowed in CLS files.
//////////////////////////////////////////////////////////////////////////
*******************************************/
BOOL CSchedVal::Validate_CLS_File ()
{

	if (TRUE == m_eValRTCSProcs){
		ReadRTCSSetFile();
	}

	BOOL eReturn_Status = TRUE;
	m_nGrdBeginCnt = 0;
	m_bBeginFound = false;

//	bool bStartOfFile = true;  // Flag to determine if at beginning of the file.
//	int nNumPrologLines = GetPrologLineCount();
//	int nLineCount      = GetLineCount() + nNumPrologLines;
	CString strFormat = _T("CLS file validation in progress. \n")
						_T("Currently on line %d of %d\n");

	CWaitCursor wait;   // display wait cursor
	CString strLine;
	CString strText;
	while ( GetFileDirNext (strLine) == TRUE){

//		int nLineNum = GetCurrentFileLineNum () + nNumPrologLines + 1;
//		strText.Format(strFormat, nLineNum, nLineCount);
		CString strLineTrimmed(strLine);
		strLineTrimmed.TrimLeft ();
		strLineTrimmed.TrimRight ();
		strLineTrimmed.MakeUpper ();

		// Validate CLS line.
		if (strLineTrimmed.IsEmpty()){
			// Empty line
		} else if (ctchCOMMENT == strLineTrimmed[0]){
			/* Scheduler directive
			// Test for ##ground begin & ##ground end directives.
			// Test for ##activity directive.  */
			CString strTokenBegin(ctstrGROUND);
			strTokenBegin += _T(' ');
			strTokenBegin += ctstrBEGIN;
			CString strTokenEnd(ctstrGROUND);
			strTokenEnd += _T(' ');
			strTokenEnd += ctstrEND;

			if (-1 != strLineTrimmed.Find(strTokenBegin)){
				m_bBeginFound = true;
				m_nGrdBeginCnt++;
//			} else if ((0 == strLineTrimmed.Find(ctstrGROUND))  &&
//				(-1 != strLineTrimmed.Find(ctstrEND))){
			} else if (-1 != strLineTrimmed.Find(strTokenEnd)){
				if ( m_nGrdBeginCnt <=0){ // No begin found before end.  
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 16);
					eReturn_Status = FALSE;
				}else {
					m_nGrdBeginCnt--;
					m_bBeginFound = false;
				}
			} else if (0 == strLineTrimmed.Find(ctstrGROUND)){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 93, strLine);
				eReturn_Status = FALSE;
			} else if (-1 != strLineTrimmed.Find(ctstrACTIVITY)){
				if (!Validate_Activity_Dir(strLine)){
					eReturn_Status = FALSE;
				}
			}
		} else if ((m_eValLoc == ONBOARD ) && m_bBeginFound){
			// Inside ground begin section and validation location is onboard.
			// skip lines
		} else {

			CString strTimeTag;
			// Extract time tag.
			if (strLine.GetLength () >= ctnCPTTLength){
				strTimeTag = strLine.Left(ctnCPTTLength);
				strLine.Delete(0, ctnCPTTLength);
				strTimeTag.TrimRight();
				strTimeTag.TrimLeft();
				strLine.TrimLeft();
				strLine.TrimRight();
				strLineTrimmed = strLine;
				// strLineTrimmed.TrimLeft ();
				// strLineTrimmed.TrimRight ();
				strLineTrimmed.MakeUpper ();

				if (!strTimeTag.IsEmpty()) {  // A time tag is present.
					CSchedTimeSpan cST;
					CSchedTime     cBaseTime;
					if (TRUE != cST.Create(strTimeTag)){
						CString strSubMsg = cST.GetLastErrorText();
						// CString strErrorMsg;
						// strErrorMsg.Format(_T("Invalid timetag; %s"), strSubMsg);
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strSubMsg, 2);
						eReturn_Status = FALSE;
						strTimeTag.Empty();
					} else { // Valid time tag.
						// Create a Schedule time to use to compare againest the time tag found.
						cBaseTime = cBaseTime + cST;
						// Check if the time tag found is
						// greater than the current time tag.
						if (m_CSTCurrTime >= cBaseTime  && m_bCurrTimeFound){
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 9);
							eReturn_Status = FALSE;
						} else if (m_bCurrTimeFound){
							// Current time tag exists & time tag is greater than
							// current time tag.
							m_CSTPrevTime = m_CSTCurrTime;
							m_CSTCurrTime =  cBaseTime;  // Set time to current.
							m_bPrevTimeFound = true;
						} else  {
							m_CSTCurrTime = cBaseTime;  // Set time to current.
							m_bCurrTimeFound = true;
						}

						if (m_bPrevTimeFound){
							CSchedTimeSpan cMinTimeInterval;
							cMinTimeInterval = cBaseTime - m_CSTPrevTime;
							CString strTimeInterval = cMinTimeInterval.GetTimeSpanString(FALSE, TRUE);
						//	TCHAR   *cStopString;
							double dNumSeconds = cMinTimeInterval.GetSchedTimeInSeconds();
							double dTimeTagSpacing = _tcstod(GetAppRegSC()->m_strTimeTagSpacing[GetAppRegSite()->m_nDefaultSC], NULL);

							if ((dNumSeconds -  dTimeTagSpacing) <= 0.0){
								m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 96, strTimeTag );
								eReturn_Status = FALSE;
							}
						}

						// SSGS-1206: Check if previous CLS has had enough time to execute
						if(m_bCLSWinTimeFound) {
							if(m_bCurrTimeFound) {
								if(m_CSTCurrTime < m_CSTCLSWinTime) {
									m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 100);
									eReturn_Status = FALSE;
									// Reset CLS timing counter to empty.
									m_bCLSWinTimeFound =  false;
								}
							} else if(m_bPrevTimeFound) {
								if(m_CSTPrevTime < m_CSTCLSWinTime) {
									m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum(), 100);
									eReturn_Status = FALSE;
									// Reset CLS timing counter to empty.
									m_bCLSWinTimeFound = false;
								}
							}
						}

						if (!m_bCLSStartTimeFound){
							m_CSTCLSStartTime = cST;
							m_bCLSStartTimeFound = true;
						} else if (cST <= m_CSTCLSStartTime){
							m_CSTCLSStartTime = cST;
						}

						if (!m_bCLSStopTimeFound){
							m_CSTCLSStopTime = cST;
							m_bCLSStopTimeFound = true;
						} else if (m_CSTCLSStopTime <= cST){
							m_CSTCLSStopTime = cST;
							m_bCLSStopTimeFound = true;
						}

						if (m_bTotalCMDTimeFound){
							if (m_bCurrTimeFound){
								if (m_CSTCurrTime < m_CSTTotalCMDTime){
									m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 26, strLine);
									eReturn_Status = FALSE;
								}
							} /*
							else if (m_bPrevTimeFound) {
								if ((m_CSTPrevTime < m_CSTRTCSWinTime)){
									WriteValidationMsg(GetCurrentFileLineNum (), 26, strLine);
									eReturn_Status = FALSE;
								}
							} */
							else {
								// Reset command timing counter to empty.
								m_bTotalCMDTimeFound =  false;
							}
						}
					}

					CString strCurrTime = m_CSTCurrTime.GetTimeString(TRUE);
					CString strStarTime = m_CSTSTARWinTime.GetTimeString(TRUE);
					CString strPrevTime = m_CSTPrevTime.GetTimeString(TRUE);
					if (m_bSTARWinTimeFound){
						if (m_bCurrTimeFound){
							if ((m_CSTCurrTime < m_CSTSTARWinTime)){
								m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 34);
								eReturn_Status = FALSE;
							}
						} else if (m_bPrevTimeFound) {
							if ((m_CSTPrevTime < m_CSTSTARWinTime)){
								m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 34);
								eReturn_Status = FALSE;
							}
						} else {
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 34);
							eReturn_Status = FALSE;
						}
					} else {
						m_bSTARWinTimeFound = false;
					}

					if (0 == strLineTrimmed.Find(ctstrSTARINFO)){
						// StarInfo line\
						// Not allowed in a CLS
					} else if (0 == strLineTrimmed.Find(ctstrSTARDATA)){
						// Stardata line
						// Not allowed in a CLS
					} else if (0 == strLineTrimmed.Find(ctstrSCAN)){
						// Scan line
						if(Validate_Scan_Dir(strLine) != TRUE){
							eReturn_Status = FALSE;
						}
					} else if (0 == strLineTrimmed.Find(ctstrHEXCMD)){
						// Hexcmd directive are not valid in CLCs.
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 74, strLine);
					} else if (0 == strLineTrimmed.Find(ctstrSTART)){
						if(Validate_START_Dir(strLine) != TRUE){
							eReturn_Status = FALSE;
						}
					} else if(0 == strLineTrimmed.Find(ctstrSTAR)){
						if(Validate_Star_Dir(strLine) != TRUE){
							eReturn_Status = FALSE;
						}
					} else if(0 == strLineTrimmed.Find(ctstrSCANDATA)){
						/*
						if(Validate_ScanData_Dir(strLine) != TRUE){
							eReturn_Status = FALSE;
						}
						*/
					} else if(0 == strLineTrimmed.Find(ctstrCMD)){
						// Validate_CMD_Dir(strLine);
					} else if(0 == strLineTrimmed.Find(ctstrCLS)){
						if (Validate_CLS_Dir(strLine) != TRUE){
							eReturn_Status = FALSE;
						}
					} else if((0 == strLineTrimmed.Find(ctstrRTCS))
							&& TRUE == m_eValRTCSProcs){
						if (Validate_RTCS_Dir(strLine) != TRUE){
							eReturn_Status = FALSE;
						}
					}
				}
			}
		}

		#ifndef _NO_PROGRESS
			wndProgress.PeekAndPump(false);
			if (wndProgress.Cancelled()){
				break;
			}
		#endif
	}

	if (m_nGrdBeginCnt > 0){
		// ##ground begin was found but ground end was not found.
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 0);
		eReturn_Status = FALSE;
	} else if (m_nGrdBeginCnt < 0){
		// ##ground end was found but ground begin was not found.
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 16);
		eReturn_Status = FALSE;
	}

	#ifndef _NO_PROGRESS
		// sleep(nctSLPTIME);
	#endif
	return eReturn_Status;
}
/*********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 08-06-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function:	CSchedVal::Validate_RTCS_SET_File
//
//	Description :	This routine validates a RTCS set file.
//
//	Return :		BOOL
//						TRUE, if file passed validation.
//                      FALSE, if file failed validation.
//
//	Parameters :	none.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
*********************************************/
BOOL CSchedVal::Validate_RTCS_SET_File ()
{
	BOOL eReturn_Status = TRUE;

#ifndef _NO_PROGRESS
	int nNumPrologLines = GetPrologLineCount();
	int nLineCount      = GetLineCount() + nNumPrologLines;
	CString strFormat = _T("RTCS Set file validation in progress. \n")
						_T("Currently on line %d of %d\n");
	CProgressWnd wndProgress(AfxGetMainWnd(), _T("Validation"), TRUE);
	wndProgress.SetRange(0, nLineCount, FALSE);
#endif

	CWaitCursor wait;   // display wait cursor
	CString strLine;
	CString strText;
	int     nNumRTCSDefined = 0;
	bool    bRTCSDefinedError = false;
	while ( GetFileDirNext (strLine) == TRUE){

#ifndef _NO_PROGRESS
		int nLineNum = GetCurrentFileLineNum () + nNumPrologLines + 1;
		strText.Format(strFormat, nLineNum, nLineCount);
		wndProgress.SetText (FALSE, strText);
		wndProgress.SetPos (nLineNum, FALSE);
#endif
		CString strLineTrimmed(strLine);
		strLineTrimmed.TrimLeft ();
		strLineTrimmed.TrimRight ();
		strLineTrimmed.MakeUpper ();
		strLine.TrimLeft();
		strLine.TrimRight();

		// Validate line.
		if (!strLineTrimmed.IsEmpty() && ctchCOMMENT != strLineTrimmed[0]){
			if (ScanLine(strLine, ctstrInclude) == TRUE){
				if(Validate_Include_Dir(strLine) != TRUE){
					eReturn_Status = FALSE;
				} else {
					nNumRTCSDefined++;
				}
//				int nTemp = GetAppRegSC()->m_nNumRTCS[GetAppRegSC()->m_nDefault];
				if ((nNumRTCSDefined > (GetAppRegSC()->m_nNumRTCS[GetAppRegSite()->m_nDefaultSC]))
					&& !bRTCSDefinedError){
						bRTCSDefinedError = true;
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 75, strLine);
				}
			}
		}

		#ifndef _NO_PROGRESS
			wndProgress.PeekAndPump(false);
			if (wndProgress.Cancelled()){
				break;
			}
		#endif
	}

	#ifndef _NO_PROGRESS
		// sleep(nctSLPTIME);
	#endif

	return eReturn_Status;
}

/****************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-26-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function:	CSchedVal::Validate_RTCS_PRC
//
//	Description :  This routine validates a RTCS procedure file.
//
//	Return :	 BOOL: TRUE is success, FALSE is fail.
//
//	Parameters : none
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
****************************************/
BOOL CSchedVal::Validate_RTCS_PRC_File ()
{
	BOOL eReturn_Status = TRUE;
	CWaitCursor wait;   // display wait cursor
	CString strLine;
	bool bDone = false;
	int nIndex = 0;
	while (!bDone){
		// Verify the first command is a no_op command.
 		int nRtnLine = 0;
		strLine = GetDirLine(nIndex, nRtnLine);
		int nPos = 0;
		if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
			strLine.Delete(nPos, strLine.GetLength() - nPos);
		}

		CString strLineTrimmed(strLine);
		strLineTrimmed.TrimLeft ();
		strLineTrimmed.TrimRight ();
		strLineTrimmed.MakeUpper ();
		strLine.TrimLeft();
		strLine.TrimRight();

		if ((!strLineTrimmed.IsEmpty()) && (ctchCOMMENT != strLineTrimmed[0])) {
			bDone = true;
			if(ScanLine(strLine, ctstrCMD) == TRUE){
				if ((!CheckForNoOpCmd(strLine)) && (!CheckForMovTopPtrCmd(strLine))) {
					m_NewGblValData.WriteValidationMsg(nIndex, 72);
					eReturn_Status = FALSE;
				}
			} else {
				m_NewGblValData.WriteValidationMsg(nIndex, 72);
				eReturn_Status = FALSE;
			}
		}
		nIndex += nRtnLine;
	}

	while (GetFileDirNext(strLine)){

		int nPos = 0;
		if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
			strLine.Delete(nPos, strLine.GetLength() - nPos);
		}

		CString strLineTrimmed(strLine);
		bool bWaitDirFound = false;
		strLineTrimmed.TrimLeft ();

		if (strLineTrimmed.IsEmpty()){
			// Empty line
		} else if (ctchCOMMENT == strLineTrimmed[0]){
			// Comment line
			// Test for #ground begin & #ground end directives.
			// Test for #activity directive.
			CString strTokenBegin(ctstrGROUND);
			strTokenBegin += _T(' ');
			strTokenBegin += ctstrBEGIN;
			CString strTokenEnd(ctstrGROUND);
			strTokenEnd += _T(' ');
			strTokenEnd += ctstrEND;
			if (0 == strLineTrimmed.Find(strTokenBegin)){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 35, strLine);
			} else if (0 == strLineTrimmed.Find(strTokenEnd)){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 35, strLine);
				eReturn_Status = FALSE;
			}
		} else {
			strLine.MakeUpper ();
			m_bCurrTimeFound = false;
			if (0 == strLineTrimmed.Find(ctstrWAIT)){
				if (!Validate_Wait_Dir(strLineTrimmed)){
					eReturn_Status = FALSE;
				}
				bWaitDirFound = true;
			} else {
				bWaitDirFound = false;
			}

			if (m_bCurrTimeFound && (m_CSTCurrTime >= m_CSTPrevTime)){
				m_CSTPrevTime = m_CSTCurrTime;
				m_bPrevTimeFound = true;
			} else if ((m_bCurrTimeFound) && (m_bPrevTimeFound) && (m_CSTCurrTime >= m_CSTPrevTime)) {
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 9, strLine);
				m_bCurrTimeFound = false;
				eReturn_Status = FALSE;
			}

			if (m_bTotalCMDTimeFound){
				if (m_bCurrTimeFound){
					if (m_CSTCurrTime < m_CSTTotalCMDTime){
						CString strTotalCMDTime = m_CSTTotalCMDTime.GetTimeString(TRUE);
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 26, strTotalCMDTime);
						eReturn_Status = FALSE;
						// Reset command timing counter to empty.
						m_bTotalCMDTimeFound = false;
					}
				} /*
				else if (m_bPrevTimeFound){
					if (m_CSTPrevTime < m_CSTTotalCMDTime){
						CString strTotalCMDTime = m_CSTTotalCMDTime.GetTimeString(TRUE);
						WriteValidationMsg(GetCurrentFileLineNum (), 26, strTotalCMDTime);
						eReturn_Status = FALSE;
						// Reset command timing counter to empty.
						m_bTotalCMDTimeFound = false;
					}
				} */
				else {
					// Reset command timing counter to empty.
					m_bTotalCMDTimeFound = false;
				}
			}

			if((!bWaitDirFound) && ScanLine(strLine, ctstrCMD) == TRUE){
				Validate_CMD_Dir(strLine);
			}
		}
	}

	bDone = false;
	nIndex = GetLineCount() - 1;
	while (!bDone){

		// Verify that the last command is a move STO top pointer to zero.
		//	strLine = GetTextAt(nIndex);
		int nRtnLine = 0;
		strLine = GetDirLineAtEOF(nIndex, nRtnLine);
		int nPos = 0;
		if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
			strLine.Delete(nPos, strLine.GetLength() - nPos);
		}

		CString strLineTrimmed(strLine);
		strLineTrimmed.TrimLeft ();
		strLineTrimmed.MakeUpper ();
		if ((!strLineTrimmed.IsEmpty()) && (ctchCOMMENT != strLineTrimmed[0])) {
			bDone = true;
			if(0 == strLineTrimmed.Find(ctstrCMD)){
				if (!CheckForMovTopPtrCmd(strLine)){
					m_NewGblValData.WriteValidationMsg(nIndex, 73);
					eReturn_Status = FALSE;
				}
			} else {
				m_NewGblValData.WriteValidationMsg(nIndex, 73);
					eReturn_Status = FALSE;
			}
		}
		nIndex -= nRtnLine;
	}

	return eReturn_Status;
}

/*
*****************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-26-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function:	CSchedVal::Validate_STORED_File
//
//	Description :
//
//	Return :		BOOL
//						Always returns TRUE.
//
//	Parameters :	none.
//
//	Note :
//			Currently not implemented.
//////////////////////////////////////////////////////////////////////////
*****************************************
BOOL CSchedVal::Validate_STORED_File ()
{
	BOOL eReturnStatus = TRUE;
	return eReturnStatus;
}
*/
/**************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 08-06-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function:	CSchedVal::Validate_RTCS_Dir
//
//	Description :  This function verifies that the called RTCS file exists
//                 and that it has passed RTCS validation.
//
//	Return :		BOOL
//						TRUE	-	SUCCESS
//						FALSE	-	FAIL
//
//	Parameters :
//					CString strLine	- directive line for parsing
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************************************************/
BOOL CSchedVal::Validate_RTCS_Dir (CString strLine)
{
	BOOL eReturn_Status = TRUE;

	// Removing trailing comments
	int nPos = 0;
	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove the RTCS directive.
	nPos = WhereIsToken(strLine, ctstrRTCS);
	CString strRTCSLabel = strLine;
	if (nPos >= 0){
		int nLen = _tcsclen(ctstrRTCS);
		strLine.Delete(nPos, nLen);
		strLine.TrimLeft();
		strLine.TrimRight();

		// Extract the label.
		// The first characters are the RTCS label.
		int nRTCSLabelIndex = strLine.Find(_T("="),0);
		strRTCSLabel = strLine;
		// Extract out the RTCS name
		if (nRTCSLabelIndex > 0){
//			int nLength = strRTCSLabel.GetLength() + 1;
			// strRTCSLabel.Delete(nRTCSLabelIndex, nLength - nRTCSLabelIndex);
			// Remove the RTCS label.
			strRTCSLabel.Delete(0, nRTCSLabelIndex + 1);
			strRTCSLabel.TrimLeft();
			strRTCSLabel.TrimRight();
		}
	}

	// A RTCS directive is called,
	// Check if enough time has elapsed, for previous RTCS to execute.
	//
	if (m_bRTCSWinTimeFound){
		CString strCurrTime = m_CSTCurrTime.GetTimeString(FALSE);
		CString strRTCSWinTime = m_CSTRTCSWinTime.GetTimeString(FALSE);
		if (m_bCurrTimeFound){
			if (m_CSTCurrTime < m_CSTRTCSWinTime){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 24, strLine);
				eReturn_Status = FALSE;
				// Reset command timing counter to empty.
				m_bRTCSWinTimeFound =  false;
			}
		} else if (m_bPrevTimeFound) {
			if ((m_CSTPrevTime < m_CSTRTCSWinTime)){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 24, strLine);
				eReturn_Status = FALSE;
				// Reset command timing counter to empty.
				m_bRTCSWinTimeFound =  false;
			}
		} else {
			// Reset command timing counter to empty.
			m_bRTCSWinTimeFound =  false;
		}
	}

	// Look up the label to get the RTCS proc name.
	CString strRTCSName = LookUpLabel(strRTCSLabel);
	if (!strRTCSName.IsEmpty()){
		// If a proc name is returned validate it.
		if (TRUE != Validate_RTCSName(strRTCSName)){
			eReturn_Status = FALSE;
		}
	}else {
		eReturn_Status = FALSE;
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 76, strRTCSLabel);
	}

	// Reset command gap to zero.
	m_bCMDGapFound = false;

	if (m_eValLoc == GROUND || m_eValLoc == BOTH){

		if ((m_bCurrTimeFound)){
		   m_CSTTotalCMDTime = m_CSTCurrTime;
		} else if ((m_bPrevTimeFound)){
		   m_CSTTotalCMDTime = m_CSTPrevTime;
		}

		m_CSTTotalCMDTime = m_CSTTotalCMDTime + ((float)m_nRetryCnt * m_fCmdTime);
		m_bTotalCMDTimeFound = true;
	}

	return eReturn_Status;
}
/****************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		Validate_CLS_Dir()
//
//	Description : This routine validates the CLS directive.
//                   It checks if the CLS exists and has been validated.
//                   The paths the software searchs are:
//                   The path where the procedure being validated is located.
//                   If this path is not an absolute path, the next location is
//                   based on the search path retireved from the NT environmental
//                   variable ("EPOCH_PROCS").
//
//	Return :		BOOL
//							TRUE	-	SUCCESS
//							FALSE	-	FAIL
//
//	Parameters :	CString strLine	-	Directive line for parsing.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
**********************************************/
BOOL CSchedVal::Validate_CLS_Dir(CString strLine)
{
	BOOL eReturn_Status = TRUE;
	int nPos = 0;

	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove the CLS directive.
	nPos = WhereIsToken(strLine, ctstrCLS);

	CString strCLSName;
	CPassedParam PP_Data;
	if (nPos < 0){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 2);
		eReturn_Status = FALSE;
	} else {

		strLine.Replace(_T('('), _T(' '));
		strLine.Replace(_T(')'), _T(' '));
		strLine.Replace(_T(','), _T(' '));
		int nLen = _tcsclen(ctstrCLS);
		strLine.Delete(nPos, nLen);
		strLine.TrimLeft();
		strLine.TrimRight();

		// The first characters are the CLS name.
		int nCLSNameIndex = strLine.Find(_T(" "),0);
		strCLSName = strLine;
		// Extract out the CLS name
		if (nCLSNameIndex > 0){
			int nLength = strCLSName.GetLength();
			strCLSName.Delete(nCLSNameIndex, nLength - nCLSNameIndex);
			// Remove the CLS name for parsing the start and stop time.
			strLine.Delete(0, nCLSNameIndex);
			strLine.TrimLeft();
			strLine.TrimRight();
			strCLSName.TrimLeft();
			strCLSName.TrimRight();

			int nStartIndex = -1;
			int nStopIndex  = -1;
			PP_Data.Process_PP(strLine);
			int nNumPP = PP_Data.GetParamSize();
			for (int i = 0; i < nNumPP; i++){

				CString strTemp02 = ctstrSTOP;
				strTemp02.TrimLeft();
				strTemp02.TrimRight();

				CString strTemp03 = ctstrSTART;
				strTemp03.TrimLeft();
				strTemp03.TrimRight();

				CString strTemp = PP_Data.GetParamName(i);
				if ( 0 == strTemp02.CompareNoCase(PP_Data.GetParamName(i))){
					CSchedTimeSpan CSTStopTime;
					if (TRUE != CSTStopTime.Create(PP_Data.GetParamValue(i))){
						CString strSubMsg = CSTStopTime.GetLastErrorText();
						CString strErrorMsg;
						strErrorMsg.Format(_T("Error validating CLS stop time; %s"), strSubMsg);
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
						eReturn_Status = FALSE;
					}
					nStopIndex = i;
				} else if (0 == strTemp03.CompareNoCase(PP_Data.GetParamName(i))){
					CSchedTimeSpan CSTStopTime;
					if (TRUE != CSTStopTime.Create(PP_Data.GetParamValue(i))){
						CString strSubMsg = CSTStopTime.GetLastErrorText();
						CString strErrorMsg;
						strErrorMsg.Format(_T("Error validating CLS stop time; %s"), strSubMsg);
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
						eReturn_Status = FALSE;
					}
					nStartIndex = i;
				}
			}

			if (nStopIndex != -1){
				PP_Data.Remove(nStopIndex);
			}

			if (nStartIndex != -1){
				PP_Data.Remove(nStartIndex);
			}

			/*
			int nStopIndex = 0;
			if (-1 != (nStopIndex = WhereIsToken(strLine, ctstrSTOPEQ))){
				CString strStopTime = strLine;
				strStopTime.Delete(0, nStopIndex);
				strStopTime.TrimLeft();
				strStopTime.TrimRight();
				strStopTime.Delete(nStopIndex, ctstrSTOPEQ.GetLength ());  // Remove "stop="
				strLine.Delete(nStopIndex, strLine.GetLength () - nStopIndex);

				CSchedTimeSpan CSTStopTime;
				if (TRUE != CSTStopTime.Create(strStopTime)){
					CString strSubMsg = CSTStopTime.GetLastErrorText();
					CString strErrorMsg;
					strErrorMsg.Format(_T("Error validating CLS stop time; %s"), strSubMsg);
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
					eReturn_Status = FALSE;
				}
			}

			int nStartIndex = 0;
			if (-1 != (nStartIndex = WhereIsToken(strLine, ctstrSTARTEQ))){
				CString strStartTime = strLine;
				strStartTime.Delete(0, nStartIndex);
				strStartTime.TrimLeft();
				strStartTime.TrimRight();
				strStartTime.Delete(0, ctstrSTARTEQ.GetLength ());  // Remove "start="
				strLine.Delete(nStartIndex, strLine.GetLength () - nStartIndex);

				CSchedTimeSpan CSTStartTime;
				if (!CSTStartTime.Create(strStartTime)){
					CString strSubMsg = CSTStartTime.GetLastErrorText();
					CString strErrorMsg;
					strErrorMsg.Format(_T("Error validating CLS start time; %s"), strSubMsg);
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
					eReturn_Status = FALSE;
				}
			}
			*/
		}
	}

	CString strExt(_T(".cls"));
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nCLS])->GetDocString(strExt, CDocTemplate::filterExt);

	// File paths
	COXUNC	UNCCLSPath (m_UNCCLSPath);
	UNCCLSPath.File() = strCLSName + strExt;
	CString strTemp = m_UNCCLSPath.Directory();
	CString strFullPath(UNCCLSPath.Full());

// Check if file exists.  First look in current directory, then cycle through search
// path array, load data, check if valid prolog, and if valid location.

//	eValidLoc eVLoc = INVALID_TYPE;
//	BOOL eSuccessful = FALSE;
	CFileException Exception;
	eReturn_Status = FALSE;

	CTextFile CLS;
	if (TRUE != CLS.Load(strFullPath, CTextFile::RO, &Exception)){
		COXUNC  UNCSearchPath;
		int nMaxNumSearchPaths = m_SearchPathArray.GetSize();
		int nNumPaths = 0;
//		bool bDone = false;
		while ((TRUE != eReturn_Status) && (nNumPaths < nMaxNumSearchPaths)){
			UNCSearchPath.File () = m_SearchPathArray[nNumPaths] + strCLSName + strExt;
			UNCSearchPath.Full();

			if (TRUE ==	CLS.Load(UNCSearchPath.Full(), CTextFile::RO, &Exception)){
				eReturn_Status = TRUE;
			} else {
				nNumPaths++;
			}
		}

		// Save the last filepath for printing out error messges.
		strFullPath = UNCSearchPath.Full();
		if (TRUE != eReturn_Status){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
			// GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
		}
	} else {
		eReturn_Status = TRUE;
	}

	if (TRUE == eReturn_Status){
		int nNumPPNestCLS = CLS.GetNumberPassedParams();
		int nNumPPCLS	  = PP_Data.GetParamSize();
		if (nNumPPCLS != nNumPPNestCLS){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 12, strFullPath);
		}
		eValidLoc eVLoc		   = INVALID_TYPE;
		eValidStatus  eVStatus = INVALID_STATUS;

		//SSGS-1206: Extract exectime if present in the prolog
		CSchedTimeSpan cSt(((CLS.GetProlog())->GetExectime())->GetTimeSpan(), 0);
		if (m_bCurrTimeFound){
			m_CSTCLSWinTime = m_CSTCurrTime + cSt;
			m_bCLSWinTimeFound = true;
		} else if (m_bPrevTimeFound){
			m_CSTCLSWinTime = m_CSTPrevTime + cSt;
			m_bCLSWinTimeFound = true;
		}
//		BOOL bORSet = CLS.IsOverRideSet();
		// Check if file is valid and retrieve the execution location
		if (TRUE != CLS.IsFileValid(eVStatus, eVLoc)){
			// Priolog is invalid.
			eReturn_Status = FALSE;
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 1, strFullPath);
		} else if (TRUE == CLS.IsOverRideSet()){
			// Override directive in file is set.
			CString strMsg(_T("Validation status was overridden!"));
			CString strTitle;
			strTitle.Format(_T("%s %s"), strMsg, strFullPath);
		//	GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strTitle, 0);
		} else if (FAIL == eVStatus ){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 11, strFullPath);
			eReturn_Status = FALSE;
		} else if (WARNING == eVStatus){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 37, strFullPath);
			eReturn_Status = FALSE;
		}
		if (eVLoc != BOTH  && m_eValLoc != eVLoc){
			// Verify that execution locations match
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 25, strFullPath);
			eReturn_Status = FALSE;
		}

		if (TRUE != CLS.Close(CTextFile::NOWRITE, &Exception)){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. %s"), strErrorMsg, strFullPath);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	}

	return eReturn_Status;
}

/*******************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		Validate_Next_Dir()
//
//	Description :	This routine validates the NextSched directive.
//
//	Return :		BOOL
//						TRUE	-	SUCCESS
//						FALSE	-	FAIL
//
//	Parameters :
//					CString strLine	-	directive line for parsing.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
****************************************************/
BOOL CSchedVal::Validate_NEXT_Dir(CString strLine)
{
	BOOL eReturnStatus = TRUE;

//	strLine.MakeUpper();
	// Removing trailing comments
	int nPos = 0;
	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove the SCHEDX directive.
	CString strToken(ctstrSCHEDMON);
	nPos = WhereIsToken(strLine, strToken);
	if (nPos >= 0){
		strLine.Delete(nPos, strToken.GetLength());
		strLine.TrimLeft();
		strLine.TrimRight();
	}

	// Remove the NEXTSCHED directive.
	strToken = ctstrNEXTSCHED;
	nPos = WhereIsToken(strLine, strToken);
	if (nPos >= 0){
		strLine.Delete(nPos, strToken.GetLength());
		strLine.TrimLeft();
		strLine.TrimRight();
	}

	if (0 == WhereIsToken(strLine, ctstrGO)){
		// If NextSchedx is not present before NextSched Go, issue a warning.
		m_bNextSchedGo = true;
		if (!m_bNextSchedSet){
			// Set is not present before NextSched GO.
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 61);
		}
	} else if (0 == WhereIsToken(strLine, ctstrSET)){
		m_bNextSchedSet = true;
	}

	return eReturnStatus;
}

/******************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		Validate_Start_Dir()
//
//	Description :	This routine extracts out the start directive.  Verifies
//                  that the STOL proc called exists, is valid, and the number
//                  of passed parameters is correct.
//
//	Return :		BOOL
//
//	Parameters :	CString strLine; text line containing the start directive.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
***************************************************/
BOOL CSchedVal::Validate_START_Dir(CString strLine)
{
	BOOL eReturn_Status = TRUE;
	int nPos= 0;
	CString strProcName;
	CPassedParam PP_Data;

	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove directive
	CString strUpperCaseCMDLine(strLine);
	strUpperCaseCMDLine.MakeUpper();
	nPos = (strUpperCaseCMDLine.Find(ctstrSTART, 0));
	// if (nPos < 0) nPos = 0;

	if (nPos < 0){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 29);
		eReturn_Status = FALSE;
	} else {
		int nLen = _tcsclen(ctstrSTART);
		strLine.Delete(0, nPos + nLen);
		strLine.TrimLeft();
		strLine.TrimRight();

		int nProcNameIndex = -1;
		strProcName = strLine;
		// First look for a "(".
		if (-1 == (nProcNameIndex = strLine.Find(_T('('), 0))){
			// If a "(" is not found, then look for space.
			nProcNameIndex = strLine.Find(_T(' '), 0);
		}

		// Extract out the Proc name
		if (nProcNameIndex > 0){
			int nLength = strProcName.GetLength();
			strProcName.Delete(nProcNameIndex, nLength - nProcNameIndex);
			// Remove the CP name for parsing the passed parameters.
			strLine.Delete(0, nProcNameIndex);
			strLine.TrimLeft();
			strLine.TrimRight();
			strProcName.TrimLeft();
			strProcName.TrimRight();
		}
	}

	CString strExt(_T(".prc"));
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nSTOL])->GetDocString(strExt, CDocTemplate::filterExt);

	CTextFile Prc;

//	m_UNCInputFileName.Full();
	m_UNCPrcPath.Full();
	CString strTemp = m_UNCPrcPath.Directory();
//	CString strTemp = m_UNCInputFileName.Directory();

	// File paths
	COXUNC	UNCPrcPath (m_UNCPrcPath);
//	COXUNC	UNCPrcPath (m_UNCInputFileName);
	UNCPrcPath.File() = strProcName + strExt;
	CString strFullPath(UNCPrcPath.Full());

	// Check if file exists.  First look in current directory, then cycle through search
	// path array, load data, check if valid prolog, and if valid location.
	
//	BOOL eSuccessful = FALSE;
	eReturn_Status = FALSE;
	CFileException Exception;
	if (TRUE != Prc.LoadCaseSensitive(UNCPrcPath.Full(), CTextFile::RO, &Exception)){

		int nMaxNumSearchPaths = m_SearchPathArray.GetSize();
		int nNumPaths = 0;
//		bool bDone = false;
		while ((TRUE != eReturn_Status) && (nNumPaths < nMaxNumSearchPaths)){
			// UNCSearchPath.Directory() = m_SearchPathArray[nNumPaths];
			COXUNC  UNCSearchPath;
			UNCSearchPath.File () = m_SearchPathArray[nNumPaths] + strProcName + strExt;
			UNCSearchPath.Full();
			strFullPath = UNCSearchPath.Full();
			
			if (TRUE ==	Prc.LoadCaseSensitive(strFullPath, CTextFile::RO, &Exception)){
				eReturn_Status = TRUE;
			} else {
				nNumPaths++;
			}
		}

		// Save the last filepath for printing out error messges.
		// strFullPath = UNCSearchPath.Full();
		if (TRUE != eReturn_Status){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	} else {
		eReturn_Status = TRUE;
	}

	eValidLoc eVLoc = INVALID_TYPE;
	eValidStatus  eVStatus = INVALID_STATUS;
//	BOOL bORSet = Prc.IsOverRideSet();
//	BOOL bValid = Prc.IsFileValid(eVStatus, eVLoc);
	if (TRUE == eReturn_Status){
		// Check if file is valid and retrieve the execution location
		if (TRUE != Prc.IsFileValid(eVStatus, eVLoc)){
			// Prolog is invalid.
			eReturn_Status = FALSE;
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 1, strFullPath);
		} else if (TRUE == Prc.IsOverRideSet()){
			// Override directive in file is set.
			CString strMsg(_T("Validation status was overridden!"));
			CString strTitle;
			strTitle.Format(_T("%s %s"), strMsg, strFullPath);
//			GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strTitle, 0);
		} else if (FAIL == eVStatus){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 11, strFullPath);
			eReturn_Status = FALSE;
		} else if (WARNING == eVStatus){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 37, strFullPath);
			eReturn_Status = FALSE;
		}

		if (eVLoc != BOTH  && m_eValLoc != eVLoc){
			// Verify that execution locations match
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 25, strFullPath);
			eReturn_Status = FALSE;
		}

		if (TRUE != Prc.CheckPassedParams(strLine)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 12, strFullPath);
			eReturn_Status = FALSE;
		}

		CSchedTimeSpan cSt(((Prc.GetProlog())->GetExectime())->GetTimeSpan(), 0);
		if (m_bCurrTimeFound){
			m_CSTPrcWinTime = m_CSTCurrTime + cSt;
			m_bPrcWinTimeFound = true;
		} else if (m_bPrevTimeFound){
			m_CSTPrcWinTime = m_CSTPrevTime + cSt;
			m_bPrcWinTimeFound = true;
		}

		bool bBeginFound = false;
		int nRtnLine = 0;
		int nLine = 0;
		int nMaxLineCount = Prc.GetLineCount();

		while (nLine < nMaxLineCount){
			CString strPrcLine;
			strPrcLine = Prc.GetDirLine(nLine, nRtnLine);

			CString strLineTrimmed(strPrcLine);
//			bool bWaitDirFound = false;
			strLineTrimmed.TrimLeft ();
			CString strLineTrimmedUpper(strLineTrimmed);
			strLineTrimmedUpper.MakeUpper();

			if (strLineTrimmed.IsEmpty()){
				// Empty line
			}else if ((0 == strLineTrimmedUpper.Find(ctstrWRITE)) && ScanLine(strLineTrimmedUpper, ctstrWRITE)){
				// write statement ignore line
			}else if (ctchCOMMENT == strLineTrimmed[0]){
				// Comment
				/* Scheduler directive
				// Test for ##ground begin & ##ground end directives.
				// Test for ##activity directive.  */
				CString strTokenBegin(ctstrGROUND);
				strTokenBegin += _T(' ');
				strTokenBegin += ctstrBEGIN;
				CString strTokenEnd(ctstrGROUND);
				strTokenEnd += _T(' ');
				strTokenEnd += ctstrEND;
				strLineTrimmed.MakeUpper();
				if (0 == strLineTrimmed.Find(strTokenBegin)){
					if (bBeginFound){
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 0);
						eReturn_Status = FALSE;
					}else {
						bBeginFound = true;
					}
				} else if (0 == strLineTrimmed.Find(strTokenEnd)){
					if (!bBeginFound){
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 16);
						eReturn_Status = FALSE;
					}else {
						bBeginFound = false;
					}
				}
			} else {
				strPrcLine.MakeUpper ();

				if(ScanLine(strPrcLine, ctstrCMD) == TRUE){
					Validate_CMD_Dir(strPrcLine);

					if ((!bBeginFound) && (m_eValLoc == ONBOARD || m_eValLoc == BOTH)){
						CheckForLocalVariables(strPrcLine, Prc);
					}

					// Reset command gap to zero.
					m_bCMDGapFound = false;

					if (m_eValLoc == GROUND || m_eValLoc == BOTH){

						if ((m_bCurrTimeFound)){
						   m_CSTTotalCMDTime = m_CSTCurrTime;
						} else if ((m_bPrevTimeFound)){
						   m_CSTTotalCMDTime = m_CSTPrevTime;
						}

						m_CSTTotalCMDTime = m_CSTTotalCMDTime + ((float)m_nRetryCnt * m_fCmdTime);
						m_bTotalCMDTimeFound = true;
					}
				}
			}
			nLine += nRtnLine;
		}
		if (TRUE != Prc.Close(CTextFile::NOWRITE, &Exception)){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	}
	return eReturn_Status;
}

/*********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSchedVal::Validate_ScanData_Dir()
//
//
//	Return :		BOOL
//
//	Parameters :	CString strLine
//
//	Note :
//
//  Description:	Routine to validate a schedule file line containing
//                      a SCANDATA directive.
//                      The CString is parsed into it's individual tokens.
//                      A space signifies the break between tokens.
//
//
// Revision history:	Frederick Shaw 1999
//                      Initial release.
//
// Note:
//
//////////////////////////////////////////////////////////////////////
************************************************/
BOOL CSchedVal::Validate_ScanData_Dir(CString strLine){

	BOOL eReturn_Status = TRUE;

	int nPos = 0;
	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Check if line contains SCAN directive.
	nPos = WhereIsToken(strLine, ctstrSCANDATA);
	if (nPos < 0){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 17);
		eReturn_Status = FALSE;
	} else {

		CString strTrimmedLine(strLine);
		int nLen = _tcsclen(ctstrSCANDATA);

		// Remove directive
		strLine.Delete(0, nPos + nLen);
		strLine.TrimLeft();
		strLine.TrimRight();
		strLine.MakeUpper();

		// Parse remaining line.
		// Store keywords and values.
		CPassedParam PP_Data;

		if (TRUE != PP_Data.Process_PP(strLine)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 31, strLine);
			eReturn_Status = FALSE;
		} else {

			bool bInstrFound = false;
			bool bDurFound   = false;

			CString strParamName;
			CString strInstrValue;
			CString strDurValue;
			CString strSideValue;
			CString strModeValue;
			CString strPriorityValue;
			CString strBBCalValue;

			// Cycle through the parameters looking for duration and instrument.
			for (int i = 0; i < PP_Data.GetParamSize(); i++){
				strParamName = PP_Data.GetParamName(i);

				if (0 == strParamName.CompareNoCase(ctstrINSTRUMENT)){ // Keyword INSTRUMENT
					strInstrValue = PP_Data.GetParamValue(i);
					bInstrFound = true;
				} else if (0 == strParamName.CompareNoCase(ctstrDUR)){ // Keyword Duration
					strDurValue = PP_Data.GetParamValue(i);
					bDurFound = true;
				} else if (0 == strParamName.CompareNoCase(ctstrSIDE)){ // Keyword Duration
					strSideValue = PP_Data.GetParamValue(i);
				} else if (0 == strParamName.CompareNoCase(ctstrMODE)){ // Keyword Duration
					strModeValue = PP_Data.GetParamValue(i);
				} else if (0 == strParamName.CompareNoCase(ctstrBBCAL)){ // Keyword Duration
					strBBCalValue = PP_Data.GetParamValue(i);
				} else if (0== strParamName.CompareNoCase(ctstrPRIORITY)){ // Keyword Priority
					strPriorityValue = PP_Data.GetParamValue(i);
				}
			}

			if ((0 != strInstrValue.CompareNoCase(m_strInstrValue))){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 5, strInstrValue);
				eReturn_Status = FALSE;
				bInstrFound = false;
			}

			if (0 == strInstrValue.CompareNoCase(ctstrIMGR)){ // Value: imager
				if (bDurFound ){
					if ( !strDurValue.IsEmpty()){
						CSchedTimeSpan CSTDur;
						if (TRUE != CSTDur.Create(strDurValue)){
							CString strSubMsg = CSTDur.GetLastErrorText();
							CString strErrorMsg;
							strErrorMsg.Format(_T("Error validating imager duration time; %s"), strSubMsg);
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
							eReturn_Status = FALSE;
						} else {
							 if (m_bCurrTimeFound){
								// Current Time Tag is valid
								if (m_CSTCurrTime < m_CSTIMGRWinTime){
									m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (),32);
									eReturn_Status = FALSE;
								} else {
									m_CSTIMGRWinTime = m_CSTCurrTime + CSTDur;
								}
							} else if (m_bPrevTimeFound){ // Else add to previous time tag
								if (m_CSTPrevTime < m_CSTIMGRWinTime){
									m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 32);
									eReturn_Status = FALSE;
								} else {
									m_CSTIMGRWinTime =  m_CSTPrevTime + CSTDur;
								}
							} else {
								m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 4, strDurValue);
								eReturn_Status = FALSE;
							}
						}
					}
				}
			} else if (0 == strInstrValue.CompareNoCase(ctstrSDR)){
				if (bDurFound ){
					if ( !strDurValue.IsEmpty()){
						CSchedTimeSpan CSTDur;
						if (TRUE != CSTDur.Create(strDurValue)){
							CString strSubMsg = CSTDur.GetLastErrorText();
							CString strErrorMsg;
							strErrorMsg.Format(_T("Error validating sounder duration time; %s"), strSubMsg);
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
							// WriteValidationMsg(GetCurrentFileLineNum (), 7, strDurValue);
							eReturn_Status = FALSE;
						} else {
							 if (m_bCurrTimeFound){
								// Current Time Tag is valid
								if (m_CSTCurrTime < m_CSTSDRWinTime){
									m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (),32);
									eReturn_Status = FALSE;
								} else {
									m_CSTSDRWinTime = m_CSTCurrTime + CSTDur;
								}
							} else if (m_bPrevTimeFound){ // Else add to previous time tag
								if (m_CSTPrevTime < m_CSTSDRWinTime){
									m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 32);
									eReturn_Status = FALSE;
								} else {
									m_CSTSDRWinTime =  m_CSTPrevTime + CSTDur;
								}
							} else {
								m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 4, strDurValue);
								eReturn_Status = FALSE;
							}
						}
					}
				}

				if ((!strModeValue.IsEmpty())
					&& (0 != strModeValue.CompareNoCase(ctstrSINGLE_0_1))){
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 52, strModeValue);
					eReturn_Status = FALSE;
				}

				if (!strBBCalValue.IsEmpty()
					&& (0 == strBBCalValue.CompareNoCase(ctstrSUPP))){
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 23, strBBCalValue);
					eReturn_Status = FALSE;
				}
			}

			// Need to convert to minus_x or plus_x
			if (!m_strSideValue.IsEmpty()){
				if (0 == m_strSideValue.CompareNoCase(ctstrEAST)){
					if (0 == strSideValue.CompareNoCase(ctstrPLUS_X)){
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 53, strSideValue);
						eReturn_Status = FALSE;
					}
				} else {
					if (0 == strSideValue.CompareNoCase(ctstrMINUS_X)){
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 53, strSideValue);
						eReturn_Status = FALSE;
					}
				}
			}
		}
	}
	return eReturn_Status;
}

/******************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSchedVal::Validate_Scan_Dir()
//
//
//	Return :		BOOL
//
//	Parameters :	CString
//
//  Description:	This routine validates the SCAN update block.  First
//                  the scan directive is validated.  The parameters
//                  validated for SCAN are side and instrument.  The lines
//                  following the scan directive are retrieved and checked.
//                  It is assumed that the scan update block if present
//                  has been inserted by the software and follows a fixed
//                  format.
//
//
// Revision history:	Frederick Shaw 1999
//                      Initial release.
//
// Note:
//
//////////////////////////////////////////////////////////////////////
*****************************************/
BOOL CSchedVal::Validate_Scan_Dir(CString strLine){

	BOOL eReturn_Status = TRUE;

	// Reset command gap to zero.
	m_bCMDGapFound = false;

	int nPos = 0;
	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Check if line contains SCAN directive.
	nPos = WhereIsToken(strLine, ctstrSCAN);
	if (nPos < 0){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 17);
		eReturn_Status = FALSE;
	}

	if (FALSE != eReturn_Status) {

		// Remove directive
		strLine.Delete(nPos, ctstrSCAN.GetLength());
		strLine.TrimLeft();
		strLine.TrimRight();
		strLine.MakeUpper();

		// Parse remaining line.
		// Store keywords and values.
		CPassedParam PP_Data;

		if (TRUE != PP_Data.Process_PP(strLine)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 31, strLine);
			eReturn_Status = FALSE;
		} else {
			bool bInstrFound     = false;
			CString strBBCALValue;
			CString strModeValue;
			CString strSideValue;

			// Cycle through the parameters looking for BBCal, side, and instrument.
			for (int i = 0; i < PP_Data.GetParamSize(); i++){
				CString strParamName = PP_Data.GetParamName(i);

				// Look for instrument value.
				if (0 == strParamName.CompareNoCase(ctstrINSTRUMENT)){ // Keyword INSTRUMENT
					m_strInstrValue = PP_Data.GetParamValue(i);
					bInstrFound = true;
				}

				// Check for BBCAL: [do_not_suppress, suppress_bbcal]
				if (0 == strParamName.CompareNoCase(ctstrBBCAL)){ // Keyword BBCAL
					strBBCALValue = PP_Data.GetParamValue(i);
				}

				// Save Side: [ EAST, WEST, OATS]
				if (0 == strParamName.CompareNoCase(ctstrSIDE)){ // Keyword SIDE
					strSideValue = PP_Data.GetParamValue(i);
				}

				// Check for Sounder Mode
				if (0 == strParamName.CompareNoCase(ctstrMODE)){ // Keyword SIDE
					strModeValue = PP_Data.GetParamValue(i);
				}
			}

			if ((0 != m_strInstrValue.CompareNoCase(ctstrIMGR))
				&& (0 != m_strInstrValue.CompareNoCase(ctstrSDR))){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 33, m_strInstrValue);
				eReturn_Status = FALSE;
				bInstrFound = false;
			}

			// Check if side is correct for intrusions.
			CheckForIntrusion();
			if (!m_strSideValue.IsEmpty()){
				if (0 == strSideValue.CompareNoCase(m_strSideValue)){
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 53, strSideValue);
					eReturn_Status = FALSE;
				}
			}

			if (0 == m_strInstrValue.CompareNoCase(ctstrSDR)){
				if ((!strBBCALValue.IsEmpty())
					&& (0 == strBBCALValue.CompareNoCase(ctstrSUPP))){
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 23, strBBCALValue);
					eReturn_Status = FALSE;
				}

				if ((!strModeValue.IsEmpty())
					&& (0 != strModeValue.CompareNoCase(ctstrSINGLE_0_1))){
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 52, strModeValue);
					eReturn_Status = FALSE;
				}
			}

			// Reset command gap to zero.
			m_bCMDGapFound = false;

			if (m_eValLoc == GROUND || m_eValLoc == BOTH){

				if ((m_bCurrTimeFound)){
				   m_CSTTotalCMDTime = m_CSTCurrTime;
				} else if ((m_bPrevTimeFound)){
				   m_CSTTotalCMDTime = m_CSTPrevTime;
				}

				m_CSTTotalCMDTime = m_CSTTotalCMDTime + ((float)m_nRetryCnt * m_fCmdTime);
				m_bTotalCMDTimeFound = true;
			}
		}
	}

	bool bContinue = false;  // Continue while in SCAN update block
	// if in SCAN update block, next directive is SCANDATA
	int nLine =  GetCurrentFileLineNum();
	int nRtnLine = 0;
	strLine = GetDirLine(nLine, nRtnLine);
	CString strLineTrimmed;
	strLineTrimmed = strLine;
	strLineTrimmed.TrimLeft ();
	strLineTrimmed.TrimRight();
	strLineTrimmed.MakeUpper();

	if(0 == strLineTrimmed.Find(ctstrSCANDATA)){
//		nLine += nRtnLine;
		SetCurrentFileLineNum(nLine + 1);
		bContinue = true;
		if(!Validate_ScanData_Dir(strLineTrimmed)){
			eReturn_Status = FALSE;
		}
	}

	//if in SCAN update block, next directive is ##GROUND BEGIN
	if (bContinue){
		nLine += nRtnLine;
		strLine = GetDirLine(nLine, nRtnLine);
		strLineTrimmed = strLine;
		strLineTrimmed.TrimLeft ();
		strLineTrimmed.TrimRight();
		strLineTrimmed.MakeUpper();
		CString strTokenBegin(ctstrGROUND);
		strTokenBegin += _T(' ');
		strTokenBegin += ctstrBEGIN;
		if (0 == strLineTrimmed.Find(strTokenBegin)){
			SetCurrentFileLineNum(nLine + 1);
		} else {
			bContinue = false;
		}
	}

	// Next directive is START
	// Need to keep scanning while start directives are found.
	if (bContinue){
		bool bDone = false;
		while (!bDone){
			nLine += nRtnLine;
			strLine = GetDirLine(nLine, nRtnLine);
			strLineTrimmed = strLine;
			strLineTrimmed.TrimLeft ();
			strLineTrimmed.TrimRight();
			strLineTrimmed.MakeUpper();

			if (0 == strLineTrimmed.Find(ctstrSTART)){
				SetCurrentFileLineNum(nLine + 1);
				if(!Validate_ScanStart_Dir(strLine)){
					eReturn_Status = FALSE;
				}
			} else {
				bDone = true;
			}
		}
	}

	// Next directive is ##GROUND END
	if (bContinue){
		strLineTrimmed = strLine;
		strLineTrimmed.TrimLeft ();
		strLineTrimmed.TrimRight();
		strLineTrimmed.MakeUpper();
		CString strTokenEnd(ctstrGROUND);
		strTokenEnd += _T(' ');
		strTokenEnd += ctstrEND;
		if (0 == strLineTrimmed.Find(strTokenEnd)){
			nLine += nRtnLine;
			SetCurrentFileLineNum(nLine);
		}
	}

	m_strSideValue.Empty();
	m_strInstrValue.Empty();
	return eReturn_Status;
}

/*****************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 03-02-02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CSchedVal::Validate_ScanStart_Dir
//
//  Description:	This routine validates the START directive in the SCAN
//					update block.
//
//	Return :		BOOL
//							TRUE	-	SUCCESS
//							FALSE	-	FAIL
//
//	Parameters :	CString strLine	-	STOL directive line
//
// Revision history:	Frederick Shaw 2002
//                      Initial release.
//
// Note:
//
//////////////////////////////////////////////////////////////////////
*****************************************/
BOOL CSchedVal::Validate_ScanStart_Dir(CString strLine){

	BOOL eReturn_Status = TRUE;
	int nPos= 0;
	CString strProcName;

	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove directive
	CString strUpperCaseStartLine(strLine);
	strUpperCaseStartLine.MakeUpper();
	nPos = (strUpperCaseStartLine.Find(ctstrSTART, 0));
	// if (nPos < 0) nPos = 0;

	if (nPos < 0){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 29);
		eReturn_Status = FALSE;
	} else {
		int nLen = _tcsclen(ctstrSTART);
		strProcName = strLine;
		strProcName.Delete(0, nPos + nLen);
		strProcName.TrimLeft();
		strProcName.TrimRight();

		int nProcNameIndex = -1;
		// First look for a "(".
		if (-1 == (nProcNameIndex = strProcName.Find(_T('('), 0))){
			// If a "(" is not found, then look for space.
			nProcNameIndex = strProcName.Find(_T(' '), 0);
		}

		// Extract out the Proc name
		if (nProcNameIndex > 0){
			int nLength = strProcName.GetLength();
			strProcName.Delete(nProcNameIndex, nLength - nProcNameIndex);
			strProcName.TrimLeft();
			strProcName.TrimRight();
		}

		/* Check what stol proc was called.  */

		// File paths
		COXUNC	UNCPrcPath (strProcName);
		CString strPrcFileName(UNCPrcPath.File());
		CString strFrameMS = GetAppRegSC()->m_strFrameMiniSched[GetAppRegSite()->m_nDefaultSC];
		CString strFrameIOMS = GetAppRegSC()->m_strFrameIOMiniSched[GetAppRegSite()->m_nDefaultSC];

		if (!strFrameMS.IsEmpty() && (0 == strPrcFileName.CompareNoCase(strFrameMS))){
			Validate_FrameGrd_Proc(strLine);
		} else if (!strFrameIOMS.IsEmpty() && (0 == strPrcFileName.CompareNoCase(strFrameIOMS))){
			Validate_FrameObj_Proc(strLine);
		}
	}
	return eReturn_Status;
}

/****************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSchedVal::Validate_Star_Dir()
//
//	Return :		BOOL
//
//	Parameters :	CString strLine	-
//						STOL directive line
//
//  Description:	Routine to validate a schedule file line containing
//                      a Star directive.  It is assumed that any time tags
//                      contained on the line have been removed before being
//                      passed into the routine.  The CString is parsed into
//                      it's individual tokens.  A space signifies the break
//                      between tokens. There must be at least four tokens
//                      or the routine writes an error message out to the
//                      validation report file and returns.
//
//                      The first token[0] contains the directive.
//						STAR INSTRUMENT=<imager|sounder> DUR=<time> MAX=<num>
//
//
//
// Revision history:	Frederick Shaw 1999
//                      Initial release.
//	Note :
//
//
//////////////////////////////////////////////////////////////////////
**************************************************/
BOOL CSchedVal::Validate_Star_Dir(CString strLine){

	BOOL eReturn_Status = TRUE;
	CPassedParam PP_Data;

	// Reset command gap to zero.
	m_bCMDGapFound = false;

	int nPos = 0;
	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos,strLine.GetLength() - nPos);
	}

	CString strLineUpperCase = strLine;
	strLineUpperCase.MakeUpper();
	nPos = (strLineUpperCase.Find(ctstrSTAR, 0));
	if (nPos < 0){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 18);
		eReturn_Status = FALSE;
	} else {
		// Remove directive
		strLine.Delete(0, nPos + ctstrSTAR.GetLength());
		strLine.TrimLeft();
		strLine.TrimRight();

		if (TRUE != PP_Data.Process_PP(strLine)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 31, strLine);
			eReturn_Status = FALSE;
		} else {
			bool bDurFound   = false;
			bool bInstrFound = false;
			for ( int i = 0; i < PP_Data.GetParamSize(); i++){
				CString strParamName = PP_Data.GetParamName(i);
				// strParamName.MakeUpper();

				/* Look for instrument value.  */
				if (0 == strParamName.CompareNoCase(ctstrINSTRUMENT)){ // Keyword INSTRUMENT
					m_strInstrValue = PP_Data.GetParamValue(i);
					bInstrFound = true;
				}

				if( 0 == strParamName.CompareNoCase(ctstrDUR)){
					bDurFound   = true;
					// Found star duration, need to validate time tag.
					// What remains is the time tag.
					CString strTimeTag = PP_Data.GetParamValue(i);
					if ( !strTimeTag.IsEmpty()){
						CSchedTimeSpan CSTDur;
						if (TRUE != CSTDur.Create(PP_Data.GetParamValue(i))){
							CString strSubMsg = CSTDur.GetLastErrorText();
							// CString strErrorMsg;
							// strErrorMsg.Format(_T("validating duration time; %s"), strSubMsg);
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strSubMsg, 2);
							// WriteValidationMsg(GetCurrentFileLineNum (), 7, strTimeTag );
							eReturn_Status = FALSE;
						} else {
							if (m_bCurrTimeFound){ // Add to current time tag
								m_CSTSTARWinTime = m_CSTCurrTime + CSTDur;
								m_bSTARWinTimeFound =  true;
							} else if (m_bPrevTimeFound){ // Else add to previous time tag
								m_CSTSTARWinTime = m_CSTPrevTime + CSTDur;
								m_bSTARWinTimeFound = true;
							} else {
								m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 4, strTimeTag);
								eReturn_Status = FALSE;
							}
						}
					}
				}
			}

			if ((0 != m_strInstrValue.CompareNoCase(ctstrIMGR))
				&& (0 != m_strInstrValue.CompareNoCase(ctstrSDR))){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 33, m_strInstrValue);
				eReturn_Status = FALSE;
				bInstrFound = false;
			}
		}
	}

	bool bContinue = false;  // Continue while in STAR update block
	int nLine =  GetCurrentFileLineNum();
	int nRtnLine = 0;
//	strLine = GetDirLine(nLine, nRtnLine);
	CString strLineTrimmed = GetDirLine(nLine, nRtnLine);
//	CString strLineTrimmed = strLine;
	strLineTrimmed.TrimLeft ();
	strLineTrimmed.TrimRight();
	strLineTrimmed.MakeUpper();


	// if in STAR update block, next directive is STARDATA
	bool bEmpty = false;
	if(0 == strLineTrimmed.Find(ctstrSTARDATA)){
		nLine += nRtnLine;
		SetCurrentFileLineNum(nLine);
		bContinue = true;
		bEmpty = true;
	}

	//if in STAR update block, next directive is STARINFO
	if (bContinue){
		bool bDone = false;
		bContinue = false;
		while (!bDone){
			// nLine += nRtnLine;
//			CString strLine = GetDirLine(nLine, nRtnLine);
			strLineTrimmed = GetDirLine(nLine, nRtnLine);
//			CString strLineTrimmed = strLine;
			strLineTrimmed.TrimLeft ();
			strLineTrimmed.TrimRight();
			strLineTrimmed.MakeUpper();

			if (0 == strLineTrimmed.Find(ctstrSTARINFO)){
				nLine += nRtnLine;
				SetCurrentFileLineNum(nLine);
				bContinue = true;
				bEmpty = false;
			} else {
				bDone = true;
			}
		}
	}

	// Next directive is ##ground begin star
	if (bContinue){
//		CString strLine = GetDirLine(nLine, nRtnLine);
		CString strLineTrimmed = GetDirLine(nLine, nRtnLine);
		strLineTrimmed.TrimLeft ();
		strLineTrimmed.TrimRight();
		strLineTrimmed.MakeUpper();
		CString strTokenBegin(ctstrGROUND);
		strTokenBegin += _T(' ');
		strTokenBegin += ctstrBEGIN;
		if (0 == strLineTrimmed.Find(strTokenBegin)){
			nLine += nRtnLine;
			SetCurrentFileLineNum(nLine);
		} else {
			bContinue = false;
		}
	} else if (bEmpty){
		m_NewGblValData.WriteValidationMsg(nLine, 38);
	}

	// Next directive is START
	// Need to keep scanning while start directives are found.
	if (bContinue){
		bool bDone    = false;
		bool bOffFlag = false;
		bool bOnFlag  = false;
		int  nSavedLineNum = 0;
		while (!bDone){
			m_strStarBit.Empty();
//			nLine += nRtnLine;
//			strLine = GetDirLine(nLine, nRtnLine);
			strLineTrimmed = GetDirLine(nLine, nRtnLine);
			strLineTrimmed.TrimLeft ();
			strLineTrimmed.TrimRight();
			strLineTrimmed.MakeUpper();

			if (0 == strLineTrimmed.Find(ctstrSTART)){
				nLine += nRtnLine;
				SetCurrentFileLineNum(nLine);
				if(!Validate_StarStart_Dir(strLineTrimmed)){
					eReturn_Status = FALSE;
				}
			} else {
				bDone = true;
			}

			// Need to check starbit is on/off.
			// Last star look must be off.

			if (!m_strStarBit.IsEmpty()){
				if (0 == m_strStarBit.CompareNoCase(ctstrON)){
					if (bOffFlag){
						m_NewGblValData.WriteValidationMsg(nSavedLineNum, 50);
						eReturn_Status = FALSE;
					}
					bOffFlag = false;
					bOnFlag  = true;
					nSavedLineNum = GetCurrentFileLineNum ();
				} else if (0 == m_strStarBit.CompareNoCase(ctstrOFF)){
					if (bOffFlag){
						m_NewGblValData.WriteValidationMsg(nSavedLineNum, 50);
						eReturn_Status = FALSE;
					}
					bOffFlag = true;
					bOnFlag  = false;
					nSavedLineNum = GetCurrentFileLineNum ();
				}
			}
		}
		if (bOnFlag){
			m_NewGblValData.WriteValidationMsg(nSavedLineNum, 51);
			eReturn_Status = FALSE;
		}
	}

	// Next directive is ##GROUND END
	if (bContinue){
//		strLineTrimmed = strLine;
		strLineTrimmed.TrimLeft ();
		strLineTrimmed.TrimRight();
		strLineTrimmed.MakeUpper();
		CString strTokenEnd(ctstrGROUND);
		strTokenEnd += _T(' ');
		strTokenEnd += ctstrEND;
		if (0 == strLineTrimmed.Find(strTokenEnd)){
			nLine += nRtnLine;
			SetCurrentFileLineNum(nLine);
		}
	}

	// Reset command gap to zero.
	m_bCMDGapFound = false;
	if (m_eValLoc == GROUND || m_eValLoc == BOTH){
		if ((m_bCurrTimeFound)){
		   m_CSTTotalCMDTime = m_CSTCurrTime;
		} else if ((m_bPrevTimeFound)){
		   m_CSTTotalCMDTime = m_CSTPrevTime;
		}

		m_CSTTotalCMDTime = m_CSTTotalCMDTime + ((float)m_nRetryCnt * m_fCmdTime);
		m_bTotalCMDTimeFound = true;
	}

	m_strInstrValue.Empty();
	m_strStarBit.Empty();
	return eReturn_Status;
}

/*************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :		CSchedVal::Validate_Wait_Dir()
//
//	Description :
//					Routine to validate a STOL wait directive.
//						WAIT   <time tag>
//						WAIT + <time tag>
//			            WAIT @ <time tag>
//
//	Return :		BOOL
//
//	Parameters :
//
//	Notes :
//
//  Revision history:	Frederick Shaw 1999
//                      Initial release.
//
//////////////////////////////////////////////////////////////////////////
**************************************************/
BOOL CSchedVal::Validate_Wait_Dir(CString strLine){

	BOOL eReturn_Status = TRUE;
	int nPos = 0;
	CString strSave(strLine);

	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove "schedx_bias" from wait timetag.
	CString strToken(_T("$"));
	strToken += ctstrSCHEDX_BIAS;
	if ( -1 != (nPos = strLine.Find(strToken, 0))){
		// Remove SCHEDX_BIAS variable.
		strLine.Delete(nPos, strToken.GetLength());
		if ( -1 != (nPos = strLine.Find(_T('+'), 0))){
			// remove "+"
			strLine.Delete(nPos, 1);
		}
	}

	// Remove "wait"
	strLine.MakeUpper();
	if (-1 != (nPos = strLine.Find(ctstrWAIT, 0))){
	//	nPos = WhereIsToken(strLine, ctstrWAIT);
	//	if (nPos < 0) nPos = 0;
		int nLen = _tcsclen(ctstrWAIT);
		strLine.Delete(nPos, nLen);
		strLine.TrimLeft();
		strLine.TrimRight();
	}

	if ( -1 != (nPos = strLine.Find(_T(';'), 0))){
		// remove ";"
		strLine.Delete(nPos, 1);
	}

	bool bRelTime = false;
	if ( -1 != (nPos = strLine.Find(_T('@'), 0))){
		// remove "@"
		strLine.Delete(nPos, 1);
	} else if ( -1 != (nPos = strLine.Find(_T('+'), 0))){
		// remove "+"
		strLine.Delete(nPos, 1);
		bRelTime = true;
	} else {
//		WriteValidationMsg(GetCurrentFileLineNum (), 7, strSave);
//		eReturn_Status = FALSE;
		bRelTime = true;
	}

	bool bDone = false;
	while (!bDone){
		if ( -1 != (nPos = strLine.Find(_T('('), 0))){
			strLine.Delete(nPos, 1);
			if ( -1 != (nPos = strLine.Find(_T(')'), 0))){
				strLine.Delete(nPos, 1);
			} else{
				// error in wait directive format.
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 20);
				eReturn_Status = FALSE;
			}
		} else if (-1 != (nPos = strLine.Find(_T(')'), 0))){
			// error in wait directive format.
			strLine.Delete(nPos, 1);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 64);
			eReturn_Status = FALSE;
		} else if ( -1 != (nPos = strLine.Find(_T('\"'), 0))){
			strLine.Delete(nPos, 1);
			if ( -1 != (nPos = strLine.Find(_T('\"'), 0))){
				strLine.Delete(nPos, 1);
			} else{
				// error in wait directive format.
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 21);
				eReturn_Status = FALSE;
			}
		}else
			bDone = true;
	}

	//	WhereIsToken(strLine, ctstrTIME)
	if ( -1 != (nPos = strLine.Find(ctstrTIME, 0))){
		// remove "time"
		int nLen = _tcslen(ctstrTIME);
		strLine.Delete(0, nPos + nLen);
	}

	strLine.TrimLeft();
	strLine.TrimRight();

	if (!strLine.IsEmpty()){
		if (bRelTime){
			// Relative time tag found; add to current time tag.
			CSchedTimeSpan cST;
			if (TRUE == cST.Create(strLine)){
				if (m_bCurrTimeFound){
					m_CSTCurrTime =  m_CSTCurrTime + cST;
					m_bCurrTimeFound = true;
					// m_strCurrTime = m_CSTCurrTime.GetTimeSpanString(TRUE,TRUE);
				} else if (m_bPrevTimeFound){
					// There is no current time tag
//					m_CSTPrevTime = m_CSTPrevTime + cST;
					m_CSTCurrTime = m_CSTPrevTime + cST;
					m_bCurrTimeFound = true;
				} else {
					// There is no previous time tag.
					if (TRUE != m_CSTCurrTime.Create(ctstrZeroTimeTag)){
						CString strErrorMsg = cST.GetLastErrorText();
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
						eReturn_Status = FALSE;
					} else {
						m_CSTCurrTime = m_CSTCurrTime + cST;
						m_bCurrTimeFound = true;
					}
				}
			} else {
				CString strSubMsg = cST.GetLastErrorText();
				// CString strErrorMsg;
				// strErrorMsg.Format(_T("Error invalid time tag; %s"), strSubMsg);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strSubMsg, 2);
				eReturn_Status = FALSE;
			} //  End of relative time tag validation
		} else {
			// Absolute time tag found.
			// Now check if year is present.
			if (strLine.GetLength() > 16){
				// Time tag contains a year value.
				CSchedTime cST;
				if (TRUE == cST.Create(strLine)){
					if (m_bPrevTimeFound){
						CSchedTimeSpan cMinTimeInterval;
						cMinTimeInterval = cST - m_CSTPrevTime;
						CString strTimeInterval = cMinTimeInterval.GetTimeSpanString(FALSE, TRUE);
						// TCHAR   *cStopString;
						double dNumSeconds = cMinTimeInterval.GetSchedTimeInSeconds();
						double dTimeTagSpacing = _tcstod((LPCTSTR)GetAppRegSC()->m_strTimeTagSpacing[GetAppRegSite()->m_nDefaultSC], NULL);

						if ((dNumSeconds -  dTimeTagSpacing) <= 0.0){
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 96, strLine);
							eReturn_Status = FALSE;
						}
					}

					if (m_bCurrTimeFound && (cST <= m_CSTCurrTime)){
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 9);
						eReturn_Status = FALSE;
					} else if (m_bPrevTimeFound && (cST <= m_CSTPrevTime)){
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 9);
						eReturn_Status = FALSE;
					} else { // Current time tag is greater than previous time tag.
						m_bCurrTimeFound = true;
						m_CSTCurrTime = cST;
					}
				} else {
					CString strSubMsg = cST.GetLastErrorText();
					// CString strErrorMsg;
					// strErrorMsg.Format(_T("Error invalid time tag; %s"), strSubMsg);
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strSubMsg, 2);
					eReturn_Status = FALSE;
					m_bCurrTimeFound = false;
				}
			} else {
				// Time tag does not contain a year value.
				// Create a time span object and add to current time.
				// If time tag is greater than prev time tag, copy to current time tag.
				CSchedTimeSpan cSTSpan;
				CSchedTime     CSTTempTime;
				if (TRUE == cSTSpan.Create(strLine)){
					CSTTempTime    = CSTTempTime + cSTSpan ;
					if (m_bPrevTimeFound){
						CSchedTimeSpan cMinTimeInterval;
						cMinTimeInterval = CSTTempTime - m_CSTPrevTime;
						CString strCurrTime = cMinTimeInterval.GetTimeSpanString(FALSE,TRUE);
						double dNumSeconds = cMinTimeInterval.GetSchedTimeInSeconds();
					//	TCHAR   *cStopString;
						double dTimeTagSpacing = _tcstod(GetAppRegSC()->m_strTimeTagSpacing[GetAppRegSite()->m_nDefaultSC], NULL);
						if ((dNumSeconds -  dTimeTagSpacing) <= 0.0){
							m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 6);
							eReturn_Status = FALSE;
						}
					}

					if (m_bCurrTimeFound && (CSTTempTime <= m_CSTCurrTime)){
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 9);
						eReturn_Status = FALSE;
					} else if (m_bPrevTimeFound && (CSTTempTime <= m_CSTPrevTime)){
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 9);
						eReturn_Status = FALSE;
					} else { // Current time tag is greater than previous time tag.
						m_bCurrTimeFound = true;
						m_CSTCurrTime = CSTTempTime;
					}
				} else {
					CString strSubMsg = cSTSpan.GetLastErrorText();
					// CString strErrorMsg;
					// strErrorMsg.Format(_T("Error invalid time tag; %s"), strSubMsg);
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strSubMsg, 2);
					eReturn_Status = FALSE;
					m_bCurrTimeFound = false;
				}
			}
		}
	} else {
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 22);
		eReturn_Status = FALSE;
		m_bCurrTimeFound = false;
	}

	// Check if enough time has elapsed.
	if ((m_bPrcWinTimeFound) && (!bRelTime)){
		CString strCurrTime = m_CSTCurrTime.GetTimeString(FALSE);
		CString strPrcWinTime = m_CSTPrcWinTime.GetTimeString(FALSE);
		if (m_bCurrTimeFound){
			if (m_CSTCurrTime < m_CSTPrcWinTime){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 30, strLine);
				eReturn_Status = FALSE;
				// Reset command timing counter to empty.
				m_bPrcWinTimeFound =  false;
			}
		} else if (m_bPrevTimeFound) {
			if ((m_CSTPrevTime < m_CSTPrcWinTime)){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 30, strLine);
				eReturn_Status = FALSE;
				// Reset command timing counter to empty.
				m_bPrcWinTimeFound =  false;
			}
		} else {
			// Reset command timing counter to empty.
			m_bPrcWinTimeFound =  false;
		}
	}
	return eReturn_Status;
}


/*************************************************************
//////////////////////////////////////////////////////////////////////
//	Author : Fred Shaw		Date : 08-18-2003			version 1.0
//////////////////////////////////////////////////////////////////////////
// Routine name:		CSchedVal::Validate_ARGS_Dir()
//
// Routine description:	Routine to validate a schedule file line containing
//						the STOL directive args().
//
//
//	Return :		    BOOL
//
//	Parameters :	    CString strLine
//                      Complete STOL directive line is passed in.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
**********************************************************/
BOOL CSchedVal::Validate_ARGS_Dir (CString strLine){

	BOOL eReturn_Status = TRUE;
	CString strSave = strLine;
	strLine.MakeUpper();

	int nPos = -1;
	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove "args()" from command line.
	CString strToken(ctstrARGS);
	if ( -1 != (nPos = strLine.Find(strToken, 0))){
		strLine.Delete(nPos, strToken.GetLength());
	}

	/*
		Parse tokens based on commas.
		Count number of tokens.
		Compare number of tokens found versus number of param directives.
	*/

	strLine.Replace(_T('('), _T(' '));
	strLine.Replace(_T(')'), _T(' '));
	strLine.Replace(_T(','), _T(' '));
	strLine.TrimLeft();

	CStringArray strPPData;
	if (!strLine.IsEmpty()){
		CTextFile::TokenizeString(strLine, strPPData);
	}

	// Check size of token array.
	int nPPDataSize   = strPPData.GetSize();
	int nPrologPPSize = GetNumberPassedParams ();

	if (nPPDataSize != nPrologPPSize){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 42, strSave);
		eReturn_Status = FALSE;
	}

	return eReturn_Status;
}

/*************************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
// Routine name:		CSchedVal::Validate_CMD_Dir()
//
// Routine description:	Routine to validate a schedule file line containing
//                      a CMD directive.  It is assumed that any time tags
//                      contained on the line have been removed before being
//                      passed into the routine.  The CString is parsed into
//                      it's individual tokens.  A space signifies the break
//                      between tokens.  There must be at least two tokens
//                      or the routine writes an error message out to the
//                      validation report file and returns.  No further
//                      processing is performed on the file.
//                      The first token[0] contains the directive.  The second
//                      token[1] is either the spacecraft address
//                      or the command mnemonic.  The second token if it is the
//                      CTCU addr must be either an integer or "$QBCTCUSEL".
//                      The third token is either spacecraft mnemonic or data.
//						CMD <ctcu> <mnemonic> [data...]
//
//	Return :		    BOOL
//
//	Parameters :	    CString strLine
//                      Complete STOL directive line is passed in.
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
**********************************************************/
BOOL CSchedVal::Validate_CMD_Dir(CString strLine){

	BOOL eReturn_Status = TRUE;
//	int nTimeDelay = 0;

 	int nPos= 0;
	// Remove trailing comment.
	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		strLine.Delete(nPos, strLine.GetLength() - nPos + 1);
	}

	// Does directive contain a timetag?
	// If so need to remove it before processing
	int nAtSign = 0;
	if ( -1 != (nAtSign = (strLine.Find(_T('@'), 0)))){
		if (-1 != (nPos = (strLine.Find(_T(')'),0)))){
			CString strTimeTag = strLine;
			strTimeTag.MakeUpper();
			strTimeTag.Delete(nPos, strTimeTag.GetLength() - nPos);
			strTimeTag.Delete(0,nPos - nAtSign);
		 	strTimeTag.Remove(_T('@'));
			strTimeTag.Remove(_T('('));
			strTimeTag.Remove(_T(')'));
			strTimeTag.Remove(_T('+'));

			int nParen = 0;
			if ( -1 != (nParen = (strLine.Find(_T(')'), 0)))){
				strLine.Delete(nAtSign, nParen - nAtSign + 1);
			}

			// _tcslen(ctstrPRC_STRT)
			CString strTemp(_T("$"));
			strTemp += ctstrPRC_STRT;
			if ( -1 != (nPos = (strTimeTag.Find(strTemp, 0)))){
				strTimeTag.Delete(nPos, strTemp.GetLength());
			}

			int nTimeDelay = 0;
			nTimeDelay = _ttoi((LPCTSTR)strTimeTag);

//			CSchedTimeSpan cstTimeDelay;
//			cstTimeDelay.Create(strTimeTag);

			CString strCurrTime     = m_CSTCurrTime.GetTimeString(FALSE);
			CString strPrevTime     = m_CSTPrevTime.GetTimeString(FALSE);
			CString strPrcStartTime = m_CSTPrc_StartTime.GetTimeString(FALSE);

			if (m_eValLoc != ONBOARD){
				CSchedTime CMDTime2Exec;
				CMDTime2Exec = m_CSTPrc_StartTime + nTimeDelay;
				CString strCMDTime2Exec = CMDTime2Exec.GetTimeString(FALSE);

				if (m_bCurrTimeFound){
					if (m_CSTCurrTime > CMDTime2Exec){
						// Output an error message.
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 48);
						eReturn_Status = FALSE;
					}
				} else {
					if (m_CSTPrevTime > CMDTime2Exec){
						// Output an error message.
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 48);
						eReturn_Status = FALSE;
					}
				}
			}
		}
	}

	strLine.TrimLeft();
	strLine.TrimRight();
	CString strMnemonic;

	if (TRUE  == Parse_CMD_Line(strLine, strMnemonic)){
		if (m_bPaceCMDTimeFound) {
		    // This means that the previous command mnemonic commanded had a time restriction
			// on it.
			if (m_bCurrTimeFound){
				if (m_CSTPaceCMDTime >= m_CSTCurrTime){
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 78, strMnemonic);
					eReturn_Status = FALSE;
				} else {
					// command pacing time is less than current time tag.
					// Clear command pacing time.
					m_bPaceCMDTimeFound = false;
				}

			// if (m_bPrevTimeFound)
			} else {
				if (m_CSTPaceCMDTime >= m_CSTPrevTime){
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 78, strMnemonic);
					eReturn_Status = FALSE;
				} else {
					// Command pacing time is less than previous time tag.
					// Clear command pacing time.
					m_bPaceCMDTimeFound = false;
				}
			}
		}
		// Is the mnemonic on the time pacing restricted list?
		CheckMnemonicCurrentTimeTag(strMnemonic);
		// Does the mnemonic have command pacing restrictions?
		CheckCmdPacingFile(strMnemonic);

		// Update the running total of the number of command frames uplinked.
		// Use this value to verify that enough time has elapsed.
		// First check if mnemonic list is loaded.
		if (!m_mapNumCmdFrame.IsEmpty()){
			UpdateNumberCmdFrames(strMnemonic);
		}
	}

	// Reset command gap to zero.
	m_bCMDGapFound = false;

	if (m_eValLoc == GROUND || m_eValLoc == BOTH){

		if (m_bCurrTimeFound){
		   m_CSTTotalCMDTime = m_CSTCurrTime;
		} else if (m_bPrevTimeFound){
		   m_CSTTotalCMDTime = m_CSTPrevTime;
		}
		m_CSTTotalCMDTime = m_CSTTotalCMDTime + ((float)m_nRetryCnt * m_fCmdTime);
		m_bTotalCMDTimeFound = true;
	}

	// Check if mnemonic is restricted for all STOL procedures
	IsMnemonicRestricted4STOL(strMnemonic);

	if (m_eValType == RTCS_PRC_VAL){
		// If file is a RTCS file, check for restricted mnemonics
		IsMnemonicRestricted(strMnemonic);
	} else if (STOL_VAL == m_eValType){
		// If file is a STOL procedure, check for restricted mnemonics
		IsMnemonicRestricted4Procs(strLine);
	}
	return eReturn_Status;
}

/**********************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 			version 2.0
//////////////////////////////////////////////////////////////////////////
// Routine name: CSchedVal::CheckForLocalVariables
//
// Routine description:	This routine checks for local variables in command
//						directives.  It first verifies that the variables
//						are not system globals or passed parameters.
//						This is an overridden function which contains
//						a second passed parameter.
//
//	Return :		BOOL
//						Always returns true.
//
//	Parameters :
//					CString strLine,
//					CTextFile &Prc
//
//	Note :
//////////////////////////////////////////////////////////////////////////
****************************************************/
BOOL CSchedVal::CheckForLocalVariables(CString strLine, CTextFile &Prc){

	BOOL  eReturn_Status = TRUE;
	int nSpacePos = -1;
	CString strToParse(strLine);
	CString strVariable;

	/*
		Get number of passed parameters in file
		Compare to values passed in
     */
	CObArray  ObjPassedParam;
	ObjPassedParam.Copy(Prc.GetPassedParamObj());
	int nNumParam       = ObjPassedParam.GetSize();

	// Find the position of the CMD directive.
	int	nPos = (strToParse.Find(ctstrCMD));
	int nLen = _tcsclen(ctstrCMD);
 	strToParse.Delete(0, nPos + nLen);
	strToParse.TrimLeft();
	strToParse.TrimRight();

	// Does directive contain a timetag?
	// If so need to remove it before processing
	int nAtSign = 0;
	if ( -1 != (nAtSign = (strToParse.Find(_T('@'), 0)))){
		if (-1 != (nPos = (strToParse.Find(_T(')'),0)))){
			int nParen = 0;
			if ( -1 != (nParen = (strToParse.Find(_T(')'), 0)))){
				strToParse.Delete(nAtSign, nParen - nAtSign + 1);
			}
		}
	}

	// Not inside a ground begin/end block
	if (!m_bBeginFound){
		nPos = strToParse.Find(_T('$'), 0);
		while (nPos != -1){
			strToParse.Delete(0, nPos);
			strToParse.TrimLeft();
			strToParse.TrimRight();
			nSpacePos = strToParse.Find(_T(' '), 0);
			if (nSpacePos == -1){
				nSpacePos = strToParse.GetLength();
			}
			strVariable = strToParse.Left(nSpacePos);
			strToParse.Delete(0, nSpacePos);
			strVariable.TrimLeft();
			strVariable.TrimRight();
			nPos = strToParse.Find(_T('$'), 0);

			bool bDone = false;

			// First compare to system globals.
			// If it matches then set done to true.
			if (0 == strVariable.CompareNoCase(ctstr$QBACE_SEL)){
				bDone = true;
			} else if (0 == strVariable.CompareNoCase(ctstr$QBCTCU_SEL)){
				bDone = true;
			}

			// Next check if variable is a passed parameter
			bool bFound = false;
			for (int i = 0; i < nNumParam && !bDone; i++){
				CString strName(_T('$'));
				strName += ((strctPassedParamList *)ObjPassedParam[i])->strName;
				if (0 == strVariable.CompareNoCase(strName)){
					bFound = true;
					bDone = true;
				}
			}

			// If not a system global or a passed parameter
			// Output validation message.
			if (!bDone){
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 8, strVariable);
			}
		}
	}
	return eReturn_Status;
}

/************************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 			version 2.0
//////////////////////////////////////////////////////////////////////////
// Routine name: CSchedVal::CheckForLocalVariables
//
// Routine description:	This routine checks for local variables in command
//						directives.  It first verifies that the variables
//						are not system globals or passed parameters.
//
//	Return :		BOOL	-
//						Always returns TRUE
//
//	Parameters : CString strLine	-	STOL command line
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*******************************************************/
BOOL CSchedVal::CheckForLocalVariables(CString strLine){

	BOOL  eReturn_Status = TRUE;
	int nSpacePos = -1;
	CString strToParse(strLine);
	CString strVariable;

	/*
		Get number of passed parameters in file
		Compare to values passed in
     */
	CObArray  ObjPassedParam;
	ObjPassedParam.Copy(GetPassedParamObj());
	int nNumParam       = ObjPassedParam.GetSize();

	// Find the position of the CMD directive.
	strToParse.MakeUpper();
	int	nPos = (strToParse.Find(ctstrCMD));
	int nLen = _tcsclen(ctstrCMD);
 	strToParse.Delete(0, nPos + nLen);
	strToParse.TrimLeft();
	strToParse.TrimRight();

	// Does directive contain a timetag?
	// If so need to remove it before processing
	int nAtSign = 0;
	if ( -1 != (nAtSign = (strToParse.Find(_T('@'), 0)))){
		if (-1 != (nPos = (strToParse.Find(_T(')'),0)))){
			int nParen = 0;
			if ( -1 != (nParen = (strToParse.Find(_T(')'), 0)))){
				strToParse.Delete(nAtSign, nParen - nAtSign + 1);
			}
		}
	}

	// Not inside a ground begin/end block
	if (!m_bBeginFound){
		nPos = strToParse.Find(_T('$'), 0);
		while (nPos != -1){
			strToParse.Delete(0, nPos);
			strToParse.TrimLeft();
			strToParse.TrimRight();
			nSpacePos = strToParse.Find(_T(' '), 0);
			if (nSpacePos == -1){
				nSpacePos = strToParse.GetLength();
			}
			strVariable = strToParse.Left(nSpacePos);
			strToParse.Delete(0, nSpacePos);
			strVariable.TrimLeft();
			strVariable.TrimRight();
			nPos = strToParse.Find(_T('$'), 0);

			bool bDone = false;

			// First compare to system globals.
			// If it matches then set done to true.
			if (0 == strVariable.CompareNoCase(ctstr$QBACE_SEL)){
				bDone = true;
			} else if (0 == strVariable.CompareNoCase(ctstr$QBCTCU_SEL)){
				bDone = true;
			}

			// Next check if variable is a passed parameter
			bool bFound = false;
			for (int i = 0; i < nNumParam && !bDone; i++){
				CString strName(_T('$'));
				strName += ((strctPassedParamList *)ObjPassedParam[i])->strName;
				if (0 == strVariable.CompareNoCase(strName)){
					bFound = true;
					bDone = true;
				}
			}

			// If not a system global or a passed parameter
			// Output validation message.
			if (!bDone){
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 8, strVariable);
			}
		}
	}
	return eReturn_Status;
}

/**********************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
// Routine name:	Validate_Prolog()
//
// Routine description:	Routine to validate the prolog of the file.
//
//
//	Return :		BOOL -
//						TRUE	-	prolog valid
//						FALSE	-	prolog invalid
//
//	Parameters :
//			const enum eValType eFileValType	-	Type of validation
//
//	Note :
//////////////////////////////////////////////////////////////////////////
*************************************************************/
BOOL CSchedVal::Validate_Prolog(const enum eValType eFileValType){

	BOOL eReturn_Status = TRUE;

	if (TRUE != WasBeginFoundInFile()){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 1);
		eReturn_Status = FALSE;
	}

	if (TRUE != WasEndFoundInFile()){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 6);
		eReturn_Status = FALSE;
	}

	// validate based on file type
	switch (eFileValType)
	{
		case STOL_VAL :
		{
			if (TRUE != WasExectimeFoundInFile()){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 27);
				AddPrologExectime(ctstrZeroTimeTag);
				eReturn_Status = FALSE;
			}

			if (TRUE == WasUpdateFoundInFile()){
				// Retrieve the type of update and the instrument object name.
				CStringArray * strDataUpdate;
				strDataUpdate = GetUpdateTextArray();
				if (TRUE != StoreFilenameAndType(strDataUpdate)){
					// m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 27);
					// eReturn_Status = FALSE;
				}
			}

			CheckForDuplicatePassedParams();
		}
		break;

		case RTCS_PRC_VAL :
		{
			if (TRUE != WasExectimeFoundInFile()){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 27);
				AddPrologExectime(ctstrZeroTimeTag);
				eReturn_Status = FALSE;
			}
		}
		break;

		case CLS_VAL :
		{
			if (TRUE != WasExectimeFoundInFile()){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 27);
				AddPrologExectime(ctstrZeroTimeTag);
				eReturn_Status = FALSE;
			}

			CheckForDuplicatePassedParams();
		}
		break;
	}
	return eReturn_Status;
}

/****************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 03-07-2002			version 2.0
//////////////////////////////////////////////////////////////////////////
// Routine name: CheckForDuplicatePassedParams()
//
// Routine description:	This routine checks if there are duplicate passed
//						parameter names.  An validation message is generated
//						if a match is found.
//
//
//	Return :		BOOL -
//						Always returns true.
//
//
//	Parameters :	None	-	void.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
*****************************************/
BOOL CSchedVal::CheckForDuplicatePassedParams(){

	BOOL eReturn_Status = TRUE;

	CObArray  ObjPassedParam;
	ObjPassedParam.Copy(GetPassedParamObj());
	int nNumParam       = ObjPassedParam.GetSize();

//	CString strTemp = ((strctPassedParamList *)ObjPassedParam[0])->strName;
	if (nNumParam > 0 && ((strctPassedParamList *)ObjPassedParam[0])->strName.IsEmpty()){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 41, ((strctPassedParamList *)ObjPassedParam[0])->strName);
	}

	for (int i = 0; i < nNumParam - 1; i++){
		for (int j = i+1; j < nNumParam; j++){
			CString strCurrent = ((strctPassedParamList *)ObjPassedParam[i])->strName;
			CString strNext    = ((strctPassedParamList *)ObjPassedParam[j])->strName;
			if (0 == strCurrent.CompareNoCase(strNext)){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 13, strNext);
			}
			if (strNext.IsEmpty()){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 41, strNext);
			}
		}
	}
	return eReturn_Status;
}

/************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : CSchedVal::Validate_Include_Dir()
//
//	Description :	This routine validates the include directive.
//
//	Return :		BOOL -
//						TRUE	-	valid directive
//						FALSE	-	invalid
//
//	Parameters :	CString strLine	-	STOL line
//
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
BOOL CSchedVal::Validate_Include_Dir(CString strLine){

	BOOL eReturn_Status = TRUE;
 	int nPos= 0;
	CString strRtcsName;

	// Remove trailing comment.
	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove directive
	nPos = WhereIsToken(strLine, ctstrInclude);
	if (nPos >= 0){
		int nLen = _tcsclen(ctstrInclude);
		strLine.Delete(nPos, nLen);
	}

	strLine.TrimLeft();
	strLine.TrimRight();

	 /*
		Next token is the RTCS name
			call validate rtcs_dir
			check if file exists and has passed validation for a RTCS
     */

	// Extract out the RTS name
	int nRtcsNameIndex = -1;
	strRtcsName = strLine;
	if (-1 != (nRtcsNameIndex = strLine.Find(_T(' '), 0))){

		if (nRtcsNameIndex > 0){
			int nLength = strRtcsName.GetLength();
			strRtcsName.Delete(nRtcsNameIndex, nLength - nRtcsNameIndex);
			// Remove the RTCS name for passing to the validate RTCS dir routine.
			strLine.Delete(0, nRtcsNameIndex);
			strLine.TrimLeft();
			strLine.TrimRight();
			strRtcsName.TrimLeft();
			strRtcsName.TrimRight();
		}

		eReturn_Status = Validate_RTCSName(strRtcsName);
	}

	/*
		Next token is the label
			Must be unique to this file
			check againest labels already found and store if not found
			if not unique output an error message.

	*/

	// Extract out the label.
	CString	strLabelName = strLine;

  	if (-1 != (nRtcsNameIndex = strLine.Find(_T(' '), 0))){
		if (nRtcsNameIndex > 0){
			int nLength = strLabelName.GetLength();
			strLabelName.Delete(nRtcsNameIndex, nLength - nRtcsNameIndex);
			strLine.Delete(0, nRtcsNameIndex);
			strLine.TrimLeft();
			strLine.TrimRight();
			strLabelName.TrimLeft();
			strLabelName.TrimRight();
			int nLabelIndex = -1;
			if (-1 != (nLabelIndex = strLabelName.Find(ctstrLabelEq, 0))){
				// Remove the RTCS name for passing to the validate RTCS dir routine.
				int nLength = _tcsclen(ctstrLabelEq);
				strLabelName.Delete(nLabelIndex, nLength - nLabelIndex);
				strLabelName.TrimLeft();
				strLabelName.TrimRight();
			}
		}
	}

	bool bFound = false;
	for (int i = 0; i < m_strRTCSLabel.GetSize() && !bFound; i++){
		if ( 0 == m_strRTCSLabel[i].CompareNoCase(strLabelName)){
			bFound = true;
			// Output an error message.
			// Duplicate labels found in file.
		}
	}

	if (!bFound){
		m_strRTCSLabel.Add(strLabelName);
	}

	/*
		Next token is start parameter.
			verify that value is greater than previous value.

		CWordArray   m_wdStartVal;
	*/
	strLine.MakeUpper();
	strLine.TrimLeft();
	strLine.TrimRight();
	nPos = 0;
	if ( -1 != WhereIsToken(strLine, ctstrSTARTEQ)){
//		strStartParam = strLine;
		// Find space and delete extraneous characters
		int nSpaceIndex = -1;
		if (-1 != (nSpaceIndex = strLine.Find(_T(' '), 0))){
			if (nSpaceIndex > 0){
				int nLength = strLine.GetLength();
				strLine.Delete(nSpaceIndex, nLength - nSpaceIndex);
				strLine.TrimLeft();
				strLine.TrimRight();
			}
		}
		// Now remove start equals parameter.
		int nEqIndex = -1;
		if (-1 != (nEqIndex = strLine.Find(_T('='), 0))){
			strLine.Delete(0, nEqIndex + 1);
			strLine.TrimLeft();
			strLine.TrimRight();

			int nStartValue = _ttoi(strLine);
			if ( m_nPrevStartValue <= nStartValue){
				// Output error message start value not less than previous one.
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 70);
				eReturn_Status = FALSE;
			} else {
				m_nPrevStartValue = nStartValue;
			}
		}
	}
	return eReturn_Status;
}

/*****************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : CSchedVal::Validate_Activity_Dir
//
//	Description :	This routine validates the activity directive.
//
//	Return :		BOOL
//
//	Parameters :	CString strLine	-	STOL command line
//
//	Note :
//////////////////////////////////////////////////////////////////////////
******************************************/
BOOL CSchedVal::Validate_Activity_Dir(CString strLine){
	BOOL eReturn_Status = TRUE;

	CSchedTime stFinalTime;
	int nPos = 0;
	int nActIdx = 0;  // Activity name array index

	// Parse string

	// Remove directive
	strLine.TrimLeft();
	strLine.TrimRight();
	strLine.MakeUpper();
	nPos = WhereIsToken(strLine, ctstrACTIVITY);

	/*
	if begin found
	   extract name
	   extract duration
	   add name to array
	   final time equals duration plus current time
	   insert final time into array
	*/

	if (-1 == nPos){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 65, strLine);
		eReturn_Status = FALSE;
	} else {
		int nLen = _tcsclen(ctstrACTIVITY);
		strLine.Delete(nPos, nLen);
		strLine.TrimLeft();
		strLine.TrimRight();

		if (-1 != (nPos = WhereIsToken(strLine, ctstrBEGIN))){
				/* 	if begin found
					   extract name
					   extract duration
					   Check if name already exists in array
						   Output an error if name already exists.
					   add name to array
					   final time equals duration plus current time
					   insert final time into array
				*/
			int nLen = _tcsclen(ctstrBEGIN);
			strLine.Delete(nPos, nLen);
			strLine.TrimLeft();
			strLine.TrimRight();
			nPos = strLine.Find(_T(' '), 0);

			CString strActivityName = strLine;
			int nStrLineLgth =  strLine.GetLength();
			strActivityName.Delete(nPos, nStrLineLgth - nPos);

			bool bFound = false;
			int nActNameSize = m_strActivityName.GetSize();
			for ( nActIdx = 0; nActIdx < nActNameSize; nActIdx++){
				if ( 0 == m_strActivityName[nActIdx].CompareNoCase(strActivityName)){
					eReturn_Status = FALSE;
					bFound = true;
					break;
				}
			}

			if (!bFound){
				strLine.Delete(0, nPos);
				strLine.TrimLeft();
				strLine.TrimRight();
				m_strActivityName.Add(strActivityName);

				CSchedTimeSpan cstFinalTimeSpan;
				CSchedTime *  pcstActivityEndTime = new CSchedTime;
				if (TRUE != cstFinalTimeSpan.Create(strLine)){
					CString strSubMsg = cstFinalTimeSpan.GetLastErrorText();
					CString strErrorMsg;
					strErrorMsg.Format(_T("Invalid time tag; %s"), strSubMsg);
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, 2);
					eReturn_Status = FALSE;
				} else {
					*pcstActivityEndTime = m_CSTCurrTime + cstFinalTimeSpan;
					m_objActivityEndTime.Add((CObject *)pcstActivityEndTime);
				}

				// stFinalTime.SetSchedTime(m_strCurrTime);
				// stFinalTime.Add(strLine);
			//	m_strActivityEndTime.Add(cstFinalTimeSpan.GetTimeSpanString(TRUE));
				m_uActivitySchedLineNum.Add((UINT)(GetCurrentFileLineNum ()));
			} else {
				// Error acitivy already defined.
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 66, strActivityName);
			}

		} else if (-1 != (nPos = WhereIsToken(strLine, ctstrEND))){
			int nLen = _tcsclen(ctstrEND);
			strLine.Delete(nPos, nLen);
			strLine.TrimLeft();
			strLine.TrimRight();
			bool bFound = false;
			int nActNameSize = m_strActivityName.GetSize();
			for ( nActIdx = 0; nActIdx < nActNameSize; nActIdx++){
				if ( 0 == m_strActivityName[nActIdx].CompareNoCase(strLine)){
					eReturn_Status = FALSE;
					bFound = true;
					break;
				}
			}

			if (!bFound){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 67, strLine);
			} else {
				// Activity found in list.
				// Is the time within the duration?
				CSchedTime CSTActivityEndTime(*(CSchedTime *)m_objActivityEndTime[nActIdx]);
				CString strActivityEndTime = CSTActivityEndTime.GetTimeString(TRUE);
				CString strCurrTime = m_CSTCurrTime.GetTimeString(TRUE);
				if (m_CSTCurrTime < CSTActivityEndTime){
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 19, strLine);
					// Remove matching elements from the list.
					m_strActivityName.RemoveAt       (nActIdx,1);
					delete ((CSchedTime*) m_objActivityEndTime[nActIdx]);
					m_objActivityEndTime.RemoveAt(nActIdx);
					m_uActivitySchedLineNum.RemoveAt (nActIdx,1);
				} else {
					// Remove matching elements from the list.
					m_strActivityName.RemoveAt       (nActIdx,1);
					delete ((CSchedTime*) m_objActivityEndTime[nActIdx]);
					m_objActivityEndTime.RemoveAt    (nActIdx);
					m_uActivitySchedLineNum.RemoveAt (nActIdx,1);
				}
			}
		} else {
			// begin/end directive not found.
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 69, strLine);
	 		eReturn_Status = FALSE;
		}
	}
	return eReturn_Status;
}

/*****************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 06-04-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : Check_Activity_Log()
//
//	Description :	This routine verifies that there are not any
//					begin/end activities directives left. Any remaining
//					in the log file means they were not paired up so
//					an validation message is posted.
//
//
//	Return :		BOOL 	-
//							TRUE	-	empty log file
//							FALSE	-	log file is not empty.
//
//	Parameters :	none	-	void
//
//	Note :
//////////////////////////////////////////////////////////////////////////
****************************************/
BOOL CSchedVal::Check_Activity_Log(){
	BOOL eReturnStatus = TRUE;

	int nActNameSize = m_strActivityName.GetSize();
	for (int nActIdx = 0; nActIdx < nActNameSize; nActIdx++){
		m_NewGblValData.WriteValidationMsg(m_uActivitySchedLineNum[nActIdx], 68, m_strActivityName[nActIdx]);
		eReturnStatus = FALSE;
	}

	return eReturnStatus;
}

/*****************************
///////////////////////////////////////////////////////////
// STOL Procedure Command Pacing Validation
///////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-07-01			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : LoadCmdPacingFile()
//
//	Description :  This routine loads the command pacing spec file.  Read file line
//                 by line. Store mnemonic and the duration in a linked list.
//
//	Return :		BOOL: TRUE = SUCCESFUL; FALSE = FAIL.
//
//	Parameters :	none
//
//	Note :
//			The format of the file is: string (mnemonic), duration (float),
//          string (mnemonic).  Before or during loading of file, need to verify all
//          mnemonics are valid.
//
//
//////////////////////////////////////////////////////////////////////////
*************************************/
BOOL CSchedVal::LoadCmdPacingFile(){

	BOOL			eReturnStatus = TRUE;
	bool			bStatus = true;
	CTextFile		dataFile;
	CParseCMDPace	cParseCMDPace;
	CString         strCmdPaceFileName;

	m_bCMDPaceFileLoaded = true;

	// Get the file extension.
	CString strExt(_T(".pcmd"));
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nPaceCmd])->GetDocString(strExt, CDocTemplate::filterExt);

	// Get the path for the command pacing file.
//	int	nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int	nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
//	strCmdPaceFileName.Format(_T("\\\\%s\\%s\\%s\\%s\\"),
//		GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server.
//		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share.
	
	strCmdPaceFileName.Format(_T("%s\\%s\\%s\\%s\\%s\\"),
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC], 		// SC Specific Directory.
        GetAppRegFolder()->m_strDir[nFolderData]);                  // Folder containing the file.

	// Get the name of the command pacing validation file.
	strCmdPaceFileName += GetAppRegSC()->m_strPaceCmd[GetAppRegSite()->m_nDefaultSC];
	strCmdPaceFileName += strExt;

	// Need to validate command pacing file before loading.
	// Call syntax checker to validate the mnemonics.
	// Load the document.
	// Extract the prolog.


	CFileException cFE;
	if (TRUE != dataFile.Load(strCmdPaceFileName, CTextFile::RO, &cFE)){
		bStatus = false;
		m_bCMDPaceFileLoaded = false;
		// CString strError = ExceptionErrorText(&cFE);
		CString strMsg;
		strMsg.Format(_T("Unable to load command pacing file: %s"), strCmdPaceFileName);
		m_NewGblValData.WriteValidationMsg(0, strMsg, 0);
	} else {
		dataFile.ExtractProlog();
	}

	// Read the data from the file into the document
	if (bStatus)
	{
		int nCurObj = 0;
		while ((bStatus) && (nCurObj < dataFile.GetLineCount()))
		{
			CString strLine = dataFile.GetTextAt(nCurObj);
			strLine.TrimLeft ();
			strLine.TrimRight ();
			if ((!strLine.IsEmpty()) && (ctchCOMMENT != strLine[0])) {
				cParseCMDPace.SetData(strLine);
				if (!cParseCMDPace.ParseData())
				{
					CString strError;
					strError.Format (IDS_BAD_DATA, nCurObj+1);
					GetAppOutputBar()->OutputToBuild(strError, COutputBar::WARNING);
					bStatus = false;
				}
				else
				{
					strctCMDPaceMnemonicList *pCMDPace = new strctCMDPaceMnemonicList;
					pCMDPace->strMnemonic01    = cParseCMDPace.GetDataCMDPaceMnemonic01();
					pCMDPace->fDelayTime       = cParseCMDPace.GetDataCMDPaceDelay();
					pCMDPace->strMnemonic02    = cParseCMDPace.GetDataCMDPaceMnemonic02();
					m_objCMDPaceMnemonic.Add((CObject *)pCMDPace);
				}
			}
			nCurObj++;
		}
		// Close the file, all of the data is now local

		CFileException cFE;
		if (TRUE != dataFile.Close(&cFE)){
			CString strErrorMsg = ExceptionErrorText(&cFE);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strCmdPaceFileName);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	}

	return eReturnStatus;
}

/************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-07-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : CheckMnemonicCurrentTimeTag
//
//	Description :
//     Checks mnemonic passed in againest the current list of restricted mnemonics.
//     First checks if a timetag is available.  Next checks if the delay time has passed.
//     If the time is passed the element is removed.  Otherwise an error message is generated
//	   stating that not enough time is elapsed for the previous command mnemonic.
//
//	Return :	TRUE = SUCCESS, FALSE = FAIL.
//
//	Parameters : CString strMnemonic
//
//	Note :
//////////////////////////////////////////////////////////////////////////
************************************************/
BOOL CSchedVal::CheckMnemonicCurrentTimeTag(CString strMnemonic){

  BOOL	eReturnStatus = TRUE;
  int nNumElements = m_objCMDPaceRestriction.GetSize();
  bool bFound = false;
  int nObjIdx = 0;

  while (!bFound && nObjIdx < nNumElements){

	/*
	  Check mnemonic againest the running mnemonic restricted list.
      If found
         check if enough elapsed time
			output an error message if not enough time is elapsed.

         Else if past the elapsed time
            delete entry for list.
	*/

	  if (m_bCurrTimeFound){
		  if (m_CSTCurrTime > (((strctCMDPaceRestrictList *)m_objCMDPaceRestriction[nObjIdx])->cDelayTime)){
			  // Passed the delay time. Remove element from list.
			  delete ((strctCMDPaceRestrictList *)m_objCMDPaceRestriction[nObjIdx]);
			  m_objCMDPaceRestriction.RemoveAt(nObjIdx);
			  nNumElements--; // Decrement size of array since one element is removed.
		  } else if (0 == strMnemonic.CompareNoCase(((strctCMDPaceRestrictList *)m_objCMDPaceRestriction[nObjIdx])->strMnemonic01)){
			  // Output an error message  Not enough time has elapsed before the current mnemonic can be commanded.
              m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 78, strMnemonic);
			  bFound = true;
		  } else {
			  nObjIdx++;
		  }

	  } else {

 		  CString strSearch = ((strctCMDPaceRestrictList *)m_objCMDPaceRestriction[nObjIdx])->strMnemonic01;
		  CSchedTime    CSTDelayTime = (((strctCMDPaceRestrictList *)m_objCMDPaceRestriction[nObjIdx])->cDelayTime);
		  if (m_CSTPrevTime > CSTDelayTime ){
			  // Passed the delay time. Remove element from list.
			  delete ((strctCMDPaceRestrictList *)m_objCMDPaceRestriction[nObjIdx]);
			  m_objCMDPaceRestriction.RemoveAt(nObjIdx);
			  nNumElements--; // Decrement size of array since one element is removed.
		  } else if (0 == strMnemonic.CompareNoCase(strSearch)){
			  // Output an error message.  Not enough time has elapsed before the current mnemonic can be commanded.
			  m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 78, strMnemonic);
			  bFound = true;
		  } else {
			  nObjIdx++;
		  }
	  }
  }

  return eReturnStatus;
}

/***********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-07-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : CheckCmdPacingFile
//
//	Description :
//
//	Return :		BOOL: TRUE = SUCCESS, FALSE = FAIL
//
//	Parameters :	CString strMnemonic (passed in)
//
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************************/
BOOL CSchedVal::CheckCmdPacingFile(CString strMnemonic){

	BOOL	eReturnStatus = TRUE;
	if (!m_bCMDPaceFileLoaded){
		// File could not be loaded, return with fail status.
		// Skip all processing.
 		eReturnStatus = FALSE;
	}

	/*****
	If mnemonic has pacing restrictions
	  If no mnemonic is listed, set command pacing time.
      Else if mnemonic is listed add to mnemonic restricted list
        and calculate associated time tag.
        (note: add time duration to current time tag).
	*****/

	if (eReturnStatus){
		int  nNumElements = m_objCMDPaceMnemonic.GetSize();
		bool bFound = false;
		int  nObjIdx = 0;
		while (!bFound && nObjIdx < nNumElements){
			CString strSearch = ((strctCMDPaceMnemonicList *)m_objCMDPaceMnemonic[nObjIdx])->strMnemonic01;
			if (0 == strMnemonic.CompareNoCase(strSearch)){
				bFound = true;
			} else {
				nObjIdx++;
			}
		}

		if (bFound) {
			if (m_bCurrTimeFound){
				CString strMnemonic02 = ((strctCMDPaceMnemonicList *)m_objCMDPaceMnemonic[nObjIdx])->strMnemonic02;
				if (strMnemonic02.IsEmpty()){
					m_CSTPaceCMDTime = m_CSTCurrTime + ((strctCMDPaceMnemonicList *)m_objCMDPaceMnemonic[nObjIdx])->fDelayTime;
					m_bPaceCMDTimeFound = true;
				} else {
					strctCMDPaceRestrictList * pCMDPace = new strctCMDPaceRestrictList;
					pCMDPace->strMnemonic01             =  strMnemonic02;
					pCMDPace->cDelayTime                = m_CSTCurrTime + ((strctCMDPaceMnemonicList *)m_objCMDPaceMnemonic[nObjIdx])->fDelayTime;
					m_objCMDPaceRestriction.Add((CObject *)pCMDPace);
				}
			} else {
				CString strMnemonic02 = ((strctCMDPaceMnemonicList *)m_objCMDPaceMnemonic[nObjIdx])->strMnemonic02;
				if (strMnemonic02.IsEmpty()){
					m_CSTPaceCMDTime = m_CSTPrevTime + ((strctCMDPaceMnemonicList *)m_objCMDPaceMnemonic[nObjIdx])->fDelayTime;
					m_bPaceCMDTimeFound = true;
				} else {
					strctCMDPaceRestrictList * pCMDPace = new strctCMDPaceRestrictList;
					pCMDPace->strMnemonic01             = strMnemonic02;
					pCMDPace->cDelayTime                = m_CSTPrevTime + ((strctCMDPaceMnemonicList *)m_objCMDPaceMnemonic[nObjIdx])->fDelayTime;
					m_objCMDPaceRestriction.Add((CObject *)pCMDPace);
				}
			}
		}
	}

	return eReturnStatus;
}

/********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 01-16-02		version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : 		Parse_CMD_Line
//
//	Description :	This routine parses the CMD directive line and returns the
//		            mnemonic.
//
//	Return :		BOOL: TRUE = SUCCESS, FALSE = FAIL
//
//	Parameters :	CString strCMDLine (passed in)
//                  CString &strMnemonic (output)
//
//	Note :
//                  How to parse the cmd directive line?
//
//                  (0) CMD (1)[SCADDR] (2)MNEMONIC (3)SUBMNEMONIC ...
//                  The number of tokens must be greater than two.
//                  This routine assumes that the SCADDR is greater than 0.
//
//////////////////////////////////////////////////////////////////////////
***********************************************/
BOOL CSchedVal::Parse_CMD_Line(CString strCMDLine, CString &strMnemonic){

	BOOL	eReturnStatus = TRUE;
	strMnemonic.Empty();

  	int nPos;
	if ( -1 != (nPos = (strCMDLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strCMDLine.Delete(nPos, strCMDLine.GetLength() - nPos);
	}

	/*****
		First replace all equal signs with spaces before parsing.
		Parse line and determine # of tokens.
	    If number of tokens is less than two;
          output an error message.
          status is false.
	*****/

	strCMDLine.Replace(_T('='), _T(' '));

	CStringArray strCMDData;
	CTextFile::TokenizeString(strCMDLine, strCMDData);
	// Check size of token array.
	int nCMDDataSize = strCMDData.GetSize();
	if (nCMDDataSize < 2){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 15, strCMDLine);
		eReturnStatus = TRUE;
	} else if (nCMDDataSize == 2) {
		/*****
		 only two tokens present
		 If verify second token does not contain '$' or an integer,
			Set 2nd token to mnemonic variable.
		 else
		   output an error and return.
		*****/

		int nSCADDR = _ttoi(strCMDData[1]);
		int nPos   = strCMDData[1].Find(_T('$'));

		if ((nPos == -1) && (nSCADDR <= 0)){
			strMnemonic = strCMDData[1];
		} else {
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 15, strCMDLine);
			eReturnStatus = TRUE;
		}

	} else {

		/*****
        Else more than two tokens.
		    If second token contains '$' or an integer.
			     Set 3rd token to mnemonic variable.
			Else
				 Set 2nd token to mnemonic variable.
		******/

		int nSCADDR = _ttoi(strCMDData[1]);
		int nPos   = strCMDData[1].Find(_T('$'));

		if ((nPos == -1) && (nSCADDR <= 0)){
			strMnemonic = strCMDData[1];
		} else {
			strMnemonic = strCMDData[2];
		}
	}

	return eReturnStatus;
}

/****************************************
///////////////////////////////////////////////////////////
// RTCS Procedure Validation Section
///////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 01-31-02			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : LoadRTCSCmdRestrictFile()
//
//	Description :  This routine loads the RTCS command restriction file.
//                 Read the file line by line.  If allow directive found,
//                 add to the allow list.  Otherwise the mnemonic is added
//                 to the restricted list as is (including a wild card).
//
//	Return :		BOOL: TRUE = SUCCESFUL; FALSE = FAIL.
//
//	Parameters :	none
//
//	Note :
//			The format of the file is: string (mnemonic).
//          An allow directive can also be present: "cmdallow mnemonic"
//          A single wild card character is permitted: "*add".
//          Character must be either the first or last character in string
//
//
//////////////////////////////////////////////////////////////////////////
*****************************************/
BOOL CSchedVal::LoadRTCSCmdRestrictFile(){

	BOOL			eReturnStatus = TRUE;
	bool			bStatus = true;
	CTextFile		dataFile;
	CString         strRTCSCMDRestrictedFileName;

	m_bRTCSCMDRestrictedFileLoaded = true;

	// Get the file extension.
	CString strExt(_T(".rcmd"));
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nRTCSCmd])->GetDocString(strExt, CDocTemplate::filterExt);

//  Get the path for the RTCSCMDRestricted file.
//	int	nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int	nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
//	strRTCSCMDRestrictedFileName.Format(_T("\\\\%s\\%s\\%s\\%s\\"),
//		GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server.
//		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share.

	strRTCSCMDRestrictedFileName.Format(_T("%s\\%s\\%s\\%s\\%s\\"),
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC], 		// SC Specific Directory.
        GetAppRegFolder()->m_strDir[nFolderData]);                  // Folder containing the file.

	// Get the name of the RTCS CMD Restricted file.
	strRTCSCMDRestrictedFileName += GetAppRegSC()->m_strRTCSCmd[GetAppRegSite()->m_nDefaultSC];
	strRTCSCMDRestrictedFileName += strExt;

	// Need to validate file before RTCS Command Restriction loading.
	// Call syntax checker to validate the mnemonics.
	// Open the document
	CFileException cFE;
	short int	nType	= nRTCSCmd;
	char		rType[9]= {"RESTRICT"};
	if (TRUE != ((CMainFrame *)(CSchedMgrECPApp *)AfxGetApp()->m_pMainWnd)->SyntaxCheck(nType, rType, strRTCSCMDRestrictedFileName)){
	// if (TRUE != CMainFrame::SyntaxCheck(nType, rType, strRTCSCMDRestrictedFileName)){
		bStatus = false;
		m_bRTCSCMDRestrictedFileLoaded = false;
		eReturnStatus = false;
		CString strTitle (_T("RTCS Restricted Command file is invalid."));
		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::WARNING);
//		GetAppOutputBar()->OutputToEvents(strTitle, COutputBar::WARNING);
	} else if (TRUE != dataFile.Load(strRTCSCMDRestrictedFileName, CTextFile::RO, &cFE)){
		bStatus = false;
		m_bRTCSCMDRestrictedFileLoaded = false;
		// CString strError = ExceptionErrorText(&cFE);
		CString strMsg;
		strMsg.Format(_T("Unable to load RTCS Command restriction file: %s"), strRTCSCMDRestrictedFileName);
		m_NewGblValData.WriteValidationMsg(0, strMsg, 1);
	} else {
		dataFile.ExtractProlog();
		// Read the data from the file into the document

		int nCurObj = 0;
		while (nCurObj < dataFile.GetLineCount())
		{
		 	int nPos = 0;
			CString strMnemonic = dataFile.GetTextAt(nCurObj);
			strMnemonic.TrimLeft();
			strMnemonic.TrimRight();
			strMnemonic.MakeUpper();

			// First check if allow directive is present.
			if ( -1 != (nPos = (strMnemonic.Find(ctstrCMDALLOW, 0)))){
				int nDirSize = _tcsclen(ctstrCMDALLOW) + 1;
				strMnemonic.Delete(nPos, nDirSize - nPos);
				strMnemonic.TrimLeft();
				strMnemonic.TrimRight();
			//	Assume second token is valid mnemonic.
				if (!strMnemonic.IsEmpty()){
					m_strRTCSCmdAllow.Add(strMnemonic);
				}
			} else if (!strMnemonic.IsEmpty() && (ctchCOMMENT != strMnemonic[0])){
				m_strRTCSRestrict.Add(strMnemonic);
			}
			nCurObj++;
		}

		// Close the file, all of the data is now local
		CFileException cFE;
		if (TRUE != dataFile.Close(&cFE)){
			CString strErrorMsg = ExceptionErrorText(&cFE);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strRTCSCMDRestrictedFileName);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	}

	m_strRTCSCmdAllow.FreeExtra();
	m_strRTCSRestrict.FreeExtra();
	return eReturnStatus;
}

/**********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 01-31-02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : IsMnemonicRestricted
//
//	Description :
//     Checks mnemonic passed in againest the current list of restricted mnemonics.
//     First checks if a timetag is available.  Next checks if the delay time has passed.
//     If the time is passed the element is removed.  Otherwise an error message is generated
//	   stating that not enough time is elapsed for the previous command mnemonic.
//
//	Return :	TRUE = SUCCESS, FALSE = FAIL.
//
//	Parameters : CString strMnemonic
//
//	Note :
//////////////////////////////////////////////////////////////////////////
**********************************************/
BOOL CSchedVal::IsMnemonicRestricted(CString strMnemonic){

  BOOL	eReturnStatus = TRUE;
  bool bFound = false;
  int nObjIdx = 0;

  int nNumElementsAllow = m_strRTCSCmdAllow.GetSize();
  while (!bFound && nObjIdx < nNumElementsAllow){

	  if (0 == strMnemonic.CompareNoCase(m_strRTCSCmdAllow[nObjIdx])){
		  bFound = true;
	  } else {
		  nObjIdx++;
	  }
  }

  nObjIdx = 0;
  int nNumElementsRestrict = m_strRTCSRestrict.GetSize();
  while (!bFound && nObjIdx < nNumElementsRestrict){
	  CString strRestrictLeft, strRestrictRight;
	  CString strRestrictMnemonic = m_strRTCSRestrict[nObjIdx];
	  strMnemonic.MakeUpper();
	  strMnemonic.TrimLeft();
	  strMnemonic.TrimRight();
	  strRestrictMnemonic.MakeUpper();
	  strRestrictMnemonic.TrimLeft();
	  strRestrictMnemonic.TrimRight();

   	  int nPos = 0;
	  int nSize = strRestrictMnemonic.GetLength();
 	  if ( -1 != (nPos = (strRestrictMnemonic.Find(_T('*'), 0)))){
		 strRestrictLeft = strRestrictMnemonic.Left(nPos);
		 strRestrictRight = strRestrictMnemonic.Right(nSize - nPos - 1);
	  }

	  if ((!strRestrictRight.IsEmpty()) && (-1 != strMnemonic.Find(strRestrictRight))){
	     if ((!strRestrictLeft.IsEmpty()) && (-1 != strMnemonic.Find(strRestrictLeft))){
	  	    // Output an error message  Command is restricted.
	        m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 79, strMnemonic);
			bFound = true;
		 } else if (strRestrictLeft.IsEmpty()) {
		    // Output an error message  Command is restricted.
	        m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 79, strMnemonic);
		    bFound = true;
		 }
	  } else if (!strRestrictLeft.IsEmpty()) {
		 if ((strRestrictRight.IsEmpty()) && ( -1 != strMnemonic.Find(strRestrictLeft))){
			// Output an error message  Command is restricted.
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 79, strMnemonic);
			bFound = true;
		 }
	  } else if (0 == strMnemonic.CompareNoCase(strRestrictMnemonic)){
	     // Output an error message  Command is restricted.
         m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 79, strMnemonic);
         bFound = true;
	  }
	  nObjIdx++;
   }
   return eReturnStatus;
}

/**********************************
///////////////////////////////////////////////////////////
// RTCS Set File Validation Section
///////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-07-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : CheckForMovTopPtrCmd()
//
//	Description :	This routine verifies that the last command mnemonic
//					in the file is "ACE_STO_MOVE_TOP_08".  If the mnemonic
//					is not found a validation message is generated.
//
//	Return :		BOOL
//						TRUE	-	if mnemonic found.
//						FALSE	-	if mnemonic not found
//
//	Parameters :	CString strCmdLine
//
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************/
BOOL CSchedVal::CheckForMovTopPtrCmd(CString strCmdLine){

	BOOL eFound = FALSE;
	CStringArray strTokenData;

	TokenizeString(strCmdLine, strTokenData);

	bool bDone = false;
	for (int i = 0; i < strTokenData.GetSize() && !bDone; i++){

		strTokenData[i].TrimLeft();
		strTokenData[i].TrimRight();

		/* 	ctstrNO_OP
			ctstrMovTopPtr   	*/

			CString strTemp = strTokenData[i];

		if (0 == strTokenData[i].CompareNoCase(ctstrMovTopPtr)){
			eFound = TRUE;
			bDone = true;
		}
	}

	return eFound;
}

/******************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-07-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : CSchedVal::CheckForNoOpCmd()
//
//	Description :	This routine verifies that the first mnemonic in a
//                  RTCS procedure is a "ACE_NO_OP_FF" mnemonic.
//
//	Return :		BOOL
//						TRUE	-	if mnemonic found.
//						FALSE	-	if mnemonic not found
//
//	Parameters :	CString strCmdLine	-	RTCS directive line
//
//	Note :
//////////////////////////////////////////////////////////////////////////
***********************************************/
BOOL CSchedVal::CheckForNoOpCmd(CString strCmdLine){

	BOOL eFound = FALSE;
	CStringArray strTokenData;

	TokenizeString(strCmdLine, strTokenData);

	bool bDone = false;
	for (int i = 0; i < strTokenData.GetSize() && !bDone; i++){

		strTokenData[i].TrimLeft();
		strTokenData[i].TrimRight();

		/* 	ctstrNO_OP
			ctstrMovTopPtr   */

		CString strTemp = strTokenData[i];

		if (0 == strTokenData[i].CompareNoCase(ctstrNO_OP)){
			eFound = TRUE;
			bDone = true;
		}
	}
	return eFound;
}


/***********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 11-12-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : Validate_RTCSName
//
//	Description :  This function verifies that the RTCS procedure exists
//                 and is validated for RTCSs.
//
//	Return :		BOOL -
//						TRUE	-	RTCS exists and is valid.
//						FALSE	-	RTCS does not exist or failed validation.
//
//	Parameters :	CString strRTCSName
//
//	Note :
//////////////////////////////////////////////////////////////////////////
*************************************************/
BOOL CSchedVal::Validate_RTCSName(CString strRTCSName){
	BOOL eReturn_Status = TRUE;
	CString strExt(_T(".rtcs"));
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nRTCS])->GetDocString(strExt, CDocTemplate::filterExt);

	// File paths
	COXUNC	UNCRTCSPath (m_strRTCSSetFileName);
	UNCRTCSPath.File() = strRTCSName + strExt;
	CString strFullPath(UNCRTCSPath.Full());

	// Check if file exists.  First look in current directory, then cycle through search
	// path array, load data, check if valid prolog, and if valid location.

	CTextFile RTCS_TextFile;
	COXUNC  UNCSearchPath;
//	eValidLoc eVLoc = INVALID_TYPE;
	BOOL eSuccessful = TRUE;
	CFileException Exception;
	if (TRUE != RTCS_TextFile.Load(UNCRTCSPath.Full(), CTextFile::RO, &Exception)){
		int nMaxNumSearchPaths = m_SearchPathArray.GetSize();
		int nNumPaths = 0;
		bool bDone = false;
		while (!bDone && (nNumPaths < nMaxNumSearchPaths)){
			// UNCSearchPath.Directory() = m_SearchPathArray[nNumPaths];
			UNCSearchPath.File () = m_SearchPathArray[nNumPaths] + strRTCSName + strExt;
			UNCSearchPath.Full();

			if (TRUE !=	RTCS_TextFile.Load(UNCSearchPath.Full(), CTextFile::RO, &Exception)){
				eReturn_Status = FALSE;
				eSuccessful = FALSE;
			} else {
				eReturn_Status = TRUE;
				eSuccessful    = TRUE;
				bDone          = true;
			}
			nNumPaths++;
		}
		if (TRUE != eSuccessful){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			// GetAppOutputBar()->OutputToBuild(strErrorMsg, COutputBar::FATAL);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strErrorMsg, COutputBar::FATAL);
		}
		// Save the last filepath for printing out error messges.
		strFullPath = UNCSearchPath.Full();
	} else {
		eReturn_Status = TRUE;
	}


	if (TRUE == eSuccessful){
		eValidLoc eVLoc		   = INVALID_TYPE;
		eValidStatus  eVStatus = INVALID_STATUS;
//		BOOL bORSet = RTCS_TextFile.IsOverRideSet();
//		BOOL bValid = RTCS_TextFile.IsFileValid(eVStatus, eVLoc);

		// Check if file is valid and retrieve the execution location
		if (TRUE != RTCS_TextFile.IsFileValid(eVStatus, eVLoc)){
			// Priolog is invalid.
			eReturn_Status = FALSE;
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 1, strFullPath);
		} else if (TRUE == RTCS_TextFile.IsOverRideSet()){
			// Override directive in file is set.
			CString strMsg(_T("Validation status was overridden!"));
			CString strTitle;
			strTitle.Format(_T("%s %s"), strMsg, strFullPath);
		//	GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strTitle, 0);
		} else if (FAIL == eVStatus || WARNING == eVStatus){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 11, strFullPath);
			eReturn_Status = FALSE;
		}

		if (eVLoc != RTCS){
			// Verify that execution location is RTCS.
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 25, UNCSearchPath.Full());
			eReturn_Status = FALSE;
		}

		CSchedTimeSpan cSt(((RTCS_TextFile.GetProlog())->GetExectime())->GetTimeSpan(), 0);
		if (m_bCurrTimeFound){
			m_CSTRTCSWinTime = m_CSTCurrTime + cSt;
			m_bRTCSWinTimeFound = true;
		} else if (m_bPrevTimeFound){
			m_CSTRTCSWinTime = m_CSTPrevTime + cSt;
			m_bRTCSWinTimeFound = true;
		}

		int nRtnLine = 0;
		int nLine = 0;
		int nMaxLineCount = RTCS_TextFile.GetLineCount();

		while (nLine < nMaxLineCount){
			CString strLine;
			strLine = RTCS_TextFile.GetDirLine(nLine, nRtnLine);

			CString strLineTrimmed(strLine);
//			bool bWaitDirFound = false;
			strLineTrimmed.TrimLeft ();

			if (strLineTrimmed.IsEmpty()){
				// Empty line
			} else if (ctchCOMMENT == strLineTrimmed[0]){
				// Comment
			} else {
				strLine.MakeUpper ();
				if(ScanLine(strLine, ctstrCMD) == TRUE){
					Validate_CMD_Dir(strLine);
				}
			}
			nLine += nRtnLine;
		}

		if (TRUE != RTCS_TextFile.Close(CTextFile::NOWRITE, &Exception)){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strFullPath);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	}
	return eReturn_Status;
}

/***************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 11-12-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : LookUpLabel()
//
//	Description :	This function looks up a RTCS label and returns the
//                  associated RTCS procedure name.
//
//	Return :		CString -
//						Name of RTCS procedure
//						empty CString if RTCS procedure not found
//
//	Parameters :	CString -
//							strLabel	-	RTCS label to search for.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
**************************************************/
CString CSchedVal::LookUpLabel(CString strLabel){

	CString strRTCSProcName;
	BOOL eReturnStatus = TRUE;

	if (!m_bReadRTCSSetFile){
		if (TRUE != ReadRTCSSetFile()){
			eReturnStatus = FALSE;
		}
	}

	if (eReturnStatus){
		int  nNumElements = m_objRTCSSetProc.GetSize();
		bool bFound = false;
		int  nObjIdx = 0;
		CString strTemp = ((strctRTCSSetProcList *)m_objRTCSSetProc[nObjIdx])->strLabel;
		while (!bFound && nObjIdx < nNumElements){
			if (0 == strLabel.CompareNoCase(((strctRTCSSetProcList *)m_objRTCSSetProc[nObjIdx])->strLabel)){
				bFound = true;
				strRTCSProcName = ((strctRTCSSetProcList *)m_objRTCSSetProc[nObjIdx])->strName;
			} else {
				nObjIdx++;
			}
		}
	}

	return strRTCSProcName;
}

/********************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 11-12-01			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : ReadRTCSSetFile()
//
//	Description :	This function reads the RTCS set file and loads the data
//                  into memory.
//
//	Return :		BOOL -
//						TRUE	-	SUCCESS
//						FALSE	-	FAIL
//
//	Parameters :	none	-	 void
//
//	Note :
//////////////////////////////////////////////////////////////////////////
****************************************************/
BOOL CSchedVal::ReadRTCSSetFile(){

	BOOL eReturn_Status = TRUE;
	CFileException Exception;
	eReturn_Status = TRUE;

	CTextFile RTCSSet;
	// Check if file exists.
	if (TRUE != RTCSSet.Load(m_strRTCSSetFileName, CTextFile::RO, &Exception)){
		CString strErrorMsg = ExceptionErrorText(&Exception);
		CString strMsg;
		strMsg.Format(_T("%s. %s"), strErrorMsg, m_strRTCSSetFileName);
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		eReturn_Status = FALSE;
		m_eValRTCSProcs = FALSE;
		m_bReadRTCSSetFile = true;
		return eReturn_Status;
	}

	eValidLoc eVLoc		   = INVALID_TYPE;
	eValidStatus  eVStatus = INVALID_STATUS;
//	BOOL bORSet = RTCSSet.IsOverRideSet();
	if (TRUE != RTCSSet.IsFileValid(eVStatus, eVLoc)){
		// Priolog is invalid.
		eReturn_Status = FALSE;
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 1, m_strRTCSSetFileName);
		eReturn_Status = FALSE;
		m_eValRTCSProcs = FALSE;
		m_bReadRTCSSetFile = true;
		if (TRUE != RTCSSet.Close(CTextFile::NOWRITE, &Exception)){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. %s"), strErrorMsg, m_strRTCSSetFileName);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	} else if (TRUE == RTCSSet.IsOverRideSet()){
		// Override directive in file is set.
		CString strMsg(_T("Validation status was overridden!"));
		CString strTitle;
		strTitle.Format(_T("%s %s"), strMsg, m_strRTCSSetFileName);
//		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strTitle, 0);
	} else if (FAIL == eVStatus || WARNING == eVStatus){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 11, m_strRTCSSetFileName);
		eReturn_Status = FALSE;
	}

	if (eVLoc != RTCS){
		// Verify that execution locations match
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 25, m_strRTCSSetFileName);
		eReturn_Status = FALSE;
		m_eValRTCSProcs = FALSE;
		m_bReadRTCSSetFile = true;
		if (TRUE != RTCSSet.Close(CTextFile::NOWRITE, &Exception)){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. %s"), strErrorMsg, m_strRTCSSetFileName);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	} else {
		CString strMsg(_T("Successfully loaded RTCS Set file: "));
		CString strTitle;
		strTitle.Format(_T("%s %s"), strMsg, m_strRTCSSetFileName);
		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
		m_bReadRTCSSetFile = true;

		int nRtnLine = 0;
		int nLine = 0;
		int nMaxLineCount = RTCSSet.GetLineCount();
		while (nLine < nMaxLineCount){
			CString strLine, strSave;
			strLine = RTCSSet.GetDirLine(nLine, nRtnLine);
			strSave = strLine;  // Save line for error messages.
 			int nPos= 0;
			// Remove trailing comment.
			if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
				strLine.Delete(nPos, strLine.GetLength() - nPos);
			}

			// Remove directive
			nPos = WhereIsToken(strLine, ctstrInclude);
			if (nPos >= 0){
				int nLen = _tcsclen(ctstrInclude);
				strLine.Delete(nPos, nLen);
				strLine.TrimLeft();
				strLine.TrimRight();

				/*****
		 			Next token is the RTCS name
					call validate rtcs_dir
					check if file exists and has passed validation for a RTCS
				 *****/

				// Extract out the RTS name
				CString strRtcsName;
				int nRtcsNameIndex = -1;
				strRtcsName = strLine;
				if (-1 != (nRtcsNameIndex = strLine.Find(_T(' '), 0))){
					if (nRtcsNameIndex > 0){
						int nLength = strRtcsName.GetLength();
						strRtcsName.Delete(nRtcsNameIndex, nLength - nRtcsNameIndex);
						// Remove the RTCS name for passing to the validate RTCS dir routine.
						strLine.Delete(0, nRtcsNameIndex);
						strLine.TrimLeft();
						strLine.TrimRight();
						strRtcsName.TrimLeft();
						strRtcsName.TrimRight();
					}
				}

				/*****
					Next token is the label.
					First check if the label directive is present.
					Next find either space or end of line.
					Extract out the label name and save.
				*****/

				// Extract out the label.
				strLine.MakeLower();
				CString	strLabelName = strLine;
				nPos = strLine.Find(ctstrLabelEq,0);
				if (nPos >= 0){
					// Label directive is present.
					int nLabelNameDirPairIndex = strLine.Find(_T(' '), 0);
					int nLabelNameLength = _tcsclen(ctstrLabelEq);

					if (-1 == nLabelNameDirPairIndex){
						// No space found; use end of line instead.
						nLabelNameDirPairIndex = strLine.GetLength();
					} else {
						int nStrSize = strLabelName.GetLength();
						strLabelName.Delete(nLabelNameDirPairIndex, nStrSize - nLabelNameDirPairIndex);
					}

					strLabelName.Delete(nPos, nLabelNameLength - nPos);
					strLine.Delete(0, nLabelNameDirPairIndex);
					strLabelName.TrimLeft();
					strLabelName.TrimRight();
					strLine.TrimLeft();
					strLine.TrimRight();
				}

				// Next token is start parameter.  This parameter is optional.

				int nIndexValue = 0;
				if (!strLine.IsEmpty()){
					strLine.MakeUpper();
					nPos = WhereIsToken(strLine, ctstrSTARTEQ);
					// strStartParam = strLine;
					if (nPos >= 0){
						// Find space and delete extraneous characters
						int nSpaceIndex = strLine.Find(_T(' '), 0);
						int nStartIndexLength = _tcsclen(ctstrSTARTEQ);
						if (-1 == nSpaceIndex){
							// No space found; use end of line instead.
							nSpaceIndex = strLine.GetLength();
						}
						strLine.Delete(0, nStartIndexLength);
						strLine.TrimLeft();
						strLine.TrimRight();
						nIndexValue = _ttoi(strLine);
					}
				}
				strctRTCSSetProcList *pRTCSSetProc = new strctRTCSSetProcList;
				pRTCSSetProc->strLabel         = strLabelName;
				pRTCSSetProc->strName          = strRtcsName;
				pRTCSSetProc->nIndex           = nIndexValue;
				m_objRTCSSetProc.Add((CObject *)pRTCSSetProc);
			}
			nLine += nRtnLine;
		}
		if (TRUE != RTCSSet.Close(CTextFile::NOWRITE, &Exception)){
			CString strErrorMsg = ExceptionErrorText(&Exception);
			CString strMsg;
			strMsg.Format(_T("%s. %s"), strErrorMsg, m_strRTCSSetFileName);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	}

		m_objRTCSSetProc.FreeExtra();
		return eReturn_Status;
	}

/*
**********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 03-22-02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CSchedVal::Validate_StarData_Dir
//
//	Description :
//
//
//	Return :		BOOL
//
//	Parameters :	CString strLine
//
//	Note :	Not implemented.
//////////////////////////////////////////////////////////////////////////
*********************************
BOOL CSchedVal::Validate_StarData_Dir (CString strLine){
	BOOL eReturn_Status = TRUE;
	return eReturn_Status;
}
*/
/***********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 03-22-02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CSchedVal::Validate_StarStart_Dir
//
//	Description :	This routine validates the calls to the STAR
//					minischedules.
//
//	Return :		BOOL -
//						TRUE	-	SUCCESS
//						FALSE	-	FAIL
//
//	Parameters :	CString strLine
//						STOL directive line.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
*********************************************/
BOOL CSchedVal::Validate_StarStart_Dir(CString strLine){

	BOOL eReturn_Status = TRUE;
	int nPos= 0;
	CString strProcName;

	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove directive
	CString strUpperCaseStartLine(strLine);
	strUpperCaseStartLine.MakeUpper();
	nPos = (strUpperCaseStartLine.Find(ctstrSTART, 0));

	if (nPos < 0){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 29);
		eReturn_Status = FALSE;
	} else {
		strProcName = strLine;
		int nLen = _tcsclen(ctstrSTART);
		strProcName.Delete(0, nPos + nLen);
		strProcName.TrimLeft();
		strProcName.TrimRight();

		int nProcNameIndex = -1;
		// First look for a "(".
		if (-1 == (nProcNameIndex = strProcName.Find(_T('('), 0))){
			// If a "(" is not found, then look for space.
			nProcNameIndex = strProcName.Find(_T(' '), 0);
		}

		// Extract out the Proc name
		if (nProcNameIndex > 0){
			int nLength = strProcName.GetLength();
			strProcName.Delete(nProcNameIndex, nLength - nProcNameIndex);
			strProcName.TrimLeft();
			strProcName.TrimRight();
		}

		//  Check what stol proc was called.
		// File paths
		COXUNC	UNCPrcPath (strProcName);
		CString strPrcFileName(UNCPrcPath.File());
		CString strStarMS = GetAppRegSC()->m_strStarMiniSched[GetAppRegSite()->m_nDefaultSC];
		CString strStarIOMS = GetAppRegSC()->m_strStarIOMiniSched[GetAppRegSite()->m_nDefaultSC];

		if (0 == strPrcFileName.CompareNoCase(strStarMS)){
			Validate_StarGrd_Proc(strLine);
		} else if ( 0 == strPrcFileName.CompareNoCase(strStarIOMS)){
			Validate_StarObj_Proc(strLine);
		}
	}

	return eReturn_Status;
}

/*
**********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 03-22-02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CSchedVal::Validate_StarInfo_Dir
//
//	Description :
//
//
//	Return :		BOOL
//						TRUE	-	SUCCESS
//						FALSE	-	FAIL
//
//
//	Parameters :	CString strLine
//						STOL directive line.
//	Note :
//			Not implemented
//////////////////////////////////////////////////////////////////////////
**********************************************
BOOL CSchedVal::Validate_StarInfo_Dir(CString strLine){
	BOOL eReturn_Status = TRUE;
	return eReturn_Status;
}
*/

/****************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 03-22-02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :	CSchedVal::Validate_FrameObj_Proc
//
//	Description :	This routine validates the scan instrument object
//					minischedule.
//
//	Return :		BOOL	-
//						TRUE	-	valid
//						FALSE	-	invalid
//
//	Parameters :	CString strLine
//
//	Note :
//////////////////////////////////////////////////////////////////////////
****************************************************/
BOOL CSchedVal::Validate_FrameObj_Proc(CString strLine){

	BOOL eReturn_Status = TRUE;
	CParseFrameIOMS FrameObjParse;

	if (!FrameObjParse.ParseData(strLine)){
		CString strError;
		strError.Format (IDS_BAD_DATA, GetCurrentFileLineNum ());
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strError, 1);
		return FALSE;
	}

	CString strInstrValue = FrameObjParse.GetINST();
	if ((0 != strInstrValue.CompareNoCase(m_strInstrValue))){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 5, strInstrValue);
		eReturn_Status = FALSE;
	}

	CString strSideValue = FrameObjParse.GetSPLSIDE();
	// Need to convert to minus_x or plus_x
	if (!m_strSideValue.IsEmpty()){
		if (0 == m_strSideValue.CompareNoCase(ctstrEAST)){
			if (0 == strSideValue.CompareNoCase(ctstrPLUS_X)){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 53, strSideValue);
				eReturn_Status = FALSE;
			}
		} else {
			if (0 == strSideValue.CompareNoCase(ctstrMINUS_X)){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 53, strSideValue);
				eReturn_Status = FALSE;
			}
		}
	}
	return eReturn_Status;
}

/**************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 03-22-02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function :
//
//	Description :	This routine validates the scan minischedule that
//                  commands the instrument directly.  Only the passed
//					parameters are checked.
//
//	Return :		BOOL	-
//						TRUE	-	valid
//						FALSE	-	invalid
//
//	Parameters :	CString strLine	-	STOL command line.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
***************************************************/
BOOL CSchedVal::Validate_FrameGrd_Proc(CString strLine){
	BOOL eReturn_Status = TRUE;
	CParseFrameMS FrameMSParse;

	// Parse master schedule line that is calling the frame minischedule
	if (!FrameMSParse.ParseData(strLine)){
		CString strError;
		strError.Format (IDS_BAD_DATA, GetCurrentFileLineNum ());
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strError, 1);
		return FALSE;
	}

	// Get the instrument type and verify it matches what was specified
	// in the scan directive.
	CString strInstrValue = FrameMSParse.GetINST();
	if ((0 != strInstrValue.CompareNoCase(m_strInstrValue))){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 5, strInstrValue);
		eReturn_Status = FALSE;
	}

	// Get the spacelook side (EAST or WEST).
	// Need to convert to minus_x or plus_x
	CString strSideValue = FrameMSParse.GetSPLSIDE();
	if (!m_strSideValue.IsEmpty()){
		if (0 == m_strSideValue.CompareNoCase(strSideValue)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 53, strSideValue);
			eReturn_Status = FALSE;
		}
	}

	double dTotalEWStart = 0.0;
	double dTotalNSStart = 0.0;
	double dTotalEWStop  = 0.0;
	double dTotalNSStop  = 0.0;
	// Which instrument selected, drives what calculations are performed.
	if (0 == strInstrValue.CompareNoCase(ctstrIMGR)){ // Value: imager
		// X1 Cycles => E/W Start Coordinates in cycles
		CString strTemp;
		strTemp = FrameMSParse.GetX1CYC();
		int nEWCycle = _ttoi(strTemp);
		if ((nEWCycle < 0) || (nEWCycle > ctnMaxEWCycles )){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalEWStart += nEWCycle * ctnMaxImgIncrements;
		}

		// X1 increments => E/W Start Coordinates in increments
		strTemp = FrameMSParse.GetX1INC();
		int nEWIncr = _ttoi(strTemp);
		if ((nEWIncr < 0) || (nEWIncr >= ctnMaxImgIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalEWStart += nEWIncr;
		}

		// X2 cycles => E/W Stop Coordinate in cycles
		strTemp = FrameMSParse.GetX2CYC();
		nEWCycle = _ttoi(strTemp);
		if ((nEWCycle < 0) || (nEWCycle > ctnMaxEWCycles)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalEWStop += nEWCycle * ctnMaxImgIncrements;
		}

		// X2 increments => E/W Stop Coordinates in increments
		strTemp = FrameMSParse.GetX2INC();
		nEWIncr = _ttoi(strTemp);
		if ((nEWIncr < 0) || (nEWIncr >= ctnMaxImgIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalEWStop += nEWIncr;
		}

		// Y1 Cycles => N/S Start Coordinates in cycles
		strTemp = FrameMSParse.GetY1CYC();
		int nNSCycle = _ttoi(strTemp);
		if ((nNSCycle < 0) || (nNSCycle > ctnMaxNSCycles)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalNSStart += nNSCycle * ctnMaxImgIncrements;
		}

		// Y1 increments => N/S Start Coordinates in increments
		strTemp = FrameMSParse.GetY1INC();
		int nNSIncr = _ttoi(strTemp);
		if ((nNSIncr < 0) || (nNSIncr >= ctnMaxImgIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalNSStart += nNSIncr;
		}

		// Y2 cycles => N/S Stop Coordinates in cycles
		strTemp = FrameMSParse.GetY2CYC();
		nNSCycle = _ttoi(strTemp);
		if ((nNSCycle < 0) || (nNSCycle > ctnMaxNSCycles)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalNSStop += nNSCycle * ctnMaxImgIncrements;
		}

		// Y2 increments => N/S Stop Increments
		strTemp = FrameMSParse.GetY2INC();
		nNSIncr = _ttoi(strTemp);
		if ((nNSIncr < 0) || (nNSIncr >= ctnMaxImgIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalNSStop += nNSIncr;
		}

		if (TRUE == eReturn_Status){

			// Retrieve the instrument E/W nadir point in degrees
			double dScanDegsEWNadirImgr = _tcstod(GetAppRegSC()->m_strScanDegsEWNadirImgr[GetAppRegSite()->m_nDefaultSC], NULL);
			// double dTotIncrLimitEW = 0.0;
			// Convert nadir point to total increments
			// double dTotIncrLimitEW = (dScanDegsEWNadirImgr / ctdDegs2IncrEWImgr);
			double dTotIncrLimitEW = ((dScanDegsEWNadirImgr * ctdIncr2CyclesImgr) / (2 * ctdDegrees2Cycles));

			// Retrieve the instrument E/W center point in increments
			double dInstrCenterPtImgrEW = (double)GetAppRegSC()->m_nInstrCenterPtImgrEW[GetAppRegSite()->m_nDefaultSC];
			// Calculate the instrument low limit
			double dLowLimitEW = dInstrCenterPtImgrEW - dTotIncrLimitEW;
			// Calculate the instrument high limit
			double dHiLimitEW  = dInstrCenterPtImgrEW + dTotIncrLimitEW;

			// Verify coordinates are with in the valid range.
			if ((dTotalEWStart - dLowLimitEW) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalEWStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 45, strMsg);
				eReturn_Status = FALSE;
			}

			// Verify coordinates are with in the valid range.
			if ((dHiLimitEW - dTotalEWStart) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalEWStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 44, strMsg);
				eReturn_Status = FALSE;
			}

			// Verify coordinates are with in the valid range.
			if ((dTotalEWStop - dLowLimitEW) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalEWStop);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 45, strMsg);
				eReturn_Status = FALSE;
			}

			// Verify coordinates are within the valid range.
			if ((dHiLimitEW - dTotalEWStop) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalEWStop);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 44, strMsg);
				eReturn_Status = FALSE;
			}

			// Verify coordinates are within the valid range.
			if ((fabs(dTotalEWStop - dTotalEWStart)) < 4) {
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 82, strTemp);
				eReturn_Status = FALSE;
			}

			// Retrieve the N/S nadir point in degrees
			double dScanDegsNSNadirImgr = _tcstod(GetAppRegSC()->m_strScanDegsNSNadirImgr[GetAppRegSite()->m_nDefaultSC], NULL);
			// Convert to total increments
			// double dTotIncrLimitNS = (dScanDegsNSNadirImgr / ctdDegs2IncrNSImgr);
			double dTotIncrLimitNS = ((dScanDegsNSNadirImgr * ctdIncr2CyclesImgr) / (ctdDegrees2Cycles));
			// Retrieve the instrument N/S center point.
			double dInstrCenterPtImgrNS = (double)GetAppRegSC()->m_nInstrCenterPtImgrNS[GetAppRegSite()->m_nDefaultSC];
			// Calculate the lower instrument limit
			double dLowLimitNS = dInstrCenterPtImgrNS - dTotIncrLimitNS;
			// Calculate the upper instrument limit
			double dHiLimitNS  = dInstrCenterPtImgrNS + dTotIncrLimitNS;

			// Verify coordinates are within the valid range
			if ((dTotalNSStart - dLowLimitNS) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalNSStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 47, strMsg);
				eReturn_Status = FALSE;
			}

			// Verify coordinates are within the valid range
			if ((dHiLimitNS - dTotalNSStart) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalNSStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 46, strMsg);
				eReturn_Status = FALSE;
			}

			// Verify coordinates are within the valid range//
			if ((dTotalNSStop - dLowLimitNS) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalNSStop);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 47, strMsg);
				eReturn_Status = FALSE;
			}

			// Verify coordinates are within the valid range
			if ((dHiLimitNS - dTotalNSStop) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalNSStop);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 46, strMsg);
				eReturn_Status = FALSE;
			}

			/* Check if the difference between the North and South start and stop
			   cycle/increment coordinates are not an integral multiple of 28. */
			if (fmod((dTotalNSStop - dTotalNSStart),28) != 0.0){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 81, strTemp);
				eReturn_Status = FALSE;
			}

			/* Check if the West start and stop addresses are not an integral
			   multiple of data blocks from the space look addresses. */
			double dSpaceLookImgrWest = (double)GetAppRegSC()->m_nSpaceLookImgrWest[GetAppRegSite()->m_nDefaultSC];
			double dTemp = fmod((dTotalEWStart - dSpaceLookImgrWest), 4);
			if ((dTemp - 0.0) > 0.000001){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 49);
				eReturn_Status = FALSE;
			}

			/* Check if the East start and stop addresses are not an integral
			   multiple of data blocks from the space look addresses. */
			double dSpaceLookImgrEast = (double)GetAppRegSC()->m_nSpaceLookImgrEast[GetAppRegSite()->m_nDefaultSC];
			dTemp = fmod((dTotalEWStop - dSpaceLookImgrEast), 4);
			if (fabs(dTemp - 0.0) > 0.000001){
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 80);
				eReturn_Status = FALSE;
			}

			/*
				Check if the East-West frame boundary on the space look side
				is 400 blocks (1600 increments) or less from the space look
				address for space clamp frames.
				This only applies to space clamp frames.
			 */

			CString strModeValue = FrameMSParse.GetSMODE();
			CString strPriorityValue = FrameMSParse.GetPRIOR();
			// int nTemp01 = strModeValue.CompareNoCase(ctstrMODE_1);
			// int nTemp02 = strPriorityValue.CompareNoCase(ctstrNORMAL);
			// int nTemp03 = strPriorityValue.CompareNoCase(ctstrPRIORITY_2);
			if (0 == strModeValue.CompareNoCase(ctstrMODE_0)
					|| (0 == strModeValue.CompareNoCase(ctstrMODE_1)
					&& 0 == strPriorityValue.CompareNoCase(ctstrPRIORITY_1))){
				if (0 == strSideValue.CompareNoCase(ctstrEAST)){
					double dSpaceLookImgrEast = (double)GetAppRegSC()->m_nSpaceLookImgrEast[GetAppRegSite()->m_nDefaultSC];
					if (fabs(dTotalEWStart - dSpaceLookImgrEast) < 1600){
						CString strMsg;
						strMsg.Format(_T("%.0f"), dTotalEWStart);
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 91, strMsg);
						eReturn_Status = FALSE;
					}
				} else {
					double dSpaceLookImgrWest = (double)GetAppRegSC()->m_nSpaceLookImgrWest[GetAppRegSite()->m_nDefaultSC];
					if (fabs(dSpaceLookImgrWest - dTotalEWStart) < 1600){
						CString strMsg;
						strMsg.Format(_T("%.0f"), dTotalEWStart);
						m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 91, strMsg);
						eReturn_Status = FALSE;
					}
				}
			}
		}
	} else if (0 == strInstrValue.CompareNoCase(ctstrSDR)){

		// X1 Cycles => E/W Start Coordinates
		CString strTemp;
		strTemp = FrameMSParse.GetX1CYC();
		int nEWCycle = _ttoi(strTemp);
		if ((nEWCycle < 0) || ( nEWCycle > ctnMaxEWCycles)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalEWStart += nEWCycle * ctnMaxSndIncrements;
		}

		// X1 increments => E/W Start Coordinates
		strTemp = FrameMSParse.GetX1INC();
		int nEWIncr = _ttoi(strTemp);
		if ((nEWIncr < 0) || (nEWIncr >= ctnMaxSndIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalEWStart += nEWIncr;
		}

		// X2 cycles => E/W Stop Coordinate
		strTemp = FrameMSParse.GetX2CYC();
		nEWCycle = _ttoi(strTemp);
		if ((nEWCycle < 0) || (nEWCycle  >= ctnMaxEWCycles)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalEWStop += nEWCycle * ctnMaxSndIncrements;
		}

		// X2 increments => E/W Stop Coordinate
		strTemp = FrameMSParse.GetX2INC();
		nEWIncr = _ttoi(strTemp);
		if ((nEWIncr < 0) || (nEWIncr > ctnMaxSndIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalEWStop += nEWIncr;
		}

		// Y1 Cycles => N/S Start Coordinates
		strTemp = FrameMSParse.GetY1CYC();
		int nNSCycle = _ttoi(strTemp);
		if ((nNSCycle < 0) || (nNSCycle > ctnMaxCmdScanNSCyc)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalNSStart += nNSCycle * ctnMaxSndIncrements;
		}

		// Y1 increments => N/S Start Coordinates
		strTemp = FrameMSParse.GetY1INC();
		int nNSIncr = _ttoi(strTemp);
		if ((nNSIncr < 0) ||  (nNSIncr >= ctnMaxSndIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			// Convert to total increments
			dTotalNSStart += nNSIncr;
		}

		// Y2 cycles => N/S Stop Coordinates
		strTemp = FrameMSParse.GetY2CYC();
		nNSCycle = _ttoi(strTemp);
		if ((nNSCycle < 0) || (nNSCycle > ctnMaxCmdScanNSCyc)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			dTotalNSStop += nNSCycle * ctnMaxSndIncrements;
		}

		// Y2 increments => N/S Stop Coordinates
		strTemp = FrameMSParse.GetY2INC();
		nNSIncr = _ttoi(strTemp);
		if ((nNSIncr < 0) || (nNSIncr >=ctnMaxSndIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			dTotalNSStop += nNSIncr;
		}

		if (TRUE == eReturn_Status){
			// double dTotIncrLimitEW = 0.0;
			double dScanDegsEWNadirSdr = _tcstod(GetAppRegSC()->m_strScanDegsEWNadirSdr[GetAppRegSite()->m_nDefaultSC], NULL);
		//	dTotIncrLimitEW = (dScanDegsEWNadirSdr / ctdDegs2IncrEWSdr);
			double dTotIncrLimitEW = ((dScanDegsEWNadirSdr * ctdIncr2CyclesSdr) / (2 * ctdDegrees2Cycles));

			double dInstrCenterPtSdrEW = GetAppRegSC()->m_nInstrCenterPtSdrEW[GetAppRegSite()->m_nDefaultSC];
			double dLowLimitEW = dInstrCenterPtSdrEW - dTotIncrLimitEW;
			double dHiLimitEW  = dInstrCenterPtSdrEW + dTotIncrLimitEW;

			//
			if ((dTotalEWStart - dLowLimitEW) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalEWStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 57, strMsg);
				eReturn_Status = FALSE;
			}

			if ((dHiLimitEW - dTotalEWStart) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalEWStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 56, strMsg);
				eReturn_Status = FALSE;
			}
			//
			if ((dTotalEWStop - dLowLimitEW) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalEWStop);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 57, strMsg);
				eReturn_Status = FALSE;
			}

			if ((dHiLimitEW - dTotalEWStop) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalEWStop);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 56, strMsg);
				eReturn_Status = FALSE;
			}

			if ((fabs(dTotalEWStop - dTotalEWStart)) < 4) {
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 82, strTemp);
				eReturn_Status = FALSE;
			}

			// double dTotIncrLimitNS = 0.0;
			double dScanDegsNSNadirSdr = _tcstod(GetAppRegSC()->m_strScanDegsNSNadirSdr[GetAppRegSite()->m_nDefaultSC], NULL);
			// dTotIncrLimitNS = (dScanDegsNSNadirSdr / ctdDegs2IncrNSSdr);

			double dTotIncrLimitNS = ((dScanDegsNSNadirSdr * ctdIncr2CyclesSdr) / (ctdDegrees2Cycles));

			double dInstrCenterPtSdrNS = GetAppRegSC()->m_nInstrCenterPtSdrNS[GetAppRegSite()->m_nDefaultSC];
			double dLowLimitNS = dInstrCenterPtSdrNS - dTotIncrLimitNS;
			double dHiLimitNS  = dInstrCenterPtSdrNS + dTotIncrLimitNS;

			//
			if ((dTotalNSStart - dLowLimitNS) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalNSStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 59, strMsg);
				eReturn_Status = FALSE;
			}

			if ((dHiLimitNS - dTotalNSStart) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalNSStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 58, strMsg);
				eReturn_Status = FALSE;
			}
						//
			if ((dTotalNSStop - dLowLimitNS) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalNSStop);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 59, strMsg);
				eReturn_Status = FALSE;
			}

			if ((dHiLimitNS - dTotalNSStop) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalNSStop);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 58, strMsg);
				eReturn_Status = FALSE;
			}

			if (0 == strSideValue.CompareNoCase(ctstrEAST)){
				double dSpaceLookSdrEast = (double)GetAppRegSC()->m_nSpaceLookSdrEast[GetAppRegSite()->m_nDefaultSC];
				if (fabs(dTotalEWStart - dSpaceLookSdrEast) < 1600){
					CString strMsg;
					strMsg.Format(_T("%.0f"), dTotalEWStart);
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 91, strMsg);
					eReturn_Status = FALSE;
				}
			} else {
				double dSpaceLookSdrWest = (double)GetAppRegSC()->m_nSpaceLookSdrWest[GetAppRegSite()->m_nDefaultSC];
				if (fabs(dSpaceLookSdrWest - dTotalEWStart) < 1600){
					CString strMsg;
					strMsg.Format(_T("%.0f"), dTotalEWStart);
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 91, strMsg);
					eReturn_Status = FALSE;
				}
			}
		}
	}
	return eReturn_Status;
}

/*************************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 03-22-02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	CSchedVal::Validate_StarObj_Proc
//
//	Description :	This routine validates the star instrument object
//					minischedule.  Only the passed parameters are checked.
//
//	Return :		BOOL	-
//						TRUE	-	valid
//						FALSE	-	invalid
//
//	Parameters :	CString strLine	-	STOL command line.
//
//	Note :
//////////////////////////////////////////////////////////////////////////
**********************************************************/
BOOL CSchedVal::Validate_StarObj_Proc(CString strLine){
	BOOL eReturn_Status = TRUE;

	CParseStarIOMS StarObjParse;
	CString strInstrValue;

	if (!StarObjParse.ParseData(strLine)){
		CString strError;
		strError.Format (IDS_BAD_DATA, GetCurrentFileLineNum ());
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strError, 1);
		return FALSE;
	}

	strInstrValue = StarObjParse.GetINST();
	if ((0 != strInstrValue .CompareNoCase(m_strInstrValue))){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 5, strInstrValue);
		eReturn_Status = FALSE;
	}

	// Set Star bit to off since in an instrument object minischedule.
	// Otherwise schedule will not pass validation.
	// m_strStarBit = ctstrOFF;

	return eReturn_Status;
}

/***************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 03-22-02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Description :	This routine validates the star minischedule that
//                  commands the instrument directly.  Only the passed
//					parameters are checked.
//
//	Return :		BOOL	-
//						TRUE	-	valid
//						FALSE	-	invalid
//
//	Parameters :	none
//
//	Note :
//////////////////////////////////////////////////////////////////////////
**************************************/
BOOL CSchedVal::Validate_StarGrd_Proc(CString strLine){
	BOOL eReturn_Status = TRUE;

	CParseStarMS StarParse;
	CString strInstrValue;

	if (!StarParse.ParseData(strLine)){
		CString strError;
		strError.Format (IDS_BAD_DATA, GetCurrentFileLineNum ());
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strError, 1);
		return FALSE;
	}

	m_strStarBit = StarParse.GetSTARBIT();
	m_strStarBit.Remove(_T('\"'));

	strInstrValue = StarParse.GetINST();
	if ((0 != strInstrValue.CompareNoCase(m_strInstrValue))){
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 5, strInstrValue);
		eReturn_Status = FALSE;
	}

	if (0 == strInstrValue.CompareNoCase(ctstrIMGR)){ // Value: imager
	  	double dTotalEWStart = 0;
		double dTotalNSStart = 0;

		CString strTemp;
		strTemp = StarParse.GetXCYC();
		int nEWCycle = _ttoi(strTemp);
		if ((nEWCycle < 0) || (nEWCycle > ctnMaxEWCycles)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		}else {
			dTotalEWStart += nEWCycle * ctnMaxImgIncrements;
		}

		strTemp = StarParse.GetXINC();
		int nEWIncr = _ttoi(strTemp);
		if ((nEWIncr < 0) || (nEWIncr > ctnMaxImgIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			dTotalEWStart += nEWIncr;
		}

		strTemp = StarParse.GetYCYC();
		int nNSCycle = _ttoi(strTemp);
		if ((nNSCycle < 0) || (nNSCycle > ctnMaxNSCycles)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			dTotalNSStart += nNSCycle * ctnMaxImgIncrements;
		}

		strTemp = StarParse.GetYINC();
		int nNSIncr = _ttoi(strTemp);
		if ((nNSIncr < 0) || (nNSIncr > ctnMaxImgIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			dTotalNSStart += nNSIncr;
		}

		if (TRUE == eReturn_Status){
			// Imager
			// double dTotIncrLimitNS = 0.0;
			double dStarDegsNSNadirImgr = _tcstod(GetAppRegSC()->m_strStarDegsNSNadirImgr[GetAppRegSite()->m_nDefaultSC], NULL);
			// Be careful with rounding. 

			/*
				const double ctdDegrees2Cycles          = 2.8125;
				const double ctdIncr2CyclesImgr         = 6136.0;
				// N/S imager: 2.8125 degrees/cycle / 6136 incr/cycle
				// const double ctdDegs2IncrNSImgr         = 4.584e-4;
				const double ctdDegs2IncrNSImgr		    = 4.5836049543676662320730117340287e-4;
			*/

	//		dTotIncrLimitNS = (dStarDegsNSNadirImgr / ctdDegs2IncrNSImgr);
			double dTotIncrLimitNS = ((dStarDegsNSNadirImgr  * ctdIncr2CyclesImgr) / ctdDegrees2Cycles);

			double dInstrCenterPtImgrNS = (double)GetAppRegSC()->m_nInstrCenterPtImgrNS[GetAppRegSite()->m_nDefaultSC];

			double dLowLimitNS = dInstrCenterPtImgrNS - dTotIncrLimitNS;
			double dHiLimitNS  = dInstrCenterPtImgrNS + dTotIncrLimitNS;

			//
			if ((dTotalNSStart - dLowLimitNS) < 0){
			  CString strMsg;
			  strMsg.Format(_T("%.2f"), dTotalNSStart);
			  m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 86, strMsg);
			  eReturn_Status = FALSE;
			}

			if ((dHiLimitNS - dTotalNSStart) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalNSStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 85, strMsg);
				eReturn_Status = FALSE;
			}

			// double dTotIncrLimitEW = 0.0;
			double dStarDegsEWNadirImgr = _tcstod(GetAppRegSC()->m_strStarDegsEWNadirImgr[GetAppRegSite()->m_nDefaultSC], NULL);

			// dTotIncrLimitEW = (dStarDegsEWNadirImgr / ctdDegs2IncrEWImgr);
			double dTotIncrLimitEW = ((dStarDegsEWNadirImgr  * ctdIncr2CyclesImgr) / (2 * ctdDegrees2Cycles));
			double dInstrCenterPtImgrEW = (double)GetAppRegSC()->m_nInstrCenterPtImgrEW[GetAppRegSite()->m_nDefaultSC];

			double dLowLimitEW = dInstrCenterPtImgrEW - dTotIncrLimitEW;
			double dHiLimitEW  = dInstrCenterPtImgrEW + dTotIncrLimitEW;

			//
			if ((dTotalEWStart - dLowLimitEW) < 0){
			  CString strMsg;
			  strMsg.Format(_T("%.2f"), dTotalEWStart);
			  m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 84, strMsg);
			  eReturn_Status = FALSE;
			}

			if ((dHiLimitEW - dTotalEWStart) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalEWStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 83, strMsg);
				eReturn_Status = FALSE;
			}
		}

	} else if (0 == strInstrValue.CompareNoCase(ctstrSDR)){
	  	double dTotalEWStart = 0.0;
		double dTotalNSStart = 0.0;
		CString strTemp;
		strTemp = StarParse.GetXCYC();
		int nEWCycle = _ttoi(strTemp);

		if ((nEWCycle < 0 ) || (nEWCycle > ctnMaxEWCycles)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			dTotalEWStart += nEWCycle * ctnMaxSndIncrements;
		}

		strTemp = StarParse.GetXINC();
		int nEWIncr = _ttoi(strTemp);
		if ((nEWIncr < 0 ) || (nEWIncr > ctnMaxSndIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		}  else {
			dTotalEWStart += nEWIncr;
		}

		strTemp = StarParse.GetYCYC();
		int nNSCycle = _ttoi(strTemp);
		if ((nNSCycle < 0) || (nNSCycle > ctnMaxCmdScanNSCyc)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		} else {
			dTotalNSStart += nNSCycle * ctnMaxSndIncrements;
		}

		strTemp = StarParse.GetYINC();
		int nNSIncr = _ttoi(strTemp);
		if ((nNSCycle < 0) || (nNSIncr > ctnMaxSndIncrements)){
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 39, strTemp);
			eReturn_Status = FALSE;
		}  else {
			dTotalNSStart += nNSIncr;
		}

		if (TRUE == eReturn_Status){
			// Sounder
			// double dTotIncrLimitNS = 0.0;
			double dStarDegsNSNadirSdr = _tcstod(GetAppRegSC()->m_strStarDegsNSNadirSdr[GetAppRegSite()->m_nDefaultSC], NULL);
			// dTotIncrLimitNS = (dStarDegsNSNadirSdr / ctdDegs2IncrNSSdr);

			double dTotIncrLimitNS = ((dStarDegsNSNadirSdr  * ctdIncr2CyclesSdr) / ctdDegrees2Cycles);
			double dInstrCenterPtSdrNS = (double)GetAppRegSC()->m_nInstrCenterPtSdrNS[GetAppRegSite()->m_nDefaultSC];

			double dLowLimitNS = dInstrCenterPtSdrNS - dTotIncrLimitNS;
			double dHiLimitNS  = dInstrCenterPtSdrNS + dTotIncrLimitNS;

			//
			if ((dTotalNSStart - dLowLimitNS) < 0){
			  CString strMsg;
			  strMsg.Format(_T("%.2f"), dTotalNSStart);
			  m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 90, strMsg);
			  eReturn_Status = FALSE;
			}

			if ((dHiLimitNS - dTotalNSStart) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalNSStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 89, strMsg);
				eReturn_Status = FALSE;
			}

			// double dTotIncrLimitEW = 0.0;
			double dStarDegsEWNadirSdr = _tcstod(GetAppRegSC()->m_strStarDegsEWNadirSdr[GetAppRegSite()->m_nDefaultSC], NULL);

			// dTotIncrLimitEW = (dStarDegsEWNadirSdr / ctdDegs2IncrEWSdr);

			double dInstrCenterPtSdrEW = GetAppRegSC()->m_nInstrCenterPtSdrEW[GetAppRegSite()->m_nDefaultSC];
			double dTotIncrLimitEW = ((dStarDegsEWNadirSdr * ctdIncr2CyclesSdr ) / (2 * ctdDegrees2Cycles));

			double dLowLimitEW = dInstrCenterPtSdrEW - dTotIncrLimitEW;
			double dHiLimitEW  = dInstrCenterPtSdrEW + dTotIncrLimitEW;

			//
			if ((dTotalEWStart - dLowLimitEW) < 0){
			  CString strMsg;
			  strMsg.Format(_T("%.2f"), dTotalEWStart);
			  m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 88, strMsg);
			  eReturn_Status = FALSE;
			}

			if ((dHiLimitEW - dTotalEWStart) < 0){
				CString strMsg;
				strMsg.Format(_T("%.2f"), dTotalEWStart);
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 87, strMsg);
				eReturn_Status = FALSE;
			}
		}
	}

	return eReturn_Status;
}

/*
*******************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 03-22-02			version 1.0
//////////////////////////////////////////////////////////////////////////
//	Function : CSchedVal::Validate_Side_Param
//
//	Description :
//
//
//	Return :		BOOL
//
//	Parameters :	CString strLine	-	STOL directive string
//
//	Note :	Not implemented
//////////////////////////////////////////////////////////////////////////
********************************************
BOOL CSchedVal::Validate_Side_Param(CString strLine){
	BOOL eReturn_Status = TRUE;
	return eReturn_Status;
}
*/
/********************************
///////////////////////////////////////////////////////////
// Sensor Intrusion File Loading
///////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-07-01			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	BOOL LoadSensorIntrusionRptFile();
//
//	Description :  This routine loads the sensor intrusion report file.
//                 .
//
//	Return :		BOOL	-
//						TRUE	-	report file was successfully loaded.
//						FALSE	-	error loading the intrusion report.
//
//	Parameters :	none 	-	void
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
*************/
BOOL CSchedVal::LoadSensorIntrusionRptFile(){

	BOOL			eReturnStatus = TRUE;
	bool			bStatus = true;
	CTextFile		dataFile;
	CString         strIntrRptFileName;
	COXUNC          UNCIntrRptPathName;

	m_bSensorIntrFileLoaded = true;
	UNCIntrRptPathName = m_UNCInputFileName;

	// Get the file extension.
	CString strExt(_T(".rpt"));
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nRpt])->GetDocString(strExt, CDocTemplate::filterExt);

	// Get the name of the sensor intrusion file.
	strIntrRptFileName = GetAppRegSC()->m_strIntrusionRpt[GetAppRegSite()->m_nDefaultSC];
	strIntrRptFileName += strExt;
	UNCIntrRptPathName.File() = strIntrRptFileName;

	// Replace proc with oats to create the correct file path for
	// searching for the sensor intrusion report file.
	strIntrRptFileName = UNCIntrRptPathName.Full();
    strIntrRptFileName.Replace(GetAppRegFolder()->m_strDir[nFolderSTOL],GetAppRegFolder()->m_strDir[nFolderOATS]);
	CFileException cFE;

	if (TRUE != dataFile.Load(strIntrRptFileName, CTextFile::RO, &cFE)){
		bStatus = false;
		// CString strError = ExceptionErrorText(&cFE);
		CString strMsg;
		strMsg.Format(_T("Unable to load sensor intrusion file: %s"), strIntrRptFileName);
		m_NewGblValData.WriteValidationMsg(0, strMsg, 0);
	}

	// Read the data from the file into the document
	if (bStatus)
	{
		int nCurObj = 15;
		CString strLine = dataFile.GetTextAt(nCurObj);
		nCurObj++;
		strLine.TrimLeft ();
		strLine.TrimRight ();
		// First find the line containing "------------".
		while ((-1 == strLine.Find(_T("------------"), 0))
				&& (nCurObj < dataFile.GetLineCount())){
			strLine = dataFile.GetTextAt(nCurObj);
			nCurObj++;
		}

		while ((bStatus) && (nCurObj < dataFile.GetLineCount()))
		{
			CParseIntrRpt	cParseIntrRpt;
			strLine = dataFile.GetTextAt(nCurObj);
			strLine.TrimLeft ();
			strLine.TrimRight ();

			if (!strLine.IsEmpty()) {
				if (!cParseIntrRpt.ParseData(strLine))
				{
					CString strError;
					strError.Format (IDS_BAD_DATA, nCurObj+1);
					GetAppOutputBar()->OutputToBuild(strError, COutputBar::WARNING);
					bStatus = false;
				}
				else
				{
					strctIntrusionRptList *pIntrRpt = new strctIntrusionRptList;
					pIntrRpt->strInstrument    = cParseIntrRpt.GetSensor();
					pIntrRpt->strBody          = cParseIntrRpt.GetBody();
					pIntrRpt->strEdge          = cParseIntrRpt.GetEdge();
					pIntrRpt->strStartTime     = cParseIntrRpt.GetStart();
					pIntrRpt->strStopTime      = cParseIntrRpt.GetStop();
					m_objSensorIntrusions.Add((CObject *)pIntrRpt);
				}
			}
			nCurObj++;
		}
	}

	// Close the file, all of the data is now local
	if (bStatus)
	{
		CFileException cFE;
		if (TRUE != dataFile.Close(&cFE)){
			CString strErrorMsg = ExceptionErrorText(&cFE);
			CString strMsg;
			strMsg.Format(_T("%s. %s"), strErrorMsg, strIntrRptFileName);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	}
	m_objSensorIntrusions.FreeExtra();
	return eReturnStatus;
}

/*******************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-07-01			version 2.0
//////////////////////////////////////////////////////////////////////
//	Function : 	CSchedVal::CheckForIntrusion()
//
//	Description :   This routine checks if a intrusion interefers with
//					with the selected spacelook side.
//
//	Return :		BOOL	-
//						TRUE	- no intrusion intereference.
//						FALSE	- intrusion interferes with spacelook
//
//	Parameters :	none	-	void
//
//	Note :
//		This routine is dependent on the sensro intrusion report file.
//////////////////////////////////////////////////////////////////////////
*************************************/
BOOL CSchedVal::CheckForIntrusion(){

	BOOL eReturnStatus = TRUE;
	m_strSideValue.Empty();

	if (!m_bSensorIntrFileLoaded){
		if (TRUE != LoadSensorIntrusionRptFile()){
			eReturnStatus = FALSE;
		}
	}

	if (!m_bPrevTimeFound && !m_bCurrTimeFound){
		//  Error no timetag is set
		eReturnStatus = FALSE;
	}

	if (TRUE == eReturnStatus){
		int  nNumElements = m_objSensorIntrusions.GetSize();
		bool bFound = false;
		int  nObjIdx = 0;
		while (!bFound && nObjIdx < nNumElements){
			CString strTemp = ((strctIntrusionRptList *)m_objSensorIntrusions[nObjIdx])->strInstrument;
			if (0 == m_strInstrValue.CompareNoCase(strTemp)){
				CString strStartTime = ((strctIntrusionRptList *)m_objSensorIntrusions[nObjIdx])->strStartTime;
				CString strStopTime  = ((strctIntrusionRptList *)m_objSensorIntrusions.GetAt(nObjIdx))->strStopTime;
				CString strTime;
				if (m_bCurrTimeFound){
					strTime = m_CSTCurrTime.GetTimeString(TRUE);
				} else if (m_bPrevTimeFound){
					strTime = m_CSTPrevTime.GetTimeString(TRUE);
				}
				// Check if in range of intrusion.
				if ((strTime > strStartTime) && (strTime < strStopTime)){
					// Found an intrusion, if it is the sun then done.
					// Else need to keep checking in case the sun intrudes too.
					// Save info first.
					bFound = true;
					// Get the intrusion side.
					m_strSideValue = ((strctIntrusionRptList *)m_objSensorIntrusions[nObjIdx])->strEdge;
					m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 54, strTime);

					// Check if intruding body is Sun
					CString strBody = ((strctIntrusionRptList *)m_objSensorIntrusions[nObjIdx])->strBody;
					if (0 != strBody.CompareNoCase(ctstrSUN)){
						// Keep checking in case there is a sun intrusion.
						bool bDone2 = false;
						nObjIdx++;
						while (!bDone2 && nObjIdx < nNumElements){
							strTemp = ((strctIntrusionRptList *)m_objSensorIntrusions.GetAt(nObjIdx))->strInstrument;
							if (0 == m_strInstrValue.CompareNoCase(strTemp)){
								strStartTime = ((strctIntrusionRptList *)m_objSensorIntrusions.GetAt(nObjIdx))->strStartTime;
								strStopTime  = ((strctIntrusionRptList *)m_objSensorIntrusions.GetAt(nObjIdx))->strStopTime;
								if ((strTime > strStartTime) && (strTime < strStopTime)){
									bDone2 = true;
									bFound = true;
									// Check if intruding body is Sun
									CString strBody = ((strctIntrusionRptList *)m_objSensorIntrusions.GetAt(nObjIdx))->strBody;
									if (0 == strBody.CompareNoCase(ctstrSUN)){
										// Get the intrusion side.
										m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 54, strTime);
										m_strSideValue = ((strctIntrusionRptList *)m_objSensorIntrusions[nObjIdx])->strEdge;
									} else {
										// output an error intruding body not recognized.
										m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 55, strBody);
									}
								}
							}
							nObjIdx++;
						}
					}
				}
			}
			nObjIdx++;
		}
	}
	return eReturnStatus;
}

/*
*******************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 04-19-02			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	CSchedVal::Validate_PRC_START
//
//	Description :
//
//
//	Return :		BOOL: TRUE = SUCCESSFUL; FALSE = FAIL.
//
//	Parameters :	CString strLine
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
BOOL CSchedVal::Validate_PRC_START(CString strLine){

	BOOL eReturnStatus = TRUE;

 	int nPos= 0;
	strLine.MakeUpper();

	// Remove trailing comment.
	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	// Remove directive
	CString strPrc_Start(ctstrPRC_STRT);
	nPos = WhereIsToken(strLine, strPrc_Start);
	if (nPos >= 0){
		strLine.Delete(nPos, strPrc_Start.GetLength());

		strLine.TrimLeft();
		strLine.TrimRight();

		// Check if variable is set to "= gmt()"
		if (-1 != strLine.Find(_T('='), 0)){
			if (-1 == WhereIsToken(strLine, (_T("GMT()")))){
				// Output .
				WriteValidationMsg(GetCurrentFileLineNum (), 92, strLine);
				eReturnStatus = FALSE;
			} else {
				m_bPrc_StartTimeFound = true;

				CString strCurrTime = m_CSTCurrTime.GetTimeString(FALSE);
				CString strPrevTime = m_CSTPrevTime.GetTimeString(FALSE);
				m_CSTPrc_StartTime = m_CSTCurrTime;
				if (m_bPrevTimeFound){
					m_CSTPrc_StartTime = m_CSTPrevTime;
				}
			}
		}
	}

	return eReturnStatus;
}
*****************
*/

/*************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 04-23-02			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	CSchedVal::IsMnemonicRestricted4Procs
//
//	Description :   This routine checks the mnemonic passed in againest
//					specific mnemonics to determine if it is restricted or
//					not.  If the mnemonic is restricted, a validation
//					validation message is posted.
//
//
//	Return :		BOOL	-
//						 TRUE	-	mnemonic is not restricted.
//						 FALSE	-	mnemonic is restricted.
//
//	Parameters :	CString strMnemonic
//
//	Note :
//		Mnemonics are hard coded in the routine.
//////////////////////////////////////////////////////////////////////////
****************************************/
BOOL CSchedVal::IsMnemonicRestricted4Procs(CString strLine){
  BOOL	eReturnStatus = TRUE;
  CString strMnemonic;

    int nPos;
	if ( -1 != (nPos = (strLine.Find(ctchCOMMENT, 0)))){
		// Remove trailing comment.
		strLine.Delete(nPos, strLine.GetLength() - nPos);
	}

	/*
		First replace all equal signs with a space.
		Parse line and determine # of tokens.
	*/

	strLine.Replace(_T('='), _T(' '));
	CStringArray strCMDData;
	CTextFile::TokenizeString(strLine, strCMDData);
	// Check size of token array.
	int nCMDDataSize = strCMDData.GetSize();
	if (nCMDDataSize == 2) {
		/*
		 only two tokens present
		 If verify second token does not contain '$' or an integer,
			Set 2nd token to mnemonic variable.
		 else
		   output an error and return.
		*/

		int nSCADDR = _ttoi(strCMDData[1]);
		int nPos   = strCMDData[1].Find(_T('$'));

		if ((nPos == -1) && (nSCADDR <= 0)){
			strMnemonic = strCMDData[1];
		}

	} else if (nCMDDataSize > 2) {

		/*
        Else more than two tokens.
		    If second token contains '$' or an integer.
			     Set 3rd token to mnemonic variable.
			Else
				 Set 2nd token to mnemonic variable.
		*/

		int nSCADDR = _ttoi(strCMDData[1]);
		int nPos   = strCMDData[1].Find(_T('$'));

		if ((nPos == -1) && (nSCADDR <= 0)){
			strMnemonic = strCMDData[1];
		} else {
			strMnemonic = strCMDData[2];
		}
	}

	CString strStartAddrValue, strStopAddrValue, strStartASValue, strStopASValue;

//	if (-1 != strMnemonic.Find(_T("_AE"), 0)){
		  // Output an error message  Command is restricted.
//		  m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 93, strMnemonic);
//		  eReturnStatus = FALSE;
//	} else if (-1 != strMnemonic.Find(_T("_23"), 0)){
	if (-1 != strMnemonic.Find(_T("_23"), 0)){

		/*
			ACE_RCS_FIRING_CONTROL_23 RCS_SEQUENCE=ENABLE

			ACE_AUTO_SKM_ENBL_30 CHG_EN_AUTO_SKM = CHANGE EN_AUTO_SKM = ENABLE

			ACE_GAIN_SET_RECONFIGURE_43	SC_CONTROL_TYPE=OVERLAP
		*/

		  bool bDone = false;
		  for (int n = 2; n < nCMDDataSize && !bDone; n++){
			  CString strTemp01 = strCMDData[n];
			  int nVal = strCMDData[n].CompareNoCase(_T("RCS_SEQUENCE"));

			  if (0 == nVal){
				bDone = true; // Found submnemonic looking for.
				// Next token is the value.
				n++;
				CString strTemp = strCMDData[n];
				if ((-1 == strCMDData[n].Find(_T('$')) &&
					(0 == strCMDData[n].CompareNoCase(_T("ENABLE"))))){
				  // Output an error message  Command is restricted.
				  m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 94, strMnemonic);
				  eReturnStatus = FALSE;
				}
			  }
		  }
	} else if (-1 != strMnemonic.Find(_T("_43"), 0)){
		// Output an error message  Command is restricted.
		/* ACE_GAIN_SET_RECONFIGURE_43 SC_CONTROL_TYPE=OVERLAP 	*/

		  bool bDone = false;
		  for (int n = 2; n < nCMDDataSize && !bDone; n++){
			  CString strTemp01 = strCMDData[n];
			  int nVal = strCMDData[n].CompareNoCase(_T("SC_CONTROL_TYPE"));
			  if (0 == nVal){
					bDone = true; // Found submnmonic looking for.
					// Next token is the value.
					n++;
					CString strTemp = strCMDData[n];
					if ((-1 == strCMDData[n].Find(_T('$')) &&
						(0 == strCMDData[n].CompareNoCase(_T("OVERLAP"))))){

					  // Output an error message  Command is restricted.
					  m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 94, strMnemonic);
					  eReturnStatus = FALSE;
					}
			  }
		  }
	} else if (-1 != strMnemonic.Find(_T("_30"), 0)){
	/* ACE_AUTO_SKM_ENBL_30 CHG_EN_AUTO_SKM = CHANGE EN_AUTO_SKM = ENABLE */

		  bool bEN_AUTO_SKM = false;
		  bool bCHG_EN_AUTO_SKM = false;
		  for (int n = 2; n < nCMDDataSize; n++){
			  CString strTemp = strCMDData[n];
			  if (0 == strCMDData[n].CompareNoCase(_T("EN_AUTO_SKM"))){
				// Next token is the value.
				n++;
				CString strTemp = strCMDData[n];
				if ((-1 == strCMDData[n].Find(_T('$')) &&
					(0 == strCMDData[n].CompareNoCase(_T("ENABLE"))))){
					  bEN_AUTO_SKM = true;
				}
			  } else if (0 == strCMDData[n].CompareNoCase(_T("CHG_EN_AUTO_SKM"))){
				// Next token is the value.
				n++;
				CString strTemp = strCMDData[n];
				if ((-1 == strCMDData[n].Find(_T('$')) &&
					(0 == strCMDData[n].CompareNoCase(_T("CHANGE"))))){
					bCHG_EN_AUTO_SKM = true;
				}
			  }
		  }

		  if ( bEN_AUTO_SKM && bCHG_EN_AUTO_SKM){
			  // Output an error message  Command is restricted.
			  m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 94, strMnemonic);
			  eReturnStatus = FALSE;
		  }

	} else if (-1 != strMnemonic.Find(_T("_98"), 0)){

  		/* 	CMD ACE_NORM_DWL_MEM_DUMP_PARAM_98 \
				MEMDUMP_START_ADDR 0x7530 \
				MEMDUMP_STOP_ADDR 0x7534
				MEMDUMP_START_AS 0xD \
				MEMDUMP_STOP_AS 0xD 		*/

		for (int n = 2; n < nCMDDataSize; n++){
			  strCMDData[n].MakeUpper();
			  CString strValue = strCMDData[n];
			  //int nPos = 0;
			  if (-1 != strCMDData[n].Find(_T("_START_ADDR"))){
					// Next token is the value.
					n++;
					strStartAddrValue = strCMDData[n];
					if (-1 != strStartAddrValue.Find(_T('$'))){
						// Equal sign found, extract the value.
						strStartAddrValue.Empty();
					}
			  } else if (-1 != strCMDData[n].Find(_T("_START_AS"))){
					// Next token is the value.
					n++;
					strStartASValue = strCMDData[n];
					if (-1 != strStartASValue.Find(_T('$'))){
						// Equal sign found, extract the value.
						strStartASValue.Empty();
					}
			  } else if ( -1 != strCMDData[n].Find(_T("_STOP_ADDR"))){
					// Next token is the value.
					n++;
					strStopAddrValue = strCMDData[n];
					if (-1 != strStopAddrValue.Find(_T('$'))){
						// Dollar sign found
						strStopAddrValue.Empty();
					}
			  } else if ( -1 != strCMDData[n].Find(_T("_STOP_AS"))){
				  // Next token is the value.
					n++;
					strStopASValue = strCMDData[n];
					if (-1 != strStopASValue.Find(_T('$'))){
						// Dollar sign found, extract the value.
						strStopASValue.Empty();
					}
			  }
		  }

		  if (!strStartASValue.IsEmpty()){
			  if (strStartASValue != strStopASValue){
			    // Output an error message  Command is restricted.
			    m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 97, strMnemonic);
				eReturnStatus = FALSE;
			  } else if (strStartAddrValue >= strStopAddrValue){
			    // Output an error message  Command is restricted.
				m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 95, strMnemonic);
				eReturnStatus = FALSE;
			  }
		}
  }

  return eReturnStatus;
}

/*********************************************
///////////////////////////////////////////////////////////
// Number of Command Frames File Loading
///////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-07-01			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	BOOL LoadNumCmdFrameFile();
//
//	Description :  This routine loads the command frame file.
//                 .
//	Return :		BOOL	-
//					 TRUE -	File successfully loaded
//					 FALSE - problem loading file.
//
//	Parameters :	none	-	void
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
********************************************/
BOOL CSchedVal::LoadNumCmdFrameFile(){

	BOOL			eReturnStatus = TRUE;
	bool			bStatus = true;
	CTextFile		dataFile;

	m_bNumCmdFrameFileLoaded = true;

// 	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
//	CString	strSite			= GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef];
//	CString	strShare		= GetAppRegGTACS()->m_strDBShare[nGTACSSiteDef][nGTACSDef];
//	CString strDBPath;
//	strDBPath.Format(_T("\\\\%s\\%s"), strSite, strShare);
	CString	strNumCmdFrameFileName 	= GetAppRegMisc()->m_strDBDrive +
						   + GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC] +
						  _T("\\") + GetAppRegSC()->m_strNumCmdFrameFile[GetAppRegSite()->m_nDefaultSC] +
						  _T(".rpt");

	CFileException cFE;
	if (TRUE != dataFile.Load(strNumCmdFrameFileName, CTextFile::RO, &cFE)){
		bStatus = false;
		// CString strError = ExceptionErrorText(&cFE);
		CString strMsg;
		strMsg.Format(_T("Unable to load command frame file: %s"), strNumCmdFrameFileName);
		m_NewGblValData.WriteValidationMsg(0, strMsg, 0);
		eReturnStatus = FALSE;
		m_bNumCmdFrameFileLoaded = false;
	} else {
		// CString strMsg(_T("Successfully loaded the Command Frame File:"));
		CString strTitle;
		strTitle.Format(_T("Successfully loaded the Command Frame File: %s"), strNumCmdFrameFileName);
		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::INFO);
	}

	// Read the data from the file into the document
	if (bStatus)
	{
		int nCurObj = 3;
		CString strLine = dataFile.GetTextAt(nCurObj);
		nCurObj++;
		strLine.TrimLeft ();
		strLine.TrimRight ();
		// First find the line containing "======".
		while ((-1 == strLine.Find(_T("======"), 0))
				&& (nCurObj < dataFile.GetLineCount())){
			strLine = dataFile.GetTextAt(nCurObj);
			nCurObj++;
		}

		while ((bStatus) && (nCurObj < dataFile.GetLineCount()))
		{
			CParseCMDFrmNum	cCParseCMDFrmNum;
			strLine = dataFile.GetTextAt(nCurObj);
			strLine.TrimLeft ();
			strLine.TrimRight ();

			if (!strLine.IsEmpty()) {
				if (!cCParseCMDFrmNum.ParseData(strLine))
				{
					CString strError;
					strError.Format (IDS_BAD_DATA, nCurObj+1);
					GetAppOutputBar()->OutputToBuild(strError, COutputBar::WARNING);
					bStatus = false;
				}
				else
				{
					CString pNumFrames;
					CString strTemp = cCParseCMDFrmNum.GetMnemonic();
					pNumFrames = cCParseCMDFrmNum.GetNumFrms();
					m_mapNumCmdFrame.SetAt(strTemp, pNumFrames);
				}
			}
			nCurObj++;
		}
	}

	// Close the file, all of the data is now local
	if (bStatus)
	{
		CFileException cFE;
		if (TRUE != dataFile.Close(&cFE)){
			CString strErrorMsg = ExceptionErrorText(&cFE);
			CString strMsg;
			strMsg.Format(_T("%s. %s"), strErrorMsg, strNumCmdFrameFileName);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	}

	return eReturnStatus;
}

/*****************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-07-01			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	LoadNumCmdFrameFile();
//
//	Description :  This routine updates the running total of the number of
//						command frames for the  current menmonic.
//                 .
//
//	Return :		BOOL
//						TRUE - mnemonic found in database
//						FALSE - mnemonic was not found in database.
//
//	Parameters :	CString strMnemonic	-	mnemonic to be checked.
//
//	Note :
//		This routine is dependent on the command frame file being loaded.
//////////////////////////////////////////////////////////////////////////
***************************************/
BOOL CSchedVal::UpdateNumberCmdFrames(CString strMnemonic){

	BOOL			eReturnStatus = TRUE;
	int             nNumCmdFrames = 0;
	CString         strNumCmdFrames;
	int             nIndex = 0;

	strMnemonic.MakeUpper();
	if (0 == (nIndex = m_mapNumCmdFrame.Lookup(strMnemonic, strNumCmdFrames))){
		// Error mnemonic not found in file
		m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 99, strMnemonic);
		eReturnStatus = FALSE;
	} else {
		nNumCmdFrames = _ttoi(strNumCmdFrames);
		m_nTotalNumCmdFrames += nNumCmdFrames;
	}

	return eReturnStatus;
}

/*********************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-07-01			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	LoadSTOLCmdRestrictFile();
//
//	Description :  This routine loads the STOL command restriction file.
//                 .
//
//	Return :		BOOL	-
//						 TRUE - file successfully loaded
//						 FALSE - unable to load file.
//
//	Parameters :	none
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
****************************************/
BOOL CSchedVal::LoadSTOLCmdRestrictFile(){

	BOOL			eReturnStatus = TRUE;
	bool			bStatus = true;
	CTextFile		dataFile;
	CString         strSTOLCMDRestrictedFileName;
	m_bSTOLCMDRestrictedFileLoaded = true;

	// Get the file extension.
	CString strExt(_T(".scmd"));
	(((CSchedMgrECPApp *)AfxGetApp())->m_pDocTemplate[nSTOLCmd])->GetDocString(strExt, CDocTemplate::filterExt);

//  Get the path for the STOLCMDRestricted file.
//	int	nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int	nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
//	strSTOLCMDRestrictedFileName.Format(_T("\\\\%s\\%s\\%s\\%s\\"),
//		GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server.
//		GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share.

	strSTOLCMDRestrictedFileName.Format(_T("%s\\%s\\%s\\%s\\%s\\"),
		ctcProcDrive, ctcMountPoint, ctcProcsDir,
		GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC], 		// SC Specific Directory.
        GetAppRegFolder()->m_strDir[nFolderData]);                  // Folder containing the file.

	// Get the name of the STOL CMD Restricted file.
	strSTOLCMDRestrictedFileName += GetAppRegSC()->m_strSTOLCmd[GetAppRegSite()->m_nDefaultSC];
	strSTOLCMDRestrictedFileName += strExt;

	// Need to validate file before STOL Command Restriction loading.
	// Call syntax checker to validate the mnemonics.
	// Open the document
	CFileException cFE;
	/*
	short int	nType	= nSTOLCmd;
	char		rType[9]= {"RESTRICT"};
	if (TRUE != ((CMainFrame *)(CSchedMgrECPApp *)AfxGetApp()->m_pMainWnd)->SyntaxCheck(nType, rType, strRTCSCMDRestrictedFileName)){
		bStatus = false;
		m_bSTOLCMDRestrictedFileLoaded = false;
		eReturnStatus = false;
		CString strTitle (_T("STOL Restricted Command file is invalid."));
		GetAppOutputBar()->OutputToBuild(strTitle, COutputBar::WARNING);
	} else
	*/
	if (TRUE != dataFile.Load(strSTOLCMDRestrictedFileName, CTextFile::RO, &cFE)){
		bStatus = false;
		m_bSTOLCMDRestrictedFileLoaded = false;
		// CString strError = ExceptionErrorText(&cFE);
		CString strMsg;
		strMsg.Format(_T("Unable to load STOL command restriction file: %s"), strSTOLCMDRestrictedFileName);
		m_NewGblValData.WriteValidationMsg(0, strMsg, 0);
	} else {
		dataFile.ExtractProlog();
		// Read the data from the file into the document
		int nCurObj = 0;
		while (nCurObj < dataFile.GetLineCount())
		{
		 	int nPos = 0;
			CString strMnemonic = dataFile.GetTextAt(nCurObj);
			strMnemonic.TrimLeft();
			strMnemonic.TrimRight();
			strMnemonic.MakeUpper();

			// First check if allow directive is present.
			if ( -1 != (nPos = (strMnemonic.Find(ctstrCMDALLOW, 0)))){
				int nDirSize = _tcsclen(ctstrCMDALLOW) + 1;
				strMnemonic.Delete(nPos, nDirSize - nPos);
				strMnemonic.TrimLeft();
				strMnemonic.TrimRight();
			//	Assume second token is valid mnemonic.
				if (!strMnemonic.IsEmpty()){
					m_strSTOLCmdAllow.Add(strMnemonic);
				}
			} else if (!strMnemonic.IsEmpty() && (ctchCOMMENT != strMnemonic[0])){
				m_strSTOLRestrict.Add(strMnemonic);
			}
			nCurObj++;
		}

		// Close the file, all of the data is now local
		CFileException cFE;
		if (TRUE != dataFile.Close(&cFE)){
			CString strErrorMsg = ExceptionErrorText(&cFE);
			CString strMsg;
			strMsg.Format(_T("%s. file: %s"), strErrorMsg, strSTOLCMDRestrictedFileName);
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), strMsg, 2);
		}
	}

	m_strSTOLCmdAllow.FreeExtra();
	m_strSTOLRestrict.FreeExtra();
	return eReturnStatus;
};

/*************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 08-26-2002			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	IsMnemonicRestricted4STOL
//
//	Description :  This routine checks the mnemonic passed in againest
//				   the mnemonics contained in the STOL command restriction
//				   file.
//
//	Return :	BOOL	-
//			 		Always returns true.
//
//	Parameters :	CString
//			strMnemonic -	Command mnemonic to be checked
//
//	Note :
//			This function uses the STOL command restriction file.
//
//////////////////////////////////////////////////////////////////////////
***************************************************/
BOOL CSchedVal::IsMnemonicRestricted4STOL(CString strMnemonic){

  BOOL   eReturnStatus = TRUE;
  bool bFound = false;
  int nObjIdx = 0;

  int nNumElementsAllow = m_strSTOLCmdAllow.GetSize();
  while (!bFound && nObjIdx < nNumElementsAllow){

	  if (0 == strMnemonic.CompareNoCase(m_strSTOLCmdAllow[nObjIdx])){
		  bFound = true;
	  } else {
		  nObjIdx++;
	  }
  }

  nObjIdx = 0;
  int nNumElementsRestrict = m_strSTOLRestrict.GetSize();
  while (!bFound && nObjIdx < nNumElementsRestrict){
	  CString strRestrictLeft, strRestrictRight;
	  CString strRestrictMnemonic = m_strSTOLRestrict[nObjIdx];
	  strMnemonic.MakeUpper();
	  strMnemonic.TrimLeft();
	  strMnemonic.TrimRight();
	  strRestrictMnemonic.MakeUpper();
	  strRestrictMnemonic.TrimLeft();
	  strRestrictMnemonic.TrimRight();

   	  int nPos = 0;
	  int nSize = strRestrictMnemonic.GetLength();
 	  if ( -1 != (nPos = (strRestrictMnemonic.Find(_T('*'), 0)))){
		 strRestrictLeft = strRestrictMnemonic.Left(nPos);
		 strRestrictRight = strRestrictMnemonic.Right(nSize - nPos - 1);
	  }

	  if ((!strRestrictRight.IsEmpty()) && (-1 != strMnemonic.Find(strRestrictRight))){
	     if ((!strRestrictLeft.IsEmpty()) && (-1 != strMnemonic.Find(strRestrictLeft))){
	  	    // Output an error message  Command is restricted.
	        m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 71, strMnemonic);
			bFound = true;
		 } else if (strRestrictLeft.IsEmpty()) {
		    // Output an error message  Command is restricted.
	        m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 71, strMnemonic);
		    bFound = true;
		 }
	  } else if (!strRestrictLeft.IsEmpty()) {
		 if ((strRestrictRight.IsEmpty()) && ( -1 != strMnemonic.Find(strRestrictLeft))){
			// Output an error message  Command is restricted.
			m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 71, strMnemonic);
			bFound = true;
		 }
	  } else if (0 == strMnemonic.CompareNoCase(strRestrictMnemonic)){
	     // Output an error message  Command is restricted.
         m_NewGblValData.WriteValidationMsg(GetCurrentFileLineNum (), 71, strMnemonic);
         bFound = true;
	  }
	  nObjIdx++;
   }
   return eReturnStatus;
}


/*************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-15-2003			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	StoreFilename&Type
//
//	Description :  This routine processes the list of filenames
//				    found when parsing the file's prolog.
//
//	Return :	BOOL	-
//
//
//	Parameters :	CString *
//					strDataUpdate -	Update directive string array
//
//	Note :
//
//
//////////////////////////////////////////////////////////////////////////
***************************************************/
BOOL CSchedVal::StoreFilenameAndType(CStringArray * strDataUpdate){

	// First get the number of filenames to process.
	int nNumOfElements = strDataUpdate->GetSize();
	CString			strTemp;
	CStringArray	strTokenData;
	BOOL eReturn_Status = TRUE;

	for (int i = 0; i < nNumOfElements; i++){
		strTemp.Empty();
		strTokenData.RemoveAll();
		strTemp = strDataUpdate->GetAt(i);
		if (FALSE == TokenizeString(strDataUpdate->GetAt(i), strTokenData)){
			CString strError = "Problems processing update filename list.";
			m_NewGblValData.WriteValidationMsg(0, strError, 0);
			eReturn_Status = FALSE;
		} else {
			CString strTokenTmp = strTokenData[0];
			if (0 == strTokenData[0].CompareNoCase(ctstrSTAR)){
				m_strStarFilename.Add(strTokenData[3]);
				CString strStarTmp = strTokenData[3];
			} else if (0 == strTokenData[0].CompareNoCase(ctstrSCAN)){
				m_strScanFilename.Add(strTokenData[3]);
				CString strScanTmp = strTokenData[3];
			}
		}
	}

	return eReturn_Status;
}


/*************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-15-2003			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : Check_Star_instrObjFile
//
//	Description :  This routine cycles through the star instrument list
//                 of object files.
//
//	Return :	BOOL	-	 eReturn_Status
//			 			TRUE	- SUCCESS
//                      FALSE	- FAIL
//
//	Parameters :	None
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
***************************************************/
BOOL CSchedVal::Check_Star_InstrObjFile(){

	BOOL eReturn_Status = TRUE;

	int nNumOfElements = m_strStarFilename.GetSize();
	for (int i = 0; (i < nNumOfElements) && eReturn_Status; i++){
		CString strTemp = m_strStarFilename[i];
		eReturn_Status = Validate_StarInstrObjFileLine((LPCTSTR)m_strStarFilename[i]);
	}
	return eReturn_Status;
}


/*************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-15-2003			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	Check_Scan_InstrObjFile()
//
//	Description :  This routine cycles through the scan instrument object
//                 list of files.
//
//	Return :	BOOL	-	eReturn_Status
//						TRUE	- success
//						FALSE	- fail
//
//	Parameters :	None
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
***************************************************/
BOOL CSchedVal::Check_Scan_InstrObjFile(){

	BOOL eReturn_Status = TRUE;
	int nNumOfElements = m_strScanFilename.GetSize();
	for (int i = 0; (i < nNumOfElements) && eReturn_Status; i++){
		CString strTemp = m_strScanFilename[i];
		eReturn_Status =  Validate_ScanInstrObjFileLine((LPCTSTR)m_strScanFilename[i]);
	}
	return eReturn_Status;
}

/*************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-15-2003			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	Validate_StarInstrObjFileLine()
//
//	Description :  This routine validates the contents of the star instrument
//                 object file.
//
//	Return :	BOOL	-	eReturn_Status
//							FALSE	-	fail
//							TRUE	-	success
//
//	Parameters :	LPCTSTR lpszPathName
//						star instrument object filename
//
//	Note :
//
//////////////////////////////////////////////////////////////////////////
***************************************************/
BOOL CSchedVal::Validate_StarInstrObjFileLine(LPCTSTR lpszPathName){

	BOOL		 eReturn_Status = TRUE;
  	CTextFile	 dataFile;
	CString		 strError;
	CParseStar	 cParseStar;
	CStringArray strDataRecord;
	CString      strInstrValue;
	bool         bFileNameHdr = false;

	// Open the document
	CFileException cFE;
	strDataRecord.RemoveAll();
	if (!dataFile.Load(lpszPathName, CTextFile::RO, &cFE))
	{
		strError.Format(_T("Unable to validate instrument object file: %s"), lpszPathName);
		m_NewGblValData.WriteValidationMsg(0, strError, 0);
		strError = CTextFile::ExceptionErrorText(&cFE);
		m_NewGblValData.WriteValidationMsg(0, strError, 0);
		eReturn_Status = FALSE;
	}

	if (eReturn_Status) {
		cParseStar.SetHdr(dataFile.GetTextAt(0), dataFile.GetTextAt(1));
		cParseStar.ParseHdr();
		int nCurObj = 2;
		while ((nCurObj < dataFile.GetLineCount()))
		{
			bFileNameHdr = false;
			cParseStar.SetData(dataFile.GetTextAt(nCurObj));
			if (!cParseStar.ParseData()) {
				if (!bFileNameHdr){
					// Only output message once per entry
					strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
					m_NewGblValData.WriteValidationMsg(0, strError, 0);
					bFileNameHdr = true;
				}
				strError.Format (IDS_BAD_DATA, nCurObj+1);
				// GetAppOutputBar()->OutputToEvents(strError, COutputBar::INFO);
				m_NewGblValData.WriteValidationMsg(0, strError, 0);
				eReturn_Status = FALSE;
			} else {
				bool bInvalid_coord = true;
				strInstrValue = cParseStar.GetDataInstrument();
				if (0 == strInstrValue.CompareNoCase(ctstrIMGR)){ // Value: imager
	  				double dTotalEWStart = 0;
					double dTotalNSStart = 0;
					CString strTemp;
					strTemp = cParseStar.GetDataCycEW();
					int nEWCycle = _ttoi(strTemp);
					if ((nEWCycle < 0) || (nEWCycle > ctnMaxEWCycles)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					}else {
						dTotalEWStart += nEWCycle * ctnMaxImgIncrements;
					}

					strTemp = cParseStar.GetDataIncEW();
					int nEWIncr = _ttoi(strTemp);
					if ((nEWIncr < 0) || (nEWIncr > ctnMaxImgIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bFileNameHdr = true;
						bInvalid_coord = false;
					} else {
						dTotalEWStart += nEWIncr;
					}

					strTemp = cParseStar.GetDataCycNS();
					int nNSCycle = _ttoi(strTemp);
					if ((nNSCycle < 0) || (nNSCycle > ctnMaxNSCycles)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStart += nNSCycle * ctnMaxImgIncrements;
					}

					strTemp = cParseStar.GetDataIncNS();
					int nNSIncr = _ttoi(strTemp);
					if ((nNSIncr < 0) || (nNSIncr > ctnMaxImgIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStart += nNSIncr;
					}

					if (bInvalid_coord){
						// Imager
						double dTotIncrLimitNS = 0.0;
						double dStarDegsNSNadirImgr = _tcstod(GetAppRegSC()->m_strStarDegsNSNadirImgr[GetAppRegSite()->m_nDefaultSC], NULL);
						
						/*
							const double ctdDegrees2Cycles          = 2.8125;
							const double ctdIncr2CyclesImgr         = 6136.0;
							// N/S imager: 2.8125 degrees/cycle / 6136 incr/cycle
							// const double ctdDegs2IncrNSImgr         = 4.584e-4;
							const double ctdDegs2IncrNSImgr		    = 4.5836049543676662320730117340287e-4;
						*/

						//		dTotIncrLimitNS = (dStarDegsNSNadirImgr / ctdDegs2IncrNSImgr);
						dTotIncrLimitNS = ((dStarDegsNSNadirImgr  * ctdIncr2CyclesImgr) / ctdDegrees2Cycles);
												
						// dTotIncrLimitNS = (dStarDegsNSNadirImgr / ctdDegs2IncrNSImgr);

						double dInstrCenterPtImgrNS = (double)GetAppRegSC()->m_nInstrCenterPtImgrNS[GetAppRegSite()->m_nDefaultSC];
						double dLowLimitNS = dInstrCenterPtImgrNS - dTotIncrLimitNS;
						double dHiLimitNS  = dInstrCenterPtImgrNS + dTotIncrLimitNS;

						//
						if ((dTotalNSStart - dLowLimitNS) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("%.2f"), dTotalNSStart);
							m_NewGblValData.WriteValidationMsg(0, 86, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitNS - dTotalNSStart) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("%.2f"), dTotalNSStart);
							m_NewGblValData.WriteValidationMsg(0, 85, strMsg);
							eReturn_Status = FALSE;
						}

						double dTotIncrLimitEW = 0.0;
						double dStarDegsEWNadirImgr = _tcstod(GetAppRegSC()->m_strStarDegsEWNadirImgr[GetAppRegSite()->m_nDefaultSC], NULL);

						dTotIncrLimitEW = ((dStarDegsEWNadirImgr  * ctdIncr2CyclesImgr) / (ctdDegrees2Cycles * 2));

						double dInstrCenterPtImgrEW = (double)GetAppRegSC()->m_nInstrCenterPtImgrEW[GetAppRegSite()->m_nDefaultSC];
						double dLowLimitEW = dInstrCenterPtImgrEW - dTotIncrLimitEW;
						double dHiLimitEW  = dInstrCenterPtImgrEW + dTotIncrLimitEW;

						//
						if ((dTotalEWStart - dLowLimitEW) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("%.2f"), dTotalEWStart);
							m_NewGblValData.WriteValidationMsg(0, 84, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitEW - dTotalEWStart) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("%.2f"), dTotalEWStart);
							m_NewGblValData.WriteValidationMsg(0, 83, strMsg);
							eReturn_Status = FALSE;
						}
					}
				} else if (0 == strInstrValue.CompareNoCase(ctstrSDR)){
		  			double dTotalEWStart = 0.0;
					double dTotalNSStart = 0.0;
					CString strTemp;
					strTemp = cParseStar.GetDataCycEW();
					int nEWCycle = _ttoi(strTemp);

					if ((nEWCycle < 0 ) || (nEWCycle > ctnMaxEWCycles)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalEWStart += nEWCycle * ctnMaxSndIncrements;
					}

					strTemp = cParseStar.GetDataIncEW();
					int nEWIncr = _ttoi(strTemp);
					if ((nEWIncr < 0 ) || (nEWIncr > ctnMaxSndIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					}  else {
						dTotalEWStart += nEWIncr;
					}

					strTemp = cParseStar.GetDataCycNS();
					int nNSCycle = _ttoi(strTemp);
					if ((nNSCycle < 0) || (nNSCycle > ctnMaxCmdScanNSCyc)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStart += nNSCycle * ctnMaxSndIncrements;
					}

					strTemp = cParseStar.GetDataIncNS();
					int nNSIncr = _ttoi(strTemp);
					if ((nNSCycle < 0) || (nNSIncr > ctnMaxSndIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					}  else {
						dTotalNSStart += nNSIncr;
					}

					if (bInvalid_coord){
						// Sounder
						// double dTotIncrLimitNS = 0.0;
						double dStarDegsNSNadirSdr = _tcstod(GetAppRegSC()->m_strStarDegsNSNadirSdr[GetAppRegSite()->m_nDefaultSC], NULL);

						// dTotIncrLimitNS = (dStarDegsNSNadirSdr / ctdDegs2IncrNSSdr);

						double dTotIncrLimitNS = ((dStarDegsNSNadirSdr * ctdIncr2CyclesSdr) / ctdDegrees2Cycles);
						double dInstrCenterPtSdrNS = (double)GetAppRegSC()->m_nInstrCenterPtSdrNS[GetAppRegSite()->m_nDefaultSC];

						double dLowLimitNS = dInstrCenterPtSdrNS - dTotIncrLimitNS;
						double dHiLimitNS  = dInstrCenterPtSdrNS + dTotIncrLimitNS;

						//
						if ((dTotalNSStart - dLowLimitNS) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("%.2f"), dTotalNSStart);
							m_NewGblValData.WriteValidationMsg(0, 90, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitNS - dTotalNSStart) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("%.2f"), dTotalNSStart);
							m_NewGblValData.WriteValidationMsg(0, 89, strMsg);
							eReturn_Status = FALSE;
						}

						// double dTotIncrLimitEW = 0.0;
						double dStarDegsEWNadirSdr = _tcstod(GetAppRegSC()->m_strStarDegsEWNadirSdr[GetAppRegSite()->m_nDefaultSC], NULL);

						// dTotIncrLimitEW = (dStarDegsEWNadirSdr / ctdDegs2IncrEWSdr);

						double dTotIncrLimitEW = ((dStarDegsEWNadirSdr * ctdIncr2CyclesSdr) / (2 * ctdDegrees2Cycles));
						double dInstrCenterPtSdrEW = GetAppRegSC()->m_nInstrCenterPtSdrEW[GetAppRegSite()->m_nDefaultSC];

						double dLowLimitEW = dInstrCenterPtSdrEW - dTotIncrLimitEW;
						double dHiLimitEW  = dInstrCenterPtSdrEW + dTotIncrLimitEW;

						//
						if ((dTotalEWStart - dLowLimitEW) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("%.2f"), dTotalEWStart);
							m_NewGblValData.WriteValidationMsg( 0, 88, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitEW - dTotalEWStart) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("%.2f"), dTotalEWStart);
							m_NewGblValData.WriteValidationMsg( 0, 87, strMsg);
							eReturn_Status = FALSE;
						}
					}
				}
			}
			nCurObj++;
		}
	}

	// Close the file, all of the data is now local
	if (eReturn_Status && (!dataFile.Close(&cFE))){
		strError.Format(_T("Error closing instrument object file: %s"), lpszPathName);
		m_NewGblValData.WriteValidationMsg(0, strError, 0);
		strError = CTextFile::ExceptionErrorText(&cFE);
		m_NewGblValData.WriteValidationMsg(0, strError, 0);
	}

	return eReturn_Status;

};

/*************************************************
//////////////////////////////////////////////////////////////////////
//	Author :Fred Shaw		Date : 09-15-2003			version 2.0
//////////////////////////////////////////////////////////////////////////
//	Function : 	Check_Scan_InstrObjFile()
//
//	Description :  This routine validates the instrument object file.
//
//
//	Return :	BOOL	-
//
//
//	Parameters :	None
//
//
//	Note :
//
//
//////////////////////////////////////////////////////////////////////////
***************************************************/
BOOL CSchedVal::Validate_ScanInstrObjFileLine(LPCTSTR lpszPathName){

	BOOL eReturn_Status = TRUE;
	CTextFile	   dataFile;
	CFileException cFE;

	if (!dataFile.Load(lpszPathName, CTextFile::RO, &cFE)){
		CString		   strError;
		strError.Format(_T("Error loading instrument object file: %s"), lpszPathName);
		m_NewGblValData.WriteValidationMsg(0, strError, 0);
		strError = CTextFile::ExceptionErrorText(&cFE);
		m_NewGblValData.WriteValidationMsg(0, strError, 0);
		eReturn_Status = FALSE;
	}

	// Make sure that there are atleast 2 header records in the file and if OK
	// read in the header records
	if (eReturn_Status) {
		CParseFrame	   cParseFrame;
		cParseFrame.SetHdr(dataFile.GetTextAt(0), dataFile.GetTextAt(1));
		cParseFrame.ParseHdr();
		int nCurObj = 2;
		bool           bFileNameHdr = false;

		while ((nCurObj < dataFile.GetLineCount())) {
			double dTotalEWStart = 0.0;
			double dTotalNSStart = 0.0;
			double dTotalEWStop  = 0.0;
			double dTotalNSStop  = 0.0;
			bFileNameHdr = false;
			cParseFrame.SetData(dataFile.GetTextAt(nCurObj));
			CString		   strError;

			if (!cParseFrame.ParseData()) {
				if (!bFileNameHdr){
					// Only output message once per entry
					strError.Format(_T("Unable to parse entry # %d in the instrument object file: %s"), nCurObj-1, lpszPathName);
					m_NewGblValData.WriteValidationMsg(0, strError, 0);
					bFileNameHdr = true;
				}
				strError.Format (IDS_BAD_DATA, nCurObj+1);
				m_NewGblValData.WriteValidationMsg(0, strError, 0);
				eReturn_Status = FALSE;
			}else {
				CString strSideValue = cParseFrame.GetDataSpacelookSide();
				CString strInstrValue = cParseFrame.GetDataInstrument();
				bool bInvalid_coord = true;
				if (0 == strInstrValue.CompareNoCase(ctstrIMGR)){ // Value: imager

					CString strTemp;
					strTemp = cParseFrame.GetDataStartCycEW();
					int nEWCycle = _ttoi(strTemp);
					if ((nEWCycle < 0) || (nEWCycle > ctnMaxEWCycles )){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("The E/W start cycle entry # %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalEWStart += nEWCycle * ctnMaxImgIncrements;
					}

					strTemp = cParseFrame.GetDataStartIncEW();
					int nEWIncr = _ttoi(strTemp);
					if ((nEWIncr < 0) || (nEWIncr >= ctnMaxImgIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("The E/W start increment entry # %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalEWStart += nEWIncr;
					}

					strTemp = cParseFrame.GetDataStopCycEW();
					nEWCycle = _ttoi(strTemp);
					if ((nEWCycle < 0) || (nEWCycle > ctnMaxEWCycles)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("The E/W stop cycle entry # %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalEWStop += nEWCycle * ctnMaxImgIncrements;
					}

					strTemp = cParseFrame.GetDataStopIncEW();
					nEWIncr = _ttoi(strTemp);
					if ((nEWIncr < 0) || (nEWIncr >= ctnMaxImgIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("The E/W stop increment entry # %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalEWStop += nEWIncr;
					}

					strTemp = cParseFrame.GetDataStartCycNS();
					int nNSCycle = _ttoi(strTemp);
					if ((nNSCycle < 0) || (nNSCycle > ctnMaxNSCycles)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("The N/S start cycle entry # %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStart += nNSCycle * ctnMaxImgIncrements;
					}

					strTemp = cParseFrame.GetDataStartIncNS();
					int nNSIncr = _ttoi(strTemp);
					if ((nNSIncr < 0) || (nNSIncr >= ctnMaxImgIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("The N/S start increment entry # %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStart += nNSIncr;
					}

					strTemp = cParseFrame.GetDataStopCycNS();
					nNSCycle = _ttoi(strTemp);
					if ((nNSCycle < 0) || (nNSCycle > ctnMaxNSCycles)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("The N/S stop cycle entry # %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStop += nNSCycle * ctnMaxImgIncrements;
					}

					strTemp = cParseFrame.GetDataStopIncNS();
					nNSIncr = _ttoi(strTemp);
					if ((nNSIncr < 0) || (nNSIncr >= ctnMaxImgIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("The N/S stop increment entry # %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStop += nNSIncr;
					}

					if (bInvalid_coord){
						double dScanDegsEWNadirImgr = _tcstod(GetAppRegSC()->m_strScanDegsEWNadirImgr[GetAppRegSite()->m_nDefaultSC], NULL);
						// double dTotIncrLimitEW = 0.0;
						// dTotIncrLimitEW = (dScanDegsEWNadirImgr / ctdDegs2IncrEWImgr);

						double dTotIncrLimitEW = ((dScanDegsEWNadirImgr * ctdIncr2CyclesImgr) / (2 * ctdDegrees2Cycles));

						double dInstrCenterPtImgrEW = (double)GetAppRegSC()->m_nInstrCenterPtImgrEW[GetAppRegSite()->m_nDefaultSC];
						double dLowLimitEW = dInstrCenterPtImgrEW - dTotIncrLimitEW;
						double dHiLimitEW  = dInstrCenterPtImgrEW + dTotIncrLimitEW;

						//
						if ((dTotalEWStart - dLowLimitEW) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsEWNadirImgr, dTotalEWStart);
							m_NewGblValData.WriteValidationMsg(0, 45, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitEW - dTotalEWStart) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsEWNadirImgr, dTotalEWStart);
							m_NewGblValData.WriteValidationMsg(0, 44, strMsg);
							eReturn_Status = FALSE;
						}
							//
						if ((dTotalEWStop - dLowLimitEW) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsEWNadirImgr, dTotalEWStop);
							m_NewGblValData.WriteValidationMsg(0, 45, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitEW - dTotalEWStop) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsEWNadirImgr, dTotalEWStop);
							m_NewGblValData.WriteValidationMsg(0, 44, strMsg);
							eReturn_Status = FALSE;
						}

						if ((fabs(dTotalEWStop - dTotalEWStart)) < 4) {
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							m_NewGblValData.WriteValidationMsg(0, 82, strTemp);
							eReturn_Status = FALSE;
						}

						double dScanDegsNSNadirImgr = _tcstod(GetAppRegSC()->m_strScanDegsNSNadirImgr[GetAppRegSite()->m_nDefaultSC], NULL);
						// double dTotIncrLimitNS = (dScanDegsNSNadirImgr / ctdDegs2IncrNSImgr);

						double dTotIncrLimitNS = ((dScanDegsNSNadirImgr * ctdIncr2CyclesImgr) / (ctdDegrees2Cycles));

						double dInstrCenterPtImgrNS = (double)GetAppRegSC()->m_nInstrCenterPtImgrNS[GetAppRegSite()->m_nDefaultSC];

						double dLowLimitNS = dInstrCenterPtImgrNS - dTotIncrLimitNS;
						double dHiLimitNS  = dInstrCenterPtImgrNS + dTotIncrLimitNS;

						//
						if ((dTotalNSStart - dLowLimitNS) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsNSNadirImgr, dTotalNSStart);
							m_NewGblValData.WriteValidationMsg(0, 47, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitNS - dTotalNSStart) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsNSNadirImgr, dTotalNSStart);
							m_NewGblValData.WriteValidationMsg(0, 46, strMsg);
							eReturn_Status = FALSE;
						}
						//
						if ((dTotalNSStop - dLowLimitNS) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsNSNadirImgr, dTotalNSStop);
							m_NewGblValData.WriteValidationMsg(0, 47, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitNS - dTotalNSStop) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsNSNadirImgr, dTotalNSStop);
							m_NewGblValData.WriteValidationMsg(0, 46, strMsg);
							eReturn_Status = FALSE;
						}

						if (fmod((dTotalNSStop - dTotalNSStart),28) != 0.0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							m_NewGblValData.WriteValidationMsg(0, 81, strTemp);
							eReturn_Status = FALSE;
						}

						double dSpaceLookImgrWest = (double)GetAppRegSC()->m_nSpaceLookImgrWest[GetAppRegSite()->m_nDefaultSC];
						double dTemp = fmod((dTotalEWStart - dSpaceLookImgrWest), 4);
						if ((dTemp - 0.0) > 0.000001){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							m_NewGblValData.WriteValidationMsg(0, 49);
							eReturn_Status = FALSE;
						}

						double dSpaceLookImgrEast = (double)GetAppRegSC()->m_nSpaceLookImgrEast[GetAppRegSite()->m_nDefaultSC];
						dTemp = fmod((dTotalEWStop - dSpaceLookImgrEast), 4);
						if (fabs(dTemp - 0.0) > 0.000001){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							m_NewGblValData.WriteValidationMsg(0, 80);
							eReturn_Status = FALSE;
						}

						// This only applies to space clamp frames.
						CString strSpaceLookMode = cParseFrame.GetDataSpacelookMode();
						if (0 != strSpaceLookMode.CompareNoCase(ctstrSCAN)){
							if (0 == strSideValue.CompareNoCase(ctstrEAST)){
								double dSpaceLookImgrEast = (double)GetAppRegSC()->m_nSpaceLookImgrEast[GetAppRegSite()->m_nDefaultSC];
								if (fabs(dTotalEWStart - dSpaceLookImgrEast) < 1600){
									if (!bFileNameHdr){
										// Only output message once per entry
										strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
										m_NewGblValData.WriteValidationMsg(0, strError, 0);
										bFileNameHdr = true;
									}
									CString strMsg;
									strMsg.Format(_T("%.0f"), dTotalEWStart);
									m_NewGblValData.WriteValidationMsg(0, 91, strMsg);
									eReturn_Status = FALSE;
								}
							} else if (0 == strSideValue.CompareNoCase(ctstrWEST)){
								double dSpaceLookImgrWest = (double)GetAppRegSC()->m_nSpaceLookImgrWest[GetAppRegSite()->m_nDefaultSC];
								if (fabs(dSpaceLookImgrWest - dTotalEWStart) < 1600){
									if (!bFileNameHdr){
										// Only output message once per entry
										strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
										m_NewGblValData.WriteValidationMsg(0, strError, 0);
										bFileNameHdr = true;
									}
									CString strMsg;
									strMsg.Format(_T("%.0f"), dTotalEWStart);
									m_NewGblValData.WriteValidationMsg(0, 91, strMsg);
									eReturn_Status = FALSE;
								}
							}
						}
					}
				} else if (0 == strInstrValue.CompareNoCase(ctstrSDR)){

					CString strTemp;
					strTemp = cParseFrame.GetDataStartCycEW();
					int nEWCycle = _ttoi(strTemp);
					if ((nEWCycle < 0) || ( nEWCycle > ctnMaxEWCycles)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalEWStart += nEWCycle * ctnMaxSndIncrements;
					}

					strTemp = cParseFrame.GetDataStartIncEW();
					int nEWIncr = _ttoi(strTemp);
					if ((nEWIncr < 0) || (nEWIncr >= ctnMaxSndIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalEWStart += nEWIncr;
					}

					strTemp = cParseFrame.GetDataStopCycEW();
					nEWCycle = _ttoi(strTemp);
					if ((nEWCycle < 0) || (nEWCycle  >= ctnMaxEWCycles)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalEWStop += nEWCycle * ctnMaxSndIncrements;
					}

					strTemp = cParseFrame.GetDataStopIncEW();
					nEWIncr = _ttoi(strTemp);
					if ((nEWIncr < 0) || (nEWIncr > ctnMaxSndIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalEWStop += nEWIncr;
					}

					strTemp = cParseFrame.GetDataStartCycNS();
					int nNSCycle = _ttoi(strTemp);
					if ((nNSCycle < 0) || (nNSCycle > ctnMaxCmdScanNSCyc)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStart += nNSCycle * ctnMaxSndIncrements;
					}

					strTemp = cParseFrame.GetDataStartIncNS();
					int nNSIncr = _ttoi(strTemp);
					if ((nNSIncr < 0) ||  (nNSIncr >= ctnMaxSndIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStart += nNSIncr;
					}

					strTemp = cParseFrame.GetDataStopCycNS();
					nNSCycle = _ttoi(strTemp);
					if ((nNSCycle < 0) || (nNSCycle > ctnMaxCmdScanNSCyc)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStop += nNSCycle * ctnMaxSndIncrements;
					}

					strTemp = cParseFrame.GetDataStopIncNS();
					nNSIncr = _ttoi(strTemp);
					if ((nNSIncr < 0) || (nNSIncr >=ctnMaxSndIncrements)){
						if (!bFileNameHdr){
							// Only output message once per entry
							strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
							m_NewGblValData.WriteValidationMsg(0, strError, 0);
							bFileNameHdr = true;
						}
						m_NewGblValData.WriteValidationMsg(0, 39, strTemp);
						eReturn_Status = FALSE;
						bInvalid_coord = false;
					} else {
						dTotalNSStop += nNSIncr;
					}

					if (bInvalid_coord){
						double dTotIncrLimitEW = 0.0;
						double dScanDegsEWNadirSdr = _tcstod(GetAppRegSC()->m_strScanDegsEWNadirSdr[GetAppRegSite()->m_nDefaultSC], NULL);
						// dTotIncrLimitEW = (dScanDegsEWNadirSdr / ctdDegs2IncrEWSdr);

						dTotIncrLimitEW = ((dScanDegsEWNadirSdr * ctdIncr2CyclesSdr) / (ctdDegrees2Cycles * 2));

						double dInstrCenterPtSdrEW = GetAppRegSC()->m_nInstrCenterPtSdrEW[GetAppRegSite()->m_nDefaultSC];
						double dLowLimitEW = dInstrCenterPtSdrEW - dTotIncrLimitEW;
						double dHiLimitEW  = dInstrCenterPtSdrEW + dTotIncrLimitEW;

						//
						if ((dTotalEWStart - dLowLimitEW) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsEWNadirSdr, dTotalEWStart);
							m_NewGblValData.WriteValidationMsg(0, 57, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitEW - dTotalEWStart) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);\
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsEWNadirSdr, dTotalEWStart);
							m_NewGblValData.WriteValidationMsg(0, 56, strMsg);
							eReturn_Status = FALSE;
						}
						//
						if ((dTotalEWStop - dLowLimitEW) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsEWNadirSdr, dTotalEWStop);
							m_NewGblValData.WriteValidationMsg(0, 57, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitEW - dTotalEWStop) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsEWNadirSdr, dTotalEWStop);
							m_NewGblValData.WriteValidationMsg(0, 56, strMsg);
							eReturn_Status = FALSE;
						}

						if ((fabs(dTotalEWStop - dTotalEWStart)) < 4) {
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							m_NewGblValData.WriteValidationMsg(0, 82, strTemp);
							eReturn_Status = FALSE;
						}

						double dTotIncrLimitNS = 0.0;
						double dScanDegsNSNadirSdr = _tcstod(GetAppRegSC()->m_strScanDegsNSNadirSdr[GetAppRegSite()->m_nDefaultSC], NULL);
						// dTotIncrLimitNS = (dScanDegsNSNadirSdr / ctdDegs2IncrNSSdr);

						dTotIncrLimitNS = ((dScanDegsNSNadirSdr * ctdIncr2CyclesSdr) / (ctdDegrees2Cycles));

						double dInstrCenterPtSdrNS = GetAppRegSC()->m_nInstrCenterPtSdrNS[GetAppRegSite()->m_nDefaultSC];
						double dLowLimitNS = dInstrCenterPtSdrNS - dTotIncrLimitNS;
						double dHiLimitNS  = dInstrCenterPtSdrNS + dTotIncrLimitNS;

						//
						if ((dTotalNSStart - dLowLimitNS) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsNSNadirSdr, dTotalNSStart);
							m_NewGblValData.WriteValidationMsg(0, 59, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitNS - dTotalNSStart) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsNSNadirSdr, dTotalNSStart);
							m_NewGblValData.WriteValidationMsg(0, 58, strMsg);
							eReturn_Status = FALSE;
						}
						//
						if ((dTotalNSStop - dLowLimitNS) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
								bFileNameHdr = true;
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsNSNadirSdr, dTotalNSStop);
							m_NewGblValData.WriteValidationMsg(0, 59, strMsg);
							eReturn_Status = FALSE;
						}

						if ((dHiLimitNS - dTotalNSStop) < 0){
							if (!bFileNameHdr){
								// Only output message once per entry
								strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
								m_NewGblValData.WriteValidationMsg(0, strError, 0);
							}
							CString strMsg;
							strMsg.Format(_T("limit:%.2f� total increments:%.2f"), dScanDegsNSNadirSdr, dTotalNSStop);
							m_NewGblValData.WriteValidationMsg(0, 58, strMsg);
							eReturn_Status = FALSE;
						}

						if (0 == strSideValue.CompareNoCase(ctstrEAST)){
							double dSpaceLookSdrEast = (double)GetAppRegSC()->m_nSpaceLookSdrEast[GetAppRegSite()->m_nDefaultSC];
							if (fabs(dTotalEWStart - dSpaceLookSdrEast) < 1600){
								if (!bFileNameHdr){
									// Only output message once per entry
									strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
									m_NewGblValData.WriteValidationMsg(0, strError, 0);
									bFileNameHdr = true;
								}
								CString strMsg;
								strMsg.Format(_T("%.0f"), dTotalEWStart);
								m_NewGblValData.WriteValidationMsg(0, 91, strMsg);
								eReturn_Status = FALSE;
							}
						} else {
							double dSpaceLookSdrWest = (double)GetAppRegSC()->m_nSpaceLookSdrWest[GetAppRegSite()->m_nDefaultSC];
							if (fabs(dSpaceLookSdrWest - dTotalEWStart) < 1600){
								if (!bFileNameHdr){
									// Only output message once per entry
									strError.Format(_T("Entry %d is invalid in the instrument object file: %s"), nCurObj-1, lpszPathName);
									m_NewGblValData.WriteValidationMsg(0, strError, 0);
									bFileNameHdr = true;
								}
								CString strMsg;
								strMsg.Format(_T("%.0f"), dTotalEWStart);
								m_NewGblValData.WriteValidationMsg(0, 91, strMsg);
								eReturn_Status = FALSE;
							}
						}
					}
				}
			}
		nCurObj++;
		}
	}
	// Close the file, all of the data is now local
	if (eReturn_Status) {
		CFileException cFE;
		dataFile.Close(&cFE);
	}
	return eReturn_Status;
}
