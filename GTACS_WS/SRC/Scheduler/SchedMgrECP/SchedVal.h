#if !defined(__SCHEDVAL_H__)
#define __SCHEDVAL_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SchedVal.h : header file
//

#define _NO_PROGRESS 1

#include "NewGblValData.h"
#include "SchedTime.h"
#include "TextFile.h"
// #include "Path.h"
/////////////////////////////////////////////////////////////////////////////
// SchedVal window

// 	typedef enum eFileType {CLS, STOL, FRAME, STAR, IMC, RTCS_CLS, RTCS_STO, RPT, INV, UNDEF};

class CSchedVal : public CNewSchedFile
{

// Construction
public:

typedef enum eValType {CLS_VAL, STOL_VAL, SKB_VAL, RTCS_SET_VAL, RTCS_PRC_VAL, STORED_VAL, INV_VAL, UNDEF_VAL};
typedef	struct CMDPaceMnemonicList{
			CString strMnemonic01;	// Mnemonic which has a time delay associated with it.
			float   fDelayTime;	    // Time that must elapsed before next mnemonic can be executed.
			CString strMnemonic02;	// Mnemonic which can't be executed after mnemonic01
		}strctCMDPaceMnemonicList;

typedef	struct CMDPaceRestrictList{
  			   CSchedTime   cDelayTime;	// Time that must elapsed before mnemonic can be executed.
			   CString strMnemonic01;	// Specific mnemonic that can not be executed before elapsed.
				                        // If empty no mnemonics can be executed before elapsed time. 
		}strctCMDPaceRestrictList;

typedef	struct RTCSSetProcList{
  			   CString strLabel; // RTCS label
			   CString strName;	 // RTCS proc name associated with the label.
			   int     nIndex;
		}strctRTCSSetProcList;

typedef	struct IntrusionRptList{
  			   CString strInstrument; // 
			   CString strEdge;	 // 
			   CString strBody;
			   CString strStartTime;
			   CString strStopTime;
		}strctIntrusionRptList;


	CSchedVal(const CString strInputFileName);
	virtual ~CSchedVal();
	BOOL Validate(const enum eValType eFileValType);
	eValidLoc GetValLoc(){return m_eValLoc;};
	void SetValLoc(eValidLoc eValLoc){m_eValLoc = eValLoc;};
	CString GetRTCSSetFileName(){return m_strRTCSSetFileName;};
	void SetRTCSSetFileName(CString strRTCSSetFileName){m_strRTCSSetFileName = strRTCSSetFileName;};
	BOOL GetValRTCSProcsFlag(){return m_eValRTCSProcs;};
	void SetValRTCSProcsFlag(BOOL eValRTCSProcsFlag){m_eValRTCSProcs = eValRTCSProcsFlag;};
	
protected:

	BOOL Validate_STOL_File ();
	BOOL Validate_CLS_File ();
	BOOL Validate_RTCS_SET_File ();
	BOOL Validate_RTCS_PRC_File ();
	BOOL Validate_STORED_File ();
	BOOL Check_Activity_Log ();
	BOOL Validate_Prolog(const enum eValType eFileValType);
	BOOL Validate_CLS_Dir (CString strLine);
	BOOL Validate_Include_Dir (CString strLine);
	BOOL Validate_NEXT_Dir (CString strLine);
	BOOL Validate_RTCS_Dir (CString strLine);
	BOOL Validate_START_Dir (CString strLine);
	BOOL Validate_ARGS_Dir (CString strLine);
	BOOL Validate_SCHEDULE_Dir (CString strLine);
	
	BOOL Validate_Scan_Dir (CString strLine);
	BOOL Validate_ScanData_Dir (CString strLine);
	BOOL Validate_ScanStart_Dir(CString strLine);
	BOOL Validate_FrameObj_Proc(CString strLine);
	BOOL Validate_FrameGrd_Proc(CString strLine);
//	BOOL Validate_Side_Param(CString strLine);

	BOOL Validate_Star_Dir (CString strLine);
//	BOOL Validate_StarData_Dir (CString strLine);
	BOOL Validate_StarStart_Dir(CString strLine);
//	BOOL Validate_StarInfo_Dir(CString strLine);
	BOOL Validate_StarObj_Proc(CString strLine);
	BOOL Validate_StarGrd_Proc(CString strLine);

	BOOL Validate_Wait_Dir (CString strLine);
	BOOL Validate_Activity_Dir (CString strLine);
	BOOL Validate_CMD_Dir (CString strMnemonic);
	BOOL CheckForDuplicatePassedParams();

//	BOOL Validate_PRC_START(CString strLine);

	BOOL LoadCmdPacingFile();
	BOOL CheckMnemonicCurrentTimeTag(CString strMnemonic);
	BOOL CheckCmdPacingFile(CString strMnemonic); 
	BOOL Parse_CMD_Line(CString strCMDLine, CString &strMnemonic);

	BOOL CheckForMovTopPtrCmd(CString strCmdLine);
	BOOL CheckForNoOpCmd(CString strCmdLine);
	BOOL Validate_RTCSName(CString strRTCSName);
	BOOL ReadRTCSSetFile();
	CString LookUpLabel(CString strLabel);

	BOOL LoadRTCSCmdRestrictFile();
	BOOL IsMnemonicRestricted(CString strMnemonic);

	BOOL LoadSTOLCmdRestrictFile();
	// This function uses the command restriction file
	BOOL IsMnemonicRestricted4STOL(CString strMnemonic);

	// This function contains hard-coded mnemonics 
	BOOL IsMnemonicRestricted4Procs(CString strMnemonic);	
	
	BOOL CheckForLocalVariables(CString strLine);
	BOOL CheckForLocalVariables(CString strLine, CTextFile &Prc);

	BOOL LoadSensorIntrusionRptFile();
	BOOL CheckForIntrusion();

	BOOL LoadNumCmdFrameFile();
	BOOL UpdateNumberCmdFrames(CString strMnemonic);

	BOOL StoreFilenameAndType(CStringArray * strDataUpdate);

	// 
	BOOL Check_Star_InstrObjFile(); 
	BOOL Check_Scan_InstrObjFile(); 
	BOOL Validate_StarInstrObjFileLine(LPCTSTR lpszPathName);
	BOOL Validate_ScanInstrObjFileLine(LPCTSTR lpszPathName);

private:
	
	CNewGblValData	m_NewGblValData;
	bool            m_bBeginFound; 
	int             m_nGrdBeginCnt; // Running count of ##ground begin directives found.
//	bool            m_bDOYFound;
//	int             m_nNumPrologLines;
	eValidStatus    m_eValidStatus; // Validation status of file
	eValidLoc	    m_eValLoc;  // Location where the procedure is to be executed.
	eValType        m_eValType;


	CSchedTimeSpan  m_CSTCLSStopTime;
	bool            m_bCLSStopTimeFound;

	CSchedTimeSpan  m_CSTCLSStartTime;
	bool            m_bCLSStartTimeFound;

	// SSGS-1206: For validating if enough time has elasped since last CLS execution.
	CSchedTime      m_CSTCLSWinTime;
	bool			m_bCLSWinTimeFound;
	
	CSchedTime      m_CSTPrevTime;
	bool            m_bPrevTimeFound;
	
	CSchedTime      m_CSTCurrTime;
	bool            m_bCurrTimeFound; 

	CSchedTime      m_CSTSTARWinTime; // Time the star window ends. 
	bool            m_bSTARWinTimeFound;   

	CSchedTime      m_CSTIMGRWinTime; // Scanning time for the imager instrument.
	bool            m_bIMGRWinTimeFound;

	CSchedTime      m_CSTSDRWinTime;  // Scanning time for the sounder instrument.
	bool            m_bSDRWinTimeFound;

//	CSchedTimeSpan m_CSTExecTime;
//	bool           m_bExecTimeFound;
    
	CSchedTime	    m_CSTPreHseKpgTime;
	bool            m_bPreHseKpgTimeFound;
	 
	CSchedTimeSpan  m_CSTPreHseKpgGap;
	bool            m_bPreHseKpgGapFound;

	CSchedTime	    m_CSTPrcWinTime;
	bool            m_bPrcWinTimeFound;

	CSchedTime	    m_CSTRTCSWinTime;
	bool            m_bRTCSWinTimeFound;

	// File paths 
	COXUNC			m_UNCPrcPath;	
	COXUNC			m_UNCCLSPath;
	COXUNC			m_UNCInputFileName;
	CStringArray    m_SearchPathArray;

	// User supplied: Maximum time that can elapse without a command being sent.
	CSchedTimeSpan  m_CSTCMDGap;
	bool            m_bCMDGapFound;

	// Time that a command needs to be sent by.
	CSchedTime m_CSTCurrCMDGapCmd;
	bool       m_bCurrCMDGapCmdFound;
	
	// Running time total for a series of commands.
	CSchedTime m_CSTTotalCMDTime;
	bool       m_bTotalCMDTimeFound;

	// User supplied parameters for calculating total time required by individual
	// commands.
	int   m_nRetryCnt;
	float m_fCmdTime;

	// Variable names of passed parameters.
	// CLS's only.
	CStringArray m_PassedParamNames;

	// Activity directive member variables
	CStringArray m_strActivityName;
	CObArray     m_objActivityEndTime;
	CUIntArray   m_uActivitySchedLineNum;

	// RTCS validation section
	CStringArray m_strRTCSLabel;
	int			 m_nPrevStartValue;
	CString		 m_strRTCSSetFileName;
	CObArray     m_objRTCSSetProc;
	BOOL		 m_eValRTCSProcs;
	bool		 m_bReadRTCSSetFile;
	
	// RTCS command restriction section.
	CStringArray m_strRTCSCmdAllow;
	CStringArray m_strRTCSRestrict;
	bool m_bRTCSCMDRestrictedFileLoaded;

	// Command pacing validation section.

		// Time that has to elapse before any command can be sent.
	CSchedTime m_CSTPaceCMDTime;
	bool       m_bPaceCMDTimeFound;

	bool       m_bCMDPaceFileLoaded;
	CObArray   m_objCMDPaceMnemonic;
	CObArray   m_objCMDPaceRestriction;

	// Scan directive validation parameters
	CString	   m_strInstrValue;
	CString    m_strSideValue;
	CString    m_strStarBit;

	// Reading sensor intrusion report
	bool       m_bSensorIntrFileLoaded;
	CObArray   m_objSensorIntrusions;

	// SCHEDX NEXTSCHED directive validation
	bool       m_bNextSchedGo;
	bool       m_bNextSchedSet;

	// Prc_Start variable validation 
	CSchedTime m_CSTPrc_StartTime;
	bool       m_bPrc_StartTimeFound;

	// Reading number of frame command report
	bool	           m_bNumCmdFrameFileLoaded;
	CMapStringToString m_mapNumCmdFrame;
	int                m_nTotalNumCmdFrames;

	// Schedule command restriction section.
	CStringArray m_strSTOLCmdAllow;
	CStringArray m_strSTOLRestrict;
	bool         m_bSTOLCMDRestrictedFileLoaded;

	// InstrumentObject Validation
	CStringArray  m_strScanFilename;
	CStringArray  m_strStarFilename;
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(__SCHEDVAL_H__)

