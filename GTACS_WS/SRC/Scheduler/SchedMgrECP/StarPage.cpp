/**
/////////////////////////////////////////////////////////////////////////////
// StarPage.cpp : implementation of the StarPage class.                    //
// (c) 2001 Ray Mosley                                                     //
//  PDL Updated                                                            //
//																		   //
// MSD (06/12) PR000446: Implemented Defaults during SchedUpd.			   //
/////////////////////////////////////////////////////////////////////////////

// StarPage.cpp : implementation file
//
/////////////////////////////////////////////////////////////////////////////
// This class will allow the user to choose star parameters for updating
// a schedule for stars.
// 
/////////////////////////////////////////////////////////////////////////////
**/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "GenUpdSheet.h"
#include "FinishDialog.h"
#include "StarPage.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CStarPage property page

IMPLEMENT_DYNCREATE(CStarPage, CResizablePage)

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CStarPage::CStarPage
//	Description :	Constructor
//
//	Return :		Constructor
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CStarPage::CStarPage() : CResizablePage(CStarPage::IDD)
{
	//{{AFX_DATA_INIT(CStarPage)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CStarPage::~CStarPage
//	Description :	Destructor
//
//	Return :		Destructor -
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
CStarPage::~CStarPage()
{
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CStarPage::DoDataExchange
//  Description :   This routine maps the dialog ccontrol boxes to the
//                  appropriate member variables. This routine is 
//					generated and maintained by the code wizard.
//
//  Returns :		void	-  
//
//  Parameters : 
//           CDataExchange* pDX
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
void CStarPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CStarPage)
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CStarPage, CResizablePage)
	//{{AFX_MSG_MAP(CStarPage)
	ON_BN_CLICKED(IDC_STAR_UPD_CHECK, OnUpdateControls)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_STAR_GEN_CHECK, OnUpdateControls)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CStarPage message handlers

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CStarPage::Init
//  Description :	This routine initializes the member variables based on 
//					the passed parameters.
//
//  Returns :		void	-
//
//  Parameters :
//		BOOL bFirstWizPage	-	first display page.
//		BOOL bLastWizPage	-	last display page.
//		BOOL bGen			-	update for stars.
//		BOOL bUseDef		-   use default values.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CStarPage::Init(BOOL bFirstWizPage, BOOL bLastWizPage, BOOL bGen, BOOL bUseDef)
{
	m_bFirstWizPage	= bFirstWizPage;
	m_bLastWizPage	= bLastWizPage;
//	m_bGen			= bGen;
//	m_bUse			= TRUE;
	m_bSchedUpd		= bUseDef;
	//PR00446: MSD (06/12) - Always update for stars if bUseDef is TRUE.
	//Ignore use options while updating schedule.
	if(bUseDef) {
		m_bGen = nRegMakeSchedStarGenDefValue;
		m_bUse = nRegMakeSchedStarUpdateDefValue;
	} else {
		m_bGen			= GetAppRegMakeSched()->m_nStarGen;
		m_bUse			= GetAppRegMakeSched()->m_nStarUpdate;
	}
}

/**
//////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////
//	Function :		CStarPage::OnInitDialog
//  Description :	This routine creates the StarPage dialog window.  
//
//	Return :		BOOL	-	TRUE if successful
//
//	Parameters :
//	
//	Note :
//////////////////////////////////////////////////////////////////////
**/
BOOL CStarPage::OnInitDialog() 
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	CResizablePage::OnInitDialog();
	
	// Add all of the necessary anchors
	AddAnchor(IDC_STAR_UPD_GROUP, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_STAR_UPD_CHECK, TOP_LEFT, TOP_LEFT);
	
	AddAnchor(IDC_STAR_GEN_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_STAR_GEN_CHECK, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_STAR_GEN_BIN_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_STAR_GEN_BIN_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_STAR_GEN_PROC_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_STAR_GEN_PROC_STATIC, TOP_LEFT, TOP_RIGHT);
	
	AddAnchor(IDC_STAR_LOC_GROUP, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_STAR_LOC_STAR_LABEL, TOP_LEFT, TOP_LEFT);
	AddAnchor(IDC_STAR_LOC_STAR_STATIC, TOP_LEFT, TOP_RIGHT);

	// Set the mask, prompt symbol, and range of the various Day controls

	CImageList imageList;
	imageList.Create(IDR_STAR_MENUIMAGES, 16, 0, RGB (192,192,192));
	((CStatic*)GetDlgItem(IDC_STAR_UPD_ICON))->SetIcon(imageList.ExtractIcon(1));
	((CStatic*)GetDlgItem(IDC_STAR_GEN_ICON))->SetIcon(imageList.ExtractIcon(0));
	
	GetDlgItem(IDC_STAR_UPD_ICON)->BringWindowToTop();
	GetDlgItem(IDC_STAR_GEN_ICON)->BringWindowToTop();

	BOOL bGenEnable = TRUE;
	
	switch (pSheet->GetFunc())
	{
		case nPackage:	bGenEnable = TRUE;	break;
		default:		bGenEnable = FALSE;	break;
	}
	
	GetDlgItem(IDC_STAR_GEN_GROUP)->EnableWindow(bGenEnable);	
	GetDlgItem(IDC_STAR_GEN_CHECK)->EnableWindow(bGenEnable);	
	GetDlgItem(IDC_STAR_GEN_ICON)->EnableWindow(bGenEnable);	
	GetDlgItem(IDC_STAR_GEN_BIN_LABEL)->EnableWindow(bGenEnable);	
	GetDlgItem(IDC_STAR_GEN_BIN_STATIC)->EnableWindow(bGenEnable);	
	GetDlgItem(IDC_STAR_GEN_PROC_LABEL)->EnableWindow(bGenEnable);	
	GetDlgItem(IDC_STAR_GEN_PROC_STATIC)->EnableWindow(bGenEnable);	
	
	m_bInInit = FALSE;

	return TRUE;
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CStarPage::OnSetActive  
//  Description :	This routine disables/enables, as appropriate, the 
//					different window controls.
//                  
//  Returns :		BOOL	-	TRUE  if successful
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CStarPage::OnSetActive()
{
	CGenUpdSheet* pSheet = (CGenUpdSheet*)GetParent();
	
	// Update all of the check boxes with the proper status
	// PR00445: MSD (06/12) - Don't need these. Done in init()
	//m_bUse =  GetAppRegMakeSched()->m_nStarUpdate;
	//m_bGen =  GetAppRegMakeSched()->m_nStarGen;
	((CButton*)GetDlgItem(IDC_STAR_UPD_CHECK))->SetCheck(m_bUse);
	((CButton*)GetDlgItem(IDC_STAR_GEN_CHECK))->SetCheck(m_bGen);
	
	// Update the visibility of the controls based upon the radio button positions
	// Update the filenames
	UpdateControls();
	
	// Setup the proper Wizard Buttons
	DWORD dWizFlags = PSWIZB_FINISH;
	if ((m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_FINISH;
	else if ((m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_NEXT;
	else if ((!m_bFirstWizPage) && (m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_FINISH;
	else if ((!m_bFirstWizPage) && (!m_bLastWizPage))
		dWizFlags = PSWIZB_BACK | PSWIZB_NEXT;
	
	pSheet->SetWizardButtons(dWizFlags);
	
	return CResizablePage::OnSetActive();
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CStarPage::OnSize  
//  Description :   This routine resizes the dialog window based on the 
//					the coordinates passed into the routine.	
//                  
//  Returns :		void	-
//  Parameters : 
//           UINT nType	-	type of window resizing requested.
//           int cx		-	new width of window.
//           int cy		-	new height of window.
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CStarPage::OnSize(UINT nType, int cx, int cy) 
{
	CResizablePage::OnSize(nType, cx, cy);
	ResizeFilenames();
}	

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CStarPage::ResizeFilenames  
//  Description :   This routine is called to resize the pathname is 
//					displayed. So that it fits within the dialog control. 	
//                  
//  Returns :		void	-
//
//  Parameters :  
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CStarPage::ResizeFilenames()
{
	CGenUpdSheet* pSheet		= (CGenUpdSheet*)GetParent();

	CString strBinFilename		= pSheet->MakeFilepath(nFolderStar, nBinary,TRUE);
	CString strLoadFilename		= pSheet->MakeFilepath(nFolderStar, nSTOL,	TRUE);
	CString strUpdateFilename	= pSheet->MakeFilepath(nFolderStar, nStar,	FALSE);
	
	PathSetDlgItemPath(this->m_hWnd, IDC_STAR_GEN_BIN_STATIC,	strBinFilename);
	PathSetDlgItemPath(this->m_hWnd, IDC_STAR_GEN_PROC_STATIC,	strLoadFilename);
	PathSetDlgItemPath(this->m_hWnd, IDC_STAR_LOC_STAR_STATIC,	strUpdateFilename);
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CStarPage::UpdateControls  
//  Description :	This routine updates all window controls associated 
//					with the CStarPage class.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CStarPage::UpdateControls()
{
	OnUpdateControls();
}

/**
//////////////////////////////////////////////////////////////////////////
//	Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CStarPage::OnUpdateControls  
//  Description :	This routine updates all window controls associated 
//					with the CStarPage class.
//                  
//  Returns :		void	-  
//
//  Parameters : 
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
void CStarPage::OnUpdateControls()
{
	m_bUse = ((CButton*)GetDlgItem(IDC_STAR_UPD_CHECK))->GetCheck();	
	m_bGen = ((CButton*)GetDlgItem(IDC_STAR_GEN_CHECK))->GetCheck();

	// Save off checked values
	// PR00445: MSD (06/12) - Only save if not updating schedule.
	if(!m_bSchedUpd) {
		GetAppRegMakeSched()->m_nStarUpdate = m_bUse;
		GetAppRegMakeSched()->m_nStarGen = m_bGen;
	}
	GetDlgItem(IDC_STAR_GEN_GROUP)->EnableWindow(m_bUse);
	GetDlgItem(IDC_STAR_GEN_CHECK)->EnableWindow(m_bUse);
	GetDlgItem(IDC_STAR_GEN_BIN_LABEL)->EnableWindow(m_bUse && m_bGen);
	GetDlgItem(IDC_STAR_GEN_BIN_STATIC)->EnableWindow(m_bUse && m_bGen);
	GetDlgItem(IDC_STAR_GEN_PROC_LABEL)->EnableWindow(m_bUse && m_bGen);
	GetDlgItem(IDC_STAR_GEN_PROC_STATIC)->EnableWindow(m_bUse && m_bGen);
	
	GetDlgItem(IDC_STAR_LOC_GROUP)->EnableWindow(m_bUse);
	GetDlgItem(IDC_STAR_LOC_STAR_LABEL)->EnableWindow(m_bUse);
	GetDlgItem(IDC_STAR_LOC_STAR_STATIC)->EnableWindow(m_bUse);
}

/**
//////////////////////////////////////////////////////////////////////////
//  Author : Don Sanders		Date : 4/23/01		version 1.0
//////////////////////////////////////////////////////////////////////////
//  Function :		CStarPage::OnWizardFinish  
//  Description :   This routine is called when the okay button is checked
//					on the finish dialog box 	
//                  
//  Returns :		BOOL	-	TRUE if successful.
//
//  Parameters :  
//
//  Note :
//
//////////////////////////////////////////////////////////////////////////
**/
BOOL CStarPage::OnWizardFinish() 
{
	CFinishDialog dlg;
	if (dlg.DoModal() == IDOK)
		return TRUE;
	else
		return FALSE;
}
