#if !defined(AFX_STARPAGE_H__3D27A6E1_0D80_11D5_9A63_0003472193C8__INCLUDED_)
#define AFX_STARPAGE_H__3D27A6E1_0D80_11D5_9A63_0003472193C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StarPage.h : header file
//
// MSD (06/12) PR000446: Implemented defaults during SchedUpd.
/////////////////////////////////////////////////////////////////////////////
// CStarPage dialog

class CStarPage : public CResizablePage
{
	DECLARE_DYNCREATE(CStarPage)

// Construction
public:
	CStarPage();
	~CStarPage();
	void Init(BOOL bFirstWizPage, BOOL bLastWizPage, BOOL bGen, BOOL bUseDef);
	BOOL GetGen(){return m_bGen;};
	BOOL GetUse(){return m_bUse;};
	
// Dialog Data
	//{{AFX_DATA(CStarPage)
	enum { IDD = IDD_STAR_DIALOG };
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CStarPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CStarPage)
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnSetActive();
	afx_msg void OnUpdateControls();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	virtual BOOL OnWizardFinish();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	void	UpdateControls();
	void	ResizeFilenames();
	BOOL	m_bUse;			// TRUE if stars should be used
	BOOL	m_bGen;			// Generate upload - if TRUE, then generate upload files
	BOOL    m_bSchedUpd;	// TRUE if doing schedule update, so we don't store values to reg.
	BOOL	m_bFirstWizPage;// TRUE if this the first page of a Wizard
	BOOL	m_bLastWizPage;	// TRUE if this the last page of a Wizard
	BOOL	m_bInInit;		// TRUE when in InitInstance.  This is needed so that the OnUpdate
							// routines get ignored for the day and version controls.  These
							// controls are COXMaskedEdit classes and the OnUpdate functions get
							// called whenever the mask and prompt symbol is set (in addition to
							// when the value is changed).  Because of this, we want to ignore
							// processing in these routines when we are initializing the control
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STARPAGE_H__3D27A6E1_0D80_11D5_9A63_0003472193C8__INCLUDED_)
