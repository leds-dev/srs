// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__A464C530_D502_11D4_800D_00609704053C__INCLUDED_)
#define AFX_STDAFX_H__A464C530_D502_11D4_800D_00609704053C__INCLUDED_

// Must be compiled for WinNT 4.0 / Windows 2000 and Unicode
// #define _WIN32_WINNT 0x400
#define _UNICODE
#define SECURITY_WIN32

// Must be compiled for WinNT 4.0 / Windows 2000 and Unicode
// sdm - beginning w/ VC++ 2008 valid values are 0x500=Win2k, 0x501=XP 
#define WINVER 0x0501
#define _WIN32_WINNT	0x0501

//#define WINVER				0x0400
//#if _MSC_VER >= 1300
//	#define _WIN32_WINNT	0x0400
//#endif

#ifdef _DEBUG
#pragma message ("Compiling for WinNT/2000, Unicode, Debug")
#else
#pragma message ("Compiling for WinNT/2000, Unicode, Release")
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxadv.h>
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#include <afxcview.h>
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxsock.h>		// MFC socket extensions

#include <windows.h>
#include <tlhelp32.h>
//Other library includes
// #include <BCGCBPro.h>			// BCG Control Bar
#include <BCGCBProInc.h>
#include <BCGPDockingControlBar.h>
#include <BCGPTabbedControlBar.h>
#include <BCGPSkinManager.h>
#include <BCGPDialog.h>
#include <BCGPWorkspace.h>
#include <BCGPBaseControlBar.h>
#include <BCGPMDIChildWnd.h>
#include <BCGPMDIFrameWnd.h>
#include <BCGPToolbarCustomize.h>
#include <BCGPMouseManager.h>
#include <BCGPContextMenuManager.h>
#include <BCGPKeyboardManager.h> 
#include <BCGPUserToolsManager.h>
#include <BCGPShellManager.h>
#include <BCGPTooltipManager.h>

#include <CodeMax.h>
#include <CMaxAfx.h>
#include <lm.h>
#include <EpCommon.h>
#include <math.h>

#include "OXUNC.h"
#include "OXEdit.h"
#include "OXMaskedEdit.h"
#include "Shlwapi.h"  //Need to link with Shlwapi.lib
#include "OutputBar.h"
#include "ResizableDialog.h"
#include "ResizablePage.h"
#include "ResizableSheet.h"
#include "Resource.h"
#include "Constants.h"
#include "SearchConstants.h"
#include "SchedMgrMultiDocTemplate.h"
#include "CmnHdr.h"
#include "atlbase.h"

BOOL CALLBACK AbortPrintDlgProc (HWND hWndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

#define WM_PROMPTRELOADDOC (WM_USER + 1)
#define WM_RELOADDOC (WM_USER + 2)

#define PUMP_MSG \
	{MSG msg; while (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE)) {DispatchMessage(&msg);}}

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__A464C530_D502_11D4_800D_00609704053C__INCLUDED_)
