/******************
////////////////////////////////////////////////////////
// WorkspaceBar.cpp : implementation of the 
//			CWorkspaceBar class
//
// (c) 2001 Frederick J. Shaw 
//	Prolog Updated            
////////////////////////////////////////////////////////
******************/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "MainFrm.h"
#include "WorkspaceBar.h"
#include "Parse.h"
#include "SchedMisc.h"
#include "TextFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar

BEGIN_MESSAGE_MAP(CWorkspaceBar, CBCGPDockingControlBar)
	//{{AFX_MSG_MAP(CWorkspaceBar)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CWorkspaceBar::CWorkspaceBar() 
//
//  Description :   Class constructor. 
//                  
//  Return :         
//					void	-	none.
//  Parameters : 
//				  	void	-	none. 
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceBar::CWorkspaceBar()
{
	m_pWorkspacePropertySheet = NULL;
	m_pdlgGenProp = NULL;
	m_pdlgDetailProp = NULL;
}

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CWorkspaceBar::~CWorkspaceBar() 
//
//  Description :   Class destructor. 
//                  
//  Return :         
//					void	-	none.
//  Parameters : 
//				  	void	-	none. 
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceBar::~CWorkspaceBar()
{
	if (m_pWorkspacePropertySheet != NULL)
	{
		delete m_pWorkspacePropertySheet;
		delete m_pdlgGenProp;
		delete m_pdlgDetailProp;
	}
}

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar message handlers
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CWorkspaceBar::OnCreate 
//
//  Description :   The framework calls this member function when an
//					application requests that the Windows window be created
//					by calling the Create or CreateEx member function. . 
//                  
//  Return :         
//					int 
//						OnCreate must return 0 to continue the creation of
//						the CWnd object. If the application returns -1, the
//						window will be destroyed.
//
//  Parameters : 
//				  	LPCREATESTRUCT lpCreateStruct 
//						Points to a CREATESTRUCT structure that contains 
//						information about the CWnd object being created.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
int CWorkspaceBar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CBCGPDockingControlBar::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect rectDummy;
	rectDummy.SetRectEmpty ();

	// Create tabs window:
	if (!m_wndTabs.Create (CBCGPTabWnd::STYLE_3D, rectDummy, this, 1))
	{
		TRACE0("Failed to create workspace tab window\n");
		return -1;      // fail to create
	}

	m_wndTabs.SetImageList (IDB_WORKSPACE, 16, RGB (255, 0, 255));

	// Create tree windows.
	// TODO: create your own tab windows here:
	const DWORD dwViewStyle =	WS_CHILD | WS_VISIBLE | TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS;
	
	m_wndMasterTreeCtrl.Create (dwViewStyle, rectDummy, &m_wndTabs, IDC_MASTER_TREE);
	m_wndDailyTreeCtrl.Create (dwViewStyle, rectDummy, &m_wndTabs, IDC_MASTER_TREE);
	// Attach tree windows to tab:

	m_wndTabs.AddTab (&m_wndMasterTreeCtrl, _T("Master View"), 0 /* Image number */);
	m_wndTabs.AddTab (&m_wndDailyTreeCtrl, _T("Daily View"), 1 /* Image number */);

	Refresh();

	return 0;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CWorkspaceBar::OnSize  
//
//  Description :   The framework calls this member function after the
//					window�s size has changed. 
//                  
//  Return :         
//					void	none. 
//
//  Parameters : 
//				  	UINT nType	-	Specifies the type of resizing requested.
//					int cx		-	Specifies the new width of the client area.
//					int cy		-	Specifies the new height of the client area.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceBar::OnSize(UINT nType, int cx, int cy) 
{
	CBCGPDockingControlBar::OnSize(nType, cx, cy);

	// Tab control should cover a whole client area:
	m_wndTabs.SetWindowPos (NULL, -1, -1, cx, cy,
		SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :		CWorkspaceBar::Refresh()
//
//  Description :   
//					 
//                  
//  Return :         
//					void	none. 
//
//  Parameters : 
//				  	void	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceBar::Refresh()
{
	m_wndMasterTreeCtrl.Init();
	m_wndDailyTreeCtrl.Init();
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :	CWorkspaceBar::OnContextMenu 
//
//  Description : Called by the framework when the user has clicked the 
//				  right mouse button (right clicked) in the window.   
//                  
//  Return :         
//					void	none. 
//
//  Parameters : 
//				  	CWnd* pWnd		-	Handle to the window in which the user
//										right clicked the mouse. This can be a
//										child window of the window receiving the
//										message. 
//					CPoint point	-	Position of the cursor, in screen 
//										coordinates, at the time of the mouse
//										click.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceBar::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	UINT uiMenuResId = 0;
	int iActiveTab = m_wndTabs.GetActiveTab ();
	if (iActiveTab < 0)
	{
		return;
	}
	
	CWorkspaceTreeCtrlEx* pWndTree = NULL;
	CWorkspaceTreeCtrlData* pData = NULL;

	switch (iActiveTab)
	{
		case 0: pWndTree = &m_wndMasterTreeCtrl;	break;
		case 1: pWndTree = &m_wndDailyTreeCtrl;		break;
	}
		
	pData = ((CWorkspaceTreeCtrlData*)pWndTree->GetItemData(pWndTree->GetSelectedItem()));
	if (pData->m_bFolder)
	{
		uiMenuResId = IDR_CONTEXT_WORKSPACE_MENU;
	}
	else
	{
		switch (pData->m_nType)
		{
			case nSTOL		:	uiMenuResId = IDR_CONTEXT_WORKSPACE_STOL_MENU;		break;
			case nCLS		:	uiMenuResId = IDR_CONTEXT_WORKSPACE_CLS_MENU;		break;
			case nRpt		:	uiMenuResId = IDR_CONTEXT_WORKSPACE_RPT_MENU;		break;
			case nSector	:	uiMenuResId = IDR_CONTEXT_WORKSPACE_SECTOR_MENU;	break;
			case nFrame		:	uiMenuResId = IDR_CONTEXT_WORKSPACE_FRAME_MENU;		break;
			case nStar		:	uiMenuResId = IDR_CONTEXT_WORKSPACE_STAR_MENU;		break;
			case nBinary	:   uiMenuResId = IDR_CONTEXT_WORKSPACE_BIN_MENU;	    break;
			case nSTOLMap	:   uiMenuResId = IDR_CONTEXT_WORKSPACE_MAP_MENU;	    break;
			case nIMC		:   uiMenuResId = IDR_CONTEXT_WORKSPACE_IMC_MENU;	    break;
			case nRTCSSet	:   uiMenuResId = IDR_CONTEXT_WORKSPACE_RTCSSET_MENU;   break;
			case nRTCSCmd	:   uiMenuResId = IDR_CONTEXT_WORKSPACE_RTCSCMD_MENU;   break;
			case nPaceCmd	:   uiMenuResId = IDR_CONTEXT_WORKSPACE_PACECMD_MENU;   break;
			case nLis		:   uiMenuResId = IDR_CONTEXT_WORKSPACE_LIS_MENU;	    break;
			case nRTCS		:   uiMenuResId = IDR_CONTEXT_WORKSPACE_RTCS_MENU;		break;
			case nSTOLCmd	:   uiMenuResId = IDR_CONTEXT_WORKSPACE_STOLCMD_MENU;   break;
			case nErr		:   uiMenuResId = IDR_CONTEXT_WORKSPACE_MENU;			break;
			default			:	uiMenuResId = IDR_CONTEXT_WORKSPACE_MENU;			break;
		}
	}

	ASSERT_VALID (pWndTree);

	if (point != CPoint (-1, -1))
	{
		//---------------------
		// Select clicked item:
		//---------------------
		CPoint ptTree = point;
		pWndTree->ScreenToClient (&ptTree);

		HTREEITEM hTreeItem = pWndTree->HitTest (ptTree);
		if (hTreeItem == NULL)
			uiMenuResId = IDR_CONTEXT_WORKSPACE_MENU;
	}

	if (uiMenuResId != 0)
	{
		theApp.ShowPopupMenu (uiMenuResId, point, AfxGetMainWnd());
		pWndTree->SetFocus ();
	}	
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :		CWorkspaceBar::PreTranslateMessage
//
//  Description :   Override this function to filter window messages before
//					they are dispatched to the Windows functions: 
//					TranslateMessage and DispatchMessage. 
//					 
//                  
//  Return :        BOOL	-
//					 Nonzero if the message was fully processed in 
//					 PreTranslateMessage and should not be processed further.
//					 Zero if the message should be processed in the normal
//					 way.
//
//  Parameters :	MSG* pMsg	-	 
//				  		A pointer to a MSG structure that contains the 
//						message to process.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CWorkspaceBar::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_KEYDOWN &&
		pMsg->wParam == VK_ESCAPE)
	{
		CMainFrame* pMainFrame = (CMainFrame*) AfxGetMainWnd ();
		ASSERT_VALID (pMainFrame);

		if (pMainFrame->GetActivePopup () == NULL)
		{
			pMainFrame->SetFocus ();
			return TRUE;
		}
	}

	return CBCGPDockingControlBar::PreTranslateMessage(pMsg);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :		CWorkspaceBar::UpdatePropertySheet()
//
//  Description :   
//					 
//                  
//  Return :         
//					void	none. 
//
//  Parameters : 
//				  	void	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceBar::UpdatePropertySheet()
{
	if (m_pWorkspacePropertySheet == NULL)
	{
		m_pWorkspacePropertySheet = new CWorkspacePropertySheet(_T("Properties"));
		m_pWorkspacePropertySheet->m_cImageList.Create(IDB_FILETYPES, 16, 0, RGB(255,0,255));
		m_pdlgGenProp = new CGeneralPropPage;
		m_pdlgDetailProp = new CDetailPropPage;
		m_pWorkspacePropertySheet->AddPage(m_pdlgGenProp);
		m_pWorkspacePropertySheet->AddPage(m_pdlgDetailProp);
		m_pWorkspacePropertySheet->Create(this, WS_POPUP|WS_CAPTION|WS_SYSMENU);
		m_pWorkspacePropertySheet->SetActivePage(1);
		m_pWorkspacePropertySheet->SetActivePage(0);
	}

	HTREEITEM hItem = m_pTreeCtrlInFocus->GetSelectedItem();
	CWorkspaceTreeCtrlData* pTreeData = (CWorkspaceTreeCtrlData*)m_pTreeCtrlInFocus->GetItemData(hItem);

	if (pTreeData->m_bFolder)
	{
		m_pWorkspacePropertySheet->SetIcon(m_pWorkspacePropertySheet->m_cImageList.ExtractIcon(3), FALSE);		
		m_pdlgGenProp->GetDlgItem(IDC_GENERALFILEPATH_STATIC)->SetWindowText(_T("Folder:"));
		m_pdlgGenProp->GetDlgItem(IDC_GENERALFILEPATH_EDIT)->SetWindowText(pTreeData->m_strFilePath);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALCREATIONDATE_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALCREATIONDATE_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALACCESSDATE_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALACCESSDATE_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALMODDATE_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALMODDATE_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALSIZE_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALSIZE_EDIT)->ShowWindow(SW_HIDE);

		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_STATIC)->ShowWindow(SW_HIDE);
		m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->ShowWindow(SW_HIDE);
	}
	else
	{
		CFileFind	finder;
		FILETIME	timeRef;
		CString		strCreationDate = _T("Unknown");
		CString		strAccessDate = _T("Unknown");
		CString		strModDate = _T("Unknown");
		CString		strSize = _T("Unknown");

		if (finder.FindFile(pTreeData->m_strFilePath))
		{
			finder.FindNextFile();
			finder.GetCreationTime(&timeRef);
			strCreationDate = CSchedMisc::ConvertFTtoString(timeRef);
			finder.GetLastAccessTime(&timeRef);
			strAccessDate = CSchedMisc::ConvertFTtoString(timeRef);
			finder.GetLastWriteTime(&timeRef);
			strModDate = CSchedMisc::ConvertFTtoString(timeRef);
			strSize.Format(_T("%d"), finder.GetLength());
		}

		m_pWorkspacePropertySheet->SetIcon(m_pWorkspacePropertySheet->m_cImageList.ExtractIcon(4+pTreeData->m_nType), FALSE);		
		m_pdlgGenProp->GetDlgItem(IDC_GENERALFILEPATH_STATIC)->SetWindowText(_T("Path:"));
		m_pdlgGenProp->GetDlgItem(IDC_GENERALFILEPATH_EDIT)->SetWindowText(pTreeData->m_strFilePath);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALCREATIONDATE_STATIC)->ShowWindow(SW_SHOW);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALCREATIONDATE_EDIT)->ShowWindow(SW_SHOW);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALCREATIONDATE_EDIT)->SetWindowText(strCreationDate);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALACCESSDATE_STATIC)->ShowWindow(SW_SHOW);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALACCESSDATE_EDIT)->ShowWindow(SW_SHOW);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALACCESSDATE_EDIT)->SetWindowText(strAccessDate);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALMODDATE_STATIC)->ShowWindow(SW_SHOW);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALMODDATE_EDIT)->ShowWindow(SW_SHOW);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALMODDATE_EDIT)->SetWindowText(strModDate);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALSIZE_STATIC)->ShowWindow(SW_SHOW);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALSIZE_EDIT)->ShowWindow(SW_SHOW);
		m_pdlgGenProp->GetDlgItem(IDC_GENERALSIZE_EDIT)->SetWindowText(strSize);

		switch (pTreeData->m_nType)
		{
			case nSTOL:
			case nCLS:
			case nRTCS:
			{
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_STATIC)->SetWindowText(_T("Description:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_STATIC)->SetWindowText(_T("Validation:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_STATIC)->SetWindowText(_T("Execute Time:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->ShowWindow(SW_SHOW);
				CTextFile cTextFile;
				CFileException	cFileException;
				// BOOL bDataOK = FALSE;
				CString			strDesc;
				CString			strExectime;
				CString			strValid;
				BOOL bDataOK = cTextFile.Load(pTreeData->m_strFilePath, CTextFile::PR, &cFileException);
				if (bDataOK)
				{
					CStringArray*	pstrDesc = cTextFile.GetProlog()->GetDescription()->GetTextArray();

					TCHAR cr = 13;
					TCHAR lf = 10;
					
					for (int nCnt=0; nCnt<pstrDesc->GetSize(); nCnt++)
					{
						if (strDesc.IsEmpty())
							strDesc = pstrDesc->GetAt(nCnt);
						else
							strDesc = strDesc + cr + lf + pstrDesc->GetAt(nCnt);				
					}
					strExectime = cTextFile.GetProlog()->GetExectime()->GetTextString();
					strValid = cTextFile.GetProlog()->GetValid()->GetText();
				}
				cTextFile.Close(&cFileException);
				if (bDataOK)
				{
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->SetWindowText(strDesc);
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->SetWindowText(strValid);
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->SetWindowText(strExectime);
				}
				else
				{
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->SetWindowText(_T("Not Available"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->SetWindowText(_T("Not Available"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->SetWindowText(_T("Not Available"));
				}
				
				break;
			}
			case nFrame:
			{
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->SetWindowText(_T("Version:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->SetWindowText(_T("Number of Frames:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->SetWindowText(_T("SRSO Starts At:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->SetWindowText(_T("Last Modified On:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->SetWindowText(_T("Last Modified By:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_STATIC)->SetWindowText(_T("Last Updated On:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_STATIC)->SetWindowText(_T("Last Updated By:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->ShowWindow(SW_HIDE);
				BOOL		bDataOK = FALSE;
				CParseFrame cParseFrame;
				CStdioFile	cStdioFile;
				if(cStdioFile.Open(pTreeData->m_strFilePath, CFile::modeRead | CFile::typeText))
				{
					CString strLine1;
					CString strLine2;
					bDataOK = cStdioFile.ReadString(strLine1);
					if (bDataOK)
					{
						bDataOK = cStdioFile.ReadString(strLine2);
						if (bDataOK)
						{
							cParseFrame.SetHdr(strLine1, strLine2);
							bDataOK = cParseFrame.ParseHdr();
						}
					}
				}
				cStdioFile.Close();
				if (bDataOK)
				{
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->SetWindowText(cParseFrame.GetHdrVersion());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->SetWindowText(cParseFrame.GetHdrNumObj());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->SetWindowText(cParseFrame.GetHdrSRSOFirst());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->SetWindowText(cParseFrame.GetHdrModTimeFmt());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->SetWindowText(cParseFrame.GetHdrModUser());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->SetWindowText(cParseFrame.GetHdrUpdTimeFmt());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->SetWindowText(cParseFrame.GetHdrUpdUser());
				}
				else
				{
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->SetWindowText(_T("Undefined"));
				}
				break;
			}
			case nRpt:
			case nLis:
			{
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->ShowWindow(SW_HIDE);
				break;
			}
			case nSector:
			{
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->SetWindowText(_T("Version:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->SetWindowText(_T("Number of Sectors:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->SetWindowText(_T("Last Modified On:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->SetWindowText(_T("Last Modified By:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->ShowWindow(SW_HIDE);
				BOOL		bDataOK = FALSE;
				CParseSector cParseSector;
				CStdioFile	cStdioFile;
				if(cStdioFile.Open(pTreeData->m_strFilePath, CFile::modeRead | CFile::typeText))
				{
					CString strLine1;
					CString strLine2;
					bDataOK = cStdioFile.ReadString(strLine1);
					if (bDataOK)
					{
						bDataOK = cStdioFile.ReadString(strLine2);
						if (bDataOK)
						{
							cParseSector.SetHdr(strLine1, strLine2);
							bDataOK = cParseSector.ParseHdr();
						}
					}
				}
				cStdioFile.Close();
				if (bDataOK)
				{
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->SetWindowText(cParseSector.GetHdrVersion());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->SetWindowText(cParseSector.GetHdrNumObj());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->SetWindowText(cParseSector.GetHdrModTimeFmt());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->SetWindowText(cParseSector.GetHdrModUser());
				}
				else
				{
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->SetWindowText(_T("Undefined"));
				}
				break;
			}
			case nStar:
			{
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->SetWindowText(_T("Version:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->SetWindowText(_T("Number of Stars:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->SetWindowText(_T("Last Modified On:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->SetWindowText(_T("Last Modified By:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->ShowWindow(SW_HIDE);
				BOOL		bDataOK = FALSE;
				CParseStar cParseStar;
				CStdioFile	cStdioFile;
				if(cStdioFile.Open(pTreeData->m_strFilePath, CFile::modeRead | CFile::typeText))
				{
					CString strLine1;
					CString strLine2;
					bDataOK = cStdioFile.ReadString(strLine1);
					if (bDataOK)
					{
						bDataOK = cStdioFile.ReadString(strLine2);
						if (bDataOK)
						{
							cParseStar.SetHdr(strLine1, strLine2);
							bDataOK = cParseStar.ParseHdr();
						}
					}
				}
				cStdioFile.Close();
				if (bDataOK)
				{
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->SetWindowText(cParseStar.GetHdrVersion());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->SetWindowText(cParseStar.GetHdrNumObj());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->SetWindowText(cParseStar.GetHdrModTimeFmt());
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->SetWindowText(cParseStar.GetHdrModUser());
				}
				else
				{
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->SetWindowText(_T("Undefined"));
					m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->SetWindowText(_T("Undefined"));
				}
				break;
			}
			case nSTOLMap:
			case nRTCSMap:
			{
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->SetWindowText(_T("Map Type:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->SetWindowText(_T("Version:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->SetWindowText(_T("Number of Lines:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->SetWindowText(_T("Generation Time:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->SetWindowText(_T("Generation User:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->ShowWindow(SW_HIDE);
	
				CTextFile	cMapFile;
				CFileException cFE;
				CString strType		= _T("Unknown");
				CString strVersion	= _T("Unknown");
				CString strNumObj	= _T("Unknown");
				CString strModTime	= _T("Unknown");
				CString strModUser	= _T("Unknown");
				
				if (cMapFile.Load(pTreeData->m_strFilePath, CTextFile::RO, &cFE))
				{	
					if (pTreeData->m_nType == nSTOLMap)
					{
						CParseSTOLMap	cParseMap;
						if ((cParseMap.m_nNumObj = cMapFile.GetLineCount()) > 1)
						{
							cParseMap.SetHdr(cMapFile.GetTextAt(0), cMapFile.GetTextAt(1));
							if (cParseMap.ParseHdr())
							{
								strType = cParseMap.m_strType;
								strVersion =  cParseMap.GetHdrVersion();
								strNumObj = cParseMap.GetHdrNumObj();
								strModTime = cParseMap.GetHdrModTimeFmt();
								strModUser = cParseMap.GetHdrModUser();
							}
						}
					}
					else if (pTreeData->m_nType == nRTCSMap)
					{
						CParseRTCSMap	cParseMap;
						if ((cParseMap.m_nNumObj = cMapFile.GetLineCount()) > 1)
						{
							cParseMap.SetHdr(cMapFile.GetTextAt(0), cMapFile.GetTextAt(1));
							if (cParseMap.ParseHdr())
							{
								strType = cParseMap.m_strType;
								strVersion =  cParseMap.GetHdrVersion();
								strNumObj = cParseMap.GetHdrNumObj();
								strModTime = cParseMap.GetHdrModTimeFmt();
								strModUser = cParseMap.GetHdrModUser();
							}
						}
					}
					cMapFile.Close(&cFE);
				}

				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->SetWindowText(strType);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->SetWindowText(strVersion);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->SetWindowText(strNumObj);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->SetWindowText(strModTime);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->SetWindowText(strModUser);

				break;
			}
			case nBinary:
			{
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->SetWindowText(_T("Load Type:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->SetWindowText(_T("IMC Type:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->SetWindowText(_T("CTCU ID:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->SetWindowText(_T("RT Address:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->SetWindowText(_T("Number of CMD Words:"));
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->ShowWindow(SW_SHOW);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->ShowWindow(SW_HIDE);

				CFile	cBinFile;
				CFileException e;
				CString strLoadType		= _T("Unknown");
				CString strIMCType		= _T("Unknown");
				CString strCTCUID		= _T("Unknown");
				CString strRTAddress	= _T("Unknown");
				CString strNumCmdWords	= _T("Unknown");
				struct sBinHeaderStruct
				{
					byte m_nLoadType;
					byte m_nIMCType;
					byte m_nCTCUID;
					byte m_nRTAddress;
					long m_nNumCmdWords;
				} sBinHeader;
				
				if (cBinFile.Open(pTreeData->m_strFilePath, CFile::modeRead, &e))
				{
					UINT nHeaderSize = sizeof (sBinHeader);
					if (cBinFile.Read(&sBinHeader, nHeaderSize) == nHeaderSize)
					{
						switch (sBinHeader.m_nLoadType)
						{
							case 1	: strLoadType = _T("RAM AE Load");					break;
							case 2	: strLoadType = _T("RAM AF Load");					break;
							case 3	: strLoadType = _T("RAM Torque Load");				break;
							case 4	: strLoadType = _T("IMC Set Load");					break;
							case 5	: strLoadType = _T("Stored Command Buffer Load");	break;
							case 6	: strLoadType = _T("Schedule Buffer Load");			break;
							case 7	: strLoadType = _T("Star Table Load");				break;
							case 8	: strLoadType = _T("Frame Table Load");				break;
							case 9	: strLoadType = _T("RTCE Buffer Load");				break;
						}
						switch (sBinHeader.m_nIMCType)
						{
							case 1	: strIMCType = _T("Full Set (24 Hour)");			break;
							case 2	: strIMCType = _T("New Orbit Reference Set");		break;
							case 3	: strIMCType = _T("SSAA, Coupled Imgr/Sndr");		break;
							case 4	: strIMCType = _T("SSAA, Uncoupled");				break;
							case 5	: strIMCType = _T("Post-SK Full Set");				break;
							case 6	: strIMCType = _T("Short Span O&A Adjust");			break;
						}
						switch (sBinHeader.m_nCTCUID)
						{
							case 1	: strCTCUID = _T("CTCU 1");							break;
							case 2	: strCTCUID = _T("CTCU 2");							break;
						}
						switch (sBinHeader.m_nRTAddress)
						{
							case 4	: strRTAddress = _T("Ace 1");						break;
							case 5	: strRTAddress = _T("Ace 2");						break;
						}
					}
					strNumCmdWords.Format(_T("%d"), ntohl(sBinHeader.m_nNumCmdWords));
					cBinFile.Close();
				}

				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->SetWindowText(strLoadType);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->SetWindowText(strIMCType);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->SetWindowText(strCTCUID);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->SetWindowText(strRTAddress);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->SetWindowText(strNumCmdWords);

				break;
			}
			default:
			{
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL01_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL02_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL03_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL04_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL05_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL06_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL07_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL11_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL12_EDIT)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_STATIC)->ShowWindow(SW_HIDE);
				m_pdlgDetailProp->GetDlgItem(IDC_DETAIL13_EDIT)->ShowWindow(SW_HIDE);
				break;
			}
		}
	}

	m_pWorkspacePropertySheet->m_nPreviousType = pTreeData->m_nType;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :	CWorkspaceBar::ShowPropertySheet()
//
//  Description :   
//					 
//                  
//  Return :         
//					void	none. 
//
//  Parameters : 
//				  	void	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceBar::ShowPropertySheet()
{
	m_pWorkspacePropertySheet->ShowWindow(SW_SHOW);
	m_pWorkspacePropertySheet->m_bOnScreen = TRUE;
}	
