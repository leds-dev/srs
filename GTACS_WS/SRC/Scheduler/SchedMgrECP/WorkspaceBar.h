// WorkspaceBar.h : interface of the CWorkspaceBar class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_WORKSPACEBAR_H__A464C53B_D502_11D4_800D_00609704053C__INCLUDED_)
#define AFX_WORKSPACEBAR_H__A464C53B_D502_11D4_800D_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "WorkspaceTreeCtrl.h"
#include "WorkspaceProperties.h"

class CWorkspaceBar : public CBCGPDockingControlBar
{
public:
	CWorkspaceBar();
	CWorkspaceTreeCtrlMaster	m_wndMasterTreeCtrl;
	CWorkspaceTreeCtrlDaily		m_wndDailyTreeCtrl;
	CTreeCtrl*					m_pTreeCtrlInFocus;
	void UpdatePropertySheet();
	void ShowPropertySheet();
	void Refresh();
	CWorkspacePropertySheet*	GetPropertySheet(){return m_pWorkspacePropertySheet;};
// Attributes
protected:
	CBCGPTabWnd					m_wndTabs;
	CWorkspacePropertySheet*	m_pWorkspacePropertySheet;
	CGeneralPropPage*			m_pdlgGenProp;
	CDetailPropPage*			m_pdlgDetailProp;
// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWorkspaceBar)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL
// Implementation
public:
	virtual ~CWorkspaceBar();
// Generated message map functions
protected:
	//{{AFX_MSG(CWorkspaceBar)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_WORKSPACEBAR_H__A464C53B_D502_11D4_800D_00609704053C__INCLUDED_)
