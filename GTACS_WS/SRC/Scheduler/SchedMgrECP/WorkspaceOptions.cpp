/******************
////////////////////////////////////////////////////////
// WorkspaceOptions.cpp : implementation of the
//			CWorkspaceOptions class
//
// (c) 2001 Frederick J. Shaw
//	Prolog Updated
// Revision History:													   
// Manan Dalal - July-August 2014										   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   
// Release in Build16.2
////////////////////////////////////////////////////////
******************/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "WorkspaceOptions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

BEGIN_MESSAGE_MAP(CWorkspaceOptionsSheet, CResizableSheet)
	//{{AFX_MSG_MAP(CWorkspaceOptionsSheet)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

IMPLEMENT_DYNAMIC(CWorkspaceOptionsSheet, CResizableSheet)

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceOptionsSheet::CWorkspaceOptionsSheet
//
//  Description :   Class constructor.
//
//  Return :        void	-	none.
//
//  Parameters :
//				  	UINT nIDCaption	-	ID of the name to be placed in the
//						tab for this page. If 0, the name will be taken
//						from the dialog template for this page. By default,
//						0.
//
//					CWnd* pParentWnd	-	Specifies the button control�s
//						parent window, usually a CDialog. It must not be
//						NULL.
//
//					UINT iSelectPage	-	The index of the page that will
//						initially be on top. Default is the first page added
//						to the sheet.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceOptionsSheet::CWorkspaceOptionsSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CResizableSheet(nIDCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_dlgSCProp);
	// AddPage(&m_dlgGTACSProp);
	AddPage(&m_dlgOATSProp);
	AddPage(&m_dlgRSOProp);
	AddPage(&m_dlgFolderProp);
	AddPage(&m_dlgMiscProp);
	AddPage(&m_dlgInstObjProp);
	AddPage(&m_dlgMapObjProp);
	m_bReadOnly = FALSE;
}

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceOptionsSheet::CWorkspaceOptionsSheet
//
//  Description :   Class constructor.
//
//  Return :        void	-	none.
//
//  Parameters :
//				  	UINT nIDCaption	-	ID of the name to be placed in the
//						tab for this page. If 0, the name will be taken
//						from the dialog template for this page. By default,
//						0.
//
//					CWnd* pParentWnd	-	Specifies the button control�s
//						parent window, usually a CDialog. It must not be
//						NULL.
//
//					UINT iSelectPage	-	The index of the page that will
//						initially be on top. Default is the first page added
//						to the sheet.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceOptionsSheet::CWorkspaceOptionsSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CResizableSheet(pszCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_dlgSCProp);
//	AddPage(&m_dlgGTACSProp);
	AddPage(&m_dlgOATSProp);
	AddPage(&m_dlgRSOProp);
	AddPage(&m_dlgFolderProp);
	AddPage(&m_dlgMiscProp);
	AddPage(&m_dlgInstObjProp);
	AddPage(&m_dlgMapObjProp);
	m_bReadOnly = FALSE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceOptionsSheet::~CWorkspaceOptionsSheet
//
//  Description :   Class destructor.
//
//  Return :        void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceOptionsSheet::~CWorkspaceOptionsSheet()
{
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceOptionsSheet::OnInitDialog()
//
//  Description :  This member function is called in response to the
//				   WM_INITDIALOG message.
//
//  Return :        BOOL
//					Specifies whether the application has set the input
//					focus to one of the controls in the dialog box.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CWorkspaceOptionsSheet::OnInitDialog()
{
	CResizableSheet::OnInitDialog();
	m_bRefresh = FALSE;
	m_strOldDBDrive = GetAppRegMisc()->m_strDBDrive;

	CImageList imageList;
	imageList.Create(IDR_MAINFRAME, 16, 0, RGB (192,192,192));
	SetIcon(imageList.ExtractIcon(10), FALSE);

	if (m_bReadOnly)
	{
		CString strTitle;
		GetWindowText(strTitle);
		GetDlgItem(IDOK)->EnableWindow(false);
		strTitle = strTitle + _T(" - Read only");
		SetWindowText(strTitle);
	}

	EnableSaveRestore(_T("Resizable Sheets"), _T("Workspace Options"), FALSE);
	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceOptionsSheet::Apply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceOptionsSheet::Apply()
{
	m_dlgSCProp.Apply();
	// m_dlgGTACSProp.Apply();
	m_dlgOATSProp.Apply();
	m_dlgRSOProp.Apply();
	m_dlgFolderProp.Apply();
	m_dlgMiscProp.Apply();
	m_dlgInstObjProp.Apply();
	m_dlgMapObjProp.Apply();
}

const int nGTACSPropAbbrev			= 0;
const int nGTACSPropProcShare		= 1;
const int nGTACSPropDBShare			= 2;
const int nGTACSPropTitle			= 3;

BEGIN_MESSAGE_MAP(CGTACSPropListCtrl, CEditListCtrl)
	//{{AFX_MSG_MAP(CGTACSPropListCtrl)
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropListCtrl::CGTACSPropListCtrl()
//
//  Description :  Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CGTACSPropListCtrl::CGTACSPropListCtrl(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropListCtrl::~CGTACSPropListCtrl()
//
//  Description :  Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CGTACSPropListCtrl::~CGTACSPropListCtrl(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :    void CGTACSPropListCtrl::OnLButtonDown
//
//  Description :  The framework calls this member function when the user
//				   releases the left mouse button.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					UINT nFlags	 -	Indicates whether various virtual
//									keys are down.
//					CPoint point -	Specifies the x- and y-coordinate of
//									the cursor.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CGTACSPropListCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	int index;
	CEditListCtrl::OnLButtonDown(nFlags, point);

	int colnum;

	if( (index = HitTestEx(point, &colnum)) != -1 )
	{
		UINT flag = LVIS_FOCUSED;

		if( (GetItemState(index, flag) & flag) == flag && colnum > 0 )
		{
			if( !m_bReadOnly )
			{
				// Add check for LVS_EDITLABELS
				if( GetWindowLong(m_hWnd, GWL_STYLE) & LVS_EDITLABELS )
					EditSubLabel(index, colnum);
			}
			else
				SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		}
		else
			SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	}
}

BEGIN_MESSAGE_MAP(CGTACSPropPage, CResizablePage)
	//{{AFX_MSG_MAP(CGTACSPropPage)
	ON_BN_CLICKED(IDC_SOCCGTACS_RADIO, OnPropChange)
	ON_BN_CLICKED(IDC_WALLOPSGTACS_RADIO, OnPropChange)
	ON_BN_CLICKED(IDC_GODDARDGTACS_RADIO, OnPropChange)
	ON_BN_CLICKED(IDC_SECGTACS_RADIO, OnPropChange)
	ON_BN_CLICKED(IDC_FAIRBANKSGTACS_RADIO, OnPropChange)
	ON_CBN_SELCHANGE(IDC_GODDARDGTACS_COMBO, OnPropChange)
	ON_CBN_SELCHANGE(IDC_SOCCGTACS_COMBO, OnPropChange)
	ON_CBN_SELCHANGE(IDC_WALLOPSGTACS_COMBO, OnPropChange)
	ON_CBN_SELCHANGE(IDC_SECGTACS_COMBO, OnPropChange)
	ON_CBN_SELCHANGE(IDC_FAIRBANKSGTACS_COMBO, OnPropChange)
	ON_CBN_SELCHANGE(IDC_GTACSSEL_COMBO, OnSelChangeGTACSSelCombo)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_GTACSPROP_LIST, OnEndLabelEditBasePropList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

IMPLEMENT_DYNCREATE(CGTACSPropPage, CResizablePage)

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::CGTACSPropPage()
//
//  Description :  Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CGTACSPropPage::CGTACSPropPage() : CResizablePage(CGTACSPropPage::IDD)
{
	//{{AFX_DATA_INIT(CGTACSPropPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::~CGTACSPropPage()
//
//  Description :   Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CGTACSPropPage::~CGTACSPropPage(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::DoDataExchange
//
//  Description :  This method is called by the framework to exchange and
//				   validate dialog data.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CGTACSPropPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGTACSPropPage)
	DDX_Control(pDX, IDC_GTACSPROP_LIST, m_cGTACSPropList);
	//}}AFX_DATA_MAP
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CGTACSPropPage::OnInitDialog()
//
//  Description :  This member function is called in response to the
//				   WM_INITDIALOG message.
//
//  Return :
//					BOOL	-	Specifies whether the application has set
//								the input focus to one of the controls in
//								the dialog box.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CGTACSPropPage::OnInitDialog()
{
	CResizablePage::OnInitDialog();

//	int nGTACS = 0;
//	int nSite = 0;

	for(int nGTACS=nMaxGTACS[nSOCC]-1; nGTACS>=0; nGTACS-- )
		((CComboBox*)GetDlgItem(IDC_SOCCGTACS_COMBO))->InsertString(0, GetAppRegGTACS()->m_strTitle[nSOCC][nGTACS]);

	((CComboBox*)GetDlgItem(IDC_SOCCGTACS_COMBO))->SetCurSel(GetAppRegGTACS()->m_nDefault[nSOCC]);
	GetDlgItem(IDC_SOCCGTACS_RADIO)->SetWindowText(GetAppRegSite()->m_strTitle[nSOCC]);

	for(int nGTACS=nMaxGTACS[nWallops]-1; nGTACS>=0; nGTACS-- )
		((CComboBox*)GetDlgItem(IDC_WALLOPSGTACS_COMBO))->InsertString(0, GetAppRegGTACS()->m_strTitle[nWallops][nGTACS]);

	((CComboBox*)GetDlgItem(IDC_WALLOPSGTACS_COMBO))->SetCurSel(GetAppRegGTACS()->m_nDefault[nWallops]);
	GetDlgItem(IDC_WALLOPSGTACS_RADIO)->SetWindowText(GetAppRegSite()->m_strTitle[nWallops]);

	for(int nGTACS=nMaxGTACS[nGoddard]-1; nGTACS>=0; nGTACS-- )
		((CComboBox*)GetDlgItem(IDC_GODDARDGTACS_COMBO))->InsertString(0, GetAppRegGTACS()->m_strTitle[nGoddard][nGTACS]);

	((CComboBox*)GetDlgItem(IDC_GODDARDGTACS_COMBO))->SetCurSel(GetAppRegGTACS()->m_nDefault[nGoddard]);
	GetDlgItem(IDC_GODDARDGTACS_RADIO)->SetWindowText(GetAppRegSite()->m_strTitle[nGoddard]);

	for(int nGTACS=nMaxGTACS[nSEC]-1; nGTACS>=0; nGTACS-- )
		((CComboBox*)GetDlgItem(IDC_SECGTACS_COMBO))->InsertString(0, GetAppRegGTACS()->m_strTitle[nSEC][nGTACS]);

	((CComboBox*)GetDlgItem(IDC_SECGTACS_COMBO))->SetCurSel(GetAppRegGTACS()->m_nDefault[nSEC]);
	GetDlgItem(IDC_SECGTACS_RADIO)->SetWindowText(GetAppRegSite()->m_strTitle[nSEC]);

	for(int nGTACS=nMaxGTACS[nFairbanks]-1; nGTACS>=0; nGTACS-- )
		((CComboBox*)GetDlgItem(IDC_FAIRBANKSGTACS_COMBO))->InsertString(0, GetAppRegGTACS()->m_strTitle[nFairbanks][nGTACS]);

	((CComboBox*)GetDlgItem(IDC_FAIRBANKSGTACS_COMBO))->SetCurSel(GetAppRegGTACS()->m_nDefault[nFairbanks]);
	GetDlgItem(IDC_FAIRBANKSGTACS_RADIO)->SetWindowText(GetAppRegSite()->m_strTitle[nFairbanks]);

	switch( GetAppRegSite()->m_nDefaultGTACS )
	{
		case nSOCC:			((CButton*)GetDlgItem(IDC_SOCCGTACS_RADIO))->SetCheck(TRUE);		break;
		case nWallops:		((CButton*)GetDlgItem(IDC_WALLOPSGTACS_RADIO))->SetCheck(TRUE);		break;
		case nGoddard:		((CButton*)GetDlgItem(IDC_GODDARDGTACS_RADIO))->SetCheck(TRUE);		break;
		case nSEC:			((CButton*)GetDlgItem(IDC_SECGTACS_RADIO))->SetCheck(TRUE);			break;
		case nFairbanks:	((CButton*)GetDlgItem(IDC_FAIRBANKSGTACS_RADIO))->SetCheck(TRUE);	break;
		default:			((CButton*)GetDlgItem(IDC_SOCCGTACS_RADIO))->SetCheck(TRUE);		break;
	}

	OnPropChange();

	m_nCurSite = nSOCC;

	for(int nSite=nMaxSites-1; nSite>=0; nSite-- )
		((CComboBox*)GetDlgItem(IDC_GTACSSEL_COMBO))->InsertString(0, GetAppRegSite()->m_strTitle[nSite]);

	((CComboBox*)GetDlgItem(IDC_GTACSSEL_COMBO))->SetCurSel(m_nCurSite);

	for(int nSite=0; nSite<nMaxSites; nSite++ )
	{
		for(int nGTACS=0; nGTACS<nMaxGTACS[nSite]; nGTACS++ )
		{
			m_strAbbrev[nSite][nGTACS] = GetAppRegGTACS()->m_strAbbrev[nSite][nGTACS];
			m_strProcShare[nSite][nGTACS] = GetAppRegGTACS()->m_strProcShare[nSite][nGTACS];
			m_strDBShare[nSite][nGTACS] = GetAppRegGTACS()->m_strDBShare[nSite][nGTACS];
			m_strTitle[nSite][nGTACS] = GetAppRegGTACS()->m_strTitle[nSite][nGTACS];
		}
	}

	if( ((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
	{
		GetDlgItem(IDC_SOCCGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_SOCCGTACS_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_WALLOPSGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_WALLOPSGTACS_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_GODDARDGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_GODDARDGTACS_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_SECGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_SECGTACS_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_FAIRBANKSGTACS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_FAIRBANKSGTACS_RADIO)->EnableWindow(FALSE);
	}

	int nSite = ((CComboBox*)GetDlgItem(IDC_GTACSSEL_COMBO))->GetCurSel();

	if( (*GetAppAdminMode()) && (!((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly) )
		GetDlgItem(IDC_GTACS_STATIC)->SetWindowText(GetAppRegSite()->m_strTitle[nSite] + _T(" GTACS Properties"));
	else
		GetDlgItem(IDC_GTACS_STATIC)->SetWindowText(GetAppRegSite()->m_strTitle[nSite] + _T(" GTACS Properties (Read only)"));

	m_cGTACSPropList.InsertColumn(0, _T("Function"), LVCFMT_LEFT, 100);

	for(int nGTACS=0; nGTACS<nMaxGTACS[nSite]; nGTACS++ )
		m_cGTACSPropList.InsertColumn(nGTACS+1, GetAppRegGTACS()->m_strTitle[nSite][nGTACS], LVCFMT_LEFT, 100);

	m_cGTACSPropList.InsertItem(nGTACSPropAbbrev,_T("Server"));
	m_cGTACSPropList.InsertItem(nGTACSPropProcShare,_T("Proc Share Name"));
	m_cGTACSPropList.InsertItem(nGTACSPropDBShare,_T("DB Share Name"));
	m_cGTACSPropList.InsertItem(nGTACSPropTitle,_T("Title"));

	for(int nGTACS=0; nGTACS<nMaxGTACS[nSite]; nGTACS++ )
	{
		m_cGTACSPropList.SetItemText(nGTACSPropAbbrev, nGTACS+1, m_strAbbrev[nSite][nGTACS]);
		m_cGTACSPropList.SetItemText(nGTACSPropProcShare, nGTACS+1, m_strProcShare[nSite][nGTACS]);
		m_cGTACSPropList.SetItemText(nGTACSPropDBShare, nGTACS+1, m_strDBShare[nSite][nGTACS]);
		m_cGTACSPropList.SetItemText(nGTACSPropTitle, nGTACS+1, m_strTitle[nSite][nGTACS]);
	}

	AddAnchor(IDC_GTACSDEFAULT_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_GTACS_STATIC, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_GTACSPROP_LIST, TOP_LEFT, BOTTOM_RIGHT);

	if( (((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly) || (!*GetAppAdminPriv()) )
		m_cGTACSPropList.SetReadOnly();

	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::OnEndLabelEditBasePropList
//
//  Description :  The framework calls this function when the property
//				   page�s list control is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					NMHDR* pNMHDR		-	Pointer to a NMHDR object.
//					LRESULT* pResult	-	Pointer to a LRESULT object.
//											Always set to 0.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CGTACSPropPage::OnEndLabelEditBasePropList(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO* pDispInfo	= (LV_DISPINFO*)pNMHDR;
	CString	strError;
	CString strEntry		= pDispInfo->item.pszText;
//	BOOL	bValid			= TRUE;

	switch( pDispInfo->item.iItem )
	{
		case (nGTACSPropAbbrev):	strError = CheckAbbrev(pDispInfo);			break;
		case (nGTACSPropProcShare):	strError = CheckProcShare(pDispInfo);		break;
		case (nGTACSPropDBShare):	strError = CheckDBShare(pDispInfo);			break;
		case (nGTACSPropTitle):		strError = CheckTitle(pDispInfo);			break;
		default:					strError.Empty();							break;
	}

	if( strError.IsEmpty() )
	{
		m_cGTACSPropList.OnEndLabelEdit(pNMHDR, pResult);

		if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
			SetModified(TRUE);
	}
	else
		GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);

	*pResult = 0;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CGTACSPropPage::CheckAbbrev
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString	-	Error message
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CGTACSPropPage::CheckAbbrev(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Abbreviation (qualifier) value can not be empty.");

	if( strError.IsEmpty() && (strEntry.Find(_T(' '), 0) != -1) )
		strError = _T("Abbreviation (qualifier) value can not contain any spaces.");

	if( strError.IsEmpty() && (strEntry.Find(_T('\\'), 0) != -1) )
		strError = _T("Abbreviation (qualifier) value can not contain any \\ characters.");

	if( strError.IsEmpty() )
		((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::CheckProcShare
//
//  Description :  Checks if the entry is valid.  Returns an error
//					message if it is invalid.
//
//  Return :
//					CString -	Error message
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CGTACSPropPage::CheckProcShare(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Proc Share value can not be empty.");

	if( strError.IsEmpty() && (strEntry.Find(_T(' '), 0) != -1) )
		strError = _T("Proc Share value can not contain any spaces.");

	if( strError.IsEmpty() && (strEntry.Find(_T('\\'), 0) != -1) )
		strError = _T("Proc Share value can not contain any \\ characters.");

	if( strError.IsEmpty() )
		((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::CheckDBShare
//
//
//  Description :  Checks if the entry is valid.  Returns an error
//					message if it is invalid.
//
//  Return :
//					CString -	Error message
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CGTACSPropPage::CheckDBShare(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("DB Share value can not be empty.");

	if( strError.IsEmpty() && (strEntry.Find(_T(' '), 0) != -1) )
		strError = _T("DB Share value can not contain any spaces.");

	if( strError.IsEmpty() && (strEntry.Find(_T('\\'), 0) != -1) )
		strError = _T("DB Share value can not contain any \\ characters.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::CheckTitle
//
//  Description :  Checks if the entry is valid.  Returns an error
//					message if it is invalid.
//
//  Return :
//					CString		-
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CGTACSPropPage::CheckTitle(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Title value can not be empty.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::OnPropChange()
//
//  Description :  The framework calls this function when the property
//				   page�s controls are changed.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CGTACSPropPage::OnPropChange()
{
	if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
	{
		GetDlgItem(IDC_SOCCGTACS_COMBO)->EnableWindow(((CButton*)GetDlgItem(IDC_SOCCGTACS_RADIO))->GetCheck());
		GetDlgItem(IDC_WALLOPSGTACS_COMBO)->EnableWindow(((CButton*)GetDlgItem(IDC_WALLOPSGTACS_RADIO))->GetCheck());
		GetDlgItem(IDC_GODDARDGTACS_COMBO)->EnableWindow(((CButton*)GetDlgItem(IDC_GODDARDGTACS_RADIO))->GetCheck());
		GetDlgItem(IDC_SECGTACS_COMBO)->EnableWindow(((CButton*)GetDlgItem(IDC_SECGTACS_RADIO))->GetCheck());
		GetDlgItem(IDC_FAIRBANKSGTACS_COMBO)->EnableWindow(((CButton*)GetDlgItem(IDC_FAIRBANKSGTACS_RADIO))->GetCheck());
		SetModified(TRUE);
	}
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::OnApply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					BOOL
//						Always returns TRUE.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CGTACSPropPage::OnApply()
{
	// TODO::
	// Need static variable to eliminate multiple displays of same window
	// AfxMessageBox(_T("Changing GTACS server requires that all Epoch client applications be closed!"), MB_OK);
	((CWorkspaceOptionsSheet*)GetParent())->Apply();
	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::Apply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CGTACSPropPage::Apply()
{
	if( m_hWnd )
	{
		bool bChange = false;  // If default GTACS server was changed, need to remap R: drive 
		//((CSchedMgrECPApp*)AfxGetApp())->m_NewGTACS = FALSE; // Default to server not changed.

		int nOldGTACS	= GetAppRegGTACS()->m_nDefault[nSOCC];
		GetAppRegGTACS()->m_nDefault[nSOCC]	= ((CComboBox*)GetDlgItem(IDC_SOCCGTACS_COMBO))->GetCurSel();

		if( nOldGTACS != GetAppRegGTACS()->m_nDefault[nSOCC]){
			((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;
			bChange = true;
		}

		nOldGTACS = GetAppRegGTACS()->m_nDefault[nWallops];
		GetAppRegGTACS()->m_nDefault[nWallops]	= ((CComboBox*)GetDlgItem(IDC_WALLOPSGTACS_COMBO))->GetCurSel();

		if( nOldGTACS != GetAppRegGTACS()->m_nDefault[nWallops]){
			((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;
			bChange = true;
		}

		nOldGTACS = GetAppRegGTACS()->m_nDefault[nGoddard];
		GetAppRegGTACS()->m_nDefault[nGoddard]	= ((CComboBox*)GetDlgItem(IDC_GODDARDGTACS_COMBO))->GetCurSel();

		if( nOldGTACS != GetAppRegGTACS()->m_nDefault[nGoddard]){
			((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;
			bChange = true;
		}

		nOldGTACS = GetAppRegGTACS()->m_nDefault[nSEC];
		GetAppRegGTACS()->m_nDefault[nSEC]	= ((CComboBox*)GetDlgItem(IDC_SECGTACS_COMBO))->GetCurSel();

		if( nOldGTACS != GetAppRegGTACS()->m_nDefault[nSEC]){
			((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;
			bChange = true;
		}

		nOldGTACS = GetAppRegGTACS()->m_nDefault[nFairbanks];
		GetAppRegGTACS()->m_nDefault[nFairbanks] = ((CComboBox*)GetDlgItem(IDC_FAIRBANKSGTACS_COMBO))->GetCurSel();

		if( nOldGTACS != GetAppRegGTACS()->m_nDefault[nFairbanks]){
			((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;
			bChange = true;
		}

		int nOldSite = GetAppRegSite()->m_nDefaultGTACS;

		if( ((CButton*)GetDlgItem(IDC_SOCCGTACS_RADIO))->GetCheck()){
			GetAppRegSite()->m_nDefaultGTACS = nSOCC;
		}else if( ((CButton*)GetDlgItem(IDC_WALLOPSGTACS_RADIO))->GetCheck()){
			GetAppRegSite()->m_nDefaultGTACS = nWallops;
		}else if( ((CButton*)GetDlgItem(IDC_GODDARDGTACS_RADIO))->GetCheck()){
			GetAppRegSite()->m_nDefaultGTACS = nGoddard;
		}else if( ((CButton*)GetDlgItem(IDC_SECGTACS_RADIO))->GetCheck()){
			GetAppRegSite()->m_nDefaultGTACS = nSEC;
		}else if( ((CButton*)GetDlgItem(IDC_FAIRBANKSGTACS_RADIO))->GetCheck()){
			GetAppRegSite()->m_nDefaultGTACS = nFairbanks;
		}

		if( nOldSite != GetAppRegSite()->m_nDefaultGTACS){
			((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;
			bChange = true;
		}

		// int nGTACS;
		// int nSite = ((CComboBox*)GetDlgItem(IDC_GTACSSEL_COMBO))->GetCurSel();
		for(int nGTACS=0; nGTACS<nMaxGTACS[m_nCurSite]; nGTACS++ ){
			m_strAbbrev[m_nCurSite][nGTACS]		= m_cGTACSPropList.GetItemText(nGTACSPropAbbrev, nGTACS+1);
			m_strProcShare[m_nCurSite][nGTACS]	= m_cGTACSPropList.GetItemText(nGTACSPropProcShare, nGTACS+1);
			m_strDBShare[m_nCurSite][nGTACS]	= m_cGTACSPropList.GetItemText(nGTACSPropDBShare, nGTACS+1);
			m_strTitle[m_nCurSite][nGTACS]		= m_cGTACSPropList.GetItemText(nGTACSPropTitle, nGTACS+1);
		}

		for(int nSite=0; nSite<nMaxSites; nSite++ )	{
			for(int nGTACS=0; nGTACS<nMaxGTACS[nSite]; nGTACS++ )
			{
				GetAppRegGTACS()->m_strAbbrev[nSite][nGTACS]	= m_strAbbrev[nSite][nGTACS];
				GetAppRegGTACS()->m_strProcShare[nSite][nGTACS]	= m_strProcShare[nSite][nGTACS];
				GetAppRegGTACS()->m_strDBShare[nSite][nGTACS]	= m_strDBShare[nSite][nGTACS];
				GetAppRegGTACS()->m_strTitle[nSite][nGTACS]		= m_strTitle[nSite][nGTACS];
			}
		}

		GetAppRegGTACS()->SetValues();
		GetAppRegSite()->SetValues();

		if (bChange){
		//	AfxMessageBox(_T("Close all epoch applications before continuing!"), MB_OK);
			// TODO:: stop all services
			((CSchedMgrECPApp*)AfxGetApp())->m_NewGTACS = TRUE;
		}
	}
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGTACSPropPage::OnSelChangeGTACSSelCombo()
//
//  Description :  The framework calls this function when the property
//				   page�s combo box is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CGTACSPropPage::OnSelChangeGTACSSelCombo()
{
	int nGTACS;
	int nSite = ((CComboBox*)GetDlgItem(IDC_GTACSSEL_COMBO))->GetCurSel();

	if( *GetAppAdminMode() )
		GetDlgItem(IDC_GTACS_STATIC)->SetWindowText(GetAppRegSite()->m_strTitle[nSite] + _T(" GTACS Properties"));
	else
		GetDlgItem(IDC_GTACS_STATIC)->SetWindowText(GetAppRegSite()->m_strTitle[nSite] + _T(" GTACS Properties (Read only)"));

	for( nGTACS=0; nGTACS<nMaxGTACS[m_nCurSite]; nGTACS++ )
	{
		m_strAbbrev[m_nCurSite][nGTACS]		= m_cGTACSPropList.GetItemText(nGTACSPropAbbrev, nGTACS+1);
		m_strProcShare[m_nCurSite][nGTACS]	= m_cGTACSPropList.GetItemText(nGTACSPropProcShare, nGTACS+1);
		m_strDBShare[m_nCurSite][nGTACS]	= m_cGTACSPropList.GetItemText(nGTACSPropDBShare, nGTACS+1);
		m_strTitle[m_nCurSite][nGTACS]		= m_cGTACSPropList.GetItemText(nGTACSPropTitle, nGTACS+1);
	}

	m_cGTACSPropList.DeleteAllItems();

	for( int nCol=0; nCol<=nMaxGTACS[m_nCurSite]; nCol++ )
		m_cGTACSPropList.DeleteColumn(0);

	m_nCurSite = nSite;

	m_cGTACSPropList.InsertColumn(0, _T("Function"), LVCFMT_LEFT, 100);

	for( nGTACS=0; nGTACS<nMaxGTACS[nSite]; nGTACS++ )
		m_cGTACSPropList.InsertColumn(nGTACS+1, GetAppRegGTACS()->m_strTitle[nSite][nGTACS], LVCFMT_LEFT, 100);

	m_cGTACSPropList.InsertItem(nGTACSPropAbbrev,_T("Qualifier"));
	m_cGTACSPropList.InsertItem(nGTACSPropProcShare,_T("Proc Share Name"));
	m_cGTACSPropList.InsertItem(nGTACSPropDBShare,_T("DB Share Name"));
	m_cGTACSPropList.InsertItem(nGTACSPropTitle,_T("Title"));

	for( nGTACS=0; nGTACS<nMaxGTACS[nSite]; nGTACS++ )
	{
		m_cGTACSPropList.SetItemText(nGTACSPropAbbrev, nGTACS+1, m_strAbbrev[nSite][nGTACS]);
		m_cGTACSPropList.SetItemText(nGTACSPropProcShare, nGTACS+1, m_strProcShare[nSite][nGTACS]);
		m_cGTACSPropList.SetItemText(nGTACSPropDBShare, nGTACS+1, m_strDBShare[nSite][nGTACS]);
		m_cGTACSPropList.SetItemText(nGTACSPropTitle, nGTACS+1, m_strTitle[nSite][nGTACS]);
	}
}
/////////////////////////////////////////////////////////////////////////////
// COATSPropPage property page

BEGIN_MESSAGE_MAP(COATSPropPage, CResizablePage)
	//{{AFX_MSG_MAP(COATSPropPage)
	ON_CBN_SELCHANGE(IDC_SOCCOATS_COMBO, OnPropsChange)
	ON_CBN_SELCHANGE(IDC_WALLOPSOATS_COMBO, OnPropsChange)
	ON_CBN_SELCHANGE(IDC_GODDARDOATS_COMBO, OnPropsChange)
	ON_BN_CLICKED(IDC_GODDARDOATS_RADIO, OnPropsChange)
	ON_BN_CLICKED(IDC_SOCCOATS_RADIO, OnPropsChange)
	ON_BN_CLICKED(IDC_WALLOPSOATS_RADIO, OnPropsChange)
	ON_EN_CHANGE(IDC_OATSPORT_EDIT, OnPropsChange)
	ON_EN_CHANGE(IDC_OATSTIMEOUT_EDIT, OnPropsChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

IMPLEMENT_DYNCREATE(COATSPropPage, CResizablePage)

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     COATSPropPage::COATSPropPage()
//
//  Description :    Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
COATSPropPage::COATSPropPage() : CResizablePage(COATSPropPage::IDD)
{
	//{{AFX_DATA_INIT(COATSPropPage)
	m_nOATSTimeoutEdit = 0;
	m_nOATSPortEdit = 0;
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     COATSPropPage::~COATSPropPage()
//
//  Description :  Destructor
//
//  Return :
//				   void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
COATSPropPage::~COATSPropPage()
{
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     COATSPropPage::DoDataExchange
//
//  Description :  This method is called by the framework to exchange and
//				   validate dialog data.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void COATSPropPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COATSPropPage)
	DDX_Text(pDX, IDC_OATSTIMEOUT_EDIT, m_nOATSTimeoutEdit);
	DDV_MinMaxInt(pDX, m_nOATSTimeoutEdit, 1, 600);
	DDX_Text(pDX, IDC_OATSPORT_EDIT, m_nOATSPortEdit);
	DDV_MinMaxInt(pDX, m_nOATSPortEdit, 1000, 9999);
	//}}AFX_DATA_MAP
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     COATSPropPage::OnInitDialog()
//
//  Description :  This member function is called in response to the
//				   WM_INITDIALOG message.
//
//  Return :
//				BOOL	-	Specifies whether the application has set
//								the input focus to one of the controls in
//								the dialog box.
//  Parameters :
//				void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL COATSPropPage::OnInitDialog()
{
	CResizablePage::OnInitDialog();

	int nOATS;

	for( nOATS=nMaxOATS[nSOCC]-1; nOATS>=0; nOATS-- )
		((CComboBox*)GetDlgItem(IDC_SOCCOATS_COMBO))->InsertString(0, GetAppRegOATS()->m_strTitle[nSOCC][nOATS]);

	((CComboBox*)GetDlgItem(IDC_SOCCOATS_COMBO))->SetCurSel(GetAppRegOATS()->m_nDefault[nSOCC]);
	GetDlgItem(IDC_SOCCOATS_RADIO)->SetWindowText(GetAppRegSite()->m_strTitle[nSOCC]);

	for( nOATS=nMaxOATS[nWallops]-1; nOATS>=0; nOATS-- )
		((CComboBox*)GetDlgItem(IDC_WALLOPSOATS_COMBO))->InsertString(0, GetAppRegOATS()->m_strTitle[nWallops][nOATS]);

	((CComboBox*)GetDlgItem(IDC_WALLOPSOATS_COMBO))->SetCurSel(GetAppRegOATS()->m_nDefault[nWallops]);
	GetDlgItem(IDC_WALLOPSOATS_RADIO)->SetWindowText(GetAppRegSite()->m_strTitle[nWallops]);

	for( nOATS=nMaxOATS[nGoddard]-1; nOATS>=0; nOATS-- )
		((CComboBox*)GetDlgItem(IDC_GODDARDOATS_COMBO))->InsertString(0, GetAppRegOATS()->m_strTitle[nGoddard][nOATS]);

	((CComboBox*)GetDlgItem(IDC_GODDARDOATS_COMBO))->SetCurSel(GetAppRegOATS()->m_nDefault[nGoddard]);
	GetDlgItem(IDC_GODDARDOATS_RADIO)->SetWindowText(GetAppRegSite()->m_strTitle[nGoddard]);

	switch( GetAppRegSite()->m_nDefaultOATS )
	{
		case nSOCC:		((CButton*)GetDlgItem(IDC_SOCCOATS_RADIO))->SetCheck(TRUE);	break;
		case nWallops:	((CButton*)GetDlgItem(IDC_WALLOPSOATS_RADIO))->SetCheck(TRUE);	break;
		case nGoddard:	((CButton*)GetDlgItem(IDC_GODDARDOATS_RADIO))->SetCheck(TRUE);	break;
		default:		((CButton*)GetDlgItem(IDC_SOCCOATS_RADIO))->SetCheck(TRUE);	break;
	}

	OnPropsChange();

	m_nOATSPortEdit		= GetAppRegOATS()->m_nPort;
	m_nOATSTimeoutEdit	= GetAppRegOATS()->m_nTimeout;

	if( ((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
	{
		GetDlgItem(IDC_SOCCOATS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_SOCCOATS_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_WALLOPSOATS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_WALLOPSOATS_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_GODDARDOATS_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_GODDARDOATS_RADIO)->EnableWindow(FALSE);
		GetDlgItem(IDC_OATSPORT_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_OATSTIMEOUT_EDIT)->EnableWindow(FALSE);
	}

	if( (*GetAppAdminMode()) && (!((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly) )
	{
		GetDlgItem(IDC_OATSPORT_EDIT)->EnableWindow(TRUE);
		GetDlgItem(IDC_OATSTIMEOUT_EDIT)->EnableWindow(TRUE);
		GetDlgItem(IDC_OATS_STATIC)->SetWindowText(_T("OATS Properties"));
	}
	else
	{
		GetDlgItem(IDC_OATS_STATIC)->SetWindowText(_T("OATS Properties (Read only)"));
		GetDlgItem(IDC_OATSPORT_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_OATSTIMEOUT_EDIT)->EnableWindow(FALSE);
	}

	AddAnchor(IDC_OATSDEFAULT_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_OATS_STATIC, TOP_LEFT, TOP_RIGHT);
	UpdateData(FALSE);

	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     COATSPropPage::OnPropsChange()
//
//  Description :  The framework calls this function when the property
//				   page�s controls are changed.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void COATSPropPage::OnPropsChange()
{
	if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
	{
		GetDlgItem(IDC_SOCCOATS_COMBO)->EnableWindow(((CButton*)GetDlgItem(IDC_SOCCOATS_RADIO))->GetCheck());
		GetDlgItem(IDC_WALLOPSOATS_COMBO)->EnableWindow(((CButton*)GetDlgItem(IDC_WALLOPSOATS_RADIO))->GetCheck());
		GetDlgItem(IDC_GODDARDOATS_COMBO)->EnableWindow(((CButton*)GetDlgItem(IDC_GODDARDOATS_RADIO))->GetCheck());
		SetModified(TRUE);
	}
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     COATSPropPage::OnApply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :
//					BOOL
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL COATSPropPage::OnApply()
{
	((CWorkspaceOptionsSheet*)GetParent())->Apply();
	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     COATSPropPage::Apply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void COATSPropPage::Apply()
{
	if( m_hWnd )
	{
		GetAppRegOATS()->m_nDefault[nSOCC]		= ((CComboBox*)GetDlgItem(IDC_SOCCOATS_COMBO))->GetCurSel();
		GetAppRegOATS()->m_nDefault[nWallops]	= ((CComboBox*)GetDlgItem(IDC_WALLOPSOATS_COMBO))->GetCurSel();
		GetAppRegOATS()->m_nDefault[nGoddard]	= ((CComboBox*)GetDlgItem(IDC_GODDARDOATS_COMBO))->GetCurSel();

		if( ((CButton*)GetDlgItem(IDC_SOCCOATS_RADIO))->GetCheck() )
			GetAppRegSite()->m_nDefaultOATS = nSOCC;
		else if( ((CButton*)GetDlgItem(IDC_WALLOPSOATS_RADIO))->GetCheck() )
			GetAppRegSite()->m_nDefaultOATS = nWallops;
		else if( ((CButton*)GetDlgItem(IDC_GODDARDOATS_RADIO))->GetCheck() )
			GetAppRegSite()->m_nDefaultOATS = nGoddard;

		GetAppRegOATS()->m_nPort	= m_nOATSPortEdit;
		GetAppRegOATS()->m_nTimeout = m_nOATSTimeoutEdit;
		GetAppRegOATS()->SetValues();
	}
}

/////////////////////////////////////////////////////////////////////////////
// CRSOPropPage property page

BEGIN_MESSAGE_MAP(CRSOPropPage, CResizablePage)
	//{{AFX_MSG_MAP(COATSPropPage)
	ON_CBN_SELCHANGE(IDC_RSO_SCID_COMBO, OnSelChangeSCSelCombo)
	ON_EN_UPDATE(IDC_RSO_FRM_TABLE_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_CURRSO_LABEL_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_NEWRSO_LABEL_EDIT1, OnPropsChange)
	ON_EN_UPDATE(IDC_NEWRSO_LABEL_EDIT2, OnPropsChange)
	ON_EN_UPDATE(IDC_NEWRSO_LABEL_EDIT3, OnPropsChange)
	ON_EN_UPDATE(IDC_NEWRSO_LABEL_EDIT4, OnPropsChange)
	ON_EN_UPDATE(IDC_NEWRSO_LABEL_EDIT5, OnPropsChange)
	ON_EN_UPDATE(IDC_NEWRSO_LABEL_EDIT6, OnPropsChange)
	ON_EN_UPDATE(IDC_NEWRSO_LABEL_EDIT7, OnPropsChange)
	ON_EN_UPDATE(IDC_NEWRSO_LABEL_EDIT8, OnPropsChange)
	ON_EN_UPDATE(IDC_NEWRSO_LABEL_EDIT9, OnPropsChange)
	ON_EN_UPDATE(IDC_NEWRSO_LABEL_EDIT10, OnPropsChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

IMPLEMENT_DYNCREATE(CRSOPropPage, CResizablePage)

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CRSOPropPage::COATSPropPage()
//
//  Description :    Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CRSOPropPage::CRSOPropPage() : CResizablePage(CRSOPropPage::IDD)
{
	//{{AFX_DATA_INIT(COATSPropPage)
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CRSOPropPage::~COATSPropPage()
//
//  Description :  Destructor
//
//  Return :
//				   void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CRSOPropPage::~CRSOPropPage()
{
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CRSOPropPage::DoDataExchange
//
//  Description :  This method is called by the framework to exchange and
//				   validate dialog data.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CRSOPropPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRSOPropPage)
	DDX_Text(pDX, IDC_RSO_FRM_TABLE_EDIT, m_cRSOFrmTableName);
	DDX_Text(pDX, IDC_CURRSO_LABEL_EDIT, m_cRSOCurRSOName);
	DDX_Text(pDX, IDC_NEWRSO_LABEL_EDIT1, m_cRSONewRSOName1);
	DDX_Text(pDX, IDC_NEWRSO_LABEL_EDIT2, m_cRSONewRSOName2);
	DDX_Text(pDX, IDC_NEWRSO_LABEL_EDIT3, m_cRSONewRSOName3);
	DDX_Text(pDX, IDC_NEWRSO_LABEL_EDIT4, m_cRSONewRSOName4);
	DDX_Text(pDX, IDC_NEWRSO_LABEL_EDIT5, m_cRSONewRSOName5);
	DDX_Text(pDX, IDC_NEWRSO_LABEL_EDIT6, m_cRSONewRSOName6);
	DDX_Text(pDX, IDC_NEWRSO_LABEL_EDIT7, m_cRSONewRSOName7);
	DDX_Text(pDX, IDC_NEWRSO_LABEL_EDIT8, m_cRSONewRSOName8);
	DDX_Text(pDX, IDC_NEWRSO_LABEL_EDIT9, m_cRSONewRSOName9);
	DDX_Text(pDX, IDC_NEWRSO_LABEL_EDIT10, m_cRSONewRSOName10);
	//}}AFX_DATA_MAP
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CRSOPropPage::OnInitDialog()
//
//  Description :  This member function is called in response to the
//				   WM_INITDIALOG message.
//
//  Return :
//				BOOL	-	Specifies whether the application has set
//								the input focus to one of the controls in
//								the dialog box.
//  Parameters :
//				void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CRSOPropPage::OnInitDialog()
{
	CResizablePage::OnInitDialog();

	int nSCID;
	for( nSCID=nMaxSC-1; nSCID>=0; nSCID-- )
	{
		((CComboBox*)GetDlgItem(IDC_RSO_SCID_COMBO))->InsertString(0, GetAppRegRSO()->m_strTitle[nSCID]);
	
		//Rest of the data
		m_strRSOFrmTableName[nSCID] = GetAppRegRSO()->m_strFrmTable[nSCID];
		m_strRSOCurRSOName[nSCID] = GetAppRegRSO()->m_strCurRSOName[nSCID];
		m_strRSONewRSOName1[nSCID] = GetAppRegRSO()->m_strNewRSOName1[nSCID];
		m_strRSONewRSOName2[nSCID] = GetAppRegRSO()->m_strNewRSOName2[nSCID];
		m_strRSONewRSOName3[nSCID] = GetAppRegRSO()->m_strNewRSOName3[nSCID];
		m_strRSONewRSOName4[nSCID] = GetAppRegRSO()->m_strNewRSOName4[nSCID];
		m_strRSONewRSOName5[nSCID] = GetAppRegRSO()->m_strNewRSOName5[nSCID];
		m_strRSONewRSOName6[nSCID] = GetAppRegRSO()->m_strNewRSOName6[nSCID];
		m_strRSONewRSOName7[nSCID] = GetAppRegRSO()->m_strNewRSOName7[nSCID];
		m_strRSONewRSOName8[nSCID] = GetAppRegRSO()->m_strNewRSOName8[nSCID];
		m_strRSONewRSOName9[nSCID] = GetAppRegRSO()->m_strNewRSOName9[nSCID];
		m_strRSONewRSOName10[nSCID] = GetAppRegRSO()->m_strNewRSOName10[nSCID];
	}

	((CComboBox*)GetDlgItem(IDC_RSO_SCID_COMBO))->SetCurSel(GetAppRegSite()->m_nDefaultSC);

	m_nCurSCID=nSCID   = ((CComboBox*)GetDlgItem(IDC_RSO_SCID_COMBO))->GetCurSel();
	m_cRSOFrmTableName = GetAppRegRSO()->m_strFrmTable[m_nCurSCID];
	m_cRSOCurRSOName   = GetAppRegRSO()->m_strCurRSOName[m_nCurSCID];
	m_cRSONewRSOName1  = GetAppRegRSO()->m_strNewRSOName1[m_nCurSCID];
	m_cRSONewRSOName2  = GetAppRegRSO()->m_strNewRSOName2[m_nCurSCID];
	m_cRSONewRSOName3  = GetAppRegRSO()->m_strNewRSOName3[m_nCurSCID];
	m_cRSONewRSOName4  = GetAppRegRSO()->m_strNewRSOName4[m_nCurSCID];
	m_cRSONewRSOName5  = GetAppRegRSO()->m_strNewRSOName5[m_nCurSCID];
	m_cRSONewRSOName6  = GetAppRegRSO()->m_strNewRSOName6[m_nCurSCID];
	m_cRSONewRSOName7  = GetAppRegRSO()->m_strNewRSOName7[m_nCurSCID];
	m_cRSONewRSOName8  = GetAppRegRSO()->m_strNewRSOName8[m_nCurSCID];
	m_cRSONewRSOName9  = GetAppRegRSO()->m_strNewRSOName9[m_nCurSCID];
	m_cRSONewRSOName10 = GetAppRegRSO()->m_strNewRSOName10[m_nCurSCID];

	UpdateData(FALSE);

	if( !(*GetAppAdminMode()) || (((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly) )
	{
		GetDlgItem(IDC_RSO_FRM_TABLE_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_CURRSO_LABEL_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT1)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT2)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT3)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT4)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT5)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT6)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT7)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT8)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT9)->EnableWindow(FALSE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT10)->EnableWindow(FALSE);
	}
	else
	{
		GetDlgItem(IDC_RSO_FRM_TABLE_EDIT)->EnableWindow(TRUE);
		GetDlgItem(IDC_CURRSO_LABEL_EDIT)->EnableWindow(TRUE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT1)->EnableWindow(TRUE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT2)->EnableWindow(TRUE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT3)->EnableWindow(TRUE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT4)->EnableWindow(TRUE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT5)->EnableWindow(TRUE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT6)->EnableWindow(TRUE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT7)->EnableWindow(TRUE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT8)->EnableWindow(TRUE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT9)->EnableWindow(TRUE);
		GetDlgItem(IDC_NEWRSO_LABEL_EDIT10)->EnableWindow(TRUE);
	}
	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CRSOPropPage::OnPropsChange()
//
//  Description :  The framework calls this function when the property
//				   page�s controls are changed.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CRSOPropPage::OnPropsChange()
{
	if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
	{
		SetModified(TRUE);
	}
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CRSOPropPage::OnSelChangeSCSelCombo()
//
//  Description :  The framework calls this function when the property
//				   page�s list control is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CRSOPropPage::OnSelChangeSCSelCombo()
{
	UpdateData(TRUE);

	if( m_nCurSCID != -1 )
	{
		m_strRSOFrmTableName[m_nCurSCID] = m_cRSOFrmTableName;
		m_strRSOCurRSOName[m_nCurSCID] = m_cRSOCurRSOName;
		m_strRSONewRSOName1[m_nCurSCID] = m_cRSONewRSOName1;
		m_strRSONewRSOName2[m_nCurSCID] = m_cRSONewRSOName2;
		m_strRSONewRSOName3[m_nCurSCID] = m_cRSONewRSOName3;
		m_strRSONewRSOName4[m_nCurSCID] = m_cRSONewRSOName4;
		m_strRSONewRSOName5[m_nCurSCID] = m_cRSONewRSOName5;
		m_strRSONewRSOName6[m_nCurSCID] = m_cRSONewRSOName6;
		m_strRSONewRSOName7[m_nCurSCID] = m_cRSONewRSOName7;
		m_strRSONewRSOName8[m_nCurSCID] = m_cRSONewRSOName8;
		m_strRSONewRSOName9[m_nCurSCID] = m_cRSONewRSOName9;
		m_strRSONewRSOName10[m_nCurSCID] = m_cRSONewRSOName10;
	}
	//Get the new SCID Index
	m_nCurSCID = ((CComboBox*)GetDlgItem(IDC_RSO_SCID_COMBO))->GetCurSel();
	m_cRSOFrmTableName=m_strRSOFrmTableName[m_nCurSCID];
	m_cRSOCurRSOName=m_strRSOCurRSOName[m_nCurSCID];
	m_cRSONewRSOName1=m_strRSONewRSOName1[m_nCurSCID];
	m_cRSONewRSOName2=m_strRSONewRSOName2[m_nCurSCID];
	m_cRSONewRSOName3=m_strRSONewRSOName3[m_nCurSCID];
	m_cRSONewRSOName4=m_strRSONewRSOName4[m_nCurSCID];
	m_cRSONewRSOName5=m_strRSONewRSOName5[m_nCurSCID];
	m_cRSONewRSOName6=m_strRSONewRSOName6[m_nCurSCID];
	m_cRSONewRSOName7=m_strRSONewRSOName7[m_nCurSCID];
	m_cRSONewRSOName8=m_strRSONewRSOName8[m_nCurSCID];
	m_cRSONewRSOName9=m_strRSONewRSOName9[m_nCurSCID];
	m_cRSONewRSOName10=m_strRSONewRSOName10[m_nCurSCID];

	UpdateData(FALSE);

}
/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CRSOPropPage::OnApply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :
//					BOOL
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CRSOPropPage::OnApply()
{
	((CWorkspaceOptionsSheet*)GetParent())->Apply();
	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CRSOPropPage::Apply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CRSOPropPage::Apply()
{
	if( m_hWnd )
	{
		OnSelChangeSCSelCombo();

		for (int nSCID = 0; nSCID < nMaxSC; nSCID++)
		{
			GetAppRegRSO()->m_strFrmTable[nSCID] = m_strRSOFrmTableName[nSCID];
			GetAppRegRSO()->m_strCurRSOName[nSCID] = m_strRSOCurRSOName[nSCID];
			GetAppRegRSO()->m_strNewRSOName1[nSCID] = m_strRSONewRSOName1[nSCID];
			GetAppRegRSO()->m_strNewRSOName2[nSCID] = m_strRSONewRSOName2[nSCID];
			GetAppRegRSO()->m_strNewRSOName3[nSCID] = m_strRSONewRSOName3[nSCID];
			GetAppRegRSO()->m_strNewRSOName4[nSCID] = m_strRSONewRSOName4[nSCID];
			GetAppRegRSO()->m_strNewRSOName5[nSCID] = m_strRSONewRSOName5[nSCID];
			GetAppRegRSO()->m_strNewRSOName6[nSCID] = m_strRSONewRSOName6[nSCID];
			GetAppRegRSO()->m_strNewRSOName7[nSCID] = m_strRSONewRSOName7[nSCID];
			GetAppRegRSO()->m_strNewRSOName8[nSCID] = m_strRSONewRSOName8[nSCID];
			GetAppRegRSO()->m_strNewRSOName9[nSCID] = m_strRSONewRSOName9[nSCID];
			GetAppRegRSO()->m_strNewRSOName10[nSCID] = m_strRSONewRSOName10[nSCID];
		}

		GetAppRegRSO()->SetValues();
	}
}

const int nSCPropDirectory				= 0;
const int nSCPropAbbrev					= 1;
const int nSCPropOATSID					= 2;
const int nSCPropFrameSize				= 3;
const int nSCPropFrameSRSOStart			= 4;
const int nSCPropStarSize				= 5;
const int nSCPropStoredBufferSize		= 6;
const int nSCPropRTCSSize				= 7;
const int nSCPropRTCSNum				= 8;
const int nSCPropSchedBufferSize		= 9;
const int nSCPropSchedBufferSegSize		= 10;
const int nSCPropCTCUID					= 11;
const int nSCPropRTAddress				= 12;
const int nSCPropDatabaseName			= 13;
const int nSCPropMaxCmdGap				= 14;
const int nSCPropPreHKGap				= 15;
const int nSCPropRetryCnt				= 16;
const int nSCPropSTOGotoTopCmd			= 17;
const int nSCPropRTCSCmdFile			= 18;
const int nSCPropPaceCmdFile			= 19;
const int nSCPropFrameMiniSchedFile		= 20;
const int nSCPropFrameIOMiniSchedFile	= 21;
const int nSCPropStarMiniSchedFile		= 22;
const int nSCPropStarIOMiniSchedFile	= 23;
const int nSCPropSFCSMiniSchedFile		= 24;
const int nIntrusionRpt					= 25;

const int nInstrCenterPtImgrNS		    = 26;
const int nInstrCenterPtImgrEW		    = 27;
const int nInstrCenterPtSdrNS 		    = 28;
const int nInstrCenterPtSdrEW		    = 29;

const int nSpaceLookSdrEast		        = 30;
const int nSpaceLookSdrWest		        = 31;
const int nSpaceLookImgrEast		    = 32;
const int nSpaceLookImgrWest		    = 33;

const int nScanDegsNSNadirImgr 		    = 34;
const int nScanDegsEWNadirImgr		    = 35;
const int nScanDegsNSNadirSdr 		    = 36;
const int nScanDegsEWNadirSdr 		    = 37;

const int nStarDegsNSNadirImgr 		    = 38;
const int nStarDegsEWNadirImgr 		    = 39;
const int nStarDegsNSNadirSdr		    = 40;
const int nStarDegsEWNadirSdr		    = 41;


const int nTimeTagSpacing		        = 42;
//const int nCmdExeTime                 = 43;

const int nNumCmdFrameFile              = 43;
const int nCmdFrameUplinkRate           = 44;
const int nSTOLCmdRestrctFile           = 45;
const int nSRSOSpacelookMode            = 46;
const int nSRSOFrameLabel	            = 47;


BEGIN_MESSAGE_MAP(CSCPropListCtrl, CEditListCtrl)
	//{{AFX_MSG_MAP(CSCPropListCtrl)
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropListCtrl::CSCPropListCtrl()
//
//  Description :  Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CSCPropListCtrl::CSCPropListCtrl(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropListCtrl::~CSCPropListCtrl()
//
//  Description :  Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CSCPropListCtrl::~CSCPropListCtrl(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     void CSCPropListCtrl::OnLButtonDown
//
//  Description :  The framework calls this member function when the user
//				   releases the left mouse button.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					UINT nFlags	 -	Indicates whether various virtual
//									keys are down.
//					CPoint point -	Specifies the x- and y-coordinate of
//									the cursor.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CSCPropListCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	int index;
	CEditListCtrl::OnLButtonDown(nFlags, point);

	int colnum;
	if( (index = HitTestEx(point, &colnum)) != -1 )
	{
		UINT flag = LVIS_FOCUSED;

		if( (GetItemState(index, flag) & flag) == flag && colnum > 0 )
		{
			if( !m_bReadOnly )
			{
				// Add check for LVS_EDITLABELS
				if( GetWindowLong(m_hWnd, GWL_STYLE) & LVS_EDITLABELS )
				{
					if( index == nSCPropCTCUID+1 ) // Only row two
					{
						CStringList lstItems;
						lstItems.AddTail("ACE_1");
						lstItems.AddTail("ACE_2");
						CString strItem = GetItemText(index, colnum);

						int			nTopItem	= -1;
						bool		bFound		= false;
						POSITION	pos			= lstItems.GetHeadPosition();

						while( (pos!=NULL)&&(!bFound) )
						{
							nTopItem++;

							if( lstItems.GetAt(pos) == strItem )
								bFound = true;
							else
								lstItems.GetNext(pos);
						}

						if( !bFound ) nTopItem = 0;
						ShowInPlaceList(index, colnum, lstItems, nTopItem);
					}
					else
						EditSubLabel(index, colnum);
				}
			}
			else
				SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		}
		else
			SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CSCPropPage property page

BEGIN_MESSAGE_MAP(CSCPropPage, CResizablePage)
	//{{AFX_MSG_MAP(CSCPropPage)
	ON_CBN_SELCHANGE(IDC_SCID_COMBO, OnPropChange)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_SCPROP_LIST, OnEndLabelEditBasePropList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

IMPLEMENT_DYNCREATE(CSCPropPage, CResizablePage)

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CSCPropPage() :
//
//  Description :  Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CSCPropPage::CSCPropPage() : CResizablePage(CSCPropPage::IDD)
{
	//{{AFX_DATA_INIT(CSCPropPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::~CSCPropPage()
//
//  Description :  Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CSCPropPage::~CSCPropPage(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::DoDataExchange
//
//  Description :  This method is called by the framework to exchange and
//				   validate dialog data.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CSCPropPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSCPropPage)
	DDX_Control(pDX, IDC_SCPROP_LIST, m_cSCPropList);
	//}}AFX_DATA_MAP
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     BOOL CSCPropPage::OnInitDialog()
//
//  Description :  This member function is called in response to the
//				   WM_INITDIALOG message.
//
//  Return :
//					BOOL	-	Specifies whether the application has set
//								the input focus to one of the controls in
//								the dialog box.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CSCPropPage::OnInitDialog()
{
	CResizablePage::OnInitDialog();

	int nSCID;

	for( nSCID=nMaxSC-1; nSCID>=0; nSCID-- )
		((CComboBox*)GetDlgItem(IDC_SCID_COMBO))->InsertString(0, GetAppRegSC()->m_strTitle[nSCID]);

	((CComboBox*)GetDlgItem(IDC_SCID_COMBO))->SetCurSel(GetAppRegSite()->m_nDefaultSC);

	m_cSCPropList.InsertColumn(0, _T("Function"), LVCFMT_LEFT, 150);

	for( nSCID=0; nSCID<nMaxSC; nSCID++ )
		m_cSCPropList.InsertColumn(nSCID+1, GetAppRegSC()->m_strTitle[nSCID], LVCFMT_LEFT, 65);

	m_cSCPropList.InsertItem(nSCPropDirectory, _T("Directory"));
	m_cSCPropList.InsertItem(nSCPropAbbrev, _T("Qualifier"));
	m_cSCPropList.InsertItem(nSCPropOATSID, _T("OATS ID"));
	m_cSCPropList.InsertItem(nSCPropFrameSize, _T("Number of Frames"));
	m_cSCPropList.InsertItem(nSCPropFrameSRSOStart, _T("SRSO Start Frames"));
	m_cSCPropList.InsertItem(nSCPropStarSize, _T("Number of Stars"));
	m_cSCPropList.InsertItem(nSCPropStoredBufferSize, _T("Number of Cmd Words in STO"));
	m_cSCPropList.InsertItem(nSCPropRTCSSize, _T("Starting index of the RTCS area"));
	m_cSCPropList.InsertItem(nSCPropRTCSNum, _T("Number of RTCSs Allowed in STO Buffer"));
	m_cSCPropList.InsertItem(nSCPropSchedBufferSize, _T("Number of Cmd Words in SKB"));
	m_cSCPropList.InsertItem(nSCPropSchedBufferSegSize, _T("SKB Upload Segment Size"));
	m_cSCPropList.InsertItem(nSCPropCTCUID, _T("CTCU Address"));
	m_cSCPropList.InsertItem(nSCPropRTAddress, _T("RT Address"));
	m_cSCPropList.InsertItem(nSCPropDatabaseName, _T("Database Name"));
	m_cSCPropList.InsertItem(nSCPropMaxCmdGap, _T("Max Commanding Gap"));
	m_cSCPropList.InsertItem(nSCPropPreHKGap, _T("Pre Housekeepping Gap"));
	m_cSCPropList.InsertItem(nSCPropRetryCnt, _T("Retry Count"));
	m_cSCPropList.InsertItem(nSCPropSTOGotoTopCmd, _T("STO Buffer Goto Top Cmd"));
	m_cSCPropList.InsertItem(nSCPropRTCSCmdFile, _T("RTCS Command Restriction File"));
	m_cSCPropList.InsertItem(nSCPropPaceCmdFile, _T("Command Pacing File"));
	m_cSCPropList.InsertItem(nSCPropFrameMiniSchedFile, _T("Frame Mini Schedule File"));
	m_cSCPropList.InsertItem(nSCPropFrameIOMiniSchedFile, _T("Frame Inst Obj Mini Schedule File"));
	m_cSCPropList.InsertItem(nSCPropStarMiniSchedFile, _T("Star Mini Schedule File"));
	m_cSCPropList.InsertItem(nSCPropStarIOMiniSchedFile, _T("Star Inst Obj Mini Schedule File"));
	m_cSCPropList.InsertItem(nSCPropSFCSMiniSchedFile, _T("SFCS Mini Schedule File"));
	m_cSCPropList.InsertItem(nIntrusionRpt,            _T("Sensor Intrusion Report"));
	m_cSCPropList.InsertItem(nInstrCenterPtImgrNS,     _T("Imager Instr Center Pt N/S"));
	m_cSCPropList.InsertItem(nInstrCenterPtImgrEW,     _T("Imager Instr Center Pt E/W"));
	m_cSCPropList.InsertItem(nInstrCenterPtSdrNS,      _T("Sounder Instr Center Pt N/S"));
	m_cSCPropList.InsertItem(nInstrCenterPtSdrEW,      _T("Sounder Instr Center Pt E/W"));
	m_cSCPropList.InsertItem(nSpaceLookSdrEast,        _T("Sounder Space Look East"));
	m_cSCPropList.InsertItem(nSpaceLookSdrWest,        _T("Sounder Space Look West"));
	m_cSCPropList.InsertItem(nSpaceLookImgrEast,       _T("Imager Space Look East"));
	m_cSCPropList.InsertItem(nSpaceLookImgrWest,       _T("Imager Space Look West"));
	m_cSCPropList.InsertItem(nScanDegsNSNadirImgr,     _T("Scan N/S Nadir Imager (deg)"));
	m_cSCPropList.InsertItem(nScanDegsEWNadirImgr,     _T("Scan E/W Nadir Imager (deg)"));
	m_cSCPropList.InsertItem(nScanDegsNSNadirSdr,      _T("Scan N/S Nadir Sounder (deg)"));
	m_cSCPropList.InsertItem(nScanDegsEWNadirSdr,      _T("Scan E/W Nadir Sounder (deg)"));
	m_cSCPropList.InsertItem(nStarDegsNSNadirImgr,     _T("Star N/S Nadir Imager (deg)"));
	m_cSCPropList.InsertItem(nStarDegsEWNadirImgr,     _T("Star E/W Nadir Imager (deg)"));
	m_cSCPropList.InsertItem(nStarDegsNSNadirSdr,      _T("Star N/S Nadir Sounder (deg)"));
	m_cSCPropList.InsertItem(nStarDegsEWNadirSdr,      _T("Star E/W Nadir Sounder (deg)"));
	m_cSCPropList.InsertItem(nTimeTagSpacing,          _T("Time Tag Spacing"));
	//m_cSCPropList.InsertItem(nCmdExeTime,			   _T("Command Execution Time"));
	m_cSCPropList.InsertItem(nNumCmdFrameFile,	       _T("Number of Cmd Frames File Name"));
	m_cSCPropList.InsertItem(nCmdFrameUplinkRate,	   _T("Command Frame Uplink Rate (frms/sec)"));
	m_cSCPropList.InsertItem(nSTOLCmdRestrctFile,	   _T("STOL Command Restriction File Name"));
	m_cSCPropList.InsertItem(nSRSOSpacelookMode,	   _T("SRSO Spacelook Mode Name"));
	m_cSCPropList.InsertItem(nSRSOFrameLabel,          _T("SRSO Scan Frame Label Name"));

	for( nSCID=0; nSCID<nMaxSC; nSCID++ )
	{
		TCHAR buffer[20];
		m_cSCPropList.SetItemText(nSCPropDirectory,				nSCID+1,GetAppRegSC()->m_strDir[nSCID]);
		m_cSCPropList.SetItemText(nSCPropAbbrev,				nSCID+1,GetAppRegSC()->m_strAbbrev[nSCID]);
		_ltow_s(GetAppRegSC()->m_nID[nSCID],						buffer, 10);
		m_cSCPropList.SetItemText(nSCPropOATSID,				nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nFrameSize[nSCID],				buffer, 10);
		m_cSCPropList.SetItemText(nSCPropFrameSize,				nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nFrameSRSOStart[nSCID],			buffer, 10);
		m_cSCPropList.SetItemText(nSCPropFrameSRSOStart,		nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nStarSize[nSCID],				buffer, 10);
		m_cSCPropList.SetItemText(nSCPropStarSize,				nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nSTOSize[nSCID],					buffer, 10);
		m_cSCPropList.SetItemText(nSCPropStoredBufferSize,		nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nSTORTCSSize[nSCID],				buffer, 10);
		m_cSCPropList.SetItemText(nSCPropRTCSSize,				nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nNumRTCS[nSCID],					buffer, 10);
		m_cSCPropList.SetItemText(nSCPropRTCSNum,				nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nSKBSize[nSCID],					buffer, 10);
		m_cSCPropList.SetItemText(nSCPropSchedBufferSize,		nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nSKBSegSize[nSCID],				buffer, 10);
		m_cSCPropList.SetItemText(nSCPropSchedBufferSegSize,	nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nCTCUID[nSCID],					buffer, 10);
		m_cSCPropList.SetItemText(nSCPropCTCUID,				nSCID+1,buffer);
		m_cSCPropList.SetItemText(nSCPropRTAddress,				nSCID+1,GetAppRegSC()->m_strRTAddress[nSCID]);
		m_cSCPropList.SetItemText(nSCPropDatabaseName,			nSCID+1,GetAppRegSC()->m_strDatabaseName[nSCID]);
		m_cSCPropList.SetItemText(nSCPropMaxCmdGap,				nSCID+1,GetAppRegSC()->m_strMaxCmdGap[nSCID]);
		m_cSCPropList.SetItemText(nSCPropPreHKGap,				nSCID+1,GetAppRegSC()->m_strPreHKGap[nSCID]);
		_ltow_s(GetAppRegSC()->m_nRetryCnt[nSCID],				buffer, 10);
		m_cSCPropList.SetItemText(nSCPropRetryCnt,				nSCID+1,buffer);
		m_cSCPropList.SetItemText(nSCPropSTOGotoTopCmd,			nSCID+1,GetAppRegSC()->m_strSTOGotoTopCmd[nSCID]);
		m_cSCPropList.SetItemText(nSCPropRTCSCmdFile,			nSCID+1,GetAppRegSC()->m_strRTCSCmd[nSCID]);
		m_cSCPropList.SetItemText(nSCPropPaceCmdFile,			nSCID+1,GetAppRegSC()->m_strPaceCmd[nSCID]);
		m_cSCPropList.SetItemText(nSCPropFrameMiniSchedFile,	nSCID+1,GetAppRegSC()->m_strFrameMiniSched[nSCID]);
		m_cSCPropList.SetItemText(nSCPropFrameIOMiniSchedFile,	nSCID+1,GetAppRegSC()->m_strFrameIOMiniSched[nSCID]);
		m_cSCPropList.SetItemText(nSCPropStarMiniSchedFile,		nSCID+1,GetAppRegSC()->m_strStarMiniSched[nSCID]);
		m_cSCPropList.SetItemText(nSCPropStarIOMiniSchedFile,	nSCID+1,GetAppRegSC()->m_strStarIOMiniSched[nSCID]);
		m_cSCPropList.SetItemText(nSCPropSFCSMiniSchedFile,		nSCID+1,GetAppRegSC()->m_strSFCSMiniSched[nSCID]);
		m_cSCPropList.SetItemText(nIntrusionRpt,				nSCID+1,GetAppRegSC()->m_strIntrusionRpt[nSCID]);
		m_cSCPropList.SetItemText(nScanDegsEWNadirImgr,			nSCID+1,GetAppRegSC()->m_strScanDegsEWNadirImgr[nSCID]);
		m_cSCPropList.SetItemText(nScanDegsNSNadirImgr,			nSCID+1,GetAppRegSC()->m_strScanDegsNSNadirImgr[nSCID]);
		m_cSCPropList.SetItemText(nScanDegsEWNadirSdr,			nSCID+1,GetAppRegSC()->m_strScanDegsEWNadirSdr[nSCID]);
		m_cSCPropList.SetItemText(nScanDegsNSNadirSdr,			nSCID+1,GetAppRegSC()->m_strScanDegsNSNadirSdr[nSCID]);
		m_cSCPropList.SetItemText(nStarDegsEWNadirImgr,			nSCID+1,GetAppRegSC()->m_strStarDegsEWNadirImgr[nSCID]);
		m_cSCPropList.SetItemText(nStarDegsNSNadirImgr,			nSCID+1,GetAppRegSC()->m_strStarDegsNSNadirImgr[nSCID]);
		m_cSCPropList.SetItemText(nStarDegsEWNadirSdr,			nSCID+1,GetAppRegSC()->m_strStarDegsEWNadirSdr[nSCID]);
		m_cSCPropList.SetItemText(nStarDegsNSNadirSdr,			nSCID+1,GetAppRegSC()->m_strStarDegsNSNadirSdr[nSCID]);
		_ltow_s(GetAppRegSC()->m_nInstrCenterPtImgrNS[nSCID],		buffer, 10);
		m_cSCPropList.SetItemText(nInstrCenterPtImgrNS,			nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nInstrCenterPtImgrEW[nSCID],		buffer, 10);
		m_cSCPropList.SetItemText(nInstrCenterPtImgrEW,			nSCID+1,buffer);
		_ltow_s(GetAppRegSC()->m_nInstrCenterPtSdrNS[nSCID],		buffer,  10);
		m_cSCPropList.SetItemText(nInstrCenterPtSdrNS,			nSCID+1, buffer);
		_ltow_s(GetAppRegSC()->m_nInstrCenterPtSdrEW[nSCID],		buffer,  10);
		m_cSCPropList.SetItemText(nInstrCenterPtSdrEW,			nSCID+1, buffer);
		_ltow_s(GetAppRegSC()->m_nSpaceLookSdrWest[nSCID],		buffer,  10);
		m_cSCPropList.SetItemText(nSpaceLookSdrWest,			nSCID+1, buffer);
		_ltow_s(GetAppRegSC()->m_nSpaceLookSdrEast[nSCID],		buffer,  10);
		m_cSCPropList.SetItemText(nSpaceLookSdrEast,			nSCID+1, buffer);
		_ltow_s(GetAppRegSC()->m_nSpaceLookImgrWest[nSCID],		buffer,  10);
		m_cSCPropList.SetItemText(nSpaceLookImgrWest,			nSCID+1, buffer);
		_ltow_s(GetAppRegSC()->m_nSpaceLookImgrEast[nSCID],		buffer,  10);
		m_cSCPropList.SetItemText(nSpaceLookImgrEast,			nSCID+1, buffer);
		m_cSCPropList.SetItemText(nTimeTagSpacing,				nSCID+1, GetAppRegSC()->m_strTimeTagSpacing[nSCID]);
		//_ltow_s(GetAppRegSC()->m_fCmdExeTime[nSCID],			buffer,	 10);
		//m_cSCPropList.SetItemText(nCmdExeTime,				nSCID+1, buffer);
		m_cSCPropList.SetItemText(nNumCmdFrameFile,		   	    nSCID+1, GetAppRegSC()->m_strNumCmdFrameFile[nSCID]);
		_ltow_s(GetAppRegSC()->m_nCmdFrameUplinkRate[nSCID],		buffer,  10);
		m_cSCPropList.SetItemText(nCmdFrameUplinkRate,		    nSCID+1, buffer);
		m_cSCPropList.SetItemText(nSTOLCmdRestrctFile,			nSCID+1, GetAppRegSC()->m_strSTOLCmd[nSCID]);
		m_cSCPropList.SetItemText(nSRSOSpacelookMode,			nSCID+1, GetAppRegSC()->m_strSRSOSpacelookMode[nSCID]);
		m_cSCPropList.SetItemText(nSRSOFrameLabel,				nSCID+1, GetAppRegSC()->m_strSRSOFrameLabel[nSCID]);
	}

	if( ((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
		GetDlgItem(IDC_SCID_COMBO)->EnableWindow(FALSE);

	if( *GetAppAdminMode() )
		GetDlgItem(IDC_SC_STATIC)->SetWindowText(_T("Spacecraft Properties"));
	else
		GetDlgItem(IDC_SC_STATIC)->SetWindowText(_T("Spacecraft Properties (Read only)"));

	AddAnchor(IDC_SCDEFAULT_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_SC_STATIC, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_SCPROP_LIST, TOP_LEFT, BOTTOM_RIGHT);

	if( (((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly) || (!*GetAppAdminMode()) )
		m_cSCPropList.SetReadOnly();

	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::OnEndLabelEditBasePropList

//
//  Description :  The framework calls this function when the property
//				   page�s list control is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					NMHDR* pNMHDR		-	Pointer to a NMHDR object.
//					LRESULT* pResult	-	Pointer to a LRESULT object.
//											Always set to 0.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CSCPropPage::OnEndLabelEditBasePropList(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO* pDispInfo	= (LV_DISPINFO*)pNMHDR;
	CString	strError;
	CString strEntry		= pDispInfo->item.pszText;
//	BOOL	bValid			= TRUE;

	switch( pDispInfo->item.iItem )
	{
		case (nSCPropDirectory):				strError = CheckDirectory(pDispInfo);			break;
		case (nSCPropAbbrev):					strError = CheckAbbrev(pDispInfo);				break;
		case (nSCPropOATSID):					strError = CheckOATSID(pDispInfo);				break;
		case (nSCPropFrameSize):				strError = CheckFrameSize(pDispInfo);			break;
		case (nSCPropFrameSRSOStart):			strError = CheckFrameSRSOStart(pDispInfo);		break;
		case (nSCPropStarSize):					strError = CheckStarSize(pDispInfo);			break;
		case (nSCPropStoredBufferSize):			strError = CheckSTOSize(pDispInfo);				break;
		case (nSCPropRTCSSize):					strError = CheckSTORTCSSize(pDispInfo);			break;
		case (nSCPropRTCSNum):					strError = CheckSTORTCSNum(pDispInfo);			break;
		case (nSCPropSchedBufferSize):			strError = CheckSKBSize(pDispInfo);				break;
		case (nSCPropSchedBufferSegSize):		strError = CheckSKBSegSize(pDispInfo);			break;
		case (nSCPropCTCUID):					strError = CheckCTCUID(pDispInfo);				break;
		case (nSCPropRTAddress):				strError.Empty();								break;
		case (nSCPropDatabaseName):				strError = CheckDatabaseName(pDispInfo);		break;
		case (nSCPropMaxCmdGap):				strError = CheckMaxCmdGap(pDispInfo);			break;
		case (nSCPropPreHKGap):					strError = CheckPreHKGap(pDispInfo);			break;
		case (nSCPropRetryCnt):					strError = CheckRetryCnt(pDispInfo);			break;
		case (nSCPropSTOGotoTopCmd):			strError = CheckSTOGotoTopCmd(pDispInfo);		break;
		case (nSCPropRTCSCmdFile):				strError = CheckRTCSCmdFile(pDispInfo);			break;
		case (nSCPropPaceCmdFile):				strError = CheckPaceCmdFile(pDispInfo);			break;
		//case (nSCPropFrameMiniSchedFile):		strError = CheckFrameMiniSchedFile(pDispInfo);	break;
		//case (nSCPropFrameIOMiniSchedFile):	strError = CheckFrameIOMiniSchedFile(pDispInfo);break;
		//case (nSCPropStarMiniSchedFile):		strError = CheckStarMiniSchedFile(pDispInfo);	break;
		//case (nSCPropStarIOMiniSchedFile):	strError = CheckStarIOMiniSchedFile(pDispInfo);	break;
		case (nSCPropSFCSMiniSchedFile):		strError = CheckSFCSMiniSchedFile(pDispInfo);	break;
		default:								strError.Empty();								break;
	}

	if( strError.IsEmpty() )
	{
		m_cSCPropList.OnEndLabelEdit(pNMHDR, pResult);

		if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
			SetModified(TRUE);
	}
	else
		GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);

	*pResult = 0;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckDirectory
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckDirectory(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Directory value can not be empty.");

	if( strError.IsEmpty() && (strEntry.Find(_T(' '), 0) != -1) )
		strError = _T("Directory value can not contain any spaces.");

	if( strError.IsEmpty() && ((strEntry.Find(_T('\\')) == 0) ||
		(strEntry.ReverseFind(_T('\\')) == strEntry.GetLength()-1)) )
		strError = _T("Directory must not begin or end with a \\");

	if( strError.IsEmpty() )
		((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckAbbrev
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckAbbrev(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Abbreviation (qualifier) value can not be empty.");

	if( strError.IsEmpty() && (strEntry.Find(_T(' '), 0) != -1) )
		strError = _T("Abbreviation (qualifier) value can not contain any spaces.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckOATSID
//
//  Description :  Checks the entry. Returns an error
//				   message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckOATSID(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0 )
		strError = _T("OATS ID must be a valid integer number in the range of 1 to 99.");

	if( (nEntry < 1) || (nEntry > 99) )
		strError = _T("OATS ID must be a valid integer number in the range of 1 to 99.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckFrameSize
//
//  Description :  Checks the entry. Returns an error
//				   message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckFrameSize(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0 )
		strError = _T("Frame size must be a valid integer number in the range of 0 to 9999.");

	if( (nEntry < 0) || (nEntry > 9999) )
		strError = _T("Frame size must be a valid integer number in the range of 0 to 9999.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckFrameSRSOStart
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckFrameSRSOStart(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0 )
		strError = _T("SRSO Start Frame must be a valid integer number in the range of 0 to 9999.");

	if( (nEntry < 0) || (nEntry > 9999) )
		strError = _T("SRSO Start Frame must be a valid integer number in the range of 0 to 9999.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckStarSize
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckStarSize(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0 )
		strError = _T("Number of Stars must be a valid integer number in the range of 0 to 9999.");

	if( (nEntry < 0) || (nEntry > 9999) )
		strError = _T("Number of Stars must be a valid integer number in the range of 0 to 9999.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckSTOSize
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckSTOSize(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	int nMinSize = GetAppRegSC()->m_nSTORTCSSize[pDispInfo->item.iSubItem - 1];

	if( strEntry.Compare(buffer) != 0 )
		strError.Format(_T("Number of Cmd Words in STO must be a valid integer number in the range of %d to 9999."), nMinSize);

	if( (nEntry < nMinSize) || (nEntry > 9999) )
		strError.Format(_T("Number of Cmd Words in STO must be a valid integer number in the range of %d to 9999."), nMinSize);

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckSKBSize
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckSKBSize(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0 )
		strError = _T("Number of Cmd Words in SKB must be a valid integer number in the range of 0 to 9999.");

	if( (nEntry < 0) || (nEntry > 9999) )
		strError = _T("Number of Cmd Words in SKB must be a valid integer number in the range of 0 to 9999.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckSKBSegSize
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckSKBSegSize(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0 )
		strError = _T("SKB Upload Segment Size must be a valid integer number in the range of -1 to 9999.");

	if( (nEntry < -1) || (nEntry > 9999) )
		strError = _T("SKB Upload Segment Size must be a valid integer number in the range of -1 to 9999.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckCTCUID
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckCTCUID(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0 )
		strError = _T("CTCU ID must be a valid integer number in the range of 1 to 2.");

	if( (nEntry < 1) || (nEntry > 2) )
		strError = _T("CTCU ID must be a valid integer number in the range of 1 to 2.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckDatabaseName
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckDatabaseName(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Database Name can not be empty.");

	if( strError.IsEmpty() && (strEntry.Find(_T(' '), 0) != -1) )
		strError = _T("Database Name can not contain any spaces.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckMaxCmdGap
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckMaxCmdGap(LV_DISPINFO* pDispInfo)
{
	CString	strError = _T("Max Cmd Gap Time must be in the format of hh:mm:ss where hh=00-99, mm=00-59, ss=00-59");
	CString strEntry = pDispInfo->item.pszText;

	if( strEntry.GetLength() == 8 )
	{
		if( (strEntry[0] >= _T('0')) &&
			(strEntry[0] <= _T('9')) &&
			(strEntry[1] >= _T('0')) &&
			(strEntry[1] <= _T('9')) &&
			(strEntry[2] == _T(':')) &&
			(strEntry[3] >= _T('0')) &&
			(strEntry[3] <= _T('5')) &&
			(strEntry[4] >= _T('0')) &&
			(strEntry[4] <= _T('9')) &&
			(strEntry[5] == _T(':')) &&
			(strEntry[6] >= _T('0')) &&
			(strEntry[6] <= _T('5')) &&
			(strEntry[7] >= _T('0')) &&
			(strEntry[7] <= _T('9')) )
			strError.Empty();
	}

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckPreHKGap
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckPreHKGap(LV_DISPINFO* pDispInfo)
{
	CString	strError = _T("Pre HK Gap Time must be in the format of hh:mm:ss where hh=00-99, mm=00-59, ss=00-59");
	CString strEntry = pDispInfo->item.pszText;

	if( strEntry.GetLength() == 8 )
	{
		if( (strEntry[0] >= _T('0')) &&
			(strEntry[0] <= _T('9')) &&
			(strEntry[1] >= _T('0')) &&
			(strEntry[1] <= _T('9')) &&
			(strEntry[2] == _T(':')) &&
			(strEntry[3] >= _T('0')) &&
			(strEntry[3] <= _T('5')) &&
			(strEntry[4] >= _T('0')) &&
			(strEntry[4] <= _T('9')) &&
			(strEntry[5] == _T(':')) &&
			(strEntry[6] >= _T('0')) &&
			(strEntry[6] <= _T('5')) &&
			(strEntry[7] >= _T('0')) &&
			(strEntry[7] <= _T('9')) )
			strError.Empty();
	}

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckRetryCnt
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckRetryCnt(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0 )
		strError = _T("Retry Count must be a valid integer number in the range of 0 to 9.");

	if( (nEntry < 0) || (nEntry > 9) )
		strError = _T("Retry Count must be a valid integer number in the range of 0 to 9.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckSTOGotoTopCmd
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckSTOGotoTopCmd(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Default command can not be empty.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckRTCSCmdFile
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckRTCSCmdFile(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("RTCS Command File can not be empty.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckPaceCmdFile
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckPaceCmdFile(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Command Pacing File can not be empty.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckFrameMiniSchedFile
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckFrameMiniSchedFile(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Frame Mini Schedule File can not be empty.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckFrameIOMiniSchedFile
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckFrameIOMiniSchedFile(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Frame Mini Inst Obj Schedule File can not be empty.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckStarMiniSchedFile
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckStarMiniSchedFile(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Star Mini Schedule File can not be empty.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckStarIOMiniSchedFile
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckStarIOMiniSchedFile(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Star Mini Inst Obj Schedule File can not be empty.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckSFCSMiniSchedFile
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckSFCSMiniSchedFile(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("SFCS Mini Schedule File can not be empty.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckSTORTCSSize
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckSTORTCSSize(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	int nMaxSize = GetAppRegSC()->m_nSTOSize[pDispInfo->item.iSubItem - 1];

	if( strEntry.Compare(buffer) != 0)
		strError.Format(_T("Number of RTCS Cmd Words in STO must be a valid integer number in the range of 0 to %d."), nMaxSize);

	if( (nEntry < 0) || (nEntry > nMaxSize) )
		strError.Format(_T("Number of RTCS Cmd Words in STO must be a valid integer number in the range of 0 to %d."), nMaxSize);

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::CheckSTORTCSNum
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CSCPropPage::CheckSTORTCSNum(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0)
		strError.Format(_T("Number of RTCSs Allowed in STO must be a valid integer number in the range of 1 to 99."));

	if( (nEntry < 1) || (nEntry > 99) )
		strError.Format(_T("Number of RTCSs Allowed in STO must be a valid integer number in the range of 1 to 99."));

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//	Function :     CSCPropPage::OnPropChange()
//
//  Description :  The framework calls this function when the property
//				   page�s controls are changed.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CSCPropPage::OnPropChange()
{
	if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
		SetModified(TRUE);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//	Function :     CSCPropPage::OnApply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :        BOOL
//					Always returns TRUE.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CSCPropPage::OnApply()
{
	((CWorkspaceOptionsSheet*)GetParent())->Apply();
	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CSCPropPage::Apply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CSCPropPage::Apply()
{
	if (m_hWnd)
	{
		int nPrevDefault = GetAppRegSite()->m_nDefaultSC;
		GetAppRegSite()->m_nDefaultSC = ((CComboBox*)GetDlgItem(IDC_SCID_COMBO))->GetCurSel();

		if (nPrevDefault != GetAppRegSite()->m_nDefaultSC){
			((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;
			GetAppRegSite()->SetValues();
		}

		for (int nSCID=0; nSCID<nMaxSC; nSCID++)
		{
			GetAppRegSC()->m_strDir[nSCID]					= m_cSCPropList.GetItemText(nSCPropDirectory,				nSCID+1);
			GetAppRegSC()->m_strAbbrev[nSCID]				= m_cSCPropList.GetItemText(nSCPropAbbrev,					nSCID+1);
			GetAppRegSC()->m_nID[nSCID]						= _ttol(m_cSCPropList.GetItemText(nSCPropOATSID,			nSCID+1));
			GetAppRegSC()->m_nFrameSize[nSCID]				= _ttol(m_cSCPropList.GetItemText(nSCPropFrameSize,			nSCID+1));
			GetAppRegSC()->m_nFrameSRSOStart[nSCID]			= _ttol(m_cSCPropList.GetItemText(nSCPropFrameSRSOStart,	nSCID+1));
			GetAppRegSC()->m_nStarSize[nSCID]				= _ttol(m_cSCPropList.GetItemText(nSCPropStarSize,			nSCID+1));
			GetAppRegSC()->m_nSTOSize[nSCID]				= _ttol(m_cSCPropList.GetItemText(nSCPropStoredBufferSize,	nSCID+1));
			GetAppRegSC()->m_nSTORTCSSize[nSCID]			= _ttol(m_cSCPropList.GetItemText(nSCPropRTCSSize,			nSCID+1));
			GetAppRegSC()->m_nNumRTCS[nSCID]				= _ttol(m_cSCPropList.GetItemText(nSCPropRTCSNum,			nSCID+1));
			GetAppRegSC()->m_nSKBSize[nSCID]				= _ttol(m_cSCPropList.GetItemText(nSCPropSchedBufferSize,	nSCID+1));
			GetAppRegSC()->m_nSKBSegSize[nSCID]				= _ttol(m_cSCPropList.GetItemText(nSCPropSchedBufferSegSize,nSCID+1));
			GetAppRegSC()->m_nCTCUID[nSCID]					= _ttol(m_cSCPropList.GetItemText(nSCPropCTCUID,			nSCID+1));
			GetAppRegSC()->m_strRTAddress[nSCID]			= m_cSCPropList.GetItemText(nSCPropRTAddress,				nSCID+1);
			GetAppRegSC()->m_strDatabaseName[nSCID]			= m_cSCPropList.GetItemText(nSCPropDatabaseName,			nSCID+1);
			GetAppRegSC()->m_strMaxCmdGap[nSCID]			= m_cSCPropList.GetItemText(nSCPropMaxCmdGap,				nSCID+1);
			GetAppRegSC()->m_strPreHKGap[nSCID]				= m_cSCPropList.GetItemText(nSCPropPreHKGap,				nSCID+1);
			GetAppRegSC()->m_nRetryCnt[nSCID]				= _ttol(m_cSCPropList.GetItemText(nSCPropRetryCnt,			nSCID+1));
			GetAppRegSC()->m_strSTOGotoTopCmd[nSCID]		= m_cSCPropList.GetItemText(nSCPropSTOGotoTopCmd,			nSCID+1);
			GetAppRegSC()->m_strRTCSCmd[nSCID]				= m_cSCPropList.GetItemText(nSCPropRTCSCmdFile,				nSCID + 1);
			GetAppRegSC()->m_strPaceCmd[nSCID]				= m_cSCPropList.GetItemText(nSCPropPaceCmdFile,				nSCID + 1);
			GetAppRegSC()->m_strFrameMiniSched[nSCID]		= m_cSCPropList.GetItemText(nSCPropFrameMiniSchedFile,		nSCID + 1);
			GetAppRegSC()->m_strFrameIOMiniSched[nSCID]		= m_cSCPropList.GetItemText(nSCPropFrameIOMiniSchedFile,	nSCID + 1);
			GetAppRegSC()->m_strStarMiniSched[nSCID]		= m_cSCPropList.GetItemText(nSCPropStarMiniSchedFile,		nSCID + 1);
			GetAppRegSC()->m_strStarIOMiniSched[nSCID]		= m_cSCPropList.GetItemText(nSCPropStarIOMiniSchedFile,		nSCID + 1);
			GetAppRegSC()->m_strSFCSMiniSched[nSCID]	    = m_cSCPropList.GetItemText(nSCPropSFCSMiniSchedFile,		nSCID + 1);
			GetAppRegSC()->m_strIntrusionRpt[nSCID]         = m_cSCPropList.GetItemText(nIntrusionRpt,					nSCID + 1);
			GetAppRegSC()->m_strScanDegsEWNadirImgr[nSCID]  = m_cSCPropList.GetItemText(nScanDegsEWNadirImgr,			nSCID + 1);
			GetAppRegSC()->m_strScanDegsNSNadirImgr[nSCID]  = m_cSCPropList.GetItemText(nScanDegsNSNadirImgr,			nSCID + 1);
			GetAppRegSC()->m_strScanDegsEWNadirSdr[nSCID]   = m_cSCPropList.GetItemText(nScanDegsEWNadirSdr,			nSCID + 1);
			GetAppRegSC()->m_strScanDegsNSNadirSdr[nSCID]   = m_cSCPropList.GetItemText(nScanDegsNSNadirSdr,			nSCID + 1);
			GetAppRegSC()->m_strStarDegsEWNadirImgr[nSCID]  = m_cSCPropList.GetItemText(nStarDegsEWNadirImgr,			nSCID + 1);
			GetAppRegSC()->m_strStarDegsNSNadirImgr[nSCID]  = m_cSCPropList.GetItemText(nStarDegsNSNadirImgr,			nSCID + 1);
			GetAppRegSC()->m_strStarDegsEWNadirSdr[nSCID]   = m_cSCPropList.GetItemText(nStarDegsEWNadirSdr,			nSCID + 1);
			GetAppRegSC()->m_strStarDegsNSNadirSdr[nSCID]   = m_cSCPropList.GetItemText(nStarDegsNSNadirSdr,			nSCID + 1);
			GetAppRegSC()->m_nInstrCenterPtImgrNS[nSCID]    = _ttol(m_cSCPropList.GetItemText(nInstrCenterPtImgrNS,		nSCID + 1));
			GetAppRegSC()->m_nInstrCenterPtImgrEW[nSCID]    = _ttol(m_cSCPropList.GetItemText(nInstrCenterPtImgrEW,		nSCID + 1));
			GetAppRegSC()->m_nInstrCenterPtSdrNS[nSCID]     = _ttol(m_cSCPropList.GetItemText(nInstrCenterPtSdrNS,		nSCID + 1));
			GetAppRegSC()->m_nInstrCenterPtSdrEW[nSCID]     = _ttol(m_cSCPropList.GetItemText(nInstrCenterPtSdrEW,		nSCID + 1));
			GetAppRegSC()->m_nSpaceLookSdrWest[nSCID]       = _ttol(m_cSCPropList.GetItemText(nSpaceLookSdrWest,		nSCID + 1));
			GetAppRegSC()->m_nSpaceLookSdrEast[nSCID]       = _ttol(m_cSCPropList.GetItemText(nSpaceLookSdrEast,		nSCID + 1));
			GetAppRegSC()->m_nSpaceLookImgrWest[nSCID]      = _ttol(m_cSCPropList.GetItemText(nSpaceLookImgrWest,		nSCID + 1));
			GetAppRegSC()->m_nSpaceLookImgrEast[nSCID]      = _ttol(m_cSCPropList.GetItemText(nSpaceLookImgrEast,		nSCID + 1));
			GetAppRegSC()->m_strTimeTagSpacing[nSCID]       =       m_cSCPropList.GetItemText(nTimeTagSpacing,			nSCID + 1);
			//GetAppRegSC()->m_fCmdExeTime[nSCID]           = _ttol(m_cSCPropList.GetItemText(nCmdExeTime,				nSCID + 1));
			GetAppRegSC()->m_strNumCmdFrameFile[nSCID]      =       m_cSCPropList.GetItemText(nNumCmdFrameFile,	   	    nSCID + 1);
			GetAppRegSC()->m_nCmdFrameUplinkRate[nSCID]     = _ttol(m_cSCPropList.GetItemText(nCmdFrameUplinkRate,		nSCID + 1));
			GetAppRegSC()->m_strSTOLCmd[nSCID]              = m_cSCPropList.GetItemText(nSTOLCmdRestrctFile,            nSCID + 1);
			GetAppRegSC()->m_strSRSOSpacelookMode[nSCID]    = m_cSCPropList.GetItemText(nSRSOSpacelookMode,             nSCID + 1);
			GetAppRegSC()->m_strSRSOFrameLabel[nSCID]		= m_cSCPropList.GetItemText(nSRSOFrameLabel,	            nSCID + 1);
		}

		GetAppRegSC()->SetValues();
	}
}

BEGIN_MESSAGE_MAP(CFolderPropListCtrl, CEditListCtrl)
	//{{AFX_MSG_MAP(CFolderPropListCtrl)
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CFolderPropListCtrl::CFolderPropListCtrl()
//
//  Description :  Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CFolderPropListCtrl::CFolderPropListCtrl(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CFolderPropListCtrl::~CFolderPropListCtrl()
//
//  Description :  Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CFolderPropListCtrl::~CFolderPropListCtrl(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :    CFolderPropListCtrl::OnLButtonDown
//
//  Description :  The framework calls this member function when the user
//				   releases the left mouse button.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					UINT nFlags	 -	Indicates whether various virtual
//									keys are down.
//					CPoint point -	Specifies the x- and y-coordinate of
//									the cursor.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CFolderPropListCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	int index;
	CEditListCtrl::OnLButtonDown(nFlags, point);

	int colnum;

	if(	(index = HitTestEx(point, &colnum)) != -1 )
	{
		UINT flag = LVIS_FOCUSED;

		if( (GetItemState(index, flag) & flag) == flag && colnum > 0 )
		{
			if( !m_bReadOnly )
			{
				// Add check for LVS_EDITLABELS
				if( GetWindowLong(m_hWnd, GWL_STYLE) & LVS_EDITLABELS )
					EditSubLabel(index, colnum);
			}
			else
				SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		}
		else
			SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	}
}


IMPLEMENT_DYNCREATE(CFolderPropPage, CResizablePage)

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CFolderPropPage::CFolderPropPage()
//
//  Description :  Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CFolderPropPage::CFolderPropPage() : CResizablePage(CFolderPropPage::IDD)
{
	//{{AFX_DATA_INIT(CFolderPropPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CFolderPropPage::~CFolderPropPage()
//
//  Description :  Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CFolderPropPage::~CFolderPropPage(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CFolderPropPage::DoDataExchange
//
//  Description :  This method is called by the framework to exchange and
//				   validate dialog data.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CFolderPropPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFolderPropPage)
	DDX_Control(pDX, IDC_FOLDERPROP_LIST, m_cFolderPropList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFolderPropPage, CResizablePage)
	//{{AFX_MSG_MAP(CFolderPropPage)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_FOLDERPROP_LIST, OnEndLabelEditBasePropList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CFolderPropPage::Apply()
//
//  Description :  The framework calls this function when the property
//				   page�s list control is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CFolderPropPage::Apply()
{
	if( m_hWnd )
	{
		CString strOld = GetAppRegFolder()->m_strDayFormat;
		GetDlgItem(IDC_FOLDERFORMAT_EDIT)->GetWindowText(GetAppRegFolder()->m_strDayFormat);
		GetAppRegFolder()->m_strDayFormat.TrimLeft();
		GetAppRegFolder()->m_strDayFormat.TrimRight();

		if( strOld != GetAppRegFolder()->m_strDayFormat )
			((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;

		for( int nFolder=0; nFolder<nMaxFolderTypes; nFolder++ )
			GetAppRegFolder()->m_strDir[nFolder] = m_cFolderPropList.GetItemText(nFolder, 1);

		GetAppRegFolder()->SetValues();
	}
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CFolderPropPage::OnInitDialog()
//
//  Description :  The framework calls this function when the property
//				   page�s dialog is initialized.
//
//  Return :        BOOL
//						Always returns TRUE.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CFolderPropPage::OnInitDialog()
{
	CResizablePage::OnInitDialog();
	int nFolder;

	GetDlgItem(IDC_FOLDERFORMAT_EDIT)->SetWindowText(GetAppRegFolder()->m_strDayFormat);

	m_cFolderPropList.InsertColumn(0, _T("Folder Type"), LVCFMT_LEFT, 150);
	m_cFolderPropList.InsertColumn(1, _T("Directory"), LVCFMT_LEFT, 150);

	for( nFolder=0; nFolder<nMaxFolderTypes; nFolder++ )
	{
		m_cFolderPropList.InsertItem(nFolder, GetAppRegFolder()->m_strTitle[nFolder]);
		m_cFolderPropList.SetItemText(nFolder, 1, GetAppRegFolder()->m_strDir[nFolder]);
	}

	if( (*GetAppAdminMode()) && (!((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly) )
		GetDlgItem(IDC_FOLDER_STATIC)->SetWindowText(_T("Folder Properties"));
	else
		GetDlgItem(IDC_FOLDER_STATIC)->SetWindowText(_T("Folder Properties (Read only)"));

	AddAnchor(IDC_FOLDER_STATIC, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_FOLDERFORMAT_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_FOLDERPROP_LIST, TOP_LEFT, BOTTOM_RIGHT);

	if( (((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly) || (!*GetAppAdminMode()) )
		m_cFolderPropList.SetReadOnly();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CFolderPropPage::OnEndLabelEditBasePropList
//
//  Description :  The framework calls this function when the property
//				   page�s list control is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					NMHDR* pNMHDR		-	Pointer to a NMHDR object.
//					LRESULT* pResult	-	Pointer to a LRESULT object.
//											Always set to 0.
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CFolderPropPage::OnEndLabelEditBasePropList(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
	CString	strError = CheckDirectory(pDispInfo);

	if( strError.IsEmpty() )
	{
		m_cFolderPropList.OnEndLabelEdit(pNMHDR, pResult);

		if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
			SetModified(TRUE);
	}
	else
		GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);

	*pResult = 0;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CFolderPropPage::CheckDirectory
//
//  Description :   Checks the entry. Returns an error
//					message if it is invalid.
//
//  Return :
//					CString
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CFolderPropPage::CheckDirectory(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry = pDispInfo->item.pszText;

	if( strError.IsEmpty() && (strEntry.IsEmpty()) )
		strError = _T("Directory value can not be empty.");

	if( strError.IsEmpty() && (strEntry.Find(_T(' '), 0) != -1) )
		strError = _T("Directory value can not contain any spaces.");

	if( strError.IsEmpty() && ((strEntry.Find(_T('\\')) == 0) ||
		(strEntry.ReverseFind(_T('\\')) == strEntry.GetLength()-1)) )
		strError = _T("Directory must not begin or end with a \\");

	if( strError.IsEmpty() )
		((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CFolderPropPage::OnApply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :        BOOL
//					Always returns TRUE.
//
//  Parameters :
//					void	-	none.

//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CFolderPropPage::OnApply()
{
	((CWorkspaceOptionsSheet*)GetParent())->Apply();
	return TRUE;
}

IMPLEMENT_DYNCREATE(CMiscPropPage, CResizablePage)

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMiscPropPage::CMiscPropPage()
//
//  Description :   Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CMiscPropPage::CMiscPropPage() : CResizablePage(CMiscPropPage::IDD)
{
	//{{AFX_DATA_INIT(CMiscPropPage)
	m_nDayEdit = 0;
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMiscPropPage::~CMiscPropPage()
//
//  Description :  Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CMiscPropPage::~CMiscPropPage(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMiscPropPage::DoDataExchange
//
//  Description :  This method is called by the framework to exchange and
//				   validate dialog data.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMiscPropPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMiscPropPage)
	DDX_Text(pDX, IDC_DAY_EDIT, m_nDayEdit);
	DDV_MinMaxInt(pDX, m_nDayEdit, 1, 366);
	DDX_Control(pDX, IDC_MISCTOP_EDIT, m_cTopMargin);
	DDX_Control(pDX, IDC_MISCBOTTOM_EDIT, m_cBottomMargin);
	DDX_Control(pDX, IDC_MISCLEFT_EDIT, m_cLeftMargin);
	DDX_Control(pDX, IDC_MISCRIGHT_EDIT, m_cRightMargin);
	DDX_Control(pDX, IDC_MISCCMDEXE_EDIT, m_cCmdExeTime);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMiscPropPage, CResizablePage)
	//{{AFX_MSG_MAP(CMiscPropPage)
	ON_EN_UPDATE(IDC_DAY_EDIT, OnPropsChange)
	ON_BN_CLICKED(IDC_MISCFONTCHANGE_BUTTON, OnMiscFontChangeButton)
	ON_EN_UPDATE(IDC_MISCBOTTOM_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_MISCLEFT_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_MISCRIGHT_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_MISCTOP_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_MISCCMDEXE_EDIT, OnPropsChange)
	ON_CBN_SELCHANGE(IDC_DBDRIVE_COMBO, OnDBDrivePropsChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMiscPropPage::Apply()
//
//  Description :  The framework calls this function when the property
//				   page�s list control is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMiscPropPage::Apply()
{
	if(m_hWnd)
	{
		// This keeps the invalid directory message from being generated twice.
		static bool bDisplay = true;
		CString strDay;
		CString strPrefix;
	//	int		nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
	//	int		nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
		// strPrefix.Format(_T("\\\\%s\\%s\\%s\\"),

		strPrefix.Format(_T("%s\\%s\\%s\\%s\\"),
			ctcProcDrive, ctcMountPoint, ctcProcsDir,
		//	GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],	// Server
		//	GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
			GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC]);	// Spacecraft

		strDay.Format(GetAppRegFolder()->m_strDayFormat + _T("\\"), m_nDayEdit);

		CString strMsg;
		CString strFmt(strPrefix+strDay);
		COXUNC uncDay(strPrefix+strDay);
		COXUNC uncSTOL(strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderSTOL]	+ _T("\\"));
		COXUNC uncFrame(strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderFrame]	+ _T("\\"));
		COXUNC uncStar(strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderStar]	+ _T("\\"));
		COXUNC uncOATS(strPrefix+strDay+GetAppRegFolder()->m_strDir[nFolderOATS]	+ _T("\\"));

		strFmt.Delete(strFmt.GetLength()-1);

		if(!uncDay.Exists()){
			// Day directory doesn't exist.
			if(GetAppRegMisc()->m_nDOY != m_nDayEdit){
				if (bDisplay){
					strMsg.Format(_T("Day directory does not exist: %s"), (LPCTSTR)strFmt);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
					AfxMessageBox(strMsg);
					bDisplay = false;
				} else {
					bDisplay = true;
				}
				// If directory doesn't exist, revert back to the default value.
				m_nDayEdit = GetAppRegMisc()->m_nDOY;
			} else if (bDisplay){
					strMsg.Format(_T("Day directory does not exist: %s\n Create directory and Refresh!!"), (LPCTSTR)strFmt);
					AfxMessageBox(strMsg);
					strMsg.Format(_T("Day directory does not exist: %s "), (LPCTSTR)strFmt);
					GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
					bDisplay = false;
			} else {
				bDisplay = true;
			}
		}else {
			// Day directory exists.  Now check if subdirectories also exist.  If not post a message.
			if (!uncSTOL.Exists()) {
				strMsg.Format(_T("The STOL day directory does not exist.  Create directory and Refresh!"));
				AfxMessageBox(strMsg);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
				// Reset day directory display back to registry value.
				m_nDayEdit = GetAppRegMisc()->m_nDOY;
			} else if (!uncFrame.Exists()){
				strMsg.Format(_T("The Frame day directory does not exist.  Create directory and Refresh!"));
				AfxMessageBox(strMsg);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
				// Reset day directory display back to registry value.
				m_nDayEdit = GetAppRegMisc()->m_nDOY;
			} else if (!uncStar.Exists()){
				strMsg.Format(_T("The Star day directory does not exist.  Create directory and Refresh!"));
				AfxMessageBox(strMsg);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
				// Reset day directory display back to registry value.
				m_nDayEdit = GetAppRegMisc()->m_nDOY;
			} else if (!uncOATS.Exists()){
				strMsg.Format(_T("The OATS day directory does not exist.  Create directory and Refresh!"));
				AfxMessageBox(strMsg);
				GetAppOutputBar()->OutputToEvents(strMsg, COutputBar::WARNING);
				// Reset day directory display back to registry value.
				m_nDayEdit = GetAppRegMisc()->m_nDOY;
			} else if (GetAppRegMisc()->m_nDOY != m_nDayEdit){
				// Current value is valid so need to save to registry.
				GetAppRegMisc()->m_nDOY = m_nDayEdit;
			}
			bDisplay = true;
		}

		double fTopMargin = m_cTopMargin.GetValue() < 0.0 ? (m_cTopMargin.GetValue() * -1.0) : m_cTopMargin.GetValue();
		double fBottomMargin = m_cBottomMargin.GetValue() < 0.0 ? (m_cBottomMargin.GetValue() * -1.0) : m_cBottomMargin.GetValue();
		double fLeftMargin = m_cLeftMargin.GetValue() < 0.0 ? (m_cLeftMargin.GetValue() * -1.0) : m_cLeftMargin.GetValue();
		double fRightMargin = m_cRightMargin.GetValue() < 0.0 ? (m_cRightMargin.GetValue() * -1.0) : m_cRightMargin.GetValue();
		double fCmdExe = m_cCmdExeTime.GetValue() < 0.0 ? (m_cCmdExeTime.GetValue() * -1.0) : m_cCmdExeTime.GetValue();
		GetAppRegMisc()->m_strTopMargin.Format(_T("%4.2f"), fTopMargin);
		GetAppRegMisc()->m_strBottomMargin.Format(_T("%4.2f"), fBottomMargin);
		GetAppRegMisc()->m_strLeftMargin.Format(_T("%4.2f"), fLeftMargin);
		GetAppRegMisc()->m_strRightMargin.Format(_T("%4.2f"), fRightMargin);
		GetAppRegMisc()->m_strCmdExe.Format(_T("%5.3f"), fCmdExe);
		GetAppRegMisc()->m_strFontFaceName = m_strFontFaceName;
		GetAppRegMisc()->m_nFontSize = m_nFontSize;
		GetAppRegMisc()->m_nFontWeight = m_nFontWeight;
		GetAppRegMisc()->m_bFontIsStrikeOut = m_bFontIsStrikeOut;
		GetAppRegMisc()->m_bFontIsUnderline = m_bFontIsUnderline;
		GetAppRegMisc()->m_bFontIsItalic = m_bFontIsItalic;
		GetAppRegMisc()->m_strDBDrive = m_strDBDrive;
		GetAppRegMisc()->SetValues();
		m_strOldDBDrive = m_strDBDrive; 
		
		/*
		// Remove drive mapping for Epoch services in the registry
		 //if (GetAppRegMisc()->m_strDBDrive != m_strDBDrive){
			COXRegistryItem	cRegMappedDrives;
			cRegMappedDrives.SetFullRegistryItem(cRegMappedDrives.m_pszLocalMachine);
			cRegMappedDrives.SetKeyNames(ctcEPOCHMappedDrive);
			// cRegMappedDrives.SetKeyNames(_T("\\SOFTWARE\\ISI\\EpochParams\\MappedDrives"));
			// int nNumValues = cRegMappedDrives.GetNumberOfSubkeys();
			cRegMappedDrives.SetStringValue(_T(""), m_strOldDBDrive, false);
			cRegMappedDrives.SetKeyNames(m_strOldDBDrive);
			cRegMappedDrives.Delete();
		// }
		*/

		// Make sure the WorkSpaceBar retrieves the updated day directory.
		((CWorkspaceOptionsSheet*)GetParent())->m_bRefresh = TRUE;
	}
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMiscPropPage::OnApply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :        BOOL
//					Always returns TRUE.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CMiscPropPage::OnApply()
{
	((CWorkspaceOptionsSheet*)GetParent())->Apply();
	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMiscPropPage::OnInitDialog()
//
//  Description :  This member function is called in response to the
//				   WM_INITDIALOG message.
//
//  Return :       BOOL
//				   Specifies whether the application has set the input
//				   focus to one of the controls in the dialog box.
//
//  Parameters :
//				   void	-	none.

//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CMiscPropPage::OnInitDialog()
{
	CResizablePage::OnInitDialog();

	((CSpinButtonCtrl*)GetDlgItem(IDC_DAY_SPIN))->SetRange(1,366);
	m_nDayEdit = GetAppRegMisc()->m_nDOY;
	m_nOldDOY = m_nDayEdit;
	m_strDBDrive = GetAppRegMisc()->m_strDBDrive;
	m_cTopMargin.SetMask(_T("#\""));
	m_cTopMargin.SetShowLeadingZero(TRUE);
	m_cTopMargin.SetFractionalDigitCount(2);
	m_cTopMargin.SetDecimalDigitCount(1);
	m_cTopMargin.SetWindowText(GetAppRegMisc()->m_strTopMargin);
	m_cBottomMargin.SetMask(_T("#\""));
	m_cBottomMargin.SetShowLeadingZero(TRUE);
	m_cBottomMargin.SetFractionalDigitCount(2);
	m_cBottomMargin.SetDecimalDigitCount(1);
	m_cBottomMargin.SetWindowText(GetAppRegMisc()->m_strBottomMargin);
	m_cLeftMargin.SetMask(_T("#\""));
	m_cLeftMargin.SetShowLeadingZero(TRUE);
	m_cLeftMargin.SetFractionalDigitCount(2);
	m_cLeftMargin.SetDecimalDigitCount(1);
	m_cLeftMargin.SetWindowText(GetAppRegMisc()->m_strLeftMargin);
	m_cRightMargin.SetMask(_T("#\""));
	m_cRightMargin.SetShowLeadingZero(TRUE);
	m_cRightMargin.SetFractionalDigitCount(2);
	m_cRightMargin.SetDecimalDigitCount(1);
	m_cRightMargin.SetWindowText(GetAppRegMisc()->m_strLeftMargin);
	m_strFontFaceName	= GetAppRegMisc()->m_strFontFaceName;
	m_nFontSize			= GetAppRegMisc()->m_nFontSize;
	m_nFontWeight		= GetAppRegMisc()->m_nFontWeight;
	m_bFontIsStrikeOut	= GetAppRegMisc()->m_bFontIsStrikeOut;
	m_bFontIsUnderline	= GetAppRegMisc()->m_bFontIsUnderline;
	m_bFontIsItalic		= GetAppRegMisc()->m_bFontIsItalic;

	m_cFont.DeleteObject();
	HDC hDC = ::GetDC (NULL);
	m_cFont.CreateFont (-MulDiv (m_nFontSize, GetDeviceCaps (hDC, LOGPIXELSY), 72),
							  0, 0, 0, m_nFontWeight,
							  m_bFontIsItalic,
							  m_bFontIsUnderline,
							  m_bFontIsStrikeOut,
							  0,
							  0,
							  0,
							  0,
							  0,
							  m_strFontFaceName);
	::ReleaseDC (NULL, hDC);
	GetDlgItem(IDC_MISCFONT_STATIC)->SetFont(&m_cFont);

	m_cCmdExeTime.SetMask(_T("#"));
	m_cCmdExeTime.SetShowLeadingZero(TRUE);
	m_cCmdExeTime.SetFractionalDigitCount(3);
	m_cCmdExeTime.SetDecimalDigitCount(1);
	m_cCmdExeTime.SetWindowText(GetAppRegMisc()->m_strCmdExe);

	if( ((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
	{
		GetDlgItem(IDC_DAY_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_DAY_SPIN)->EnableWindow(FALSE);
		// GetDlgItem(IDC_DBDRIVE_COMBO)->EnableWindow(FALSE);
		GetDlgItem(IDC_MISCLEFT_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_MISCRIGHT_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_MISCTOP_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_MISCBOTTOM_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_MISCFONTCHANGE_BUTTON)->EnableWindow(FALSE);
		GetDlgItem(IDC_MISCCMDEXE_EDIT)->EnableWindow(FALSE);
	}

//	((CComboBox*)GetDlgItem(IDC_DBDRIVE_COMBO))->SelectString(0, m_strDBDrive);

	UpdateData(FALSE);

	AddAnchor(IDC_DAY_STATIC, TOP_LEFT, TOP_LEFT);
//	AddAnchor(IDC_DBDRIVE_STATIC, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_MISCPRINTERFONT_STATIC, TOP_LEFT, TOP_RIGHT);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMiscPropPage::OnPropsChange()
//
//  Description :  The framework calls this function when the property
//				   page�s controls are changed.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMiscPropPage::OnPropsChange()
{
	if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
		SetModified(TRUE);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMiscPropPage::OnDBDrivePropsChange()
//
//  Description :  The framework calls this function when the property
//				   page�s controls are changed.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMiscPropPage::OnDBDrivePropsChange()
{
	int nTop = ((CComboBox*)GetDlgItem(IDC_DBDRIVE_COMBO))->GetCurSel();
	((CComboBox*)GetDlgItem(IDC_DBDRIVE_COMBO))->GetLBText(nTop, m_strDBDrive);
	AfxMessageBox(_T("Close all epoch applications before continuing!"), MB_OK);
	((CSchedMgrECPApp*)AfxGetApp())->m_NewGTACS = TRUE;  //  Need to stop and restart services, since database drive has changed.
	SetModified(TRUE);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMiscPropPage::OnMiscFontChangeButton()
//
//  Description :  The framework calls this function when the property
//				   page�s controls are changed.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMiscPropPage::OnMiscFontChangeButton()
{
	LOGFONT lf;// = m_sLogfont;
	HDC hDC = ::GetDC (NULL);
	HFONT hFont = CreateFont (-MulDiv (m_nFontSize, GetDeviceCaps (hDC, LOGPIXELSY), 72),
							  0, 0, 0, m_nFontWeight,
							  m_bFontIsItalic,
							  m_bFontIsUnderline,
							  m_bFontIsStrikeOut,
							  0, 0, 0, 0, 0, m_strFontFaceName);
	::ReleaseDC (NULL, hDC);
	int nSize = ::GetObject(hFont, sizeof(LOGFONT), NULL);
	VERIFY (::GetObject (hFont, nSize, &lf));

	CFontDialog fontDlg(&lf, CF_SCREENFONTS | CF_FIXEDPITCHONLY |
									  CF_INITTOLOGFONTSTRUCT | CF_NOSCRIPTSEL);
	if( fontDlg.DoModal() == IDOK )
	{
		m_cFont.DeleteObject();
		m_cFont.CreateFontIndirect(&lf);
		GetDlgItem(IDC_MISCFONT_STATIC)->SetFont(&m_cFont);
		SetModified(TRUE);
		m_strFontFaceName	= fontDlg.GetFaceName();
		m_nFontSize			= fontDlg.GetSize() / 10;
		m_nFontWeight		= fontDlg.GetWeight();
		m_bFontIsStrikeOut	= fontDlg.IsStrikeOut();
		m_bFontIsUnderline	= fontDlg.IsUnderline();
		m_bFontIsItalic		= fontDlg.IsItalic();
		m_cFont.DeleteObject();
		HDC hDC = ::GetDC (NULL);
		m_cFont.CreateFont (-MulDiv (m_nFontSize, GetDeviceCaps (hDC, LOGPIXELSY), 72),
								  0, 0, 0, m_nFontWeight,
								  m_bFontIsItalic,
								  m_bFontIsUnderline,
								  m_bFontIsStrikeOut,
								  0, 0, 0, 0, 0, m_strFontFaceName);
		::ReleaseDC (NULL, hDC);
		GetDlgItem(IDC_MISCFONT_STATIC)->SetFont(&m_cFont);
	}
}

const int nInstObjProperties		= 0;
const int nInstObjDisplay			= 1;
const int nInstObjDisplayWidth		= 2;
const int nInstObjDisplayJustify	= 3;
const int nInstObjPrint				= 4;
const int nInstObjPrintWidth		= 5;
const int nInstObjPrintJustify		= 6;

BEGIN_MESSAGE_MAP(CInstObjPropListCtrl, CEditListCtrl)
	//{{AFX_MSG_MAP(CInstObjPropListCtrl)
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropListCtrl::CInstObjPropListCtrl()
//
//  Description :  Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CInstObjPropListCtrl::CInstObjPropListCtrl(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropListCtrl::~CInstObjPropListCtrl()
//
//  Description :  Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CInstObjPropListCtrl::~CInstObjPropListCtrl(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropListCtrl::OnLButtonDown
//
//  Description :  The framework calls this member function when the user
//				   releases the left mouse button.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					UINT nFlags	 -	Indicates whether various virtual
//									keys are down.
//					CPoint point -	Specifies the x- and y-coordinate of
//									the cursor.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CInstObjPropListCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	int index;
	CEditListCtrl::OnLButtonDown(nFlags, point);

	int colnum;

	if( (index = HitTestEx(point, &colnum)) != -1 )
	{
		UINT flag = LVIS_FOCUSED;

		if( (GetItemState(index, flag) & flag) == flag && colnum > 0 )
		{
			if( !m_bReadOnly )
			{
				if( GetWindowLong(m_hWnd, GWL_STYLE) & LVS_EDITLABELS )
				{
					if( (colnum == nInstObjDisplayJustify)||(colnum == nInstObjPrintJustify) )
					{
						CStringList lstItems;
						lstItems.AddTail(_T("Left"));
						lstItems.AddTail(_T("Center"));
						lstItems.AddTail(_T("Right"));
						CString strItem = GetItemText(index, colnum);
						int			nTopItem=-1;
						bool		bFound = false;
						POSITION	pos=lstItems.GetHeadPosition();

						while( (pos!=NULL)&&(!bFound) )
						{
							nTopItem++;

							if( lstItems.GetAt(pos) == strItem )
								bFound = true;
							else
								lstItems.GetNext(pos);
						}

						if( !bFound )
							nTopItem = 0;

						ShowInPlaceList(index, colnum, lstItems, nTopItem);
					}
					else if( (colnum == nInstObjDisplay)||(colnum == nInstObjPrint) )
					{
						CStringList lstItems;
						lstItems.AddTail(_T("Hide"));
						lstItems.AddTail(_T("Show"));
						CString strItem = GetItemText(index, colnum);
						int			nTopItem=-1;
						bool		bFound = false;
						POSITION	pos=lstItems.GetHeadPosition();

						while( (pos!=NULL)&&(!bFound) )
						{
							nTopItem++;

							if( lstItems.GetAt(pos) == strItem )
								bFound = true;
							else
								lstItems.GetNext(pos);
						}

						if( !bFound )
							nTopItem = 0;

						ShowInPlaceList(index, colnum, lstItems, nTopItem);
					}
					else
						EditSubLabel(index, colnum);
				}
			}
			else
				SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		}
		else
			SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	}
}

IMPLEMENT_DYNCREATE(CInstObjPropPage, CResizablePage)

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::CInstObjPropPage()
//
//  Description :  constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CInstObjPropPage::CInstObjPropPage() : CResizablePage(CInstObjPropPage::IDD)
{
	//{{AFX_DATA_INIT(CInstObjPropPage)
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::~CInstObjPropPage()
//
//  Description :  Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CInstObjPropPage::~CInstObjPropPage(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::DoDataExchange
//
//  Description :  This method is called by the framework to exchange and
//				   validate dialog data.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CInstObjPropPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInstObjPropPage)
	DDX_Control(pDX, IDC_INSTOBJPROP_LIST, m_cInstObjPropList);
	DDX_Control(pDX, IDC_INSTOBJTOP_EDIT, m_cTopMargin);
	DDX_Control(pDX, IDC_INSTOBJBOTTOM_EDIT, m_cBottomMargin);
	DDX_Control(pDX, IDC_INSTOBJLEFT_EDIT, m_cLeftMargin);
	DDX_Control(pDX, IDC_INSTOBJRIGHT_EDIT, m_cRightMargin);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CInstObjPropPage, CResizablePage)
	//{{AFX_MSG_MAP(CInstObjPropPage)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_INSTOBJPROP_LIST, OnEndLabelEditBasePropList)
	ON_EN_UPDATE(IDC_INSTOBJLEFT_EDIT, OnPropsChange)
	ON_CBN_SELCHANGE(IDC_INSTOBJSEL_COMBO, OnSelChangeInstObjSelCombo)
	ON_EN_UPDATE(IDC_INSTOBJRIGHT_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_INSTOBJTOP_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_INSTOBJBOTTOM_EDIT, OnPropsChange)
	ON_BN_CLICKED(IDC_INSTOBJFONTCHANGE_BUTTON, OnFontChangeButton)
	ON_EN_UPDATE(IDC_INSTOBJTOP_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_INSTOBJBOTTOM_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_INSTOBJLEFT_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_INSTOBJRIGHT_EDIT, OnPropsChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::OnInitDialog()
//
//  Description :  This member function is called in response to the
//				   WM_INITDIALOG message.
//
//  Return :
//					BOOL	-	Specifies whether the application has set
//								the input focus to one of the controls in
//								the dialog box.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CInstObjPropPage::OnInitDialog()
{
	CResizablePage::OnInitDialog();

	int	nInstObj;

	for( nInstObj=nMaxInstObj-1; nInstObj>=0; nInstObj-- )
		((CComboBox*)GetDlgItem(IDC_INSTOBJSEL_COMBO))->InsertString(0, GetAppRegInstObj()->m_strTitle[nInstObj]);

	((CComboBox*)GetDlgItem(IDC_INSTOBJSEL_COMBO))->SetCurSel(0);

	m_cTopMargin.SetMask(_T("#\""));
	m_cTopMargin.SetShowLeadingZero(TRUE);
	m_cTopMargin.SetFractionalDigitCount(2);
	m_cTopMargin.SetDecimalDigitCount(1);
	m_cTopMargin.SetWindowText(GetAppRegInstObj()->m_strTopMargin[0]);
	m_cBottomMargin.SetMask(_T("#\""));
	m_cBottomMargin.SetShowLeadingZero(TRUE);
	m_cBottomMargin.SetFractionalDigitCount(2);
	m_cBottomMargin.SetDecimalDigitCount(1);
	m_cBottomMargin.SetWindowText(GetAppRegInstObj()->m_strBottomMargin[0]);
	m_cLeftMargin.SetMask(_T("#\""));
	m_cLeftMargin.SetShowLeadingZero(TRUE);
	m_cLeftMargin.SetFractionalDigitCount(2);
	m_cLeftMargin.SetDecimalDigitCount(1);
	m_cLeftMargin.SetWindowText(GetAppRegInstObj()->m_strLeftMargin[0]);
	m_cRightMargin.SetMask(_T("#\""));
	m_cRightMargin.SetShowLeadingZero(TRUE);
	m_cRightMargin.SetFractionalDigitCount(2);
	m_cRightMargin.SetDecimalDigitCount(1);
	m_cRightMargin.SetWindowText(GetAppRegInstObj()->m_strLeftMargin[0]);

	HDC hDC = ::GetDC (NULL);

	for( nInstObj=0; nInstObj<nMaxInstObj; nInstObj++ )
	{
		m_strTopMargin[nInstObj]		= GetAppRegInstObj()->m_strTopMargin[nInstObj];
		m_strBottomMargin[nInstObj]		= GetAppRegInstObj()->m_strBottomMargin[nInstObj];
		m_strLeftMargin[nInstObj]		= GetAppRegInstObj()->m_strLeftMargin[nInstObj];
		m_strRightMargin[nInstObj]		= GetAppRegInstObj()->m_strRightMargin[nInstObj];
		m_strFontFaceName[nInstObj]		= GetAppRegInstObj()->m_strFontFaceName[nInstObj];
		m_nFontSize[nInstObj]			= GetAppRegInstObj()->m_nFontSize[nInstObj];
		m_nFontWeight[nInstObj]			= GetAppRegInstObj()->m_nFontWeight[nInstObj];
		m_bFontIsStrikeOut[nInstObj]	= GetAppRegInstObj()->m_bFontIsStrikeOut[nInstObj];
		m_bFontIsUnderline[nInstObj]	= GetAppRegInstObj()->m_bFontIsUnderline[nInstObj];
		m_bFontIsItalic[nInstObj]		= GetAppRegInstObj()->m_bFontIsItalic[nInstObj];
		m_cFont[nInstObj].DeleteObject();
		m_cFont[nInstObj].CreateFont (-MulDiv (m_nFontSize[nInstObj], GetDeviceCaps (hDC, LOGPIXELSY), 72),
								  0, 0, 0, m_nFontWeight[nInstObj],
								  m_bFontIsItalic[nInstObj],
								  m_bFontIsUnderline[nInstObj],
								  m_bFontIsStrikeOut[nInstObj],
								  0, 0, 0, 0, 0, m_strFontFaceName[nInstObj]);

		for( int nProp=0; nProp<nMaxInstObjProps[nInstObj]; nProp++ )
		{
			m_nDisplay[nInstObj][nProp] = GetAppRegInstObj()->m_nDisplay[nInstObj][nProp];
			m_nDisplayWidth[nInstObj][nProp] = GetAppRegInstObj()->m_nDisplayWidth[nInstObj][nProp];
			m_nDisplayJustify[nInstObj][nProp] = GetAppRegInstObj()->m_nDisplayJustify[nInstObj][nProp];
			m_nPrint[nInstObj][nProp] = GetAppRegInstObj()->m_nPrint[nInstObj][nProp];
			m_nPrintWidth[nInstObj][nProp] = GetAppRegInstObj()->m_nPrintWidth[nInstObj][nProp];
			m_nPrintJustify[nInstObj][nProp] = GetAppRegInstObj()->m_nPrintJustify[nInstObj][nProp];
		}
	}

	::ReleaseDC (NULL, hDC);

	m_cInstObjPropList.InsertColumn(nInstObjProperties, _T("Property"), LVCFMT_LEFT, 100);
	m_cInstObjPropList.InsertColumn(nInstObjDisplay, _T("Display"), LVCFMT_LEFT, 70);
	m_cInstObjPropList.InsertColumn(nInstObjDisplayWidth, _T("Display Width"), LVCFMT_LEFT, 90);
	m_cInstObjPropList.InsertColumn(nInstObjDisplayJustify, _T("Display Justification"), LVCFMT_LEFT, 110);
	m_cInstObjPropList.InsertColumn(nInstObjPrint, _T("Print"), LVCFMT_LEFT, 70);
	m_cInstObjPropList.InsertColumn(nInstObjPrintWidth, _T("Print Width"), LVCFMT_LEFT, 90);
	m_cInstObjPropList.InsertColumn(nInstObjPrintJustify, _T("Print Justification"), LVCFMT_LEFT, 110);

	m_nCurInstObj = -1;
	OnSelChangeInstObjSelCombo();
	UpdateData(FALSE);

	AddAnchor(IDC_INSTOBJ_STATIC, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_INSTOBJPROP_LIST, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_INSTOBJPRINTERFONT_STATIC, TOP_LEFT, TOP_RIGHT);

	if( ((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
	{
		GetDlgItem(IDC_INSTOBJLEFT_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_INSTOBJRIGHT_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_INSTOBJTOP_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_INSTOBJBOTTOM_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_INSTOBJFONTCHANGE_BUTTON)->EnableWindow(FALSE);
		m_cInstObjPropList.SetReadOnly();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::OnPropsChange()
//
//  Description :  The framework calls this function when the property
//				   page�s controls are changed.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CInstObjPropPage::OnPropsChange()
{
	if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
		SetModified(TRUE);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::Apply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CInstObjPropPage::Apply()
{
	if( m_hWnd )
	{
		OnSelChangeInstObjSelCombo();

		for( int nInstObj=0; nInstObj<nMaxInstObj; nInstObj++ )
		{
			GetAppRegInstObj()->m_strLeftMargin[nInstObj]		= m_strLeftMargin[nInstObj];
			GetAppRegInstObj()->m_strRightMargin[nInstObj]		= m_strRightMargin[nInstObj];
			GetAppRegInstObj()->m_strTopMargin[nInstObj]		= m_strTopMargin[nInstObj];
			GetAppRegInstObj()->m_strBottomMargin[nInstObj]		= m_strBottomMargin[nInstObj];
			GetAppRegInstObj()->m_strFontFaceName[nInstObj]		= m_strFontFaceName[nInstObj];
			GetAppRegInstObj()->m_nFontSize[nInstObj]			= m_nFontSize[nInstObj];
			GetAppRegInstObj()->m_nFontWeight[nInstObj]			= m_nFontWeight[nInstObj];
			GetAppRegInstObj()->m_bFontIsStrikeOut[nInstObj]	= m_bFontIsStrikeOut[nInstObj];
			GetAppRegInstObj()->m_bFontIsUnderline[nInstObj]	= m_bFontIsUnderline[nInstObj];
			GetAppRegInstObj()->m_bFontIsItalic[nInstObj]		= m_bFontIsItalic[nInstObj];

			for( int nProp=0; nProp<nMaxInstObjProps[nInstObj]; nProp++ )
			{
				GetAppRegInstObj()->m_nDisplay[nInstObj][nProp] = m_nDisplay[nInstObj][nProp];
				GetAppRegInstObj()->m_nDisplayWidth[nInstObj][nProp] = m_nDisplayWidth[nInstObj][nProp];
				GetAppRegInstObj()->m_nDisplayJustify[nInstObj][nProp] = m_nDisplayJustify[nInstObj][nProp];
				GetAppRegInstObj()->m_nPrint[nInstObj][nProp] = m_nPrint[nInstObj][nProp];
				GetAppRegInstObj()->m_nPrintWidth[nInstObj][nProp] = m_nPrintWidth[nInstObj][nProp];
				GetAppRegInstObj()->m_nPrintJustify[nInstObj][nProp] = m_nPrintJustify[nInstObj][nProp];
			}
		}

		GetAppRegInstObj()->SetValues();
	}
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::OnApply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :        BOOL
//					Always returns TRUE.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CInstObjPropPage::OnApply()
{
	((CWorkspaceOptionsSheet*)GetParent())->Apply();
	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::OnSelChangeInstObjSelCombo()
//
//  Description :  The framework calls this function when the property
//				   page�s list control is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CInstObjPropPage::OnSelChangeInstObjSelCombo()
{
	UpdateData(TRUE);

	if( m_nCurInstObj != -1 )
	{
		double fTopMargin		= m_cTopMargin.GetValue() < 0.0 ? (m_cTopMargin.GetValue() * -1.0) : m_cTopMargin.GetValue();
		double fBottomMargin	= m_cBottomMargin.GetValue() < 0.0 ? (m_cBottomMargin.GetValue() * -1.0) : m_cBottomMargin.GetValue();
		double fLeftMargin		= m_cLeftMargin.GetValue() < 0.0 ? (m_cLeftMargin.GetValue() * -1.0) : m_cLeftMargin.GetValue();
		double fRightMargin		= m_cRightMargin.GetValue() < 0.0 ? (m_cRightMargin.GetValue() * -1.0) : m_cRightMargin.GetValue();
		m_strTopMargin[m_nCurInstObj].Format(_T("%4.2f"), fTopMargin);
		m_strBottomMargin[m_nCurInstObj].Format(_T("%4.2f"), fBottomMargin);
		m_strLeftMargin[m_nCurInstObj].Format(_T("%4.2f"), fLeftMargin);
		m_strRightMargin[m_nCurInstObj].Format(_T("%4.2f"), fRightMargin);

		for( int nProp=0; nProp<nMaxInstObjProps[m_nCurInstObj]; nProp++ )
		{
			CString strTemp;
			strTemp = m_cInstObjPropList.GetItemText(nProp, nInstObjDisplay);

			if( strTemp.Compare(_T("Hide")) == 0 )
				m_nDisplay[m_nCurInstObj][nProp] = 0;
			else
				m_nDisplay[m_nCurInstObj][nProp] = 1;

			strTemp = m_cInstObjPropList.GetItemText(nProp, nInstObjDisplayWidth);
			m_nDisplayWidth[m_nCurInstObj][nProp] = _ttoi(strTemp);
			strTemp = m_cInstObjPropList.GetItemText(nProp, nInstObjDisplayJustify);

			if( strTemp.Compare(_T("Left")) == 0 )
				m_nDisplayJustify[m_nCurInstObj][nProp] = 0;
			else if (strTemp.Compare(_T("Center")) == 0)
				m_nDisplayJustify[m_nCurInstObj][nProp] = 1;
			else
				m_nDisplayJustify[m_nCurInstObj][nProp] = 2;

			strTemp = m_cInstObjPropList.GetItemText(nProp, nInstObjPrint);

			if( strTemp.Compare(_T("Hide")) == 0 )
				m_nPrint[m_nCurInstObj][nProp] = 0;
			else
				m_nPrint[m_nCurInstObj][nProp] = 1;

			strTemp = m_cInstObjPropList.GetItemText(nProp, nInstObjPrintWidth);
			m_nPrintWidth[m_nCurInstObj][nProp] = _ttoi(strTemp);
			strTemp = m_cInstObjPropList.GetItemText(nProp, nInstObjPrintJustify);

			if( strTemp.Compare(_T("Left")) == 0 )
				m_nPrintJustify[m_nCurInstObj][nProp] = 0;
			else if( strTemp.Compare(_T("Center")) == 0 )
				m_nPrintJustify[m_nCurInstObj][nProp] = 1;
			else
				m_nPrintJustify[m_nCurInstObj][nProp] = 2;
		}
	}

	m_cInstObjPropList.DeleteAllItems();
	m_nCurInstObj = ((CComboBox*)GetDlgItem(IDC_INSTOBJSEL_COMBO))->GetCurSel();

	GetDlgItem(IDC_INSTOBJFONT_STATIC)->SetFont(&m_cFont[m_nCurInstObj]);

	CString strInstObj;
	((CComboBox*)GetDlgItem(IDC_INSTOBJSEL_COMBO))->GetLBText(m_nCurInstObj, strInstObj);

	GetDlgItem(IDC_INSTOBJPRINTERMARGINS_STATIC)->SetWindowText(strInstObj + _T(" File Printer Margins"));
	GetDlgItem(IDC_INSTOBJPRINTERFONT_STATIC)->SetWindowText(strInstObj + _T(" File Printer Font"));

	for( int nProperty=0; nProperty<nMaxInstObjProps[m_nCurInstObj]; nProperty++ )
	{
		CString strTemp;
		m_cInstObjPropList.InsertItem(nProperty, GetAppRegInstObj()->m_strPropTitle[m_nCurInstObj][nProperty]);

		switch( m_nDisplay[m_nCurInstObj][nProperty] )
		{
			case 0:	strTemp = _T("Hide");	break;
			case 1:	strTemp = _T("Show");	break;
		}

		m_cInstObjPropList.SetItemText(nProperty, nInstObjDisplay, strTemp);
		strTemp.Format(_T("%d"), m_nDisplayWidth[m_nCurInstObj][nProperty]);
		m_cInstObjPropList.SetItemText(nProperty, nInstObjDisplayWidth, strTemp);

		switch( m_nDisplayJustify[m_nCurInstObj][nProperty] )
		{
			case 0:	strTemp = _T("Left");	break;
			case 1:	strTemp = _T("Center");	break;
			case 2:	strTemp = _T("Right");	break;
		}

		m_cInstObjPropList.SetItemText(nProperty, nInstObjDisplayJustify, strTemp);

		switch( m_nPrint[m_nCurInstObj][nProperty] )
		{
			case 0:	strTemp = _T("Hide");	break;
			case 1:	strTemp = _T("Show");	break;
		}

		m_cInstObjPropList.SetItemText(nProperty, nInstObjPrint, strTemp);
		strTemp.Format(_T("%d"), m_nPrintWidth[m_nCurInstObj][nProperty]);
		m_cInstObjPropList.SetItemText(nProperty, nInstObjPrintWidth, strTemp);

		switch( m_nPrintJustify[m_nCurInstObj][nProperty] )
		{
			case 0:	strTemp = _T("Left");	break;
			case 1:	strTemp = _T("Center");	break;
			case 2:	strTemp = _T("Right");	break;
		}

		m_cInstObjPropList.SetItemText(nProperty, nInstObjPrintJustify, strTemp);
	}

	m_cTopMargin.SetWindowText(m_strTopMargin[m_nCurInstObj]);
	m_cBottomMargin.SetWindowText(m_strBottomMargin[m_nCurInstObj]);
	m_cLeftMargin.SetWindowText(m_strLeftMargin[m_nCurInstObj]);
	m_cRightMargin.SetWindowText(m_strRightMargin[m_nCurInstObj]);

	UpdateData(FALSE);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::OnEndLabelEditBasePropList
//
//  Description :  The framework calls this function when the property
//				   page�s list control is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					NMHDR* pNMHDR		-	Pointer to a NMHDR object.
//					LRESULT* pResult	-	Pointer to a LRESULT object.
//											Always set to 0.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CInstObjPropPage::OnEndLabelEditBasePropList(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO* pDispInfo	= (LV_DISPINFO*)pNMHDR;
	CString	strError;
	CString strEntry		= pDispInfo->item.pszText;
//	BOOL	bValid			= TRUE;

	switch( pDispInfo->item.iSubItem )
	{
		case (nInstObjDisplayWidth)	:strError = CheckWidth(pDispInfo);			break;
		case (nInstObjPrintWidth)	:strError = CheckWidth(pDispInfo);			break;
		default:					strError.Empty();							break;
	}

	if( strError.IsEmpty() )
	{
		m_cInstObjPropList.OnEndLabelEdit(pNMHDR, pResult);

		if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
			SetModified(TRUE);
	}
	else
		GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);

	*pResult = 0;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::CheckWidth
//
//  Description :  Checks if the width entered is valid.  Returns an error
//					message if width is outside of range.
//
//  Return :
//					CString	-	Error message
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//												structure
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CInstObjPropPage::CheckWidth(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0 )
		strError = _T("Column width must be a valid integer number in the range of 0 to 999.");

	if( (nEntry < 0) || (nEntry > 999) )
		strError = _T("Column width must be a valid integer number in the range of 0 to 999.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CInstObjPropPage::OnFontChangeButton()
//
//  Description :  Changes the font for the instrument object.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CInstObjPropPage::OnFontChangeButton()
{
	LOGFONT lf;// = m_sLogfont;
	int		nSize = ::GetObject(m_cFont[m_nCurInstObj], sizeof(LOGFONT), NULL);
	VERIFY (::GetObject (m_cFont[m_nCurInstObj], nSize, &lf));

	CFontDialog fontDlg(&lf, CF_SCREENFONTS | CF_FIXEDPITCHONLY | CF_INITTOLOGFONTSTRUCT | CF_NOSCRIPTSEL);

	if( fontDlg.DoModal() == IDOK )
	{
		m_cFont[m_nCurInstObj].DeleteObject();
		m_cFont[m_nCurInstObj].CreateFontIndirect(&lf);
		GetDlgItem(IDC_INSTOBJFONT_STATIC)->SetFont(&m_cFont[m_nCurInstObj]);
		SetModified(TRUE);
		m_strFontFaceName[m_nCurInstObj]	= fontDlg.GetFaceName();
		m_nFontSize[m_nCurInstObj]			= fontDlg.GetSize() / 10;
		m_nFontWeight[m_nCurInstObj]		= fontDlg.GetWeight();
		m_bFontIsStrikeOut[m_nCurInstObj]	= fontDlg.IsStrikeOut();
		m_bFontIsUnderline[m_nCurInstObj]	= fontDlg.IsUnderline();
		m_bFontIsItalic[m_nCurInstObj]		= fontDlg.IsItalic();
	}
}

const int nMapObjProperties		= 0;
const int nMapObjDisplay		= 1;
const int nMapObjDisplayWidth	= 2;
const int nMapObjDisplayJustify	= 3;
const int nMapObjPrint			= 4;
const int nMapObjPrintWidth		= 5;
const int nMapObjPrintJustify	= 6;

BEGIN_MESSAGE_MAP(CMapObjPropListCtrl, CEditListCtrl)
	//{{AFX_MSG_MAP(CMapObjPropListCtrl)
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropListCtrl::CMapObjPropListCtrl()
//
//  Description :   Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CMapObjPropListCtrl::CMapObjPropListCtrl(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropListCtrl::~CMapObjPropListCtrl()
//
//  Description :  Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CMapObjPropListCtrl::~CMapObjPropListCtrl(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropListCtrl::OnLButtonDown
//
//  Description :  The framework calls this member function when the user
//				   presses the left mouse button.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					UINT nFlags		-	Indicates whether various virtual
//										keys are down.
//					CPoint point	-	Specifies the x- and y-coordinate
//										of the cursor.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMapObjPropListCtrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	int index;
	CEditListCtrl::OnLButtonDown(nFlags, point);

	int colnum;

	if( (index = HitTestEx(point, &colnum)) != -1 )
	{
		UINT flag = LVIS_FOCUSED;

		if( (GetItemState(index, flag) & flag) == flag && colnum > 0 )
		{
			if( !m_bReadOnly )
			{
				if( GetWindowLong(m_hWnd, GWL_STYLE) & LVS_EDITLABELS )
				{
					if( (colnum == nMapObjDisplayJustify)||(colnum == nMapObjPrintJustify) )
					{
						CStringList lstItems;
						lstItems.AddTail(_T("Left"));
						lstItems.AddTail(_T("Center"));
						lstItems.AddTail(_T("Right"));
						CString strItem = GetItemText(index, colnum);
						int			nTopItem=-1;
						bool		bFound = false;
						POSITION	pos=lstItems.GetHeadPosition();

						while( (pos!=NULL)&&(!bFound) )
						{
							nTopItem++;

							if( lstItems.GetAt(pos) == strItem )
								bFound = true;
							else
								lstItems.GetNext(pos);
						}

						if( !bFound )
							nTopItem = 0;

						ShowInPlaceList(index, colnum, lstItems, nTopItem);
					}
					else if( (colnum == nMapObjDisplay)||(colnum == nMapObjPrint) )
					{
						CStringList lstItems;
						lstItems.AddTail(_T("Hide"));
						lstItems.AddTail(_T("Show"));
						CString strItem = GetItemText(index, colnum);
						int			nTopItem=-1;
						bool		bFound = false;
						POSITION	pos=lstItems.GetHeadPosition();

						while( (pos!=NULL)&&(!bFound) )
						{
							nTopItem++;

							if( lstItems.GetAt(pos) == strItem )
								bFound = true;
							else
								lstItems.GetNext(pos);
						}

						if( !bFound )
							nTopItem = 0;

						ShowInPlaceList(index, colnum, lstItems, nTopItem);
					}
					else
						EditSubLabel(index, colnum);
				}
			}
			else
				SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		}
		else
			SetItemState(index, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
	}
}

IMPLEMENT_DYNCREATE(CMapObjPropPage, CResizablePage)

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropPage::CMapObjPropPage
//
//  Description :  Constructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CMapObjPropPage::CMapObjPropPage() : CResizablePage(CMapObjPropPage::IDD)
{
	//{{AFX_DATA_INIT(CMapObjPropPage)
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropPage::~CMapObjPropPage
//
//  Description :   Destructor
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CMapObjPropPage::~CMapObjPropPage()
{
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropPage::DoDataExchange
//
//  Description :  Called by the framework to exchange and validate
//				   dialog data.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMapObjPropPage::DoDataExchange(CDataExchange* pDX)
{
	CResizablePage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMapObjPropPage)
	DDX_Control(pDX, IDC_MAPOBJPROP_LIST, m_cMapObjPropList);
	DDX_Control(pDX, IDC_MAPOBJTOP_EDIT, m_cTopMargin);
	DDX_Control(pDX, IDC_MAPOBJBOTTOM_EDIT, m_cBottomMargin);
	DDX_Control(pDX, IDC_MAPOBJLEFT_EDIT, m_cLeftMargin);
	DDX_Control(pDX, IDC_MAPOBJRIGHT_EDIT, m_cRightMargin);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMapObjPropPage, CResizablePage)
	//{{AFX_MSG_MAP(CMapObjPropPage)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_MAPOBJPROP_LIST, OnEndLabelEditBasePropList)
	ON_EN_UPDATE(IDC_MAPOBJLEFT_EDIT, OnPropsChange)
	ON_CBN_SELCHANGE(IDC_MAPOBJSEL_COMBO, OnSelChangeMapObjSelCombo)
	ON_EN_UPDATE(IDC_MAPOBJRIGHT_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_MAPOBJTOP_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_MAPOBJBOTTOM_EDIT, OnPropsChange)
	ON_BN_CLICKED(IDC_MAPOBJFONTCHANGE_BUTTON, OnFontChangeButton)
	ON_EN_UPDATE(IDC_MAPOBJTOP_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_MAPOBJBOTTOM_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_MAPOBJLEFT_EDIT, OnPropsChange)
	ON_EN_UPDATE(IDC_MAPOBJRIGHT_EDIT, OnPropsChange)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropPage::OnInitDialog()
//
//  Description :  The framework calls this function when the property
//				   page�s dialog is initialized.
//
//  Return :        BOOL
//						Always returns TRUE.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CMapObjPropPage::OnInitDialog()
{
	CResizablePage::OnInitDialog();

	int	nMapObj;

	for( nMapObj=nMaxMapObj-1; nMapObj>=0; nMapObj-- )
		((CComboBox*)GetDlgItem(IDC_MAPOBJSEL_COMBO))->InsertString(0, GetAppRegMapObj()->m_strTitle[nMapObj]);

	((CComboBox*)GetDlgItem(IDC_MAPOBJSEL_COMBO))->SetCurSel(0);

	m_cTopMargin.SetMask(_T("#\""));
	m_cTopMargin.SetShowLeadingZero(TRUE);
	m_cTopMargin.SetFractionalDigitCount(2);
	m_cTopMargin.SetDecimalDigitCount(1);
	m_cTopMargin.SetWindowText(GetAppRegMapObj()->m_strTopMargin[0]);
	m_cBottomMargin.SetMask(_T("#\""));
	m_cBottomMargin.SetShowLeadingZero(TRUE);
	m_cBottomMargin.SetFractionalDigitCount(2);
	m_cBottomMargin.SetDecimalDigitCount(1);
	m_cBottomMargin.SetWindowText(GetAppRegMapObj()->m_strBottomMargin[0]);
	m_cLeftMargin.SetMask(_T("#\""));
	m_cLeftMargin.SetShowLeadingZero(TRUE);
	m_cLeftMargin.SetFractionalDigitCount(2);
	m_cLeftMargin.SetDecimalDigitCount(1);
	m_cLeftMargin.SetWindowText(GetAppRegMapObj()->m_strLeftMargin[0]);
	m_cRightMargin.SetMask(_T("#\""));
	m_cRightMargin.SetShowLeadingZero(TRUE);
	m_cRightMargin.SetFractionalDigitCount(2);
	m_cRightMargin.SetDecimalDigitCount(1);
	m_cRightMargin.SetWindowText(GetAppRegMapObj()->m_strLeftMargin[0]);

	HDC hDC = ::GetDC (NULL);

	for( nMapObj=0; nMapObj<nMaxMapObj; nMapObj++ )
	{
		m_strTopMargin[nMapObj]			= GetAppRegMapObj()->m_strTopMargin[nMapObj];
		m_strBottomMargin[nMapObj]		= GetAppRegMapObj()->m_strBottomMargin[nMapObj];
		m_strLeftMargin[nMapObj]		= GetAppRegMapObj()->m_strLeftMargin[nMapObj];
		m_strRightMargin[nMapObj]		= GetAppRegMapObj()->m_strRightMargin[nMapObj];
		m_strFontFaceName[nMapObj]		= GetAppRegMapObj()->m_strFontFaceName[nMapObj];
		m_nFontSize[nMapObj]			= GetAppRegMapObj()->m_nFontSize[nMapObj];
		m_nFontWeight[nMapObj]			= GetAppRegMapObj()->m_nFontWeight[nMapObj];
		m_bFontIsStrikeOut[nMapObj]		= GetAppRegMapObj()->m_bFontIsStrikeOut[nMapObj];
		m_bFontIsUnderline[nMapObj]		= GetAppRegMapObj()->m_bFontIsUnderline[nMapObj];
		m_bFontIsItalic[nMapObj]		= GetAppRegMapObj()->m_bFontIsItalic[nMapObj];
		m_cFont[nMapObj].DeleteObject();
		m_cFont[nMapObj].CreateFont (-MulDiv (m_nFontSize[nMapObj], GetDeviceCaps (hDC, LOGPIXELSY), 72),
								  0, 0, 0, m_nFontWeight[nMapObj],
								  m_bFontIsItalic[nMapObj],
								  m_bFontIsUnderline[nMapObj],
								  m_bFontIsStrikeOut[nMapObj],
								  0, 0, 0, 0, 0, m_strFontFaceName[nMapObj]);


		for( int nProp=0; nProp<nMaxMapObjProps[nMapObj]; nProp++ )
		{
			m_nDisplay[nMapObj][nProp] = GetAppRegMapObj()->m_nDisplay[nMapObj][nProp];
			m_nDisplayWidth[nMapObj][nProp] = GetAppRegMapObj()->m_nDisplayWidth[nMapObj][nProp];
			m_nDisplayJustify[nMapObj][nProp] = GetAppRegMapObj()->m_nDisplayJustify[nMapObj][nProp];
			m_nPrint[nMapObj][nProp] = GetAppRegMapObj()->m_nPrint[nMapObj][nProp];
			m_nPrintWidth[nMapObj][nProp] = GetAppRegMapObj()->m_nPrintWidth[nMapObj][nProp];
			m_nPrintJustify[nMapObj][nProp] = GetAppRegMapObj()->m_nPrintJustify[nMapObj][nProp];
		}
	}

	::ReleaseDC (NULL, hDC);

	m_cMapObjPropList.InsertColumn(nMapObjProperties, _T("Property"), LVCFMT_LEFT, 100);
	m_cMapObjPropList.InsertColumn(nMapObjDisplay, _T("Display"), LVCFMT_LEFT, 70);
	m_cMapObjPropList.InsertColumn(nMapObjDisplayWidth, _T("Display Width"), LVCFMT_LEFT, 90);
	m_cMapObjPropList.InsertColumn(nMapObjDisplayJustify, _T("Display Justification"), LVCFMT_LEFT, 110);
	m_cMapObjPropList.InsertColumn(nMapObjPrint, _T("Print"), LVCFMT_LEFT, 70);
	m_cMapObjPropList.InsertColumn(nMapObjPrintWidth, _T("Print Width"), LVCFMT_LEFT, 90);
	m_cMapObjPropList.InsertColumn(nMapObjPrintJustify, _T("Print Justification"), LVCFMT_LEFT, 110);

	m_nCurMapObj = -1;
	OnSelChangeMapObjSelCombo();
	UpdateData(FALSE);

	AddAnchor(IDC_MAPOBJ_STATIC, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_MAPOBJPROP_LIST, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_MAPOBJPRINTERFONT_STATIC, TOP_LEFT, TOP_RIGHT);

	if( ((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
	{
		GetDlgItem(IDC_MAPOBJLEFT_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_MAPOBJRIGHT_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_MAPOBJTOP_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_MAPOBJBOTTOM_EDIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_MAPOBJFONTCHANGE_BUTTON)->EnableWindow(FALSE);
		m_cMapObjPropList.SetReadOnly();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropPage::OnPropsChange()
//
//  Description :  The framework calls this function when the property
//				   page�s controls are changed.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMapObjPropPage::OnPropsChange()
{
	if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
		SetModified(TRUE);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceOptionsSheet::Apply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMapObjPropPage::Apply()
{
	if( m_hWnd )
	{
		OnSelChangeMapObjSelCombo();

		for( int nMapObj=0; nMapObj<nMaxMapObj; nMapObj++ )
		{
			GetAppRegMapObj()->m_strLeftMargin[nMapObj]		= m_strLeftMargin[nMapObj];
			GetAppRegMapObj()->m_strRightMargin[nMapObj]	= m_strRightMargin[nMapObj];
			GetAppRegMapObj()->m_strTopMargin[nMapObj]		= m_strTopMargin[nMapObj];
			GetAppRegMapObj()->m_strBottomMargin[nMapObj]	= m_strBottomMargin[nMapObj];
			GetAppRegMapObj()->m_strFontFaceName[nMapObj]	= m_strFontFaceName[nMapObj];
			GetAppRegMapObj()->m_nFontSize[nMapObj]			= m_nFontSize[nMapObj];
			GetAppRegMapObj()->m_nFontWeight[nMapObj]		= m_nFontWeight[nMapObj];
			GetAppRegMapObj()->m_bFontIsStrikeOut[nMapObj]	= m_bFontIsStrikeOut[nMapObj];
			GetAppRegMapObj()->m_bFontIsUnderline[nMapObj]	= m_bFontIsUnderline[nMapObj];
			GetAppRegMapObj()->m_bFontIsItalic[nMapObj]		= m_bFontIsItalic[nMapObj];

			for( int nProp=0; nProp<nMaxMapObjProps[nMapObj]; nProp++ )
			{
				GetAppRegMapObj()->m_nDisplay[nMapObj][nProp] = m_nDisplay[nMapObj][nProp];
				GetAppRegMapObj()->m_nDisplayWidth[nMapObj][nProp] = m_nDisplayWidth[nMapObj][nProp];
				GetAppRegMapObj()->m_nDisplayJustify[nMapObj][nProp] = m_nDisplayJustify[nMapObj][nProp];
				GetAppRegMapObj()->m_nPrint[nMapObj][nProp] = m_nPrint[nMapObj][nProp];
				GetAppRegMapObj()->m_nPrintWidth[nMapObj][nProp] = m_nPrintWidth[nMapObj][nProp];
				GetAppRegMapObj()->m_nPrintJustify[nMapObj][nProp] = m_nPrintJustify[nMapObj][nProp];
			}
		}

		GetAppRegMapObj()->SetValues();
	}
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropPage::OnApply()
//
//  Description :  Applies the current values to the underlying objects
//				   associated with the property page as previously
//				   passed to.
//
//  Return :        BOOL
//					Always returns TRUE.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CMapObjPropPage::OnApply()
{
	((CWorkspaceOptionsSheet*)GetParent())->Apply();
	return TRUE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropPage::OnSelChangeMapObjSelCombo()
//
//  Description :  The framework calls this function when the property
//				   page�s combo box is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMapObjPropPage::OnSelChangeMapObjSelCombo()
{
	UpdateData(TRUE);

	if( m_nCurMapObj != -1 )
	{
		double fTopMargin		= m_cTopMargin.GetValue() < 0.0 ? (m_cTopMargin.GetValue() * -1.0) : m_cTopMargin.GetValue();
		double fBottomMargin	= m_cBottomMargin.GetValue() < 0.0 ? (m_cBottomMargin.GetValue() * -1.0) : m_cBottomMargin.GetValue();
		double fLeftMargin		= m_cLeftMargin.GetValue() < 0.0 ? (m_cLeftMargin.GetValue() * -1.0) : m_cLeftMargin.GetValue();
		double fRightMargin		= m_cRightMargin.GetValue() < 0.0 ? (m_cRightMargin.GetValue() * -1.0) : m_cRightMargin.GetValue();
		m_strTopMargin[m_nCurMapObj].Format(_T("%4.2f"), fTopMargin);
		m_strBottomMargin[m_nCurMapObj].Format(_T("%4.2f"), fBottomMargin);
		m_strLeftMargin[m_nCurMapObj].Format(_T("%4.2f"), fLeftMargin);
		m_strRightMargin[m_nCurMapObj].Format(_T("%4.2f"), fRightMargin);

		for( int nProp=0; nProp<nMaxMapObjProps[m_nCurMapObj]; nProp++ )
		{
			CString strTemp;
			strTemp = m_cMapObjPropList.GetItemText(nProp, nMapObjDisplay);

			if( strTemp.Compare(_T("Hide")) == 0 )
				m_nDisplay[m_nCurMapObj][nProp] = 0;
			else
				m_nDisplay[m_nCurMapObj][nProp] = 1;

			strTemp = m_cMapObjPropList.GetItemText(nProp, nMapObjDisplayWidth);
			m_nDisplayWidth[m_nCurMapObj][nProp] = _ttoi(strTemp);
			strTemp = m_cMapObjPropList.GetItemText(nProp, nMapObjDisplayJustify);

			if( strTemp.Compare(_T("Left")) == 0 )
				m_nDisplayJustify[m_nCurMapObj][nProp] = 0;
			else if( strTemp.Compare(_T("Center")) == 0 )
				m_nDisplayJustify[m_nCurMapObj][nProp] = 1;
			else
				m_nDisplayJustify[m_nCurMapObj][nProp] = 2;

			strTemp = m_cMapObjPropList.GetItemText(nProp, nMapObjPrint);

			if( strTemp.Compare(_T("Hide")) == 0 )
				m_nPrint[m_nCurMapObj][nProp] = 0;
			else
				m_nPrint[m_nCurMapObj][nProp] = 1;

			strTemp = m_cMapObjPropList.GetItemText(nProp, nMapObjPrintWidth);
			m_nPrintWidth[m_nCurMapObj][nProp] = _ttoi(strTemp);
			strTemp = m_cMapObjPropList.GetItemText(nProp, nMapObjPrintJustify);

			if( strTemp.Compare(_T("Left")) == 0 )
				m_nPrintJustify[m_nCurMapObj][nProp] = 0;
			else if( strTemp.Compare(_T("Center")) == 0 )
				m_nPrintJustify[m_nCurMapObj][nProp] = 1;
			else
				m_nPrintJustify[m_nCurMapObj][nProp] = 2;
		}
	}

	m_cMapObjPropList.DeleteAllItems();
	m_nCurMapObj = ((CComboBox*)GetDlgItem(IDC_MAPOBJSEL_COMBO))->GetCurSel();

	GetDlgItem(IDC_MAPOBJFONT_STATIC)->SetFont(&m_cFont[m_nCurMapObj]);

	CString strMapObj;
	((CComboBox*)GetDlgItem(IDC_MAPOBJSEL_COMBO))->GetLBText(m_nCurMapObj, strMapObj);

	GetDlgItem(IDC_MAPOBJPRINTERMARGINS_STATIC)->SetWindowText(strMapObj + _T(" File Printer Margins"));
	GetDlgItem(IDC_MAPOBJPRINTERFONT_STATIC)->SetWindowText(strMapObj + _T(" File Printer Font"));

	for( int nProperty=0; nProperty<nMaxMapObjProps[m_nCurMapObj]; nProperty++ )
	{
		CString strTemp;
		m_cMapObjPropList.InsertItem(nProperty, GetAppRegMapObj()->m_strPropTitle[m_nCurMapObj][nProperty]);

		switch( m_nDisplay[m_nCurMapObj][nProperty] )
		{
			case 0:	strTemp = _T("Hide");	break;
			case 1:	strTemp = _T("Show");	break;
		}

		m_cMapObjPropList.SetItemText(nProperty, nMapObjDisplay, strTemp);
		strTemp.Format(_T("%d"), m_nDisplayWidth[m_nCurMapObj][nProperty]);
		m_cMapObjPropList.SetItemText(nProperty, nMapObjDisplayWidth, strTemp);

		switch( m_nDisplayJustify[m_nCurMapObj][nProperty] )
		{
			case 0:	strTemp = _T("Left");	break;
			case 1:	strTemp = _T("Center");	break;
			case 2:	strTemp = _T("Right");	break;
		}

		m_cMapObjPropList.SetItemText(nProperty, nMapObjDisplayJustify, strTemp);

		switch( m_nPrint[m_nCurMapObj][nProperty] )
		{
			case 0:	strTemp = _T("Hide");	break;
			case 1:	strTemp = _T("Show");	break;
		}

		m_cMapObjPropList.SetItemText(nProperty, nMapObjPrint, strTemp);
		strTemp.Format(_T("%d"), m_nPrintWidth[m_nCurMapObj][nProperty]);
		m_cMapObjPropList.SetItemText(nProperty, nMapObjPrintWidth, strTemp);

		switch( m_nPrintJustify[m_nCurMapObj][nProperty] )
		{
			case 0:	strTemp = _T("Left");	break;
			case 1:	strTemp = _T("Center");	break;
			case 2:	strTemp = _T("Right");	break;
		}

		m_cMapObjPropList.SetItemText(nProperty, nMapObjPrintJustify, strTemp);
	}

	m_cTopMargin.SetWindowText(m_strTopMargin[m_nCurMapObj]);
	m_cBottomMargin.SetWindowText(m_strBottomMargin[m_nCurMapObj]);
	m_cLeftMargin.SetWindowText(m_strLeftMargin[m_nCurMapObj]);
	m_cRightMargin.SetWindowText(m_strRightMargin[m_nCurMapObj]);

	UpdateData(FALSE);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CMapObjPropPage::OnEndLabelEditBasePropList
//
//  Description :  The framework calls this function when the property
//				   page�s list control is selected.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					NMHDR* pNMHDR		-	Pointer to a NMHDR object.
//					LRESULT* pResult	-	Pointer to a LRESULT object.
//											Always set to 0.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMapObjPropPage::OnEndLabelEditBasePropList(NMHDR* pNMHDR, LRESULT* pResult)
{
	LV_DISPINFO*	pDispInfo	= (LV_DISPINFO*)pNMHDR;
	CString			strError;
	CString			strEntry	= pDispInfo->item.pszText;
//	BOOL			bValid		= TRUE;

	switch( pDispInfo->item.iSubItem )
	{
		case (nMapObjDisplayWidth)	:strError = CheckWidth(pDispInfo);	break;
		case (nMapObjPrintWidth)	:strError = CheckWidth(pDispInfo);	break;
		default						:strError.Empty();					break;
	}

	if( strError.IsEmpty() )
	{
		m_cMapObjPropList.OnEndLabelEdit(pNMHDR, pResult);

		if( !((CWorkspaceOptionsSheet*)GetParent())->m_bReadOnly )
			SetModified(TRUE);
	}
	else
		GetAppOutputBar()->OutputToEvents(strError, COutputBar::WARNING);

	*pResult = 0;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CString CMapObjPropPage::CheckWidth
//
//  Description :  Checks if the width entered is valid.  Returns an error
//					message if width is outside of range.
//
//  Return :
//					CString	-	Error message
//
//  Parameters :
//					LV_DISPINFO* pDispInfo	-	Pointer to a LV_DISPINFO
//						structure
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
CString CMapObjPropPage::CheckWidth(LV_DISPINFO* pDispInfo)
{
	CString	strError;
	strError.Empty();
	CString strEntry= pDispInfo->item.pszText;
	long	nEntry	= _wtol(pDispInfo->item.pszText);
	TCHAR	buffer[20];
	_ltow_s(nEntry, buffer, 10);

	if( strEntry.Compare(buffer) != 0 )
		strError = _T("Column width must be a valid integer number in the range of 0 to 999.");

	if( (nEntry < 0) || (nEntry > 999) )
		strError = _T("Column width must be a valid integer number in the range of 0 to 999.");

	return strError;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     void CMapObjPropPage::OnFontChangeButton()
//
//  Description :    Changes the font for the map object.
//
//  Return :
//					void	-	none.
//
//  Parameters :
//					void	-	none.
//
//  Notes :
//
//////////////////////////////////////////////////////////////////////////
******************/
void CMapObjPropPage::OnFontChangeButton()
{
	LOGFONT lf;// = m_sLogfont;
	int		nSize = ::GetObject(m_cFont[m_nCurMapObj], sizeof(LOGFONT), NULL);
	VERIFY (::GetObject (m_cFont[m_nCurMapObj], nSize, &lf));

	CFontDialog fontDlg(&lf, CF_SCREENFONTS | CF_FIXEDPITCHONLY | CF_INITTOLOGFONTSTRUCT | CF_NOSCRIPTSEL);

	if( fontDlg.DoModal() == IDOK )
	{
		m_cFont[m_nCurMapObj].DeleteObject();
		m_cFont[m_nCurMapObj].CreateFontIndirect(&lf);
		GetDlgItem(IDC_MAPOBJFONT_STATIC)->SetFont(&m_cFont[m_nCurMapObj]);
		SetModified(TRUE);
		m_strFontFaceName[m_nCurMapObj]	= fontDlg.GetFaceName();
		m_nFontSize[m_nCurMapObj]		= fontDlg.GetSize() / 10;
		m_nFontWeight[m_nCurMapObj]		= fontDlg.GetWeight();
		m_bFontIsStrikeOut[m_nCurMapObj]= fontDlg.IsStrikeOut();
		m_bFontIsUnderline[m_nCurMapObj]= fontDlg.IsUnderline();
		m_bFontIsItalic[m_nCurMapObj]	= fontDlg.IsItalic();
	}
}
