#if !defined(AFX_WORKSPACEOPTIONS_H__46DB6903_E337_11D4_8013_00609704053C__INCLUDED_)
#define AFX_WORKSPACEOPTIONS_H__46DB6903_E337_11D4_8013_00609704053C__INCLUDED_

#include "EditListCtrl.h"
#include "OXEdit.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Revision History:													   
// Manan Dalal - July-August 2014										   
// PR0000951 - Enhance SchedMgrECP with RSO Sectors						   
// Release in Build16.2

class CSCPropListCtrl : public CEditListCtrl
{
public:
	CSCPropListCtrl();
	virtual ~CSCPropListCtrl();
protected:
	//{{AFX_MSG(CSCPropListCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CGTACSPropListCtrl : public CEditListCtrl
{
public:
	CGTACSPropListCtrl();
	virtual ~CGTACSPropListCtrl();
protected:
	//{{AFX_MSG(CGTACSPropListCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CFolderPropListCtrl : public CEditListCtrl
{
public:
	CFolderPropListCtrl();
	virtual ~CFolderPropListCtrl();
protected:
	//{{AFX_MSG(CFolderPropListCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CInstObjPropListCtrl : public CEditListCtrl
{
public:
	CInstObjPropListCtrl();
	virtual ~CInstObjPropListCtrl();
protected:
	//{{AFX_MSG(CInstObjPropListCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CMapObjPropListCtrl : public CEditListCtrl
{
public:
	CMapObjPropListCtrl();
	virtual ~CMapObjPropListCtrl();
protected:
	//{{AFX_MSG(CMapObjPropListCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CSCPropPage : public CResizablePage
{
	DECLARE_DYNCREATE(CSCPropPage)

// Construction
public:
	CSCPropPage();
	~CSCPropPage();
	void Apply();
// Dialog Data
	//{{AFX_DATA(CSCPropPage)
	enum { IDD = IDD_SCPROP_DIALOG };
	CSCPropListCtrl	m_cSCPropList;
	//}}AFX_DATA
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CSCPropPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CSCPropPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnPropChange();
	afx_msg void OnEndLabelEditBasePropList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString CheckDirectory(LV_DISPINFO* pDispInfo);
	CString CheckAbbrev(LV_DISPINFO* pDispInfo);
	CString CheckOATSID(LV_DISPINFO* pDispInfo);
	CString CheckFrameSize(LV_DISPINFO* pDispInfo);
	CString CheckFrameSRSOStart(LV_DISPINFO* pDispInfo);
	CString CheckStarSize(LV_DISPINFO* pDispInfo);
	CString CheckSTOSize(LV_DISPINFO* pDispInfo);
	CString CheckSTORTCSSize(LV_DISPINFO* pDispInfo);
	CString CheckSTORTCSNum(LV_DISPINFO* pDispInfo);
	CString CheckSKBSize(LV_DISPINFO* pDispInfo);
	CString CheckSKBSegSize(LV_DISPINFO* pDispInfo);
	CString CheckCTCUID(LV_DISPINFO* pDispInfo);
	CString CheckDatabaseName(LV_DISPINFO* pDispInfo);
	CString CheckMaxCmdGap(LV_DISPINFO* pDispInfo);
	CString CheckPreHKGap(LV_DISPINFO* pDispInfo);
	CString CheckRetryCnt(LV_DISPINFO* pDispInfo);
	CString CheckSTOGotoTopCmd(LV_DISPINFO* pDispInfo);
	CString CheckRTCSCmdFile(LV_DISPINFO* pDispInfo);
	CString CheckPaceCmdFile(LV_DISPINFO* pDispInfo);
	CString CheckFrameMiniSchedFile(LV_DISPINFO* pDispInfo);
	CString CheckFrameIOMiniSchedFile(LV_DISPINFO* pDispInfo);
	CString CheckStarMiniSchedFile(LV_DISPINFO* pDispInfo);
	CString CheckStarIOMiniSchedFile(LV_DISPINFO* pDispInfo);
	CString CheckSFCSMiniSchedFile(LV_DISPINFO* pDispInfo);
};

class CGTACSPropPage : public CResizablePage
{
	DECLARE_DYNCREATE(CGTACSPropPage)

// Construction
public:
	CGTACSPropPage();
	~CGTACSPropPage();
	void Apply();
// Dialog Data
	//{{AFX_DATA(CGTACSPropPage)
	enum { IDD = IDD_GTACSPROP_DIALOG };
	CGTACSPropListCtrl	m_cGTACSPropList;
	//}}AFX_DATA
protected:
	CString	m_strAbbrev[nMaxSites][nMaxGTACSperSite];
	CString	m_strProcShare[nMaxSites][nMaxGTACSperSite];
	CString	m_strDBShare[nMaxSites][nMaxGTACSperSite];
	CString	m_strTitle[nMaxSites][nMaxGTACSperSite];
	int		m_nCurSite;
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CGTACSPropPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CGTACSPropPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnPropChange();
	afx_msg void OnSelChangeGTACSSelCombo();
	afx_msg void OnEndLabelEditBasePropList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString CheckAbbrev(LV_DISPINFO* pDispInfo);
	CString CheckProcShare(LV_DISPINFO* pDispInfo);
	CString CheckDBShare(LV_DISPINFO* pDispInfo);
	CString CheckTitle(LV_DISPINFO* pDispInfo);
};

class COATSPropPage : public CResizablePage
{
	DECLARE_DYNCREATE(COATSPropPage)

// Construction
public:
	COATSPropPage();
	~COATSPropPage();
	void Apply();
// Dialog Data
	//{{AFX_DATA(COATSPropPage)
	enum { IDD = IDD_OATSPROP_DIALOG };
	int		m_nOATSTimeoutEdit;
	int		m_nOATSPortEdit;
	//}}AFX_DATA
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(COATSPropPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(COATSPropPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnPropsChange();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

// RSO Property page in Workspaceoptions
class CRSOPropPage : public CResizablePage
{
	DECLARE_DYNCREATE(CRSOPropPage)

// Construction
public:
	CRSOPropPage();
	~CRSOPropPage();
	void Apply();
// Dialog Data
	//{{AFX_DATA(COATSPropPage)
	enum { IDD = IDD_RSOPROP_DIALOG };
	int		m_nCurSCID;
	CString	m_cRSOFrmTableName;
	CString m_cRSOCurRSOName;
	CString m_cRSONewRSOName1;
	CString m_cRSONewRSOName2;
	CString m_cRSONewRSOName3;
	CString m_cRSONewRSOName4;
	CString m_cRSONewRSOName5;
	CString m_cRSONewRSOName6;
	CString m_cRSONewRSOName7;
	CString m_cRSONewRSOName8;
	CString m_cRSONewRSOName9;
	CString m_cRSONewRSOName10;
	//}}AFX_DATA

	CString	m_strRSOFrmTableName[nMaxSC];
	CString m_strRSOCurRSOName[nMaxSC];
	CString m_strRSONewRSOName1[nMaxSC];
	CString m_strRSONewRSOName2[nMaxSC];
	CString m_strRSONewRSOName3[nMaxSC];
	CString m_strRSONewRSOName4[nMaxSC];
	CString m_strRSONewRSOName5[nMaxSC];
	CString m_strRSONewRSOName6[nMaxSC];
	CString m_strRSONewRSOName7[nMaxSC];
	CString m_strRSONewRSOName8[nMaxSC];
	CString m_strRSONewRSOName9[nMaxSC];
	CString m_strRSONewRSOName10[nMaxSC];
	
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(COATSPropPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CRSOPropPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnPropsChange();
	afx_msg void OnSelChangeSCSelCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CFolderPropPage : public CResizablePage
{
	DECLARE_DYNCREATE(CFolderPropPage)

// Construction
public:
	CFolderPropPage();
	~CFolderPropPage();
	void Apply();
// Dialog Data
	//{{AFX_DATA(CFolderPropPage)
	enum { IDD = IDD_FOLDERPROP_DIALOG };
	CFolderPropListCtrl	m_cFolderPropList;
	//}}AFX_DATA
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CFolderPropPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CFolderPropPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnEndLabelEditBasePropList(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString CheckDirectory(LV_DISPINFO* pDispInfo);
};

class CMiscPropPage : public CResizablePage
{
	DECLARE_DYNCREATE(CMiscPropPage)

// Construction
public:
	CMiscPropPage();
	~CMiscPropPage();
	void Apply();
// Dialog Data
	//{{AFX_DATA(CMiscPropPage)
	enum { IDD = IDD_MISCPROP_DIALOG };
	int				m_nDayEdit;
	COXNumericEdit	m_cTopMargin;
	COXNumericEdit	m_cBottomMargin;
	COXNumericEdit	m_cLeftMargin;
	COXNumericEdit	m_cRightMargin;
	COXNumericEdit	m_cCmdExeTime;
	//}}AFX_DATA
	CString m_strDBDrive;
	CString m_strOldDBDrive;
	
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CMiscPropPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CMiscPropPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnDBDrivePropsChange();
	afx_msg void OnPropsChange();
	afx_msg void OnMiscFontChangeButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	CFont	m_cFont;
	CString	m_strFontFaceName;
	int		m_nFontSize;
	int		m_nFontWeight;
	int		m_nOldDOY;
	BOOL	m_bFontIsStrikeOut;
	BOOL	m_bFontIsUnderline;
	BOOL	m_bFontIsItalic;
};

class CInstObjPropPage : public CResizablePage
{
	DECLARE_DYNCREATE(CInstObjPropPage)
// Construction
public:
	CInstObjPropPage();
	~CInstObjPropPage();
	void Apply();
// Dialog Data
	//{{AFX_DATA(CInstObjPropPage)
	enum { IDD = IDD_INSTOBJPROP_DIALOG };
	CInstObjPropListCtrl	m_cInstObjPropList;
	COXNumericEdit			m_cTopMargin;
	COXNumericEdit			m_cBottomMargin;
	COXNumericEdit			m_cLeftMargin;
	COXNumericEdit			m_cRightMargin;
	//}}AFX_DATA
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CInstObjPropPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CInstObjPropPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnEndLabelEditBasePropList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPropsChange();
	afx_msg void OnSelChangeInstObjSelCombo();
	afx_msg void OnFontChangeButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	CString	m_strTopMargin[nMaxInstObj];
	CString	m_strBottomMargin[nMaxInstObj];
	CString	m_strLeftMargin[nMaxInstObj];
	CString	m_strRightMargin[nMaxInstObj];
	CString	m_strFontFaceName[nMaxInstObj];
	int		m_nFontSize[nMaxInstObj];
	int		m_nFontWeight[nMaxInstObj];
	BOOL	m_bFontIsStrikeOut[nMaxInstObj];
	BOOL	m_bFontIsUnderline[nMaxInstObj];
	BOOL	m_bFontIsItalic[nMaxInstObj];
	CFont	m_cFont[nMaxInstObj];
	int		m_nDisplay[nMaxInstObj][nMaxInstObjPropsPerObj];
	int		m_nDisplayWidth[nMaxInstObj][nMaxInstObjPropsPerObj];
	int		m_nDisplayJustify[nMaxInstObj][nMaxInstObjPropsPerObj];
	int		m_nPrint[nMaxInstObj][nMaxInstObjPropsPerObj];
	int		m_nPrintWidth[nMaxInstObj][nMaxInstObjPropsPerObj];
	int		m_nPrintJustify[nMaxInstObj][nMaxInstObjPropsPerObj];
	int		m_nCurInstObj;
private:
	CString CheckWidth(LV_DISPINFO* pDispInfo);
};

class CMapObjPropPage : public CResizablePage
{
	DECLARE_DYNCREATE(CMapObjPropPage)
// Construction
public:
	CMapObjPropPage();
	~CMapObjPropPage();
	void Apply();
// Dialog Data
	//{{AFX_DATA(CMapObjPropPage)
	enum { IDD = IDD_MAPOBJPROP_DIALOG };
	CMapObjPropListCtrl		m_cMapObjPropList;
	COXNumericEdit			m_cTopMargin;
	COXNumericEdit			m_cBottomMargin;
	COXNumericEdit			m_cLeftMargin;
	COXNumericEdit			m_cRightMargin;
	//}}AFX_DATA
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CInstObjPropPage)
	public:
	virtual BOOL OnApply();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CMapObjPropPage)
	virtual BOOL OnInitDialog();
	afx_msg void OnEndLabelEditBasePropList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnPropsChange();
	afx_msg void OnSelChangeMapObjSelCombo();
	afx_msg void OnFontChangeButton();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	CString	m_strTopMargin[nMaxMapObj];
	CString	m_strBottomMargin[nMaxMapObj];
	CString	m_strLeftMargin[nMaxMapObj];
	CString	m_strRightMargin[nMaxMapObj];
	CString	m_strFontFaceName[nMaxMapObj];
	int		m_nFontSize[nMaxMapObj];
	int		m_nFontWeight[nMaxMapObj];
	BOOL	m_bFontIsStrikeOut[nMaxMapObj];
	BOOL	m_bFontIsUnderline[nMaxMapObj];
	BOOL	m_bFontIsItalic[nMaxMapObj];
	CFont	m_cFont[nMaxMapObj];
	int		m_nDisplay[nMaxMapObj][nMaxMapObjPropsPerObj];
	int		m_nDisplayWidth[nMaxMapObj][nMaxMapObjPropsPerObj];
	int		m_nDisplayJustify[nMaxMapObj][nMaxMapObjPropsPerObj];
	int		m_nPrint[nMaxMapObj][nMaxMapObjPropsPerObj];
	int		m_nPrintWidth[nMaxMapObj][nMaxMapObjPropsPerObj];
	int		m_nPrintJustify[nMaxMapObj][nMaxMapObjPropsPerObj];
	int		m_nCurMapObj;
private:
	CString CheckWidth(LV_DISPINFO* pDispInfo);
};

class CWorkspaceOptionsSheet : public CResizableSheet
{
	DECLARE_DYNAMIC(CWorkspaceOptionsSheet)

// Construction
public:
	CWorkspaceOptionsSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CWorkspaceOptionsSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	void Apply();

// Attributes
public:
	CSCPropPage			m_dlgSCProp;
	CGTACSPropPage		m_dlgGTACSProp;
	COATSPropPage		m_dlgOATSProp;
	CRSOPropPage		m_dlgRSOProp;
	CFolderPropPage		m_dlgFolderProp;
	CMiscPropPage		m_dlgMiscProp;
	CInstObjPropPage	m_dlgInstObjProp;
	CMapObjPropPage		m_dlgMapObjProp;
	BOOL				m_bReadOnly;
	BOOL				m_bRefresh;
	CString				m_strOldDBDrive;
	void SetReadOnly(){m_bReadOnly = TRUE;};
// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWorkspaceOptionsSheet)
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL
// Implementation
public:
	virtual ~CWorkspaceOptionsSheet();
	// Generated message map functions
protected:
	//{{AFX_MSG(CWorkspaceOptionsSheet)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORKSPACEOPTIONS_H__46DB6903_E337_11D4_8013_00609704053C__INCLUDED_)
