/******************
////////////////////////////////////////////////////////
// WorkspaceProperities.cpp : implementation file
//
// (c) 2001 Frederick J. Shaw 
//	Prolog Updated            
////////////////////////////////////////////////////////
******************/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "WorkspaceProperties.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

IMPLEMENT_DYNAMIC(CWorkspacePropertySheet, CResizableSheet)

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspacePropertySheet construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceOptionsSheet::CWorkspaceOptionsSheet
//
//  Description :   Class constructor. 
//                  
//  Return :        void	-	none. 
//					
//  Parameters : 
//				  	UINT nIDCaption	-	ID of the name to be placed in the
//						tab for this page. If 0, the name will be taken
//						from the dialog template for this page. By default,
//						0.
//
//					CWnd* pParentWnd	-	Specifies the button control�s
//						parent window, usually a CDialog. It must not be
//						NULL.
//
//					UINT iSelectPage	-	The index of the page that will
//						initially be on top. Default is the first page added
//						to the sheet.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspacePropertySheet::CWorkspacePropertySheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CResizableSheet(nIDCaption, pParentWnd, iSelectPage)
{
	m_nPreviousType		= nInvalidType;
	m_bPreviousFolder	= FALSE;
	m_bOnScreen			= FALSE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceOptionsSheet::CWorkspaceOptionsSheet
//
//  Description :   Class constructor. 
//                  
//  Return :        void	-	none. 
//					
//  Parameters : 
//					CWnd* pParentWnd	-	Specifies the button control�s
//						parent window, usually a CDialog. It must not be
//						NULL.
//
//					UINT iSelectPage	-	The index of the page that will
//						initially be on top. Default is the first page added
//						to the sheet.
//
//					LPCTSTR pszCaption	-	Pointer to a string containing
//                                          the caption to be used for the
//                                          property sheet. Cannot be NULL.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspacePropertySheet::CWorkspacePropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CResizableSheet(pszCaption, pParentWnd, iSelectPage)
{
	m_nPreviousType		= nInvalidType;
	m_bPreviousFolder	= FALSE;
	m_bOnScreen			= FALSE;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspacePropertySheet::OnInitDialog() 
//
//  Description :  The framework calls this function when the property
//				   page�s dialog is initialized.  
//                  
//  Return :        BOOL 
//						Always returns TRUE.
//					
//  Parameters : 
//					void	-	none.
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CWorkspacePropertySheet::OnInitDialog() 
{
	CResizableSheet::OnInitDialog();
	EnableSaveRestore(_T("Resizable Sheets"), _T("Workspace Properties"), FALSE);
	return TRUE;
}

CWorkspacePropertySheet::~CWorkspacePropertySheet(){}
BEGIN_MESSAGE_MAP(CWorkspacePropertySheet, CResizableSheet)
	//{{AFX_MSG_MAP(CWorkspacePropertySheet)
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspacePropertySheet::OnClose()
//
//  Description :   The framework calls this member function as a signal
//					that the CWnd or an application is to terminate. The
//					default implementation calls DestroyWindow.
//
//  Return :        void	-	none. 
//					
//  Parameters : 
//				  	void	-	none.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspacePropertySheet::OnClose() 
{
	this->ShowWindow(SW_HIDE);
	m_bOnScreen			= FALSE;
}

IMPLEMENT_DYNCREATE(CGeneralPropPage, CResizablePage)

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGeneralPropPage::CGeneralPropPage()
//
//  Description :   Class constructor. 
//                  
//  Return :        
//					void	-	none. 
//					
//  Parameters : 
//					void	-	none.	
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CGeneralPropPage::CGeneralPropPage() : CResizablePage(CGeneralPropPage::IDD)
{
	//{{AFX_DATA_INIT(CGeneralPropPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGeneralPropPage::~CGeneralPropPage()
//
//  Description :   Class destructor. 
//                  
//  Return :       
//					void	-	none. 
//					
//  Parameters : 
//					void	-	none.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CGeneralPropPage::~CGeneralPropPage(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CGeneralPropPage::DoDataExchange
//
//  Description :  Called by the framework to exchange and validate 
//				   dialog data.
//                  
//  Return :         
//					void	-	none.
//					
//  Parameters : 
//					CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CGeneralPropPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGeneralPropPage)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CGeneralPropPage, CResizablePage)
	//{{AFX_MSG_MAP(CGeneralPropPage)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceBar construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CGeneralPropPage::OnInitDialog() 
//
//  Description :  This member function is called in response to the
//				   WM_INITDIALOG message.  
//                  
//  Return :         
//					BOOL	-	Specifies whether the application has set
//								the input focus to one of the controls in
//								the dialog box.
//					
//  Parameters : 
//					void	-	none.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CGeneralPropPage::OnInitDialog() 
{
	CResizablePage::OnInitDialog();
	
	AddAnchor(IDC_GENERALFILEPATH_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_GENERALCREATIONDATE_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_GENERALACCESSDATE_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_GENERALMODDATE_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_GENERALSIZE_EDIT, TOP_LEFT, TOP_RIGHT);

	return TRUE;
}

IMPLEMENT_DYNCREATE(CDetailPropPage, CResizablePage)

/******************
/////////////////////////////////////////////////////////////////////////////
// CDetailPropPage construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CDetailPropPage::CDetailPropPage()
//
//  Description :   Class constructor. 
//                  
//  Return :        void	-	none. 
//					
//  Parameters : 
//					void	-	none.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CDetailPropPage::CDetailPropPage() : CResizablePage(CDetailPropPage::IDD)
{
	//{{AFX_DATA_INIT(CDetailPropPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceOptionsSheet::CWorkspaceOptionsSheet
//
//  Description :   Class desttructor. 
//                  
//  Return :        
//					void	-	none. 
//					
//  Parameters : 
//					void -	none.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CDetailPropPage::~CDetailPropPage(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CDetailPropPage::DoDataExchange
//
//  Description :  This method is called by the framework to exchange and
//				   validate dialog data.
//                  
//  Return :         
//					void	-	none.
//					
//  Parameters : 
//					CDataExchange* pDX	-	A pointer to a CDataExchange
//						 object.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CDetailPropPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDetailPropPage)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDetailPropPage, CResizablePage)
	//{{AFX_MSG_MAP(CDetailPropPage)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :      CDetailPropPage::OnInitDialog() 
//
//  Description :  This member function is called in response to the 
//				   WM_INITDIALOG message.  
//                  
//  Return :        BOOL 
//					Specifies whether the application has set the input
//					focus to one of the controls in the dialog box.
//					
//  Parameters : 
//				  	void	-	none.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CDetailPropPage::OnInitDialog() 
{
	CResizablePage::OnInitDialog();
	
	AddAnchor(IDC_DETAIL01_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_DETAIL02_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_DETAIL03_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_DETAIL04_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_DETAIL05_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_DETAIL06_EDIT, TOP_LEFT, TOP_RIGHT);
	AddAnchor(IDC_DETAIL07_EDIT, TOP_LEFT, TOP_RIGHT);

	AddAnchor(IDC_DETAIL11_EDIT, TOP_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_DETAIL12_EDIT, BOTTOM_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_DETAIL12_STATIC, BOTTOM_LEFT, BOTTOM_LEFT);
	AddAnchor(IDC_DETAIL13_EDIT, BOTTOM_LEFT, BOTTOM_RIGHT);
	AddAnchor(IDC_DETAIL13_STATIC, BOTTOM_LEFT, BOTTOM_LEFT);
	
	m_cDescFont.CreatePointFont(100, _T("Courier New"));

	GetDlgItem(IDC_DETAIL11_EDIT)->SetFont(&m_cDescFont, TRUE);

	return TRUE;
}