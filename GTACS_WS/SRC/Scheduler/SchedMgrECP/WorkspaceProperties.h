#if !defined(AFX_WORKSPACEPROPERITIES_H__2D1B0652_ED41_11D4_8015_00609704053C__INCLUDED_)
#define AFX_WORKSPACEPROPERITIES_H__2D1B0652_ED41_11D4_8015_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CGeneralPropPage : public CResizablePage
{
	DECLARE_DYNCREATE(CGeneralPropPage)
// Construction
public:
	CGeneralPropPage();
	~CGeneralPropPage();
// Dialog Data
	//{{AFX_DATA(CGeneralPropPage)
	enum { IDD = IDD_GENERALPROP_DIALOG };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CGeneralPropPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CGeneralPropPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

class CDetailPropPage : public CResizablePage
{
	DECLARE_DYNCREATE(CDetailPropPage)
// Construction
public:
	CDetailPropPage();
	~CDetailPropPage();
// Dialog Data
	//{{AFX_DATA(CDetailPropPage)
	enum { IDD = IDD_DETAILPROP_DIALOG };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA
// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CDetailPropPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
// Implementation
protected:
	// Generated message map functions
	//{{AFX_MSG(CDetailPropPage)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	CFont m_cDescFont;
};

class CWorkspacePropertySheet : public CResizableSheet
{
	DECLARE_DYNAMIC(CWorkspacePropertySheet)
// Construction
public:
	CWorkspacePropertySheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	CWorkspacePropertySheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
// Attributes
public:
	int			m_nPreviousType;
	BOOL		m_bPreviousFolder;
	BOOL		m_bOnScreen;
	CImageList	m_cImageList;
// Operations
public:
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CWorkspacePropertySheet)
	virtual BOOL OnInitDialog();
	//}}AFX_VIRTUAL
// Implementation
public:
	virtual ~CWorkspacePropertySheet();
	// Generated message map functions
protected:
	//{{AFX_MSG(CWorkspacePropertySheet)
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORKSPACEPROPERITIES_H__2D1B0652_ED41_11D4_8015_00609704053C__INCLUDED_)
