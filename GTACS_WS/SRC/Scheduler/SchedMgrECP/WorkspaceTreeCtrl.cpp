/******************
////////////////////////////////////////////////////////
// WorkspaceTreeCtrl.cpp : implementation file
//
// (c) 2001 Frederick J. Shaw 
//	Prolog Updated            
////////////////////////////////////////////////////////
******************/

#include "StdAfx.h"
#include "SchedMgrECP.h"
#include "WorkspaceTreeCtrl.h"
//#include "OXFileWatcher.h"
#include "RegistryGbl.h"
#include "RegistryUsr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWorkspaceTreeCtrlEx
/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceTreeCtrlData construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlData::CWorkspaceTreeCtrlData()
//
//  Description :   Class constructor. 
//                  
//  Return :        void	-	none. 
//					
//  Parameters :	void	-	none. 
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceTreeCtrlData::CWorkspaceTreeCtrlData()
{
	m_nType			= nInvalidType;
	m_bFolder		= FALSE;
	m_strFilePath	= _T("");
}

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceTreeCtrlData construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlData::~CWorkspaceTreeCtrlData()
//
//  Description :   Class destructor. 
//                  
//  Return :        void	-	none. 
//					
//  Parameters :	void	-	none. 
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceTreeCtrlData::~CWorkspaceTreeCtrlData(){}

//////////////////////////////////////////////////////////////////////////
//	The following code is used to automatically watch for changes in a
//  directory.  Unfortunally, SAMBA does not report back changes in a
//  timely fashion.  In addition, SAMBA does not report back the extended
//  information associated with the change that we need.  Maybe this code
//  will be usefull with a future release of SAMBA :>)
//////////////////////////////////////////////////////////////////////////
//CWorkspaceTreeCtrlFileWatcher::CWorkspaceTreeCtrlFileWatcher(){}
//CWorkspaceTreeCtrlFileWatcher::~CWorkspaceTreeCtrlFileWatcher(){}
//
//BOOL CWorkspaceTreeCtrlFileWatcher::OnNotify(COXFileWatchNotifier fileWatchNotifier)
//{
//	// If the wP is an invalid ID then the notifier is empty
//	if( !fileWatchNotifier.IsEmpty())
//	{
//		// This function only works for Unicode and WinNT (Windows 2000) Builds
//		// If the action was either an Add or Remove (or rename) then update the tree control
//		DWORD dwAction = fileWatchNotifier.GetAction();
//		if ((dwAction == COXFileWatchNotifier::OXFileWatchActionAdded) ||
//			(dwAction == COXFileWatchNotifier::OXFileWatchActionRemoved) ||
//			(dwAction == COXFileWatchNotifier::OXFileWatchActionRename))
//		{
//			COXUNC cUNCFilename(fileWatchNotifier.GetFileName());
//			COXUNC cUNCNewFilename(fileWatchNotifier.GetNewFileName()); // Only valid for renames
//			BOOL bNeedUpdate = FALSE;
//			int nDocType;
//			int nDocCnt = 0;
//			while ((nDocCnt<m_narrDocTypes.GetSize()) && (!bNeedUpdate))
//			{
//				nDocType = m_narrDocTypes.GetAt(nDocCnt);
//				CSchedMgrMultiDocTemplate* pDocTemplate = ((CSchedMgrECPApp*)AfxGetApp())->m_pDocTemplate[nDocType];
//				CString strExt;
//				pDocTemplate->GetDocString(strExt, CDocTemplate::filterExt);
//				if ((strExt.CompareNoCase(cUNCFilename.Extension()) == 0) ||
//					(strExt.CompareNoCase(cUNCNewFilename.Extension()) == 0))
//					bNeedUpdate = TRUE;
//				else
//					nDocCnt++;
//			}
//			if (bNeedUpdate)
//				m_pTreeCtrl->UpdateTree(m_nFolderType, nDocType);
//		}
//	}
//	return TRUE;

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceTreeCtrlData construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::CWorkspaceTreeCtrlEx()
//
//  Description :   Class constructor. 
//                  
//  Return :        void	-	none. 
//					
//  Parameters :	void	-	none. 
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceTreeCtrlEx::CWorkspaceTreeCtrlEx()
{
	m_hRootMain = NULL;
	m_cListImages.m_hImageList = NULL;
//	See note for CWorkspaceTreeCtrlFileWatcher above
//	for (int nIndex=0; nIndex<nMaxFolderTypes; nIndex++)
//		m_strWatchedDir[nIndex].Empty();
}

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceTreeCtrlData construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::~CWorkspaceTreeCtrlEx()
//
//  Description :   Class constructor. 
//                  
//  Return :        void	-	none. 
//					
//  Parameters :	void	-	none. 
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceTreeCtrlEx::~CWorkspaceTreeCtrlEx(){}

BEGIN_MESSAGE_MAP(CWorkspaceTreeCtrlEx, CTreeCtrl)
	//{{AFX_MSG_MAP(CWorkspaceTreeCtrlEx)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblClk)
	ON_NOTIFY_REFLECT(TVN_SELCHANGED, OnSelChanged)
	ON_WM_RBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceTreeCtrlEx construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::Init()
//
//  Description :  Inititializes the CWorkspaceTreeCtrlEx. 
//                  
//  Return :        void	-	none. 
//					
//  Parameters :	void	-	none. 
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlEx::Init()
{
	if (!m_cListImages.m_hImageList)
	{
		m_cListImages.Create(IDB_FILETYPES, 16, 8, RGB(255, 0, 255));
		SetImageList(&m_cListImages, TVSIL_NORMAL);
	}
}


/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::AddFolder
//
//  Description :  Adds a new folder to the workspace tree control 
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					int nFolderType	-	The folder type
//					
//					BOOL bMaster	-	Is it under the master directory?
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlEx::AddFolder(int nFolderType, BOOL bMaster)
{
//	int			nGTACSSiteDef	= GetAppRegSite()->m_nDefaultGTACS;
//	int			nGTACSDef		= GetAppRegGTACS()->m_nDefault[nGTACSSiteDef];
	CString		strPath;
	CString		strDir;

	if (bMaster)
		strDir = GetAppRegFolder()->m_strDir[nFolderType];
	else
		strDir.Format(GetAppRegFolder()->m_strDayFormat + _T("\\") +
					  GetAppRegFolder()->m_strDir[nFolderType], GetAppRegMisc()->m_nDOY);

	strPath.Format(_T("%s\\%s\\%s\\%s\\%s\\"),
//	strPath.Format(_T("S:\\EPOCH_DATABASE\\procs\\%s\\"),
			ctcProcDrive, ctcMountPoint, ctcProcsDir,
				//   GetAppRegGTACS()->m_strAbbrev[nGTACSSiteDef][nGTACSDef],		// Server
				//   GetAppRegGTACS()->m_strProcShare[nGTACSSiteDef][nGTACSDef],	// Share
				   GetAppRegSC()->m_strDir[GetAppRegSite()->m_nDefaultSC],			// SC Specific Directory
				   strDir);														// Doc Specific Directory

	m_hRoot[nFolderType] = InsertItem(GetAppRegFolder()->m_strTitle[nFolderType], 2, 3, m_hRootMain);
//	See note for CWorkspaceTreeCtrlFileWatcher above
//	m_cFileWatcher[nFolderType].m_nFolderType = nFolderType;
	// Create the data object associated with the new item and set its values
	CWorkspaceTreeCtrlData* pWorkspaceTreeCtrlData = new CWorkspaceTreeCtrlData;
	pWorkspaceTreeCtrlData->m_nType			= nInvalidType;
	pWorkspaceTreeCtrlData->m_bFolder		= TRUE;
	pWorkspaceTreeCtrlData->m_strFilePath	= strPath;
		
	SetItemData(m_hRoot[nFolderType], (DWORD) pWorkspaceTreeCtrlData);

//	See note for CWorkspaceTreeCtrlFileWatcher above
//	m_cFileWatcher[nFolderType].RemoveWatch(m_strWatchedDir[nFolderType]);
//	m_cFileWatcher[nFolderType].m_pTreeCtrl = this;
//	if (((CSchedMgrECPApp*)AfxGetApp())->m_bBackupPriv)
//	{
//		m_cFileWatcher[nFolderType].AddWatch(strPath);
//		m_strWatchedDir[nFolderType] = strPath;
//	}
	m_narrFolderTypes.Add((WORD)nFolderType);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::AddDocType
//
//  Description :  Adds a new doc type to the workspace tree control 
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					int nFolderType	-	The folder type
//					
//					int nDocType	-	The document type
//							
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlEx::AddDocType(int nFolderType, int nDocType)
{
//	See note for CWorkspaceTreeCtrlFileWatcher above
//	m_cFileWatcher[nFolderType].m_narrDocTypes.Add(nDocType);
	m_narrDocTypes[nFolderType].Add((WORD)nDocType);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::UpdateTree
//
//  Description :  Updates the worksapce tree control.  
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//				   void	-	none.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlEx::UpdateTree()
{
	CWaitCursor cWaitCursor;
	SetRedraw(FALSE);
	TVSORTCB tvs;
	tvs.lpfnCompare = SortFunction;
	tvs.lParam = (LPARAM) this;

	for (int nFolderCnt=0; nFolderCnt<m_narrFolderTypes.GetSize(); nFolderCnt++)
	{
		int nFolderType = m_narrFolderTypes.GetAt(nFolderCnt);
		for (int nDocCnt=0; nDocCnt<m_narrDocTypes[nFolderType].GetSize(); nDocCnt++)
		{
			int nDocType = m_narrDocTypes[nFolderType].GetAt(nDocCnt);
			UpdateTreeEx(nFolderType, nDocType);
		}
		
		tvs.hParent = m_hRoot[nFolderType];
//		tvs.hParent = m_hRoot[nFolderCnt];
		SortChildrenCB(&tvs);
	}
	SetRedraw(TRUE);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::UpdateTree
//
//  Description :  Updates the worksapce tree control.  
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					int nFolderType	-	The type of folder.
//					int nDocType	-	The type of document.
//					
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlEx::UpdateTree(int nFolderType, int nDocType)
{
	CWaitCursor cWaitCursor;
	SetRedraw(FALSE);
	UpdateTreeEx(nFolderType, nDocType);
	SetRedraw(TRUE);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::UpdateTreeEx
//
//  Description :  Updates the worksapce tree control.  
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					int nFolderType	-	The type of folder.
//					int nDocType	-	The type of document.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlEx::UpdateTreeEx(int nFolderType, int nDocType)
{
	// Get the HTREEITEM for the current branch
	// If it is valid, go through of its children and delete the
	//   data objects and children
	CWorkspaceTreeCtrlData* pTreeCtrlData;
	HTREEITEM hCurrent = GetNextItem(m_hRoot[nFolderType], TVGN_CHILD);
	while (hCurrent != NULL)
	{
		HTREEITEM hNext = GetNextItem(hCurrent, TVGN_NEXT);
		pTreeCtrlData = (CWorkspaceTreeCtrlData*)GetItemData(hCurrent);
		if (pTreeCtrlData->m_nType == nDocType)
		{
			delete (CWorkspaceTreeCtrlData*)GetItemData(hCurrent);			
			DeleteItem(hCurrent);
		}
		hCurrent = hNext;
	}

	// Get the wildcard directory path so that all of the files can be displayed
	CFileFind	finder;
	CString		strExt;
	CString		strFile;
	pTreeCtrlData = (CWorkspaceTreeCtrlData*)GetItemData(m_hRoot[nFolderType]);
	CSchedMgrMultiDocTemplate* pDocTemplate = ((CSchedMgrECPApp*)AfxGetApp())->m_pDocTemplate[nDocType];
	pDocTemplate->GetDocString(strExt, CDocTemplate::filterExt);
	//strFile.Format(_T("%s*%s"), pTreeCtrlData->m_strFilePath, strExt);

	//SSGS-1214: As a result of changing Samba config to make samba/windows shares case-sensitive, it stops reading any files above
	// 8-characters (ie., DOS-only filenames). This is the workaround to display all files above 8-characters too.
	strFile.Format(_T("%s*"), pTreeCtrlData->m_strFilePath);

	// Go through the directory and add each file found to the tree control.
	// For each file, create and initialize the data object for it
	BOOL bWorking = finder.FindFile(strFile);
	while (bWorking)
	{
		
		CWorkspaceTreeCtrlData* pWorkspaceTreeCtrlData;	
		bWorking = finder.FindNextFile();
		//SSGS-1214: Workaround to display all files.
		CString tempPath = ::PathFindExtension((LPCTSTR)finder.GetFileName());
		if(tempPath == strExt)
		{
			CString strFilename = GetAppRegMisc()->m_bShowExt ? finder.GetFileName() : finder.GetFileTitle();
			HTREEITEM hItem = InsertItem (strFilename, nDocType+4, nDocType+4, m_hRoot[nFolderType]);
			pWorkspaceTreeCtrlData = new CWorkspaceTreeCtrlData;
			pWorkspaceTreeCtrlData->m_nType			= (WORD)nDocType;
			pWorkspaceTreeCtrlData->m_bFolder		= FALSE;
			pWorkspaceTreeCtrlData->m_strFilePath	= finder.GetFilePath();
			SetItemData(hItem, (DWORD) pWorkspaceTreeCtrlData);
		}
	}
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::SortFunction
//
//  Description :  Callback function to sort the workspace tree control.
//                  
//  Return :       int	-
//						Zero if the strings are identical (ignoring case),
//						<0 if this CString object is less than lpsz
//						(ignoring case), or >0 if this CString object
//						is greater than lpsz (ignoring case).
//
//  Parameters :	
//					LPARAM lParam1	-	The lParam of an item is just
//						its handle.
//					LPARAM lParam2	-	The lParam of an item is just
//						its handle. 
//					LPARAM lParamSort	-	lParamSort contains a
//						pointer to the tree control.	
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
int CALLBACK CWorkspaceTreeCtrlEx::SortFunction(LPARAM lParam1,
												LPARAM lParam2, 
												LPARAM lParamSort)
{
	// lParamSort contains a pointer to the tree control.
	// The lParam of an item is just its handle.
//	CTreeCtrl* pTreeCtrl = (CTreeCtrl*) lParamSort;
	CWorkspaceTreeCtrlData* pTreeCtrlData1 = (CWorkspaceTreeCtrlData*)lParam1;
	CWorkspaceTreeCtrlData* pTreeCtrlData2 = (CWorkspaceTreeCtrlData*)lParam2;
	
	COXUNC	cUNCFile1(pTreeCtrlData1->m_strFilePath);
	COXUNC	cUNCFile2(pTreeCtrlData2->m_strFilePath);
	
	if (GetAppRegMisc()->m_bSortByExt)
	{
		if (cUNCFile1.Extension().CompareNoCase(cUNCFile2.Extension()) == 0)
			return cUNCFile1.Base().CompareNoCase(cUNCFile2.Base());
		else
			return cUNCFile1.Extension().CompareNoCase(cUNCFile2.Extension());
	}
	else
	{
		if (cUNCFile1.Base().CompareNoCase(cUNCFile2.Base()) == 0)
			return cUNCFile1.Extension().CompareNoCase(cUNCFile2.Extension());
		else
			return cUNCFile1.Base().CompareNoCase(cUNCFile2.Base());
	}
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::OnDblClk 
//
//  Description :	The framework calls this member function when the user
//					double-clicks the left mouse button. 
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					NMHDR* pNMHDR	-	Indicates whether various virtual
//						 keys are down.
//						
//					LRESULT* pResult -	Specifies the x- and y-coordinate
//						 of the cursor. 
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlEx::OnDblClk(NMHDR* pNMHDR, LRESULT* pResult) 
{
	CWorkspaceTreeCtrlData* pData = (CWorkspaceTreeCtrlData*) GetItemData(GetSelectedItem());
	if ((!pData->m_bFolder) && (pData->m_nType != nInvalidType))
	{
		CSchedMgrMultiDocTemplate* pDocTemplate = ((CSchedMgrECPApp*)AfxGetApp())->m_pDocTemplate[pData->m_nType];

		if (pDocTemplate != NULL)
		{
			BOOL bFound = FALSE;
			POSITION posDoc = pDocTemplate->GetFirstDocPosition();
			CDocument *pDoc = NULL;
			while ((posDoc != NULL) && (!bFound))
			{
				pDoc = pDocTemplate->GetNextDoc(posDoc);
				CString strPath = pDoc->GetPathName();
				if (strPath.CompareNoCase(pData->m_strFilePath) == 0)
					bFound = TRUE;
			}
			if (bFound)
			{
				POSITION posView = pDoc->GetFirstViewPosition();
				CView* pView = pDoc->GetNextView(posView);
				if (pView != NULL)
					((CMDIChildWnd*)(pView->GetParent()))->MDIActivate();
			}
			else
			{
				if (pData->m_nType != nBinary)
					pDocTemplate->OpenDocumentFile (pData->m_strFilePath);
			}
		}
	}

	*pResult = 0;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::DeleteTreeData
//
//  Description :  Inititializes the CWorkspaceTreeCtrlEx. 
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					HTREEITEM hParentItem	-	Handle of a tree item.
//					
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlEx::DeleteTreeData(HTREEITEM hParentItem)
{
	// Delete the data associated with the parent
	delete (CWorkspaceTreeCtrlData*)GetItemData(hParentItem);

	// If the parent has and children, then call DeleteTreeData on the children
	if (ItemHasChildren(hParentItem) != 0)
	{
		HTREEITEM hNextItem;
		HTREEITEM hChildItem = GetChildItem(hParentItem);

		while (hChildItem != NULL)
		{
			hNextItem = GetNextItem(hChildItem, TVGN_NEXT);	
			DeleteTreeData(hChildItem);
			hChildItem = hNextItem;
		}
	}
}

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceTreeCtrlEx construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::OnSelChanged 
//
//  Description :  The OnSelChanged event is fired whenever the item
//				   selection in a control changes. 
//
//  Return :       void	-	none. 
//					
//  Parameters :	
//					NMHDR* pNMHDR		-	Pointer to a NMHDR object.
//					LRESULT* pResult	-	Pointer to a LRESULT object.
//											Always set to 0.
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlEx::OnSelChanged(NMHDR* pNMHDR, LRESULT* pResult) 
{
//	NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;

	if (GetAppWorkspaceBar()->GetPropertySheet() != NULL)
	{
		if (GetAppWorkspaceBar()->GetPropertySheet()->m_bOnScreen)
			GetAppWorkspaceBar()->UpdatePropertySheet();
	}
	*pResult = 0;
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::DestroyWindow()
//
//  Description : Destroys the Windows window attached to the CWnd object.   
//                  
//  Return :       BOOL
//					Nonzero if the window is destroyed; otherwise 0.
//					
//  Parameters :	
//					void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CWorkspaceTreeCtrlEx::DestroyWindow() 
{	
	HTREEITEM hRootItem;

	hRootItem = GetRootItem();
	if (hRootItem != NULL)
		DeleteTreeData(hRootItem);

	return CTreeCtrl::DestroyWindow();
}

/******************
/////////////////////////////////////////////////////////////////////////////
// CWorkspaceTreeCtrlEx construction/destruction
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlEx::OnRButtonDown 
//
//  Description : The framework calls this member function when the user 
//				  presses the right mouse button.
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//				   UINT nFlags	-	Indicates whether various virtual
//						 keys are down.
//				   CPoint point	-	Specifies the x and y coordinates 
//						 of the cursor. These coordinates are always
//						 relative to the upper-left corner of the window.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlEx::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CPoint ptTree = point;
	HTREEITEM hTreeItem = HitTest (ptTree);
	if (hTreeItem != NULL)
	{
		Select(hTreeItem, TVGN_CARET);
	}

	CTreeCtrl::OnRButtonDown(nFlags, point);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     BOOL CWorkspaceTreeCtrlEx::PreTranslateMessage() 
//
//  Description :  Override this function to filter window messages before
//				   they are dispatched to the Windows functions 
//                 TranslateMessage andDispatchMessage The default 
//                 implementation performs accelerator-key translation, 
//                 so you must call the CWinApp::PreTranslateMessage member
//                 function in your overridden version.
//                  
//  Return :       
//					BOOL	-	
//					Nonzero if the message was fully processed in 
//					PreTranslateMessage and should not be processed further.
//					Zero if the message should be processed in the normal
//					way.
//					
//  Parameters :	
//					MSG* pMsg	-	A pointer to a MSG structure that 
//						contains the message to process.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
BOOL CWorkspaceTreeCtrlEx::PreTranslateMessage(MSG* pMsg) 
{
	CString strErrorMsg;
	if (pMsg->message == WM_KEYDOWN)
	{
		if ((UINT) pMsg->wParam == VK_DELETE)
		{
			BOOL bFound = FALSE;
			HTREEITEM hSelItem = GetSelectedItem();
			CWorkspaceTreeCtrlData* pData = (CWorkspaceTreeCtrlData*) GetItemData(hSelItem);
			if (!pData->m_bFolder)
			{
				CSchedMgrMultiDocTemplate* pDocTemplate = ((CSchedMgrECPApp*)AfxGetApp())->m_pDocTemplate[pData->m_nType];
				if (pDocTemplate != NULL)
				{
					// BOOL bFound = FALSE;
					POSITION posDoc = pDocTemplate->GetFirstDocPosition();
					CDocument *pDoc = NULL;
					while ((posDoc != NULL) && (!bFound))
					{
						pDoc = pDocTemplate->GetNextDoc(posDoc);
						CString strPath = pDoc->GetPathName();
						if (strPath.CompareNoCase(pData->m_strFilePath) == 0)
						{
				  			bFound = TRUE;
						}
					}
				}
				if (FALSE == bFound)
				{
					TRY
					{
						CFile::Remove(pData->m_strFilePath);
						if (GetNextSiblingItem(hSelItem) == NULL)
						{
							if (GetPrevSiblingItem(hSelItem) == NULL)
								SelectItem(GetParentItem(hSelItem));
							else
								SelectItem(GetPrevSiblingItem(hSelItem));
						}
						strErrorMsg.Format(IDS_FILE_DELETED, pData->m_strFilePath);
						GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::INFO);
						delete (CWorkspaceTreeCtrlData*)GetItemData(hSelItem);
						DeleteItem(hSelItem);
					}
					CATCH(CFileException, e)
					{
						strErrorMsg.Format(IDS_DEL_ERROR, pData->m_strFilePath);
						GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
					}
					END_CATCH
				}
				else
				{
					strErrorMsg.Format(IDS_ERROR_FILE_SHARING, pData->m_strFilePath);
					GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
					strErrorMsg.Format(IDS_ERROR_FILE_DELETE, pData->m_strFilePath);
					GetAppOutputBar()->OutputToEvents(strErrorMsg, COutputBar::FATAL);
				}
			}// m_bFolder
		}// VK_DELETE
	}// WM_KEYDWN
	return CTreeCtrl::PreTranslateMessage(pMsg);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlMaster::CWorkspaceTreeCtrlMaster()
//
//  Description :  Class constructor 
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceTreeCtrlMaster::CWorkspaceTreeCtrlMaster(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlMaster::~CWorkspaceTreeCtrlMaster()
//
//  Description :  Class destructor 
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					void	-	 none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceTreeCtrlMaster::~CWorkspaceTreeCtrlMaster(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlMaster::Init()
//
//  Description :  Inititializes the CWorkspaceTreeCtrlMaster. 
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlMaster::Init()
{
	CWaitCursor	cWaitCursor;
	CWorkspaceTreeCtrlEx::Init();

	if (m_hRootMain != NULL)
	{
		DeleteTreeData(m_hRootMain);
		DeleteAllItems();
	}
	
	CString strText;
	strText.Format(_T("Master Files - %s"), GetAppRegSC()->m_strTitle[GetAppRegSite()->m_nDefaultSC]);
	m_hRootMain	= InsertItem (strText, 0, 0);
	CWorkspaceTreeCtrlData* pWorkspaceTreeCtrlData = new CWorkspaceTreeCtrlData;
	pWorkspaceTreeCtrlData->m_nType = nInvalidType;
	pWorkspaceTreeCtrlData->m_bFolder = TRUE;
	SetItemData(m_hRootMain, (DWORD) pWorkspaceTreeCtrlData);

	// Call UpdateTree to populate the new item with all of it files

	AddFolder(nFolderSTOL, TRUE);
	AddDocType(nFolderSTOL, nSTOL);
	AddDocType(nFolderSTOL, nRpt);
	AddDocType(nFolderSTOL, nSTOLMap);
	AddDocType(nFolderSTOL, nBinary);

	AddFolder(nFolderCLS, TRUE);
	AddDocType(nFolderCLS, nCLS);
	AddDocType(nFolderCLS, nRpt);

	AddFolder(nFolderFrame, TRUE);
	AddDocType(nFolderFrame, nSector);
	AddDocType(nFolderFrame, nFrame);
	AddDocType(nFolderFrame, nBinary);
	AddDocType(nFolderFrame, nRpt);

	AddFolder(nFolderRTCS, TRUE);
	AddDocType(nFolderRTCS, nRTCS);
	AddDocType(nFolderRTCS, nRTCSMap);
	AddDocType(nFolderRTCS, nLis);
	AddDocType(nFolderRTCS, nRTCSSet);
	AddDocType(nFolderRTCS, nBinary);
	AddDocType(nFolderRTCS, nRpt);
	AddDocType(nFolderRTCS, nSTOL);

	AddFolder(nFolderData, TRUE);
	AddDocType(nFolderData, nRTCSCmd);
	AddDocType(nFolderData, nPaceCmd);
	AddDocType(nFolderData, nRpt);
	AddDocType(nFolderData, nSTOLCmd);
	
	UpdateTree();
	
	Expand(m_hRootMain, TVE_EXPAND);
}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlDaily::CWorkspaceTreeCtrlDaily()
//
//  Description :  Class constructor
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					void -	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceTreeCtrlDaily::CWorkspaceTreeCtrlDaily(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlDaily::~CWorkspaceTreeCtrlDaily()
//
//  Description :  Class Destructor
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					void	-	none.				
//
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
CWorkspaceTreeCtrlDaily::~CWorkspaceTreeCtrlDaily(){}

/******************
//////////////////////////////////////////////////////////////////////////
//  Author :	Don Sanders       Date : 08/20/2001       version 1.0
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//  Function :     CWorkspaceTreeCtrlDaily::Init()
//
//  Description :  Inititializes the CWorkspaceTreeCtrlEx. 
//                  
//  Return :       void	-	none. 
//					
//  Parameters :	
//					void	-	none.
//					
//  Notes : 
//			
//////////////////////////////////////////////////////////////////////////
******************/
void CWorkspaceTreeCtrlDaily::Init()
{
	CWaitCursor	cWaitCursor;
	CWorkspaceTreeCtrlEx::Init();

	int nDOY = GetAppRegMisc()->m_nDOY;

	if (m_hRootMain != NULL)
	{
		DeleteTreeData(m_hRootMain);
		DeleteAllItems();
	}

	// Create the root directory
	CString strText;
	strText.Format(_T("Daily Files [%03d] - %s"), nDOY, GetAppRegSC()->m_strTitle[GetAppRegSite()->m_nDefaultSC]);
	m_hRootMain	= InsertItem (strText, 1, 1);
	CWorkspaceTreeCtrlData* pWorkspaceTreeCtrlData = new CWorkspaceTreeCtrlData;
	pWorkspaceTreeCtrlData->m_nType = nInvalidType;
	pWorkspaceTreeCtrlData->m_bFolder = TRUE;
	SetItemData(m_hRootMain, (DWORD) pWorkspaceTreeCtrlData);

	// Add all of the document types
	AddFolder(nFolderSTOL, FALSE);
	AddDocType(nFolderSTOL, nSTOL);
	AddDocType(nFolderSTOL, nRpt);
	AddDocType(nFolderSTOL, nSTOLMap);
	AddDocType(nFolderSTOL, nBinary);
	AddDocType(nFolderSTOL, nLis);

	AddFolder(nFolderFrame, FALSE);
	AddDocType(nFolderFrame, nSTOL);
	AddDocType(nFolderFrame, nFrame);
	AddDocType(nFolderFrame, nBinary);
	AddDocType(nFolderFrame, nRpt);

	AddFolder(nFolderStar, FALSE);
	AddDocType(nFolderStar, nSTOL);
	AddDocType(nFolderStar, nStar);
	AddDocType(nFolderStar, nBinary);
	AddDocType(nFolderStar, nRpt);

	AddFolder(nFolderOATS, FALSE);
	AddDocType(nFolderOATS, nIMC);
	AddDocType(nFolderOATS, nRpt);
	AddDocType(nFolderOATS, nBinary);
	AddDocType(nFolderOATS, nSTOL);
	
	UpdateTree();

	Expand(m_hRootMain, TVE_EXPAND);
}
