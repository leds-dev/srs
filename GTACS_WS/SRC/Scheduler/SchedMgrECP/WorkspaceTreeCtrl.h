#if !defined(AFX_WORKSPACETREECTRL_H__A464C547_D502_11D4_800D_00609704053C__INCLUDED_)
#define AFX_WORKSPACETREECTRL_H__A464C547_D502_11D4_800D_00609704053C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SchedMgrECPTreeCtrl.h : header file
//

#include "OXFileWatcher.h"

class CWorkspaceTreeCtrlEx;

class CWorkspaceTreeCtrlData
{
public:
	CWorkspaceTreeCtrlData();
	virtual		~CWorkspaceTreeCtrlData();
	CString		m_strFilePath;
	short int	m_nType;
	BOOL		m_bFolder;
};

//////////////////////////////////////////////////////////////////////////
//	The following code is used to automatically watch for changes in a
//  directory.  Unfortunally, SAMBA does not report back changes in a
//  timely fashion.  In addition, SAMBA does not report back the extended
//  information associated with the change that we need.  Maybe this code
//  will be usefull with a future release of SAMBA :>)
//////////////////////////////////////////////////////////////////////////
//class CWorkspaceTreeCtrlFileWatcher : public COXFileWatcher
//{
//public:
//	CWorkspaceTreeCtrlFileWatcher();
//	virtual	~CWorkspaceTreeCtrlFileWatcher();
//	virtual BOOL OnNotify(COXFileWatchNotifier fileWatchNotifier);
//	int						m_nFolderType;
//	CWordArray				m_narrDocTypes;
//	CWorkspaceTreeCtrlEx*	m_pTreeCtrl;
//};

class CWorkspaceTreeCtrlEx : public CTreeCtrl
{
public:
	CWorkspaceTreeCtrlEx();
public:
	//{{AFX_VIRTUAL(CWorkspaceTreeCtrlEx)
	public:
	virtual BOOL DestroyWindow();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//}}AFX_VIRTUAL
public:
	virtual ~CWorkspaceTreeCtrlEx();
	void UpdateTree();
	void UpdateTree(int nFolderType, int nDocType);
	void AddDocType(int nFolderType, int nDocType);
	void AddFolder(int nFolderType, BOOL bMaster=TRUE);

protected:
	HTREEITEM m_hRootMain;
	HTREEITEM m_hRoot[nMaxFolderTypes];
	CImageList	m_cListImages;

//	See note for CWorkspaceTreeCtrlFileWatcher above
//	CString		m_strWatchedDir[nMaxFolderTypes];
//	CWorkspaceTreeCtrlFileWatcher m_cFileWatcher[nMaxFolderTypes];
	void UpdateTreeEx(int nFolderType, int nDocType);
	void DeleteTreeData(HTREEITEM hParentItem);
	void Init();
	//{{AFX_MSG(CWorkspaceTreeCtrlEx)
	afx_msg void OnDblClk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	static int CALLBACK SortFunction(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
		
	CWordArray				m_narrFolderTypes;
	CWordArray				m_narrDocTypes[nMaxFolderTypes];
	
	DECLARE_MESSAGE_MAP()
};

class CWorkspaceTreeCtrlMaster : public CWorkspaceTreeCtrlEx
{
public:
	CWorkspaceTreeCtrlMaster();
	virtual ~CWorkspaceTreeCtrlMaster();
	void Init();
};

class CWorkspaceTreeCtrlDaily : public CWorkspaceTreeCtrlEx
{
public:
	CWorkspaceTreeCtrlDaily();
	virtual ~CWorkspaceTreeCtrlDaily();
	void Init();
};
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WORKSPACETREECTRL_H__A464C547_D502_11D4_800D_00609704053C__INCLUDED_)
