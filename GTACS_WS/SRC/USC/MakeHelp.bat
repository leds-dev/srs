@echo off
REM -- First make map file from Microsoft Visual C++ generated resource.h
echo // MAKEHELP.BAT generated Help Map file.  Used by USCVIEWER.HPJ. >"hlp\USCviewer.hm"
echo. >>"hlp\USCviewer.hm"
echo // Commands (ID_* and IDM_*) >>"hlp\USCviewer.hm"
makehm ID_,HID_,0x10000 IDM_,HIDM_,0x10000 resource.h >>"hlp\USCviewer.hm"
echo. >>"hlp\USCviewer.hm"
echo // Prompts (IDP_*) >>"hlp\USCviewer.hm"
makehm IDP_,HIDP_,0x30000 resource.h >>"hlp\USCviewer.hm"
echo. >>"hlp\USCviewer.hm"
echo // Resources (IDR_*) >>"hlp\USCviewer.hm"
makehm IDR_,HIDR_,0x20000 resource.h >>"hlp\USCviewer.hm"
echo. >>"hlp\USCviewer.hm"
echo // Dialogs (IDD_*) >>"hlp\USCviewer.hm"
makehm IDD_,HIDD_,0x20000 resource.h >>"hlp\USCviewer.hm"
echo. >>"hlp\USCviewer.hm"
echo // Frame Controls (IDW_*) >>"hlp\USCviewer.hm"
makehm IDW_,HIDW_,0x50000 resource.h >>"hlp\USCviewer.hm"
REM -- Make help for Project USCVIEWER


echo Building Win32 Help files
start /wait hcw /C /E /M "hlp\USCviewer.hpj"
if errorlevel 1 goto :Error
if not exist "hlp\USCviewer.hlp" goto :Error
if not exist "hlp\USCviewer.cnt" goto :Error
echo.
if exist Debug\nul copy "hlp\USCviewer.hlp" Debug
if exist Debug\nul copy "hlp\USCviewer.cnt" Debug
if exist Release\nul copy "hlp\USCviewer.hlp" Release
if exist Release\nul copy "hlp\USCviewer.cnt" Release
echo.
goto :done

:Error
echo hlp\USCviewer.hpj(1) : error: Problem encountered creating help file

:done
echo.
