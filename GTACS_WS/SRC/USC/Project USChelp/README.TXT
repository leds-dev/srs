
RTF2HLP for Windows NT/9x
Copyright (C) 2000 Ariacom SARL, Geneva, Switzerland.
Web: http://www.ariacom.com  Email: mailto:gerard.chong@ariacom.com

What Is RTF2HLP ?
-----------------
RTF2HLP is a freeware tool that converts directly a Microsoft� Word RTF
document to a Windows Help file.
The main advantage of RTF2HLP is that it converts documents without the
need of any extra project files.

Writing a help file is simple as this:
1. Write your document using Microsoft� Word.
2. Save it to RTF.
3. Compile it to HLP using RTF2HLP.

This also works for almost any existing document.

RTF2HLP requires to have previously installed Microsoft� Help Workshop
which can be freely downloaded from
ftp://ftp.microsoft.com/Softlib/MSLFILES/hcwsetup.EXE

Installation
------------
RTF2HLP does not require any installation. Just UNZIP the contents of the
ZIP package to any directory and run RTF2HLP.EXE.

Upon first execution of RTF2HLP you will have to setup the location of the
"Microsoft� Help Compiler" (HCW.EXE). To do this select the menu
Setup->Configure and specifiy the correct location where you've installed
it.

To compile your RTF document, just drag & drop it to the RTF2HLP main window.
You'll get a fully functional HLP file within a few seconds in the same
directory as your RTF file.

Please read the online help file RTF2HLP.HLP for more information.

ZIP contents
------------
The ZIP package contains:
RTF2HLP.EXE      The main executable file.
                 Just run it without any installation.
RTF2HLP.HLP      The online help file.
TEST.RTF         A sample help file written with Word 2000.
                 Drag and drop it to RTF2HLP's main window.

Terms of Use
------------
This software is provided "as is", without any guarantee made
as to its suitability or fitness for any particular use. It may
contain bugs, so use of this tool is at your own risk. We take
no responsilbity for any damage that may unintentionally be caused
through its use.

Reporting Problems
------------------
If you encounter problems, please visit http://www.ariacom.com/rtf2hlp
and download the latest version to see if the issue has been resolved.
If not, please send a bug report to:

	gerard.chong@ariacom.com

