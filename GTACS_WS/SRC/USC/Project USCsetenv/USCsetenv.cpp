// USCsetenv.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "USCsetenv.h"
#include "USCsetenvDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUSCsetenvApp

BEGIN_MESSAGE_MAP(CUSCsetenvApp, CWinApp)
	//{{AFX_MSG_MAP(CUSCsetenvApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUSCsetenvApp construction

CUSCsetenvApp::CUSCsetenvApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CUSCsetenvApp object

CUSCsetenvApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CUSCsetenvApp initialization

BOOL CUSCsetenvApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

/*
	CUSCsetenvDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
*/
	// set env USER to user logon name (for use with epoch,acc)
	char UserName[100];
    DWORD UserNameSize = sizeof(UserName);
	char DomainName[100];
    DWORD DomainNameSize = sizeof(DomainName);
	char LogonID[1024];
	CString  test;

	GetCurrentUserAndDomain(UserName,&UserNameSize,DomainName,&DomainNameSize);
	sprintf(LogonID,"%s\\%s",DomainName,UserName);

	HKEY hkey = NULL;

	//if (RegOpenKeyEx(HKEY_LOCAL_MACHINE,_T("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment"),
	//		0,KEY_SET_VALUE,&hkey) != ERROR_SUCCESS) {
	//	return TRUE;
	//}

	if (RegOpenKeyEx(HKEY_CURRENT_USER,_T("Environment"),0,KEY_SET_VALUE,&hkey) != ERROR_SUCCESS) {
		return TRUE;
	}

	if( RegSetValueEx(hkey,_T("USER"),0,REG_SZ,(CONST BYTE*)LogonID,strlen(LogonID)) != ERROR_SUCCESS )
	{
		LPVOID lpMsgBuf;
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				  FORMAT_MESSAGE_FROM_SYSTEM |
				  FORMAT_MESSAGE_IGNORE_INSERTS,
				  NULL,
				  GetLastError(),
				  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				  (LPTSTR) &lpMsgBuf,
				  0,
				  NULL);
		CString strMsg((LPTSTR)lpMsgBuf);
		strMsg.Remove(_T('\x0D'));
		strMsg.Remove(_T('\x0A'));
		LocalFree(lpMsgBuf);
		AfxMessageBox(strMsg, MB_OK);
		return TRUE;
	}
	RegCloseKey(hkey);
	
	DWORD dwReturnValue;
	LRESULT lr = SendMessageTimeout(HWND_BROADCAST, WM_SETTINGCHANGE, 0,
		(LPARAM)"Environment", SMTO_BLOCK, 5000, &dwReturnValue);	

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


//**********************************************************************
//
//  FUNCTION:     GetCurrentUserAndDomain - This function looks up
//                the user name and domain name for the user account
//                associated with the calling thread.
//
//  PARAMETERS:   szUser - a buffer that receives the user name
//                pcchUser - the size, in characters, of szUser
//                szDomain - a buffer that receives the domain name
//                pcchDomain - the size, in characters, of szDomain
//
//  RETURN VALUE: TRUE if the function succeeds. Otherwise, FALSE and
//                GetLastError() will return the failure reason.
//
//                If either of the supplied buffers are too small,
//                GetLastError() will return ERROR_INSUFFICIENT_BUFFER
//                and pcchUser and pcchDomain will be adjusted to
//                reflect the required buffer sizes.
//
//**********************************************************************

BOOL CUSCsetenvApp::GetCurrentUserAndDomain(PTSTR szUser, PDWORD pcchUser,
      PTSTR szDomain, PDWORD pcchDomain) {

   BOOL         fSuccess = FALSE;
   HANDLE       hToken   = NULL;
   PTOKEN_USER  ptiUser  = NULL;
   DWORD        cbti     = 0;
   SID_NAME_USE snu;

   __try {

      // Get the calling thread's access token.
      if (!OpenThreadToken(GetCurrentThread(), TOKEN_QUERY, TRUE,
            &hToken)) {

         if (GetLastError() != ERROR_NO_TOKEN)
            __leave;

         // Retry against process token if no thread token exists.
         if (!OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY,
               &hToken))
            __leave;
      }

      // Obtain the size of the user information in the token.
      if (GetTokenInformation(hToken, TokenUser, NULL, 0, &cbti)) {

         // Call should have failed due to zero-length buffer.
         __leave;

      } else {

         // Call should have failed due to zero-length buffer.
         if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
            __leave;
      }

      // Allocate buffer for user information in the token.
      ptiUser = (PTOKEN_USER) HeapAlloc(GetProcessHeap(), 0, cbti);
      if (!ptiUser)
         __leave;

      // Retrieve the user information from the token.
      if (!GetTokenInformation(hToken, TokenUser, ptiUser, cbti, &cbti))
         __leave;

      // Retrieve user name and domain name based on user's SID.
      if (!LookupAccountSid(NULL, ptiUser->User.Sid, szUser, pcchUser,
            szDomain, pcchDomain, &snu))
         __leave;

      fSuccess = TRUE;

   } __finally {

      // Free resources.
      if (hToken)
         CloseHandle(hToken);

      if (ptiUser)
         HeapFree(GetProcessHeap(), 0, ptiUser);
   }

   return fSuccess;
}