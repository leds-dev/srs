// USCsetenv.h : main header file for the USCSETENV application
//

#if !defined(AFX_USCSETENV_H__D9A2F502_4859_48A0_8804_76C413BEFEEE__INCLUDED_)
#define AFX_USCSETENV_H__D9A2F502_4859_48A0_8804_76C413BEFEEE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CUSCsetenvApp:
// See USCsetenv.cpp for the implementation of this class
//

class CUSCsetenvApp : public CWinApp
{
public:
	CUSCsetenvApp();

	BOOL GetCurrentUserAndDomain(PTSTR, PDWORD, PTSTR, PDWORD);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUSCsetenvApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CUSCsetenvApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USCSETENV_H__D9A2F502_4859_48A0_8804_76C413BEFEEE__INCLUDED_)
