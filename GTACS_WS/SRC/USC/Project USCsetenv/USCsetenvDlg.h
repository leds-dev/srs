// USCsetenvDlg.h : header file
//

#if !defined(AFX_USCSETENVDLG_H__9A9AE5E9_727D_4C03_A9C4_4530721F0311__INCLUDED_)
#define AFX_USCSETENVDLG_H__9A9AE5E9_727D_4C03_A9C4_4530721F0311__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CUSCsetenvDlg dialog

class CUSCsetenvDlg : public CDialog
{
// Construction
public:
	CUSCsetenvDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CUSCsetenvDlg)
	enum { IDD = IDD_USCSETENV_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUSCsetenvDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CUSCsetenvDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USCSETENVDLG_H__9A9AE5E9_727D_4C03_A9C4_4530721F0311__INCLUDED_)
