BUGS
	- ERR: read_stream error crashes everything
TBD
	- send ON_DISCONNECT to USCviewerDlg
	- have minimize button without x button or disable x button
	- automatic reconnect
	- set font in listbox
	- do not create streamDlg until connect_stream is successful 

USCviewer 1.0
	- add uscviewer.hlp
	- add flush to log file
	- add @stream to stol_execute
	- add streamname to connect_stream
	- add FlexGrid
	- add SetRows
	- add col scid and SetColWidth
	- fix findstring in USC ACK callback
	- disable close button
	- add flexgrid object
	- request event
	- send stol directive
	- fixed setTimer arg (*1000)
	- add poll_sound_interval 
	- use read_stream
	- add streamname to request_point
	- add write events to log file
	- add usc_term_stream
	- send stol directive 
	- connect to stream
	- req point value from stream
	- 3 timer intervals in cfg file
	- blinking icontray
	- playsound
	- Add OnTimer
	- Force horizontal scrollbar in events display
	- position cursel after AddString in events display
	- read usc.cfg file  
	- display alarm count in titlebar
	- try to connect to default streams 
	- set initial pos for StreamViewer
	- add data to StreamViewer class 
	- use AddString and gmt timetag in log_event
	- destroy stream viewer on DISCONNECT
	- create stream viewer on CONNECT
	- define class CUSCstreamDlg
	- close and minimize USCstream



========================================================================
       MICROSOFT FOUNDATION CLASS LIBRARY : USCviewer
========================================================================


AppWizard has created this USCviewer application for you.  This application
not only demonstrates the basics of using the Microsoft Foundation classes
but is also a starting point for writing your application.

This file contains a summary of what you will find in each of the files that
make up your USCviewer application.

USCviewer.dsp
    This file (the project file) contains information at the project level and
    is used to build a single project or subproject. Other users can share the
    project (.dsp) file, but they should export the makefiles locally.

USCviewer.h
    This is the main header file for the application.  It includes other
    project specific headers (including Resource.h) and declares the
    CUSCviewerApp application class.

USCviewer.cpp
    This is the main application source file that contains the application
    class CUSCviewerApp.

USCviewer.rc
    This is a listing of all of the Microsoft Windows resources that the
    program uses.  It includes the icons, bitmaps, and cursors that are stored
    in the RES subdirectory.  This file can be directly edited in Microsoft
	Visual C++.

USCviewer.clw
    This file contains information used by ClassWizard to edit existing
    classes or add new classes.  ClassWizard also uses this file to store
    information needed to create and edit message maps and dialog data
    maps and to create prototype member functions.

res\USCviewer.ico
    This is an icon file, which is used as the application's icon.  This
    icon is included by the main resource file USCviewer.rc.

res\USCviewer.rc2
    This file contains resources that are not edited by Microsoft 
	Visual C++.  You should place all resources not editable by
	the resource editor in this file.




/////////////////////////////////////////////////////////////////////////////

AppWizard creates one dialog class:

USCviewerDlg.h, USCviewerDlg.cpp - the dialog
    These files contain your CUSCviewerDlg class.  This class defines
    the behavior of your application's main dialog.  The dialog's
    template is in USCviewer.rc, which can be edited in Microsoft
	Visual C++.

/////////////////////////////////////////////////////////////////////////////

Help Support:

hlp\USCviewer.hpj
    This file is the Help Project file used by the Help compiler to create
    your application's Help file.

hlp\*.bmp
    These are bitmap files required by the standard Help file topics for
    Microsoft Foundation Class Library standard commands.

hlp\*.rtf
    This file contains the standard help topics for standard MFC
    commands and screen objects.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named USCviewer.pch and a precompiled types file named StdAfx.obj.

Resource.h
    This is the standard header file, which defines new resource IDs.
    Microsoft Visual C++ reads and updates this file.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" to indicate parts of the source code you
should add to or customize.

If your application uses MFC in a shared DLL, and your application is 
in a language other than the operating system's current language, you
will need to copy the corresponding localized resources MFC42XXX.DLL
from the Microsoft Visual C++ CD-ROM onto the system or system32 directory,
and rename it to be MFCLOC.DLL.  ("XXX" stands for the language abbreviation.
For example, MFC42DEU.DLL contains resources translated to German.)  If you
don't do this, some of the UI elements of your application will remain in the
language of the operating system.

/////////////////////////////////////////////////////////////////////////////
