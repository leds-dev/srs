// USCstreamDlg.cpp : implementation file
//

#include "stdafx.h"
#include "USCviewer.h"
#include "USCviewerDlg.h"
#include "USCstreamDlg.h"

#include "atlbase.h"

//#include "include\epservertypes.h"
//#include "include\ssnt.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static int 	STRM_SUCCESS = 0;
static epoch_client_usc *client_pid;
static epoch_client_point *point_pid;
static struct timeval client_timeout = {600, 0};
static struct timeval max_interval = {10, 0};

void log_event(CString msg);
void draw_stream_line(USCstreamDlg *s);

static void callback_usc_event (epoch_client_usc * cpid)
{
	USCstreamDlg *s;
	CString text;
	CString name, id, state;
	CString ack_id;
	double	raw;
	int index;

	s = (USCstreamDlg *)cpid->user_data;

	//msg.Format("(%s) callback_usc_event: type = %d",s->streamName,cpid->uscargs->type);
	//log_event(msg);

	switch (cpid->uscargs->type) {

		case EPUSCNOTIFICATION: 
			// get USC alarm data
			name  = cpid->uscargs->update.notification.point_name;
			id    = cpid->uscargs->update.notification.reference_id;
			state = cpid->uscargs->update.notification.state_text;
			raw   = cpid->uscargs->update.notification.raw_value;
			// insert new alarm line in display
			// text.Format("%-32s  %s  %8s       %G",name,id,state,raw);
			text.Format("%s  %-32s  %8s       %G",id,name,state,raw);
			// s->m_display_alarms.InsertString(0,text);
			s->m_display_alarms.AddString(text);
			break;

		case EPUSCACK:
			// get USC ack data
			name = cpid->uscargs->update.acknowledgment.point_name;
			id   = cpid->uscargs->update.acknowledgment.reference_id;
			// ack_id.Format("%-32s  %s",name,id);
			ack_id.Format("%s  %-32s",id,name);
			// find line with matching ack id in alarm display
			index = s->m_display_alarms.FindString(-1,ack_id);
			if (index >= 0) {
				// remove alarm from display
				s->m_display_alarms.DeleteString(index);
			} else {
				msg.Format("(%s) invalid USC ACK received. id = (%s)",ack_id);
				log_event(msg);
			}
			break;

		case EPUSCENABLE:
			msg.Format("GTACS-USC enable: point=%s",
				cpid->uscargs->update.state.point_name);
			log_event(msg);
			break;

		case EPUSCDISABLE:
			msg.Format("GTACS-USC disable: point=%s",
				cpid->uscargs->update.state.point_name);
			log_event(msg);
			break;

		default:
			break;
	}
}


static void callback_usc_point (epoch_client_point *cpid)
{
	USCstreamDlg *s;
	CString text;
	#define EPPTDATATYPE_U_LONG  0

	// get value of UNACKED_USC_COUNT
	int type = cpid->data_type;
	long  value = *(long *)cpid->value;
	s = (USCstreamDlg *)cpid->user_data;

	//msg.Format("(%s) callback_usc_point: value = %d",s->streamName,value);
	//log_event(msg);

	// do nothing if count has not changed
	if (s->streamAlarms == value) return;

	// update count in titlebar
	s->streamAlarms = value;
	text.Format("%s    STREAM:  %6.6s   ALARMS:  %d",USC_VERSION,
		s->streamName,s->streamAlarms);
	s->SetWindowText(text);

}

/////////////////////////////////////////////////////////////////////////////
// USCstreamDlg dialog


USCstreamDlg::USCstreamDlg(CWnd* pParent /*=NULL*/)
	: CDialog(USCstreamDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(USCstreamDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	// EnableDocking(CBRS_ALIGN_ANY);

}


void USCstreamDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(USCstreamDlg)
	DDX_Control(pDX, IDC_LISTBOX_ALARMS, m_display_alarms);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(USCstreamDlg, CDialog)
	//{{AFX_MSG_MAP(USCstreamDlg)
	ON_BN_CLICKED(ID_BUTTON_ACK, OnButtonAck)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// USCstreamDlg message handlers

BOOL USCstreamDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	char name[16+1];
	int status;
	char pointname[1024];

	// setup timer to poll stream
	UINT  TimerVal;
	TimerVal = SetTimer(IDT_POLL_STREAM,1000*streamPollInterval,NULL);

	// convert cstring to char
	strncpy_s(name,(LPCTSTR)streamName, sizeof(name));
	// strncpy(name,(LPCTSTR)streamName,sizeof(name));  // strncpy_s

	// Connect to stream. 
	if (ssnt_connect_stream (name, client_timeout, &streamConnection)) {
		if (streamIsDefault == USC_NO) {
			msg.Format("(%s) connect_stream failed",streamName);
			log_event(msg);
		}
		streamInitError = 1;
		return FALSE;
	}

	// get scid from point in sc stream
	epoch_client_point *scid_point;
	sprintf(pointname,"%s:SC_504_02_ID",streamName);  //sprintf_s 
	status = ssnt_look_up_point (pointname,client_timeout,&scid_point);
	if (status == STRM_SUCCESS) {
		if (ssnt_poll_point(scid_point) == STRM_SUCCESS) {
			// get point value
			long n = *(long *) scid_point->value;
			streamSCID.Format("%2d  ",n);
		}
	} else {
		msg.Format("(%s) ERR: look_up_point (%s) failed. status=%d",
			streamName,pointname,status);
		log_event(msg);
	}

	// request point service for unacked usc alarms 
	sprintf(pointname,"%s:UNACKED_USC_COUNT",streamName);

	status = ssnt_request_point ( pointname,  // point name
				EPCONVTYPE_DEFAULT,		// default conversion
				EPCBCOND_CHANGED_SAMPLES,		// on change 
				EPSERVICEMODE_ASYNC,	// async
				1,						// period
				client_timeout,			// timeout
				callback_usc_point,		// callback routine
				(void *)this,			// user_data 
				&point_pid );			// client point

	if (status != STRM_SUCCESS) {
		msg.Format("(%s) ERR: request for point %s failed. status=%d",
			streamName,pointname,status);
		log_event(msg);
		streamInitError = 1;
		return FALSE;
	}

	// request all current USC events
	status = ssnt_request_usc(name,EPUSCFULL,client_timeout,callback_usc_event,(void *)this,&client_pid);
	if (status) {
		msg.Format("(%s) ERR: request for USC-FULL service failed. status=%d",streamName,status);
		log_event(msg);
		streamInitError = 1;
		return FALSE;
	}

	// wait n seconds?

	// request future USC events
	status = ssnt_request_usc(name,EPUSCUPDATE,client_timeout,callback_usc_event,(void *)this,&client_pid);
	if (status) {
		msg.Format("(%s) ERR: request for USC-UPDATE failed. status=%d",streamName,status);
		log_event(msg);
		streamInitError = 1;
		return FALSE;
	}
	
	// set stream status to connected 
	streamInitError = 0;
	streamConnectStatus = USC_CONNECTED;
	streamAlarms = 0;
	streamAlarmsPrev = -1;

	// redraw line in stream display
	draw_stream_line(this);

	msg.Format("(%s) stream connected. scid = %s",streamName,streamSCID);
	log_event(msg);

	// display stream alarm window
	ShowWindow(SW_SHOWNORMAL);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void USCstreamDlg::OnButtonAck() 
{
	// TODO: Add your control notification handler code here

	CString  text;
	char stream[32+1];
	char	directive[1024];
	char	point[1024];
	char	id[1024];

	// get index of selected line
	int index = m_display_alarms.GetCurSel();
	if (index == LB_ERR) return;

	// convert streamname cstring to char
	strncpy(stream,(LPCTSTR)streamName,sizeof(stream));

	// get text of selected line
	m_display_alarms.GetText(index,text);

	// get point name and reference id from text
	sscanf(text,"%s %s",id,point);

	// construct "usc ack" directive 
	sprintf(directive,"@%s  USC ACK %s %s \n",stream,point,id);
	// send directive to stream
	int err = ssnt_execute_stol(directive,client_timeout,NULL);
	if (err != STRM_SUCCESS) {
		msg.Format("(%s) Error executing directive USC ACK",streamName);
		log_event(msg);
	}

	//msg.Format("ACK: execute_stol(%s)",directive);
	//log_event(msg);
}



void USCstreamDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnTimer(nIDEvent);

	int status = 0;

	switch (nIDEvent) {

		case IDT_POLL_STREAM: 

			// init error, terminate stream display
			if (streamInitError) {
				DestroyWindow();
				streamConnectStatus = USC_DISCONNECTED;
				streamSCID = " ";
				break;
			}

			if (streamConnectStatus == USC_DISCONNECTED) break;

			// poll stream
			if ((status = ssnt_read_stream(streamConnection,max_interval)) != STRM_SUCCESS) {
				// error reading stream 
				msg.Format("(%s) ERR: ssnt_read_stream failed. status=%d",streamName,status);
				log_event(msg);
				ssnt_cancel_point(point_pid);
				ssnt_disconnect_stream(streamConnection);

				streamConnectStatus = USC_DISCONNECTED;
				streamSCID = " ";

				// redraw line in stream display
				draw_stream_line(this);

				// terminate streamViewer
				DestroyWindow();
				msg.Format("(%s) streamViewer terminated by read_stream error",streamName);
				log_event(msg);


			}
			break;

		default:
			break;

	}
}

void USCstreamDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	msg.Format("(%S) OnClose ...",streamName);
	log_event(msg);
	CDialog::OnClose();
}
