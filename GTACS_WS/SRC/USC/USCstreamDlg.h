#if !defined(AFX_USCSTREAMDLG_H__FC121BB8_8A18_476C_BCE0_8743B7E940FB__INCLUDED_)
#define AFX_USCSTREAMDLG_H__FC121BB8_8A18_476C_BCE0_8743B7E940FB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// USCstreamDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// USCstreamDlg dialog

class USCstreamDlg : public CDialog
{
// Construction
public:
	CString streamSCID;
	USCstreamDlg(CWnd* pParent = NULL);   // standard constructor


// Dialog Data
	//{{AFX_DATA(USCstreamDlg)
	enum { IDD = IDD_USCSTREAM_DIALOG };
	CListBox	m_display_alarms;
	CString		streamName;
	CString		streamConnectStatus;
	CString		streamAlarmSound;
	CString		streamIsDefault;
	int			streamPollInterval;
	int			streamAlarms;
	int			streamAlarmsPrev;
	long		streamGridRow;
	epoch_client_connection *streamConnection;
	int			streamInitError;
	int			streamConnectCount;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(USCstreamDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(USCstreamDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonAck();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USCSTREAMDLG_H__FC121BB8_8A18_476C_BCE0_8743B7E940FB__INCLUDED_)
