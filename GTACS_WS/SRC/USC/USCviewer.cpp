// USCviewer.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "USCviewer.h"
#include "USCviewerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CUSCviewerApp

BEGIN_MESSAGE_MAP(CUSCviewerApp, CWinApp)
	//{{AFX_MSG_MAP(CUSCviewerApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUSCviewerApp construction

CUSCviewerApp::CUSCviewerApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CUSCviewerApp object

CUSCviewerApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CUSCviewerApp initialization

BOOL CUSCviewerApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.
/*
#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
*/
	CUSCviewerDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

int CUSCviewerApp::ExitInstance() 
{
	// TODO: Add your specialized code here and/or call the base class
		HKEY hkey = NULL;

	if (RegOpenKeyEx(HKEY_CURRENT_USER,_T("Environment"),0,KEY_SET_VALUE,&hkey) != ERROR_SUCCESS) {
		msg.Format("ExitInstance: error from RegOpenKeyEx");
		AfxMessageBox(msg,MB_OK);
		return -1;
	}
	if (RegDeleteValue(hkey,"USER") != ERROR_SUCCESS) {
		msg.Format("ExitInstance: error from RegDeleteValue");
		AfxMessageBox(msg,MB_OK);
		return -1;	
	}
	RegCloseKey(hkey);

	return CWinApp::ExitInstance();
}
