// USCviewer.h : main header file for the USCVIEWER application
//

#if !defined(AFX_USCVIEWER_H__A491591A_4F2B_4CE9_B074_F18979C0D9A2__INCLUDED_)
#define AFX_USCVIEWER_H__A491591A_4F2B_4CE9_B074_F18979C0D9A2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols


// user definition
// #include "epservertypes.h"
#include "ssnt.h"

#define USC_DEBUG        "USC_DEBUG"
#define USC_VERSION      "USCviewer 2.0"
#define USC_YES          "Yes"
#define USC_NO           "No "
#define USC_CONNECTED    "Connected   "
#define USC_DISCONNECTED "Disconnected"
#define IDT_POLL_SOUND    9996
#define IDT_POLL_ALARM    9997
#define IDT_POLL_CONNECT  9998
#define IDT_POLL_STREAM   9999


static CString msg;
static CString USCmode;

/////////////////////////////////////////////////////////////////////////////
// CUSCviewerApp:
// See USCviewer.cpp for the implementation of this class
//

class CUSCviewerApp : public CWinApp
{
public:
	CUSCviewerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUSCviewerApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CUSCviewerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USCVIEWER_H__A491591A_4F2B_4CE9_B074_F18979C0D9A2__INCLUDED_)
