// USCviewerDlg.cpp : implementation file
//



#include "stdafx.h"
#include "stdlib.h"

#include "USCviewer.h"
#include "USCviewerDlg.h"
#include "USCstreamDlg.h"

#include "epservertypes.h"
#include "ssnt.h"
static int 	STRM_SUCCESS= 0;
static struct timeval max_interval = {10, 0};

#include "SS_TrayIcon.h"
#include "SS_Wnd.h"

#include "mmsystem.h"

#include <iostream>
#include <fstream>
#include <string.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



// begin user declarations

//**********************************************************************
//
//  FUNCTION:     GetCurrentUserAndDomain - This function looks up
//                the user name and domain name for the user account
//                associated with the calling thread.
//
//  PARAMETERS:   szUser - a buffer that receives the user name
//                pcchUser - the size, in characters, of szUser
//                szDomain - a buffer that receives the domain name
//                pcchDomain - the size, in characters, of szDomain
//
//  RETURN VALUE: TRUE if the function succeeds. Otherwise, FALSE and
//                GetLastError() will return the failure reason.
//
//                If either of the supplied buffers are too small,
//                GetLastError() will return ERROR_INSUFFICIENT_BUFFER
//                and pcchUser and pcchDomain will be adjusted to
//                reflect the required buffer sizes.
//
//**********************************************************************

BOOL GetCurrentUserAndDomain(PTSTR szUser, PDWORD pcchUser,
      PTSTR szDomain, PDWORD pcchDomain) {

   BOOL         fSuccess = FALSE;
   HANDLE       hToken   = NULL;
   PTOKEN_USER  ptiUser  = NULL;
   DWORD        cbti     = 0;
   SID_NAME_USE snu;

   __try {

      // Get the calling thread's access token.
      if (!OpenThreadToken(GetCurrentThread(), TOKEN_QUERY, TRUE,
            &hToken)) {

         if (GetLastError() != ERROR_NO_TOKEN)
            __leave;

         // Retry against process token if no thread token exists.
         if (!OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY,
               &hToken))
            __leave;
      }

      // Obtain the size of the user information in the token.
      if (GetTokenInformation(hToken, TokenUser, NULL, 0, &cbti)) {

         // Call should have failed due to zero-length buffer.
         __leave;

      } else {

         // Call should have failed due to zero-length buffer.
         if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
            __leave;
      }

      // Allocate buffer for user information in the token.
      ptiUser = (PTOKEN_USER) HeapAlloc(GetProcessHeap(), 0, cbti);
      if (!ptiUser)
         __leave;

      // Retrieve the user information from the token.
      if (!GetTokenInformation(hToken, TokenUser, ptiUser, cbti, &cbti))
         __leave;

      // Retrieve user name and domain name based on user's SID.
      if (!LookupAccountSid(NULL, ptiUser->User.Sid, szUser, pcchUser,
            szDomain, pcchDomain, &snu))
         __leave;

      fSuccess = TRUE;

   } __finally {

      // Free resources.
      if (hToken)
         CloseHandle(hToken);

      if (ptiUser)
         HeapFree(GetProcessHeap(), 0, ptiUser);
   }

   return fSuccess;
}


#define  TRAYICON_ID  0
SS_TrayIcon* m_pTrayIcon;
USCstreamDlg  *s;
static int x = 0;
static int y = 0;
static int events_line = 0;
static int events_log  = 0;
static int connect_index = 0;
static char LogFileName[1024];

//static ofstream events_file(LogFile,ios::trunc);
static ofstream events_file;

void log_event (CString msg) {


	CString text;
	CTime   time = CTime::GetCurrentTime();
	CString yyyy = time.FormatGmt( "%Y" );
	CString ddd  = time.FormatGmt( "%j" );
	CString hh   = time.FormatGmt( "%H" );
	CString mm   = time.FormatGmt( "%M" );
	CString ss   = time.FormatGmt( "%S" );
	text.Format("%s-%s:%s:%s:%s   %s",yyyy,ddd,hh,mm,ss,msg);

	// add message into event display
	m_display_events.AddString(text);
	m_display_events.SetCurSel(events_line++);	
	m_display_events.UpdateWindow();

	// open log file if first time
	if (events_line == 1) {
		events_file.open(LogFileName,ios::trunc,644);
		events_log = events_file.is_open();
	}

	// write message to log file
	if (events_log) events_file << text + "\n" << flush;

}

void draw_stream_line (USCstreamDlg *s)
{

	long color = 0;

	if (s->streamGridRow < 1) return;

	//msg.Format("(draw_stream_line) stream=%s  scid=%s  status=%s",
	//	s->streamName,s->streamSCID,s->streamConnectStatus);
	//log_event(msg);

	long row = s->streamGridRow;
	m_grid_streams.SetRowSel(row);

	if (s->streamConnectStatus == USC_CONNECTED) 
		// color = 0x0002F000; // green
		color = 0x00FF0000; // blue
	else 
		//color = 0x000000FF; // red
		color = 0x00F00000; // blue


	CString text;
	text.Format("%3d",s->streamAlarms);

	m_grid_streams.SetCol(0);
	m_grid_streams.SetCellForeColor(color);
	m_grid_streams.SetCol(1);
	m_grid_streams.SetCellForeColor(color);
	m_grid_streams.SetCol(2);
	m_grid_streams.SetCellForeColor(color);
	m_grid_streams.SetCol(3);
	m_grid_streams.SetCellForeColor(color);
	m_grid_streams.SetCol(4);
	m_grid_streams.SetCellForeColor(color);
				
	m_grid_streams.SetTextArray(row*grid_streams_cols + COL_NAME,s->streamName);
	m_grid_streams.SetTextArray(row*grid_streams_cols + COL_SCID,s->streamSCID);
	m_grid_streams.SetTextArray(row*grid_streams_cols + COL_ALARMS,text);
	m_grid_streams.SetTextArray(row*grid_streams_cols + COL_CONNECT,s->streamConnectStatus);
	m_grid_streams.SetTextArray(row*grid_streams_cols + COL_DEFAULT,s->streamIsDefault);

}



int usc_read_cfg (CString filename) {

    // open usc configuration file.
	//ifstream ifs(filename,ios::in | ios::nocreate);
	ifstream ifs(filename,ios::in);
	if (!ifs) {
		//msg.Format("ERR: cannot open config file (%s)",filename);
		//log_event(msg);
		return -1;
	}

	char line[1024];
	char arg1[1024];
	char arg2[1024];
	char arg3[1024];

    while (ifs.getline(line,1024)) {

		arg1[0] = NULL;
		arg2[0] = NULL;
		arg3[0] = NULL;
		CString input = line;
		sscanf(input,"%s %s %s",arg1,arg2,arg3);
		CString keyword = arg1;
		CString delim   = arg2;
		CString value   = arg3;

		if (keyword.Find("NORMAL_STREAM") != -1) {
			s = new USCstreamDlg;
			s->streamName = value;
			s->streamSCID = " ";
			s->streamIsDefault = USC_NO;
			s->streamAlarms = 0;
			s->streamAlarmsPrev = -1;
			s->streamAlarmSound = AlarmSoundFile;
			s->streamConnectStatus = USC_DISCONNECTED;
			s->streamConnectCount = 0;
			s->streamPollInterval = PollStreamInterval;
			StreamArray.Add(s);

		} else if (keyword.Find("DEFAULT_STREAM") != -1) {
			s = new USCstreamDlg;
			s->streamName = value;
			s->streamSCID = " ";
			s->streamAlarms = 0;
			s->streamAlarmsPrev = -1;
			s->streamIsDefault = USC_YES;
			s->streamAlarmSound = AlarmSoundFile;
			s->streamConnectStatus = USC_DISCONNECTED;
			s->streamConnectCount = 0;
			s->streamPollInterval = PollStreamInterval;
			StreamArray.Add(s);

		} else if (keyword.Find("POLL_CONNECT_INTERVAL") != -1) {
			sscanf(value,"%d",&PollConnectInterval);

		} else if (keyword.Find("POLL_ALARM_INTERVAL") != -1) {
			sscanf(value,"%d",&PollAlarmInterval);

		} else if (keyword.Find("POLL_STREAM_INTERVAL") != -1) {
			sscanf(value,"%d",&PollStreamInterval);

		} else if (keyword.Find("POLL_SOUND_INTERVAL") != -1) {
			sscanf(value,"%d",&PollSoundInterval);

		} else if (keyword.Find("USC_LOG_FILE") != -1) {
			LogFile = value;
			strcpy(LogFileName,arg3);
		
		} else if (keyword.Find("ALARM_WAV_FILE") != -1) {
			AlarmSoundFile = value;
		} 
		
	}

	// close usc configuration file
	ifs.close();
	return 0;

	 //AfxMessageBox("Click OK to continue");
}


// end user code

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUSCviewerDlg dialog

CUSCviewerDlg::CUSCviewerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUSCviewerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUSCviewerDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUSCviewerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUSCviewerDlg)
	//DDX_Control(pDX, IDC_LISTBOX_STREAMS, m_display_streams);
	DDX_Control(pDX, IDC_LISTBOX_EVENTS, m_display_events);
	DDX_Control(pDX, IDC_MSFLEXGRID_STREAMS, m_grid_streams);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CUSCviewerDlg, CDialog)
	//{{AFX_MSG_MAP(CUSCviewerDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_BUTTON_CONNECT, OnButtonConnect)
	ON_BN_CLICKED(ID_BUTTON_DISCONNECT, OnButtonDisconnect)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(ID_BUTTON_HELP, OnButtonHelp)
	ON_WM_HELPINFO()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUSCviewerDlg message handlers

BOOL CUSCviewerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	// Create the font to be used
	CFont cfont;    
	//cfont.CreateFont(14, 0, 0, 0, 
	//	FW_THIN,
    //   0, 0, 0, DEFAULT_CHARSET, 
	//	OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, 
	//	FIXED_PITCH | FF_MODERN, "courier");

	cfont.CreateFont(-11, 0, 0, 0, 
		FW_THIN,
        0, 0, 0, DEFAULT_CHARSET, 
		OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, 
		FIXED_PITCH | FF_MODERN, _T("courier"));

	// Set the font for the display area
	//m_display_events.SetFont(&cfont);

	msg.Format("%s      ",USC_VERSION);
	SetWindowText(msg);
	msg.Format("%s starting...",USC_VERSION);
	log_event(msg);

	CString UserName = getenv("USER");
	msg.Format("getenv(USER) = (%s)",UserName);
	log_event(msg);

	USCmode = getenv("USC_MODE");
	if (USCmode != "") {
		msg.Format("getenv(USC_MODE) = (%s)",USCmode);
		log_event(msg);
	}

	// read usc configuration file
	int err = usc_read_cfg("S:\\EPOCH_DATABASE\\reports\\usc.cfg");
	if (err != 0) 
		err = usc_read_cfg("usc.cfg.txt");

	int n = StreamArray.GetSize();

	msg.Format("Log File is (%s)",LogFile);
	log_event(msg);
	msg.Format("Alarm Sound File is (%s)",AlarmSoundFile);
	log_event(msg);
	msg.Format("Number of streams in config file = %d",n);
	log_event(msg);
	msg.Format("Connect Poll Interval is %d secs",PollConnectInterval);
	log_event(msg);
	msg.Format("Alarm Poll Interval is  %d secs",PollAlarmInterval);
	log_event(msg);
	msg.Format("Sound Poll Interval is %d secs",PollSoundInterval);
	log_event(msg);
	msg.Format("Stream Poll Interval is %d secs",PollStreamInterval);
	log_event(msg);

	//
	// display all streams in main viewer
	//

	grid_streams_cols = m_grid_streams.GetCols();

	m_grid_streams.SetTextArray(COL_NAME," Stream");
	m_grid_streams.SetTextArray(COL_SCID,"  SCID");
	m_grid_streams.SetTextArray(COL_ALARMS," Alarms");
	m_grid_streams.SetTextArray(COL_CONNECT," ConnectStatus");
	m_grid_streams.SetTextArray(COL_DEFAULT," DefaultStream");

	USCstreamDlg *s;
	CString TotalAlarms = "  0";
	long i;
	int row = 1;
	for (i=0; i<n; i++) {
		s = (USCstreamDlg *)StreamArray[i];
		s->streamGridRow = row;		
		m_grid_streams.SetTextArray(row*grid_streams_cols + COL_NAME,s->streamName);
		m_grid_streams.SetTextArray(row*grid_streams_cols + COL_SCID,s->streamSCID);
		m_grid_streams.SetTextArray(row*grid_streams_cols + COL_ALARMS,TotalAlarms);
		m_grid_streams.SetTextArray(row*grid_streams_cols + COL_CONNECT,"Not Connected");
		m_grid_streams.SetTextArray(row*grid_streams_cols + COL_DEFAULT,s->streamIsDefault);
		row++;
	}

	//m_grid_streams.SetColWidth(COL_SCID, m_grid_streams.GetCellWidth());
	m_grid_streams.SetColWidth(COL_NAME, 1000);
	m_grid_streams.SetColWidth(COL_SCID, 1000);
	m_grid_streams.SetColWidth(COL_ALARMS, 1000);
	m_grid_streams.SetColWidth(COL_CONNECT,2400);
	m_grid_streams.SetColWidth(COL_DEFAULT,2000);

	// set total rows in grid to total streams
	m_grid_streams.SetRows(row);

	//grid_streams = CUSCviewer::m_grid_streams;

	// force horizontal scroll
	m_display_events.SetHorizontalExtent(900);

	// 
	// setup timers
	//

	UINT  TimerVal;
	TimerVal = SetTimer(IDT_POLL_ALARM,PollAlarmInterval*1000,NULL);
	TimerVal = SetTimer(IDT_POLL_SOUND,PollSoundInterval*1000,NULL);
	TimerVal = SetTimer(IDT_POLL_CONNECT,PollConnectInterval*1000,NULL);
	
	// create blinking usc tray icon
	m_pTrayIcon = new SS_TrayIcon(AfxGetInstanceHandle(),1,1);
	m_pTrayIcon->LoadIcon(TRAYICON_ID, 0, IDI_USC_NORMAL);
	m_pTrayIcon->Mode(TRAYICON_ID, SSTI_MODE_BLINK);
	m_pTrayIcon->ToolTip(TRAYICON_ID,_T("USC alarm not ack'ed"));
	m_pTrayIcon->SetAnimateSpeed(TRAYICON_ID, 1000);
	m_pTrayIcon->HideIcon(); 

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CUSCviewerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

void CUSCviewerDlg::OnDestroy()
{
	WinHelp(0L, HELP_QUIT);
	CDialog::OnDestroy();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CUSCviewerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CUSCviewerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CUSCviewerDlg::OnButtonConnect() 
{
	// TODO: Add your control notification handler code here
	CString selection;

	// get stream name from selected row
	long row = m_grid_streams.GetRowSel();
	if (row < 1) return;
	selection = m_grid_streams.GetTextMatrix(row,0);

	// get stream object
	USCstreamDlg *s;
	s = (USCstreamDlg *)StreamArray[row-1];

	// draw_stream_line(s);

	if (s->streamConnectStatus == USC_CONNECTED) {
		msg.Format("Stream %s is already connected",s->streamName);
		AfxMessageBox(msg);
		return;
	}

	// output message if not default stream
	//if (s->streamIsDefault != USC_YES) {
		msg.Format("Connecting to stream %s (row=%d)",s->streamName,row);
		log_event(msg);
	//}

	// create new stream viewer
	CString title;
	title.Format("%s    STREAM:  %6.6s   ALARMS:  %d",USC_VERSION,selection,0);
	s->Create(IDD_USCSTREAM_DIALOG, this);
	s->SetWindowPos(NULL,x,y,0,0,SWP_NOSIZE);
	s->SetWindowText(title);
	// s->ShowWindow(SW_SHOWNORMAL);
	x = x + 30;
	y = y + 30;

}

void CUSCviewerDlg::OnButtonDisconnect() 
{
	// TODO: Add your control notification handler code here

	CString selection;

	// get row selected
	long row = m_grid_streams.GetRowSel();
	if (row < 1) return;
	// get stream name
	selection = m_grid_streams.GetTextMatrix(row,0);

	//index = m_display_streams.GetCurSel();
	//if (index == LB_ERR) return;

	//m_display_streams.GetText(index,selection);
	USCstreamDlg *s;
	s = (USCstreamDlg *)StreamArray[row-1];

	if (s->streamIsDefault == USC_YES) {	
		msg.Format("Default Stream %s cannot be disconnected",s->streamName);
		AfxMessageBox(msg);
		return;
	}

	if (s->streamConnectStatus == USC_DISCONNECTED) {
		msg.Format("Stream %s is not connected",s->streamName);
		AfxMessageBox(msg);
		return;
	}

	msg.Format("Disconnecting stream %s",selection);
	log_event(msg);
	s->DestroyWindow();
	s->streamConnectStatus = USC_DISCONNECTED;
	s->streamAlarms = 0;
	s->streamAlarmsPrev = -1;
	s->streamSCID = " ";
	ssnt_disconnect_stream(s->streamConnection);

	// redraw line in stream display
	draw_stream_line(s);

}

void CUSCviewerDlg::OnTimer(UINT nIDEvent) 
{

	int		i,n,max;
	CString title;

	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnTimer(nIDEvent);
	
	//msg.Format("*** OnTimer called with id=%d",nIDEvent);
	//log_event(msg);

	switch (nIDEvent) {

		case IDT_POLL_CONNECT: 

			// connect to one unconnected default stream
			max = StreamArray.GetSize();
			n = max;

			while (n > 0) {

				// get stream object
				s = (USCstreamDlg *)StreamArray[connect_index];
				// update pointers
				n--;
				i = connect_index + 1;
				if (++connect_index >= max) connect_index = 0;

				if (s->streamConnectStatus==USC_DISCONNECTED && s->streamIsDefault==USC_YES ) {
					m_grid_streams.SetRow(i);
					if (s->streamConnectCount==0) {
						msg.Format("Connecting to Default stream %s",s->streamName);
						s->streamConnectCount++;
						log_event(msg);
					} 
					// create new stream viewer
					// using code snippet from OnButtonConnect()
					title.Format("%s    STREAM:  %6.6s   ALARMS:  %d",
						USC_VERSION,s->streamName,0);
					s->Create(IDD_USCSTREAM_DIALOG, this);
					s->SetWindowPos(NULL,x,y,0,0,SWP_NOSIZE);
					s->SetWindowText(title);
					x = x + 30;
					y = y + 30;
					break;
				}
			}
			break;


		case IDT_POLL_SOUND: 

			if (TotalAlarms > 0) {
				// play alarm sound  
				HMODULE hmodThis = GetModuleHandle(NULL);
				PlaySound(s->streamAlarmSound,hmodThis,SND_FILENAME||SND_ASYNC); 
				//msg.Format("playing sound file (%s)",s->streamAlarmSound);
				//log_event(msg); 
			}
			break;

		case IDT_POLL_ALARM: 
			// count connected streams and alarms
			TotalConnectedStreams = 0;
			TotalAlarms = 0;
			n = StreamArray.GetSize();

			for (i=0; i<n; i++) {
				s = (USCstreamDlg *)StreamArray[i];
				if (s->streamConnectStatus==USC_CONNECTED) {
					TotalConnectedStreams++;
					TotalAlarms += s->streamAlarms;
					// redraw line in stream display if alarms has changed
					if (s->streamAlarms != s->streamAlarmsPrev) {
						draw_stream_line(s);
						s->streamAlarmsPrev = s->streamAlarms;
					}
				}
			}

			if (TotalAlarms > 0) {
				// show blinking tray icon if first time
				if (IconIsFlashing == FALSE) {
					m_pTrayIcon->ShowIcon(TRAYICON_ID); 
					IconIsFlashing = TRUE;
					// play alarm sound  
					HMODULE hmodThis = GetModuleHandle(NULL);
					PlaySound(s->streamAlarmSound,hmodThis,SND_FILENAME||SND_ASYNC); 
				}
			} else {
				// hide blinking icon if already blinking
				if (IconIsFlashing == TRUE) {
					m_pTrayIcon->HideIcon(); 
					IconIsFlashing = FALSE;
				}
			}

			if (TotalConnectedStreams != PrevTotalConnectedStreams ||
				TotalAlarms != PrevTotalAlarms) {
				// update info in titlebar
				msg.Format("%s      Streams Connected: %d   Total Alarms: %d",
					USC_VERSION,TotalConnectedStreams,TotalAlarms);
				SetWindowText(msg);
				PrevTotalAlarms = TotalAlarms;
				PrevTotalConnectedStreams = TotalConnectedStreams;
			}
			break;

		default:
		break;

	}


}

void CUSCviewerDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default

	AfxMessageBox("USCviewer cannot be terminated");

	if (USCmode == USC_DEBUG) {

		// comment out next line to disable X button
		CDialog::OnClose();
		// close all stream connections
		int n = StreamArray.GetSize();
		int i = 0;
		for (i=0; i<n; i++) {
			s = (USCstreamDlg *)StreamArray[i];
			if (s->streamConnectStatus==USC_CONNECTED) 
				ssnt_disconnect_stream(s->streamConnection);
		}
		
	}
}

BEGIN_EVENTSINK_MAP(CUSCviewerDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(CUSCviewerDlg)
	ON_EVENT(CUSCviewerDlg, IDC_MSFLEXGRID_STREAMS, 69 /* SelChange */, OnSelChangeMsflexgridStreams, VTS_NONE)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CUSCviewerDlg::OnSelChangeMsflexgridStreams() 
{
	// TODO: Add your control notification handler code here
	int nRow = m_grid_streams.GetRow();

	
}




void CUSCviewerDlg::OnButtonHelp() 
{
	// TODO: Add your control notification handler code here
	// AfxMessageBox("Help not yet available");
	ShellExecute(this->m_hWnd,"open","USCViewer.hlp","","", SW_SHOW );
}

BOOL CUSCviewerDlg::OnHelpInfo(HELPINFO* pHelpInfo) 
{
	// TODO: Add your message handler code here and/or call default
	
	ShellExecute(this->m_hWnd,"open","USCViewer.hlp","","", SW_SHOW );
	// return CDialog::OnHelpInfo(pHelpInfo);
	return TRUE;
}
