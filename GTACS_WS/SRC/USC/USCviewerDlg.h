// USCviewerDlg.h : header file
//
//{{AFX_INCLUDES()
#include "msflexgrid.h"
//}}AFX_INCLUDES


#include "USCstreamDlg.h"

#if !defined(AFX_USCVIEWERDLG_H__FDEDE0E8_03B8_4D1D_A94F_95CAAAEE4973__INCLUDED_)
#define AFX_USCVIEWERDLG_H__FDEDE0E8_03B8_4D1D_A94F_95CAAAEE4973__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// User Global Declarations

#define COL_NAME    0
#define COL_SCID	1
#define COL_ALARMS  2
#define COL_CONNECT 3
#define COL_DEFAULT 4
static  int  grid_streams_cols = 0;

//static	CListBox	m_display_streams;
static  CMSFlexGrid	m_grid_streams;
static	CListBox	m_display_events;
static  CString		AlarmSoundFile;
static  CString		LogFile = "usc.log.txt";
static  int			TotalConnectedStreams = 0;
static  int			TotalAlarms = 0;
static  int			PrevTotalConnectedStreams = 0;
static  int			PrevTotalAlarms = 0;
static  int			PollConnectInterval = 10;
static  int			PollSoundInterval = 5;
static  int			PollAlarmInterval = 1;
static  int			PollStreamInterval = 1;
static  boolean		IconIsFlashing = FALSE;
static  CPtrArray	StreamArray;
 
/////////////////////////////////////////////////////////////////////////////
// CUSCviewerDlg dialog

class CUSCviewerDlg : public CDialog
{
// Construction
public:
	CUSCviewerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CUSCviewerDlg)
	enum { IDD = IDD_USCVIEWER_DIALOG };
	// CListBox	m_display_streams;
	// CMSFlexGrid	m_grid_streams;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUSCviewerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CUSCviewerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButtonConnect();
	afx_msg void OnButtonDisconnect();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	afx_msg void OnSelChangeMsflexgridStreams();
	afx_msg void OnButtonHelp();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_USCVIEWERDLG_H__FDEDE0E8_03B8_4D1D_A94F_95CAAAEE4973__INCLUDED_)
