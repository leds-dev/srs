Image Editor
	URelease\
		Contains the final release for WinNT
		(Faster for any NT system.)
	Release\
		Contains the final release for Win9x/NT

	The only file needed for the release is ImageEditor.exe.  It'll set up
	a nest wherever it runs, as long as there are a few extra kB of hard drive
	space for a temp help file.  If it's running from a hard disk or network
	path, it also associates the .IMG type to itself.
	For compilation, ImageEditor's code depends on the HTML Help libraries,
	downloadable from Microsoft after a fair amount of searching.

	The source is peppered with comments and notes.  Peruse at will.
	