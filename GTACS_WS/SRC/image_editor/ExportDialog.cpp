/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       ExportDialog.cpp
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale

         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
		 V2.0        10/04       F. Shaw         Modifications for Virtual memory
		                                         mapping.  GTACS Build 9
*******************************************************************************/
// Implementation for the [Export] dialog box.

////////////////////////////////////////////////////////////////////////////////
// Headers                            //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ImageEditor.h"
#include "ImageEditorDlg.h"
#include "ExportDialog.h"
#include <time.h>
#include "htmlhelp.h"

////////////////////////////////////////////////////////////////////////////////
// Definitions                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#define DEF_ExportTopic _T("export.htm")
#undef super
#define super CDialog

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define _validate(x) (m_bHex?Doubleword::ValidateHex(x,sz_dword):Doubleword::ValidateDec(x,sz_dword))

////////////////////////////////////////////////////////////////////////////////
// Construction/Destruction           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CExportDialog::CExportDialog
Parameters:     [CExportDialog *this]
                CWnd *pParent
                    Pointer to parent window
Returns:        void
Description:    Called to initialize the dialog object.
PDL:
    Clear values to their defaults and save the editor's window.
**/
CExportDialog::CExportDialog(CWnd *pParent)
	:super(CExportDialog::IDD,pParent)
{
	//{{AFX_DATA_INIT(CExportDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
    editor=0;
    exporting=cancel=inContextHelp=chlbDown=false;
    editor=(CImageEditorDlg *)pParent;
    m_bHex=true;
}

#if 0
void CExportDialog::DoDataExchange(CDataExchange *pDX)
{
	super::DoDataExchange(pDX);
    //This keeps the ClassWizard happy.
	//{{AFX_DATA_MAP(CExportDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}
#endif

////////////////////////////////////////////////////////////////////////////////
// Message Map                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
BEGIN_MESSAGE_MAP(CExportDialog,super)
	//{{AFX_MSG_MAP(CExportDialog)
	ON_WM_CLOSE()
	ON_EN_CHANGE(IDC_EDT_EXPRANGEHI,OnChangeEdtExpRangeHi)
	ON_EN_CHANGE(IDC_EDT_EXPRANGELO,OnChangeEdtExpRangeLo)
	ON_BN_CLICKED(IDC_CHK_EXPHEADER,OnChkBox)
    ON_BN_CLICKED(IDC_RDO_EXPFLEX,OnChkExpFlex)
	ON_BN_CLICKED(IDC_RDO_EXPHTML,OnChkExpHTML)
    ON_BN_CLICKED(IDC_RDO_EXPTEXT,OnChkExpText)
	ON_BN_CLICKED(IDC_RDO_EXPALL,OnRdoExpAll)
	ON_BN_CLICKED(IDC_RDO_EXPRANGE,OnRdoExpRange)
	ON_EN_KILLFOCUS(IDC_EDT_EXPRANGELO,OnKillFocusEdtExpRangeLo)
	ON_EN_KILLFOCUS(IDC_EDT_EXPRANGEHI,OnKillFocusEdtExpRangeHi)
    ON_WM_KEYUP()
	ON_BN_CLICKED(IDC_BTN_EXPORTFILE,OnBtnSelExport)
	ON_EN_SETFOCUS(IDC_EDT_EXPRANGELO,OnSetFocusEdtExprangelo)
	ON_EN_SETFOCUS(IDC_EDT_EXPRANGEHI,OnSetFocusEdtExprangehi)
	ON_BN_CLICKED(IDC_BTN_CONTEXTHELP,OnContextHelp)
	ON_BN_CLICKED(IDC_RDO_EXPHEXASCII,OnRdoExpHexASCII)
    ON_BN_CLICKED(IDC_CHK_SIMPLE,OnChkBox)
	ON_BN_CLICKED(IDC_CHK_ADDXHEX, OnChkAddxHex)
	//}}AFX_MSG_MAP
#ifdef HAVE_TOOLTIPS
    ON_NOTIFY_EX(TTN_NEEDTEXT,0,OnToolTipNeedText)
#endif
END_MESSAGE_MAP()

////////////////////////////////////////////////////////////////////////////////
// Message Handlers                   //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CExportDialog::OnInitDialog
Parameters:     [CExportDialog *this]
Returns:        BOOL
                    FALSE to leave window focus alone
Description:    Called to initialize the Export dialog window
                (Handles WM_INITDIALOG)
PDL:
    Set the dialog's icon
    Load the previously used address range and radio button settings
    Initialize the progress control
    Load the previously used file format and settings
    Load the most recently used output directory
**/
BOOL CExportDialog::OnInitDialog()
{
	super::OnInitDialog();

    HICON i=::AfxGetApp()->LoadIcon(IDI_EXPORTBOX);
    SetIcon(i,TRUE);
    SetIcon(i,FALSE);

    inM=true;
    if(editor->ExportData.flags & CImageEditorDlg::__ExportData::AllSel){
        OnRdoExpAll();
    } else {
        OnRdoExpRange();

        CString s;
        // s.Format(_T("%08.8X"),editor->minDisplayed>>1);
		s.Format(_T("%08.8X"),editor->minDisplayed);

        SetDlgItemText(IDC_EDT_EXPRANGELO,s);

		if(editor->minDisplayed+1024>editor->highestAddress) {
            // s.Format(_T("%08.8X"),editor->highestAddress>>1);
			s.Format(_T("%08.8X"),editor->highestAddress);
		} else {
            // s.Format(_T("%08.8X"),(editor->minDisplayed+1023)>>1);
			s.Format(_T("%08.8X"),(editor->minDisplayed+1023));
		}
		SetDlgItemText(IDC_EDT_EXPRANGEHI,s);
    }
    CheckDlgButton(IDC_CHK_ADDXHEX,TRUE);
    inM=false;

    m_CrlProgress=((CProgressCtrl *)GetDlgItem(IDC_PRG_EXPORT));
    m_CrlProgress->SetRange(0,99);
    m_CrlProgress->SetPos(0);
    m_CrlProgress->EnableWindow(FALSE);

    CheckDlgButton(IDC_CHK_EXPHEADER,!!(editor->ExportData.flags & CImageEditorDlg::__ExportData::Header));
    CheckDlgButton(IDC_CHK_SIMPLE,!!(editor->ExportData.flags & CImageEditorDlg::__ExportData::ColHeaders));
    SetPreviewText();

    LPCTSTR cf;
    int m = 0;

    SetDlgItemText(IDC_EDT_EXPIMGFILE,CImageEditorDlg::Ellipsis(editor->currentFile));

    cf=(LPCTSTR)editor->currentFile;
    int l=m=editor->currentFile.GetLength();
    for(--l;l && cf[l]!='.' && cf[l]!='\\';l--)
            ; // Null for loop

    if(!l || cf[l]=='\\'){
        outputFile=editor->currentFile+_T(".txt");
    } else {
        outputFile=editor->currentFile.Left(l)+_T(".txt");
    }

    if(editor->ExportData.outputDir.GetLength()) {
        l=outputFile.ReverseFind('\\');
        if(l>=0)
            outputFile=editor->ExportData.outputDir+outputFile.Right((m-l)-1);
    }

    SetDlgItemText(IDC_BTN_EXPORTFILE,CImageEditorDlg::Ellipsis(outputFile));

    GetDlgItem(IDOK)->SetFocus();

    ((CButton *)GetDlgItem(IDC_BTN_CONTEXTHELP))->SetIcon(::AfxGetApp()->LoadIcon(IDI_CONTEXTHELP));

    switch(editor->ExportData.format)
    {
    case 0: OnChkExpText();break;
    case 1: OnChkExpHTML();break;
    case 2: OnChkExpFlex();break;
    default: OnRdoExpHexASCII();
    }
    UpdateHelpTopic(this,DEF_ExportTopic);

#ifdef HAVE_TOOLTIPS
#if HAVE_TOOLTIPS==1
    EnableTrackingToolTips();
#else
    EnableToolTips();
#endif
#endif
    return FALSE;
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnRdoExpAll
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the "Export all" radio button is selected
PDL:
    Check the "Export all" radio button
    Disable and clip the range edit boxes and text
    Update the preview text
**/
void CExportDialog::OnRdoExpAll()
{
    CheckRadioButton(IDC_RDO_EXPALL,IDC_RDO_EXPRANGE,IDC_RDO_EXPALL);
    CEdit *edt=(CEdit *)GetDlgItem(IDC_EDT_EXPRANGELO);
    edt->EnableWindow(FALSE);

    edt=(CEdit *)GetDlgItem(IDC_EDT_EXPRANGEHI);
    edt->EnableWindow(FALSE);

    GetDlgItem(IDC_STC_EXPTO)->EnableWindow(FALSE);
    SetPreviewText();
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnRdoExpRange
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the "Export range" radio button is selected.
PDL:
    Check the "Export range" radio button
    Enable and clip the range edit boxes and text
    IF message catching isn't blocked THEN
        Ensure that the range values are valid
    ENDIF
    Update the preview text
**/
void CExportDialog::OnRdoExpRange()
{
    CheckRadioButton(IDC_RDO_EXPALL,IDC_RDO_EXPRANGE,IDC_RDO_EXPRANGE);
    CEdit *edt1=(CEdit *)GetDlgItem(IDC_EDT_EXPRANGELO);
    CEdit *edt2=(CEdit *)GetDlgItem(IDC_EDT_EXPRANGEHI);

    edt1->EnableWindow();
    edt1->SetFocus();
    edt2->EnableWindow();
    GetDlgItem(IDC_STC_EXPTO)->EnableWindow();

    if(!inM)
    {
        if(m_bHex)
        {
            Doubleword::ValidateHex(edt1,sz_dword);
            Doubleword::ValidateHex(edt2,sz_dword);
        }
        else
        {
            Doubleword::ValidateDec(edt1,sz_dword);
            Doubleword::ValidateDec(edt2,sz_dword);
        }
        SetPreviewText();
    }
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnChkBox
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Catches any check-box clicking to refresh the preview.
PDL:
    Update preview text
**/
void CExportDialog::OnChkBox()
{
    SetPreviewText();
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnChangeEdtExpRangeLo
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the lower end of the range is changed.
PDL:
    IF message-catching is enabled AND the function isn't
       already executing THEN
        Ensure that the lower end of the range is a valid number
        Update preview text
    ENDIF
**/
void CExportDialog::OnChangeEdtExpRangeLo()
{
    static bool recursion=false;
    if(recursion) return;
    recursion=true;

    register CEdit *e=(CEdit *)GetDlgItem(IDC_EDT_EXPRANGELO);
    _validate(e);
    SetPreviewText();
    recursion=false;
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnChangeEdtExpRangeHi
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the higher end of the range is changed.
PDL:
    IF message-catching is enabled AND the function isn't
       already executing THEN
        Ensure that the higher end of the range is a valid number
        Update preview text
    ENDIF
**/
void CExportDialog::OnChangeEdtExpRangeHi()
{
    static bool recursion=false;
    if(recursion) return;
    recursion=true;

    register CEdit *e=(CEdit *)GetDlgItem(IDC_EDT_EXPRANGEHI);
    _validate(e);
    SetPreviewText();
    recursion=false;
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnKillFocusEdtExpRangeLo
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the range-low control loses focus to
                validate its text.
PDL:
    IF the function is not currently executing THEN
        Validate the range-low text control
        IF it needs a default value THEN
            Set it to the lowest loaded value in the editor
        ENDIF
        Update the preview text
    ENDIF
**/
void CExportDialog::OnKillFocusEdtExpRangeLo()
{
    static bool recursion=false;
    if(recursion) return;
    recursion=true;

    CEdit *edt=(CEdit *)GetDlgItem(IDC_EDT_EXPRANGELO);
    _validate(edt);
    if(!edt->GetWindowTextLength())
    {
        CString s;
        s.Format(m_bHex?_T("%X"):_T("%u"),editor->lowestAddress>>1);
        edt->SetWindowText(s);
    }
    SetPreviewText();
    recursion=false;
}

///////////////////////////////////////
/**
Function:       OnKillFocusEdtExpRangeHi
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the range-high control loses focus
                to validate its text.
PDL:
    IF the function is not currently executing THEN
        Validate the range-high text control
        IF it needs a default value THEN
            Set it to the highest loaded value in the editor
        ENDIF
        Update the preview text
    ENDIF
**/
void CExportDialog::OnKillFocusEdtExpRangeHi()
{
    static bool recursion=false;
    if(recursion) return;
    recursion=true;

    CEdit *edt=(CEdit *)GetDlgItem(IDC_EDT_EXPRANGEHI);
    _validate(edt);
    if(!edt->GetWindowTextLength())
    {
        CString s;
        s.Format(m_bHex?_T("%X"):_T("%u"),editor->highestAddress>>1);
        edt->SetWindowText(s);
    }

    SetPreviewText();
    recursion=false;
}

///////////////////////////////////////
/**
Function:       OnBtnSelExport
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the output file selection button is clicked.
PDL:
    Open the "Save As" dialog
    IF the user cancelled the operation THEN
        RETURN
    ENDIF

    Save the selected output filename

    IF a valid extension is returned in the filename THEN
        Select the appropriate output format radio button
    ELSE
        Change the extension on the filename to match the selected
            output format.
    ENDIF
    Update the filename boxes
**/
void CExportDialog::OnBtnSelExport()
{
    CFileDialog dlg(FALSE,NULL,outputFile,OFN_HIDEREADONLY|OFN_ENABLESIZING|OFN_OVERWRITEPROMPT|OFN_SHAREAWARE,
        _T("Auto-export (*.txt, *.htm, *.html, *.dmp)|*.txt;*.htm;*.html;*.dmp|All files (*.*)|*.*||"),this);
    CString s,d;
    int q;

    if(dlg.DoModal()!=IDOK)
        return;
    outputFile=dlg.GetPathName();
    s=dlg.GetFileTitle();

    if((q=outputFile.ReverseFind(_T('\\')))>=0)
        d=outputFile.Left(q+1);
    else
        d=_T("");

    CString r4(outputFile.Right(4));
    CString r5(outputFile.Right(5));
    if(dlg.m_ofn.nFilterIndex!=2)
    {
        if(r4==_T(".htm") || r5==_T(".html"))
            OnChkExpHTML();
        else if(r4==_T(".txt"))
            OnChkExpText();
        else if(r4==_T(".dmp"))
            OnChkExpFlex();
        else if(IsDlgButtonChecked(IDC_RDO_EXPHTML))
            outputFile=d+s+_T(".htm");
        else if(IsDlgButtonChecked(IDC_RDO_EXPTEXT)
                ||IsDlgButtonChecked(IDC_RDO_EXPHEXASCII))
            outputFile=d+s+_T(".txt");
        else
            outputFile=d+s+_T(".dmp");
    }
    UpdateOpenBoxes();
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnChkExpText
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the "Export text" radio button is selected.
                It updates the extension in the "Output file" box.
PDL:
    Ensure that the "Export text" radio button is checked
    Separate the filename and directory
    IF the extension isn't ".txt" THEN
        Add a ".txt" extension
    ENDIF
    Enable the "Column headers" and "Include header" check boxes
    Update preview text
**/
void CExportDialog::OnChkExpText()
{
    CString name,dir;
    int n;

    CheckRadioButton(IDC_RDO_EXPTEXT,IDC_RDO_EXPHEXASCII,IDC_RDO_EXPTEXT);
    if((n=outputFile.ReverseFind(_T('\\')))>=0)
    {
        dir=outputFile.Left(n+1);
        name=outputFile.Right(outputFile.GetLength()-(n+1));
    }
    else
    {
        dir=_T("");
        name=outputFile;
    }

    if((n=name.ReverseFind(_T('.')))>=0)
        name=name.Left(n);

    if(outputFile.Right(4)!=_T(".txt"))
    {
        outputFile=dir+name+_T(".txt");
        UpdateOpenBoxes();
    }

    GetDlgItem(IDC_CHK_SIMPLE)->EnableWindow();
    GetDlgItem(IDC_CHK_EXPHEADER)->EnableWindow();
    SetPreviewText();
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnChkExpHTML
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the "Export HTML" button is selected.
PDL:
    Ensure that the "Export HTML" radio button is checked
    Separate the filename and directory
    IF the extension isn't ".htm" or ".html" THEN
        Add a ".htm" extension
    ENDIF
    Enable the "Column headers" and "Include header" check boxes
    Update preview text
**/
void CExportDialog::OnChkExpHTML()
{
    CString name,dir;
    int n;

    CheckRadioButton(IDC_RDO_EXPTEXT,IDC_RDO_EXPHEXASCII,IDC_RDO_EXPHTML);
    if((n=outputFile.ReverseFind(_T('\\')))>=0)
    {
        dir=outputFile.Left(n+1);
        name=outputFile.Right(outputFile.GetLength()-(n+1));
    }
    else
    {
        dir=_T("");
        name=outputFile;
    }

    if((n=name.ReverseFind(_T('.')))>=0)
        name=name.Left(n);

    if(outputFile.Right(4)!=_T(".htm") && name.Right(5)!=_T(".html"))
    {
        outputFile=dir+name+_T(".htm");
        UpdateOpenBoxes();
    }

    GetDlgItem(IDC_CHK_SIMPLE)->EnableWindow();
    GetDlgItem(IDC_CHK_EXPHEADER)->EnableWindow();
    SetPreviewText();
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnChkExpFlex
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the "Export Flex Buffer" button is selected.
PDL:
    Ensure that the "Export Flex Buffer" radio button is checked
    Separate the filename and directory
    IF the extension isn't ".dmp" or ".bin" THEN
        Add a ".dmp" extension
    ENDIF
    Disable the "Column headers" and "Include header" check boxes
    Update preview text
**/
void CExportDialog::OnChkExpFlex()
{
    CString name,dir;
    int n;
    CheckRadioButton(IDC_RDO_EXPTEXT,IDC_RDO_EXPHEXASCII,IDC_RDO_EXPFLEX);
    if((n=outputFile.ReverseFind(_T('\\')))>=0)
    {
        dir=outputFile.Left(n+1);
        name=outputFile.Right(outputFile.GetLength()-(n+1));
    }
    else
    {
        dir=_T("");
        name=outputFile;
    }

    if((n=name.ReverseFind(_T('.')))>=0)
        name=name.Left(n);

    if(outputFile.Right(4)!=_T(".dmp") && outputFile.Right(4)!=_T(".bin"))
    {
        outputFile=dir+name+_T(".dmp");
        UpdateOpenBoxes();
    }

    GetDlgItem(IDC_CHK_SIMPLE)->EnableWindow(FALSE);
    GetDlgItem(IDC_CHK_EXPHEADER)->EnableWindow(FALSE);
    SetPreviewText();
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnRdoExpHexASCII
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the "Export hex. dump" button is selected.
PDL:
    Ensure that the "Export hex. dump" radio button is checked
    Separate the filename and directory
    IF the extension isn't ".txt" THEN
        Add a ".txt" extension
    ENDIF
    Disable the "Include header" check box
    Enable the "Column headers" check box
    Update preview text
**/
void CExportDialog::OnRdoExpHexASCII()
{
    CString name,dir;
    int n;
    CheckRadioButton(IDC_RDO_EXPTEXT,IDC_RDO_EXPHEXASCII,IDC_RDO_EXPHEXASCII);
    if((n=outputFile.ReverseFind(_T('\\')))>=0)
    {
        dir=outputFile.Left(n+1);
        name=outputFile.Right(outputFile.GetLength()-(n+1));
    }
    else
    {
        dir=_T("");
        name=outputFile;
    }

    if((n=name.ReverseFind(_T('.')))>=0)
        name=name.Left(n);

    if(outputFile.Right(4)!=_T(".txt"))
    {
        outputFile=dir+name+_T(".txt");
        UpdateOpenBoxes();
    }

    GetDlgItem(IDC_CHK_SIMPLE)->EnableWindow();
    GetDlgItem(IDC_CHK_EXPHEADER)->EnableWindow();
    SetPreviewText();
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnChkAddxHex
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the "hex" checkbox is toggled.
PDL:
    Set a flag according to the checkbox's state.
    Convert the value in the text box to the correct form.
**/
void CExportDialog::OnChkAddxHex()
{
    m_bHex=!!IsDlgButtonChecked(IDC_CHK_ADDXHEX);

    CString s,t;
    GetDlgItemText(IDC_EDT_EXPRANGELO,s);
    GetDlgItemText(IDC_EDT_EXPRANGEHI,t);
    if(m_bHex)
    {
        s.Format(_T("%X"),Doubleword::ParseDecimal(s));
        t.Format(_T("%X"),Doubleword::ParseDecimal(t));
    }
    else
    {
        s.Format(_T("%u"),Doubleword::ParseHex(s));
        t.Format(_T("%u"),Doubleword::ParseHex(t));
    }
    register bool b=inM;inM=true;
    SetDlgItemText(IDC_EDT_EXPRANGELO,s);
    SetDlgItemText(IDC_EDT_EXPRANGEHI,t);
    inM=b;
}

#ifdef HAVE_TOOLTIPS
BOOL CExportDialog::OnToolTipNeedText(UINT id, NMHDR *pNMHDR, LRESULT *pResult)
{
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
    UINT nID =pNMHDR->idFrom;

    if((pTTT->uFlags&TTF_IDISHWND) && (nID=::GetDlgCtrlID((HWND)nID)))
    {
        switch(nID)
        {
        case IDC_EDT_EXPIMGFILE:    pTTT->lpszText=_T("Displays the image file to be exported.");break;
        case IDC_BTN_EXPORTFILE:    pTTT->lpszText=_T("Selects a destination file.");break;
        case IDC_RDO_EXPALL:        pTTT->lpszText=_T("Exports all addresses.");break;
        case IDC_RDO_EXPRANGE:      pTTT->lpszText=_T("Allows you to select a range for export.");break;
        case IDC_EDT_EXPRANGELO:    pTTT->lpszText=_T("Selects the lowest address for export (hexadecimal).");break;
        case IDC_EDT_EXPRANGEHI:    pTTT->lpszText=_T("Selects the highest address for export (hexadecimal).");break;
        case IDC_CHK_EXPHEADER:     pTTT->lpszText=_T("Turns on/off the inclusion of a file header.");break;
        case IDC_CHK_SIMPLE:        pTTT->lpszText=_T("Turns on/off the inclusion of row headers.");break;
        case IDC_RDO_EXPHTML:       pTTT->lpszText=_T("Exports as an HTML table.");break;
        case IDC_RDO_EXPTEXT:       pTTT->lpszText=_T("Exports as a plain text table.");break;
        case IDC_RDO_EXPFLEX:       pTTT->lpszText=_T("Exports as a Flex Buffer.");break;
        case IDOK:                  pTTT->lpszText=_T("Begins exporting the image file.");break;
        case IDCANCEL:              pTTT->lpszText=_T("Cancels the export.");break;
        case IDC_PRG_EXPORT:        pTTT->lpszText=_T("Shows the progress of the current export operation.");break;
        case IDC_STC_EXPPREVIEW:    pTTT->lpszText=_T("Shows a preview of the exported file.");break;
        default: pTTT->lpszText=NULL;
        }
        pTTT->hinst=NULL;
        return TRUE;
    }
    return FALSE;
}
#endif

///////////////////////////////////////
/**
Function:       CExportDialog::OnKeyUp
Parameters:     [CExportDialog *this]
                UINT nChar
                    Character code passed by Windows
                UINT nRepCnt
                    Repeat count passed by Windows
                UINT nFlags
                    Key flags passed by Windows
Returns:        void
Description:    Catches any uncaught accelerator keys.
PDL:
    IF the key is not being repeated AND Alt is down THEN
        Set focus to and/or select the appropriate item
    ENDIF
    Superclass the function call
**/
void CExportDialog::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags)
{
    register CWnd *p;

    if(!(nFlags&KF_REPEAT) && (nFlags&KF_ALTDOWN))
        switch(nChar)
        {
        case 'F':   //Export file
            OnBtnSelExport();
            return;

        case 'A':   //Export all
            GetDlgItem(IDC_RDO_EXPALL)->SetFocus();
            OnRdoExpAll();
            return;

        case 'R':   //Export range
            OnRdoExpRange();
            return;

        case 'H':   //Include header
            p=GetDlgItem(IDC_CHK_EXPHEADER);
            ((CButton *)p)->SetCheck(!((CButton *)p)->GetCheck());
            p->SetFocus();
            OnChkBox();
            return;

        case 'C':
            p=GetDlgItem(IDC_CHK_SIMPLE);
            ((CButton *)p)->SetCheck(TRUE);
            p->SetFocus();
            OnChkBox();
            return;

        case 'E':
            OnChkExpHTML();
            return;
        case 'T':
            OnChkExpText();
            return;
        case 'X':
            OnChkExpFlex();
            return;
        }

    super::OnKeyUp(nChar,nRepCnt,nFlags);
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnSetFocusEdtExprangelo
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the range-low text box gains focus.
PDL:
    Select everything in the text box
**/
void CExportDialog::OnSetFocusEdtExprangelo()
{
    CEdit *p=(CEdit *)GetDlgItem(IDC_EDT_EXPRANGELO);
    p->SetSel(0,p->GetWindowTextLength(),TRUE);
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnSetFocusEdtExprangehi
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the range-high text box gains focus.
PDL:
    Select everything in the text box
**/
void CExportDialog::OnSetFocusEdtExprangehi()
{
    CEdit *p=(CEdit *)GetDlgItem(IDC_EDT_EXPRANGEHI);
    p->SetSel(0,p->GetWindowTextLength(),TRUE);
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnContextHelp
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the Help button is clicked.
PDL:
    Display this dialog box's help topic.
**/
void CExportDialog::OnContextHelp()
{
    DisplayHelpTopic(GetParent(),DEF_ExportTopic);
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnClose
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the "Close" button is clicked
PDL:
    CALL CExportDialog::OnCancel([this])
**/
void CExportDialog::OnClose() {OnCancel();}

////////////////////////////////////////////////////////////////////////////////
// Virtual Overrides                  //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CExportDialog::OnCancel
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the "Cancel"/"Abort" button is clicked
PDL:
    IF currently exporting THEN
        Raise the cancellation flag
    ELSE
        CALL SaveValues
        End the dialog and return "cancel"
    ENDIF
**/
void CExportDialog::OnCancel()
{
    if(exporting)
        cancel=true;
    else
    {
        SaveValues();
        EndDialog(IDCANCEL);
    }
}

///////////////////////////////////////
/**
Function:       CExportDialog::OnOK
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called when the "Export"/"Done" button is clicked
PDL:
    IF just finished exporting THEN
        CALL SaveValues
        End the dialog and return "OK"
        RETURN
    ENDIF

    IF the output file exists THEN
        Warn the user and allow him/her to cancel
    ENDIF

    IF the output file couldn't be opened THEN
        Alert the user
        RETURN
    ENDIF

    IF the user would like to export a range THEN
        Read the values in the range
        Adjust the values to fit within the editor's minimum/maximum
    ELSE
        Use the editor's minimum/maximum for the range
    ENDIF
    IF the range is backwards THEN
        Reverse the range
    ENDIF

    Set up the window for the export
    IF exporting hex. dump or Flex Buffer THEN
        IF the address range is invalid THEN
            Warn the user and allow them to cancel
        ENDIF
        Write the appropriate file header and optional column headers

        DOFOR each address in the range
            Make sure the user isn't trying to abort
            Write the line or record for that address
        ENDDO
    ELSE
        Write the file header and/or column headers
        DOFOR each address in the range
            Make sure that the user isn't trying to abort
            Write the line for that address
        ENDDO
        IF the source image was empty THEN
            Write a line in the output stating this
        ENDIF
    ENDIF
    Clean up the window
    IF the user didn't abort THEN
        Set the "Export" button's text to "Done"
    ENDIF
**/
void CExportDialog::OnOK()
{
    bool expHTML;
    DWORD rangeLow,rangeHigh;
    int i = 0;
    CStdioFile output;
  //  struct tm *tmp;
    LPCTSTR p = _T("");
	const CImageEditorDlg::Record *q;


    CString buf;
    GetDlgItemText(IDOK,buf);
    if(buf==_T("Done")) //(The button text gets set to this after an export)
    {
        SaveValues();
        EndDialog(IDOK);
        return;
    }

    DWORD currentAddress;
    currentAddress=::GetFileAttributes(outputFile);
    if(~currentAddress && !(currentAddress&FILE_ATTRIBUTE_DIRECTORY))
    {
        if(MessageBox(_T("Output file exists.  Overwrite?"),_T("Warning"),
            MB_YESNO|MB_ICONQUESTION)!=IDYES)
            return;
    }

    //Read in the range and make any necessary corrections
    if(IsDlgButtonChecked(IDC_RDO_EXPRANGE)) {
        GetDlgItemText(IDC_EDT_EXPRANGELO,buf);
        rangeLow=m_bHex?Doubleword::ParseHex(buf):Doubleword::ParseDecimal(buf);

        GetDlgItemText(IDC_EDT_EXPRANGEHI,buf);
        rangeHigh=m_bHex?Doubleword::ParseHex(buf):Doubleword::ParseDecimal(buf);
    }
    else
    {
        rangeLow=editor->lowestAddress;
        rangeHigh=editor->highestAddress;
    }

    if(rangeLow>rangeHigh)
    {
//      register DWORD temp = rangeLow;
        rangeLow=rangeHigh;
        rangeHigh=rangeLow;
    }

//  BeginWaitCursor();
    SetWindowText(_T("Exporting, click \"Abort\" to stop..."));
    SetDlgItemText(IDCANCEL,_T("Abort"));
    DisableEverything();
//  m_CrlProgress=(CProgressCtrl *)GetDlgItem(IDC_PRG_EXPORT);
//  m_CrlProgress->SetRange(0,256);
//  m_CrlProgress->SetPos(0);

    int nFormat=0;
	int nFileOpenStatus = 0;
	CFileException ex;

    if(IsDlgButtonChecked(IDC_RDO_EXPFLEX)){
		nFormat = 1;
		nFileOpenStatus = output.Open(outputFile, CFile::modeCreate|CFile::modeReadWrite|CFile::shareExclusive|CFile::typeBinary, &ex);
	} else if(IsDlgButtonChecked(IDC_RDO_EXPHEXASCII)){
		nFormat=2;
		nFileOpenStatus = output.Open(outputFile, CFile::modeCreate|CFile::modeReadWrite|CFile::shareExclusive, &ex);
	} else {
		nFileOpenStatus = output.Open(outputFile, CFile::modeCreate|CFile::modeReadWrite|CFile::shareExclusive, &ex);
	}


//  if(!output.Open(outputFile,
//  CFile::modeCreate|CFile::modeWrite|CFile::shareExclusive|CFile::typeBinary))

//	if(!output.Open(outputFile, CFile::modeCreate|CFile::modeWrite|CFile::shareExclusive|CFile::typeBinary))


	if (nFileOpenStatus == 0){
//	    MessageBox(_T("Couldn't open output file for writing."),_T("Error"), MB_OK|MB_ICONERROR);

		TCHAR szError[1024];
        ex.GetErrorMessage(szError, 1024);
  //      cout << "Couldn't open source file: ";
  //      cout << szError;

		MessageBox(szError,_T("Error"), MB_OK|MB_ICONERROR);

		return;
    }

    if(nFormat) {
    // Flex Buffer/Hex ASCII
        --nFormat;
        DWORD x,cliplow,cliphigh;
//        bool tog=true;
   //     TCHAR bufx[9];

        // The Flex Buffer/hex ASCII don't allow anything outside
        // of the 1-MB range, so clip the addresses off appropriately.
        cliplow=rangeLow&((1<<20)-1);
        cliphigh=rangeHigh&((1<<20)-1);


        if(cliphigh < cliplow || rangeHigh >= (rangeLow + (1 << 20)))
        {
            if(MessageBox(_T("Address range crosses a 1-MB boundary; output file may be unusable.\r\n")
                          _T("Continue?"),_T("Warning"),MB_YESNO|MB_ICONQUESTION)==IDNO)
            {
                output.Close();
                exporting=cancel=false;
                SetWindowText(_T("Export"));
                SetDlgItemText(IDOK,_T("Export"));
                SetDlgItemText(IDCANCEL,_T("Cancel"));
                EnableEverything();
                return;
            }
        }

        //Write the headers
        if(!nFormat){
            x = ::BE2LE_D(cliplow);
			output.Write(&x,sizeof(x));
            x = ::BE2LE_D(cliphigh);
			output.Write(&x,sizeof(x));
        } else {

			if(IsDlgButtonChecked(IDC_CHK_EXPHEADER)) {

				CTime now = CTime::GetCurrentTime();
				if ((CImageEditorDlg::ACE1_IMAGE == editor->m_strctFileHdr.strtHeader.eType)
				|| (CImageEditorDlg::ACE2_IMAGE == editor->m_strctFileHdr.strtHeader.eType)){
					if (0 == editor->m_nAddrSpace) { // Operand space
						buf.Format(_T("# Output file:         %s\r\n")
							_T("# Source file:         %s\r\n")
							_T("# Address Space:       Operand\r\n")
							_T("# Source file:         %s\r\n")
							_T("# Start address:       0x%08.8X\r\n")
							_T("# End address:         0x%08.8X\r\n")
							_T("# Creation date:       %02.2d-%02.2d-%04.4d\r\n")
							_T("# Creation time:       %02.2d:%02.2d:%02.2d\r\n\r\n"),
							(LPCTSTR)editor->currentFile,(LPCTSTR)outputFile,
							cliplow,cliphigh,now.GetDay(),now.GetMonth(),
							now.GetYear(),now.GetHour(),now.GetMinute(),
							now.GetSecond());
					} else if (1 == editor->m_nAddrSpace){ // Instrument Space
						buf.Format(_T("# Output file:         %s\r\n")
							_T("# Source file:         %s\r\n")
							_T("# Address Space:       Instrument\r\n")
							_T("# Start address:       0x%08.8X\r\n")
				            _T("# End address:         0x%08.8X\r\n")
				            _T("# Creation date:       %02.2d-%02.2d-%04.4d\r\n")
				            _T("# Creation time:       %02.2d:%02.2d:%02.2d\r\n\r\n"),
							(LPCTSTR)editor->currentFile,(LPCTSTR)outputFile,
							cliplow,cliphigh,now.GetDay(),now.GetMonth(),
							now.GetYear(),now.GetHour(),now.GetMinute(),
							now.GetSecond());
					}

				}  else if (CImageEditorDlg::SXI_IMAGE == editor->m_strctFileHdr.strtHeader.eType) {
				// Not ACE memory report
				buf.Format(_T("# Output file:         %s\r\n")
               _T("# Source file:         %s\r\n")
                       _T("# Start address:       0x%08.8X\r\n")
                       _T("# End address:         0x%08.8X\r\n")
                       _T("# Creation date:       %02.2d-%02.2d-%04.4d\r\n")
                       _T("# Creation time:       %02.2d:%02.2d:%02.2d\r\n\r\n"),
						(LPCTSTR)editor->currentFile,(LPCTSTR)outputFile,
						cliplow,cliphigh,now.GetDay(),now.GetMonth(),
						now.GetYear(),now.GetHour(),now.GetMinute(),
						now.GetSecond());
			}
	        output.WriteString((LPCTSTR)buf);
		}

		if(IsDlgButtonChecked(IDC_CHK_SIMPLE))
		     output.WriteString(_T("adstaddress    data\r\n"));
		}

		MSG msg;
		CWinThread *curThread;

		curThread=::AfxGetThread();
		exporting=true;
		cancel=false;
        currentAddress=rangeLow;
		// rangeHigh += 2;
		const CImageEditorDlg::Record *strtHighAddr;

        //Main write loop
    continueFlex:
        for(i=0;!cancel && currentAddress<rangeHigh; currentAddress +=2, i++)
        {
            if(!(i&31))
                while(::PeekMessage(&msg,NULL,0,0,PM_NOREMOVE))
                {
                    if(!curThread->PumpMessage())
                        break;
                    // BeginWaitCursor();
                }

            m_CrlProgress->SetPos(((currentAddress-rangeLow))/(rangeHigh-rangeLow));
            // q = editor->GetAddx(currentAddress);      //see <1>

			bool bValidAddr = true;
			bool bRecMod = false;
			q = editor->GetAddx(currentAddress, bValidAddr, bRecMod);

			if (!bValidAddr){
				// If invalid address, skip it.
				// Want to increment address but stay at same display line
				i--;
				continue;
			}

//			strtLowAddr  = q;
		    strtHighAddr = editor->GetAddx(currentAddress + 1, bValidAddr, bRecMod);

			DWORD dwLowerWord = q->data;
			DWORD dwHigherWord = strtHighAddr->data;
			if(q) {
                //Hex ASCII data is printed in big-endian format.
               // dwHigherWord   = strtHighAddr->data;
				dwLowerWord <<= 16;
				dwLowerWord   |= dwHigherWord;
//				TRACE(_T("currentAddress = %x\n"), currentAddress);
//				TRACE(_T("Before conversion dwLowerWord = %x\n"), dwLowerWord);
				x = nFormat ? dwLowerWord : (::BE2LE_D(dwLowerWord));
//				TRACE(_T("After conversion dwLowerWord = %x\n"), x);
//				TRACE(_T("rangeHigh = %x\n"), rangeHigh);
			} else {
                x = 0;
			}

            if(nFormat) {
				CString strBufx;
  				strBufx.Format(_T("%08.8X    "), currentAddress);
                output.WriteString((LPCTSTR)strBufx);
				strBufx.Empty();
				strBufx.Format(_T("%08.8X\r\n"), x);
                output.WriteString((LPCTSTR)strBufx);
            } else {
                output.Write(&x,sizeof(DWORD));
				// This is for debugging purposes only
				CString strBufx;
				strBufx.Empty();
				strBufx.Format(_T("%08.8X\r\n"), x);

			}
        }
       // EndWaitCursor();
        //cancel is true if the user aborted the operation.
        if(cancel)
        {
            int j=MessageBox(_T("Keep partially exported file?\r\n(Press Cancel to resume)"),
                _T("Aborted"),MB_YESNOCANCEL|MB_ICONQUESTION);
            if(j!=IDCANCEL) output.Close();
            if(j==IDNO)
            {
                cancel=false;
                ::DeleteFile(outputFile);
            }
            else if(j==IDCANCEL)
            {
                cancel=false;
               // BeginWaitCursor();
                goto continueFlex;
            }

            exporting=cancel=false;
            SetWindowText(_T("Export"));
            SetDlgItemText(IDOK,_T("Export"));
            SetDlgItemText(IDCANCEL,_T("Cancel"));
            ((CProgressCtrl *)GetDlgItem(IDC_PRG_EXPORT))->SetPos(0);
            EnableEverything();
            return;
        }
    }
    else
    {
        //Write the HTML header
		expHTML = !!IsDlgButtonChecked(IDC_RDO_EXPHTML);
        if(expHTML)
        {
            output.WriteString(_T("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\">\n")
                _T("<html>\n")
                _T("    <head>\n")
                _T("        <meta http-equiv=\"Content-Type\" content=\"text/html\" />\n")
                _T("        <meta name=\"Generator\" content=\"GOES Image Editor\" />\n")
                _T("        <title>Image export from \""));
            output.WriteString((LPCTSTR)editor->currentFile);
            output.WriteString(_T("\"</title>\n")
                _T("    </head>\n")
                _T("    <body>\n")
                _T("        <center><h2>Image export from <b>"));
            output.WriteString((LPCTSTR)editor->currentFile);
            output.WriteString(_T("</b></h2></center>\n"));
        }

        //Write the main portion of the header
        if(IsDlgButtonChecked(IDC_CHK_EXPHEADER))
        {
            time_t now;
            // struct tm *t;
			struct tm t;
            TCHAR wksName[64];
            DWORD l=63;
            time(&now);
            // t=gmtime(&now);
			errno_t err = gmtime_s(&t, &now);

            ::GetComputerName(wksName,&l);

            // rangeLow &=-2;
            // rangeHigh &=-2;
            if(rangeHigh<rangeLow)
            {
                register DWORD t=rangeHigh;
                rangeHigh=rangeLow;
                rangeLow=t;
            }
            else if(rangeHigh==rangeLow)
                rangeHigh=rangeLow+2;

            if(expHTML){

				if ((CImageEditorDlg::ACE1_IMAGE == editor->m_strctFileHdr.strtHeader.eType)
					|| (CImageEditorDlg::ACE2_IMAGE == editor->m_strctFileHdr.strtHeader.eType)){

					if (0 == editor->m_nAddrSpace) {  // Operand space

						p=_T("        <table border=\"0\" cellpadding=\"5\">\n")
                          _T("            <tr><td>Original File:</td><td>%s</td></tr>\n")
                          _T("            <tr><td>Report Date:</td>  <td>%04.4u-%03.3u-%02.2u:%02.2u:%02.2u</td></tr>\n")
                          _T("            <tr><td>Address Space:</td><td>Operand</td></tr>\n")
						  _T("            <tr><td>Address Range:</td><td>0x%08.8X-0x%08.8X</td></tr>\n")
                          _T("            <tr><td>Workstation:</td>  <td>%s</td></tr>\n")
                          _T("        </table>\n")
                          _T("        <hr />\n\n");
					} else if (1 == editor->m_nAddrSpace){ // Instrument space

						p=_T("        <table border=\"0\" cellpadding=\"5\">\n")
		                  _T("            <tr><td>Original File:</td><td>%s</td></tr>\n")
						  _T("            <tr><td>Report Date:</td>  <td>%04.4u-%03.3u-%02.2u:%02.2u:%02.2u</td></tr>\n")
						  _T("            <tr><td>Address Range:</td><td>0x%08.8X-0x%08.8X</td></tr>\n")
						  _T("            <tr><td>Address Space:</td><td>Instrument</td></tr>\n")
						  _T("            <tr><td>Workstation:</td>  <td>%s</td></tr>\n")
                          _T("            </table>\n")
                          _T("            <hr />\n\n");
					}
				} else{  // Not ACE memory data

					p = _T("        <table border=\"0\" cellpadding=\"5\">\n")
					    _T("            <tr><td>Original File:</td><td>%s</td></tr>\n")
                        _T("            <tr><td>Report Date:</td>  <td>%04.4u-%03.3u-%02.2u:%02.2u:%02.2u</td></tr>\n")
                        _T("            <tr><td>Address Range:</td><td>0x%08.8X-0x%08.8X</td></tr>\n")
                        _T("            <tr><td>Workstation:</td>  <td>%s</td></tr>\n")
                        _T("        </table>\n")
                        _T("        <hr />\n\n");
				}
			} else {

				if (0 == editor->m_nAddrSpace) { // Operand space

					 p=_T("Original File:  %s\r\n")
					   _T("Report Date:    %04.4u-%03.3u-%02.2u:%02.2u:%02.2u\r\n")
					   _T("Address Space:  Operand\r\n")
					   _T("Address Range:  0x%08.8X-0x%08.8X\r\n")
                       _T("Workstation:    %s\r\n\r\n");
				} else if (1 == editor->m_nAddrSpace){ // Instrument Space

					 p=_T("Original File:  %s\r\n")
                       _T("Report Date:    %04.4u-%03.3u-%02.2u:%02.2u:%02.2u\r\n")
					   _T("Address Space:  Instrument\r\n")
                       _T("Address Range:  0x%08.8X-0x%08.8X\r\n")
                       _T("Workstation:    %s\r\n\r\n");

				} else {   // Not ACE memory report

					 p=_T("Original File:  %s\r\n")
					   _T("Report Date:    %04.4u-%03.3u-%02.2u:%02.2u:%02.2u\r\n")
                       _T("Address Range:  0x%08.8X-0x%08.8X\r\n")
                       _T("Workstation:    %s\r\n\r\n");
				}
			}

			buf.Format((LPCTSTR)p,(LPCTSTR)editor->currentFile,t.tm_year+1900,t.tm_yday+1,
               t.tm_hour,t.tm_min,t.tm_sec,
               rangeLow,rangeHigh,wksName);

            output.WriteString((LPCTSTR)buf);
            buf.Empty();
        }

        if(editor->m_Loaded)
        {
            if(expHTML)
                output.WriteString(_T("      <center>\n")
                                   _T("        <table border=\"1\" cellpadding=\"3\" cellspacing=\"2\">\n"));

            //Write column headers
            if(IsDlgButtonChecked(IDC_CHK_SIMPLE))
            {
                if(expHTML)
                    output.WriteString(
                        _T("            <tr><th>Hex Addr</th><th>Dec Addr</th><th>Hex</th>")
                        _T("<th>Dec</th><th>Time</th></tr>\n"));
                else
                    output.WriteString(
                        _T("Hex Addr Dec Addr Hex   Dec    Time\r\n")
                        _T("-------- -------- ----  -----  ---------------------\r\n"));
            }

            // rangeHigh += 2;
            MSG msg;
            CWinThread *curThread;
            curThread =::AfxGetThread();
            exporting = true;
			cancel = false;
            currentAddress = rangeLow;

		//	i=0;

            //Main write loop
    continueForLoop:
            for(i = 0;!cancel && currentAddress<rangeHigh; currentAddress += 1,i++)
            {
                if(!(i&31))
                    while(::PeekMessage(&msg,NULL,0,0,PM_NOREMOVE))
                    {
                        if(!curThread->PumpMessage())
                            break;
                        // BeginWaitCursor();
                    }

//				q=editor->GetAddx(currentAddress);      //see note <1> at end of file

				bool bValidAddr = true;
				bool bRecMod = false;
				q=editor->GetAddx(currentAddress, bValidAddr, bRecMod);

				if (!bValidAddr){
					// If invalid address, skip it.
					// Want to increment address but stay at same display line
					i--;
					continue;
				}

				if(!q){
					continue;
				}

				/*
					struct tm then;
					__int64 ltime = cur->seconds; 
					errno_t err = _gmtime_s(&then, &ltime);
				*/
				// tmp=gmtime((time_t *)&(q->seconds));

				__int64 ltime = q->seconds;
				struct tm tmp;
				errno_t err = gmtime_s(&tmp, &ltime);
                if(err != 0)
                {
                    if(expHTML)
                        _T("            <tr><td><tt>%08.8X</tt></td><td><tt>%08.8X</tt></td><td><tt>%04.4X</tt></td><td><tt>%-5.5u</tt></td>")
                        _T("<td>Invalid timestamp <tt>0x%08.8X</tt></td></tr>\n");
                    else
                        p = _T("%08.8X %08.8u %04.4X  %-5.5u  (Invalid timestamp 0x%08.8X)\r\n");

				//	unsigned int nDecimalAddr = (unsigned int)currentAddress>>1;
                    buf.Format(p, currentAddress, currentAddress, q->data, q->data, q->seconds);
                }
                else
                {
                    if(expHTML)
                        p=_T("            <tr><td><tt>%08.8X</tt></td><td><tt>%08.8d</tt></td><td><tt>%04.4X</tt></td><td><tt>%-5.5u</tt></td>")
                          _T("<td><tt>%04.4u-%03.3u-%02.2u:%02.2u:%02.2u.%03.3u</tt></td></tr>\n");
                    else
                        p=_T("%08.8X %08.8u %04.4X  %-5.5u  %04.4u-%03.3u-%02.2u:%02.2u:%02.2u.%03.3u\r\n");

                   // unsigned int nDecimalAddr = (unsigned int)currentAddress>>1;
					buf.Format(p, currentAddress, currentAddress, q->data, q->data,
                        tmp.tm_year+1900, tmp.tm_yday+1, tmp.tm_hour, tmp.tm_min, tmp.tm_sec, q->milliseconds);
                }
                output.WriteString((LPCTSTR)buf);
            }
            if(expHTML)
                output.WriteString(_T("        </table>\n")
                                   _T("      </center>\n"));
        }
        else
            if(expHTML)
                output.WriteString(_T("<center><i>No data in source image</i></center>\n"));
            else
                output.WriteString(_T("NO DATA IN SOURCE IMAGE"));

        if(expHTML)
            output.WriteString(_T("    </body>\n")
                               _T("</html>\n"));

       // EndWaitCursor();
        if(cancel)
        {
            int j=MessageBox(_T("Keep partially exported file?\r\n(Press Cancel to resume)"),
                _T("Aborted"),MB_YESNOCANCEL|MB_ICONQUESTION);
            if(j!=IDCANCEL) output.Close();
            if(j==IDNO)
            {
                cancel=false;
                ::DeleteFile(outputFile);
            }
            else if(j==IDCANCEL)
            {
                cancel=false;
               // BeginWaitCursor();
                goto continueForLoop;
            }

            exporting=cancel=false;
            SetWindowText(_T("Export"));
            SetDlgItemText(IDOK,_T("Export"));
            SetDlgItemText(IDCANCEL,_T("Cancel"));
            // ((CProgressCtrl *)GetDlgItem(IDC_PRG_EXPORT))->SetPos(0);
            EnableEverything();
            return;
        }
    }

    output.Close();
    exporting=cancel=false;
    SetWindowText(_T("Export"));
    SetDlgItemText(IDOK,_T("Done"));
    EnableEverything();
    SetDlgItemText(IDCANCEL,_T("Cancel"));
    GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
}

////////////////////////////////////////////////////////////////////////////////
// Internal Functions                 //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
//Updates the text in the Preview box.
/**
Function:       CExportDialog::SetPreviewText
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Catches any check-box clicking to refresh the preview.
PDL:
    Set the "Export" button's text back to "Export"
    Enable the "Cancel" button
    Reset the progress bar control
    IF selected output format is "Binary" THEN
        Clear and disable the preview box
    ELSE
        Enable the preview box
    ENDIF

    Retrieve the range from the edit boxes
    Get the system time
    Write the appropriately formatted data to the preview box
**/
void CExportDialog::SetPreviewText()
{
    CEdit *prev=(CEdit *)GetDlgItem(IDC_STC_EXPPREVIEW);
    CString newText(_T(""));
    DWORD rangeLo,rangeHi;
    time_t now;
    tm nowPsd;
    TCHAR wksName[64];
    DWORD len=63;

    //Clean up after a previous export...
    SetDlgItemText(IDOK,_T("Export"));
    GetDlgItem(IDCANCEL)->EnableWindow();
    ((CProgressCtrl *)GetDlgItem(IDC_PRG_EXPORT))->SetPos(0);

    //If they selected a binary output, don't try to preview it
    if(IsDlgButtonChecked(IDC_RDO_EXPFLEX))
    {
        prev->SetWindowText(_T(""));
        prev->EnableWindow(FALSE);
        GetDlgItem(IDC_STC_PREVUBOX)->EnableWindow(FALSE);
        return;
    }
    else
    {
        prev->EnableWindow();
        GetDlgItem(IDC_STC_PREVUBOX)->EnableWindow();
    }

    //Figure out the range
    if(IsDlgButtonChecked(IDC_RDO_EXPALL))
    {
        rangeLo=editor->lowestAddress;
        rangeHi=editor->highestAddress;
    }
    else
    {
        CString txt;

        GetDlgItemText(IDC_EDT_EXPRANGELO,txt);
        rangeLo=(m_bHex?Doubleword::ParseHex(txt):Doubleword::ParseDecimal(txt));
        GetDlgItemText(IDC_EDT_EXPRANGEHI,txt);
        rangeHi=(m_bHex?Doubleword::ParseHex(txt):Doubleword::ParseDecimal(txt));
    }

    time(&now);
    if(IsDlgButtonChecked(IDC_RDO_EXPHEXASCII)){
        struct tm temp;
        // nowPsd=localtime(&now);
		errno_t err = localtime_s(&nowPsd, &now);
        if(!err) {
			::ZeroMemory(&temp,sizeof(temp));
			nowPsd = temp;
		}

		if(IsDlgButtonChecked(IDC_CHK_EXPHEADER)){
			if ((CImageEditorDlg::ACE1_IMAGE == editor->m_strctFileHdr.strtHeader.eType)
				|| (CImageEditorDlg::ACE2_IMAGE == editor->m_strctFileHdr.strtHeader.eType)){

					if (0 == editor->m_nAddrSpace) { // Operand space
				       newText.Format(_T("# Output file:         %s\r\n")
				   	   _T("# Source file:         %s\r\n")
					   _T("# Address Space:       Operand\r\n")
					   _T("# Start address:       0x00000000\r\n")
					   _T("# End address:         0x%08.8X\r\n")
					   _T("# Creation date:       %02.2d-%02.2d-%04.4d\r\n")
					   _T("# Creation time:       %02.2d:%02.2d:%02.2d\r\n\r\n"),
					     (LPCTSTR)outputFile,(LPCTSTR)editor->currentFile,rangeHi,
					      nowPsd.tm_mday,nowPsd.tm_mon, nowPsd.tm_year,
					      nowPsd.tm_hour,nowPsd.tm_min, nowPsd.tm_sec);

					} else if (1 == editor->m_nAddrSpace){ // Instrument Space
				       newText.Format(_T("# Output file:         %s\r\n")
		               _T("# Source file:         %s\r\n")
				       _T("# Address Space:       Instrument\r\n")
	                   _T("# Start address:       0x00000000\r\n")
	                   _T("# End address:         0x%08.8X\r\n")
	                   _T("# Creation date:       %02.2d-%02.2d-%04.4d\r\n")
	                   _T("# Creation time:       %02.2d:%02.2d:%02.2d\r\n\r\n"),
	                     (LPCTSTR)outputFile,(LPCTSTR)editor->currentFile,rangeHi,
	                     nowPsd.tm_mday,nowPsd.tm_mon,nowPsd.tm_year,
	                     nowPsd.tm_hour,nowPsd.tm_min,nowPsd.tm_sec);
					}

			} else if (CImageEditorDlg::SXI_IMAGE == editor->m_strctFileHdr.strtHeader.eType){
			   // Not ACE memory report
		       newText.Format(_T("# Output file:         %s\r\n")
	           _T("# Source file:         %s\r\n")
			   _T("# Start address:       0x00000000\r\n")
		       _T("# End address:         0x%08.8X\r\n")
	           _T("# Creation date:       %02.2d-%02.2d-%04.4d\r\n")
	           _T("# Creation time:       %02.2d:%02.2d:%02.2d\r\n\r\n"),
	             (LPCTSTR)outputFile,(LPCTSTR)editor->currentFile,rangeHi,
	             nowPsd.tm_mday,nowPsd.tm_mon,nowPsd.tm_year,
	             nowPsd.tm_hour,nowPsd.tm_min,nowPsd.tm_sec);
			}
		}

		if(IsDlgButtonChecked(IDC_CHK_SIMPLE)){

          newText += CString(_T("adstaddress  data\r\n"));
          // newText += CString(_T("#-------    --------\r\n"));

		}
		newText += CString(_T("01234567     89ABCDEF\r\n"));
	    newText += CString(_T("89ABCDEF     01234567\r\n"));

    } else {
        // nowPsd=gmtime(&now);
		errno_t err = gmtime_s(&nowPsd, &now);
        ::GetComputerName(wksName,&len);

        if(IsDlgButtonChecked(IDC_CHK_EXPHEADER)){

			if ((CImageEditorDlg::ACE1_IMAGE == editor->m_strctFileHdr.strtHeader.eType)
				|| (CImageEditorDlg::ACE2_IMAGE == editor->m_strctFileHdr.strtHeader.eType)){

   				if (0 == editor->m_nAddrSpace) { // Operand space
					newText.Format(_T("Original File:  %s\r\n")
					_T("Date:           %04.4u-%03.3u-%02.2u:%02.2u:%02.2u\r\n")
					_T("Address Space:  Operand\r\n")
					_T("Range:          %08.8X-%08.8X\r\n")
					_T("Workstation:    %s\r\n\r\n"),
					(LPCTSTR)editor->currentFile,nowPsd.tm_year+1900,nowPsd.tm_yday+1,
					nowPsd.tm_hour,nowPsd.tm_min,nowPsd.tm_sec,
					rangeLo, rangeHi, wksName);
				} else if (1 == editor->m_nAddrSpace){ // Instrument Space
				    newText.Format(_T("Original File:  %s\r\n")
		            _T("Date:           %04.4u-%03.3u-%02.2u:%02.2u:%02.2u\r\n")
                    _T("Address Space:  Instrument\r\n")
				    _T("Range:          %08.8X-%08.8X\r\n")
                    _T("Workstation:    %s\r\n\r\n"),
                      (LPCTSTR)editor->currentFile,nowPsd.tm_year+1900,nowPsd.tm_yday+1,
                    nowPsd.tm_hour,nowPsd.tm_min,nowPsd.tm_sec, rangeLo, rangeHi, wksName);
				}

			} else if (CImageEditorDlg::SXI_IMAGE == editor->m_strctFileHdr.strtHeader.eType){

			    newText.Format(_T("Original File:  %s\r\n")
                  _T("Date:           %04.4u-%03.3u-%02.2u:%02.2u:%02.2u\r\n")
                  _T("Range:          %08.8X-%08.8X\r\n")
                  _T("Workstation:    %s\r\n\r\n"),
                  (LPCTSTR)editor->currentFile,nowPsd.tm_year+1900,nowPsd.tm_yday+1,
                  nowPsd.tm_hour,nowPsd.tm_min,nowPsd.tm_sec,
                  rangeLo, rangeHi, wksName);
			}
		}

        if(IsDlgButtonChecked(IDC_CHK_SIMPLE)){
            newText+=CString(_T("Hex Addr Dec Addr Hex   Dec    Time\r\n")
                             _T("-------- -------- ----  -----  -----------------\r\n"));
		}
        newText+=CString(_T("01234567 01234567 89AB  12345  2002-202-02:02:02\r\n")
                         _T("89ABCDEF 01234567 0123  54321  2002-020-20:20:20"));
    }
    prev->SetWindowText(newText);
}

///////////////////////////////////////
/**
Function:       CExportDialog::UpdateOpenBoxes
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called to update the text in the input/output
                file text boxes.
PDL:
    Set the input file box's text to the editor's current file
    Set the output file box's text to the current output file
**/
void CExportDialog::UpdateOpenBoxes()
{
    SetDlgItemText(IDC_EDT_EXPIMGFILE,CImageEditorDlg::Ellipsis(editor->currentFile));
    SetDlgItemText(IDC_BTN_EXPORTFILE,CImageEditorDlg::Ellipsis(outputFile));
}

///////////////////////////////////////
/**
Function:       CExportDialog::SaveValues
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Called before the dialog is destroyed to save settings.
PDL:
    Save settings to the editor.
**/
void CExportDialog::SaveValues()
{
        CImageEditorDlg::__ExportData *e=&(editor->ExportData);
        e->flags=0;
        if(IsDlgButtonChecked(IDC_RDO_EXPALL))
            e->flags|=CImageEditorDlg::__ExportData::AllSel;
        if(IsDlgButtonChecked(IDC_CHK_EXPHEADER))
            e->flags|=CImageEditorDlg::__ExportData::Header;
        if(IsDlgButtonChecked(IDC_CHK_SIMPLE))
            e->flags|=CImageEditorDlg::__ExportData::ColHeaders;

        e->format=0;
        if(IsDlgButtonChecked(IDC_RDO_EXPHTML))
            e->format=1;
        else if(IsDlgButtonChecked(IDC_RDO_EXPFLEX))
            e->format=2;
        else if(IsDlgButtonChecked(IDC_RDO_EXPHEXASCII))
            e->format=3;

        int n=outputFile.ReverseFind('\\');
        if(n>=0)
            e->outputDir=outputFile.Left(n+1);
        else
            e->outputDir=_T("");
}

///////////////////////////////////////
//Disable-able id list...
static UINT xableIDs[]={
	IDC_EDT_EXPIMGFILE,
	IDC_BTN_EXPORTFILE,
    IDC_RDO_EXPALL,
	IDC_RDO_EXPRANGE,
	IDC_EDT_EXPRANGEHI,
	IDC_EDT_EXPRANGELO,
    IDC_STC_EXPTO,
	IDC_CHK_EXPHEADER,
	IDC_CHK_SIMPLE,
	IDC_RDO_EXPTEXT,
	IDC_RDO_EXPHTML,
    IDC_RDO_EXPFLEX,
	IDOK,
	IDC_STC_EXPPREVIEW,
	IDC_RDO_EXPHEXASCII,
	IDC_CHK_ADDXHEX,
	IDC_STC_PREVUBOX,
	IDC_STC_EXPPREVIEW
};

///////////////////////////////////////
/**
Function:       CExportDialog::DisableEverything
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Disables most items in the dialog in preparation for an export.
PDL:
    DOFOR each item that should be disabled
        Disable the item
    ENDDO
**/
void CExportDialog::DisableEverything()
{
    int i;
    for(i=0;i<sizeof(xableIDs)/sizeof(*xableIDs);i++)
        GetDlgItem(xableIDs[i])->EnableWindow(FALSE);
}

///////////////////////////////////////
/**
Function:       CExportDialog::EnableEverything
Parameters:     [CExportDialog *this]
Returns:        void
Description:    Re-enables items after an export
PDL:
    DOFOR each item that should be enabled
        Enable the item
    ENDDO
    IF the "Export All" radio button is checked THEN
        Disable the export range text boxes and label
    ENDIF
    IF the "Export Flex Buffer" radio button is checked THEN
        Disable the text formatting check boxes
    ENDIF
    IF the "Export hex. dump" radio button is checked THEN
        Disable the "Include header" check box
    ENDIF
**/
void CExportDialog::EnableEverything()
{
 //   int i;
    for(int i=0;i<sizeof(xableIDs)/sizeof(*xableIDs);i++){
        GetDlgItem(xableIDs[i])->EnableWindow();
	}

    if(IsDlgButtonChecked(IDC_RDO_EXPALL)){

        GetDlgItem(IDC_EDT_EXPRANGEHI)->EnableWindow(FALSE);
        GetDlgItem(IDC_EDT_EXPRANGELO)->EnableWindow(FALSE);
        GetDlgItem(IDC_STC_EXPTO)->EnableWindow(FALSE);
    }
	if(IsDlgButtonChecked(IDC_RDO_EXPFLEX)){

        GetDlgItem(IDC_CHK_EXPHEADER)->EnableWindow(FALSE);
        GetDlgItem(IDC_CHK_SIMPLE)->EnableWindow(FALSE);

    }
	/*
	else if(IsDlgButtonChecked(IDC_RDO_EXPHEXASCII))
        GetDlgItem(IDC_CHK_EXPHEADER)->EnableWindow(FALSE);
	*/
}

/* Notes:
 * #<1> lines 1049,1204
 *    These lines are where the function slows down considerably.  Might be
 *    nice to change these to direct cache/change access, all though it
 *    would add copious quantities of code.
 */
