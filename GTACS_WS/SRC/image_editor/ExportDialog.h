/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       ExportDialog.h
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
 		 V2.0        10/04       F. Shaw         Modifications for Virtual memory
		                                         mapping.  GTACS Build 9
*******************************************************************************/
// Header for the [Export] dialog

#if !defined(AFX_EXPORTDIALOG_H__5F6AC086_DBA2_475A_9C20_4689382C38E0__INCLUDED_)
#define AFX_EXPORTDIALOG_H__5F6AC086_DBA2_475A_9C20_4689382C38E0__INCLUDED_
#if _MSC_VER > 1000
#pragma once
#endif

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "ImageEditorDlg.h"

////////////////////////////////////////////////////////////////////////////////
// Class Definitions                  //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class CExportDialog:public CDialog
{
public:
    ///////////////////////////////////
    // Public interface functions    //
    ///////////////////////////////////
	CExportDialog(CWnd *pParent=0);

protected:
    ///////////////////////////////////
    // Protected member variables    //
    ///////////////////////////////////
    CImageEditorDlg *editor;
    CProgressCtrl *m_CrlProgress;
    CString outputFile;
    bool inContextHelp,chlbDown;
    bool exporting,cancel;
    bool inM;
    bool m_bHex;

protected:
    ///////////////////////////////////
    // Protected internal functions  //
    ///////////////////////////////////
    void UpdateOpenBoxes();
    void SetPreviewText();
    void DisableEverything();
    void EnableEverything();
    void SaveValues();

protected:
    ///////////////////////////////////
    // Message map declarations      //
    ///////////////////////////////////
    //{{AFX_MSG(CExportDialog)
    virtual void OnCancel();
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnClose();
	afx_msg void OnChangeEdtExpRangeHi();
	afx_msg void OnChangeEdtExpRangeLo();
	afx_msg void OnChkBox();
    afx_msg void OnChkExpFlex();
	afx_msg void OnChkExpHTML();
    afx_msg void OnChkExpText();
	afx_msg void OnRdoExpAll();
	afx_msg void OnRdoExpRange();
	afx_msg void OnKillFocusEdtExpRangeLo();
	afx_msg void OnKillFocusEdtExpRangeHi();
    afx_msg void OnKeyUp(UINT,UINT,UINT);
    afx_msg void OnBtnSelExport();
	afx_msg void OnSetFocusEdtExprangelo();
	afx_msg void OnSetFocusEdtExprangehi();
	afx_msg void OnContextHelp();
	afx_msg void OnRdoExpHexASCII();
	afx_msg void OnChkAddxHex();
	//}}AFX_MSG
#   ifdef HAVE_TOOLTIPS
    afx_msg BOOL OnToolTipNeedText(UINT,NMHDR *,LRESULT *);
#   endif
	DECLARE_MESSAGE_MAP()

    ///////////////////////////////////
    // ClassWizard insertions        //
    ///////////////////////////////////
	//{{AFX_DATA(CExportDialog)
	enum {IDD=IDD_DLG_EXPORT};
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

#   if 0
	//{{AFX_VIRTUAL(CExportDialog)
	protected:
	virtual void DoDataExchange(CDataExchange *pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
#   endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPORTDIALOG_H__5F6AC086_DBA2_475A_9C20_4689382C38E0__INCLUDED_)
