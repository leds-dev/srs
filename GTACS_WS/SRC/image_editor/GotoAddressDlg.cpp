/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       GotoAddressDlg.cpp
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
*******************************************************************************/
// Implementation for the [Go to Address] dialog box.

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ImageEditor.h"
#include "ImageEditorDlg.h"
#include "GotoAddressDlg.h"
#include "htmlhelp.h"

////////////////////////////////////////////////////////////////////////////////
// Definitions                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
#   define new DEBUG_NEW
#   undef THIS_FILE
    static char THIS_FILE[]=__FILE__;
#endif

#define DEF_GoToTopic _T("gotoaddx.htm")
#define combo ((CComboBox *)GetDlgItem(IDC_CBO_NEWADDX))
#undef super
#define super CDialog

////////////////////////////////////////////////////////////////////////////////
// Construction/Destruction           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CGotoAddressDlg::CGotoAddressDlg
Parameters:     [CGotoAddressDlg *this]
                CWnd *pParent
                    Pointer to the parent window.
Returns:        void
Description:    Called to initialize the dialog structure.
PDL:
    Save the parent window as the editor
    Initialize data elements
**/
CGotoAddressDlg::CGotoAddressDlg(CWnd *pParent)
	:super(CGotoAddressDlg::IDD,pParent)
{
	//{{AFX_DATA_INIT(CGotoAddressDlg)
		// NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
    editor=(CImageEditorDlg *)pParent;
    address=0xFFFFFFFF;
    wantsAdded=false;
}

#if 0
void CGotoAddressDlg::DoDataExchange(CDataExchange *pDX)
{
	super::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGotoAddressDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}
#endif
           
////////////////////////////////////////////////////////////////////////////////
// Message Map                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
BEGIN_MESSAGE_MAP(CGotoAddressDlg,super)
	//{{AFX_MSG_MAP(CGotoAddressDlg)
	ON_CBN_EDITCHANGE(IDC_CBO_NEWADDX,OnEditChangeCboNewAddx)
	ON_WM_KEYUP()
	ON_CBN_SETFOCUS(IDC_CBO_NEWADDX,OnSetFocusCboNewaddx)
	ON_BN_CLICKED(IDC_BTN_CONTEXTHELP,OnContextHelp)
	ON_BN_CLICKED(IDC_CHK_ADDXHEX, OnChkAddxHex)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

////////////////////////////////////////////////////////////////////////////////
// Message Handlers                   //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CGotoAddressDlg::OnInitDialog
Parameters:     [CGotoAddressDlg *this]
Returns:        BOOL
                    Returns TRUE to set focus to the combo box.
Description:    Initializes the dialog box's window.
PDL:
    Set the dialog's icon
    Add previous address entries to the combo box's menu
    Select the most recent address
**/
BOOL CGotoAddressDlg::OnInitDialog() 
{
    //Not much to do here; just initialize the combo box and make sure
    //everything's clean
	super::OnInitDialog();
	
    m_hIcon=::AfxGetApp()->LoadIcon(IDI_GOTOBOX);
    SetIcon(m_hIcon,TRUE);
    SetIcon(m_hIcon,FALSE);

    // int i = 0;
    TCHAR buf[9];
    const unsigned int *axe=editor->addressEntries;
    for(int i=0;i<DEF_NumAddressEntries && ~axe[i];i++)
    {
	    if ((CImageEditorDlg::ACE1_IMAGE == editor->m_strctFileHdr.strtHeader.eType)
		  	|| (CImageEditorDlg::ACE2_IMAGE == editor->m_strctFileHdr.strtHeader.eType)){

			_stprintf_s(buf, 9,  _T("%05.5X"),axe[i]);
 	  	}  else if (CImageEditorDlg::SXI_IMAGE == editor->m_strctFileHdr.strtHeader.eType){ 
  
			_stprintf_s(buf, 9, _T("%08.8X"),axe[i]);
	  	}
        combo->AddString(buf);
    }
    combo->SetCurSel(0);

    ((CButton *)GetDlgItem(IDC_BTN_CONTEXTHELP))->SetIcon(::AfxGetApp()->LoadIcon(IDI_CONTEXTHELP));

    UpdateHelpTopic(this,DEF_GoToTopic);
    CheckDlgButton(IDC_CHK_ADDXHEX,TRUE);
	return TRUE;
}

///////////////////////////////////////
/**
Function:       CGotoAddressDlg::OnEditChangeCboNewAddx
Parameters:     [CGotoAddressDlg *this]
Returns:        void
Description:    Validates the hexadecimal number in the combo box.
PDL:
    IF the function is not currently executing THEN
        IF the string is too long THEN
            Clip the string to 8 characters
        ENDIF

        DOFOR each character in the text
            IF the character is a valid hexadecimal character THEN
                Add it to the output string
            ELSE
                IF the current character's index is before the
                   selection or cursor THEN
                    Shift the selection to the left
                ENDIF
            ENDIF
        ENDDO
        IF the string was changed THEN
            Update it in the combo box
        ENDIF
**/
void CGotoAddressDlg::OnEditChangeCboNewAddx() 
{
    if(m_bValidate) Doubleword::ValidateHex((CEdit *)combo,sz_dword);
    else Doubleword::ValidateDec((CEdit *)combo,sz_dword);
}

///////////////////////////////////////
/**
Function:       CGotoAddressDlg::OnKeyUp      
Parameters:     [CGotoAddressDlg *this]
                UINT nChar
                    Character value passed by Windows
                UINT nRepCnt
                    Repeat count passed by Windows
                UINT nFlags
                    Keypress flags passed by Windows
Returns:        void
Description:    Catches any uncaught accelerator keys.
PDL:
    IF the key is repeating THEN
        Superclass the function
    ELSE
        Perform the requested action
    ENDIF
**/
void CGotoAddressDlg::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
    if(nFlags&KF_REPEAT)
    {
        super::OnKeyUp(nChar,nRepCnt,nFlags);
        return;
    }

    if(nChar=='N' && (nFlags&KF_ALTDOWN))
    {
        combo->SetFocus();
        return;
    }
    if(nChar=='S' && (nFlags&KF_ALTDOWN))
    {
        GetDlgItem(IDC_CHK_SAVERECENT)->SetFocus();
        CheckDlgButton(IDC_CHK_SAVERECENT,!IsDlgButtonChecked(IDC_CHK_SAVERECENT));
        return;
    }
	super::OnKeyUp(nChar, nRepCnt, nFlags);
}

///////////////////////////////////////
/**
Function:       CGotoAddressDlg::OnSetFocusCboNewAddx
Parameters:     [CGotoAddressDlg *this]
Returns:        void
Description:    Called when the combo box receives focus
PDL:
    Select everything in the combo box
**/
void CGotoAddressDlg::OnSetFocusCboNewaddx() 
{
    combo->SetEditSel(0,combo->GetWindowTextLength());
}

///////////////////////////////////////
/**
Function:       CGotoAddressDlg::OnContextHelp
Parameters:     [CGotoAddressDlg *this]
Returns:        void
Description:    Called when the user requests help.
PDL:
    Display help topic for this dialog box.
**/
void CGotoAddressDlg::OnContextHelp() 
{
    DisplayHelpTopic(GetParent(),DEF_GoToTopic);
}

///////////////////////////////////////
/**
Function:       CGotoAddressDlg::OnChkAddxHex
Parameters:     [CGotoAddressDlg *this]
Returns:        void
Description:    Called when the "hex" check box is toggled.
PDL:
    Toggle the value in the combo box between hexadecimal and decimal.
**/
void CGotoAddressDlg::OnChkAddxHex() 
{
    m_bValidate=!!IsDlgButtonChecked(IDC_CHK_ADDXHEX);
    CString s;

    CComboBox *c=combo;
    c->GetWindowText(s);
    if(s.GetLength())
        s.Format(m_bValidate?_T("%X"):_T("%u"),
            (m_bValidate?(Doubleword::ParseDecimal(s)):(Doubleword::ParseHex(s))));
    c->ResetContent();

    // int i;
    TCHAR buf[12];
    const unsigned int *axe=editor->addressEntries;
    for(int i=0;i<DEF_NumAddressEntries && ~axe[i];i++)
    {
        _stprintf_s (buf, 12, m_bValidate?_T("%08.8X"):_T("%u"),axe[i]);
        combo->AddString(buf);
    }
    c->SetWindowText(s);
}

////////////////////////////////////////////////////////////////////////////////
// Virtual Overrides                  //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CGotoAddressDlg::OnOK
Parameters:     [CGotoAddressDlg *this]
Returns:        void
Description:    Called when the OK button is pressed.  Saves the
                currently entered address and ends the dialog.
PDL:
    IF no address has been entered THEN
        End the dialog and return "cancel"
        RETURN
    ENDIF

    Save the address and checkbox state
    End the dialog and return "OK"
**/
void CGotoAddressDlg::OnOK() 
{
    CString p;
    combo->GetWindowText(p);
    if(p.GetLength()<1)
    {
        EndDialog(IDCANCEL);
        return;
    }

    address=m_bValidate?Doubleword::ParseHex(p):Doubleword::ParseDecimal(p);
    wantsAdded=!!IsDlgButtonChecked(IDC_CHK_SAVERECENT);
	EndDialog(IDOK);
}
