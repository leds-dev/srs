/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       GotoAddressDlg.h
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
*******************************************************************************/
// Header for the [Go to Address] dialog.

#if !defined(AFX_GOTOADDRESSDLG_H__789DF800_4529_4C47_BFBB_4B697876C6EF__INCLUDED_)
#define AFX_GOTOADDRESSDLG_H__789DF800_4529_4C47_BFBB_4B697876C6EF__INCLUDED_
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "ImageEditorDlg.h"

////////////////////////////////////////////////////////////////////////////////
// Class Definitions                  //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class CGotoAddressDlg:public CDialog
{
public:
    ///////////////////////////////////
    //Public interface functions     //
    ///////////////////////////////////
	CGotoAddressDlg(CWnd *pParent=NULL);
/**
Function:       CGotoAddressDlg::GetAddress (inline)
Parameters:     [const CGotoAddressDlg *this]
Returns:        DWORD
Description:    Returns the entered address.
**/ inline DWORD GetAddress() const {return address;}
/**
Function:       CGotoAddressDlg::WantsRecentAdded (inline)
Parameters:     [const CGotoAddressDlg *this]
Returns:        bool
Description:    Returns true if the user wants the current address added
                to the "Recent Address" list.
**/ inline bool WantsRecentAdded() const
        {return wantsAdded;}

protected:
    ///////////////////////////////////
    // Protected member variables    //
    ///////////////////////////////////
    const CImageEditorDlg *editor;
    HICON m_hIcon;
    DWORD address;
    bool wantsAdded;
    bool m_bValidate;

protected:
    ///////////////////////////////////
    //Message map declarations       //
    ///////////////////////////////////
	//{{AFX_MSG(CGotoAddressDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnEditChangeCboNewAddx();
	virtual void OnOK();
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnSetFocusCboNewaddx();
	afx_msg void OnContextHelp();
	afx_msg void OnChkAddxHex();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

    ///////////////////////////////////
    // ClassWizard insertions        //
    ///////////////////////////////////
	//{{AFX_DATA(CGotoAddressDlg)
	enum {IDD=IDD_DLG_GOTOADDRESS};
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

#if 0
	//{{AFX_VIRTUAL(CGotoAddressDlg)
	protected:
	virtual void DoDataExchange(CDataExchange *pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GOTOADDRESSDLG_H__789DF800_4529_4C47_BFBB_4B697876C6EF__INCLUDED_)
