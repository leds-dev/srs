/*
@(#)  File Name: ImageEditor.cpp  Release 1.0 Date 07/16/02 17:41:00
*/
/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       ImageEditor.cpp
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
*******************************************************************************/
// Implementation for the ImageEditor application class.

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ImageEditor.h"
#include "ImageEditorDlg.h"
#include "htmlhelp.h"

////////////////////////////////////////////////////////////////////////////////
// Definitions                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#undef super
#define super CWinApp

#ifdef _AFXDLL
#   define _Enable3DControls Enable3dControls
#else
#   define _Enable3DControls Enable3dControlsStatic
#endif

////////////////////////////////////////////////////////////////////////////////
// Global Definitions                 //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
CImageEditorApp theApp;
static CString ltfi;

////////////////////////////////////////////////////////////////////////////////
// Construction/Destruction           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/**
Function:       CImageEditorApp::CImageEditorApp
Parameters:     [CImageEditor *this]
Returns:        void
Description:    Called to initialize the CImageEditorApp object.
**/
CImageEditorApp::CImageEditorApp():CWinApp(_T("GOES Image Editor")) {}

/**
Function:       CImageEditorApp::~CImageEditorApp
Parameters:     [CImageEditor *this]
Returns:        void
Description:    Called to uninitialize the CImageEditorApp object.
**/
CImageEditorApp::~CImageEditorApp() {}

////////////////////////////////////////////////////////////////////////////////
// Virtual Overrides                  //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/**
Function:       CImageEditorApp::InitInstance
Parameters:     [CImageEditor *this]
Returns:        BOOL
                    FALSE so that the application exits.
Description:    Called before the main message pump is started.
PDL:
    Initialize HTML Help
    IF arguments were passed to the program THEN
        Retrieve the first argument along with any quoted sections
        Select that argument as a file to load
        IF there are more arguments THEN
            Spawn a copy of the program with all arguments beyond the first
        ENDIF
    ENDIF
    IF the program is being run from a fixed or remote drive THEN
        Register the .img file type
    ENDIF
    Start the editor dialog
**/
BOOL CImageEditorApp::InitInstance()
{
 //    _Enable3DControls();
 //   SetDialogBkColor(::GetSysColor(COLOR_BTNFACE),::GetSysColor(COLOR_BTNTEXT));

    BeginWaitCursor();
    __HTMLHELP HH;

    //Handle any arguments
    if(__argc>=2)       
    {
        TCHAR *cmdline=::GetCommandLine();
        int i;
        int endpnm=-1,startfa=-1,endfa=-1,otherargs=-1;
    
        for(i=0;cmdline[i] && cmdline[i]!=' ' && cmdline[i]!='\t';i++)
            if(cmdline[i]=='"')
            {
                ++i;
                while(cmdline[i] && cmdline[i]!='"')
                    ++i;
            }
        if(!cmdline[i]) --i;
        endpnm=i;
        if(cmdline[i])
        {
            ++i;
            while(cmdline[i]==' ' || cmdline[i]=='\t')
                ++i;
            startfa=i;

            for(;cmdline[i] && cmdline[i]!=' ' && cmdline[i]!='\t';i++)
                if(cmdline[i]=='"')
                {
                    ++i;
                    while(cmdline[i] && cmdline[i]!='"')
                        ++i;
                }
        
            if(!cmdline[i]) endfa=--i;
            if(cmdline[i])
            {
                endfa=i;
                ++i;
                while(cmdline[i]==' ' || cmdline[i]=='\t')
                    ++i;
                if(cmdline[i])
                    otherargs=i;
            }
        }

        if(startfa>0 && endfa>0)
        {
            ltfi=CString(cmdline);
            ltfi=ltfi.Left(endfa+1);
            ltfi=ltfi.Right(endfa+1-startfa);
            TCHAR *out=new TCHAR[ltfi.GetLength()+1];
            LPCTSTR in=(LPCTSTR)ltfi;
            int j;

            for(i=j=0;in[i];i++)
                if(in[i]!='"')
                    out[j++]=in[i];
            out[j]=in[i];
            ltfi=CString(out);
            delete[] out;

            app_LoadFileImmediately=true;
            app_LoadThisFile=(LPCTSTR)ltfi;

            if(otherargs>0)
            {
                CString runThis(cmdline);
                runThis=runThis.Left(endpnm+1)+_T(" ");
                runThis+=CString(cmdline+otherargs);

#ifdef _UNICODE
                char *buffer=new char[runThis.GetLength()+1];
                LPCTSTR p=(LPCTSTR)runThis;
                for(i=0;p[i];i++) buffer[i]=(char)p[i];
                buffer[i]=(char)p[i];
                ::WinExec(buffer,SW_SHOWNORMAL);
                delete[] buffer;
#else
                ::WinExec((LPCSTR)runThis,SW_SHOWNORMAL);
#endif
            }
        }
    }

    //Register the .img file type and icon if we're on a fixed/remote drive
    TCHAR s[MAX_PATH+1];
    ::GetModuleFileName(NULL,s,MAX_PATH);
    CString ss(s);
    bool reg=false;

    if(s[1]==':')
    {
        CString st(s);
        st=st.Left(3);
        DWORD d=::GetDriveType(st);
        if(d==DRIVE_FIXED || d==DRIVE_REMOTE)
            reg=true;
    }
    else
        reg=true;

    HKEY key;
    DWORD whatHappened;
    if(reg && ::RegCreateKeyEx(HKEY_CLASSES_ROOT,_T("ImageEditor.Image"),0,NULL,
        REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&key,&whatHappened)==ERROR_SUCCESS)
    {
        HKEY key2;
        if(::RegCreateKey(key,_T("shell\\open\\command"),&key2)==ERROR_SUCCESS)
        {
            ss=CString(_T("\""))+ss+_T("\" \"%1\"");
            ::RegSetValueEx(key2,_T(""),0,REG_SZ,(LPBYTE)(LPCTSTR)ss,(ss.GetLength()+1)*sizeof(TCHAR));
            ::RegCloseKey(key2);
        }
    
        ::RegSetValueEx(key,_T(""),0,REG_SZ,(LPBYTE)_T("GOES Memory Image"),sizeof(_T("GOES Memory Image")));
        ss=s;
        ss+=_T(",1");
        if(::RegCreateKey(key,_T("DefaultIcon"),&key2)==ERROR_SUCCESS)
        {
            ::RegSetValueEx(key2,_T(""),0,REG_SZ,(LPBYTE)(LPCTSTR)ss,(ss.GetLength()+1)*sizeof(TCHAR));
            ::RegCloseKey(key2);
        }

        if(whatHappened!=REG_OPENED_EXISTING_KEY)
            ::SendMessage(HWND_BROADCAST,WM_SETTINGCHANGE,NULL,(DWORD)_T("�SHCH!"));
    }

    if(reg && ::RegCreateKey(HKEY_CLASSES_ROOT,_T(".img"),&key)==ERROR_SUCCESS)
    {
        ::RegSetValueEx(key,_T(""),0,REG_SZ,(LPBYTE)_T("ImageEditor.Image"),sizeof(_T("ImageEditor.Image")));
        ::RegCloseKey(key);
    }

    //Start the dialog
    EndWaitCursor();
    CImageEditorDlg dlg;
    m_pMainWnd=&dlg;
    (void)dlg.DoModal();

    //Exit
	return FALSE;
}

////////////////////////////////////////////////////////////////////////////////
// Message Map                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
BEGIN_MESSAGE_MAP(CImageEditorApp,super)
	//{{AFX_MSG_MAP(CImageEditorApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
END_MESSAGE_MAP()