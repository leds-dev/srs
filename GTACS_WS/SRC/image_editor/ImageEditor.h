/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       ImageEditor.h
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
*******************************************************************************/
// Header for the ImageEditor application class

#ifndef __AFXWIN_H__
#   include "stdafx.h"
#endif
#if !defined(AFX_IMAGEEDITOR_H__17C7E490_2686_4BD9_9945_D1C82F7D8C72__INCLUDED_)
#define AFX_IMAGEEDITOR_H__17C7E490_2686_4BD9_9945_D1C82F7D8C72__INCLUDED_
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "resource.h"
#include "ImageEditorDlg.h"

////////////////////////////////////////////////////////////////////////////////
// Class Definitions                  //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class CImageEditorApp:public CWinApp
{
public:
    ///////////////////////////////////
    // Public interface functions    //
    ///////////////////////////////////
	CImageEditorApp();
    ~CImageEditorApp();

    ///////////////////////////////////
    // ClassWizard insertions        //
    ///////////////////////////////////
    //{{AFX_VIRTUAL(CImageEditorApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

	//{{AFX_MSG(CImageEditorApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGEEDITOR_H__17C7E490_2686_4BD9_9945_D1C82F7D8C72__INCLUDED_)
