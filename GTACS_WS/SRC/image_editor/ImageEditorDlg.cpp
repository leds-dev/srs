/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       ImageEditorDlg.cpp
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
		 V.1.1		 09/06       F. Shaw         Ported to Visual Studio 2005
*******************************************************************************/
// Implementation file for the main editor dialog.

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include <winuser.h>
#include <time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "ImageEditor.h"
#include "ImageEditorDlg.h"
#include "GotoAddressDlg.h"
#include "htmlhelp.h"

////////////////////////////////////////////////////////////////////////////////
// Definitions                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
#   define new DEBUG_NEW
#   undef THIS_FILE
    static char THIS_FILE[]=__FILE__;
#endif

#define _USE_32BIT_TIME_T 1

#define helpDefRef _T("index.htm")
#define DEF_ShortcutButtonTopic _T("buttons/shortcut.htm")
#define DEF_CaptionTopic _T("buttons/caption.htm")
#define DEF_TableTopic _T("table.htm")
#define DEF_EditTopic _T("edit.htm")
#define DEF_ScrollTopic _T("buttons/scroll.htm")
#define DEF_ApplyTopic _T("buttons/apply.htm")
#define DEF_CloseTopic _T("buttons/caption.htm")
#define DEF_FileMenuTopic _T("menu/file.htm")
#define DEF_GoMenuTopic _T("menu/go.htm")
#define DEF_UpdateMenuTopic _T("menu/update.htm")
#define DEF_SizerTopic _T("buttons/size.htm")

#define Lst_Changed ((CListBox *)GetDlgItem(IDC_LST_CHANGED))
#define Lst_Address ((CListBox *)GetDlgItem(IDC_LST_ADDRESS))
#define Lst_ValueHex ((CListBox *)GetDlgItem(IDC_LST_VALUE_HEX))
#define Lst_ValueDec ((CListBox *)GetDlgItem(IDC_LST_VALUE_DEC))
#define Lst_UTCTime ((CListBox *)GetDlgItem(IDC_LST_UTCTIME))
#define Scr_Address ((CScrollBar *)GetDlgItem(IDC_SCR_ADDRESS))
#define Edt_Address ((CEdit *)GetDlgItem(IDC_EDT_ADDRESS))
#define Edt_ValueHex ((CEdit *)GetDlgItem(IDC_EDT_VALUE_HEX))
#define Edt_ValueDec ((CEdit *)GetDlgItem(IDC_EDT_VALUE_DEC))
#define Edt_UTCTime ((CEdit *)GetDlgItem(IDC_EDT_UTCTIME))
#define FakeBtn_Top ((CButton *)GetDlgItem(IDC_FAKEBTN_TOP))
#define FakeBtn_Bot ((CButton *)GetDlgItem(IDC_FAKEBTN_BOT))
#define FakeBtn_Apply ((CButton *)GetDlgItem(IDC_FAKEBTN_APPLY))
#define Btn_(x) ((CButton *)GetDlgItem(IDC_BTN_##x))

#undef super    
#define super CDialog

////////////////////////////////////////////////////////////////////////////////
// Global Type/Structure Definitions  //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
struct tagHelpRef                     //Represents a link between a control and
{                                     // its topic within the help file.
    UINT ctlID;
    LPCTSTR URI;
};

////////////////////////////////////////////////////////////////////////////////
// Global Variable Definitions        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static tagHelpRef helpRefs[]=         //Links each control to its help topic.
{
    {IDC_BTN_FILEOPEN,_T("buttons/shortcut.htm")},
    {IDC_BTN_FILESAVE,_T("buttons/shortcut.htm")},
    {IDC_BTN_SAVECOPY,_T("buttons/shortcut.htm")},
    {IDC_BTN_FILEPRINT,_T("buttons/shortcut.htm")},
    {IDC_BTN_FILEEXPORT,_T("buttons/shortcut.htm")},
    {IDC_BTN_GOTOADDRESS,_T("buttons/shortcut.htm")},
    {IDC_BTN_NEXTCHANGE,_T("buttons/shortcut.htm")},
    {IDC_BTN_PREVCHANGE,_T("buttons/shortcut.htm")},
    {IDC_BTN_CONTEXTHELP,_T("buttons/shortcut.htm")},
    {IDC_BTN_REFRESH,_T("buttons/shortcut.htm")},
    {IDC_LST_CHANGED,_T("table.htm")},
    {IDC_LST_ADDRESS,_T("table.htm")},
    {IDC_LST_VALUE_HEX,_T("table.htm")},
    {IDC_LST_VALUE_DEC,_T("table.htm")},
    {IDC_LST_UTCTIME,_T("table.htm")},
    {IDC_SCR_ADDRESS,_T("buttons/scroll.htm")},
    {IDC_FAKEBTN_TOP,_T("buttons/scroll.htm")},
    {IDC_FAKEBTN_BOT,_T("buttons/scroll.htm")},
    {IDC_FAKEBTN_APPLY,_T("buttons/apply.htm")},
    {IDC_EDT_ADDRESS,_T("edit.htm")},
    {IDC_EDT_VALUE_HEX,_T("edit.htm")},
    {IDC_EDT_VALUE_DEC,_T("edit.htm")},
    {IDC_EDT_UTCTIME,_T("edit.htm")},
    {0,_T("buttons/resize.htm")},
    {0,0},
};

////////////////////////////////////////////////////////////////////////////////
// Construction/Destruction           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CImageEditorDlg::CImageEditorDlg
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called to initialize the CImageEditorDlg structure.
PDL:
    Load icon and save handle
    Load standard cursors
    Clear recent file/recent address entries
    Initialize member data to safe defaults
**/
CImageEditorDlg::CImageEditorDlg(CWnd *pParent)
	:super(CImageEditorDlg::IDD,pParent),
     currentFile()
{
	//{{AFX_DATA_INIT(CImageEditorDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
    
    CWinApp *p=::AfxGetApp();
    cursHelp=p->LoadStandardCursor(IDC_HELP);
    cursNorm=p->LoadStandardCursor(IDC_ARROW);
    cursText=p->LoadStandardCursor(IDC_IBEAM);
    cursResize=p->LoadStandardCursor(IDC_SIZENS);
	m_hIcon=p->LoadIcon(IDR_MAINFRAME);

    memset(openEntries,0,sizeof(openEntries));
    memset(addressEntries,0xFF,sizeof(addressEntries));
    
    inMsg=working=changed=bReadOnly=inContextHelp=chlbDown=
        m_bApplyDown=m_bTopDown=m_bBotDown=m_bInApply=m_bInTop=
        m_bInBot=m_bSizerDown=m_bSizerDownR=m_bMBDn=
        m_bDragScrolling=!(bEnableAutoRefresh=true);
    
    minDisplayed=0;
	lowestAddress=0;
	highestAddress=0xFFFFFF;
    InvdCache();

    changes = 0;
	curIO = 0;
    m_Loaded = 0;
	m_CacheSize = 0;
    timerInterval = 1;
	timerCount = 0;
	m_nAddrSpace = 0;

    ExportData.flags=__ExportData::Header|__ExportData::ColHeaders;
    PrintData.bMargin=PrintData.lMargin=PrintData.tMargin=PrintData.rMargin=Float2DWord(1.0F);
    PrintData.font=_T("Courier New");PrintData.fontSize=10;
    PrintData.pprHgt=PrintData.pprWid=0;
}

/**
Function:       CImageEditorDlg::~CImageEditorDlg
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called to destroy the dialog and its data.
PDL:
    Delete all recently-opened entry strings.
    Destroy the change list.
    Free the cache.
    Close any currently open files.
**/
CImageEditorDlg::~CImageEditorDlg()
{
 //   register int i;

    //Frees up everything allocated.
    for(register int i=0;i<DEF_NumOpenEntries;i++)
        if(openEntries[i])
        {
            delete[] openEntries[i];
            openEntries[i]=0;
        }
   ClearChanges();
   if(m_Loaded)
       delete[] m_Loaded;
   if(curIO) fclose(curIO);
}

#if 0
void CImageEditorDlg::DoDataExchange(CDataExchange *pDX)
{
	super::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImageEditorDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}
#endif

BEGIN_MESSAGE_MAP(CImageEditorDlg,super)
	//{{AFX_MSG_MAP(CImageEditorDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_LBN_SELCHANGE(IDC_LST_CHANGED,OnSelchangeLstChanged)
	ON_LBN_SELCHANGE(IDC_LST_ADDRESS,OnSelchangeLstAddress)
	ON_LBN_SELCHANGE(IDC_LST_VALUE_HEX,OnSelchangeLstValueHex)
	ON_LBN_SELCHANGE(IDC_LST_VALUE_DEC,OnSelchangeLstValueDec)
	ON_LBN_SELCHANGE(IDC_LST_UTCTIME,OnSelchangeLstUtctime)
	ON_EN_CHANGE(IDC_EDT_VALUE_HEX,OnChangeEdtValueHex)
	ON_EN_CHANGE(IDC_EDT_VALUE_DEC,OnChangeEdtValueDec)
	ON_COMMAND(ID_FILE_EXPORT,OnMenu_FileExport)
	ON_COMMAND(ID_FILE_OPEN,OnMenu_FileOpen)
	ON_COMMAND(ID_FILE_PRINT,OnMenu_FilePrint)
	ON_COMMAND(ID_FILE_SAVE_COPY_AS,OnMenu_FileSaveCopy)
	ON_COMMAND(ID_VIEW_GOTOADDRESS,OnMenu_ViewGotoaddress)
    ON_COMMAND(ID_VIEW_GONEXTCHANGED,OnMenu_ViewGotoNextCh)
    ON_COMMAND(ID_VIEW_GOPREVCHANGED,OnMenu_ViewGotoPrevCh)
	ON_COMMAND(ID_FILE_SAVE,OnMenu_FileSave)
	ON_COMMAND(ID_HELP_INDEX,OnMenu_HelpIndex)
	ON_COMMAND(ID_FILE_REVERT,OnMenu_FileRevert)
    ON_COMMAND(ID_REFRESH_ENABLE,OnRefreshEnable)
	ON_COMMAND(ID_APP_EXIT,OnAppExit)
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDBLCLK()
	ON_WM_MOUSEMOVE()
	ON_WM_VSCROLL()
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
    ON_WM_MOUSEWHEEL()
	ON_COMMAND(ID_FILE_CLOSE,OnMenu_FileClose)
    ON_COMMAND(ID_VIEW_GOTOCUR,OnMenu_ViewGotoHighlighted)
    ON_WM_CONTEXTMENU()
	ON_EN_SETFOCUS(IDC_EDT_VALUE_HEX,OnSetFocusEdtValueHex)
	ON_EN_SETFOCUS(IDC_EDT_VALUE_DEC,OnSetFocusEdtValueDec)
    ON_COMMAND(ID_CONTEXT_HELP,OnContextHelp)
	ON_WM_HELPINFO()
	ON_WM_TIMER()
    ON_COMMAND(ID_VIEW_REFRESH,OnBtnRefresh)
    ON_WM_MBUTTONDOWN()
    ON_WM_MBUTTONUP()
    ON_WM_SETCURSOR()
	ON_BN_CLICKED(IDC_FAKEBTN_TOP,OnBtnTop)
	ON_BN_CLICKED(IDC_FAKEBTN_BOT,OnBtnBot)
	ON_BN_CLICKED(IDC_FAKEBTN_APPLY,OnBtnApply)
	ON_BN_CLICKED(IDC_RADIO_OPERAND, OnAddrSpaceRadio)
	ON_LBN_DBLCLK(IDC_LST_CHANGED,OnSelchangeLstChanged)
	ON_LBN_DBLCLK(IDC_LST_ADDRESS,OnSelchangeLstAddress)
	ON_LBN_DBLCLK(IDC_LST_VALUE_HEX,OnSelchangeLstValueHex)
	ON_LBN_DBLCLK(IDC_LST_VALUE_DEC,OnSelchangeLstValueDec)
	ON_LBN_DBLCLK(IDC_LST_UTCTIME,OnSelchangeLstUtctime)
	ON_BN_CLICKED(IDC_BTN_FILEOPEN,OnMenu_FileOpen)
	ON_BN_CLICKED(IDC_BTN_FILESAVE,OnMenu_FileSave)
	ON_BN_CLICKED(IDC_BTN_FILEPRINT,OnMenu_FilePrint)
	ON_BN_CLICKED(IDC_BTN_FILEEXPORT,OnMenu_FileExport)
	ON_BN_CLICKED(IDC_BTN_GOTOADDRESS,OnMenu_ViewGotoaddress)
	ON_BN_CLICKED(IDC_BTN_NEXTCHANGE,OnMenu_ViewGotoNextCh)
	ON_BN_CLICKED(IDC_BTN_PREVCHANGE,OnMenu_ViewGotoPrevCh)
	ON_BN_CLICKED(IDC_BTN_CONTEXTHELP,OnContextHelp)
	ON_BN_CLICKED(IDC_BTN_SAVECOPY,OnMenu_FileSaveCopy)
	ON_BN_CLICKED(IDC_BTN_REFRESH,OnBtnRefresh)
	ON_BN_CLICKED(IDC_RADIO_INSTRUCT, OnAddrSpaceRadio)
	//}}AFX_MSG_MAP
  //  ON_MESSAGE(WM_DISPLAYCHANGE, OnWinIniChange)
#ifdef HAVE_TOOLTIPS
    ON_NOTIFY_EX(TTN_NEEDTEXT,0,OnToolTipNeedText)
#endif
END_MESSAGE_MAP()

////////////////////////////////////////////////////////////////////////////////
// Message Handlers                   //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnInitDialog
Parameters:     [CImageEditorDlg *this]
Returns:        BOOL
                    TRUE to set the focus to the Open button
Description:    Initializes the dialog window.
PDL:
    Remove the "Size" and "Maximize" commands from the menu
    Set the dialog's icon
    Set the icons of all shortcut buttons
    Load and set up the menu
    Initialize the controls
    Start the auto-refresh timer
    Load registry values
    Load any files passed on the command line
**/
BOOL CImageEditorDlg::OnInitDialog()
{
    super::OnInitDialog();

    CMenu *m=GetSystemMenu(FALSE);
    m->DeleteMenu(SC_SIZE,MF_BYCOMMAND);
    m->DeleteMenu(SC_MAXIMIZE,MF_BYCOMMAND);

	SetIcon(m_hIcon,TRUE);
	SetIcon(m_hIcon,FALSE);
	
    //Set all the shortcut buttons' icons
    register CWinApp *app=::AfxGetApp();
    Btn_(FILEOPEN)->SetIcon(app->LoadIcon(IDI_FILEOPEN));
    Btn_(FILESAVE)->SetIcon(app->LoadIcon(IDI_FILESAVE));
    Btn_(FILEPRINT)->SetIcon(app->LoadIcon(IDI_FILEPRINT));
    Btn_(FILEEXPORT)->SetIcon(app->LoadIcon(IDI_FILEEXPORT));
    Btn_(SAVECOPY)->SetIcon(app->LoadIcon(IDI_SAVECOPY));
    Btn_(GOTOADDRESS)->SetIcon(app->LoadIcon(IDI_VIEWGOTOADDX));
    Btn_(NEXTCHANGE)->SetIcon(app->LoadIcon(IDI_VIEWNEXTCHANGE));
    Btn_(PREVCHANGE)->SetIcon(app->LoadIcon(IDI_VIEWPREVCHANGE));
    Btn_(CONTEXTHELP)->SetIcon(app->LoadIcon(IDI_CONTEXTHELP));
    Btn_(REFRESH)->SetIcon(app->LoadIcon(IDI_REFRESH));
    ((CButton *)GetDlgItem(IDC_FAKEBTN_TOP))->SetIcon(app->LoadIcon(IDI_BTNTOP));
    ((CButton *)GetDlgItem(IDC_FAKEBTN_BOT))->SetIcon(app->LoadIcon(IDI_BTNBOT));
	((CButton*)GetDlgItem(IDC_RADIO_OPERAND))->SetCheck(1); // Default operand checked

    //Set the menu...
    if(m_Menu.LoadMenu(IDR_MAINFRAME))
        SetMenu(&m_Menu);
    
    Scr_Address->EnableWindow(FALSE);

    Edt_ValueHex->LimitText(4);
    Edt_ValueDec->LimitText(5);

    //Set up the special buttons
    CRect changedArea,addxArea,scrollArea;
    Lst_Changed->GetWindowRect(&changedArea);
    Edt_Address->GetWindowRect(&addxArea);
    m_bApply.SetRect(changedArea.left,addxArea.top,changedArea.right,addxArea.bottom);
    ScreenToClient(&m_bApply);

    Scr_Address->GetWindowRect(&scrollArea);
    m_bTop.SetRect(scrollArea.left,changedArea.top,scrollArea.right,scrollArea.top-1);
    m_bBot.SetRect(scrollArea.left,scrollArea.bottom+4,scrollArea.right,changedArea.bottom);
    ScreenToClient(&m_bTop);
    ScreenToClient(&m_bBot);

    GetWindowRect(&changedArea);
    winWidth=changedArea.Width();
    int cx=(changedArea.left+changedArea.right)>>1;
    m_cSizer.SetRect(cx-20,addxArea.bottom+2,cx+20,changedArea.bottom-1);
    ScreenToClient(&m_cSizer);

    //Set up the auto-refresh timer
    tickID=SetTimer(1,1000,NULL);

    LoadRegistryValues();
    UpdateMenu();

    //Catch anything from the command line...
    if(app_LoadFileImmediately)
    {
        if(app_LoadThisFile[0]=='*')
            bReadOnly=true;
        this->DoLoad(CString(app_LoadThisFile));
        app_LoadFileImmediately=0;
            //v- Note that this is OK because it points into a CString.
        app_LoadThisFile=0;
        
        //If an invalid filename was passed, quit.
        if(!working) EndDialog(IDCANCEL);
        Edt_ValueHex->SetFocus();
    }
    else
        GetDlgItem(IDC_BTN_FILEOPEN)->SetFocus();

#   if(defined(HAVE_TOOLTIPS))
#       if(HAVE_TOOLTIPS==1)
            EnableTrackingToolTips();
#       else
            EnableToolTips();
#       endif
#   endif
	return TRUE;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnPaint
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Updates graphics in the window.
PDL:
    IF window is minimized THEN
        Draw icon
    ELSE
        Draw buttons in their current states
    ENDIF
**/
void CImageEditorDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this);

		SendMessage(WM_ICONERASEBKGND,(WPARAM)dc.GetSafeHdc(),0);

		int cxIcon=GetSystemMetrics(SM_CXICON);
		int cyIcon=GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x=(rect.Width()-cxIcon+1)>>1;
		int y=(rect.Height()-cyIcon+1)>>1;

		dc.DrawIcon(x, y, m_hIcon);
	}
	else
    {
        super::OnPaint();
        DrawBtnApply(m_bApplyDown);
        DrawBtnsTopBot(true,m_bTopDown);
        DrawBtnsTopBot(false,m_bBotDown);
        DrawSizer(m_bSizerDown || m_bSizerDownR);

        CPaintDC _dc(this);
    }
}

///////////////////////////////////////
/**
Function:       CImageEditor::OnQueryDragIcon
Parameters:     [CImageEditor *this]
Returns:        HCURSOR
                    Handle to cursor for dragging the icon.
Description:    Causes the cursor to appear as the window's icon when the
                window is dragged in its iconic state.
**/
HCURSOR CImageEditorDlg::OnQueryDragIcon() {return (HCURSOR)m_hIcon;}

///////////////////////////////////////
/**
Function:       CImageEditor::OnTimer
Parameters:     [CImageEditor *this]
                UINT nIDEvent
                    ID of timer that just ticked.
Returns:        void
Description:    Handles auto-refresh and some window update.
PDL:
    IF not already handling a message THEN
        IF the window is too large to fit on the screen THEN
            Resize the window to fit on the screen
            Repaint the window
        ENDIF
        IF auto-refresh is disabled THEN
            RETURN
        ENDIF

        Increment the tick count
        IF it's not time to check the file for updates THEN
            RETURN
        ENDIF

        Reset the tick count
        IF no file is open THEN
            RETURN
        ENDIF
        IF the file has changed THEN
            Reload the cache
            Refresh the display
        ENDIF
    ENDIF
**/
void CImageEditorDlg::OnTimer(UINT nIDEvent) 
{
    static bool inside=false;
    struct _stat buf;

    if(inside || inMsg)
        return;

    if(nIDEvent!=1)
    {
	    super::OnTimer(nIDEvent);
        inside=false;
        return;
    }

    if(working && !m_bDragScrolling){
		// Don't need to convert address since not displaying it.
		// Only reading from disk.
		GetAddx(minDisplayed);
	}

    if(!bEnableAutoRefresh){
        return;
	}
    inside=true;

    ++timerCount;
    if(timerCount<timerInterval)
    {
        inside=false;
        return;
    }

    timerCount=0;

    if(!working || curIO->_ptr == NULL )
    {
        inside=false;
        return;
    }

    //If the file's last-modified time has changed,
    //flush/reload the cache and re-set the selections


	// Open current file that matches data in memory.
//	errno_t err;
//	err = _tfopen_s(&curIO, (LPCTSTR)currentFile, _T("r+b"));

    if(!(_fstat(_fileno(curIO),&buf)))
    {
        if(buf.st_mtime!=savedSTMTime)
        {
            savedSTMTime = buf.st_mtime;
            InvdCache();
            int i=Lst_Changed->GetCurSel();
			// Don't need to convert address.
			// DoListing will convert addresses when displaying it.
			// Addresses are kept in physical format until displayed.
    //        GetAddx(minDisplayed);
            DoListing(minDisplayed);
            inMsg=true;
            NewEditValue(i);
            inMsg=false;
        }
    }
    inside=false;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnClose
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Aborts the copy process or closes the dialog.
PDL:
    IF copying THEN
        Abort the copy process
        RETURN
    ENDIF

    Save registry values
    IF the file has been changed THEN
        IF the user wants to cancel THEN
            RETURN
        ELSE
            IF the user wants to save changes THEN
                Save changes
            ENDIF
        ENDIF
    ENDIF
    Stop the timer
    End the dialog and return "OK"
**/
void CImageEditorDlg::OnClose()
{
    //The only time this can get called while a message handler is
    //blocking messages is while DoSave() is copying a file.
    if(inMsg) {bAbort=true;return;}

    if(working && changed)
    {
        int result=MessageBox(_T("Save changes before quitting?"),_T("Save changes"),
            MB_YESNOCANCEL|MB_ICONQUESTION|MB_DEFBUTTON1);

        if(result==IDCANCEL || result==IDABORT) return;
        if(result==IDYES) DoSave(currentFile);
    }

    UpdateMenu();
    if(tickID) KillTimer(tickID);
    WriteRegistryValues();
    EndDialog(IDOK);
}
///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnAppExit
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Aborts the copy process or closes the dialog.
PDL:
    CALL OnClose
**/
void CImageEditorDlg::OnAppExit() {OnClose();}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnCancel
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Cancels the context help capture loop if active.
PDL:
    IF the context help capture loop is active THEN
        Release mouse capture
        Restore the cursor
        Restore the caption text
    ENDIF
**/
void CImageEditorDlg::OnCancel()
{
    if(inContextHelp)
    {
        ::SetCursor(cursNorm);
        ReleaseCapture();
        inContextHelp=false;
        SetWindowText(_owt);
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnOK
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Simulates a click of the Apply button.
PDL:
    CALL OnBtnApply
**/
void CImageEditorDlg::OnOK() {OnBtnApply();}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnSelchangeLstChanged
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches a click within the Changed table column.
PDL:
    IF not in a message handler AND a file is loaded THEN
        CALL NewEditValue on the current selection
        Set focus to the Hex Value edit box
    ENDIF
**/
void CImageEditorDlg::OnSelchangeLstChanged() 
{
    if(inMsg) return;
    UpdateHelpTopic(this,DEF_TableTopic);
    if(working)
    {
        NewEditValue(Lst_Changed->GetCurSel());
        CEdit *p=(CEdit *)Edt_ValueHex;
        p->SetFocus();
        OnSetFocusEdtValueHex();
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnSelchangeLstAddress
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches a click within the Address table column.
PDL:
    IF not in a message handler AND a file is loaded THEN
        CALL NewEditValue on the current selection
        Set focus to the Hex Value edit box
    ENDIF
**/
void CImageEditorDlg::OnSelchangeLstAddress() 
{
    if(inMsg) return;
    UpdateHelpTopic(this,DEF_TableTopic);
    if(working)
    {
        NewEditValue(Lst_Address->GetCurSel());
        CEdit *p=(CEdit *)Edt_ValueHex;
        p->SetFocus();
        OnSetFocusEdtValueHex();
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnSelchangeLstValueHex
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches a click within the Hex Value table column.
PDL:
    IF not in a message handler AND a file is loaded THEN
        CALL NewEditValue on the current selection
        Set focus to the Hex Value edit box
    ENDIF
**/
void CImageEditorDlg::OnSelchangeLstValueHex() 
{
    if(inMsg) return;
    UpdateHelpTopic(this,DEF_TableTopic);
    if(working)
    {
        NewEditValue(Lst_ValueHex->GetCurSel());
        CEdit *p=(CEdit *)Edt_ValueHex;
        p->SetFocus();
        OnSetFocusEdtValueHex();
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnSelchangeLstValueDec
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches a click within the Decimal Value table column.
PDL:
    IF not in a message handler AND a file is loaded THEN
        CALL NewEditValue on the current selection
        Set focus to the Decimal Value edit box
    ENDIF
**/
void CImageEditorDlg::OnSelchangeLstValueDec() 
{
    if(inMsg) return;
    UpdateHelpTopic(this,DEF_TableTopic);
    if(working)
    {
        NewEditValue(Lst_ValueDec->GetCurSel());
        CEdit *p=(CEdit *)Edt_ValueDec;
        p->SetFocus();
        OnSetFocusEdtValueDec();
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnSelchangeLstUtctime
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches a click within the UTC Timestamp table column.
PDL:
    IF not in a message handler AND a file is loaded THEN
        CALL NewEditValue on the current selection
        Set focus to the Decimal Value edit box
    ENDIF
**/
void CImageEditorDlg::OnSelchangeLstUtctime() 
{
    if(inMsg) return;
    UpdateHelpTopic(this,DEF_TableTopic);
    if(working)
    {
        NewEditValue(Lst_UTCTime->GetCurSel());
        CEdit *p=(CEdit *)Edt_ValueDec;
        p->SetFocus();
        OnSetFocusEdtValueDec();
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnBtnApply
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches clicks on Apply button.
PDL:
    IF in a message handler
      OR no file is loaded
      OR the file is in read-only mode
      OR no address is selected THEN
        RETURN
    ENDIF
    
    IF the data in the edit boxes is the same as the old data THEN
        Move the selection down one row, scrolling as necessary
    ELSE
        Update the value of the address
        Update the list boxes and caption
    ENDIF
**/
void CImageEditorDlg::OnBtnApply() 
{
    if(!working || inMsg || bReadOnly){
		return;
	}

    inMsg=true;

//  CString oldValue;
//  DWORD addxval, v;
    const Record *p;

    //Figure out what address the user's editing and save the
    //changes to the appropriate location.
    CString strValueHex;
	Edt_ValueHex->GetWindowText(strValueHex);
    if(strValueHex.GetLength()<1)
    {
        inMsg=false;
        return;
    }
    DWORD v = Doubleword::ParseHex(strValueHex);

	CString strAddress;
	Edt_Address->GetWindowText(strAddress);

	// Shift address back for reading from memory or disk
 //   DWORD addxval = Doubleword::ParseHex(strAddress)<<1;
	DWORD addxval = Doubleword::ParseHex(strAddress);

	p=GetAddx(addxval);
	if(!p)
    {
        inMsg=false;
        return;
    }

    // Move down a row if the data hasn't been changed
    if(p->data == v)
    {
        CRect r;
        CListBox *l=Lst_Changed;
        int nn,qq;
        
        l->GetClientRect(&r);
        nn=r.Height()/l->GetItemHeight(0);
        qq=l->GetCurSel();

        if(qq<nn-1)
            NewEditValue(qq+1);
        else
        {
            DWORD lastValid=(highestAddress+2)-(nn<<1);
            DWORD newaddx=minDisplayed+2;

            if(newaddx>minDisplayed && newaddx<=lastValid)
            {
                DoListing(newaddx);
                if(qq>=0) NewEditValue(qq);
            }
        }

        CWnd *w=GetFocus();
        CRect s,t;
        w->GetWindowRect(&r); 
		Edt_ValueHex->GetWindowRect(&s);
        Edt_ValueDec->GetWindowRect(&t);
        if(r==s || r==t)
            ((CEdit *)w)->SetSel(0,w->GetWindowTextLength());

        inMsg=false;
        return;
    }

    // Change the value...
//     SetAddx(addxval,(WORD)v);
	// Make sure we use the physical address for when updating
	// Only use logical when displaying.
	SetAddx(addxval, (WORD)v);
	
    //...and make sure everything's back where it was.
    v=Lst_Changed->GetCurSel();
    DoListing(minDisplayed);
    inMsg=true;
    Lst_Changed->SetCurSel(v);
    Lst_Address->SetCurSel(v);
    Lst_ValueHex->SetCurSel(v);
    Lst_ValueDec->SetCurSel(v);
    Lst_UTCTime->SetCurSel(v);

    CString s(_T("GOES Image Editor: "));
    s += currentFile + CString(_T(" [MODIFIED]"));
    SetWindowText((LPCTSTR)Ellipsis(s));
    UpdateMenu();
    inMsg=false;
    CWnd *q=GetFocus();
    int n=0;

    if(q){
		n=q->GetDlgCtrlID();
	}

    if(n==IDC_EDT_VALUE_HEX){
        OnSelchangeLstAddress();
	} else if(n==IDC_EDT_VALUE_DEC){
        OnSelchangeLstUtctime();
	}

    UpdateHelpTopic(this,DEF_ApplyTopic);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnChangeEditValueHex
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Corrects the hexadecimal value in the edit box
PDL:
    IF not already in the function AND messages not blocked THEN
        IF no value is selected for editing THEN
            Clear the text in the edit box
            RETURN
        ENDIF

        Validate the hexadecimal value
        Synchronize the decimal edit box with the hexadecimal
    ENDIF
**/
void CImageEditorDlg::OnChangeEdtValueHex() 
{
    static bool recursion=false;
    if(recursion || inMsg) return;
    recursion=true;

    CEdit *edt=Edt_ValueHex;
    if(!Edt_Address->GetWindowTextLength())
    {
        edt->SetWindowText(_T(""));
        recursion=false;
        return;
    }
    Doubleword::ValidateHex(edt,sz_word);

    inMsg=true;
    if(!edt->GetWindowTextLength())
        Edt_ValueDec->SetWindowText(_T(""));
    else
    {
        CString s;
        edt->GetWindowText(s);
        s.Format(_T("%u"),(DWORD)Doubleword::ParseHex(s));

        CEdit *vd=Edt_ValueDec;
        int st,nd;
        vd->GetSel(st,nd);
        vd->SetWindowText(s);
        vd->SetSel(st,nd);
    }
    inMsg=false;
    UpdateHelpTopic(this,DEF_EditTopic);
    recursion=false;
}

///////////////////////////////////////
/**
Function:       OnChangeEdtValueDec
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Corrects the decimal value in the edit box
PDL:
    IF not already in the function AND messages not blocked THEN
        IF no value is selected for editing THEN
            Clear the text in the edit box
            RETURN
        ENDIF

        Validate the decimal value
        Synchronize the hexadecimal edit box with the decimal
    ENDIF
**/
void CImageEditorDlg::OnChangeEdtValueDec() 
{
    static bool recursion=false;
    if(recursion || inMsg) return;
    recursion=true;

    CEdit *edt=Edt_ValueDec;
    if(!Edt_Address->GetWindowTextLength())
    {
        edt->SetWindowText(_T(""));
        recursion=false;
        return;
    }
    Doubleword::ValidateDec(edt,sz_word);

    inMsg=true;
    if(!edt->GetWindowTextLength())
        Edt_ValueHex->SetWindowText(_T(""));
    else
    {
        CString s;
        edt->GetWindowText(s);
        s.Format(_T("%X"),(DWORD)Doubleword::ParseDecimal(s));

        CEdit *vh=Edt_ValueHex;
        int st,nd;
        vh->GetSel(st,nd);
        vh->SetWindowText(s);
        vh->SetSel(st,nd);
    }
    inMsg=false;
    UpdateHelpTopic(this,DEF_EditTopic);
    recursion=false;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnContextMenu
Parameters:     [CImageEditorDlg *this]
                CWnd *wnd
                    Passed by windows; the window in which the user clicked.
                CPoint point
                    The point at which the user clicked.
Returns:        void
Description:    Pops up any context menus handled by the editor.
PDL:
    IF the user clicked the Open button THEN
        Pop up the list of recent files
    ELSEIF the user clicked the Help button THEN
        Pop up the Help menu
    ELSEIF the user clicked in the Go to Address button
           AND a file is open THEN
        Pop up the list of recent addresses
    ELSEIF a file is open AND the user clicked in the Update button THEN
        Pop up the Update menu
    ELSEIF the mouse is within the address/value table
           AND a file is open THEN
        Pop up the context menu for the table
    ELSE
        Let MFC handle the context menu
    ENDIF
**/
void CImageEditorDlg::OnContextMenu(CWnd *wnd,CPoint point)
{
    if(inMsg) return;
    CRect rAVT0,rAVT4;
    CMenu *p;

    //Some of the shortcut buttons have pop-up shortcut menus:
    GetDlgItem(IDC_LST_CHANGED)->GetWindowRect(rAVT0);
    GetDlgItem(IDC_LST_UTCTIME)->GetWindowRect(rAVT4);
    rAVT0.right=rAVT4.right;
    rAVT0.bottom=rAVT4.bottom;

    int id=wnd?wnd->GetDlgCtrlID():0;
    if(id==IDC_BTN_FILEOPEN)
    {
        CRect rOpen;
		wnd->GetWindowRect(&rOpen);
        p=GetMenu();
        p=p->GetSubMenu(0);
        p=p->GetSubMenu(1);
        p->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON,
            rOpen.left,rOpen.bottom-2,this,&rOpen);
        return;
    }
    else if(id==IDC_BTN_CONTEXTHELP)
    {
        CRect rHelp;wnd->GetWindowRect(&rHelp);
        p=GetMenu();
        p=p->GetSubMenu(3);
        p->TrackPopupMenu(TPM_RIGHTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON,
            rHelp.right,rHelp.bottom-2,this,&rHelp);
        return;
    }
    else if(working && id==IDC_BTN_GOTOADDRESS)
    {
        CRect rGotoAddx;wnd->GetWindowRect(&rGotoAddx);
        p=GetMenu();
        p=p->GetSubMenu(1);
        p=p->GetSubMenu(2);
        p->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON,
            rGotoAddx.left,rGotoAddx.bottom-2,this,&rGotoAddx);
        return;
    }
    else if(working && id==IDC_BTN_REFRESH)
    {
        CRect rUpdate;wnd->GetWindowRect(&rUpdate);
        p=GetMenu();
        p=p->GetSubMenu(2);
        p->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON,
            rUpdate.left,rUpdate.bottom-2,this,&rUpdate);
        return;
    }
    else if(working && rAVT0.PtInRect(point))
    {
        CMenu m,*n;
        m.LoadMenu(IDR_CONTEXTMENU);
        n=m.GetSubMenu(0);
        if(bReadOnly || !changed)
        {
            n->EnableMenuItem(4,MF_BYPOSITION|MF_GRAYED);
            n->EnableMenuItem(5,MF_BYPOSITION|MF_GRAYED);
        }
        else
        {
            n->EnableMenuItem(4,MF_BYPOSITION|MF_ENABLED);
            n->EnableMenuItem(5,MF_BYPOSITION|MF_ENABLED);
        }
        n->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON,
            point.x,point.y,this,NULL);
        return;
    }
    DefWindowProc(WM_CONTEXTMENU,(WPARAM)wnd->GetSafeHwnd(),(LPARAM)((point.y<<16)|(WORD)point.x));
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnSetCursor
Parameters:     [CImageEditorDlg *this]
                CWnd *pWnd
                    Pointer to the window that contains the cursor.
                UINT nHitTest
                    The code for the area the cursor is over.
                UINT message
                    The mouse message number.
Returns:        BOOL
                    TRUE to end processing of the message.
Description:    Called by Windows when the cursor may need to change.
PDL:
    IF currently in context help THEN
        Change the cursor to the context help cursor
        RETURN
    ENDIF

    IF the cursor is over an edit box THEN
        Change the cursor to an I-beam
    ELSEIF the cursor is over the resizer THEN
        Change the cursor to a N/S resize
    ELSE
        Change the cursor to a normal cursor
    ENDIF

**/
BOOL CImageEditorDlg::OnSetCursor(CWnd *,UINT ,UINT)
{
    if(inContextHelp)
    {
       ::SetCursor(cursHelp);
        return TRUE;
    }
    CPoint p;
    ::GetCursorPos(&p);

    CRect a,b,c,d;
    Edt_Address->GetWindowRect(&a);
    Edt_ValueHex->GetWindowRect(&b);
    Edt_ValueDec->GetWindowRect(&c);
    Edt_UTCTime->GetWindowRect(&d);
    if(a.PtInRect(p) || b.PtInRect(p) || c.PtInRect(p) || d.PtInRect(p))
        ::SetCursor(cursText);
    else
    {
        ScreenToClient(&p);
        if(m_cSizer.PtInRect(p))
            ::SetCursor(cursResize);

        else
            ::SetCursor(cursNorm);
    }
    return TRUE;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnContextHelp
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called from the Help button or the Help|On Item command.
PDL:
    Capture the mouse
    Set the cursor to the help cursor
    Save the caption text and change it to indicate the program state
    Begin the context help capture loop
**/
void CImageEditorDlg::OnContextHelp()
{
    SetCapture();
    
    ::SetCursor(::AfxGetApp()->LoadStandardCursor(IDC_HELP));
    inContextHelp=true;
    GetWindowText(_owt);
    SetWindowText(_T("Left-click on an item for help, right-click to cancel..."));
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnBtnTop
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called when the To-Top button is clicked.
PDL:
    IF not copying a file AND a file is loaded THEN
        Scroll to the lowest address
    ENDIF
**/
void CImageEditorDlg::OnBtnTop() 
{
    if(inMsg || !working) return;
    ScrollTo(lowestAddress);
    UpdateHelpTopic(this,DEF_ScrollTopic);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnBtnBot
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called when the To-Bottom button is clicked.
PDL:
    IF not copying a file AND a file is loaded THEN
        Scroll to the highest possible address
    ENDIF
**/
void CImageEditorDlg::OnBtnBot() 
{
    if(inMsg || !working) return;
    ScrollTo(highestAddress);   //<-(This address gets clipped in ScrollTo.)
    UpdateHelpTopic(this,DEF_ScrollTopic);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMouseWheel
Parameters:     [CImageEditorDlg *this]
                UINT flags
                    Mouse/keyboard status flags.
                short delta
                    Amount the wheel has turned.
                CPoint point
                    Location of the cursor.
Returns:        BOOL
                    TRUE because mouse scrolling is supported.
Description:    Scrolls the table when the mouse wheel is turned.
PDL:
    IF not copying a file AND a file is open THEN
        Make sure the cache is fully loaded
        Update the minimum address
        Scroll to the new address
    ENDIF
**/
BOOL CImageEditorDlg::OnMouseWheel(UINT flags,short delta,CPoint point)
{
    if(inMsg || !working) {
		return TRUE;
	}

	// Don't need to convert address.
	// DoListing will convert addresses when displaying it.
	// Addresses are kept in physical format until displayed.

    GetAddx(minDisplayed);
    //Note that because of unsigned arithmetic, overflow wraps values
    //around to zero if they're too high and to ~0 if they're too low.
    DWORD oldaddx=minDisplayed;
    DWORD newaddx=oldaddx-delta/60;
    if((delta > 0 && newaddx > oldaddx) || (delta < 0 && newaddx < oldaddx))
        return TRUE;

    ScrollTo(newaddx);
    return TRUE;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnVScroll
Parameters:     [CImageEditorDlg *this]
                UINT nSBCode
                    Scroll event code.
                UINT nPos
                    Position within the scrollbar, if applicable.
                CScrollBar *pScrollBar
                    Pointer to the relevant scroll bar.
Returns:        void
Description:    Catches any vertical scrolling events.
PDL:
    IF no file is open OR already in this function OR copying a file
      OR the scrollbar isn't valid THEN
        RETURN
    ENDIF

    IF the scrollbar is valid THEN
        Calculate the last possible lowest-displayed address
        Calculate the addresses-per-step value
        DOCASE the scroll event code
            CASE (scroll to bottom)
                Set the new address to the last possible address
            CASE (scroll one line down)
                Move down by two address rows
            CASE (scroll one line up)
                Move up by two address rows
            CASE (scroll one page down)
                Move down by half of the number of visible addresses
            CASE (scroll one page up)
                Move up by half of the number of visible addresses
            CASE (the thumbnail is being dragged)
              OR (the thumbnail has been dragged and released)
                Set the address to the adjusted step position
            CASE (scroll to top)
                Set the address to the lowest address
        ENDDO
        Ensure that the address is within the valid range
        Update the list boxes
        Update the scroll bar
        Reselect any selected rows if visible
    ENDIF
**/
void CImageEditorDlg::OnVScroll(UINT nSBCode,UINT nPos,CScrollBar *pScrollBar) 
{
    static bool here=false;
    DWORD addx=minDisplayed,lastValid,delta;
    DWORD nnPos;
    DWORD newaddx=addx;
    double conversion;
    int num;
    CRect rect;
	bool bValidAddr = true;
	bool bRecMod    = false;


    if(!working || here || inMsg) {
		return;
	}

    if(pScrollBar)
    {
        Lst_Changed->GetClientRect(&rect);
        num=(rect.Height())/Lst_Changed->GetItemHeight(0);

        // lastValid=(highestAddress+2)-(num<<1);
		lastValid=(highestAddress+2)-(num);
		
		if(lastValid > highestAddress){
			lastValid=highestAddress;
		}
        // delta=(num+1)&~1;
		delta = (num+1);


        conversion= double(lastValid-lowestAddress)/32767.0;

        inMsg=true;
        switch(nSBCode)
        {
        case SB_BOTTOM:
            newaddx = lastValid - 2;
            break;

        case SB_LINEDOWN:
            newaddx += 4;
            //(newaddx<addx if it wrapped)
            if(newaddx < addx || newaddx > lastValid){
                newaddx=lastValid;
			}

			// Step until find a valid address
			GetAddx(newaddx, bValidAddr, bRecMod);
			while (!bValidAddr && (newaddx <= highestAddress) && (newaddx >= lowestAddress)){
  				GetAddx(newaddx, bValidAddr, bRecMod);
				newaddx++;
//				TRACE(_T("In While loop newaddx = %u\n"), newaddx);
			}

//			TRACE(_T("SB_LINEDOWN: newaddx = %u\n"), newaddx);
            break;

        case SB_LINEUP:
            newaddx -= 4;
            if(newaddx>addx || addx<lowestAddress) {
                newaddx=lowestAddress;
			}

			// Step until find a valid address
			GetAddx(newaddx, bValidAddr, bRecMod);
			while (!bValidAddr && (newaddx <= highestAddress) && (newaddx >= lowestAddress)){
  				GetAddx(newaddx, bValidAddr, bRecMod);
				newaddx--;
//				TRACE(_T("In While loop newaddx = %u\n"), newaddx);
			}

//			TRACE(_T("SB_LINEDOWN: newaddx = %u\n"), newaddx);
            break;

        case SB_PAGEDOWN:
            newaddx+=delta;
            if(newaddx<addx || newaddx>lastValid)
                newaddx=lastValid;
            break;

        case SB_PAGEUP:
            newaddx-=delta;
            if(newaddx>addx || newaddx<lowestAddress)
                newaddx=lowestAddress;
            break;

        case SB_THUMBTRACK:
        case SB_THUMBPOSITION:
            nnPos=(DWORD)(double(nPos)*conversion);
            newaddx=lowestAddress+nnPos;
            m_bDragScrolling=(nSBCode==SB_THUMBTRACK);
            break;

        case SB_TOP:
            newaddx=lowestAddress;
            break;
        }

        if(newaddx>lastValid)
            newaddx=lastValid;
        if(newaddx<lowestAddress)
            newaddx=lowestAddress;
        if(newaddx==addx)
        {
            inMsg=false;
            return;
        }

		// Don't need to convert address.
		// DoListing will convert addresses when displaying it.
		// Addresses are kept in physical format until displayed.
        addx=newaddx;
        if(nSBCode!=SB_THUMBTRACK && nSBCode!=SB_PAGEDOWN && nSBCode!=SB_PAGEUP)
            GetAddx(addx);

        DoListing(addx);
        here=true;

        DWORD curAddx=minDisplayed-lowestAddress;
        pScrollBar->SetScrollPos((DWORD)(double(curAddx)/conversion));
        here=false;
        
        CString s;
        Edt_Address->GetWindowText(s);
        if(s.GetLength()>=1)
        {
            // delta=Doubleword::ParseHex(s)<<1;
			delta=Doubleword::ParseHex(s);

            // if(delta >= minDisplayed && delta < (minDisplayed + (num<<1) - 1))
            if(delta >= minDisplayed && delta < (minDisplayed + (num - 1)))
            {
                delta -= minDisplayed;
			//	delta >>=1;
                Lst_Changed->SetCurSel(delta);
                Lst_Address->SetCurSel(delta);
                Lst_ValueHex->SetCurSel(delta);
                Lst_ValueDec->SetCurSel(delta);
                Lst_UTCTime->SetCurSel(delta);
            }
        }
        inMsg=false;
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnSetFocusEdtValueHex
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches focus to the hex. value edit box and selects all of the
                text therein.
PDL:
    IF not copying a file AND a file is open THEN
        Select all text in the hexadecimal edit box
    ENDIF
**/
void CImageEditorDlg::OnSetFocusEdtValueHex() 
{
    if(inMsg || !working) return;
    CEdit *p=(CEdit *)GetDlgItem(IDC_EDT_VALUE_HEX);
    p->SetSel(0,p->GetWindowTextLength(),TRUE);
    UpdateHelpTopic(this,DEF_EditTopic);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnSetFocusEdtValueDec
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches focus to the decimal value edit box and selects all of
                the text therein.
PDL:
    IF not copying a file AND a file is open THEN
        Select all text in the decimal edit box
    ENDIF
**/
void CImageEditorDlg::OnSetFocusEdtValueDec() 
{
    if(inMsg || !working) return;
    CEdit *p=(CEdit *)GetDlgItem(IDC_EDT_VALUE_DEC);
    p->SetSel(0,p->GetWindowTextLength(),TRUE);
    UpdateHelpTopic(this,DEF_EditTopic);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnLButtonDown
Parameters:     [CImageEditorDlg *this]
                UINT nFlags
                    Mouse/keyboard flags.
                CPoint point
                    Cursor position.
Returns:        void
Description:    Called when the left mouse button's switch is closed.
PDL:
    IF copying a file OR in a message handler THEN
        RETURN
    ENDIF

    IF not in the context help capture loop THEN
        IF a file is open AND the file is read/write
          AND the mouse is in the Apply button rectangle THEN
            Capture the mouse
            Raise the Apply-down flag
            Redraw the button
        ELSEIF a file is open AND the mouse is in the To-Top button THEN
            Capture the mouse
            Raise the To-Top-down flag
            Redraw the button
        ELSEIF a file is open AND the mouse is in the To-Bottom button THEN
            Capture the mouse
            Raise the To-Bottom-down flag
            Redraw the button
        ELSEIF the mouse is in the resizer button AND the sizer isn't being
          right-dragged THEN
            Capture the mouse
            Raise the sizer-left-button-down flag
            Redraw the button
        ELSE
            Superclass the function
        ENDIF
    ELSE
        Raise the context-help-left-button-down flag
    ENDIF
**/
void CImageEditorDlg::OnLButtonDown(UINT nFlags,CPoint point)
{
    if(inMsg) return;
    if(!inContextHelp)
    {
        if(working && !bReadOnly && m_bApply.PtInRect(point))
        {
            SetCapture();
            m_bApplyDown=true;
            DrawBtnApply(true);
        }
        else if(working && m_bTop.PtInRect(point))
        {
            SetCapture();
            m_bTopDown=true;
            DrawBtnsTopBot(true,true);
        }
        else if(working && m_bBot.PtInRect(point))
        {
            SetCapture();
            m_bBotDown=true;
            DrawBtnsTopBot(false,true);
        }
        else if(m_cSizer.PtInRect(point) && !m_bSizerDownR)
        {
            SetCapture();
            m_bSizerDown=true;
            DrawSizer(true);
        }
        else
            super::OnLButtonDown(nFlags, point);
    }
    else
        chlbDown=true;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnLButtonUp
Parameters:     [CImageEditorDlg *this]
                UINT nFlags
                    Keyboard/mouse flags.
                CPoint point
                    Cursor position.
Returns:        void
Description:    Called when the left mouse button's switch is opened
PDL:
    IF copying a file OR handling a message THEN
        RETURN
    ENDIF

    IF the Apply button is being held down THEN
        Release mouse capture
        Redraw the button
        IF the mouse is still in the button THEN
            CALL OnBtnApply
        ENDIF
    ELSEIF the To-Top button is being held down THEN
        Release mouse capture
        Redraw the button
        IF the mouse is still in the button THEN
            CALL OnBtnTop
        ENDIF
    ELSEIF the To-Bottom button is being held down THEN
        Release mouse capture
        Redraw the button
        IF the mouse is still in the button THEN
            CALL OnBtnBot
        ENDIF
    ELSEIF the sizer is being held down with the left button THEN
        Release mouse capture
        Synchronize everything's size
        Repaint the window
        IF a file is open THEN
            Reselect any lost rows of the table
        ENDIF
    ELSEIF in the context help loop and the left button was pressed THEN
        Pop up context help for the object under the mouse
    ENDIF
    Superclass the function
**/
void CImageEditorDlg::OnLButtonUp(UINT nFlags,CPoint point) 
{
    if(inMsg) return;
    if(m_bApplyDown)
    {
        ReleaseCapture();
        DrawBtnApply(m_bApplyDown=false);
        if(m_bApply.PtInRect(point))
            OnBtnApply();
    }
    else if(m_bTopDown)
    {
        ReleaseCapture();
        DrawBtnsTopBot(true,m_bTopDown=false);
        if(m_bTop.PtInRect(point))
            OnBtnTop();
    }
    else if(m_bBotDown)
    {
        ReleaseCapture();
        DrawBtnsTopBot(false,m_bBotDown=false);
        if(m_bBot.PtInRect(point))
            OnBtnBot();
    }
    else if(m_bSizerDown)
    {
        ReleaseCapture();
        ResizeElem();
        DrawSizer(m_bSizerDown=false);
        DrawBtnApply(false);
        DrawBtnsTopBot(false,false);
        DrawBtnsTopBot(true,false);
        if(working)
        {
            int n=Lst_Address->GetCurSel();
            DoListing(minDisplayed);
            if(n>=0) NewEditValue(n);
        }
        UpdateHelpTopic(this,DEF_SizerTopic);
    }
    else if(inContextHelp && chlbDown)
    {
        CRect r;
        ::GetWindowRect(::GetDesktopWindow(),&r);
        ::ClipCursor(&r);
        ReleaseCapture();
        inContextHelp=chlbDown=false;

        SetWindowText(_owt);
        SetCursor(::AfxGetApp()->LoadStandardCursor(IDC_ARROW));

        GetClientRect(&r);
        if(!r.PtInRect(point))
        {
            DisplayHelpTopic(this,helpDefRef);
            return;
        }

        int i;
        for(i=0;helpRefs[i].ctlID || helpRefs[i].URI;i++)
        {
            if(!helpRefs[i].ctlID)
            {
                if(m_cSizer.PtInRect(point))
                    break;
            }
            else
            {
                GetDlgItem(helpRefs[i].ctlID)->GetWindowRect(&r);
                ScreenToClient(&r);
                if(r.PtInRect(point))
                    break;
            }
        }
        if(helpRefs[i].ctlID || helpRefs[i].URI)
            DisplayHelpTopic(this,helpRefs[i].URI);
        else
            DisplayHelpTopic(this,helpDefRef);
        return;
    }
	super::OnLButtonUp(nFlags, point);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMButtonDown
Parameters:     [CImageEditorDlg *this]
                UINT nFlags
                    Keyboard/mouse flags.
                CPoint point
                    Cursor location.
Returns:        void
Description:    Called when the middle mouse button's switch is closed.
PDL:
    IF a file is open THEN
        Capture the mouse
    ENDIF
**/
void CImageEditorDlg::OnMButtonDown(UINT nFlags,CPoint point)
{
    if(working)
        SetCapture();
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMButtonUp
Parameters:     [CImageEditorDlg *this]
                UINT nFlags
                    Keyboard/mouse flags.
                CPoint point
                    Cursor location.
Returns:        void
Description:    Called when the middle mouse button's switch is opened.
PDL:
    IF a file is open THEN
        Release mouse capture
    ENDIF
**/
void CImageEditorDlg::OnMButtonUp(UINT nFlags,CPoint point)
{
    if(working)
        ReleaseCapture();
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnLButtonDblClk
Parameters:     [CImageEditorDlg *this]
                UINT nFlags
                    Keyboard/mouse flags.
                CPoint point
                    Cursor location.
Returns:        void
Description:    Called when the left mouse button is double-clicked.
PDL:
    IF not copying a file THEN
        IF the mouse is in an internally-handled button THEN
            CALL OnLButtonDown
        ELSE
            Superclass the function
        ENDIF
    ENDIF
**/
void CImageEditorDlg::OnLButtonDblClk(UINT nFlags,CPoint point) 
{
    if(inMsg) return;
    if(m_bApply.PtInRect(point) || m_bTop.PtInRect(point)
        || m_bBot.PtInRect(point))
        OnLButtonDown(nFlags,point);
    else
        super::OnLButtonDblClk(nFlags, point);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnRButtonDown
Parameters:     [CImageEditorDlg *this]
                UINT nFlags
                    Keyboard/mouse flags
                CPoint point
                    Cursor location
Returns:        void
Description:    Called when the right mouse button's switch is closed.
PDL:
    IF not copying a file AND not in context help THEN
        IF the mouse is in the sizer AND the sizer isn't being
          left-dragged THEN
            Capture the mouse
            Raise the sizer-right-button-down flag
            Redraw the sizer
        ENDIF
        Superclass the function
    ENDIF
**/
void CImageEditorDlg::OnRButtonDown(UINT nFlags,CPoint point) 
{
    if(inMsg || inContextHelp) return;
    if(m_cSizer.PtInRect(point) && !m_bSizerDown)
    {
        SetCapture();
        m_bSizerDownR=true;
        DrawSizer(true);        
    }

    super::OnRButtonDown(nFlags, point);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnRButtonUp
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called when the right mouse button's switch is opened
PDL:
    IF no file is being copied THEN
        IF the contest help capture loop is active THEN
            Cancel the loop
        ELSEIF the sizer was being right-dragged THEN
            Release mouse capture
            Redraw the button
        ENDIF
        Superclass the function
    ENDIF
**/
void CImageEditorDlg::OnRButtonUp(UINT nFlags,CPoint point) 
{
    if(inMsg) return;
    if(inContextHelp)
    {
        ReleaseCapture();
        SetCursor(cursNorm);
        inContextHelp=chlbDown=false;
        SetWindowText(_owt);
    }
    else if(m_bSizerDownR)
    {
        ReleaseCapture();
        DrawSizer(m_bSizerDownR=false);
    }
	super::OnRButtonUp(nFlags, point);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMouseMove
Parameters:     [CImageEditorDlg *this]
                UINT nFlags
                    Keyboard/mouse status flags.
                CPoint point
                    Cursor location
Returns:        void
Description:    Called when the mouse cursor moves.
PDL:
    IF copying a file THEN
        RETURN
    ENDIF

    IF the mouse hasn't actually moved THEN
        RETURN
    ENDIF
    
    IF the Apply button is down AND the mouse-in-Apply flag
      state differs from the mouse's position THEN
        Redraw the Apply button
    ENDIF
    Update the mouse-in-Apply flag
        
    IF the To-Top button is down AND the mouse-in-To-Top flag
      state differs from the mouse's position THEN
        Redraw the To-Top button
    ENDIF
    Update the mouse-in-To-Top flag

    IF the To-Bottom button is down AND the mouse-in-To-Bottom flag
      state differs from the mouse's position THEN
        Redraw the To-Bottom button
    ENDIF
    Update the mouse-in-To-Bottom flag

    IF the sizer is being left-dragged THEN
        Re-set the cursor to the N/S resize
        IF the mouse hasn't moved vertically THEN
            RETURN
        ENDIF

        Clip the new window size to 370 pixels, minimum.
        Resize the window
        Resize the elements within the window
        Redraw everything
        Refresh the list boxes
    ELSEIF the sizer is being right-dragged THEN
        IF the mouse hasn't moved vertically THEN
            RETURN
        ENDIF

        Reposition the window
    ENDIF
    Save the current point as the old point
**/
void CImageEditorDlg::OnMouseMove(UINT nFlags,CPoint point) 
{
    if(inMsg) return;

    static CPoint oldPoint(-32768,-32768);
    bool inApply=!!m_bApply.PtInRect(point);
    bool inTop=!!m_bTop.PtInRect(point);
    bool inBot=!!m_bBot.PtInRect(point);
    bool inSizer=!!m_cSizer.PtInRect(point);

    if(point==oldPoint) return;

    if(m_bApplyDown && inApply!=m_bInApply)
        DrawBtnApply(inApply);
    m_bInApply=inApply;

    if(m_bTopDown && inTop!=m_bInTop)
        DrawBtnsTopBot(true,inTop);
    m_bInTop=inTop;

    if(m_bBotDown && inBot!=m_bInBot)
        DrawBtnsTopBot(false,inBot);
    m_bInBot=inBot;

    if(m_bSizerDown)
    {
        ::SetCursor(cursResize);
        if(point.y==oldPoint.y) return;
        ClientToScreen(&point);

        CRect wr;
        GetWindowRect(wr);
        point.y+=12;
        if(point.y<wr.top+370)
            point.y=wr.top+370;
        if(point.y!=wr.bottom)
        {
            SetWindowPos(NULL,0,0,wr.Width(),point.y-wr.top,
                SWP_NOACTIVATE|SWP_NOMOVE|SWP_NOZORDER);
            ResizeElem();
            DrawSizer(true);
            DrawBtnApply(false);
            DrawBtnsTopBot(false,false);
            DrawBtnsTopBot(true,false);
            if(working)
                DoListing(minDisplayed);
        }
    }
    else if(m_bSizerDownR)
    {
        ClientToScreen(&point);
        if(point.y==oldPoint.y) return;

        point.y+=12;
        CRect wr;
        GetWindowRect(wr);
        int h=wr.Height();

        wr.bottom=point.y+1;
        wr.top=point.y-h;
        SetWindowPos(NULL,wr.left,point.y-h,0,0,
            SWP_NOACTIVATE|SWP_NOSIZE|SWP_NOZORDER);
    }
    oldPoint=point;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnCmdMsg
Parameters:     [CImageEditorDlg *this]
                UINT nID
                    Command message ID.
                int nCode
                    Notification code.
                void *pExtra
                    Extra data for notification.
                AFX_CMDHANDLERINFO *pHandlerInfo
                    Should be NULL.
Returns:        BOOL
                    TRUE iff the message was handled.
Description:    Catches command messages to implement Open/Go to Recent.
PDL:
    IF copying a file OR within a message handler THEN
        RETURN FALSE
    ENDIF

    IF the message is a File|Open Recent command THEN
        IF the message is on its second round THEN
            RETURN TRUE
        ENDIF
        Get the filename for the message
        IF the filename begins with '*' THEN
            Load the file read-only
        ELSE
            Load the file read/write
        ENDIF
        RETURN TRUE
    ELSEIF the message is a Go|Go to Recent command THEN
        IF the message is on its second round THEN
            RETURN TRUE
        ENDIF
        Get the address for the message
        Scroll to and select the new address
        RETURN TRUE
    ENDIF
    Superclass the function
**/
BOOL CImageEditorDlg::OnCmdMsg(UINT nID,int nCode,void *pExtra,AFX_CMDHANDLERINFO *pHandlerInfo) 
{
    static bool echo=false;
    if(inMsg) return FALSE;

#if DEF_NumOpenEntries!=8
#   error Changed the open entry count without updating CImageEditorDlg::OnCmdMsg.
#endif

    if(nID>=ID_FILE_OPENRECENT0 && nID<=ID_FILE_OPENRECENT7)
    {
        if(echo){ 
			echo=false;
			return TRUE;
		}
        echo=true;

		// File already openned, User must close current file befroe openning.
		if (working){
			MessageBox(_T("A file is currently open; Close before openning another!"),
				_T("Warning"), MB_OK|MB_ICONWARNING);
			return TRUE;
		}

        nID -= ID_FILE_OPENRECENT0;
        if(openEntries[nID])
        {
            CString s(openEntries[nID]);
            if(s[0]=='*')
            {
                s=(1+(LPCTSTR)s);
                bReadOnly=true;
            }
            else
                bReadOnly=false;
            DoLoad(s);
            UpdateMenu();
            return TRUE;
        }
    }

#if DEF_NumAddressEntries!=16
#   error Changed the address entries without updating CImageEditorDlg::OnCmdMsg.
#endif

    else if(nID>=ID_VIEW_GOTORECENT0 && nID<=ID_VIEW_GOTORECENTF)
    {
        if(echo) {echo=false;return TRUE;}
        echo=true;

        nID-=ID_VIEW_GOTORECENT0;
        if(~addressEntries[nID])
        {

			// Don't need to convert address.
			// DoListing will convert addresses when displaying it.
			// Addresses are kept in physical format until displayed.

            DWORD newAddx=addressEntries[nID];

		//  if(!GetAddx(newAddx<<1))
            if(!GetAddx(newAddx))
            {
                MessageBox(_T("Invalid address."),_T("Error"),MB_OK|MB_ICONWARNING);
                return TRUE;
            }
 
            ScrollTo(newAddx);
            CString s;
            s.Format(_T("%08.8X"),newAddx);
            NewEditValue(Lst_Address->FindString(0,s));
            AddAddressEntry(newAddx);
            UpdateMenu();
            return TRUE;
        }
    }

    TRACE(_T("  (ignored)\n"));
    return super::OnCmdMsg(nID,nCode,pExtra,pHandlerInfo);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_FileOpen
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Handles the File|Open command to load a new file.
PDL:
    IF not copying a file THEN
        Get the filename from the user
        Set read-only mode if necessary
        CALL DoLoad on the file
        Synchronize the menu
    ENDIF
**/
void CImageEditorDlg::OnMenu_FileOpen() 
{
    if(inMsg) return;

	// File already openned, User must close current file befroe openning.
	if (working){
		MessageBox(_T("A file is currently open; Close before openning another!"),
			_T("Warning"), MB_OK|MB_ICONWARNING);
		return;
	}

    UpdateHelpTopic(this,DEF_FileMenuTopic _T("#open"));
    CFileDialog dlg(TRUE,NULL,NULL,
        OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST|OFN_SHAREAWARE,
        _T("Image file (*.img)|*.img|All files (*.*)|*.*||"),
           this);
  
    if(dlg.DoModal()==IDOK)
    {
        CString cf=currentFile;
        bReadOnly=!!dlg.GetReadOnlyPref();
        DoLoad(dlg.GetPathName());
        UpdateMenu();
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_FileRevert
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches the File|Revert command
PDL:
    IF copying a file OR no file is open OR no changes have been made THEN
        RETURN
    ENDIF

    IF the user wants to lose changes THEN
        Clear changes
        Invalidate and reload the cache
        Synchronize the menu
        Reselect the table row
    ENDIF
**/
void CImageEditorDlg::OnMenu_FileRevert() 
{
	if(inMsg || !working || !changed) return;
    UpdateHelpTopic(this,DEF_FileMenuTopic _T("#revert"));

    int n=MessageBox(_T("Lose changes?"),_T("Revert"),MB_YESNO|MB_ICONQUESTION);
    if(n!=IDNO)
    {    
        ClearChanges();
        InvdCache();
        UpdateMenu();
        GetAddx(minDisplayed);
        n=Lst_Address->GetCurSel();
        DoListing(minDisplayed);
        if(n>=0)
            NewEditValue(n);
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_FileSave
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches the File|Save command.
PDL:
    IF not copying a file AND a file is open AND changes have been made THEN
        Save the changes to the file
        Synchronize the menu
    ENDIF
**/
void CImageEditorDlg::OnMenu_FileSave() 
{
    if(inMsg || !working || !changed) return;
    UpdateHelpTopic(this,DEF_FileMenuTopic _T("#save"));

    DoSave(currentFile);
    UpdateMenu();
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_FileSaveCopy
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches the File|Save As command
PDL:
    IF not copying a file AND a file is open THEN
        Request an output filename
        CALL DoSave to copy to the new file
    ENDIF
**/
void CImageEditorDlg::OnMenu_FileSaveCopy() 
{
    if(inMsg || !working) return;
    UpdateHelpTopic(this,DEF_FileMenuTopic _T("#saveAs"));

    CFileDialog dlg(FALSE,_T("img"),NULL,
        OFN_HIDEREADONLY|OFN_SHAREAWARE|OFN_OVERWRITEPROMPT,
        _T("Image file (*.img)|*.img|All files (*.*)|*.*||"),this);

    if(dlg.DoModal()==IDOK)
        DoSave(dlg.GetPathName(),true);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_FilePrint
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches the File|Print command.
PDL:
    IF a file is open AND not copying a file THEN
        Pop up the [Print] dialog
    ENDIF
**/
void CImageEditorDlg::OnMenu_FilePrint()
{
    if(inMsg || !working)
        return;

    UpdateHelpTopic(this,helpDefRef);
    CPrintAddxDialog dlg(this);
    dlg.DoModal();
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_FileExport
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Pops up the Export dialog.
PDL:
    IF not copying a file AND a file is open THEN
        Display the [Export] dialog
    ENDIF
**/
void CImageEditorDlg::OnMenu_FileExport() 
{
    if(inMsg || !working) return;

    CExportDialog dlg(this);
    dlg.DoModal();

    UpdateHelpTopic(this,helpDefRef);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_FileClose
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called when File|Close is selected to close the file.
PDL:
    IF not copying a file AND a file is open THEN
        IF changes were made THEN
            IF the user wants to save changes THEN
                Save changes
            ELSEIF the user wants to cancel THEN
                RETURN
            ENDIF
        ENDIF
        Reset the filename to ""
        Clear any changes made
        Clear the recent address entries
        Flush and free the cache
        Clear the list boxes
        Synchronize the menu
        Update the window text
        Close the current file
        Redraw the window
    ENDIF
**/
void CImageEditorDlg::OnMenu_FileClose() 
{
    if(inMsg || !working)
        return;

    //Close the working file and free up the cache.
    if(changed)
    {
        int n=MessageBox(_T("Save changes before closing?"),_T("Save changes"),
            MB_YESNOCANCEL|MB_ICONQUESTION);
        if(n==IDCANCEL)
            return;
        if(n==IDYES)
            DoSave(currentFile);
    }
    currentFile.Empty();
    working=false;
    ClearChanges();
//#if 0
    for(int i=0;i<DEF_NumAddressEntries;i++)// see note <1> at file end
        addressEntries[i]=(unsigned)~0;
//#endif
    InvdCache();
    lowestAddress=highestAddress=(unsigned)~0l;
    if(m_Loaded)
    {
        delete[] m_Loaded;
        m_Loaded=0;
    }
    m_CacheSize=0;

    inMsg=true;
    Lst_Changed->ResetContent();
    Lst_Address->ResetContent();
    Lst_ValueHex->ResetContent();
    Lst_ValueDec->ResetContent();
    Lst_UTCTime->ResetContent();
    Edt_Address->SetWindowText(_T(""));
    Edt_ValueHex->SetWindowText(_T(""));
    Edt_ValueDec->SetWindowText(_T(""));
    Edt_UTCTime->SetWindowText(_T(""));
    Scr_Address->EnableWindow(FALSE);
    UpdateMenu();
    SetWindowText(_T("GOES Image Editor"));
    inMsg=false;
    if(curIO) fclose(curIO);
    curIO=0;
    DrawBtnApply(m_bApplyDown);
    DrawBtnsTopBot(true,m_bTopDown);
    DrawBtnsTopBot(false,m_bTopDown);
    UpdateHelpTopic(this,DEF_FileMenuTopic _T("#close"));
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_ViewGotoaddress
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called by the Co|Go to Address command
PDL:
    IF copying a file OR no file is open THEN
        RETURN
    ENDIF
    Request an address from the user
    Round the address to a multiple of two
    IF the address is out of range THEN
        Notify the user
        RETURN
    ENDIF

    IF the user wants the current address added to the Go|Go to Recent
       menu THEN
        Add the address entry
    ENDIF

    Scroll the address into view and select it
**/
void CImageEditorDlg::OnMenu_ViewGotoaddress() 
{
    if(inMsg || !working){
		return;
	}
    
    CGotoAddressDlg dlg(this);
    int result=dlg.DoModal();

    if(result==IDOK)
    {
//		DWORD newAddx=dlg.GetAddress()<<1;
		DWORD dwNewAddx=dlg.GetAddress();

		if ((dwNewAddx < lowestAddress ) || (dwNewAddx > highestAddress)){
			::MessageBox(m_hWnd,_T("Address out of range."),_T("Error"),MB_OK|MB_ICONWARNING);
		    return;
		}

		bool bValidAddr = true;
		bool bRecMod    = false;

		const Record *cur = GetAddx(dwNewAddx, bValidAddr, bRecMod);
		if(NULL == cur) {
			::MessageBox(m_hWnd,_T("Address out of range."),_T("Error"),MB_OK|MB_ICONWARNING);
		   return;
		} else if (!bValidAddr){
			::MessageBox(m_hWnd,_T("Invalid address."),_T("Error"),MB_OK|MB_ICONWARNING);
		    return;
		}

         if(dlg.WantsRecentAdded()){
//            AddAddressEntry(minDisplayed>>1);
			AddAddressEntry(minDisplayed);
		}

  //      AddAddressEntry(newAddx>>1);
		AddAddressEntry(dwNewAddx);
		UpdateMenu();

        CString s;
 //     s.Format(_T("%08.8X"),dwNewAddx>>1);
 //		s.Format(_T("%08.8X"),dwNewAddx);	

		if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
		  	|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){

			s.Format(_T("%05.5X"),dwNewAddx);	
 	  	}  else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 
  
			s.Format(_T("%08.8X"),dwNewAddx);	
	  	}

        inMsg=true;
		Edt_Address->SetWindowText((LPCTSTR)s);
		inMsg=false;
        ScrollTo(dwNewAddx);
        NewEditValue(Lst_Address->FindString(0,s));
    }
    UpdateHelpTopic(this,helpDefRef);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_ViewGotoHighlighted
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called to catch the Go|Go to Highlighted command.
PDL:
    IF not copying a file AND a file is open THEN
        IF the address isn't in view THEN
            Scroll the address into view
        ENDIF
    ENDIF
**/
void CImageEditorDlg::OnMenu_ViewGotoHighlighted()
{
    if(inMsg || !working)
        return;

    CString s;
    DWORD addx;
    Edt_Address->GetWindowText(s);
    if(s.GetLength()<1)
    {
        MessageBox(_T("No value is highlighted."),_T("Error"),MB_OK|MB_ICONWARNING);
        return;
    }

   //  addx=Doubleword::ParseHex(s)<<1;
	addx=Doubleword::ParseHex(s);
    ScrollTo(addx);
    UpdateHelpTopic(this,DEF_GoMenuTopic _T("#highlight"));
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_ViewGotoNextCh
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called to catch the Go|Go to Next Change command.
PDL:
    IF copying a file OR no file is open THEN
        RETURN
    ENDIF
    IF no changes have been made THEN
        Notify the user
        RETURN
    ENDIF

    IF a row is selected THEN
        Use that as the current address
    ELSE
        Use the lowest address on-screen as the current address
    ENDIF

    IF the current address has been changed THEN
        IF there is an address after this that has been changed THEN
            Scroll the address into view if necessary
            Select the address
        ELSE
            Notify the user
        ENDIF
        RETURN
    ENDIF

    Find the fist change beneath the current address, then move to the next
    IF the end of the list was reached THEN
        Notify the user
    ELSE
        Scroll the address into view if necessary
        Select the address
    ENDIF
**/
void CImageEditorDlg::OnMenu_ViewGotoNextCh()
{
    if(inMsg || !working) return;
    if(!changed)
    {
        MessageBox(_T("No changes have been made."),_T("Error"),MB_OK|MB_ICONINFORMATION);
        return;
    }

   // const change *p;
    DWORD curAddx=0;
 	const Record *strtRec;
	bool bValidAddr = true;
	bool bRecMod = false;

    UpdateHelpTopic(this,DEF_GoMenuTopic _T("#nextChange"));

    //Find out what the actual current address is...
    CWnd *e=Edt_Address;
	CString str;
    e->GetWindowText(str);
    if(str.GetLength()<1)
        curAddx=minDisplayed;
    else {
        // curAddx=Doubleword::ParseHex(str)<<1;
		curAddx=Doubleword::ParseHex(str);
	}

    // Step through the change list...
	curAddx += 1;
    while (!bRecMod && (curAddx<=highestAddress) && (curAddx >= lowestAddress)){
  	   strtRec= GetAddx(curAddx, bValidAddr, bRecMod);
	   curAddx++;
	}
	
	// Off by one  [TODO: can this be removed?]
	curAddx -=1;
	if (bRecMod){
  
       // str.Format(_T("%08.8X"),curAddx>>1);

	   if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
	      || (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){

		   str.Format(_T("%05.5X"), curAddx);

 	  	}  else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 
  
		   str.Format(_T("%08.8X"), curAddx);

		}

        int i=Lst_Address->FindString(0,(LPCTSTR)str);

        if(i<0) {
           ScrollTo(curAddx);
           NewEditValue(Lst_Address->FindString(0,(LPCTSTR)str));
        } else {
           NewEditValue(i);
		}
	} else {

		MessageBox(_T("No changes found after this address."),_T("Finished"),MB_OK|MB_ICONINFORMATION);
	  
	    // Start back at the beginning if there were no changes above curAddx...
		// Step through the change list...

		curAddx = lowestAddress;
		strtRec= GetAddx(curAddx, bValidAddr, bRecMod);
		while (!bRecMod && (curAddx<=highestAddress) && (curAddx >= lowestAddress)){
 			strtRec= GetAddx(curAddx, bValidAddr, bRecMod);
			curAddx++;
		}
 
		// Off by one [TODO:: can this be removed]
		curAddx -=1;
		if(!bRecMod)
			MessageBox(_T("No changes found after this address."),_T("Finished"),MB_OK|MB_ICONINFORMATION);
		else {
       		if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
				|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){

				str.Format(_T("%05.5X"), curAddx);

 	  		} else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 
  
				str.Format(_T("%08.8X"), curAddx);
			}
        
		    int i=Lst_Address->FindString(0,(LPCTSTR)str);

			if(i<0) {
				ScrollTo(curAddx);
				NewEditValue(Lst_Address->FindString(0,(LPCTSTR)str));
			} else {
              NewEditValue(i);
			}
		}	
	}
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_ViewGotoPrevCh
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Called to catch the Go|Go to Previous Change command.
PDL:
    IF copying a file OR no file is open THEN
        RETURN
    ENDIF
    IF no changes have been made THEN
        Notify the user
        RETURN
    ENDIF

    IF a row is selected THEN
        Use that address as the current address
    ELSE
        Use the lowest address on-screen as the current address
    ENDIF

    Find the first change before this address
    IF the end of the list was reached THEN
        Notify the user
    ELSE
        Scroll the address into view if necessary
        Select the row
    ENDIF
**/
void CImageEditorDlg::OnMenu_ViewGotoPrevCh()
{
    if(inMsg || !working) return;
    if(!changed)
    {
        MessageBox(_T("No changes have been made."),_T("Error"),MB_OK|MB_ICONINFORMATION);
        return;
    }

	// const change *p;
    DWORD curAddx=0;
 	const Record *strtRec;
	bool bValidAddr = true;
	bool bRecMod = false;

    UpdateHelpTopic(this,DEF_GoMenuTopic _T("#nextChange"));

    //Find out what the actual current address is...
    CWnd *e=Edt_Address;
	CString str;
    e->GetWindowText(str);
    if(str.GetLength()<1)
        curAddx=minDisplayed;
    else {
        // curAddx=Doubleword::ParseHex(str)<<1;
		curAddx=Doubleword::ParseHex(str);
	}

    // Step through the change list...

	curAddx -= 1;
    while (!bRecMod && (curAddx>=lowestAddress) && (curAddx<=highestAddress)){
   	   strtRec= GetAddx(curAddx, bValidAddr, bRecMod);
       curAddx--;
	}
	
	// Off by one need [TODO:: can this be removed]
	curAddx +=1;
	if ( bRecMod){
  
       // str.Format(_T("%08.8X"),curAddx>>1);
	   if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
	      || (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){

		   str.Format(_T("%05.5X"), curAddx);
 	  	}  else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 
  
		   str.Format(_T("%08.8X"), curAddx);
		}

        int i=Lst_Address->FindString(0,(LPCTSTR)str);

        if(i<0) {
           ScrollTo(curAddx);
           NewEditValue(Lst_Address->FindString(0,(LPCTSTR)str));
        } else {
           NewEditValue(i);
		}
	  } else {

		MessageBox(_T("No changes found after this address."),_T("Finished"),MB_OK|MB_ICONINFORMATION);
	  
	    // Start back at the beginning if there were no changes above curAddx...
		// Step through the change list...

		curAddx = highestAddress;
		strtRec= GetAddx(curAddx, bValidAddr, bRecMod);
		while (!bRecMod && (curAddx >= lowestAddress) && (curAddx<=highestAddress)){
 			strtRec= GetAddx(curAddx, bValidAddr, bRecMod);
			curAddx--;
		}
 
		// Off by one need [TODO:: can this be removed]
		curAddx +=1;
		if(!bRecMod){
			MessageBox(_T("No changes found after this address."),_T("Finished"),MB_OK|MB_ICONINFORMATION);
		}else {
       		if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
				|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){

				str.Format(_T("%05.5X"), curAddx);

 	  		} else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 
  
				str.Format(_T("%08.8X"), curAddx);
			}
        
		    int i=Lst_Address->FindString(0,(LPCTSTR)str);

			if(i<0) {
				ScrollTo(curAddx);
				NewEditValue(Lst_Address->FindString(0,(LPCTSTR)str));
			} else {
              NewEditValue(i);
			}
		}	
	}
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnBtnRefresh
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Handles the "Update" button/menu command.
PDL:
    Reload cache
    Update display
    Reselect the item
**/
void CImageEditorDlg::OnBtnRefresh() 
{
    int n=Lst_Address->GetCurSel();
    InvdCache();
    GetAddx(minDisplayed);
    DoListing(minDisplayed);
    if(n>=0)
        NewEditValue(n);
    UpdateHelpTopic(this,DEF_UpdateMenuTopic);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnRefreshEnable
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Handles the "Enable Auto-Refresh" menu command.
PDL:
    Toggle the checkmark in the menu
    Invert the state of the auto-update flag
**/
void CImageEditorDlg::OnRefreshEnable()
{
    bEnableAutoRefresh=!bEnableAutoRefresh;
    m_Menu.GetSubMenu(2)->CheckMenuItem(1,
        MF_BYPOSITION|(bEnableAutoRefresh?MF_CHECKED:MF_UNCHECKED));
    UpdateHelpTopic(this,DEF_UpdateMenuTopic);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnMenu_HelpIndex
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Catches the Help|Contents command.
PDL:
    Display help for the main window
**/
void CImageEditorDlg::OnMenu_HelpIndex() {DisplayHelpTopic(this,helpDefRef);}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::OnHelpInfo
Parameters:     [CImageEditorDlg *this]
                HELPINFO *pHelpInfo
                    Pointer to the HELPINFO structure describing the
                    help target.
Returns:        BOOL
                    Always TRUE.
Description:    Brings up help using the given structure.
PDL:
    IF help is requested for a menu item THEN
        Bring up help for the window
    ELSE
        Bring up help for whatever the mouse is over
    ENDIF
**/
BOOL CImageEditorDlg::OnHelpInfo(HELPINFO *pHelpInfo) 
{
    if(pHelpInfo->iContextType==HELPINFO_MENUITEM)
        DisplayHelpTopic(this,helpDefRef);
    else
    {
        inContextHelp=true;
        ScreenToClient(&(pHelpInfo->MousePos));
        OnLButtonUp(0,pHelpInfo->MousePos);
    }
    return TRUE;
}

#ifdef HAVE_TOOLTIPS
BOOL CImageEditorDlg::OnToolTipNeedText(UINT id, NMHDR *pNMHDR, LRESULT *pResult)
{
    TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
    UINT nID =pNMHDR->idFrom;

    if((pTTT->uFlags&TTF_IDISHWND) && (nID=::GetDlgCtrlID((HWND)nID)))
    {
        switch(nID)
        {
        case IDC_LST_CHANGED: pTTT->lpszText=_T("Shows which addresses have changed values.");break;
        case IDC_LST_ADDRESS:
        case IDC_LST_VALUE_HEX:
        case IDC_LST_VALUE_DEC: pTTT->lpszText=_T("Select a row to edit an address.");break;
        case IDC_LST_UTCTIME: pTTT->lpszText=_T("Shows the time of the last modification to this address.");break;
        case IDC_FAKEBTN_TOP: pTTT->lpszText=_T("Seeks to the first address.");break;
        case IDC_SCR_ADDRESS: pTTT->lpszText=_T("Scrolls through the addresses.");break;
        case IDC_FAKEBTN_BOT: pTTT->lpszText=_T("Seeks to the last address.");break;
        case IDC_FAKEBTN_APPLY: pTTT->lpszText=_T("Applies changes made.");break;
        case IDC_EDT_ADDRESS: pTTT->lpszText=_T("Displays the address currently being edited.");break;
        case IDC_EDT_VALUE_HEX: pTTT->lpszText=_T("Displays the value currently being edited in hexadecimal.");break;
        case IDC_EDT_VALUE_DEC: pTTT->lpszText=_T("Displays the value currently being edited in decimal.");break;
        case IDC_EDT_UTCTIME: pTTT->lpszText=_T("Displays the time of the last modification to the current address.");break;
        case IDC_BTN_FILEOPEN: pTTT->lpszText=_T("Open file (Right click for recent)");break;
        case IDC_BTN_FILESAVE: pTTT->lpszText=_T("Save changes");break;
        case IDC_BTN_FILEPRINT: pTTT->lpszText=_T("Print data");break;
        case IDC_BTN_FILEEXPORT: pTTT->lpszText=_T("Export data");break;
        case IDC_BTN_GOTOADDRESS: pTTT->lpszText=_T("Go to address (Right click for recent)");break;
        case IDC_BTN_NEXTCHANGE: pTTT->lpszText=_T("Next change");break;
        case IDC_BTN_PREVCHANGE: pTTT->lpszText=_T("Previous change");break;
        case IDC_BTN_SAVECOPY:  pTTT->lpszText=_T("Saves a new copy of the file");break;
        case IDC_BTN_REFRESH:   pTTT->lpszText=_T("Updates the view of the file");break;
        default: pTTT->lpszText=NULL;
        }
        pTTT->hinst=NULL;
        return TRUE;
    }
    return FALSE;
}
#endif

////////////////////////////////////////////////////////////////////////////////
// Internal Functions                 //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CImageEditorDlg::ScrollTo
Parameters:     [CImageEditorDlg *this]
                DWORD address
                    The new bottommost address.
Returns:        void
Description:    Scrolls the table so that the given address is as close to
                the top of the table as possible.
PDL:
    IF no file is loaded OR copying a file THEN
        RETURN
    ENDIF

    Round the address
    Clip the address to the lowest possible address
    Calculate the last lowest-displayed address and clip to that
    Synchronize the contents of the list boxes to the new location
    Synchronize the scroll bar
    IF a row is selected AND the selection should be visible THEN
        Select the appropriate row
    ENDIF
**/
void CImageEditorDlg::ScrollTo(DWORD address)
{
    if(!working || inMsg) return;
    CRect rect;
    int num;
    DWORD lastValid,d;
    double conversion;

    // address&=~1;
    Lst_Changed->GetClientRect(&rect);
    num=(rect.Height())/Lst_Changed->GetItemHeight(0);
	// lastValid=(highestAddress+2)-(num<<1);
	lastValid=(highestAddress+2)-(num);

    if(lastValid>highestAddress){
		lastValid=highestAddress;
	}
    conversion = (double)(lastValid + 2 - lowestAddress)/32767.0;

    if(address<lowestAddress) {
		address=lowestAddress;
	}
    if(address>lastValid){
		address=lastValid;
	}
    
  //  GetAddx(address);
    if(address!=minDisplayed)
    {
        CScrollBar *pScrollBar=Scr_Address;
 
		if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 
  
	       pScrollBar->SetScrollPos(int(double(address - lowestAddress)/conversion));
			
        } else {
	       pScrollBar->SetScrollPos(int(double(address)/conversion));
		}

		DoListing(address);
        CString s;
        Edt_Address->GetWindowText(s);
        if(s.GetLength() >= 1)
        {
            inMsg=true;
            // d = Doubleword::ParseHex(s)<<1;
			d = Doubleword::ParseHex(s);

//			if(d>=minDisplayed && d<(minDisplayed+(num<<1)-1))
            if(d>=minDisplayed && d < (minDisplayed + num - 1))
            {
                d -= minDisplayed;
				// d >>= 1;
                Lst_Changed->SetCurSel(d);
                Lst_Address->SetCurSel(d);
                Lst_ValueHex->SetCurSel(d);
                Lst_ValueDec->SetCurSel(d);
                Lst_UTCTime->SetCurSel(d);
            }
            inMsg=false;
        }
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::WriteRegistryValues
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Saves window parameters and dialog settings to the registry.
PDL:
    Open the registry key
    Delete old Open Recent values
    Add the current set of Open Recent values
    Save window dimensions and position
    Save the Enable Auto-Refresh state
    Save Export and Print settings
**/
void CImageEditorDlg::WriteRegistryValues()
{
    TCHAR tempstr[]=_T("OpenEntry0");
    int l;
    RegistryKey rkey(HKEY_CURRENT_USER,DEF_RegistrySubkey);
    
    if(!rkey.IsOpen()) return;

    //Clean out any old Open Recent entries...
    for(l=0;l<DEF_NumOpenEntries;l++)
    {
        rkey.Delete(tempstr);
        tempstr[9]++;
    }

    //Add the new ones...
    tempstr[9]='0';
    for(l=0;l<DEF_NumOpenEntries;l++)
        if(openEntries[l])
        {
            rkey.Write(tempstr,openEntries[l]);
            tempstr[9]++;
        }

    //Save window dimensions...
    WINDOWPLACEMENT wndplcmt;
    GetWindowPlacement(&wndplcmt);
    if(wndplcmt.showCmd==SW_MINIMIZE || wndplcmt.showCmd==SW_SHOWMINIMIZED
        || wndplcmt.showCmd==SW_SHOWMINNOACTIVE)
    {
        wndplcmt.showCmd=
            SW_SHOWNORMAL;
        SetWindowPlacement(&wndplcmt);
    }

    CRect wr;GetWindowRect(&wr);
    
    rkey.Write(_T("WindowHeight"),wr.Height());
    rkey.Write(_T("WindowLeft"),wr.left);
    rkey.Write(_T("WindowTop"),wr.top);
    rkey.Write(_T("EnableRefresh"),bEnableAutoRefresh);

    //Save the Export dialog box options...
    rkey.Write(_T("ExFlags"),ExportData.flags);
    rkey.Write(_T("ExFormat"),ExportData.format);
    rkey.Write(_T("ExOutputDir"),ExportData.outputDir);

    //Save the Print dialog box options...
    rkey.Write(_T("PrFlags"),PrintData.flags);
    rkey.Write(_T("PrFont"),PrintData.font);
    rkey.Write(_T("PrFontSize"),PrintData.fontSize);
    rkey.Write(_T("PrMetrics"),PrintData.metrics,sizeof(PrintData.metrics));
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::LoadRegistryValues
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Load settings and position from the registry.
PDL:
    Open the registry key
    Clear current open entries
    DOFOR each OpenEntryX entry in the registry
        Read the entry
        IF the entry isn't already in the loaded list THEN
            IF the file to which the entry refers exists THEN
                Add the entry to the list
            ENDIF
        ENDIF
    ENDDO

    Load the Auto-Refresh state
    Load the Export and Print dialog settings
    Load and adjust the window size and position
**/
void CImageEditorDlg::LoadRegistryValues()
{
//    int i,j,k,l;
    TCHAR tempstr[]=_T("OpenEntry0");
    CString str;
    RegistryKey rkey(HKEY_CURRENT_USER,DEF_RegistrySubkey);
    
    if(!rkey.IsOpen()) return;

    //Make sure we're not leaking recently-opened strings...
    for(int i=0;i<DEF_NumOpenEntries;i++)
        if(openEntries[i]) 
        {
            delete[] openEntries[i];
            openEntries[i]=0;
        }


	int j = 0;
    for(int i=0; i<DEF_NumOpenEntries; i++, tempstr[9]++)
    {
        str=rkey.Read(tempstr);
		int l=str.GetLength();
        if(l)
        {
			int k=0;
			for(k=0; k<j; k++)
                if(!str.CompareNoCase(openEntries[k]))
                    break;
            if(k<j)
                continue;

            if(str[0]=='*' && !~::GetFileAttributes(1+(LPCTSTR)str))
                continue;
            else if(!~::GetFileAttributes(str))
                continue;

			openEntries[j] = new TCHAR[l+1];
            if(openEntries[j])
                _tcscpy_s(openEntries[j++], (l+1), str);
        }
    }

    bEnableAutoRefresh=!!rkey.Read(_T("EnableRefresh"),1);

    ExportData.format=(BYTE)rkey.Read(_T("ExFormat"),(DWORD)0);
    ExportData.outputDir=rkey.Read(_T("ExOutputDir"));
    ExportData.flags=(BYTE)rkey.Read(_T("ExFlags"),
        __ExportData::Header|__ExportData::ColHeaders);

    PrintData.flags=(BYTE)rkey.Read(_T("PrFlags"),__PrintData::HdrFirst);
    PrintData.fontSize=rkey.Read(_T("PrFontSize"),10);        
    PrintData.font=rkey.Read(_T("PrFont"),_T("Courier New"));
    static const int defmetr[6]=
    {
        Float2DWord(1.0F),Float2DWord(1.0F),Float2DWord(1.0F),Float2DWord(1.0F),
        Float2DWord(8.5F),Float2DWord(11.0F)
    };
    rkey.Read(_T("PrMetrics"),PrintData.metrics,sizeof(PrintData.metrics),defmetr);

    DWORD hgt,top,left;
    CRect wr;
    GetWindowRect(&wr);
    top=wr.top;left=wr.left;
    hgt=rkey.Read(_T("WindowHeight"),wr.Height());
    top=rkey.Read(_T("WindowTop"),wr.top);
    left=rkey.Read(_T("WindowLeft"),wr.left);

	if(hgt<370){
		hgt=370;
	}
	
	DWORD i = ::GetSystemMetrics(SM_CYSCREEN);
	if(hgt > i){
		hgt = i;
	}

    SetWindowPos(0,left,top,wr.Width(),hgt,SWP_NOACTIVATE|SWP_NOZORDER);
    ResizeElem();
    DrawSizer(true);
    DrawBtnApply(false);
    DrawBtnsTopBot(false,false);
    DrawBtnsTopBot(true,false);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::NewEditValue
Parameters:     [CImageEditorDlg *this]
                int n
                    The row in the list boxes to select.
Returns:        void
Description:    Selects a row in the list boxes and updates the edit boxes.
PDL:
    Set the address, hex. value, dec. value, and timestamp edit boxes'
      values to the currently selected row's values.
    Indirectly select the given row in all list boxes.
**/
void CImageEditorDlg::NewEditValue(int n)
{
    if(n<0) return;

    inMsg=true;
    CListBox *lChanged=Lst_Changed;
    CListBox *lAddress=Lst_Address;
    CListBox *lValHex=Lst_ValueHex;
    CListBox *lValDec=Lst_ValueDec;
    CListBox *lUTCTime=Lst_UTCTime;

    //Update the edit boxes
    CString address,valueDec,valueHex,utcTime;

	lAddress->GetText(n, address);
    lValHex->GetText(n, valueHex);
	lValDec->GetText(n, valueDec);
    lUTCTime->GetText(n, utcTime);
 
	if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
   	|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){
   
		 address.Format(_T("%05.5X"),Doubleword::ParseHex(address));
	     valueDec.Format(_T("%05.5u"),Doubleword::ParseDecimal(valueDec));
		 valueHex.Format(_T("%4.4X"),Doubleword::ParseHex(valueHex));

	}  else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 

		 address.Format(_T("%08.8X"),Doubleword::ParseHex(address));
         valueDec.Format(_T("%03.3u"),Doubleword::ParseDecimal(valueDec));
		 valueHex.Format(_T("%2.2X"),Doubleword::ParseHex(valueHex));
	}

 //   valueHex.Format(_T("%X"),Doubleword::ParseHex(valueHex));

    Edt_Address->SetWindowText(address);
    Edt_ValueHex->SetWindowText(valueHex);
    Edt_ValueDec->SetWindowText(valueDec);
    Edt_UTCTime->SetWindowText(utcTime);

    //Notify the list boxes that they need to set their current selection.
    lChanged->PostMessage(LB_SETCURSEL,(WPARAM)n,0);
    lAddress->PostMessage(LB_SETCURSEL,(WPARAM)n,0);
    lValHex->PostMessage(LB_SETCURSEL,(WPARAM)n,0);
    lValDec->PostMessage(LB_SETCURSEL,(WPARAM)n,0);
    lUTCTime->PostMessage(LB_SETCURSEL,(WPARAM)n,0);
    
    inMsg=false;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::DoSave
Parameters:     [CImageEditorDlg *this]
                const CString &filename
                    Name of the file to save if copy is true.
                bool copy
                    true if a Save As should be performed.
                    false if a normal Save should be performed.
Returns:        void
Description:    Saves changes to the image file or saves a new copy of the
                image file (with changes) and opens that file for editing.
PDL:
    IF no file is open OR the I/O stream is invalid OR the normal
      Save function is requested and no changes have been made OR
      the file is read-only and the Save function is requested THEN
        RETURN
    ENDIF

    Set the cursor to indicate that the program is busy.
    IF the Save As function is requested THEN
        IF the output file can't be opened THEN
            Notify the user
            RETURN
        ELSE
            Open the output file
        ENDIF
        Disable all necessary controls in the dialog
        Clear the cache
        DOFOR each cache-sized set of records in the file
            Update the caption text and progress bar every 64 virtual-KB
            IF the user wants to abort THEN
                Break out of the loop
            ENDIF
            Read a cache-sized chunk of records from the current file
            Write a cache-sized chunk of records to the output file
        ENDDO

        IF not aborted THEN
            DOFOR each unsaved change to the file
                Write the change to the output file
            ENDDO
        ENDIF

        Re-enable the controls
        Clear the cache

        IF not aborted THEN
            Close the current file
            Close the output file
            Save the current position, address, and recent address entries
            Open the output file as the current file
            Restore the position, address, and recent address entries
        ENDIF
    ELSE
        Reopen the current file for writing
        DOFOR each change
            Write each change to the file
        ENDDO
        Reopen the current file for reading only
        Reload the cache
        Refresh the listing and reselect the row
    ENDIF
**/
void CImageEditorDlg::DoSave(const CString &filename,bool copy)
{
    static const UINT ids[]={IDC_BTN_FILEOPEN,IDC_BTN_FILESAVE,
        IDC_BTN_FILEPRINT,IDC_BTN_FILEEXPORT,IDC_BTN_GOTOADDRESS,
        IDC_BTN_NEXTCHANGE,IDC_BTN_PREVCHANGE,IDC_BTN_CONTEXTHELP,
        IDC_LST_CHANGED,IDC_LST_ADDRESS,IDC_LST_VALUE_HEX,IDC_LST_VALUE_DEC,
        IDC_LST_UTCTIME,IDC_SCR_ADDRESS,IDC_EDT_ADDRESS,IDC_EDT_VALUE_HEX,
        IDC_EDT_VALUE_DEC,IDC_EDT_UTCTIME,IDC_BTN_REFRESH,IDC_BTN_SAVECOPY};
   // CStdioFile output;
    DWORD flags=CFile::modeWrite|CFile::typeBinary|CFile::shareExclusive;

    if(!(working) || (!copy && !changed) || (bReadOnly && !copy))
        return;


	CWaitCursor wait;
    if(copy) {

        static bool inHere=false;
        if(inHere){ 
			return;
		}
        
		inHere=true;
        inMsg=true;
  		  
		CString fnm=filename;

        //Disable everything disable-able in the window
        for(int i=0; i<sizeof(ids)/sizeof(*ids); i++)
            GetDlgItem(ids[i])->EnableWindow(FALSE);
        m_Menu.EnableMenuItem(0,MF_BYPOSITION|MF_GRAYED);
        m_Menu.EnableMenuItem(1,MF_BYPOSITION|MF_GRAYED);
        m_Menu.EnableMenuItem(2,MF_BYPOSITION|MF_GRAYED);
        m_Menu.EnableMenuItem(3,MF_BYPOSITION|MF_GRAYED);
        DrawMenuBar();

  

        //Pretend that there's not a file open so the buttons gray themselves
		
        working=false;
        DrawBtnApply(false);
        DrawBtnsTopBot(true,false);
        DrawBtnsTopBot(false,false);

  /*
		 errno_t err;

   // Open for read (will fail if file "crt_fopen_s.c" does not exist)
   if( (err  = fopen_s( &stream, "crt_fopen_s.c", "r" )) !=0 )


  */

		// Open current file that matches data in memory.
		errno_t err;
	    err = _tfopen_s(&curIO, (LPCTSTR)currentFile, _T("r+b"));

       if(0 != err){
		  
		  // strErrMsg = _tcserror(strErrMsg, "Open failed"); 
		  TCHAR cBuffer[80];
		  err  = _tcserror_s(cBuffer, 80);
		  CString strErrMsg;  
		  strErrMsg.Format(_T("Open failed: %s"), &cBuffer[0]); 
 
		  MessageBox(strErrMsg, _T("Error"), MB_OK|MB_ICONWARNING);
		  inMsg = false;
		  working = true;
          OnMenu_FileClose();
//		  fclose(curIO);
//		  curIO = NULL;
		  return;
	   }


  //   ::rewind(curIO);



	// Read in file header from file.
	//  Once data is read in write it back out to new file.

	   file_hdr		strctFileHdrTemp;
	   DWORD dwReadStatus = 0;
       dwReadStatus=fread(&strctFileHdrTemp, sizeof(strctFileHdrTemp), 1, curIO);

       if(!dwReadStatus && ferror(curIO)){
		    CString strErrMsg;
		    char cBuffer[256];
			err	= _strerror_s(cBuffer, 256, "Error reading file header! "); 
			// strErrMsg = _strerror("Error reading file header! ");
			// MessageBox(strErrMsg, _T("Error"),MB_OK|MB_ICONWARNING);
			
			strErrMsg = cBuffer;
			MessageBox(strErrMsg, _T("Error"),MB_OK|MB_ICONWARNING);
			clearerr(curIO);
			fclose(curIO);
            inHere=inMsg=false;
			working = false;
            return;
       }

 
//     int nWriteStatus = fwrite(&strctFileHdrTemp, sizeof(strctFileHdrTemp), 1, output);

	// Convert to WinTel format before testing.
//	   strctFileHdrTemp.strtHeader.eType = (image_type)::BE2LE_D(strctFileHdrTemp.strtHeader.eType);

	// Read memory conversion table.
	// Once data is read in, write out to a new file.

	   conv_table unionConv_TableTemp;
	
	// Start writing after the file header
	   int nSeekStatus = fseek(curIO, DEF_IMG_HEAD_SIZE, SEEK_SET);
	// verify that fseek was successful
		 
	   dwReadStatus=fread(&unionConv_TableTemp.ace_table, sizeof(unionConv_TableTemp.ace_table), 1, curIO);
	   if(!dwReadStatus && ferror(curIO)){
		   CString strErrMsg;
		   char cBuffer[256];
		   
		   err = _strerror_s(cBuffer, 256, "Error reading conversion table! "); 
		   // strErrMsg = _strerror( "Error reading conversion table! "); 
		   strErrMsg = cBuffer;
	   	   MessageBox(strErrMsg, _T("Error"),MB_OK|MB_ICONWARNING);
		   clearerr(curIO);
		   fclose(curIO);
		   inHere=inMsg=false;
		   working = false;
		   return;
        }


       // DWORD curAddx = lowestAddress;
        long a = fread(m_Loaded, sizeof(*m_Loaded), m_CacheSize, curIO);
        if(!a && ferror(curIO)){
		   CString strErrMsg;
		   char cBuffer[256];
		   err = _strerror_s(cBuffer, 256, "Error reading conversion table! "); 
		   strErrMsg = cBuffer;
		   err = _strerror_s(cBuffer, 256, "Error reading data! "); 
		   // strErrMsg = _strerror( "Error reading data! "); 
           MessageBox(strErrMsg, _T("Error"),MB_OK|MB_ICONWARNING);
           bAbort=true;
        }

        fclose(curIO);
        curIO=0;

		/*
		 errno_t err;

		// Open for read (will fail if file "crt_fopen_s.c" does not exist)
		if( (err  = fopen_s( &stream, "crt_fopen_s.c", "r" )) !=0 )
		  */

		FILE *output = NULL;
		err = _tfopen_s(&output, (LPCTSTR)filename, _T("w+b"));
        if(0 != err){
//          EndWaitCursor();
            CString poo;
            poo.Format(_T("Couldn't save the file \"%s\"."),filename);
            MessageBox(poo,_T("Error"),MB_OK|MB_ICONWARNING);
			clearerr(curIO);
            inHere=inMsg=false;
            return;
        }
 //     output=f.m_pStream;

        int nWriteStatus = fwrite(&strctFileHdrTemp, sizeof(strctFileHdrTemp), 1, output);

		nSeekStatus = fseek(output, DEF_IMG_HEAD_SIZE, SEEK_SET);
		// verify that fseek was successful
		 
		nWriteStatus = fwrite(&unionConv_TableTemp, sizeof(unionConv_TableTemp), 1, output);
		// verify that write was successful

		nSeekStatus = fseek(output, DEF_IMG_HEAD_SIZE + 2048, SEEK_SET);
        nWriteStatus = fwrite(m_Loaded, sizeof(*m_Loaded), m_CacheSize, output);

        const change *c;
        c = changes;
        bAbort = false;

        Record rec;
        for(c=changes; (!bAbort && c); c=c->next)
        {
			// Need to convert values back to little endian
            rec.data=::BE2LE_W(c->newValue);
            rec.milliseconds=::BE2LE_W(c->newMillis);
            rec.seconds=::BE2LE_D(c->newTimestamp);

			// need to skip over file header & conversion table	
		//	long lIdx = (long)(c->address<<2) + DEF_IMG_HEAD_SIZE + 2048;

			long lIdx = (long)((c->address) * sizeof(rec)) + DEF_IMG_HEAD_SIZE + 2048;

           // fseek(output, c->address<<2, SEEK_SET);
			fseek(output, lIdx, SEEK_SET);
			fwrite(&rec , 1 ,sizeof(rec), output);
        }

        inMsg=false;
        inHere=false;
        working=true;

        //Enable everything
	
        for(int i=0; i<sizeof(ids)/sizeof(*ids); i++){
            GetDlgItem(ids[i])->EnableWindow();
		}

        m_Menu.EnableMenuItem(0,MF_BYPOSITION|MF_ENABLED);
        m_Menu.EnableMenuItem(1,MF_BYPOSITION|MF_ENABLED);
        m_Menu.EnableMenuItem(2,MF_BYPOSITION|MF_ENABLED);
        m_Menu.EnableMenuItem(3,MF_BYPOSITION|MF_ENABLED);
        DrawMenuBar();
        DrawBtnApply(false);
        DrawBtnsTopBot(true,false);
        DrawBtnsTopBot(false,false);

        fclose(output);
     // f.Close();
 //     fclose(curIO);
        curIO=0;
		ClearChanges();

        //If we're not aborting, open the new file and discard the old one
		if(!bAbort) {
            DWORD d=minDisplayed;
 //           BYTE p[sizeof(addressEntries)];
            int i=Lst_Changed->GetCurSel();
            
        //  fclose(curIO);
            curIO=0;
 //           ClearChanges();
            bReadOnly=false;
 
        }
    } else {
		/*
		 errno_t err;

		// Open for read (will fail if file "crt_fopen_s.c" does not exist)
		if( (err  = fopen_s( &stream, "crt_fopen_s.c", "r" )) !=0 )
		  */


        //If nothing's getting copied, just write the changes in
        //memory to disk and discard them
        Record rec;
		errno_t err;
        const change *p=changes;
		err = _tfreopen_s(&curIO, (LPCTSTR)currentFile, _T("r+b"), curIO);
        if(0 != err)
        {
 //         EndWaitCursor();
			/*
				errno_t err;

				// Open for read (will fail if file "crt_fopen_s.c" does not exist)
				if( (err  = fopen_s( &stream, "crt_fopen_s.c", "r" )) !=0 )
			*/
            errno_t err=_tfopen_s(&curIO, (LPCTSTR)currentFile, _T("rb"));
            if(0 != err){
                CString strErrMsg;
			    char cBuffer[256];
				strErrMsg = cBuffer;
		   		err	= _strerror_s(cBuffer, 256, "Couldn't open input file for writing.  ");
				// strErrMsg = _strerror( "Couldn't open input file for writing.  ");
	   	        MessageBox(strErrMsg, _T("Error"),MB_OK|MB_ICONWARNING);
		        clearerr(curIO);
            } else {
                MessageBox(_T(""),_T("Error"),MB_OK|MB_ICONWARNING);
			    CString strErrMsg;
				char cBuffer[256];
				strErrMsg = cBuffer;
		   		err = _strerror_s(cBuffer, 256, "Couldn't open input file for reading or writing. "); 
	   			// strErrMsg = _strerror("Couldn't open input file for reading or writing. "); 
	 			MessageBox(strErrMsg, _T("Error"),MB_OK|MB_ICONWARNING);
				clearerr(curIO);
                OnMenu_FileClose();
            }
            return;
        }
        rewind(curIO);
        fflush(curIO);

        while(p)
        {
            rec.data=::BE2LE_W(p->newValue);
            rec.milliseconds=::BE2LE_W(p->newMillis);
            rec.seconds=::BE2LE_D(p->newTimestamp);

			// need to skip over file header & conversion table	
		//	long lIdx = (long)(p->address<<2) + DEF_IMG_HEAD_SIZE + sizeof(m_unionConv_Table);
			long lIdx = (long)(p->address * sizeof(rec)) + DEF_IMG_HEAD_SIZE + sizeof(m_unionConv_Table);


			// fseek(curIO,p->address<<2,SEEK_SET);
			fseek(curIO, lIdx, SEEK_SET);
            fwrite(&rec, sizeof(rec), 1, curIO);
            p=p->next;
        }
        ClearChanges();
        fflush(curIO);
        rewind(curIO);
    }

//    EndWaitCursor();

//	fclose(curIO);
//  curIO=0;

	delete[] m_Loaded;

	DoLoad(filename);
    CString s(_T("GOES Image Editor: "));
    s+=Ellipsis(currentFile);
    if(changed){
		s+=_T(" [MODIFIED]");
	}
    SetWindowText(s);
    UpdateMenu();

	int n=Lst_Address->GetCurSel();
    DoListing(minDisplayed);
    if(n>=0){
		NewEditValue(n);
	}

    ScrollTo(minDisplayed);
    bReadOnly=false;
 //   EndWaitCursor();
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::DoLoad
Parameters:     [CImageEditorDlg *this]
                const CString &_filename
                    The name of the output file, possibly with a * beforehand
                    to indicate that the file should be opened as read-only.
Returns:        void
Description:    Opens the given file for editing.
PDL:
    IF the file can't be opened THEN
        Notify the user
        RETURN
    ENDIF
    Open the file
    
    IF the file contains no data THEN
        Notify the user
        RETURN
    ENDIF

    IF the file contains partial records THEN
        Notify the user
    ENDIF

    Save the last-modified time for Auto-Refresh
    IF a file is already open AND it has been changed THEN
        Save the changes if the user wants to
    ENDIF

    Clear all changes
    IF no cache has been allocated THEN
    Start requesting a cache of the predefined max. cache size
    DOWHILE the requested cache can't be allocated AND size is over 64 records
        divide the requested size by two
    ENDDO
    IF the cache couldn't be allocated
        Alert the user
        RETURN
    ENDIF
    Zero out the cache

    Set the current I/O stream to the input stream
    Set the busy cursor
    Clear recent addresses
    Add the file as the most recently opened file in the list
    Update the caption text
    Load the cache
    Reposition the listing to the lowest address and display the table entries
    Synchronize the menu
    Redraw everything
**/
void CImageEditorDlg::DoLoad(const CString &_filename)
{
	CWaitCursor wait;
    DWORD fileSize = 0;
    FILE    *input;
    CString strFilename = _filename;

    if(strFilename[0]=='*'){
		strFilename = ( 1 + (LPCTSTR)strFilename);
	}

    if(0 == strFilename.GetLength())
    {
        MessageBox(_T("Invalid filename."),_T("Error"),MB_OK|MB_ICONWARNING);
        return;
    }

	/*
		errno_t err;
		// Open for read (will fail if file "crt_fopen_s.c" does not exist)
		if( (err  = fopen_s( &stream, "crt_fopen_s.c", "r" )) !=0 )
	*/

	errno_t err = _tfopen_s(&input, (LPCTSTR)strFilename, _T("rb"));
    if(0 != err)
    {
        CString poo;
        poo.Format(_T("Couldn't load the file \"%s\"."), strFilename);
        MessageBox(poo,_T("Error"),MB_OK|MB_ICONWARNING);
        fclose(input);
		input = NULL;
        return;
    }

    struct _stat stbuf;
    if(_tstat((LPCTSTR)strFilename, &stbuf) || (fileSize = stbuf.st_size) < 8)
    {
        MessageBox((fileSize<8)?_T("File contains no data."):_T("Invalid file."),
            _T("Error"),MB_OK|MB_ICONWARNING);
		        fclose(input);
		input = NULL;
        return;
    }

//    if(fileSize&7)
//        MessageBox(_T("Warning: File contains partial records.\r\n")
//                   _T("Probably incorrect format."),_T("Warning"),
//                   MB_OK|MB_ICONWARNING);

    savedSTMTime = stbuf.st_mtime;

    if(working && changed)
    {
        DWORD i = MessageBox(_T("Save changes before loading another?"),_T("Save changes"),
            MB_YESNOCANCEL|MB_ICONQUESTION|MB_DEFBUTTON1);
        if(i == IDCANCEL){
            fclose(input);
			input = NULL;
            return;
        }

        if(i == IDYES) {
            DoSave(currentFile);
		}
    }
 
 
    if(curIO) {
		fclose(curIO);
		curIO = NULL;
	}

    curIO = input;

//  for(int j=0; j<DEF_NumAddressEntries; j++) {       //see <1>
    //  addressEntries[i]=~0; // Why is memory being set to one's compliment.
  //	addressEntries[j]= 0;
// 	}
  
    currentFile = strFilename;
    working = true;
    InvdCache();
	ClearChanges();

    //Internally, anything opened as read-only begins with *.
    if(bReadOnly && currentFile[0]!='*'){
        AddOpenEntry(CString('*') + currentFile);
    } else {
        AddOpenEntry(currentFile);
    }

	CString strWindowTitle(_T("GOES Image Editor: "));
    strWindowTitle += currentFile;
    if(bReadOnly) {
        strWindowTitle += _T(" [READ-ONLY]");
	}

    bool _b = inMsg;
    inMsg = true;
    SetDlgItemText(IDC_EDT_ADDRESS,_T(""));
    SetDlgItemText(IDC_EDT_VALUE_HEX,_T(""));
    SetDlgItemText(IDC_EDT_VALUE_DEC,_T(""));
    SetDlgItemText(IDC_EDT_UTCTIME,_T(""));
    inMsg = _b;

    SetWindowText((LPCTSTR)Ellipsis(strWindowTitle));

	// Verify file header is valid after openning.
	int nStatus = ReadFileHdr();
    if(0 != nStatus) {
        CString strErrorMsg;
        strErrorMsg.Format(_T("Error reading file header!  filename:\"%s\"."), strFilename);
        MessageBox(strErrorMsg, _T("Error"), MB_OK|MB_ICONWARNING);
		fclose(curIO);
		curIO = NULL;
	    working = false;
        return;
    }

	int nMAX_ACE_RECORDS = DEF_MAX_ACE_RECORDS;
	int nMAX_SXI_RECORDS = DEF_MAX_SXI_RECORDS;
	int nACE_FILEVERSION = DEF_ACE_FILEVERSION;
	int nSXI_FILEVERSION = DEF_SXI_FILEVERSION;

	if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
	|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){
		if (nACE_FILEVERSION != m_strctFileHdr.strtHeader.nVersion){
		    CString strErrorMsg;
			strErrorMsg.Format(_T("Warning: Wrong version number!  filename:\"%s\"."), strFilename);
			MessageBox(strErrorMsg, _T("Warning"), MB_OK|MB_ICONWARNING);
		} 
		if (nMAX_ACE_RECORDS != m_strctFileHdr.strtHeader.nRecords){
			CString strErrorMsg;
			strErrorMsg.Format(_T("Warning: Number of records is not what is expected!  filename:\"%s\"."), strFilename);
			MessageBox(strErrorMsg, _T("Warning"), MB_OK|MB_ICONWARNING);
		}

	} else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){
		if (nSXI_FILEVERSION != m_strctFileHdr.strtHeader.nVersion){
			CString strErrorMsg;
			strErrorMsg.Format(_T("Warning: Wrong version number!  filename:\"%s\"."), strFilename);
			MessageBox(strErrorMsg, _T("Warning"), MB_OK|MB_ICONWARNING);
		}
		if (nMAX_SXI_RECORDS != m_strctFileHdr.strtHeader.nRecords){
			CString strErrorMsg;
			strErrorMsg.Format(_T("Warning: Number of records is not what is expected!  filename:\"%s\"."), strFilename);
			MessageBox(strErrorMsg, _T("Error"), MB_OK|MB_ICONWARNING);
		}
	}

	nStatus = ReadConvTable();
    if(0 != nStatus){
        CString strErrorMsg;
		working = false;
		fclose(curIO);
		curIO = NULL;
        strErrorMsg.Format(_T("Error reading file conversion table!  filename:\"%s\"."), strFilename);
        MessageBox(strErrorMsg,_T("Error"),MB_OK|MB_ICONWARNING);
        return;
    }

	m_CacheSize = m_strctFileHdr.strtHeader.nRecords;
    m_Loaded    = new Record[m_CacheSize];
 
	if(!m_Loaded) {
       MessageBox(_T("Insufficient memory."), _T("Error"), MB_OK|MB_ICONWARNING);
       m_CacheSize = 0;
	   delete[] m_Loaded;
       return;
    }

    TRACE(_T("Cache of %u records (%u KB) allocated\n"),m_CacheSize,
            (m_CacheSize*sizeof(*m_Loaded)));
 
	::memset(m_Loaded, 0, m_CacheSize*sizeof(*m_Loaded));

 
	if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
	|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){
	    highestAddress = DEF_ACE_HIGHESTADDRESS; 
		lowestAddress  = DEF_ACE_LOWESTADDRESS;
	}  else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 
		highestAddress = DEF_SXI_HIGHESTADDRESS;
		lowestAddress  = DEF_SXI_LOWESTADDRESS;
	}
 
    // Calling GetAddx with one parameter causes it to load the entire cache.
 //   InvdCache();

	// need to skip over file header & conversion table	

	int nConvTableSize = sizeof(m_unionConv_Table);
	long lIdx = DEF_IMG_HEAD_SIZE + nConvTableSize;
	
    // Fill the cache   
	fseek(curIO, lIdx, SEEK_SET);              // see note <2> at end of file
    DWORD dwSize = fread(m_Loaded, sizeof(*m_Loaded), m_CacheSize, curIO);
	if(dwSize != m_CacheSize){
        CString strErrorMsg;
        strErrorMsg.Format(_T("Error reading data from file!  filename:\"%s\"."), strFilename);
        MessageBox(strErrorMsg,_T("Error"),MB_OK|MB_ICONWARNING);
        return;
    }

	fclose(curIO);
 
	// TRACE(_T("m_Loaded.data[%u] = %08.8X\n"), addx-lowestLoaded, m_Loaded[addx-lowestLoaded].data);
    //Reverse byte ordering
	#ifndef BIG_ENDIAN
		for(DWORD i=0; i<m_CacheSize; i++){

	        m_Loaded[i].seconds = ::BE2LE_D(m_Loaded[i].seconds);
		    m_Loaded[i].milliseconds=::BE2LE_W(m_Loaded[i].milliseconds);
			m_Loaded[i].data=::BE2LE_W(m_Loaded[i].data);
//			TRACE(_T("m_Loaded.data[%x] = %X\n"), i, m_Loaded[i].data);
	    }
	#endif

//  GetAddx(lowestAddress);
    DoListing(lowestAddress);
    Scr_Address->EnableWindow();

    int num;
    CRect rect;
    Lst_Changed->GetClientRect(&rect);
    num = rect.Height()/Lst_Changed->GetItemHeight(0);
    
    Scr_Address->SetScrollRange(0,32767);
    Scr_Address->SetScrollPos(0);
    UpdateMenu();
    DrawBtnApply(m_bApplyDown);
    DrawBtnsTopBot(true,  m_bTopDown);
    DrawBtnsTopBot(false, m_bTopDown);

	if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){
	   // Disable address space button if SXI data is loaded
	   ((CButton*)GetDlgItem(IDC_RADIO_OPERAND))->EnableWindow(FALSE);
	   ((CButton*)GetDlgItem(IDC_RADIO_INSTRUCT))->EnableWindow(FALSE);
	   Edt_ValueHex->LimitText(2);
	   Edt_ValueDec->LimitText(3);
	} else {
	  // Otherwise Enable address space button 
	  ((CButton*)GetDlgItem(IDC_RADIO_OPERAND))->EnableWindow(TRUE);
	  ((CButton*)GetDlgItem(IDC_RADIO_INSTRUCT))->EnableWindow(TRUE);
	  Edt_ValueHex->LimitText(4);
	  Edt_ValueDec->LimitText(5);
	}
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::SetAddx
Parameters:     [CImageEditorDlg *this]
                DWORD addx
                    The address whose value should be changed.
                WORD val
                    The new value for the address.
Returns:        void
Description:    Overlays a change of the given address to the given value.
PDL:
    Round the address
    IF no file is open OR the address is invalid THEN
        RETURN
    ENDIF

    Raise the file-changed flag
    IF no changes have been made THEN
        Create a new change structure
        Set the address and value appropriately
        Set the timestamp to the current time
        Set the list to point to this change structure
    ELSE
        Search for a change at this address
        IF one exists THEN
            Update that change structure to the new value and time
        ELSE
            Create a new change structure
            Set the address and value appropriately
            Set the timestamp to the current time
            Insert the structure into the list after lower addresses and
              before higher addresses
        ENDIF
    ENDIF
**/
void CImageEditorDlg::SetAddx(DWORD addx,WORD val)
{
    SYSTEMTIME now;

	DWORD dwPhysAddx = 0;
	bool bValidAddr = true;
	// Convert to physical addresses for retrieving from memory.
	if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
		|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){

		dwPhysAddx = CImageEditorDlg::ConvertACELogical2Physical(addx, bValidAddr);

	} else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 

		dwPhysAddx = CImageEditorDlg::ConvertSXILogical2Physical(addx, bValidAddr);
	}


 //   addx&=~1;
    if(!working || addx<lowestAddress || addx>highestAddress){
        return;
	}

    changed=true;
    if(!changes)
    {
        // No changes have been made
		changes=new change;
        if(!changes)
        {
            MessageBox(_T("Not enough memory."),_T("Error"),MB_OK|MB_ICONWARNING);
            return;
        }

        changes->address = dwPhysAddx;
        changes->next = 0;
        time((time_t *)&(changes->newTimestamp));
        SYSTEMTIME now;::GetSystemTime(&now);
        changes->newMillis = now.wMilliseconds;
        changes->newValue = val;
        return;
    }

    change *p,*q;

    //Try to find the change in the list
    for(p=0, q=changes; q; p=q, q=q->next){
        if(q->address>=dwPhysAddx){
			break;
		}
	}

    //Not found?
    if(!q || q->address!=dwPhysAddx) {
        change *r=new change;
        if(!r)
        {
            MessageBox(_T("Not enough memory."),_T("Error"),MB_OK|MB_ICONWARNING);
            return;
        }
        if(p){
			p->next=r;
		} else {
			changes=r;
		}
        r->next = q;
        r->address = dwPhysAddx;
        time((time_t *)&(r->newTimestamp));
        ::GetSystemTime(&now);
        r->newMillis=now.wMilliseconds;
        r->newValue=val;
    }
    else
    {
        q->newValue=val;
        ::GetSystemTime(&now);
        q->newMillis=now.wMilliseconds;
        time((time_t *)&(q->newTimestamp));
    }
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::IsChanged
Parameters:     [CImageEditorDlg *this]
                DWORD addx
                    The address to check.
Returns:        bool
                    Returns true iff the given address has been changed.
Description:    Checks to see if the given address has been changed.
PDL:
    IF a change structure exists for the given address THEN
        RETURN true
    ELSE
        RETURN false
    ENDIF
**/
bool CImageEditorDlg::IsChanged(DWORD addx) 
{
	DWORD dwPhysAddx = 0;
	bool bValidAddr = true;
	// Convert to physical addresses for retrieving from memory.
	if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
		|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){

		dwPhysAddx = CImageEditorDlg::ConvertACELogical2Physical(addx, bValidAddr);

	} else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 

		dwPhysAddx = CImageEditorDlg::ConvertSXILogical2Physical(addx, bValidAddr);
	}

    const change *p;
    for(p=changes;p && p->address<dwPhysAddx;p=p->next)
        ;
    
    return p && (p->address==dwPhysAddx);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::GetAddx
Parameters:     [CImageEditorDlg *this]
                DWORD addx
                    The address whose record should be retrieved.
                bool fast=false
                    Set to true iff the tiny cache size should be used
                    for loads.
Returns:        const CImageEditorDlg::Record *
                    A pointer to the record at the given address.
Description:    Returns the record at the given address or NULL if none exists.
PDL:
    Round the address
    IF no file is loaded OR the current I/O stream is invalid OR the
      requested address is invalid THEN
        RETURN null
    ENDIF

    IF changes have been made AND the requested address has been changed THEN
        Fill a static record structure with the newer data
        RETURN a pointer to the static structure
    ENDIF

    IF the address is currently in the cache THEN
        IF trying to be fast OR the cache is fully loaded THEN
            RETURN a pointer to the record within the cache
        ENDIF
    ENDIF

    IF allowed to be slow OR the current cache size is smaller than the tiny
      cache size THEN
        Use the entire cache size for loading
    ELSE
        Use the tiny cache size for loading
    ENDIF

    IF the amount is bigger than 1 kB THEN
        Display the busy cursor
    ENDIF

    IF the address is close to the lowest address THEN
        Place most of the cache above the address
    ELSEIF the address is close to the highest address THEN
        Place most of the cache beneath the address
    ELSE
        Center the cache around the address
    ENDIF
    Load the cache in from the file

    IF the amount was bigger than 1 kB THEN
        Clear the busy cursor
    ENDIF
    RETURN the address from the cache
**/
const CImageEditorDlg::Record *CImageEditorDlg::GetAddx(DWORD addx, bool fast)
{
    const change *p;// member structure holds changes to 
    //If something's changed, it has a different structure from an unchanged
    //item, so just keep a small set of static elements here for those changed
    //elements.
    static Record __x__[4];
    static int __i__=0;
	DWORD dwPhysAddx = 0;
	bool bValidAddr = true;

	// Convert to physical addresses for retrieving from memory.
	if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
		|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){

		dwPhysAddx = CImageEditorDlg::ConvertACELogical2Physical(addx, bValidAddr);

	} else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 

		dwPhysAddx = CImageEditorDlg::ConvertSXILogical2Physical(addx, bValidAddr);
	}

    // If the address has been changed, return the changed version:
	p=GetCh(dwPhysAddx);
    if(changed && p)
    {
        __x__[__i__].data=p->newValue;
        __x__[__i__].milliseconds=p->newMillis;
        __x__[__i__].seconds=p->newTimestamp;
        int j=__i__;
        ++__i__;
		__i__&=3;
        mrChanged=dwPhysAddx;
 		// return __x__+j;
		return &__x__[j];	
	}

    mrChanged = 0;
    return &m_Loaded[dwPhysAddx];	
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::GetAddx
Parameters:     [CImageEditorDlg *this]
                DWORD addx
                    The address whose record should be retrieved.
Returns:        const CImageEditorDlg::Record *
                    A pointer to the record at the given address.
Description:    Returns the record at the given address or NULL if none exists.
PDL:
    Round the address
    IF no file is loaded OR the current I/O stream is invalid OR the
      requested address is invalid THEN
        RETURN null
    ENDIF

    IF changes have been made AND the requested address has been changed THEN
        Fill a static record structure with the newer data
        RETURN a pointer to the static structure
    ENDIF

    IF the address is currently in the cache THEN
        IF trying to be fast OR the cache is fully loaded THEN
            RETURN a pointer to the record within the cache
        ENDIF
    ENDIF

    IF allowed to be slow OR the current cache size is smaller than the tiny
      cache size THEN
        Use the entire cache size for loading
    ELSE
        Use the tiny cache size for loading
    ENDIF

    IF the amount is bigger than 1 kB THEN
        Display the busy cursor
    ENDIF

    IF the address is close to the lowest address THEN
        Place most of the cache above the address
    ELSEIF the address is close to the highest address THEN
        Place most of the cache beneath the address
    ELSE
        Center the cache around the address
    ENDIF
    Load the cache in from the file

    IF the amount was bigger than 1 kB THEN
        Clear the busy cursor
    ENDIF
    RETURN the address from the cache
**/
const CImageEditorDlg::Record *CImageEditorDlg::GetAddx(DWORD addx, bool &bValidAddr, bool &bRecMod)
{
    const change *p;// member structure holds changes to 
    //If something's changed, it has a different structure from an unchanged
    //item, so just keep a small set of static elements here for those changed
    //elements.
    static Record __x__[4];
    static int __i__=0;
	DWORD dwPhysAddx = 0;
	bValidAddr = true;
	bRecMod = false;

	// Convert to physical addresses for retrieving from memory.
	if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
		|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){

		dwPhysAddx = CImageEditorDlg::ConvertACELogical2Physical(addx, bValidAddr);

	} else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 

		dwPhysAddx = CImageEditorDlg::ConvertSXILogical2Physical(addx, bValidAddr);
	}

    // If the address has been changed, return the changed version:
	p=GetCh(dwPhysAddx);
    if(changed && p)
    {
        __x__[__i__].data=p->newValue;
        __x__[__i__].milliseconds=p->newMillis;
        __x__[__i__].seconds=p->newTimestamp;
        int j=__i__;
        ++__i__;
		__i__&=3;
        mrChanged=dwPhysAddx;
		bRecMod = true;
 		// return __x__+j;
		return &__x__[j];	
	}

    mrChanged = 0;
    return &m_Loaded[dwPhysAddx];	
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::GetCh
Parameters:     [CImageEditorDlg *this]
                DWORD addx
                    The address to find.
Returns:        const CImageEditorDlg::change *
                    A pointer to the change structure or NULL if none exists.
Description:    Returns the change structure corresponding to the given
                address, or NULL if none exists.
PDL:
    Find the change at the given address
    IF none exists THEN
        RETURN null
    ELSE
        RETURN a pointer to the change structure
    ENDIF
**/
const CImageEditorDlg::change *CImageEditorDlg::GetCh(DWORD addx)
{
    const change *p;
    for(p=changes;p && p->address<addx;p=p->next) //see <3>
        ;
    return (p && p->address==addx)?p:0;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::ClearChanges
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Removes all changes made to the image.
PDL:
    DOFOR each change made
        Free the change before it in the list
    ENDDO
    Free the last change
    Set the list pointer to NULL
**/
void CImageEditorDlg::ClearChanges()
{
    change *p,*q;
    for(p=0,q=changes;q;p=q,q=q->next)
        if(p) delete p;
    if(p) delete p;
    changes=0;
    changed=false;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::DoListing
Parameters:     [CImageEditorDlg *this]
                DWORD starting
                    The new lowest address on-screen.
Returns:        void
Description:    Fills the list boxes with the table rows starting at the
                requested address.
PDL:
    Round address
    Calculate the number of rows in the list boxes (rounding up)
    Clear the list boxes
    IF no file is open THEN
        RETURN
    ENDIF

    Block message handlers
    DOFOR each row in the table
        Get the record at the address corresponding to the row
        IF the address exists THEN
            Write the address
            Write the value in hex and decimal
            Write the timestamp value
        ELSE
            Write "?|0|0|0|(Error reading record)"
        ENDIF
    ENDDO
    Allow message handlers
**/
void CImageEditorDlg::DoListing(DWORD starting)// see <note 4> at end of file.
{
	CWaitCursor wait;  
    TCHAR changedStr[2]={0,0},timestampStr[22];
    CListBox *boxes[5]={Lst_Changed,Lst_Address,Lst_ValueHex, Lst_ValueDec,Lst_UTCTime};

    CRect rect;
	boxes[0]->GetClientRect(&rect);
    DWORD i=rect.Height()-1;
    DWORD dwItemHght = boxes[0]->GetItemHeight(0);
 //   DWORD num = 1+((i-1)/dwItemHght);
	DWORD num = ((i-1)/dwItemHght);
 //   DWORD actualNum=boxes[0]->GetCount();

    for(i=0;i<5;i++){
        boxes[i]->ResetContent();
		boxes[i]->EnableWindow(TRUE);
	}

    if(!working){ 
		return;
	}

	// 
    bool oldInMsg=inMsg;
    inMsg=true;
    DWORD dwPhysAddx=minDisplayed=starting;
    const Record *cur;
	DWORD caddx=minDisplayed;
	bool bValidAddr = true;
	bool bRecMod    = false;

    for(i=0; (i<num) && (caddx<=highestAddress); i++, caddx++){

	// Need to use physical address space so matches
	// what was stored in the changed object.

	// Verify valid record was returned.
	// If not skip.

	   cur = GetAddx(caddx, bValidAddr, bRecMod);
	//	changedStr[0]=(mrChanged == caddx)?'*':' ';
	   changedStr[0]=(bRecMod) ? '*' : ' ';

  	   if(NULL == cur) {
			boxes[0]->AddString(_T("?"));
			boxes[1]->AddString(_T("0"));
			boxes[2]->AddString(_T("0"));
			boxes[3]->AddString(_T("0"));
			boxes[4]->AddString(_T("(Error reading record)"));
			continue;
		} else if (!bValidAddr){
			// If invalid address, skip it.
			// Want to increment address but stay at same display line
			i--;
			continue;
		}

	// Display in logical address space.
	//	TRACE(_T("physical %#X logical %#X Hex data: %u\n"), dwPhysAddx, caddx, cur);
	// Display is dependent on type of data retrieved.
		TCHAR addxStr[25];
		TCHAR dataHexStr[6], dataDecStr[6];
		if ((CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType)
			|| (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType)){
			_stprintf_s(addxStr, _T("% 5.5X      % 10u"), caddx, caddx);
			_stprintf_s(dataHexStr, _T("% 4.4X"), cur->data);
			_stprintf_s(dataDecStr, _T("% 5.5u"), cur->data);

		}  else if (CImageEditorDlg::SXI_IMAGE == m_strctFileHdr.strtHeader.eType){ 
			
			_stprintf_s(addxStr , _T("%08X     % 9u"), caddx, caddx);
			_stprintf_s(dataHexStr,_T("% 2.2X"),cur->data);
			_stprintf_s(dataDecStr,_T("% 3.3u"),cur->data);
		}

 		// struct tm *then = gmtime((time_t *)&(cur->seconds));
		struct tm then;
		__int64 ltime = cur->seconds; 
		errno_t err = gmtime_s(&then, &ltime);

		//if( NULL != then){
		if (err == 0){
			int millis = cur->milliseconds;
			if(millis > 999){
				millis = 999;
			}
			// _stprintf_s(timestampStr, 22, _T("%04.4d-%03.3d-%02.2d:%02.2d:%02.2d.%03.3d"),
			//    then->tm_year + 1900, then->tm_yday + 1,then->tm_hour, then->tm_min,
			//    then->tm_sec, millis);

			int nErr = _stprintf_s(timestampStr, 22,  _T("%04.4d-%03.3d-%02.2d:%02.2d:%02.2d.%03.3d"),
			       then.tm_year + 1900, then.tm_yday + 1,then.tm_hour, then.tm_min,
			       then.tm_sec, millis);

		} else {
			_tcscpy_s(timestampStr,_T("(Invalid timestamp)"));
		}

		boxes[0]->AddString(changedStr);
		boxes[1]->AddString(addxStr);
		boxes[2]->AddString(dataHexStr);
		boxes[3]->AddString(dataDecStr);
		boxes[4]->AddString(timestampStr);
		// Increment to next address
	}

	// Need to save max value, since SXI data has addresses that are not displayed.
	m_dwMaxDisplayed = caddx - 1;  // Set member variable to max value displayed.
	inMsg=oldInMsg;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::AddAddressEntry
Parameters:     [CImageEditorDlg *this]
                DWORD addx
                    The address to add.
Returns:        void
Description:    Adds an address to the recently-accessed list.
PDL:
    IF the address is already in the list THEN
        Move that entry to the top of the list
    ELSE
        IF the list is full THEN
            Delete the last entry
            Scoot each element in the list towards the end
            Insert the new element at the top
        ELSE
            Scoot each element in the list towards the end
            Insert the new element at the top
        ENDIF
    ENDIF
**/
void CImageEditorDlg::AddAddressEntry(DWORD addx)
{
    int i;
    bool alreadyThere=true;

    for(i=0;i<DEF_NumAddressEntries;i++)
    {
        if(!~addressEntries[i])
            break;
        if(addressEntries[i]==addx)
            break;
    }

    if(!alreadyThere && i==DEF_NumAddressEntries)
            addressEntries[--i]=0xFFFFFFFF;

    for(;i;i--)
        addressEntries[i]=addressEntries[i-1];
    addressEntries[i]=addx;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::AddOpenEntry
Parameters:     [CImageEditorDlg *this]
                const CString &file
                    Name of the file to add (prefix '*' for read-only).
Returns:        void
Description:    Adds the given file to the recently-accessed list.
PDL:
    IF the file is in the list THEN
        Move that entry to the top of the list
    ELSE
        IF the list is full THEN
            Delete the last entry
            Scoot each element in the list towards the end
            Insert the new element at the top
        ELSE
            Scoot each element in the list towards the end
            Insert the new element at the top
        ENDIF
    ENDIF
**/
void CImageEditorDlg::AddOpenEntry(const CString &file)
{
    int i = 0;
    TCHAR *p = _T("");
    bool alreadyThere = false;

    for(i=0;i<DEF_NumOpenEntries;i++)
    {
        if(!openEntries[i])
            break;

        if(!::_tcsicmp(openEntries[i],(LPCTSTR)file))
        {
            alreadyThere=true;
            p=openEntries[i];
            break;
        }
    }

    if(!alreadyThere)
    {
        if(i==DEF_NumOpenEntries)
            delete[] openEntries[--i];
        p=new TCHAR[file.GetLength()+1];
		if(!p){
			return;
		}

		// _tcscpy(p,(LPCTSTR)file);
        int nErr =  _tcscpy_s(p, file.GetLength()+1, (LPCTSTR)file);
    }

    for(;i;i--)
        openEntries[i]=openEntries[i-1];
    openEntries[i] = p;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::UpdateMenu
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Synchronizes the menu and controls to the editor's state.
PDL:
    Enable necessary menu items
    IF a file is loaded THEN
        Enable all applicable menu items and buttons
        Update the check for Update|Enable...
        IF changes have been made THEN
            Allow saves and reversion
        ELSE
            Disallow saves and reversion
        ENDIF
        IF read-only mode THEN
            Disable the edit boxes
        ENDIF
        Clear and refill the Go|Go to Recent menu
    ELSE
        Disable all applicable menu items and buttons
    ENDIF
    Clear and refill the File|Open Recent menu
    Repaint the menu bar
**/
void CImageEditorDlg::UpdateMenu()
{
    CMenu *m,*l;
    int i,n;

    m_Menu.EnableMenuItem(0,MF_BYPOSITION|MF_ENABLED);  //File
    m_Menu.EnableMenuItem(2,MF_BYPOSITION|MF_ENABLED);  //Help

    m=m_Menu.GetSubMenu(0);                             //File
    m->EnableMenuItem(0,MF_BYPOSITION|MF_ENABLED);      //File|Open,,,
    m->EnableMenuItem(1,MF_BYPOSITION|MF_ENABLED);      //File|Open Recent >
    m->EnableMenuItem(5,MF_BYPOSITION|MF_ENABLED);      //File|----------
    m->EnableMenuItem(8,MF_BYPOSITION|MF_ENABLED);      //File|----------
    m->EnableMenuItem(10,MF_BYPOSITION|MF_ENABLED);     //File|Exit
    GetDlgItem(IDC_BTN_FILEOPEN)->EnableWindow();

    l=m_Menu.GetSubMenu(2);

    if(working)
    {
        m_Menu.EnableMenuItem(2,MF_BYPOSITION|MF_ENABLED); //Refresh
        m->EnableMenuItem(4,MF_BYPOSITION|MF_ENABLED);  //File|Save Copy...
        m->EnableMenuItem(6,MF_BYPOSITION|MF_ENABLED);  //File|Export...
        m->EnableMenuItem(7,MF_BYPOSITION|MF_ENABLED);  //File|Print...
        m->EnableMenuItem(9,MF_BYPOSITION|MF_ENABLED);  //File|Close
        l->EnableMenuItem(0,MF_BYPOSITION|MF_ENABLED);  //Refresh|Now
        l->EnableMenuItem(1,MF_BYPOSITION|MF_ENABLED);  //Refresh|Enable...
        if(bEnableAutoRefresh)
            l->CheckMenuItem(1,MF_BYPOSITION|MF_CHECKED);
        else
            l->CheckMenuItem(1,MF_BYPOSITION|MF_UNCHECKED);
        GetDlgItem(IDC_BTN_FILEEXPORT)->EnableWindow();
        GetDlgItem(IDC_BTN_FILEPRINT)->EnableWindow();
        GetDlgItem(IDC_BTN_SAVECOPY)->EnableWindow();
        GetDlgItem(IDC_BTN_GOTOADDRESS)->EnableWindow();
        GetDlgItem(IDC_BTN_NEXTCHANGE)->EnableWindow();
        GetDlgItem(IDC_BTN_PREVCHANGE)->EnableWindow();
        GetDlgItem(IDC_BTN_REFRESH)->EnableWindow();
        if(changed)
        {
            m->EnableMenuItem(2,MF_BYPOSITION|MF_ENABLED);//File|Revert
            if(!bReadOnly)
            {
                m->EnableMenuItem(3,MF_BYPOSITION|MF_ENABLED);//File|Save
                GetDlgItem(IDC_BTN_FILESAVE)->EnableWindow();
            }
        }
        else
        {
            m->EnableMenuItem(2,MF_BYPOSITION|MF_GRAYED);//File|Revert
            m->EnableMenuItem(3,MF_BYPOSITION|MF_GRAYED);//File|Save
            GetDlgItem(IDC_BTN_FILESAVE)->EnableWindow(FALSE);
        }

        GetDlgItem(IDC_EDT_VALUE_HEX)->EnableWindow(!bReadOnly);
        GetDlgItem(IDC_EDT_VALUE_DEC)->EnableWindow(!bReadOnly);
    }
    else
    {
        m_Menu.EnableMenuItem(2,MF_BYPOSITION|MF_GRAYED);//Refresh
        m->EnableMenuItem(2,MF_BYPOSITION|MF_GRAYED);   //File|Revert
        m->EnableMenuItem(3,MF_BYPOSITION|MF_GRAYED);   //File|Save
        m->EnableMenuItem(4,MF_BYPOSITION|MF_GRAYED);   //File|Save Copy...
        m->EnableMenuItem(6,MF_BYPOSITION|MF_GRAYED);   //File|Export...
        m->EnableMenuItem(7,MF_BYPOSITION|MF_GRAYED);   //File|Print...
        m->EnableMenuItem(9,MF_BYPOSITION|MF_GRAYED);   //File|Close
        l->EnableMenuItem(0,MF_BYPOSITION|MF_GRAYED);   //Refresh|Now
        l->EnableMenuItem(1,MF_BYPOSITION|MF_GRAYED);   //Refresh|Enable...
        GetDlgItem(IDC_BTN_FILEEXPORT)->EnableWindow(FALSE);
        GetDlgItem(IDC_BTN_FILEPRINT)->EnableWindow(FALSE);
        GetDlgItem(IDC_BTN_FILESAVE)->EnableWindow(FALSE);
        GetDlgItem(IDC_BTN_SAVECOPY)->EnableWindow(FALSE);
        GetDlgItem(IDC_BTN_GOTOADDRESS)->EnableWindow(FALSE);
        GetDlgItem(IDC_BTN_NEXTCHANGE)->EnableWindow(FALSE);
        GetDlgItem(IDC_BTN_PREVCHANGE)->EnableWindow(FALSE);
        GetDlgItem(IDC_BTN_REFRESH)->EnableWindow(FALSE);
    }

    m=m->GetSubMenu(1);                                 //File|Open Recent >
    for(i=m->GetMenuItemCount();i>=0;i--)
        m->DeleteMenu(i,MF_BYPOSITION);

    CString ind;

    for(i=n=0;i<DEF_NumOpenEntries;i++)
        if(openEntries[i])
        {
            CString s(openEntries[i]);
            if(s[0]=='*')
            {
                s=(1+(LPCTSTR)s);
                s+=_T(" (RO)");
            }
            ++n;
            ind.Format(_T("&%d\t"),n);
            m->AppendMenu(MF_UNCHECKED|MF_ENABLED|MF_STRING,ID_FILE_OPENRECENT0+i,
                (LPCTSTR)(ind+Ellipsis(s)));
        }

    if(!n)
        m->AppendMenu(MF_UNCHECKED|MF_GRAYED|MF_STRING,IDCANCEL,_T("(No entries)"));

    if(working)
    {
        TCHAR buffer[16];

        m_Menu.EnableMenuItem(1,MF_BYPOSITION|MF_ENABLED);  //Go
        m=m_Menu.GetSubMenu(1);
        m->EnableMenuItem(0,MF_BYPOSITION|MF_ENABLED);      //Go|Go to Address...
        m->EnableMenuItem(1,MF_BYPOSITION|MF_ENABLED);      //Go|Go to Highlighted
        m->EnableMenuItem(2,MF_BYPOSITION|MF_ENABLED);      //Go|Go to Recent >
        m->EnableMenuItem(3,MF_BYPOSITION|MF_ENABLED);      //Go|-------------
        m->EnableMenuItem(4,MF_BYPOSITION|MF_ENABLED);      //Go|Go to Next Change
        m->EnableMenuItem(5,MF_BYPOSITION|MF_ENABLED);      //Go|Go to Previous Change

        m=m->GetSubMenu(2);                                 //Go|Go to Recent >
        for(i=m->GetMenuItemCount();i>=0;i--)
            m->DeleteMenu(i,MF_BYPOSITION);

        for(i=n=0;i<DEF_NumAddressEntries;i++)
            if(addressEntries[i]!=0xFFFFFFFF)
            {
                // _stprintf(buffer,_T("%08.8X"),addressEntries[i]);
				int nErr = _stprintf_s(buffer, 16, _T("%08.8X"), addressEntries[i]);
                ++n;
                ind.Format(_T("&%c\t%s"),"123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"[n],buffer);
                m->AppendMenu(MF_UNCHECKED|MF_ENABLED|MF_STRING,ID_VIEW_GOTORECENT0+i,ind);
            }

        if(!n)
            m->AppendMenu(MF_UNCHECKED|MF_GRAYED|MF_STRING,IDCANCEL,_T("(No entries)"));
    }
    else
    {
        m_Menu.EnableMenuItem(1,MF_BYPOSITION|MF_GRAYED);   //Go
        m=m_Menu.GetSubMenu(1);
        m->EnableMenuItem(0,MF_BYPOSITION|MF_GRAYED);       //Go|Go to Address
        m->EnableMenuItem(1,MF_BYPOSITION|MF_GRAYED);       //Go|Go to Highlighted
        m->EnableMenuItem(2,MF_BYPOSITION|MF_GRAYED);       //Go|Go to Recent >
        m->EnableMenuItem(3,MF_BYPOSITION|MF_GRAYED);       //Go|-------------
        m->EnableMenuItem(4,MF_BYPOSITION|MF_GRAYED);       //Go|Go to Next Change
        m->EnableMenuItem(5,MF_BYPOSITION|MF_GRAYED);       //Go|Go to Previous Change
    }

    m=m_Menu.GetSubMenu(3);                             //Help
    m->EnableMenuItem(0,MF_BYPOSITION|MF_ENABLED);      //Help|Contents
    m->EnableMenuItem(1,MF_BYPOSITION|MF_ENABLED);      //Help|On Item...
    DrawMenuBar();
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::DrawBtnApply
Parameters:     [CImageEditorDlg *this]
                bool down
                    true iff the button is currently depressed.
Returns:        void
Description:    Draws the Apply button
PDL:
    Clear the button's background
    IF down THEN
        Shift the image to the right and down
        Reverse the border colors
    ELSEIF no file is loaded OR it's loaded read-only THEN
        Use "disabled" colors
    ENDIF
    Draw the filled outline
    Draw a gray border around the image
**/
void CImageEditorDlg::DrawBtnApply(bool down)
{
    CClientDC dc(this);
    CRect &cr=m_bApply;
    int middle,top;
    COLORREF dark=::GetSysColor(COLOR_3DSHADOW);
    COLORREF light=::GetSysColor(COLOR_3DLIGHT);
    CPen pen(PS_SOLID,1,::GetSysColor(COLOR_3DSHADOW));
    
    //If it's depressed, flop dark and light to make it look ... down.
    if(down)
    {
        register DWORD t=dark;
        dark=light;
        light=t;
    }
    CPen dkPen(PS_SOLID,1,dark);
    CPen ltPen(PS_SOLID,1,light);

    CBrush bgBrush;
    CBrush bgBrush2;
    bgBrush.CreateSysColorBrush(COLOR_BTNFACE);
    dc.FillRect(cr,&bgBrush);

    middle=(cr.left+cr.right)>>1;
    top=cr.top+3;

    if(down) {middle++;top++;}
    POINT points[11]=
    {{middle-2,top},    {middle-8,top+6},   {middle-5,top+6},
    {middle-5,top+12},  {middle+1,top+18},  {middle+1,top+6},
    {middle+4,top+6},
    {middle+1,top+12},  {middle+1,top+18},  {middle+7,top+18},
    {middle+7,top+12}};

    if(!working || bReadOnly)
    {
        bgBrush2.CreateSysColorBrush(COLOR_3DSHADOW);
        dc.SelectObject(&bgBrush2);
        dc.SelectObject(&pen);
    }
    else
    {
        dc.SelectStockObject(WHITE_BRUSH);
        dc.SelectStockObject(BLACK_PEN);
    }
    //Draw the left bit with the arrowhead
    dc.Polygon(points,7);
    //Draw the right bit without the arrowhead
    dc.Polygon(points+7,4);

    //Expand by one pixel for the light/dark excitement
    points[0].y--;
    points[1].x--;points[1].y++;
    points[2].x--;points[2].y++;
    points[4].y++;
    points[9].x++;points[9].y++;
    points[10].x++;points[10].y--;
    points[7].x++;points[7].y--;
    points[5].x++;points[5].y++;
    points[6].x++;points[6].y++;
    
    dc.SelectObject(&ltPen);dc.MoveTo(points[0]);
        dc.LineTo(points[1].x,points[1].y-1);dc.LineTo(points[1]);
    dc.SelectObject(&dkPen);dc.LineTo(points[2]);points[2].y++;
        dc.LineTo(points[2]);
    points[3].x--;
        dc.SelectObject(&ltPen);dc.LineTo(points[3]);
        dc.MoveTo(points[3]);dc.LineTo(points[3]);
        points[3].x++;points[3].y++;
        dc.LineTo(points[3]);
    dc.SelectObject(&dkPen);dc.LineTo(points[4]);dc.LineTo(points[9]);
        dc.LineTo(points[10]);
    dc.SelectObject(&ltPen);dc.LineTo(points[7]);
    dc.SelectObject(&dkPen);dc.LineTo(points[5]);dc.LineTo(points[6]);
        dc.LineTo(points[6].x,points[6].y-1);dc.LineTo(points[0]);
    dc.SelectStockObject(NULL_BRUSH);
    dc.SelectStockObject(NULL_PEN);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::DrawBtnsTopBot
Parameters:     [CImageEditorDlg *this]
                bool topbtn
                    Set to true to draw the To-Top button.
                    Set to false to draw the To-Bottom button.
                bool down
                    true iff the button is depressed.
Returns:        void
Description:    Draws a To-Top or To-Bottom button.
PDL:
    Clear the button's background
    IF down THEN
        Shift the image to the right and down
        Reverse the border colors
    ELSEIF no file is loaded OR it's loaded read-only THEN
        Use "disabled" colors
    ENDIF
    Draw the image
    Draw the button rectangle around the images
**/
void CImageEditorDlg::DrawBtnsTopBot(bool topbtn,bool down)
{
    CClientDC dc(this);
    int i,j,middle,lf,rt,top,bot,xd;
    COLORREF dark=::GetSysColor(COLOR_3DSHADOW);
    COLORREF light=::GetSysColor(COLOR_3DHIGHLIGHT);
    CRect &cr=topbtn?m_bTop:m_bBot;
    CPen pen(PS_SOLID,1,::GetSysColor(COLOR_3DSHADOW));
    
    CBrush bgBrush,bgBrush2;
    bgBrush.CreateSysColorBrush(COLOR_BTNFACE);
    dc.FillRect(cr,&bgBrush);

    middle=((cr.left+cr.right)>>1)-1;
    top=cr.top+6;

    xd=1+(cr.right-cr.left)/6;
    if(down)
    {
        register DWORD q=dark;
        dark=light;
        light=q;
        middle++;top++;
    }
    if(!topbtn)
        top++;
    lf=middle-xd;
    rt=middle+xd;
    bot=top+xd;
    
    dc.Draw3dRect(cr,light,dark);
    cr.DeflateRect(1,1,1,1);
    dc.Draw3dRect(cr,light,dark);
    cr.InflateRect(1,1,1,1);

    dc.SelectStockObject(BLACK_PEN);
    if(working)
        dc.SelectStockObject(BLACK_BRUSH);
    else
    {
        bgBrush2.CreateSysColorBrush(COLOR_3DSHADOW);
        dc.SelectObject(&bgBrush2);
        dc.SelectObject(&pen);
    }

    POINT pts[3];

    bot-=2;top-=2;lf++;rt--;top++;
    pts[0].x=lf;pts[1].x=middle;pts[2].x=rt;
    if(topbtn)  {pts[0].y=bot;pts[1].y=top;pts[2].y=bot;}
    else        {pts[0].y=top;pts[1].y=bot;pts[2].y=top;}

    for(j=0;j<2;j++)
    {
        dc.Polygon(pts,3);
        for(i=0;i<3;i++)
            pts[i].y+=4;
    }
    dc.SelectStockObject(NULL_BRUSH);
    dc.SelectStockObject(NULL_PEN);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::DrawSizer
Parameters:     [CImageEditorDlg *this]
                bool down
                    true iff the sizer is currently being held down.
Returns:        void
Description:    Draws the sizer control at the bottom of the window.
PDL:
    Clear the control's background
    IF down THEN
        Reverse the border colors
    ENDIF
    Draw the image
    IF held down by the right mouse button THEN
        Draw two lines to the sides
    ENDIF
**/
void CImageEditorDlg::DrawSizer(bool down)
{
    COLORREF light=::GetSysColor(COLOR_3DHIGHLIGHT);
    COLORREF dark=::GetSysColor(COLOR_3DSHADOW);
    CBrush bgBrush;
    bgBrush.CreateSysColorBrush(COLOR_BTNFACE);
    CClientDC dc(this);
    int cx,l2y;

    cx=(m_cSizer.left+m_cSizer.right)>>1;
    l2y=(m_cSizer.top+m_cSizer.bottom)>>1;

    if(down)
    {
        register COLORREF x=light;
        light=dark;dark=x;
    }

    m_cSizer.InflateRect(0,1,0,1);
    dc.FillRect(m_cSizer,&bgBrush);
    m_cSizer.DeflateRect(0,1,0,1);
    dc.SelectObject(&bgBrush);
    if(!down) dc.Draw3dRect(m_cSizer,light,dark);

    if(down) {cx++;l2y++;}
    dc.Draw3dRect(cx-10,l2y-4,21,3,light,dark);
    dc.Draw3dRect(cx-6, l2y-1,13,3,light,dark);
    dc.Draw3dRect(cx-3, l2y+2,7, 3,light,dark);
    if(m_bSizerDownR)
    {
        dc.Draw3dRect(cx-15,l2y-5,3,10,light,dark);
        dc.Draw3dRect(cx+14,l2y-5,3,10,light,dark);
    }
    dc.SelectStockObject(NULL_BRUSH);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::ResizeElem
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Resizes the elements of the window so that they fit.
PDL:
    Adjust the heights of the list boxes in the table
    Adjust the height of the scroll bar and position of the To-Bottom button
    Adjust the positions of the edit boxes, Apply button, and sizer
**/
void CImageEditorDlg::ResizeElem()
{
    CRect rWindow;
    CRect r;
    int ygap,xgap,sgap,ehgt,listBottom,bhgt,lhgt;
    int x,y,w,w0,w1,w2,w3,w4;
    const register DWORD flags=SWP_FRAMECHANGED|SWP_NOACTIVATE|SWP_NOZORDER;
    CWnd *p;

    //(p=Lst_Changed)->GetWindowRect(&r);
	p = Lst_Changed;
	p->GetWindowRect(&r);
	ScreenToClient(&r);
    Edt_Address->GetWindowRect(&rWindow);
	ScreenToClient(&rWindow);
    xgap=rWindow.left-r.right;
    ygap=rWindow.top-r.bottom;
    ehgt=rWindow.Height();

    sgap=m_cSizer.Height()+1;
    bhgt=m_bBot.Height();

    GetClientRect(&rWindow);
    y=rWindow.top=r.top;
    x=rWindow.left=r.left;

    //Lst_Changed
    listBottom = r.bottom = rWindow.bottom-(ehgt + ygap + m_cSizer.Height() + 3);
    p->SetWindowPos(NULL, x, y, w0=r.Width(), lhgt=r.Height(), flags);
    x += w0 + xgap;

    //Lst_Address
//  (p=Lst_Address)->GetWindowRect(&r);ScreenToClient(&r);
	p = Lst_Address;
	p->GetWindowRect(&r);
	ScreenToClient(&r);
    
	p->SetWindowPos(NULL,x,y,w1=r.Width(),lhgt,flags);
    x+=w1+xgap;

    //Lst_ValueHex
    (p=Lst_ValueHex)->GetWindowRect(&r);
	ScreenToClient(&r);
    p->SetWindowPos(NULL,x,y,w2=r.Width(),lhgt,flags);
    x+=w2+xgap;

    //Lst_ValueDec, Edt_ValueDec
    (p=Lst_ValueDec)->GetWindowRect(&r);
	ScreenToClient(&r);
    p->SetWindowPos(NULL,x,y,w3=r.Width(),lhgt,flags);
    x+=w3+xgap;

    //Lst_UTCTime
    (p=Lst_UTCTime)->GetWindowRect(&r);
	ScreenToClient(&r);
    p->SetWindowPos(NULL,x,y,w4=r.Width(),lhgt,flags);
    x+=w4+xgap;

    //Scr_Address
    (p=Scr_Address)->GetWindowRect(&r);
	ScreenToClient(&r);
    r.bottom=listBottom-bhgt-1;
    p->SetWindowPos(NULL,x,r.top,(w=r.Width()),r.Height(),flags);
    w4+=w+xgap;

    //Btn_Bot/FakeBtn_Bot
    r.top=r.bottom+1;r.bottom=listBottom;
    m_bBot=r;
    FakeBtn_Bot->SetWindowPos(NULL,r.left,r.top,w,bhgt,flags);

    x=rWindow.left;
    y=listBottom+ygap;
    //Btn_Apply/FakeBtn_Apply
    (p=Edt_Address)->GetWindowRect(&r);ScreenToClient(&r);
    m_bApply.SetRect(x,y,x+w0,y+ehgt);
    FakeBtn_Apply->SetWindowPos(NULL,x,y,w0,ehgt,flags);
    x+=w0+xgap;

    //Edt_Address
    p->SetWindowPos(NULL,x,y,w1,ehgt,flags);
    x+=w1+xgap;

    //Edt_ValueHex
    Edt_ValueHex->SetWindowPos(NULL,x,y,w2,ehgt,flags);
    x+=w2+xgap;

    //Edt_ValueDec
    Edt_ValueDec->SetWindowPos(NULL,x,y,w3,ehgt,flags);
    x+=w3+xgap;

    //Edt_UTCTime
    Edt_UTCTime->SetWindowPos(NULL,x,y,w4,ehgt,flags);

    //The sizer
    m_cSizer.bottom=rWindow.bottom-1;
    m_cSizer.top=y+ehgt+2;
}

////////////////////////////////////////////////////////////////////////////////
// Static member functions            //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CImageEditorDlg::Ellipsis (static)
Parameters:     const CString &p
                    The string to truncate
                int n
                    The maximum length of the string
Returns:        void
Description:    Clips a string's length by inserting an ellipsis.
PDL:
    IF the string's length is under the maximum length THEN
        RETURN a copy of the string
    ENDIF

    Place the first (n/2)-3 characters in the output string
    Add "..."
    Place the last (n/2) characters in the output string
**/
CString CImageEditorDlg::Ellipsis(const CString &p,int n)
{
    TCHAR *x=0;
    LPCTSTR s;
    CString rv;
    int i,j,l;

    l=p.GetLength();
    if(l<=n)
        return p;
    x=rv.GetBuffer(n+4);

    s=(LPCTSTR)p;
    n=(n>>1)-4;
    for(i=0;i<n;i++)
        x[i]=s[i];
    x[i++]='.';x[i++]='.';x[i++]='.';
    for(j=i,i=l-n;s[i];i++,j++)
        x[j]=s[i];
    x[j]=0;
    rv.ReleaseBuffer();
    return rv;
}


///////////////////////////////////////
/**
Function:       CImageEditorDlg::ReadFileHdr()
Parameters:     none
                
Returns:        int
					Currently always 0.
					Need to check seek and read status and update return status.

Description:	Reads in the file header.
				Converts values to wintel format.
				Sets member varialbe file header status.     

PDL:

			Go to the beginning of the file.
				Verify seek was successfull.  If not successful, abort and return.
				Return status will be 1 if an error occurred.
			Read the file header status.
				Verify the read was successfull.
				Convert the data to wintel format and set to member variable.
			Return the function status.
**/

int CImageEditorDlg::ReadFileHdr()
{
	int nReturnStatus = 0;
	file_hdr		strctFileHdrTemp;

//	file_hdr		strctFileHdrTemp;

	// Start reading at beginning of file
	int nSeekStatus = fseek(curIO, 0, SEEK_SET); 
	if (0 != nSeekStatus ){
		// verify that fseek was successful
		nReturnStatus = 1; // return with error status set
	} else {

		// Read in file header
		int nReadStatus = fread(&strctFileHdrTemp, sizeof(strctFileHdrTemp), 1, curIO);
		if (0 == nReadStatus ){
			// verify that fread was successful
			nReturnStatus = 1; // return with error status set
		}

		// Convert data in object to WinTel format if set.
		#ifndef BIG_ENDIAN
			m_strctFileHdr.strtHeader.nVersion 
				= BE2LE_D((unsigned)strctFileHdrTemp.strtHeader.nVersion);
			m_strctFileHdr.strtHeader.eType 
				= (image_type)BE2LE_D((unsigned)strctFileHdrTemp.strtHeader.eType);
			m_strctFileHdr.strtHeader.nSize 
				= BE2LE_D((unsigned)strctFileHdrTemp.strtHeader.nSize);
			m_strctFileHdr.strtHeader.nRecords 
				= BE2LE_D((unsigned)strctFileHdrTemp.strtHeader.nRecords);
		#endif
	}
	return nReturnStatus;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::ReadConvTable()
Parameters:     none
                
Returns:        int
					Currently always 0.
					Need to check seek and read status and update return status.

Description: 
				Reads in the address conversion table.	

PDL:
				Move to right after the header.
					Verify seek was successfull.  If not successful, abort and return.
					Return status should be 1 if an error occured.
				Based on the file type read the address conversion table.
					Verify the read was successfull.
					Convert the data to wintel format and set to the member variable.
				Return the function status.
**/

int CImageEditorDlg::ReadConvTable()
{
	int nReturnStatus = 0;
	conv_table unionConv_TableTemp;
	
	// Start reading at after the file header
	int nSeekStatus = fseek(curIO, DEF_IMG_HEAD_SIZE, SEEK_SET);
	if (0 != nSeekStatus ){
		// verify that fseek was successful
		nReturnStatus = 1; // return with error status set
	}

	// verify that fseek was successful
	int nReadStatus = -1;

	// Convert data in object to WinTel format if set.
	#ifndef BIG_ENDIAN
	if (CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType){

		// Read in conversion table
	    nReadStatus = fread(&unionConv_TableTemp, sizeof(unionConv_TableTemp), 1, curIO);
		if (0 == nReadStatus ){
			// verify that fread was successful
			nReturnStatus = 1; // return with error status set
		}
		// Convert data in object to WinTel format.
		for (int i = 0; i < MAX_ADDR_SPACE; i++){
			for (int j = 0; j < DEF_MAX_ADDR_STATE; j++){
				for (int k = 0; k < DEF_MAX_PAGE_REG; k++){
					m_unionConv_Table.ace_table[i][j][k] 
						= BE2LE_D((unsigned)unionConv_TableTemp.ace_table[i][j][k]);
				}
			}
		}
	} else if (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType){

		// Read in conversion table
		nReadStatus = fread(&unionConv_TableTemp, sizeof(unionConv_TableTemp), 1, curIO);
//		if (0 == nReadStatus ){
			// verify that fread was successful
//			nReturnStatus = 1; // return with error status set
//		}
		
		// Convert data in object to WinTel format.
		for (int i = 0; i < MAX_ADDR_SPACE; i++){
			for (int j = 0; j < DEF_MAX_ADDR_STATE; j++){
				for (int k = 0; k < DEF_MAX_PAGE_REG; k++){
					m_unionConv_Table.ace_table[i][j][k] 
						= BE2LE_D((unsigned)unionConv_TableTemp.ace_table[i][j][k]);
				}
			}
		}

	} else if (m_strctFileHdr.strtHeader.eType == CImageEditorDlg::SXI_IMAGE){ 

		// Read in conversion table
	    nReadStatus = fread(&unionConv_TableTemp, sizeof(unionConv_TableTemp), 1, curIO);
//		if (0 == nReadStatus ){
			// verify that fread was successful
//			nReturnStatus = 1; // return with error status set
//		}

		// Convert data in object to WinTel format.
		for (int i = 0; i < DEF_MAX_SXI_TABLES; i++){
			m_unionConv_Table.sxi_table[i].start_addr 
				= BE2LE_D((unsigned)unionConv_TableTemp.sxi_table[i].start_addr);
			m_unionConv_Table.sxi_table[i].size 
				= BE2LE_D((unsigned)unionConv_TableTemp.sxi_table[i].size);
		}
	}	else {
		// error file type is not valid need to post a message and abort reading.
		nReturnStatus = -1;
	}

	#endif

	return nReturnStatus;
}


///////////////////////////////////////
/**
Function:       CImageEditorDlg::ConvertACELogical2Physical
Parameters:		unsigned nLogAddr - logical address
				bool &bValidAddr  - flag for valid/invalid address
               
Returns:        unsigned 
                    Physical address converted from the logical address.
					If physical address is invalid, this value is undefined

Description:    Converts the logical address passed in to a physical address.

PDL:
		Retrieve address space (Instruction/Operand) from dialog window.
		Use the first four bits of the logical address and memeory space
		to determine what memory mapping register to use.
		Replace the first four bits of the address with the page from
		the page register.  (extracted from SSGS Build 9 Working Group 2)
**/

unsigned CImageEditorDlg::ConvertACELogical2Physical(unsigned nLogAddr, bool &bValidAddr)
{
	unsigned nAddr_state = 0;
	unsigned nPage_reg   = 0;
	unsigned nOffset     = 0;
	unsigned nBase       = 0;
	unsigned nPhysical   = 0;
	bValidAddr			 = true; 
	
	nAddr_state = nLogAddr >> 16;
	nPage_reg   = (nLogAddr & 0x0F000) >> 12;
	nOffset     = nLogAddr & 0x00FFF;
	nBase       = m_unionConv_Table.ace_table[m_nAddrSpace][nAddr_state][nPage_reg];
	nPhysical   = nBase + nOffset;


	// Check if physical address is within the file.
	if (nPhysical > (unsigned)m_strctFileHdr.strtHeader.nRecords){
		bValidAddr = false;
		nPhysical = 0;
	}
	
	return nPhysical;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::ConvertSXILogical2Physical

Parameters:     unsigned nLogAddr - logical address
				bool &bValidAddr  - flag for valid/invalid address
                
Returns:        unsigned 
                    the converted SXI physical address

Description:    Converts the logical SXI address to a physical address.

  PDL:
    If the logical addresss is less than 0x01000000
		logical equals physical
	Else
		Calculate the table number by masking off the logical address 
		and shifting right two bytes.
		The offset is determined by the four most significant bytes.
		Use the table number to determine the base address from the 
		sxi table. 
		Add the base to the offsst to calculate the physical address.
**/

unsigned CImageEditorDlg::ConvertSXILogical2Physical(unsigned nLogAddr, bool &bValidAddr)
{
	unsigned nPhysical   = 0;
//	unsigned nPage_reg   = 0;
//	unsigned nOffset     = 0;
//	unsigned nBase		 = 0;
//	unsigned nTableNum   = 0;
	bValidAddr			 = true; 

	/*
		#define DEF_SXI_HIGHESTADDRESS  0x012F4E1F;
		#define DEF_SXI_LOWESTLOGADDR   0x01000000;
		#define DEF_SXI_HIGHESTPHYSADDR 0x3FFFFF;
		#define DEF_SXI_LOWESTADDRESS   0x0;
  */

//	unsigned dwSXI_HIGHESTADDRESS  = 0x012F4E1F;
	unsigned dwSXI_LOWESTLOGADDR   = 0x01000000;
	unsigned dwSXI_HIGHESTPHYSADDR = 0x3FFFFF;
//    unsigned dwSXI_LOWESTADDRESS   = 0x0;

	if (nLogAddr <= dwSXI_HIGHESTPHYSADDR) {
		nPhysical  = nLogAddr;
	} else if (nLogAddr >= dwSXI_LOWESTLOGADDR) {
		int nTableNum = (nLogAddr & 0x00FF0000)>> 16;
		int nOffset = nLogAddr & 0x0000FFFF;
		int nBase = (unsigned)m_unionConv_Table.sxi_table[nTableNum].start_addr;
		int nVal = m_unionConv_Table.sxi_table[nTableNum].size - nOffset;
		if ((-1 == nBase) || (nVal < 0)){
			// Invalid logical address, don't display
			nPhysical = 0;
			bValidAddr = false;
		} else {
			nPhysical = nBase + nOffset;
		}
	} else {
		nPhysical  = 0;
		bValidAddr = false;
	}

	if (nPhysical > (unsigned)m_strctFileHdr.strtHeader.nRecords){
		bValidAddr = false;
	}
	
	return nPhysical;
}


///////////////////////////////////////
/**
Function:       void CImageEditorDlg::OnAddrSpaceRadio() 
Parameters:     none
                
                   
Returns:        void
                    
Description:    Checks the address space radio button and sets the member variable.
PDL:
		If operand radio button is checked
			set member variable to 0
		Else if instruction radio button is checked
			set memeber variable to 1

**/
void CImageEditorDlg::OnAddrSpaceRadio() 
{
	if (((CButton*)GetDlgItem(IDC_RADIO_OPERAND))->GetCheck()){
		m_nAddrSpace = 0;
	} else if (((CButton*)GetDlgItem(IDC_RADIO_INSTRUCT))->GetCheck()) {
		m_nAddrSpace = 1;
	}

	// Need to refresh list if Address space is changed.
	CImageEditorDlg::DoListing(minDisplayed);
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::WriteFileHdr()
Parameters:     none
                
Returns:        int
					Currently always 0.
					Need to check seek and read status and update return status.

Description:	Reads in the file header.
				Converts values to wintel format.
				Sets member varialbe file header status.     

PDL:

			Go to the beginning of the file.
				Verify seek was successfull.  If not successful, abort and return.
				Return status should be 1 if an error occurred.
			Read the file header status.
				Verify the read was successfull.
				Convert the data to wintel format and set to member variable.
			Return the function status.
**/
int CImageEditorDlg::WriteFileHdr()
{

	int nReturnStatus = 0;

	file_hdr		strctFileHdrTemp;

	// Convert data in object to Sun format.
	strctFileHdrTemp.strtHeader.nVersion 
		= LE2BE_D((unsigned)m_strctFileHdr.strtHeader.nVersion);
	
	strctFileHdrTemp.strtHeader.eType 
		= (image_type)BE2LE_D((unsigned)m_strctFileHdr.strtHeader.eType);
	
	strctFileHdrTemp.strtHeader.nSize 
		= LE2BE_D((unsigned)m_strctFileHdr .strtHeader.nSize);

	strctFileHdrTemp.strtHeader.nRecords 
		= LE2BE_D((unsigned)m_strctFileHdr.strtHeader.nRecords);

	// Start reading at beginning of file
//	int nSeekStatus = fseek(output, 0, SEEK_SET); 
	// verify that fseek was successful
	// Read in file header
//    int nReadStatus = fwrite(&strctFileHdrTemp, sizeof(strctFileHdrTemp), 1, output);
	// verify that fread was successful

	return nReturnStatus;
}

///////////////////////////////////////
/**
Function:       CImageEditorDlg::WriteConvTable()
Parameters:     none
                
Returns:        int
					Currently always 0.
					Need to check seek and read status and update return status.

Description: 
				Reads in the address conversion table.	

PDL:
				Move to right after the header.
					Verify seek was successfull.  If not successful, abort and return.
					Return status should be 1 if an error occured.
				Based on the file type read the address conversion table.
					Verify the read was successfull.
					Convert the data to wintel format and set to the member variable.
				Return the function status.
**/


int CImageEditorDlg::WriteConvTable()
{

	int nReturnStatus = 0;
	conv_table unionConv_TableTemp;
	
	// Start writing after the file header
//	int nSeekStatus = fseek(output, DEF_IMG_HEAD_SIZE, SEEK_SET);
	// verify that fseek was successful

//	int nWriteStatus = -1;

	if (CImageEditorDlg::ACE1_IMAGE == m_strctFileHdr.strtHeader.eType){

		// Convert data in object to Sun format.
		for (int i = 0; i < MAX_ADDR_SPACE; i++){
			for (int j = 0; j < DEF_MAX_ADDR_STATE; j++){
				for (int k = 0; k < DEF_MAX_PAGE_REG; k++){
					unionConv_TableTemp.ace_table[i][j][k] 
						= LE2BE_D((unsigned)m_unionConv_Table.ace_table[i][j][k]);
				}
			}
		}

		// Read in conversion table
//		nWriteStatus = fwrite(&unionConv_TableTemp.ace_table, sizeof(unionConv_TableTemp.ace_table), 1, output);

	} else if (CImageEditorDlg::ACE2_IMAGE == m_strctFileHdr.strtHeader.eType){

		// Convert data in object to SUN format.
		for (int i = 0; i < MAX_ADDR_SPACE; i++){
			for (int j = 0; j < DEF_MAX_ADDR_STATE; j++){
				for (int k = 0; k < DEF_MAX_PAGE_REG; k++){
					unionConv_TableTemp.ace_table[i][j][k] 
						= LE2BE_D((unsigned)m_unionConv_Table.ace_table[i][j][k]);
				}
			}
		}

		// Write out conversion table
//		nWriteStatus = fwrite(&unionConv_TableTemp.ace_table, sizeof(unionConv_TableTemp.ace_table), 1, output);

	} else if (m_strctFileHdr.strtHeader.eType == CImageEditorDlg::SXI_IMAGE){ 

		// Convert data in object to Sun format.
		for (int i = 0; i < DEF_MAX_SXI_TABLES; i++){
			unionConv_TableTemp.sxi_table[i].start_addr 
				= LE2BE_D((unsigned) m_unionConv_Table.sxi_table[i].start_addr);
			unionConv_TableTemp.sxi_table[i].size 
				= LE2BE_D((unsigned)  m_unionConv_Table.sxi_table[i].size);
		}

		// Read in conversion table
//	    nWriteStatus = fwrite(&unionConv_TableTemp.sxi_table, sizeof(unionConv_TableTemp.sxi_table), 1, output);
		// verify that fread was successful

	}	else {
		// error file type is not valid need to post a message and abort reading.
		nReturnStatus = -1;
	}	
	return nReturnStatus;
}







////////////////////////////////////////////////////////////////////////////////
// Helper Class Functions             //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#undef super
///////////////////////////////////////
/**
Function:       RegistryKey::Write
Parameters:     [RegistryKey *this]
                LPCTSTR valName
                    The name of the value in the current key.
                DWORD dw
                    The value of the doubleword to write.
Returns:        bool
                    true iff the write was successful.
Description:    Writes the given doubleword to registry.
PDL:
    Set the named registry value to the given doubleword
    IF success THEN
        RETURN true
    ENDIF
    RETURN false
**/
bool RegistryKey::Write(LPCTSTR valName,DWORD dw)
{
    return hkey?(::RegSetValueEx(hkey,valName,0,REG_DWORD,
        (LPBYTE)&dw,sizeof(dw))==ERROR_SUCCESS):false;
}

///////////////////////////////////////
/**
Function:       RegistryKey::Write
Parameters:     [CImageEditorDlg *this]
                LPCTSTR valName
                    The name of the value in the current key.
                LPCTSTR str
                    The string to write to the value.
Returns:        bool
                    true iff the write was successful.
Description:    Writes the given string to the registry.
PDL:
    Set the named registry value to the given string
    IF success THEN
        RETURN true
    ENDIF
    RETURN false
**/
bool RegistryKey::Write(LPCTSTR valName,LPCTSTR str)
{
    return hkey?(::RegSetValueEx(hkey,valName,0,REG_SZ,(LPBYTE)str,
        (_tcslen(str)+1)*sizeof(TCHAR))==ERROR_SUCCESS):false;
}

///////////////////////////////////////
/**
Function:       RegistryKey::Write
Parameters:     [CImageEditorDlg *this]
                LPCTSTR valName
                    The name of the value in the current key.
                const void *p
                    A pointer to the binary data.
                unsigned int size
                    The size, in bytes, of the data.
Returns:        bool
                    true iff the write was successful.
Description:    Writes the given binary data to the registry.
PDL:
    Write the binary data to the named registry value
    IF success THEN
        RETURN true
    ENDIF
    RETURN false
**/
bool RegistryKey::Write(LPCTSTR valName,const void *p,unsigned int size)
{
    return hkey?(::RegSetValueEx(hkey,valName,0,REG_BINARY,
        (LPBYTE)p,size)==ERROR_SUCCESS):false;
}

///////////////////////////////////////
/**
Function:       RegistryKey::Read
Parameters:     [CImageEditorDlg *this]
                LPCTSTR valName
                    The name of the value to read.
                DWORD def
                    The default if the value cannot be read.
Returns:        DWORD
                    The read value (or the default).
Description:    Reads the requested value from the registry.
PDL:
    IF the value can be read from the registry THEN
        RETURN the value
    ELSE
        RETURN the default value
    ENDIF
**/
DWORD RegistryKey::Read(LPCTSTR valName,DWORD def)
{
    if(!hkey) return def;

    DWORD retval,sz=4,type=REG_DWORD;

    if(::RegQueryValueEx(hkey,valName,0,&type,(LPBYTE)&retval,&sz)==ERROR_SUCCESS
        && type==REG_DWORD)
        return retval;
    else
        return def;
}

///////////////////////////////////////
/**
Function:       RegistryKey::Read
Parameters:     [CImageEditorDlg *this]
                LPCTSTR valName
                    The name of the value to read.
                void *dest
                    The destination buffer.
                unsigned int size
                    The size of the destination buffer.
                void *def
                    A pointer to the default buffer contents or NULL if the
                    buffer should be zeroed by default.
Returns:        void
Description:    Reads binary data from the registry.
PDL:
    IF the value can be read from the registry THEN
        Fill the buffer with the value's contents
    ELSE
        IF the caller gave a default value THEN
            Copy that value into the buffer
        ELSE
            Fill the buffer with 0
        ENDIF
    ENDIF
**/
void RegistryKey::Read(LPCTSTR valName,void *dest,unsigned int size,const void *def)
{
    if(!hkey) {memset(dest,0,size);return;}

    DWORD type=REG_BINARY,sz=size;
    if(::RegQueryValueEx(hkey,valName,0,&type,(LPBYTE)dest,&sz)!=ERROR_SUCCESS || sz!=size)
        if(def) memcpy(dest,def,size);
        else memset(dest,0,size);
}

///////////////////////////////////////
/**
Function:       RegistryKey::Read
Parameters:     [CImageEditorDlg *this]
                LPCTSTR valName
                    The name of the value to read.
                LPCTSTR def
                    The default value of the string.
Returns:        CString
                    The string read from the registry.
Description:    Reads a string from the registry.
PDL:
    IF the value can be read from the registry THEN
        Allocate a buffer for the string
        Read the string
        RETURN the string
    ELSE
        RETURN the default value
    ENDIF
**/
CString RegistryKey::Read(LPCTSTR valName,LPCTSTR def)
{
    if(!hkey) return CString(def);

    DWORD ret,sz=0,type=REG_SZ;
    CString rv;

    ret=::RegQueryValueEx(hkey,valName,0,&type,0,&sz);
    if(ret==ERROR_SUCCESS && type==REG_SZ)
    {
        ret=::RegQueryValueEx(hkey,valName,0,&type,(LPBYTE)rv.GetBuffer(sz),&sz);
        rv.ReleaseBuffer();
        if(ret==ERROR_SUCCESS)
            return rv;
    }
    return CString(def);
}

///////////////////////////////////////
/**
Function:       RegistryKey::Open
Parameters:     [CImageEditorDlg *this]
                HKEY above
                    Handle to the parent key or section of the registry.
                LPCTSTR keyname
                    The name of the key to be opened.
Returns:        bool
                    true iff the key was opened successfully.
Description:    Opens the given registry key for reading/writing.
PDL:
    IF the registry key can be opened THEN
        Open it
        RETURN true
    ELSE
        RETURN false
    ENDIF
**/
bool RegistryKey::Open(HKEY above,LPCTSTR keyname)
{
    if(hkey) {::RegCloseKey(hkey);hkey=0;}
    register DWORD ret;
    DWORD q;
    ret=::RegCreateKeyEx(above,keyname,0,_T(""),REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,0,&hkey,&q);
    return (ret==ERROR_SUCCESS && hkey!=INVALID_HANDLE_VALUE);
}

/* Notes:
 * #<1> lines 2169,3323
 *     The recently-accessed address list is cleared between files.  Uncomment
 *     the #if 0 and #endif lines around these two sections to prevent this.
 *
 * #<2> line 3619
 *     A slight optimization here would be to only load the portion of the
 *     cache that is still unloaded.  It would also be possible to load
 *     pieces of the cache at a time for the tiny loads and keeping a cache
 *     base variable so an address in the cache would be roughly (addx+base)
 *     mod size.
 *
 * #<3> line 3659
 *     This is where the program could potentially bog down incredibly if too
 *     many changes are made at once.  If higher speed is needed, a 3-tier
 *     table/bitmap or even a hash-like setup would probably be much nicer for
 *     larger numbers of changes.
 *
 * #<4> line 3717
 *     DoListing could be greatly optimized, at the expense of code size, if
 *     it used a delete item/add item interface so that scrolls of smaller
 *     numbers of lines didn't refresh the entire list box.  However, this
 *     method is fast enough on relatively recent computers; the only draw-
 *     back is the heavy flicker.
 *
 */


