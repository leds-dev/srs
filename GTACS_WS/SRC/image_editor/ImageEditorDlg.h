/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       ImageEditorDlg.h
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
 		 V2.0        10/04       F. Shaw         Modifications for Virtual memory
												 mapping.  GTACS Build 9
*******************************************************************************/
// Header for the main editor dialog

#if !defined(AFX_IMAGEEDITORDLG_H__7179DD6B_9319_43FF_BF16_CD892419E3A0__INCLUDED_)
#define AFX_IMAGEEDITORDLG_H__7179DD6B_9319_43FF_BF16_CD892419E3A0__INCLUDED_
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "PrintAddxDialog.h"
#include "ExportDialog.h"
#include "GotoAddressDlg.h"

////////////////////////////////////////////////////////////////////////////////
// Definitions                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#define DEF_NumOpenEntries 8
#define DEF_NumAddressEntries 16
#define DEF_RegistrySubkey _T("Software\\ISI\\GOES Image Editor")
// #define DEF_CacheSize 32768           //Max. records in cache (256 kB)
//#define DEF_CacheSize 4194304
//#define DEF_TinyCache 512             //Records in a smaller load (4 kB)
#define DEF_IMG_HEAD_SIZE       512
#define DEF_MAX_ADDR_STATE      16
#define DEF_MAX_PAGE_REG        16
#define DEF_MAX_SXI_TABLES      64
#define DEF_MAX_ACE_RECORDS     655360
#define DEF_MAX_SXI_RECORDS     4194304
#define DEF_ACE_HIGHESTADDRESS  0xFFFFF 
#define DEF_ACE_LOWESTADDRESS   0x0
#define DEF_SXI_HIGHESTADDRESS  0x012F4E1F;
#define DEF_SXI_LOWESTLOGADDR   0x01000000;
#define DEF_SXI_HIGHESTPHYSADDR 0x3FFFFF;
#define DEF_SXI_LOWESTADDRESS   0x0;
#define DEF_ACE_FILEVERSION     1;
#define DEF_SXI_FILEVERSION     1;


////////////////////////////////////////////////////////////////////////////////
// Class Definitions                  //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class CImageEditorDlg:public CDialog
{
    ///////////////////////////////////
    // Friends                       //
    ///////////////////////////////////
    friend class ::CPrintAddxDialog;
    friend class ::CExportDialog;
    friend class ::CGotoAddressDlg;

public:
    ///////////////////////////////////
    // Public structures and types   //
    ///////////////////////////////////


	typedef enum image_type { ACE1_IMAGE, ACE2_IMAGE, SXI_IMAGE, MAX_IMAGES};

	typedef enum addr_space_type {OPERAND, INSTRUCTION, MAX_ADDR_SPACE};

	struct image_hdr{
		int nVersion;
		enum image_type eType;
		int nSize;
		int nRecords;
	};

	struct file_hdr{
		image_hdr strtHeader;
		unsigned char spare[DEF_IMG_HEAD_SIZE - sizeof(image_hdr)];
	};

	struct sxi_conv_table {
		int start_addr;
		int size;
	};

	union conv_table {
		int ace_table[MAX_ADDR_SPACE][DEF_MAX_ADDR_STATE][DEF_MAX_PAGE_REG];
		sxi_conv_table sxi_table[DEF_MAX_SXI_TABLES];
	};

	struct location {
		unsigned long  seconds;
		unsigned short milliseconds;
		unsigned short data;
	};

	struct image_file{
		image_hdr   *hdr;
		conv_table *table;
		location *image;
	};
	
	struct Record                    //A data record on-disk
    {                                //
        DWORD seconds;               //UTC seconds
        WORD milliseconds;           //
        WORD data;                   //
    };                               //
                                     //
    struct __ExportData              //The [Export] saved settings structure
    {                                //
        enum ChkFlags                //
        {                            //
            Header=1,                //x Include file headerx
            ColHeaders=2,            //x Include column headers
            AllSel=4                 //o Export all
        };                           //
        enum Formats                 //
        {                            //
            Text=0,HTML=1,           //o Text   o HTML
            Flex=2,Hex=3             //o Flex   o Hex
        };                           //
        BYTE flags;                  //Uses ChkFlags
        BYTE format;                 //Uses Formats
        CString outputDir;           //Most recent output directory
    };                               //
                                     //
    struct __PrintData               //The [Print] saved settings structure
    {                                //
        enum ChkFlags                //
        {                            //
            PgNums=1,                //x Page numbers
            HdrFirst=2,              //x Column headers   o First page
            HdrEvery=4,              //                   o Every page
            CustPpr=8,               //o Custom paper size
            AllSel=16                //o Export all
        };                           //
        BYTE flags;                  //Uses ChkFlags
        int fontSize;                //Last font size
        union {                      //Printer metrics structure
            struct {                 //
                int lMargin,rMargin; //Left/right margins
                int tMargin,bMargin; //Top/bottom margins
                int pprWid,pprHgt;   //Paper width/height
            };                       //
            int metrics[6];          //For reading/writing with registry
        };                           //
        CString font;                //Most recent font
    };

protected:
    ///////////////////////////////////
    // Protected structures & types  //
    ///////////////////////////////////
    struct change                    //A change overlay
    {
        change *next;
        DWORD address;
        DWORD newTimestamp;
        WORD newMillis;
        WORD newValue;
    };

public:
    ///////////////////////////////////
    // Public member variables       //
    ///////////////////////////////////
    __ExportData ExportData;
    __PrintData PrintData;

public:
	int WriteFileHdr();
	int WriteConvTable();
	Record * GetLogicalAddx(DWORD addx, bool fast=false);
	int LoadFile();
	unsigned ConvertSXILogical2Physical(unsigned nLogAddr, bool &bValidAddr);
	unsigned ConvertACELogical2Physical(unsigned nLogAddr,bool &bValidAddr);
	int ReadConvTable();
	int ReadFileHdr();
    ///////////////////////////////////
    // Public interface functions    //
    ///////////////////////////////////
	CImageEditorDlg(CWnd* pParent=0);
    ~CImageEditorDlg();



protected:
    ///////////////////////////////////
    // Protected member variables    //
    ///////////////////////////////////
    HCURSOR cursHelp,cursNorm;       //Saved OEM cursors
    HCURSOR cursText,cursResize;     //
	HICON m_hIcon;                   //The dialog's icon
    CMenu m_Menu;                    //The menu
                                     //
    UINT tickID;                     //The ID of the timer
    bool inMsg;                      //Blocks message handling
    change *changes;                 //The list of changes
     __time64_t savedSTMTime;        //Last-modified time of the file
                                     //
    CRect m_bApply;                  //Rectangle for Apply button
    bool m_bApplyDown,m_bInApply;    //Button down/mouse in button
    CRect m_bTop;                    //Rectangle for To-Top button
    bool m_bTopDown,m_bInTop;        //
    CRect m_bBot;                    //Rectangle for To-Bottom button
    bool m_bBotDown,m_bInBot;        //
    CRect m_cSizer;                  //Rectangle for resizer button
    bool m_bSizerDown,m_bSizerDownR; //Left/right drag on button
    bool m_bMBDn;                    //Middle button down
    bool m_bDragScrolling;           //Currently dragging the thumb
                                     //
    bool inContextHelp;              //In context help capture loop
    bool chlbDown;                   //In context help + left button down
    CString _owt;                    //Window text before help loop
                                     //
    BOOL bAbort;                     //Abort the copy process
    bool bReadOnly;                  //The file is open as read-only
    bool bEnableAutoRefresh;         //Enable auto-refresh
    int timerInterval,timerCount;    //Set to change seconds between refreshes
                                     //
    bool working,changed;            // A file is loaded | It's been changed
    CString currentFile;             // Path of current file
    FILE *curIO;                     // Current I/O stream
                                     //
    unsigned int winWidth;           // The width of the window
    Record *m_Loaded;                // The cache
    DWORD m_CacheSize;               // Current number of records in cache
    DWORD lowestLoaded;				 // Lowest addresses in cache
	DWORD highestLoaded;             // Highest addresses in cache
    DWORD lowestAddress;             // Lowest address in file
    DWORD highestAddress;            // Highest address in file
    DWORD minDisplayed;              // First address displayed on-screen
    DWORD mrChanged;                 // Most-recently-accessed changed address
                                     // v- Open|Recent entries
    LPTSTR openEntries[DEF_NumOpenEntries];
                                     // v- Go|Go to Recent entries
    unsigned int addressEntries[DEF_NumAddressEntries];
	//

	addr_space_type m_strctAddrSpaceType; // Address space type, set by user
	image_hdr		m_strctImageHdr;	  // image header object
	file_hdr		m_strctFileHdr;		  // File header object
	sxi_conv_table  m_strctSXIConvTable;  // SXI logical to physical memory conversion table
	conv_table		m_unionConv_Table;    // Memory Converison table dependent on type of data
	DWORD			m_dwMaxDisplayed;     // maximum value displayed
//	CObArray		m_strctLocation;

//	location        *m_strctLocation;

protected:
    ///////////////////////////////////
    // Internal functions            //
    ///////////////////////////////////
    void LoadRegistryValues();
    void WriteRegistryValues();
    void DoSave(const CString &,bool copy=false);
    void DoLoad(const CString &);
    void DoListing(DWORD starting);
    void NewEditValue(int n);
    bool IsChanged(DWORD addx);
    const change *GetCh(DWORD addx);
    void SetAddx(DWORD addx,WORD val);
    const Record *GetAddx(DWORD addx,bool fast=false);
	const Record *GetAddx(DWORD addx, bool &bValidAddr, bool &bRecMod);
    void AddAddressEntry(DWORD addx);
    void AddOpenEntry(const CString &file);
    void UpdateMenu();
	void DrawBtnsTopBot(bool top,bool down);
	void DrawBtnApply(bool down);
    void DrawSizer(bool down);
    void ResizeElem();
    void ClearChanges();
    void ScrollTo(DWORD address);
	static CString Ellipsis(const CString &,int maxlen=64);

/**
Function:       CImageEditorDlg::InvdCache (inline)
Parameters:     [CImageEditorDlg *this]
Returns:        void
Description:    Invalidates the record cache for the editor window.
PDL:
    Set the lowest-loaded address to FFFFFFFF and the highest-loaded
    address to 0.
**/ inline void InvdCache() {lowestLoaded=~(highestLoaded=0);}

    ///////////////////////////////////
    // Message map declarations      //
    ///////////////////////////////////
    //{{AFX_MSG(CImageEditorDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	afx_msg void OnSelchangeLstChanged();
	afx_msg void OnSelchangeLstAddress();
	afx_msg void OnSelchangeLstValueHex();
	afx_msg void OnSelchangeLstValueDec();
	afx_msg void OnSelchangeLstUtctime();
	afx_msg void OnChangeEdtValueHex();
	afx_msg void OnChangeEdtValueDec();
    afx_msg void OnMenu_FileExport();
	afx_msg void OnMenu_FileOpen();
	afx_msg void OnMenu_FilePrint();
	afx_msg void OnMenu_FileSaveCopy();
	afx_msg void OnMenu_ViewGotoaddress();
    afx_msg void OnMenu_ViewGotoNextCh();
    afx_msg void OnMenu_ViewGotoPrevCh();
	afx_msg void OnMenu_FileSave();
	afx_msg void OnMenu_HelpIndex();
	afx_msg void OnMenu_FileRevert();
    afx_msg void OnRefreshEnable();
	afx_msg void OnAppExit();
	afx_msg void OnLButtonDown(UINT,CPoint);
	afx_msg void OnLButtonUp(UINT,CPoint);
	afx_msg void OnLButtonDblClk(UINT,CPoint);
	afx_msg void OnMouseMove(UINT,CPoint);
	afx_msg void OnVScroll(UINT,UINT,CScrollBar *);
	afx_msg void OnRButtonDown(UINT,CPoint);
	afx_msg void OnRButtonUp(UINT,CPoint);
    afx_msg BOOL OnMouseWheel(UINT,short,CPoint);
	afx_msg void OnMenu_FileClose();
    afx_msg void OnMenu_ViewGotoHighlighted();
    afx_msg void OnContextMenu(CWnd *,CPoint);
	afx_msg void OnSetFocusEdtValueHex();
	afx_msg void OnSetFocusEdtValueDec();
    afx_msg void OnContextHelp();
	afx_msg BOOL OnHelpInfo(HELPINFO *);
	afx_msg void OnTimer(UINT);
	afx_msg void OnBtnRefresh();
    afx_msg void OnMButtonDown(UINT,CPoint);
    afx_msg void OnMButtonUp(UINT,CPoint);
    afx_msg BOOL OnSetCursor(CWnd *,UINT,UINT);
	afx_msg void OnBtnTop();
	afx_msg void OnBtnBot();
	afx_msg void OnBtnApply();
	afx_msg void OnAddrSpaceRadio();
	//}}AFX_MSG
    virtual void OnCancel();
    virtual void OnOK();
    virtual BOOL OnCmdMsg(UINT,int,void *,AFX_CMDHANDLERINFO *);
#ifdef HAVE_TOOLTIPS
    afx_msg BOOL OnToolTipNeedText(UINT,NMHDR *,LRESULT *);
#endif
    DECLARE_MESSAGE_MAP()

    ///////////////////////////////////
    // ClassWizard insertions        //
    ///////////////////////////////////
	//{{AFX_DATA(CImageEditorDlg)
	enum {IDD=IDD_IMAGEEDITOR_DIALOG};
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

#if 0
	//{{AFX_VIRTUAL(CImageEditorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange *pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
#endif
private:
	int m_nAddrSpace;
};

///////////////////////////////////////
///////////////////////////////////////
class RegistryKey
{
private:
    ///////////////////////////////////
    // Private member variables      //
    ///////////////////////////////////
    HKEY hkey;

public:
    ///////////////////////////////////
    // Public interface functions    //
    ///////////////////////////////////
    bool Open(HKEY under,LPCTSTR name);
    bool Write(LPCTSTR,DWORD);
    bool Write(LPCTSTR,LPCTSTR);
    bool Write(LPCTSTR,const void *,unsigned int);

    DWORD Read(LPCTSTR valName,DWORD def);
    CString Read(LPCTSTR valName,LPCTSTR def=_T(""));
    void Read(LPCTSTR valName,void *dest,unsigned int size,const void *def=0);
    
/**
Function:       RegistryKey::RegistryKey (inline)
Parameters:     [RegistryKey *this]
Returns:        void
Description:    Initializes the registry key object.
**/ RegistryKey() {hkey=0;}

/**
Function:       RegistryKey::RegistryKey (inline)
Parameters:     [RegistryKey *this]
                HKEY under
                    The parent key or section.
                LPCTSTR name
                    The name of the subkey to open.
Returns:        void
Description:    Opens the given key for reading/writing.
**/  RegistryKey(HKEY under,LPCTSTR name) {hkey=0;Open(under,name);}

/**
Function:       RegistryKey::~RegistryKey (inline)
Parameters:     [RegistryKey *this]
Returns:        void
Description:    Closes the registry key if it's open.
**/ ~RegistryKey() {Close();}

/**
Function:       RegistryKey::Close (inline)
Parameters:     [RegistryKey *this]
Returns:        bool
                    true iff the registry key was open.
Description:    Closes the registry key if it's open.
**/ inline bool Close()
        {if(hkey) {::RegCloseKey(hkey);hkey=0;return true;}return false;}

/**
Function:       RegistryKey::Delete (inline)
Parameters:     [RegistryKey *this]
                LPCTSTR val
                    The name of the value to delete.
Returns:        bool
                    true iff the value was successfully deleted.
Description:    Deletes the named registry value.
**/ inline bool RegistryKey::Delete(LPCTSTR val)
        {return hkey?(::RegDeleteValue(hkey,val)==ERROR_SUCCESS):false;}
    
/**
Function:       RegistryKey::IsOpen (inline)
Parameters:     [const RegistryKey *this]
Returns:        bool
                    true iff the registry key was opened.
Description:    Tests whether the object is usable.
**/ inline bool IsOpen() const {return !!hkey;}

/**
Function:       RegistryKey::operator HKEY (inline)
Parameters:     [const RegistryKey *this]
Returns:        HKEY
                    A handle to the registry key.
Description:    Returns a handle to the current registry key.
**/ inline operator HKEY() const {return hkey;}

/**
Function:       RegistryKey::operator bool (inline)
Parameters:     [const RegistryKey *this]
Returns:        bool
                    true iff the registry key is open.
Description:    Tests whether the object is usable.
**/ inline operator bool() const {return !!hkey;}
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IMAGEEDITORDLG_H__7179DD6B_9319_43FF_BF16_CD892419E3A0__INCLUDED_)
