/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       ImageEditorDlg.cpp
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
		 V2.0        10/04       F. Shaw         Modifications for Virtual memory
		                                         mapping.  GTACS Build 9
*******************************************************************************/
// Implementation file for the main editor dialog.

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ImageEditor.h"
#include "PrintAddxDialog.h"
#include "htmlhelp.h"
#include <winspool.h>

////////////////////////////////////////////////////////////////////////////////
// Definitions                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define DEF_PrintTopic _T("print.htm")
#define isM do{if(dism) return;}while(0)
#define _pd CImageEditorDlg::__PrintData
#define _epd editor->PrintData

#undef super
#define super CDialog

////////////////////////////////////////////////////////////////////////////////
// Global declarations                //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
static int CALLBACK fontEnummer(const LOGFONT *logFontData,
                                const TEXTMETRIC *physFontData,
                                unsigned long fontType,LPARAM box);

////////////////////////////////////////////////////////////////////////////////
// Construction/Destruction           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CPrintAddxDialog::CPrintAddxDialog
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Initializes the dialog structure.
**/
CPrintAddxDialog::CPrintAddxDialog(CWnd *pParent)
	:super(CPrintAddxDialog::IDD,pParent)
{       
    //{{AFX_DATA_INIT(CPrintAddxDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
    editor=(CImageEditorDlg *)pParent;
    fontPointSize=10;
    fontFace=_T("Courier New");
    fontInstance=0;
    dism=printing=cancel=printPageNumbers=false;
    printRange=true;
    printColHeaders=2;
    m_bHex=true;
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::~CPrintAddxDialog
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Frees the font allocated to the structure.
**/
CPrintAddxDialog::~CPrintAddxDialog()
{
    if(fontInstance)
        delete fontInstance;
}

#if 0
void CPrintAddxDialog::DoDataExchange(CDataExchange *pDX)
{
	super::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPrintAddxDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}
#endif

////////////////////////////////////////////////////////////////////////////////
// Message Map                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
BEGIN_MESSAGE_MAP(CPrintAddxDialog, super)
	//{{AFX_MSG_MAP(CPrintAddxDialog)
	ON_WM_PAINT()
	ON_CBN_SELCHANGE(IDC_CBO_PAPERTYPE,OnSelChangeCboPaperType)
	ON_WM_KEYUP()
	ON_BN_CLICKED(IDC_RDO_PRINTALL,OnRdoPrintAll)
	ON_BN_CLICKED(IDC_RDO_PRINTRANGE,OnRdoPrintRange)
	ON_BN_CLICKED(IDC_CHK_COLHEADERS,OnChkColHeaders)
	ON_CBN_EDITCHANGE(IDC_CBO_PAPERTYPE,OnRdoPaperSelChange)
	ON_CBN_EDITCHANGE(IDC_CBO_PRINTER,OnEditChangeCboPrinter)
	ON_BN_CLICKED(IDC_BTN_PRINTPROP,OnBtnPrintProp)
	ON_EN_CHANGE(IDC_EDT_LEFTMARGIN,OnChangeEdtPageParams)
	ON_NOTIFY(UDN_DELTAPOS,IDC_SPN_FONTSIZE,OnDeltaPosSpnFontSize)
	ON_CBN_EDITCHANGE(IDC_CBO_SELFONT,OnEditchangeCboSelFont)
	ON_EN_CHANGE(IDC_EDT_RANGEMIN,OnChangeEdtRangeMin)
    ON_EN_CHANGE(IDC_EDT_RANGEMAX,OnChangeEdtRangeMax)
	ON_BN_CLICKED(IDC_BTN_ONSCREEN,OnBtnOnScreen)
	ON_WM_CLOSE()
	ON_EN_SETFOCUS(IDC_EDT_RANGEMIN,OnSetFocusEdtRangemin)
	ON_EN_SETFOCUS(IDC_EDT_RANGEMAX,OnSetFocusEdtRangemax)
	ON_BN_CLICKED(IDC_BTN_CONTEXTHELP,OnContextHelp)
	ON_EN_CHANGE(IDC_EDT_COPIES,OnChangeEdtCopies)
	ON_BN_CLICKED(IDC_CHK_PAGENUMS,OnChkPageNumbers)
	ON_BN_CLICKED(IDC_RDO_HDRFIRST,OnRdoHdrChange)
	ON_CBN_SELCHANGE(IDC_CBO_SELFONT,OnEditchangeCboSelFont)
	ON_BN_CLICKED(IDC_RDO_PAPERSEL,OnRdoPaperSelChange)
	ON_BN_CLICKED(IDC_RDO_HDREVERY,OnRdoHdrChange)
	ON_BN_CLICKED(IDC_RDO_CUSTPPR,OnRdoPaperSelChange)
	ON_EN_CHANGE(IDC_EDT_RGTMARGIN,OnChangeEdtPageParams)
	ON_EN_CHANGE(IDC_EDT_TOPMARGIN,OnChangeEdtPageParams)
	ON_EN_CHANGE(IDC_EDT_BOTMARGIN,OnChangeEdtPageParams)
	ON_EN_CHANGE(IDC_EDT_PAPERWID,OnChangeEdtPageParams)
	ON_EN_CHANGE(IDC_EDT_PAPERHGT,OnChangeEdtPageParams)
	ON_BN_CLICKED(IDC_CHK_ADDXHEX, OnChkAddxHex)
	//}}AFX_MSG_MAP
#ifdef HAVE_TOOLTIPS
    ON_NOTIFY_EX(TTN_NEEDTEXT,0,OnToolTipNeedText)
#endif
END_MESSAGE_MAP()

////////////////////////////////////////////////////////////////////////////////
// Message Handlers                   //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnInitDialog
Parameters:     [CPrintAddxDialog *this]
Returns:        BOOL
                    TRUE to set focus on an internal control item.
Description:    Initializes the dialog and its contents.
PDL:
    Set the icon
    Load settings from the editor's saved configuration
    Set up printer combo
    Update the page count
**/
BOOL CPrintAddxDialog::OnInitDialog() 
{
	super::OnInitDialog();
	
    HICON i=::AfxGetApp()->LoadIcon(IDI_PRINTBOX);
    SetIcon(i,TRUE);
    SetIcon(i,FALSE);

    DisM();
    CheckDlgButton(IDC_CHK_ADDXHEX,TRUE);

    //Set the range text
	CString strRangeText;

//    strRangeText.Format(_T("%08.8X"),editor->minDisplayed);

	strRangeText.Format(_T("%08.8X"),editor->lowestAddress);
    SetDlgItemText(IDC_EDT_RANGEMIN, strRangeText);
 //   if(editor->minDisplayed+1024>editor->highestAddress)
 //       strRangeText.Format(_T("%08.8X"),editor->highestAddress>>1);
 //   else
 //       strRangeText.Format(_T("%08.8X"),(editor->minDisplayed+1023)>>1);

	strRangeText.Format(_T("%08.8X"),editor->highestAddress);
	SetDlgItemText(IDC_EDT_RANGEMAX, strRangeText);

    if(_epd.flags & _pd::AllSel)
        OnRdoPrintAll();
    else
        OnRdoPrintRange();

    //Set up the paper size
    if(_epd.flags & _pd::CustPpr)
    {
        CheckRadioButton(IDC_RDO_PAPERSEL,IDC_RDO_CUSTPPR,IDC_RDO_CUSTPPR);
        CString s;
        s.Format(_T("%d.%d"),DWIPart(_epd.pprWid),DWFPart(_epd.pprHgt));
        SetDlgItemText(IDC_EDT_PAPERWID,s);
        s.Format(_T("%d.%d"),DWIPart(_epd.pprHgt),DWFPart(_epd.pprHgt));
        SetDlgItemText(IDC_EDT_PAPERHGT,s);

        GetDlgItem(IDC_EDT_PAPERWID)->EnableWindow();
        GetDlgItem(IDC_EDT_PAPERHGT)->EnableWindow();
        GetDlgItem(IDC_STC_PDIM1)->EnableWindow();
        GetDlgItem(IDC_STC_PDIM2)->EnableWindow();
        ((CComboBox *)GetDlgItem(IDC_CBO_PAPERTYPE))->SetCurSel(0);
    }
    else
    {
        CheckRadioButton(IDC_RDO_PAPERSEL,IDC_RDO_CUSTPPR,IDC_RDO_PAPERSEL);
        SetDlgItemText(IDC_EDT_PAPERWID,_T("8.50"));
        SetDlgItemText(IDC_EDT_PAPERHGT,_T("11.0"));
        GetDlgItem(IDC_EDT_PAPERWID)->EnableWindow(FALSE);
        GetDlgItem(IDC_EDT_PAPERHGT)->EnableWindow(FALSE);
        GetDlgItem(IDC_STC_PDIM1)->EnableWindow(FALSE);
        GetDlgItem(IDC_STC_PDIM2)->EnableWindow(FALSE);
        CComboBox *ccb=(CComboBox *)GetDlgItem(IDC_CBO_PAPERTYPE);
        if(_epd.pprWid<0 || _epd.pprWid>=ccb->GetCount())
            ccb->SetCurSel(0);
        else
            ccb->SetCurSel(_epd.pprWid);
    }

    //Set up the formatting options
    CheckDlgButton(IDC_CHK_PAGENUMS,printPageNumbers=!!(_epd.flags & _pd::PgNums));
    int b=_epd.flags & (_pd::HdrFirst|_pd::HdrEvery);
    CheckDlgButton(IDC_CHK_COLHEADERS,!!b);
    if(!b || b==_pd::HdrFirst)
        CheckRadioButton(IDC_RDO_HDRFIRST,IDC_RDO_HDREVERY,IDC_RDO_HDRFIRST);
    else
        CheckRadioButton(IDC_RDO_HDRFIRST,IDC_RDO_HDREVERY,IDC_RDO_HDREVERY);

    if(!b)
    {
        GetDlgItem(IDC_RDO_HDRFIRST)->EnableWindow(FALSE);
        GetDlgItem(IDC_RDO_HDREVERY)->EnableWindow(FALSE);
    }

    //Set up margins
	CString s;
    s.Format(_T("%d.%d"),DWIPart(_epd.lMargin),DWFPart(_epd.lMargin));
        SetDlgItemText(IDC_EDT_LEFTMARGIN,s);
    s.Format(_T("%d.%d"),DWIPart(_epd.rMargin),DWFPart(_epd.rMargin));
        SetDlgItemText(IDC_EDT_RGTMARGIN,s);
    s.Format(_T("%d.%d"),DWIPart(_epd.tMargin),DWFPart(_epd.tMargin));
        SetDlgItemText(IDC_EDT_TOPMARGIN,s);
    s.Format(_T("%d.%d"),DWIPart(_epd.bMargin),DWFPart(_epd.bMargin));
        SetDlgItemText(IDC_EDT_BOTMARGIN,s);

    //Set up the font
    fontPointSize=_epd.fontSize;
    fontFace=_epd.font;
    s.Format(_T("%s [%d]"),(LPCTSTR)fontFace,fontPointSize);
    SetDlgItemText(IDC_STC_FONTPREVIEW,s);
    s.Format(_T("Size %02.2d"),fontPointSize);
    SetDlgItemText(IDC_STC_FONTSIZE,s);
    
	fontInstance=new CFont();
    if(fontInstance)
    {
        fontInstance->CreatePointFont(fontPointSize*10,fontFace);
        GetDlgItem(IDC_STC_FONTPREVIEW)->SetFont(fontInstance);
    }

    UpdateFontCombo();
    ((CComboBox *)GetDlgItem(IDC_CBO_SELFONT))->SelectString(0,fontFace);
    m_SpnFontSizePos=fontPointSize;

    //Default to 1 copy
    SetDlgItemText(IDC_EDT_COPIES,_T("1"));

    //Set the help button's icon
    ((CButton *)GetDlgItem(IDC_BTN_CONTEXTHELP))->SetIcon(::AfxGetApp()->LoadIcon(IDI_CONTEXTHELP));

    UpdatePrinterCombo();
    EnaM();
    UpdatePageCount();
    GetDlgItemText(IDC_STC_PAGECOUNT,s);
    SetDlgItemText(IDC_STC_PAGECOUNT,s);
    UpdateHelpTopic(this,DEF_PrintTopic);

#ifdef HAVE_TOOLTIPS
#if HAVE_TOOLTIPS==1
    EnableTrackingToolTips();
#else
    EnableToolTips();
#endif
#endif
    return TRUE;
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnChkPageNumbers
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the "Page Numbers" check box is toggled.
PDL:
    Set the internal variable according to the box's state
    Update the page count
**/
void CPrintAddxDialog::OnChkPageNumbers()
{
    printPageNumbers=!!IsDlgButtonChecked(IDC_CHK_PAGENUMS);
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::UpdatePreview
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called whenever page settings change.
PDL:
    Enable controls
    Restore the window to a pre-print state
**/
void CPrintAddxDialog::UpdatePreview()
{
    EnableEverything();
    GetDlgItem(IDCANCEL)->EnableWindow();
    SetWindowText(_T("Print"));
    SetDlgItemText(IDOK,_T("Print"));
    ((CProgressCtrl *)GetDlgItem(IDC_PRG_PRINTPROG))->SetPos(0);
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnRdoHdrChange
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the header-related radio buttons are clicked
PDL:
    IF "Include column headers" is checked THEN
        IF "Every page" is checked THEN
            Set the internal variable to 2
        ELSE
            Set the internal variable to 1
        ENDIF
    ELSE
        Set the internal variable to 0
    ENDIF
**/
void CPrintAddxDialog::OnRdoHdrChange()
{
    if(IsDlgButtonChecked(IDC_CHK_COLHEADERS))
    {
        if(IsDlgButtonChecked(IDC_RDO_HDREVERY))
            printColHeaders=2;
        else
            printColHeaders=1;
    }
    else
        printColHeaders=0;
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnPaint
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called to repaint the window.
PDL:
    Acknowledge the repaint
**/
void CPrintAddxDialog::OnPaint() 
{
    isM;
	CPaintDC dc(this);
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnSelChangeCboPaperType
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the "Paper type" combo box's selection changes.
PDL:
    Update the page width/height in the edit boxes
    Update the page count
**/
void CPrintAddxDialog::OnSelChangeCboPaperType() 
{
    isM;
    TCHAR buffer[16];
    int w=GetPageWidth();
    _stprintf_s(buffer, 16, _T("%d.%d"),w/100,w%100);
    SetDlgItemText(IDC_EDT_PAPERWID,buffer);
    w=GetPageHeight();
    _stprintf_s(buffer, 16, _T("%d.%d"),w/100,w%100);
    SetDlgItemText(IDC_EDT_PAPERHGT,buffer);
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnRdoPaperSelChange
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when a paper size/type radio button is clicked.
PDL:
    IF the "Custom paper" radio button is checked THEN
        Set focus to the paper width edit control
        Enable the paper width/height edit controls and surrounding
          text
    ELSE
        Update the paper dimension edit boxes to the new paper size
        Disable the paper width/height edit controls and surrounding
          text
    ENDIF
**/
void CPrintAddxDialog::OnRdoPaperSelChange() 
{
    isM;

    BOOL x=IsDlgButtonChecked(IDC_RDO_CUSTPPR);
	if(x){
        GetDlgItem(IDC_EDT_PAPERWID)->SetFocus();
    } else {
        TCHAR buffer[16];
        int w=GetPageWidth();
        _stprintf_s(buffer, 16, _T("%d.%d"),w/100,w%100);
        SetDlgItemText(IDC_EDT_PAPERWID,buffer);
        w=GetPageHeight();
        _stprintf_s(buffer, 16, _T("%d.%d"),w/100,w%100);
        SetDlgItemText(IDC_EDT_PAPERHGT,buffer);
    }
    GetDlgItem(IDC_EDT_PAPERWID)->EnableWindow(x);
    GetDlgItem(IDC_EDT_PAPERHGT)->EnableWindow(x);
    GetDlgItem(IDC_STC_PDIM1)->EnableWindow(x);
    GetDlgItem(IDC_STC_PDIM2)->EnableWindow(x);
    GetDlgItem(IDC_CBO_PAPERTYPE)->EnableWindow(!x);
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnEditChangeCboPrinter
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the selection in the "Printer" combo box changes.
PDL:
    Update the page count
**/
void CPrintAddxDialog::OnEditChangeCboPrinter() 
{
    isM;
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnBtnPrintProp
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the "Print properties" button is clicked
PDL:
    Open the selected printer
    Open printer properties for the printer
    Close the printer
    Update the page count
**/
void CPrintAddxDialog::OnBtnPrintProp() 
{
    HANDLE ph=OpenSelPrinter(PRINTER_ACCESS_ADMINISTER);
    if(!ph)
        return;

    if(!::PrinterProperties(m_hWnd,ph))
        MessageBox(_T("Couldn't open properties for the selected printer."),_T("Error"),MB_OK|MB_ICONWARNING);
    ::ClosePrinter(ph);
    UpdatePageCount();
    UpdateWindow();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnRdoPrintAll
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the "Print all" radio button is clicked.
PDL:
    Ensure that the "Print all" radio button is checked
    Disable the range edit and text controls
    Update the page count
**/
void CPrintAddxDialog::OnRdoPrintAll() 
{
    CheckRadioButton(IDC_RDO_PRINTALL,IDC_RDO_PRINTRANGE,IDC_RDO_PRINTALL);
    GetDlgItem(IDC_EDT_RANGEMIN)->EnableWindow(FALSE);
    GetDlgItem(IDC_STC_PRRANGETO)->EnableWindow(FALSE);
    GetDlgItem(IDC_EDT_RANGEMAX)->EnableWindow(FALSE);
    printRange=false;
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnRdoPrintRange
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the "Print range" radio button is clicked.
PDL:
    Ensure that the "Print range" radio button is checked
    Enable the range edit and text controls
    Set focus to the range minimum edit control
    Update the page count
**/
void CPrintAddxDialog::OnRdoPrintRange() 
{
	CheckRadioButton(IDC_RDO_PRINTALL,IDC_RDO_PRINTRANGE,IDC_RDO_PRINTRANGE);
    CEdit *p=(CEdit *)GetDlgItem(IDC_EDT_RANGEMIN);
    p->EnableWindow(TRUE);
    p->SetFocus();

    GetDlgItem(IDC_STC_PRRANGETO)->EnableWindow(TRUE);
    (p=(CEdit *)GetDlgItem(IDC_EDT_RANGEMAX))->EnableWindow(TRUE);
    printRange=true;
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnChangeEdtRangeMin
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the range minimum is changed.
PDL:
    Disable message handlers
    Ensure that a valid value appears in the edit box
    Update the page count
**/
void CPrintAddxDialog::OnChangeEdtRangeMin() 
{
    isM;   
    register CEdit *e=(CEdit *)GetDlgItem(IDC_EDT_RANGEMIN);

    DisM();
    m_bHex?Doubleword::ValidateHex(e,sz_dword):Doubleword::ValidateDec(e,sz_dword);
    EnaM();
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnChangeEdtRangeMax
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the range maximum is changed
PDL:
    Disable message handlers
    Ensure that a valid range appears in the edit box
    Update the page count
**/
void CPrintAddxDialog::OnChangeEdtRangeMax()
{
    isM;
    register CEdit *e=(CEdit *)GetDlgItem(IDC_EDT_RANGEMAX);

    DisM();
    m_bHex?Doubleword::ValidateHex(e,sz_dword):Doubleword::ValidateDec(e,sz_dword);
    EnaM();
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnSetFocusEdtRangemin
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the range minimum control receives focus.
PDL:
    Select all text in the edit control
**/
void CPrintAddxDialog::OnSetFocusEdtRangemin() 
{
    CEdit *p=(CEdit *)GetDlgItem(IDC_EDT_RANGEMIN);
    p->SetSel(0,p->GetWindowTextLength(),TRUE);
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnSetFocusEdtRangemax
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the range maximum control receives focus
PDL:
    Select all text in the edit control
**/
void CPrintAddxDialog::OnSetFocusEdtRangemax() 
{
    CEdit *p=(CEdit *)GetDlgItem(IDC_EDT_RANGEMAX);
    p->SetSel(0,p->GetWindowTextLength(),TRUE);
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnBtnOnScreen
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the "Addresses on-screen" button is clicked
PDL:
    Set the range minimum to the editor's lowest displayed address
    Calculate the number of rows in the list boxes
    Set the range maximum to the editor's highest displayed address
**/
void CPrintAddxDialog::OnBtnOnScreen() 
{
    isM;
    DisM();

    OnRdoPrintRange();

    // int d;
    DWORD q;
    CRect r;
    CListBox *p=((CListBox *)(editor->GetDlgItem(IDC_LST_CHANGED)));

    CString min,max;
    min.Format(m_bHex?_T("%X"):_T("%u"),editor->minDisplayed);
    SetDlgItemText(IDC_EDT_RANGEMIN,min);
    
    int d = p->GetItemHeight(0);
    p->GetWindowRect(&r);
	editor->ScreenToClient(&r);
 
	d = r.Height()/d;
    // q = editor->minDisplayed + d + d - 2;


	q = editor->m_dwMaxDisplayed;
    if(q>editor->highestAddress){
        q=editor->highestAddress;
	}

    max.Format(m_bHex?_T("%X"):_T("%u"),q);
    SetDlgItemText(IDC_EDT_RANGEMAX,max);
    EnaM();
    printRange=true;
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnChkColHeaders
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the "Include column headers" check box is clicked.
PDL:
    IF the "Include column headers" check box is checked THEN
        Enable the "First page"/"Every page" radio buttons
    ENDIF
**/
void CPrintAddxDialog::OnChkColHeaders() 
{
    BOOL x=IsDlgButtonChecked(IDC_CHK_COLHEADERS);
    GetDlgItem(IDC_RDO_HDRFIRST)->EnableWindow(x);
    GetDlgItem(IDC_RDO_HDREVERY)->EnableWindow(x);
    if(x)
    {
        printColHeaders=1;
        if(IsDlgButtonChecked(IDC_RDO_HDREVERY))
            ++printColHeaders;
    }
    else
        printColHeaders=0;
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnChangeEdtPageParams
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called whenever margins or paper dimensions change.
PDL:
    Disable message handlers
    Ensure that all boxes have valid floating-point values
    Enable message handlers
    Update the page count
**/
void CPrintAddxDialog::OnChangeEdtPageParams() 
{
    isM;
    DisM();
    static bool inCEdit=false;
    if(inCEdit) return;
    inCEdit=true;
    
    Float::Validate((CEdit *)GetDlgItem(IDC_EDT_LEFTMARGIN));
    Float::Validate((CEdit *)GetDlgItem(IDC_EDT_TOPMARGIN));
    Float::Validate((CEdit *)GetDlgItem(IDC_EDT_RGTMARGIN));
    Float::Validate((CEdit *)GetDlgItem(IDC_EDT_BOTMARGIN));
    Float::Validate((CEdit *)GetDlgItem(IDC_EDT_PAPERWID));
    Float::Validate((CEdit *)GetDlgItem(IDC_EDT_PAPERHGT));

    EnaM();
    inCEdit=false;
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnDeltaPosSpnFontSize
Parameters:     [CPrintAddxDialog *this]
                NMHDR *pNMHDR
                    Pointer to the notification message header structure
                LRESULT *pResult
                    Pointer to the result return value
Returns:        void
Description:    Called when the font size spin box is clicked.
PDL:
    Increment or decrement the font size
    Update the font preview text and instance
**/   
void CPrintAddxDialog::OnDeltaPosSpnFontSize(NMHDR *pNMHDR,LRESULT *pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
    if(pNMUpDown->iDelta>0 && m_SpnFontSizePos>4)
        --m_SpnFontSizePos;
    else if(pNMUpDown->iDelta<0 && m_SpnFontSizePos<24)
        ++m_SpnFontSizePos;
    fontPointSize=m_SpnFontSizePos;

    TCHAR buffer[10];

    _stprintf_s(buffer, 16, _T("Size %02.2d"),m_SpnFontSizePos);
	if(buffer[5]=='0'){
		buffer[5]=' ';
	}
    SetDlgItemText(IDC_STC_FONTSIZE,buffer);
	*pResult = 0;
    if(fontInstance) {
		delete fontInstance;
		fontInstance=0;
	}
	
	fontInstance = new CFont();
    if(!fontInstance)
    {
        MessageBox(_T("Not enough memory to update the font."),_T("Error"),MB_OK);
        return;
    }

    fontInstance->CreatePointFont(fontPointSize*10,fontFace);

    GetDlgItem(IDC_STC_FONTPREVIEW)->SetFont(fontInstance);
    CString s;
    s.Format(_T(" [%d]"),fontPointSize);
    SetDlgItemText(IDC_STC_FONTPREVIEW,fontFace+s);
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnEditchangeCboSelFont
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the user selects a font from the dropdown box.
PDL:
    Retrieve the font name
    Create an instance of the given font
    Update the font preview text
**/
void CPrintAddxDialog::OnEditchangeCboSelFont()
{
    GetDlgItemText(IDC_CBO_SELFONT,fontFace);

    if(fontInstance) {
		delete fontInstance;
		fontInstance=0;
	}

	fontInstance = new CFont();
    if(NULL == fontInstance){
        MessageBox(_T("Not enough memory to update the font."),_T("Error"),MB_OK);
        return;
    }
    fontInstance->CreatePointFont(fontPointSize*10,fontFace);

    CString s;
    s.Format(_T(" [%d]"),fontPointSize);
    SetDlgItemText(IDC_STC_FONTPREVIEW,fontFace+s);
    GetDlgItem(IDC_STC_FONTPREVIEW)->SetFont(fontInstance);
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnClose
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Redirects to OnCancel; called when the Close button is clicked.
**/
void CPrintAddxDialog::OnClose() {OnCancel();}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnContextHelp
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Pops up help for the current dialog.
PDL:
    Display the help topic for this window
**/
void CPrintAddxDialog::OnContextHelp() 
{
    DisplayHelpTopic(GetParent(),DEF_PrintTopic);
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnChangeEdtCopies
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Called when the number of copies is changed.
PDL:
    Update the page count
**/
void CPrintAddxDialog::OnChangeEdtCopies() 
{
    UpdatePageCount();
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnKeyUp
Parameters:     [CPrintAddxDialog *this]
                UINT nChar
                    Character/key code
                UINT nRepCnt
                    Repeat count
                UINT nFlags
                    Keyboard/mouse status flags
Returns:        void
Description:    Called when a key is released.  Handles any unhandled
                accelerator keys.
PDL:
    IF an unhandled key is pressed THEN
        Simulate the action the key should take
    ENDIF
    Superclass the function
**/
void CPrintAddxDialog::OnKeyUp(UINT nChar,UINT nRepCnt,UINT nFlags) 
{
    isM;
    register CButton *p;

    if(!(nFlags&KF_REPEAT) && (nFlags&KF_ALTDOWN))
        switch(nChar)
        {
        case 'A':   //Print { all addresses }
            OnRdoPrintAll();return;
        case 'G':   //Print { range }
            OnRdoPrintRange();return;
        case 'O':   //Printer properties
            OnBtnPrintProp();return;
        case 'N':   //Page numbers
            p=(CButton *)GetDlgItem(IDC_CHK_PAGENUMS);
            p->SetCheck(!p->GetCheck());
            UpdatePageCount();
            return;

        case 'I':   //Include column headers
            p=(CButton *)GetDlgItem(IDC_CHK_COLHEADERS);
            p->SetCheck(!p->GetCheck());
            OnChkColHeaders();
            return;

        case 'F':   //First page
            CheckRadioButton(IDC_RDO_HDRFIRST,IDC_RDO_HDREVERY,IDC_RDO_HDRFIRST);
            UpdatePageCount();
            return;

        case 'E':   //Every page
            CheckRadioButton(IDC_RDO_HDRFIRST,IDC_RDO_HDREVERY,IDC_RDO_HDREVERY);
            UpdatePageCount();
            return;

        case 'L':   //Left margin
            GetDlgItem(IDC_EDT_LEFTMARGIN)->SetFocus();
            return;

        case 'R':   //Paper type
            CheckRadioButton(IDC_RDO_PAPERSEL,IDC_RDO_CUSTPPR,IDC_RDO_PAPERSEL);
            OnRdoPaperSelChange();
            GetDlgItem(IDC_CBO_PAPERTYPE)->SetFocus();
            return;

        case 'C':   //Custom paper type
            CheckRadioButton(IDC_RDO_PAPERSEL,IDC_RDO_CUSTPPR,IDC_RDO_CUSTPPR);
            OnRdoPaperSelChange();
            GetDlgItem(IDC_EDT_PAPERWID)->SetFocus();
            return;
        }
	super::OnKeyUp(nChar, nRepCnt, nFlags);
}

void CPrintAddxDialog::OnChkAddxHex() 
{
    m_bHex=!!IsDlgButtonChecked(IDC_CHK_ADDXHEX);
    CString s,t;
    
    GetDlgItemText(IDC_EDT_RANGEMIN,s);
    GetDlgItemText(IDC_EDT_RANGEMAX,t);
    if(m_bHex)
    {
        s.Format(_T("%X"),Doubleword::ParseDecimal(s));
        t.Format(_T("%X"),Doubleword::ParseDecimal(t));
    }
    else
    {
        s.Format(_T("%u"),Doubleword::ParseHex(s));
        t.Format(_T("%u"),Doubleword::ParseHex(t));
    }
    register bool b=dism;
    DisM();
    SetDlgItemText(IDC_EDT_RANGEMIN,s);
    SetDlgItemText(IDC_EDT_RANGEMAX,t);
    dism=b;
}

#ifdef HAVE_TOOLTIPS
BOOL CPrintAddxDialog::OnToolTipNeedText(UINT id,NMHDR *pNMHDR,LRESULT *pResult)
{
    TOOLTIPTEXT *pTTT=(TOOLTIPTEXT *)pNMHDR;

    if(pTTT->uFlags & TTF_IDISHWND)
    {
        UINT nID;
        if(nID=::GetDlgCtrlID((HWND)(pNMHDR->idFrom)))
        {
            switch(nID)
            {
            case IDC_RDO_PRINTALL: pTTT->lpszText=_T("Selects the entire image for printing");break;
            case IDC_RDO_PRINTRANGE: pTTT->lpszText=_T("Selects an address range for printing");break;
            case IDC_EDT_RANGEMIN: pTTT->lpszText=_T("The lower end of the address range");break;
            case IDC_EDT_RANGEMAX: pTTT->lpszText=_T("The higher end of the address range");break;
            case IDC_BTN_ONSCREEN: pTTT->lpszText=_T("Selects the addresses in the main window for printing");break;
            case IDC_CBO_PRINTER: pTTT->lpszText=_T("Selects the printer");break;
            case IDC_BTN_PRINTPROP: pTTT->lpszText=_T("Brings up the selected printer's properties");break;
            case IDC_CHK_PAGENUMS: pTTT->lpszText=_T("Adds page/range numbering to the output");break;
            case IDC_CHK_COLHEADERS: pTTT->lpszText=_T("Adds column headers to the output");break;
            case IDC_RDO_HDRFIRST: pTTT->lpszText=_T("Places column headers only at the top of the first page");break;
            case IDC_RDO_HDREVERY: pTTT->lpszText=_T("Places column headers at the top of every page");break;
            case IDC_CBO_PAPERTYPE:
            case IDC_RDO_PAPERSEL: pTTT->lpszText=_T("Allows selection from several standard paper types");break;
            case IDC_RDO_CUSTPPR: pTTT->lpszText=_T("Allows entry of custom paper sizes");break;
            case IDC_EDT_PAPERWID: pTTT->lpszText=_T("The width of the paper");break;
            case IDC_EDT_PAPERHGT: pTTT->lpszText=_T("The height of the paper");break;
            case IDC_EDT_LEFTMARGIN: pTTT->lpszText=_T("Sets the left margin of the page");break;
            case IDC_EDT_TOPMARGIN: pTTT->lpszText=_T("Sets the top margin of the page");break;
            case IDC_EDT_RGTMARGIN: pTTT->lpszText=_T("Sets the right margin of the page");break;
            case IDC_EDT_BOTMARGIN: pTTT->lpszText=_T("Sets the bottom margin of the page");break;
            case IDC_CBO_SELFONT: pTTT->lpszText=_T("Selects the font for printing");break;

            case IDC_SPN_FONTSIZE:
            case IDC_STC_FONTSIZE: pTTT->lpszText=_T("Selects the font size for printing");break;
            case IDC_EDT_COPIES: pTTT->lpszText=_T("Sets the number of copies to print");break;

            case IDC_STC_FONTPREVIEW: pTTT->lpszText=_T("A preview of the output font");break;
            case IDCANCEL: pTTT->lpszText=_T("Exits without printing");break;
            case IDOK:
                CString s;
                GetDlgItemText(IDOK,s);
                if(s!=CString(_T("Done")))
                    pTTT->lpszText=_T("Begins printing");
                else
                    pTTT->lpszText=_T("Closes the dialog box");
                break;
            }
            return(TRUE);
        }
    }
    return(FALSE);
}
#endif

////////////////////////////////////////////////////////////////////////////////
// Virtual Overrides                  //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OnCancel
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Cancels or aborts printing.
PDL:
    IF currently printing THEN
        Abort printing
    ELSE
        Save dialog settings
        End the dialog and return "cancel"
    ENDIF
**/
void CPrintAddxDialog::OnCancel()
{
    if(printing)
        cancel=true;
    else
    {
        SaveValues();
        EndDialog(IDCANCEL);
    }
}

/**
Function:       CPrintAddxDialog::OnOK
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Begins printing or closes the dialog box.
PDL:
    IF there aren't any printers THEN
        Notify the user
        RETURN
    ENDIF

    IF the print job is done and settings haven't changed THEN
        Save dialog settings
        End the dialog and return "OK"
    ENDIF

    Initialize the dialog box's controls
    Retrieve and adjust the print range
    Read and adjust the margins and page dimensions
    Set up the printer for printing
    DOFOR each requested copy
        Start with the current address at the bottom of the range
        DOWHILE the current address is below the top of the range
            IF on the first page THEN
                Print the header
            ENDIF

            IF (on first page AND column headers requested)
              OR column headers requested on every page THEN
                Print the column headers
            ENDIF

            DOFOR each row on the page
                Print a table row
                Increase the current address
                Pump several messages
                Check for process abortion
            ENDDO

            IF page numbers are requested THEN
                Print the page numbers
            ENDIF
        ENDDO
    ENDDO

**/
void CPrintAddxDialog::OnOK()
{
    CString s;
    int curcopy;
    CWinThread *curThread=::AfxGetThread();
    MSG msg;

    GetDlgItemText(IDOK,s);

    //Are there any printers?
    CComboBox *printsel=(CComboBox *)GetDlgItem(IDC_CBO_PRINTER);
    if(printsel->GetCount()<1)
    {
        MessageBox(_T("No printers have been defined."),_T("Error"),MB_OK);
        return;
    }

    //If the print job has just finished, exit the dialog
    if(s==_T("Done"))
    {
        if(fontInstance) {
			delete fontInstance;fontInstance=0;
		}
        SaveValues();
        EndDialog(IDOK);
        return;
    }

    DisM();
    CProgressCtrl *progress=(CProgressCtrl *)GetDlgItem(IDC_PRG_PRINTPROG);
    progress->SetRange(0,256);

    //Choose relatively safe defaults
    DWORD rangeMin=editor->lowestAddress,rangeMax=editor->lowestAddress+1022;
    DWORD address,cpa;

    //Read the range
    if(printRange)
    {
        Doubleword d;
        rangeMin=rangeMax=0;
        GetDlgItemText(IDC_EDT_RANGEMIN,s);
        if(s.GetLength()<1)
        {
            MessageBox(_T("Please specify the minimum for the address range."),_T("Error"),MB_OK);
            GetDlgItem(IDC_EDT_RANGEMIN)->SetFocus();
            return;
        }
        if(!(m_bHex?d.FromHex(s):d.FromDecimal(s)))
        {
            MessageBox(_T("Invalid minimum value in address range."),_T("Error"),MB_OK);
            GetDlgItem(IDC_EDT_RANGEMIN)->SetFocus();
            return;
        }
        // rangeMin=d<<1;
		rangeMin = d;

        GetDlgItemText(IDC_EDT_RANGEMAX,s);
        if(s.GetLength()<1)
        {
            MessageBox(_T("Please specify the maximum for the address range."),_T("Error"),MB_OK);
            GetDlgItem(IDC_EDT_RANGEMAX)->SetFocus();
            return;
        }
        if(!(m_bHex?d.FromHex(s):d.FromDecimal(s)))
        {
            MessageBox(_T("Invalid maximum value in address range."),_T("Error"),MB_OK);
            GetDlgItem(IDC_EDT_RANGEMAX)->SetFocus();
            return;
        }
        // rangeMax = d<<1;
		rangeMax = d;
    }
    else
    {
        rangeMin=editor->lowestAddress;
        rangeMax=editor->highestAddress;
    }

    EnaM();
    pageInfo pi;
    DWORD page=0;
    UpdatePageCount(&pi);

    if(rangeMin>rangeMax) {register DWORD d=rangeMax;rangeMax=rangeMin;rangeMin=d;}
    if(rangeMin<editor->lowestAddress) rangeMin=editor->lowestAddress;
    if(rangeMax>editor->highestAddress) rangeMax=editor->highestAddress;

    //Read the margins
    int lm,rm,tm,bm;
    {
        margins m;
        m=GetMargins();
        lm=int(m.left*1000.0F);
        rm=int(m.right*1000.0F);
        tm=int(m.top*1000.0F);
        bm=int(m.bottom*1000.0F);
    }

    //Start setting up the printer
    CDC printDC;
    DOCINFO doc;
    HANDLE phandle=OpenSelPrinter(PRINTER_ACCESS_USE);

    if(!phandle) return;

    LPBYTE printInfo=0;
    DWORD needed=0;
    CString driver,device,port;

    //Get printer information for the device context
    ::GetPrinter(phandle,2,printInfo,0,&needed);

	printInfo=new BYTE[needed];
    if(!needed || !printInfo || !::GetPrinter(phandle,2,printInfo,needed,&needed))
    {
        if(printInfo) delete[] printInfo;
        MessageBox(_T("Couldn't get printer driver information."),_T("Error"),MB_OK);
        ::ClosePrinter(phandle);
        return;
    }
    driver=((PRINTER_INFO_2 *)printInfo)->pDriverName;
    device=((PRINTER_INFO_2 *)printInfo)->pPrinterName;
    port=((PRINTER_INFO_2 *)printInfo)->pPortName;
    delete[] printInfo;

    SetWindowText(_T("Printing, click \"Abort\" to stop.."));
    SetDlgItemText(IDCANCEL,_T("Abort"));
    DisableEverything();

    //Calculate text size, margins, etc.
    int cy,cx,wid,cwid;
    int pw=10*GetPageWidth(),ph=10*GetPageHeight();
    CRect pageBounds;
    const CImageEditorDlg::Record *p;
    // struct tm *t;

    pageBounds.SetRect(lm,tm,pw-rm,ph-bm);

    doc.cbSize=sizeof(doc);
    doc.fwType=0;
    doc.lpszDatatype=NULL;
    doc.lpszDocName=_T("Image file printout");
    doc.lpszOutput=NULL;

    printDC.CreateDC(driver,device,port,NULL);

    printDC.SetWindowOrg(0,0);
    printDC.SetViewportOrg(0,0);
    printDC.SetMapMode(MM_HIENGLISH);   //(Note: Screen y==-MM_HIENGLISH y)
    printDC.SetWindowExt(pw,ph);
    printDC.SetViewportExt(pw,ph);

    CFont theFont;
    theFont.CreatePointFont(fontPointSize*10,fontFace,&printDC);
    printDC.SelectObject(&theFont);

    //If printing to a file, the print can be cancelled at this point
    if(printDC.StartDoc(&doc)<0)
    {
        ::ClosePrinter(phandle);
        SetDlgItemText(IDCANCEL,_T("Cancel"));
        UpdatePageCount();
        return;
    }

    //  rangeMax&=~1;
    printing=true;
	cancel=false;
 
    //Get the width of the timestamp string for margin clipping
    wid=printDC.GetTextExtent(CString(_T("XXXX-XXX-XX:XX:XX:XXX"))).cx;
    cwid=wid/21;

    //Step through each page of each copy
    for(curcopy=1;(DWORD)curcopy<=pi.copies && !cancel;curcopy++) {
        TRACE(_T("Beginning copy %u of %u...\n"),curcopy,pi.copies);
        for(cpa=address=rangeMin; address<=rangeMax && !cancel; address=cpa)
        {
            TRACE(_T("  Beginning page %u of %u at address %X...\n"),page, pi.subtotal, cpa);
            DisM();
            s.Format(_T("Copy %u/%u, page %u/%u"),curcopy,pi.copies,page+1,pi.subtotal);
            SetDlgItemText(IDC_STC_PAGECOUNT,s);
            EnaM();

            cy=pageBounds.top;
            cx=pageBounds.left;
            progress->SetPos(((page*(address-rangeMin)))/(pi.subtotal*(rangeMax-rangeMin)));
            printDC.StartPage();

            s.Empty();
            
            //Handle the first page's header and other contents
            if(!page)
            {
                int txr=pageBounds.left+printDC.GetTextExtent(CString(_T("Address Range:"))).cx+500;
 		        printDC.TextOut(pageBounds.left,-cy,_T("Original File:"));

                //Wrap the filename so it doesn't trail off the page...
                int n=editor->currentFile.GetLength();
                int wid=(pageBounds.Width()-txr)/pi.cwid;
                // int st;
                for(int st=0;st<n;st+=wid)
                {
                    printDC.TextOut(txr,-cy,editor->currentFile.Mid(st,wid));
                    cy+=pi.chgt;
                }

				struct tm t;
                time_t now = time(0);
                errno_t err = gmtime_s(&t, &now);
		        printDC.TextOut(pageBounds.left,-cy,_T("Report Date:"));
                s.Format(_T("%04.4d-%03.3d-%02.2d:%02.2d:%02.2d"),
                    1900+t.tm_year,t.tm_yday+1,t.tm_hour,t.tm_min,t.tm_sec);
                printDC.TextOut(txr,-cy,s);
				cy+=pi.chgt;
                
		        printDC.TextOut(cx,-cy,_T("Address Range:"));
                s.Format(_T("0x%08.8X-0x%08.8X"),rangeMin,rangeMax);
                printDC.TextOut(txr,-cy,s);
				cy+=pi.chgt;

				if ((CImageEditorDlg::ACE1_IMAGE == editor->m_strctFileHdr.strtHeader.eType)
				|| (CImageEditorDlg::ACE2_IMAGE == editor->m_strctFileHdr.strtHeader.eType)){
		
					if (0 == editor->m_nAddrSpace) {
						printDC.TextOut(cx,  -cy, _T("Address Space:"));
						printDC.TextOut(txr, -cy, _T("Operand"));
					} else if (1 == editor->m_nAddrSpace){
						printDC.TextOut(cx,  -cy, _T("Address Space:"));
						printDC.TextOut(txr, -cy, _T("Instrument"));
					}
					cy +=pi.chgt;
				}  
 
				TCHAR buffer[64]=_T("");
                DWORD bufsz=63;
				::GetComputerName(buffer,&bufsz);
		        printDC.TextOut(pageBounds.left,-cy,_T("Workstation:"));
                printDC.TextOut(txr,-cy,buffer);
				cy+=pi.chgt+pi.chgt;
            }

            //Print column headers if apropos
            if(printColHeaders>1 || (!page && printColHeaders))
            {
                cx=pageBounds.left;
                printDC.TextOut(cx,-cy,_T("Hex Addr"));
                cx += 1000 + (pi.cwid<<3);
				cx -= ((cx-pageBounds.left)%500);

				printDC.TextOut(cx,-cy,_T("Dec Addr"));
                cx += 500+(pi.cwid<<3);
				cx -= ((cx-pageBounds.left)%500);

                printDC.TextOut(cx,-cy,_T("Hex Val "));
                cx+=500+(pi.cwid<<3);
				cx-=((cx-pageBounds.left)%500);

                printDC.TextOut(cx,-cy,_T("Dec Val "));
                cx+=500+(pi.cwid<<3);
				cx-=((cx-pageBounds.left)%500);

                printDC.TextOut(cx,-cy,_T("Timestamp"));
                cx+=wid;

                printDC.MoveTo(pageBounds.left,-(cy+pi.chgt-10));
                printDC.LineTo(cx,-(cy+pi.chgt-30));
                cy+=pi.chgt;
                cx=pageBounds.left;
            }

            // Print the rows of the table
            for(DWORD nValidAddrs = 0; (cpa <= rangeMax) && (nValidAddrs <= pi.linesPerPage) && !cancel; nValidAddrs++, cpa++) {

				bool bValidAddr = true;
				bool bRecMod    = false;

				// p=editor->GetAddx(cpa);
				p=editor->GetAddx(cpa, bValidAddr, bRecMod);
				if (NULL == p){
					// If record can not be read, jump out of do loop and reset page to 0.
					// This probably can be remvoed, since all records are loaded into memory.
					// Left over from previous design.
                    goto out2;
				} else if (!bValidAddr){
					// Skip address if invalid.
					// But don't increment page line counter.
					nValidAddrs--;
					continue;
				}

                if(cy>pageBounds.bottom-(pi.chgt+(printPageNumbers?(pi.chgt<<1):0))) {
                    break;
				}
    
                s.Format(_T("%08.8X"), cpa);
				printDC.TextOut(cx,-cy,s);
                cx += 1000 + (pi.cwid<<3);
				cx -=((cx-pageBounds.left)%500);

				s.Format(_T("%08.8d"), cpa);
				printDC.TextOut(cx,-cy,s);
                cx+=500+(pi.cwid<<3);
				cx-=((cx-pageBounds.left)%500);

                s.Format(_T("%04.4X"),p->data);
				printDC.TextOut(cx,-cy,s);
                cx += 500 + (pi.cwid<<3);
				cx -= ((cx-pageBounds.left)%500);

                s.Format(_T("%05.5u"),p->data);
				printDC.TextOut(cx,-cy,s);
                cx += 500 + (pi.cwid<<3);
				cx -= ((cx-pageBounds.left)%500);

		//	    t=gmtime((const time_t *)(&(p->seconds)));	

				// __int64 ltime = cur->seconds; 

				struct tm t;
				__int64 ltime = p->seconds;
				errno_t err = gmtime_s( &t, &ltime);

                if(err != 0)
                    s=_T("(Invalid timestamp)");
                else
                    s.Format(_T("%04.4d-%03.3d-%02.2d:%02.2d:%02.2d.%03.3d"),
                        t.tm_year+1900,t.tm_yday,t.tm_hour,t.tm_min,
                        t.tm_sec,p->milliseconds);
                if(cx+wid>pageBounds.right)
                    s=s.Left(((cx+wid)-pageBounds.right)/cwid);

                printDC.TextOut(cx,-cy,s);
                cy+=pi.chgt;
                cx=pageBounds.left;

                //Ensure that any abort-related messages are caught
                while(::PeekMessage(&msg,NULL,0,0,PM_NOREMOVE))
                {
                    if(!curThread->PumpMessage())
                        break;
                }

                if(cancel)
                {
                    if(MessageBox(_T("Abort printing?"),_T("Abort"),MB_YESNO|MB_ICONQUESTION)==IDYES)
                        break;
                    else
                    {
                      cancel=false;
                    }
                }
            }   

            //Do page numbering at the bottom/center
            if(printPageNumbers)
            {
                cy+=pi.chgt;
                CSize sz;
                s.Format(_T("Page %u/%u  [Addresses 0x%08.8X-0x%08.8X]"),page+1,pi.subtotal,address,(cpa)-1);
                sz=printDC.GetTextExtent(s);
                printDC.TextOut((pageBounds.left+pageBounds.right-sz.cx)>>1,
                    sz.cy-pageBounds.bottom,s);
            }
    
            page++;
            printDC.EndPage();
            TRACE(_T("  Ended page %u of %u at address %X...\n"),page,pi.subtotal,cpa);
        }

	out2:   page=0;
    }

    printDC.EndDoc();
    printDC.SelectStockObject(SYSTEM_FONT);
    SetWindowText(_T("Print"));
    EnableEverything();
    SetDlgItemText(IDCANCEL,_T("Cancel"));
    DrawMenuBar();
    ::DeleteDC(printDC.Detach());
    ::ClosePrinter(phandle);

    if(!cancel)
    {
        progress->SetPos(256);
        SetDlgItemText(IDOK,_T("Done"));
        GetDlgItem(IDCANCEL)->EnableWindow(FALSE);
    }
    else
    {
        progress->SetPos(0);
        SetDlgItemText(IDOK,_T("Print"));
    }
    cancel=printing=false;
}

////////////////////////////////////////////////////////////////////////////////
// Internal functions                 //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       CPrintAddxDialog::OpenSelPrinter
Parameters:     [CPrintAddxDialog *this]
                DWORD mode
                    The initial printer access mode.
Returns:        HANDLE
                    Handle to the opened printer or NULL if the printer could
                    not be opened.
Description:    Opens the selected printer in the requested mode.
PDL:
    IF no printers are found OR there isn't enough memory THEN
        RETURN null
    ENDIF

    IF the printer can't be opened THEN
        IF the calling function requested administrative privelege THEN
            Try to open the printer for normal use
        ENDIF
        IF the printer couldn't be opened THEN
            Alert the user
            RETURN null
        ENDIF
    ENDIF
    RETURN a handle to the printer
**/
HANDLE CPrintAddxDialog::OpenSelPrinter(DWORD mode)
{
    CString curSel;
    LPTSTR buf;
    HANDLE phandle;
    PRINTER_DEFAULTS pdflt;
    int nItems;

    GetDlgItemText(IDC_CBO_PRINTER,curSel);
    nItems=((CComboBox *)GetDlgItem(IDC_CBO_PRINTER))->GetCount();
    if(!nItems || curSel==_T("(No printers found)"))
        return 0;

    buf=new TCHAR[curSel.GetLength()+1];
    if(!buf)
    {
        MessageBox(_T("Not enough memory."),_T("Error"),MB_OK|MB_ICONWARNING);
        return 0;
    }

    ::_tcscpy_s(buf, (curSel.GetLength()+1), (LPCTSTR)curSel);
    curSel.Empty();

    pdflt.pDatatype=NULL;pdflt.pDevMode=NULL;
    pdflt.DesiredAccess=mode;
    if(!::OpenPrinter(buf,&phandle,&pdflt))
    {
        bool f=false;
        if(mode==PRINTER_ACCESS_ADMINISTER)
        {
            pdflt.DesiredAccess=PRINTER_ACCESS_USE;
            f=!!::OpenPrinter(buf,&phandle,&pdflt);
        }
        if(!f)
        {
            MessageBox(_T("Couldn't open the selected printer."),_T("Error"),MB_OK|MB_ICONWARNING);
            delete[] buf;
            return 0;
        }
    }
    delete[] buf;
    return phandle;
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::GetPageHeight
Parameters:     [CPrintAddxDialog *this]
Returns:        int
                    The height of the page, in hundredths of an inch.
Description:    Returns the currently selected or entered page height.
PDL:
    IF the user has selected a custom paper size THEN
        RETURN the height entered by the user
    ELSE
        RETURN the preset height
    ENDIF
**/
int CPrintAddxDialog::GetPageHeight()
{
    static const int heights[]=
        {1100,1400,1050,1700,1654,1169,827,1390,984};
    int n;

    if(IsDlgButtonChecked(IDC_RDO_CUSTPPR))
    {
        CString h;
        float hgt=-1.0;
        GetDlgItemText(IDC_EDT_PAPERHGT,h);

        _stscanf_s(h, _T("%f"), &hgt);
        if(hgt<=1.0)
            return 1100;
        else
            return int(hgt*100.0);
    }

    n=((CComboBox *)GetDlgItem(IDC_CBO_PAPERTYPE))->GetCurSel();
    if(n<0 || n>=(sizeof(heights)/sizeof(*heights)))
        return 1100;
    return heights[n];
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::GetPageWidth
Parameters:     [CPrintAddxDialog *this]
Returns:        int
                    The width of the page in hundredths of an inch.
Description:    Returns the currently selected or entered page width.
PDL:
    IF the user has selected a custom paper size THEN
        RETURN the width entered by the user
    ELSE
        RETURN the preset width
    ENDIF
**/
int CPrintAddxDialog::GetPageWidth()
{
    static const int widths[]=
        {850,850,725,1100,1109,827,585,984,693};
    int n;

    if(IsDlgButtonChecked(IDC_RDO_CUSTPPR))
    {
        CString w;
        float wid=-1.0;
        GetDlgItemText(IDC_EDT_PAPERWID,w);

        _stscanf_s(w, _T("%d"),&wid);
        if(wid<=1.0)
            return 850;
        else
            return int(wid*100.0);
    }

    n=((CComboBox *)GetDlgItem(IDC_CBO_PAPERTYPE))->GetCurSel();
    if(n<0 || n>=(sizeof(widths)/sizeof(*widths)))
        return 850;
    return widths[n];
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::SaveValues
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Saves the dialog's settings back to the editor.
PDL:
    Read margins into editor structures
    Read radio and check boxes' values and set editor flags
    IF a custom paper size is selected THEN
        Save the custom paper size
    ELSE
        Save the paper type
    ENDIF
**/
void CPrintAddxDialog::SaveValues()
{
    _pd *p=&(editor->PrintData);

    p->fontSize=fontPointSize;
    p->font=fontFace;

    //The margins can't be directly saved due to floating-point drift.
    p->flags=0;
    int l,r;CString wtxt;
    GetDlgItemText(IDC_EDT_LEFTMARGIN,wtxt);
    l=r=0;if(!_stscanf_s((LPCTSTR)wtxt,_T("%d.%d"),&l,&r)) {l=1;r=0;}
    p->lMargin=FCombine(l,r);

    GetDlgItemText(IDC_EDT_RGTMARGIN,wtxt);
    l=r=0;if(!_stscanf_s((LPCTSTR)wtxt,_T("%d.%d"),&l,&r)) {l=1;r=0;}
    p->rMargin=FCombine(l,r);

    GetDlgItemText(IDC_EDT_TOPMARGIN,wtxt);
    l=r=0;if(!_stscanf_s((LPCTSTR)wtxt,_T("%d.%d"),&l,&r)) {l=1;r=0;}
    p->tMargin=FCombine(l,r);

    GetDlgItemText(IDC_EDT_BOTMARGIN,wtxt);
    l=r=0;if(!_stscanf_s((LPCTSTR)wtxt,_T("%d,%d"),&l,&r)) {l=1;r=0;}
    p->bMargin=FCombine(l,r);

    p->flags=0;
    if(IsDlgButtonChecked(IDC_CHK_PAGENUMS))
        p->flags|=_pd::PgNums;
    if(IsDlgButtonChecked(IDC_CHK_COLHEADERS))
    {
        if(IsDlgButtonChecked(IDC_RDO_HDREVERY))
            p->flags|=_pd::HdrEvery;
        else
            p->flags|=_pd::HdrFirst;
    }
    if(IsDlgButtonChecked(IDC_RDO_CUSTPPR))
    {
        p->flags|=_pd::CustPpr;
        p->pprWid=GetPageWidth();
        p->pprHgt=GetPageHeight();
    }
    else
        p->pprWid=p->pprHgt=
            ((CComboBox *)GetDlgItem(IDC_CBO_PAPERTYPE))->GetCurSel();

    if(IsDlgButtonChecked(IDC_RDO_PRINTALL))
        p->flags|=_pd::AllSel;
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::GetMargins
Parameters:     [CPrintAddxDialog *this]
Returns:        CPrintAddxDialog::margins
                    The current margins in inches.
Description:    Returns a structure filled with the current margin values.
PDL:
    Read the margins
    Clip them to valid values
    RETURN the margins
**/
CPrintAddxDialog::margins CPrintAddxDialog::GetMargins()
{
    float wid=float(GetPageWidth())/100.0F;
    float hgt=float(GetPageHeight())/100.0F;
    margins m={1.0F,1.0F,1.0F,1.0F};
    CString txt;

    GetDlgItemText(IDC_EDT_LEFTMARGIN,txt);
    if(txt.GetLength())
        _stscanf_s((LPCTSTR)txt,_T("%f"),&(m.left));
    
    GetDlgItemText(IDC_EDT_TOPMARGIN,txt);
    if(txt.GetLength())
        _stscanf_s((LPCTSTR)txt,_T("%f"),&(m.top));

    GetDlgItemText(IDC_EDT_RGTMARGIN,txt);
    if(txt.GetLength())
        _stscanf_s((LPCTSTR)txt,_T("%f"),&(m.right));

    GetDlgItemText(IDC_EDT_BOTMARGIN,txt);
    if(txt.GetLength())
        _stscanf_s((LPCTSTR)txt,_T("%f"),&(m.bottom));

    wid/=2.0F;hgt/=2.0F;
    if(m.left<0) m.left=0;
    if(m.right<0) m.right=0;
    if(m.top<0) m.top=0;
    if(m.bottom<0) m.bottom=0;

    if(m.left>wid-1.0F)  m.left=wid-1.0F;
    if(m.right>wid-1.0F) m.right=wid-1.0F;
    if(m.top>hgt-1.0F)   m.top=hgt-1.0F;
    if(m.bottom>hgt-1.0F) m.bottom=hgt-1.0F;

    return m;
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::UpdatePageCount
Parameters:     [CPrintAddxDialog *this]
                CPrintAddxDialog::pageInfo *p
                    A pointer to an information structure to fill or NULL if
                    none should be filled.
Returns:        DWORD
                    The expected number of pages in the print job.
Description:    Updates the page count field, updates any preview or diabled
                items, and returns all calculated values.
PDL:
    CALL UpdatePreview
    Get the margins
    Get the text measurements for the current font
    Get the address range
    Calculate the lines per page and lines per copy
    Convert the lines/page and lines/copy into a page count
    Fill the passed structure if non-null
    RETURN the page count
**/
DWORD CPrintAddxDialog::UpdatePageCount(CPrintAddxDialog::pageInfo *p)
{
    UpdatePreview();

    margins m=GetMargins();
    CRect r(int(1000.0F*m.left),int(1000.0F*m.top),
        10*GetPageWidth()-int(1000.0F*m.right),
        10*GetPageHeight()-int(1000.0F*m.bottom));
    int hgt=r.Height();
    int txthgt = 0;

    // Get text measurements
  
    CClientDC dc(this);
    dc.SetMapMode(MM_HIENGLISH);
        
    CFont f;
    f.CreatePointFont(fontPointSize*10,fontFace,&dc);

    dc.SelectObject(&f);

    TEXTMETRIC metr;
    ::memset(&metr,0,sizeof(metr));
    dc.GetTextMetrics(&metr);
    if(!(txthgt=metr.tmHeight)){
		txthgt=148;
	}

    dc.SelectStockObject(SYSTEM_FONT);
    if(p) {
       p->chgt=metr.tmHeight;
       p->cwid=metr.tmAveCharWidth;
    }
 
    int linesPerPage  = hgt/txthgt;
    int linesPerPageO = linesPerPage;
    TRACE(_T("Got height %d.%03.3d in UpdatePageCount()\n")
          _T("    ch hgt %d.%03.3d\n")
          _T("  per page %d.%03.3d\n"),
            hgt/1000,hgt%1000,
            txthgt/1000,txthgt%1000,
            linesPerPage/1000,linesPerPage%1000);

    DWORD min = (unsigned)0, max=~0;
    DWORD linesNeeded = 0;
    CString txt = _T("");

    if(printRange)
    {
        GetDlgItemText(IDC_EDT_RANGEMIN,txt);
        if(txt.GetLength())
            min=(m_bHex?Doubleword::ParseHex(txt):Doubleword::ParseDecimal(txt));
        else
            return 0;

        GetDlgItemText(IDC_EDT_RANGEMAX,txt);
        if(txt.GetLength())
            max=(m_bHex?Doubleword::ParseHex(txt):Doubleword::ParseDecimal(txt));
        else
            return 0;
    }
    else
    {
        min=editor->lowestAddress;
        max=editor->highestAddress;
    }

    // max&=~1;
	// min&=~1;
    if(min>max) {
		register DWORD t=min;
		min=max;
		max=t;
	}

    if(min<editor->lowestAddress){
		min=editor->lowestAddress;
	}

    if(max>editor->highestAddress){
		max=editor->highestAddress;
	}
    
	// linesNeeded=5+(((max+2)-min));

	// DWORD dwTemp = 5 + (((max + 2) - min));

	linesNeeded = GetNumberLinesNeeded(min, max);
    if(!linesNeeded){
		++linesNeeded;
	}

    //Adjust for the headers on the pages
    if(printColHeaders==1)
        linesNeeded++;
    else if(printColHeaders>1)
        linesPerPage--;

    //Two lines per page gone from page numbers
    if(printPageNumbers)
        linesPerPage-=2;

    DWORD perCopy=(linesNeeded+linesPerPage-1)/linesPerPage;
    
    int copies=1;
    GetDlgItemText(IDC_EDT_COPIES,txt);
    if(txt.GetLength()>=1)
        _stscanf_s((LPCTSTR)txt,_T("%u"),&copies);
    if(copies<1) copies=1;

    DWORD total=perCopy*copies;

    if(!p)
    {
        txt.Format(_T("%d cop%s x %u page%s each = %u page%s"),
            copies,(copies!=1)?_T("ies"):_T("y"),
            perCopy,(perCopy!=1)?_T("s"):_T(""),
            total,(total!=1)?_T("s"):_T(""));
        SetDlgItemText(IDC_STC_PAGECOUNT,txt);
    }
    else
    {
        p->copies=copies;
        p->linesPerPage=linesPerPage;
        p->subtotal=perCopy;
        p->total=total;
        p->needed=linesNeeded;
        p->uaLinesPerPage=linesPerPageO;
    }

    TRACE(_T("Got %u pages/copy in UpdatePageCount()\n"),perCopy);
    return perCopy;
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::UpdatePrinterCombo
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Fills the printer selection combo box with printer names.
PDL:
    Find all "local" printers and insert them into the combo box
    Find all "network" printers and insert them into the combo box
    Select the default printer
    IF no printers were found THEN
        Place a message in the combo box
        Disable applicable windows
    ENDIF
**/
void CPrintAddxDialog::UpdatePrinterCombo()
{
    CComboBox *printers=(CComboBox *)GetDlgItem(IDC_CBO_PRINTER);
    int nItems=0;
    LPBYTE prnInfo;
    TCHAR retStr[60];
    DWORD needed,returned,size,i;

    printers->ResetContent();

    //Get the size of the memory we need for local printers...
    i=::EnumPrinters(PRINTER_ENUM_LOCAL,0,1,0,0,&size,&returned);
    if(size && (prnInfo=new BYTE[size]))
    {
        //Enumerate the printer structures (see <1>)
        i=::EnumPrinters(PRINTER_ENUM_LOCAL,0,1,(LPBYTE)prnInfo,size,&needed,&returned);

        //Add everything to the combo box
        for(i=0;i<returned;i++)
            printers->AddString(((PRINTER_INFO_1 *)prnInfo)[i].pName);
        nItems+=returned;
        delete[] prnInfo;
    }
    else
        if(size)
            MessageBox(_T("Not enough memory to enumerate printers."),_T("Error"),
                MB_OK|MB_ICONWARNING);

    //Get any network printers that weren't included in the last function
    i=::EnumPrinters(PRINTER_ENUM_CONNECTIONS,0,1,0,0,&size,&returned);
    if(size && (prnInfo=new BYTE[size]))
    {
        i=::EnumPrinters(PRINTER_ENUM_CONNECTIONS,0,1,(LPBYTE)prnInfo,size,&needed,&returned);

        for(i=0;i<returned;i++)
            printers->AddString(((PRINTER_INFO_1 *)prnInfo)[i].pName);
        nItems+=returned;
        delete[] prnInfo;
    }
    else
        if(size)
            MessageBox(_T("Not enough memory to enumerate printers."),_T("Error"),
                MB_OK|MB_ICONWARNING);

    //Read the default "device" ("device" == "printer"?)
    if(nItems && ::GetProfileString(_T("windows"),_T("device"),_T(""),retStr,59))
    {
        //Clean off extraneous data from the name
        for(i=0;i<60 && retStr[i];i++)
            if(retStr[i]==',')
            {
                retStr[i]=0;break;
            }

        //Select either the default or the first printer found
        if(printers->SelectString(0,retStr)==CB_ERR)
             printers->SetCurSel(0);
    }
    else
        printers->SetCurSel(0);

    if(!nItems)
    {
        printers->AddString(_T("(No printers found)"));
        printers->RedrawWindow();
        printers->SetCurSel(0);
        printers->EnableWindow(FALSE);
        GetDlgItem(IDC_BTN_PRINTPROP)->EnableWindow(FALSE);
        GetDlgItem(IDOK)->EnableWindow(FALSE);
    }
    else
    {
        printers->SetCurSel(0);
        printers->EnableWindow();
        GetDlgItem(IDC_BTN_PRINTPROP)->EnableWindow();
    }
}

static UINT xableIDs[]={IDC_STATIC,IDC_RDO_PRINTALL,IDC_RDO_PRINTRANGE,IDC_EDT_RANGEMIN,
    IDC_EDT_RANGEMAX,IDC_BTN_ONSCREEN,IDC_CBO_PRINTER,IDC_BTN_PRINTPROP,IDC_CHK_PAGENUMS,
    IDC_CHK_COLHEADERS,IDC_RDO_HDRFIRST,IDC_RDO_HDREVERY,IDC_RDO_PAPERSEL,IDC_RDO_CUSTPPR,
    IDC_EDT_PAPERWID,IDC_EDT_PAPERHGT,IDC_EDT_LEFTMARGIN,IDC_EDT_TOPMARGIN,IDC_EDT_RGTMARGIN,
    IDC_EDT_BOTMARGIN,IDC_STC_PDIM1,IDC_STC_PRRANGETO,IDC_CBO_SELFONT,IDC_STC_FONTSIZE,
    IDC_SPN_FONTSIZE,IDC_STC_FONTPREVIEW,IDC_STC_PDIM2,IDOK,IDC_CBO_PAPERTYPE,
    IDC_EDT_COPIES};
///////////////////////////////////////
/**
Function:       CPrintAddxDialog::DisableEverything
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Prepare for a print by disabling most controls in the window.
PDL:
    DOFOR each disable-able control
        Disable the control
    ENDDO
**/
void CPrintAddxDialog::DisableEverything()
{
    for(register int i=0;i<sizeof(xableIDs)/sizeof(*xableIDs);i++)
        GetDlgItem(xableIDs[i])->EnableWindow(FALSE);
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::EnableEverything
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Re-enable controls after a print.
PDL:
    DOFOR each disable-able control
        Enable the control
    ENDDO
    Ensure that the settings and the disabled items are synchronized.
**/
void CPrintAddxDialog::EnableEverything()
{
    for(register int i=0;i<sizeof(xableIDs)/sizeof(*xableIDs);i++)
        GetDlgItem(xableIDs[i])->EnableWindow();
    if(IsDlgButtonChecked(IDC_RDO_PRINTALL))
    {
        GetDlgItem(IDC_EDT_RANGEMIN)->EnableWindow(FALSE);
        GetDlgItem(IDC_EDT_RANGEMAX)->EnableWindow(FALSE);
        GetDlgItem(IDC_STC_PRRANGETO)->EnableWindow(FALSE);
    }

    if(IsDlgButtonChecked(IDC_RDO_CUSTPPR))
        GetDlgItem(IDC_CBO_PAPERTYPE)->EnableWindow(FALSE);
    else
    {
        GetDlgItem(IDC_EDT_PAPERWID)->EnableWindow(FALSE);
        GetDlgItem(IDC_EDT_PAPERHGT)->EnableWindow(FALSE);
        GetDlgItem(IDC_STC_PDIM1)->EnableWindow(FALSE);
        GetDlgItem(IDC_STC_PDIM2)->EnableWindow(FALSE);
    }

    CComboBox *p=(CComboBox *)GetDlgItem(IDC_CBO_PRINTER);
    if(p->GetCount()==1)
    {
        CString s;
        p->GetWindowText(s);
        if(s==CString(_T("(No printers found)")))
        {
            p->EnableWindow(FALSE);
            GetDlgItem(IDC_BTN_PRINTPROP)->EnableWindow(FALSE);
        }
    }
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::UpdateFontCombo
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Fills the font selection combo box with font names.
PDL:
    Enumerate the font families by repeatedly calling fontEnummer
**/
void CPrintAddxDialog::UpdateFontCombo()
{
    LOGFONT lf;
    CClientDC dc(this);

    ::memset(&lf,0,sizeof(lf));
    lf.lfCharSet=DEFAULT_CHARSET;

    register CComboBox *p=(CComboBox *)GetDlgItem(IDC_CBO_SELFONT);
    p->ResetContent();
    ::EnumFontFamiliesEx(dc.GetSafeHdc(),&lf,fontEnummer,(LPARAM)p,0);
}

///////////////////////////////////////
/**
Function:       CPrintAddxDialog::GetNumberLinesNeeded
Parameters:     DWORD dwStartAddress, DWORD dwEndAddress)
Returns:        DWORD 
Description:    
PDL:
    Calculate number of valid addresses in range.
**/
DWORD CPrintAddxDialog::GetNumberLinesNeeded(DWORD dwStartAddress, DWORD dwEndAddress){

	DWORD dwLinesNeeded = 0;

	for (DWORD i = dwStartAddress; i <= dwEndAddress; i++){

		bool bValidAddr = true;
		bool bRecMod = true;
		const CImageEditorDlg::Record *p;
		p = editor->GetAddx(i, bValidAddr, bRecMod);
	    if (p && bValidAddr){
			// Only count valid addresses
			dwLinesNeeded++;
		} 
	}

	return dwLinesNeeded;
}

////////////////////////////////////////////////////////////////////////////////
// Global functions                   //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       fontEnummer
Parameters:     const LOGFONT *logFontData
                    Data on the current font.
                const TEXTMETRIC *physFontData
                    Text metric data for the font in the given device context.
                unsigned long fontType
                    The variety of font.
                LPARAM box
                    A pointer to the combo box window that should be filled.
Returns:        int
                    Nonzero to continue enumeration.
Description:    Called for each installed font by the EnumFontFamiliesEx
                function in CPrintAddxDialog::UpdateFontCombo (see above).
PDL:
    Add the passed font to the combo box.
**/
static int CALLBACK fontEnummer(const LOGFONT *logFontData,const TEXTMETRIC *physFontData,
                                unsigned long fontType,LPARAM box)
{
    if(((CComboBox *)box)->FindStringExact(0,logFontData->lfFaceName)==CB_ERR)
        ((CComboBox *)box)->AddString(logFontData->lfFaceName);
    return 1;
}

/* Notes:
 * #<1> line 1897
 *    The printer functions' return values vary wildly between Windows
 *    versions and varieties.  The request for all local printers may or
 *    may not return all local and network printers, and the request for
 *    network printers may or may not return anything even if network
 *    printers are installed.
 */
