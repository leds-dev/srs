/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       PrintAddxDialog.h
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
		 V2.0        10/04       F. Shaw         Modifications for Virtual memory
		                                         mapping.  GTACS Build 9
*******************************************************************************/
// Header for the [Print] dialog

#if !defined(AFX_PRINTADDXDIALOG_H__E35AC793_545A_4D75_AA4E_0E9B6D44D46F__INCLUDED_)
#define AFX_PRINTADDXDIALOG_H__E35AC793_545A_4D75_AA4E_0E9B6D44D46F__INCLUDED_
#if _MSC_VER > 1000
#   pragma once
#endif

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "ImageEditorDlg.h"

////////////////////////////////////////////////////////////////////////////////
// Class Definitions                  //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class CImageEditorDlg;
class CPrintAddxDialog : public CDialog
{
protected:
    ///////////////////////////////////
    // Protected types & structures  //
    ///////////////////////////////////
    struct pageInfo
    {
        DWORD linesPerPage,copies;
        DWORD subtotal,total,needed,uaLinesPerPage;
        int chgt,cwid;
    };

    struct margins
    {
        float left,top,right,bottom;
    };

public:
    ///////////////////////////////////
    // Public interface functions    //
    ///////////////////////////////////
    CPrintAddxDialog(CWnd *pParent=NULL);
    ~CPrintAddxDialog();

protected:
    ///////////////////////////////////
    // Protected member variables    //
    ///////////////////////////////////
    CImageEditorDlg *editor;
    CString fontFace;
    CFont *fontInstance;
    bool dism,m_bHex;
    bool printing,cancel;
    bool printRange;
    bool printPageNumbers;
    int printColHeaders;
    int m_SpnFontSizePos;
    int fontPointSize;

protected:
    ///////////////////////////////////////
    // Protected internal functions      //
    ///////////////////////////////////////
    margins GetMargins();
    HANDLE OpenSelPrinter(DWORD);
    DWORD UpdatePageCount(pageInfo *p=0);
    void  SaveValues();
    void  UpdatePrinterCombo();
    void  UpdateFontCombo();
    void  EnableEverything();
    void  DisableEverything();
    void  UpdatePreview();
    int   GetPageWidth();
	int   GetPageHeight();
	DWORD GetNumberLinesNeeded(DWORD dwStartAddress, DWORD dwEndAddress);

/**
Function:       CPrintAddxDialog::DisM (inline)
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Disables most minor message handlers to prevent recursion.
**/
    inline void DisM() {dism=true;}

/**
Function:       CPrintAddxDialog::EnaM (inline)
Parameters:     [CPrintAddxDialog *this]
Returns:        void
Description:    Enables most minor message handlers.
**/
    inline void EnaM() {dism=false;}

protected:
    ///////////////////////////////////
    // Message map declarations      //
    ///////////////////////////////////
	//{{AFX_MSG(CPrintAddxDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnSelChangeCboPaperType();
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnRdoPrintAll();
	afx_msg void OnRdoPrintRange();
	afx_msg void OnChkColHeaders();
	afx_msg void OnRdoPaperSelChange();
	afx_msg void OnEditChangeCboPrinter();
	afx_msg void OnBtnPrintProp();
	virtual void OnOK();
    virtual void OnCancel();
	afx_msg void OnChangeEdtPageParams();
	afx_msg void OnDeltaPosSpnFontSize(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnEditchangeCboSelFont();
	afx_msg void OnChangeEdtRangeMin();
    afx_msg void OnChangeEdtRangeMax();
	afx_msg void OnBtnOnScreen();
	afx_msg void OnClose();
	afx_msg void OnSetFocusEdtRangemin();
	afx_msg void OnSetFocusEdtRangemax();
	afx_msg void OnContextHelp();
	afx_msg void OnChangeEdtCopies();
    afx_msg void OnChkPageNumbers();
    afx_msg void OnRdoHdrChange();
	afx_msg void OnChkAddxHex();
	//}}AFX_MSG
#ifdef HAVE_TOOLTIPS
    afx_msg BOOL OnToolTipNeedText(UINT,NMHDR *,LRESULT *);
#endif
	DECLARE_MESSAGE_MAP()

    ///////////////////////////////////
    // ClassWizard insertions        //
    ///////////////////////////////////
    //{{AFX_DATA(CPrintAddxDialog)
	enum {IDD=IDD_DLG_PRINT};
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

#if 0
	//{{AFX_VIRTUAL(CPrintAddxDialog)
	protected:
	virtual void DoDataExchange(CDataExchange *pDX);
	//}}AFX_VIRTUAL
#endif
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PRINTADDXDIALOG_H__E35AC793_545A_4D75_AA4E_0E9B6D44D46F__INCLUDED_)
