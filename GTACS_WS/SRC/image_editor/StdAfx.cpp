/*
 @(#)  File Name: StdAfx.cpp  Release 1.0 Date 07/16/02, 17:14:00
*/
/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       StdAfx.cpp
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
*******************************************************************************/
//Global implementation file

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "resource.h"
#include "htmlhelp.h"
#include <process.h>

////////////////////////////////////////////////////////////////////////////////
// Global variables                   //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
bool app_LoadFileImmediately=false;   //Command-line file loading assistance
LPCTSTR app_LoadThisFile=0;           //_/

                                      //////////////////////////////////////////
static DWORD __hCookie=0;             //Global variables for HTML Help
static CString __HelpFile;            //
static CFile __output;                //
static bool __outputOpen=false;       //
static HGLOBAL __GlHelpFile=0;        //_/

////////////////////////////////////////////////////////////////////////////////
// Global functions                   //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////
/**
Function:       BE2LE_D, LE2BE_D
Parameters:     DWORD p
                    The doubleword whose byte order should be reversed.
Returns:        DWORD
                    The reversed form of p.
Description:    Reverses the byte order of the given doubleword.
PDL:
    Take the highest- and lowest-order bytes of the doubleword, reverse them,
        and place them in the central word of the returned doubleword
    Take the central word of the doubleword, reverse the bytes, and place them
        in the outer two bytes of the returned doubleword
**/
#if !defined(BIG_ENDIAN) 
    DWORD __cdecl BE2LE_D(DWORD p)
    {
        register DWORD d;
        d=(p<<24)|(p>>24);
        d|=((p&0xFF00)<<8) | ((p&0xFF0000)>>8);
        return d;
    }
#endif  // defined(BIG_ENDIAN)


///////////////////////////////////////
/**
Function:       BE2LE_W, LE2BE_W
Parameters:     WORD p
                    The word whose bytes should be reversed.
Returns:        WORD
                    The reversed form of p.
Description:    Reverses the byte order of the given word.
PDL:
    Shift the top byte of the word to the bottom or the returned word and
        the bottom byte to the top
**/
#if !defined(BIG_ENDIAN) 
    WORD __cdecl BE2LE_W(WORD p)
    {
        return (p<<8)|(p>>8);
    }
#endif  // defined(BIG_ENDIAN)

///////////////////////////////////////
/**
Function:       DisplayHelpTopic
Parameters:     CWnd *p
                    The parent window of the HTML Help window.
                LPCTSTR within
                    The relative URI of the topic within the help file.
Returns:        void
Description:
PDL:
    Ensure that the help file exists
    Display help on the given topic
    Synchronize the table of contents to the displayed help topic
**/
void DisplayHelpTopic(CWnd *p,LPCTSTR within)
{
    if(__HelpFile.GetLength())
    {
        CString hlp=__HelpFile;
        hlp+=_T("::/");
        hlp+=within;
        hlp+=_T(">main");
        ::HtmlHelp(NULL,hlp,HH_DISPLAY_TOPIC,NULL);
        ::HtmlHelp(NULL,hlp,HH_SYNC,NULL);
    }
    else
        p->MessageBox(_T("Unable to load help."),_T("Error"),MB_OK);
}

///////////////////////////////////////
/**
Function:       UpdateHelpTopic
Parameters:     CWnd *p
                    The parent window of the HTML Help window.
                LPCTSTR s
                    The relative URI of the topic within the help file.
Returns:        void
Description:    Displays the given HTML Help topic if an HTML Help window is open.
PDL:
    IF the help window is already open THEN
        Display the given help topic
        Place the help window beneath the current window
    ENDIF
**/
#ifdef UPDATE_CONTEXT_HELP
    void UpdateHelpTopic(CWnd *p,LPCTSTR s)
    {
        if(__HelpFile.GetLength()
            && ::HtmlHelp(NULL,__HelpFile,HH_GET_WIN_HANDLE,(DWORD)_T("main")))
        {
            CString hlp=__HelpFile;
            hlp+=_T("::/");
            hlp+=s;
            hlp+=_T(">main");
            ::HtmlHelp(NULL,hlp,HH_DISPLAY_TOPIC,NULL);
            ::SetWindowPos(p->GetSafeHwnd(),HWND_TOP,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE|SWP_SHOWWINDOW);
        }
    }
#endif  //defined(UPDATE_CONTEXT_HELP)

////////////////////////////////////////////////////////////////////////////////
// Helper Classes                     //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////
/**
Function:       __HTMLHelp::__HTMLHelp
Parameters:     [__HTMLHelp *this]
Returns:        void
Description:    Initializes the HTML Help interface.
PDL:
    Retrieve the temporary directory for the current user
    IF an ImageEditor help file currently exists THEN
        Open that file for reading
        Save the help filename for future use
    ELSE
        Write the HTML Help file contained within this executable to a
            temporary file
        Open the file for reading
        Save the temporary filename for future use
    ENDIF
    Initialize the HTML Help interface
**/
__HTMLHELP::__HTMLHELP()
{
    HRSRC RsHelpFile;
    void *BinHelpFile;

    CString tfnm;
    ::GetTempPath(MAX_PATH,tfnm.GetBuffer(MAX_PATH+1));tfnm.ReleaseBuffer();

    //Try to find one of Image Editor's help files...
    WIN32_FIND_DATA srchdata;
    HANDLE search=::FindFirstFile(tfnm+_T("\xA0\xA0\xA0*.tmp.chm"),&srchdata);
    if(search!=INVALID_HANDLE_VALUE && __output.Open(
        tfnm+srchdata.cFileName,CFile::modeRead|CFile::shareDenyWrite))
    {
        __outputOpen=true;
        __HelpFile=__output.GetFilePath();
        goto doneHelp;
    }

    //It wasn't found, so load it from within the program.
    HMODULE m;
    {
        TCHAR buf[MAX_PATH+1];
        ::GetModuleFileName(NULL,buf,MAX_PATH);
        m=::LoadLibrary(buf);
    }
    if(!m) goto doneHelp;
     
    //Find the internal help file resource and load it.
    RsHelpFile=::FindResource(m,_T("helpfile.chm"),RT_HTML);
    if(!RsHelpFile || !(__GlHelpFile=::LoadResource(m,RsHelpFile)))
        {::FreeLibrary(m);goto doneHelp;}

    //Obtain a pointer to the resource and lock it.
    BinHelpFile=::LockResource(__GlHelpFile);
    if(!BinHelpFile)
        {::FreeResource(__GlHelpFile);::FreeLibrary(m);goto doneHelp;}

    //Write the resource to the temporary file.
    ::GetTempFileName(tfnm,_T("\xA0\xA0\xA0"),0,__HelpFile.GetBuffer(1024));
    __HelpFile.ReleaseBuffer();
    ::DeleteFile(__HelpFile);
    __HelpFile+=_T(".chm");

    if(!__output.Open(__HelpFile,CFile::modeCreate|CFile::modeWrite
           |CFile::shareExclusive|CFile::typeBinary))
        __HelpFile.Empty();
    else
    {
		// __output.WriteHuge(BinHelpFile,::SizeofResource(NULL,RsHelpFile));
		// WriteHuge - obsolete replaced with write.
        __output.Write(BinHelpFile,::SizeofResource(NULL,RsHelpFile));
        __HelpFile=__output.GetFilePath();
        __output.Close();
        __outputOpen=!!__output.Open(__HelpFile,CFile::modeRead|CFile::shareDenyWrite);
    }

    //Free everything up as best as possible
    ::UnlockResource(__GlHelpFile);
    ::FreeResource(__GlHelpFile);
    ::FreeLibrary(m);

doneHelp:
    //Initialize the HTML help interface
    ::HtmlHelp(NULL,NULL,HH_INITIALIZE,(DWORD)&__hCookie);
    ::SetProcessWorkingSetSize(::GetCurrentProcess(),~0,~0);
}

///////////////////////////////////////
/**
Function:       __HTMLHELP::~__HTMLHelp
Parameters:     [__HTMLHelp *this]
Returns:        void
Description:    Deinitializes the HTML Help interface.
PDL:
    Deinitialize HTML Help
    IF the help file was successfully created THEN
        Close the file
        Try to delete the file
    ENDIF
**/
__HTMLHELP::~__HTMLHELP()
{
    ::HtmlHelp(NULL,NULL,HH_CLOSE_ALL,0);
    ::HtmlHelp(NULL,NULL,HH_UNINITIALIZE,__hCookie);

    if(__HelpFile.GetLength())
    {
        if(__outputOpen)
        {
            __output.Close();
            __outputOpen=false;
        }
        ::DeleteFile(__HelpFile);
        __HelpFile.Empty();
    }
}

///////////////////////////////////////
/**
Function:       Doubleword::ParseDecimal
Parameters:     [Doubleword *this]
                const CString &s
                    The decimal value to parse.
Returns:        bool
                    true iff a valid value was given.
Description:    Parses the decimal value in the given string.
PDL:
    IF the string is empty OR not a valid number THEN
        RETURN false
    ELSE
        Set the new value to the string's value
        RETURN true
    ENDIF
**/
bool Doubleword::FromDecimal(const CString &s)
{
    if(!s.GetLength()) return false;

    DWORD v;
    if(!_stscanf_s((LPCTSTR)s,_T("%d"),&v))
        return false;
    value=v;
    return true;
}

///////////////////////////////////////
/**
Function:       Doubleword::ParseHex
Parameters:     [Doubleword *this]
                const CString &s
                    The hexadecimal value to parse.
Returns:        bool
                    true iff a valid value was given.
Description:    Parses the hexadecimal value in the given string.
PDL:
    IF the string is empty OR not a valid number THEN
        RETURN false
    ELSE
        Set the new value to the string's value
        RETURN true
    ENDIF
**/
bool Doubleword::FromHex(const CString &s)
{
    if(!s.GetLength()) return false;

    DWORD v;
    if(!_stscanf_s((LPCTSTR)s,_T("%X"),&v))
        return false;
    value=v;
    return true;
}

///////////////////////////////////////
/**
Function:       Doubleword::ValidateHex
Parameters:     CEdit *ebox
                    The edit box whose value should be validated.
                unsigned size
                    The maximum size of the data in bytes
Returns:        bool
                    true iff no changes had to be made.
Description:    Ensures that the given edit box contains a correct value.
PDL:
    IF recursion occurred THEN
        RETURN
    ENDIF

    Adjust the requested size
    IF the text is too long THEN
        Clip the text
    ENDIF
    DOFOR each character in the text
        IF the character is a valid hexadecimal digit THEN
            Write the character to the output string
        ELSE
            Adjust the selection
        ENDIF
    ENDDO
    Skip leading zeroes
    IF the text was changed THEN
        Write the new text
    ENDIF
    RETURN true iff the text was changed
**/
bool Doubleword::ValidateHex(CEdit *ebox,unsigned size)
{
    static bool recursion=false;
    if(recursion) return false;
    recursion=true;

    bool changed=false;
    bool combo=false;
    TCHAR inbuf[16],outbuf[16];
    int i,j,sst=-1,snd=-1;

    if(!size || size>4) size=4;
    size<<=1;   //Number of digits

    ebox->GetWindowText(inbuf,15);
    ebox->GetSel(sst,snd);
    if(_tcslen(inbuf)>size)
    {
        changed=true;
        inbuf[size]=0;
    }
    else if(!inbuf[0])
        return recursion=false;
    if(sst<0 || snd<0)
    {
        combo=true;
        DWORD d=((CComboBox *)ebox)->GetEditSel();
        sst=d&0xFFFF;
        snd=d>>16;
    }

    for(i=j=0;inbuf[i];i++)
        if((inbuf[i]>='0' && inbuf[i]<='9')
         ||(inbuf[i]>='A' && inbuf[i]<='F')
         ||(inbuf[i]>='a' && inbuf[i]<='f'))
            outbuf[j++]=inbuf[i];
        else
        {
            changed=true;
            if(i<=sst) {
				--sst;
				--snd;
			}
            else if(i<=snd) --snd;
        }
    outbuf[j]=inbuf[i];

    if(!j)
    {
        ebox->SetWindowText(outbuf);
        return recursion=false;
    }

    i=0;
    if(outbuf[0]=='0' && outbuf[1])
    {
        changed=true;
        for(;outbuf[i]=='0';i++)
            if(i<=sst) {--sst;--snd;}
            else if(i<=snd) --snd;
        if(!outbuf[i]) --i;
    }

    if(changed)
    {
        ebox->SetWindowText(outbuf+i);
        if(combo)
            ((CComboBox *)ebox)->SetEditSel(sst,snd);
        else
            ebox->SetSel(sst,snd);
    }
    recursion=false;
    return changed;
}

///////////////////////////////////////
/**
Function:       Doubleword::ValidateDec
Parameters:     CEdit *ebox
                    Pointer to the edit box control.
                unsigned size
                    The size of the data in bytes.
Returns:        bool
                    true iff the control contained a valid number.
Description:    Ensures that the edit control contains a valid number.
PDL:
    IF recursion occurred THEN
        RETURN
    ENDIF

    Adjust the requested size
    IF the text is too long THEN
        Clip the text
    ENDIF
    DOFOR each character in the text
        IF the character is a valid hexadecimal digit THEN
            Write the character to the output string
        ELSE
            Adjust the selection
        ENDIF
    ENDDO
    Skip leading zeroes
    Clip to the maximum value
    IF the text was changed THEN
        Write the new text
    ENDIF
    RETURN true iff the text was changed    
**/
bool Doubleword::ValidateDec(CEdit *ebox,unsigned size)
{
    static bool recursion=false;
    if(recursion) return false;
    recursion=true;

    bool changed=false;
    bool combo=false;
    TCHAR inbuf[16],outbuf[16];
    LPTSTR obptr;
    int i,j,sst=-1,snd=-1;

    if(!size || size>4) size=4;
    --size;

    static const BYTE lengths[]={3,5,8,10};
    ebox->GetWindowText(inbuf,15);
    ebox->GetSel(sst,snd);
    if(_tcslen(inbuf)>lengths[size])
    {
        changed=true;
        inbuf[lengths[size]]=0;
    }
    else if(!inbuf[0])
        return recursion=false;
    if(sst<0 && snd<0)
    {
        combo=true;
        DWORD d=((CComboBox *)ebox)->GetEditSel();
        sst=d&0xFFFF;
        snd=d>>16;
    }

    for(i=j=0;inbuf[i];i++)
        if(inbuf[i]>='0' && inbuf[i]<='9')
            outbuf[j++]=inbuf[i];
        else
        {
            changed=true;
            if(i<=sst) {--sst;--snd;}
            else if(i<=snd) --snd;
        }
    outbuf[j]=inbuf[i];

    if(!j)
    {
        ebox->SetWindowText(outbuf);
        return recursion=false;
    }

    //Skip leading zeroes
    i=0;
    if(outbuf[0]=='0' && outbuf[1])
    {
        changed=true;
        for(;outbuf[i]=='0';i++)
            if(i<=sst) {--sst;--snd;}
            else if(i<=snd) --snd;
        if(!outbuf[i]) --i;
    }
    obptr=outbuf+i;

    //Check range
    static const TCHAR *const maxnums[]=
        {_T("255"),_T("65535"),_T("16777215"),_T("4294967295")};
    if(j==lengths[size])
        for(i=0;obptr[i];i++)
            if(outbuf[i]>maxnums[size][i])
            {
                _tcscpy_s(obptr, sizeof(maxnums[size]), maxnums[size]);
                changed=true;
                break;
            }
    
    if(changed)
    {
        ebox->SetWindowText(obptr);
        if(combo)
            ((CComboBox *)ebox)->SetEditSel(sst,snd);
        else
            ebox->SetSel(sst,snd);
    }
    recursion=false;
    return changed;
}

///////////////////////////////////////
/**
Function:       Float::Parse
Parameters:     [Float *this]
                const CString &s
                    The value to parse.
Returns:        bool
                    true iff a valid value was given.
Description:    Parses the floating-point value in the given string.
PDL:
    IF the string is empty OR not a valid number THEN
        RETURN false
    ELSE
        Set the new value to the string's value
        RETURN true
    ENDIF
**/
bool Float::Parse(const CString &s)
{
    if(!s.GetLength())
        return false;

    float f;
    if(!_stscanf_s((LPCTSTR)s,_T("%f"),&f))
        return false;
    value=f;
    return true;
}

///////////////////////////////////////
/**
Function:       Float::Validate
Parameters:     CEdit *ebox
                    Pointer to the edit box control.
Returns:        bool
                    true iff a correct value was given.
Description:    Ensures that the edit box contains a valid value.
PDL:
**/
bool Float::Validate(CEdit *ebox)
{
    static bool recursion=false;
    if(recursion) return false;
    recursion=true;

    bool changed=false;
    int hadDecimalPoint=-1;
    bool hadDigitBefore=false;
    bool hadDigitAfter=false;
    TCHAR inbuf[16],outbuf[16];
    LPTSTR obptr;
    int i,j,sst,snd;

    ebox->GetWindowText(inbuf,15);
    if(!inbuf[0])
        return recursion=false;
    ebox->GetSel(sst,snd);

    for(i=j=0;inbuf[i];i++)
        if(inbuf[i]>='0' && inbuf[i]<='9')
        {
            outbuf[j++]=inbuf[i];
            if(hadDecimalPoint<0)
                hadDigitBefore=true;
            else
                hadDigitAfter=true;
        }
        else if(inbuf[i]=='.' && hadDecimalPoint<0)
        {
            if(!hadDigitBefore)
            {
                hadDigitBefore=true;
                outbuf[j++]='0';
            }
            hadDecimalPoint=j;
            outbuf[j++]=inbuf[i];
        }
        else
        {
            changed=true;
            if(i<=sst) {--sst;--snd;}
            else if(i<=snd) --snd;
        }
    outbuf[j]=inbuf[i];

    if(!j)
    {
        ebox->SetWindowText(_T(""));
        return recursion=false;
    }

    //Skip leading zeroes
    i=0;
    if(outbuf[0]=='0' && outbuf[1])
    {
        changed=true;
        for(;outbuf[i]=='0' && outbuf[i+1] && outbuf[i+1]!='.';i++)
            if(i<=sst) {--sst;--snd;}
            else if(i<=snd) --snd;
    }
    obptr=outbuf+i;

    //Clip trailing zeroes
    j=_tcslen(outbuf)-1;
    if(hadDecimalPoint>=0)
        while(j>hadDecimalPoint+1 && outbuf[j]=='0')
        {
            if(j<sst) {--sst;--snd;}
            else if(j<snd) --snd;
            outbuf[j--]=0;
            changed=true;
        }
    
    if(changed)
    {
        ebox->SetWindowText(obptr);
        ebox->SetSel(sst,snd);
    }
    recursion=false;
    return changed;
}