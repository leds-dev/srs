/*
@(#)  File Name: StdAfx.h  Release 1.0 Date 07/16/02 17:41:00
*/
/*******************************************************************************
         Integral Systems, Inc.
********************************************************************************

         PROJECT         :       GOES N-Q
         HOST SYSTEM     :       WINDOWS NT WORKSTATION
         SOURCE          :       StdAfx.h
         EXE. NAME       :       ImageEditor
         PROGRAMMER      :       C. Lauderdale
	
         VER.        DATE        BY              COMMENT
         V1.0        05/02       C. Lauderdale   Initial creation
*******************************************************************************/
// Global include file

#if !defined(AFX_STDAFX_H__1E98DB64_F37C_482B_8655_C31D13A3DC81__INCLUDED_)
#define AFX_STDAFX_H__1E98DB64_F37C_482B_8655_C31D13A3DC81__INCLUDED_
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

////////////////////////////////////////////////////////////////////////////////
// Definitions                        //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//#define UPDATE_CONTEXT_HELP         //Causes help to sync with user actions
//#define BIG_ENDIAN                  //Don't reverse byte order in loads/saves
#undef OEMRESOURCE
#define OEMRESOURCE 1                 //Include OEM resources
#define VC_EXTRALEAN		          //Exclude less-used things from headers
#define WINVER 0x0500				  // Window2000
#define _WIN32_WINNT 0x0500

////////////////////////////////////////////////////////////////////////////////
// Includes                           //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#include <afxwin.h>                   //MFC core/standard components
#include <afxext.h>                   //MFC extensions
#include <afxdtctl.h>		          //MFC support for IE4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#   include <afxcmn.h>			      //MFC support for Windows Common Controls
#endif

#include <iostream>

////////////////////////////////////////////////////////////////////////////////
// Global declarations                //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
extern bool app_LoadFileImmediately;  //File-loading assistance
extern LPCTSTR app_LoadThisFile;      //_/

#ifndef BIG_ENDIAN
    DWORD __cdecl BE2LE_D(DWORD);     //Little-to-big-endian & vice versa
    WORD __cdecl BE2LE_W(WORD);       //
#else                                 //
    inline DWORD BE2LE_D(DWORD x) {return x;}
    inline DWORD BE2LE_W(WORD x) {return x;}
#endif                                //
#define LE2BE_D(x) BE2LE_D(x)         //
#define LE2BE_W(x) BE2LE_W(x)         //_/

void DisplayHelpTopic(CWnd *,LPCTSTR within);
#ifndef UPDATE_CONTEXT_HELP
    inline void UpdateHelpTopic(CWnd *,LPCTSTR) {}
#else
    void UpdateHelpTopic(CWnd *p,LPCTSTR s);
#endif

enum {sz_dword=sizeof(DWORD),sz_word=sizeof(WORD),sz_byte=sizeof(BYTE)};

////////////////////////////////////////////////////////////////////////////////
// Inline definitions                 //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/**
Function:       Float2DWord (inline)
Parameters:     float x
                    The floating-point value to integer-ize.
Returns:        DWORD
                    A fixed-point form of x.
Description:    Converts x into a fixed-point integer.
PDL:
    Place the integer part of x into the top word of the return value
    Place the first two fractional digits of x into the lower word
**/
inline DWORD Float2DWord(float x)
{
    return (DWORD)((int(x)<<16)|(int(x*100.0F)%100));
}

/**
Function:       FCombine (inline)
Parameters:     DWORD l
                    The higher half of the doubleword.
                DWORD r
                    The lower half of the doubleword.
Returns:        DWORD
                    l and r combined.
Description:    Combines the two arguments into one doubleword by shifting.
PDL:
    Combine and return the arguments
**/
inline DWORD FCombine(DWORD l,DWORD r)
{
    return (l<<16)|r;
}

/**
Function:       DWIPart (inline)
Parameters:     DWORD x
                    A fixed-point value.
Returns:        DWORD
                    The integer part of x.
Description:    Returns the integer part of the fixed-point value x.
PDL:
    Return the higher word of the argument
**/
inline DWORD DWIPart(DWORD x)
{
    return x>>16;
}

/**
Function:       DWFPart
Parameters:     DWORD x
                    A fixed-point value.
Returns:        DWORD
                    The fractional part of x.
Description:    Returns the fractional part of the fixed-point value x.
PDL:
    Return the lower word of the argument
**/
inline DWORD DWFPart(DWORD x)
{
    return x&0xFFFF;
}

////////////////////////////////////////////////////////////////////////////////
// Class declarations                 //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class __HTMLHELP                      //This class uses its constructor and
{                                     // destructor to set up/shut down the HTML
public:                               // Help interface.
    __HTMLHELP();
    ~__HTMLHELP();
};

class Doubleword
{
protected:
    DWORD value;
public:
/**
Function:       Doubleword::Doubleword (inline)
Parameters:     [Doubleword *this]
Returns:        void
Description:    Initializes the object to zero.
**/ inline Doubleword() {value=0;}

/**
Function:       Doubleword::Doubleword (inline)
Parameters:     [Doubleword *this]
                DWORD v
                    The value for the doubleword.
Returns:        void
Description:    Initializes the doubleword object.
**/ inline Doubleword(DWORD v) {value=v;}

/**
Function:       Doubleword::operator DWORD (inline)
Parameters:     [const Doubleword *this]
Returns:        DWORD
                    The actual value for the doubleword.
Description:    Casts the doubleword to a DWORD.
**/ inline operator DWORD() const {return value;}

    bool FromDecimal(const CString &s);
    bool FromHex(const CString &s);
    inline static Doubleword ParseDecimal(const CString &s)
        {Doubleword d;d.FromDecimal(s);return d;}
    inline static Doubleword ParseHex(const CString &s)
        {Doubleword d;d.FromHex(s);return d;}
    static bool ValidateHex(CEdit *,unsigned size);
    static bool ValidateDec(CEdit *,unsigned size);

/**
Function:       Doubleword::operator= (inline)
Parameters:     [Doubleword *this]
                DWORD d
                    The new value for the doubleword.
Description:    Sets the doubleword to a new value.
**/ inline Doubleword &operator=(DWORD d) {value=d;return *this;}
};

class Float
{
protected:
    float value;
public:
    bool Parse(const CString &s);
    static bool Validate(CEdit *);

/**
Function:       Float::Float (inline)
Parameters:     [Float *this]
Returns:        void
Description:    Initializes the object to zero.
**/ inline Float() {value=0.0F;}

/**
Function:       Float::Float (inline)
Parameters:     [Float *this]
                float v
                    Value for the object.
Returns:        void
Description:    Initializes the object to the passed value.
**/ inline Float(float v) {value=v;}

/**
Function:       Float::Float (inline)
Parameters:     [Float *this]
                const CString &s
                    A string to parse for the value.
Description     Initializes the object from a string.
**/ inline Float(const CString &s) {value=0.0F;Parse(s);}

/**
Function:       Float::operator float (inline)
Parameters:     [const Float *this]
Returns:        float
                    The value of the object.
Description:    Casts the object to type float.
**/ inline operator float() const {return value;}

/**
Function:       Float::ToFixedForm (inline)
Parameters:     [const Float *this]
Returns:        DWORD
                    The fixed-point form of the floating-point value.
Description:    Converts the object to a storable DWORD.
**/ inline DWORD ToFixedForm() const
        {return Float2DWord(value);}

/**
Function:       Float::FromFixedForm (inline)
Parameters:     [Float *this]
                DWORD d
                    The fixed-point value to convert.
Returns:        void
Description:    Converts the given fixed-point value to a floating-point value.
**/ inline void FromFixedForm(DWORD d)
        {value=float(DWIPart(d))+float(DWFPart(d))/100.0F;}

/**
Function:       Float::operator= (inline)
Parameters:     [Float *this]
                float v
                    The new value for the object.
Returns:        Float &
                    A reference to the object.
Description:    Sets the value of the object.
**/ inline Float &operator=(float v) {value=v;return *this;}
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__1E98DB64_F37C_482B_8655_C31D13A3DC81__INCLUDED_)
