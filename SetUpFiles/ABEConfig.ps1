######################################################################
#
# Abstract: ABEConfig.ps1 configures the ABE and PV-WAVE 
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
.SYNOPSIS  
    This Windows powershell script is intended to support installation and 
    configuration of Windows based SSGS subsystems.
.DESCRIPTION
    ABEConfig.ps1 powershell script provides the automted starting
    and stopping of EpDBSrvc, EpSMon, SSAPISrvc and EpLPSrvc services
.EXAMPLE 
    Install only components required to build at socc-dev network site.
    PS > .\ABEConfig.ps1
#>

. .\common.ps1

$SetUpFiles=(Resolve-Path . -ea 0).Path
$ThirdPartySoftware=(Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
$InstallScripts=(Resolve-Path "..\InstallScripts" -ea 0).Path
$ABESource=(Resolve-Path "C:\abe\abe_7_23_3" -ea 0).Path
$VNIDestination=(Resolve-Path "C:\VNI\license" -ea 0).Path
$domain=$env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain

# copy pv-wave license file to C:\VNI\license\license.lic
if (([System.IO.File]::Exists("$VNIDestination\license.lic"))) {
    if (!([System.IO.File]::Exists("$VNIDestination\license.lic.bak"))) {
        mv $VNIDestination\license.lic $VNIDestination\license.lic.bak
    }
}

copy $SetUpFiles\admin\tps\ABE\pack.lic C:\VNI\license\pack.lic
Set-ItemProperty $VNIDestination\pack.lic -name IsReadOnly -value $false

if ($domain -eq "FGOESNOPS.NCWS.OPS") {
    # Fairbanks, ops
    log-and-show-me "Updating ABE license file for FGOESNOPS.NCWS.OPS!" $logfile 
	copy $SetUpFiles\admin\tps\ABE\license_fgoesnops.lic C:\VNI\license\license.lic
	set-ItemProperty $VNIDestination\license.lic -name IsReadOnly -value $false
} elseif ($domain -eq "GGOESNOPS.NCWS.OPS") {
    # Goddard, ops
    log-and-show-me "Updating ABE license file settings for GGOESNOPS.NCWS.OPS!" $logfile 
	copy $SetUpFiles\admin\tps\ABE\license_ggoesnops.lic C:\VNI\license\license.lic
	Set-ItemProperty $VNIDestination\license.lic -name IsReadOnly -value $false
} elseif ($domain -eq "NCWS.DEV") {
	# NSOF, dev
    Write-Host "Updating ABE license file for NCWS.DEV!" 
    copy $SetUpFiles\admin\tps\ABE\license_sgoesndev.lic C:\VNI\license\license.lic
	Set-ItemProperty $VNIDestination\license.lic -name IsReadOnly -value $false
} elseif ($domain -eq "NCWS.OPS") {
    # NSOF, ops
    Write-Host "Updating ABE license file for NCWS.OPS!" 
    copy $SetUpFiles\admin\tps\ABE\license_sgoesnops.lic C:\VNI\license\license.lic
	Set-ItemProperty $VNIDestination\license.lic -name IsReadOnly -value $false
} elseif ($domain -eq "WGOESNDEV.NCWS.DEV") {
    # Wallops, dev
    Write-Host "Updating ABE license file for WGOESNDEV.NCWS.DEV!"   
    copy $SetUpFiles\admin\tps\ABE\license_wgoesndev.lic C:\VNI\license\license.lic
	Set-ItemProperty $VNIDestination\license.lic -name IsReadOnly -value $false
} elseif ($domain -eq "WGOESNOPS.NCWS.OPS") {
    # Wallops, ops
    Write-Host "Updating ABE license file for WGOESNOPS.NCWS.OPS" 
    copy $SetUpFiles\admin\tps\ABE\license_wgoesnops.lic C:\VNI\license\license.lic
	Set-ItemProperty $VNIDestination\license.lic -name IsReadOnly -value $false
}

# set system environment variable LM_LICENSE_FILE
[environment]::SetEnvironmentVariable("LM_LICENSE_FILE", "C:\VNI\license\license.lic;C:\VNI\license\pack.lic", "Machine")
Write-Host "Created LM_LICENSE_FILE environment variable"

move-item $ABESource\*  -destination  C:\abe\.
remove-item $ABESource -force

$strAdminABE = (Resolve-Path "$SetUpFiles\admin\ABE" -ea 0).Path
xcopy /y/i/r/e $strAdminABE\*   C:\abe\.

# add 3 entries to services files
#grep ArchiveRequestHandler C:\Windows\System32\drivers\etc\services | out-null
$f=select-string -path "C:\Windows\System32\drivers\etc\services" -pattern ArchiveRequestHandler
if ( "$f" -eq "" ) {
    add-content -value "ArchiveRequestHandler    15021/tcp" -path C:\Windows\System32\drivers\etc\services
    add-content -value "GaRetStats    15000/tcp" -path C:\Windows\System32\drivers\etc\services
    add-content -value "GaRetTlm    15020/tcp" -path C:\Windows\System32\drivers\etc\services
}

# replace {home} in C:\abe\slsService.ini with C:\abe
#grep "WorkingDir = C:\abe" C:\abe\slsService.ini
$f=select-string -path "C:\abe\slsService.ini" -pattern "WorkingDir = C:\abe"
if ( "$f" -eq "" ) {
    (Get-Content C:\abe\slsService.ini ) | 
    Foreach-Object {$_ -replace "WorkingDir = {home}", "WorkingDir = C:\abe"} | 
    Set-Content C:\abe\slsService.ini
}



