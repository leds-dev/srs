BCGControlBar Installation instructions

PREREQUISITES
-------------
Make sure InstallShield and Microsoft Visual Studio is installed befor proceeding.
Make sure user logged on has administrative privilege

INSTALLATION 
------------
1) Navigate to <path to installed scripts>\tps2\BCGControlBar

2) Double click on BCGCBPro19-00.exe
   "Do you want to run this file" security warning pops up, press "Run".

3) InstallShield Wizard prompts for Password, enter
   3373C552-8942-4ad6-8A59-5564821FEF0B
   and press "Next >"

4) Welcome popup appears, press "Next >"

5) License Agreement popes up, select "I accept the items of the license agreement" 
   and press "Next >"
   
6) The "Setup Type" window pops up, click "Complete" and press "Next >"

7) "Ready to Install the Program" window pops up, press "Install".

8) Installation begins and can be seen on "Setup Status" window.
   
9) "InstallShield Wizard Complete" window pops up, press "Finish" to continue.

10) Welcome screen to "BCGControlBar Pro Integration Wizard", that automatically 
   Integrates the product with Microsoft Visual Studio, pops up. Press "Cancel".
   
11) Reboot