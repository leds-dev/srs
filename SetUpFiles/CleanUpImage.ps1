######################################################################
#
# Abstract: Windows Powershell 
#
# Author:  Fred Shaw
# 
#  
# Creation Date:  /2014
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by  
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 2014 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

$InstallScripts=(Resolve-Path . -ea 0).Path
$ThirdPartySoftware=(Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
$StagingArea=(Resolve-Path "..\InstallScripts" -ea 0).Path

. .\common.ps1
. .\GTACSFinalConfig.ps1
. .\Init_Viewer_ShortCuts.ps1

#setup the logging directory 
$logDir = [System.Environment]::GetFolderPath("CommonApplicationData") 
$logDir = Join-Path -Path $logDir -Childpath "Gtacs"

if(-not(Test-Path -path $logdir) )
{New-Item -Path $logdir -ItemType directory | Out-Null }  

#keep track of starting time
$startTime = (Get-Date).tostring()
$logfiletimestamp=Get-Date -format yyyyMMdd-HH-mm-ss 

#initialize the logfile to null
$logfile=$null

$logfile= Join-Path -Path $logDir -childpath "gtacs-build-install-$logfiletimestamp.log"

#define the hostname 
$MYHOSTNAME=hostname

#define the ostype 
$OSTYPE = (Get-WmiObject Win32_OperatingSystem).Caption

$Myscript = resolve-path $MyInvocation.InvocationName

$msg = "Running script: $Myscript  starting $startTime as $env:username on $MYHOSTNAME."

$seperator = "##############################################################################"

$wshShell = New-Object -ComObject "WScript.Shell"
$DESKTOP_DIR = $wshShell.SpecialFolders.Item("AllusersDesktop")

#####################################################################################################
#
# main entry point of the script
#
# 
    Delete-EpochClient_ShortCuts $logfile
    Delete-LinkedCloneTools $logfile
	Delete-Files $logfile
	
	# =============================
	# These modules are executed in the CleanDomainDependence standalone script
	#	Delete-DeskTopShortcuts $logfile
	#	Delete-StartMenuShortCuts $logfile
	#	Delete-GSS_ShortCuts $logfile
	#	Delete-GSS_Files $logfile
	#	Delete-Init_File_Viewer $logfile
	#   Delete-StartupShortCuts $logfile
    
    
    
    