######################################################################
#
# Abstract: Windows Powershell installation script for SSGS subsystems
#
# Author:
# KTTS   
# 5200 Philadelphia Way
# Lanham MD.
# fshaw@integ.com 
#  
 
# Creation Date:  4/9/2014
#
# 
# 
# 
# Disclaimer: 
# Originally developed by KTTS under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################
<#
.SYNOPSIS 
This Windows powershell script is intended to support installation and 
configuration of Windows based SSGS subsystems.

.DESCRIPTION
setup.ps1 powershell script provides the automated installation of SSGS 
Windows based subsystems.  The script provides unattended installation 
of required GTACS application software for intended subsystem,  
then installs subsystem silently, and finally configures necessary 
settings for SSGS operational environment.  The script provides command 
line switch for users to alter installation behavior if desired. 

.Example 

PS > .\EpExtendedHelp.ps1   
#>

#Running this script require admin privilage
$currentUser = [Security.Principal.WindowsIdentity]::GetCurrent()
$admin=(New-Object Security.Principal.WindowsPrincipal $currentUser).IsInRole([Security.Principal.windowsBuiltinrole]::Administrator)      
if ( -not $admin) {
    "Admin rights are required for this script"
     exit 0
}

$InstallScripts=(Resolve-Path . -ea 0).Path
$ThirdPartySoftware=(Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
$StagingArea=(Resolve-Path "..\InstallScripts" -ea 0).Path

#source the logging script to include functions
. .\common.ps1

#setup the logging directory 
$logDir = [System.Environment]::GetFolderPath("CommonApplicationData") 
$logDir = Join-Path -Path $logDir -Childpath "Gtacs"

if(-not(Test-Path -path $logdir)){
    New-Item -Path $logdir -ItemType directory | Out-Null 
}  

#define the hostname 
$MYHOSTNAME=hostname

#define the ostype 
$OSTYPE=(Get-WmiObject Win32_OperatingSystem).Caption

#keep track of starting time
$startTime = (Get-Date).tostring()
$logfiletimestamp=Get-Date -format yyyyMMdd-HH-mm-ss 

#initialize the logfile to null
# $logfile=$null
$logfile= Join-Path -Path $logDir -childpath "EpExtendedHelp-install-$logfiletimestamp.log"

$Myscript=resolve-path $MyInvocation.InvocationName

$msg = @"
Running script: $Myscript  starting $startTime as $env:username on $MYHOSTNAME.
"@

$seperator = @"
##############################################################################
"@

$wshShell=New-Object -ComObject "WScript.Shell"
$DESKTOP_DIR = $wshShell.SpecialFolders.Item("AllusersDesktop")

#####################################################################################################
#
# main entry point of the script
#
#

    log-and-show-me "Starting installation of ExHelp ..." $logfile  

    $installed=get-installed "ExHelp"
    
    if ( -not $installed ) {
        #Invoke-expression "msiexec /l*+ $logfile /i '"$StagingArea\EpochExtendedHelp\ExHelp.msi" /qn USERNAME=$currentUser | out-null"
        Invoke-expression "msiexec /l*+ $logfile /i '$StagingArea\EpochExtendedHelp\ExHelp.msi' /qn"

        # b=need to wait for installation to complete.  Less than five seconds fails
        start-sleep -s 5

        if ( $lastexitcode ){
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
            throw "Running $installer_exe failed!!. See log $logfile for more detail"  
        }
        
        C:\Windows\SysWOW64\regsvr32 /s /u C:\EPOCHClient\bin\EpExtendedHelp.dll

    	if (!([System.IO.File]::Exists("C:\EPOCHClient\bin\EpExtendedHelp.dll.1210"))) {
             mv C:\EPOCHClient\bin\EpExtendedHelp.dll C:\EPOCHClient\bin\EpExtendedHelp.dll.1210
	    }
        
        if (!([System.IO.File]::Exists("C:\EPOCHClient\bin\EpExtendedHelp.dll"))) {
             copy "C:\Program Files (x86)\Integral Systems\ExHelp\EpExtendedHelp.dll" C:\EPOCHClient\bin\EpExtendedHelp.dll
	    }
       
        C:\Windows\SysWOW64\regsvr32 /s C:\EPOCHClient\bin\EpExtendedHelp.dll  
         
    } else {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }
    
  log-and-show-me "Finished with installing of ExHelp!" $logfile 

