######################################################################
#
# Abstract: GTACSFinalConfig.ps1 configures 
#
# Authors:

#
# Frederick Shaw   
# KTTS   
# 5200 Philadelphia Way
# Lanham MD.
# fshaw@integ.com
#
#  
# Creation Date:  9/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by KTTS 
# Under  U.S. Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
.SYNOPSIS  
    This Windows powershell script is intended to support installation and 
    configuration of Windows based SSGS subsystems.
.DESCRIPTION
    GTACSFinalConfig.ps1 powershell script is the final steps for configuring the image.
    
.EXAMPLE 
    PS > .\GTACSFinalConfig.ps1
#>

 param([string]$logfile)

# dot source common.ps1 to include common logging functions
. .\common.ps1

$seperator = @"
##############################################################################
"@

# log-and-show-me "Starting GTACS workstation Configuration" $logfile  
# initialize variables to directories
$SetUpFiles = (Resolve-Path . -ea 0).Path
$ThirdPartySoftware = (Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
$InstallScripts = (Resolve-Path "..\InstallScripts" -ea 0).Path
$AdminDir = (Resolve-Path "..\SetupFiles\admin" -ea 0).Path

function install-Registrykeys
{
	# Update registry settings for Putty - domain and location dependent
	# Update registry settings  for Epoch - domain and location dependent
	# Update registry settings for DNS - domain and location dependent
	
    $domain=$env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain
	
	# Clean up convert_repository.bat 
	if (([System.IO.File]::Exists("C:\SSGS\SSGSUtil\convert_repository.bat"))) {
        log-and-show-me "delete convert_repository.bat file!" $logfile 
		remove-item "C:\SSGS\SSGSUtil\convert_repository.bat" -force
	} 
    
    # $a.ToLower().Contains("OPS".ToLower())
    # if ($domain.ToLower().Contains("FGOESNOPS".ToLower())) {    
    if ($domain -eq "FGOESNOPS.NCWS.OPS") {
        # Fairbanks, ops
        log-and-show-me "Updating registry settings for FGOESNOPS.NCWS.OPS!" $logfile 
        regedit /s "$SetUpFiles\admin\RegistryFiles\DNSReg_fgoesnops.reg"
	    # regedit /s $SetUpFiles\admin\RegistryFiles\Putty_Profiles_ops.reg # current user
		# regedit /s "$SetUpFiles\admin\RegistryFiles\SNTPsgoesnops.reg"
		regedit /s "$SetUpFiles\admin\RegistryFiles\EpSMon_Parameters_fgoesnops.reg"
		copy "C:\SSGS\SSGSUtil\convert_repository_fgoesnops.bat" "C:\SSGS\SSGSUtil\convert_repository.bat"	
    } elseif ($domain -eq "GGOESNOPS.NCWS.OPS") {
        # Goddard, ops
        log-and-show-me "Updating registry settings for GGOESNOPS.NCWS.OPS!" $logfile 
        regedit /s "$SetUpFiles\admin\RegistryFiles\DNSReg_ggoesnops.reg"
		regedit /s "$SetUpFiles\admin\RegistryFiles\EpSMon_Parameters_ggoesnops.reg"
		# regedit /s $SetUpFiles\admin\RegistryFiles\Putty_Profiles_ops.reg # curretn user
		# regedit /s "$SetUpFiles\admin\RegistryFiles\SNTPsgoesnops.reg"
		# Rename file depending on domain
		copy "C:\SSGS\SSGSUtil\convert_repository_ggoesnops.bat" "C:\SSGS\SSGSUtil\convert_repository.bat"
    } elseif ($domain -eq "NCWS.DEV") {
		# NSOF, dev
        log-and-show-me "Updating registry settings for NCWS.DEV!" $logfile 
        regedit /s "$SetUpFiles\admin\RegistryFiles\DNSReg_sgoesndev.reg"
		# regedit /s "$SetUpFiles\admin\RegistryFiles\SNTPsgoesndev.reg"
		# regedit /s $SetUpFiles\admin\RegistryFiles\Putty_Profiles_dev.reg
		regedit /s "$SetUpFiles\admin\RegistryFiles\EpSMon_Parameters_sgoesndev.reg"
	    copy "C:\SSGS\SSGSUtil\convert_repository_sgoesndev.bat" "C:\SSGS\SSGSUtil\convert_repository.bat"
    } elseif ($domain -eq "NCWS.OPS") {
        # NSOF, ops
        log-and-show-me "Updating registry settings for NCWS.OPS!" $logfile 
        regedit /s "$SetUpFiles\admin\RegistryFiles\DNSReg_sgoesnops.reg"
		# regedit /s "$SetUpFiles\admin\RegistryFiles\SNTPsgoesnops.reg"
		# regedit /s $SetUpFiles\admin\RegistryFiles\Putty_Profiles_ops.reg 
		regedit /s "$SetUpFiles\admin\RegistryFiles\EpSMon_Parameters_sgoesnops.reg"
		copy "C:\SSGS\SSGSUtil\convert_repository_sgoesnops.bat" "C:\SSGS\SSGSUtil\convert_repository.bat"
    } elseif ($domain -eq "WGOESNDEV.NCWS.DEV") {
        # Wallops, dev
        log-and-show-me "Updating registry settings for WGOESNDEV.NCWS.DEV!" $logfile   
        regedit /s "$SetUpFiles\admin\RegistryFiles\DNSReg_wgoesndev.reg"
		# regedit /s "$SetUpFiles\admin\RegistryFiles\SNTPsgoesndev.reg"
		# regedit /s $SetUpFiles\admin\RegistryFiles\Putty_Profiles_dev.reg
		regedit /s "$SetUpFiles\admin\RegistryFiles\EpSMon_Parameters_wgoesndev.reg"
		copy "C:\SSGS\SSGSUtil\convert_repository_wgoesndev.bat" "C:\SSGS\SSGSUtil\convert_repository.bat"
    } elseif ($domain -eq "WGOESNOPS.NCWS.OPS") {
        # Wallops, ops
        log-and-show-me "Updating registry settings for WGOESNOPS.NCWS.OPS" $logfile 
        regedit /s "$SetUpFiles\admin\RegistryFiles\DNSReg_wgoesnops.reg"
		# regedit /s "$SetUpFiles\admin\RegistryFiles\SNTPsgoesnops.reg"
		# regedit /s $SetUpFiles\admin\RegistryFiles\Putty_Profiles_ops.reg
		regedit /s "$SetUpFiles\admin\RegistryFiles\EpSMon_Parameters_wgoesnops.reg"
        copy "C:\SSGS\SSGSUtil\convert_repository_wgoesnops.bat" "C:\SSGS\SSGSUtil\convert_repository.bat"
    } else {
		log-and-show-me "Error: Domain not found or not joined to domain!" $logfile 
	}
	
    # Set up Windows Error Reporting for Epoch Client and other applications
    
    log-and-show-me "Updating registry settings for Epoch WER!" $logfile 
    regedit /s "$SetUpFiles\admin\RegistryFiles\EpochClient_WER.reg"
	
	#
	# copy media file to windows\media for USC - only copy if doesn't exist
	if ((![System.IO.File]::Exists("C:\Windows\Media\alert1.wav"))) {
        log-and-show-me "copying alert1.wav file!" $logfile 
		copy "$SetUpFiles\admin\media\alert1.wav" C:\Windows\Media\alert1.wav
	} else {
        #log-and-show-me "alert1.wav file exists!" $logfile    
    }
	
	# log-and-show-me "Finished with GTACS workstation Configuration!" $logfile
}

function install-DeskTopShortcuts
{
    log-and-show-me $seperator $logfile
	log-and-show-me "Updating DeskTop ShortCuts!" $logfile 
	# 
    # 
    
    $domain=$env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain
    
	$WshShell = New-Object -comObject WScript.Shell
    $strDesktopFolder = $WshShell.SpecialFolders.item("AllUsersDesktop")
    $strDeskTopSource = (Resolve-Path "$SetUpFiles\admin\Desktop_Links" -ea 0).Path
    
    $DeskTop          = Get-ChildItem $strDeskTopSource\*.lnk
    foreach ($file in $DeskTop)
    {
       if(!([System.IO.File]::Exists("$strDesktopFolder\$file.FullName"))){
         Write-Host ($file.FullName)
         # copy -Force ($file.FullName) "$strDesktopFolder\."   fgoesnops
		 copy -Force ($file.FullName) "$strDesktopFolder\."
       } else{
          log-and-show-me "$file.FullName file exists!" $logfile  
       }
    }
	
	# Copy domain dependent files
	# $a.ToLower().Contains("OPS".ToLower())
    # if ($domain.ToLower().Contains("FGOESNOPS".ToLower())) {    
    if ($domain -eq "FGOESNOPS.NCWS.OPS") {
        # Fairbanks, ops
        log-and-show-me "Updating DeskTop Shortcuts for FGOESNOPS.NCWS.OPS!" $logfile 
        $DomainDeskTop = Get-ChildItem "$strDeskTopSource\fgoesnops\*.lnk"
		foreach ($file in $DomainDeskTop) {
			if(!([System.IO.File]::Exists("$DomainDeskTop\$file.FullName"))){
				Write-Host ($file.FullName)
				# copy -Force ($file.FullName) "$strDesktopFolder\."   fgoesnops
				copy -Force ($file.FullName) "$strDesktopFolder\."
			} 
		}
    } elseif ($domain -eq "GGOESNOPS.NCWS.OPS") {
        # Goddard, ops
        log-and-show-me "Updating DeskTop Shortcuts for GGOESNOPS.NCWS.OPS!" $logfile 
        $DomainDeskTop = Get-ChildItem "$strDeskTopSource\ggoesnops\*.lnk"
        foreach ($file in $DomainDeskTop) {
			if(!([System.IO.File]::Exists("$DomainDeskTop\$file.FullName"))){
				Write-Host ($file.FullName)
				# copy -Force ($file.FullName) "$strDesktopFolder\."   fgoesnops
				copy -Force ($file.FullName) "$strDesktopFolder\."
			} 
		}
    } elseif ($domain -eq "NCWS.DEV") {
		# NSOF, dev
        log-and-show-me "Updating DeskTop Shortcuts for NCWS.DEV!" $logfile 
        $DomainDeskTop = Get-ChildItem "$strDeskTopSource\sgoesndev\*.lnk"
        foreach ($file in $DomainDeskTop) {
			if(!([System.IO.File]::Exists("$DomainDeskTop\$file.FullName"))){
				Write-Host ($file.FullName)
				# copy -Force ($file.FullName) "$strDesktopFolder\."   
				copy -Force ($file.FullName) "$strDesktopFolder\."
			} 
		}
    } elseif ($domain -eq "NCWS.OPS") {
        # NSOF, ops
        log-and-show-me "Updating DeskTop Shortcuts  for NCWS.OPS!" $logfile 
        $DomainDeskTop = Get-ChildItem "$strDeskTopSource\sgoesnops\*.lnk"
        foreach ($file in $DomainDeskTop) {
			if(!([System.IO.File]::Exists("$DomainDeskTop\$file.FullName"))){
				Write-Host ($file.FullName)
				# copy -Force ($file.FullName) "$strDesktopFolder\."   
				copy -Force ($file.FullName) "$strDesktopFolder\."
			} 
		}
    } elseif ($domain -eq "WGOESNDEV.NCWS.DEV") {
        # Wallops, dev
        log-and-show-me "Updating DeskTop Shortcuts for WGOESNDEV.NCWS.DEV!" $logfile 
        $DomainDeskTop = Get-ChildItem "$strDeskTopSource\wgoesndev\*.lnk"  
        foreach ($file in $DomainDeskTop) {
			if(!([System.IO.File]::Exists("$DomainDeskTop\$file.FullName"))){
				Write-Host ($file.FullName)
				# copy -Force ($file.FullName) "$strDesktopFolder\."   
				copy -Force ($file.FullName) "$strDesktopFolder\."
			} 
		}
    } elseif ($domain -eq "WGOESNOPS.NCWS.OPS") {
        # Wallops, ops
        log-and-show-me "Updating DeskTop Shortcuts for WGOESNOPS.NCWS.OPS" $logfile
        $DomainDeskTop = Get-ChildItem "$strDeskTopSource\wgoesnops\*.lnk" 
        foreach ($file in $DomainDeskTop) {
			if(!([System.IO.File]::Exists("$DomainDeskTop\$file.FullName"))){
				Write-Host ($file.FullName)
				# copy -Force ($file.FullName) "$strDesktopFolder\."   fgoesnops
				copy -Force ($file.FullName) "$strDesktopFolder\."
			} 
		}
    } else {
		log-and-show-me "Error: Domain not found or not joined to domain!" $logfile 
	}
}

function install-StartMenuShortCuts
{
    log-and-show-me $seperator $logfile
	log-and-show-me "Updating Start Menu ShortCuts!" $logfile
    # Get the all users Start Program directory path 	        
	$WshShell = New-Object -comObject WScript.Shell
	$Path = $WshShell.SpecialFolders.item("AllUsersPrograms")
    $domain=$env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain
     
    # Copy all links in admin\NewStartmenu
    $List = (Resolve-Path "$AdminDir\NewStartmenu" -ea 0).Path
    $StartMenu = Get-ChildItem $List\*.lnk
    
   if (!(test-path("$Path\SSGS\"))) {
        new-item -path "$Path\" -name "SSGS" -type directory
	} 
         
    foreach ($file in $StartMenu)
    {
       Write-Host ($file.FullName)
       attrib -R /S /D $Path
       # copy -Force ($file.FullName) $Path\. -recurse
	   copy -Force ($file.FullName) $Path\SSGS\. 
    }
	
	# Copy domain dependent files
	# $a.ToLower().Contains("OPS".ToLower())
    # if ($domain.ToLower().Contains("FGOESNOPS".ToLower())) {    
    if ($domain -eq "FGOESNOPS.NCWS.OPS") {
        # Fairbanks, ops
        log-and-show-me "Updating Start Menu settings for FGOESNOPS.NCWS.OPS!" $logfile 
		$DomainStartMenu = Get-ChildItem $List\fgoesnops\*.lnk
		foreach ($file in $DomainStartMenu) {
			if(!([System.IO.File]::Exists("$DomainStartMenu\$file.FullName"))){
				Write-Host ($file.FullName)
				attrib -R /S /D $Path
				copy -Force ($file.FullName) $Path\SSGS\.
			} 
		}
	} elseif ($domain -eq "GGOESNOPS.NCWS.OPS") {
        # Goddard, ops
        log-and-show-me "Updating  settings for GGOESNOPS.NCWS.OPS!" $logfile 
        $DomainStartMenu = Get-ChildItem "$List\ggoesnops\*.lnk"
		foreach ($file in $DomainStartMenu) {
			if(!([System.IO.File]::Exists("$DomainStartMenu\$file.FullName"))){
				Write-Host ($file.FullName)
				attrib -R /S /D $Path
				copy -Force ($file.FullName) $Path\SSGS\.
			} 
		}
    } elseif ($domain -eq "NCWS.DEV") {
		# NSOF, dev
        log-and-show-me "Updating Start Menu settings for NCWS.DEV!" $logfile 
        $DomainStartMenu = Get-ChildItem "$List\sgoesndev\*.lnk"
		foreach ($file in $DomainStartMenu) {
			if(!([System.IO.File]::Exists("$DomainStartMenu\$file.FullName"))){
				Write-Host ($file.FullName)
				attrib -R /S /D $Path
				copy -Force ($file.FullName) $Path\SSGS\.
			} 
		}
    } elseif ($domain -eq "NCWS.OPS") {
        # NSOF, ops
        log-and-show-me "Updating Start Menu settings for NCWS.OPS!" $logfile 
        $DomainStartMenu = Get-ChildItem "$List\sgoesnops\*.lnk"
		foreach ($file in $DomainStartMenu) {
			if(!([System.IO.File]::Exists("$DomainStartMenu\$file.FullName"))){
				Write-Host ($file.FullName)
				attrib -R /S /D $Path
				copy -Force ($file.FullName) $Path\SSGS\.
			} 
		}
    } elseif ($domain -eq "WGOESNDEV.NCWS.DEV") {
        # Wallops, dev
        log-and-show-me "Updating Start Menu settings for WGOESNDEV.NCWS.DEV!" $logfile   
        $DomainStartMenu = Get-ChildItem "$List\wgoesndev\*.lnk"
		foreach ($file in $DomainStartMenu) {
			if(!([System.IO.File]::Exists("$DomainStartMenu\$file.FullName"))){
				Write-Host ($file.FullName)
				attrib -R /S /D $Path
				copy -Force ($file.FullName) $Path\SSGS\.
			} 
		}
    } elseif ($domain -eq "WGOESNOPS.NCWS.OPS") {
        # Wallops, ops
        log-and-show-me "Updating Start Menu settings for WGOESNOPS.NCWS.OPS" $logfile 
        $DomainStartMenu = Get-ChildItem "$List\wgoesnops\*.lnk"
		foreach ($file in $DomainStartMenu) {
			if(!([System.IO.File]::Exists("$DomainStartMenu\$file.FullName"))){
				Write-Host ($file.FullName)
				attrib -R /S /D $Path
				copy -Force ($file.FullName) $Path\SSGS\.
			} 
		}
    } else {
		log-and-show-me "Error: Domain not found or not joined to domain!" $logfile 
	}
          
    log-and-show-me $seperator $logfile
	log-and-show-me "Updating Start Menu RestoreWorkspaceOptions ShortCuts!" $logfile
    # if it doesn't exist, create directory it
               
    if (!(test-path("$Path\SSGS\RestoreWorkspaceOptions"))) {
        new-item -path "$Path\SSGS\" -name "RestoreWorkspaceOptions" -type directory
	} 
    
    $List = (Resolve-Path "$AdminDir\NewStartmenu\RestoreWorkspaceOptions" -ea 0).Path
    $StartMenu = Get-ChildItem $List\*.lnk -Force
         
    foreach ($file in $StartMenu)
    {
       Write-Host ($file.FullName)
       attrib -R /S /D $Path\SSGS\RestoreWorkspaceOptions
       copy -Force ($file.FullName) $Path\SSGS\RestoreWorkspaceOptions\. -recurse
    }

	# Moved from here to the startup script: 9UpdateSSGSShortcuts.bat
    # log-and-show-me $seperator $logfile
	# log-and-show-me "Updating Start Menu SaveWorkspaceOptions ShortCuts!" $logfile
    # first need to create directory
    # if (!(test-path("$Path\SaveWorkspaceOptions"))) {
    #    new-item -path "$Path\" -name "SaveWorkspaceOptions" -type directory
	# } 
    
    # $List = (Resolve-Path "$AdminDir\NewStartmenu\SaveWorkspaceOptions" -ea 0).Path
    # $StartMenu = Get-ChildItem $List\*.lnk -Force
         
    # foreach ($file in $StartMenu)
    # {
    #   Write-Host ($file.FullName)
    #   attrib -R /S /D $Path\SaveWorkspaceOptions
    #   copy -Force ($file.FullName) $Path\SaveWorkspaceOptions\. -recurse
    # }
}
 
function install-GSS_ShortCuts
{
    #
	log-and-show-me $seperator $logfile
    log-and-show-me "Updating Start Menu ShortCuts For GSS ShortCuts!" $logfile
    # Get the all users Start Program directory path 	        
	$WshShell = New-Object -comObject WScript.Shell
	$Path = $WshShell.SpecialFolders.item("AllUsersPrograms")
    $domain = $env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain
    
    if (!(test-path("$Path\SSGS\"))) {
        new-item -path "$Path\" -name "SSGS" -type directory
	} 
    
    # if directory doesn't exist create it 
    if (!(test-path("$Path\SSGS\Ground SIAD Software (GSS)"))) {
        new-item -path "$Path\SSGS\" -name "\Ground SIAD Software (GSS)" -type directory
	}
    
    if ($domain.ToLower().Contains("OPS".ToLower())) {
       #log-and-show-me "Updating for OPS!" $logfile 
       $strGSS = (Resolve-Path "$AdminDir\GSS_Start_Menu\Ops" -ea 0).Path
	   $StartMenu = Get-ChildItem $strGSS\*.lnk -Force 
       #Write-Host ($StartMenu)
       log-and-show-me "Updating Ground SIAD Software (GSS) ShortCuts!" $logfile
       foreach ($file in $StartMenu)
       {
          Write-Host ($file.FullName)
          attrib -R /S /D "$Path\SSGS\Ground SIAD Software (GSS)"
          copy -Force ($file.FullName) "$Path\SSGS\Ground SIAD Software (GSS)\." -recurse
       }
   } elseif ($domain.ToLower().Contains("DEV".ToLower())) {
       #log-and-show-me "Updating for DEV!" $logfile 
       $strGSS = (Resolve-Path "$AdminDir\GSS_Start_Menu\Dev" -ea 0).Path
	   $StartMenu = Get-ChildItem $strGSS\*.lnk -Force 
       #Write-Host ($StartMenu)
       log-and-show-me "Updating Ground SIAD Software (GSS) ShortCuts!" $logfile
       foreach ($file in $StartMenu)
       {
          Write-Host ($file.FullName)
          attrib -R /S /D "$Path\SSGS\Ground SIAD Software (GSS)"
          copy -Force ($file.FullName) "$Path\SSGS\Ground SIAD Software (GSS)\." -recurse
       }
   } elseif ($domain.ToLower().Contains("SGROUP".ToLower())) {
       #log-and-show-me "Updating for SGROUP!" $logfile 
       $strGSS = (Resolve-Path "$AdminDir\GSS_Start_Menu\Ops" -ea 0).Path
	   $StartMenu = Get-ChildItem $strGSS\*.lnk -Force 
       #Write-Host ($StartMenu)
       log-and-show-me "Updating Ground SIAD Software (GSS) ShortCuts!" $logfile
       foreach ($file in $StartMenu)
       {
          Write-Host ($file.FullName)
          attrib -R /S /D "$Path\SSGS\Ground SIAD Software (GSS)"
          copy -Force ($file.FullName) "$Path\SSGS\Ground SIAD Software (GSS)\." -recurse
       }
   } else {
       log-and-show-me "Domain not OPS or DEV!" $logfile 
   }
}

function install-GSS_Files
{
    #
    log-and-show-me $seperator $logfile
	log-and-show-me "Installing GSS files!" $logfile
	# source is C:\Setup\SetUpFiles\admin\GSS_Start_Menu\
	#
    $domain = $env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain
    $strGSSFileLoc = (Resolve-Path "C:\SSGS\GSS" -ea 0).Path
    
    # if directory doesn't exist create it 
    if (!(test-path("C:\SSGS\GSS\"))) {
        new-item -path "C:\SSGS\" -name "\GSS" -type directory
	}
    
    if ($domain.ToLower().Contains("OPS".ToLower())) {
       #log-and-show-me "Updating for OPS!" $logfile 
       $strGSS = (Resolve-Path "$AdminDir\GSS_Start_Menu\Ops" -ea 0).Path
	   $StartMenu = Get-ChildItem "$strGSS\*" -Force 
       # Write-Host ($StartMenu)
       log-and-show-me "Updating GSS files!" $logfile
       foreach ($file in $StartMenu)
       {
          Write-Host ($file.FullName)
          copy -Force "$file" "C:\SSGS\GSS\" -recurse
       }
   } elseif ($domain.ToLower().Contains("DEV".ToLower())) {
       #log-and-show-me "Updating for DEV!" $logfile 
       $strGSS = (Resolve-Path "$AdminDir\GSS_Start_Menu\Dev" -ea 0).Path
	   $StartMenu = Get-ChildItem "$strGSS\*" -Force 
       # Write-Host ($StartMenu)
       log-and-show-me "Updating GSS files!" $logfile
       foreach ($file in $StartMenu)
       {
          Write-Host ($file.FullName)
          copy -Force "$file" "C:\SSGS\GSS\" -recurse
       }
   } else {
       log-and-show-me "Domain not OPS or DEV!" $logfile 
   }
}

function Move-EpochClient_ShortCuts
{
    #
	log-and-show-me $seperator $logfile
    log-and-show-me "Updating Start Menu ShortCuts For Epoch Client ShortCuts!" $logfile
    # Get the all users Start Program directory path 	        
	$WshShell = New-Object -comObject WScript.Shell
	$Path = $WshShell.SpecialFolders.item("AllUsersPrograms")
    
    
    if (!(test-path("$Path\SSGS\"))) {
        new-item -path "$Path\" -name "SSGS" -type directory
	} 
    
    # if directory doesn't exist create it 
    if (!(test-path("$Path\SSGS\Epoch Client\"))) {
        new-item -path "$Path\SSGS\" -name "\Epoch Client" -type directory
	}
    
    #log-and-show-me "Updating for OPS!" $logfile 
    $strEpochClient = (Resolve-Path "$Path\EPOCH Client\" -ea 0).Path
	$StartMenu = Get-ChildItem $strEpochClient\*.lnk -Force 
    #Write-Host ($StartMenu)
    log-and-show-me "Updating ShortCuts!" $logfile
      
    foreach ($file in $StartMenu) {
        Write-Host ($file.FullName)
        attrib -R /S /D "$Path\SSGS\EPOCH Client"
        copy -Force ($file) "$Path\SSGS\EPOCH Client\." -recurse
    }
}

function Copy-LinkedCloneTools
{
	# Save off the LinkedCloneTools directory to the C: drive        
	$strLCTSource = "C:\setup\SetUpFiles\LinkedCloneTools"
    $strLCTDestination =  "C:\LinkedCloneTools" 
    # Write-Host ($strLCTDestination)
	log-and-show-me $seperator $logfile
	
	if (test-path("$strLCTSource")) {
		log-and-show-me "Saving off the LinkedCloneTools directory to the C: drive!" $logfile 
		if (test-path("$strLCTDestination")) {
			# log-and-show-me "Found C:\LinkedCloneTools directory!" $logfile
			remove-item -force $strLCTDestination -recurse 
			# need a delay otherwise get an error
			start-sleep -s 1
		}
		
		new-item -path "$strLCTDestination" -type directory 
			
		# Saving off the Linked Clone Tool applications
		xcopy /y/i/r/e /q $strLCTSource\* $strLCTDestination\. 
	} else {
		log-and-show-me "LinkedCloneTools source directory not found!" $logfile 
	}
}

function delete-DeskTopShortcuts
{
	log-and-show-me $seperator $logfile
    log-and-show-me "Deleting DeskTop ShortCuts!" $logfile 
	# 
    # 
    $domain = $env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain
    
	$WshShell         = New-Object -comObject WScript.Shell
    $strDesktopFolder = $WshShell.SpecialFolders.item("AllUsersDesktop")
       
    if(([System.IO.File]::Exists("$strDesktopFolder\ABE CONVERT.lnk"))){
          Write-Host("$strDesktopFolder\ABE CONVERT.lnk")
          remove-item -Force  "$strDesktopFolder\ABE CONVERT.lnk"
    } 
    
    if(([System.IO.File]::Exists("$strDesktopFolder\ABE GAS.lnk"))){
          Write-Host("$strDesktopFolder\ABE GAS.lnk")
          remove-item -Force  "$strDesktopFolder\ABE GAS.lnk"
    }     
    
    if(([System.IO.File]::Exists("$strDesktopFolder\Stream Viewer.lnk"))){
          Write-Host("$strDesktopFolder\Stream Viewer.lnk")
          remove-item -Force  "$strDesktopFolder\Stream Viewer.lnk"
    } 
    
    if(([System.IO.File]::Exists("$strDesktopFolder\CPMTs.lnk"))){
          Write-Host("$strDesktopFolder\CPMTs.lnk")
          remove-item -Force  "$strDesktopFolder\CPMTs.lnk"
    } 
     
    if(([System.IO.File]::Exists("$strDesktopFolder\Goesnopblue_DEV.lnk"))){
          Write-Host("$strDesktopFolder\Goesnopblue_DEV.lnk")
          remove-item -Force  "$strDesktopFolder\Goesnopblue_DEV.lnk"
    } 
    
    if(([System.IO.File]::Exists("$strDesktopFolder\Goesnopblue_OPS.lnk"))){
          Write-Host("$strDesktopFolder\Goesnopblue_OPS.lnk")
          remove-item -Force  "$strDesktopFolder\Goesnopblue_OPS.lnk"
    } 
    
    if(([System.IO.File]::Exists("$strDesktopFolder\UsersGuides and Documents.lnk"))){
          Write-Host("$strDesktopFolder\UsersGuides and Documents.lnk")
          remove-item -Force  "$strDesktopFolder\UsersGuides and Documents.lnk"
    }

	log-and-show-me "Remove Snipping Tool lnk from Desktop" $logfile
	if(([System.IO.File]::Exists("$strDesktopFolder\Snipping Tool.lnk"))){
         Write-Host("$strDesktopFolder\Snipping Tool.lnk")
         remove-item -Force  "$strDesktopFolder\Snipping Tool.lnk"
    }
}


function Delete-StartMenuShortCuts
{
	log-and-show-me $seperator $logfile
    log-and-show-me "Deleting Start Menu ShortCuts!" $logfile
    # Get the all users Start Program directory path 	        
	$WshShell = New-Object -comObject WScript.Shell
	$Path = $WshShell.SpecialFolders.item("AllUsersPrograms")
    $SSGSPath = (Resolve-Path "$Path\SSGS" -ea 0).Path
    
    if(([System.IO.File]::Exists("$SSGSPath\CPMTs.lnk"))){
          Write-Host("$SSGSPath\CPMTs.lnk")
          remove-item -Force  "$SSGSPath\CPMTs.lnk"
    }
    
    if(([System.IO.File]::Exists("$SSGSPath\UsersGuides and Documents.lnk"))){
          Write-Host("$SSGSPath\UsersGuides and Documents.lnk")
          remove-item -Force  "$SSGSPath\UsersGuides and Documents.lnk"
    } 
    
    if(([System.IO.File]::Exists("$SSGSPath\ABE Batch Server GUI.lnk"))){
          Write-Host("$SSGSPath\ABE Batch Server GUI.lnk")
          remove-item -Force  "$SSGSPath\ABE Batch Server GUI.lnk"
    }      
          
    log-and-show-me $seperator $logfile
	log-and-show-me "Deleting Start Menu RestoreWorkspaceOptions ShortCuts!" $logfile
   
    $List = (Resolve-Path "$SSGSPath\RestoreWorkspaceOptions" -ea 0).Path
    $StartMenu = Get-ChildItem $List\*.lnk -Force
         
    foreach ($file in $StartMenu) {
       $shortname = $file.name
       if(([System.IO.File]::Exists("$SSGSPath\$shortname"))){
           Write-Host ("Deleting $SSGSPath\RestoreWorkspaceOptions\$shortname")
           remove-item -Force  "$SSGSPath\RestoreWorkspaceOptions\$shortname"
       } 
    }
    
	# need delay to prevent error
	start-sleep -s 1   
    if ((test-path("$SSGSPath\RestoreWorkspaceOptions"))) {
		Write-Host ("Deleting $SSGSPath\RestoreWorkspaceOptions")
        remove-item -Force "$Path\SSGS\RestoreWorkspaceOptions" -Recurse
	}
	
	log-and-show-me $seperator $logfile
	log-and-show-me "Deleting Start Menu SaveWorkspaceOptions ShortCuts!" $logfile
   
    $wshShell = New-Object -ComObject "WScript.Shell"
    $Program_Dir = $wshShell.SpecialFolders.Item("PROGRAMS")
	$SSGSProgramsPath = (Resolve-Path "$Program_Dir\SSGS" -ea 0).Path
   
    $List2 = (Resolve-Path "$SSGSProgramsPath\SaveWorkspaceOptions" -ea 0).Path
    $StartMenu2 = Get-ChildItem $List2\*.lnk -Force
         
    foreach ($file2 in $StartMenu2)
    {
       $shortname2 = $file2.name
       if(([System.IO.File]::Exists("$SSGSProgramsPath\$shortname2"))){
           Write-Host ("$SSGSProgramsPath\SaveWorkspaceOptions\$shortname2")
           remove-item -Force  "$SSGSProgramsPath\SaveWorkspaceOptions\$shortname2"
       } 
    }

    # need delay to prevent error
	start-sleep -s 1  
    if ((test-path("$SSGSProgramsPath\SaveWorkspaceOptions"))) {
        remove-item -Force "$SSGSProgramsPath\SSGS\SaveWorkspaceOptions" -Recurse
	}
	
	if ((Get-ChildItem -Path "$SSGSProgramsPath") -eq $null) {
		Write-Host ("Deleting $SSGSProgramsPath")
        remove-item -Force "$SSGSProgramsPath" 
    } 	
	  
    if ((Get-ChildItem -Path "$SSGSPath") -eq $null) {
		Write-Host ("Deleting $SSGSPath")
        remove-item -Force "$SSGSPath" 
    } 
}
   
function delete-GSS_ShortCuts
{
    #
	log-and-show-me $seperator $logfile
    log-and-show-me "Deleting Start Menu ShortCuts For GSS ShortCuts!" $logfile
    # Get the all users Start Program directory path 	        
	$WshShell = New-Object -comObject WScript.Shell
	$Path = $WshShell.SpecialFolders.item("AllUsersPrograms")
    $domain = $env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain
    $strGSS_StartMenu = "$Path\SSGS\Ground SIAD Software (GSS)"
    # $SSGSPath = (Resolve-Path "$Path\SSGS" -ea 0).Path
    $SSGSPath = "$Path\SSGS"
    
    # if directory doesn't exist create it 
    if ((test-path("$Path\SSGS\Ground SIAD Software (GSS)"))) {
        
       # $strGSS = (Resolve-Path "$AdminDir\GSS_Start_Menu\Ops" -ea 0).Path
   	   $StartMenu = Get-ChildItem "$strGSS_StartMenu\*.lnk" -Force 
           
       # log-and-show-me "Deleting Ground SIAD Software (GSS) ShortCuts!" $logfile
       foreach ($file in $StartMenu) {
          $shortname = $file.name
          if(([System.IO.File]::Exists("$strGSS_StartMenu\$shortname"))){
                Write-Host ("Deleting $shortname")
                remove-item -Force  "$strGSS_StartMenu\$shortname" 
          }
      }
	  
	  # need delay to prevent error
	  start-sleep -s 1     
      
	  if ((test-path("$strGSS_StartMenu"))) {
		if ((Get-ChildItem -Path "$strGSS_StartMenu") -eq $null) {
			Write-Host ("Deleting $strGSS_StartMenu")
			remove-item -Force "$strGSS_StartMenu" 
		}
	  }
    }
   
   if ((test-path("$SSGSPath"))) {
        if ((Get-ChildItem -Path "$SSGSPath") -eq $null) {
			Write-Host ("Deleting $strGSS_StartMenu")
            remove-item -Force "$SSGSPath" 
       }
   }
   
   # $wshShell = New-Object -ComObject "WScript.Shell"
   #	$PROGRAM_DIR = $wshShell.SpecialFolders.Item("Programs")
}

function delete-GSS_Files
{
   #
   log-and-show-me $seperator $logfile
   log-and-show-me "Deleting GSS files!" $logfile
   # source is C:\Setup\SetUpFiles\admin\GSS_Start_Menu\
   #
   $domain = $env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain
   # $strGSSFileLoc = (Resolve-Path "C:\SSGS\GSS" -ea 0).Path
   $strGSS_SSHKeys = "C:\SSGS\GSS\OpenSSHKeys" 
   $SSGSPath = "C:\SSGS"
   $strGSSFileLoc = "C:\SSGS\GSS"   
  
	if (test-path("$strGSSFileLoc")) { 
		remove-item -Force "$strGSSFileLoc" -recurse
		log-and-show-me "Deleted GSS files!" $logfile
	}
	
	<#
   if (test-path("$strGSSFileLoc")) { 
       $StartMenu = Get-ChildItem "$strGSSFileLoc\*" -Force -recurse
       # Write-Host ($StartMenu)
       log-and-show-me "Deleting GSS files!" $logfile
       foreach ($file in $StartMenu) {
          $shortname = $file.name
          if(([System.IO.File]::Exists("$strGSSFileLoc\$shortname"))){
             Write-Host ($shortname)
             remove-item -Force "$strGSSFileLoc\$shortname"  -recurse
          }
       }
    }
	#>
	
	if (test-path("$strGSS_SSHKeys")) { 
		remove-item -Force "$strGSS_SSHKeys" -recurse
		log-and-show-me "Deleted GSS SSH keys!" $logfile
	}
  
   if (test-path("$strGSS_SSHKeys")){
         Write-Host ($strGSS_SSHKeys)
         remove-item -Force "$strGSS_SSHKeys" 
   }
      
   if (test-path("$strGSSFileLoc")){
         Write-Host ($strGSSFileLoc)
         remove-item -Force "$strGSSFileLoc" 
   }
   
   # Check if directory exists
   if (test-path("$SSGSPath")){
   # Check if it is empty
		if ((Get-ChildItem -Path "$SSGSPath") -eq $null) {
			remove-item -Force "$SSGSPath" 
		}
	}
}

function delete-EpochClient_ShortCuts
{
    #
	log-and-show-me $seperator $logfile
    # Get the all users Start Program directory path 	        
	$WshShell = New-Object -comObject WScript.Shell
	$Path = $WshShell.SpecialFolders.item("AllUsersPrograms")
    $SSGSPath = "$Path\SSGS" 
    $strEpochClient = "$SSGSPath\EPOCH Client\" 
    
	if (test-path("$strEpochClient")) {
    	remove-item -Force  "$strEpochClient" -recurse
		log-and-show-me "Deleting Epoch shortcuts and directory!" $logfile
    }
	
	<#
	if ((test-path("$strEpochClient"))) {
    	$StartMenu = Get-ChildItem $strEpochClient\*.lnk -Force 
        #Write-Host ($StartMenu)
        # log-and-show-me "Updating ShortCuts!" $logfile
          
        foreach ($file in $StartMenu) {
           $shortname = $file.name
           if(([System.IO.File]::Exists("$strEpochClient\$shortname"))){
              remove-item -Force  "$strEpochClient\$shortname"
           } 
        }
    }
	#>
  
	# $wshShell = New-Object -ComObject "WScript.Shell"
    # $PROGRAM_DIR = $wshShell.SpecialFolders.Item("Programs")
	
    # Delete the directory if it is exists and is empty 
	if (test-path("$SSGSPath")) {
        if (($Ret =  (Get-ChildItem -Path "$SSGSPath")) -eq $null) {
            remove-item -Force "$SSGSPath" 
	    }
    }
}

function Delete-LinkedCloneTools
{
    #
	log-and-show-me $seperator $logfile
    log-and-show-me "Deleting LinkedCloneTools Directory!" $logfile
	# Remove C:\LinkedCloneTools directory        
	# $strLCTSource = (Resolve-Path "C:\setup\SetUpFiles\LinkedCloneTools" -ea 0).Path 
    $strLCTDestination = "C:\LinkedCloneTools" 
    # Write-Host ($strLCTDestination)
	    
	if (test-path("$strLCTDestination")) {
		remove-item -force $strLCTDestination -recurse 
	}
}

function Delete-StartUpLinks
{
    #
	log-and-show-me $seperator $logfile
    log-and-show-me "Deleting StartUp links!" $logfile	
    # delete DirMapper in Startup
	if ([System.IO.File]::Exists("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Launch DirMapper.lnk")) {
	    remove-item -force "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Launch DirMapper.lnk"
		log-and-show-me "Deleted DirMapper in Startup" $logfile		
    }
	
    # delete T-Clock in Startup
	if ([System.IO.File]::Exists("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Launch T-Clock 2010.lnk")) {
	    remove-item -force "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Launch T-Clock 2010.lnk"
		log-and-show-me "Deleted T-Clock in Startup" $logfile		
    }
	
	# Deleting Win7 T-Clock in Start Menu\Programs\Startup\
	if ([System.IO.File]::Exists("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Win7 T-Clock 2010.lnk")) {
	    log-and-show-me "Delete Win7 T-Clock 2010.lnk in Startup" $logfile
	    remove-item -force "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Win7 T-Clock 2010.lnk" 
    }
	        
	# Delete UpdateSSGSShortcuts.bat in Startup 
	if ([System.IO.File]::Exists("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\UpdateSSGSShortcuts.lnk")) {
		remove-item -Force  "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\UpdateSSGSShortcuts.lnk" 
		log-and-show-me "Deleted UpdateSSGSShortcuts.bat in Startup " $logfile
	}
	
	# Delete USCsetenv in Startup
	if ([System.IO.File]::Exists("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\USCsetenv.lnk")) {
		remove-item -Force "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\USCsetenv.lnk" 
		log-and-show-me "Deleted USCsetenv in Startup" $logfile
	}
	
	# Delete bringforward in Startup
	if ([System.IO.File]::Exists("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\BringForward.lnk")) {
		remove-item -Force "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\BringForward.lnk" 
		log-and-show-me "Deleted BringForward in Startup" $logfile
	}
	
	# Delete BGInfo in Startup
	if ([System.IO.File]::Exists("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\BGInfo.bat")) {
		remove-item -Force "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\BGInfo.bat" 
		log-and-show-me "Deleted BGInfo in Startup" $logfile
	}
}		

function Delete-Files
{
	#
	log-and-show-me $seperator $logfile
	log-and-show-me "Deleting files!" $logfile 
	$Path = $WshShell.SpecialFolders.item("AllUsersPrograms")
   	
	# Delete EpExtendedHelp.dll.1210 in C:\EpochClient\bin\
    if ([System.IO.File]::Exists("C:\EPOCHClient\bin\EpExtendedHelp.dll.1210")) {
	    log-and-show-me "Deleting C:\EPOCHClient\bin\EpExtendedHelp.dll.1210" $logfile   
		remove-item -force  "C:\EPOCHClient\bin\EpExtendedHelp.dll.1210"
	}
    
    # Delete \SSAPISrvc.exe.121 in C:\EpochClient\bin	
    if ([System.IO.File]::Exists("C:\EPOCHClient\bin\SSAPISrvc.exe.121")) {
		log-and-show-me "Deleting C:\EPOCHClient\bin\SSAPISrvc.exe.121" $logfile   
        remove-item -force  "C:\EPOCHClient\bin\SSAPISrvc.exe.121" 
    }
	
	# Delete EpServiceCtrl.exe in C:\EpochClient\bin
	if ([System.IO.File]::Exists("C:\EPOCHClient\bin\EpServiceCtrl.exe")) {
        log-and-show-me "Deleting EpServiceCtrl.exe in C:\EpochClient\bin" $logfile
        remove-item -force "C:\EPOCHClient\bin\EpServiceCtrl.exe"
    }
	
	# Delete the C:\EPOCHClient\output\Get Client directory  
	if (test-path("C:\EPOCHClient\output\GET Client")) {
		if (($Ret =  (Get-ChildItem -Path "C:\EPOCHClient\output\GET Client")) -eq $null) {
			log-and-show-me "Deleting C:\EPOCHClient\output\GET Client" $logfile
			remove-item -force "C:\EPOCHClient\output\GET Client"
		} else {
			log-and-show-me "C:\EPOCHClient\output\GET Client directory is not empty!" $logfile
			log-and-show-me "C:\EPOCHClient\output\GET Client directory was not deleted!" $logfile
		}
	}
      
    # Delete the C:\EPOCHClient\output directory  
	if (test-path("C:\EPOCHClient\output")) {
		if (($Ret =  (Get-ChildItem -Path "C:\EPOCHClient\output")) -eq $null) {
			log-and-show-me "Deleting C:\EPOCHClient\output directory" $logfile
			remove-item -force C:\EPOCHClient\output 
		} else {
			log-and-show-me "C:\EPOCHClient\output directory is not empty!" $logfile
			log-and-show-me "C:\EPOCHClient\output directory was not deleted!" $logfile
		}
	}
	
	# Delete the C:\EPOCHClient\bin directory  
	if (test-path("C:\EPOCHClient\bin")) {
		if (($Ret =  (Get-ChildItem -Path "C:\EPOCHClient\bin")) -eq $null) {
			log-and-show-me "Deleting C:\EPOCHClient\bin directory" $logfile
			remove-item -force C:\EPOCHClient\bin 
		} else {
			log-and-show-me "C:\EPOCHClient\bin directory is not empty!" $logfile
			log-and-show-me "C:\EPOCHClient\bin directory was not deleted!" $logfile
		}
	}
	
	# Delete the C:\EPOCHClient\ directory  
	if (test-path("C:\EPOCHClient")) {
		if (($Ret =  (Get-ChildItem -Path "C:\EPOCHClient")) -eq $null) {
			log-and-show-me "Deleting C:\EPOCHClient directory" $logfile
			remove-item -force C:\EPOCHClient 
		} else {
			log-and-show-me "C:\EPOCHClient directory is not empty!" $logfile
			log-and-show-me "C:\EPOCHClient directory was not deleted!" $logfile
		}
	}
   
    # Delete Bginfo utility files
	if (test-path("C:\Program Files (x86)\BGinfo")) {
		log-and-show-me "Deleting C:\Program Files (x86)\BGinfo" $logfile
		remove-item -force "C:\Program Files (x86)\BGinfo" -recurse
	}
	
	# if ([System.IO.File]::Exists("C:\Program Files (x86)\BGinfo\Bginfo.bgi")) {
	#	log-and-show-me "Deleting C:\Program Files (x86)\BGinfo\Bginfo.bgi" $logfile
    #    remove-item -force "C:\Program Files (x86)\BGinfo\Bginfo.bgi"
	# }
    
    # if ([System.IO.File]::Exists("C:\Program Files (x86)\BGinfo\Bginfo.bat")) {
	#	log-and-show-me "Deleting C:\Program Files (x86)\BGinfo\Bginfo.bat" $logfile
    #    remove-item -force "C:\Program Files (x86)\BGinfo\Bginfo.bat"
	# }
    
    # if ([System.IO.File]::Exists("C:\Program Files (x86)\BGinfo\Bginfo.exe")) {
	#	log-and-show-me "Deleting C:\Program Files (x86)\BGinfo\Bginfo.exe" $logfile
    #    remove-item -force "C:\Program Files (x86)\BGinfo\Bginfo.exe"
	# }
}

function Delete-EpSMonLog 
{
    log-and-show-me $seperator $logfile
	log-and-show-me "Deleting the stream messages log file!" $logfile 

	# ========================================
	# Stop EpMon service if running

    # net stop EpSMon 
    # net stop EpDBSrvc
        
    $strComputer = "localhost"
    $strClass    = "win32_service"
    $strService  = "EpSMon"
    $objWmiService = Get-Wmiobject -Class $strClass -computer $strComputer -filter "name = '$strService'"
    if ($objWMIService.state -eq "Running"){ 
        log-and-show-me "stopping the $strService service now ..." $logfile
        $rtn = $objWMIService.stopService()
    } else { 
        log-and-show-me "$strService is not running" $logfile
    }
    
    # need to wait for installation to complete.  Less than five seconds fails
    start-sleep -s 1    
	
	# Deleting stream messages log file in C:\EPOCHClient\Logs
	if ([System.IO.File]::Exists("C:\EPOCHClient\Logs\EpSMon.log")) {
		log-and-show-me "Deleting EpSMon.log in C:\EPOCHClient\Logs\" $logfile 
		remove-item -force "C:\EPOCHClient\Logs\EpSMon.log" 
    } else {
		log-and-show-me "EpSMon.log not found in C:\EPOCHClient\Logs\" $logfile 
	}
}


<#
	
#>

