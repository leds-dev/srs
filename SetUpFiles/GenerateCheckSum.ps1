######################################################################
#
# Abstract: Install SNTPServ and related tool set silently using all 
# default options of the installer.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
# .SYNOPSIS 
# Install SNTPServ and related tool set silently using all default options
# of the installer.
#
# 
# .DESCRIPTION
# install-SNTPServ provides unattended installatino of SNTPServ tool set.
#
# .Example 
# PS > .\install-SNTPServ.ps1 $installer_exe $logfile $app_name \
#         $app_display_name $install_dir $username
#
#>

#dot source common.ps1 to include common logging functions
. .\common.ps1

 $wshShell = New-Object -ComObject "WScript.Shell"
 $PROGRAM_DIR = $wshShell.SpecialFolders.Item("ALLUsersPrograms")

function GenerateCheckSum {
    param([string]$logfile)
    	
    log-and-show-me "Generating CheckSum  ..." $logfile  
    	
	if ([System.IO.File]::Exists("C:\Program Files (x86)\Integral Systems\ChecksumSSGS\ssgs.w7.cfg.txt")) {
	 	remove-item "C:\Program Files (x86)\Integral Systems\ChecksumSSGS\ssgs.w7.cfg.txt" -force
	}
	
	if ([System.IO.File]::Exists("C:\Program Files (x86)\Integral Systems\ChecksumSSGS\ssgs.w7.report.txt")) {
	 	remove-item "C:\Program Files (x86)\Integral Systems\ChecksumSSGS\ssgs.w7.report.txt" -force 
	}
    
    start "C:\Program Files (x86)\Integral Systems\ChecksumSSGS\ssgs.w7.generate.redirect.bat" 
                
    log-and-show-me "Finished Generating CheckSum" $logfile 
}

