######################################################################
#
# Abstract: Init_Viewer_ShortCuts.ps1 configures 
#
# Authors:

#
# Frederick Shaw   
# KTTS   
# 5200 Philadelphia Way
# Lanham MD 20706
# fshaw@integ.com
#
#  
# Creation Date:  12/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by KTTS 
# Under  U.S. Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights Â© 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
.SYNOPSIS  
    This Windows powershell script is intended to support installation and 
    configuration of Windows based SSGS subsystems.
.DESCRIPTION
    GTACSFinalConfig.ps1 powershell script is the final steps for configuring the image.
    
.EXAMPLE 
    PS > .\GTACSFinalConfig.ps1
#>

 param([string]$logfile)

# dot source common.ps1 to include common logging functions
. .\common.ps1

# log-and-show-me "Starting GTACS workstation Configuration" $logfile  
# initialize variables to directories
$SetUpFiles = (Resolve-Path . -ea 0).Path
$ThirdPartySoftware = (Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
$InstallScripts = (Resolve-Path "..\InstallScripts" -ea 0).Path
$AdminDir = (Resolve-Path "..\SetupFiles\admin" -ea 0).Path


function install-Init_File_Viewer
{
	log-and-show-me "Updating Start Menu ShortCuts For Events and Limit Viewer!" $logfile
	# located in C:\Setup\SetUpFiles\admin\NewStartmenu
	#
    # Get the all users Start Program directory path 	        
	$WshShell = New-Object -comObject WScript.Shell
	$Path = $WshShell.SpecialFolders.item("AllUsersPrograms")
    $domain=$env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain
    
    if ($domain.ToLower().Contains("OPS".ToLower())) {
        #log-and-show-me "Updating for OPS!" $logfile 
		# if Events Viewer directory doesn't exist create it 
        if (!(test-path("$Path\SSGS\Events Viewer - FILTERED"))) 
        {
		     log-and-show-me "Creating $Path\Events Viewer - FILTERED" $logfile 
		     new-item -path "$Path\SSGS\" -name "\Events Viewer - FILTERED" -type directory
             $strEventsViewer = (Resolve-Path "$AdminDir\Viewer_init_links\OPS\Events Viewer - FILTERED" -ea 0).Path
			 # $StartMenu = Get-ChildItem $strEventsViewer\ -Force -recurse 
             # Write-Host ($StartMenu)
             log-and-show-me "Updating Viewer_init_links\OPS\Events Viewer - FILTERED ShortCuts!" $logfile
             # foreach ($file in $StartMenu)
             #       {
                      log-and-show-me "Updating ShortCuts!" $logfile
                      # Write-Host ($file.FullName)
                      # copy-item "$strEventsViewer\$file" -destination "$Path\Events Viewer - FILTERED\\$file\." 
                      copy-item "$strEventsViewer\*" -destination "$Path\SSGS\Events Viewer - FILTERED" -recurse
			 #		}
		} else {
            log-and-show-me "$Path\SSGS\Events Viewer - FILTERED exists!" $logfile 
        }
       
        # if Limit Alarm Viewer directory doesn't exist create it
        # C:\Setup\SetUpFiles\admin\Viewer_init_links\DEV\Limit Alarm Viewer - FILTERED 
        if (!(test-path("$Path\SSGS\Limit Alarm Viewer - FILTERED"))) 
        {
             log-and-show-me "Creating $Path\Limit Alarm Viewer - FILTERED " $logfile 
		     #
             new-item -path "$Path\SSGS\" -name "\Limit Alarm Viewer - FILTERED" -type directory
	         # 
             $strLimitAlarm = (Resolve-Path "$AdminDir\Viewer_init_links\OPS\Limit Alarm Viewer - FILTERED" -ea 0).Path
             # $StartMenu = Get-ChildItem $strLimitAlarm\ -Force -Recurse
             # Write-Host ($StartMenu)
             log-and-show-me "Updating Viewer_init_links\OPS ShortCuts!" $logfile
             #   
             # foreach ($file in $StartMenu)
             #       {
                      log-and-show-me "Updating ShortCuts!" $logfile
                      # Write-Host ($file.FullName)
             #         copy-item "$strLimitAlarm\$file" -destination "$Path\Limit Alarm Viewer - FILTERED\$file\."
                      copy-item "$strLimitAlarm\*" -destination "$Path\SSGS\Limit Alarm Viewer - FILTERED" -recurse
			 #		}
        } else {
            log-and-show-me "$Path\Limit Alarm Viewer - FILTERED exists!" $logfile 
        }
   } elseif ($domain.ToLower().Contains("DEV".ToLower())) {
        #log-and-show-me "Updating for DEV!" $logfile 
        # if Events Viewer directory doesn't exist create it 
        if (!(test-path("$Path\SSGS\Events Viewer - FILTERED"))) 
        {
             log-and-show-me "Creating $Path\SSGS\Events Viewer - FILTERED" $logfile 
		     new-item -path "$Path\SSGS\" -name "\Events Viewer - FILTERED" -type directory
	         $strEventsViewer = (Resolve-Path "$AdminDir\Viewer_init_links\DEV\Events Viewer - FILTERED" -ea 0).Path
             # $StartMenu = Get-ChildItem $strEventsViewer\ -Force -Recurse
             # Write-Host ($StartMenu)
             log-and-show-me "Updating Viewer_init_links\DEV\Events Viewer - FILTERED ShortCuts!" $logfile
             #  
             # foreach ($file in $StartMenu)
             #       {
                      log-and-show-me "Updating ShortCuts!" $logfile
                      # Write-Host ($file.FullName)
                      # copy-item "$strEventsViewer\$file" -destination "$Path\SSGS\Events Viewer - FILTERED\$file\."
                      copy-item "$strEventsViewer\*" -destination "$Path\SSGS\Events Viewer - FILTERED" -recurse
			 #		}
        } else {
            log-and-show-me "$Path\SSGS\Events Viewer - FILTERED exists!" $logfile 
        }
        # if Limit Alarm Viewer directory doesn't exist create it
        # C:\Setup\SetUpFiles\admin\Viewer_init_links\DEV\Limit Alarm Viewer - FILTERED 
        if (!(test-path("$Path\SSGS\Limit Alarm Viewer - FILTERED"))) 
        {
             log-and-show-me "Creating $Path\SSGS\Limit Alarm Viewer - FILTERED" $logfile 
		 
             new-item -path "$Path\SSGS\" -name "\Limit Alarm Viewer - FILTERED" -type directory
	
             $strLimitAlarm = (Resolve-Path "$AdminDir\Viewer_init_links\DEV\Limit Alarm Viewer - FILTERED" -ea 0).Path
             # $StartMenu = Get-ChildItem $strLimitAlarm\ -Force -recurse 
             # Write-Host ($StartMenu)
             log-and-show-me "Updating Viewer_init_links\DEV ShortCuts!" $logfile
                
             #foreach ($file in $StartMenu)
             #       {
                      log-and-show-me "Updating ShortCuts!" $logfile
                      # Write-Host ($file.FullName)
                      # copy-item "$strLimitAlarm\$file" -destination "$Path\SSGS\Limit Alarm Viewer - FILTERED\$file\." 
                      copy-item "$strLimitAlarm\*" -destination "$Path\SSGS\Limit Alarm Viewer - FILTERED" -recurse
			 #		}
        } else {
            log-and-show-me "$Path\SSGS\Limit Alarm Viewer - FILTEREDD exists!" $logfile 
        }
   #     
   } elseif ($domain.ToLower().Contains("SGROUP".ToLower())) {
        # sgroup
        log-and-show-me "Updating for SGROUP!" $logfile 
        # if Events Viewer directory doesn't exist create it 
        if (!(test-path("$Path\SSGS\Events Viewer - FILTERED"))) 
        {
             # log-and-show-me "Creating $Path\SSGS\Events Viewer - FILTERED" $logfile 
		     new-item -path "$Path\SSGS\" -name "\Events Viewer - FILTERED" -type directory
             $strEventsViewer = (Resolve-Path "$AdminDir\Viewer_init_links\DEV\Events Viewer - FILTERED" -ea 0).Path
             # $StartMenu = Get-ChildItem $strEventsViewer\ -Force -name 
             # Write-Host ($StartMenu)
             log-and-show-me "Updating Viewer_init_links\DEV\Events Viewer - FILTERED ShortCuts!" $logfile
             # foreach ($file in $StartMenu)
             #       {
                      log-and-show-me "Updating ShortCuts!" $logfile
                      # new-item -path "$Path\SSGS\Events Viewer - FILTERED\" -name $file -type directory
                      Write-Host ("$Path\SSGS\Events Viewer - FILTERED\")
                      Write-Host ("$strEventsViewer\$file")
                      # $LinkMenu = Get-ChildItem "$strEventsViewer\$file" -Force -Name
                      # Write-Host ($StartMenu)
                      # copy-item "$strEventsViewer\$file" -destination "$Path\SSGS\Events Viewer - FILTERED\\$file\." 
                      copy-item "$strEventsViewer\*" -destination "$Path\SSGS\Events Viewer - FILTERED"  -recurse
            #        }
        } else {
            log-and-show-me "$Path\SSGS\Events Viewer - FILTERED exists!" $logfile 
        }
   } else {
        log-and-show-me "Domain not OPS or DEV!" $logfile 
   }
   #  
}


function delete-Init_File_Viewer
{
	log-and-show-me $seperator $logfile
	log-and-show-me "Removing Start Menu ShortCuts For Events and Limit Viewer!" $logfile
	# located in C:\Setup\SetUpFiles\admin\NewStartmenu
	#
    # Get the all users Start Program directory path 	        
	$WshShell = New-Object -comObject WScript.Shell
	$Path = $WshShell.SpecialFolders.item("AllUsersPrograms")
    
     
	# if Events Viewer directory exist delete it 
    if ((test-path("$Path\SSGS\Events Viewer - FILTERED"))) {
    	log-and-show-me "Deleting Events Viewer files!" $logfile
        remove-item -Force "$Path\SSGS\Events Viewer - FILTERED\"  -recurse
 	}
	
	# if Limit Alarm Viewer directory exists delete it
    # C:\Setup\SetUpFiles\admin\Viewer_init_links\DEV\Limit Alarm Viewer - FILTERED 
    if ((test-path("$Path\SSGS\Limit Alarm Viewer - FILTERED"))) {
       log-and-show-me "Deleteing Viewer_init_links\OPS ShortCuts!" $logfile
       remove-item -Force "$Path\SSGS\Limit Alarm Viewer - FILTERED\" -recurse
	}
}





