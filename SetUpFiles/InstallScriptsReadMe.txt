Here are the steps to run the installation scripts on Windows 7 VM

1.	Open Console to the VM running Windows 7. 
These scripts would only run through the Console port, because ActiveClient had problems running through Remote Desktop.
2.	Unzip the InstallScripts.zip file in C:
3.	Cd to C:\InstallScripts
4.	Set-ExecutionPolicy �ExecutionPolicy bypass
And enter �Y� to say you trust the scripts you are about to run.
5.	.\setup.ps1 opr gtacs socc-dev
Run this command to install operational systems.
6.	.\setup.ps1 dev gtacs socc-dev
Run this command to install development systems. These are all the packages that require licenses (like MS Visual Studio, InstallShield etc).
7.	Install BCGControllBar manually following directions in BCGControlBar_README.txt
8.	Reboot
