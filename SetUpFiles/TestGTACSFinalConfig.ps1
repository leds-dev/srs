######################################################################
#
# Abstract: Windows Powershell installation script for SSGS subsystems
#
# Author:
# 
#  
# Creation Date:  /2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by  
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

$InstallScripts=(Resolve-Path . -ea 0).Path
$ThirdPartySoftware=(Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
$StagingArea=(Resolve-Path "..\InstallScripts" -ea 0).Path

. .\GTACSFinalConfig.ps1
. .\Init_Viewer_ShortCuts.ps1
. .\install-SNTPServ 
. .\install-NuTCRACKER

. .\common.ps1

#setup the logging directory 
$logDir = [System.Environment]::GetFolderPath("CommonApplicationData") 
$logDir = Join-Path -Path $logDir -Childpath "Gtacs"

if(-not(Test-Path -path $logdir) )
{New-Item -Path $logdir -ItemType directory | Out-Null }  

#keep track of starting time
$startTime = (Get-Date).tostring()
$logfiletimestamp=Get-Date -format yyyyMMdd-HH-mm-ss 

#initialize the logfile to null
$logfile=$null

$logfile= Join-Path -Path $logDir -childpath "gtacs-build-install-$logfiletimestamp.log"

#define the hostname 
$MYHOSTNAME=hostname

#define the ostype 
$OSTYPE = (Get-WmiObject Win32_OperatingSystem).Caption

$Myscript = resolve-path $MyInvocation.InvocationName

$msg = "Running script: $Myscript  starting $startTime as $env:username on $MYHOSTNAME."

$seperator = "##############################################################################"

$wshShell = New-Object -ComObject "WScript.Shell"
$DESKTOP_DIR = $wshShell.SpecialFolders.Item("AllusersDesktop")

#####################################################################################################
#
# main entry point of the script
#
# 

          # Last step need to configure image.
		  # log-and-show-me $seperator $logfile
		  # GTACSFinalConfig $logfile
          # install-Registrykeys
          # install-DeskTopShortcuts
          # install-StartMenuShortCuts $logfile
          # install-Init_File_Viewer $logfile
          # install-SNTPServ 
          install-NuTCRACKER "$ThirdPartySoftware\EPOCH_Client_1.21\NuTCRACKER\nutc.v9.4.patch1.exe"  `
                          "$logdir" `
                          "$logfile"  `
                          'MKS Platform Components 9.x' `
                          "$tmpDir"
