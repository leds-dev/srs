######################################################################
#
# Abstract: Windows Powershell installation script for SSGS subsystems
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
.SYNOPSIS  
    This Windows powershell script is intended to get versions of the
    installed pkgs. The pkgs to check are read from input file.
.DESCRIPTION
    get_versions.ps1 powershell script reads the input files and returns
    installed pkg DisplayName and DisplayVersion. The pkgs which are missing
    or have version mismatch are displayed in magenta color.
.PARAMETER filename 
    Required parameter, this file contains attribute, value pair of 
    installed pkgs to be checked. For example
    putty=0.62
    xming=6.9.0.30
.EXAMPLE 
    PS > .\get_version.ps1 C:\Setup\versions.txt
#>


#Param(
#    [Parameter(
#        Mandatory=$true,
#        Position=0,
#        ValueFromPipeline=$true,
#        ValueFromPipelineByPropertyName=$true,
#        HelpMessage="Full path to filename containing attribute, value pair")]
#    [string]
#    $filename
#)


function Get-FTPFile {
    Param(
        $URI,
        $localfile,
        $username,
        $password
    )
    $credentials=New-Object System.Net.NetworkCredential($username,$password)
    $ftp=[System.Net.FtpWebRequest]::Create($URI)
    $ftp.Credentials=$credentials
    $ftp.UseBinary=1
    $ftp.KeepAlive=0
    $response=$ftp.GetResponse()
    $responseStream = $response.GetResponseStream()
    #$responseStream
    $file = New-Object IO.FileStream($localfile,[IO.FileMode]::Create)
    [byte[]]$buffer = New-Object byte[] 1024
    $read = 0
    do{
        $read=$responseStream.Read($buffer,0,1024)
        $file.Write($buffer,0,$read)
    }while ($read -ne 0)$file.close()
}

function build_sw
{
    param([string]$logfile)

    write-host "start building ..."
    if ( Test-path -path ".\admin\tps\razor\razor_conf.socc-dev" ) {
        write-host "missing razor_conf file. Unable to build application!"
        return 1
    }

    $razor_info_file="C:\Setup\SetUpFiles\admin\tps\razor\razor-conf.socc-dev"
    
    $cm_user=select-string -pattern "CM_USER" $razor_info_file | foreach {$_.ToString().split("=")[1]}
    $cm_passwd=select-string -pattern "CM_PASSWD" $razor_info_file | foreach {$_.ToString().split("=")[1]}
    $cm_host_ip_addr=select-string -pattern "CM_HOST_IP_ADDRESS" $razor_info_file | foreach {$_.ToString().split("=")[1]}
    $razor_db=select-string -pattern "RAZOR_DB" $razor_info_file | foreach {$_.ToString().split("=")[1]}
    $razor_group=select-string -pattern "RAZOR_GROUP" $razor_info_file | foreach {$_.ToString().split("=")[1]}
    $razor_thread=select-string -pattern "RAZOR_THREAD" $razor_info_file | foreach {$_.ToString().split("=")[1]}

    write-host "$cm_user,$cm_passwd,$cm_host_ip_addr,$razor_db,$razor_group,$razor_thread"
    #use this temporary directory to the file 
    $tmp_dir="/tmp/razor"
    $build_cmds="source /home/razoradm/razor_db/$razor_db/rz_prep.sh;"
    $build_cmds="$build_cmds mkdir $tmp_dir;cd $tmp_dir;"
    #$build_cmds="$build_cmds razor -c thread -f $razor_thread -g $razor_group -d $tmp_dir;"
    #$build_cmds="$build_cmds sh rz_build_$razor_thread*;"     
    #$build_cmds="$build_cmds tar cvf src.tar -C ~ SRC;"  

    write-host "$build_cmds"
    Import-Module $ThirdPartySoftware\SSH\SSH-Sessions

    Write-Host "Extracting source files from CM server. This could take a while, be patient ..."
    New-SshSession -ComputerName $cm_host_ip_addr -Username $cm_user -Password $cm_passwd
    $SshResults=Invoke-SshCommand -InvokeOnAll -Command '$build_cmds' -Quiet
    Remove-SshSession -RemoveAll
    
    # Create directory if does not exist
    if (!([IO.Directory]::Exists("C:\NOAA_Code\Razor_Holding")))
    {
        New-Item C:\NOAA_Code\Razor_Holding\ -type directory | out-null
    }
    
    $localfile = "C:\NOAA_Code\Razor_Holding\SRC.zip"
    $URI = "ftp://$cm_host_ip_addr/SRC.zip"
    Get-FTPFile $URI $localfile $cm_user $cm_passwd

    Write-Host "done with extracting source files from CM server!"
    cd C:\NOAA_Code\Razor_Holding

    $shell_app=new-object -com shell.application
    $filename = "SRC.zip"
    $zip_file = $shell_app.namespace((Get-Location).Path + "\$filename")
    $destination = $shell_app.namespace((Get-Location).Path)
    $destination.Copyhere($zip_file.items())
    
    rm C:\NOAA_Code\Razor_Holding\SRC.zip
    cd $SetUpFiles
    
    Import-Module $ThirdPartySoftware\MsBuild\Invoke-MsBuild.psm1
    $buildSucceeded = Invoke-MsBuild -Path "C:\NOAA_Code\Razor_Holding\SRC\Checksum\check_files.sln" -PassThru
    
    if ($buildSucceeded) {
        write-host "Check_files: Build Completed Successfully"    }
    else {
        write-host "Build failed. Check the build log got errors"    }
    
#    #build and install JSEQ 
#    if [ -d njgs/${subsys} ] 
#    then  
#        show_and_log_this $1 "building JSEQ application!"
#        cd $cwd
#        chown -R root:root njgs/${subsys}
#        cd njgs/${subsys}
#        find . -exec touch {} \; 
#        make rpm >> $logfile 2>&1 
#        if [ $? -eq 0 ] 
#        then
#           show_and_log_this $1 "make rpm successful, rpm package is  available to be installed!"
#        else
#           #oops, this  
#           show_and_log_this $1 "make rpm failed, no rpm package is not available!"
#           retval=1
#           return $retval
#        fi
#    fi

    write-host "done with building and installing jseq applications!"
    return 0
}


#Running this script require admin privilage
#$currentUser = [Security.Principal.WindowsIdentity]::GetCurrent()
#$admin=(New-Object Security.Principal.WindowsPrincipal $currentUser).IsInRole([Security.Principal.windowsBuiltinrole]::Administrator)      
#if ( -not $admin) 
#{
#    "Admin rights are required for this script"
#     exit 0
#}

$InstallScripts=(Resolve-Path . -ea 0).Path
$SetUpFiles=(Resolve-Path . -ea 0).Path
$ThirdPartySoftware=(Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
$StagingArea=(Resolve-Path "..\InstallScripts" -ea 0).Path

#source the logging script to include functions
#. .\common.ps1

#setup the logging directory 
$logDir = [System.Environment]::GetFolderPath("CommonApplicationData") 
$logDir = Join-Path -Path $logDir -Childpath "Gtacs"

if(-not(Test-Path -path $logdir) )
{New-Item -Path $logdir -ItemType directory | Out-Null }  

#initialize the logfile to null
$logfile=$null

#define the hostname 
$MYHOSTNAME=hostname


#define the ostype 
$OSTYPE=(Get-WmiObject Win32_OperatingSystem).Caption

#keep track of starting time
$startTime = (Get-Date).tostring()
$logfiletimestamp=Get-Date -format yyyyMMdd-HH-mm-ss 

$Myscript=resolve-path $MyInvocation.InvocationName

$msg = @"
Running script: $Myscript  starting $startTime as $env:username on $MYHOSTNAME.
"@

$seperator = @"
##############################################################################
"@

$wshShell=New-Object -ComObject "WScript.Shell"
$DESKTOP_DIR = $wshShell.SpecialFolders.Item("AllusersDesktop")

#####################################################################################################
#
# main entry point of the script
#
# 

#$logfile= Join-Path -Path $logDir -childpath "get_versions-$logfiletimestamp.log"
#$logfile= Join-Path -Path "$InstallScripts" -childpath "gtacs-build-install.log"
            
#log-and-show-me $msg $logfile 
#log-and-show-me "Check $logfile if needed"  $logfile

build_sw "C:\Setup\build_sw.log"

##now we should be done
#$endTime = (Get-Date).tostring()

#$msg = @"
#Finished script $endTime. Total script time was $((New-TimeSpan -Start $startTime -End $endtime).totalSeconds) seconds. 
#"@

#log-and-show-me $msg $logfile 
#log-and-show-me "Check $logfile if needed"  $logfile

