######################################################################
#
# Abstract: Collection of common utility functions. 
#
# Author:
# Ye Men   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Ye.Men@avayagov.com
#  
# Creation Date: 10/31/2011
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################
function log-me
{
    param([string]$msg,
            [string]$logfile)


        $timestamp=Get-Date -format yyyy.MM.dd-HH:mm:ss 
        $msg="$timestamp" + ": " + "$msg" 
        $msg >> $logfile
}

function log-and-show-me
{
    param([string]$msg,
            [string]$logfile)

        $timestamp=Get-Date -format yyyy.MM.dd-HH:mm:ss 
        $msg="$timestamp" + ": " + "$msg" 

        Write-Output $msg | tee-object -Variable log  
        Write-Output $log | Out-File $logfile -Append
}


function get-installed 
{
    param([string]$app)

    $keys=get-childitem HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall

    $items = $keys | foreach-object { Get-ItemProperty $_.PsPath } 

    foreach ($item in $items) 
    {
        if (($item.DisplayName) -and ($item.DisplayName -like "$app"))
        {
            return $true
        }
    }
    return $false
}

function get-64bit-installed 
{

    param([string]$app)

        $keys=get-childitem HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall

        $items = $keys | foreach-object { Get-ItemProperty $_.PsPath } 

        foreach ($item in $items) 
        {
            if (($item.DIsplayName) -and ($item.DisplayName -like "$app"))
            {
                return $true
            }
        }
        return $false
}
