######################################################################
#
# Abstract: Windows Powershell installation script for SSGS subsystems
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
.SYNOPSIS  
    This Windows powershell script is intended to get versions of the
    installed pkgs. The pkgs to check are read from input file.
.DESCRIPTION
    get_versions.ps1 powershell script reads the input files and returns
    installed pkg DisplayName and DisplayVersion. The pkgs which are missing
    or have version mismatch are displayed in magenta color.
.PARAMETER filename 
    Required parameter, this file contains attribute, value pair of 
    installed pkgs to be checked. For example
    putty=0.62
    xming=6.9.0.30
.EXAMPLE 
    PS > .\get_version.ps1 C:\Setup\versions.txt
#>


Param(
    [Parameter(
        Position=0,
        ValueFromPipeline=$true,
        ValueFromPipelineByPropertyName=$true,
        HelpMessage="Full path to filename containing attribute, value pair")]
    [string]$filename
)

function get-installed 
{
    param([string]$app)

    $app="$app*"
    # check 32bit installs
    $keys=get-childitem HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall

    $items = $keys | foreach-object { Get-ItemProperty $_.PsPath } 

    foreach ($item in $items) 
    {
        if (($item.DisplayName) -and ($item.DisplayName -like $app))
        {
            return $item.DisplayName
        }
    }
    # check 64bit installs
    $keys=get-childitem HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall

    $items = $keys | foreach-object { Get-ItemProperty $_.PsPath } 

    foreach ($item in $items) 
    {
        if (($item.DisplayName) -and ($item.DisplayName -like $app))
        {
            return $item.DisplayName
        }
    }    
    return
}


function get-installed-version
{
    param([string]$app)

    $app="$app*"
    # check 32bit installs
    $keys=get-childitem HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall

    $items = $keys | foreach-object { Get-ItemProperty $_.PsPath } 

    foreach ($item in $items) 
    {
        if (($item.DisplayName) -and ($item.DisplayName -like "$app"))
        {
                if ($item.DisplayVersion)
                {
                    return $item.DisplayVersion
                }
        }
    }
    #check 64bit installs
    $keys=get-childitem HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall

    $items = $keys | foreach-object { Get-ItemProperty $_.PsPath } 

    foreach ($item in $items) 
    {
        if (($item.DisplayName) -and ($item.DisplayName -like "$app"))
        {
            if ($item.DisplayVersion)
            {
                return $item.DisplayVersion
            }
        }
    }
    return " "
}

#Running this script require admin privilage
#$currentUser = [Security.Principal.WindowsIdentity]::GetCurrent()
#$admin=(New-Object Security.Principal.WindowsPrincipal $currentUser).IsInRole([Security.Principal.windowsBuiltinrole]::Administrator)      
#if ( -not $admin) 
#{
#    "Admin rights are required for this script"
#     exit 0
#}

$InstallScripts=(Resolve-Path . -ea 0).Path
$ThirdPartySoftware=(Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
$StagingArea=(Resolve-Path "..\InstallScripts" -ea 0).Path

#source the logging script to include functions
# . .\common.ps1

#setup the logging directory 
$logDir = [System.Environment]::GetFolderPath("CommonApplicationData") 
$logDir = Join-Path -Path $logDir -Childpath "Gtacs"

if(-not(Test-Path -path $logdir) )
{New-Item -Path $logdir -ItemType directory | Out-Null }  

#initialize the logfile to null
$logfile=$null

#define the hostname 
$MYHOSTNAME=hostname


#define the ostype 
$OSTYPE=(Get-WmiObject Win32_OperatingSystem).Caption

#keep track of starting time
$startTime = (Get-Date).tostring()
$logfiletimestamp=Get-Date -format yyyyMMdd-HH-mm-ss 

$Myscript=resolve-path $MyInvocation.InvocationName

$msg = @"
Running script: $Myscript  starting $startTime as $env:username on $MYHOSTNAME.
"@

$seperator = @"
##############################################################################
"@

$wshShell=New-Object -ComObject "WScript.Shell"
$DESKTOP_DIR = $wshShell.SpecialFolders.Item("AllusersDesktop")

#####################################################################################################
#
# main entry point of the script
#
# 

$logfile= Join-Path -Path $logDir -childpath "get_versions-$logfiletimestamp.log"
#$logfile= Join-Path -Path "$InstallScripts" -childpath "gtacs-build-install.log"
            
# log-and-show-me $msg $logfile 
#log-and-show-me "Check $logfile if needed"  $logfile

if (!$filename) {
    $filename = "$InstallScripts\versions.txt"
}

write-host ""
$reader = [System.IO.File]::OpenText($filename)
try {
    for(;;) {
        $line = $reader.ReadLine()
        if ($line -eq $null) { break }
        # process the line
        $sub_line=$line.Split('=')
        $check_pkg=$sub_line[0]
        if ( "$check_pkg" -eq "BGInfo" ) {
            if (!([System.IO.File]::Exists("C:\Program Files (x86)\BGInfo\BGInfo.exe"))) {
               $installed_pkg=""
            }
            else {
               $installed_pkg="$check_pkg"
            }
        }
        else {   
           $installed_pkg=get-installed "$check_pkg"
        }   
        if ( -not $installed_pkg ) {
            write-host �$check_pkg >>>> " -foregroundcolor "magenta"
            # log-me �$check_pkg >>>> " $logfile
            �$check_pkg >>>> " >> $logfile
        }
        else {
            if ( "$check_pkg" -eq "BGInfo" ) {
               $x=[System.Diagnostics.FileVersionInfo]::GetVersionInfo("C:\Program Files (x86)\BGInfo\BGInfo.exe").ProductVersion
            }
            else {
               $x=get-installed-version "$check_pkg"
            }   
            if ( $sub_line[1] -ne "$x" ) {
                write-host �$installed_pkg >>>> $x" -foregroundcolor "magenta"
                # log-me �$installed_pkg >>>> $x" $logfile
                �$installed_pkg >>>> $x" >> $logfile
            }
            else {
                write-host �$installed_pkg >>>> $x"
                # log-me �$installed_pkg >>>> $x" $logfile
                �$installed_pkg >>>> $x"  >> $logfile
            }
        }
    }
}
finally {
    $reader.Close()
}
write-host ""

#now we should be done
$endTime = (Get-Date).tostring()

$msg = @"
Finished script $endTime. Total script time was $((New-TimeSpan -Start $startTime -End $endtime).totalSeconds) seconds. 
"@

#log-and-show-me $msg $logfile
write-host "$msg >>>> $logfile"
$msg >> $logfile
 
#log-and-show-me "Check $logfile if needed"  $logfile

