######################################################################
#
# Abstract: Windows Powershell installation script for SSGS subsystems
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################
<#
.SYNOPSIS 
This Windows powershell script is intended to support installation and 
configuration of Windows based SSGS subsystems.

.DESCRIPTION
setup.ps1 powershell script provides the automated installation of SSGS 
Windows based subsystems.  The script provides unattended installation 
of required GTACS application software for intended subsystem,  
then installs subsystem silently, and finally configures necessary 
settings for SSGS operational environment.  The script provides command 
line switch for users to alter installation behavior if desired. 

.PARAMETER install 
Required parameter, indicating type of installation to be performed, 
valid SSGS installation types are either: dev, or opr.

.PARAMETER subsystem
Required parameter, indicating applicable SSGS subystems,
valid SSGS windows subsystems are gtacs.

.PARAMETER site 
Required parameter, indicating networking, development, or deployment
environment in which this installation is intended, 
valid SSGS site names are: socc-ops, socc-dev 

.Example 
Install only components required to build gtacs subsystem at socc-dev 
network site.
PS > .\gtacs_setup.ps1 -install dev -subsystem gtacs -site socc-dev  
#>

Param(
    [Parameter(
        Mandatory=$true,
        Position=0,
        ValueFromPipeline=$true,
        ValueFromPipelineByPropertyName=$true,
        HelpMessage="Enter type of installation to be performed, valid strings are: dev, opr")]
    [validateset("dev", "opr")] 
    [string]
    $install,

    [Parameter(
        Mandatory=$true,
        Position=1,
        ValueFromPipeline=$true,
        ValueFromPipelineByPropertyName=$true,
        HelpMessage="Enter type of subsystem that this installation is intended for, valid strings are: gtacs")]
    [validateset("gtacs")] 
    [string]
    $subsystem,

    [Parameter(
            Mandatory=$true,
            Position=2,
            ValueFromPipeline=$true,
            ValueFromPipelineByPropertyName=$true,
            HelpMessage="Enter site name where this installation is to be done, valid string are: socc-opr, socc-dev")]
    [validateset("socc-opr", "socc-dev")] 
    [string]
    $site,

   

    #boolean flag to control if all components of a subsystem are installed
    [switch]$all,       

    #boolean flag to control if the machine will be rebooted after the 
    #install script finishes
    [switch]$reboot,

    #boolean flag to control if install will include necessary security 
    #application like McAfee anti vius, and etc
    [switch]$nosecurityapps

)



#Running this script require admin privilage
$currentUser = [Security.Principal.WindowsIdentity]::GetCurrent()
$admin=(New-Object Security.Principal.WindowsPrincipal $currentUser).IsInRole([Security.Principal.windowsBuiltinrole]::Administrator)      
if ( -not $admin) 
{
    "Admin rights are required for this script"
     exit 0
}

## Running this script is NOT allowed through Remote Desktop Connection
#$hostip = [System.Net.Dns]::GetHostAddresses("$MYHOSTNAME")[1].IPAddressToString
#$c = netstat -aonp TCP | select-string "ESTABLISHED" | select-string "${hostip}:3389"
#if ($c)
#{
#    "Connected through RDP, install scripts should be run through Console Port"
#    exit 0
#}

$InstallScripts=(Resolve-Path . -ea 0).Path
$ThirdPartySoftware=(Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
$StagingArea=(Resolve-Path "..\InstallScripts" -ea 0).Path

#source the logging script to include functions
. .\common.ps1
. .\install-java.ps1
. .\install-Application.ps1

#setup the logging directory 
$logDir = [System.Environment]::GetFolderPath("LocalApplicationData") 
$logDir = Join-Path -Path $logDir -Childpath "Gtacs"

if(-not(Test-Path -path $logdir) )
{New-Item -Path $logdir -ItemType directory | Out-Null }  

#initialize the logfile to null
$logfile=$null

#define the hostname 
$MYHOSTNAME=hostname


#define the ostype 
$OSTYPE=(Get-WmiObject Win32_OperatingSystem).Caption

#keep track of starting time
$startTime = (Get-Date).tostring()
$logfiletimestamp=Get-Date -format yyyyMMdd-HH-mm-ss 

$Myscript=resolve-path $MyInvocation.InvocationName

$msg = @"
Running script: $Myscript  starting $startTime as $env:username on $MYHOSTNAME.
"@

$seperator = @"
##############################################################################
"@

$wshShell=New-Object -ComObject "WScript.Shell"
$DESKTOP_DIR = $wshShell.SpecialFolders.Item("AllusersDesktop")

#####################################################################################################
#
# main entry point of the script
#
#
switch ($subsystem) 
{
                  
    "gtacs" {

        if (( $install -eq "dev" ) -or ( $install -eq "opr" )) 
        {
            $logfile= Join-Path -Path $logDir -childpath "gtacs-build-install-$logfiletimestamp.log"
#            $logfile= Join-Path -Path "$InstallScripts" -childpath "gtacs-build-install.log"
            
            log-and-show-me $msg $logfile 
            log-and-show-me "Check $logfile if needed"  $logfile

           install-Application 'msiexec' `
        				  "$logfile" `
				          'Checksum.msi' `
				          'Checksum' `
			           	  "$StagingArea\Checksum" `
				          'epoch'

           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'DirMapper.msi' `
				          'DirMapper' `
				          "$StagingArea\DirMapper" `
				          'epoch'

           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'DumpTool.msi' `
				          'DumpTool.ise' `
				          "$StagingArea\DumpTool" `
				          'epoch'

           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'Get Client.msi' `
				          'Get Client' `
				          "$StagingArea\Get-Client" `
				          'epoch'

           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'ImageEditor.msi' `
				          'Image Editor' `
				          "$StagingArea\ImageEditor" `
				          'epoch'

           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'USC.msi' `
				          'USC' `
				          "$StagingArea\USC" `
				          'epoch'

           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'SSGS_Util.msi' `
				          'SSGS_Util' `
				          "$StagingArea\Archive_Converter" `
				          'epoch'

           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'SchedMgr.msi' `
				          'SchedMgr' `
				          "$StagingArea\SchedMgr" `
				          'epoch'             

           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'ExHelp.msi' `
				          'ExHelp' `
				          "$StagingArea\EpochExtendedHelp" `
				          'epoch'             
         }
    }
    Default {
        Write-Error "Invalid subsystems encountered"
    }
}

#now we should be done
$endTime = (Get-Date).tostring()

$msg = @"
Finished script $endTime. Total script time was $((New-TimeSpan -Start $startTime -End $endtime).totalSeconds) seconds. 
"@

log-and-show-me $msg $logfile 
log-and-show-me "Check $logfile if needed"  $logfile

if ( $reboot ) 
{
    log-and-show-me "Rebooting this host in 15 seconds ... " $logfile
    Sleep 15
    Restart-Computer -force
}
