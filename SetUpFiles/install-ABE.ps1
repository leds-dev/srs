######################################################################
#
# Abstract: Install ABE silently using prepared ISS file. The iss 
# file is prepared by running installer with "-r" option.
# of the installer.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
# .SYNOPSIS 
# Install ABE silently using prepared ISS file. The iss 
# file is prepared by running installer with "-r" option.
# of the installer.
#
# 
# .DESCRIPTION
# install-ABE provides unattended installation of Epoch client.
#
#>

#dot source logging.ps1 to include common logging functions
. .\common.ps1


function install-ABE
{
    param([string]$installer_exe,
          [string]$logdir,
          [string]$logfile,
          [string]$app_name,
    	  [string]$install_dir,
	      [string]$library_path)


    log-and-show-me "Starting installation of ABE ..." $logfile  

    # Create directory if does not exist
    if (!([IO.Directory]::Exists("C:\abe")))
    {
        log-and-show-me "response file $installer_exe" $logfile  

        Invoke-expression "$installer_exe /auto $install_dir | out-null"
        Invoke-expression "$library_path | out-null"
	
        if ( $lastexitcode )  
        {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
            throw "Running $installer_exe failed!!. See log $logfile for more detail"  
        }
        #log-and-show-me " " $logfile
        #log-and-show-me "Follow the instructions to manually install PV-WAVE " $logfile
        #log-and-show-me " " $logfile
        
    }
    else
    {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }

    log-and-show-me "Finished with installing ABE!" $logfile 
}
