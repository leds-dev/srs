######################################################################
#
# Abstract: Install ActiveClient silently using all default options
# of the installer.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights � 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################
<#
#
# .SYNOPSIS 
# Install ActiveClient and related tool set silently using all default 
# options of the installer.
#
# 
# .DESCRIPTION
# install-ActiveClient provides unattended installation of ActiveClient 
# tool set.
#
# .Example 
# PS > .\install-ActiveClient.ps1 $installer_exe $logfile $app_name
#
#
#>

#dot source common.ps1 to include common logging functions
. .\common.ps1

function install-ActiveClient
{
    param([string]$installer_exe,
          [string]$logfile,
          [string]$app_name,
          [string]$app_display_name,
          [string]$install_dir,
          [string]$username)

    log-and-show-me "Starting installation of $app_name ..." $logfile  

    $installed=get-64bit-installed "$app_display_name"
    if ( -not $installed )
    {
        Invoke-expression "$installer_exe /l*+ $logfile /i `"$install_dir\$app_name`" /qn /norestart USERNAME=$username | out-null"  

        # throw error if lastexitcode not ERROR_SUCCESS or ERROR_SUCCESS_REBOOT_REQUIRED
        if (( $LASTEXITCODE -ne 0 ) -and ( $LASTEXITCODE -ne 3010 )) 
        {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
            throw "Running $installer_exe failed!!. See log $logfile for more detail"  
        }
        elseif ( $LASTEXITCODE -eq 3010 )
        {
            log-me "$app_name installation exited with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
        }
    }
    else 
    {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }
    log-and-show-me "Finished with installing of $app_name!" $logfile 
}
