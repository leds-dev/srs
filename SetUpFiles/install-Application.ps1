######################################################################
#
# Abstract: Install GTACS application silently using all default options
# of the installer.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################
<#
#
# .SYNOPSIS 
# Install GTACS application silently using all default options
# of the installer.
#
# 
# .DESCRIPTION
# install-Application provides unattended installation of GTACS application.
#
# .Example 
# PS > .\install-Application.ps1 $installer_exe $logfile $app_name $username
#
#
#>

#dot source common.ps1 to include common logging functions
. .\common.ps1

function install-Application
{
    param([string]$installer_exe,
          [string]$logfile,
          [string]$app_name,
          [string]$app_display_name,
          [string]$install_dir,
          [string]$username)

		  
    log-and-show-me "Starting installation of $app_name ..." $logfile  
	
	$arguments=@(
	"/i"
	"`"$install_dir\$app_name`""
	"/qn"
	"USERNAME=$username"
	"/lv+* $logfile"
	)
	$process = Start-Process -FilePath msiexec -ArgumentList $arguments -Wait -PassThru 
	if ( $process.Exitcode -ne 0 ) { 
	    log-me "$app_name installation failed with $($process.Exitcode)" $logfile
	    Invoke-expression "net helpmsg $($process.Exitcode)" | tee-object -Variable log
	    Write-Output $log | Out-File $logfile -Append
        throw "Running $installer_exe failed!!. See log $logfile for more detail" 
	}
    log-and-show-me "Finished with installing of $app_name!" $logfile 
	
}
