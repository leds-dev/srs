######################################################################
#
# Abstract: Runs BCGControlBar in silent mode to perform unattended 
# installation.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################
<#
#
# .SYNOPSIS 
# Runs installshiled in silent mode to perform unattended 
# installation.
#
# 
# .DESCRIPTION
# install-BCGControlBar runs installer in silent
# mode to automate installation of BCGControlBar tools. 
#
#>

#dot source common.ps1 to include common logging functions
. .\common.ps1

function is_it_dead
{
    param([string]$proc)
    
    $numProcs=@(Get-Process -ea silentlycontinue "$proc").count
    if ($numProcs -eq 0) {
        start-sleep -s 5
        $numProcs=@(Get-Process -ea silentlycontinue "$proc").count
        if ($numProcs -eq 0) {
            $dead = 'yes'
        }
        else {
            $dead = 'no'
        }
    }
    else {
        $dead = 'no'
    }
    return $dead
}

function BCGWizard
{
    $numProcs=@(Get-Process -ea silentlycontinue "BCGCBProIntegrationWizard").count
            
    for ($i=1; $i -le 5; $i++) {
        while ($numProcs -eq 0) { 
            $numProcs=@(Get-Process -ea silentlycontinue "BCGCBProIntegrationWizard").count
            start-sleep -s 2 
        }
        #Write-Host "click Next $i"
        select-window -Title "BCGControlBar Pro Integration Wizard" | select-control -Title "&Next >" | send-click                          
        $numProcs=0
        start-sleep -s 2 
    }
            
    # long wait check for some process cl.exe to complete for more then a minute
    start-sleep -s 30
    $ret = is_it_dead 'cl'
    while ($ret -ne 'yes') {
        start-sleep -s 30
        $ret = is_it_dead 'cl'
        if ($ret -eq 'yes') {
            start-sleep -s 60
            $ret = is_it_dead 'cl'
        }
        #Write-Host "dead $ret"
    }
            
    # continue
    while ($numProcs -eq 0) { 
        $numProcs=@(Get-Process -ea silentlycontinue "BCGCBProIntegrationWizard").count
        start-sleep -s 2 
    }
    #Write-Host "click Next"
    select-window -Title "BCGControlBar Pro Integration Wizard" | select-control -Title "&Next >" | send-click                          
    $numProcs=0
    start-sleep -s 2 

    while ($numProcs -eq 0) { 
        $numProcs=@(Get-Process -ea silentlycontinue "BCGCBProIntegrationWizard").count
        start-sleep -s 2 
    }
    #Write-Host "click Finish"
    select-window -Title "BCGControlBar Pro Integration Wizard" | select-control -Title "Finish" | send-click                          
    start-sleep -s 2 
}


function install-BCGControlBar
{
    param([string]$installer_exe,
            [string]$issfile,
            [string]$logfile,
            [string]$app_name)

        log-and-show-me "Starting BCGControlBar setup ..." $logfile  
        
        $installed=get-installed $app_name
        if ( -not $installed )
        {
            log-and-show-me "Be patient, this could take a while ... " $logfile  
            $numProcs=@(Get-Process -ea silentlycontinue "BCGCBProIntegrationWizard").count
            Invoke-expression "$installer_exe /a /s /sms /f1$issfile | out-null"
            $lastext = $lastexitcode

            BCGWizard
                        
            $lastexitcode = $lastext
            if ( $lastexitcode )  
            {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
                throw "Running $installer_exe failed!!. See log $logfile for more detail"  
            }
        }
        else
        {
            log-me "Skipping installing $app_name because it is already installed" $logfile  
            Write-Warning "Skipping installing $app_name because it is already installed"  

        }
    log-and-show-me "Finished with setup of BCGControlBar!" $logfile 
}
