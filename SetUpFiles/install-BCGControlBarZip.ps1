########################################################################
#
# install-BCGControlBar
#
# Author: Ye Men Ye.Men@avayagov.com
# Creation Date:  10/31/2011
# Revision:
# $log: &
# 
# CopyRight:
#
########################################################################

#
# .SYNOPSIS 
# Runs installshiled in silent mode to perform unattended 
# installation.
#
# 
# .DESCRIPTION
# install-BCGControlBar runs installer in silent
# mode to automate installation of BCGControlBar tools. 
#

#dot source common.ps1 to include common logging functions
. .\common.ps1

function install-BCGControlBar
{
    param([string]$installed_dir,
            [string]$installer_exe,
            [string]$logfile,
            [string]$install_dir)

        log-and-show-me "Starting BCGControlBar setup ..." $logfile  
        
        $installed=test-path -path $installed_dir
        if ( -not $installed )
        {
#            Invoke-expression "$install_dir\$installer_exe /a /s /sms /f1`"$InstallScripts\BCGControlBar.iss`" | out-null"  
            $shell = new-object -com shell.application
            $zipfile = $shell.NameSpace($installer_exe)
            $destination = $shell.NameSpace($install_dir)
            $destination.CopyHere($zipfile.Items()) 

            if ( $lastexitcode )  
            {
                throw "Running $installer_exe failed!!. See log $logfile for more detail"  
            }
        }
        else
        {
            log-me "Skipping installing $app_name because it is already installed" $logfile  
            Write-Warning "Skipping installing $app_name because it is already installed"  

        }
    log-and-show-me "Finished with setup of BCGControlBar!" $logfile 
}
