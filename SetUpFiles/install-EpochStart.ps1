######################################################################
#
# Abstract: EpochStart start or stop the epoch services. 
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
.SYNOPSIS  
    This Windows powershell script is intended to support installation and 
    configuration of Windows based SSGS subsystems.
.DESCRIPTION
    install-EpochStart.ps1 powershell script provides the automted starting
    and stopping of EpDBSrvc, EpSMon, SSAPISrvc and EpLPSrvc services
.PARAMETER start 
    Required parameter, indicating type of installation to be performed, 
    valid SSGS installation types are either: dev, or opr.
.EXAMPLE 
    Install only components required to build at socc-dev network site.
    PS > .\install-EpochStart.ps1 start
    PS > .\install-EpochStart.ps1 stop
#>

Param(
    [Parameter(
        Mandatory=$true,
        Position=0,
        ValueFromPipeline=$true,
        ValueFromPipelineByPropertyName=$true,
        HelpMessage="Enter start/stop")]
    [validateset("start", "stop")] 
    [string]
    $start        
)

#dot source logging.ps1 to include common logging functions
. .\common.ps1

C:\EPOCHClient\bin\EpBackup.exe /restore .\admin\tps\EpochClient\epoch_sgtacs2_backup.reg

if ( $start -eq "stop" )
{
    net stop EpLPSrvc
    net stop SSAPISrvc
    net stop EpSMon
    net stop EpDBSrvc

}
else
{
    $domain=$env:USERDNSDOMAIN
    $password = Read-Host -Prompt "Enter password for user $domain\gtacsops" -AsSecureString
    $password = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto([System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($password))
        
    sc.exe config EpDBSrvc  obj= $domain\gtacsops password= $password
    sc.exe config SSAPISrvc obj= $domain\gtacsops password= $password
    sc.exe config EpLPSrvc  obj= $domain\gtacsops password= $password
    sc.exe config EpSMon    obj= $domain\gtacsops password= $password
            
    net start EpDBSrvc
    net start EpSMon
    net start SSAPISrvc
    net start EpLPSrvc
}

