﻿######################################################################
#
# Abstract: Install GTACS application ExHelp silently using all default options
# of the installer.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################
<#
#
# .SYNOPSIS 
# Install GTACS application ExHelp silently using all default options
# of the installer.
#
# 
# .DESCRIPTION
# install-ExHelp provides unattended installation of GTACS application.
#
# .Example 
# PS > .\install-ExHelp.ps1 $installer_exe $logfile $app_name $username
#
#
#>

#dot source common.ps1 to include common logging functions
. .\common.ps1

function install-ExHelp
{
    param([string]$installer_exe,
          [string]$logfile,
          [string]$app_name,
          [string]$app_display_name,
          [string]$install_dir,
          [string]$username)

    log-and-show-me "Starting installation of $app_name ..." $logfile  
	
	$arguments=@(
	"/i"
	"`"$install_dir\$app_name`""
	"/qn"
	"USERNAME=$username"
	"/lv+* $logfile"
	)
	$process = Start-Process -FilePath msiexec -ArgumentList $arguments -Wait -PassThru 
	# Need to wait for installation to complete.  Less than five seconds fails
    start-sleep -s 5
	
	if ( $process.Exitcode -ne 0 ) { 
	    log-me "$app_name installation failed with $($process.Exitcode)" $logfile
	    Invoke-expression "net helpmsg $($process.Exitcode)" | tee-object -Variable log
	    Write-Output $log | Out-File $logfile -Append
        throw "Running $installer_exe failed!!. See log $logfile for more detail" 
	}
	else { 
	    C:\Windows\SysWOW64\regsvr32 /s /u C:\EPOCHClient\bin\EpExtendedHelp.dll

	    if (!([System.IO.File]::Exists("C:\EPOCHClient\bin\EpExtendedHelp.dll.1210"))) {
         mv C:\EPOCHClient\bin\EpExtendedHelp.dll C:\EPOCHClient\bin\EpExtendedHelp.dll.1210
	    }
        copy 'C:\Program Files (x86)\Integral Systems\ExHelp\EpExtendedHelp.dll' C:\EPOCHClient\bin\EpExtendedHelp.dll
        C:\Windows\SysWOW64\regsvr32 /s C:\EPOCHClient\bin\EpExtendedHelp.dll   
	}
	
    log-and-show-me "Finished with installing of $app_name!" $logfile 
}
