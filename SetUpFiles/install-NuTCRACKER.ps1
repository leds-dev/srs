######################################################################
#
# Abstract: Install NuTCRACKER silently using prepared ISS file. The iss 
# file is prepared by running installer with "-r" option.
# of the installer.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
# .SYNOPSIS 
# Install NuTCRACKER silently using prepared ISS file. The iss 
# file is prepared by running installer with "-r" option.
# of the installer.
#
# 
# .DESCRIPTION
# install-NuTCRACKER provides unattended installation of Epoch client.
#
#>

#dot source logging.ps1 to include common logging functions
. .\common.ps1

function install-NuTCRACKER {
    param([string]$installer_exe,
          [string]$logdir,
          [string]$logfile,
          [string]$app_name,
          [string]$install_dir)

    log-and-show-me "Starting installation of NuTCRACKER ..." $logfile  

    $installed=get-64bit-installed $app_name
    if ( -not $installed ) {        
        $InitialNumProcs=@(Get-Process -ea silentlycontinue "msiexec").count
        Invoke-expression "$installer_exe /auto $install_dir | out-null"
        Invoke-expression "msiexec /I `"$install_dir\oe\Toolkit64x.msi`" /qn+ /l* $logfile"

        $process = Get-Process | Where-Object {$_.ProcessName -eq "msiexec"}
        $numProcs=@(Get-Process -ea silentlycontinue "msiexec").count      
        while ($numProcs -gt $initialNumProcs) {
            $numProcs=@(Get-Process -ea silentlycontinue "msiexec").count
            # (Windows Automation Snapin for Powershell) WASP command
            select-window -Title "MKS Platform Components 9.x" | select-control -Title OK | send-click                          
            start-sleep -s 2
        }

        if ( $lastexitcode ) {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
            throw "Running $installer_exe failed!!. See log $logfile for more details"  
        } 
          
    } else {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }
        
    log-and-show-me "Deleting environment variable: shell" $logfile
    [environment]::SetEnvironmentVariable("shell", "", "machine")
         
    log-and-show-me "Finished with installing NuTCRACKER!" $logfile 
}
