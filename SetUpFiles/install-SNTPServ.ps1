######################################################################
#
# Abstract: Install SNTPServ and related tool set silently using all 
# default options of the installer.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
# .SYNOPSIS 
# Install SNTPServ and related tool set silently using all default options
# of the installer.
#
# 
# .DESCRIPTION
# install-SNTPServ provides unattended installatino of SNTPServ tool set.
#
# .Example 
# PS > .\install-SNTPServ.ps1 $installer_exe $logfile $app_name \
#         $app_display_name $install_dir $username
#
#>

#dot source common.ps1 to include common logging functions
. .\common.ps1

function install-SNTPServ {
    param([string]$installer_exe,
          [string]$logfile,
          [string]$app_name,
          [string]$app_display_name,
          [string]$install_dir,
          [string]$username)

    log-and-show-me "Starting installation of SNTPServ ..." $logfile  

    $installed=get-64bit-installed "$app_display_name"
    if ( -not $installed ){
        # Invoke-expression "$installer_exe /l*+ $logfile /i `"$install_dir\$app_name`" /q USERNAME=$username | out-null"  
        # Invoke-expression "$installer_exe /l*+ $logfile /i `"$install_dir\$app_name`" /q INSTALLCTLSHORTCUTS=0  | out-null"
        Invoke-expression "$installer_exe /l*+ $logfile /i `"$install_dir\$app_name`" /q INSTALLCTLSHORTCUTS=0 INSTALLHELP=0 | out-null"

        if ( $lastexitcode ) {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
            throw "Running $installer_exe failed!!. See log $logfile for more detail"  
        }
    } else  {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }
        
    log-and-show-me "Finished with installing of SNTPServ!" $logfile 
}

function license-SNTPServ {

    $InstallScripts=(Resolve-Path . -ea 0).Path
    $ThirdPartySoftware=(Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
    $StagingArea=(Resolve-Path "..\InstallScripts" -ea 0).Path
    $SetUpFiles = (Resolve-Path . -ea 0).Path
    $domain=$env:USERDNSDOMAIN  #  Retrieve the domain name and store it in $domain
    
    log-and-show-me "Starting licensing of SNTPServ ..." $logfile  
    
    if ($domain -eq "FGOESNOPS.NCWS.OPS") {
        # Fairbanks
        log-and-show-me "Updating registry settings for FGOESNOPS!" $logfile 
        regedit /s $SetUpFiles\admin\RegistryFiles\SNTPsgoesn_FCDAS.reg
        
	} elseif (($domain -eq "NCWS.DEV") -or ($domain -eq "NCWS.OPS")){ 
        # NSOF
        log-and-show-me "Updating registry settings for NCWS!" $logfile 
        regedit /s $SetUpFiles\admin\RegistryFiles\SNTPsgoesn_NCWS.reg
    
    } elseif ($domain -eq "GGOESNOPS.NCWS.OPS"){
        # Wallops
        log-and-show-me "Updating registry settings for WGOESNDEV!" $logfile   
        regedit /s $SetUpFiles\admin\RegistryFiles\SNTPsgoesn_WCDA.reg
    
    } elseif (($domain -eq "WGOESNDEV.NCWS.DEV") -or ($domain -eq "WGOESNOPS.NCWS.OPS")){
        # Goddard 
        log-and-show-me "Updating registry settings for GGOESNDEV!" $logfile   
        regedit /s $SetUpFiles\admin\RegistryFiles\SNTPsgoesn_WBU.reg

    } elseif ($domain -eq "sgroup.isi"){
        # Lanham
        log-and-show-me "Updating registry settings for sgroup!" $logfile   
        regedit /s $SetUpFiles\admin\RegistryFiles\SNTPsgoesn_NCWS.reg
        
    } else {
        #  Domain not found
        log-and-show-me "Error: Domain not found!" $logfile 
        Write-Warning "Error: Domain not found!"
    }
   
    log-and-show-me "Finished with installing of SNTPServ!" $logfile 
}
