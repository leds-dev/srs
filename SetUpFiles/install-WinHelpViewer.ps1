######################################################################
#
# Abstract: Install Windows Helper Viewer KB917607. This update is required by 
#		    the USC application for the online help. 
# Author:
# Fred Shaw
# Kratos Technology and Training Solutions
# 5200 Philadelphia Way
# Lanham, Maryland 20706
# www.integ.com
# 
# Creation Date:  04/05/2014
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by 
# Kratos Technology and Training Solutions
# under U.S. Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights  
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
# .SYNOPSIS 
#     Install Windows Helper Viewer KB917607. This update is required by
#     the USC application for the online help.
# 
#
# 
# .DESCRIPTION
# install-EpochClient provides unattended installation of Epoch client.
#
#>


#dot source logging.ps1 to include common logging functions
. .\common.ps1

function install-WinHelpViewer
{
    param([string]$installer_exe,
          [string]$logfile,
          [string]$app_name,
          [string]$app_display_name,
          [string]$install_dir,
          [string]$current_user)

    log-and-show-me "Starting installation of $app_name ..." $logfile  

    $installed=get-installed $app_display_name
    if ( -not $installed )  {
         
        #  Invoke-expression "$installer_exe /i `"$install_dir\$app_name`" /qn USERNAME=$username | out-null" 
        Invoke-expression "$installer_exe `"$install_dir\$app_name`" /quiet /norestart "  
        
    } else {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }
    log-and-show-me "Finished with installing of $app_name!" $logfile 
}
