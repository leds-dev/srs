######################################################################
#
# Abstract: Install Xming silently using all default options
# of the installer.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
# .SYNOPSIS 
# Install Xming and related tool set silently using all default options
# of the installer.
#
# 
# .DESCRIPTION
# install-Xming provides unattended installation of Xming tool set.
#
# .Example 
# PS > .\install-Xming.ps1 $installer_exe $logfile $app_name \
#         $app_display_name $install_dir
#
#>

#dot source common.ps1 to include common logging functions
. .\common.ps1

function install-Xming
{
    param([string]$installer_exe,
          [string]$logfile,
          [string]$app_display_name,
          [string]$install_dir,
          [string]$arch)

    log-and-show-me "Starting installation of $app_display_name ..." $logfile  

    if ($arch -eq "x32") {
	$installed=get-installed "$app_display_name"
    }
    else {
        $installed=get-64bit-installed "$app_display_name"
    }

    if ( -not $installed )
    {
        Invoke-expression "$install_dir\$installer_exe /LOG=$logfile /VERYSILENT /SP- /NORESTART | out-null"  

        if ( $lastexitcode ) 
        {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
            throw "Running $installer_exe failed!!. See log $logfile for more detail"  
        }
    }
    else 
    {
        log-me "Skipping installing $app_display_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_display_name because it is already installed"  
    }
    log-and-show-me "Finished with installing of $app_display_name!" $logfile 
}

