######################################################################
#
# Abstract: Runs installshiled in silent mode to perform unattended 
# installation.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################
<#
#
# .SYNOPSIS 
# Runs installshiled in silent mode to perform unattended 
# installation.
#
# 
# .DESCRIPTION
# install-installshield runs installer in silent
# mode to automate installation of installshield tools. 
#
#>

#dot source common.ps1 to include common logging functions
. .\common.ps1

function install-installshield
{
    param([string]$installer_exe,
            [string]$logfile,
            [string]$app_name,
            [string]$license_mgr,
            [string]$license_file,
            [string]$install_dir)

        log-and-show-me "Starting installshield 2012 setup ..." $logfile  

        $installed=get-installed $app_name
        if ( -not $installed )
        {
            Invoke-expression "$installer_exe /silent | out-null"  

            $oldPath = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).Path 
            $newPath=$oldPath + ";$install_dir"
            set-itemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $newPath
        

            if ( $lastexitcode )  
            {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
                throw "Running $installer_exe failed!!. See log $logfile for more detail"  
            }

            #license file
            copy -Force $license_file -destination "$install_dir\License.lic"
 
        }
        else
        {
            log-me "Skipping installing $app_name because it is already installed" $logfile  
            Write-Warning "Skipping installing $app_name because it is already installed"  

        }
    log-and-show-me "Finished with setup of installshield 2012!" $logfile 
}
