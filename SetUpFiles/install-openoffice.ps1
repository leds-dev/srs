######################################################################
#
# Abstract: Install openoffice silently using all default options
# of the installer.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################
<#
#
# .SYNOPSIS 
# Install openoffice silently using all default options
# of the installer.
#
# 
# .DESCRIPTION
# install-openoffice provides unattended openoffice installation.  
#
#>

#dot source common.ps1 to include common logging functions
. .\common.ps1

function install-openoffice
{
    param([string]$installer_exe,
          [string]$logfile,
          [string]$app_name)

    log-and-show-me "Starting installation of openoffice ..." $logfile  

    $installed=get-installed $app_name
    if ( -not $installed )
    {
        Invoke-expression "$installer_exe /qr | out-null"  

        if ( $lastexitcode ) 
        {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
            throw "Running $installer_exe failed!!. See log $logfile for more detail"  
        }
    }
    else 
    {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }

    log-and-show-me "Finished with installing openoffice!" $logfile 
}

