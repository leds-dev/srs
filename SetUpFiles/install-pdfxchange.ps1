######################################################################
#
# Abstract: Install PDF Xchange Viewer silently using all default options of 
# the installer.
#
# Author:
# Manan Dalal   
# OSPO/NOAA/NESDIA
# x4076
# manan.dalal@noaa.gov
#  
# Creation Date:  6/30/2015
#
# Revision History:
# $log: $
# 
# Copyrights © 2015, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################
<#
#
# .SYNOPSIS 
# Install PDF XChange Viewer silently using all default options of the installer.
#
# 
# .DESCRIPTION
# install-pdfxchange provides unattended installation of PDF XChange Viewer
#
#>

#dot source common.ps1 to include common logging functions
. .\common.ps1

function install-pdfxchange
{
    param([string]$installer_exe,
          [string]$logfile,
          [string]$app_name)

    log-and-show-me "Starting installation of PDF XChange Viewer ..." $logfile  

    $installed=get-64bit-installed $app_name
    if ( -not $installed )
    {
        #PDF XChange Viewer installer options
        #/VERYSILENT slient mode without user interaction
		#/NORESTART reboot suppress
        #/COMPONENTS components to install
        #/PDFV  make pdf XChange viewer default viewer
        #/LOG  enable error logging
	
        log-me "$installer_exe /VERYSILENT /NORESTART /COMPONENTS= ""PDFViewer, LiveUpdate, Help"" /PDFV /LOG=""$logfile"" | out-null" $logfile
        Invoke-expression "$installer_exe /VERYSILENT /NORESTART /COMPONENTS= ""PDFViewer, LiveUpdate, Help"" /PDFV /LOG=""$logfile"" | out-null"  

        # throw error if lastexitcode not ERROR_SUCCESS or ERROR_SUCCESS_REBOOT_REQUIRED
		if ( $lastexitcode )  
        {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
            throw "Running $installer_exe failed!!. See log $logfile for more detail"  
        }
    }
    else 
    {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }

    log-and-show-me "Finished with installing PDF XChange Viewer" $logfile 
}

