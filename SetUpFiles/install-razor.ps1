######################################################################
#
# Abstract: Install Razor silently using prepared ISS file. The iss 
# file is prepared by running installer with "-r" option.
# of the installer.
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
# .SYNOPSIS 
# Install Razor silently using prepared ISS file. The iss 
# file is prepared by running installer with "-r" option.
# of the installer.
#
# 
# .DESCRIPTION
# install-razor provides unattended installation of RazorPC client.
#
#>

#dot source logging.ps1 to include common logging functions
. .\common.ps1


function install-razor
{
    param([string]$installer_exe,
          [string]$issfile,
          [string]$logdir,
          [string]$logfile,
          [string]$app_name,
          [string]$razorini_file)


    log-and-show-me "Starting installation of RazorPC client ..." $logfile  

    $installed=get-installed $app_name
    if ( -not $installed )
    {
        $tmpfile = [System.IO.Path]::GetTempFileName()

        log-and-show-me "response file $issfile" $logfile  

        Invoke-expression "$installer_exe /s /a /s /sms /f1$issfile /f2$tmpfile | out-null"  

        if ( $lastexitcode )  
        {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
            throw "Running $installer_exe failed!!. See log $logfile for more detail"  
        }

        $timestamp=Get-Date -format yyyyMMdd-HH-mm-ss 
        $razor_log_file=$logdir + "\razor-$timestamp.log"
        mv $tmpfile $razor_log_file
        log-me "Razor installation log is kept in $razor_log_file" $logfile

        $appDataDir=[System.Environment]::GetFolderPath("ApplicationData");

        if ( -not (test-path  "$appDataDir/Razor" ) ) 
        {
            New-Item -Path "$appDataDir/Razor" -ItemType directory | Out-Null  
        }

        cp -force  $razorini_file  "$appDataDir/Razor/Razor.ini"
        sp  "$appDataDir/Razor/Razor.ini" IsReadOnly $false

    }
    else
    {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }

    log-and-show-me "Finished with installing RazorPC client!" $logfile 
}
