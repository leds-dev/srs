######################################################################
#
# Abstract: Install Microsoft Visual Studio 2010 silently using all 
# default choices. 
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
# .SYNOPSIS 
# Install Microsoft Visual Studio 2010 silently using all default 
# choices. 
#
# 
# .DESCRIPTION
# install-vs2010 provides unattended installation of Visual Studio 2010.
# 
#>

#dot source logging.ps1 to include common logging functions
. .\common.ps1

function install-vs2010
{
    param([string]$installer_exe,
          [string]$logfile,
          [string]$full_path_to_vs_iss_file,
          [string]$app_name)

    log-and-show-me "Starting Visual Studio 2010 ..." $logfile  

    $installed=get-installed $app_name
    if ( -not $installed )
    {
        log-and-show-me "Be patient, this could take a while ... " $logfile  
        log-me "$installer_exe /UnattendFile $full_path_to_vs_iss_file | out-null" $logfile 
        Invoke-expression "$installer_exe /UnattendFile $full_path_to_vs_iss_file | out-null"  

        if ( $lastexitcode ) 
        {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
           throw "Running $installer_exe failed!!. See log $logfile for more detail"  
        }
        #call vsvar32.bat to set all env
        Invoke-Expression "& `'C:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\Tools\vsvars32.bat`'"
    }
    else
    {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }
    log-and-show-me "Finished with Visual Studio 2010!" $logfile 
}
