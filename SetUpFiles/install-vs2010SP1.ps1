######################################################################
#
# Abstract: Install Microsoft Visual Studio 2010 SP1 silently using all 
# default choices. 
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
# .SYNOPSIS 
# Install Microsoft Visual Studio 2010 SP1 silently using all default 
# choices. 
#
# 
# .DESCRIPTION
# install-vs2010SP1 provides unattended installation of Visual Studio 2010.
# 
#>

#dot source logging.ps1 to include common logging functions
. .\common.ps1

function install-vs2010SP1
{
    param([string]$installer_exe,
          [string]$logfile,
          [string]$app_name)

    log-and-show-me "Starting Visual Studio 2010 SP1 ..." $logfile  

    $installed=get-installed $app_name
    if ( -not $installed )
    {
        log-and-show-me "Be patient, this could take a while ... " $logfile  
        log-me "$installer_exe /passive /promptrestart | out-null" $logfile 
        #Invoke-expression "$installer_exe /passive /promptrestart | out-null"  
        Invoke-expression "$installer_exe /passive /norestart | out-null"  

        # throw error if lastexitcode not ERROR_SUCCESS or ERROR_SUCCESS_REBOOT_REQUIRED
        if (( $LASTEXITCODE -ne 0 ) -and ( $LASTEXITCODE -ne 3010 )) 
        {
            log-me "$app_name installation failed with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
            throw "Running $installer_exe failed!!. See log $logfile for more detail"  
        }
        elseif ( $LASTEXITCODE -eq 3010 )
        {
            log-me "$app_name installation exited with $lastexitcode" $logfile
            Invoke-expression "net helpmsg $lastexitcode" | tee-object -Variable log
            Write-Output $log | Out-File $logfile -Append
        }
    }
    else
    {
        log-me "Skipping installing $app_name because it is already installed" $logfile  
        Write-Warning "Skipping installing $app_name because it is already installed"  
    }
    log-and-show-me "Finished with Visual Studio 2010 SP1!" $logfile 
}
