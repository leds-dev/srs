This file logs the different changes made to the scripts and what updates need to be made to implement them.  

07-14-2014

1) Merged the checksum cfg file generation into the main setup script. This is the last script executed. It is not automatically called and does not need to be run separately.  The user is prompted to continue when the script executes.  To test, the setup script can be executed and verified that a cfg file is created.  Also when the script executes the user is prompted to continue.  

2) Merged the custom extended help into the main setup script.  It is not automatically called and does not need to be run separately.  
Steps for testing extended help if it has already been installed:
	a) Uninstall the extended help on Control Panel | Programs and Features.
    b) Delete "C:\epochclient\bin\EpExtendedHelp.dll".
	c) Rename "C:\epochclient\bin\EpExtendedHelp.dll.121" to "C:\epochclient\bin\EpExtendedHelp.dll".  This restores the orginal extended help DLL which is installed with the epoch client.
	d) Execute the setup script. 
    e) Verify extended help functions correctly.	
		
	

