﻿######################################################################
#
# Abstract: Windows Powershell installation script for SSGS subsystems
#
# Author:
# Indy Saggu   
# Avaya Government Solutions 
# 12730 Fair Lakes Cir
# Fairfax, VA, 22033.
# Indy.Saggu@avayagov.com
#  
# Creation Date:  4/2/2013
#
# Revision History:
# $log: $
# 
# Disclaimer: 
# Originally developed by Avaya Government Solutions under  U.S. 
# Government sponsorship NOAA Contract SSGS Refresh Support (SRS)
#
# Copyrights © 2012, 2013, 
# Office of Systems Development
# National Environmental Satellite Data Information Service (NESDIS)
# National Oceanic and Atmospheric Administration  (NOAA)
# All rights reserved.  
######################################################################

<#
.SYNOPSIS  
    This Windows powershell script is intended to support installation and 
    configuration of Windows based SSGS subsystems.
.DESCRIPTION
    setup.ps1 powershell script provides the automated installation of SSGS 
    Windows based subsystems.  The script provides unattended installation 
    of required COTS software for intended subsystem,  
    then installs subsystem silently, and finally configures necessary 
    settings for SSGS operational environment.  The script provides command 
    line switch for users to alter installation behavior if desired. 
.PARAMETER install 
    Required parameter, indicating type of installation to be performed, 
    valid SSGS installation types are either: dev, or opr.
.PARAMETER site 
    Required parameter, indicating networking, development, or deployment
    environment in which this installation is intended, 
    valid SSGS site names are: socc-ops, socc-dev 
.PARAMETER pkg_type
    Required parameter, indicating applicable SSGS subystems,
    valid SSGS windows subsystems are COTS_only, custom_only and epoch_only.
.EXAMPLE 
    Install only components required to build at socc-dev network site.
    PS > .\setup.ps1 -install opr -site socc-dev  
    PS > .\setup.ps1 -install opr -site socc-dev COTS_only 
    PS > .\setup.ps1 -install opr -site socc-dev custom_only 
    PS > .\setup.ps1 -install opr -site socc-dev epoch_only
    PS > .\setup.ps1 -install build -site socc-dev COTS_only 
#>


Param(
    [Parameter(
        Mandatory=$true,
        Position=0,
        ValueFromPipeline=$true,
        ValueFromPipelineByPropertyName=$true,
        HelpMessage="Enter type of installation to be performed, valid strings are: build, opr")]
    [validateset("build", "opr")] 
    [string]
    $install,

	
    [Parameter(
            Mandatory=$true,
            Position=1,
            ValueFromPipeline=$true,
            ValueFromPipelineByPropertyName=$true,
            HelpMessage="Enter site name where this installation is to be done, valid string are: socc-opr, 
			socc-dev. wcda-dev, wcda-opr, wfda-opr, wbu-opr")]
    [validateset("socc-opr", "socc-dev", "wcda-dev", "wcda-opr", "wfda-opr", "wbu-opr")] 
    [string]
    $site,
    
    [Parameter(
        Mandatory=$false,
        Position=2,
        ValueFromPipeline=$true,
        ValueFromPipelineByPropertyName=$true,
        HelpMessage="Enter type of pkg_type, valid strings are: COTS_only, custom_only, epoch_only")]
    [validateset("COTS_only", "custom_only", "epoch_only")] 
    [string]
    $pkg_type
)

#Running this script requires admin privilage
$currentUser = [Security.Principal.WindowsIdentity]::GetCurrent()
$admin=(New-Object Security.Principal.WindowsPrincipal $currentUser).IsInRole([Security.Principal.windowsBuiltinrole]::Administrator)      
if ( -not $admin) 
{
    "Admin rights are required for this script"
     exit 0
}

## Running this script is NOT allowed through Remote Desktop Connection
#$hostip = [System.Net.Dns]::GetHostAddresses("$MYHOSTNAME")[1].IPAddressToString
#$c = netstat -aonp TCP | select-string "ESTABLISHED" | select-string "${hostip}:3389"
#if ($c)
#{
#    "Connected through RDP, install scripts should be run through Console Port"
#    exit 0
#}

$SetUpFile=(Resolve-Path . -ea 0).Path
$ThirdPartySoftware=(Resolve-Path "..\ThirdPartySoftware" -ea 0).Path
$InstallScripts=(Resolve-Path "..\InstallScripts" -ea 0).Path
$StagingArea=(Resolve-Path "..\InstallScripts" -ea 0).Path

#source the logging script to include functions
. .\common.ps1
. .\install-razor.ps1
. .\install-putty.ps1
. .\install-vs2010.ps1
. .\install-vs2010SP1.ps1
. .\install-installshield.ps1
#. .\install-BCGControlBar.ps1

#. .\install-adobereader.ps1
. .\install-pdfxchange.ps1
. .\install-SNTPServ.ps1
. .\install-ActivePerl.ps1
. .\install-openoffice.ps1
. .\install-GOLD.ps1
. .\install-GPL_Ghostscript
. .\install-NotePad
#. .\install-Free_Desktop_Clock
. .\install-WinMerge.ps1
. .\install-ActiveClient.ps1
. .\install-Xming.ps1

#source to include custom functions
# . .\install-java.ps1
. .\install-Application.ps1
. .\install-ExHelp.ps1
. .\install-USC.ps1
. .\GTACSFinalConfig.ps1

#source to include epoch functions
. .\install-EpochClient.ps1
. .\install-NuTCRACKER.ps1
. .\install-ABE.ps1
# . .\epochconfig.ps1
. .\Init_Viewer_ShortCuts.ps1
. .\install-WinHelpViewer.ps1
. .\GenerateCheckSum.ps1

#setup the logging directory 
$logDir = [System.Environment]::GetFolderPath("CommonApplicationData") 
$logDir = Join-Path -Path $logDir -Childpath "Gtacs"

if(-not(Test-Path -path $logdir) )
{New-Item -Path $logdir -ItemType directory | Out-Null }  

#initialize the logfile to null
$logfile=$null

#define the hostname 
$MYHOSTNAME=hostname

#define the ostype 
$OSTYPE=(Get-WmiObject Win32_OperatingSystem).Caption

#keep track of starting time
$startTime = (Get-Date).tostring()
$logfiletimestamp=Get-Date -format yyyyMMdd-HH-mm-ss 

$Myscript=resolve-path $MyInvocation.InvocationName

$msg = @"
Running script: $Myscript  starting $startTime as $env:username on $MYHOSTNAME.
"@

$seperator = @"
##############################################################################
"@

$wshShell=New-Object -ComObject "WScript.Shell"
$DESKTOP_DIR = $wshShell.SpecialFolders.Item("AllusersDesktop")

#####################################################################################################
#
# main entry point of the script
#
# 

$logfile= Join-Path -Path $logDir -childpath "gtacs-build-install-$logfiletimestamp.log"
#$logfile= Join-Path -Path "$InstallScripts" -childpath "gtacs-build-install.log"
            
log-and-show-me $msg $logfile 
log-and-show-me "Check $logfile if needed"  $logfile

#import WASP package
import-module $ThirdPartySoftware\WASP\WASP.dll

switch ($install) 
{
    "build" {
        if (( $pkg_type -eq "" ) -or  ( $pkg_type -eq "custom_only" ) -or ( $pkg_type -eq "epoch_only" ))
        {
            $Host.UI.WriteErrorLine("For build install type ONLY pkg_type allowed is COTS_only !!!")
            exit 0
        }
        elseif ( $pkg_type -eq "COTS_only" )
        {
    	    # check installshield lience because it is node locked license, it is only applied 
            # to one host only   
            if ( -not ( test-path -pathtype leaf ".\admin\tps\installshield\license.$MYHOSTNAME") )
            {
                $Host.UI.WriteErrorLine("Installshield license not avalaible for this host.  Aborting installation !!!")
                exit 0
            }

           log-and-show-me $seperator $logfile
		   if (( test-path -pathtype leaf ".\admin\tps\razor\Razor.ini.$site")){
				$fullpath_to_razor_iss_file=(Resolve-Path .\admin\tps\razor\setup.iss -ea 0).Path
				$fullpath_to_razor_ini_file=(Resolve-Path ".\admin\tps\razor\Razor.ini.$site" -ea 0).Path 
				# Made a generic file for all sites
				# $fullpath_to_razor_ini_file=(Resolve-Path ".\admin\tps\razor\Razor.ini" -ea 0).Path
		   
				install-razor "$ThirdPartySoftware\RazorPC\setup.exe"  `
                          "$fullpath_to_razor_iss_file" `
                          "$logdir" `
                          "$logfile"  `
                          'RazorPC*' `
                          "$fullpath_to_razor_ini_file" 
			} else {
				log-me "ini file not found: .\admin\tps\razor\Razor.ini.$site"  $logfile
                Write-Warning "ini file not found: .\admin\tps\razor\Razor.ini.$site"
			}

           log-and-show-me $seperator $logfile
           install-putty "$ThirdPartySoftware\putty\putty-0.64-installer.exe" `
                          "$logfile" `
                          'putty *' `
                          'C:\Program Files (x86)\PuTTY'

           log-and-show-me $seperator $logfile
           if (( test-path -pathtype leaf ".\admin\tps\vs2010\vs2010pro-setup-ini.$site")){
    		   $fullpath_to_vs_iss_file =(Resolve-Path .\admin\tps\vs2010\vs2010pro-setup-ini.$site -ea 0).Path
    		   # Made a generic file for all sites.
    		   # $fullpath_to_vs_iss_file =(Resolve-Path .\admin\tps\vs2010\vs2010pro-setup-ini -ea 0).Path
               install-vs2010 "$ThirdPartySoftware\vs2010\vsPro2010\Setup\setup.exe" `
                              $logfile `
                              $fullpath_to_vs_iss_file `
                              'Microsoft Visual Studio 2010 Professional - ENU'
           } else {
              log-me "ini file not found: .\admin\tps\vs2010\vs2010pro-setup-ini.$site"  $logfile
              Write-Warning "ini file not found: .\admin\tps\vs2010\vs2010pro-setup-ini.$site"  
		   }	

           log-and-show-me $seperator $logfile
           install-vs2010SP1 "$ThirdPartySoftware\vs2010\vs2010SP1\Setup.exe" `
                          $logfile `
                          'Microsoft Visual Studio 2010 Service Pack 1'

           log-and-show-me $seperator $logfile
   	       $license_file=(Resolve-Path ".\admin\tps\installshield\$MYHOSTNAME.lic" -ea 0).Path
           install-installshield "$ThirdPartySoftware\InstallShield\InstallShieldProfessional\Disk1\InstallShield2012SPRProfessionalComp.exe" `
                          "$logfile" `
                          'InstallShield 2012 Spring' `
                          'C:\Program Files (x86)\InstallShield\2012Spring\System\TSConfig.exe' `
                          "$license_file" `
                          'C:\Program Files (x86)\InstallShield\2012Spring\System' 

           log-and-show-me $seperator $logfile
           install-NotePad 'npp.6.7.9.2.Installer.exe' `
				          "$logfile" `
				          'NotePad++' `
				          "$ThirdPartySoftware\NotePad++" `
				          'x32' `
				          "epoch"

           log-and-show-me " " $logfile
           log-me "Follow the instructions to manually install BCGControlBar " $logfile
           log-me "and build DLLs and Libraries. " $logfile
           Write-Host "Follow the instructions to manually install BCGControlBar " -foregroundcolor "yellow"
           Write-Host "and build DLLs and Libraries. " -foregroundcolor "yellow"
           log-and-show-me " " $logfile

 		   log-and-show-me $seperator $logfile
           install-GOLD 'msiexec' `
		  		          "$logfile" `
		  		          'Setup.msi' `
		   		          'GOLD Parser Builder' `
		  		          "$ThirdPartySoftware\GOLD" `
		  		          'epoch'
                          
            # Create directory if does not exist
            if (!([IO.Directory]::Exists("C:\Program Files (x86)\BGinfo")))
            {
                 New-Item "C:\Program Files (x86)\BGinfo\" -type directory | out-null
            }

            # Get the all users Start Program directory path 	        
	        $WshShell = New-Object -comObject WScript.Shell
	        $Path = $WshShell.SpecialFolders.item("AllUsersPrograms")

            #copy Bginfo utility to startup for all users
            copy -Force "$ThirdPartySoftware\BGinfo\Bginfo.exe" "C:\Program Files (x86)\BGinfo\Bginfo.exe"
            copy -Force "$ThirdPartySoftware\BGinfo\Bginfo.bgi" "C:\Program Files (x86)\BGinfo\Bginfo.bgi"
            copy -Force "$ThirdPartySoftware\BGinfo\Bginfo.bat" "$Path\Startup\Bginfo.bat"                          
            log-and-show-me "Setup Bginfo utility in Startup" $logfile

            # Copy Snipping Tool lnk to Desktop
            copy "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Accessories\Snipping Tool.lnk" C:\Users\Public\Desktop
            log-and-show-me "Setup Snipping Tool lnk to Desktop" $logfile

			# This was moved to the startup script
            # copy Launch T-Clock to Startup lnk
            # copy -Force "$SetUpFiles\admin\NewStartUp_Links\Clock.lnk" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Win7 T-Clock 2010.lnk" 
            # log-and-show-me "Setup T-Clock 2010 lnk in Startup" $logfile
         }
    }
    
    "opr" {
        if (( $pkg_type -eq "" ) -or ( $pkg_type -eq "COTS_only" ))
        {
           log-and-show-me $seperator $logfile
           install-pdfxchange  "$ThirdPartySoftware\PDFXChange\PDFXVwer" `
                          "$logfile" `
                          'PDF-Viewer'
      
           log-and-show-me $seperator $logfile
           install-SNTPServ 'msiexec' `
				          "$logfile" `
			              'SNTPServ_x64.5.1.15.msi' `
				          'SNTPServ' `
				          "$ThirdPartySoftware\DillobitsSNTPServer" `
				          'epoch'
						
		   license-SNTPServ

          log-and-show-me $seperator $logfile
          install-openoffice "$ThirdPartySoftware\OpenOffice\setup.exe" `
                         "$logfile" `
                         'OpenOffice 4.1.1'
          
           log-and-show-me $seperator $logfile
           install-putty "$ThirdPartySoftware\putty\putty-0.64-installer.exe" `
                          "$logfile" `
                          'putty *' `
                          'C:\Program Files (x86)\PuTTY'

           log-and-show-me $seperator $logfile
           install-GPL_Ghostscript "$ThirdPartySoftware\GPLGhostscript\gs906w32.exe" `
				          "$logfile" `
				          'GPL Ghostscript' `
                          'C:\Program Files (x86)\gs\gs9.06' `
				          'x32' `

           log-and-show-me $seperator $logfile
           install-NotePad 'npp.6.7.9.2.Installer.exe' `
				          "$logfile" `
				          'NotePad++' `
				          "$ThirdPartySoftware\NotePad++" `
				          'x32' `
				          "epoch"
          
          
             
			# Moved to the startup script  
            # log-and-show-me $seperator $logfile
            # if (!([System.IO.File]::Exists("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Win7 T-Clock 2010.lnk"))) {
            #   $objShell = New-Object -ComObject ("WScript.Shell")
            #    $objShortCut = $objShell.CreateShortcut("C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup" + "\Win7 T-Clock 2010.lnk")
            #    $objShortCut.TargetPath = "C:\Program Files (x86)\Integral Systems\T-Clock 2010\Clock.exe"
            #    $objShortCut.Save()
            #    log-and-show-me "Launch T-Clock 2010 lnk created" $logfile
            # } else {
            #    log-and-show-me "Launch T-Clock 2010 lnk already exists" $logfile
            #    Write-Host "Launch T-Clock 2010 lnk already exists" -foregroundcolor "yellow"
            #}
           
           log-and-show-me $seperator $logfile
           install-WinMerge 'WinMerge-2.14.0-Setup.exe' `
                          "$logfile" `
                          'WinMerge 2.14.0' `
                          "$ThirdPartySoftware\WinMerge" 

          # need to run from Console port.
          # ActivClient doesn't install via Remote Desktop Connection
          log-and-show-me $seperator $logfile
          install-ActiveClient 'msiexec' `
                          "$logfile" `
                          'ActivClient x64 7.0.1.msi' `
                          'ActivClient x64' `
                          "$ThirdPartySoftware\ActivClient\7.0.1\Product" `
                          'epoch'
                                
           log-and-show-me $seperator $logfile
           install-Xming 'Xming-6-9-0-31-setup.exe' `
    				      "$logfile" `
    	   			      'Xming 6.9.0.31' `
			     	      "$ThirdPartySoftware\Xming" `
				          'x32' `
				          '"$currentUser"'
        }
        if (( $pkg_type -eq "" ) -or ( $pkg_type -eq "custom_only" )) {
           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
        				  "$logfile" `
				          'ChecksumSSGS.msi' `
				          'ChecksumSSGS' `
			           	  "$StagingArea\Checksum" `
				          '"$currentUser"'

          log-and-show-me $seperator $logfile
          install-Application 'msiexec' `
				          "$logfile" `
				          'DirMapper.msi' `
				          'DirMapper' `
				          "$StagingArea\DirMapper" `
				          '"$currentUser"'

          log-and-show-me $seperator $logfile
          install-Application 'msiexec' `
				          "$logfile" `
				          'DumpTool.msi' `
				          'DumpTool' `
				          "$StagingArea\DumpTool" `
				          '"$currentUser"'

          log-and-show-me $seperator $logfile
          install-Application 'msiexec' `
		   	          "$logfile" `
		   		      'ImageEditor.msi' `
			          'ImageEditor' `
			          "$StagingArea\ImageEditor" `
			          '"$currentUser"'

           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'Get Client.msi' `
				          'Get Client' `
				          "$StagingArea\Get-Client" `
				          '"$currentUser"'

           log-and-show-me $seperator $logfile
           install-USC 'msiexec' `
				          "$logfile" `
				          'USC.msi' `
				          'USC ' `
				          "$StagingArea\USC" `
				          '"$currentUser"'
                          
           # Install win help viewer needed by USC to display online help               
           log-and-show-me $seperator $logfile
           install-WinHelpViewer 'wusa.exe' `
         				  "$logfile" `
		 		          'Windows6.1-KB917607-x64.msu' `
		 		          'Windows6.1-KB917607-x64' `
		 	           	  "$ThirdPartySoftware\MS_Help_Vwr" `
        	             '"$currentUser"'
                 
           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'SSGS_Util.msi' `
				          'SSGSUtilInstall' `
				          "$StagingArea\Archive_Converter" `
				          '"$currentUser"'

           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
				          "$logfile" `
				          'SchedMgr.msi' `
				          'SchedMgr' `
				          "$StagingArea\SchedMgr" `
				          '"$currentUser"' 
			install-Application 'msiexec' `
        				  "$logfile" `
				          'T-Clock 2010.msi' `
				          'T-Clock 2010' `
			           	  "$StagingArea\T-Clock 2010" `
				          "$currentUser"
                          
           # Create directory if does not exist
           if (!([IO.Directory]::Exists("C:\EPOCHClient\output")))
           {
                log-and-show-me $seperator $logfile
                New-Item C:\EPOCHClient\output\ -type directory | out-null
           }
         }
        if (( $pkg_type -eq "" ) -or ( $pkg_type -eq "epoch_only" ))
        {
           log-and-show-me $seperator $logfile
           #setup the tmp directory 
           $tmpDir = [System.Environment]::GetFolderPath("LocalApplicationData") 
           $tmpDir = Join-Path -Path $tmpDir -Childpath "Temp"
           install-NuTCRACKER "$ThirdPartySoftware\EPOCH_Client_1.21\NuTCRACKER\nutc.v9.4.patch1.exe"  `
                          "$logdir" `
                          "$logfile"  `
                          'MKS Platform Components 9.x' `
                          "$tmpDir"
                          
           log-and-show-me $seperator $logfile
	       $fullpath_to_epoch_iss_file=(Resolve-Path .\admin\tps\EpochClient\epochclient.iss -ea 0).Path
           $fullpath_to_epoch_reg_file=(Resolve-Path ".\admin\tps\EpochClient\epoch_sgtacs2_backup.reg" -ea 0).Path 
           $Epoch_logfile= Join-Path -Path $logDir -childpath "Epoch-client-install-$logfiletimestamp.log"
           install-EpochClient "$ThirdPartySoftware\EPOCH_Client_1.21\EPOCH_Client\setup.exe"  `
                          "$fullpath_to_epoch_iss_file" `
                          "$logdir" `
                          "$logfile" `
                          "$Epoch_logfile"  `
                          'EPOCH Client' `
                          "$fullpath_to_epoch_reg_file"
                          
           log-and-show-me "Epoch Client installation log is in $Epoch_logfile" $logfile
	       if ((![System.IO.File]::Exists("C:\EPOCHClient\bin\EpServiceCtrl.exe")))  { 
                log-and-show-me $seperator $logfile
				copy $ThirdPartySoftware\EPOCH_Client_1.21\EpServiceCtrl.exe C:\EPOCHClient\bin\EpServiceCtrl.exe
           }
           
           if (([System.IO.File]::Exists("C:\EPOCHClient\bin\EpGMS.dll")) -And
           ([System.IO.File]::Exists("$ThirdPartySoftware\EPOCH_Client_1.21\EpGMS.dll")) -And 
           -NOT ([System.IO.File]::Exists("C:\EPOCHClient\bin\EpGMS.dll.121"))) {
				log-and-show-me $seperator $logfile
                mv C:\EPOCHClient\bin\EpGMS.dll C:\EPOCHClient\bin\EpGMS.dll.121 
                copy $ThirdPartySoftware\EPOCH_Client_1.21\EpGMS.dll C:\EPOCHClient\bin\EpGMS.dll
           }
		   
		   if (([System.IO.File]::Exists("C:\EPOCHClient\bin\SSAPISrvc.exe")) -And
           ([System.IO.File]::Exists("$ThirdPartySoftware\EPOCH_Client_1.21\SSAPISrvc.exe")) -And 
           -NOT ([System.IO.File]::Exists("C:\EPOCHClient\bin\SSAPISrvc.exe.121"))) {
				log-and-show-me $seperator $logfile
                mv C:\EPOCHClient\bin\SSAPISrvc.exe C:\EPOCHClient\bin\SSAPISrvc.exe.121 
                copy $ThirdPartySoftware\EPOCH_Client_1.21\SSAPISrvc.exe C:\EPOCHClient\bin\SSAPISrvc.exe
           }
		   
		   if (([System.IO.File]::Exists("C:\EPOCHClient\bin\proc.exe")) -And
		   ([System.IO.File]::Exists("$ThirdPartySoftware\EPOCH_Client_1.21\proc.exe")) -And
		   -NOT ([System.IO.File]::Exists("C:\EPOCHClient\bin\proc.exe.121"))) {
				log-and-show-me $separator $logfile
				mv C:\EPOCHClient\bin\proc.exe C:\EPOCHClient\bin\proc.exe.121
				copy $ThirdPartySoftware\EPOCH_Client_1.21\proc.exe C:\EPOCHClient\bin\proc.exe
			}
		   
		   if (([System.IO.File]::Exists("C:\EPOCHClient\bin\ssnt.dll")) -And
           ([System.IO.File]::Exists("$ThirdPartySoftware\EPOCH_Client_1.21\ssnt.dll")) -And 
           -NOT ([System.IO.File]::Exists("C:\EPOCHClient\bin\ssnt.dll.121"))) {
				log-and-show-me $seperator $logfile
                mv C:\EPOCHClient\bin\ssnt.dll C:\EPOCHClient\bin\ssnt.dll.121 
                copy $ThirdPartySoftware\EPOCH_Client_1.21\ssnt.dll C:\EPOCHClient\bin\ssnt.dll
           }
           
           # Create directory if does not exist
           if (!([IO.Directory]::Exists("C:\EPOCHClient\output")))
           {
                log-and-show-me $seperator $logfile
                New-Item C:\EPOCHClient\output\ -type directory | out-null
           }
		   
           log-and-show-me $seperator $logfile
           install-ExHelp 'msiexec' `
		             "$logfile" `
		             'ExHelp.msi' `
		             'ExHelp' `
		             "$StagingArea\EpochExtendedHelp" `
		            '"$currentUser"'             

           log-and-show-me $seperator $logfile
           install-ABE "$ThirdPartySoftware\ABE\7.23.3\abe\windows\abe_7_23_3.exe"  `
                          "$logdir" `
                          "$logfile"  `
                          'ABE' `
                          "C:\" `
                          "$ThirdPartySoftware\ABE\7.23.3\vni\utilwin\vcredist_x86.exe"
            
			#	This is taken care of in the ABEConfig.ps1 script.
           # log-and-show-me $seperator $logfile
           # install-Application 'msiexec' `
			#				"$logfile" `
			#	          'ABETools.msi' `
			#	          'ABETools' `
			#	          "$StagingArea\ABETools" `
			#	          '"$currentUser"'						  
                          
           log-and-show-me $seperator $logfile
           install-Application 'msiexec' `
		              "$logfile" `
		   	          'EpochConfigr.msi' `
		   	          "Epoch Configuration" `
		   	          "$StagingArea\EpochConfigr" `
		   	          '"$currentUser"'
						  
		    log-and-show-me $seperator $logfile
            install-Application 'msiexec' `
		   		          "$logfile" `
		   		          'View_Init.msi' `
		   		          'Viewer_init_Files' `
		   		          "$StagingArea\View_Init" `
		   		          '"$currentUser"'
    	
		   # Last step need to configure image.
		   # These scripts are contained in GTACSFinalConfig.ps1 & Init_Viewer_ShortCuts.ps1
		   
		   log-and-show-me $seperator $logfile
		   install-Registrykeys $logfile
           log-and-show-me $seperator $logfile 
		   install-DeskTopShortcuts $logfile
           log-and-show-me $seperator $logfile
           install-StartMenuShortCuts $logfile
           log-and-show-me $seperator $logfile 
           install-GSS_ShortCuts $logfile
           log-and-show-me $seperator $logfile
           install-GSS_Files $logfile
           log-and-show-me $seperator $logfile
           install-Init_File_Viewer
           log-and-show-me $seperator $logfile
		   Move-EpochClient_ShortCuts
		   log-and-show-me $seperator $logfile
        }  
        
        # Create directory if does not exist
        if (!([IO.Directory]::Exists("C:\Program Files (x86)\BGinfo")))
        {
             New-Item "C:\Program Files (x86)\BGinfo\" -type directory | out-null
        }

        # Get the all users Start Program directory path 	        
	    $WshShell = New-Object -comObject WScript.Shell
	    $Path = $WshShell.SpecialFolders.item("AllUsersPrograms")

        #copy Bginfo utility to startup for all users
        copy -Force "$ThirdPartySoftware\BGinfo\Bginfo.exe" "C:\Program Files (x86)\BGinfo\Bginfo.exe"
        copy -Force "$ThirdPartySoftware\BGinfo\Bginfo.bgi" "C:\Program Files (x86)\BGinfo\Bginfo.bgi"
		copy -Force "$ThirdPartySoftware\BGinfo\Bginfo.bat" "C:\Program Files (x86)\BGinfo\Bginfo.bat"
		
        copy -Force "$ThirdPartySoftware\BGinfo\Bginfo.bat" "$Path\Startup\Bginfo.bat"
        log-and-show-me "Setup Bginfo utility in Startup" $logfile

		# This was moved to the startup script 
        # copy Launch T-Clock to Startup lnk
        # copy -Force "$SetUpFiles\admin\NewStartUp_Links\Clock.lnk" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Launch T-Clock 2010.lnk" 
        # log-and-show-me "Setup T-Clock 2010 lnk in Startup" $logfile
        
        # Copy Snipping Tool lnk to Desktop
        copy -Force "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Accessories\Snipping Tool.lnk" C:\Users\Public\Desktop
        log-and-show-me "Setup Snipping Tool lnk to Desktop" $logfile
        
        # copy SSGS shortcut to Startup lnk
		# This startup script is installed in SSGSUtil 
        # copy -Force "$SetUpFiles\admin\Scripts\StartupScripts\9UpdateSSGSShortcuts.bat" "C:\SSGS\Scripts\StartupScripts\"
        copy -Force "$SetUpFiles\admin\NewStartUp_Links\UpdateSSGSShortcuts.lnk" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\" 
        log-and-show-me "Setup UpdateSSGSShortcuts lnk in Startup" $logfile
        # copy Launch DirMapper to Startup lnk
        copy -Force "$SetUpFiles\admin\NewStartUp_Links\Launch DirMapper.lnk" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\" 
        log-and-show-me "Setup Launch DirMapper lnk in Startup" $logfile
		# This was moved to the startup script 
        # copy Launch T-Clock to Startup lnk
        # copy -Force "$SetUpFiles\admin\NewStartUp_Links\Clock.lnk" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\Win7 T-Clock 2010.lnk" 
        # log-and-show-me "Setup T-Clock 2010 lnk in Startup" $logfile
        # copy USCsetenv to Startup lnk
        copy -Force "$SetUpFiles\admin\NewStartUp_Links\USCsetenv.lnk" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\" 
        log-and-show-me "Setup USCsetenv lnk in Startup" $logfile

		# copy bringforward to Startup lnk
        copy -Force "$SetUpFiles\admin\NewStartUp_Links\BringForward.lnk" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\" 
        log-and-show-me "Setup BringForward lnk in Startup" $logfile

		# Save off the Linked Clone tools to the C: drive
        Copy-LinkedCloneTools		
		# log-and-show-me "Save off the Linked Clone tools to the C: drive" $logfile
		
        log-and-show-me $seperator $logfile
		# Uncomment the next lines to install ActivePerl-5.16.3
		# Make sure ActivePerl-5.8.7 is commented out
        # install-ActivePerl 'msiexec' `
		#	          "$logfile" `
		#	          'ActivePerl-5.16.3.1603-MSWin32-x86-296746.msi' `
		#	          'ActivePerl 5.16.3 Build 1603' `
		#	          "$ThirdPartySoftware\ActivePerl" `
		#	          'epoch'
		
		# Comment out these lines if ActivePerl-5.16.3 is being installed
		install-ActivePerl 'msiexec' `
		            "$logfile" `
			        'ActivePerl-5.8.7.815-MSWin32-x86-211909.msi' `
			        'ActivePerl 5.8.7 Build 815' `
			        "$ThirdPartySoftware\ActivePerl" `
			        'epoch'
					
        # GenerateCheckSum $logfile         
		 
        log-and-show-me $seperator $logfile
        log-and-show-me "Follow the instructions to " $logfile
        log-and-show-me "    1) Manually install PV-WAVE " $logfile
        # log-and-show-me "    2) Run EpExtendedHelp.ps1 " $logfile
		log-and-show-me "    2) License EPOCH client " $logfile
        log-and-show-me "    3) Reboot " $logfile
        log-and-show-me "    4) Run EpochConfig.ps1 " $logfile
        log-and-show-me "    5) Run ABEConfig.ps1 " $logfile
        log-and-show-me "    6) Reboot " $logfile
        log-and-show-me "    7) Generate checksum " $logfile
        log-and-show-me $seperator $logfile
                      
    }
    Default {
        Write-Error "Invalid install encountered"
    }
}

#now we should be done
$endTime = (Get-Date).tostring()

$msg = @"
Finished script $endTime. Total script time was $((New-TimeSpan -Start $startTime -End $endtime).totalSeconds) seconds. 
"@

log-and-show-me $msg $logfile 
log-and-show-me "Check $logfile if needed"  $logfile

